<!DOCTYPE html>
<html>
<head><meta http-equiv="X-UA-Compatible" content="requiresActiveX=true" /><title>
	MeadCo's ScriptX :: Working with Frames - Left frame
</title>
<head>
<style type="text/css" media="print">
body {
        margin: 0;
        padding: 0;
        background-color: #FAFAFA;
        font: 12pt "Tahoma";
    }
    * {
        box-sizing: border-box;
        -moz-box-sizing: border-box;
    }
    .page {
        width: 11in;
        min-height: 8in;
        padding: 0;
        background: white;
    }
    .subpage {
         padding: 1mm;
       /* height: 256mm;*/
    }
    .page_manifest {
        width: 17in;
        min-height: 10in;
        padding: 0;
        background: white;
    }
    .subpage_manifest {
        padding: 1mm;
       /* height: 256mm;*/
    } 
    
    .page_ship {
        width: 17in;
        min-height: 10in;
        padding: 0cm;
        background: white;
    }
    .subpage_ship {
       padding: 1mm;
       /* height: 256mm;*/
    }     
    
    @page {
	<!--
        size: auto; 
        margin: 0;
		-->
    }
    @media print {
        .page {
            orientation: landscape;
            margin: 0;
            border: initial;
            border-radius: initial;
            width: 11in;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
        .page_manifest {
            orientation: landscape;
            margin: 0;
            border: initial;
            border-radius: initial;
            width: 11in;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
        .page_ship {
            orientation: landscape;
            margin: 0;
            border: initial;
            border-radius: initial;
            width: 11in;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
    }
</style>
<script type="text/javascript" >
function abc() {
   	factory.printing.portrait = false;
    factory.printing.leftMargin = 1.0;
    factory.printing.topMargin = 1.0;
    factory.printing.rightMargin = 1.0;
    factory.printing.bottomMargin = 1.0;
    factory.printing.print(false);

}
</script>
 </head>
<body role="document" cz-shortcut-listen="true"><input type="button" onclick="abc();" value="Print"/>
<object id=factory style="display:none" classid="clsid:1663ed61-23eb-11d2-b92f-008048fdd814"
  codebase="http://localhost/test/redist/smsx.cab#Version=7,5,0,20 "><param name="template" value="MeadCo://baseEx" />
</object>
<div class="book">
    <div class="page">
        <div class="subpage">This is test content. This is test content. This is test content. This is test content. This is test content. This is test content. This is test content. This is test content. This is test content. This is test content. This is test content. This is test content. This is test content. This is test content. This is test content. This is test content. This is test content. This is test content. This is test content. This is test content. This is test content. This is test content. This is test content. This is test content. 
</div>    
    </div>
    <div class="page_manifest">
        <div class="subpage_manifest"><img src="T715-01490051.TIF" width="1344px" height="864px">
        </div>  
    </div>

    <div class="page_ship">
        <div class="subpage_ship"><img src="luciana.jpg" width="1152px;" height="864px;">
        </div>  
    </div>
    
    
</div>
</body>
</html>