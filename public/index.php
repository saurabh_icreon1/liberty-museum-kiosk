<?php

/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));
error_reporting(0);
// Setup autoloading
require 'init_autoloader.php';
date_default_timezone_set('UTC');
define('SITE_URL', 'http://' . $_SERVER['SERVER_NAME']);
define('SITE_URL_HTTPS', 'https://' . $_SERVER['SERVER_NAME']);
define('DOMAIN_NAME', 'www.'.$_SERVER['SERVER_NAME']);
define('DATE_TIME_FORMAT', date("Y-m-d H:i:s"));
define('EXPORT_DATE_TIME_FORMAT', rand(time() - 1, time()));
define('LIVE_SITE_URL', 'http://www.libertyellisfoundation.org');

// Run the application!
Zend\Mvc\Application::init(require 'config/application.config.php')->run();

function asd($params,$exit=true){
    echo "<pre>";
    print_r($params);
    if($exit)
        exit;
}

