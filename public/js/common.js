function createContact(){
    showAjxLoader();
    $.ajax({
        type:"GET",
        data:{},
        url:"/create-popup-contact",
        success:function(
            response){
            if(!checkUserAuthenticationAjx(response))return false;
            else{
                hideAjxLoader();
                $.colorbox.close();
                $.colorbox({
                    width:"900px",
                    height:"500px",
                    iframe:true,
                    href:"/create-popup-contact"
                })
            }
        }
    })
}
$(document).ready(function(){
    $.ajaxSetup({
        beforeSend:function(){
            
            if(this.url != '/get-campaign-code' && this.url !='/get-tra-campaigns' && this.url !='/get-contacts' && this.url !='/get-postal-lookup' && this.url !='/get-country-lookup'){
                showAjxLoader();
            }
        },
        error:function(){
            hideAjxLoader();            
        },
        complete:function(result){
            hideAjxLoader();
        }
    });
    var firstBreadCrumbAnchor=$(".breadcrumb a").first();
    $(".breadcrumb").prepend($(firstBreadCrumbAnchor).text());
    $(firstBreadCrumbAnchor).remove();
    $(".e1").select2({});
    $(".e2").customInput();
    $($(".e4")).each(function(i){
        if($(this).is(
            "[type=checkbox],[type=radio]")){
            var input=$(this),input_class=input.attr("class").split(" ");
            input.attr({
                id:input_class
                ["1"]+"_"+i
            });
            var label=$("label[for="+input.attr("id")+"]"),outerLabel=input.parent(),outerLabelParent=input.parent().
            parent(),inputType=input.is("[type=checkbox]")?"checkbox":"radio",oDiv=$(document.createElement("label")).attr({
                "for":
                input_class["1"]+"_"+i
            }).html($(outerLabel).text());
            $(outerLabel).remove();
            outerLabelParent.append(input,oDiv)
        }
    });
    $(
        ".e4").customInput();
    $($(".e3")).each(function(i){
        if($(this).is("[type=checkbox],[type=radio]")){
            var input=$(this),
            input_class=input.attr("class").split(" ");
            input.attr({
                id:input_class["1"]+"_"+i
            });
            var label=$("label[for="+input.attr(
                "id")+"]"),outerLabel=input.parent(),outerLabelParent=input.parent().parent(),inputType=input.is("[type=checkbox]")?
            "checkbox":"radio";
            if(inputType=="radio"){
                var oDiv=$(document.createElement("label")).attr({
                    "for":input_class["1"]+"_"+i
                }).html($(outerLabel).text());
                $(outerLabel).remove();
                outerLabelParent.append(input,oDiv)
            }
        }
    });
    $(".e3").customInput()
});
function showManifestImage(imageName,arrivalDate,shipName){
    $.colorbox({
        width:"1000px",
        height:"800px",
        iframe:true,
        href:
        "/show-manifest-image/"+imageName+"/"+arrivalDate+"/"+shipName
    })
}
function exportExcel(gridId,actionUrls){
    var mya=[];
    mya=
    $("#"+gridId).getDataIDs();
    var data=$("#"+gridId).getRowData(mya[0]),colNames=[],ii=0,html="";
    for(var i in data)if(i!=
        "Action(s)"){
        colNames[ii++]=i;
        html=html+i+"\t"
    }
    html=html+"\n";
    for(var i=0;i<mya.length;i++){
        data=$("#"+gridId).
        getRowData(mya[i]);
        for(j=0;j<colNames.length;j++)html=html+data[colNames[j]]+"\t";
        html=html+"\n"
    }
    html=html+"\n";
    var 
    postData=$("#"+gridId).getGridParam("postData");
	postData['export'] = 'excel';
	//alert(postData);
	//console.log(postData);return false;
    document.forms[0].csvBuffer.value=html;
    if(typeof(postData.searchString)=='undefined'){
		document.forms[0].searchString.value=JSON.stringify(postData);
	}else{
		document.forms[0].searchString.value=postData.searchString;
	}
    document.forms[0].sidx.value=postData.sidx;
    document.forms[0].sord.value=postData.sord;
    document.forms[0].rows.value=0;
    document.forms[0].page.value=0;
    document.forms[0].method="POST";
    document.forms[0].action=
    actionUrls;
    document.forms[0].submit()
}
$(document).ready(function(){
    $(".grid-box").find("th:last").addClass("action-last"
        )
});
function checkUserAuthenticationAjx(response){
    response=response.toString();
    if(response.indexOf(
        "PageHasNotPermission")==0){
        var responseArr=response.split("###");
        window.location.href=responseArr[1];
        return false
    }
    var 
    referer=response.split("##icpl");
    if(referer[0]=="session_expired"){
        window.location.href="/crm/"+referer[1];
        return false
    }
    return true
}
function showAjxLoader(){
    var scrollheight=$(document).height(),scrollwidth=$(document).width(),windowheight=
    $(window).height(),windowwidth=$(window).width();
    $("#ajx_loader").css({
        height:scrollheight,
        width:scrollwidth,
        display:
        "block"
    });
    $("#ajx_loader_image").css({
        top:windowheight/2,
        left:windowwidth/2,
        position:"absolute"
    })
}
$(document).ready(
    function(){
        $(window).scroll(function(){
            if($("#ajx_loader_image").css("display")=="block"){
                var windowheight=$(window).
                height(),scrolltop=$(document).scrollTop(),windowwidth=$(window).width();
                $("#ajx_loader_image").css({
                    top:scrolltop+
                    windowheight/2,
                    left:windowwidth/2,
                    position:"absolute"
                })
            }
        })
    });
function hideAjxLoader(){
    $("#ajx_loader").hide()
}
function 
trimString(str){
    return str.replace(/^,|,$/g,"")
}
$(document).ready(function(){
    $("#colorbox").click(function(e){
        if(!$(e.
            target).hasClass("select2-chosen")&&!$(e.target).parent().hasClass("select2-arrow")){
            $(".select2-drop").css("display",
                "none");
            $("#s2id_new_category_id").removeClass("select2-dropdown-open select2-drop-above");
            $("div[id^='s2id_']").
            removeClass("select2-dropdown-open select2-drop-above")
        }
    })
});
function generateAllReportsGrid(gridInfoArr){
	if(gridInfoArr
        .colNamesArr[0].length>0){
        var numOfGrids=gridInfoArr.redirectUrlArr.length,gridStr="";
        $(gridInfoArr.redirectUrlArr).each
        (function(index){
            var seq=index+1;
            if(gridInfoArr.headingGrid[index]!=""&&gridInfoArr.headingGrid[index]!="undefined")
                gridStr+='<div class="create-contacts"><div class="head no-img"><span>'+gridInfoArr.headingGrid[index]+
                "</span></div></div>";
            gridStr+=
            '<div style="margin-bottom:10px;">&nbsp;</div><div class="grid-box" id="gview_list"><table id="'+gridInfoArr.gridId+"_"+
            seq+'"></table><div id="'+gridInfoArr.gridId+"_"+seq+'_rud"></div></div>'
        });
        $("#"+gridInfoArr.gridBlockId).html(gridStr)
        ;
        var emptyMsgDivGeneral=[];
        $(gridInfoArr.redirectUrlArr).each(function(index,redirectUrlVal){ 
			var seq=index+1,listGridId=
            gridInfoArr.gridId+"_"+seq,listGridPager=gridInfoArr.gridId+"_"+seq+"_rud";
            listGrid=jQuery("#"+listGridId);
            var 
            colNamesArr=gridInfoArr.colNamesArr[index],colModelsArr=gridInfoArr.colModelsArr[index],sortName=gridInfoArr.sortNameArr
            [index],sortOrder=gridInfoArr.sortOrdArr[index],postDataArr=gridInfoArr.postDataArr[index],filterValArr=gridInfoArr.filterValuesArr[index],reportColumnArr=gridInfoArr.reportColumnArr[index],isRunOnly=gridInfoArr.isRunOnly[index],mst_report_id=gridInfoArr.mst_report_id[index],filterIdArr=gridInfoArr.filterIdArr[index],OrderByArr=gridInfoArr.OrderByArr[index],groupByColArr=gridInfoArr.groupByColArr[index];
			/*if(isRunOnly==null)
				isRunOnly='1';*/
            emptyMsgDivGeneral[index]=$(
                '<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");
            $(listGrid).jqGrid({
                mtype:"POST",
                url:redirectUrlVal,
                datatype:
                "json",
                sortable:true,
				postData:{
					data:postDataArr,
					colNames:colNamesArr,
					filterValues: filterValArr,
					reportColumns: reportColumnArr,
					isRunOnly: isRunOnly,
					mst_report_id : mst_report_id,
					filterIds: filterIdArr,
					OrderBy: OrderByArr,
					groupByCols: groupByColArr
				},                
                colNames:colNamesArr,
                colModel:colModelsArr,
                viewrecords:true,
                sortname:sortName,
                sortorder:sortOrder,
                rowNum:numRecPerPage,
                rowList:pagesArr,
                pager:"#"+listGridPager,
                viewrecords:true,
                autowidth:true,
                shrinkToFit:true,
                caption:"",
                width:"100%",
                cmTemplate:{
                    title:false
                },
                loadComplete:function(){
                    hideAjxLoader();
                    var count=
                    listGrid.getGridParam(),ts=listGrid[0];
                    if(ts.p.reccount===0&&gridInfoArr.gridBlockId=="reportDetailGrids"){
                        listGrid.hide
                        ();
                        $("#gridNoRec_"+index).show();
                        $("#"+listGridPager+"_right div.ui-paging-info").css("display","none")
                    }else{
                        listGrid.
                        show();
                        $("#gridNoRec_"+index).hide();
                        $("#"+listGridPager+"_right div.ui-paging-info").css("display","block")
                    }
                }
            });
            $(
                '<div class="no-record-msz" id="gridNoRec_'+index+'">'+NO_RECORD_FOUND+"</div>").insertAfter(listGrid.parent());
            $(
                "#gridNoRec_"+index).hide();
            $(listGrid).jqGrid("navGrid","#"+listGridPager,{
                reload:true,
                edit:false,
                add:false,
                search:
                false,
                del:false
            });
            if(redirectUrlVal=="/get-report-grid-info")$(listGrid).jqGrid("navButtonAdd","#"+listGridPager,{
                caption:"",
                title:"Export",
                id:"exportExcel",
                onClickButton:function(){
                    exportExcel(listGridId,redirectUrlVal)
                },
                position:
                "last"
            })
        })
    }else $("#"+gridInfoArr.gridBlockId).html("")
}


(function($) {
    $.fn.extend( {
        limiter: function(limit, elem) {
            $(this).on("keyup focus", function() { setCount(this, elem); });
            function setCount(src, elem) {
                if(src != undefined) { 
                  var chars = src.value.length;
                  if (chars >= limit) {
                     src.value = src.value.substr(0, limit);
                     chars = limit;
                  }
                  elem.html(chars);
                } 
            }
            setCount($(this)[0], elem);
        }
    });
})(jQuery);