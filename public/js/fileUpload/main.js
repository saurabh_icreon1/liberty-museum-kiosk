/*
 * jQuery File Upload Plugin JS Example 8.8.2
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/*jslint nomen: true, regexp: true */
/*global $, window, blueimp */

$(function () {
    $("#save_images").click(function(){
     $("#fileupload").validate({
        submitHandler: function() {
            var dataStr = $('#fileupload').serialize();
            showAjxLoader();
            $.ajax({
                type: "POST",
                url: '/upload-crm-gallery-content',
                data: dataStr,
                success: function(responseData){
                    var jsonObj =  jQuery.parseJSON(responseData);                        
                    if(jsonObj.status =='success'){
                         window.location.href = '/crm-galleries';
                    }else{
                        $.each(jsonObj.message, function (i, msg) {
                            $("#"+i).after('<label class="error" style="display:block;">'+msg+'</label>');                                    
                        });
                    }
                    hideAjxLoader();
                }
            });
        },
        ignore: "",
        rules: {
         counter:{
                required: true
            }
        },
    
        messages: {
          counter:{
                required: IMAGES_EMPTY
            }
        }
    });

    });
    //'use strict';

    //alert(gallery_file_type);
    var fileTypes=(typeof gallery_file_type !='undefined')?gallery_file_type:'/(\.|\/)(jpe?g|png|mp4)$/';
    var fileTypeAllowed = new RegExp(fileTypes, "i");
    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
        url: '/cms-upload',
        maxImageFileSize: (typeof gallery_file_size !='undefined')?gallery_file_size:((Math.pow(1024, 2)) * 50),//10000000, // 10 MB
        maxVideoFileSize:(typeof gallery_file_size !='undefined')?gallery_file_size:((Math.pow(1024, 2)) * 50),
        acceptFileTypes: /(\.|\/)(jpe?g|png|mp4)$/
    });

        // Load existing files:
        $('#fileupload').addClass('fileupload-processing');
		var id = $("#user_id").val(); 
        $.ajax({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            url: '/cms-upload', //$('#fileupload').fileupload('option', 'url'),
            // dataType: 'json',
            context: $('#fileupload')[0]
        }).always(function () {
            $(this).removeClass('fileupload-processing');
        }).done(function (result) {
             $(this).fileupload('option', 'done')
                 .call(this, null, {result: result});
        });

});

/* @Function to add 
 * @author  Icreon Tech - AP
 **/
 function addClass(){
    
    if ($('#deletAll').is(':checked')) {
           $(".checkDeleteLabel").addClass("checked");
    } else {
          $(".checkDeleteLabel").removeClass("checked");
    } 
    
}
/* @Function to decrement counter for no of images deleted 
 * @param counet value
 * @return counetr value
 * @author  Icreon Tech - AP
 **/
function subcountValue(val){
  var counter = $("#counter").val();
  counter = parseInt(counter)-1;
  if(counter>0){
      counter = counter;
  }
     else{
         counter = '';
     }
     $("#counter").val(counter);
}