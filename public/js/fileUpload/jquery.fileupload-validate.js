(function(factory){"use strict";if(typeof define==="function"&&define.amd)define(["jquery","./jquery.fileupload-process"
],factory);else factory(window.jQuery)})(function($){"use strict";$.blueimp.fileupload.prototype.options.processQueue.
push({action:"validate",always:true,acceptFileTypes:"@",maxImageFileSize:"@",maxVideoFileSize:"@",minFileSize:"@",
maxNumberOfFiles:"@",disabled:"@disableValidation"});$.widget("blueimp.fileupload",$.blueimp.fileupload,{options:{
acceptFileTypes:/(\.|\/)(jpe?g|png|mp4)$/i,maxImageFileSize:Math.pow(1024,2)*2,maxVideoFileSize:Math.pow(1024,2)*1,
minFileSize:undefined,maxNumberOfFiles:100,getNumberOfFiles:$.noop,messages:{maxNumberOfFiles:
"Maximum number of files exceeded",acceptFileTypes:"File type not allowed",maxImageFileSize:"Image is too large",
maxVideoFileSize:"Video is too large",minFileSize:"File is too small"}},processActions:{validate:function(data,options){
if(options.disabled)return data;var dfd=$.Deferred(),settings=this.options,file=data.files[data.index];if($.type(options
.maxNumberOfFiles)==="number"&&(settings.getNumberOfFiles()||0)+data.files.length>options.maxNumberOfFiles)file.error=
settings.i18n("maxNumberOfFiles");else if(options.acceptFileTypes&&!(options.acceptFileTypes.test(file.type)||options.
acceptFileTypes.test(file.name)))file.error=settings.i18n("acceptFileTypes");else if(file.type=="video/mp4"&&options.
maxVideoFileSize&&file.size>options.maxVideoFileSize)file.error=settings.i18n("maxVideoFileSize");else if(file.type!=
"video/mp4"&&options.maxImageFileSize&&file.size>options.maxImageFileSize)file.error=settings.i18n("maxImageFileSize");
else if($.type(file.size)==="number"&&file.size<options.minFileSize)file.error=settings.i18n("minFileSize");else delete 
file.error;if(file.error||data.files.error){data.files.error=true;dfd.rejectWith(this,[data])}else dfd.resolveWith(this,
[data]);return dfd.promise()}}})})