$(document).ready(function(){jQuery.validator.addMethod("selectType",function(value,element){return this.optional(
element)||/^[1-4 -]+$/i.test(value)});jQuery.validator.addMethod("alpha",function(value,element){return this.optional(
element)||/^[A_Za-z -.,]+$/i.test(value)});jQuery.validator.addMethod("alphalabel",function(value,element){return this.
optional(element)||/^[A_Za-z -]+$/i.test(value)});jQuery.validator.addMethod("alphanumericunderscore",function(value,
element){return this.optional(element)||/^[A-Za-z0-9 -]+$/i.test(value)});$("#createAttribute").validate({submitHandler:
function(){var dataStr=$("#createAttribute").serialize(),attribute;if($("#id").val()!=0&&$("#id").val()!="")attribute=
"/edit-product-attribute";else attribute="/create-product-attribute";$.ajax({type:"POST",url:attribute,data:dataStr,
success:function(responseData){if(!checkUserAuthenticationAjx(responseData))return false;var jsonObj=jQuery.parseJSON(
responseData);if(jsonObj.status=="success")window.location.href="/crm-product-attributes";else $.each(jsonObj.message,
function(i,msg){$("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")})}})},rules:{name:{
required:true,alphanumericunderscore:true,minlength:2,maxlength:50},label:{required:true,minlength:2,maxlength:12,
alphalabel:true},helpText:{minlength:2,maxlength:50,alpha:true},displayType:{required:true,selectType:true}},messages:{
name:{required:NAME_MSG,alphanumericunderscore:NAME_MSG},label:{required:LABEL_MSG,alphalabel:LABEL_MSG},helpText:{alpha
:VALID_DETAILS},displayType:{required:DISPLAY_TYPE_MSG,selectType:DISPLAY_TYPE_MSG}}});$.jgrid.no_legacy_api=true;$.
jgrid.useJSON=true;$(".e1").select2();$(".e2").customInput();var gridProAttrib=jQuery("#list"),emptyMsgDiv=$(
'<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");$("#list").jqGrid({mtype:"POST",url:"/list-product-attributes",
datatype:"json",sortable:true,colNames:["Name","Label","Required","No. Of Options","Display Type","Action(s)"],colModel:
[{name:"Name",index:"attribute_name"},{name:"Label",index:"attribute_label"},{name:"Required",index:
"attribute_mandatory"},{name:"No. Of Options",index:"num_options"},{name:"Display Type",index:"attribute_display_type"},
{name:"Action(s)",index:"action",sortable:false}],viewrecords:true,sortname:"tbl_pro_attributes.modified_date",sortorder
:"DESC",rowNum:10,rowList:[10,20,30],pager:"#pcrud",viewrecords:true,autowidth:true,shrinkToFit:true,caption:"",width:
"100%",cmTemplate:{title:false},loadComplete:function(){var count=gridProAttrib.getGridParam(),ts=gridProAttrib[0];if(ts
.p.reccount===0){gridProAttrib.hide();emptyMsgDiv.show();$("#pcrud_right div.ui-paging-info").css("display","none")}else
{gridProAttrib.show();emptyMsgDiv.hide();$("#pcrud_right div.ui-paging-info").css("display","block")}}});emptyMsgDiv.
insertAfter(gridProAttrib.parent());emptyMsgDiv.hide();$("#list").jqGrid("navGrid","#pcrud",{reload:true,edit:false,add:
false,search:false,del:false})});function deleteAttribute(id){$(".delete_attribute").colorbox({width:"700px",height:
"180px",inline:true});$("#no_delete").click(function(){$.colorbox.close();$("#yes_delete").unbind("click");return false}
);$("#yes_delete").click(function(){$.ajax({url:"/delete-product-attribute",method:"POST",dataType:"json",data:{
attribute_id:id},success:function(jsonResult){if(!checkUserAuthenticationAjx(jsonResult))return false;location.reload()}
})})}