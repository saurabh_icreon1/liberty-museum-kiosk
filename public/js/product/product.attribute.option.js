$(document).ready(function(){var relationBreadCrumbAnchor=$(".breadcrumb a:nth-child(3)");$(".breadcrumb a:nth-child(3)"
).after($(relationBreadCrumbAnchor).text());$(relationBreadCrumbAnchor).remove();jQuery.validator.addMethod(
"numericchar",function(value,element){return this.optional(element)||/^\d*\.?\d{0,2}$/i.test(value)});jQuery.validator.
addMethod("alphanumericunderscore",function(value,element){return this.optional(element)||/^[A-Za-z0-9 -\/]+$/i.test(
value)});$("#createAttributeOption").validate({submitHandler:function(){var dataStr=$("#createAttributeOption").
serialize(),attribute;if($("#id").val()!=0&&$("#id").val()!="")attribute="/edit-product-attribute-option/"+ATTRIBUTE_ID;
else attribute="/create-product-attribute-option/"+ATTRIBUTE_ID;$.ajax({type:"POST",url:attribute,data:dataStr,success:
function(responseData){if(!checkUserAuthenticationAjx(responseData))return false;var jsonObj=jQuery.parseJSON(
responseData);if(jsonObj.status=="success")window.location.href="/product-attribute-options/"+ATTRIBUTE_ID;else $.each(
jsonObj.message,function(i,msg){$("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")})}})},
rules:{name:{required:true,alphanumericunderscore:true,minlength:2,maxlength:50},cost:{maxlength:8,numericchar:true},
price:{maxlength:8,numericchar:true}},messages:{name:{required:NAME_MSG,alphanumericunderscore:NAME_MSG},cost:{
numericchar:COST_MSG},price:{numericchar:PRICE_MSG}}});$.jgrid.no_legacy_api=true;$.jgrid.useJSON=true;var gridProAttrib
=jQuery("#list"),emptyMsgDiv=$('<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");$("#list").jqGrid({mtype:"POST",
url:"/list-product-attribute-options/"+ATTRIBUTE_ID,datatype:"json",sortable:true,colNames:["Name","Default Cost",
"Default Price","Action(s)"],colModel:[{name:"Name",index:"option_name"},{name:"Default Cost",index:"option_cost"},{name
:"Default Price",index:"option_price"},{name:"Action(s)",index:"action",sortable:false}],viewrecords:true,sortname:
"tbl_pro_attribute_options.modified_date",sortorder:"DESC",rowNum:10,rowList:[10,20,30],pager:"#pcrud",viewrecords:true,
autowidth:true,shrinkToFit:true,caption:"",width:"100%",cmTemplate:{title:false},loadComplete:function(){var count=
gridProAttrib.getGridParam(),ts=gridProAttrib[0];if(ts.p.reccount===0){gridProAttrib.hide();emptyMsgDiv.show();$(
"#pcrud_right div.ui-paging-info").css("display","none")}else{gridProAttrib.show();emptyMsgDiv.hide();$(
"#pcrud_right div.ui-paging-info").css("display","block")}}});emptyMsgDiv.insertAfter(gridProAttrib.parent());
emptyMsgDiv.hide();$("#list").jqGrid("navGrid","#pcrud",{reload:true,edit:false,add:false,search:false,del:false})});
function deleteAttributeOption(id){$(".delete_attribute_option").colorbox({width:"700px",height:"180px",inline:true});$(
"#no_delete").click(function(){$.colorbox.close();$("#yes_delete").unbind("click");return false});$("#yes_delete").click
(function(){$.ajax({url:"/delete-product-attribute-option/"+ATTRIBUTE_ID,method:"POST",dataType:"json",data:{id:id},
success:function(jsonResult){if(!checkUserAuthenticationAjx(jsonResult))return false;location.reload()}})})}