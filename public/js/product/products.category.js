$(document).ready(function(){
    jQuery.validator.addMethod("alphanumericspecialchar",function(value,element){
        return this.
        optional(element)||/^[A-Za-z0-9\"\½\n\r ,.-]+$/i.test(value)},VALID_DETAILS);jQuery.validator.addMethod(
    "alphanumericwithhyphen",function(value,element){
        return this.optional(element)||/^[A-Za-z0-9 -]+$/i.test(value)
        },
    VALID_DETAILS);
    $("#create-product-category").validate({
        submitHandler:function(){
            var dataStr=$("#create-product-category"
                ).serialize();
            showAjxLoader();
            $.ajax({
                type:"POST",
                url:"/create-product-category",
                data:dataStr,
                success:function(
                    responseData){
                    hideAjxLoader();
                    if(!checkUserAuthenticationAjx(responseData))return false;
                    var jsonObj=jQuery.parseJSON(
                        responseData);
                    if(jsonObj.status=="success"){
                        $("#success-message").show();
                        $("#success-message").html(jsonObj.message);
                        window.location.href="/crm-product-categories"
                        }else{
                        $("#success-message").hide();
                        $.each(jsonObj.message,function(i,msg){
                            $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                            })
                        }
                    }
            })
    },
    rules:{
        category_name:{
            required:true
            ,
            alphanumericwithhyphen:true,
            minlength:2,
            maxlength:25
        },
        category_description:{
            required:true,
            minlength:2,
            maxlength:200
        }
    },
messages:{
    category_name:{
        required:CATEGORY_NAME_EMPTY
    },
    category_description:{
        required:
        DESCRIPTION_EMPTY
    }
}
});
$("#edit-product-category").validate({
    submitHandler:function(){
        var dataStr=$(
            "#edit-product-category").serialize();
        showAjxLoader();
        $.ajax({
            type:"POST",
            url:"/edit-product-category",
            data:dataStr,
            success:function(responseData){
                hideAjxLoader();
                if(!checkUserAuthenticationAjx(responseData))return false;
                var jsonObj=
                jQuery.parseJSON(responseData);
                if(jsonObj.status=="success"){
                    $("#success-message").show();
                    $("#success-message").html(
                        jsonObj.message);
                    window.location.href="/crm-product-categories"
                    }else{
                    $("#success-message").hide();
                    $.each(jsonObj.message
                        ,function(i,msg){
                            $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                            })
                    }
                }
        })
},
rules:{
    category_name:{
        required:true,
        alphanumericwithhyphen:true,
        minlength:2,
        maxlength:25
    },
    category_description:{
        required:true,
        minlength:2,
        maxlength:200
    }
},
messages:{
    category_name:{
        required:CATEGORY_NAME_EMPTY
    },
    category_description:{
        required:DESCRIPTION_EMPTY
    }
}
})
})