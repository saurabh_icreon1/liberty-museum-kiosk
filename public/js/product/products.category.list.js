$(function(){var $tree=$("#tree1");$tree.bind("tree.init",function(){if(editCategoryId!=""){var node=$tree.tree(
"getNodeById",editCategoryId);$tree.tree("selectNode",node)}});$tree.tree({dragAndDrop:true,autoOpen:false,data:
categoriesJsonStr,openedIcon:'<img src="/img/close.png" />',closedIcon:'<img src="/img/open.png" />',onCreateLi:function
(node,$li){if(node.is_deletable==1){var spanEle='<span class="right" id="editDeleteBlock'+node.name+
'"><a class="edit" href="/edit-product-category/'+node.id+
'">Edit</a><a class="delete_category" href="#delete_category_box" onclick="deleteCategory(\''+node.id+
"')\">Delete</a></span>";$li.find(".jqtree-title").after(spanEle)}}});$("#tree1").bind("tree.move",function(event){var 
childsOfParent=[],parentId,siblings="",categoryId=event.move_info.moved_node.id;if(event.move_info.position=="after"||
event.move_info.position=="before"){childsOfParent=event.move_info.target_node.parent.children;parentId=event.move_info.
target_node.parent.id}else{childsOfParent=event.move_info.target_node.children;parentId=event.move_info.target_node.id}
if(editCategoryId!=0){$tree.tree("selectNode",null);editCategoryId=0}if(event.move_info.moved_node.id=="czoxOiIxIjs="||
parentId=="czoxOiIxIjs="||event.move_info.moved_node.id=="czoxOiIyIjs="||parentId=="czoxOiIyIjs=")event.preventDefault()
;else{for(i=0;i<childsOfParent.length;i++)if(childsOfParent[i].id==event.move_info.target_node.id){if(event.move_info.
moved_node.id!=childsOfParent[i].id)if(event.move_info.position=="after")siblings+=childsOfParent[i].id+","+event.
move_info.moved_node.id+",";else siblings+=event.move_info.moved_node.id+","+childsOfParent[i].id+","}else if(event.
move_info.moved_node.id!=childsOfParent[i].id)siblings+=childsOfParent[i].id+",";if(event.move_info.position=="inside")
siblings=event.move_info.moved_node.id+","+siblings;siblings=trimString(siblings);parentId=typeof parentId=="undefined"?
0:parentId;showAjxLoader();$.ajax({type:"POST",dataType:"json",data:{parentId:parentId,categoryId:categoryId,siblings:
siblings},url:"/edit-product-category-drag-drop",success:function(responseData){hideAjxLoader();if(!
checkUserAuthenticationAjx(responseData))return false;var jsonObj=responseData;if(jsonObj.status=="success"){$(
"#success_message").html(jsonObj.message);$("#success_message").show();setTimeout(function(){$("#success_message").html(
"");$("#success_message").hide()},5000)}else $("#error_message").html(jsonObj.message)}})}});$("#cboxClose").click(
function(){$.colorbox.close();$("#yes_delete_category").unbind("click");if($("#is_move").is(":checked"))$("#is_move").
trigger("click");$("#is_move").unbind("change")})});function deleteCategory(categoryId){$(".delete_category").colorbox({
width:"700px",height:"285px",inline:true,trapFocus:false});$("#no_delete_category").click(function(){$.colorbox.close();
$("#yes_delete_category").unbind("click");if($("#is_move").is(":checked"))$("#is_move").trigger("click");$("#is_move").
unbind("change");return false});$("#yes_delete_category").click(function(){$("#yes_delete_category").unbind("click");var
 is_move=$("#is_move").is(":checked")?1:0;showAjxLoader();$.colorbox.close();$.ajax({type:"POST",dataType:"html",data:{
category_id:categoryId,is_move:is_move,new_category_id:$("#new_category_id").val()},url:"/delete-product-category",
success:function(responseData){hideAjxLoader();if(!checkUserAuthenticationAjx(responseData))return false;var jsonObj=
JSON.parse(responseData);if(jsonObj.status=="success")window.location.href="/crm-product-categories";else $(
"#error_message").html(jsonObj.message)}})});$("#is_move").change(function(){if($(this).is(":checked")){$.ajax({type:
"POST",data:{notCategoryId:categoryId},url:"/get-select-category",success:function(response){if(!
checkUserAuthenticationAjx(response))return false;$("#new_category_id option").each(function(){$(this).remove()});$(
"#new_category_id").append(response)}});$("#new_category_div").show()}else $("#new_category_div").hide()})}