$(document).ready(function(){$.jgrid.no_legacy_api=true;$.jgrid.useJSON=true;$(".e1").select2();$(".e2").customInput();
var gridProAttrib=jQuery("#list"),emptyMsgDiv=$('<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");$("#list").
jqGrid({mtype:"POST",url:"/list-manage-product-attributes/"+id,datatype:"json",sortable:true,colNames:[REMOVE,NAME,LABEL
,NO_OF_OPTIONS,REQUIRED,DISPLAY_TYPE],colModel:[{name:"Id",index:"Id",sortable:false,align:"center"},{name:"Name",index:
"attribute_name"},{name:"Label",index:"attribute_label"},{name:"No. Of Options",index:"num_options",align:"center"},{
name:"Required",index:"attribute_mandatory",sortable:false,align:"center"},{name:"Display Type",index:
"attribute_display_type",sortable:false}],viewrecords:true,sortname:"prodAttTab.modified_date",sortorder:"DESC",rowNum:
10,rowList:[10,20,30],pager:"#pcrud",viewrecords:true,autowidth:true,shrinkToFit:true,caption:"",width:"100%",cmTemplate
:{title:false},loadComplete:function(){var count=gridProAttrib.getGridParam(),ts=gridProAttrib[0];if(ts.p.reccount===0){
$("#save").hide();$("#save_continue").hide();gridProAttrib.hide();emptyMsgDiv.show();$("#pcrud_right div.ui-paging-info"
).css("display","none")}else{$("#save").show();$("#save_continue").show();gridProAttrib.show();emptyMsgDiv.hide();$(
"#pcrud_right div.ui-paging-info").css("display","block")}}});emptyMsgDiv.insertAfter(gridProAttrib.parent());
emptyMsgDiv.hide();$("#list").jqGrid("navGrid","#pcrud",{reload:true,edit:false,add:false,search:false,del:false});$(
"#attributeList").validate({submitHandler:function(){var dataStr=$("#attributeList").serialize(),attribute=
"/list-manage-product-attributes/"+id;$.ajax({type:"POST",url:attribute,data:dataStr,success:function(responseData){if(!
checkUserAuthenticationAjx(responseData))return false;var jsonObj=jQuery.parseJSON(responseData);if(jsonObj.status==
"success"){hideAjxLoader();if(jsonObj.act=="save")$("#tab-4").click();else $("#tab-5").click()}else $.each(jsonObj.
message,function(i,msg){$("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")})}})},rules:{},
messages:{}});$($(".e4")).each(function(i){if($(this).is("[type=checkbox],[type=radio]")){var input=$(this),input_class=
input.attr("class").split(" ");input.attr({id:input_class["1"]+"_"+i});var label=$("label[for="+input.attr("id")+"]"),
outerLabel=input.parent(),outerLabelParent=input.parent().parent(),inputType=input.is("[type=checkbox]")?"checkbox":
"radio",oDiv=$(document.createElement("label")).attr({"for":input_class["1"]+"_"+i}).html($(outerLabel).text());$(
outerLabel).remove();outerLabelParent.append(input,oDiv)}});$(".e4").customInput()});function addProcudtAttributes(){
showAjxLoader();$.ajax({type:"GET",data:{},url:"/attach-product-attributes/"+id,success:function(response){if(!
checkUserAuthenticationAjx(response))return false;hideAjxLoader();$("#productContent").html(response)}})}