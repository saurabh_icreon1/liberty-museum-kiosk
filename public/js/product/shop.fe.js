function getProductFront(){
    if($("#product_id").val() != undefined && $.trim($("#product_id").val()) != "") {  
        showAjxLoader();
        $.ajax({
            type:"POST",
            data:{
                page:$("#page").val(),
                product_id:$("#product_id").
                val()
            },
            url:"/get-shop-product-lists",
            success:function(response){
                $("#productlist").html(response);
                hideAjxLoader()
            }
        });
    }    
}
$(
    function(){
        $("#maywesuggest_product_description_more").click(function(){
            if($.trim($(this).text())=="MORE +"){
                $(this).
                text("HIDE -");
                $(".maywesuggest_product_description1").css({
                    display:"none"
                });
                $(".maywesuggest_product_description2").css
                ({
                    display:"inline"
                })
            }else if($.trim($(this).text())=="HIDE -"){
                $(this).text("MORE +");
                $(".maywesuggest_product_description1").css({
                    display:"inline"
                });
                $(".maywesuggest_product_description2").css({
                    display:"none"
                })
            }
        });
        $(".categorylink").click(function(){
            $(".shop-products > li > small").each(function(){
                $(this).removeClass("removeCat");
                $(this).addClass("plusCat");
                $(this).next().next().hide()
            });
            $(this).parent("li.categorylinkli").find("small").next().next().show();
            $(this).parent("li.categorylinkli").find("small").removeClass("plusCat");
            $(this).parent("li.categorylinkli").find("small").addClass("removeCat");
            $(".categorylinksub1").removeClass("active");
            $(".categorylink").removeClass("selected-menu");
            $(this).addClass("selected-menu");
            $(".categorylink").removeClass("active");
            $(this).addClass("active");
            var catid=$(this).attr("id");
            $("#categoryid").val(catid);
            $("#product_id").val("");
            $("#page").val(1);
            showAjxLoader();
            var catname=$.trim($(this).html());
            $.ajax({
                type:"POST",
                data:{
                    page:$("#page").val(),
                    categoryid:$("#categoryid").val(),
                    parentcatid:"0"
                },
                url:"/get-shop-product-lists",
                success:function(response){
                    $("#productlist").html(
                        response);
                    hideAjxLoader()
                },
                complete:function(){
                    if(categoryid.value != 'aTowOw=='){
                        $("#contentMainCategory").css({
                            display:"block"
                        });
                        $("#contentMainCategoryFeatured").css({
                            display:"none"
                        });                        
                    }
                    $("#contentSubCategory").css({
                        display:"none"
                    })
                }
            })
        });
        $(".categorylinksub1").click(function(){
            $(".categorylinksub1").removeClass("active");
            $(this)
            .addClass("active");
            var catid=$(this).attr("id");
            $("#categoryid").val(catid);
            $("#product_id").val("");
            $("#page").val(1);
            showAjxLoader();
            var parentcatid=$(this).closest("li.categorylinkli").find("a.categorylink").attr("id");
            $.ajax({
                type:
                "POST",
                data:{
                    page:$("#page").val(),
                    categoryid:$("#categoryid").val(),
                    parentcatid:parentcatid
                },
                url:
                "/get-shop-product-lists",
                success:function(response){
                    $("#productlist").html(response);
                    hideAjxLoader()
                },
                complete:function
                (){
                    if(categoryid.value != 'aTowOw=='){
                        $("#contentMainCategory").css({
                            display:"block"
                        });
                        $("#contentMainCategoryFeatured").css({
                            display:"none"
                        });                        
                    }
                    $("#contentSubCategory").css({
                        display:"block"
                    })
                }
            })
        });
        $(document).on("click",".plusCat",function(){
            $(".shop-products > li > small").each(function(){
                $(this).removeClass("removeCat");
                $(this).
                addClass("plusCat");
                $(this).next().next().hide()
            });
            $(this).next().next().show();
            $(this).removeClass("plusCat");
            $(this).
            addClass("removeCat")
        });
        $(document).on("click",".removeCat",function(){
            $(".shop-products > li > small").each(function(){
                $(this).removeClass("removeCat");
                $(this).addClass("plusCat");
                $(this).next().next().hide()
            });
            $(this).next().next().hide()
            ;
            $(this).removeClass("removeCat");
            $(this).addClass("plusCat")
        })
    });

function getCartViewUpdtLS() {
    try {
        showAjxLoader();
        $.ajax({
            type:"POST",
            url:"/cart-front-product",
            async:true,
            success:function(responseData){
                hideAjxLoader();
                parent.$("#cartlist").html(responseData);
            }
        });	
        $.ajax({
            type:"POST",
            url:"/cart-details",
            async:true,
            success:function(responseData){
                parent.$("#cart-details-div").html(responseData);
            }
        });
    }
    catch(err) {}
}

function addtocart(product_id){
    //authenticateUserBeforeAjax();
    var status = false;
    if($("#quantity_"+product_id).val() > 0)
        var quantity=$("#quantity_"+product_id).val(),status=/^\d+$/.test(quantity);
    if(status==false){
        $("#quantity_"+product_id).addClass("error");
        $("#quantityerror_"+product_id).show()
    }else $.ajax({
        type:"POST",
        url:
        "/add-to-cart-inventory-front",
        data:{
            product_id:product_id,
            num_quantity:quantity
        },
        beforeSend:function(){
            showAjxLoader()
        }
        ,
        success:function(responseData){
            hideAjxLoader();
            var jsonObj=jQuery.parseJSON(responseData);
            $("label.error").remove();
            if(
                jsonObj.status=="success"){
                $("label.error").remove();
                showAddToCartConfirmation();
                parent.showAddToCartConfirmation();
                // added - start
                getCartViewUpdtLS();
            // added - end                
            }else

            {
                if(jsonObj.product_id!=undefined&&jsonObj.message!=undefined&&jsonObj.product_id!=""&&jsonObj.message!=""){
                    $(
                        'label[for="quantity_'+jsonObj.product_id+'"]').remove();
                    $("#quantity_"+jsonObj.product_id).after(
                        '<label class="error mar-b-10" for="quantity_'+jsonObj.product_id+'" style="display:block;">'+jsonObj.message+"</label>"
                        )
                }
                $.each(jsonObj.message,function(i,msg){
                    $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                })
            }
        }
    })
}
function addtowishlist(product_id){
    var quantity=$("#quantity_"+product_id).val(),status=/^\d+$/.test(quantity);
    if(
        status==false){
        $("#quantity_"+product_id).addClass("error");
        $("#quantityerror_"+product_id).show()
    }else $.ajax({
        type:
        "POST",
        url:"/add-to-wishlist",
        data:{
            product_id:product_id,
            num_quantity:quantity
        },
        beforeSend:function(){
            showAjxLoader()
        },
        success:function(responseData){
            hideAjxLoader();
            if(responseData=="session_expired")if(!checkFrontUserAuthenticationAjx(
                responseData))return false;
            var jsonObj=jQuery.parseJSON(responseData);
            if(jsonObj.status=="success")$.colorbox({
                width:
                "500px",
                href:"#addedToWishlist",
                height:"250px",
                inline:true
            });else $.each(jsonObj.message,function(i,msg){
                $("#"+i).after(
                    '<label class="error" style="display:block;">'+msg+"</label>")
            })
        }
    })
}