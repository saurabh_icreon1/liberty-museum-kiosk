function getProductFront(){
    if($("#product_id").val() != undefined && $.trim($("#product_id").val()) != "") {  
        showAjxLoader();
        $.ajax({
            type:"POST",
            data:{
                page:$("#page").val(),
                product_id:$("#product_id").val()
            },
            url:"/get-kiosk-shop-product-lists",
            success:function(response){
                $("#productlist").html(response);
                hideAjxLoader()
            }
        });
    }    
}
$(function(){
        $("#maywesuggest_product_description_more").click(function(){
            if($.trim($(this).text())=="MORE +"){
                $(this).
                text("HIDE -");
                $(".maywesuggest_product_description1").css({
                    display:"none"
                });
                $(".maywesuggest_product_description2").css
                ({
                    display:"inline"
                })
            }else if($.trim($(this).text())=="HIDE -"){
                $(this).text("MORE +");
                $(".maywesuggest_product_description1").css({
                    display:"inline"
                });
                $(".maywesuggest_product_description2").css({
                    display:"none"
                })
            }
        });
        $(".categorylink").click(function(){
            $(".shop-products > li > small").each(function(){
                $(this).removeClass("removeCat");
                $(this).addClass("plusCat");
                $(this).next().next().hide()
            });
            $(this).parent("li.categorylinkli").find("small").next().next().show();
            $(this).parent("li.categorylinkli").find("small").removeClass("plusCat");
            $(this).parent("li.categorylinkli").find("small").addClass("removeCat");
            $(".categorylinksub1").removeClass("active");
            $(".categorylink").removeClass("selected-menu");
            $(this).addClass("selected-menu");
            $(".categorylink").removeClass("active");
            $(this).addClass("active");
            var catid=$(this).attr("id");
            $("#categoryid").val(catid);
            $("#product_id").val("");
            $("#page").val(1);
            showAjxLoader();
            var catname=$.trim($(this).html());
            $.ajax({
                type:"POST",
                data:{
                    page:$("#page").val(),
                    categoryid:$("#categoryid").val(),
                    parentcatid:"0"
                },
                url:"/get-kiosk-shop-product-lists",
                success:function(response){
                    $("#productlist").html(
                        response);
                    hideAjxLoader()
                },
                complete:function(){
                    if(categoryid.value != 'aTowOw=='){
                        $("#contentMainCategory").css({
                            display:"block"
                        });
                        $("#contentMainCategoryFeatured").css({
                            display:"none"
                        });                        
                    }
                    $("#contentSubCategory").css({
                        display:"none"
                    })
                }
            })
        });
        $(".categorylinksub1").click(function(){
            $(".categorylinksub1").removeClass("active");
            $(this)
            .addClass("active");
            var catid=$(this).attr("id");
            $("#categoryid").val(catid);
            $("#product_id").val("");
            $("#page").val(1);
            showAjxLoader();
            var parentcatid=$(this).closest("li.categorylinkli").find("a.categorylink").attr("id");
            $.ajax({
                type:
                "POST",
                data:{
                    page:$("#page").val(),
                    categoryid:$("#categoryid").val(),
                    parentcatid:parentcatid
                },
                url:
                "/get-kiosk-shop-product-lists",
                success:function(response){
                    $("#productlist").html(response);
                    hideAjxLoader()
                },
                complete:function
                (){
                    if(categoryid.value != 'aTowOw=='){
                        $("#contentMainCategory").css({
                            display:"block"
                        });
                        $("#contentMainCategoryFeatured").css({
                            display:"none"
                        });                        
                    }
                    $("#contentSubCategory").css({
                        display:"block"
                    })
                }
            })
        });
        $(document).on("click",".plusCat",function(){
            $(".shop-products > li > small").each(function(){
                $(this).removeClass("removeCat");
                $(this).
                addClass("plusCat");
                $(this).next().next().hide()
            });
            $(this).next().next().show();
            $(this).removeClass("plusCat");
            $(this).
            addClass("removeCat")
        });
        $(document).on("click",".removeCat",function(){
            $(".shop-products > li > small").each(function(){
                $(this).removeClass("removeCat");
                $(this).addClass("plusCat");
                $(this).next().next().hide()
            });
            $(this).next().next().hide()
            ;
            $(this).removeClass("removeCat");
            $(this).addClass("plusCat")
        })
    });

function addtocart(product_id){
    authenticateUserBeforeAjax();
    var status = false;

    
       var isWheelchair = 0;
        var isNextAvailableTime = 0;
       /* if($("#addMeTime").is(":checked")){
                isNextAvailableTime = 1;
        }
        if($("#wheelchair").is(":checked")){
                isWheelchair = 1;
            }*/
    $.ajax({
        type:"POST",
        url:"/add-to-cart-inventory-front",
        data:{
            product_id:product_id,
            num_quantity:1,
            is_wheelchair:isWheelchair,
            next_available_time:isNextAvailableTime,
        },
        beforeSend:function(){
            showAjxLoader()
        }
        ,
        success:function(responseData){
            hideAjxLoader();
            var jsonObj=jQuery.parseJSON(responseData);
            $("label.error").remove();
            if(jsonObj.status=="success"){
                // added - start
                getCartViewUpdtLS();
				return true;
            // added - end                
            }else{
					return true;
            }
        }
    })
}

function getCartViewUpdtLS() {
    try {
        showAjxLoader();
        $.ajax({
            type:"POST",
            url:"/cart-front-product",
            async:true,
            success:function(responseData){
                hideAjxLoader();
                parent.$("#cartlist").html(responseData);
            }
        });	
        $.ajax({
            type:"POST",
            url:"/cart-details",
            async:true,
            success:function(responseData){
                parent.$("#cart-details-div").html(responseData);
				addKioskFee();//add data to kiosk kart
            }
        });
		
    }
    catch(err) {}
}
/*add kiosk fee or add to cart for kiosk */
function addKioskFee(){
	  var isWheelchair = 0;
      var isNextAvailableTime = 0;
      if($("#addMeTime").is(":checked")){
                isNextAvailableTime = 1;
       }
       if($("#wheelchair").is(":checked")){
                isWheelchair = 1;
       }
	 $.ajax({
        type:"POST",
        url:"/add-to-cart-kiosk-fee",
        data:{
            is_wheelchair:isWheelchair,
            next_available_time:isNextAvailableTime,
        },
        beforeSend:function(){
            showAjxLoader()
        },
		success:function(responseData){
            hideAjxLoader();
            var jsonObj=jQuery.parseJSON(responseData);
            $("label.error").remove();
            if(jsonObj.status=="success"){
                 parent.window.location.href="/checkout";      
            }else{
					return false;
            }
        },
		error:function(){
			 parent.window.location.href="/checkout";    
		},
    })
}
// html for current slot and total slot 
function callReservation(){
	$.ajax({
        type:"POST",
        url:"/get-kiosk-data",
        data:{},
        beforeSend:function(){
            showAjxLoader()
        },
		success:function(responseData){
            hideAjxLoader();
            var jsonObj=jQuery.parseJSON(responseData);
            $("label.error").remove();
            if(jsonObj.status=="success"){
                   $("#addKioskData").append(jsonObj.message); 
            }else{
					return false;
            }
        },
		error:function(){
			 $("#addKioskData").append("Server Busy");  
		},
    })
}