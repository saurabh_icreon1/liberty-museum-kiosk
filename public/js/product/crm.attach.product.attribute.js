$(document).ready(function(){$("#attachAttribute").validate({submitHandler:function(){var dataStr=$("#attachAttribute").
serialize(),attribute="/attach-product-attributes/"+$("#id").val();$.ajax({type:"POST",url:attribute,data:dataStr,
success:function(responseData){if(!checkUserAuthenticationAjx(responseData))return false;var jsonObj=jQuery.parseJSON(
responseData);if(jsonObj.status=="success")$("#tab-4").click();else $.each(jsonObj.message,function(i,msg){$("#"+i).
after('<label class="error" style="display:block;">'+msg+"</label>")})}})},rules:{"productAttributes[]":{required:true}}
,messages:{productAttributes:{required:"Please select at least one attribute"}}})})