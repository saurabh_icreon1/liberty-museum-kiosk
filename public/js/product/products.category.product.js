$(function(){
    loadData();    
    $("#category_id").change(function(){
        loadData();
    });      
});

function loadData(){
    showAjxLoader();
    id = $("#category_id").val();
    $.ajax
    ({
        type:"POST",
        data:{},
        url:"/category-product-list/"+id,
        success:function(response){
            if(!checkUserAuthenticationAjx(response))
                return false;
            hideAjxLoader();
            if(response != '[]'){                
                var cmsJsonStr=jQuery.parseJSON(response);
                var $tree=$("#tree1");
                $tree.bind("tree.init");
                $tree.tree({
                    dragAndDrop:true,
                    autoOpen:false,
                    data:cmsJsonStr                                        
                });
                $("#tree1").bind("tree.move",
                    function(event){
                        var childsOfParent=[];
                        var parentId;
                        var siblings="";
                        var menuId=event.move_info.moved_node.id;
                        if(event.move_info.position=="after"||event.move_info.position=="before"){
                            childsOfParent=event.move_info.target_node.parent.children;
                            parentId=event.move_info.target_node.parent.id
                            targetNode=event.move_info.target_node.id
                        }else{
                            return false;
                        }
                        if(event.move_info.moved_node.id==0||parentId==0)event.preventDefault();
                        else{
                            for(var i=0;i<childsOfParent.length;i++){
                                if(childsOfParent[i].id==event.move_info.target_node.id){
                                    if(event.move_info.position=="after"){
                                        siblings+=childsOfParent[i].id+","+event.move_info.moved_node.id+",";
                                    }
                                    else {
                                        siblings+=event.move_info.moved_node.id+","+childsOfParent[i].id+",";
                                    }
                                }
                                else{
                                    siblings+=childsOfParent[i].id+",";
                                }
                            }
                            siblings=trimString(siblings);
                            parentId=typeof parentId=="undefined"?0:parentId;
                            showAjxLoader();
                            $.ajax({
                                type:"POST",
                                dataType:"json",
                                data:{
                                    parentId:parentId,
                                    menuId:menuId,
                                    siblings:siblings,
                                    targetNode : targetNode
                                },
                                url:"/edit-cat-product-drag-drop",
                                success:function(responseData){
                                    if(!checkUserAuthenticationAjx(responseData))return false;
                                    hideAjxLoader();
                                    var jsonObj=responseData;
                                    if(jsonObj.status=="success"){
                                        $("#success_message").html(jsonObj.message);
                                        $("#success_message").show();
                                        setTimeout(function(){
                                            $("#success_message").html("");
                                            $("#success_message").hide()
                                        },5E3)
                                    }else $("#error_message").html(jsonObj.message)
                                }
                            })
                        }
                    }); 
            }else{
                $("#tree1").html("<p class='no-record-found'>No Products Available.</p>");
            }            
        }
        
    })    
}