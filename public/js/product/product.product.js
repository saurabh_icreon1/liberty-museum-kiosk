function checkisValidWeight(){
    if($("#product_type").val()!=2&&$("#product_type").val()!=3&&$("#weight").val()=="")
        return false;else return true
        }
        $(document).ready(function(){
    var container=$("div.error_message");
    jQuery.validator.
    addMethod("alphanumericunderscoreapostrohpe",function(value,element){
        return this.optional(element)||
        /^[A-Za-z0-9'\/ -]+$/i.test(value)},VALID_DETAILS);jQuery.validator.addMethod("alphanumericunderscore",function(value,
    element){
        return this.optional(element)||/^[A-Za-z0-9\/ -]+$/i.test(value)
        },VALID_DETAILS);
jQuery.validator.addMethod(
    "alphanumericspecialchar",function(value,element){
        return this.optional(element)||/^[A-Za-z0-9-\n ,.-@#$%*()!\\'""]+$/i.
        test(value)
        },VALID_DETAILS);
jQuery.validator.addMethod("numericchar",function(value,element){
    return this.optional(
        element)||/^[0-9.]+$/i.test(value)
    });
jQuery.validator.addMethod("checkrequired",function(value,element){
    return checkisValidWeight()
    },"Please enter valid weight");
if($("#product_type").val()!=2&&$("#product_type").val()!=3)$(
    "#weightLabel").show();else $("#weightLabel").hide();
    $("#product_type").change(function(){
    if($("#product_type").val()!=2 &&$("#product_type").val()!=3)$("#weightLabel").show();else $("#weightLabel").hide()});
$("#createProduct").validate({
    submitHandler:function(){
        var dataStr=$("#createProduct").serialize();
        showAjxLoader();
        $.ajax({
            type:"POST",
            url:"/create-crm-product",
            data:dataStr,
            success:function(responseData){
                if(!checkUserAuthenticationAjx(responseData))
                    return false;
                hideAjxLoader();
                var jsonObj=jQuery.parseJSON(responseData);
                if(jsonObj.status=="success")
                    window.location.href="/crm-products";
                else $.each(jsonObj.message,function(i,msg){
                        $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                    })
                }
                })
    },
rules:{
    product_type:{
        required:true
    },
    name:{
        required:
        true,
        //alphanumericunderscoreapostrohpe:true,
        minlength:2,
        maxlength:100
    },
    sku:{
        required:true,
        alphanumericunderscore:true,
        maxlength:15
    },
    description:{
        //alphanumericspecialchar:true,
        required:true,
        minlength:2,
        maxlength:500
    },
    shipping_description:{
       // alphanumericspecialchar:true,
        required:true,
        minlength:2,
        maxlength:500
    },
    "available[]":{
        required:true
    },
    sell_price:{
        numericchar:true,
        required:function(){
            return $("#product_type option:selected").val()=="1"
            },
        maxlength:8
    },
    list_price:{
        numericchar:true,
        maxlength:8
    },
    cost:{
        numericchar:true,
        maxlength:8
    },
    weight:{
        numericchar:true,
        maxlength:6,
        checkrequired:
        true
    },
    length:{
        numericchar:true,
        maxlength:6
    },
    width:{
        numericchar:true,
        maxlength:6
    },
    height:{
        numericchar:true,
        maxlength:6
    },
    in_Stock_units:{
        numericchar:true,
        maxlength:6
    },
    threshold:{
        numericchar:true,
        maxlength:6
    },
    total_in_ellis_stock:{
        numericchar
        :true,
        maxlength:6
    },
    ellis_product_threshold:{
        numericchar:true,
        maxlength:6
    },
    gl_code_value:{
        digits:true,
        minlength:1,
        maxlength:10
    },
    prog_code_value:{
        digits:true,
        minlength:1,
        maxlength:10
    }
},
messages:{
    product_type:{
        required:PRODUCT_TYPE_EMPTY
    },
    name:{
        required:NAME_EMPTY
       // alphanumericunderscoreapostrohpe:PRODUCT_NAME_VALID
    },
    sku:{
        required:SKU_EMPTY,
        alphanumericunderscore:SKU_VALID
    },
    description:{
        required:DESCRIPTION_EMPTY
        //alphanumericspecialchar:DESCRIPTION_VALID
    },
    shipping_description:{
        required:
        SHIPPING_DESCRIPTION_EMPTY
        //alphanumericspecialchar:DESCRIPTION_VALID
    },
    "available[]":{
        required:AVAILABLE_EMPTY
    },
    sell_price:{
        required:SELLEIN_PRICE_EMPTY,
        numericchar:SELL_PRICE_VALID
    },
    list_price:{
        numericchar:LIST_PRICE_VALID
    },
    cost:{
        numericchar:COST_PRICE_VALID
    },
    weight:{
        numericchar:WEIGHT_VALID
    },
    width:{
        numericchar:WIDTH_VALID
    },
    length:{
        numericchar:
        LENGTH_VALID
    },
    height:{
        numericchar:HEIGHT_VALID
    },
    in_Stock_units:{
        numericchar:IN_STOCK_VALID
    },
    threshold:{
        numericchar:
        THRESHOLD_VALID
    },
    total_in_ellis_stock:{
        numericchar:IN_STOCK_VALID
    },
    ellis_product_threshold:{
        numericchar:THRESHOLD_VALID
    }
},
highlight:function(label){
    $(".detail-section").css("display","block")
    }
});
$("#editProduct").validate({
    submitHandler:
    function(){
        var dataStr=$("#editProduct").serialize();
        showAjxLoader();
        $.ajax({
            type:"POST",
            url:"/edit-crm-product",
            data:
            dataStr,
            success:function(responseData){
                if(!checkUserAuthenticationAjx(responseData))return false;
                var jsonObj=jQuery.
                parseJSON(responseData);
                hideAjxLoader();
                if(jsonObj.status=="success")if(jsonObj.actPage=="save_continue")$("#tab-4").
                    click();else window.location.href="/crm-products";else $.each(jsonObj.message,function(i,msg){
                    $("#"+i).after(
                        '<label class="error" style="display:block;">'+msg+"</label>")
                    })
                }
                })
    },
rules:{
    product_type:{
        required:true
    },
    name:{
        required:
        true,
        //alphanumericunderscoreapostrohpe:true,
        minlength:2,
        maxlength:100
    },
    sku:{
        required:true,
        alphanumericunderscore:true,
        maxlength:15
    },
    description:{
        //alphanumericspecialchar:true,
        required:true,
        minlength:2,
        maxlength:500
    },
    shipping_description:{
       // alphanumericspecialchar:true,
        required:true,
        minlength:2,
        maxlength:500
    },
    "available[]":{
        required:true
    },
    sell_price:{
        numericchar:true,
        required:function(){
            return $("#product_type option:selected").val()=="1"
            }
        },
list_price:{
    numericchar:true
    ,
    maxlength:8
},
cost:{
    numericchar:true,
    maxlength:8
},
weight:{
    numericchar:true,
    maxlength:6,
    checkrequired:true
},
length:{
    numericchar:true,
    maxlength:6
},
width:{
    numericchar:true,
    maxlength:6
},
height:{
    numericchar:true,
    maxlength:6
},
in_Stock_units:
{
    numericchar:true,
    maxlength:6
},
threshold:{
    numericchar:true,
    maxlength:6
},
total_in_ellis_stock:{
    numericchar:true,
    maxlength
    :6
},
ellis_product_threshold:{
    numericchar:true,
    maxlength:6
},
gl_code_value:{
    digits:true,
    minlength:1,
    maxlength:10
},
    prog_code_value:{
        digits:true,
        minlength:1,
        maxlength:10
    }
},
messages:{
    product_type:{
        required:PRODUCT_TYPE_EMPTY
    },
    name:{
        required:NAME_EMPTY
        //alphanumericunderscoreapostrohpe:
       // PRODUCT_NAME_VALID
    },
    sku:{
        required:SKU_EMPTY,
        alphanumericunderscore:SKU_VALID
    },
    description:{
        required:DESCRIPTION_EMPTY
       // alphanumericspecialchar:DESCRIPTION_VALID
    },
    shipping_description:{
        required:SHIPPING_DESCRIPTION_EMPTY
        //alphanumericspecialchar:DESCRIPTION_VALID
    },
    "available[]":{
        required:AVAILABLE_EMPTY
    },
    sell_price:{
        required:
        SELLEIN_PRICE_EMPTY,
        numericchar:SELL_PRICE_VALID
    },
    list_price:{
        numericchar:LIST_PRICE_VALID
    },
    cost:{
        numericchar:
        COST_PRICE_VALID
    },
    weight:{
        numericchar:WEIGHT_VALID
    },
    width:{
        numericchar:WIDTH_VALID
    },
    length:{
        numericchar:LENGTH_VALID
    },
    height:{
        numericchar:HEIGHT_VALID
    },
    in_Stock_units:{
        numericchar:IN_STOCK_VALID
    },
    threshold:{
        numericchar:THRESHOLD_VALID
    },
    total_in_ellis_stock:{
        numericchar:IN_STOCK_VALID
    },
    ellis_product_threshold:{
        numericchar:THRESHOLD_VALID
    },
    default_image:{
        required:IMG_DEFAULT_EMPTY
    }
},
highlight:function(label){
    $(".detail-section").css("display","block")
    }
});
$(document).on(
    "click","#add_new_file",function(){
        showAjxLoader();
        var i=$("#upload_count_div").text(),allDivBlock=$(
            "div[id^='upload_container_']");
        if(allDivBlock.length==1){
            var parId=$(this).parent().parent().attr("id"),id=parId.
            substring(parId.lastIndexOf("_")+1,parId.length);
            $(this).parent().append(
                "<a  class='minus m-l-10' onclick=remove_dynamic_contacts('.upload_container_"+id+"','"+id+"')>")
            }
            $(this).remove();
        $(
            "<div />",{
                "class":"row upload_container_"+i,
                id:"upload_container_"+i
                }).appendTo("#load_upload");
        $(".upload_container_"+
            i).load("/product-upload/"+i+"/upload_container_",function(){
            hideAjxLoader()
            });
        i++;
        $("#upload_count_div").html(i);
        $(
            "#publicdocs_upload_count").val(i)
        });
var url="/upload-product-files";
$("#file_name0").fileupload({
    url:url,
    dataType:
    "json",
    start:function(e){
        showAjxLoader()
        },
    done:function(e,data){
        data=data.result;
        hideAjxLoader();
        if(data.status==
            "success"){
            $("#filemessage_0").addClass("success-msg");
            $("#filemessage_0").removeClass("error");
            $("#file_name_value0").
            val(data.filename)
            }else{
            $("#filemessage_0").removeClass("success-msg");
            $("#filemessage_0").addClass("error");
            $(
                "#file_name_value0").val("")
            }
            $("#filemessage_0").html(data.message)
        },
    start:function(e){
        showAjxLoader()
        },
    stop:function(e)

    {
        hideAjxLoader()
        }
    });
$("#remove").click(function(){
    var imageId=[];
    $("#upload_old_div").hide();
    $("#upload_new_div").show()
    ;
    $("#thumbnail_virtualface").val("");
    imageId.push($(this).attr("id"))
    });
$("#product_name").autocomplete({
    afterAdd:true,
    selectFirst:true,
    autoFocus:true,
    source:function(request,response){
        $("#product_name").addClass("auto-suggest-loading");
        $.
        ajax({
            url:"/get-products",
            method:"POST",
            dataType:"json",
            data:{
                product_name:$("#product_name").val(),
                product_type:$(
                    "#product_type").val()
                },
            success:function(jsonResult){
                $("#product_name").removeClass("auto-suggest-loading");
                if(!
                    checkUserAuthenticationAjx(jsonResult))return false;
                var resultset=[];
                $.each(jsonResult,function(){
                    var id=this.product_id
                    ,value=this.product_name;
                    resultset.push({
                        id:this.product_id,
                        value:this.product_name
                        })
                    });
                response(resultset)
                }
            })
    },
open:
function(){
    $(".ui-menu").width(350)
    },
change:function(event,ui){
    if(ui.item){
        $("#product_name").val(ui.item.value);
        $(
            "#product_id").val(ui.item.id)
        }else{
        $("#product_name").val("");
        $("#product_id").val("")
        }
    },
focus:function(event,ui){
    $(
        "#product_id").val(ui.item.id);
    return false
    },
select:function(event,ui){
    if(ui.item.id!=""){
        $("#product_name").val(ui.item
            .value);
        $("#product_id").val(ui.item.id)
        }else{
        $("#product_name").val("");
        $("#product_id").val("")
        }
        var product=$(
        "#product_name").val(),id=$("#product_id").val();
    if(id){
        if($("#select_product li:last").attr("id")!=id){
            var liId=[];
            $(
                "ul#select_product li").each(function(i){
                liId.push($(this).attr("id"))
                });
            if($.inArray(id,liId)==-1)$("#select_product").
                append("<li id='"+id+"'>"+product+"<input type='hidden' name='related_product_id[]' value='"+id+"'> <span onClick=$('#"+
                    id+"').remove();>x</span></li>")
                }
                $("#product_name").val("")
        }
        return false
    }
});
$($(".e4")).each(function(i){
    if($(this).is(
        "[type=checkbox],[type=radio]")){
        var input=$(this),input_class=input.attr("class").split(" ");
        input.attr({
            id:input_class
            ["1"]+"_"+i
            });
        var label=$("label[for="+input.attr("id")+"]"),outerLabel=input.parent(),outerLabelParent=input.parent().
        parent(),inputType=input.is("[type=checkbox]")?"checkbox":"radio",oDiv=$(document.createElement("label")).attr({
            "for":
            input_class["1"]+"_"+i
            }).html($(outerLabel).text());
        $(outerLabel).remove();
        outerLabelParent.append(input,oDiv)
        }
    });
$($(
    ".e3")).each(function(i){
    if($(this).is("[type=checkbox],[type=radio]")){
        var input=$(this),input_class=input.attr("class"
            ).split(" ");
        input.attr({
            id:input_class["1"]+"_"+i
            });
        var label=$("label[for="+input.attr("id")+"]"),outerLabel=input.
        parent(),outerLabelParent=input.parent().parent(),inputType=input.is("[type=checkbox]")?"checkbox":"radio",oDiv=$(
            document.createElement("label")).attr({
            "for":input_class["1"]+"_"+i
            }).html($(outerLabel).text());
        $(outerLabel).remove();
        outerLabelParent.append(input,oDiv)
        }
    });
if($(".is_shippable").is(":checked")==true){
    $(".shipping_cost").parent().show();
    $
    (".shipping_cost").next().show();
    $(".shipping_cost").show()
    }else{
    $(".shipping_cost").next().hide();
    $(".shipping_cost").
    hide()
    }
    $("#available_0").click(function(){
    if($("#available_0").is(":checked")){
        var value=$(
            "#product_type option:selected").text();
        if(value=="Inventory")$("#stock_Div").show()
            }else{
        $("#stock_Div").hide();
        $(
            "#in_Stock_div").hide();
        $("#in_stock_2").attr("checked",false);
        $("#createProduct [for='in_stock_2']").removeClass(
            "checked");
        $("#editProduct [for='in_stock_2']").removeClass("checked")
        }
    });
$("#available_1").click(function(){
    if($(
        "#available_1").is(":checked")){
        var value=$("#product_type option:selected").text();
        if(value=="Inventory")$(
            "#ellis_stock_Div").show()
            }else{
        $("#ellis_stock_Div").hide();
        $("#ellis_in_Stock_div").hide();
        $("#in_ellis_stock_3").attr
        ("checked",false);
        $("#createProduct [for='in_ellis_stock_3']").removeClass("checked");
        $(
            "#editProduct [for='in_ellis_stock_3']").removeClass("checked")
        }
    });
$(".is_shippable").change(function(){
    if($(
        ".is_shippable").is(":checked")==true){
        $(".shipping_cost").parent().show();
        $(".shipping_cost").next().show();
        $(
            ".shipping_cost").show()
        }else{
        $(".shipping_cost").parent().hide();
        $(".shipping_cost").next().hide();
        $(".shipping_cost").
        hide()
        }
    })
});
function remove_dynamic_contacts(classname,i){
    $(classname).remove();
    var lastBlock=$(
        "div[id^='upload_container_']");
    if($(lastBlock).find(".plus").length==0)$("#load_upload #upload_container_"+(parseInt(i)
        -1)+" .addfile").append('<a href="javascript:void(0);" id="add_new_file" class="plus m-l-10"></a>');
    var allDivBlock=$(
        "div[id^='upload_container_']");
    if(allDivBlock.length==1)$(lastBlock).find(".minus").remove()
        }
        showOptions($(
    "#product_type").val());
function showOptions(val){
    var value=$("#product_type option:selected").text();
    if(value!=
        "Inventory"){
        $("#categoryHeading").hide();
        if(value=="Wall of Honor")$("#additional_products_div").show();else $(
            "#additional_products_div").hide();
        $(".detail-section").css("display","block");
        $("#measurement_Div").hide();
        $(
            "#in_stock_2").attr("checked",false);
        $("#in_ellis_stock_3").attr("checked",false);
        $("#createProduct [for='in_stock_2']")
        .removeClass("checked");
        $("#createProduct [for='in_ellis_stock_3']").removeClass("checked");
        $(
            "#editProduct [for='in_stock_2']").removeClass("checked");
        $("#editProduct [for='in_ellis_stock_3']").removeClass(
            "checked");
        $("#stock_Div").hide();
        $("#in_Stock_div").hide();
        $("#ellis_stock_Div").hide();
        $("#ellis_in_Stock_div").hide()
        ;
        $("#featured_Div").hide();
        $("#required").hide();
        $("#measurement_Div").show()
        }else{
        $("#categoryHeading").show();
        $(
            ".detail-section").css("display","block");
        $("#measurement_Div").show();
        if($("#available_0").is(":checked"))$(
            "#stock_Div").show();
        if($("#available_1").is(":checked"))$("#ellis_stock_Div").show();
        $("#featured_Div").show();
        $(
            "#required").show();
        $("#additional_products_div").hide()
        }
        if(val!="1"&&val!="2"&&val!="3"&&val)$("#relatedProductDiv").
        show();else $("#relatedProductDiv").hide()
        }
        function showInStock(){
    if($('#editProduct [name="in_stock"]').is(":checked")
        ||$('#createProduct [name="in_stock"]').is(":checked"))$("#in_Stock_div").show();else $("#in_Stock_div").hide()
        }
        function
showInEllisStock(){
    if($('#editProduct [name="in_ellis_stock"]').is(":checked")||$(
        '#createProduct [name="in_ellis_stock"]').is(":checked"))$("#ellis_in_Stock_div").show();else $("#ellis_in_Stock_div").
        hide()
        }
        function deleteProduct(id){
    $(".delete_product").colorbox({
        width:"700px",
        height:"285px",
        inline:true
    });
    $(
        "#no_delete_search").click(function(){
        $.colorbox.close();
        $("#yes_delete_search").unbind("click");
        return false
        });
    $(
        "#yes_delete_search").click(function(){
        $.ajax({
            type:"POST",
            data:{
                id:id
            },
            url:"/delete-crm-product",
            success:function(
                response){
                if(!checkUserAuthenticationAjx(response))return false;
                var jsonObj=jQuery.parseJSON(response);
                if(jsonObj.status
                    =="success")location.reload()
                    }
                })
    })
}