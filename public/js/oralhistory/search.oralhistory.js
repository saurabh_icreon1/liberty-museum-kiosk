$(function() {
    $("#OralHistory").validate({submitHandler: function(event, typeval) {
            $("#page").val("1");
        $("#limit").val("6");
        if (typeval == "undefined") {
            $("#type").val("");
            $("#types").attr("class", "ohistory-match-box button active")
        }
        $("#start_index").val("0");
        $("#load-more-btn").show();
        showAjxLoader();
        var datastr = $("#OralHistory").serialize();
        $.ajax({type: "POST", data: {searchString: datastr}, url: "/oral-history-list", success: function(responseData) {
                hideAjxLoader();
                if (document.getElementById("save_search"))
                    document.getElementById("save_search").style.display =
                            "block";
                document.getElementById("typediv").style.display = "block";
                document.getElementById("sortlabel").style.display = "block";
                document.getElementById("sortbyfield").style.display = "block";
                document.getElementById("sortorderfield").style.display = "block";
                $(".common-savesearch").parent().attr("class", "search-save");
                $(".sort-by-pass").show();
                $("#searchResult").html(responseData)
            }})
        }, rules: {}, messages: {}});
    $("#save_search").validate({submitHandler: function() {
            showAjxLoader();
            $.ajax({type: "POST", data: {search_name: $.trim($("#save_search_as").val()),
                    searchString: $("#OralHistory").serialize()}, url: "/save-oral-history-search", success: function(response) {
                    var jsonObj = jQuery.parseJSON(response);
                    if (jsonObj.status == "success") {
                        $("#save_search_as").val("");
                        $("#search_msg").removeClass("error");
                        document.getElementById("OralHistory").reset();
                        location.href = "/oral-history-library"
                    } else
                        $("#search_msg").html("");
                    hideAjxLoader()
                }})
        }, rules: {save_search_as: {required: true}}, messages: {save_search_as: {required: "Please enter search title"}}});
    $("#sortbyfield").change(function() {
        $("#sort_field").val($("#sortbyfield").val());
        $("#sort_order").val("asc");
        $("#sortorderfield").prop("selectedIndex", 0);
        $("#sortorderfield").select2();
        $("#search").trigger("click")
    });
    $("#savesearch").click(function() {
        $("#save_search").submit()
    });
    $("#sortorderfield").change(function() {
        $("#sort_order").val($("#sortorderfield").val());
        $("#search").trigger("click")
    })
});
function viewSavedSearch(search_id) {
    $.ajax({type: "POST", data: {search_id: search_id}, url: "/get-user-saved-search-select", success: function(response) {
            hideAjxLoader();
            var searchParamArray = response;
            var obj = jQuery.parseJSON(searchParamArray);
            document.getElementById("OralHistory").reset();
            $("#first_name").val(obj["first_name"]);
            $("#last_name").val(obj["last_name"]);
            $("#country").val(obj["country"]).select2();
            $("#topic").val(obj["topic"]).select2();
            $("#type").val(obj["type"]);
            $("#typediv").find(".active").removeClass("active");
            $("#types" + obj["type"] + "").addClass("active");
            $("#sort_field").val(obj["sort_field"]);
            $("#sortbyfield").val(obj["sort_field"]).select2();
            $("#sort_order").val(obj["sort_order"]);
            $("#sortorderfield").val(obj["sort_order"]).select2();
            $("#search").click()
        }})
}
function deleteSaveSearch(search_id) {
    $(".deletesearch").colorbox({width: "700px", height: "300px", inline: true});
    $("#no").click(function() {
        $.colorbox.close();
        $("#yes").unbind("click");
        return false
    });
    $("#yes").click(function() {
        $.ajax({type: "POST", data: {search_id: search_id}, url: "/delete-user-search", success: function(response) {
                var jsonObj = jQuery.parseJSON(response);
                if (jsonObj.status === "success") {
                    $.colorbox.close();
                    location.href = "/oral-history-library"
                }
            }})
    })
}
function loadMore(sort_field, sort_order, limit, start_index) {
    var page = parseInt($("#page").val());
    $("#sort_field").val(sort_field);
    $("#sort_order").val(sort_order);
    $("#limit").val(limit);
    $("#start_index").val(start_index);
    if ($("#loadingDiv" + "-" + page))
        $("#loadingDiv" + "-" + page).html("Loading.....");
    $("#page").val(page + 1);
    $("#load_more" + page).html("");
    var dataStr = $("#OralHistory").serialize();
    $.ajax({type: "POST", data: {searchString: dataStr, sort_field: sort_field, sort_order: sort_order, limit: limit}, url: "/more-oral-histories",
        success: function(responseData) {
            $("#loadingDiv" + "-" + page).html("");
            $("#lastDiv").append(responseData);
            $("#load_more" + "-" + page).html("")
        }})
}
function checkuserlogin() {
    $(".loginsignup").colorbox({width: "700px", height: "250px", inline: true});
    $("#cancel").click(function() {
        $.colorbox.close();
        return false
    });
    $("#login").click(function() {
        loginPopup("profile");
        return false
    });
    $("#register").click(function() {
        signupPopup();
        return false
    })
}
function sortSearchShip(fieldName) {
    $("#sort_field").val(fieldName);
    $("#start_index").val("0");
    $("#page").val("1");
    if ($("#sort_order").val() == "asc" || $("#sort_order").val() == "")
        $("#sort_order").val("desc");
    else
        $("#sort_order").val("asc");
    $("#search").trigger("click", "type")
}
function validitycheck(value) {
    if (value == "notallow")
        $(".notallow").colorbox({width: "600px", height: "260px", inline: true});
    else if (value == "exceed")
        $(".exceed").colorbox({width: "600px", height: "320px", inline: true})
}
function searchTypeResult(typeid) {
    $("#type").val(typeid);
    $("#types").attr("class", "ohistory-match-box");
    $("#types1").attr("class", "ohistory-match-box");
    $("#types2").attr("class", "ohistory-match-box");
    $("#types" + typeid + "").addClass("active");
    $("#search").trigger("click", "type")
}
function getOralHistoryFile(fileType, file) {
    authenticateUserBeforeAjax();
    if (fileType == "audio")
        $.colorbox({width: "600px", height: "300px", iframe: true, href: "/oral-history-audio/" + file});
    else if (fileType == "text")
        $.colorbox({width: "800px", height: "500px", iframe: true, href: "/oral-history-text/" + file})
}
;
