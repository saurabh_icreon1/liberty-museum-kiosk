(function(factory){if(typeof define==="function"&&define.amd)define(["jquery"],factory);else factory(jQuery)})(function(
$){var _baseDate=_generateBaseDate(),_ONE_DAY=86400,_defaults={className:null,minTime:null,maxTime:null,durationTime:
null,step:30,showDuration:false,timeFormat:"g:ia",scrollDefaultNow:false,scrollDefaultTime:false,selectOnBlur:false,
disableTouchKeyboard:true,forceRoundTime:false,appendTo:"body",disableTimeRanges:[],closeOnWindowScroll:false,
disableTextInput:false},_lang={decimal:".",mins:"mins",hr:"hr",hrs:"hrs"},methods={init:function(options){return this.
each(function(){var self=$(this);if(self[0].tagName=="SELECT"){var attrs={type:"text",value:self.val()},raw_attrs=self[0
].attributes;for(var i=0;i<raw_attrs.length;i++)attrs[raw_attrs[i].nodeName]=raw_attrs[i].nodeValue;var input=$(
"<input />",attrs);self.replaceWith(input);self=input}var settings=$.extend({},_defaults);if(options)settings=$.extend(
settings,options);if(settings.lang)_lang=$.extend(_lang,settings.lang);settings=_parseSettings(settings);self.data(
"timepicker-settings",settings);self.prop("autocomplete","off");self.on("click.timepicker focus.timepicker",methods.show
);self.on("blur.timepicker",_formatValue);self.on("keydown.timepicker",_keydownhandler);self.on("keyup.timepicker",
_keyuphandler);self.addClass("ui-timepicker-input");_formatValue.call(self.get(0))})},show:function(e){var self=$(this),
settings=self.data("timepicker-settings");if(_hideKeyboard(self))self.blur();var list=self.data("timepicker-list");if(
self.prop("readonly"))return;if(!list||list.length===0||typeof settings.durationTime==="function"){_render(self);list=
self.data("timepicker-list")}if(list.is(":visible"))return;methods.hide();list.show();if(self.offset().top+self.
outerHeight(true)+list.outerHeight()>$(window).height()+$(window).scrollTop())list.offset({left:self.offset().left+
parseInt(list.css("marginLeft").replace("px",""),10),top:self.offset().top-list.outerHeight()+parseInt(list.css(
"marginTop").replace("px",""),10)});else list.offset({left:self.offset().left+parseInt(list.css("marginLeft").replace(
"px",""),10),top:self.offset().top+self.outerHeight()+parseInt(list.css("marginTop").replace("px",""),10)});var selected
=list.find(".ui-timepicker-selected");if(!selected.length)if(_getTimeValue(self))selected=_findRow(self,list,_time2int(
_getTimeValue(self)));else if(settings.scrollDefaultNow)selected=_findRow(self,list,_time2int(new Date()));else if(
settings.scrollDefaultTime!==false)selected=_findRow(self,list,_time2int(settings.scrollDefaultTime));if(selected&&
selected.length){var topOffset=list.scrollTop()+selected.position().top-selected.outerHeight();list.scrollTop(topOffset)
}else list.scrollTop(0);_attachCloseHandler(settings);self.trigger("showTimepicker")},hide:function(e){$(
".ui-timepicker-wrapper:visible").each(function(){var list=$(this),self=list.data("timepicker-input"),settings=self.data
("timepicker-settings");if(settings&&settings.selectOnBlur)_selectValue(self);list.hide();self.trigger("hideTimepicker")
})},option:function(key,value){var self=this,settings=self.data("timepicker-settings"),list=self.data("timepicker-list")
;if(typeof key=="object")settings=$.extend(settings,key);else if(typeof key=="string"&&typeof value!="undefined")
settings[key]=value;else if(typeof key=="string")return settings[key];settings=_parseSettings(settings);self.data(
"timepicker-settings",settings);if(list){list.remove();self.data("timepicker-list",false)}return self},
getSecondsFromMidnight:function(){return _time2int(_getTimeValue(this))},getTime:function(){var self=this,today=new Date
();today.setHours(0,0,0,0);return new Date(today.valueOf()+_time2int(_getTimeValue(self))*1000)},setTime:function(value)
{var self=this,prettyTime=_int2time(_time2int(value),self.data("timepicker-settings").timeFormat);_setTimeValue(self,
prettyTime)},remove:function(){var self=this;if(!self.hasClass("ui-timepicker-input"))return;self.removeAttr(
"autocomplete","off");self.removeClass("ui-timepicker-input");self.removeData("timepicker-settings");self.off(
".timepicker");if(self.data("timepicker-list"))self.data("timepicker-list").remove();self.removeData("timepicker-list")}
};function _parseSettings(settings){if(settings.minTime)settings.minTime=_time2int(settings.minTime);if(settings.maxTime
)settings.maxTime=_time2int(settings.maxTime);if(settings.durationTime&&typeof settings.durationTime!=="function")
settings.durationTime=_time2int(settings.durationTime);if(settings.disableTimeRanges.length>0){for(var i in settings.
disableTimeRanges)settings.disableTimeRanges[i]=[_time2int(settings.disableTimeRanges[i][0]),_time2int(settings.
disableTimeRanges[i][1])];settings.disableTimeRanges=settings.disableTimeRanges.sort(function(a,b){return a[0]-b[0]})}
return settings}function _render(self){var settings=self.data("timepicker-settings"),list=self.data("timepicker-list");
if(list&&list.length){list.remove();self.data("timepicker-list",false)}list=$("<ul />",{"class":"ui-timepicker-list"});
var wrapped_list=$("<div />",{"class":"ui-timepicker-wrapper",tabindex:-1});wrapped_list.css({display:"none",position:
"absolute"}).append(list);if(settings.className)wrapped_list.addClass(settings.className);if((settings.minTime!==null||
settings.durationTime!==null)&&settings.showDuration)wrapped_list.addClass("ui-timepicker-with-duration");var durStart=
settings.minTime;if(typeof settings.durationTime==="function")durStart=_time2int(settings.durationTime());else if(
settings.durationTime!==null)durStart=settings.durationTime;var start=settings.minTime!==null?settings.minTime:0,end=
settings.maxTime!==null?settings.maxTime:start+_ONE_DAY-1;if(end<=start)end+=_ONE_DAY;var dr=settings.disableTimeRanges,
drCur=0,drLen=dr.length;for(var i=start;i<=end;i+=settings.step*60){var timeInt=i%_ONE_DAY,row=$("<li />");row.data(
"time",timeInt);row.text(_int2time(timeInt,settings.timeFormat));if((settings.minTime!==null||settings.durationTime!==
null)&&settings.showDuration){var duration=$("<span />");duration.addClass("ui-timepicker-duration");duration.text(" ("+
_int2duration(i-durStart)+")");row.append(duration)}if(drCur<drLen){if(timeInt>=dr[drCur][1])drCur+=1;if(dr[drCur]&&
timeInt>=dr[drCur][0]&&timeInt<dr[drCur][1])row.addClass("ui-timepicker-disabled")}list.append(row)}wrapped_list.data(
"timepicker-input",self);self.data("timepicker-list",wrapped_list);var appendTo=settings.appendTo;if(typeof appendTo===
"string")appendTo=$(appendTo);else if(typeof appendTo==="function")appendTo=appendTo(self);appendTo.append(wrapped_list)
;_setSelected(self,list);list.on("click","li",function(e){self.off("focus.timepicker");self.on(
"focus.timepicker-ie-hack",function(){self.off("focus.timepicker-ie-hack");self.on("focus.timepicker",methods.show)});
if(!_hideKeyboard(self))self[0].focus();list.find("li").removeClass("ui-timepicker-selected");$(this).addClass(
"ui-timepicker-selected");if(_selectValue(self))wrapped_list.hide()})}function _generateBaseDate(){return new Date(1970,
1,1,0,0,0)}function _attachCloseHandler(settings){if("ontouchstart"in document)$("body").on("touchstart.ui-timepicker",
_closeHandler);else{$("body").on("mousedown.ui-timepicker",_closeHandler);if(settings.closeOnWindowScroll)$(window).on(
"scroll.ui-timepicker",_closeHandler)}}function _closeHandler(e){var target=$(e.target),input=target.closest(
".ui-timepicker-input");if(input.length===0&&target.closest(".ui-timepicker-wrapper").length===0){methods.hide();$(
"body").unbind(".ui-timepicker");$(window).unbind(".ui-timepicker")}}function _hideKeyboard(self){var settings=self.data
("settings");return(window.navigator.msPointerEnabled||"ontouchstart"in document)&&settings.disableTouchKeyboard}
function _findRow(self,list,value){if(!value&&value!==0)return false;var settings=self.data("timepicker-settings"),out=
false,halfStep=settings.step*30;list.find("li").each(function(i,obj){var jObj=$(obj),offset=jObj.data("time")-value;if(
Math.abs(offset)<halfStep||offset==halfStep){out=jObj;return false}});return out}function _setSelected(self,list){list.
find("li").removeClass("ui-timepicker-selected");var timeValue=_time2int(_getTimeValue(self));if(!timeValue)return;var 
selected=_findRow(self,list,timeValue);if(selected){var topDelta=selected.offset().top-list.offset().top;if(topDelta+
selected.outerHeight()>list.outerHeight()||topDelta<0)list.scrollTop(list.scrollTop()+selected.position().top-selected.
outerHeight());selected.addClass("ui-timepicker-selected")}}function _formatValue(){if(this.value==="")return;var self=$
(this),seconds=_time2int(this.value);if(seconds===null){self.trigger("timeFormatError");return}var settings=self.data(
"timepicker-settings");if(settings.minTime!==null&&seconds<settings.minTime)self.trigger("timeRangeError");else if(
settings.maxTime!==null&&seconds>settings.maxTime)self.trigger("timeRangeError");$.each(settings.disableTimeRanges,
function(){if(seconds>=this[0]&&seconds<this[1]){self.trigger("timeRangeError");return false}});if(settings.
forceRoundTime){var offset=seconds%(settings.step*60);if(offset>=settings.step*30)seconds+=settings.step*60-offset;else 
seconds-=offset}var prettyTime=_int2time(seconds,settings.timeFormat);_setTimeValue(self,prettyTime)}function 
_getTimeValue(self){if(self.is("input"))return self.val();else return self.data("ui-timepicker-value")}function 
_setTimeValue(self,value){if(self.is("input"))self.val(value);else self.data("ui-timepicker-value",value)}function 
_keydownhandler(e){var self=$(this),list=self.data("timepicker-list");if(!list||!list.is(":visible"))if(e.keyCode==40){
if(!_hideKeyboard(self))self.focus()}else return _screenInput(e,self);switch(e.keyCode){case 13:if(_selectValue(self))
methods.hide.apply(this);e.preventDefault();return false;case 38:var selected=list.find(".ui-timepicker-selected");if(!
selected.length){list.find("li").each(function(i,obj){if($(obj).position().top>0){selected=$(obj);return false}});
selected.addClass("ui-timepicker-selected")}else if(!selected.is(":first-child")){selected.removeClass(
"ui-timepicker-selected");selected.prev().addClass("ui-timepicker-selected");if(selected.prev().position().top<selected.
outerHeight())list.scrollTop(list.scrollTop()-selected.outerHeight())}return false;case 40:var selected=list.find(
".ui-timepicker-selected");if(selected.length===0){list.find("li").each(function(i,obj){if($(obj).position().top>0){
selected=$(obj);return false}});selected.addClass("ui-timepicker-selected")}else if(!selected.is(":last-child")){
selected.removeClass("ui-timepicker-selected");selected.next().addClass("ui-timepicker-selected");if(selected.next().
position().top+2*selected.outerHeight()>list.outerHeight())list.scrollTop(list.scrollTop()+selected.outerHeight())}
return false;case 27:list.find("li").removeClass("ui-timepicker-selected");list.hide();break;case 9:methods.hide();break
;default:return _screenInput(e,self)}}function _screenInput(e,self){return!self.data("timepicker-settings").
disableTextInput||e.ctrlKey||e.altKey||e.metaKey||e.keyCode!=2&&e.keyCode<46}function _keyuphandler(e){var self=$(this),
list=self.data("timepicker-list");if(!list||!list.is(":visible"))return true;switch(e.keyCode){case 96:case 97:case 98:
case 99:case 100:case 101:case 102:case 103:case 104:case 105:case 48:case 49:case 50:case 51:case 52:case 53:case 54:
case 55:case 56:case 57:case 65:case 77:case 80:case 186:case 8:case 46:_setSelected(self,list);break;default:return}}
function _selectValue(self){var settings=self.data("timepicker-settings"),list=self.data("timepicker-list"),timeValue=
null,cursor=list.find(".ui-timepicker-selected");if(cursor.hasClass("ui-timepicker-disabled"))return false;if(cursor.
length)timeValue=cursor.data("time");else if(_getTimeValue(self)){timeValue=_time2int(_getTimeValue(self));_setSelected(
self,list)}if(timeValue!==null){var timeString=_int2time(timeValue,settings.timeFormat);_setTimeValue(self,timeString)}
self.trigger("change").trigger("changeTime");return true}function _int2duration(seconds){var minutes=Math.round(seconds/
60),duration;if(Math.abs(minutes)<60)duration=[minutes,_lang.mins];else if(minutes==60)duration=["1",_lang.hr];else{var 
hours=(minutes/60).toFixed(1);if(_lang.decimal!=".")hours=hours.replace(".",_lang.decimal);duration=[hours,_lang.hrs]}
return duration.join(" ")}function _int2time(seconds,format){if(seconds===null)return;var time=new Date(_baseDate.
valueOf()+seconds*1000),output="",hour,code;for(var i=0;i<format.length;i++){code=format.charAt(i);switch(code){case"a":
output+=time.getHours()>11?"pm":"am";break;case"A":output+=time.getHours()>11?"PM":"AM";break;case"g":hour=time.getHours
()%12;output+=hour===0?"12":hour;break;case"G":output+=time.getHours();break;case"h":hour=time.getHours()%12;if(hour!==0
&&hour<10)hour="0"+hour;output+=hour===0?"12":hour;break;case"H":hour=time.getHours();output+=hour>9?hour:"0"+hour;break
;case"i":var minutes=time.getMinutes();output+=minutes>9?minutes:"0"+minutes;break;case"s":seconds=time.getSeconds();
output+=seconds>9?seconds:"0"+seconds;break;default:output+=code}}return output}function _time2int(timeString){if(
timeString==="")return null;if(!timeString||timeString+0==timeString)return timeString;if(typeof timeString=="object")
timeString=timeString.getHours()+":"+_pad2(timeString.getMinutes())+":"+_pad2(timeString.getSeconds());timeString=
timeString.toLowerCase();var d=new Date(0),time;if(timeString.indexOf(":")===-1){time=timeString.match(
/^([0-9]):?([0-5][0-9])?:?([0-5][0-9])?\s*([pa]?)m?$/);if(!time)time=timeString.match(
/^([0-2][0-9]):?([0-5][0-9])?:?([0-5][0-9])?\s*([pa]?)m?$/)}else time=timeString.match(
/^(\d{1,2})(?::([0-5][0-9]))?(?::([0-5][0-9]))?\s*([pa]?)m?$/);if(!time)return null;var hour=parseInt(time[1]*1,10),
hours;if(time[4])if(hour==12)hours=time[4]=="p"?12:0;else hours=hour+(time[4]=="p"?12:0);else hours=hour;var minutes=
time[2]*1||0,seconds=time[3]*1||0;return hours*3600+minutes*60+seconds}function _pad2(n){return("0"+n).slice(-2)}$.fn.
timepicker=function(method){if(methods[method])return methods[method].apply(this,Array.prototype.slice.call(arguments,1)
);else if(typeof method==="object"||!method)return methods.init.apply(this,arguments);else $.error("Method "+method+
" does not exist on jQuery.timepicker")}})