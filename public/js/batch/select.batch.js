$(function() {
    $.jgrid.no_legacy_api = true;
    $.jgrid.useJSON = true;
    $.validator.addMethod("alphanumeric", function(value, element) {
        return this.optional(element) || /^[a-z0-9]+$/i.test(value);
    }, "Please enter valid batch id");
    var grid = jQuery("#list");
    var emptyMsgDiv = $('<div class="no-record-msz">'+NO_RECORD_FOUND+'</div>');            
    $("#list").jqGrid({ 
        postData: {
            searchString:$("#search-template").serialize()
        },
        mtype: 'POST',
        url: '/get-batch-list'+'/'+id,
        datatype: "json", 
        sortable:true,
        colNames: ['Batch ID', 'Settlement Date', 'Amount ($)', 'Created By', 'Created Date', 'Modified Date', 'Action(s)'],
        colModel: [
        {
            name: 'Batch ID', 
            index: 'batchTab.batch_id',
            align: 'center',
            width: '80px'
        },
        {
            name: 'Settlement Date', 
            index: 'batchTab.settlement_date',
            align: 'center'
        },
        {
            name: 'Amount', 
            index: 'totalamount',
            align: 'center'
        },
        {
            name: 'Created By', 
            index: 'batchTab.added_by',
            align: 'center'
        },
        {
            name: 'Created Date', 
            index: 'batchTab.added_date',
            align: 'center'
        },
        {
            name: 'Modified Date', 
            index: 'batchTab.modified_date',
            align: 'center'
        },        
        {
            name: 'Action(s)', 
            index: 'action',
            sortable :false,
            align: 'center'
        }
        ],
        viewrecords: true, 
        sortname: "modified_date",
        sortorder: "DESC",
        rowNum:10, 
        rowList:[10,20,30], 
        pager: '#pcrud',
        viewrecords: true,
        autowidth: true,
        shrinkToFit: true,
        multiselect: true,
        caption:"",
        width:"100%",
        cmTemplate: {
            title: false
        },
        loadComplete: function () {
            hideAjxLoader();
            var count = grid.getGridParam();
            var ts = grid[0];
            if (ts.p.reccount === 0) {
                grid.hide();
                emptyMsgDiv.show();
                $("#pcrud_right div.ui-paging-info").css('display','none');
            } else {
                grid.show();
                emptyMsgDiv.hide();
                $("#pcrud_right div.ui-paging-info").css('display','block');
            }
        }
    }); 
    // place div with empty message insde of div
    emptyMsgDiv.insertAfter(grid.parent());
    emptyMsgDiv.hide();    
    // For paging 
    $("#list").jqGrid('navGrid','#pcrud',{
        reload: true,
        edit: false, 
        add: false, 
        search: false, 
        del: false
    });
    $("#search").click(function(){
        showAjxLoader();
        jQuery("#list").jqGrid('setGridParam',{
            postData:{
                searchString:$("#search-template").serialize()
            }
        });
        jQuery("#list").trigger('reloadGrid',[{
            page:1
        }]);
        window.location.hash = '#search-template';
        hideAjxLoader();
        return false;    
    });
    
    $("#statement_date").datepicker({
        changeMonth: true,
        changeYear: true,
        onSelect: function(dateText, inst) {
            $("#statement_date").next('label').remove();
        }
    });
    $("#settlement_date").datepicker({
        changeMonth: true,
        changeYear: true,
        onSelect: function(dateText, inst) {
            $("#settlement_date").next('label').remove();
        }
    });
    $("#settlement_date_edit").datepicker({
        changeMonth: true,
        changeYear: true,
        onSelect: function(dateText, inst) {
            $("#settlement_date_edit").next('label').remove();
        }
    });
	
    $("#create-batch-id").validate({
        submitHandler: function() {
            showAjxLoader();
            var dataStr = $('#create-batch-id').serialize();
            $.ajax({
                type: 'POST',
                data: dataStr,
                url: '/create-batch-id',
                success: function(responseData)
                {
                    var jsonObj = jQuery.parseJSON(responseData);
                    if (jsonObj.status === 'success') {
                        hideAjxLoader();
                        $("#batch_id").val('');
                        $("#settlement_date").val('');
                        $('#success_message').show();
                        $('#success_message').html('<div>'+jsonObj.message+'</div>');
                        $('#error_message').hide();
                        $('.accordion-detail .head:last').addClass('active');
                        $('.detail-section').eq(1).show();
                        $('.accordion-detail .head:first').attr('class','head');
                        emptyMsgDiv.hide();
                        $('.detail-section').eq(0).hide();
						$('#statement_date').val('');
						$('#search').click();
                        setTimeout(function(){
                            $('#success_message').hide();
                            $('#error_message').hide();
                        },2000);
                    }else{
                        hideAjxLoader();
                        $('#success_message').hide();
                        $('#error_message').show();						
                        $('#error_message').html('<div>'+jsonObj.message+'</div>');
                        $.each(jsonObj.message, function (i, msg) {
                            $("#"+i).after('<label class="error" style="display:block;">'+msg+'</label>');                          
                        });
                        $('.accordion-detail .head:first').addClass('active');
                        $('.detail-section').eq(0).show();
                        $('.accordion-detail .head:last').attr('class','head');
                        $('.detail-section').eq(1).hide();
						
                        setTimeout(function(){
                            $('#success_message').hide();
                            $('#error_message').hide();
                        },2000);
                    }
                    
                    if(jsonObj.incremented_batch_id != undefined && $.trim(jsonObj.incremented_batch_id) != "") { $('#batch_id').val($.trim(jsonObj.incremented_batch_id)); }
                }
            });            
        },
        rules: {
            batch_id: {
                required: true,
                alphanumeric: true,
                maxlength: 50
            },
            settlement_date: {
                required: true
            }
        },
        messages: {
            batch_id: {
                required: "Please enter batch id"
            },
            settlement_date: {
                required: "Please select settlement date"
            }
        }
    });

    $("#edit-batch-id").validate({
        submitHandler: function() { 
            showAjxLoader();
            var dataStr = $('#edit-batch-id').serialize();
            $.ajax({
                type: 'POST',
                data: dataStr,
                url: '/update-batch-id',
                success: function(responseData)
                {
                    var jsonObj = jQuery.parseJSON(responseData);
                    if (jsonObj.status === 'success') {
                        hideAjxLoader();
                        $("#batch_id").val('');
                        $("#batch_id_edit").val('');
                        $("#settlement_date_edit").val('');
                        $('#success_message').show();
                        $('#success_message').html('<div>'+jsonObj.message+'</div>');
                        $('#error_message').hide();
                        $('#create-batch-header').show();
                        $('#edit-batch-header').hide();
                        $('.accordion-detail .head:last').addClass('active');
                        $('.detail-section').eq(1).show();						
                        $('#create-batch-form').show();
                        $('#edit-batch-form').hide();
                        $('.accordion-detail .head:first').attr('class','head');
                        $('.detail-section').eq(0).hide();						
						$('#statement_date').val('');
						$('#search').click();
                        setTimeout(function(){
                            $('#success_message').hide();
                            $('#error_message').hide();
                        },2000);
                    }else{
                        hideAjxLoader();
                        $('#success_message').hide();
                        $('#error_message').show();						
                        $('#error_message').html('<div>'+jsonObj.message+'</div>');
                        $.each(jsonObj.message, function (i, msg) {
                            $("#"+i).after('<label class="error" style="display:block;">'+msg+'</label>');                          
                        });
                        $('.accordion-detail .head:first').addClass('active');
                        $('.detail-section').eq(0).show();
                        $('.accordion-detail .head:last').attr('class','head');
                        $('.detail-section').eq(1).hide();
						
                        setTimeout(function(){
                            $('#success_message').hide();
                            $('#error_message').hide();
                        },2000);
                    }
                    if(jsonObj.incremented_batch_id != undefined && $.trim(jsonObj.incremented_batch_id) != "") { $('#batch_id').val($.trim(jsonObj.incremented_batch_id)); } 
                }
            });
        },
        rules: {
            batch_id: {
                required: true,
                alphanumeric: true,
                maxlength: 50
            },
            settlement_date: {
                required: true
            }
        },
        messages: {
            batch_id: {
                required: "Please enter batch id"
            },
            settlement_date: {
                required: "Please select settlement date"
            }
        }
    });
});	

function selectBatch(batch_id,payment_batch_id,id)
{
    if(typeof(id)==='undefined'){
        parent.$("#batch_id").val(batch_id);
        parent.$("#payment_batch_id").val(payment_batch_id);
    }
    else{
        parent.$("#"+id).val(batch_id);
        parent.$("#payment_"+id).val(payment_batch_id);
    }

	if(typeof(parent.$('#file_name'))!=='undefined' && id=='batch_id_upload'){
		parent.$('#file_name').removeAttr('disabled');
	}
    
    parent.$("#batch_id").next('label').remove();
    parent.jQuery.colorbox.close();
    $("#cboxClose").click();
}

function lockBatch(id){
    $('#lock-batch-link-'+id).hide();
    $('#unlock-batch-link-'+id).show();
    $('#select-batch-link-'+id).hide();	
    $('#edit-batch-link-'+id).hide();

    $.ajax
    ({
        type: 'POST',
        url: '/update-batch-id',
        data:{
            payment_batch_id: id,
            is_locked : '1'
        },
        success: function(response)
        {
            
        }
    });
}

function unLockBatch(id){
    $('#unlock-batch-link-'+id).hide();
    $('#lock-batch-link-'+id).show();
    $('#select-batch-link-'+id).show();
    $('#edit-batch-link-'+id).show();
    $.ajax
    ({
        type: 'POST',
        url: '/update-batch-id',
        data:{
            payment_batch_id: id,
            is_locked : '0'
        },
        success: function(response)
        {
            
        }
    });
}



function lockSelectedBatch(){
    var records =  new Array();
    $("#list").find('input[type=checkbox]').each(function() {
        if( $(this).is(':checked')) {
            var payment_batch_id = $(this).attr('id');
            records.push(payment_batch_id);
        }
    });
    if(records.length > 0){
        $.ajax
        ({
            type: 'POST',
            url: '/update-batch-id',
            data:{
                payment_batch_ids: records,
                is_locked : '1'
            },
            success: function(response)
            {
                var jsonObj = jQuery.parseJSON(response);
                $('#success_message').show();
                $('#success_message').html('<div>'+jsonObj.message+'</div>');
                $('#error_message').hide();
                jQuery("#list").trigger('reloadGrid', [{
                    page: 1
                }]);
                setTimeout(function(){
                    $('#success_message').hide();
                    $('#error_message').hide();
                },2000);				
            }
        });
    }else{
        $('#success_message').hide();
        $('#error_message').show();						
        $('#error_message').html('<div>Please select batch to lock</div>');
		
        setTimeout(function(){
            $('#success_message').hide();
            $('#error_message').hide();
        },2000);
    }	
}

function editBatch(id){
    showAjxLoader();
    $('#create-batch-header').hide();
    $('#edit-batch-header').show();
    $('.accordion-detail .head:first').addClass('active');
    $('.detail-section').eq(0).show();
    $('.accordion-detail .head:last').attr('class','head');
    $('.detail-section').eq(1).hide();
    $('#create-batch-form').hide();
    $('#edit-batch-form').show();

    $.ajax
    ({
        type: 'POST',
        url: '/edit-batch-id',
        data:{
            payment_batch_id: id
        },
        success: function(response)
        {
            var jsonObj = jQuery.parseJSON(response);
            $('#batch_id_edit').val(jsonObj.batch_id);
            $('#old_batch_id').val(jsonObj.batch_id);
            $('#settlement_date_edit').val(jsonObj.settlement_date);
            $('#payment_batch_id_edit').val(id);
            hideAjxLoader();
        }
    });

}