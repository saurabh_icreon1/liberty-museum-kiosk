$(function(){
    $.jgrid.no_legacy_api=true;
    $.jgrid.useJSON=true;
    var grid=jQuery("#list");
    var emptyMsgDiv=$('<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");
    $("#list").jqGrid({
        postData:{
            searchString:$("#search-template").serialize()
        },
        mtype:"POST",
        url:"/get-transaction-list",
        datatype:"json",
        sortable:true,
        colNames:["ID","Contact Name", "Transaction #","Product Name", "Amount (USD)","Payment Mode", "Created By", "Transaction Date"],
        colModel:[{
            name:"Id",
            index:"Id",
            hidden:true,
            key:true
        },{
            name:"Contact Name",
            index:"contact_name"
        },{
            name:"Transaction #",
            index:"transaction_id"
        },{
            name:"Product Name",
            index:"product_name",
            align:"center"
        },{
            name:"Amount",
            index:"amount",
            align:"center"
        },{ 
            name:"Payment Mode",
            index:"payment_mode",
            align:"center"
        },{ 
            name:"Created By",
            index:"created_by",
            align:"center"
        },{ 
            name:"Transaction Date",
            index:"transaction_date",
            align:"center"
        }],
        viewrecords:true,
        sortname: "received_payment_id",
        sortorder:"DESC",
        rowNum:10,
        rowList:[10,20,30],
        pager:"#pcrud",
        multiselect:true,
        autowidth:true,
        shrinkToFit:true,
        caption:"",
        width:"100%",
        cmTemplate:{
            title:false
        },
        loadComplete:function(){
            hideAjxLoader();
            var count=grid.getGridParam();
            var ts=grid[0];
            if(ts.p.reccount===0){
                grid.hide();
                emptyMsgDiv.show();
                $("#pcrud_right div.ui-paging-info").css("display","none")
            }else{
                grid.show();
                emptyMsgDiv.hide();
                $("#pcrud_right div.ui-paging-info").css("display","block")
            }
        }
    });
    emptyMsgDiv.insertAfter(grid.parent());
    emptyMsgDiv.hide();
    $("#list").jqGrid("navGrid","#pcrud",{
        reload:true,
        edit:false,
        add:false,
        search:false,
        del:false
    });
    $("#search").click(function(){
        showAjxLoader();
        if($("#payment_batch_id").val()=="")$("#batch_id").after('<label class="error" style="display:block;">Please select batch id</label>');
        else{
            $("#batch_id").next("label").remove();
            var records=new Array;
            $("#list").find("input[type=checkbox]").each(function(){
                if($(this).is(":checked")){
                    var received_payment_id=$(this).attr("id");
                    records.push(received_payment_id)
                }
            });
            if(records.length==0)$("#gview_list").after('<label class="error" style="display:block;">Please select any transaction to update batch</label>');
            else{
                $("#gview_list").next("label").remove();
                $.ajax({
                    type:"POST",
                    url:"/assign-batch-id",
                    data:{
                        received_payment_ids:records,
                        payment_batch_id:$("#payment_batch_id").val()
                    },
                    success:function(response){
                        var jsonObj=
                        jQuery.parseJSON(response);
                        if(jsonObj.status=="success"){
                            $("#success_message").show();
                            $("#success_message").html("<div>"+jsonObj.message+"</div>");
                            $("#batch_id").val("");
                            $("#payment_batch_id").val("");
                            $("#error_message").hide();
                            jQuery("#list").trigger("reloadGrid",[{
                                page:1
                            }]);
                            setTimeout(function(){
                                $("#success_message").hide();
                                $("#error_message").hide()
                            },3E3)
                        }
                    }
                })
            }
        }
        hideAjxLoader();
        return false
    });
    
    $("#batch_id").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:function(request,response){
            $.ajax({
                url:"/get-available-batch",
                method:"POST",
                dataType:"json",
                data:{
                    batch_id: $("#batch_id").val(), 
					is_locked:'0'
                },
                success:function(jsonResult){
                    var resultset=[];
                    $.each(jsonResult,function(){
                        var id=this.payment_batch_id;
                        var value=this.batch_id;
                        resultset.push({
                            id:this.payment_batch_id,
                            value:this.batch_id
                        })
                    });
                    response(resultset)
                }
            })
            
        },
        open:function(){
            $(".ui-menu").width(350)
        },
        change:function(event,ui){
            if(ui.item){
                $("#batch_id").val(ui.item.value);
                $("#payment_batch_id").val(ui.item.id)
            }else{
                $("#batch_id").val("");
                $("#payment_batch_id").val("")
            }
        },
        focus:function(event,ui){
            $("#payment_batch_id").val(ui.item.id);
            return false
        },
        select:function(event,ui){
            $("#batch_id").val(ui.item.value);
            $("#payment_batch_id").val(ui.item.id);
            return false
        }
    })
    
    $("#cash_batch_id").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:function(request,response){
            showAjxLoader();
            $.ajax({
                url:"/get-available-batch",
                method:"POST",
                dataType:"json",
                data:{
                    batch_id: $("#cash_batch_id").val(), 
					is_locked:'0'
                },
                success:function(jsonResult){
                    var resultset=[];
                    $.each(jsonResult,function(){
                        var id=this.payment_batch_id;
                        var value=this.batch_id;
                        resultset.push({
                            id:this.payment_batch_id,
                            value:this.batch_id
                        })
                    });
                    response(resultset)
                }
            })
            hideAjxLoader();
        },
        open:function(){
            $(".ui-menu").width(350)
        },
        change:function(event,ui){
            if(ui.item){
                $("#cash_batch_id").val(ui.item.value);
                $("#payment_cash_batch_id").val(ui.item.id)
            }else{
                $("#cash_batch_id").val("");
                $("#payment_cash_batch_id").val("")
            }
        },
        focus:function(event,ui){
            $("#payment_batch_id").val(ui.item.id);
            return false
        },
        select:function(event,ui){
            $("#cash_batch_id").val(ui.item.value);
            $("#payment_cash_batch_id").val(ui.item.id);
            return false
        }
    })
    
    $("#check_batch_id_1").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:function(request,response){
            showAjxLoader();
            $.ajax({
                url:"/get-available-batch",
                method:"POST",
                dataType:"json",
                data:{
                    batch_id: $("#check_batch_id_1").val(), 
					is_locked:'0'
                },
                success:function(jsonResult){
                    var resultset=[];
                    $.each(jsonResult,function(){
                        var id=this.payment_batch_id;
                        var value=this.batch_id;
                        resultset.push({
                            id:this.payment_batch_id,
                            value:this.batch_id
                        })
                    });
                    response(resultset)
                }
            })
            hideAjxLoader();
        },
        open:function(){
            $(".ui-menu").width(350)
        },
        change:function(event,ui){
            if(ui.item){
                $("#check_batch_id_1").val(ui.item.value);
                $("#payment_check_batch_id_1").val(ui.item.id)
            }else{
                $("#check_batch_id_1").val("");
                $("#payment_check_batch_id_1").val("")
            }
        },
        focus:function(event,
            ui){
            $("#payment_check_batch_id_1").val(ui.item.id);
            return false
        },
        select:function(event,ui){
            $("#check_batch_id_1").val(ui.item.value);
            $("#payment_check_batch_id_1").val(ui.item.id);
            return false
        }
    }) 
	/***/
	$("#invoice_batch_id").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:function(request,response){
            showAjxLoader();
            $.ajax({
                url:"/get-available-batch",
                method:"POST",
                dataType:"json",
                data:{
                    batch_id: $("#invoice_batch_id").val(), 
					is_locked:'0'
                },
                success:function(jsonResult){
                    var resultset=[];
                    $.each(jsonResult,function(){
                        var id=this.payment_batch_id;
                        var value=this.batch_id;
                        resultset.push({
                            id:this.payment_batch_id,
                            value:this.batch_id
                        })
                    });
                    response(resultset)
                }
            })
            hideAjxLoader();
        },
        open:function(){
            $(".ui-menu").width(350)
        },
        change:function(event,ui){
            if(ui.item){
                $("#invoice_batch_id").val(ui.item.value);
                $("#payment_invoice_batch_id").val(ui.item.id)
            }else{
                $("#invoice_batch_id").val("");
                $("#payment_invoice_batch_id").val("")
            }
        },
        focus:function(event,ui){
            $("#payment_batch_id").val(ui.item.id);
            return false
        },
        select:function(event,ui){
            $("#invoice_batch_id").val(ui.item.value);
            $("#payment_invoice_batch_id").val(ui.item.id);
            return false
        }
    });
});


function abc(val)
{
    $("#auto_batch_id").val(val);
}
function searchBatchId(id){
    if(id == undefined){
        $.colorbox({
            href:"/select-batch-id",
            width:"1200px",
            height:"985px",
            iframe:true
        })
    }
    else{
        $.colorbox({
            href:"/select-batch-id/"+id,
            width:"1200px",
            height:"985px",
            iframe:true
        })
    }
}
    
function getUserTransactionDetail(transactionId){
    $.colorbox({
        width:"1350px",
        href:"/user-transaction-detail-crm/"+
        transactionId,
        height:"600px",
        opacity:0.50,
        inline:false,
        onOpen:function(){
            showAjxLoader()
        },
        onComplete:function(){
            hideAjxLoader()
        }
    })
}