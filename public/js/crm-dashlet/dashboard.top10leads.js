function drawDashletGrid9(dashletId,searchArr){
    var colNames="";
    var top = 1;
    for(var i=0;i<searchArr["colName"].length;i++)colNames=colNames+",'"+searchArr["colName"][i]+"'";
    var colAllNames=eval("["+colNames.substring(1)+"]");
    var colMods="";
    for(var i=0;i<searchArr["indexName"].length;i++)colMods=colMods+",{name: '"+searchArr["colName"][i]+"',index: '"+searchArr["indexName"][i]+"',sortable:false}";
    var colAllMods=eval("["+colMods.substring(1)+"]");
    var grid=jQuery("#list_9");
    var emptyMsgDiv=
    $('<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");
    $("#list_9").jqGrid({
        postData:{
            crm_dashlet:"dashlet",
            dashletId:dashletId,
            top: top         
        },
        mtype:"POST",
        url:"/search-leads",
        datatype:"json",
        colNames:colAllNames,
        colModel:colAllMods,
        viewrecords:true,
        sortname:searchArr["sortName"],
        sortorder:"desc",
        rowNum:itemCount_9,
        rowList:[10],
        pager:"#pcrud_9",
        viewrecords:true,
        autowidth:true,
        shrinkToFit:true,
        caption:"",
        width:"100%",
        cmTemplate:{
            title:false
        },
        loadComplete:function(){
            var count=grid.getGridParam();
            var ts=grid[0];
            if(ts.p.reccount===0){
                grid.hide();
                emptyMsgDiv.show();
                $("#pcrud_9_right div.ui-paging-info").css("display","none")
                }else{
                grid.show();
                emptyMsgDiv.hide();
                $("#pcrud_9_right div.ui-paging-info").css("display","block")
                }
            }
    });
emptyMsgDiv.insertAfter(grid.parent());
emptyMsgDiv.hide();
$("#list_9").jqGrid("navGrid","#pcrud_9",{
    reload:true,
    edit:false,
    add:false,
    search:false,
    del:false
})
}
$(function(){
    $("#added_date_from_9").datepicker({
        changeMonth:true,
        changeYear:true,
        showOtherMonths:true,
        onClose:function(selectedDate){
            $("#added_date_to_9").datepicker("option","minDate",selectedDate)
            }
        });
$("#added_date_to_9").datepicker({
    changeMonth:true,
    changeYear:true,
    showOtherMonths:true,
    onClose:function(selectedDate){
        $("#added_date_from_9").datepicker("option","maxDate",selectedDate)
        }
    })
});
$("#searchbutton_9").click(function(){
    showAjxLoader();
    jQuery("#list_9").jqGrid("setGridParam",{
        postData:{
            searchString:$("#search-form-9").serialize()
            }
        });
jQuery("#list_9").trigger("reloadGrid",[{
    page:1
}]);
hideAjxLoader();
    return false
    });
function showDate(id,divId){
    if($("#"+id+" option:selected").text()=="Choose Date Range")$("#"+divId).show();else $("#"+divId).hide()
        };
