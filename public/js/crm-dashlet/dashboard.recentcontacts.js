function drawDashletGrid6(dashletId,searchArr){
    var colNames="";
    for(var i=0;i<searchArr["colName"].length;i++)colNames=colNames+",'"+searchArr["colName"][i]+"'";
    var colAllNames=eval("["+colNames.substring(1)+"]");
    var colMods="";
    for(var i=0;i<searchArr["indexName"].length;i++)colMods=colMods+",{name: '"+searchArr["colName"][i]+"',index: '"+searchArr["indexName"][i]+"',sortable:'"+searchArr["sortable"][i]+"'}";
    var colAllMods=eval("["+colMods.substring(1)+"]");
    var grid=jQuery("#list_6");
    var emptyMsgDiv=
    $('<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");
    $("#list_6").jqGrid({
        postData:{
            crm_dashlet:"dashlet",
            dashletId:dashletId
        },
        mtype:"POST",
        url:"/recent-contact/"+searchArr["newContact"],
        datatype:"json",
        colNames:colAllNames,
        colModel:colAllMods,
        viewrecords:true,
        sortname:searchArr["sortName"],
        sortorder:"desc",
        rowNum:itemCount_6,
        rowList:[20,30],
        pager:"#pcrud_6",
        viewrecords:true,
        autowidth:true,
        shrinkToFit:true,
        caption:"",
        width:"100%",
        cmTemplate:{
            title:false
        },
        loadComplete:function(){
            var count=grid.getGridParam();
            var ts=grid[0];
            if(ts.p.reccount===0){
                grid.hide();
                emptyMsgDiv.show();
                $("#pcrud_6_right div.ui-paging-info").css("display","none")
                }else{
                grid.show();
                emptyMsgDiv.hide();
                $("#pcrud_6_right div.ui-paging-info").css("display","block")
                }
            }
    });
emptyMsgDiv.insertAfter(grid.parent());
emptyMsgDiv.hide();
$("#list_6").jqGrid("navGrid","#pcrud_6",{
    reload:true,
    edit:false,
    add:false,
    search:false,
    del:false
})
}
$(function(){
    $("#added_date_from_6").datepicker({
        changeMonth:true,
        changeYear:true,
        showOtherMonths:true,
        onClose:function(selectedDate){
            $("#added_date_to_6").datepicker("option","minDate",selectedDate)
            }
        });
$("#added_date_to_6").datepicker({
    changeMonth:true,
    changeYear:true,
    showOtherMonths:true,
    onClose:function(selectedDate){
        $("#added_date_from_6").datepicker("option","maxDate",selectedDate)
        }
    })
});
$("#searchbutton_6").click(function(){
    showAjxLoader();
    jQuery("#list_6").jqGrid("setGridParam",{
        postData:{
            searchString:$("#search-form-6").serialize()
            }
        });
jQuery("#list_6").trigger("reloadGrid",[{
    page:1
}]);
hideAjxLoader();
    return false
    });
function showDate(id,divId){
    if($("#"+id+" option:selected").text()=="Choose Date Range")$("#"+divId).show();else $("#"+divId).hide()
        };
