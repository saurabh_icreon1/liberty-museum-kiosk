function drawDashletGrid12(dashletId, searchArr) {
    var colNames = "";
    for (var i = 0; i < searchArr["colName"].length; i++)
        colNames = colNames + ",'" + searchArr["colName"][i] + "'";
    var colAllNames = eval("[" + colNames.substring(1) + "]");
    var colMods = "";
    for (var i = 0; i < searchArr["indexName"].length; i++)
        colMods = colMods + ",{name: '" + searchArr["colName"][i] + "',index: '" + searchArr["indexName"][i] + "',sortable:'" + searchArr["sortable"][i] + "'}";
    var colAllMods = eval("[" + colMods.substring(1) + "]");
    var grid = jQuery("#list_12");
    var emptyMsgDiv = $('<div class="no-record-msz">' + NO_RECORD_FOUND + "</div>");
    $("#list_12").jqGrid({postData: {crm_dashlet: "dashlet", dashletId: dashletId, transaction_date_period: "13"}, mtype: "POST", url: "/transaction-summary", datatype: "json", colNames: colAllNames, colModel: colAllMods, viewrecords: true, sortname: searchArr["sortName"], sortorder: "desc", rowNum: itemCount_12, rowList: [20, 30], pager: "#pcrud_12", viewrecords:true, autowidth: true, shrinkToFit: true, caption: "", width: "100%", cmTemplate: {title: false}, loadComplete: function() {
            var count =grid.getGridParam();
            var ts = grid[0];
            if (ts.p.reccount === 0) {
                grid.hide();
                emptyMsgDiv.show();
                $("#pcrud_12_right div.ui-paging-info").css("display", "none")
            } else {
                grid.show();
                emptyMsgDiv.hide();
                $("#pcrud_12_right div.ui-paging-info").css("display", "block")
            }
        }});
    emptyMsgDiv.insertAfter(grid.parent());
    emptyMsgDiv.hide();
    $("#list_12").jqGrid("navGrid", "#pcrud_12", {reload: true, edit: false, add: false, search: false, del: false})
}
;
