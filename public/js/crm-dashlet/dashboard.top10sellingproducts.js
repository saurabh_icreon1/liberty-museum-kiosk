function drawDashletGrid10(dashletId,searchArr){
    var colNames = '';
    for(var i=0;i<searchArr['colName'].length;i++){
        colNames = colNames+",'"+searchArr['colName'][i]+"'";
    }
    var colAllNames = eval("["+colNames.substring(1)+"]");
    
    var colMods = '';
    for(var i=0;i<searchArr['indexName'].length;i++){        
        colMods = colMods+",{name: '"+searchArr['colName'][i]+"',index: '"+searchArr['indexName'][i]+"',sortable:false}";
    }
    var colAllMods = eval("["+colMods.substring(1)+"]");
        
    var grid = jQuery("#list_10");
    var emptyMsgDiv = $('<div class="no-record-msz">'+NO_RECORD_FOUND+'</div>');
    $("#list_10").jqGrid({
        postData:{
            crm_dashlet:'dashlet',
            dashletId:dashletId
        },
        mtype: 'POST',
        url : '/top-selling-products',
        datatype: "json",
        colNames: colAllNames,
        colModel: colAllMods,
        viewrecords: true, 
        sortname: searchArr['sortName'],
        sortorder: "desc",
        rowNum:itemCount_10,
        rowList:[10], 
        pager: '#pcrud_10',
        viewrecords: true, 
        autowidth: true,
        shrinkToFit: true,
        caption:"",
        width:"100%",
        cmTemplate: {
            title: false
        },
        loadComplete: function () {
            var count = grid.getGridParam();
            var ts = grid[0];
            if (ts.p.reccount === 0) {
                grid.hide();
                emptyMsgDiv.show();
                $("#pcrud_10_right div.ui-paging-info").css('display','none');
            } else {
                grid.show();
                emptyMsgDiv.hide();
                $("#pcrud_10_right div.ui-paging-info").css('display','block');
            }
        }
    });
    
    // place div with empty message insde of div
    emptyMsgDiv.insertAfter(grid.parent());
    emptyMsgDiv.hide();
    
    // For paging 
    $("#list_10").jqGrid('navGrid','#pcrud_10',{
        reload: true,
        edit: false, 
        add: false, 
        search: false, 
        del: false
    });
}

$(function() {
    
    $("#added_date_from_10").datepicker({
        changeMonth: true,
        changeYear: true,
        showOtherMonths: true,
        onClose: function( selectedDate ) {
            $( "#added_date_to_10" ).datepicker( "option", "minDate", selectedDate );
        }
    });
    $("#added_date_to_10").datepicker({
        changeMonth: true,
        changeYear: true,
        showOtherMonths: true,
        onClose: function( selectedDate ) {
            $( "#added_date_from_10" ).datepicker( "option", "maxDate", selectedDate );
        }
    });
    
    
        
});

$("#searchbutton_10").click(function(){
    showAjxLoader();
    jQuery("#list_10").jqGrid('setGridParam',{
        postData:{
            searchString:$("#search-form-10").serialize()
        }
    });
    jQuery("#list_10").trigger('reloadGrid',[{
        page:1
    }]);
    hideAjxLoader();
    return false;
});

function showDate(id,divId)
{
    if($("#"+id+" option:selected").text()=="Choose Date Range")
    {
        $("#"+divId).show();
    }
    else
    {
        $("#"+divId).hide();
    }
}