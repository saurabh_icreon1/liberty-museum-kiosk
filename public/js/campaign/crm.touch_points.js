$(function() {
    var grid = jQuery("#list");
    var emptyMsgDivPass = $('<div class="no-record-msz">' + NO_RECORD_FOUND + "</div>");
    $("#list").jqGrid({postData: {}, mtype: "POST", url: "/campaign-touch-points/" + campaignId, datatype: "json", colNames: ["ID", "CAMPAIGN_ID", L_TOUCHPOINTS, L_DATE, L_SENT, L_RESPONDED_RATE, L_OPEN_RATE, L_BOUNCE_RATE, L_CLICKED_RATE, L_ACTION, L_RESULT], colModel: [{name: "ID", index: "touch_id", hidden: true}, {name: "CAMPAIGN_ID", index: "campaign_id", hidden: true, editable: true}, {name: L_TOUCHPOINTS, sortable: false},
            {name: L_DATE, sortable: false}, {name: L_SENT, sortable: false}, {name: L_RESPONDED_RATE, sortable: false}, {name: L_OPEN_RATE, sortable: false, editable: true}, {name: L_BOUNCE_RATE, sortable: false, editable: true}, {name: L_CLICKED_RATE, sortable: false, editable: true}, {name: L_ACTION, sortable: false}, {name: L_RESULT, index: "action", sortable: false, formatter: "actions", formatoptions: {keys: true, delbutton: false, afterSave: function() {
                        jQuery("#list").trigger("reloadGrid")
                    }}}], viewrecords: true, sortname: "touch_point_id", sortorder: "desc",
        rowNum: 10, rowList: [10, 20, 30], pager: "#pcrud", viewrecords:true, autowidth: true, shrinkToFit: true, caption: "", width: "100%", cmTemplate: {title: false}, loadComplete: function() {
            $(".ui-icon-trash").hide();
            $(".ui-icon-pencil").html('<a class="edit-icon" onclick="hideIcon(this)"></a>');
            $(".ui-icon-disk").html('<a class="save-icon"></a>');
            $(".ui-icon-cancel").html('<a class="cancel-icon" onclick="showIcon(this)"></a>');
            $(".ui-icon-disk").removeClass();
            $(".ui-icon-cancel").removeClass();
            $(".ui-icon-pencil").removeClass();
            var count = grid.getGridParam();
            var ts = grid[0];
            if (ts.p.reccount === 0) {
                grid.hide();
                emptyMsgDivPass.show();
                $("#pcrud_right div.ui-paging-info").css("display", "none")
            } else {
                grid.show();
                emptyMsgDivPass.hide();
                $("#pcrud_right div.ui-paging-info").css("display", "block")
            }
        }, editurl: "/edit-campaign-touch-points"});
    emptyMsgDivPass.insertAfter(grid.parent());
    emptyMsgDivPass.hide();
    $("#list").jqGrid("navGrid", "#pcrud", {reload: true, edit: false, add: false, search: false, del: false})
});
function hideIcon(thisref) {
    $(thisref).parents("div").next().children().css("display", "block")
}
function showIcon(thisref) {
    $(thisref).parents("div").prev().children().css("display", "block")
}

function addTouchPoints() {
    showAjxLoader();
    $.ajax({type: "POST", data: {}, url: "/add-touch-point/" + campaignId, success: function(response) {
            hideAjxLoader();
            if (!checkUserAuthenticationAjx(response))
                return false;
            var jsonObj = jQuery.parseJSON(response);
            if (jsonObj.status == "success")
                jQuery("#list").trigger("reloadGrid", [{page: 1}])
            else if(jsonObj.status == "error"){ 
                jQuery('#error_message').show();
                setTimeout(function(){jQuery('#error_message').hide()},2000);
            }
        }})
}
;
window.onfocus=function(){hideAjxLoader();}
