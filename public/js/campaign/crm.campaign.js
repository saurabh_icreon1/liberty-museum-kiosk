$(function(){
    $("#start_date").datepicker({
        changeMonth:true,
        changeYear:true,
        onClose:function(selectedDate){
            $("#end_date").datepicker("option","minDate",selectedDate)
            }
        });
$("#end_date").datepicker({
    changeMonth:true,
    changeYear:true,
    onClose:function(selectedDate){
        $("#start_date").datepicker("option","maxDate",selectedDate)
        }
    });
$("#img_start").click(function(){
    $("#start_date").datepicker("show")
    });
$("#img_end").click(function(){
    $("#end_date").datepicker("show")
    });
$("#start_time").timepicker({
    "timeFormat":"h:i A"
});
$("#end_time").timepicker({
    "timeFormat":"h:i A"
});
$("#addbutton").click(function(){
    $("#group_from option:selected").each(function(){
        $("#group_to").append("<option value='"+$(this).val()+"'>"+$(this).text()+"</option>");
        $(this).remove();
        for(var i=0;i<group_to.options.length;i++)group_to.options[i].selected=true
            })
    });
$("#campaign_type").change(function(){
    $("div[class^='camp_type']").hide();
    $(".camp_type"+this.value).show()
    });
if($(".camp_type"+$("#campaign_type").val()))$(".camp_type"+$("#campaign_type").val()).show();
    $("#campaign_status").change(function(){
        $("div[class^='camp_status']").hide();
        $(".camp_status"+this.value).show()
        });
    if($(".camp_status"+$("#campaign_status").val()))$(".camp_status"+$("#campaign_status").val()).show();
    $("#channel").change(function(){
        $("div[class^='channeldesc']").hide();
        $(".channeldesc"+$("#channel").val()).show()
        });
    if($(".channeldesc"+$("#channel").val()))$(".channeldesc"+$("#channel").val()).show();
    $("#removebutton").click(function(){
        $("#group_to option:selected").each(function(){
            $("#group_from").append("<option value='"+
                $(this).val()+"'>"+$(this).text()+"</option>");
            $(this).remove();
            for(var i=0;i<group_to.options.length;i++)group_to.options[i].selected=true
                })
        });
    $("#program").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:function(request,response){
            $.ajax({
                url:"/crm-get-program",
                method:"POST",
                dataType:"json",
                data:{
                    program:$("#program").val()
                    },
                success:function(jsonResult){
                    var resultset=[];
                    $.each(jsonResult,function(){
                        var id=this.program_id;
                        var value=this.program;
                        resultset.push({
                            id:this.program_id,
                            value:this.program
                            })
                        });
                    response(resultset)
                    }
                })
        },
    open:function(){
        $(".ui-menu").width(350)
        },
    change:function(event,ui){
        if(ui.item){
            $("#program").val(ui.item.value);
            $("#program_id").val(ui.item.id)
            }else{
            $("#program").val("");
            $("#program_id").val("")
            }
        },
    select:function(event,ui){
        $("#program").val(ui.item.value);
        $("#program_id").val(ui.item.id);
        return false
        }
    });
$("#campaign_code").autocomplete({
    afterAdd:true,
    selectFirst:true,
    autoFocus:true,
    source:function(request,response){
        $.ajax({
            url:"/get-campaign-code",
            method:"POST",
            dataType:"json",
            data:{
                campaign_code:$("#campaign_code").val()
                },
            success:function(jsonResult){
                var resultset=[];
                $.each(jsonResult,function(){
                    var id=this.campaign_id;
                    var value=this.campaign_code;
                    resultset.push({
                        id:this.campaign_id,
                        value:this.campaign_code
                        })
                    });
                response(resultset)
                }
            })
    },
open:function(){
    $(".ui-menu").width(350)
    },
change:function(event,ui){
    if(ui.item){
        $("#campaign_code").val(ui.item.value);
        $("#campaign_code_id").val(ui.item.id)
        }else{
        $("#campaign_code").val("");
        $("#campaign_code_id").val("")
        }
    },
select:function(event,ui){
    $("#campaign_code").val(ui.item.value);
    $("#campaign_code_id").val(ui.item.id);
    return false
    }
});
jQuery.validator.addMethod("alphanumericunderscore",function(value,element){
    return this.optional(element)||/^[A-Za-z0-9\n -]+$/i.test(value)
    });
jQuery.validator.addMethod("alphanumericspecialchar",function(value,element){
    return this.optional(element)||/^[A-Za-z0-9 ,.-]+$/i.test(value)
    });
jQuery.validator.addMethod("alphanumericspecialcharext",function(value,element){
    return this.optional(element)||
    /^[A-Za-z0-9 ~@!#$%&*?{}+,.-_""]+$/i.test(value)
    });
jQuery.validator.addMethod("numericchar",function(value,element){
    return this.optional(element)||/^[0-9]+$/i.test(value)
    });
jQuery.validator.addMethod("alpha",function(value,element){
    return this.optional(element)||/^[A_Za-z ]+$/i.test(value)
    });
jQuery.validator.addMethod("numericspecial",function(value,element){
    return this.optional(element)||/^[0-9.,]+$/i.test(value)
    });
$("#add_campaign").validate({
    submitHandler:function(){
        var dataStr=$("#add_campaign").serialize();
        if(checkCustomValidation("add_campaign")){
            showAjxLoader();
            var groups=new Array;
            $("#group_to option").each(function(){
                groups.push($(this).val())
                });
            $("#groups").val(groups);
            $.ajax({
                type:"POST",
                data:dataStr,
                url:"/create-campaign",
                success:function(response){
                    if(!checkUserAuthenticationAjx(response))return false;
                    hideAjxLoader();
                    var jsonObj=jQuery.parseJSON(response);
                    if(jsonObj.status=="success")location.href="/campaigns";else $.each(jsonObj.message,function(i,msg){
                        $("#"+i).after('<label class="error" style="display:block;">'+
                            msg+"</label>")
                        })
                    }
                    })
        }
    },
rules:{
    title:{
        required:true,
        //alphanumericspecialchar:true,
        minlength:2,
        maxlength:50
    },
    campaign_type:{
        required:true
    },
    description:{
        required:true,
        //alphanumericspecialcharext:true,
        maxlength:200,
        minlength:2
    },
    program:{
        required:true
    },
    
    start_date:{
        required:true
    },
    start_time:{
        required:true
    },
    end_date:{
        required:true
    },
    end_time:{
        required:true
    },
    campaign_status:{
        required:true
    },
    revenue_goal:{
        required:true,
        maxlength:10,
        numericspecial:true
    },
    campaign_goal:{
        alphanumericspecialcharext:true,
        minlength:2,
        maxlength:50
    },
    designation:{
        alphanumericspecialchar:true,
        minlength:2,
        maxlength:16
    },
    department:{
        alphanumericspecialchar:true,
        minlength:2,
        maxlength:16
    },
	project:{
        required:true
    },
},
messages:{
    title:{
        required:TITLE_MSG
       // alphanumericspecialchar:TITLE_MSG
    },
    campaign_type:{
        required:CAMPAIGN_TYPE_MSG
    },
    description:{
        required:DESCRIPTION_MSG
        //alphanumericspecialcharext:DESCRIPTION_MSG
    },
    program:{
        required:PROGRAM_MSG
    },
   
    start_date:{
        required:START_DATE
    },
    start_time:{
        required:START_TIME
    },
    end_date:{
        required:END_DATE
    },
    end_time:{
        required:END_TIME
    },
    campaign_status:{
        required:CAMPAIGN_STATUS
    },
    revenue_goal:{
        required:REVENUE_GOAL,
        numericspecial:REVENUE_GOAL
    },
    campaign_goal:{
        alphanumericspecialcharext:CAMPAIGN_GOAL
    },
    designation:{
        alphanumericspecialchar:DESIGNATION_MSG
    },
    department:{
        alphanumericspecialchar:DEPARTMENT_MSG
    },
    project:{
        required:PROJECT_MSG
    },
}
});
$("#add_program").validate({
    submitHandler:function(){
        showAjxLoader()
        },
    rules:{
        name:{
            required:true,
            alphanumericunderscore:true
        },
        description:{
            required:true,
            alphanumericunderscore:true
        }
    },
messages:{
    name:{
        required:P_NAME_MSG,
        alphanumericunderscore:P_NAME_MSG
    },
    description:{
        required:P_DESC_MSG,
        alphanumericunderscore:P_DESC_MSG
    }
}
});
$("#save_search").validate({
    submitHandler:function(){
        showAjxLoader();
        $.ajax({
            type:"POST",
            data:{
                search_name:$.trim($("#save_search_as").val()),
                searchString:$("#search_campaign").serialize(),
                search_id:$("#search_id").val()
                },
            url:"/save-campaign-search",
            success:function(response){
                if(!checkUserAuthenticationAjx(response))return false;
                hideAjxLoader();
                var jsonObj=jQuery.parseJSON(response);
                if(jsonObj.status==
                    "success"){
                    $("#save_search_as").val("");
                    $("#search_msg").removeClass("error");
                    document.getElementById("search_campaign").reset();
                    location.reload()
                    }else if(jsonObj.status=="exists"){
                    $("#search_msg").addClass("error");
                    $("#search_msg").show();
                    $("#search_msg").html(jsonObj.message)
                    }else $("#search_msg").html(ERROR_SAVING_SEARCH)
                    }
                })
    },
rules:{
    save_search_as:{
        required:true
    }
},
messages:{
    save_search_as:{
        required:SEARCH_NAME_MSG
    }
}
});
function generateSavedSearch(){
    showAjxLoader();
    $("#saved_search option").each(function(){
        $(this).remove()
        });
    $.ajax({
        type:"POST",
        data:{},
        url:"/get-saved-campaign-search-select",
        success:function(response){
            if(!checkUserAuthenticationAjx(response))return false;
            hideAjxLoader();
            $("#saved_search").append(response)
            }
        })
}
$(document).on("click","#load_channel_div",function(){
    var i=$("#count_div").text();
    var allDivBlock=$("div[id^='channel_']");
    if(allDivBlock.length==1){
        var parId=$(this).parents("div[id^='channel_']").attr("id");
        var id=parId.substring(parId.lastIndexOf("_")+1,parId.length);
        $(this).parent().append("<a  class='minus' onclick=remove_dynamic_contacts('.channel_"+
            id+"','"+id+"')>")
        }
        $(this).remove();
    $("<div />",{
        "class":"channel_div  campaigns-input channel_"+i+" row address change-form-bg ",
        "id":"channel_"+i,
        "style":"padding:15px !important; width: 98% !important;"
    }).appendTo("#load_channel");
    $(".channel_"+i).load("/channel-campaign?counter="+i+"&container=channel_");
    i++;
    $("#count_div").html(i)
    });
$.jgrid.no_legacy_api=true;
$.jgrid.useJSON=true;
$(".e2").customInput();
var gridCampaign=jQuery("#list");
var emptyMsgDiv=$('<div class="no-record-msz">'+NO_RECORD_FOUND+
    "</div>");
$("#list").jqGrid({
    postData:{
        searchString:$("#search_passenger").serialize()
        },
    mtype:"POST",
    url:"/get-search-campaign",
    datatype:"json",
    sortable:true,
    colNames:["Id","Campaign Code","Title","Program","Project", "Group(s)","Start Date","End Date","Received / Goal","Status","User Added","Action(s)"],
    colModel:[{
        name:"Id",
        index:"id",
        hidden:true,
        key:true
    },{
        name:"Campaign Code",
        index:"campaign_code"
    },{
        name:"Title",
        index:"campaign_title"
    },{
        name:"Program",
        index:"program"
    },{
        name:"Project",
        index:"projects.project_name"
    },{
        name:"Group(s)",
        sortable:false
    },{
        name:"Start Date",
        index:"campaign_start_date"
    },{
        name:"End Date",
        index:"campaign_end_date"
    },{
        name:"Actual/Received",
        index:"CAST(campaign_revenue_received AS SIGNED INTEGER)"
    },{
        name:"Status",
        index:"campaign_status"
    },{
        name:"User Added",
        index:"is_user_added",
        hidden:true,
        key:true
    },{
        name:"Action(s)",
        index:"action",
        sortable:false
    }],
    viewrecords:true,
    sortname:"cam.modified_date",
    sortorder:"DESC",
    rowNum:10,
    rowList:[10,20,30],
    pager:"#pcrud",
    viewrecords:true,
    autowidth:true,
    shrinkToFit:true,
    caption:"",
    multiselect:true,
    width:"100%",
    cmTemplate:{
        title:false
    },
    beforeSelectRow:function(rowid,e){
        if(e.srcElement&&e.srcElement.type=="checkbox")return true;else return false
            },
    loadComplete:function(postData){
        hideAjxLoader();
        var rows=jQuery("#list").jqGrid("getRowData");
        for(var i=0;i<rows.length;i++){
            var row=rows[i];
            if(row["User Added"]==1){
                $("#jqg_list_"+row["Id"]).attr("checked","checked");
                $("#jqg_list_"+row["Id"]).attr("disabled",true)
                }
            }
        var count=gridCampaign.getGridParam();
    var ts=gridCampaign[0];
    if(ts.p.reccount===0){
        gridCampaign.hide();
        emptyMsgDiv.show();
        $("#pcrud_right div.ui-paging-info").css("display","none")
        }else{
        gridCampaign.show();
        emptyMsgDiv.hide();
        $("#pcrud_right div.ui-paging-info").css("display","block")
        }
    }
});
emptyMsgDiv.insertAfter(gridCampaign.parent());
emptyMsgDiv.hide();
$("#searchbutton").click(function(){
    showAjxLoader();
    $("#search_msg").hide();
    $("#success_message").hide();
    jQuery("#list").jqGrid("setGridParam",{
        postData:{
            searchString:$("#search_campaign").serialize()
            }
        });
jQuery("#list").trigger("reloadGrid",[{
    page:1
}]);
window.location.hash="#saved_search";
return false
});
$("#list").jqGrid("navGrid","#pcrud",{
    reload:true,
    edit:false,
    add:false,
    search:false,
    del:false
});
$("#list").jqGrid("navButtonAdd","#pcrud",{
    caption:"",
    title:"Export",
    id:"exportExcel",
    onClickButton:function(){
        exportExcel("list","/get-search-campaign")
        },
    position:"last"
});
$("#saved_search").change(function(){
    showAjxLoader();
    $("#search_msg").hide();
    if($("#saved_search").val()!=""){
        $("#savesearch").val("Update");
        $("#deletebutton").show();
        $('label[for="save_search_as"]').hide();
        $.ajax({
            type:"POST",
            data:{
                search_id:$("#saved_search").val()
                },
            url:"/get-crm-search-campaign-saved-select",
            success:function(response){
                if(!checkUserAuthenticationAjx(response))return false;
                hideAjxLoader();
                var searchParamArray=response;
                var obj=jQuery.parseJSON(searchParamArray);
                document.getElementById("search_campaign").reset();
                $("#name_email").val(obj["name_email"]);
                $("#campaign_code").val(obj["campaign_code"]);
                $("#program").val(obj["program"]);
                $("#start_date").val(obj["start_date"]);
                $("#end_date").val(obj["end_date"]);
                $("#campaign_type").val(obj["campaign_type"]).select2();
                $("div[class^='camp_type']").hide();
                if($(".camp_type"+$("#campaign_type").val()))$(".camp_type"+$("#campaign_type").val()).show();
                $("#campaign_status").val(obj["campaign_status"]).select2();
                $("div[class^='camp_status']").hide();
                if($(".camp_status"+$("#campaign_status").val()))$(".camp_status"+$("#campaign_status").val()).show();
                $("#save_search_as").val(obj["search_title"]);
                $("#search_id").val($("#saved_search").val());
                $("#group_to option").each(function(index,
                    option){
                    $(option).remove()
                    });
                if(obj["group_id"]!=""){
                    $("#group_to").append(obj["group_to"]);
                    $("#group_from").html("");
                    $("#group_from").append(obj["group_from"]);
                    $("#group_to option").each(function(index,option){
                        $(option).attr("selected","selected")
                        })
                    }
                    $("#searchbutton").click()
                }
            })
    }else{
    checkUserAuthenticationAjx(response);
    hideAjxLoader();
    $("#save_search_as").val("");
    $("#search_id").val("");
    $("#savesearch").val("Save");
    $("#deletebutton").hide()
    }
});
if(document.getElementById("group_to")){
    var group_to=
    document.getElementById("group_to");
    for(var i=0;i<group_to.options.length;i++)group_to.options[i].selected=true
        }

	$("#project").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:function(request,response){
            $.ajax({
                url:"/crm-get-project",
                method:"POST",
                dataType:"json",
                data:{
                    project_name:$("#project").val()
                    },
                success:function(jsonResult){
                    var resultset=[];
                    $.each(jsonResult,function(){
                        var id=this.project_id;
                        var value=this.project_name;
                        resultset.push({
                            id:this.project_id,
                            value:this.project_name
                            })
                        });
                    response(resultset)
                    }
                })
        },
    open:function(){
        $(".ui-menu").width(350)
        },
    change:function(event,ui){
        if(ui.item){
            $("#project").val(ui.item.value);
            $("#project_id").val(ui.item.id)
            }else{
            $("#project").val("");
            $("#project_id").val("")
            }
        },
    select:function(event,ui){
        $("#project").val(ui.item.value);
        $("#project_id").val(ui.item.id);
        return false
        }
    });
 });
function loadRemove(i,classname){
    $("#primary_Div").show();
    $("."+classname+""+i).append($("<a style='float: right;' class='minus m-l-5' onclick=remove_dynamic_contacts('."+classname+""+i+"','"+i+"')>",{
        href:"javascript:void(0)",
        id:"remove"
    }))
    }
function remove_dynamic_contacts(classname,i){
    var plusEle=$(classname).find(".plus");
    $(classname).remove();
    var classDivId=classname.substring(1,classname.lastIndexOf("_")+1);
    var lastBlock=$("div[id^='"+classDivId+"']").last();
    if($(lastBlock).find(".plus").length==0){
        var plusId=$(plusEle).attr("id");
        $(lastBlock).find(".addMinus").prepend('<a href="javascript:void(0);" id="'+plusId+'" class="plus m-l-5"></a>')
        }
        var allDivBlock=$("div[id^='"+classDivId+"']");
    if(allDivBlock.length==1)$(lastBlock).find(".minus").remove()
        }
$(function(){
    var url="/upload-campaign-thumbnail";
    $("#thumbnail").fileupload({
        url:url,
        dataType:"json",
        start:function(e){
            showAjxLoader()
            },
        done:function(e,data){
            hideAjxLoader();
            data=data.result;
            if(data.status=="success"){
                $("#filemessage").addClass("success-msg");
                $("#filemessage").removeClass("error");
                $("#thumbnail_image").val(data.filename)
                }else{
                $("#filemessage").removeClass("success-msg");
                $("#filemessage").addClass("error");
                $("#thumbnail_image").val("")
                }
                $("#filemessage").html(data.message)
            }
        }).prop("disabled",
    !$.support.fileInput).parent().addClass($.support.fileInput?undefined:"disabled")
    });
function deleteCampaign(id){
    $(".delete_campaign").colorbox({
        width:"700px",
        height:"180px",
        inline:true
    });
    $("#no_delete").click(function(){
        $.colorbox.close();
        $("#yes_delete").unbind("click");
        return false
        });
    $("#yes_delete").click(function(){
        $.ajax({
            url:"/delete-campaign",
            method:"POST",
            data:{
                campaign_id:id
            },
            success:function(jsonResult){
                if(!checkUserAuthenticationAjx(jsonResult))return false;
                var jsonObj=jQuery.parseJSON(jsonResult);
                if(jsonObj.status=="success")location.reload()
                    }
                })
    })
}
function deleteSearch(){
    $(".delete_campaign_search").colorbox({
        width:"700px",
        height:"180px",
        inline:true
    });
    $("#no_delete_search").click(function(){
        $.colorbox.close();
        $("#yes_delete_search").unbind("click");
        return false
        });
    $("#yes_delete_search").click(function(){
        $.ajax({
            type:"POST",
            data:{
                search_id:$("#search_id").val()
                },
            url:"/delete-crm-search-campaign",
            success:function(response){
                if(!checkUserAuthenticationAjx(response))return false;
                var jsonObj=jQuery.parseJSON(response);
                if(jsonObj.status=="success")location.reload()
                    }
                })
    })
}
function addProgram(){
    $.colorbox({
        width:"700px",
        height:"700px",
        iframe:true,
        scrolling:false,
        href:"/crm-program-popup-list"
    })
    }
    function camp_channel(id){
    $.colorbox({
        width:"600px",
        height:"400px",
        iframe:true,
        href:"/show-channels-codes/"+id
        })
    }
function addUser(){
    $(".add_user").colorbox({
        width:"700px",
        height:"180px",
        inline:true
    });
    $("#no_add").click(function(){
        $.colorbox.close();
        $("#yes_delete").unbind("click");
        return false
        });
    $("#yes_add").click(function(){
        var rows=jQuery("#list").jqGrid("getRowData");
        var ids=[];
        for(var i=0;i<rows.length;i++){
            var row=rows[i];
            if(row["User Added"]==0&&$("#jqg_list_"+row["Id"]).is(":checked"))ids.push(row["Id"])
                }
                $.ajax({
            type:"POST",
            url:"/add-campaign-user",
            data:{
                ids:ids
            },
            success:function(responseData){
                if(!checkUserAuthenticationAjx(responseData))return false;
                var jsonObj=jQuery.parseJSON(responseData);
                if(jsonObj.status=="success")window.location.href="/campaigns";else $.each(jsonObj.message,function(i,msg){
                    $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                    })
                }
                })
    })
}
function checkCustomValidation(id){
    var flagGl=true;
    var flagCh=true;
    $('#add_campaign [name="gl_code[]"]').each(function(index){
        var str=$(this).val();
        var regx=/^[a-zA-z0-9.]+$/i;
        $(this).next().remove("span");
        if(str.match(regx)||str.length==0){
            var flag=false;
            if(str.length<1){
                if($(this).next("span").length==0)$(this).after('<span class="error" for="gl_code[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'+GL_CODE+"</span>");
                flagGl=false
                }else if(str.length>10&&str!=
                ""){
                if($(this).next("span").length==0)$(this).after('<span class="error" for="gl_code[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'+GL_CODE+"</span>");
                flagGl=false
                }else{
                $(this).next().remove("span");
                flagGl=true
                }
            }else{
        if($(this).next("span").length==0)$(this).after('<span class="error" for="gl_code[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'+GL_CODE+"</span>");
        flagGl=false
        }
    });
$('#add_campaign [name="channel_cost[]"]').each(function(index){
    var str=
    $(this).val();
    var regx=/^[0-9.]+$/i;
    $(this).next().remove("span");
    if(str.match(regx)||str.length==0)if(str.length==1){
        if($(this).next("span").length==0)$(this).after('<span class="error" for="channel_cost[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'+CHANNEL_COST+"</span>");
        flagCh=false
        }else if(str.length>10&&str!=""){
        if($(this).next("span").length==0)$(this).after('<span class="error" for="channel_cost[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'+
            CHANNEL_COST+"</span>");
        flagCh=false
        }else{
        $(this).next().remove("span");
        flagCh=true
        }else{
        if($(this).next("span").length==0)$(this).after('<span class="error" for="channel_cost[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'+CHANNEL_COST+"</span>");
        flagCh=false
        }
    });
if(flagGl&&flagCh){
    $(".detail-section").css("display","block");
    return true
    }else 
		return false
    }

	function addProject(){
		$.colorbox({
			width:"700px",
			height:"700px",
			iframe:true,
			scrolling:false,
			href:"/crm-project-popup-list"
		});
    }