$(function(){
    $(document).on("click","input[id^='note']",function(){
        var noteId=this.id.substr(6),val=0;
        if($("#"+this.id).
            is(":checked"))val="1";else val="0";
        showAjxLoader();
        $.ajax({
            type:"POST",
            url:"/modify-note/"+tableName+"/"+tableField+"/"
            +noteId+"/"+val,
            success:function(responseData){
                var jsonObj=jQuery.parseJSON(responseData);
                if(jsonObj.status=="success"){
                }
                hideAjxLoader()
            }
        })
    });
    $("#transaction_status").change(function(){
        var dataStr=$("#product_status").serialize();
        $.ajax({
            type:"POST",
            url:"/update-all-status/"+encrypt_transactionId+"/"+$("#transaction_status option:selected").val(),
            data:
            dataStr,
            success:function(responseData){
                var jsonObj=jQuery.parseJSON(responseData);
                if(jsonObj.status=="success"){}
                window.
                location.reload()
            }
        })
    });
    jQuery.validator.addMethod("alphanumeric",function(value,element){
        return this.optional(element)||
        /^[A-Za-z0-9\n\r ,.'"$#@!%&*()-:;]+$/i.test(value)});$("#transaction_note").validate({submitHandler:function(){
        showAjxLoader();
        var dataStr=$("#transaction_note").serialize(),replydivClass="";
        if($("#notesdiv").find("div.row").last()
            .hasClass("alterbg-none"))replydivClass="alterbg";else replydivClass="alterbg alterbg-none";
        dataStr=dataStr+
        "&replydivClass="+replydivClass;
        $.ajax({
            type:"POST",
            url:"/add-note",
            data:dataStr,
            success:function(responseData){
                if(!
                    checkUserAuthenticationAjx(responseData))return false;
                var jsonObj=jQuery.parseJSON(responseData);
                if(jsonObj.status==
                    "success"){
                    $("#note").val("");
                    $("#notesdiv").append(jsonObj.content)
                }else $.each(jsonObj.message,function(i,msg){
                    $("#"+i
                        ).after('<label class="error" style="display:block;">'+msg+"</label>")
                });
                hideAjxLoader()
            }
        })
    },
    rules:{
        note:{
            required:true,
            alphanumeric:true
        }
    },
    messages:{
        note:{
            required:"Value required",
            alphanumeric:"Invalid entry"
        }
    }
    });
    $("#product_status").validate({
        submitHandler:function(){
            var dataStr=$("#product_status").serialize();
            if(checkCustomValidation("product_status")){
                $("#submit").hide();
                $.ajax({
                    type:"POST",
                    url:"/update-product-qty/"+encrypt_transactionId,
                    data:dataStr,
                    async:false,
                    success:function(responseData){
                        var jsonObj=JSON.parse(responseData);
                        if(jsonObj.status=="success"){
                          window.location.reload();
                        }
                        else $.each(jsonObj.message,function(i,msg){
                            $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                        })
                    }
                })
            }
        }
    });
    jQuery.validator.addMethod("alphanumericcode",
        function(value,element){
            return this.optional(element)||/^[A-Za-z0-9\n\r ,.'"()-:;]+$/i.test(value)});
        $("#update_track_code").validate({
        submitHandler:function(){
            var dataStr=$("#update_track_code").serialize();
            $("#submit").
            hide();
            $.ajax({
                type:"POST",
                url:"/update-tracking-code/"+$("#product_id").val(),
                data:dataStr,
                async:false,
                success:function
                (responseData){
                    var jsonObj=JSON.parse(responseData);
                    if(jsonObj.status=="success")parent.window.location.reload();else $.
                        each(jsonObj.message,function(i,msg){
                            $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                        })
                }
            })
        }
        ,
        rules:{
            tracking_code:{
                required:true,
                alphanumericcode:true,
                maxlength:100
            }
        },
        messages:{
            tracking_code:{
                required:
                "Value required",
                alphanumericcode:"Invalid tracking code"
            }
        }
    })
});
function checkCustomValidation(id){
    var flagGl=true;
    $('#product_status [name="product_qty[]"]').each(function(index){
        var allInputProductQty=$("input[name^='product_shp_qty']"),
        allInputProductStu=$("select[name^='product_status'],hidden[name^='product_status']"),
        allInputProductPreStu=$("input[name^='product_pre_status']"),
        shp_qty=$(allInputProductQty[index]).val(),
        pro_stu=$(allInputProductStu[index]).val(),
        pre_stu=$(allInputProductPreStu[index]).val(),
        str=$(this).val(),regx=/^[0-9]+$/i;
        $(this).next().remove("span");
        if(str.match(regx)){
            if(pre_stu==pro_stu)if(pro_stu==4||pro_stu==5||pro_stu==9)if(str.length<1||str.length==0){
                if($(this).next("span").length==0)$(this).after('<span class="error-small" for="product_qty[]" generated="true" style="display:block !important;"><span class="redarrow"></span>Please enter a <br>valid quantity</span>');
                flagGl=false
            }else if(parseFloat(str)<=0){
                if($(this).next("span").length==0)$(this).after('<span class="error-small" for="product_qty[]" generated="true" style="display:block !important;"><span class="redarrow"></span>Please enter a <br>valid quantity</span>');
                flagGl=false
            }else if(str.length>10&&str!=""){
                if($(this).next("span").length==0)$(this).after('<span class="error-small" for="product_qty[]" generated="true" style="display:block !important;"><span class="redarrow"></span>Please enter a <br>valid quantity</span>');
                flagGl=false
            }else if(shp_qty<str){
                $(this).after('<span class="error-small" for="product_qty[]" generated="true" style="display:block !important;"><span class="redarrow"></span>Please enter a <br>valid quantity</span>');
                flagGl=false
            }
        }else if(parseFloat(str) < 0){
            $(this).after('<span class="error-small" for="product_qty[]" generated="true" style="display:block !important;"><span class="redarrow"></span>Please enter a <br>valid quantity</span>');
            flagGl=false
        }
        else if(isNaN(str)){
            $(this).after('<span class="error-small" for="product_qty[]" generated="true" style="display:block !important;"><span class="redarrow"></span>Please enter a <br>valid quantity</span>');
            flagGl=false
        }
        else $(this).next().remove("span")
    });
    if(flagGl==false){
        $(".detail-section").css("display","block");
        return false
    }else return true
}
function viewFofPhoto(id,transactionid){
    $.colorbox({
        width:"650px",
        height:"800px",
        iframe:
        true,
        href:"/view-fof-image/"+id+"/"+transactionid
    })
}
function viewFofDetail(id){
    $.colorbox({
        width:"600px",
        height:"400px",
        iframe:true,
        href:"/view-fof-details/"+id
    })
}
function viewBioDetail(id){
    $.colorbox({
        width:"800px",
        height:"640px",
        iframe:
        true,
        href:"/view-bio-details/"+id
    })
}
function viewPassCertificate(id){
    $.colorbox({
        width:"1185px",
        height:"910px",
        iframe:
        true,
        href:"/view-passenger-certificate/"+id
    })
}
function viewWohDetails(id,transactionid,status){
    $.colorbox({
        width:"600px"
        ,
        height:"780px",
        iframe:true,
        href:"/view-woh-details/"+id+"/"+transactionid+"/"+status
    })
}
function viewWohLetter(id){
    $.
    colorbox({
        width:"900px",
        height:"1200px",
        iframe:true,
        href:"/view-woh-letter/"+id
    })
}
function viewWohCertificate(id){
    $.
    colorbox({
        width:"1200px",
        height:"900px",
        iframe:true,
        href:"/view-woh-certificate/"+id
    })
}
function viewBioCertificate(id){
    $
    .colorbox({
        width:"1200px",
        height:"900px",
        iframe:true,
        href:"/view-bio-certificate/"+id
    })
}
function viewManifest(id){
    $.
    colorbox({
        width:"1050px",
        height:"800px",
        iframe:true,
        href:"/view-manifest/"+id
    })
}
function wrongManifest(transaction_id,status){
var msgCorrect = "Do you want to mark it as Correct?";
var msgIncorrect = "Do you want to mark it as Incorrect?";
 if(status == 1){
 $('#msg_menifest').html(msgIncorrect);
 }else{
  $('#msg_menifest').html(msgCorrect);
 }
    $(".mark_wrong_manifest").colorbox({
        width:"700px",
        height:"200px",
        inline:true
    });
    $("#no_mark_wrong").click(function(){
        $.colorbox.close();
        $("#yes_mark_wrong").unbind("click");
        return false
    }
    );
    $("#yes_mark_wrong").click(function(){
        $.colorbox.close();
        $("#yes_mark_wrong").unbind("click");
        showAjxLoader();
        $.ajax({
            type:"POST",
            dataType:"html",
            data:{transaction_id:transaction_id,status:status},
            url:"/mark-wrong-manifest",
            success:function(responseData){
                hideAjxLoader();
                if(!checkUserAuthenticationAjx(responseData))return false;
                var jsonObj=
                JSON.parse(responseData);
                if(jsonObj.status=="success")
				location.reload(); 
				else $("#error_message").
                    html(jsonObj.message)
            }
        })
    })
}
function viewShipImage(id,frame_id){
    $.colorbox({
        width:"1185px",
        height:"910px",
        iframe:true,
        href:"/show-ship-image/"+id
    })
}
function downloadZip(id){
    $.ajax({
        type:
        "POST",
        url:"/download-zip/"+id,
        success:function(responseData){
            var jsonObj=jQuery.parseJSON(responseData);
            if(jsonObj.
                status=="success")return true
        }
    })
}
function viewDonDetail(id){
    $.colorbox({
        width:"800px",
        height:"600px",
        iframe:true,
        href:
        "/view-don-details/"+id
    })
}
function updateTracking(id){
    $.colorbox({
        width:"550px",
        height:"300px",
        iframe:true,
        href:
        "/update-tracking-code/"+id
    })
}
function showPassengerManifestImage(imageName, arrival_date, ship_name, passenger_id) {
    showAjxLoader();
    $.colorbox({
        width: "1200px",
        height: "900px",
        iframe: true,
        href: "/show-manifest-image/" + imageName +"/"+arrival_date+"/"+ship_name+"/"+passenger_id,
        onComplete: function() {
        //    hideAjxLoader();
        }
    });
}
function viewImage(productId){
    $.colorbox({
        width:"500px",
        height:"400px",
        iframe:true,
        href:"/show-product-image/"+productId
    })
}