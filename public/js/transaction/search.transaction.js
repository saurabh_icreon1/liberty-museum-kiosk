$(function(){
    $("#added_date_from").datepicker({
        changeMonth:true,
        changeYear:true,
        showOtherMonths:true,
        onClose:function(
            selectedDate){
            $("#added_date_to").datepicker("option","minDate",selectedDate)
        }
    });
    $("#added_date_to").datepicker({
        changeMonth:true,
        changeYear:true,
        showOtherMonths:true,
        onClose:function(selectedDate){
            $("#added_date_from").datepicker(
                "option","maxDate",selectedDate)
        }
    });
    $("#campaign_title").autocomplete({
        minLength:3,
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:function(request,response){
            $("#campaign_title").addClass("auto-suggest-loading");
            $.ajax({
                url:"/get-tra-campaigns",
                method:"POST",
                dataType:"json",
                data:{
                    campaign_name:$(
                        "#campaign_title").val()
                },
                success:function(jsonResult){
                    $("#campaign_title").removeClass("auto-suggest-loading");
                    if(!checkUserAuthenticationAjx(jsonResult))return false;
                    var 
                    resultset=[];
                    $.each(jsonResult,function(){
                        var id=this.campaign_code,value=this.campaign_title;
                        resultset.push({
                            id:this.
                            campaign_code,
                            value:this.campaign_title
                        })
                    });
                    response(resultset)
                }
            })
        },
        open:function(){
            $(".ui-menu").width(350)
        },
        change:
        function(event,ui){
            if(ui.item){
                $("#campaign_title").val(ui.item.value);
                $("#campaign_title_id").val(ui.item.id)
            }else{
                $(
                    "#campaign_title").val("");
                $("#campaign_title_id").val("")
            }
            $(".ui-autocomplete").jScrollPane()
        },
        focus:function(event,ui)

        {
            $("#campaign_title_id").val(ui.item.id);
            return false
        },
        select:function(event,ui){
            $("#campaign_title").val(ui.item.value)
            ;
            $("#campaign_title_id").val(ui.item.id);
            return false
        }
    });
    $("#searchtransactionbutton").click(function(){
        
        var countSelectVal = 0;
        $('form#search_transaction input,form#search_transaction select,form#search_transaction textarea').each(function(index){
            var input = $(this);
            if(input.attr('name') != undefined && input.attr('name') != 'searchtransactionbutton' && input.attr('name') != 'received_date_from' && input.attr('name') != 'received_date_to' && $.trim(input.val()) != undefined && $.trim(input.val()) != "") {
                countSelectVal = parseInt(countSelectVal) + parseInt(1); 
            }
        });
        $('label[for="added_date_from"]').remove();
        
        var diffYear = 0;
        if ($.trim($('#received_date_range').val()) == "1" && ($.trim($('#added_date_from').val()) != "" || $.trim($('#added_date_to').val()) != ""))
        {
            var toDate = new Date();
            var fromDate = new Date();

            var tmp_reg_Date_From = $.trim($('#added_date_from').val());
            var tmp_reg_Date_To = $.trim($('#added_date_to').val());
            if (tmp_reg_Date_From == "")
            {
                toDate = new Date(tmp_reg_Date_To);
            }
            else if (tmp_reg_Date_To == "")
            {
                fromDate = new Date(tmp_reg_Date_From);
            }
            else
            {
                toDate = new Date(tmp_reg_Date_To);
                fromDate = new Date(tmp_reg_Date_From);
            }
            var oneYear = 365 * 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
            var diffYear = Math.abs((toDate.getTime() - fromDate.getTime()) / (oneYear));
        }
       
        if(countSelectVal < 3) {
          $("html, body").animate({scrollTop:$(".accordion-detail").offset().top},2000);
          $('#search-err-msg_need_two').css({display: "none"});
          $('#search-err-msg').css({display:"block"}); 
          return false;  
        }
        else if (countSelectVal == 3 && (diffYear > 2 || $.trim($('#transaction_source_id').val()) != "" || $.trim($('#item_status').val()) != ""))
        {
            $("html, body").animate({
                scrollTop: $(".accordion-detail").offset().top
            }, 2000);
            $('#search-err-msg').css({display:"none"}); 
            $('#search-err-msg_need_two').css({display: "block"});
            return false;
        }
        else if($.trim($('#received_date_range').val()) == "1" && $.trim($('#added_date_from').val()) == "" && $.trim($('#added_date_to').val()) == "") {
           $("html, body").animate({scrollTop:$(".accordion-detail").offset().top},2000); 
           $('#added_date_from').after('<label for="added_date_from" class="error" style="display:block;">Please enter date range.</label>');
           return false;
        }
        else {  
            $('label[for="added_date_from"]').remove();
            $('#search-err-msg').css({display:"none"});
            $('#search-err-msg_need_two').css({display: "none"});
             showAjxLoader();
             $(".search-results").show();
             showResult();

             jQuery(
                 "#list_transaction").jqGrid("setGridParam",{
                 postData:{
                     searchString:$("#search_transaction").serialize()
                 }
             });
             jQuery(
                 "#list_transaction").trigger("reloadGrid",[{
                 page:1
             }]);
             window.location.hash="#saved_search";

             hideAjxLoader();
             return false;
        }  
    }
    );



});

function showResult()
{
    
    var listTransactionGrid=jQuery("#list_transaction"),emptyMsgDiv=$('<div class="no-record-msz">'+NO_RECORD_FOUND+
        "</div>");




    $("#list_transaction").jqGrid({
        mtype:"POST",
        url:"/crm-transactions",
        datatype:"json",
        sortable:true,
        postData:{
            searchString:$("#search_transaction").serialize()
        },
        colNames:[L_TRANSACTION_HASH,L_TRANSACTION_SOURCE,L_NAME,
        L_PAYMENT_MODE,L_CAMPAIGN,L_AMOUNT_SIMPLE,L_TRANSACTION_DATE,L_BATCH_ID,L_STATUS,L_ACTIONS],
        colModel:[{
            name:"Transaction ID",
            index:
            "usr_transaction.transaction_id"
        },{
            name:"Transaction Source",
            index:"tra_source.transaction_source"
        },{
            name:"User name",
            index:"full_name"
        },{
            name:"Payment Mode",
            index:"pay_modes.payment_mode"
        },{
            name:"Campaign",
            index:
            "campaigns.campaign_title"
        },{
            name:"Amount",
            index:"usr_transaction.transaction_amount"
        },{
            name:"Received",
            index:
            "usr_transaction.transaction_date"
        },{
            name:"Batch #",
            index:
            "batch_id"
        },{
            name:"Status",
            index:"tra_status.transaction_status"
        },{
            name:"Action",
            sortable:false,
            cmTemplate:{
                title:false
            },
            classes:"none"
        }],
        viewrecords:true,
        sortname:"modified_date",
        sortorder:"desc",
        rowNum:
        numRecPerPage,
        rowList:pagesArr,
        pager:"#listtransactionrud",
        viewrecords:true,
        autowidth:true,
        shrinkToFit:true,
        caption:"",
        width:"100%",
        cmTemplate:{
            title:false
        },
        loadComplete:function(){
            hideAjxLoader();
            var count=listTransactionGrid.getGridParam
            (),ts=listTransactionGrid[0];
            if(ts.p.reccount===0){
                listTransactionGrid.hide();
                emptyMsgDiv.show();
                $(
                    "#listtransactionrud_right div.ui-paging-info").css("display","none")
            }else{
                listTransactionGrid.show();
                emptyMsgDiv.hide()
                ;
                $("#listtransactionrud_right div.ui-paging-info").css("display","block")
            }
        }
    });
    emptyMsgDiv.insertAfter(
        listTransactionGrid.parent());
    emptyMsgDiv.hide();
    $("#list_transaction").jqGrid("navGrid","#listtransactionrud",{
        reload:
        true,
        edit:false,
        add:false,
        search:false,
        del:false
    });
    $("#list_transaction").jqGrid("navButtonAdd","#listtransactionrud",{
        caption:"",
        title:"Export",
        id:"exportExcel",
        onClickButton:function(){
            exportExcel("list_transaction","/crm-transactions")
        }
        ,
        position:"last"
    })   
    
}

function showDate(id,divId){
    if($("#"+id+" option:selected").text()=="Choose Date Range") { 
        $("#"+divId).show(); 
    }
    else { 
        $("#"+divId).hide(); 
        if(id == 'received_date_range') {
            $('#added_date_from').val('');
            $('#added_date_to').val('');
        }
    }
}
function getSavedTransactionSearchResult(){
    $("#search_msg").css("display","none");
    $("#search_msg").html("");
    if($("#saved_search").val()!=""){
        $("#savebutton").val("Update");
        $("#deletebutton").show()
    }else 
    if($("#saved_search").val()==""){
        $("#savebutton").val("Save");
        $("#deletebutton").hide();
        $("#search_name").val("");
        return false
    }
    showAjxLoader();
    $.ajax({
        type:"POST",
        data:{
            search_id:$("#saved_search").val()
        },
        url:"/get-transaction-search-saved-param",
        success:function(response){
            hideAjxLoader();
            if(!checkUserAuthenticationAjx(response
                ))return false;
            var searchParamArray=response,obj=jQuery.parseJSON(searchParamArray);
            document.getElementById("search_transaction").reset();
            
            $("#first_name").val(obj.first_name);
            $("#last_name").val(obj.last_name);
            $("#company_name").val(obj.company_name);
            $("#email").val(obj.email);
            $("#product_category").val(obj.product_category).select2();
            $("#type").val(obj.type).select2();
            $("#batch_id").val(obj.batch_id);
            $("#omx_id").val(obj.omx_id);
            $("#transaction_invoice_number").val(obj.transaction_invoice_number);
            
            $("#user_name_email").val(obj.user_name_email);
            $("#transaction_id").val(obj.transaction_id);
            $("#campaign_title").val(obj.campaign_title);
            $("#campaign_title_id").val(obj.campaign_title_id);
            $("#transaction_source_id").val(obj.transaction_source_id).select2();
            $("#received_date_range").val(obj.received_date_range).select2();
            $("#added_date_from").val(obj.received_date_from);
            $("#added_date_to").val(obj.received_date_to);
            $("#transaction_status_id").val(obj.transaction_status_id).select2();
            $("#product_type_id").val(obj.product_type_id).select2();
            $("#payment_mode_id").val(obj.payment_mode_id).select2();
            $("#transaction_amount_from").val(obj.transaction_amount_from);
            $("#transaction_amount_to").val(obj.transaction_amount_to);
            $("#product_category").val(obj.product_category);
            if(obj.pick_up==1){$("#pick_up").attr("checked","checked").customInput();}
            else{$("#pick_up").attr("checked",false).customInput();}
            $("#shipping_id").val(obj.shipping_id).select2();
            $("#search_name").val(obj.search_title);
            var itemStatusObj = [];
            if(obj.item_status){
                var itemArr=obj.item_status.split(",");
                for(k=0;k<itemArr.length;k++){
                   itemStatusObj[k]=itemArr[k];
                }
                $("#item_status").val(itemStatusObj).select2();
            }else{
                $("#item_status").val(itemStatusObj).select2();
            }
            $("#product_ids").val(obj.product_ids).select2();
            //$("#product_ids").val(obj.product_ids).select2();
            /*if(obj.item_status){
                var itemArr=obj.item_status.split(","),allOptions=$("#item_status option");
                for(i=0;i<
                    allOptions.length;i++){
                    var optionValue=allOptions[i].value;
                    for(j=0;j<itemArr.length;j++)if(itemArr[j]==optionValue)$(
                        allOptions[i]).attr("selected","selected")
                }
            }
            if(obj.product_ids){
                var productArr=obj.product_ids,allProductOptions=$("#product_ids option");
                for(i=0;i<allProductOptions.length;i++){
                    var optionValue=allProductOptions[i].value;
                    for(j=0;j<productArr.length;j++){
                        if(productArr[j]==optionValue)
                        { 
                            $(allProductOptions[i]).attr("selected","selected");
                        }
                    }
                }
            }*/
            //$("#item_status").select2();
            //$("#product_ids").select2();
            $("#search_id").val($("#saved_search").val());
            showDate("received_date_range","date_range_div");
            $("#searchtransactionbutton").click()
        }
    })
}
$("#savebutton").click(
    function(){
        if($.trim($("#search_name").val())!=""){
            $("#search_msg").html("");
            $("#search_msg").css("display","none");
            showAjxLoader();
            $.ajax({
                type:"POST",
                data:{
                    search_name:$.trim($("#search_name").val()),
                    searchString:$(
                        "#search_transaction").serialize(),
                    search_id:$("#search_id").val()
                },
                url:"/add-crm-search-transaction",
                success:function(
                    response){
                    hideAjxLoader();
                    if(!checkUserAuthenticationAjx(response))return false;
                    var jsonObj=jQuery.parseJSON(response);
                    if(jsonObj.status=="success"){
                        $("#search_name").val("");
                        document.getElementById("search_transaction").reset();
                        if($(
                            "#search_id").val()!="")location.reload();else location.reload()
                    }else{
                        $("#search_msg").html(ERROR_SAVING_SEARCH);
                        $(
                            "#search_msg").addClass("error");
                        $("#search_msg").removeClass("success-msg clear");
                        $("#search_msg").css("display",
                            "block")
                    }
                }
            })
        }else{
            $("#search_msg").html(SEARCH_NAME_MSG);
            $("#search_msg").addClass("error");
            $("#search_msg").removeClass
            ("success-msg clear");
            $("#search_msg").css("display","block");
            return false
        }
    });
function deleteSaveSearch(){
    $(
        ".delete_saved_search").colorbox({
        width:"700px",
        height:"200px",
        inline:true
    });
    $("#no_delete").click(function(){
        $.colorbox
        .close();
        $("#yes_delete").unbind("click");
        return false
    });
    $("#yes_delete").click(function(){
        $.colorbox.close();
        $(
            "#yes_delete").unbind("click");
        showAjxLoader();
        $.ajax({
            type:"POST",
            data:{
                search_id:$("#search_id").val()
            },
            url:
            "/delete-crm-search-transaction",
            success:function(response){
                hideAjxLoader();
                if(!checkUserAuthenticationAjx(response))
                    return false;
                var jsonObj=jQuery.parseJSON(response);
                if(jsonObj.status=="success")location.reload()
            }
        })
    })
}
function 
deleteTransaction(transaction_id){
    $(".delete_transaction").colorbox({
        width:"700px",
        height:"200px",
        inline:true
    });
    $(
        "#no_delete_transaction").click(function(){
        $.colorbox.close();
        $("#yes_delete_transaction").unbind("click");
        return false
    }
    );
    $("#yes_delete_transaction").click(function(){
        $.colorbox.close();
        $("#yes_delete_transaction").unbind("click");
        showAjxLoader();
        $.ajax({
            type:"POST",
            dataType:"html",
            data:{
                transaction_id:transaction_id
            },
            url:"/delete-crm-transaction",
            success:function(responseData){
                hideAjxLoader();
                if(!checkUserAuthenticationAjx(responseData))return false;
                var jsonObj=
                JSON.parse(responseData);
                if(jsonObj.status=="success")window.location.href="/crm-transactions";else $("#error_message").
                    html(jsonObj.message)
            }
        })
    })
}