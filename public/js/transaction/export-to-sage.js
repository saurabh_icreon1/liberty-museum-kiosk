$(function(){$("#transaction_date").datepicker({changeMonth:true,changeYear:true,showOtherMonths:true,onClose:function(
selectedDate){}});$("#added_date_to").datepicker({changeMonth:true,changeYear:true,showOtherMonths:true,onClose:function
(selectedDate){$("#added_date_from").datepicker("option","maxDate",selectedDate)}});$("#transaction_time").timepicker({
timeFormat:"H:i A"});var listTransactionGrid=jQuery("#list_transaction_sage"),emptyMsgDiv=$(
'<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");$("#list_transaction_sage").jqGrid({mtype:"POST",url:
"/crm-sages",datatype:"json",sortable:true,postData:{searchString:$("#search_transaction").serialize()},colNames:[(
'<input type="checkbox" class="check_all"  onclick="checkBoxstop(event)" />'+BATCH_NO),EXPORTED_DATE,DOWNLOAD],colModel:
[{name:"Batch No",index:"sage_batch_no"},{name:"Exported Date",index:"exported_date"},{name:"Download",index:""}],
viewrecords:true,sortname:"",sortorder:"",rowNum:numRecPerPage,rowList:pagesArr,pager:"#listtransactionrud_sage",
viewrecords:true,autowidth:true,shrinkToFit:true,caption:"",width:"100%",cmTemplate:{title:false},loadComplete:function(
){hideAjxLoader();var count=listTransactionGrid.getGridParam(),ts=listTransactionGrid[0];if(ts.p.reccount===0){
listTransactionGrid.hide();emptyMsgDiv.show();$("#downloadLink").css("display","none");$(
"#listtransactionrud_sage_right div.ui-paging-info").css("display","none")}else{listTransactionGrid.show();emptyMsgDiv.
hide();$("#downloadLink").css("display","block");$("#listtransactionrud_sage_right div.ui-paging-info").css("display",
"block")}}});emptyMsgDiv.insertAfter(listTransactionGrid.parent());emptyMsgDiv.hide();$("#list_transaction_sage").jqGrid
("navGrid","#listtransactionrud_sage",{reload:true,edit:false,add:false,search:false,del:false});var 
listTransactionGrid1=jQuery("#create_transaction_sage");emptyMsgDiv=$('<div class="no-record-msz">'+NO_RECORD_FOUND+
"</div>");$("#create_transaction_sage").jqGrid({mtype:"POST",url:"/create-sage-batch",datatype:"json",sortable:true,
postData:{searchString:$("#search_transaction").serialize()},colNames:[TRANSACTION_NO_SAGE,TRANSACTION_GL_ACCOUNT,DEPT,
PROGRAM,DESIGNATION,RESTRICTION,ALLOCATION,AMOUNT],colModel:[{name:TRANSACTION_NO_SAGE,index:"transaction_id"},{name:
TRANSACTION_GL_ACCOUNT,sortable:false,index:""},{name:DEPT,sortable:false,index:""},{name:PROGRAM,sortable:false,index:
""},{name:DESIGNATION,sortable:false,index:""},{name:RESTRICTION,sortable:false,index:"restricted"},{name:ALLOCATION,
sortable:false,index:""},{name:AMOUNT,index:"transaction_amount"}],viewrecords:true,sortname:"",sortorder:"",rowNum:
numRecPerPage,rowList:pagesArr,pager:"#create_listtransactionrud_sage",viewrecords:true,autowidth:true,shrinkToFit:true,
caption:"",width:"100%",cmTemplate:{title:false},loadComplete:function(){hideAjxLoader();var count=listTransactionGrid1.
getGridParam(),ts=listTransactionGrid1[0];if(ts.p.reccount===0){listTransactionGrid1.hide();emptyMsgDiv.show();$(
"#create_listtransactionrud_sage_right div.ui-paging-info").css("display","none")}else{listTransactionGrid1.show();
emptyMsgDiv.hide();$("#create_listtransactionrud_sage_right div.ui-paging-info").css("display","block")}}});emptyMsgDiv.
insertAfter(listTransactionGrid1.parent());emptyMsgDiv.hide();$("#create_transaction_sage").jqGrid("navGrid",
"#create_listtransactionrud_sage",{reload:true,edit:false,add:false,search:false,del:false});$(
"#searchtransactionbutton").click(function(){showAjxLoader();jQuery("#create_transaction_sage").jqGrid("setGridParam",{
postData:{searchString:$("#create_batch").serialize()}});jQuery("#create_transaction_sage").trigger("reloadGrid",[{page:
1}]);return false});$("#saveanddownload").click(function(){$("#create_batch").validate({submitHandler:function(){
showAjxLoader();$.ajax({type:"POST",data:$("#create_batch").serialize()+"&saveanddownnload=success",url:
"/save-download-batch",success:function(response){hideAjxLoader();var jsonObj=jQuery.parseJSON(response);if(jsonObj.
status=="success"){var batch_no=jsonObj.batch_no;location.href="/save-download-batch/"+batch_no;setTimeout(function(){
location.href="/crm-sages"},1000)}else $.each(jsonObj.message,function(i,msg){$("#"+i).after(
'<label class="error" style="display:block;">'+msg+"</label>")})}})},rules:{},messages:{}})});$(".check_all").click(
function(){var status=$(this).is(":checked");if(status)$(".batch_checkbox").prop("checked",true);else $(
".batch_checkbox").prop("checked",false)});$(".e2").customInput();$("#export_bulk_download").validate({rules:{
"batch_nos[]":{required:true}},messages:{"batch_nos[]":{required:SELECT_ANY_BATCH}}})});function checkBoxstop(e){e=e||
event;e.stopPropagation?e.stopPropagation():e.cancelBubble=true}