$(document).ready(function(){
    //$('#step1').hide();
	$('select').select2();
    jQuery.validator.addMethod("letterOnlyHyphen", function(value, element) {
        return this.optional(element) || /^[a-z .-]+$/i.test(value);
    },"Please enter valid details.");

	jQuery.validator.addMethod("state_validation", function(value, element)  {
      if($('#shipping_country').val() == "228") { 
		  if($.trim($('#shipping_state_id').val()) == "") { 
			  return false; 
		  } else { 
			  return true; 
		  } 
       } else if($('#shipping_country').val() != "228") {
		   return true; 
	   } 
    }, 'Please select state');

  var oldCity="";
   
   

		if($("input[type=radio][name=shipping_existing_contact]").length>0){}else{
			//addNewShippingAddress();
		}            
                    
     
    $("#yes").click(function(){
        $("#company_block").show()
    });
    $(
        "#no").click(function(){
        $("#company_block").hide()
    });
    $(document).on("change",".matchinggift",function(){
        if($(this).val()==1)$("#company_block").show();else $("#company_block").hide()
    });
    $("input[name='in_honoree']").click(function(){
        if($(this).val()==3)$("#honoree_name_block").hide();else $("#honoree_name_block").show()
    });
    $(document).on("change",
        "#is_notify",function(){
            if($(this).is(":checked"))$("#notify_block").show();else $("#notify_block").hide()
        });
    $("#shipping_country").change(function(){
		$('#shipping_state_id').next('label').remove();
        $(".error:contains('Your shipping address is invalid, Please enter valid address.')").hide();
        $("#ship_method_error").val(0);
    });

    $("#save_new_shipping_address").click(function(){
        var validator=$("#addNewShippingFrm").validate({
            ignore:".ignore",
            submitHandler:function(){
                //authenticateUserBeforeAjax();
                $("#save_new_shipping_address").hide();
                $("#processingAddNewAddressDiv").show();
                //$('#step1').hide();
                $("#formErrorMsg").hide();
                if($(
                    "#addNewShippingFrm [name='addressinfo_location']").val()==3)var addressinfo_location=3;
                if($(
                    "#addNewShippingFrm [name='addressinfo_location']").val()==2)var addressinfo_location=2;
                $.ajax({
                    type:"POST",
                    url:
                    "/pos-save-user-shipping-address",
                    data:$("#addNewShippingFrm").serialize(),
                    success:function(responseData){
                        var jsonObj=jQuery
                        .parseJSON(responseData);
                        //$('#step1').hide();
                        $('#processingDiv').show();
                        if(jsonObj.status=="success"){
                            hideAjxLoader();
							$('.scroll-pane').jScrollPane();
							setTimeout("$('.scroll-pane').jScrollPane();","500");
							displayshipmethod();
                        }
                    }
                })
            },
            rules:{
                shipping_title:{
                    required:false
                },
                shipping_first_name:{
                    required:true,
                    minlength:3,
                    maxlength:50,
                    //letterOnly:true
                },
                shipping_last_name:{
                    required:true,
                    minlength:3,
                    maxlength:50,
                    //letterOnly:true
                },
                shipping_address_1:{
                    required:true,
                    maxlength:40 //,
                //  alphanumericspecialchar:true
                },
                shipping_address_2:{
                    required:false,
                    maxlength:40 //,
                // alphanumericspecialchar:true
                },
                shipping_country:{
                    required:true
                },
                shipping_city:{
                    required:true,
                    maxlength:25,
                    minlength:2,
                    //letterOnlyHyphen:true
                },
                shipping_state_id:{
                    /*required:function(){
                        if(($("#is_apo_po_new").val()==1||$("#is_apo_po_new").val()==2)&&$("#addNewShippingFrm [name='shipping_country']").val()==228)return true;else return false;
                    }*/
					state_validation : true,
                },
                shipping_state:{
                    minlength:2,
                    maxlength:25,
                    //alpha:true
                },
                apo_po_state:{
                    required:function(){
                        if($("#is_apo_po").val()==3)return true;else return false
                    }
                },
                shipping_zip_code:{
                    required:true,
                    zipcodevalidation:true,
                    maxlength:12,
                    minlength:4
                },
                shipping_country_code:{
                    required:true,
                    phoneno:true,
                    minlength:1,
                    maxlength:5
                },
                shipping_area_code:{
                    required:true,
                    number:true,
                    minlength:3,
                    maxlength:5
                },
                shipping_phone:{
                    required
                    :true,
                    number:true,
                    minlength:7,
                    maxlength:10
                },email_id:{
                    required:true,
                    email:true,
                    maxlength: 100 
                }
            },
            messages:{
                shipping_title:{
                    required:"Enter title"
                },
                shipping_first_name:{
                    required:"Enter First Name"
                },
                shipping_last_name:{
                    required:"Enter Last Name"
                },
                shipping_address_1:{
                    required:"Enter Address 1"
                },
                shipping_address_2:{
                    required:"Enter Address 2"
                },                
                shipping_country:{
                    required:'Select Country'
                },
                shipping_city:{
                    required:'Enter City'
                },
                shipping_state_id:{
                    required:"Select State"
                },
                shipping_state:{
                required:"Enter State"
                },
                apo_po_state:{
                    required:
                    "Select Shipping State"
                },
                shipping_zip_code:{
                    required:'Enter Postal Code',
                    maxlength: 'Maximum length is 12',
                    minlength: 'Minimum length is 5'
                },
                shipping_country_code:{
                    required:
                    'Enter Country Code',
                    phoneno:'Enter numbers only',
                    minlength:PHONE_DIGITS_COUNTRY_CODE_MIN,
                    maxlength:
                    'Maximum length is 5'
                },
                shipping_area_code:{
                    required:'Enter Area Code',
                    number:'Enter numbers only',
                    minlength:
                    'Minimum length is 3',
                    maxlength:'Maximum length is 5'
                },
                shipping_phone:{
                    required:'Enter Phone Number',
                    number:'Enter numbers only',
                    minlength:'Minimum length is 7',
                    maxlength:
                    'Maximum length is 10'
                },email_id:{
                   email:"Enter valid Email",
				   required: 'Enter Email Address'
                }
            }
        });
        
    });

	$('#shipping_city, #shipping_zip_code').on('input',function(e){
	  $("#step1div").show();
	  $("#ship_method_radio").hide();
	  $("#save_new_shipping_address").show(); 
	  $("#save_new_shipping_address2").hide();
    });
	$('form').change(function() {
		$("#step1div").show();
		$("#ship_method_radio").hide();
		$("#save_new_shipping_address").show(); 
		$("#save_new_shipping_address2").hide();
	});
    
   /*$("#apo_po_shipping_country").change(function(){ 
       if($("#apo_po_shipping_country").val()!='' && $("#shipping_zip_code").val()!='')
                displayshipmethod();
   });
      $("#shipping_state_id").change(function(){ 
       if($("#apo_po_shipping_country").val()!='' && $("#shipping_zip_code").val()!='')
                displayshipmethod();
   });
    
   $('#shipping_zip_code').blur(function(){
	   if($("#shipping_zip_code").length!=0 && $("#shipping_zip_code").val()!='') {
			$('#shipping_zip_code').next('label').remove();
			$('#shipping_method_0').click();
		}
   }); 
  
   if($("#apo_po_shipping_country").val()!='' && $("#shipping_zip_code").val()!='')
                displayshipmethod();*/
});
var totalNotify=typeof totalNotify!=
"undefined"?totalNotify:1;
function deleteMoreDonationNotify(id){
    $("#notify_donation_"+id).remove();
    var lastBlock=$(
        "div[id^='notify_donation_']").last();
    if($(lastBlock).find(".plus").length==0)$(lastBlock).append(
        '<a class="plus plus m-l-5" href="javascript:void(0);" onclick="addMoreDonationNotify(this)" id="addMoreDonationNotify"></a>'
        );
    var allDivBlock=$("div[id^='notify_donation_']");
    if(allDivBlock.length==1)$(lastBlock).find(".minus").remove()
}
function showHonoreeName(currRef){
    if($(currRef).val()==3)$("#honoree_name_block").hide();else $("#honoree_name_block").
        show()
}
function showbilloption(currRef){
    if($(currRef).val()==1){
        $("#billing_existing_contact").val("");
        $(
            ".new-address-billing").hide();
        $(".select-bill-address").show()
    }else{
        $("#billing_existing_contact").val("");
        $(
            "#billing_title,#billing_first_name,#billing_last_name,#billing_address_1,#billing_address_2,#billing_city,#billing_country,#billing_state,#billing_zip_code,#billing_location_type"
            ).val("");
        $(".new-address-billing").show();
        $(".select-bill-address").hide()
    }
}
function showshipoption(currRef){
    if($(
        currRef).val()==1){
        $("#shipping_existing_contact").val("");
        $(".new-address").hide();
        $(".select-address").show()
    }else{
        $(
            "#shipping_existing_contact").val("");
        $(
            "#shipping_title,#shipping_first_name,#shipping_last_name,#shipping_address_1,#shipping_address_2,#shipping_country,#shipping_city,#shipping_state,#shipping_zip_code,#shipping_location_type"
            ).val("");
        $(".new-address").show();
        $(".select-address").hide()
    }
}
function showcreditcardoption(currRef){
    if($(currRef).val
        ()==1){
        $("#credit_card_exist").val("");
        $(".new-credit-card").hide();
        $(".select-existing-creditcard").show()
    }else{
        $(
            ".new-credit-card").show();
        $(".select-existing-creditcard").hide();
        $("#credit_card_exist").val("")
    }
}
function changeApoPo
(shippingAddressType){
    $("#addNewShippingFrm [name='is_apo_po']").val(shippingAddressType);
    $("#is_apo_po_new").val(
        shippingAddressType);
    if(shippingAddressType==1){
        $("#country_block").show();
        $("#country_block_old").show();
        $(
            "#apo_po_country_block").hide();
        $("#apo_po_country_block_old").hide();
        if($("#shipping_country").val()==228){
            $(
                "#shipping_text_state_block").hide();
            $("#shipping_select_state_block").show();
            $("#shipping_select_state_block_old").show
            ()
        }else{
            $("#shipping_text_state_block").show();
            $("#shipping_select_state_block").show();
            $(
                "#shipping_select_state_block_old").hide()
                
            $("#usa_state_shipping").hide();   
            $("#other_state_shipping").show();   
                
        }
        $("#shipping_apo_po_state_block").hide();
        $("#shipping_apo_po_state_block_old"
            ).hide();
       // if($("#shipping_address_information_id").val()=="")$("#checkout_form [name='shipping_city']").val("");else $(
         //   "#addNewShippingFrm [name='shipping_city']").val(oldCity);
        $("#addNewShippingFrm [name='shipping_city']").removeAttr(
            "readonly");
        if($("#shipping_address_information_id").val()=="")$("#addNewShippingFrm [name='shipping_city']").val("");
        $(
            "#addNewShippingFrm [name='shipping_city']").removeAttr("readonly")
    }else{
        $("#country_block").hide();
        $(
            "#country_block_old").hide();
        $("#apo_po_country_block").show();
        $("#apo_po_country_block_old").show();
        if($(
            "#shipping_address_information_id").val()=="")$("#addNewShippingFrm [name='shipping_city']").val("");
        if($(
            "#shipping_address_information_id").val()=="")$("#addNewShippingFrm [name='shipping_city']").val("");
        $(
            "#shipping_text_state_block").hide();
        if(shippingAddressType==3){
            $("#addNewShippingFrm [name='shipping_city']").val("APO");
            $(
                "#addNewShippingFrm [name='shipping_city']").attr("readonly","readonly");
            $("#addNewShippingFrm [name='shipping_city']").val(
                "APO");
            $("#addNewShippingFrm [name='shipping_city']").attr("readonly","readonly");
            $("#shipping_select_state_block").hide
            ();
            $("#shipping_select_state_block_old").hide();
            $("#shipping_apo_po_state_block").show();
            $(
                "#shipping_apo_po_state_block_old").show()
        }else{
            $("#addNewShippingFrm [name='shipping_city']").removeAttr("readonly");
            $(
                "#addNewShippingFrm [name='shipping_city']").removeAttr("readonly");
           // $("#addNewShippingFrm [name='shipping_city']").val(oldCity);
            $("#shipping_select_state_block").show();
            $("#shipping_select_state_block_old").show();
            $(
                "#shipping_apo_po_state_block").hide();
            $("#shipping_apo_po_state_block_old").hide();
            $("#other_state_shipping").hide();
            $(
                "#usa_state_shipping").show()
        }
    }
    $(".error:contains('Your shipping address is invalid, Please enter valid address.')").
    hide();
    $("#ship_method_error").val(0);
    //displayshipmethod();
    //updatecarttotal()
}
function displayshipmethod(){
	showAjxLoader();
    $.ajax({
        url:
        "/pos-shipping-method-front",
        method:"POST",
        dataType:"html",
        async:false,
		beforeSend:
        function(){
            $("#ship_method_radio").hide();
            //$('#step1').hide();
            //$('#processingDiv').show();                      
        },
        data:$("#addNewShippingFrm").
        serialize(),
        success:function(responseData){

            $("#ship_method_radio").show();
            $(".ship_method_class").html(responseData);
			$("#processingDivSave").hide();
			$('#processingAddNewAddressDiv2').show();
            
        },
        complete:function(jqXHR,
            textStatus){
			hideAjxLoader();
			$('#shipping_method_0').prop('checked',true);
			$("#processingDivSave").hide();

			$('#save_new_shipping_address').hide();
			$('#save_new_shipping_address2').show();

			$('#save_new_shipping_address').hide();
			$('#save_new_shipping_address2').show();
			$('#processingAddNewAddressDiv').hide();
			$('#processingAddNewAddressDiv2').hide();
			updatecarttotal("");
			//$('#shipping_method_0').click();
		}
    })
}
function editShippingAddress(address_information_id,type,act){
    //addNewShippingAddress();
    // $("#shipping_existing_contact_"+address_information_id).click();
    //document.getElementById("shipping_existing_contact_"+address_information_id).checked=true;
    
    getAddressInfoFront(address_information_id,type,act)
}
function editBillingAddress(address_information_id,type,act){
    addNewBillingAddress();
    getAddressInfoFront(address_information_id,type,act)
}

function applycouponcode(){ 
	//$('#step1').hide();
    //$('#processingDiv').show();    
    //$('#step2').hide();
    //$('#processingDiv2').show();  
    //$("#applycouponcodeDiv").hide();
      //$("#applycouponcodeProcessing").show();  	  
        $("#ship_to_add_option_error_coupon").hide();  
        
        if(totalproduct==isInventory||$("#pick_up").is(":checked")==true){
        
        }
	    else{
		        if($("input[type=radio][name=shipping_existing_contact]:checked").length>0){}else{
		            $("#ship_to_add_option_error_coupon").show();
		            $("#formErrorMsg").hide();
		            //$("#ship_to_add_option_error_coupon").html("<label class='error'>Please select or add a shipping address.</label>");
		            //$("#couponcode").val('');
		            return false
		        }
	        }    

    var couponcode=$("#couponcode").val();
    //var couponcode='';
	if(couponcode==''){
		//$('#couponcode').after('Please enter poromotion code');
		$(".applycouponcodeerror").html('Please enter promotion code').show().addClass('error');
		$("#applycouponcodeDiv").show();
		$("#applycouponcodeProcessing").hide();
		//$('#processingDiv').hide(); 
		//$('#processingDiv2').hide(); 
		return false;
	}else{ 
		$(".applycouponcodeerror").removeClass("error");
		$(".applycouponcodeerror").html('');
		$("#applycouponcodeDiv").hide();
		$("#applycouponcodeProcessing").show();
		$('#processingDiv').show(); 
		$('#processingDiv2').show();
		$(".applycouponcodeerror").hide();
		var isPickUp = 0;		
		//$("#processingDivSave").hide();
		var couponCode = $("#couponcode").val();
		//var couponCode = '';
		$.ajax({
			type:"POST",
			url:"/pos-get-total-cart",
			data:$("#addNewShippingFrm").serialize()+"&shipcode="+$("#addNewShippingFrm [name='shipping_zip_code']").val()
			+"&userid="+userId+"&ship_m=1&source_type=frontend&shipping_company=&shipping_address="+$(
				"#addNewShippingFrm [name='shipping_address_1']").val()+"&status="+status+"&couponcode="+couponCode+"&pick_up="+isPickUp,
			beforeSend: function(){},
			success:function(responseData){
				var jsonObj=jQuery.parseJSON(responseData);
				if(jsonObj.status!=undefined){
					if(jsonObj.status == 'error'){
						$('#is_promotion_applied').val('');
					}else{
						$('#is_promotion_applied').val(1);
					}
					
					$(".applycouponcodeerror").removeClass("error");
					if(jsonObj.message!='' && typeof(jsonObj.message)!='undefined')
						$(".applycouponcodeerror").html(jsonObj.message).show().addClass(jsonObj.status);
					if(jsonObj.status == 'shipping_error'){	 
						updatecarttotal("error");
						//$('#step1').show();
						$('#processingDiv').hide();  	
						$('#processingDiv2').hide();
						$("#applycouponcodeDiv").show();
						$("#applycouponcodeProcessing").hide();  											
						return false;
					}
						
				}
				$(".itemtotal").html("$"+jsonObj.itemtotal);
				$(".shipping").html("$"+jsonObj.shipping);
				$(".totalaftershipping").html("$"+jsonObj.totalaftershipping);
				$(".estimatedtax").html("$"+jsonObj.estimatedtax);
				$(".finaltotal").html("ORDER TOTAL <span>$"+jsonObj.finaltotal+"</span>");
				$(".coupondiscount").html("$"+jsonObj.coupondiscount);
				if(jsonObj.free_shipping!=undefined&&jsonObj.free_shipping==true){}else{}
				$("#free_shipping").val(jsonObj.free_shipping);
				//hideAjxLoader();
				//$('#step1').show();
				$('#processingDiv').hide();  
				//$('#step2').show();
				$('#processingDiv2').hide();
				$("#applycouponcodeDiv").show();
				$("#applycouponcodeProcessing").hide();  	
				    		    	        
				$("#processingDivSave").hide();
                $("#save_new_shipping_address2").show();            		    	        
			}
		});
	}
    
}
function updatecarttotal(status){ 
	$("#processingDivSave").show();
	$("#save_new_shipping_address").hide();   
		var isPickUp = 0;
		
		var couponCode = $("#couponcode").val();
		//var couponCode = '';
    $.ajax({
        type:"POST",
        url:"/pos-get-total-cart",
        async:false,
        beforeSend:
        function(){
            //$('#step1').hide();
            $('#processingDiv').show();                      
            //authenticateUserBeforeAjax();
		    $("#placeorder").hide();
		    $("#placeOrderProcessingDiv").show(); 	            

        //showAjxLoader()
        },
        data:$("#addNewShippingFrm").serialize()+"&shipcode="+$(
            "#addNewShippingFrm [name='shipping_zip_code']").val()+"&ship_m=1&source_type=frontend&shipping_company=&shipping_address="+$("#addNewShippingFrm [name='shipping_address_1']").val
        ()+"&status="+status+"&couponcode="+couponCode+"&shipping_method="+$(
            "input:radio[name=shipping_method]:checked").val()+"&pick_up="+isPickUp,
        success:function(responseData){
        
		    $("#placeorder").show();
		    $("#placeOrderProcessingDiv").hide(); 	
		    $("#processingDivPickup").hide();                    
        
            if($("input[name='shipping_method']").
                last().next().attr("class")=="error")$("input[name='shipping_method']").last().next().remove();
            try{
                responseData=JSON.
                parse(responseData)
            }catch(e){
                responseData=responseData
            }
            if(typeof responseData=="object")if(responseData.status==
                "shipping_error"&&$.trim($("input:radio[name=shipping_method]:checked").val())!=""){
                updatecarttotal("error");
                $(
                    "#ship_method_error").val(1);
                $.each(responseData.message,function(i,msg){
                    var elemArr=$("input[name='"+i+"']");
                    $(elemArr)
                    .last().next("label").remove();
                    $(elemArr).last().after('<label class="error" style="display:block;">'+msg+"</label>")
                })
            }
            else{ 
                var jsonObj=responseData;
                if (jsonObj.status == 'error') 
                {
                    $('#is_promotion_applied').val('');
					if(jsonObj.message!='' && typeof(jsonObj.message)!='undefined')
						$(".applycouponcodeerror").html(jsonObj.message).show().addClass(jsonObj.status);
                } 
                else 
                {
                    $('#is_promotion_applied').val(1);
                    $(".applycouponcodeerror").removeClass("error");
					$(".applycouponcodeerror").hide();
					
					if(jsonObj.message!='' && typeof(jsonObj.message)!='undefined')
						$(".applycouponcodeerror").html(jsonObj.message).show().addClass(jsonObj.status);
                }
                
                
                $(".itemtotal").html("$"+jsonObj.itemtotal);
                $(".shipping").html("$"+jsonObj.shipping);
                $(
                    ".totalaftershipping").html("$"+jsonObj.totalaftershipping);
                $(".estimatedtax").html("$"+jsonObj.estimatedtax);
                $(
                    ".finaltotal").html("ORDER TOTAL <span>$"+jsonObj.finaltotal+"</span>");
                $(".coupondiscount").html("$"+jsonObj.
                    coupondiscount)
            }
            $('#processingDiv').hide();
            //$('#step1').show();
			
			$("#processingDivSave").hide();
            $("#save_new_shipping_address2").show();   
            
            if(payment_status == 'payment_error') {
                    $("#save_new_shipping_address").click();
	                payment_status = '';
                }
        },
		complete: function(){
			$('#processingDivSave').hide();
            hideAjxLoader();
		}
    })
}
$(document).ready(function(){
	//$("#shipping_state").val($("#shipping_state_id").val());
    $("#shipping_country").change(function(){ 
		$("#shipping_state_id").next('label.error').remove();
        $("#shipping_state").val('');
        $("#shipping_state_id").val('').select2();
    });
    $("#billing_country").change(function(){
        $("#billing_state").val('');
        $("#billing_state_id").val('');
    });    
    $(document).on("click",".shipmethod,#pick_up,#ship",function(){
        if($('#pick_up').is(':checked') == true) {
			$('#step1div').hide();
			
			//$('#step1').click();
			$("#processingDivPickup").show();
			//$('.first').hide();
		}
		else {
			$("#processingDivPickup").show();
			$('#step1div').show();
			
			//displayBlock('ship');
		}
       // $('#step1').hide();
        $('#processingDiv').show();    
        $(".error:contains('Your shipping address is invalid, Please enter valid address.')").hide();
        $("#ship_method_error").val(0);
        updatecarttotal("");
    });
    
    /* Cash Payment */
     $("#cash_payment").on("click",function(){
        if($('#cash_payment').is(':checked') == true) {
           // $('#step2div').hide();
        }
        //else {
        //    $('#step2div').show();
        //}
     });
     $("#credit_payment").on("click",function(){
        if($('#credit_payment').is(':checked') == true) {
           $('#step2div').show();
        }
     });     
    
    
    jQuery.validator.addMethod("zipcodevalidation", function(value, element) {
        return this.optional(element) || /^[a-z0-9- ]+$/i.test(value);
    }, "Enter valid details");
    
    jQuery.validator.addMethod("alphanumericspecialchar",function(value,
        element){
        return this.optional(element)||/^[A-Za-z0-9 ,.-]+$/i.test(value)
    },"Enter valid details");
    jQuery.
    validator.addMethod("letterOnly",function(value,element){
        return this.optional(element)||/^[a-z '\-]+$/i.test(value)
    },
    "Enter valid details");
    jQuery.validator.addMethod("phoneno",function(value,element){
        return this.optional(element)
        ||/^[0-9-+]+$/i.test(value)
    },"Enter only alphabet.");
    jQuery.validator.addMethod("alpha",function(value,element){
        return this.optional(element)||/^[a-zA-Z ()\',.-]+$/i.test(value)},"Please enter alphabets only.");jQuery.validator.
    addMethod("creditcardexpiry",function(value,element){
        var form=element.form,expiry=form.credit_card_exp_year.value+form.
        credit_card_exp_month.value,date=new Date(),month=date.getMonth()+1,now=""+date.getFullYear()+(month<10?"0"+month:month)
        ;
        return expiry>now
    });
    
    
    $("#same_as_shipping").click(function(){    
        if($("input[type=checkbox][name=same_as_shipping]").is(":checked") == true) {
            $("#billing_to_add_option_error").hide();
            }
        else   {
            $("#billing_to_add_option_error").show();
                $("#add_new_billing_address_label").click();
            }
    });  
    
    $(document).on("click",
        ".shipmethod",function(){
            var val_checked=$(this).val();
            $('input[type=radio][name=shipping_method][value="'+val_checked+
                '"]').first().attr("checked","checked")
        });

	    
	/*$("#shipping_zip_code").blur(function(){ 
       if($("#shipping_country").val()!='' && $("#shipping_zip_code").val()!='' && $("#shipping_state_id").val()!='') {
                    //$("#processingDivSave").show();
                    $("#save_new_shipping_address").hide();					
					showAjxLoader();
					$('#shipping_method_0').click();
					setTimeout(function(){
						hideAjxLoader();
					},1200);
                    //displayshipmethod();	
                }
	});*/

	selectUsaState($('#shipping_country').val(),'shipping_country','shipping');
    
    });

function removefromcart(p_id,p_type,productMappingId,typeOfProduct,
    pageSrc){
    if(typeOfProduct=="woh")$("#delete_cart_woh").css({
        display:"block"
    });else $("#delete_cart_woh").css({
        display:
        "none"
    });
    $(".delete_cart_product").colorbox({
        width:"700px",
        height:"250px",
        inline:true
    });
    $("#no_delete_cart").click(
        function(){
            $.colorbox.close();
            $("#yes_delete_cart").unbind("click");
            return false
        });
    $("#yes_delete_cart").click(function(
        ){
        $.colorbox.close();
        $("#yes_delete_cart").unbind("click");
        showAjxLoader();
        $.ajax({
            type:"POST",
            async:false,
            data:{
                cartId:
                p_id,
                productTypeId:p_type,
                productMappingId:productMappingId,
                typeOfProduct:typeOfProduct
            },
            url:"/delete-product-to-cart",
            success:function(responseData){

                if(pageSrc)if(pageSrc=="checkout"||pageSrc=="/checkout"){
                    //$("#step1").hide();
                    location.
                    reload()
                }
                getcartlist();
                showCartProducts();
                hideAjxLoader();
                if(responseData=="session_expired")authenticateUserBeforeAjax(
                    )
            },
            beforeSend:function(){
                parent.$.colorbox.close();
                parent.$("#cboxClose").click();
                parent.$("#cboxLoadedContent").remove(
                    );
                parent.$("#cboxOverlay").css({
                    visibility:"hidden"
                });
                parent.$("#colorbox").css({
                    visibility:"hidden"
                })
            }
        })
    })
}
function getcartlist(){
    showAjxLoader();
    $.ajax({
        type:"POST",
        url:"/cart-front-product",
        success:function(responseData){
            hideAjxLoader
            ();
            $("#cartlist").html(responseData)
        }
    })
}

function removefromwishlist(p_id,p_type){
    $(".delete_cart_product").colorbox({
        width:"700px",
        height:"200px",
        inline:true
    });
    $(
        "#no_delete_wishlist").click(function(){
        $.colorbox.close();
        $("#yes_delete_wishlist").unbind("click");
        return false
    });
    $(
        "#yes_delete_wishlist").click(function(){
        $.colorbox.close();
        $("#yes_delete_wishlist").unbind("click");
        showAjxLoader();
        $.
        ajax({
            type:"POST",
            url:"/remove-wishlist-front",
            data:{
                cart_id:p_id,
                product_type:p_type
            },
            success:function(responseData){
                hideAjxLoader();
                if(responseData=="session_expired")authenticateUserBeforeAjax();
                getwishlist()
            }
        })
    })
}

function selectUsaState(value,id,userType){
    if(value=="228"){
        $("#usa_state_"+userType).show();
        $("#other_state_"+userType).hide();
        $("#"+userType+"_state").val("");
        $("#state_mandatory_sign").show();
    }else{
        $("#usa_state_"+userType).hide();
        $("#other_state_"+userType).show();
        //$("#"+userType+"_state").val("");
        $("#state_mandatory_sign").hide();
    }
    /*if($("#shipping_country").val()!='' && $("#shipping_zip_code").val()!='')
            displayshipmethod();*/
}
function assignStateValue(value,id,userType){
    if(value)$("#"+userType+"_state").val(value);
    //displayshipmethod();
}
function checkCustomValidation(id){
    var flagGl=true,flagCh=true,flag_email=true;
    $('#donation_add_to_cart [name="firstname[]"]').each(function(){
        $('label[for="'+
            $(this).attr("id")+'"]').remove();
        var str=$(this).val(),regx=/^([a-zA-Z'\s]+)$/,isvalid=regx.test(str);
        if(!isvalid&&str.
            length>0){
            $(this).after('<label class="error" for="'+$(this).attr("id")+'" generated="true">'+F_NAME_PROPER+"</label>");
            flagGl=false;
            flagCh=false
        }else $('label[for="'+$(this).attr("id")+'"]').remove()
    });
    $('#donation_add_to_cart [name="lastname[]"]').each(function(){
        $('label[for="'+$(this).attr("id")+'"]').remove();
        var str=$
        (this).val(),regx=/^([a-zA-Z'\s]+)$/,isvalid=regx.test(str);
        if(!isvalid&&str.length>0){
            $(this).after(
                '<label class="error" for="'+$(this).attr("id")+'" generated="true">'+L_NAME_PROPER+"</label>");
            flagGl=false;
            flagCh=
            false
        }else $('label[for="'+$(this).attr("id")+'"]').remove()
    });
    $('#donation_add_to_cart [name="email[]"]').each(function
        (index){
            var str=$(this).val(),regx=/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            $(this).next().
            remove("span");
            var email_message="Please enter valid email",isvalid=regx.test(str);
            $(this).next().remove("label");
            if(str
                .length>0&&str!="Email")if(!isvalid){
                $(this).after(
                    '<label for="email[]" generated="true" class="error email_error" style="display:block !important;">'+EMAIL_PROPER+
                    " </label>");
                flag_email=false
            }else{
                $(this).next().remove("label");
                flag_email=true
            }else{
                $(this).next().remove("label");
                flag_email=true
            }
        });
    if($(".email_error").length>0)flag_email=false;
    if(flag_email&&flagGl&&flagCh)return true;else
        return false
}
function chooseCoupon(couponCode,promotionId){
    $("#couponcode").val(couponCode)
}
function showHideProduct(){
    if($("#carlistItem").is(":visible")==false){
		$('#hideItem').show();
		$('#showItem').hide();
        $(".number").addClass("active");
        $("#carlistItem").slideDown(300);
    }else if($("#carlistItem").is(":visible")==true){
        $('#showItem').show();
		$('#hideItem').hide();
        $(".number").removeClass("active");
        $("#carlistItem").slideUp(300);
    }
	setTimeout("$('.scroll-pane').jScrollPane();","500");
}
function showHideProductFinalPage(){
    if($("#carlistItemOrder").is(":visible")==false){
        $('#hideItemFinal').show();
		$('#showItemFinal').hide();
        $(".number").addClass("active");
        $("#carlistItemOrder").slideDown(300);
    }else if($("#carlistItemOrder").is(":visible")==true){
        $('#hideItemFinal').hide();
		$('#showItemFinal').show();
		$(".number").removeClass("active");
        $("#carlistItemOrder").slideUp(300);
    }
		setTimeout("$('.scroll-pane').jScrollPane();","500");
}

function displayBlock(blocktype){
    if(blocktype=="ship"){
        $("#is_pick_up_span").show();
        $("#continue_step_1_span").show();
        $("#cash_payment_span").hide();    
        $("#maincheckout").show();
        if($('#pick_up').is(':checked') == false) {
            $("#step1div").show();
			$("#ship_method_radio").hide();
			//displayshipmethod();
        }
        $(".discount").show();
        $(".first,.second,.third,.fourth").removeClass("active");
        $(".first").addClass("active");
        $(".steptext").html("SHIPPING INFORMATION");
        $("#orderreview").hide().html("");
        window.location.hash=
        "#checkout_section";
		$(".first").removeClass("hoverActive");
		//$(".number").removeClass("active");
		$('#pick_up_message').show();
		$("#save_new_shipping_address").show(); 
		$("#save_new_shipping_address2").hide();
		$("#processingAddNewAddressDiv").hide();   
		setTimeout("$('.scroll-pane').jScrollPane();","500");
    }
   
}

function redirect(){ 
	showAjxLoader();
	if($('#pick_up').is(':checked') == true)
	var isPickUp = 1;
else	
	var isPickUp = 0; 
		   
if($('#cash_payment').is(':checked') == true)
	var isCashPayment = 1;
if($('#credit_payment').is(':checked') == true)	
	var isCashPayment = 0; 		       
	
	var couponcode=$("#couponcode").val(),dataStr=$("#secondstep").serialize()+
	"&"+$("#addNewShippingFrm").serialize()+"&shipcode="+$("#addNewShippingFrm [name='shipping_zip_code']").val()+
	"&ship_m=1&source_type=frontend&shipping_company=&shipping_address="+$("#addNewShippingFrm [name='shipping_address_1']").val
	()+"&shipping_country_text="+$("#addNewShippingFrm [name='shipping_country'] option:selected").text()+
	"&billing_country_text="+$("#billing_country option:selected").text()+"&couponcode="+couponcode+
	"&shipping_method="+$(
		"input:radio[name=shipping_method]:checked").val()+"&pick_up="+isPickUp+"&cash_payment="+isCashPayment;
	$.ajax({
		type:"POST",
		url:"/pos-orderreview-cart-front",
		data:dataStr,
		success:function(responseData){
			hideAjxLoader();
			window.location.hash="#checkout_section";
			if(responseData=="session_expired")authenticateUserBeforeAjax();
			$("#maincheckout").hide();
			$("#orderreview").html(responseData).show();
			//ga('require', 'displayfeatures');
			//ga('send', 'pageview',{'page': '/checkout-order-review','title': 'Checkout Order Review'});
			$(".second").removeClass("active");
			$(".third").addClass("active");
			var checked_value=$(".shipmethod:checked").val(); 
			$('input[type=radio][name=radio_button][value="'+checked_value+'"]').first().attr("checked","checked")
		},
		complete:function(
			){
			$('.scroll-pane').jScrollPane();
			var radio_html=$(".ship_method_class").html(),ch=$("input:radio[name=shipping_method]:checked").val();
			$(
				"#order_review_ship").html(radio_html);
			$("input[type=radio][value='"+ch+"']").prop("checked",true);
			
			//$('#step2').show();
			$('#processingDiv2').hide();    
			
			$(".first").addClass("hoverActive");
			$(".second").addClass("hoverActive");
			$(".third").removeClass("hoverActive");
		}
	});                            

	// redirect to order review cart page
}

function showShippingTerms(){
	$.colorbox({width:"1300px",height:"850px",iframe:true,href:"/woh-shipping-terms"});
}