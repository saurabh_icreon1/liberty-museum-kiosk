$(function() {
    $("#transaction_date_from").datepicker({changeMonth: true, changeYear: true, onClose: function(selectedDate) {
            $(
                    "#transaction_date_to").datepicker("option", "minDate", selectedDate)
        }});
    $("#transaction_date_to").datepicker({changeMonth
                : true, changeYear: true, onClose: function(selectedDate) {
            $("#transaction_date_from").datepicker("option", "maxDate",
                    selectedDate)
        }});
    $("#cboxClose").click(function() {
        $("#list_woh_entries").trigger("reloadGrid", [{page: 1}]);
    });
    $("#close_confirm").click(function() {        
        parent.$("#list_woh_entries").trigger("reloadGrid", [{page: 1}]);        
        parent.$.colorbox.close();
    });
    var listWOHGrid = jQuery(
            "#list_woh_entries"), emptyMsgDiv = $('<div class="no-record-msz">' + NO_RECORD_FOUND + "</div>");
    $("#list_woh_entries").jqGrid
            ({mtype: "POST", url: "/get-woh-entries", datatype: "json", sortable: false, postData: {searchString: $("#search_woh").serialize()
                }, colNames: [WOH_HEADING_LIST_CONTACT_NAME, WOH_HEADING_LIST_EMAIL, WOH_HEADING_LIST_TRANSACTION,
                    WOH_HEADING_LIST_TRANSACTION_DATE, WOH_HEADING_LIST_NAME_FORMAT, WOH_HEADING_LIST_HONOREE_NAME, WOH_HEADING_LIST_PANEL,
                    APPROVAL_REQ, WOH_HEADING_LIST_AMOUNT, "Item Status",L_ACTION], colModel: [{name: "usr_users.first_name", index: "usr_users.first_name"}, {
                        name: "usr_users.email_id", index: "usr_users.email_id"}, {name: "usr_transactions.transaction_id", index:
                                "usr_transactions.transaction_id"}, {name: "usr_transactions.transaction_date", index: "usr_transactions.transaction_date"},
                    {name: "pro_products.product_name", index: "pro_products.product_name"}, {name: "honoree.name", index: "honoree.name", sortable:
                        false}, {name: "usr_wall_of_honor.panel_no", index: "usr_wall_of_honor.panel_no"}, {name: "usr_wall_of_honor.status", index:
                                "usr_wall_of_honor.status"}, {name: "usr_wall_of_honor.product_total", index: "usr_wall_of_honor.product_total"},{name: "usr_wall_of_honor.product_status_id", index: "usr_wall_of_honor.product_status_id"}, {name:
                                "Action", sortable: false, cmTemplate: {title: false}}], viewrecords: true, sortname: "usr_wall_of_honor.woh_id", sortorder: "desc"
                        , rowNum: numRecPerPage, rowList: pagesArr, pager: "#listwohentries", viewrecords:true, autowidth: true, shrinkToFit: true, caption:
                        "", width: "100%", cmTemplate: {title: false}, loadComplete: function() {
                    hideAjxLoader();
                    var count = listWOHGrid.getGridParam(), ts
                            = listWOHGrid[0];
                    if (ts.p.reccount === 0) {
                        listWOHGrid.hide();
                        emptyMsgDiv.show();
                        $("#pcrud div.ui-paging-info").css("display"
                                , "none")
                    } else {
                        listWOHGrid.show();
                        emptyMsgDiv.hide();
                        $("#pcrud div.ui-paging-info").css("display", "block")
                    }
                }});
    emptyMsgDiv.insertAfter(listWOHGrid.parent());
    emptyMsgDiv.hide();
    $("#list_woh_entries").jqGrid("navGrid",
            "#listwohentries", {reload: true, edit: false, add: false, search: false, del: false});
    $("#list_woh_entries").jqGrid(
            "navButtonAdd", "#listwohentries", {caption: "", title: "Export", id: "exportExcel", onClickButton: function() {
            exportExcel(
                    "list_woh_entries", "/get-woh-entries")
        }, position: "last"});
    $("#searchbutton").click(function() {
        showAjxLoader();
        jQuery(
                "#list_woh_entries").jqGrid("setGridParam", {postData: {searchString: $("#search_woh").serialize()}});
        jQuery(
                "#list_woh_entries").trigger("reloadGrid", [{page: 1}]);
        window.location.hash = "#gview_list";
        hideAjxLoader();
        return false
    });
    $("#editwohentries").validate({submitHandler: function() {
            var dataStr = $("#editwohentries").serialize();
            showAjxLoader();
            $.
                    ajax({type: "POST", data: dataStr, url: "/edit-woh-entries", success: function(responseData) {
                    if (!checkUserAuthenticationAjx(
                            responseData))
                        return false;
                    var jsonObj = jQuery.parseJSON(responseData);
                    hideAjxLoader();
                    if (jsonObj.status === "success") {
                        $(
                                "#success_message").show();
                        $("#editwohentries").hide();
                        parent.$.colorbox.resize({innerWidth: "500px",
                            innerHeight: parseInt($("html").height()) + parseInt(10) + "px"})
                    } else
                        $.each(jsonObj.message, function(i, msg) {
                            $("#" + i).after(
                                    '<label class="error" style="display:block;">' + msg + "</label>")
                        })
                }})
        }, rules: {panel_number: {required: true}}, messages: {
            panel_number: {required: PANEL_NUMBER_EMPTY}}})
});
function editWohEntries(encWohId) {
    showAjxLoader();
    $.ajax({type: "GET",
        data: {}, url: "/edit-woh-entries/" + encWohId, success: function(response) {
            if (!checkUserAuthenticationAjx(response))
                return false;
            else {
                hideAjxLoader();
                $.colorbox.close();
                $.colorbox({width: "500px", height: "300px", iframe: true, href:
                            "/edit-woh-entries/" + encWohId})
            }
        }})
}