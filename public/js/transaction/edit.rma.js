$(function(){
    jQuery.validator.addMethod("numericchar",function(value,element){
        return this.optional(element)||
        /^[0-9.]+$/i.test(value)
    });
    $("#create_rma").validate({
        submitHandler:function(){
		    // if($("#rma_action").val() == 6)
		    //    return false;        
        
            var dataStr=$("#create_rma").serialize(),
            edit="true";
            if(checkCustomValidation("create_rma") == false){
                return false;
            }
            if($("#check_no").val()==0 && $("#rma_status").val()==1) {
                $("#check_no").after('<label for="check_no" generated="true" class="error">Provide valid details</label>');
                return false;
            }
            if($("#amount").val()==0) {
                $("#amount").after('<label for="amount" generated="true" class="error">Provide valid amount</label>');
                return false;
            }
            $.ajax({
                type:"POST",
                url:"/edit-rma-detail/"+encrypt_rma_id,
                data:{
                    data:dataStr,
                    edit:edit
                },
                success:function(responseData){
                    if(!checkUserAuthenticationAjx(responseData))return false;
                    var jsonObj=jQuery.parseJSON(responseData);
                    if(jsonObj.status=="success")
                        window.location="/crm-rma";
                    else if(jsonObj.status=="error")
                        $("#refund_tax").after('<label class="error" style="display:block;">'+jsonObj.msg+"</label>")   
                    else $.each(jsonObj.message,function(i,msg){                    
                        $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                    })
                }
            })
        },
        rules:{
            rma_action:{
                required:true
            },
            rma_status:{
                required:{
                    depends:function(){
                        if($("#rma_action").val()>5)return false;else return true
                    }
                }
            },
            bank_name:{
                required:{
                    depends:
                    function(){
                        if($("#rma_status").val()=="1"||$("#rma_status").val()=="2")return true;else return false
                    }
                }
            },
            transaction:{
                required:{
                    depends:function(){
                        if($("#rma_status").val()=="2"||$("#rma_status").val()=="3")return true;else return false
                    }
                }
            },
            amount:{
                required:{
                    depends:function(){
                        if($("#rma_status").val()=="1"||$("#rma_status").val()=="2"||$("#rma_status").val()=="3")return true;else return false
                    }
                },
                numericchar:true
            },
            check_no:{
                required:{
                    depends:function(){
                        if($("#rma_status").val()=="1")return true;
                        else return false
                    }
                },
                digits:true
            }
        },
        messages:{
            rma_action:{
                required:ERROR_RMA_ACTION
            },
            rma_status:{
                required:"Please select method"
            },
            bank_name:{
                required:"Provide valid details"
            },
            transaction:{
                required:"Provide details"
            },
            amount:{
                required:"Provide valid amount",
                numericchar:"Provide valid amount"
            },
            check_no:{
                required:"Provide valid details",
                digits:"Provide valid details"
            }
        }
    });
    $("#rma_action").change(function(){
       // $("#amount").val('');
        if($("#rma_action").val()=="5"){
            $("#rma_status_div").show();
            $("#bank_name_div").hide();
            $("#check_no_div").hide();
            $("#transaction_div").hide();
            $("#amount_div").hide()            
        }
        else {
            $("#rma_status_div").hide();
            $("#rma_status").val('').select2();
        }        
    }).change();
    $("#rma_status").change(function(){
        if($("#rma_status").val()=="1"){
            $("#bank_name_div").show();
            $("#check_no_div").show();
            $("#transaction_div").hide();
            $("#transaction").val("");
            $("#amount_div").show()
        }else if($("#rma_status").val()=="2"){
            $("#bank_name_div").show();
            $("#check_no").val("");
            $("#check_no_div").hide();
            $("#transaction_div").show();
            $("#amount_div").show()
        }else if($("#rma_status").val()=="3"){
            $("#bank_name").val("");
            $("#check_no").val("");
            $("#bank_name_div").hide();
            $("#check_no_div").hide();
            $("#transaction_div").
            show();
            $("#amount_div").show()
        }else{
            $("#bank_name_div").hide();
            $("#check_no_div").hide();
            $("#transaction_div").hide();
            $("#amount_div").hide()
        }
    }).change()
})

function checkCustomValidation(id){
    var flagQty=true,flagRsn=true,flagCon=true,pass=false;
    var atleastOneFlag = false;            
    $('#create_rma [name="returned_qty[]"]').each(function(index){
        var str=$(this).
        val(),regx=/^[0-9]+$/i;
        if(str != 0 && str!='')
        {
            atleastOneFlag = true;
        }        
        $(this).next().remove("span");
        if(str.match(regx)){
            if(str.length>0)pass=true;
            if(str.length>10&&str!=""){
                if($(this).next("span").length==0)$(this).after('<span class="error" for="returned_qty[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'+ERROR_QTY+"</span>");
                flagQty=false
                }else if($(this).parents("td").prev("td").children("div").html()<str){
                $(this).after('<span class="error" for="returned_qty[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'+ERROR_QTY+"</span>");
                flagQty=false
                }else $(this).next().remove("span")
                }else if(!str.match(regx)&&str.length>0){
            $(this).after('<span class="error" for="returned_qty[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'
                +ERROR_QTY+"</span>");
            flagQty=false
            }
        });
        
	if(atleastOneFlag == false) {   
		$("#returned_qty").after('<span class="error" for="returned_qty[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'
                +ERROR_QTY+"</span>");
			return false;        
	        } 
	        
	$('#create_rma [name="returned_reason[]"]').each(function(index){
		    var str=$(this).
		    val(),regx=/^[a-zA-z0-9.]+$/i;
		    $(this).next().remove("span");
		    if(str.match(regx))if(str.length==1){
		        if($(this).next("span").length==0)$(this).after('<span class="error" for="returned_reason[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'+ERROR_REASON+"</span>");
		        flagRsn=false
		        }else if(str.length>50&&str!=""){
		        if($(this).next("span").length==0)$(this).after('<span class="error" for="returned_reason[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'+ERROR_REASON+"</span>");
		        flagRsn=false
		        }else $(this).next().remove("span");
		    else if(str.length==0&&$(this).parents("div").parents("td").prev("td").children("div").children("input").val()>0){
		        $(this).after('<span class="error" for="returned_reason[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'
		            +ERROR_REASON+"</span>");
		        flagRsn=false
		        }
		    });
			$('#create_rma [name="item_condition[]"]').each(function(index){
			    var str=$(this).val();
			    $(this).next().remove("span");
			    if(str.length==0&&$(this).parents("div").parents("td").prev("td").prev("td").children("div").children("input").val()>0){
			        $(this).after('<span class="error" for="item_condition[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'
			            +ERROR_CONDITION+"</span>");
			        flagCon=false
			        }
			    });
			if(flagQty&&flagRsn&&flagCon){
		    $(".detail-section").css("display","block");
		    return pass
		    }else return false
    }