$(function(){$(document).on("click","input[id^='note']",function(){var noteId=this.id.substr(6),val=0;if($("#"+this.id).
is(":checked"))val="1";else val="0";showAjxLoader();$.ajax({type:"POST",url:"/modify-note/"+tableName+"/"+tableField+"/"
+noteId+"/"+val,success:function(responseData){var jsonObj=jQuery.parseJSON(responseData);if(jsonObj.status=="success"){
}hideAjxLoader()}})});jQuery.validator.addMethod("alphanumeric",function(value,element){return this.optional(element)||
/^[A-Za-z0-9\n .?-]+$/i.test(value)});$("#transaction_note").validate({submitHandler:function(){showAjxLoader();var 
dataStr=$("#transaction_note").serialize(),replydivClass="";if($("#notesdiv").find("div.row").last().hasClass(
"alterbg-none"))replydivClass="alterbg";else replydivClass="alterbg alterbg-none";dataStr=dataStr+"&replydivClass="+
replydivClass;$.ajax({type:"POST",url:"/add-rma-note",data:dataStr,success:function(responseData){if(!
checkUserAuthenticationAjx(responseData))return false;var jsonObj=jQuery.parseJSON(responseData);if(jsonObj.status==
"success"){$("#note").val("");$("#notesdiv").append(jsonObj.content)}else $.each(jsonObj.message,function(i,msg){$("#"+i
).after('<label class="error" style="display:block;">'+msg+"</label>")});hideAjxLoader()}})},rules:{note:{required:true,
alphanumeric:true}},messages:{note:{required:"Value required",alphanumeric:"Invalid entry"}}})})