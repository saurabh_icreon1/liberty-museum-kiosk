$(function() {
    $("input").customInput();
    $("#batch_id_upload").autocomplete({afterAdd: true, selectFirst: true, autoFocus: true,
        source: function(request, response) {
            $.ajax({url: "/get-available-batch", method: "POST", dataType: "json", data: {batch_id: $(
                            "#batch_id_upload").val(), is_locked:'0'}, success: function(jsonResult) {
                    var resultset = [];
                    $.each(jsonResult, function() {
                        var id = this.
                                payment_batch_id, value = this.batch_id;
                        resultset.push({id: this.payment_batch_id, value: this.batch_id})
                    });
                    response(resultset
                            )
                }})
        }, open: function() {
            $(".ui-menu").width(350)
        }, change: function(event, ui) {
            if (ui.item) {
                $("#batch_id_upload").val(ui.item.
                        value);
                $("#payment_batch_id_upload").val(ui.item.id)
            } else {
                $("#batch_id_upload").val("");
                $("#payment_batch_id_upload").
                        val("");
                $("#file_name").attr("disabled", "disabled")
            }
        }, blur: function(event, ui) {
            if ($("#payment_batch_id_upload").val() == ""
                    ) {
                $("#batch_id_upload").val("");
                $("#file_name").val("");
                $("#file_name").attr("disabled", "disabled")
            }
        }, select: function(
                event, ui) {
            $("#batch_id_upload").val(ui.item.value);
            $("#payment_batch_id_upload").val(ui.item.id);
            $("#file_name").
                    removeAttr("disabled");
            return false
        }});
    $("#batch_id").autocomplete({afterAdd: true, selectFirst: true, autoFocus: true, source
                : function(request, response) {
            $.ajax({url: "/get-available-batch", method: "POST", dataType: "json", data: {batch_id: $(
                            "#batch_id").val(), is_locked:'0'}, success: function(jsonResult) {
                    var resultset = [];
                    $.each(jsonResult, function() {
                        var id = this.
                                payment_batch_id, value = this.batch_id;
                        resultset.push({id: this.payment_batch_id, value: this.batch_id})
                    });
                    response(resultset
                            )
                }})
        }, open: function() {
            $(".ui-menu").width(350)
        }, change: function(event, ui) {
            if (ui.item) {
                $("#batch_id").val(ui.item.value);
                $("#payment_batch_id").val(ui.item.id)
            } else {
                $("#batch_id").val("");
                $("#payment_batch_id").val("")
            }
        }, focus: function(event
                , ui) {
            $("#payment_batch_id").val(ui.item.id);
            return false
        }, select: function(event, ui) {
            $("#batch_id").val(ui.item.value);
            $(
                    "#payment_batch_id").val(ui.item.id);
            return false
        }});
    var url = "/create-batch";
    if ($("#file_name"))
        $("#file_name").
                fileupload({url: url, dataType: "json", formData: function(form) {
                return $("#bulk_donation_create").serializeArray()
            }, done:
                    function(e, data) {
                        data = data.result;
                        if (data.status == "success") {
                            $("#import-success").show();
                            $("#import-success").html(data.
                                    message);
                            $("#import-error").hide();
							$("#batch_id_upload").val('');
							$("#payment_batch_id_upload").val('');
                        } else {
                            $("#import-error").show();
                            $("#import-error").html(data.message);
                            $(
                                    "#import-success").hide()
                        }
                    }, start: function(e) {
                showAjxLoader()
            }, stop: function(e) {
                hideAjxLoader()
            }});
    $("#contact_name_1").
            autocomplete({afterAdd: true, selectFirst: true, autoFocus: true, minLength: 4, source: function(request, response) {
            $.ajax({url:
                        "/get-contact-auto-search", method: "POST", dataType: "json",  data: {contact_id: $("#contact_name_1").val()}, success: function(jsonResult) {
                    if (!checkUserAuthenticationAjx(jsonResult))
                        return false;
                    var resultset = [];
                    $.each(jsonResult, function() {
                        var id = this.
                                user_id, value = this.full_name;
                        if (this.contact_id != "")
                            value = value + " ( Contact ID: " + this.contact_id;
                        if (this.email_id != "")
                            value = value +
                                    ", Email ID: " + this.email_id;
						if (this.omx_customer_number != "")
                            value = value +
                                    ", OMX Customer No. : " + this.omx_customer_number;
                        value = value + " ) ";
                        var value1 = this.full_name;
                        resultset.push({id: this.user_id, value: value, value1: value1})
                    });
                    response(resultset)
                }})
        }, open: function() {
            $(".ui-menu").width(350)
        }, change: function(event, ui) {
            if (ui.item) {
                $(
                        "#contact_name_1").val(ui.item.value1);
                $("#contact_name_id_1").val(ui.item.id)
            } else {
                $("#contact_name_1").val("");
                $(
                        "#contact_name_id_1").val("")
            }
        }, focus: function(event, ui) {
            $("#contact_name_id_1").val(ui.item.id);
            return false
        }, select:
                function(event, ui) {
                    $("#contact_name_1").val(ui.item.value1);
                    $("#contact_name_id_1").val(ui.item.id);
                    return false
                }});
    $(
            "#campaign").autocomplete({afterAdd: true, selectFirst: true, autoFocus: true, source: function(request, response) {
            $.ajax({url:
                        "/get-campaign-code", method: "POST", dataType: "json", data: {campaign_code: $("#campaign").val(),
                    active:1}, success: function(jsonResult) {
                    var resultset = [];
                    $.each(jsonResult, function() {
                        var id = this.campaign_id, value = this.campaign_code;
                        resultset.push({id: this.
                                    campaign_id, value: this.campaign_code})
                    });
                    response(resultset)
                }})
        }, open: function() {
            $(".ui-menu").width(350)
        }, change:
                function(event, ui) {
                    if (ui.item) {
                        $("#campaign").val(ui.item.value);
                        $("#campaign_id").val(ui.item.id)
                    } else {
                        $("#campaign").
                                val("");
                        $("#campaign_id").val("")
                    }
                }, focus: function(event, ui) {
            $("#campaign_id").val(ui.item.id);
            return false
        }, select:
                function(event, ui) {
                    $("#campaign").val(ui.item.value);
                    $("#campaign_id").val(ui.item.id);
                    return false
                }});
    $(
            "#bulk_donation_create").validate({submitHandler: function() {
            if (checkCustomValidation()) {
                var dataStr = $(
                        "#bulk_donation_create").serialize();
				$('#uploadbutton').attr('disabled','disabled');
				$('#submitandlock').attr('disabled','disabled');
                showAjxLoader();
                $.ajax({type: "POST", data: dataStr, url: "/create-batch", success:
                            function(responseData) {
                                if (!checkUserAuthenticationAjx(responseData))
                                    return false;
                                var jsonObj = jQuery.parseJSON(
                                        responseData);
                                hideAjxLoader();
                                if (jsonObj.status === "success") {
                                    $("#total_amount").text("0");
                                    $("#bulk_donation_create").
                                            find("input[type=text]").val("");
                                    $("#bulk_donation_create").find("select").val("3").select2();
                                    $("#success_message").show
                                            ();
                                    $("#success_message").html("<div>" + jsonObj.message + "</div>");
                                    setTimeout(function() {
                                        window.location.href =
                                                "/donation-bulk-entries"
                                    }, 1000)
                                } else {
                                    $(".accordion-detail .head:first").addClass("active");
                                    $(".detail-section").eq(0).
                                            show();
                                    $(".accordion-detail .head:last").attr("class", "head");
                                    $(".detail-section").eq(1).hide();
                                    $.each(jsonObj.message,
                                            function(i, msg) {
                                                $("#" + i).after('<label class="error" style="display:block;">' + msg + "</label>")
                                            })
                                }
                            }})
            }
        }, rules: {batch_id: {
                required: true}}, messages: {batch_id: {required: "Please enter batch id"}}, highlight: function(label) {
            $(
                    ".accordion-detail .head:first").addClass("active");
            $(".detail-section").eq(0).show();
            $(".accordion-detail .head:last").
                    attr("class", "head");
            $(".detail-section").eq(1).hide()
        }});
    $("#payment_mode_id_1").change(function() {
        if ($(
                "#payment_mode_id_1").val() == 3)
            $("#check_number_1").removeAttr("readOnly");
        else {
            $("#check_number_1").val("");
            $(
                    "#check_number_1").attr("readOnly", "true")
        }
    })
});
function showDate(id, divId) {
    if ($("#" + id + " option:selected").text() ==
            "Choose Date Range")
        $("#" + divId).show();
    else
        $("#" + divId).hide()
}
var totalDonations = typeof totalDonations != "undefined" ?
        totalDonations : 1;
function addMoreManualEntries(currAnchor) {
    showAjxLoader();
    $.ajax({type: "POST", url: "/add-more-donations"
                , data: {totalDonations: totalDonations}, success: function(responseData) {
            hideAjxLoader();
            if (!checkUserAuthenticationAjx(
                    responseData))
                return false;
            var allDivBlock = $("div[id^='person_in_database_block']");
            if (allDivBlock.length == 1) {
                var parId =
                        $(currAnchor).parent().parent().parent().attr("id"), id = parId.substring(parId.lastIndexOf("_") + 1, parId.length);
                $(
                        currAnchor).parent().append('<a class="minus m-l-5" href="javascript:void(0);" onclick="deleteMoreMnaualEntries(this,' +
                        totalDonations + ')"></a>')
            }
            $(currAnchor).remove();
            $("#person_in_database_div").append(responseData);
            totalDonations =
                    totalDonations + 1;
			$('#campaign_'+totalDonations).val($('#campaign').val());
			$('#campaign_id_'+totalDonations).val($('#campaign_id').val());
            $("#totalcount").val(totalDonations)
        }})
}
function addautofill(index) {
    $("#contact_name_" + index).
            autocomplete({afterAdd: true, selectFirst: true, autoFocus: true, minLength: 4, source: function(request, response) {
            $.ajax({url:
                        "/get-contact-auto-search", method: "POST", dataType: "json", data: {contact_id: $("#contact_name_" + index).val()}, success: function(
                        jsonResult) {
                    if (!checkUserAuthenticationAjx(jsonResult))
                        return false;
                    var resultset = [];
                    $.each(jsonResult, function() {
                        var id
                                = this.user_id, value = this.full_name;
                        if (this.contact_id != "")
                            value = value + " ( Contact ID: " + this.contact_id;
                        if (this.email_id != "")
                            value =
                                    value + ", Email ID: " + this.email_id;
						if (this.omx_customer_number != "")
                            value = value +
                                    ", OMX Customer No. : " + this.omx_customer_number;
                        value = value + " ) ";
                        var value1 = this.full_name;
                        resultset.push({id: this.user_id, value: value, value1:
                            value1})
                    });
                    response(resultset)
                }})
        }, open: function() {
            $(".ui-menu").width(350)
        }, change: function(event, ui) {
            if (ui.item) {
                $(
                        "#contact_name_" + index).val(ui.item.value1);
                $("#contact_name_id_" + index).val(ui.item.id)
            } else {
                $("#contact_name_" + index).
                        val("");
                $("#contact_name_id").val("")
            }
        }, focus: function(event, ui) {
            $("#contact_name_id_" + index).val(ui.item.id);
            return false
        }, select: function(event, ui) {
            $("#contact_name_" + index).val(ui.item.value1);
            $("#contact_name_id_" + index).val(
                    ui.item.id);
            return false
        }});
    $("#campaign_" + index).autocomplete({afterAdd: true, selectFirst: true, autoFocus: true, source:
                function(request, response) {
                    $.ajax({url: "/get-campaign-code", method: "POST", dataType: "json", data: {campaign_code: $("#campaign_"
                                    + index).val(),active:1}, success: function(jsonResult) {
                            var resultset = [];
                            $.each(jsonResult, function() {
                                var id = this.campaign_id, value =
                                        this.campaign_code;
                                resultset.push({id: this.campaign_id, value: value})
                            });
                            response(resultset)
                        }})
                }, open: function() {
            $(
                    ".ui-menu").width(350)
        }, change: function(event, ui) {
            if (ui.item) {
                $("#campaign_" + index).val(ui.item.value);
                $("#campaign_id_"
                        + index).val(ui.item.id)
            } else {
                $("#campaign_" + index).val("");
                $("#campaign_id_" + index).val("")
            }
        }, focus: function(event, ui) {
            $
                    ("#campaign_id_" + index).val(ui.item.id);
            return false
        }, select: function(event, ui) {
            $("#campaign_" + index).val(ui.item.value)
                    ;
            $("#campaign_id_" + index).val(ui.item.id);
            return false
        }});
    $("#payment_mode_id_" + index).change(function() {
        if ($(
                "#payment_mode_id_" + index).val() == 3)
            $("#check_number_" + index).removeAttr("readOnly");
        else {
            $("#check_number_" + index).val(
                    "");
            $("#check_number_" + index).attr("readOnly", "true")
        }
    })
}
function deleteMoreMnaualEntries(obj, id) {
    $("#person_in_database_block_" + id).remove();
    var lastBlock = $("div[id^='person_in_database_block']").last();
    if ($(lastBlock).find(".plus").length == 0){
		var eleid = parseInt(id)-1;
		$('#company_name_'+eleid).after('<a class="plus m-l-5" href="javascript:void(0);" onclick="addMoreManualEntries(this)" id="addMoreManualEntries"></a>');
	}
    var allDivBlock = $("div[id^='person_in_database_block']");
    if (allDivBlock.length == 1)
        $(lastBlock).find(".minus").remove();
    var totalamount = 0, tramount = 0;
    for (var i = 1; i <= $("#totalcount").val(); i++)
        if (document.getElementById("amount_" + i) && $(
                "#amount_" + i).val() != "") {
            var amount = $("#amount_" + i).val().trim();
            tramount = parseFloat($("#amount_" + i).val().trim());
            totalamount += tramount
        }
    $("#total_amount").html(totalamount.toFixed(2))
}
function updateTotalAmount(obj, event) {
    var
            elementid = obj.id, splitarr = elementid.split("_"), amountlength = document.getElementsByName("amount[]").length, totalamount = 0,
            tramount = 0;
    for (var i = 1; i <= $("#totalcount").val(); i++)
        if (document.getElementById("amount_" + i) && $("#amount_" + i).val() != "")
        {
            var amount = $("#amount_" + i).val().trim();
            tramount = parseFloat($("#amount_" + i).val().trim());
            totalamount += tramount
        }
    $(
            "#total_amount").html(totalamount.toFixed(2))
}
function checkCustomValidation() {
    var flagChkAmount = true,
            paymentModeChkAmounts = $('[name="amount[]"]'), paymentModeChkNums = $('[name="check_number[]"]'), contactName = $(
            '[name="check_number[]"]');
    $(paymentModeChkAmounts).each(function(index) {
        var str = $(this).val();
        if (str != "") {
            var regx =
                    /^-?\d*(\.\d+)?$/;
            if ($(this).val() != "")
                $(this).next().remove("span");
            else {
                if ($(this).next("span").length == 0)
                    $(this).
                            after('<span class="error" for="amount_' + (index + 1) +
                            '" generated="true" style="display:block !important;"><span class="redarrow"></span>' + CHECK_AMOUNT_VALID + "</span>");
                flagChkAmount = false
            }
            if ($("#payment_mode_id_" + (index + 1)).val() == 3) {
                var regxCheckNum = /^[a-z0-9]+$/i;
                if (document.getElementById
                        ("check_number_" + (index + 1)).value.match(regxCheckNum) && document.getElementById("check_number_" + (index + 1)).value != "" &&
                        document.getElementById("check_number_" + (index + 1)).value.length >= 3 && document.getElementById("check_number_" + (index + 1)).
                        value.length <= 20)
                    $("#check_number_" + (index + 1)).next().remove("span");
                else {
                    if ($("#check_number_" + (index + 1)).next("span").
                            length == 0)
                        $("#check_number_" + (index + 1)).after('<span class="error" for="check_number_' + (index + 1) +
                                '" generated="true" style="display:block !important;"><span class="redarrow"></span>' + CHECK_NUMBER_EMPTY + "</span>");
                    flagChkAmount = false
                }
            }
            if ($("amount_" + (index + 1)).val() != "") {
                var regxCheckAmount = /^\d+(\.\d{0,2})?$/g;
                if (document.
                        getElementById("amount_" + (index + 1)).value.match(regxCheckAmount) && document.getElementById("amount_" + (index + 1)).value != ""
                        ) {
                    $("#amount_" + (index + 1)).next().remove("span");
                    $(this).next().remove("span")
                } else {
                    if ($("#amount_" + (index + 1)).next(
                            "span").length == 0)
                        $("#amount_" + (index + 1)).after('<span class="error" for="amount_' + (index + 1) +
                                '" generated="true" style="display:block !important;"><span class="redarrow"></span>' + CHECK_VALID_DECIMAL + "</span>");
                    flagChkAmount = false
                }
            }
            if ($("#contact_name_" + (index + 1)).val() != "")
                $("#contact_name_" + (index + 1)).next().remove("span");
            else
            {
                if ($("#contact_name_" + (index + 1)).next("span").length == 0)
                    $("#contact_name_" + (index + 1)).after(
                            '<span class="error" for="contact_name_' + (index + 1) +
                            '" generated="true" style="display:block !important;"><span class="redarrow"></span>' + CONTACT_NAME_EMPTY + "</span>");
                flagChkAmount = false
            }
        } else {
            $("#amount_" + (index + 1)).after('<span class="error" for="amount_' + (index + 1) +
                    '" generated="true" style="display:block !important;"><span class="redarrow"></span>' + CHECK_AMOUNT_VALID + "</span>");
            flagChkAmount = false;
            return false
        }
    });
    return flagChkAmount
}

function searchBatchId(id){
    if(id == undefined){
        $.colorbox({
            href:"/select-batch-id",
            width:"1200px",
            height:"985px",
            iframe:true
        })
    }
    else{
        $.colorbox({
            href:"/select-batch-id/"+id,
            width:"1200px",
            height:"985px",
            iframe:true
        })
    }
}