$(function(){
    $("#ship").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:function(request,response){
            $.
            ajax({
                url:"/get-ship-list",
                method:"POST",
                dataType:"json",
                data:{
                    ship:$("#ship").val()
                    },
                success:function(jsonResult){
                    var 
                    resultset=[];
                    $.each(jsonResult,function(){
                        var value=this.ship_name;
                        resultset.push({
                            value:this.ship_name
                            })
                        });
                    response(
                        resultset)
                    }
                })
        },
    open:function(){
        $(".ui-menu").width(350)
        },
    change:function(event,ui){
        if(ui.item)$("#ship").val(ui.item.
            value);else $("#ship").val("")
            },
    focus:function(event,ui){
        return false
        },
    select:function(event,ui){
        $("#ship").val(ui.item.
            value);
        return false
        }
    })
});
$(function(){
    $("#search_passenger").validate({
        submitHandler:function(){
            showAjxLoader();
            var grid
            =jQuery("#list"),emptyMsgDiv=$('<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");
            $.jgrid.no_legacy_api=true;
            $.
            jgrid.useJSON=true;
            $("#list").jqGrid({
                postData:{
                    searchString:$("#search_passenger").serialize()+"&"+$("#add_to_cart").
                    serialize()
                    },
                mtype:"POST",
                url:"/get-add-to-cart-crm-search-passenger",
                datatype:"json",
                sortable:true,
                colNames:[
                L_PASSENGER_ID,L_LAST_NAME,L_FIRST_NAME,L_GENDER,L_AGE_AT_ARRIVAL,L_ETHINICITY,SHIP,L_DATE_OF_ARRIVAL,
                "Last Place of Residence/Birth Place"],
                colModel:[{
                    name:"Passenger ID",
                    index:"field0"
                },{
                    name:"Last Name",
                    index:"field3"
                },

                {
                    name:"First Name",
                    index:"field2"
                },{
                    name:"Gender",
                    index:"field10",
                    width:60
                },{
                    name:"Age",
                    index:"field14",
                    width:100
                },{
                    name
                    :"Ethinicity",
                    index:"field36"
                },{
                    name:"ShipName",
                    index:"field7",
                    width:110
                },
                {
                    name:"ArrivalDate",
                    index:"field18",
                    width:90
                },{
                    name:"Lastplacetovisit",
                    index:"field4",
                    sortable:false,
                    width:250
                }],
                viewrecords:true,
                sortname:"",
                sortorder:"",
                rowNum:10,
                rowList:[10,20,30],
                pager:
                "#pcrudpassenger",
                viewrecords:true,
                autowidth:true,
                shrinkToFit:true,
                caption:"",
                width:"100%",
                cmTemplate:{
                    title:false
                },
                loadComplete:function(){
                    var count=grid.getGridParam(),ts=grid[0];
                    if(ts.p.reccount===0){
                        grid.hide();
                        emptyMsgDiv.show();
                        $(
                            "#pcrudpassenger_right div.ui-paging-info").css("display","none")
                        }else{
                        grid.show();
                        emptyMsgDiv.hide();
                        $(
                            "#pcrudpassenger_right div.ui-paging-info").css("display","block")
                        }
                        hideAjxLoader();
                    $("#searchResultBlock").show()
                    }
                });
        emptyMsgDiv.insertAfter(grid.parent());
        emptyMsgDiv.hide();
        $("#list").jqGrid("navGrid","#pcrudpassenger",{
            reload:true,
            edit:false,
            add:false,
            search:false,
            del:false
        });
        jQuery("#list").jqGrid("setGridParam",{
            postData:{
                searchString:$(
                    "#search_passenger").serialize()+"&"+$("#add_to_cart").serialize()
                }
            });
    jQuery("#list").trigger("reloadGrid",[{
        page:1
    }]);
    return false
    },
    rules:{},
    messages:{}
});
$(document).on("change","#is_passenger_record",function(){
    if($(this).is(":checked")
        ){
        $(".show_passenger_record").show();
        $("#passenger_record_qty").removeAttr("readonly");
        $(
            "input[id^='attribute_option_id_pass']").removeAttr("disabled")
        }else{
        $(".show_passenger_record").hide();
        $(
            "#passenger_record_qty").attr("readonly","readonly");
        $("#passenger_record_qty").val("");
        $(
            "input[id^='attribute_option_id_pass']").attr("disabled","disabled")
        }
    });
$(document).on("change","#is_ship_image",
    function(){
        if($(this).is(":checked")){
            $(".show_ship_record").show();
            $("#ship_image_qty").removeAttr("readonly");
            $(
                "input[id^='attribute_option_id_ship']").removeAttr("disabled")
            }else{
            $(".show_ship_record").hide();
            $("#ship_image_qty").
            attr("readonly","readonly");
            $("#ship_image_qty").val("");
            $("input[id^='attribute_option_id_ship']").attr("disabled",
                "disabled")
            }
        });
$(document).on("change","#is_manifest",function(){
    if($(this).is(":checked")){
        $("#manifestBlock").show();
        $('#mycarousel').jcarousel();
        setTimeout("calculateManifestPrice()","2000");     
        setTimeout("showManifestImageSlider();","4000")
        }else $("#manifestBlock").hide()
        });
$("#advance_search").click(function(){
    $("#advanceSearchBlock").show();
    $(this).hide()
    })
});
function getPassengerRecordInfo(passengerId){
    showAjxLoader();
    $.ajax({
        url:"/get-passenger-record-info",
        method:"POST",
        dataType:"html",
        data:{
            passengerId:passengerId,
            user_id:$("#user_id").val()
        },
        success:function(
            responseData){
            hideAjxLoader();
            $("#passenger_record_info").html(responseData)
            }
        })
}
function showPassengerRecord(){
    $(
        ".show_passenger_record").colorbox({
        width:"867px",
        height:"690px",
        inline:true
    })
    }
    function showPassengerShip(){
    $(
        ".show_ship_record").colorbox({
        width:"650px",
        height:"450px",
        inline:true
    })
    }
    function calculateManifestPrice(){ 
    var imageCount = $("#mycarousel li").length;
	var generateStr="";
	var numOfAdditionalImage = 0;
    var imageArray = new Array();
    var is_first = 0;
    var totalPrice = 0;
    var additionatTotalPrice = 0;
	
	if($("#is_first_image").is(":checked") == true) {
        if($("#is_first_image").is(":checked") == true) {
			    imageArray[imageArray.length] = 1;  
				is_first = 1;  
			}
	}
    if($("#is_second_image").is(":checked") == true) {
        if($("#is_second_image").is(":checked") == true) {
                if(is_first == 1)     {
					    imageArray[imageArray.length] = 2; 
					}       		
				else if(is_first == 0)  {	
					    imageArray[imageArray.length] = 1;   
					} 
			}
	}
	var isAdditional = 0;
	if(manifestType == "TwoPage") {
			var startIndex = 3; // It means that third image will be the additional image.
			var minusIndex = 2;
		}
	if(manifestType == "OnePage")	{
			var startIndex = 2;     // It means that second image will be additional image.
			var minusIndex = 1;
		}
		
    for(var i=startIndex;i<=imageCount; i++) {
		if($("#additional_images_"+i).is(":checked") == true)
			imageArray[imageArray.length] = i; 
    }
    for(var j=0; j<imageArray.length; j++)
    {
        if(j==0)
        {
            generateStr+='<hr/><div class="col4">First image </div><div> $'+imagePriceArray[0].toFixed(2)+"</div>";
	        totalPrice+=imagePriceArray[0];
        }
        else if(j==1 && manifestType == "TwoPage")
        {
            generateStr+='<hr/><div class="col4">Second image </div><div> $'+imagePriceArray[1].toFixed(2)+"</div>";
	        totalPrice+=imagePriceArray[1];
        }
        else {
                if(imageArray.length>minusIndex) {
                    var isAdditional = 1;
                    additionalImagePrice = imagePriceArray[0].toFixed(2);
	                //additionatTotalPrice=additionalImagePrice;        
                }

        }
    }
    if(isAdditional == 1) {
	    numOfAdditionalImage = imageArray.length - minusIndex;
	    totalPrice = totalPrice + additionalImagePrice*numOfAdditionalImage;
    }
    if(numOfAdditionalImage>0) {
        generateStr+='<hr/><div class="col4">Additional Images('+numOfAdditionalImage+') </div><div> $'+additionalImagePrice*numOfAdditionalImage+"</div>";
    }
	if(totalPrice!=0)
		generateStr+='<hr/><div class="col4"><strong class="bold">Total </strong></div><div><strong class="bold"> $'+totalPrice.toFixed(2)+"</strong></div>"
	
	$("#manifestPriceDetail").html(generateStr);	                
   // return false;
    /*
    var 
    totalPrice=0,generateStr="",numOfAdditionalImage=0;
    numOfAdditionalImage=$("input[id^='additional_images_']:checked").
    size();
    var numOfAdditionalImagePrice=additionalImagePrice*numOfAdditionalImage;
    if(manifestType=="OnePage"){
        if($("#is_first_image").is(":checked"))
                {
		            generateStr+='<hr/><div class="col4">Ship Manifest</div><div> $'+
		            imagePriceArray[0].toFixed(2)+"</div>";
		            totalPrice+=imagePriceArray[0]
	            }
	            if(numOfAdditionalImagePrice!=0){
	                generateStr+='<hr/><div class="col4">Additional Images('+numOfAdditionalImage+")</div><div>$"+numOfAdditionalImagePrice+"</div>";
	                totalPrice+=numOfAdditionalImagePrice
	            }
	            if(totalPrice!=0)
	                generateStr+='<hr/><div class="col4"><strong>Total </strong></div><div> <strong>$'+totalPrice+"</strong></div>"
            }
            else
            {
	            if($("#is_first_image").is(":checked")==true&&$("#is_second_image").is(":checked")==false||$("#is_first_image").is(":checked")==false&&$("#is_second_image").is(":checked")==true)
	            {
	                $("#add_cart_error").html("");
	                generateStr+='<hr/><div class="col4">First image </div><div> $'+imagePriceArray[0].toFixed(2)+"</div>";
	                totalPrice+=imagePriceArray[0];
		            
		            if(numOfAdditionalImage>=1)
		            {
		                generateStr+='<hr/><div class="col4">Second image </div><div> $'+imagePriceArray[1].toFixed(2)+" </div>";
		                totalPrice+=imagePriceArray[1];
		                numOfAdditionalImage--
		            }
			    }
			    else if($("#is_first_image").is(":checked")==true && $("#is_second_image").is(":checked")==true)
			    {
					            $("#add_cart_error").html("");
						        generateStr+='<hr/><div class="col4">First image </div><div> $'+imagePriceArray[0].toFixed(2)+"</div>";
						        totalPrice+=imagePriceArray[0];
						        generateStr+='<hr/><div class="col4">Second image </div><div> $'+imagePriceArray[1].toFixed(2)+" </div>";
						        totalPrice+=imagePriceArray[1]
			    }
			    if(numOfAdditionalImage>=1)
			    {
			                    numOfAdditionalImagePrice=numOfAdditionalImage*additionalImagePrice;
						        generateStr+='<hr/><div class="col4">Additional Images('+numOfAdditionalImage+")</div><div>$"+numOfAdditionalImagePrice.toFixed(2)+"</div>";
			                    totalPrice+=numOfAdditionalImagePrice
			    }
		        if(totalPrice!=0)
		            generateStr+='<hr/><div class="col4"><strong class="bold">Total </strong></div><div><strong class="bold"> $'+totalPrice.toFixed(2)+"</strong></div>"
            }
        $("#manifestPriceDetail").html(generateStr)*/
}