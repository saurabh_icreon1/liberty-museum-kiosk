$(function() {
    var grid = jQuery("#list_case"), emptyMsgDiv = $('<div class="no-record-msz">No Record Found</div>');
    $(
            "#list_case").jqGrid({mtype: "POST", url: "/get-batch-detail/" + $("#encBatchId").val(), datatype: "json", colNames: ["",
            "Transaction Id", "Contact Id", "Contact", "Campaign Code", "Payment Mode", "Check #", "Amount", "Company Name", "Created By", "Batch Date"], colModel: [{name: "Batch Id", index:
                        "tbl_tra_donation_batches.batch_id", hidden: true}, {name: "Transaction Id", index: "tbl_usr_transactions.transaction_id"}, {name: "Contact Id", index: "tbl_usr_users.contact_id"}, {name: "Contact",
                index: "tbl_usr_users.first_name"}, {name: "Campaign Code", index: "tbl_cam_campaigns.campaign_code", align: "center"}, {name:
                        "Payment Mode", index: "tbl_mst_payment_modes.payment_mode", align: "center"}, {name: "Check #", index:
                        "tbl_tra_payments.cheque_number", align: "center"}, {name: "Amount", index: "tbl_usr_transactions.transaction_amount", align:
                        "center"}, {name: "Comapny Name", index: "tbl_tra_donation_products.company_name", align:
                        "center"}, {name: "Created By", index: "added_by", align:
                        "center"}, {name: "Batch Date", index: "tbl_tra_donation_batches.batch_date", align:
                        "center"}], sortname:"tbl_tra_donation_batches.batch_date",sortorder:"DESC", viewrecords: true, rowNum: 10, rowList: [10, 20, 30], pager: "#listcaserud", autowidth: true, shrinkToFit: true, caption: ""
                , width: "100%", cmTemplate: {title: false}, loadComplete: function() {
            hideAjxLoader();
            var count = grid.getGridParam(), ts = grid[0];
            if (ts.p.reccount === 0) {
                grid.hide();
                emptyMsgDiv.show();
                $("#listcaserud_right div.ui-paging-info").css("display", "none")
            }
            else {
                grid.show();
                emptyMsgDiv.hide();
                $("#listcaserud_right div.ui-paging-info").css("display", "block")
            }
        }, afterInsertRow:
                function(rowid, rowdata) {
                    if (rowdata.Urgent == 1)
                        $(this).jqGrid("setRowData", rowid, false, "urgentRecord")
                }});
    emptyMsgDiv.
            insertAfter(grid.parent());
    emptyMsgDiv.hide();
    $("#list_case").jqGrid("navGrid", "#listcaserud", {reload: true, edit: false,
        add: false, search: false, del: false});
    $("#list_case").jqGrid("navButtonAdd", "#listcaserud", {caption: "", title: "Export", id:
                "exportExcel", onClickButton: function() {
            exportExcel("list_case", "/get-batch-detail/" + $("#encBatchId").val())
        }, position:
                "last"})
});
function showDate(id, divId) {
    if ($("#" + id + " option:selected").text() == "Choose Date Range")
        $("#" + divId).show();
    else
        $("#" + divId).hide()
}