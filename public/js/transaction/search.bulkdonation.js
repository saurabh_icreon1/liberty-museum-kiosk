$(function() {
    $("#batch_date_from").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, onClose: function(
                selectedDate) {
            $("#batch_date_to").datepicker("option", "minDate", selectedDate)
        }});
    $("#batch_date_to").datepicker({
        changeMonth: true, changeYear: true, showOtherMonths: true, onClose: function(selectedDate) {
            $("#batch_date_from").datepicker(
                    "option", "maxDate", selectedDate)
        }});
    $("#batch_id").autocomplete({afterAdd: true, selectFirst: true, autoFocus: true, source:
                function(request, response) {
                    $.ajax({url: "/get-available-batch", method: "POST", dataType: "json", data: {batch_id: $("#batch_id"
                                    ).val()}, success: function(jsonResult) {
                            var resultset = [];
                            $.each(jsonResult, function() {
                                var id = this.payment_batch_id, value =
                                        this.batch_id;
                                resultset.push({id: this.payment_batch_id, value: this.batch_id})
                            });
                            response(resultset)
                        }})
                }, open: function() {
            $
                    (".ui-menu").width(350)
        }, change: function(event, ui) {
            if (ui.item) {
                $("#batch_id").val(ui.item.value);
                $("#payment_batch_id").
                        val(ui.item.id)
            } else {
                $("#batch_id").val("");
                $("#payment_batch_id").val("")
            }
        }, focus: function(event, ui) {
            $(
                    "#payment_batch_id").val(ui.item.id);
            return false
        }, select: function(event, ui) {
            $("#batch_id").val(ui.item.value);
            $(
                    "#payment_batch_id").val(ui.item.id);
            return false
        }});
    $("#created_by_name").autocomplete({afterAdd: true, selectFirst: true,
        autoFocus: true, source: function(request, response) {
            $.ajax({url: "/get-crm-contacts", method: "POST", dataType: "json", data: {
                    crm_full_name: $("#created_by_name").val()}, success: function(jsonResult) {
                    if (!checkUserAuthenticationAjx(jsonResult))
                        return false;
                    var resultset = [];
                    $.each(jsonResult, function() {
                        var id = this.crm_user_id, value;
                        if (this.crm_email_id != "")
                            value =
                                    this.crm_full_name + " (" + this.crm_email_id + ")";
                        else
                            value = this.crm_full_name;
                        var value1 = this.crm_full_name;
                        resultset.push
                                ({id: this.crm_user_id, value: value, value1: value1})
                    });
                    response(resultset)
                }})
        }, open: function() {
            $(".ui-menu").width(300)
        },
        change: function(event, ui) {
            if (ui.item) {
                $("#created_by_name").val(ui.item.value1);
                $("#added_by").val(ui.item.id)
            } else {
                $(
                        "#created_by_name").val("");
                $("#added_by").val("")
            }
        }, focus: function(event, ui) {
            $("#added_by").val(ui.item.id);
            return false
        }, select: function(event, ui) {
            $("#created_by_name").val(ui.item.value1);
            $("#added_by").val(ui.item.id);
            return false
        }});
    var grid = jQuery("#list_case"), emptyMsgDiv = $('<div class="no-record-msz">No Record Found</div>');
    $(
            "#list_case").jqGrid({mtype: "POST", url: "/donation-bulk-entries", datatype: "json", sortable: true, colNames: ["", "Batch #",
            "Total Amount", "Actions"], colModel: [{name: "Batch Id", index: "batch_id", hidden: true}, {name: "Batch #",
                index: "batch_no"}, {name: "Total Amount", index: "amount", align: "center"}, {name: "Action", sortable: false, cmTemplate: {title: false}}],
        viewrecords: true, sortname: "modified_date", sortorder: "desc", rowNum: 10, rowList: [10, 20, 30], pager: "#listcaserud", autowidth:
                true, shrinkToFit: true, caption: "", width: "100%", cmTemplate: {title: false}, loadComplete: function() {
            hideAjxLoader();
            var count
                    = grid.getGridParam(), ts = grid[0];
            if (ts.p.reccount === 0) {
                grid.hide();
                emptyMsgDiv.show();
                $(
                        "#listcaserud_right div.ui-paging-info").css("display", "none")
            } else {
                grid.show();
                emptyMsgDiv.hide();
                $(
                        "#listcaserud_right div.ui-paging-info").css("display", "block")
            }
        }});
    $("#searchbutton").click(function() {
        showAjxLoader();
        $("#search_msg").hide();
        $("#success_message").hide();
        jQuery("#list_case").jqGrid("setGridParam", {postData: {searchString:
                        $("#bulk_donation").serialize()}});
        jQuery("#list_case").trigger("reloadGrid", [{page: 1}]);
        window.location.hash =
                "#gview_list";
        hideAjxLoader();
        return false
    });
    emptyMsgDiv.insertAfter(grid.parent());
    emptyMsgDiv.hide();
    $("#list_case").
            jqGrid("navGrid", "#listcaserud", {reload: true, edit: false, add: false, search: false, del: false});
    $("#list_case").jqGrid(
            "navButtonAdd", "#listcaserud", {caption: "", title: "Export", id: "exportExcel", onClickButton: function() {
            exportExcel(
                    "list_case", "/donation-bulk-entries")
        }, position: "last"})
});
function showDate(id, divId) {
    if ($("#" + id + " option:selected").
            text() == "Choose Date Range")
        $("#" + divId).show();
    else
        $("#" + divId).hide()
}