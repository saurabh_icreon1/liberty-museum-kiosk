$(function(){
    if($("#transaction_id").val()!="")$.ajax({
        url:"/get-transaction-products",
        method:"POST",
        dataType:"html",
        data:{
            transaction_id:$("#transaction_id").val()
            },
        success:function(responseData){
            $("#product_content").html(responseData)
        }
    });
$("#transaction_id").autocomplete({
    afterAdd:true,
    selectFirst:true,
    autoFocus:true,
    source:function(request,response){
        $
        ("#product_content").html("");
        $("#savebutton").hide();
        $("#cancelbutton").hide();
        $("#transaction_id").addClass(
            "auto-suggest-loading");
        $.ajax({
            url:"/get-transaction-id",
            method:"POST",
            dataType:"json",
            data:{
                transaction_id:$(
                    "#transaction_id").val()
                },
            success:function(jsonResult){
                $("#transaction_id").removeClass("auto-suggest-loading");
                var 
                resultset=[];
                $.each(jsonResult,function(){
                    var id=this.transaction_id;
                    resultset.push({
                        value:this.transaction_id
                        })
                    });
                response(resultset)
                }
            })
    },
open:function(){
    $(".ui-menu").width(350)
    },
change:function(event,ui){
    if(ui.item)$(
        "#transaction_id").val(ui.item.value);else $("#transaction_id").val("")
        },
select:function(event,ui){
    if(ui.item.value!=
        NO_RECORD_FOUND)$("#transaction_id").val(ui.item.value);
    else{
        $("#transaction_id").val("");
        return false
        }
        $.ajax({
        url:
        "/get-transaction-products",
        method:"POST",
        dataType:"html",
        data:{
            transaction_id:$("#transaction_id").val()
            },
        success:
        function(responseData){
            $("#product_content").html(responseData);
            $("#savebutton").show();
            $("#cancelbutton").show()
            }
        });
return false
}
});
$("#create_rma").validate({
    submitHandler:function(){
        var dataStr=$("#create_rma").serialize();
        if(
            checkCustomValidation("create_rma"))$.ajax({
            type:"POST",
            url:"/create-rma",
            data:dataStr,
            success:function(responseData){
                if(!checkUserAuthenticationAjx(responseData))return false;
                var jsonObj=jQuery.parseJSON(responseData);
                if(jsonObj.status==
                    "success")window.location="/crm-rma";else $.each(jsonObj.message,function(i,msg){
                    $("#"+i).after(
                        '<label class="error" style="display:block;">'+msg+"</label>")
                    })
                }
                })
        }
    })
});
function checkCustomValidation(id){
    var flagQty=
    true,flagRsn=true,flagCon=true,pass=false;
    var atleastOneFlag = false;        
    $('#create_rma [name="returned_qty[]"]').each(function(index){
    
        var str=$(this).
        val(),regx=/^[0-9]+$/i;
        if(str != 0 && str!='')
        {
            atleastOneFlag = true;
        }
        $(this).next().remove("span");
        if(str.match(regx)){
            if(str.length>0)pass=true;
            if(str.length>10&&str
                !=""){
                if($(this).next("span").length==0)$(this).after(
                    '<span class="error" for="returned_qty[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'
                    +ERROR_QTY+"</span>");
                flagQty=false
                }else if($(this).parents("td").prev("td").children("div").html()<str){
                $(this).after(
                    '<span class="error" for="returned_qty[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'
                    +ERROR_QTY+"</span>");
                flagQty=false
                }else $(this).next().remove("span")
                }else if(!str.match(regx)&&str.length>0){
            $(this).
            after(
                '<span class="error" for="returned_qty[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'
                +ERROR_QTY+"</span>");
            flagQty=false
            }
        });
	if(atleastOneFlag == false) {   
		$("#returned_qty").after('<span class="error" for="returned_qty[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'
                +ERROR_QTY+"</span>");
			return false;        
	}        

$('#create_rma [name="returned_reason[]"]').each(function(index){
    var str=$(this).
    val(),regx=/^[a-zA-z0-9.]+$/i;
    $(this).next().remove("span");
    if(str.match(regx))if(str.length==1){
        if($(this).next("span")
            .length==0)$(this).after(
            '<span class="error" for="returned_reason[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'
            +ERROR_REASON+"</span>");
        flagRsn=false
        }else if(str.length>50&&str!=""){
        if($(this).next("span").length==0)$(this).after(
            '<span class="error" for="returned_reason[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'
            +ERROR_REASON+"</span>");
        flagRsn=false
        }else $(this).next().remove("span");
    else if(str.length==0&&$(this).parents("div").
        parents("td").prev("td").children("div").children("input").val()>0){
        $(this).after(
            '<span class="error" for="returned_reason[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'
            +ERROR_REASON+"</span>");
        flagRsn=false
        }
    });
$('#create_rma [name="item_condition[]"]').each(function(index){
    var str=$(this
        ).val();
    $(this).next().remove("span");
    if(str.length==0&&$(this).parents("div").parents("td").prev("td").prev("td").
        children("div").children("input").val()>0){
        $(this).after(
            '<span class="error" for="item_condition[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'
            +ERROR_CONDITION+"</span>");
        flagCon=false
        }
    });
if(flagQty&&flagRsn&&flagCon){
    $(".detail-section").css("display","block");
    return pass
    }else return false
    }