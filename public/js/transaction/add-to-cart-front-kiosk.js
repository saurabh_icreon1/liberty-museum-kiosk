$(document).ready(function(){
    $('#step1').hide();

    jQuery.validator.addMethod("letterOnlyHyphen", function(value, element) {
        return this.optional(element) || /^[a-z .-]+$/i.test(value);
    },"Please enter valid details.");
    var oldCity="";
    getAddressInfoFront($("input:radio[name=shipping_existing_contact]:checked")
        .val(),"shipping","");
    getAddressInfoFront($("input:radio[name=billing_existing_contact]:checked").val(),"billing","");
    $(
        document).on("focus","#other_amount",function(){
        var value=$(this).val();
        if(value=="Amount")$(this).val("")
    });
    $(document)
    .on("focus","input[id^='firstname_']",function(){
        var value=$(this).val();
        if(value=="First Name")$(this).val("")
    });
    $(
        document).on("focus","input[id^='lastname_']",function(){
        var value=$(this).val();
        if(value=="Last Name")$(this).val("")
    })
    ;
    $(document).on("focus","input[id^='email_']",function(){
        var value=$(this).val();
        if(value=="Email")$(this).val("")
    });
    $(
        document).on("focusout","#other_amount",function(){
        var value=$(this).val();
        if(value=="")$(this).val("Amount")
    });
    $(document).on("focusout","input[id^='firstname_']",function(){
        var value=$(this).val();
        if(value=="")$(this).val("First Name")
    });
    $(document).on("focusout","input[id^='lastname_']",function(){
        var value=$(this).val();
        if(value=="")$(
            this).val("Last Name")
    });
    $(document).on("focusout","input[id^='email_']",function(){
        var value=$(this).val();
        if(value==""
            )$(this).val("Email")
    });
    $("#program_id").change(function(){
        var program_id=$(this).val();
        $.ajax({
            type:"POST",
            data:{
                program_id:program_id
            },
            beforeSend:function(){
                authenticateUserBeforeAjax()
            },
            url:"/get-campaign-program",
            success:function(
                responseData){
                $("#campaign_html").html(responseData)
            }
        })
    });
    $("#amount").change(function(){
        if($("#amount").val()=="other")
            $("#other_amount").show();else $("#other_amount").hide()
    });
    $("#yes").click(function(){
        $("#company_block").show()
    });
    $(
        "#no").click(function(){
        $("#company_block").hide()
    });
    $(document).on("change",".matchinggift",function(){
        if($(this).val()
            ==1)$("#company_block").show();else $("#company_block").hide()
    });
    $("input[name='in_honoree']").click(function(){
        if($(
            this).val()==3)$("#honoree_name_block").hide();else $("#honoree_name_block").show()
    });
    $(document).on("change",
        "#is_notify",function(){
            if($(this).is(":checked"))$("#notify_block").show();else $("#notify_block").hide()
        });
    $(
        "#shipping_country").change(function(){
        $(".error:contains('Your shipping address is invalid, Please enter valid address.')").hide();
        $("#ship_method_error").val(0);
    // displayshipmethod();
    // updatecarttotal()
    });
    $("#pick_up").click(function(){
        var status=$(this).is(":checked");
        if(status){
            $(
                "#old_shipping_address_div,#ship_method_radio").hide();
            $('.ship_to_add_option input[type="text"]').val("");
            $(
                ".ship_to_add_option input:radio").prop("checked",false);
            $("#shipping_existing_contact").val("");
            $("#is_apo_po_record").
            prop("checked",false);
            $("#is_apo_po_record").change()
        }else $(".ship_to_add_option").show()
    });
    $(
        "#save_new_shipping_address").click(function(){
        var validator=$("#addNewShippingFrm").validate({
            ignore:".ignore",
            submitHandler:function(){
                authenticateUserBeforeAjax();
                //showAjxLoader();sssssssssss
                $("#save_new_shipping_address").hide();
                $('#step1').hide();
                $("#formErrorMsg").hide();
                if($(
                    "#checkout_form [name='addressinfo_location']").val()==3)var addressinfo_location=3;
                if($(
                    "#checkout_form [name='addressinfo_location']").val()==2)var addressinfo_location=2;
                $.ajax({
                    type:"POST",
                    url:
                    "/save-user-shipping-address",
                    data:$("#addNewShippingFrm").serialize(),
                    success:function(responseData){
                        var jsonObj=jQuery
                        .parseJSON(responseData);
                        if(jsonObj.status=="success"){
                            hideAjxLoader();
                            var shipping_address_information_id="";
                            if($(
                                "#shipping_address_information_id").val()!="")shipping_address_information_id="/"+$("#shipping_address_information_id").
                                val();else $("#addNewShippingFrm [name='addressinfo_location']").val("3");
                            cancelNewShippingAddress();
                            $.ajax({
                                type:"POST"
                                ,
                                url:"/user-shipping-address/"+$("#user_id").val()+"/"+addressinfo_location+shipping_address_information_id,
                                success:
                                function(responseData){
                                    $("#shipping_address_list").html(responseData);
                                    window.location.hash="#userAddress";
                                    $(
                                        "#saveAddressConfirmationMsg").html("Address is saved successfully.");
                                    $("#saveAddressConfirmationMsg").show();
                                    $("#billing_address_information_id").val($("input:radio[name=billing_existing_contact]:checked").val());
                                    
                                    getAddressInfoFront($("input:radio[name=shipping_existing_contact]:checked").val(),"shipping","");
                                    
                                    $("#formErrorMsg").html("")
                                    
                                    
                                    $('#processingDiv').show();
                                    $("#save_new_shipping_address").show();
    
                                }
                            })
                        }
                    }
                })
            },
            rules:{
                shipping_title:{
                    required:false
                },
                shipping_first_name:{
                    required:true,
                    minlength:3,
                    maxlength:50,
                    //letterOnly:true
                },
                shipping_last_name:{
                    required:true,
                    minlength:3,
                    maxlength:50,
                    //letterOnly:true
                },
                shipping_address_1:{
                    required:true,
                    maxlength:50 //,
                //  alphanumericspecialchar:true
                },
                shipping_address_2:{
                    maxlength:50 //,
                // alphanumericspecialchar:true
                },
                shipping_country:{
                    required:true
                },
                shipping_city:{
                    required:true,
                    maxlength:30,
                    minlength:2,
                    letterOnlyHyphen:true
                },
                shipping_state_id:{
                    required:function(){
                        if(($("#is_apo_po_new").val()==1||$(
                            "#is_apo_po_new").val()==2)&&$("#addNewShippingFrm [name='shipping_country']").val()==228)return true;else return false
                    }
                },
                shipping_state:{
                    /*required:false,*/ /*function(){
            if($("#is_apo_po_new").val()==1)if($(
                "#addNewShippingFrm [name='shipping_country']").val()!=228)return true;else return false;else return false
                },*/
                    minlength:2,
                    maxlength:25,
                    alpha:true
                },
                apo_po_state:{
                    required:function(){
                        if($("#is_apo_po").val()==3)return true;else return false
                    }
                },
                shipping_zip_code:{
                    required:true,
                    zipcodevalidation:true,
                    maxlength:12,
                    minlength:4
                },
                shipping_country_code:{
                    required:true,
                    phoneno:true,
                    minlength:1,
                    maxlength:5
                },
                shipping_area_code:{
                    required:true,
                    number:true,
                    minlength:3,
                    maxlength:5
                },
                shipping_phone:{
                    required
                    :true,
                    number:true,
                    minlength:7,
                    maxlength:10
                }
            },
            messages:{
                shipping_title:{
                    required:"Please enter title"
                },
                shipping_first_name:{
                    required:"Please enter value"
                },
                shipping_last_name:{
                    required:"Please enter value"
                },
                shipping_address_1:{
                    required:SHIPP_ADDRESS
                },
                shipping_country:{
                    required:SHIPP_COUNTRY
                },
                shipping_city:{
                    required:SHIPP_CITY
                },
                shipping_state_id:{
                    required:"Select Shipping State"
                },
                shipping_state:{
                //required:SHIPP_STATE
                },
                apo_po_state:{
                    required:
                    "Select Shipping State"
                },
                shipping_zip_code:{
                    required:SHIPP_ZIP
                },
                shipping_country_code:{
                    required:
                    SHIPPING_COUNTRY_CODE_EMPTY,
                    phoneno:PHONE_DIGITS,
                    minlength:PHONE_DIGITS_COUNTRY_CODE_MIN,
                    maxlength:
                    PHONE_DIGITS_COUNTRY_CODE_MAX
                },
                shipping_area_code:{
                    required:SHIPPING_AREA_CODE_EMPTY,
                    number:PHONE_DIGITS,
                    minlength:
                    PHONE_DIGITS_AREA_CODE_MIN,
                    maxlength:PHONE_DIGITS_AREA_CODE_MAX
                },
                shipping_phone:{
                    required:SHIPPING_PHONE_NUMBER_EMPTY,
                    number:"Please enter numbers only. No Spaces or Dashes",
                    minlength:PHONE_DIGITS_PHONE_NUMBER_MIN,
                    maxlength:
                    PHONE_DIGITS_PHONE_NUMBER_MAX
                }
            }
        })
    });
    $("#save_new_billing_address").click(function(){
        var validator=$("#secondstep").
        validate({
            ignore:".ignore",
            submitHandler:function(){
                authenticateUserBeforeAjax();
                //showAjxLoader();
                $('#processingDiv2').show();
                $("#save_new_billing_address").hide();
                $('#step2').hide();                
                $.ajax({
                    type:"POST",
                    url
                    :"/save-user-billing-address",
                    data:$("#secondstep").serialize(),
                    success:function(responseData){
                        var jsonObj=jQuery.
                        parseJSON(responseData);
                        if(jsonObj.status=="success"){
                            hideAjxLoader();
                            cancelNewBillingAddress();
                            var def_id;
                            if($(
                                "#billing_address_information_id").val()!="")def_id="/"+$("#billing_address_information_id").val();
                            else{
                                def_id="";
                                $(
                                    "#billing_addressinfo_location").val("2")
                            }
                            $.ajax({
                                type:"POST",
                                url:"/user-shipping-address/"+$("#user_id").val()+"/"+$(
                                    "#billing_addressinfo_location").val()+def_id,
                                success:function(responseData){
                                    // n - start
                                    $('label[for="baermsg1"]').remove();
                                    
                                    //if($('label[for="baermsg2"])'))
                                    //   $('label[for="baermsg2"])').remove();
                                    // n - end
                                    
                                    $("#billing_address_list").html(responseData);
                                    $("#saveAddressConfirmationMsg").html("Address is saved successfully.");
                                    $("#billing_address_information_id").val($("input:radio[name=billing_existing_contact]:checked").val());
                                    $("#saveAddressConfirmationMsg").show();
                                    $('#processingDiv2').hide();
                                    $('#step2').show();
                                    $("#save_new_billing_address").show();
                                    
                                }
                            })
                        }
                    }
                })
            },
            rules:{
                billing_title:{
                    required:false
                },
                billing_first_name:{
                    required:true,
                    minlength:3,
                    maxlength:50,
                    //letterOnly:true
                },
                billing_last_name:{
                    required:true,
                    minlength:3,
                    maxlength:50,
                    //letterOnly:true
                },
                billing_address_1:{
                    required:
                    true,
                    maxlength:50 //,
                // alphanumericspecialchar:true
                },
                billing_address_2:{
                    maxlength:50 //,
                // alphanumericspecialchar:true
                },
                billing_country:{
                    required:true
                },
                billing_city:{
                    required:true,
                    maxlength:30
                    ,
                    minlength:2,
                    //letterOnlyHyphen:true
                },
                billing_state_id:{
                    required:function(){
                        if($("#secondstep [name='billing_country']").val()==
                            228)return true;else return false
                    }
                },
                billing_state:{
                    /*required:false,*/ 
                    /*function(){
        if($("#secondstep [name='billing_country']").val(
            )!=228)return true;else return false
            },*/
                    minlength:2,
                    maxlength:25,
                    //alpha:true
                },
                billing_zip_code:{
                    required:true,
                    zipcodevalidation:true,
                    maxlength:12,
                    minlength:4
                },
                billing_country_code:{
                    required:false,
                    phoneno:true,
                    minlength:1,
                    maxlength:5
                },
                billing_area_code:{
                    required:false,
                    number:true,
                    minlength:1,
                    maxlength:5
                },
                billing_phone:{
                    required:false,
                    number:true,
                    minlength:1,
                    maxlength:10
                }
            }
            ,
            messages:{
                billing_title:{
                    required:"Please select title"
                },
                billing_first_name:{
                    required:"Enter first name"
                },
                billing_last_name:{
                    required:"Enter last name"
                },
                billing_address_1:{
                    required:"Enter billing address"
                },
                billing_country:{
                    required:SHIPP_COUNTRY
                },
                billing_city:{
                    required:"Enter billing city"
                },
                billing_state:{
                //required:"Enter billing state"
                },
                billing_state_id:{
                    required:"select billing state"
                },
                billing_zip_code:{
                    required:"Enter billing postal code"
                },
                billing_country_code:{
                    required:SHIPPING_COUNTRY_CODE_EMPTY,
                    phoneno:PHONE_DIGITS,
                    minlength:PHONE_DIGITS_COUNTRY_CODE_MIN,
                    maxlength:PHONE_DIGITS_COUNTRY_CODE_MAX
                },
                billing_area_code:{
                    required:SHIPPING_AREA_CODE_EMPTY,
                    number:PHONE_DIGITS,
                    minlength:PHONE_DIGITS_AREA_CODE_MIN,
                    maxlength:PHONE_DIGITS_AREA_CODE_MAX
                },
                billing_phone:{
                    required:
                    SHIPPING_PHONE_NUMBER_EMPTY,
                    number:"Please enter numbers only. No Spaces or Dashes",
                    minlength:
                    PHONE_DIGITS_PHONE_NUMBER_MIN,
                    maxlength:PHONE_DIGITS_PHONE_NUMBER_MAX
                }
            }
        })
    })
});
var totalNotify=typeof totalNotify!=
"undefined"?totalNotify:1;
function deleteMoreDonationNotify(id){
    $("#notify_donation_"+id).remove();
    var lastBlock=$(
        "div[id^='notify_donation_']").last();
    if($(lastBlock).find(".plus").length==0)$(lastBlock).append(
        '<a class="plus plus m-l-5" href="javascript:void(0);" onclick="addMoreDonationNotify(this)" id="addMoreDonationNotify"></a>'
        );
    var allDivBlock=$("div[id^='notify_donation_']");
    if(allDivBlock.length==1)$(lastBlock).find(".minus").remove()
}
function showHonoreeName(currRef){
    if($(currRef).val()==3)$("#honoree_name_block").hide();else $("#honoree_name_block").
        show()
}
function showbilloption(currRef){
    if($(currRef).val()==1){
        $("#billing_existing_contact").val("");
        $(
            ".new-address-billing").hide();
        $(".select-bill-address").show()
    }else{
        $("#billing_existing_contact").val("");
        $(
            "#billing_title,#billing_first_name,#billing_last_name,#billing_address_1,#billing_address_2,#billing_city,#billing_country,#billing_state,#billing_zip_code,#billing_location_type"
            ).val("");
        $(".new-address-billing").show();
        $(".select-bill-address").hide()
    }
}
function showshipoption(currRef){
    if($(
        currRef).val()==1){
        $("#shipping_existing_contact").val("");
        $(".new-address").hide();
        $(".select-address").show()
    }else{
        $(
            "#shipping_existing_contact").val("");
        $(
            "#shipping_title,#shipping_first_name,#shipping_last_name,#shipping_address_1,#shipping_address_2,#shipping_country,#shipping_city,#shipping_state,#shipping_zip_code,#shipping_location_type"
            ).val("");
        $(".new-address").show();
        $(".select-address").hide()
    }
}
function showcreditcardoption(currRef){
    if($(currRef).val
        ()==1){
        $("#credit_card_exist").val("");
        $(".new-credit-card").hide();
        $(".select-existing-creditcard").show()
    }else{
        $(
            ".new-credit-card").show();
        $(".select-existing-creditcard").hide();
        $("#credit_card_exist").val("")
    }
}
function changeApoPo
(shippingAddressType){
    $("#checkout_form [name='is_apo_po']").val(shippingAddressType);
    $("#is_apo_po_new").val(
        shippingAddressType);
    if(shippingAddressType==1){
        $("#country_block").show();
        $("#country_block_old").show();
        $(
            "#apo_po_country_block").hide();
        $("#apo_po_country_block_old").hide();
        if($("#shipping_country").val()==228){
            $(
                "#shipping_text_state_block").hide();
            $("#shipping_select_state_block").show();
            $("#shipping_select_state_block_old").show
            ()
        }else{
            $("#shipping_text_state_block").show();
            $("#shipping_select_state_block").show();
            $(
                "#shipping_select_state_block_old").hide()
                
            $("#usa_state_shipping").hide();   
            $("#other_state_shipping").show();   
                
        }
        $("#shipping_apo_po_state_block").hide();
        $("#shipping_apo_po_state_block_old"
            ).hide();
        if($("#shipping_address_information_id").val()=="")$("#checkout_form [name='shipping_city']").val("");else $(
            "#addNewShippingFrm [name='shipping_city']").val(oldCity);
        $("#checkout_form [name='shipping_city']").removeAttr(
            "readonly");
        if($("#shipping_address_information_id").val()=="")$("#addNewShippingFrm [name='shipping_city']").val("");
        $(
            "#addNewShippingFrm [name='shipping_city']").removeAttr("readonly")
    }else{
        $("#country_block").hide();
        $(
            "#country_block_old").hide();
        $("#apo_po_country_block").show();
        $("#apo_po_country_block_old").show();
        if($(
            "#shipping_address_information_id").val()=="")$("#checkout_form [name='shipping_city']").val("");
        if($(
            "#shipping_address_information_id").val()=="")$("#addNewShippingFrm [name='shipping_city']").val("");
        $(
            "#shipping_text_state_block").hide();
        if(shippingAddressType==3){
            $("#checkout_form [name='shipping_city']").val("APO");
            $(
                "#checkout_form [name='shipping_city']").attr("readonly","readonly");
            $("#addNewShippingFrm [name='shipping_city']").val(
                "APO");
            $("#addNewShippingFrm [name='shipping_city']").attr("readonly","readonly");
            $("#shipping_select_state_block").hide
            ();
            $("#shipping_select_state_block_old").hide();
            $("#shipping_apo_po_state_block").show();
            $(
                "#shipping_apo_po_state_block_old").show()
        }else{
            $("#checkout_form [name='shipping_city']").removeAttr("readonly");
            $(
                "#addNewShippingFrm [name='shipping_city']").removeAttr("readonly");
            $("#addNewShippingFrm [name='shipping_city']").val(
                oldCity);
            $("#shipping_select_state_block").show();
            $("#shipping_select_state_block_old").show();
            $(
                "#shipping_apo_po_state_block").hide();
            $("#shipping_apo_po_state_block_old").hide();
            $("#other_state_shipping").hide();
            $(
                "#usa_state_shipping").show()
        }
    }
    $(".error:contains('Your shipping address is invalid, Please enter valid address.')").
    hide();
    $("#ship_method_error").val(0);
    //displayshipmethod();
    //updatecarttotal()
}
function displayshipmethod(){
    $.ajax({
        url:
        "/shipping-method-front",
        method:"POST",
        dataType:"html",
        async:false,
		beforeSend:
        function(){
            $('#step1').hide();
            $('#processingDiv').show();                      
        },
        data:$("#checkout_form").
        serialize(),
        success:function(responseData){
            $(".ship_method_class").html(responseData)
        },
        complete:function(jqXHR,
            textStatus){}
    })
}
function editShippingAddress(address_information_id,type,act){
    addNewShippingAddress();
    // $("#shipping_existing_contact_"+address_information_id).click();
    //document.getElementById("shipping_existing_contact_"+address_information_id).checked=true;
    
    getAddressInfoFront(address_information_id,type,act)
}
function editBillingAddress(address_information_id,type,act){
    addNewBillingAddress();
    getAddressInfoFront(address_information_id,type,act)
}


function getAddressInfoFront(
    address_information_id,type,act){
    $("#ship_to_add_option_error").hide();
    $("#shipping_method_error_msg").hide();
    $('#step1').hide();
    $('#processingDiv').show();
    if($.trim(
        address_information_id)!=""){
        $('#processingAddNewAddressDiv').show();
        $("#save_new_shipping_address").hide();        
        //showAjxLoader();
        $.ajax({
            url:"/get-billing-shipping-address-info-front",
            method:"POST",
            dataType:"json",
            data:{
                address_information_id:address_information_id,
                user_id:userId,
                async:false
            },
            success:function(
                responseData){
                if(act=="edit"){
                    //if(type=="shipping")document.getElementById("shipping_existing_contact_"+
                    //    address_information_id).checked=true;
                    // if(type=="billing")document.getElementById("billing_existing_contact_"+
                    //    address_information_id).checked=true;
                    $("#"+type+"_new_address").show()
                }else $("#"+type+"_new_address").hide();
                //hideAjxLoader();
                authenticateUserBeforeAjax();
                var jsonObj=responseData;
                if(act=="edit")if(type=="billing"){
                    if(jsonObj.
                        user_type==1){
                        $("#"+type+"_first_name").val(jsonObj.first_name);
                        $("#"+type+"_last_name").val(jsonObj.last_name);
                        $("#"+type+"_title").val(jsonObj.title);
                    //$("#"+type+"_middle_name").val(jsonObj.middle_name)
                        
                    }else{
                        $(
                            "#"+type+"_first_name").val(jsonObj.first_name);
                        $("#"+type+"_last_name").val(jsonObj.last_name)
                    }
                    $("#"+type+
                        "_address_1").val(jsonObj.address_street_address_one);
                    $("#"+type+"_address_2").val(jsonObj.address_street_address_two);
                    $
                    ("#"+type+"_city").val(jsonObj.address_city);
                    $("#"+type+"_state").val(jsonObj.address_state);
                    $("#"+type+"_zip_code").val
                    (jsonObj.address_zip_code);
                    $("#"+type+"_country").val(jsonObj.address_country_id);
                    $("#"+type+"_location_type").val(
                        jsonObj.location_name);
                    $("#"+type+"_country_code").val(jsonObj.country_code);
                    $("#"+type+"_area_code").val(jsonObj.
                        area_code);
                    $("#"+type+"_phone").val(jsonObj.phone_num);
                    $("#"+type+"_state_id").val(jsonObj.address_state);
                    $("#"+type+
                        "_address_information_id").val(jsonObj.address_information_id);
                    $("#billing_addressinfo_location").val(jsonObj.location);
                    if(jsonObj.address_country_id==228){
                        $("#usa_state_"+type).show();
                        $("#other_state_"+type).hide();
                        $("#"+type+
                            "_state_id option:contains("+jsonObj.address_state+")").attr("selected",true)
                    }else{
                        $("#usa_state_"+type).hide();
                        $(
                            "#other_state_"+type).show()
                    }
                }else{
                    oldCity=jsonObj.address_city;
                    var $radios=$("input:radio[name=is_apo_po_record]");
                    $radios.filter("[value="+jsonObj.address_type+"]").click();
                    if(jsonObj.user_type==1){
                        $("#addNewShippingFrm [name='"+type+"_first_name']").val(jsonObj.first_name);
                        $("#addNewShippingFrm [name='"+type+"_title']").val(jsonObj.title);
                        $("#addNewShippingFrm [name='"+type+"_last_name']").val(jsonObj.last_name)
                    //$("#addNewShippingFrm [name='"+type+"_middle_name']").val(jsonObj.middle_name)
                    }else

                    {
                        $("#addNewShippingFrm [name='"+type+"_first_name']").val(jsonObj.first_name);
                        $("#addNewShippingFrm [name='"+type+"_last_name']").val(jsonObj.last_name)
                        
                    //if(jsonObj.middle_name)
                    //  $("#addNewShippingFrm [name='"+type+"_middle_name']").val(jsonObj.middle_name)
                    }
                    $("#addNewShippingFrm [name='"+type+"_address_1']").val(jsonObj.
                        street_address_one);
                    $("#addNewShippingFrm [name='"+type+"_address_2']").val(jsonObj.street_address_two);
                    $(
                        "#addNewShippingFrm [name='"+type+"_city']").val(jsonObj.address_city);
                    $("#addNewShippingFrm [name='"+type+"_zip_code']"
                        ).val(jsonObj.address_zip_code);
                    $("#addNewShippingFrm [name='"+type+"_location_type']").val(jsonObj.location_name);
                    $(
                        "#addNewShippingFrm [name='"+type+"_country_code']").val(jsonObj.country_code);
                    $("#addNewShippingFrm [name='"+type+
                        "_area_code']").val(jsonObj.area_code);
                    $("#addNewShippingFrm [name='"+type+"_phone']").val(jsonObj.phone_num);
                    $(
                        "#addNewShippingFrm [name='"+type+"_state_id']").val(jsonObj.address_state);
                    $(
                        "#addNewShippingFrm [name='addressinfo_location']").val(jsonObj.location);
                    $("#addNewShippingFrm [name='"+type+
                        "_address_information_id']").val(jsonObj.address_information_id);
                    if(jsonObj.address_country_id==228 || jsonObj.address_country_id==''){
                        $("#usa_state_"+type).show();
                        $("#other_state_"+type).hide();
                        $("#addNewShippingFrm [name='usa_state_"+type+"']").show();
                        $(
                            "#addNewShippingFrm [name='other_state_"+type+"']").hide();
                        $("#addNewShippingFrm #"+type+"_state_id option:contains("+
                            jsonObj.address_state+")").attr("selected",true)
                    }else{
                        $("#usa_state_"+type).hide();
                        $("#other_state_"+type).show();
                        $(
                            "#addNewShippingFrm [name='other_state_"+type+"']").show();
                        $("#addNewShippingFrm [name='usa_state_"+type+"']").hide();
                        $(
                            "#addNewShippingFrm [name='shipping_state']").val(jsonObj.address_state)
                    }
                    if(jsonObj.address_type==3){
                        $(
                            "#addNewShippingFrm [name='apo_po_state']").val(jsonObj.address_state);
                        $(
                            "#addNewShippingFrm [name='apo_po_shipping_country']").val(jsonObj.address_country_id)
                    }
                    if(jsonObj.address_type==2){
                        $(
                            "#addNewShippingFrm [name='shipping_state_id']").val(jsonObj.address_state);
                        $(
                            "#addNewShippingFrm [name='apo_po_shipping_country']").val(jsonObj.address_country_id)
                    }
                    if(jsonObj.address_type==1 || jsonObj.address_type==''){
                        $(
                            "#addNewShippingFrm [name='shipping_state_id']").val(jsonObj.address_state);
                        $(
                            "#addNewShippingFrm [name='shipping_country']").val(jsonObj.address_country_id)
                    }
                }else if(type=="billing"){
                    if(jsonObj.
                        user_type==1){
                        $("#"+type+"_first_name").val(jsonObj.first_name);
                        $("#"+type+"_title").val(jsonObj.title);
                        $("#"+type+"_last_name").val(jsonObj.last_name)
                    //$("#"+type+"_middle_name").val(jsonObj.middle_name)
                    }else{
                        $(
                            "#"+type+"_first_name").val(jsonObj.first_name);
                        $("#"+type+"_last_name").val(jsonObj.last_name)

                    }
                    $("#"+type+
                        "_address_1").val(jsonObj.address_street_address_one);
                    $("#"+type+"_address_2").val(jsonObj.address_street_address_two);
                    $
                    ("#"+type+"_city").val(jsonObj.address_city);
                    $("#"+type+"_state").val(jsonObj.address_state);
                    $("#"+type+"_zip_code").val
                    (jsonObj.address_zip_code);
                    $("#"+type+"_country").val(jsonObj.address_country_id);
                    $("#"+type+"_location_type").val(
                        jsonObj.location_name);
                    $("#"+type+"_country_code").val(jsonObj.country_code);
                    $("#"+type+"_area_code").val(jsonObj.
                        area_code);
                    $("#"+type+"_phone").val(jsonObj.phone_num);
                    $("#"+type+"_address_information_id").val(jsonObj.
                        address_information_id);
                    if(jsonObj.address_country_id==228){
                        $("#usa_state_"+type).show();
                        $("#other_state_"+type).hide();
                        $("#"+type+"_state_id option:contains("+jsonObj.address_state+")").attr("selected",true)
                    }else{
                        $("#usa_state_"+type).hide
                        ();
                        $("#other_state_"+type).show()
                    }
                }else{
                    if(jsonObj.user_type==1){
                        $("#checkout_form [name='"+type+"_first_name']").val(
                            jsonObj.first_name);
                        $("#checkout_form [name='"+type+"_last_name']").val(jsonObj.last_name)
                        $("#checkout_form [name='"+type+"_title']").val(jsonObj.title);
                    //$("#checkout_form [name='"+type+"_middle_name']").val(jsonObj.middle_name)
                    }else{
                        $(
                            "#checkout_form [name='"+type+"_first_name']").val(jsonObj.first_name);
                        $("#checkout_form [name='"+type+"_last_name']")
                        .val(jsonObj.last_name)
                        
                    //if(jsonObj.middle_name)
                    //  $("#checkout_form [name='"+type+"_middle_name']").val(jsonObj.middle_name)
                    }
                    $("#checkout_form [name='"+type+"_address_1']").val(jsonObj.street_address_one);
                    $(
                        "#checkout_form [name='"+type+"_address_2']").val(jsonObj.street_address_two);
                    $("#checkout_form [name='"+type+"_city']")
                    .val(jsonObj.address_city);
                    $("#checkout_form [name='"+type+"_state']").val(jsonObj.address_state);
                    $(
                        "#checkout_form [name='"+type+"_zip_code']").val(jsonObj.address_zip_code);
                    $("#checkout_form [name='"+type+"_country']")
                    .val(jsonObj.address_country_id);
                    $("#checkout_form [name='"+type+"_location_type']").val(jsonObj.location_name);
                    $(
                        "#checkout_form [name='"+type+"_country_code']").val(jsonObj.country_code);
                    $("#checkout_form [name='"+type+
                        "_area_code']").val(jsonObj.area_code);
                    $("#checkout_form [name='"+type+"_phone']").val(jsonObj.phone_num);
                    $(
                        "#checkout_form [name='"+type+"_state_id']").val(jsonObj.address_state);
                    $("#checkout_form [name='"+type+
                        "_address_information_id']").val(jsonObj.address_information_id);
                    if(jsonObj.address_country_id==228){
                        $("#usa_state_"+
                            type).show();
                        $("#other_state_"+type).hide();
                        $("#checkout_form [name='usa_state_"+type+"']").show();
                        $(
                            "#checkout_form [name='other_state_"+type+"']").hide();
                        $("#checkout_form #"+type+"_state_id option:contains("+jsonObj.
                            address_state+")").attr("selected",true)
                    }else{
                        $("#usa_state_"+type).hide();
                        $("#other_state_"+type).show();
                        $(
                            "#checkout_form [name='usa_state_"+type+"']").hide();
                        $("#checkout_form [name='other_state_"+type+"']").show()
                    }
                    if(jsonObj
                        .address_type==3){
                        $("#shipping_apo_po_state_block_old").show();
                        $("#apo_po_state").show();
                        $(
                            "#checkout_form [name='apo_po_state']").val(jsonObj.address_state);
                        $("#checkout_form [name='apo_po_shipping_country']").
                        val(jsonObj.address_country_id)
                    }
                    if(jsonObj.address_type==2){
                        $("#shipping_select_state_block_old").show();
                        $(
                            "#usa_state_shipping").show();
                        $("#shipping_state_id").show();
                        $("#checkout_form [name='shipping_state_id']").val(jsonObj.
                            address_state);
                        $("#checkout_form [name='apo_po_shipping_country']").val(jsonObj.address_country_id)
                    }
                    if(jsonObj.
                        address_type==1){
                        $("#checkout_form [name='shipping_state_id']").val(jsonObj.address_state);
                        $(
                            "#checkout_form [name='shipping_country']").val(jsonObj.address_country_id)
                    }
                }
                
                $('#processingAddNewAddressDiv').hide();
                $("#save_new_shipping_address").show();                
                
                if(type=="shipping"){
                    if(act!="edit"){
                        if($.trim(jsonObj.first_name) == '' || $.trim(jsonObj.last_name)=='' || $.trim(jsonObj.address_country_id)=='' || (jsonObj.address_country_id==228 && $.trim(jsonObj.address_state)=='') || $.trim(jsonObj.address_city) == '' || $.trim(jsonObj.address_zip_code)=='' || $.trim(jsonObj.phone_num)=='')
                        {
                            //$('#ship_method_id').hide();
                            $('#processingDiv').hide();
                            $('#step1').show();
                            //$('#formErrorMsg').show();
                            //$("#formErrorMsg").html("Please select valid address.");
                        }
                        else {
                            if(jsonObj.address_type!='1' && jsonObj.address_type!='2' && jsonObj.address_type!='3')
			       $('#is_apo_po').val('1');
			    else
			       $('#is_apo_po').val(jsonObj.address_type);
                            $('#ship_method_id').show();
                            $('#formErrorMsg').hide();
                            displayshipmethod();
                            //updatecarttotal()
                        }
                    }
                }
            }
        })
    }
}
function displayBlock(blocktype){
    if(blocktype=="ship"){
        $("#maincheckout").show();
        $("#step1div").
        show();
        $(".discount").show();
        $(".first,.second,.third,.fourth").removeClass("active");
        $(".first").addClass("active");
        $(
            ".steptext").html("SHIPPING INFORMATION");
        $("#orderreview").hide().html("");
        $("#step2div").hide();
        window.location.hash=
        "#checkout_section"
    }
    if(blocktype=="cc"){
        $("#maincheckout").show();
        $("#step2div").show();
        $("#step1div").hide();
        window.
        location.hash="#checkout_section";
        ".first,.second,.third,.fourth".removeClass("active");
        $(".second").addClass("active");
        $(".steptext").html("BILLING INFORMATION");
        $("#orderreview").hide().html("")
    }
    if(blocktype=="bill"){
        $("#maincheckout").
        show();
        $("#step2div").show();
        $("#step1div").hide();
        $("#orderreview").hide().html("");
        window.location.hash=
        "#checkout_section";
        ".first,.second,.third,.fourth".removeClass("active");
        $(".second").addClass("active");
        $(".steptext")
        .html("BILLING INFORMATION")
    }
}
function applycouponcode(){
    $('#step1').hide();
    $('#processingDiv').show();    
    $('#step2').hide();
    $('#processingDiv2').show();  
    $("#applycouponcodeDiv").hide();
    $("#applycouponcodeProcessing").show();  	    
    
    var couponcode=$("#couponcode").val();
    $.ajax({
        type:"POST",
        url:"/get-total-cart",
        data:$("#checkout_form").serialize()+"&shipcode="+$("#checkout_form [name='shipping_zip_code']").val()
        +"&userid="+userId+"&ship_m=1&source_type=frontend&shipping_company=&shipping_address="+$(
            "#checkout_form [name='shipping_address_1']").val()+"&status="+status+"&couponcode="+$("#couponcode").val(),
        beforeSend: function(){},
        success:function(responseData){
            var jsonObj=jQuery.parseJSON(responseData);
			
            if(jsonObj.status!=undefined){
				if(jsonObj.status == 'error'){
					$('#is_promotion_applied').val('');
				}else{
					$('#is_promotion_applied').val(1);
				}
				
                $(".applycouponcodeerror").removeClass("error");
                $(".applycouponcodeerror").html(jsonObj.message).show().addClass(jsonObj.status)
            }
            $(".itemtotal").html("$"+jsonObj.itemtotal);
            $(".shipping").html("$"+jsonObj.shipping);
            $(".totalaftershipping").html("$"+jsonObj.totalaftershipping);
            $(".estimatedtax").html("$"+jsonObj.estimatedtax);
            $(".finaltotal").html("ORDER TOTAL <span>$"+jsonObj.finaltotal+"</span>");
            $(".coupondiscount").html("$"+jsonObj.coupondiscount);
            if(jsonObj.free_shipping!=undefined&&jsonObj.free_shipping==true){}else{}
            $("#free_shipping").val(jsonObj.free_shipping);
            //hideAjxLoader();
            $('#step1').show();
            $('#processingDiv').hide();  
            $('#step2').show();
            $('#processingDiv2').hide();
            $("#applycouponcodeDiv").show();
            $("#applycouponcodeProcessing").hide();  	    		    	        
        }
    })
}
function updatecarttotal(status){
    $.ajax({
        type:"POST",
        url:"/get-total-cart",
        async:false,
        beforeSend:
        function(){
            $('#step1').hide();
            $('#processingDiv').show();                      
            authenticateUserBeforeAjax();
		    $("#placeorder").hide();
		    $("#placeOrderProcessingDiv").show(); 	            

        //showAjxLoader()
        },
        data:$("#checkout_form").serialize()+"&shipcode="+$(
            "#checkout_form [name='shipping_zip_code']").val()+"&userid="+userId+
        "&ship_m=1&source_type=frontend&shipping_company=&shipping_address="+$("#checkout_form [name='shipping_address_1']").val
        ()+"&status="+status+"&couponcode="+$("#couponcode").val()+"&shipping_method="+$(
            "input:radio[name=shipping_method]:checked").val(),
        success:function(responseData){
        
		    $("#placeorder").show();
		    $("#placeOrderProcessingDiv").hide(); 	                    
        
            if($("input[name='shipping_method']").
                last().next().attr("class")=="error")$("input[name='shipping_method']").last().next().remove();
            try{
                responseData=JSON.
                parse(responseData)
            }catch(e){
                responseData=responseData
            }
            if(typeof responseData=="object")if(responseData.status==
                "shipping_error"&&$.trim($("input:radio[name=shipping_method]:checked").val())!=""){
                updatecarttotal("error");
                $(
                    "#ship_method_error").val(1);
                $.each(responseData.message,function(i,msg){
                    var elemArr=$("input[name='"+i+"']");
                    $(elemArr)
                    .last().next("label").remove();
                    $(elemArr).last().after('<label class="error" style="display:block;">'+msg+"</label>")
                })
            }
            else{
                var jsonObj=responseData;
                $(".itemtotal").html("$"+jsonObj.itemtotal);
                $(".shipping").html("$"+jsonObj.shipping);
                $(
                    ".totalaftershipping").html("$"+jsonObj.totalaftershipping);
                $(".estimatedtax").html("$"+jsonObj.estimatedtax);
                $(
                    ".finaltotal").html("ORDER TOTAL <span>$"+jsonObj.finaltotal+"</span>");
                $(".coupondiscount").html("$"+jsonObj.
                    coupondiscount)
            }
            $('#processingDiv').hide();
            $('#step1').show();
            hideAjxLoader()
        }
    })
}
$(document).ready(function(){
    $("#shipping_country").change(function(){
        $("#shipping_state").val('');
        $("#shipping_state_id").val('');
    });
    $("#billing_country").change(function(){
        $("#billing_state").val('');
        $("#billing_state_id").val('');
    });    
    $(document).on("click",".shipmethod",function(){
    
        $('#step1').hide();
        $('#processingDiv').show();    
        $(".error:contains('Your shipping address is invalid, Please enter valid address.')").hide();
        $("#ship_method_error").val(0);
        updatecarttotal("")
    });
    
    jQuery.validator.addMethod("zipcodevalidation", function(value, element) {
        return this.optional(element) || /^[a-z0-9- ]+$/i.test(value);
    }, "Please enter valid details");
    
    jQuery.validator.addMethod("alphanumericspecialchar",function(value,
        element){
        return this.optional(element)||/^[A-Za-z0-9 ,.-]+$/i.test(value)
    },"Please enter valid details");
    jQuery.
    validator.addMethod("letterOnly",function(value,element){
        return this.optional(element)||/^[a-z '\-]+$/i.test(value)
    },
    "Please enter valid details");
    jQuery.validator.addMethod("phoneno",function(value,element){
        return this.optional(element)
        ||/^[0-9-+]+$/i.test(value)
    },"Please enter only alphabet.");
    jQuery.validator.addMethod("alpha",function(value,element){
        return this.optional(element)||/^[a-zA-Z ()\',.-]+$/i.test(value)},"Please enter alphabets only.");jQuery.validator.
    addMethod("creditcardexpiry",function(value,element){
        var form=element.form,expiry=form.credit_card_exp_year.value+form.
        credit_card_exp_month.value,date=new Date(),month=date.getMonth()+1,now=""+date.getFullYear()+(month<10?"0"+month:month)
        ;
        return expiry>now
    });
    $("#step1").click(function(){
		if($('#is_promotion_applied').val() == ''){
			$('#couponcode').val('');
		}
        $("#saveAddressConfirmationMsg").html("");
        $(
            "#ship_to_add_option_error").hide();
        $("#shipping_method_error_msg").hide();
        $("#ship_to_add_option_error").html("");
        if(
            totalproduct==isInventory||$("#pick_up").is(":checked")==true){
            $("form#checkout_form input[type=text]").addClass(
                "ignore");
            $("form#checkout_form input[type=select]").addClass("ignore");
            $("form#checkout_form input[type=radio]").
            addClass("ignore")
        }
        $("#ship_to_add_option_error").html("");
        if($(
            "input[type=radio][name=shipping_existing_contact]:checked").length>0){}else{
            $("#ship_to_add_option_error").show();
            $(
                "#shipping_method_error_msg").show();
            $("#formErrorMsg").hide();
            $("#ship_to_add_option_error").html(
                "<label class='error'>Please select at least one shipping address.</label>");
            return false
        }
        if($("#isCustomProductAdded").
            val()=="yes")if($("#checkout_form [name='shipping_country']").val()=="228"||$("#checkout_form [name='shipping_country']"
            ).val()=="39"){}else{
            $("#ship_to_add_option_error").show();
            $("#ship_to_add_option_error").html(
                '<label class="error">International shipment is not available for custom frame.</label>');
            hideAjxLoader();
            return false
        }
        var validator=$("#checkout_form").validate({
            ignore:".ignore",
            submitHandler:function(){
                $("#shipping_method_error_msg").
                html("");
                $("#shipping_method_error_msg").hide();
                if($("input[type=radio][name=shipping_method]:checked").length==0){
                    $(
                        "#shipping_method_error_msg").show();
                    $("#shipping_method_error_msg").html("Please select shipping method");
                    return false
                }
                showAjxLoader();
                if($("#ship_method_error").val()=="1"){
                    $(
                        ".error:contains('Your shipping address is invalid, Please enter valid address.')").show();
                    $("#formErrorMsg").show();
                    $(
                        "#formErrorMsg").html("Your shipping address is invalid, Please enter valid address.");
                    hideAjxLoader();
                    return false
                }
                $(
                    "#step1div").hide();
                $("#step2div").show();
                

                var status=$("#pick_up").is(":checked");
                if(status==true){
                    $("#old_credit_card")
                    .show();
                    $("#old_billing_address_div").show()
                }
                $(".first").removeClass("active");
                $(".second").addClass("active");
                $(
                    ".steptext").html("BILLING INFORMATION");
                hideAjxLoader();
                var couponcode=$("#couponcode").val();
                $.ajax({
                    type:"POST",
                    url:
                    "/get-total-cart",
                    data:$("#checkout_form").serialize()+"&shipcode="+$("#checkout_form [name='shipping_zip_code']").val()
                    +"&userid="+userId+"&ship_m=1&source_type=frontend&shipping_company=&shipping_address="+$(
                        "#checkout_form [name='shipping_address_1']").val()+"&status="+status+"&couponcode="+$("#couponcode").val()+
                    "&shipping_method="+$("input:radio[name=shipping_method]:checked").val(),
                    success:function(responseData){
                        var jsonObj=
                        jQuery.parseJSON(responseData);
                        if(jsonObj.status!=undefined){
                            $(".applycouponcodeerror").removeClass("error");
                            $(
                                ".applycouponcodeerror").html(jsonObj.message).show().addClass(jsonObj.status)
                        }
                        $(".itemtotal").html("$"+jsonObj.
                            itemtotal);
                        $(".shipping").html("$"+jsonObj.shipping);
                        $(".totalaftershipping").html("$"+jsonObj.totalaftershipping);
                        $(
                            ".estimatedtax").html("$"+jsonObj.estimatedtax);
                        $(".finaltotal").html("ORDER TOTAL <span>$"+jsonObj.finaltotal+"</span>"
                            );
                        $(".coupondiscount").html("$"+jsonObj.coupondiscount);
                        $(".discount").hide()
                    }
                })
            },
            rules:{
                shipping_title:{
                    required:false
                }
                ,
                ship_to_address:{
                    required:true
                },
                shipping_existing_contact:{
                    required:function(element){
                        return $(
                            'input[name="ship_to_address"]:checked').val()=="1"
                    }
                },
                shipping_first_name:{
                    required:true,
                    minlength:3,
                    maxlength:50,
                    //letterOnly:true
                },
                shipping_last_name:{
                    required:true,
                    minlength:3,
                    maxlength:50,
                    //letterOnly:true
                },
                shipping_address_1:{
                    required:true,
                    maxlength:50 //,
                //alphanumericspecialchar:true
                },
                shipping_country:{
                    required:false
                },
                shipping_city:{
                    required:true,
                    maxlength:30,
                    minlength:2,
                    //letterOnlyHyphen:true
                },
                shipping_state_id:{
                    required:false
                },
                shipping_state:{
                    // required:false,
                    minlength:2,
                    maxlength:25,
                    //alpha:true
                },
                apo_po_state:{
                    required:false
                },
                shipping_zip_code:{
                    required:true,
                    zipcodevalidation:true,
                    maxlength:12,
                    minlength:4
                },
                shipping_method:{
                    required:false
                },
                shipping_country_code:{
                    required:true,
                    phoneno:true,
                    minlength:1,
                    maxlength:5
                }
                ,
                shipping_area_code:{
                    required:true,
                    number:true,
                    minlength:1,
                    maxlength:5
                },
                shipping_phone:{
                    required:true,
                    number:true,
                    minlength:1,
                    maxlength:10
                }
            },
            messages:{
                shipping_title:{
                    required:"Please select title"
                },
                ship_to_address:{
                    required:
                    SELECT_SHIPP
                },
                shipping_existing_contact:{
                    required:SELECT_SHIPP_EXISTING
                },
                shipping_first_name:{
                    required:SHIPP_F_NAME
                },
                shipping_last_name:{
                    required:SHIPP_L_NAME
                },
                shipping_address_1:{
                    required:SHIPP_ADDRESS
                },
                shipping_country:{
                    required:
                    SHIPP_COUNTRY
                },
                shipping_city:{
                    required:SHIPP_CITY
                },
                shipping_state_id:{
                    required:"Select Shipping State"
                },
                shipping_state:{
                //required:SHIPP_STATE
                },
                apo_po_state:{
                    required:"Select Shipping State"
                },
                shipping_zip_code:{
                    required:SHIPP_ZIP
                },
                shipping_method:{
                    required:SELECT_SHIPP_METHOD
                },
                shipping_country_code:{
                    required:SHIPPING_COUNTRY_CODE_EMPTY,
                    phoneno:
                    PHONE_DIGITS,
                    minlength:PHONE_DIGITS_COUNTRY_CODE_MIN,
                    maxlength:PHONE_DIGITS_COUNTRY_CODE_MAX
                },
                shipping_area_code:{
                    required:SHIPPING_AREA_CODE_EMPTY,
                    number:PHONE_DIGITS,
                    minlength:PHONE_DIGITS_AREA_CODE_MIN,
                    maxlength:
                    PHONE_DIGITS_AREA_CODE_MAX
                },
                shipping_phone:{
                    required:SHIPPING_PHONE_NUMBER_EMPTY,
                    number:PHONE_DIGITS,
                    minlength:
                    PHONE_DIGITS_PHONE_NUMBER_MIN,
                    maxlength:PHONE_DIGITS_PHONE_NUMBER_MAX
                }
            },
            showErrors:function(errorMap,errorList){
                this.
                defaultShowErrors();
                var errors=this.numberOfInvalids();
                if(errors>0){
                    $("#formErrorMsg").show();
                    $("#formErrorMsg").html(
                        "Your address information is missing. Please complete your address information.");
                    editShippingAddress($(
                        "input:radio[name=shipping_existing_contact]:checked").val(),"shipping","edit")
                }
            }
        })
    });
    
    $("#same_as_shipping").click(function(){    
        if($("input[type=checkbox][name=same_as_shipping]").is(":checked") == true) 
            $("#billing_to_add_option_error").hide();
        else   
            $("#billing_to_add_option_error").show();
    });  
    
    $(document).on("click",
        ".shipmethod",function(){
            var val_checked=$(this).val();
            $('input[type=radio][name=shipping_method][value="'+val_checked+
                '"]').first().attr("checked","checked")
        });
    $("#step2").click(function(){
	    if($('#is_promotion_applied').val() == ''){
			$('#couponcode').val('');
		}
        $('#step2').hide();
        $('#processingDiv2').show();     
    
        $("#saveAddressConfirmationMsg").hide();
        if($(
            "input[type=radio][name=credit_card_exist]:checked").length>0){}else{
            $("#credit_card_error").html(
                "<label class='error'>Please select at least one credit card.</label>").show();
            $('#step2').show();
            $('#processingDiv2').hide();     
                
                
            return false
        }
        $(
            "#billing_to_add_option_error").html("");
        if($("input[type=radio][name=billing_existing_contact]:checked").length>0){}
        else{
        
            if($("input[type=checkbox][name=same_as_shipping]").is(":checked") == false) 
            {
                $("#billing_to_add_option_error").html("<label for='baermsg1' class='error mar-b-10'>Please select at least one billing address.</label>").show();
                $("#step2").show();
                $("#processingDiv2").hide();
                return false
            }
        }
        authenticateUserBeforeAjax();
        //showAjxLoader();
        var couponcode=$("#couponcode").val(),dataStr=$("#secondstep").serialize()+
        "&"+$("#checkout_form").serialize()+"&shipcode="+$("#checkout_form [name='shipping_zip_code']").val()+
        "&ship_m=1&source_type=frontend&shipping_company=&shipping_address="+$("#checkout_form [name='shipping_address_1']").val
        ()+"&shipping_country_text="+$("#checkout_form [name='shipping_country'] option:selected").text()+
        "&billing_country_text="+$("#billing_country option:selected").text()+"&couponcode="+couponcode+
        "&credit_card_exist_text="+$("input:radio[name=credit_card_exist]:checked").next("p").html()+"&shipping_method="+$(
            "input:radio[name=shipping_method]:checked").val();
        $.ajax({
            type:"POST",
            url:"/orderreview-cart-front",
            data:dataStr,
            success:function(responseData){
                hideAjxLoader();
                window.location.hash="#checkout_section";
                if(responseData==
                    "session_expired")authenticateUserBeforeAjax();
                $("#maincheckout").hide();
                $("#orderreview").html(responseData).show();
                $(
                    ".second").removeClass("active");
                $(".third").addClass("active");
                var checked_value=$(".shipmethod:checked").val();
                $(
                    'input[type=radio][name=radio_button][value="'+checked_value+'"]').first().attr("checked","checked")
            },
            complete:function(
                ){
                var radio_html=$(".ship_method_class").html(),ch=$("input:radio[name=shipping_method]:checked").val();
                $(
                    "#order_review_ship").html(radio_html);
                $("input[type=radio][value='"+ch+"']").prop("checked",true);
                
                $('#step2').show();
                $('#processingDiv2').hide();                     
                
            }
        })
    });
    $(
        "#add_new_credit_card").click(function(){
        $("#credit_card").validate({
            submitHandler:function(){
            
                if($("input[type=radio][name=billing_existing_contact]:checked").length>0){
                    var bill_address_id = $("input:radio[name=billing_existing_contact]:checked").val();
                }
                else{
                    if($("input[type=checkbox][name=same_as_shipping]").is(":checked") == false) 
                    {
                        $("#billing_select_address_error").html(
                            "<label for='baermsg2' class='error mar-b-10'>Please select at least one billing address.</label>").show();
                            $("#step2").show();
                            $("#processingDiv2").hide();
                        return false
                    }
                    else {
                        var bill_address_id = $("input:radio[name=shipping_existing_contact]:checked").val();
                    }   
                }        

                authenticateUserBeforeAjax();
            
                $("#credit_card_number").validateCreditCard(function(result){
                    if(result.card_type)$("#credit_card_type").val(result.card_type.name)
                });
                //showAjxLoader();
                $("#savecredit").hide();
                $("#processingDiv3").show();
                var couponcode=$("#couponcode").val(),dataStr=$("#credit_card").serialize()+"&billing_address_id="+bill_address_id;
                $.ajax({
                    type:"POST",
                    url:"/save-credit-cart-front",
                    data:dataStr,
                    success:function(responseData){
                        var jsonObj=jQuery.parseJSON(
                            responseData);
                        if(jsonObj.status=="success"){
                            cancelNewCard();
                            $.ajax({
                                type:"POST",
                                url:"/user-credit-card-listing/"+$(
                                    "#user_id").val(),
                                success:function(responseData){
                                    $("#credit_card_type").val("");
                                    $("#savecredit").show();
                                    $("#processingDiv3").hide();
                                    $("#credit-card-list").html(responseData);
                                    $("#saveAddressConfirmationMsg").removeClass("success_message");
                                    $("#saveAddressConfirmationMsg").addClass("success_message");
                                    $("#saveAddressConfirmationMsg").html(jsonObj.message);
                                    $("#saveAddressConfirmationMsg").show()
                                }
                            })
                        }
                        else if(jsonObj.status=="error"){
                            $("#saveAddressConfirmationMsg").removeClass("success_message");
                            $("#saveAddressConfirmationMsg").html("<label class='error'>"+jsonObj.message+"</label>");
                            $("#saveAddressConfirmationMsg").show();
                            $("#savecredit").show();
                            $("#processingDiv3").hide();
                        }
                    }
                })
            },
            rules:{
                credit_card_name:{
                    required:true,
                    minlength:2,
                    maxlength:50,
                    alpha:true
                },
                credit_card_number:{
                    required:true,
                    digits:true,
                    maxlength:18,
                    minlength:12
                },
                credit_card_code:{
                    maxlength:4,
                    minlength:3,
                    digits:true
                }, 
                credit_card_exp_month:{
                    required:true,
                    creditcardexpiry:true
                },
                credit_card_exp_year:{
                    required:true
                }
            },
            messages:{
                credit_card_name:{
                    required:ENTER_CREDIT_CARD_NAME
                },
                credit_card_number:{
                    required:ENTER_CREDIT_CARD_NUMBER
                },
                /* credit_card_code:{
                    required:ENTER_CREDIT_CARD_VC
                },*/
                credit_card_exp_month:{
                    required:SELECT_EXPIRY_MONTH,
                    creditcardexpiry:
                    "Plese select valid expiry date"
                },
                credit_card_exp_year:{
                    required:SELECT_EXPIRY_YEAR
                }
            }
        })
    });
    $(".place_order").click(
        function(){
            //showAjxLoader();
            $("#placeorder").hide();
            $("#placeOrderProcessingDiv").show();
            var couponcode=$("#couponcode").val(),dataStr=$("#secondstep").serialize()+"&"+$(
                "#checkout_form").serialize()+"&"+$("#credit_card").serialize()+"&shipcode="+$(
                "#checkout_form [name='shipping_zip_code']").val()+"&shipping_company=&shipping_address="+$(
                "#checkout_form [name='shipping_address_1']").val()+"&shipping_country_text="+$(
                "#checkout_form [name='shipping_country'] option:selected").text()+"&billing_country_text="+$(
                "#billing_country option:selected").text()+"&couponcode="+couponcode;
            $.ajax({
                type:"POST",
                url:"/place-order",
                data:dataStr
                ,
                beforeSend:function(){
                    authenticateUserBeforeAjax()
                },
                success:function(responseData){
                    //hideAjxLoader();
                    $(
                        ".first,.second,.third").removeClass("active");
                    $(".fourth").addClass("active");
                    if(responseData=="session_expired")
                        authenticateUserBeforeAjax();
                    try{
                        var jsonObj=jQuery.parseJSON(responseData);
                        if(jsonObj.status=="credit_card_error")$(
                            ".credit_card_error").html(jsonObj.message["0"]).show();
                        else if(jsonObj.status=="shipping_error")$.each(jsonObj.message,
                            function(i,msg){
                                $(".credit_card_error").append(msg).show()
                            });
                        else if(jsonObj.status=="error"){
                            var messagestr="";
                            $.each(
                                jsonObj.info,function(i){
                                    var pid=i,message=this.message+"<br>";
                                    messagestr+=message
                                });
                            $(".credit_card_error").html(
                                messagestr).show()
                        }else{
                            showCartProducts();
                            $("#maincheckout").hide().html("");
                            $("#orderreview").html(responseData).show(
                                )
                        }
                    }catch(e){
                        showCartProducts();
                        $("#maincheckout").hide().html("");
                        $("#orderreview").html(responseData).show()
                    }
                    
                    $("#placeorder").hide();
                    $("#placeOrderProcessingDiv").hide();                    
                    
                }
            })
        });
    jQuery.validator.addMethod("floating",function(value,element){
        return this.optional(element)||/^-?\d*(\.\d{1,2})?$/.test(
            value)
    },"Please enter valid value.");
    $("#donationaddtocart").click(function(){
        var validator=$("#donation_add_to_cart").
        validate({
            submitHandler:function(){
                var st=checkCustomValidation("donation_add_to_cart");
                if(st){
                    var dataStr=$(
                        "#donation_add_to_cart").serialize();
                    $.ajax({
                        type:"POST",
                        url:"/add-to-cart-donation-front",
                        data:dataStr,
                        beforeSend:
                        function(){
                            authenticateUserBeforeAjax()
                        },
                        success:function(responseData){
                            hideAjxLoader();
                            var jsonObj=jQuery.parseJSON(
                                responseData);
                            if(jsonObj.status=="success")location.href="/cart";else $.each(jsonObj.message,function(i,msg){
                                $("#"+i).
                                after('<label class="error" style="display:block;">'+msg+"</label>")
                            })
                        }
                    })
                }
            },
            rules:{
                amount:{
                    required:true
                },
                other_amount:{
                    required:function(){
                        if($("#amount").val()=="other")return true;else return false
                    },
                    floating:function(){
                        if($("#amount").
                            val()=="other")return true;else return false
                    },
                    minlength:function(){
                        if($("#amount").val()=="other")return 1;else
                            return false
                    },
                    maxlength:function(){
                        if($("#amount").val()=="other")return 10;else return false
                    },
                    min:function(){
                        if($(
                            "#amount").val()=="other")return 1;else return false
                    }
                },
                honor_memory_name:{
                    minlength:3,
                    maxlength:30,
                    letterOnly:true
                }
            },
            messages:{
                amount:{
                    required:"Please select membership."
                },
                other_amount:{
                    required:"Please enter donation amount.",
                    min:
                    "Please enter a donation amount.",
                    floating:"Please enter a valid donation amount."
                }
            }
        })
    });
    $("#inventory_form").validate({
        submitHandler:function(){
            var dataStr=$("#inventory_form").serialize()+"&"+$("#add_to_cart").serialize();
            $.ajax({
                type:
                "POST",
                url:"/add-to-cart-inventory",
                data:dataStr,
                success:function(responseData){
                    hideAjxLoader();
                    if(!
                        checkUserAuthenticationAjx(responseData))return false;
                    var jsonObj=jQuery.parseJSON(responseData);
                    if(jsonObj.status==
                        "success"){
                        refreshCartData();
                        parent.$.colorbox.close()
                    }else $.each(jsonObj.message,function(i,msg){
                        $("#"+i).after(
                            '<label class="error" style="display:block;">'+msg+"</label>")
                    })
                }
            })
        },
        rules:{
            num_quantity:{
                digits:true
            }
        },
        messages:{
            num_quantity:{
                digits:"Please enter a valid number."
            }
        }
    })
    });
function addMoreDonationNotifyFront(currAnchor,DivId){
    showAjxLoader();
    $.ajax({
        type:"POST",
        url:"/add-more-notify-front",
        data:{
            totalNotify:totalNotify
        },
        beforeSend:function(){
            authenticateUserBeforeAjax()
        },
        success:function(responseData){
            hideAjxLoader();
            var allDivBlock=$(
                "div[id^='notify_donation_']");
            if(allDivBlock.length==1){
                var parId=DivId,id=parId.substring(parId.lastIndexOf("_")+1,
                    parId.length);
                $(currAnchor).parent().append(
                    '<a class="remove minus-field" href="javascript:void(0);" onclick="deleteMoreDonationNotify('+id+')"></a>')
            }
            $(currAnchor
                ).remove();
            $("#notify_block").append(responseData);
            totalNotify=totalNotify+1
        }
    })
}
function deleteMoreDonationNotifyFront(
    id){
    $("#notify_donation_"+id).remove();
    var lastBlock=$("div[id^='notify_donation_']").last();
    if($(lastBlock).find(
        ".add-field").length==0){
        var html_f1=
        '<a class="remove minus-field" href="javascript:void(0);" onclick="deleteMoreDonationNotifyFront('+parseInt(id-1)+
        ')"></a>';
        html_f1+='<a id="addMoreDonationNotify" onclick="addMoreDonationNotifyFront(this,'+"'"+lastBlock.attr("id")+
        "'"+')" href="javascript:void(0);" class="add add-field"></a>';
        $("div",$(lastBlock)).last("div").html(html_f1)
    }
    var 
    allDivBlock=$("div[id^='notify_donation_']");
    if(allDivBlock.length==1)$(lastBlock).find(".minus-field").remove();
    if($(
        "div[id^='notify_donation_']").length==5)$("#addMoreDonationNotify").css("display","none");else $(
        "#addMoreDonationNotify").css("display","inline")
}
function removefromcart(p_id,p_type,productMappingId,typeOfProduct,
    pageSrc){
    if(typeOfProduct=="woh")$("#delete_cart_woh").css({
        display:"block"
    });else $("#delete_cart_woh").css({
        display:
        "none"
    });
    $(".delete_cart_product").colorbox({
        width:"700px",
        height:"250px",
        inline:true
    });
    $("#no_delete_cart").click(
        function(){
            $.colorbox.close();
            $("#yes_delete_cart").unbind("click");
            return false
        });
    $("#yes_delete_cart").click(function(
        ){
        $.colorbox.close();
        $("#yes_delete_cart").unbind("click");
        showAjxLoader();
        $.ajax({
            type:"POST",
            async:false,
            data:{
                cartId:
                p_id,
                productTypeId:p_type,
                productMappingId:productMappingId,
                typeOfProduct:typeOfProduct
            },
            url:"/delete-product-to-cart",
            success:function(responseData){
                if(pageSrc)if(pageSrc=="checkout"||pageSrc=="/checkout"){
                    $("#step1").hide();
                    location.
                    reload()
                }
                getcartlist();
                showCartProducts();
                hideAjxLoader();
                if(responseData=="session_expired")authenticateUserBeforeAjax(
                    )
            },
            beforeSend:function(){
                parent.$.colorbox.close();
                parent.$("#cboxClose").click();
                parent.$("#cboxLoadedContent").remove(
                    );
                parent.$("#cboxOverlay").css({
                    visibility:"hidden"
                });
                parent.$("#colorbox").css({
                    visibility:"hidden"
                })
            }
        })
    })
}
function getcartlist(){
    showAjxLoader();
    $.ajax({
        type:"POST",
        url:"/cart-front-product-kiosk",
        success:function(responseData){
            hideAjxLoader();
            $("#cartlist").html(responseData)
        }
    })
}
function getwishlist_backup(){
    showAjxLoader();
    $.ajax({
        type:"POST",
        url:
        "/wishlist-product",
        success:function(responseData){
            hideAjxLoader();
            $("#wishlist").html(responseData)
        }
    })
}
function removefromwishlist(p_id,p_type){
    $(".delete_cart_product").colorbox({
        width:"700px",
        height:"200px",
        inline:true
    });
    $(
        "#no_delete_wishlist").click(function(){
        $.colorbox.close();
        $("#yes_delete_wishlist").unbind("click");
        return false
    });
    $(
        "#yes_delete_wishlist").click(function(){
        $.colorbox.close();
        $("#yes_delete_wishlist").unbind("click");
        showAjxLoader();
        $.
        ajax({
            type:"POST",
            url:"/remove-wishlist-front",
            data:{
                cart_id:p_id,
                product_type:p_type
            },
            success:function(responseData){
                hideAjxLoader();
                if(responseData=="session_expired")authenticateUserBeforeAjax();
                getwishlist()
            }
        })
    })
}
function addtocartwishlist(product_id,cart_id,product_type_id){
    var quantity=1,status=/^\d+$/.test(quantity);
    if(status==false){
        $(
            "#quantity_"+product_id).addClass("error");
        $("#quantityerror_"+product_id).show()
    }else $.ajax({
        type:"POST",
        url:
        "/add-to-cart-inventory-front",
        data:{
            product_id:product_id,
            num_quantity:quantity
        },
        beforeSend:function(){
            authenticateUserBeforeAjax();
            showAjxLoader()
        },
        success:function(responseData){
            hideAjxLoader();
            var jsonObj=jQuery.
            parseJSON(responseData);
            if(jsonObj.status=="success"){
                $.ajax({
                    type:"POST",
                    url:"/remove-wishlist-front",
                    data:{
                        cart_id:
                        cart_id,
                        product_type:product_type_id
                    },
                    success:function(responseData){
                        hideAjxLoader();
                        authenticateUserBeforeAjax();
                        getwishlist()
                    }
                });
                showAddToCartConfirmation()
            }else $.each(jsonObj.message,function(i,msg){
                $("#"+i).after(
                    '<label class="error" style="display:block;">'+msg+"</label>")
            })
        }
    })
}
function selectUsaState(value,id,userType){
    if(value=="228"){
        $("#usa_state_"+userType).show();
        $("#other_state_"+userType).hide();
        $("#"+userType+"_state").val("")
    }else{
        $("#usa_state_"+userType).hide();
        $("#other_state_"+userType).show();
        $("#"+userType+"_state").val("")
    }
}
function assignStateValue(value,id,userType){
    if(value)$("#"+userType+"_state").val(value)
}
function checkCustomValidation(id){
    var flagGl=true,flagCh=true,flag_email=true;
    $('#donation_add_to_cart [name="firstname[]"]').each(function(){
        $('label[for="'+
            $(this).attr("id")+'"]').remove();
        var str=$(this).val(),regx=/^([a-zA-Z'\s]+)$/,isvalid=regx.test(str);
        if(!isvalid&&str.
            length>0){
            $(this).after('<label class="error" for="'+$(this).attr("id")+'" generated="true">'+F_NAME_PROPER+"</label>");
            flagGl=false;
            flagCh=false
        }else $('label[for="'+$(this).attr("id")+'"]').remove()
    });
    $(
        '#donation_add_to_cart [name="lastname[]"]').each(function(){
        $('label[for="'+$(this).attr("id")+'"]').remove();
        var str=$
        (this).val(),regx=/^([a-zA-Z'\s]+)$/,isvalid=regx.test(str);
        if(!isvalid&&str.length>0){
            $(this).after(
                '<label class="error" for="'+$(this).attr("id")+'" generated="true">'+L_NAME_PROPER+"</label>");
            flagGl=false;
            flagCh=
            false
        }else $('label[for="'+$(this).attr("id")+'"]').remove()
    });
    $('#donation_add_to_cart [name="email[]"]').each(function
        (index){
            var str=$(this).val(),regx=/^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            $(this).next().
            remove("span");
            var email_message="Please enter valid email",isvalid=regx.test(str);
            $(this).next().remove("label");
            if(str
                .length>0&&str!="Email")if(!isvalid){
                $(this).after(
                    '<label for="email[]" generated="true" class="error email_error" style="display:block !important;">'+EMAIL_PROPER+
                    " </label>");
                flag_email=false
            }else{
                $(this).next().remove("label");
                flag_email=true
            }else{
                $(this).next().remove("label");
                flag_email=true
            }
        });
    if($(".email_error").length>0)flag_email=false;
    if(flag_email&&flagGl&&flagCh)return true;else
        return false
}
function chooseCoupon(couponCode,promotionId){
    $("#couponcode").val(couponCode)
}
function showHideProduct(){
    if($("#carlistItem").is(":visible")==false){
        $(".number").addClass("active");
        $("#carlistItem").slideDown(300)
    }else if($(
        "#carlistItem").is(":visible")==true){
        $(".number").removeClass("active");
        $("#carlistItem").slideUp(300)
    }
}
function showHideProductFinalPage(){
    if($("#carlistItemOrder").is(":visible")==false){
        $(".number").removeClass("active");
        $(
            "#carlistItemOrder").slideDown(300)
    }else if($("#carlistItemOrder").is(":visible")==true){
        $(".number").addClass("active")
        ;
        $("#carlistItemOrder").slideUp(300)
    }
}
function addNewShippingAddress(){
    oldCity="";
    $("#addNewShippingFrm [id='shipping_address_information_id']").val("");
    $("#shipping_new_address").show();
    $("#old_shipping_address_div").hide();
    $("#ship_method_radio").hide();
    $("#add_new_shipping_address_label").hide();
    $("#cancel_new_shipping_address").show();
    $("#shipping_existing_contact").val("");
    $("#addNewShippingFrm [name='shipping_city']");
    $("#addNewShippingFrm [name='shipping_address_1'],#addNewShippingFrm [name='shipping_address_2'],#addNewShippingFrm [name='shipping_country'],#addNewShippingFrm [name='shipping_city'],#addNewShippingFrm [name='shipping_state'],#addNewShippingFrm [name='shipping_state_id'],#addNewShippingFrm [name='shipping_zip_code'],#addNewShippingFrm [name='shipping_location_type'],#addNewShippingFrm [name='shipping_title'],#addNewShippingFrm [name='shipping_first_name'],#addNewShippingFrm [name='shipping_last_name']").val("");
    var shipAddressId=$("input[name=shipping_existing_contact]:checked").val();
    $("#ship_to_add_option_error").html("");

    selectUsaState('228','shipping_country','shipping');
    
    
    $(".shipping-add-checkbox").show()
}
function cancelNewShippingAddress(){
    $("#old_shipping_address_div").show();
    $(
        "#ship_method_radio").show();
    $("#cancel_new_shipping_address").hide();
    $("#shipping_new_address").hide();
    $(
        "#add_new_shipping_address_label").show();
    $(".shipping-add-checkbox").hide();
    $("#is_apo_po_record").click()
    
    
    $('#step1').show();
    $('#processingDiv').hide();


}
function addNewBillingAddress(){
    $("#credit_card_error").hide();
    $("#old_billing_address_div").hide();
    $("#add_new_billing_address_label").hide();
    
    $("#billing_select_address_error").hide();
    
    $("#cancel_new_billing_address").show();
    $("#billing_existing_contact").val("");
    $("#billing_address_1,#billing_address_2,#billing_city,#billing_country,#billing_state,#billing_zip_code,#billing_location_type,#billing_address_information_id,#billing_state_id,#billing_title,#billing_first_name,#billing_last_name").val("");
    $(".new-address-billing").show();
    $(".select-bill-address").hide();
    $("#usa_state_billing").show();
    $( "#other_state_billing").hide();
    selectUsaState('228','billing_country','billing');
}
function cancelNewBillingAddress(){
    $("#old_billing_address_div").show();
    $(
        "#add_new_billing_address_label").show();
    $("#cancel_new_billing_address").hide();
    $("#billing_new_address").hide()
}
function addNewCard(){
    $("#credit_card_error").hide();
    $("#new_credit_card").show();
    $("#cancel_new_card").show();
    $(
        "#old_credit_card").hide();
    $("#add_new_card_label").hide()
}
function cancelNewCard(){
    $("#new_credit_card").hide();
    $("#cancel_new_card").hide();
    $("#old_credit_card").show();
    $("#add_new_card_label").show();
    $("#credit_card_exp_year").val($
        ("#credit_card_exp_year option:first").val());
    $("#credit_card_number,#credit_card_code,#credit_card_exp_month").val("")
}
function deleteBillingShippingAddress(value,location){
    $(".delete_address").colorbox({
        width:"650px",
        height:"200px",
        inline:true
    });
    $
    ("#no_delete_address").click(function(){
        $.colorbox.close();
        $("#yes_delete_address").unbind("click");
        return false
    });
    $("#yes_delete_address").click(function(){
        if(location=="billing")var address_location=$("#billing_addressinfo_location").
            val();
        else if(location=="shipping")var address_location=$("#addressinfo_location").val();
        $("#formErrorMsg").hide();
        $("#yes_delete_address").unbind("click");
        authenticateUserBeforeAjax();
        showAjxLoader();
        $.ajax({
            type:"POST",
            url: "/delete-billing-shipping-address/"+value,
            success:function(responseData){
                var jsonObj=jQuery.parseJSON(responseData);
                if(
                    jsonObj.status=="success"){
                    hideAjxLoader();
                    $.ajax({
                        type:"POST",
                        url:"/user-shipping-address/"+$("#user_id").val()+"/"+
                        address_location,
                        success:function(responseData){
                            $.colorbox.close();
                            if(location=="billing")$("#billing_address_list").html(responseData);
                            else if(location=="shipping")$("#shipping_address_list").html(responseData);
                            window.location.hash="#userAddress";
                            $("#saveAddressConfirmationMsg").html(jsonObj.message);
                            $("#saveAddressConfirmationMsg").show();
                            $("#billing_address_information_id").val($("input:radio[name=billing_existing_contact]:checked").val());
                            if(location=="shipping") {
                                    getAddressInfoFront($("input:radio[name=shipping_existing_contact]:checked").val(),"shipping","");
                                }
                                                            

                        }
                    })
                }
            }
        })
    })
}