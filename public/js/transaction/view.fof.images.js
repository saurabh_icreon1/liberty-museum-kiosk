function dataShowPost(){
    $("#previewFOF1").contents().find("#showTagsId").css({
        display:"block"
    });
    $("#previewFOF1").
    contents().find("#addTagsId").css({
        display:"none"
    });
    $("#previewFOF1").contents().find("#mapper").hide();
    var arrNames=[],
    arrNamesSerialize=[],htmlNewBox="",countS=0;
    $("#previewFOF1").contents().find("#planetmap > div").each(function(){
        arrNames.push($.trim($(this).find("div.tagged_title > span.tagged_title_text").html()));
        htmlNewBox+='<div class="'+$.
        trim($(this).attr("class").replace("tagged",""))+' tages" >'+$.trim($(this).find(
            "div.tagged_title > span.tagged_title_text").html())+'<a class="removeTag" onclick="javascript:removePeoplePhotoTag(\''+
        $.trim($(this).attr("class").replace("tagged",""))+'\');" delid="'+$.trim($(this).attr("class").replace("tagged",""))+
        '" >X</a>'+"</div>";
        arrNamesSerialize[countS]={
            name:$.trim($(this).find("div.tagged_title > span.tagged_title_text").
                html()),
            width:$(this).css("width"),
            height:$(this).css("height"),
            top:$(this).css("top"),
            left:$(this).css("left")
            };
            
        countS=
        parseInt(countS)+parseInt(1)
        });
    if(arrNamesSerialize.length>0){
        $("#people_photo").val(JSON.stringify(arrNamesSerialize));
        $("#people_photo_edit").val(JSON.stringify(arrNamesSerialize))
        }else{
        $("#people_photo").val("");
        $("#people_photo_edit").
        val("")
        }
        $("#peoplePhotoBox").html(htmlNewBox);
    $("#span_fof_people_in_photo").html(htmlNewBox)
    }
    function 
removePeoplePhotoTag(delid){
    $("#previewFOF1").contents().find("."+delid).remove();
    dataShowPost()
    }
    $(function(){
    $(
        "#upload_photo").fileupload({
        url:"/upload-user-fof-image",
        dataType:"json",
        done:function(e,data){
            data=data.result;
            if(data
                .status=="success"){
                $('label[for="upload_photo"]').remove();
                $("#upload_photo").removeClass("error");
                $("#filemessage").
                addClass("success-msg");
                $("#filemessage").removeClass("error-msg");
                $("#filemessage").html(data.message);
                $(
                    "#photo_modified_name").val(data.encrypted_filename);
                $("#photo_modified").val("1");
                $("#UploadUserFofImageIframe").attr(
                    "src","/upload-user-fof-image-iframe-crm/"+$("#fof_id").val()+"/"+data.encrypted_filename);
                $("#previewFOF1").attr("src",
                    "/upload-user-fof-image-iframe-preview-crm/"+$("#fof_id").val()+"/"+data.encrypted_filename+"/1");
                $("#fofPreviewImage").
                attr("src",data.temp_upload_medium_dir+data.filename);
                $("#countTagNum").val("");
                $("#peoplePhotoBox").empty();
                $(
                    "#span_fof_people_in_photo").empty();
                $("#people_photo").val("")
                }else{
                $('label[for="upload_photo"]').remove();
                $(
                    "#filemessage").removeClass("success-msg");
                $("#filemessage").addClass("error");
                $("#photo_modified_name").val("");
                $(
                    "#UploadUserFofImageIframe").attr("src","/upload-user-fof-image-iframe-crm/none/none");
                $("#previewFOF1").attr("src",
                    "/upload-user-fof-image-iframe-preview-crm/none/none/0");
                $("#filemessage").html('<label class="error">'+data.message+
                    "</label>");
                $("#peoplePhotoBox").empty();
                $("#span_fof_people_in_photo").empty();
                $("#people_photo").val("")
                }
            },
    start:
    function(e){
        showAjxLoader()
        },
    stop:function(e){
        hideAjxLoader()
        }
    }).prop("disabled",!$.support.fileInput).parent().addClass
    ($.support.fileInput?undefined:"disabled");
    $.validator.addMethod("AlphaNumericSpaceComma",function(alpha_numeric_text){
        var alpha_number_regex=/^([a-zA-Z0-9,'\s]+)$/;
        if(!alpha_number_regex.test(alpha_numeric_text)&&alpha_numeric_text!="")
            return false;else return true
            },"");
    $.validator.setDefaults({
        ignore:[]
    });
    $("#fof_image").validate({
        submitHandler:function
        (){
            var dataStr=$("#fof_image").serialize();
            $.ajax({
                type:"POST",
                url:"/view-fof-image/"+fof_id+"/"+fof_transaction_id,
                data
                :dataStr,
                success:function(responseData){
                    if(!checkUserAuthenticationAjx(responseData))return false;
                    var jsonObj=jQuery.
                    parseJSON(responseData);
                    if(jsonObj.status=="success"){
                        parent.$("#fof_img"+fof_id).show();
                        parent.location.reload()
                        }else $
                        .each(jsonObj.message,function(i,msg){
                            $('label[for="'+i+'"]').remove();
                            $("#"+i).after('<label for="'+i+
                                '" class="error" style="display:block;">'+msg+"</label>")
                            })
                        }
                    })
        },
    rules:{
        donated_by:{
            required:true,
            AlphaNumericSpaceComma:
            true
        },
        people_photo:{
            required:true
        },
        photo_caption:{
            required:true,
            AlphaNumericSpaceComma:true
        },
        crop_fof_image_status:{
            required:true
        }
    },
    messages:{
        donated_by:{
            required:"Please enter donated by.",
            AlphaNumericSpaceComma:
            "Please enter valid donated by."
        },
        people_photo:{
            required:"Click on Edit Photo to tag people."
        },
        photo_caption:{
            required:
            "Please enter caption.",
            AlphaNumericSpaceComma:"Please enter valid caption."
        },
        crop_fof_image_status:{
            required:
            "Please crop the image in square."
        }
    }
})
});
$("#editPhotoLink").click(function(){
    $("#viewFOF").css({
        display:"none"
    });
    $(
        "#editFOF").css({
        display:"block"
    });
    $('label[for="people_photo"]').remove();
    parent.$.colorbox.resize({
        width:"1310px",
        height:"800px"
    })
    });
    
    $("#save_photo").click(function(){
        if($.trim($('#crop_fof_image_status').val()) == "") {
           $('label[for="crop_fof_image_status"]').remove(); 
           $('#crop_fof_image_status').after('<label class="error" for="crop_fof_image_status" generated="true"><span class="redarrow"></span>Please crop the image in square.</label>');
        }
       else {
          $("#viewFOF").css({display:"block"});
           $("#editFOF").css({display:"none"});
           $('label[for="people_photo"]').remove();
           $('label[for="crop_fof_image_status"]').remove();
           parent.$.colorbox.resize({innerWidth:"650px",innerHeight:"800px"});
        }
    });