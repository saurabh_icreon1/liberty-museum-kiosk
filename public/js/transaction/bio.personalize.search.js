$(function(){
    var gridId="bio_personalize_transaction_"+searchType,listTransactionContactGrid=jQuery("#"+gridId),
    emptyMsgDiv=$('<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");
    $("#"+gridId).jqGrid({
        mtype:"POST",
        url:"/get-wall-of-honor-bio-certificate-search",
        datatype:"json",
        sortable:true,
        postData:{
            searchString:"user_id="+userId+"&name="+name+
            "&transaction_id="+transactionId+"&panel_no="+panelNo+"&search_type="+searchType
        },
        colNames:["",L_TRANSACTION_HASH,
        L_PANEL_HASH,L_WOH_PRODUCT],
        colModel:[{
            name:"",
            sortable:false,
            width:"20px"
        },{
            name:"tbl_usr_woh.transaction_id",
            index:
            "tbl_usr_woh.transaction_id",
            width:"50px"
        },{
            name:"tbl_usr_woh.panel_no",
            index:"tbl_usr_woh.panel_no",
            width:"50px"
        },{
            name
            :"tbl_products.product_name",
            index:"tbl_products.product_name"
        }],
        viewrecords:true,
        sortname:"tbl_usr_woh.modified_date",
        sortorder:"desc",
        rowNum:10,
        rowList:[10,20,30],
        pager:listTransactionContactGrid,
        viewrecords:true,
        autowidth:true,
        shrinkToFit:true,
        caption:"",
        width:"100%",
        cmTemplate:{
            title:false
        }
        ,
        loadComplete:function(){
            hideAjxLoader();
            if(searchType=="personalize" || searchType=="duplicate_certificate"){
                var allTrOfGrid=$("tr","#"+gridId);
                $("#personalize_wohid").val($(allTrOfGrid[1]).attr("id"))
            }
            var count=listTransactionContactGrid.getGridParam(),ts=
            listTransactionContactGrid[0];
            if(ts.p.reccount===0){
                listTransactionContactGrid.hide();
                emptyMsgDiv.show();
                $("#"+searchType+"_right div.ui-paging-info").css("display","none")
            }else{
                listTransactionContactGrid.show();
                emptyMsgDiv.hide();
                $("#"+searchType+"_right div.ui-paging-info").css("display","block")
            }
        }
    });
    emptyMsgDiv.insertAfter(
        listTransactionContactGrid.parent());
    emptyMsgDiv.hide();
    $("#"+gridId).jqGrid("navGrid","#"+searchType,{
        reload:true,
        edit:false,
        add:false,
        search:false,
        del:false
    });
    $("#personalized_qty").on("keyup",function(){
        var pers_qty=$("#personalized_qty").val(),bio_qty=$("#bio_certificate_qty").val(),dup_qty=$('#duplicate_certificate_qty').val(),size=$("#related_select_product li").size();
        if(pers_qty>0&&pers_qty!="")
            $("#otherproductaddtocart").show();
        else if(bio_qty==""&&size==0)$("#otherproductaddtocart").hide()
    });
    $("#bio_certificate_qty").on("keyup",function(){
        var pers_qty=$("#personalized_qty").val(),bio_qty=$("#bio_certificate_qty").val(),dup_qty=$('#duplicate_certificate_qty').val(),size=$("#related_select_product li").size();
        if(bio_qty>0&&bio_qty!="")$("#otherproductaddtocart").show();
        else 
        if(pers_qty==""&&size==0)$("#otherproductaddtocart").hide()
    });
    $("#duplicate_certificate_qty").on("keyup",function(){
        var pers_qty=$("#personalized_qty").val(),bio_qty=$("#bio_certificate_qty").val(),dup_qty=$('#duplicate_certificate_qty').val(),size=$("#related_select_product li").size();
        if(dup_qty>0&&dup_qty!="")
            $("#otherproductaddtocart").show();
        else if(dup_qty==""&&size==0)$("#otherproductaddtocart").hide()
    });
    $(document).on("change",'input[id^="woh_"]',function(){
        $("#bio_wohid").val(this.id.substr(4))
    })
});
function addBioCertificateToCart(bioProId){
    $.colorbox({
        width:"900px",
        height:
        "500px",
        iframe:true,
        href:"/create-bio-certificate/"+bioProId
    })
}
function buttonShow(){
    $("#otherproductaddtocart").show()
}