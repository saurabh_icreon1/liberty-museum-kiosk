function getUserTransactionDetail(transactionId){$.colorbox({width:"1350px",href:"/user-transaction-detail-crm/"+
transactionId,height:"650px",opacity:0.50,inline:false,onOpen:function(){showAjxLoader()},onComplete:function(){
hideAjxLoader()}})}$(function(){$($(".e4")).each(function(i){if($(this).is("[type=checkbox],[type=radio]")){var input=$(
this),input_class=input.attr("class").split(" ");input.attr({id:input_class["1"]+"_"+i});var label=$("label[for="+input.
attr("id")+"]"),outerLabel=input.parent(),outerLabelParent=input.parent().parent(),inputType=input.is("[type=checkbox]")
?"checkbox":"radio",oDiv=$(document.createElement("label")).attr({"for":input_class["1"]+"_"+i}).html($(outerLabel).text
());$(outerLabel).remove();outerLabelParent.append(input,oDiv)}});$(".e4").customInput();var grid=jQuery("#list"),
emptyMsgDiv=$('<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");$("#list").jqGrid({postData:{searchString:$(
"#user_search_transaction").serialize()},mtype:"POST",url:"/user-search-transactions",datatype:"json",colNames:[
L_TRANSACTION,L_DATE,"Product Name",L_TRANSACTION_TYPE,L_PROGRAM,L_AMOUNT],colModel:[{name:L_TRANSACTION,sortable:false}
,{name:L_DATE,index:"usr_transaction.transaction_date"},{name:"Product Name",sortable:false},{name:L_TRANSACTION_TYPE,
sortable:false},{name:L_PROGRAM,sortable:false},{name:L_AMOUNT,index:"usr_transaction.transaction_amount"}],viewrecords:
true,sortname:"usr_transaction.transaction_date",sortorder:"desc",rowNum:10,rowList:[10,20,30],pager:"#pcrud",
viewrecords:true,autowidth:true,shrinkToFit:true,caption:"",width:"100%",cmTemplate:{title:false},loadComplete:function(
){var count=grid.getGridParam(),ts=grid[0];if(ts.p.reccount===0){grid.hide();emptyMsgDiv.show();$(
"#pcrud_right div.ui-paging-info").css("display","none")}else{grid.show();emptyMsgDiv.hide();$(
"#pcrud_right div.ui-paging-info").css("display","block")}}});emptyMsgDiv.insertAfter(grid.parent());emptyMsgDiv.hide();
$("#list").jqGrid("navGrid","#pcrud",{reload:true,edit:false,add:false,search:false,del:false});$("#list").jqGrid(
"navButtonAdd","#pcrud",{caption:"",title:"Export",id:"exportExcel",onClickButton:function(){exportExcel("list",
"/user-search-transactions")},position:"last"})});$("#searchbutton").click(function(){jQuery("#list").jqGrid(
"setGridParam",{postData:{searchString:$("#user_search_transaction").serialize()}});jQuery("#list").trigger("reloadGrid"
,[{page:1}]);return false});function searchTransaction(){$("#searchbutton").click()}$("#searchbutton").click(function(){
jQuery("#list").jqGrid("setGridParam",{postData:{searchString:$("#user_search_transaction").serialize()}});jQuery(
"#list").trigger("reloadGrid",[{page:1}]);return false});function updateResponse(campaign_id,user_id){$.colorbox({width:
"500px",height:"400px",iframe:true,href:"/user-update-campaign/"+campaign_id+"/"+user_id})}function viewReceipt(
transaction_id,user_id){$.colorbox({width:"1100px",height:"810px",iframe:true,href:"/user-transaction-receipt/"+user_id+
"/"+transaction_id})}