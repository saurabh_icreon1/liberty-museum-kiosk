var loadPage=0;
var balAmnt=0;
var totalInvoiceAmount=0;
function autoCompleteCoupon(){
    $("#coupon_code").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:
        true,
        source:function(request,response){
            $.ajax({
                url:"/get-coupon-code",
                method:"POST",
                dataType:"json",
                data:{
                    coupon_code:$(
                        "#coupon_code").val()
                },
                success:function(jsonResult){
                    if(!checkUserAuthenticationAjx(jsonResult))return false;
                    var 
                    resultset=[];
                    $.each(jsonResult,function(){
                        var id=this.promotion_id,value=this.coupon_code;
                        resultset.push({
                            id:id,
                            value:
                            value
                        })
                    });
                    response(resultset)
                } 
            })
        },
        open:function(){
            $(".ui-menu").width(300)
        },
        change:function(event,ui){
            $("#is_promotion_applied").val("");
            if(ui.item){
                $("#promotion_id").val(ui.item.id);
                $("#coupon_code").val(ui.item.value)
            }
            else{
                $("#promotion_id").val("");
                $("#coupon_code").val("")
            }
        },
        select:function(event,ui){
            $("#coupon_code").val(ui.item.value);
            return false
        }
    })
    $("#campaign_code").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:
        true,
        source:function(request,response){
            $.ajax({
                url:"/get-campaign-code",
                method:"POST",
                dataType:"json",
                data:{
                    campaign_code:$("#campaign_code").val(),
                    active:1
                },
                success:function(jsonResult){
                    var resultset=[];
                $.each(jsonResult,function(){
                    var id=this.campaign_id;
                    var value=this.campaign_code;
                    resultset.push({
                        id:this.campaign_id,
                        value:this.campaign_code
                        })
                    });
                response(resultset)
                } 
            })
        },
        open:function(){
            $(".ui-menu").width(300)
        },
        change:function(event,ui){
            if(ui.item){
                $("#campaign_code_id").val(ui.item.id);
                $("#campaign_code").val(ui.item.value)
            }
            else{
                $("#campaign_code_id").val("");
                $("#campaign_code").val("")
            }
        },
        select:function(event,ui){
            $("#campaign_code").val(ui.item.value);
            return false
        }
    })
}
$(function(){
    jQuery.validator.addMethod("floating",function(value,element){
        return this.optional(
            element)||/^-?\d*(\.\d{1,2})?$/.test(value)
    },NUMERIC_VALUE);
    jQuery.validator.addMethod("alphanumerichypenchar",function(
        value,element){
        return this.optional(element)||/^[a-z0-9- ]+$/i.test(value)
    },ALPHA_NUMERIC_SPECIALSYMPOL);
    autoCompleteCoupon();
    $("#membership_afihc").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:function(
            request,response){
            $.ajax({
                url:"/get-membership-afihc",
                method:"POST",
                dataType:"json",
                data:{
                    membership_afihc:$(
                        "#membership_afihc").val()
                },
                success:function(jsonResult){
                    if(!checkUserAuthenticationAjx(jsonResult))return false;
                    var 
                    resultset=[];
                    $.each(jsonResult,function(){
                        var id=this.id,value=this.value,type=this.type;
                        resultset.push({
                            id:this.id,
                            value:this.value+" ("+this.type+")"
                        })
                    });
                    response(resultset)
                }
            })
        },
        open:function(){
            $(".ui-menu").width(300)
        },
        change:
        function(event,ui){
            if(ui.item)$("#membership_afihc").val(ui.item.value);else $("#membership_afihc").val("");
            $(
                ".ui-autocomplete").jScrollPane()
        },
        select:function(event,ui){
            $("#membership_afihc").val(ui.item.value);
            return false
        }
    });
    $
    ("#name_email").autocomplete({
        minLength:3,
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:function(request,response){
            $("#name_email").addClass("auto-suggest-loading");
            $.ajax({
                url:"/get-contacts",
                method:"POST",
                dataType:"json",
                data:{
                    user_email_with_name:$("#name_email").val()
                },
                success:function(jsonResult){
                    $("#name_email").removeClass("auto-suggest-loading");
                    if(!checkUserAuthenticationAjx(jsonResult))return false;
                    var resultset=[];
                    $.each(jsonResult,function(){
                        var id
                        =this.user_id,value=this.full_name;
                        resultset.push({
                            id:this.user_id,
                            value:this.full_name
                        })
                    });
                    response(resultset)
                }
            })
        },
        open
        :function(){
            $(".ui-menu").width(350)
        },
        change:function(event,ui){
            if(ui.item)$("#name_email").val(ui.item.value);else $(
                "#name_email").val("")
        },
        select:function(event,ui){
            $("#name_email").val(ui.item.value);
            return false
        }
    });
    $("#zip_code").autocomplete({
        minLength:3,
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:function(request,response){
            $("#zip_code").addClass("auto-suggest-loading");
            $.ajax({
                url:"/get-postal-lookup",
                method:"POST",
                dataType:"json",
                data:{
                    zip_code:$("#zip_code").val()
                },
                success:function(jsonResult){
                    $("#zip_code").removeClass("auto-suggest-loading");
                    if(!checkUserAuthenticationAjx(jsonResult))return false;
                    var resultset=[];
                    $.each(jsonResult,function(){
                        var id=this.user_id,
                        value=this.zip_code;
                        resultset.push({
                            id:this.user_id,
                            value:this.zip_code
                        })
                    });
                    response(resultset)
                }
            })
        },
        open:function(){
            $(
                ".ui-menu").width(300)
        },
        change:function(event,ui){
            if(ui.item)$("#zip_code").val(ui.item.value);else $("#zip_code").val(
                "");
            $(".ui-autocomplete").jScrollPane()
        },
        select:function(event,ui){
            $("#zip_code").val(ui.item.value);
            return false
        }
    });
    $("#country").autocomplete({
        minLength:3,
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:function(request,response){
            $("#country").addClass("auto-suggest-loading");
            $.ajax({
                url:
                "/get-country-lookup",
                method:"POST",
                dataType:"json",
                data:{
                    country:$("#country").val()
                },
                success:function(jsonResult){
                    $("#country").removeClass("auto-suggest-loading");
                    if(!checkUserAuthenticationAjx(jsonResult))return false;
                    var resultset=[];
                    $.each(jsonResult,function(){
                        var id=this.country_id
                        ,value=this.name;
                        resultset.push({
                            id:this.country_id,
                            value:this.name
                        })
                    });
                    response(resultset)
                }
            })
        },
        open:function(){
            $(
                ".ui-menu").width(300)
        },
        change:function(event,ui){
            if(ui.item)$("#country").val(ui.item.value);else $("#country").val("")
            ;
            $(".ui-autocomplete").jScrollPane()
        },
        select:function(event,ui){
            $("#country").val(ui.item.value);
            return false
        }
    });
    $("#searchbutton").click(function(){
        var countSelectVal = 0;
        $('form#search_user input,form#search_user select,form#search_user textarea').each(function(index){
            var input = $(this);
            if(input.attr('name') != undefined && input.attr('name') != 'searchbutton' && $.trim(input.val()) != undefined && $.trim(input.val()) != "") {
                countSelectVal = parseInt(countSelectVal) + parseInt(1);
            }
        });

        $('#last_transaction_fromdate').css({display:"none"});
        if(countSelectVal < 3) {
          $("html, body").animate({scrollTop:$(".accordion-detail").offset().top},2000);
          $('#search-err-msg').css({display:"block"}); 
          return false;  
        }
        else if($.trim($('#last_transaction').val()) == "1" && $.trim($('#from_date').val()) == "" && $.trim($('#to_date').val()) == "") {           
          $("html, body").animate({scrollTop:$(".accordion-detail").offset().top},2000); 
          $('#last_transaction_fromdate').css({display:"block"}); 
          return false;
       }
       else {
            $('#last_transaction_fromdate').css({display:"none"}); 
            $('#search-err-msg').css({display:"none"}); 
            showAjxLoader();
            $(".search-results").show();
            showResult();
            jQuery("#list_transaction_contact").jqGrid("setGridParam",{
                postData:{
                    searchString:$("#search_user").serialize()
                }
            });
            jQuery("#list_transaction_contact").trigger("reloadGrid",[{
                page:1
            }]);
            window.location.hash="#list_transaction_contact";
            hideAjxLoader();
            return false;
       }   
    });
    $(document).on("change","#payment_mode_cash",function(event){
        if($(this).is(":checked"))if(checkShippingSelected())$("#cashAmountDiv").show();
            else{
                $(this).removeAttr("checked");
                $(this).customInput()
            }else $("#cashAmountDiv").hide()
    });
    $(document).on("change",
        "#payment_mode_chk",function(event){
            if($(this).is(":checked"))if(checkShippingSelected())$("#checkAmountDiv").show();
                else{
                    $(this).removeAttr("checked");
                    $(this).customInput()
                }else $("#checkAmountDiv").hide()
        });
	$(document).on("change","#payment_mode_invoice",function(event){
        if($(this).is(":checked") && checkShippingSelected()){
			 $("#invoice_number_div").show();
			 balAmnt = parseFloat($("#balanceAmount").html());
			 totalInvoiceAmount = parseFloat($("#totalAmount").html());
			 $("#amount_invoice_number").val(totalInvoiceAmount);
			
			if($("#payment_mode_credit").is(":checked") && $("#payment_mode_credit").attr("disabled") =="disabled"){ 
				$("#payment_mode_invoice").removeAttr("checked");
				$("#payment_mode_invoice").customInput();
				  deleteCreditCardContent();return;
			 }else{
				if($("#payment_mode_credit").is(":checked")){
				 $("#payment_mode_credit").removeAttr("checked");
				 $("#payment_mode_credit").customInput();
				 $("#payment_mode_cash").trigger("click");
				}
			 }
			 $("#payment_mode_credit").attr("disabled", "disabled");
			 if($("#payment_mode_cash").is(":checked")){
			    $("#payment_mode_cash").trigger("click");
				$("#payment_mode_cash").removeAttr("checked");
				$("#payment_mode_cash").customInput();
				$("#payment_mode_cash_amount").val('');
				$("#cash_batch_id").val('');
				
			 }
			 $("#payment_mode_cash").attr("disabled", "disabled");
			 
			 if($("#payment_mode_chk").is(":checked")){
				 $("#payment_mode_chk").trigger("click");
				 $("#payment_mode_chk").removeAttr("checked");
				 $("#payment_mode_chk").customInput();
				 var totalCheckPayment=0;
				 checkAmounts=$("input[id^='payment_mode_chk_amount_']");
				$(checkAmounts).each(function(){
					$(this).val('');
				});
				 checkNumber=$("input[id^='payment_mode_chk_num_']");
				 $(checkNumber).each(function(){
					$(this).val('');
				});
				checkID=$("input[id^='payment_mode_chk_id_']");
				 $(checkID).each(function(){
					$(this).val('');
			
				});
				checkBatchID=$("input[id^='check_batch_id_']");
				 $(checkBatchID).each(function(){
					$(this).val('');
				});
				
				$("#totalCheckAmount").text('0.00');
				
			 }
			  $("#payment_mode_chk").attr("disabled", "disabled");
			  $("#balanceAmount").html('0.00');
			 
		}
            else{
				if(balAmnt>0)$("#balanceAmount").html(balAmnt);
                $(this).removeAttr("checked");
                $(this).customInput();
				$("#amount_invoice_number").val('0.00');
				$("#invoice_number_div").hide();
				$("#payment_mode_credit").attr("disabled", false);
				$("#payment_mode_chk").attr("disabled", false);
				$("#payment_mode_cash").attr("disabled", false);
            }
		
    });
    $(document).on("change",
        "#is_exist_billing",function(){
            if($(this).is(":checked"))$("#billing_address_info").hide();else $(
                "#billing_address_info").show()
        });
    $(document).on("change","#payment_mode_credit",function(){
        if($(this).is(":checked"))
            if(checkShippingSelected())$.colorbox({
                href:"/get-credit-card-detail/"+encryptUserId,
                width:"900px",
                height:"800px",
                iframe
                :true
            });
            else{ 
                $(this).removeAttr("checked");
                $(this).customInput()
            }
    });
    $("#is_usa").change(function(){
        if($(this).is(
            ":checked")){
            $("#state_text_select").show();
            $("#state_text_block").hide()
        }else{
            $("#state_text_select").hide();
            $(
                "#state_text_block").show()
        }
    });
    $(document).on("blur",'input[id^="transaction_product"]',function(){
        var myArray=$(this).
        attr("id"),id=myArray.split("_"),qty=$(this).val();
        if(isNaN(qty))$("#product_error_"+id[2]+"_"+id[3]+"_"+id[4]).html(
            "Please enter a valid quantity").show();
        else{
            $("#product_error_"+id[2]+"_"+id[3]+"_"+id[4]).hide();
            $.ajax({
                url:
                "/update-cart-crm",
                method:"POST",
                dataType:"json",
                async:false,
                data:{
                    cartId:id[2],
                    qty:qty,
                    productId:id[3],
                    typeOfProduct:id
                    [4],
                    productTypeId:id[5],
                    productMappingId:id[6]
                },
                success:function(response){
                    if(response.status=="success")refreshCartData
                        (0);
                    else if(response.status=="error")$("#product_error_"+id[2]+"_"+id[3]+"_"+id[4]).html(response.info[id[4]+"_"+id[2]+
                        "_"+id[3]].message).show()
                }
            })
        }
    });
    $(document).on("click","#update_crm_cart",function(){
        showAjxLoader();
        var isVisible=$(
            'label[id^="product_error_"]').is(":visible");
        if(!isVisible)refreshCartData();
        hideAjxLoader()
    });
    
    $("#last_transaction").change(function(){
        var date=$(this).val();
        if(date=="1"){
            $("#date_selection").show();
            $("#from_date").val("");
            $("#to_date").val("")
        }else $("#date_selection").hide()
    });
    $("#from_date").datepicker({
        changeMonth:true,
        changeYear:true,
        onClose:
        function(selectedDate){
            $("#to_date").datepicker("option","minDate",selectedDate)
        }
    });
    $("#to_date").datepicker({
        changeMonth:true,
        changeYear:true,
        onClose:function(selectedDate){
            $("#from_date").datepicker("option","maxDate",
                selectedDate)
        }
    });
    $("#img_from").click(function(){
        jQuery("#from_date").datepicker("show")
    });
    $("#img_to").click(function()
    {
        jQuery("#to_date").datepicker("show")
    });
        
    if (typeof defaultShippingAddressInformationId != 'undefined')    { 
		    if(defaultShippingAddressInformationId!='')    
		    {
		        $("#shipping_existing_contact").val(defaultShippingAddressInformationId);
		        getAddressInfo(defaultShippingAddressInformationId,'shipping');
		    }
        
        }
        
     $("#save_shipping_address, #save_billing_address").click(function(){
        $("#same_address_message").html('');
        if($("#billing_existing_contact").val() == $("#shipping_existing_contact").val()) {
	        if($("#save_billing_address").is(":checked") == true && $("#save_shipping_address").is(":checked") == true)
	        {
	            $("#same_address_message").html('<br>If Billing address and Shipping address are same then address will be updated with shipping address.');
	        }
        }
     });       
        
        
});
function checkShippingSelected
(){
    var numOfShipping=$("input[name='shipping_method']:checked").length;
    if(typeof numOfShipping!="undefined"&&
        numOfShipping==0&&$("#ship_information_block").is(":visible")){
        var shippingElement=$(
            "input:radio[name='shipping_method']").first();
        $(shippingElement).after('<label class="error" style="display:block;">'+
            SHIPPING_METHOD_EMPTY+"</label>");
        window.scroll($(shippingElement).offset().left,$(shippingElement).offset().top-50);
        return false
    }
    return true
}
function showCreditDetail(currVal){
    if(currVal==1){
        $("#old_card_info").show();
        $("#new_card_info"
            ).hide()
    }else{
        $("#old_card_info").hide();
        $("#new_card_info").show()
    }
}
function showAfihcNumbers(afihc_codes){
    var 
    afihcCodeArr=afihc_codes.split(","),afihcHtml='<div class="row">';
    for(i=0;i<afihcCodeArr.length;i++)afihcHtml+=
        '<div class="data">'+afihcCodeArr[i]+"</div>";
    afihcHtml+="</div>";
    $("#afhic_contents").html(afihcHtml);
    $(
        ".view_afihc_codes").colorbox({
        width:"500px",
        height:"200px",
        inline:true
    })
}
var cityVal='';
function getAddressInfo(address_information_id
    ,type){
    if(address_information_id!=""){
        showAjxLoader();
        $.ajax({
            url:"/get-billing-shipping-address-info",
            method:"POST",
            dataType:"json",
            data:{
                address_information_id:address_information_id,
                user_id:userId
            },
            success:function(responseData){
                hideAjxLoader();
                if(!checkUserAuthenticationAjx(responseData))return false;
                var jsonObj=responseData;
                if(jsonObj.country_id
                    ==228){
                    $("#"+type+"_text_state_block").hide();
                    $("#"+type+"_select_state_block").show()
                }else{
                    $("#"+type+
                        "_text_state_block").show();
                    $("#"+type+"_select_state_block").hide()
                }
                $("#"+type+"_title").val(jsonObj.title).select2();
                $("#"+type+"_first_name").val(jsonObj.first_name);
                $
                ("#"+type+"_last_name").val(jsonObj.last_name);
                $("#"+type+"_company").val(jsonObj.company_name);
                $("#"+type+"_address").
                val(jsonObj.full_address);
               
                $("#"+type+"_state").val(jsonObj.state);
                $("#"+type+
                    "_state_text").val(jsonObj.state);
                $("#"+type+"_state_select").val(jsonObj.state).select2();
                $("#"+type+"_zip_code").val(
                    jsonObj.zip_code);
                $("#"+type+"_country").val(jsonObj.country_id).select2();
                $("#"+type+"_phone").val(jsonObj.phone_num);
                $
                ("#"+type+"_country_code").val(jsonObj.country_code);
                $("#"+type+"_area_code").val(jsonObj.area_code);
                $("#"+type+
                    "_phone_info_id").val(jsonObj.phone_contact_id);
                $("#"+type+"_location_type").val(jsonObj.location_name);
                if(type==
                    "shipping"&&jsonObj.zip_code!="")refreshCartData()
                    
                cityVal = jsonObj.city;
                    

                if(jsonObj.location_name == '3' || jsonObj.location_name == '2,3' || jsonObj.location_name == '1,2,3' || jsonObj.location_name == '1,3')
                {
                    $('input[name="is_apo_po"][value="1"]').attr('checked', false);       
                    $('#is_apo_po_0').next().removeClass('checked'); 
                    $('input[name="is_apo_po"][value="2"]').attr('checked', false);        
                    $('#is_apo_po_1').next().removeClass('checked'); 
                    $('input[name="is_apo_po"][value="3"]').attr('checked', false);        
                    $('#is_apo_po_2').next().removeClass('checked'); 
                
                    $('input[name="is_apo_po"][value="' + jsonObj.address_type + '"]').attr('checked', true);    
                    if(jsonObj.address_type*1 == 1 || jsonObj.address_type == 0 || jsonObj.address_type == '') {
                        $('#is_apo_po_0').next().addClass('checked');
                        $('#is_apo_po_0').click();
                        $("#"+type+"_state_select").val(jsonObj.state).select2();
                        $('input[name="is_apo_po"][value="1"]').click();
                    }
                    else if(jsonObj.address_type*1 == 2) {
                        $('#is_apo_po_1').next().addClass('checked');
                        $('#is_apo_po_1').click();
                        $("#"+type+"_state_select").val(jsonObj.state).select2();
                        $('input[name="is_apo_po"][value="' + jsonObj.address_type + '"]').click();
                    }
                    else if(jsonObj.address_type*1 == 3) {
                        $('#is_apo_po_2').next().addClass('checked');
                        $('#is_apo_po_2').click();
		                        
                        $("#apo_po_state").val(jsonObj.state).select2();
                        $('input[name="is_apo_po"][value="' + jsonObj.address_type + '"]').click();   
                    }
                
                }
                $("#"+type+"_city").val(jsonObj.city);
                applyCoupon();
            }
        })
    }else{
        $("#"+type+"_first_name").val("");
        $("#"+type+"_last_name").
        val("");
        $("#"+type+"_company").val("");
        $("#"+type+"_address").val("");
        $("#"+type+"_city").val("");
        $("#"+type+"_state").
        val("");
        $("#"+type+"_zip_code").val("");
        $("#"+type+"_country").val("").select2();
        $("#"+type+"_phone").val("");
        $("#"+type
            +"_country_code").val("");
        $("#"+type+"_area_code").val("");
        $("#"+type+"_phone_info_id").val("");
        if(type=="shipping")
            refreshCartData()
    }
}
$(function(){
    $("#billing_shipping_same").change(function(){
    
        $('input[name="is_apo_po"][value="1"]').attr('checked', false);       
        $('#is_apo_po_0').next().removeClass('checked'); 
        $('input[name="is_apo_po"][value="2"]').attr('checked', false);        
        $('#is_apo_po_1').next().removeClass('checked'); 
        $('input[name="is_apo_po"][value="3"]').attr('checked', false);        
        $('#is_apo_po_2').next().removeClass('checked');     
        $('input[name="is_apo_po"][value="1"]').attr('checked', true);    
                
        $('#is_apo_po_0').next().addClass('checked');
        $('#is_apo_po_0').click();
        $('input[name="is_apo_po"][value="1"]').click();

    
        var type="shipping";
        if($(this).is(
            ":checked")){
            if($("#billing_country").val()==228){
                $("#"+type+"_text_state_block").hide();
                $("#"+type+
                    "_select_state_block").show()
            }else{
                $("#"+type+"_text_state_block").show();
                $("#"+type+"_select_state_block").hide()
            }
            $("#"
                +type+"_title").val($("#billing_title").val()).select2();
            $("#"+type+"_first_name").val($("#billing_first_name").val());
            $
            ("#"+type+"_last_name").val($("#billing_last_name").val());
            $("#"+type+"_company").val($("#billing_company").val());
            $("#"
                +type+"_address").val($("#billing_address").val());
            $("#"+type+"_city").val($("#billing_city").val());
            $("#"+type+"_state"
                ).val($("#billing_state").val());
            $("#"+type+"_state_text").val($("#billing_state_text").val());
            $("#"+type+
                "_state_select").val($("#billing_state_select").val()).select2();
            $("#"+type+"_zip_code").val($("#billing_zip_code").val(
                ));
            $("#"+type+"_country").val($("#billing_country").val()).select2();
            $("#"+type+"_phone").val($("#billing_phone").val())
            ;
            $("#"+type+"_country_code").val($("#billing_country_code").val());
            $("#"+type+"_area_code").val($("#billing_area_code").
                val());
            $("#"+type+"_phone_info_id").val($("#billing_phone_info_id").val())
        }else if($("#shipping_existing_contact").val()
            !="")getAddressInfo($("#shipping_existing_contact").val(),"shipping");
        else{
            $("#"+type+"_title").val("").select2();
            $("#"+
                type+"_first_name").val("");
            $("#"+type+"_last_name").val("");
            $("#"+type+"_company").val("");
            $("#"+type+"_address").val(
                "");
            $("#"+type+"_city").val("");
            $("#"+type+"_state").val("");
            $("#"+type+"_state_text").val("");
            $("#"+type+
                "_state_select").val("").select2();
            $("#"+type+"_zip_code").val("");
            $("#"+type+"_country").val("").select2();
            $("#"+type+
                "_phone").val("");
            $("#"+type+"_country_code").val("");
            $("#"+type+"_area_code").val("");
            $("#"+type+"_phone_info_id").val(
                "");
            $("#"+type+"_text_state_block").show();
            $("#"+type+"_select_state_block").hide()
        }
        refreshCartData()
    });
    $("#pick_up").
    change(function(){
        if($(this).is(":checked"))$("#shipping_address_information").hide();else $(
            "#shipping_address_information").show();
        refreshCartData()
    })
});
function showAfihc(){
    $(".view_afihc_codes").colorbox({
        width:"500px",
        height:"200px",
        inline:true
    })
}
function addProductToCartDetail(){
    $.colorbox({
        href:
        "/add-product-to-cart-detail/"+encryptUserId,
        width:"1300px",
        height:"800px",
        iframe:true
    })
}
function refreshCartData(checkShipping){ 
    var campCode = ($("#campaign_code").val());
    var campId = ($("#campaign_code_id").val());
	if($("input[name=shipping_method]:checked").val()!='custom')
		$("#custom_shipping").val('');
	if($("input[name=shipping_method]:checked").val() == 'custom' && $("#custom_shipping").is(":visible") == false && $("#custom_shipping").val()=='')
	{
		$("#custom_shipping").show();
		return false;
	}
	
	/*if($("input[name=shipping_method]:checked").val() == 'custom') {
		if($("#custom_shipping").val()*1<0 || $.isNumeric($("#custom_shipping").val()) == false) {
				$("#custom_shipping_error").show();
				return false;
			}
		else {	
			$("#custom_shipping_error").hide();	
		}	
	}*/
    showAjxLoader();
    checkShipping=typeof checkShipping!=="undefined"?checkShipping:1;
    var dataStr=$("#create-transaction").
    serialize()+"&check-shipping="+checkShipping,shippingResultResponse="",elemArr;
    $.ajax({
        type:"POST",
        dataType:"html",
        data:
        dataStr,
        async:false,
        url:"/get-crm-cart-products-reload",
        success:function(responseData){
            hideAjxLoader();
            elemArr=$("input:radio[name='shipping_method']");
            if($(elemArr[0]).next().attr("class")=="error")$(elemArr[0]).next().remove();
            if(checkShipping==1){
                $("#shippingMethodError").hide();
                $("#shippingMethodError").html("");
                if($("#shipping_zip_code").next().
                    attr("class")=="error")$($("#shipping_zip_code")).next().remove();
                if($("#shipping_country").next().attr("class")==
                    "error")$($("#shipping_country")).next().remove()
            }
            try{
                responseData=JSON.parse(responseData)
            }catch(e){
                responseData=
                responseData
            }
            if(typeof responseData=="object"){ 
                if(responseData.status=="shipping_error"){
                    $.each(responseData.message,
                        function(i,msg){
                            if(i=="shipping_method"){
                                $("#shippingMethodError").show();
                                $("#shippingMethodError").html(msg)
                            }else{
                                var 
                                elemArr=$("input[name='"+i+"'],select[name='"+i+"']");
                                $(elemArr[0]).after('<label class="error" style="display:block;">'
                                    +msg+"</label>")
                            }
                        });
                    $("#is_apo_block").show();
                    //refreshCartData(0)
                }
            }else{
                elemArr=$("input:radio[name='shipping_method']");
                if($(elemArr[0]).next().attr("class")=="error")$(elemArr[0]).next().remove();
                if(!checkUserAuthenticationAjx(responseData))return false;
                var cartDetailArr=responseData.split("#################################"),cartProductData=cartDetailArr[0].replace("^/s+|/s+$",""),applyCouponData=cartDetailArr[1],shippingMethodData=cartDetailArr[2].replace(/^\s+|\s+$/g,""),
                transactionData=cartDetailArr[3],paymentsData=cartDetailArr[4],validCouponsDetail=cartDetailArr[5],shippingBlockPrevVal;
                $("#productCartDetail").html(cartProductData);
                if($("#coupon_code").val()==""||typeof $("#coupon_code").val()==
                    "undefined"||$("#productCartDetail").html().indexOf("No Product(s) Selected")>0){
                    $("#applyCouponBlock").html(
                        applyCouponData);
                    autoCompleteCoupon()
                }
                $("#transactionTotalBlock").html(transactionData);
                if(!$("#payment_mode_chk_amount_2").val()){
                    if(typeof $("#coupon_code").val()=="undefined"||$("#paymentsBlock").html().indexOf("No Payment Information")>0){
						 $("#paymentsBlock").html(paymentsData);
						 }
						
                }
                $("#applyShippingBlock").html(shippingMethodData);
                
                
                if($("input[name=shipping_method]:checked").val() == 'custom')
						$("#custom_shipping").show();
                //$("#custom_shipping").is(":visible")
                
                if(shippingMethodData.length==0)$("#is_apo_block").hide();else $("#is_apo_block").show();
                if(document.getElementById("applyShippingBlock"))shippingBlockPrevVal=$("#applyShippingBlock").html().replace(/^\s+|\s+$/g,"");
                var numOfShipping=$("input[name='shipping_method']").length;
                if(typeof $("input[name='shipping_method']").val()=="undefined"||typeof $("#coupon_code").val()=="undefined"||shippingBlockPrevVal.length==0&&shippingMethodData.length>0||shippingBlockPrevVal.length>0&&shippingMethodData.length==0||checkShipping==0){}
                shippingResultResponse=shippingMethodData;
                $("#validCouponsBlock").html(validCouponsDetail);
                calculateTransaction();
                
                /*ssssssssssssif(jsonObj.location_name == '3' || jsonObj.location_name == '2,3' || jsonObj.location_name == '1,2,3')
                {
	                var address_type = jsonObj.address_type*1;
	                alert(address_type);
	                if(address_type == 1 || address_type == 0 || address_type == '' || address_type==2){
	                alert("aaa")
	                    $("#"+type+"_city").val(cityVal);
	                }
                }
                */

                
                $("select.e1").select2({});
                $(".e2").customInput()
            }
        }
    });
    $("#campaign_code").val(campCode);
    $("#campaign_code_id").val(campId);
    return shippingResultResponse
}
function changeShippingMethod(value,
    type){
    if(value==228){
        $("#"+type+"_text_state_block").hide();
        $("#"+type+"_select_state_block").show()
    }else{
        $("#"+type+
            "_text_state_block").show();
        $("#"+type+"_select_state_block").hide()
    }
    if(type=="shipping")refreshCartData(1)
}
function changeState(value,type){
    $("#"+type+"_state").val(value)
}
function calculateTransaction(){
    var productSubTotal=parseFloat($
        ("#productSubTotal").text()),productDiscountTotal=parseFloat($("#productDiscountTotal").text()),productTotalAmount=
    parseFloat($("#productTotalAmount").text()),creditAmountArr,balanceAmount=productTotalAmount;
    if($("#payment_mode_cash").is(":checked")){
        var paymentModeCashAmount=isNaN($("#payment_mode_cash_amount").val())||$("#payment_mode_cash_amount").
        val()==""?0:parseFloat($("#payment_mode_cash_amount").val());
        balanceAmount=balanceAmount-paymentModeCashAmount;
        $("#totalCashAmount").text(paymentModeCashAmount.toFixed(2))
    }
    else if($("#payment_mode_cash").is(":unchecked")){
        $("#totalCashAmount").text('0.00');
    }
    if($("#payment_mode_chk").is(":checked")){
        var totalCheckPayment=0,allCheckAmounts=$("input[id^='payment_mode_chk_amount_']");
        $(allCheckAmounts).each(function(){
            var paymentModeCheckAmount=isNaN($(this).val())||$(this).val()==""?0:parseFloat($(this).val());
            totalCheckPayment+=paymentModeCheckAmount
        });
        balanceAmount=balanceAmount-totalCheckPayment;
        $("#totalCheckAmount").text(totalCheckPayment.toFixed(2))
    }
    if(document.getElementById("transaction_amounts"))creditAmountArr=$("#transaction_amounts").val().split(",");
    var totalCreditPayment=0;
    if(creditAmountArr)for(i=0;i<creditAmountArr.length;i++){
        var paymentModeCreditAmount=isNaN(
            creditAmountArr[i])||creditAmountArr[i]==""?0:parseFloat(creditAmountArr[i]);
        totalCreditPayment+=paymentModeCreditAmount
    }
    balanceAmount=balanceAmount-totalCreditPayment;
    $("#totalCreditAmount").text(totalCreditPayment.toFixed(2));
    if(balanceAmount.toFixed(2)=="-0.00")$("#balanceAmount").text("0.00");
	else{ 
		$("#balanceAmount").text(balanceAmount.toFixed(2));
		//ticket 645
		if($("#payment_mode_invoice").is(":checked")){
							 balAmnt = parseFloat($("#balanceAmount").html());
							 $("#amount_invoice_number").val(productTotalAmount.toFixed(2));
							 $("#balanceAmount").html('0.00');
		}
	}
    $("#totalAmount").text(productTotalAmount.toFixed(2));
	
}
function deleteProductToCart(cartId,productTypeId,
    productMappingId,typeOfProduct){
    if(typeOfProduct=="woh")$("#delete_cart_woh_crm").css({
        display:"block"
    });else $(
        "#delete_cart_woh_crm").css({
        display:"none"
    });
    $(".delete_cart_product").colorbox({
        width:"700px",
        height:"200px",
        inline:
        true
    });
    $("#no_delete_cart_product").click(function(){
        $.colorbox.close();
        $("#yes_delete_cart_product").unbind("click");
        return false
    });
    $("#yes_delete_cart_product").click(function(){
        $.colorbox.close();
        $("#yes_delete_cart_product").unbind(
            "click");
        showAjxLoader();
        $.ajax({
            type:"POST",
            dataType:"html",
            async:false,
            data:{
                cartId:cartId,
                productTypeId:productTypeId
                ,
                productMappingId:productMappingId,
                typeOfProduct:typeOfProduct
            },
            url:"/delete-product-to-cart",
            success:function(
                responseData){
                hideAjxLoader();
                if(!checkUserAuthenticationAjx(responseData))return false;
                var jsonObj=JSON.parse(
                    responseData);
                if(jsonObj.status=="success")refreshCartData();else $("#error_message").html(jsonObj.message)
            }
        })
    })
}
var 
totalSaveSearch=1;
function addNewCheckBlock(currAnchor){
    showAjxLoader();
    $.ajax({
        type:"POST",
        url:"/add-check",
        data:{
            totalSaveSearch:totalSaveSearch
        },
        success:function(responseData){
            hideAjxLoader();
            if(!checkUserAuthenticationAjx(
                responseData))return false;
            var allDivBlock=$("div[id^='payment_mode_check_block']");
            if(allDivBlock.length==1){
                var parId=
                $(currAnchor).parents('div[id^="payment_mode_check_block"]').attr("id"),id=parId.substring(parId.lastIndexOf("_")+1,
                    parId.length);
                $(currAnchor).parent().append('<a class="minus" href="javascript:void(0);" onclick="deleteCheckBlock('+id+
                    ')"></a>')
            }
            $(currAnchor).remove();
            $("#checkAmountDiv").append(responseData);
            totalSaveSearch=totalSaveSearch+1
        }
    })
}
function deleteCheckBlock(id){
    $("#payment_mode_check_block_"+id).remove();
    var lastBlock=$(
        "div[id^='payment_mode_check_block']").last();
    if($(lastBlock).find(".plus").length==0)$(lastBlock).children("div").last(
        ).children("div").last().append(
        '<a class="plus plus m-l-5" href="javascript:void(0);" onclick="addNewCheckBlock(this)" id="addMoreCheckBlock"></a>');
    var allDivBlock=$("div[id^='payment_mode_check_block']");
    if(allDivBlock.length==1)$(lastBlock).find(".minus").remove();
    calculateTransaction()
}
$(function(){
    // setTimeout(function(){refreshCartData(1)},1000);
    jQuery.validator.addMethod("alpha",
        function(value,element){
            return this.optional(element)||/^[a-zA-Z \'(),.-]+$/i.test(value)},ALPHABET_ONLY);jQuery.
        validator.addMethod("alphanumericspecialchar",function(value,element){
            return this.optional(element)||
            /^[A-Za-z0-9\n\r\' ,.-]+$/i.test(value)},ALPHA_NUMERIC_SPECIALSYMPOL);jQuery.validator.addMethod("phoneno",function(
        value,element){
            return this.optional(element)||/^[0-9-+]+$/i.test(value)
        },ALPHABET_ONLY);
    jQuery.validator.addMethod("threeDigit",function(value,element){
        return this.optional(element)||element.value.length==3||element.value.length==4
    },CVV_VALID); 
    jQuery.validator
    .addMethod("checkValidAmount",function(value,element){
        if(parseInt(value)==0||parseInt(value)=="0.0"||parseInt(value)=="0.00")return false;else return true},"Please enter valid value.");
    $("#payment_mode_credit_form").validate({
        submitHandler:function(){
            $("#card_number").validateCreditCard(function(result){
                if(result.card_type)$("#credit_card_type"
                    ).val(result.card_type.name)
            });
            var dataStr=$("#payment_mode_credit_form").serialize()+"&"+parent.$("#create-transaction"
                ).serialize()+"&billing_country_auth_text="+$("#billing_country_auth option:selected").text()+"&billing_country_text="+$
            ("#billing_country option:selected").text();
            showAjxLoader();
            $.ajax({
                type:"POST",
                url:"/make-authorize-payment",
                data:
                dataStr,
                success:function(responseData){
                    $("#credit_card_type").val("");
                    hideAjxLoader();
                    if(!checkUserAuthenticationAjx(
                        responseData))return false;
                    var jsonObj=jQuery.parseJSON(responseData);
                    if(jsonObj.status=="success"){
                        if(jsonObj.
                            typePayment=="PledgeTransaction"){
                            parent.$("#success_message").show();
                            parent.$("#success_message").html(jsonObj.message);
                            parent.jQuery("#list").trigger("reloadGrid",[{
                                page:1
                            }])
                        }else{
                            $("#credit_card_error").html("").hide();
                            var creditCardString='<div class="full m-t-5" id="payment_'+jsonObj.paymentProfileId+'">',amount="",cardNumber="";
                            if(jsonObj.typePayment=="NewProfile"){
                                amount=$("#amount").val();
                                cardNumber="XXXX"+$("#card_number").val().substr($("#card_number")
                                    .val().length-4)
                            }else{
                                amount=$("#exist_amount").val();
                                cardNumber=$("#existing_profile_id option:selected").text()
                            }
                            if(
                                parent.$("#transaction_ids").val()==""){
                                parent.$("#transaction_ids").val(jsonObj.transaction_id);
                                parent.$("#transaction_amounts").val(amount);
                                parent.$("#customer_profile_ids").val(jsonObj.customerProfileId)
                            }else{
                                parent.$("#transaction_ids").val(parent.$("#transaction_ids").val()+","+jsonObj.transaction_id);
                                parent.$("#transaction_amounts").
                                val(parent.$("#transaction_amounts").val()+","+amount);
                                parent.$("#customer_profile_ids").val(parent.$("#customer_profile_ids").val()+","+jsonObj.customerProfileId)
                            }
                            creditCardString+="<label>"+L_AMOUNT_SIMPLE+" : "+
                            CURRENCY_SYMBOL+amount+"</label>";
                            creditCardString+='<label class="auto">'+CARD_NUMBER+": "+cardNumber+
                            "<br>Authorization Number: "+jsonObj.transaction_id+"</label>";
                            parent.$("#addMoreCredit").remove();
                            parent.$("#payment_mode_credit").attr("disabled","disabled");
                            creditCardString+='<div class="common-form m-t-5" style="width:auto;"><a id="deleteMoreCredit" onclick="deleteCredit(\''+jsonObj.
                            transaction_id+"','"+jsonObj.paymentProfileId+"','"+jsonObj.customerProfileId+
                            '\')" href="#delete_credit_card" class="minus m-l-5 delete_credit_card_detail"></a><a id="addMoreCredit" onclick="addMoreCredit(this)" href="javascript:void(0);" class="plus m-l-5"></a></div>'
                            ;
                            creditCardString+="</div>";
                            parent.$("#credit_card_block").append(creditCardString);
                            parent.refreshCartData()
                        }
                        parent.$.colorbox.close()
                    }else if(jsonObj.status=="credit_card_error")$("#credit_card_error").html(jsonObj.message["0"]).show();
                    else{
                        $("#credit_card_error").html("").hide();
                        $.each(jsonObj.message,function(i,msg){
                            $("#"+i).after(
                                '<label class="error" style="display:block;">'+msg+"</label>")
                        })
                    }
                }
            })
        },
        rules:{
            card_holder_name:{
                required:true,
                minlength:2
                ,
                maxlength:50,
                alpha:true
            },
            card_number:{
                required:true,
                minlength:13,
                maxlength:16,
                digits:true
            },
            expiry_month:{
                required:true
            }
            ,
            expiry_year:{
                required:true
            },
            /* cvv_number:{
                required:true,
                threeDigit:true,
                digits:true
            },*/
            amount:{
                required:true,
                floating:true,
                minlength:1,
                maxlength:10
            },
            billing_address_1:{
                required:true //,
            // alphanumericspecialchar:true
            },
            billing_city_auth:{
                required:
                true,
                minlength:2,
                maxlength:30,
                letterOnlyHyphen:true
            },
            billing_state_auth:{
                /* required:function(){
                    if($("#billing_country_auth").val()==
                        228)return false;
                    return true
                },*/
                minlength:2,
                maxlength:25,
                alpha:true
            },
            billing_state_auth_select:{
                required:function(){
                    if($(
                        "#billing_country_auth").val()==228)return true;
                    return false
                }
            },
            billing_zip_code_auth:{
                required:true,
                minlength:4,
                maxlength:12,
                alphanumerichypenchar:true
            },
            billing_country_auth:{
                required:true
            },
            exist_amount:{
                required:true,
                floating:true,
                minlength:1,
                maxlength:10,
                checkValidAmount:true
            },
            existing_profile_id:{
                required:true
            }
        },
        messages:{
            card_holder_name:{
                required:CARD_HOLDER_NAME_EMPTY
            },
            card_number:{
                required:CARD_NUMBER_EMPTY,
                digits:CARD_NUMBER_DIGIT
            },
            expiry_month:{
                required:EXPIRY_MONTH_EMPTY
            },
            expiry_year:{
                required:EXPIRY_YEAR_EMPTY
            },
            /* cvv_number:{
                required:CVV_EMPTY,
                threeDigit:
                CVV_VALID,
                digits:CVV_VALID
            }, */
            amount:{
                required:AMOUNT_EMPTY
            },
            billing_address_1:{
                required:BILLING_ADDRESS_EMPTY
            },
            billing_city_auth:{
                required:BILLING_CITY_EMPTY
            },
            billing_state_auth:{
            //required:BILLING_STATE_EMPTY
            },
            billing_state_auth_select:{
                required:BILLING_STATE_EMPTY_SELECT
            },
            billing_zip_code_auth:{
                required:BILLING_ZIP_CODE_EMPTY
            },
            billing_country_auth:{
                required:BILLING_COUNTRY_EMPTY
            },
            exist_amount:{
                required:AMOUNT_EMPTY
            },
            existing_profile_id:{
                required
                :CREDIT_CART_EXIST_EMPTY
            }
        }
    });
    $("#exist_cancel,#cancel,#cboxClose").click(function(){
        if(typeof parent.$("#pledge_status")
            !="undefined")parent.$("#pledge_status").val("0").select2({});
        parent.$.colorbox.close()
    });
    
    jQuery.validator.addMethod("letterOnlyHyphen", function(value, element) {
        return this.optional(element) || /^[a-z .-]+$/i.test(value);
    },"Please enter valid details.");
        
    jQuery.validator.addMethod("lettersandapostropheonlyfl", function(value, element) {
        return this.
        optional(element) || /^[a-z '\-]+$/i.test(value)
    }, "Please enter valid details.");

    $("#create-transaction").validate({
        submitHandler:function(){
            
            if($("#payment_mode_cash").next().attr("class")=="error")$("#payment_mode_cash").next().remove("span");
            if(isCustomProductAdded=="yes")if($("#shipping_country").val()=="228"||$("#shipping_country").val()=="39"){
                
            }
            else{
                $("#shipping_country").after(
                    '<label class="error" style="display:block;">International shipment is not available for custom frame.</label>');
                return false
            }
            /*if($("#shipping_country").val()=="228" && $("#billing_state").val() == ''){
                $("#billing_state_select").after(
                    '<label class="error" style="display:block;">Please select any state.</label>');
                return false
            }*/
			if($('#is_promotion_applied').val() == ''){
				$('#coupon_code').val('');
				$('#promotion_id').val('');
			}
            if(checkCustomValidation()){
                $('#transactionsubmit').hide();
                showAjxLoader();
                if($("#shipping_zip_code").next().attr("class")=="error")$($(
                    "#shipping_zip_code")).next().remove();
                if($("#shipping_country").next().attr("class")=="error")$($("#shipping_country"))
                    .next().remove();
                var billing_country_text=$("#billing_country option:selected").val()!=""?$(
                    "#billing_country option:selected").text():"",shipping_country_text=$("#shipping_country option:selected").val()!=""?$(
                    "#shipping_country option:selected").text():"",apo_po_shipping_country_text=$("#apo_po_shipping_country option:selected"
                    ).val()!=""?$("#apo_po_shipping_country option:selected").text():"",dataStr=$("#create-transaction").serialize()+
                "&billing_country_text="+billing_country_text+"&shipping_country_text="+shipping_country_text+
                "&apo_po_shipping_country_text="+apo_po_shipping_country_text;
                $.ajax({
                    type:"POST",
                    url:"/save-transaction",
                    data:dataStr,
                    success:function(responseData){
                        hideAjxLoader();
                        if(!checkUserAuthenticationAjx(
                            responseData))return false;
                        var jsonObj=jQuery.parseJSON(responseData);
                        if(jsonObj.status=="success"){
                            loadPage=1;
                            window.location.href="http://"+requestUrl+"/crm-transactions";
                        }else if(jsonObj.status=="quantity_error"){
                            var messagestr="";
                            $.each(jsonObj.message,
                                function(i){
                                    var pid=i,message=this+"<br>";
                                    messagestr+=message
                                });
                            $("#product_quantity_error").html(messagestr).show();
                            window.scrollTo(0,0)
                            
                            $("#transactionsubmit").show();
                        }else if(jsonObj.status=="shipping_error")$.each(responseData.message,function(i,msg){
                            var elemArr=$(
                                "input[name='"+i+"']");
                            $(elemArr[0]).after('<label class="error" style="display:block;">'+msg+"</label>")
                        });else $.each(
                            jsonObj.message,function(i,msg){
                                $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                            })
                    }
                })
            }
        },
        rules:{
            billing_first_name:{
                required:true,
                minlength:2,
                maxlength:50,
                lettersandapostropheonlyfl:true
            },
            billing_last_name:{
                required:true,
                minlength
                :1,
                maxlength:20,
                lettersandapostropheonlyfl:true
            },
            billing_address:{
                required:true //,
            //alphanumericspecialchar:true
            },
            billing_city:{
                required:true,
                minlength:2,
                maxlength:30,
                letterOnlyHyphen:true
            },
            billing_state_text:{
                minlength:2,
                maxlength:25,
                alpha:true
            },
            billing_state_select:{
                required:function(){
                    if($("#billing_country").val()==228)return true;
                    return false
                }
                // required:true
            },
            billing_zip_code:{
                required:true,
                minlength:4,
                maxlength:12,
                alphanumerichypenchar:true
            },
            billing_country:{
                required:true
            },
            billing_country_code:{
                phoneno:true,
                minlength:1,
                maxlength:5
            },
            billing_area_code:{
                number:
                true,
                minlength:1,
                maxlength:5
            },
            billing_phone:{
                number:true,
                minlength:1,
                maxlength:10
            },
            shipping_first_name:{
                required:true,
                minlength:2,
                maxlength:50,
                lettersandapostropheonlyfl:true
            },
            shipping_last_name:{
                required:true,
                minlength:1,
                maxlength:20,
                lettersandapostropheonlyfl:true
            },
            shipping_address:{
                required:true //,
            //alphanumericspecialchar:true
            },
            shipping_city:{
                required:true,
                minlength:2,
                maxlength:30,
                letterOnlyHyphen:true
            },
            shipping_state_text:{
                minlength:2,
                maxlength:25,
                alpha:true
            },
            shipping_state_select:{
                required:true
            },
            shipping_zip_code:{
                required:true,
                minlength:4,
                maxlength:12,
                alphanumerichypenchar:true
            },
            shipping_country:{
                required:true
            }
            ,
            shipping_country_code:{
                required:true,
                phoneno:true,
                minlength:1,
                maxlength:5
            },
            shipping_area_code:{
                required:true,
                number:
                true,
                minlength:3,
                maxlength:5
            },
            shipping_phone:{
                required:true,
                number:true,
                minlength:7,
                maxlength:10
            },
            payment_mode_cash_amount:{
                floating:true,
                minlength:1,
                maxlength:10
            },
			invoice_number:{
                required:function(){
                    if($("#payment_mode_invoice").is(":checked"))return true
                    return false
                },
                minlength:function(){
                    if($("#payment_mode_invoice").is(":checked"))return 3
                    return 0
                },
                maxlength:function(){
                    if($("#payment_mode_invoice").is(":checked"))return 15
                    return 0
                }
            }
        },
        messages:{
            billing_first_name:{
                required:
                BILLING_FIRST_NAME_EMPTY
            },
            billing_last_name:{
                required:BILLING_LAST_NAME_EMPTY
            },
            billing_address:{
                required:
                BILLING_ADDRESS_EMPTY
            },
            billing_city:{
                required:BILLING_CITY_EMPTY
            },
            billing_state_text:{
            //  required:BILLING_STATE_EMPTY
            },
            billing_state_select:{
                required:BILLING_STATE_EMPTY_SELECT
            },
            billing_zip_code:{
                required:BILLING_ZIP_CODE_EMPTY,
                alphanumerichypenchar:ZIP_VALID
            },
            billing_country:{
                required:BILLING_COUNTRY_EMPTY
            },
            billing_country_code:{
                phoneno:
                PHONE_DIGITS,
                minlength:PHONE_DIGITS_COUNTRY_CODE_MIN,
                maxlength:PHONE_DIGITS_COUNTRY_CODE_MAX
            },
            billing_area_code:{
                number:
                PHONE_DIGITS,
                minlength:PHONE_DIGITS_AREA_CODE_MIN,
                maxlength:PHONE_DIGITS_AREA_CODE_MAX
            },
            billing_phone:{
                number:
                PHONE_DIGITS,
                minlength:PHONE_DIGITS_PHONE_NUMBER_MIN,
                maxlength:PHONE_DIGITS_PHONE_NUMBER_MAX
            },
            shipping_first_name:{
                required:SHIPPING_FIRST_NAME_EMPTY
            },
            shipping_last_name:{
                required:SHIPPING_LAST_NAME_EMPTY
            },
            shipping_address:{
                required:
                SHIPPING_ADDRESS_EMPTY
            },
            shipping_city:{
                required:SHIPPING_CITY_EMPTY
            },
            shipping_state_text:{
            //required:SHIPPING_STATE_EMPTY
            }
            ,
            shipping_state_select:{
                required:SHIPPING_STATE_EMPTY_SELECT
            },
            shipping_zip_code:{
                required:SHIPPING_ZIP_CODE_EMPTY
            },
            shipping_country:{
                required:SHIPPING_COUNTRY_EMPTY
            },
            shipping_country_code:{
                required:SHIPPING_COUNTRY_CODE_EMPTY,
                phoneno:
                PHONE_DIGITS,
                minlength:PHONE_DIGITS_COUNTRY_CODE_MIN,
                maxlength:PHONE_DIGITS_COUNTRY_CODE_MAX
            },
            shipping_area_code:{
                required:SHIPPING_AREA_CODE_EMPTY,
                number:PHONE_DIGITS,
                minlength:PHONE_DIGITS_AREA_CODE_MIN,
                maxlength:
                PHONE_DIGITS_AREA_CODE_MAX
            },
            shipping_phone:{
                required:SHIPPING_PHONE_NUMBER_EMPTY,
                number:PHONE_DIGITS,
                minlength:
                PHONE_DIGITS_PHONE_NUMBER_MIN,
                maxlength:PHONE_DIGITS_PHONE_NUMBER_MAX
            },
			invoice_number:{
                required:INVOICE_NUMBER_EMPTY,
                minlength:INVOICE_NUMBER_MIN,
                maxlength:INVOICE_NUMBER_MAX,
            }
        }
    });
    $("#billing_country").change(function(){
        $("#billing_state").val('');
        $("#billing_state_text").val('');
        $("#billing_state_select").val('').select2();
        
    });
    $("#shipping_country").change(function(){
        $("#shipping_state").val('');
        $("#shipping_state_text").val('');
        $("#shipping_state_select").val('').select2();
        
    });
    
    function checkCustomValidation(){
        var 
        flagChkAmount=true,paymentModeChkAmounts=$('#create-transaction [name="payment_mode_chk_amount[]"]'),paymentModeChkTypes
        =$('#create-transaction [name="payment_mode_chk_type[]"]'),paymentModeChkNums=$(
            '#create-transaction [name="payment_mode_chk_num[]"]'),paymentModeChkIdTypes=$(
            '#create-transaction [name="payment_mode_chk_id_type[]"]'),paymentModeChkIds=$(
            '#create-transaction [name="payment_mode_chk_id[]"]');
        $(paymentModeChkAmounts).each(function(index){
            var str=$(this).val(
                );
            if(str!=""){
                var regx=/^-?\d*(\.\d+)?$/;
                if(str.match(regx)&&$(this).val().length>=1&&$(this).val().length<=10)$(this).
                    next().remove("span");
                else{
                    if($(this).next("span").length==0)$(this).after(
                        '<span class="error" for="payment_mode_chk_amount_'+(index+1)+
                        '" generated="true" style="display:block !important;"><span class="redarrow"></span>'+CHECK_AMOUNT_VALID+"</span>");
                    flagChkAmount=false
                }
                if(paymentModeChkTypes[index].value==""||paymentModeChkTypes[index].value==0){
                    if($(
                        paymentModeChkNums[index]).next("span").length==0)$(paymentModeChkNums[index]).after(
                        '<span class="error" for="payment_mode_chk_type_'+(index+1)+
                        '" generated="true" style="display:block !important;"><span class="redarrow"></span>'+CHECK_TYPE_EMPTY+"</span>");
                    flagChkAmount=false
                }else $(paymentModeChkTypes[index]).next().remove("span");
                var regxCheckNum=/^[0-9]+$/;
                if(
                    paymentModeChkNums[index].value.match(regxCheckNum)&&paymentModeChkNums[index].value!=""&&paymentModeChkNums[index].
                    value.length>=4&&paymentModeChkNums[index].value.length<=20)$(paymentModeChkNums[index]).next().remove("span");
                else{
                    if($(
                        paymentModeChkNums[index]).next("span").length==0)$(paymentModeChkNums[index]).after(
                        '<span class="error" for="payment_mode_chk_num_'+(index+1)+
                        '" generated="true" style="display:block !important;"><span class="redarrow"></span>'+CHECK_NUMBER_EMPTY+"</span>");
                    flagChkAmount=false
                }
                if(paymentModeChkIdTypes[index].value==""||paymentModeChkIdTypes[index].value==0){
                    if($(
                        paymentModeChkIdTypes[index]).next("span").length==0)$(paymentModeChkIdTypes[index]).after(
                        '<span class="error" for="payment_mode_chk_id_type'+(index+1)+
                        '" generated="true" style="display:block !important;"><span class="redarrow"></span>'+CHECK_ID_TYPE_EMPTY+"</span>");
                    flagChkAmount=false
                }else $(paymentModeChkIdTypes[index]).next().remove("span")
            }
        });
        if($("#totalProducts").val()!=$(
            "#shipCompleteStatus").val()&&typeof $("#pick_up")!="undefined"&&!$("#pick_up").is(":checked")&&$(
            "input:radio[name='shipping_method']:checked").length==0&&!isFreeShipping){
            var shippingElement=$(
                "input:radio[name='shipping_method']").first();
            $(shippingElement).after('<label class="error" style="display:block;">'+
                SHIPPING_METHOD_EMPTY+"</label>");
            flagChkAmount=false
        }else{
            var elemArr=$("input:radio[name='shipping_method']");
            if($(
                elemArr[0]).next().attr("class")=="error")$(elemArr[0]).next().remove()
        }
        var balanceAmount=parseFloat($("#balanceAmount")
            .text());
        $("span[for='payment_mode_cash']").remove("span");
        if($("#payment_mode_cash").next().attr("class")=="error")$(
            "#payment_mode_cash").next().remove("span");
        if(balanceAmount>0){
            $("#balanceAmount").after(
                '<span class="error" for="payment_mode_cash" generated="true" style="display:block !important;"><span class="redarrow"></span>'
                +BALANCE_AMOUNT_REMAINING+"</span>");
            window.scroll($("#payment_mode_cash").offset().left,$("#payment_mode_cash").offset(
                ).top-50);
            flagChkAmount=false
        }else if(balanceAmount<0){
            var exceedAmountMessage="";
            if($("#transaction_amounts").val()==""
                )exceedAmountMessage=CASH_BALANCE_AMOUNT_EXCEED;else exceedAmountMessage=CREDIT_BALANCE_AMOUNT_EXCEED;
            $(
                "#payment_mode_cash_amount").after(
                '<span class="error" for="payment_mode_cash" generated="true" style="display:block !important;"><span class="redarrow"></span>'
                +exceedAmountMessage+"</span>");
            window.scroll($("#payment_mode_cash").offset().left,$("#payment_mode_cash").offset().top
                -50);
            flagChkAmount=false
        }
        return flagChkAmount
    }
    var changeShippingZip=false;
    $("#shipping_zip_code").change(function(){
        changeShippingZip=true
    });
    $("#shipping_zip_code").blur(function(){
        if(changeShippingZip){
            refreshCartData();
            changeShippingZip=false
        }
    })
});
function addMoreCredit(currRef){
    $.colorbox({
        href:"/get-credit-card-detail/"+encryptUserId,
        width:"900px",
        height:"800px",
        iframe:true
    })
}
function deleteCredit(authTransactionId,paymentProfileId,customerProfileId){
    $
    (".delete_credit_card_detail").colorbox({
        width:"700px",
        height:"200px",
        inline:true
    });
    $("#no_delete_credit").click(
        function(){
            $.colorbox.close();
            $("#yes_delete_credit").unbind("click");
            return false
        });
    $("#yes_delete_credit").click(
        function(){
            $.colorbox.close();
            $("#yes_delete_credit").unbind("click");
            showAjxLoader();
            $.ajax({
                type:"POST",
                dataType:
                "html",
                data:{
                    paymentProfileId:paymentProfileId,
                    customerProfileId:customerProfileId
                },
                url:"/delete-credit-card",
                success:
                function(responseData){
                    hideAjxLoader();
                    if(!checkUserAuthenticationAjx(responseData))return false;
                    var jsonObj=JSON.parse(
                        responseData);
                    if(jsonObj.status=="success"){
                        var authTranIdsArr=$("#transaction_ids").val().split(","),
                        transactionAmountsArr=$("#transaction_amounts").val().split(","),customerProfileIds=$("#customer_profile_ids").val().
                        split(","),newAuthTranIds="",newTransactionAmounts="",newCustomerProfileIds="";
                        for(i=0;i<authTranIdsArr.length;i++)if(
                            authTransactionId!=authTranIdsArr[i]){
                            newAuthTranIds+=authTranIdsArr[i]+",";
                            newTransactionAmounts+=transactionAmountsArr
                            [i]+",";
                            newCustomerProfileIds+=customerProfileIds[i]+","
                        }
                        newAuthTranIds=newAuthTranIds.replace(",$","");
                        newTransactionAmounts=newTransactionAmounts.replace(",$","");
                        newCustomerProfileIds=newCustomerProfileIds.replace(",$",""
                            );
                        $("#transaction_ids").val(newAuthTranIds);
                        $("#transaction_amounts").val(newTransactionAmounts);
                        $(
                            "#customer_profile_ids").val(newCustomerProfileIds);
                        $("#payment_"+paymentProfileId).remove();
                        if($("#credit_card_block").
                            html().indexOf("Amount")<0){
                            $("#payment_mode_credit").removeAttr("checked");
                            $("#payment_mode_credit").removeAttr(
                                "disabled").customInput()
                        }
                        var addBlock=
                        '<a id="addMoreCredit" onclick="addMoreCredit(this)" href="javascript:void(0);" class="plus m-l-5"></a>',allCreditBlock=
                        $("#addMoreCredit",$("#credit_card_block > div").last()).length;
                        if(allCreditBlock==0)$("#deleteMoreCredit",$(
                            "#credit_card_block > div")).parent().append(addBlock);
                        if($("#payment_mode_cash").next().attr("class")=="error")$(
                            "#payment_mode_cash").next().remove("span");
                        refreshCartData()
                    }
                }
            })
        })
}
function viewFlagOfFacesCart(cartFofId){
    $.colorbox({
        href:"/view-fof-image-cart/"+cartFofId,
        width:"350px",
        height:"350px",
        iframe:true
    })
}
function applyCoupon(){
    if($("#promotion_id").val()!=""){
        showAjxLoader();
        var dataStr=$("#create-transaction").serialize();
        $.ajax({
            type:"POST",
            url: "/apply-coupon",
            data:dataStr,
            success:function(responseData){
                hideAjxLoader();
                if(!checkUserAuthenticationAjx(responseData))
                    return false;
                var jsonObj=jQuery.parseJSON(responseData);
                if(jsonObj.status=="success"){
                    $("#apply_coupon_msg").show();
                    $("#apply_coupon_msg").html(jsonObj.message);
                    $("#coupon_code").next("label").remove();
                    $("#is_promotion_applied").val(1);
                    parent.refreshCartData()
                }else{
                    $("#apply_coupon_msg").hide();
                    $("#coupon_code").next("label").remove();
                    $("#coupon_code").val('');
                    $("#promotion_id").val('');
					$("#is_promotion_applied").val('');
					refreshCartData()
                    $.each(jsonObj.message,function(i,msg){
                        $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                    })
                    
                }
            }
        })
    }
}
function onChangeCountry(countryId){
    if(countryId==
        228){
        $("#state_text_block").hide();
        $("#state_select_block").show()
    }else{
        $("#state_text_block").show();
        $(
            "#state_select_block").hide()
    }
}
function chooseCoupon(couponCode,promotionId){
    $("#coupon_code").val(couponCode);
    $(
        "#promotion_id").val(promotionId)
}
function changeApoPo(shippingAddressType){

    if(shippingAddressType==1){
        $(
            "#country_block").show();
        $("#apo_po_country_block").hide();
        if($("#shipping_country").val()==228){
            $(
                "#shipping_text_state_block").hide();
            $("#shipping_select_state_block").show()
        }else{
            $("#shipping_text_state_block").show(
                );
            $("#shipping_select_state_block").hide()
        }
        $("#shipping_apo_po_state_block").hide();
        $("#shipping_city").val(cityVal);
        $(
            "#shipping_city").removeAttr("readonly")
    }else{
        $("#country_block").hide();
        $("#apo_po_country_block").show();
        $(
            "#shipping_city").val("");
        $("#shipping_text_state_block").hide();
        if(shippingAddressType==3){
            $("#shipping_city").val(
                "APO");
            $("#shipping_city").attr("readonly","readonly");
            $("#shipping_select_state_block").hide();
            $(
                "#shipping_apo_po_state_block").show()
        }else{
            $("#shipping_city").removeAttr("readonly");
            $("#shipping_city").val(cityVal);
            $("#shipping_select_state_block")
            .show();
            $("#shipping_apo_po_state_block").hide()
        }
    }
    //alert("78787878");
    //refreshCartData(1)
}

function showResult()
{
    var listTransactionContactGrid=jQuery("#list_transaction_contact"),emptyMsgDiv=$('<div class="no-record-msz">'+ NO_RECORD_FOUND+"</div>");
    $("#list_transaction_contact").jqGrid({
        mtype:"POST",
        url:"/create-crm-transaction",
        datatype:
        "json",
        sortable:true,
        postData:{
            searchString:$("#search_user").serialize()
        },
        colNames:[L_MEMBERSHIP_AFIHC_HASH,
        L_CONTACT_ID,L_NAME,L_EMAIL,L_PHONE,L_POSTAL_CODE,L_STATE,L_COUNTRY],
        colModel:[{
            name:"user_membership_id,afihc_numbers",
            index:"user_membership_id,afihc_numbers"
        },{
            name:"contact_id"
        },{
            name:"full_name",
            index:"full_name"
        },{
            name:"users.email_id",
            index:"users.email_id"
        },{
            name:"phone.phone_number",
            index:"phone.phone_number"
        },{
            name:"address.zip_code"
            ,
            index:"address.zip_code"
        },{
            name:"address.state",
            index:"address.state"
        },{
            name:"country_name",
            index:"country_name"
        }],
        viewrecords:true,
        sortname:"users.modified_date",
        sortorder:"desc",
        rowNum:numRecPerPage,
        rowList:pagesArr,
        pager: "#listtransactioncontactrud",
        viewrecords:true,
        autowidth:true,
        shrinkToFit:true,
        caption:"",
        width:"100%",
        cmTemplate:{
            title:
            false
        },
        loadComplete:function(){
            hideAjxLoader();
            var count=listTransactionContactGrid.getGridParam(),ts=
            listTransactionContactGrid[0];
            if(ts.p.reccount===0){
                listTransactionContactGrid.hide();
                emptyMsgDiv.show();
                $("#listtransactioncontactrud_right div.ui-paging-info").css("display","none")
            }else{
                listTransactionContactGrid.show();
                emptyMsgDiv.hide();
                $("#listtransactioncontactrud_right div.ui-paging-info").css("display","block")
            }
        }
    });
    emptyMsgDiv.insertAfter(listTransactionContactGrid.parent());
    emptyMsgDiv.hide();
    $("#list_transaction_contact").jqGrid("navGrid",
        "#listtransactioncontactrud",{
            reload:true,
            edit:false,
            add:false,
            search:false,
            del:false
        });
}

function addShippingValue()
{

	if($("input[name=shipping_method]:checked").val() == 'custom') {
		if($("#custom_shipping").val()*1<0 || $.isNumeric($("#custom_shipping").val()) == false) {
				$("#custom_shipping_error").show();
				return false;
			}
		else {	
				$("#custom_shipping_error").hide();	
				refreshCartData()
		}	
	}

}


function showCustomField()
{
	if($('#custom_radio').is(':checked'))
	{
				$("#custom_shipping").show();
		}
	else	
		$("#custom_shipping").hide();
}
function deleteCreditCardContent(){ 

$.colorbox({width:"700px",height:"200px",inline:true,href:".delete_credit_card_invoice_content"});
    $(".delete_credit_card_invoice_content").show();
    $("#cboxClose").on("click",function(){$(".delete_credit_card_invoice_content").hide()})
	
	 $("#yes_delete_credit_invoice").click(function(){
        $('#yes_delete_credit_invoice').unbind('click');
		$.colorbox.close();
		$('.delete_credit_card_invoice_content').hide();
    }); 
}