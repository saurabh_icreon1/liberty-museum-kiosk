$(function(){if(mode=="view")viewTransactionDeatil();else getShipmentDetail();$("a[id^='tab-']").removeClass("active");
var tab_id_load=mode=="view"?"1":"2";$("#tab-"+tab_id_load).addClass("active");$("a[id^='tab-']").click(function(){$(
"a[id^='tab-']").removeClass("active");$(this).addClass("active")})});function viewTransactionDeatil(){showAjxLoader();$
.ajax({type:"POST",data:{},url:"/view-transaction/"+encrypt_transactionId,success:function(response){hideAjxLoader();if(
!checkUserAuthenticationAjx(response))return false;$("#transactionContent").html(response)}})}function getShipmentDetail
(){showAjxLoader();$.ajax({type:"POST",data:{},url:"/view-shipment/"+encrypt_transactionId,success:function(response){
hideAjxLoader();if(!checkUserAuthenticationAjx(response))return false;$("#transactionContent").html(response)}})}
function viewChangeLog(){showAjxLoader();$.ajax({type:"POST",data:{module:moduleName,id:encrypt_transactionId},url:
"/transaction-change-log/"+moduleName+"/"+encrypt_transactionId,success:function(response){hideAjxLoader();if(!
checkUserAuthenticationAjx(response))return false;$("#transactionContent").html(response)}})}

function addTransactionCase(targeted_url){
    showAjxLoader();
    $.ajax({
        type:"GET",
        data:{},
        url:targeted_url,
        success:function(
            response){
            if(!checkUserAuthenticationAjx(response))return false;
            else{
                hideAjxLoader();
                $.colorbox.close();
                $.colorbox({
                    width:"1000px",
                    height:"700px",
                    iframe:true,
                    href:targeted_url
                })
            }
        }
    })
}