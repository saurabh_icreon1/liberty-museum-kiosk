function dataShowPost(){
    $("#previewFOF1").contents().find("#showTagsId").css({
        display:"block"
    });
    $("#previewFOF1").
    contents().find("#addTagsId").css({
        display:"none"
    });
    $("#previewFOF1").contents().find("#mapper").hide();
    var arrNames=[],
    arrNamesSerialize=[],htmlNewBox="",countS=0;
    $("#previewFOF1").contents().find("#planetmap > div").each(function(){
        arrNames.push($.trim($(this).find("div.tagged_title > span.tagged_title_text").html()));
        htmlNewBox+='<div class="'+$.
        trim($(this).attr("class").replace("tagged",""))+' tages" >'+$.trim($(this).find(
            "div.tagged_title > span.tagged_title_text").html())+'<a class="removeTag" onclick="javascript:removePeoplePhotoTag(\''+
        $.trim($(this).attr("class").replace("tagged",""))+'\');" delid="'+$.trim($(this).attr("class").replace("tagged",""))+
        '" >X</a>'+"</div>";
        arrNamesSerialize[countS]={
            name:$.trim($(this).find("div.tagged_title > span.tagged_title_text").
                html()),
            width:$(this).css("width"),
            height:$(this).css("height"),
            top:$(this).css("top"),
            left:$(this).css("left")
        };
            
        countS=
        parseInt(countS)+parseInt(1)
    });
    if(arrNamesSerialize.length>0)$("#people_photo").val(JSON.stringify(arrNamesSerialize));
    else $("#people_photo").val("");
    $("#peoplePhotoBox").html(htmlNewBox)
}
function removePeoplePhotoTag(delid){
    $(
        "#previewFOF1").contents().find("."+delid).remove();
    dataShowPost()
}
function relatedProductAutocomplete(){
if($("#user_id"))
        var shop_user_id = $("#user_id").val();

    $(
        "#related_product_name").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:function(request,response){
            $
            ("#related_product_name").addClass("auto-suggest-loading");
            $.ajax({
                url:"/get-related-products",
                method:"POST",
                dataType:
                "json",
                data:{
                    related_product_name:$("#related_product_name").val(),
                    user_id:shop_user_id
                },
                success:function(jsonResult){
                    $(
                        "#related_product_name").removeClass("auto-suggest-loading");
                    if(!checkUserAuthenticationAjx(jsonResult))return false;
                    var
                    resultset=[];
                    $.each(jsonResult,function(){
                        resultset.push({
                            id:this.product_id,
                            value:this.product_name,
                            sellprice:this.
                            product_sell_price
                        })
                    });
                    response(resultset)
                }
            })
        },
        open:function(){
            $(".ui-menu").width(350)
        },
        change:function(event,ui){
            $("#related_product_name").val("")
        },
        select:function(event,ui){
            var product=ui.item.value,productId=ui.item.id,sellprice=ui.item.sellprice,size=$("#product"+productId).size();
            if(size==0)if(productId!="No Record Found"){
                $("#related_select_product").append("<li id='related_product"+productId+"'>"+product+" Each ($"+sellprice+")<span onclick='removeAutoCompleteValue("+productId+',"related_select_product")\'>x</span></li>');
                var productIdsVal=$("#related_product_name_ids").val();
                if(productIdsVal=="")
                    $("#related_product_name_ids").val(productId);
                else 
                    $("#related_product_name_ids").val(productIdsVal+","+productId);
                $("#otherproductaddtocart").show()
            }
            $("#related_product_name").val("");
            return false
        }
    })
}
function changeDonationAmount(val){
    if(val=="other")$(
        "#other_amount_div").show();else $("#other_amount_div").hide()
}
$(function(){
    relatedProductAutocomplete();
    $("#campaign").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:function(request,response){
            $.ajax({
                url:
                "/get-campaigns",
                method:"POST",
                dataType:"json",
                data:{
                    campaign_name:$("#campaign").val()
                },
                success:function(jsonResult){
                    var resultset=[];
                    $.each(jsonResult,function(){
                        var id=this.campaign_id,value=this.campaign_title;
                        resultset.push({
                            id:this.
                            campaign_id,
                            value:this.campaign_title
                        })
                    });
                    response(resultset)
                }
            })
        },
        open:function(){
            $(".ui-menu").width(350)
        },
        change:
        function(event,ui){
            if(ui.item){
                $("#campaign").val(ui.item.value);
                $("#campaign_id").val(ui.item.id)
            }else{
                $("#campaign").
                val("");
                $("#campaign_id").val("")
            }
            $(".ui-autocomplete").jScrollPane()
        },
        focus:function(event,ui){
            $("#campaign_id").val(ui
                .item.id);
            return false
        },
        select:function(event,ui){
            $("#campaign").val(ui.item.value);
            $("#campaign_id").val(ui.item.id);
            return false
        }
    });
    $("#company").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:function(request,
            response){
            $.ajax({
                url:"/get-company",
                method:"POST",
                dataType:"json",
                data:{
                    company_name:$("#company").val()
                },
                success:
                function(jsonResult){
                    var resultset=[];
                    $.each(jsonResult,function(){
                        var id=this.company_id,value=this.company_name;
                        resultset.push({
                            id:this.company_id,
                            value:this.company_name
                        })
                    });
                    response(resultset)
                }
            })
        },
        open:function(){
            $(".ui-menu").
            width(350)
        },
        change:function(event,ui){
            if(ui.item){
                $("#company").val(ui.item.value);
                $("#company_id").val(ui.item.id)
            }else

            {
                $("#company").val("");
                $("#company_id").val("")
            }
        },
        focus:function(event,ui){
            $("#company_id").val(ui.item.id);
            return false
        },
        select:function(event,ui){
            $("#company").val(ui.item.value);
            $("#company_id").val(ui.item.id);
            return false
        }
    });
    $(document
        ).on("change",".matchinggift",function(){
        if($(this).val()==1)$("#company_block").show();else $("#company_block").hide()
    }
    );
    $("input[name='in_honoree']").click(function(){
        if($(this).val()==3)$("#honoree_name_block").hide();else $(
            "#honoree_name_block").show()
    });
    $(document).on("change","#is_notify",function(){
        if($(this).is(":checked"))$(
            "#notify_block").show();else $("#notify_block").hide()
    });
    $("input[id^='woh_main_product']").change(function(){
        $(
            "#naming_convention_form").html("")
    });
    $("#is_related_product").on("change",function(){
        if($("#is_related_product").is(":unchecked")&&$("#is_bio_certificate").is(":unchecked")&&$("#is_personalize_panel").is(":unchecked")&&$("#is_duplicate_certificate").is(":unchecked"))$("#otherproductaddtocart").hide()
    });
    $("#is_bio_certificate").on("change",function(){
        if($("#is_related_product").is(":unchecked")&&$("#is_bio_certificate").is(":unchecked")&&$("#is_personalize_panel").is(":unchecked")&&$("#is_duplicate_certificate").is(":unchecked"))$("#otherproductaddtocart").hide()
    });
    $("#is_personalize_panel").on("change",function(){
        if($("#is_related_product").is(":unchecked")&&$("#is_bio_certificate").is(":unchecked")&&$("#is_personalize_panel").is(":unchecked")&&$("#is_duplicate_certificate").is(":unchecked"))$("#otherproductaddtocart").hide()
    })
    $("#is_duplicate_certificate").on("change",function(){
        if($("#is_related_product").is(":unchecked")&&$("#is_bio_certificate").is(":unchecked")&&$("#is_personalize_panel").is(":unchecked")&&$("#is_duplicate_certificate").is(":unchecked"))$("#otherproductaddtocart").hide()
    })
});
function removeAutoCompleteValue(id,type){
    var module="",elementId="";
    if(type==
        "related_select_product"){
        module="related_product";
        elementId="related_product_name_ids"
    }else if(type==
        "other_related_select_product"){
        module="other_related_product";
        elementId="other_related_product_name_ids"
    }
    var idArr=$(
        "#"+elementId).val().split(","),newIdStr="";
    for(i=0;i<idArr.length;i++)if(idArr[i]!=id)newIdStr+=idArr[i]+",";newIdStr=
    newIdStr.substr(0,newIdStr.length-1);
    $("#"+elementId).val(newIdStr);
    $("#"+module+""+id).remove();
    var pers_qty=$("#personalized_qty").val(),bio_qty=$("#bio_certificate_qty").val(),dup_qty=$('#duplicate_certificate_qty');
    if($("#related_select_product li").size()==0&&bio_qty==""&&pres_qty==""&&dup_qty=="")$("#otherproductaddtocart").hide()
}
function getAddToCartData(mappingTypeId){
    var url="";
    if(mappingTypeId==""){
        $("#addToCartData").html("");
        return false
    }else if(mappingTypeId==1)url=
        "/get-donation-add-to-cart-detail";
    else if(mappingTypeId==2)url="/get-flag-of-faces-add-to-cart-detail";
    else if(
        mappingTypeId==3)url="/get-kiosk-fee-add-to-cart-detail";
    else if(mappingTypeId==4)url=
        "/get-inventory-add-to-cart-detail";
    else if(mappingTypeId==5)url="/get-membership-add-to-cart-detail";
    else if(
        mappingTypeId==6)url="/get-passenger-document-add-to-cart-detail";
    else if(mappingTypeId==7)url=
        "/get-wall-of-honor-add-to-cart-detail";
    if(mappingTypeId!=""){
        showAjxLoader();
        $.ajax({
            url:url,
            method:"POST",
            dataType:
            "html",
            data:{
                product_mapping_id:$("#product_mapping_id").val(),
                user_id:$("#user_id").val()
            },
            success:function(
                responseData){
                hideAjxLoader();
                if(!checkUserAuthenticationAjx(responseData))return false;
                $("#addToCartData").html(
                    responseData)
            }
        })
    }else $("#addToCartData").html("")
}
var totalNotify=typeof totalNotify!="undefined"?totalNotify:1;
function addMoreDonationNotify(currAnchor){
    console.log("come");
    showAjxLoader();
    $.ajax({
        type:"POST",
        url:
        "/add-more-notify",
        data:{
            totalNotify:totalNotify
        },
        success:function(responseData){
            hideAjxLoader();
            if(!
                checkUserAuthenticationAjx(responseData))return false;
            var allDivBlock=$("div[id^='notify_donation_']");
            if(allDivBlock.
                length==1){
                var parId=$(currAnchor).parents('div[id^="notify_donation_"]').attr("id"),id=parId.substring(parId.
                    lastIndexOf("_")+1,parId.length);
                $(currAnchor).parent().append(
                    '<a class="minus" href="javascript:void(0);" onclick="deleteMoreDonationNotify('+id+')"></a>')
            }
            $(currAnchor).remove();
            $(
                "#notify_block").append(responseData);
            totalNotify=totalNotify+1
        }
    })
}
function deleteMoreDonationNotify(id){
    $(
        "#notify_donation_"+id).remove();
    var lastBlock=$("div[id^='notify_donation_']").last();
    if($(lastBlock).find(".plus").
        length==0)$(lastBlock).children("div").last().append(
        '<a class="plus plus m-l-5" href="javascript:void(0);" onclick="addMoreDonationNotify(this)" id="addMoreDonationNotify"></a>'
        );
    var allDivBlock=$("div[id^='notify_donation_']");
    if(allDivBlock.length==1)$(lastBlock).find(".minus").remove()
}
function showHonoreeName(currRef){
    if($(currRef).val()==3)$("#honoree_name_block").hide();else $("#honoree_name_block").
        show()
}
function getProductsByCategoryId(category_id){
    if(category_id==""){
        $("#product_detail_block").hide();
        $(
            "#product_block").hide();
        return false
    }
    showAjxLoader();
    $.ajax({
        type:"POST",
        url:"/get-category-product",
        dataType:"html",
        data:{
            category_id:category_id,
            product_type_id:$("#product_type_id").val()
        },
        success:function(responseData){
            hideAjxLoader(
                );
            if(!checkUserAuthenticationAjx(responseData))return false;
            $("#product_id").html(responseData).select2();
            $(
                "#product_block").show()
        }
    })
}
function getProductsDetailById(product_id){
    if(product_id==""){
        $("#product_detail_block").
        hide();
        return false
    }
    showAjxLoader();
    if($("#user_id"))
        var shop_user_id = $("#user_id").val();
    $.ajax({
        type:"POST",
        url:"/get-product-detail-inventory",
        dataType:"json",
        data:{
            product_id:product_id,
            user_id:shop_user_id
        },
        success:function(responseData){
            hideAjxLoader();
            if(!checkUserAuthenticationAjx(responseData))
                return false;
            $("#product_name").text(responseData.product_name);
            $("#product_price").text(responseData.product_sell_price
                );
            $("#product_detail_block").show()
        }
    })
}
function onChangeMembership(membershipId){
    $(
        "input[id^='minimum_donation_amount_']").attr("disabled","disabled");
    $("#minimum_donation_amount_"+membershipId).
    removeAttr("disabled")
}
function onChangeProduct(productId){
    showAjxLoader();
    $.ajax({
        type:"POST",
        url:
        "/get-wall-of-honor-other-products",
        dataType:"html",
        data:{
            productId:productId,
            userId:$("#user_id").val(),
            user_id:$("#user_id").val()
        },
        success:
        function(responseData){
            hideAjxLoader();
            if(!checkUserAuthenticationAjx(responseData))return false;
            $(
                "#other_product_block").html(responseData);
            $("div[id^='options_list_block_']").hide();
            $("div[id^='options_list_block_"+
                productId+"']").show();
            relatedProductAutocomplete()
        }
    })
}
function getWallOfHonorForm(attribute_id,option_id,option_name,
    attribute_type,format_name){
    showAjxLoader();
    $.ajax({
        type:"POST",
        url:"/generate-woh-form-by-naming-convention",
        dataType:
        "html",
        data:{
            attribute_id:attribute_id,
            option_id:option_id,
            option_name:option_name,
            attribute_type:attribute_type,
            format_name:format_name
        },
        success:function(responseData){
            hideAjxLoader();
            if(!checkUserAuthenticationAjx(responseData))
                return false;
            $("#naming_convention_form").html(responseData);
            $("#product_attribute_option_id").val(option_id)
        }
    })
}
$(
    function(){
        $(document).on("change","#is_duplicate_certificate",function(){
            if($(this).is(":checked")){
                $("#duplicate_certificate_qty").removeAttr("readonly");
                $("#duplicate_product_div").show();
            }
            else{
                $("#duplicate_certificate_qty").attr("readonly","readonly");
                $("#duplicate_product_div").hide()
            }
        });
        $(document).on("change","#is_related_product",function(){
            if($(this).is(":checked"))$("#related_product_div").show();else
                $("#related_product_div").hide()
        });
        $(document).on("change","#is_bio_certificate",function(){
            if($(this).is(":checked"))$
                ("#bio_certificate_div").show();else $("#bio_certificate_div").hide()
        });
        $(document).on("change","#is_personalize_panel",
            function(){
                if($(this).is(":checked"))$("#personalized_panel_div").show();else $("#personalized_panel_div").hide()
            });
        $(
            document).on("change","#is_usa",function(){
            if($(this).is(":checked")){
                $("#other_country").val("");
                $("#other_country").
                attr("readonly","readonly")
            }else $("#other_country").removeAttr("readonly")
        });
        $(document).on("change","#matching_gift",
            function(){
                if($(this).is(":checked"))$("#company_block").show();else $("#company_block").hide()
            })
    });
$(document).ready(
    function(){
        jQuery.validator.addMethod("floating",function(value,element){
            return this.optional(element)||
            /^-?\d*(\.\d{1,2})?$/.test(value)
        },NUMERIC_VALUE);
        $("#donation_add_to_cart").validate({
            submitHandler:function(){
                var 
                dataStr=$("#donation_add_to_cart").serialize()+"&"+$("#add_to_cart").serialize();
                showAjxLoader();
                $.ajax({
                    type:"POST",
                    url
                    :"/add-to-cart-donation",
                    data:dataStr,
                    success:function(responseData){
                        hideAjxLoader();
                        if(!checkUserAuthenticationAjx(
                            responseData))return false;
                        var jsonObj=jQuery.parseJSON(responseData);
                        if(jsonObj.status=="success"){
                            parent.
                            refreshCartData();
                            parent.$.colorbox.close()
                        }else $.each(jsonObj.message,function(i,msg){
                            $("#"+i).after(
                                '<label class="error" style="display:block;">'+msg+"</label>")
                        })
                    }
                })
            },
            rules:{
                amount:{
                    required:true
                },
                other_amount:{
                    required:false,
                    floating:true,
                    minlength:1,
                    maxlength:10
                },
                campaign:{
                    required:false
                }
            },
            messages:{
                amount:{
                    required:
                    AMOUNT_EMPTY
                },
                other_amount:{
                    required:AMOUNT_EMPTY
                },
                campaign:{
                    required:CAMPAIGN_EMPTY
                }
            }
        });
        $.validator.setDefaults({
            ignore
            :[]
        });
        $.validator.addMethod("AlphaNumericSpaceCommaFOF",function(alpha_numeric_text){
            var alpha_number_regex=
            /^([a-zA-Z0-9,'\s]+)$/;
            if(!alpha_number_regex.test(alpha_numeric_text)&&alpha_numeric_text!="")return false;else
                return true
        },"");
        $("#flag_of_faces_form").validate({
            submitHandler:function(){
                var dataStr=$("#flag_of_faces_form").
                serialize()+"&"+$("#add_to_cart").serialize();
                showAjxLoader();
                $.ajax({
                    type:"POST",
                    url:"/add-to-cart-fof",
                    data:dataStr,
                    success:function(responseData){
                        hideAjxLoader();
                        if(!checkUserAuthenticationAjx(responseData))return false;
                        var jsonObj=
                        jQuery.parseJSON(responseData);
                        if(jsonObj.status=="success"){
                            parent.refreshCartData();
                            parent.$.colorbox.close()
                        }else $.
                            each(jsonObj.message,function(i,msg){
                                $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                            })
                    }
                })
            }
            ,
            rules:{
                donated_by:{
                    required:true,
                    AlphaNumericSpaceCommaFOF:true
                },
                people_photo:{
                    required:true
                },
                photo_caption:{
                    required:
                    true,
                    AlphaNumericSpaceCommaFOF:true
                },
                crop_fof_image_status:{
                    required:true
                }
            },
            messages:{
                donated_by:{
                    required:
                    "Please enter donated by.",
                    AlphaNumericSpaceCommaFOF:"Please enter valid donated by."
                },
                people_photo:{
                    required:
                    "Please tag people in photo."
                },
                photo_caption:{
                    required:"Please enter caption.",
                    AlphaNumericSpaceCommaFOF:
                    "Please enter valid caption."
                },
                crop_fof_image_status:{
                    required:"Please crop the image in square."
                }
            }
        });
        $("#fofcancel").
        click(function(){
            parent.$.colorbox.close()
        });
        $("#kiosk_fee_form").validate({
            submitHandler:function(){
                var dataStr=$(
                    "#kiosk_fee_form").serialize()+"&"+$("#add_to_cart").serialize();
                showAjxLoader();
                $.ajax({
                    type:"POST",
                    url:
                    "/add-to-cart-kiosk-fee",
                    data:dataStr,
                    success:function(responseData){
                        hideAjxLoader();
                        if(!checkUserAuthenticationAjx(
                            responseData))return false;
                        var jsonObj=jQuery.parseJSON(responseData);
                        if(jsonObj.status=="success"){
                            parent.
                            refreshCartData();
                            parent.$.colorbox.close()
                        }else $.each(jsonObj.message,function(i,msg){
                            $("#"+i).after(
                                '<label class="error" style="display:block;">'+msg+"</label>")
                        })
                    }
                })
            },
            rules:{},
            messages:{}
        });
        $("#inventory_form").
        validate({
            submitHandler:function(){
                var dataStr=$("#inventory_form").serialize()+"&"+$("#add_to_cart").serialize();
                showAjxLoader();
                $.ajax({
                    type:"POST",
                    url:"/add-to-cart-inventory",
                    data:dataStr,
                    success:function(responseData){
                        hideAjxLoader();
                        if(!checkUserAuthenticationAjx(responseData))return false;
                        var jsonObj=jQuery.parseJSON(responseData);
                        if(
                            jsonObj.status=="success"){
                            parent.refreshCartData();
                            parent.$.colorbox.close()
                        }else $.each(jsonObj.message,function(i,msg
                            ){
                            $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                        })
                    }
                })
            },
            rules:{
                num_quantity:{
                    required:true
                    ,
                    digits:true,
                    min:1
                }
            },
            messages:{
                num_quantity:{
                    required:L_NUM_QUANTITY_REQUIRED,
                    digits:QUANTITY_NUMERIC,
                    min:
                    L_NUM_QUANTITY_MINIMUM
                }
            }
        });
        $("#membership_form").validate({
            submitHandler:function(){
                var dataStr=$("#membership_form").
                serialize()+"&"+$("#add_to_cart").serialize();
                showAjxLoader();
                $.ajax({
                    type:"POST",
                    url:"/add-to-cart-membership",
                    data:
                    dataStr,
                    success:function(responseData){
                        hideAjxLoader();
                        if(!checkUserAuthenticationAjx(responseData))return false;
                        var 
                        jsonObj=jQuery.parseJSON(responseData);
                        if(jsonObj.status=="success"){
                            parent.refreshCartData();
                            parent.$.colorbox.close()
                        }
                        else $.each(jsonObj.message,function(i,msg){
                            $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>"
                                )
                        })
                    }
                })
            },
            rules:{},
            messages:{}
        });
        $.validator.addMethod("AlphaNumericSpace",function(alpha_numeric_text){
            var 
            alpha_number_regex=/^([a-zA-Z\s-.,']+)$/;
            if(!alpha_number_regex.test(alpha_numeric_text)&&alpha_numeric_text!="")
                return false;else return true
        },"");
        $("#wall_of_honor_form").validate({
            submitHandler:function(){
                var dataStr=$("#wall_of_honor_form").serialize()+"&"+$("#add_to_cart").serialize();
                showAjxLoader();
                 if(checkCustomValidation("wall_of_honor_form")){
                    $.ajax({
                        type:"POST",
                        url:"/add-to-cart-wall-of-honor",
                        data:dataStr,
                        success:function(responseData){
                            hideAjxLoader();
                            if(!checkUserAuthenticationAjx(
                                responseData))return false;
                            var jsonObj=jQuery.parseJSON(responseData);
                            if(jsonObj.status=="success"){
                                parent.
                                refreshCartData();
                                parent.$.colorbox.close()
                            }else $.each(jsonObj.message,function(i,msg){
                                $("#"+i).after(
                                    '<label class="error" style="display:block;">'+msg+"</label>")
                            })
                        }
                    });
                 }  
                 else {
                     hideAjxLoader();
                 }
            },
            rules:{
                first_name_one:{
                    required:true,
                    AlphaNumericSpace:true
                },
                first_name_two:{
                    required:true,
                    AlphaNumericSpace:true
                },
                other_init_one:{
                    required:true,
                    AlphaNumericSpace:true
                },
                other_init_two:{
                    required:true,
                    AlphaNumericSpace:true
                },
                other_name:{
                    required:true,
                    AlphaNumericSpace:true
                },
                last_name_one:{
                    required:true,
                    AlphaNumericSpace:true
                },
                last_name_two:{
                    required:true,
                    AlphaNumericSpace:true
                },
                last_name_one:{
                    required:true,
                    AlphaNumericSpace:true
                },
                first_line:{
                    required:true,
                    AlphaNumericSpace
                    :true
                },
                second_line:{
                    required:true,
                    AlphaNumericSpace:true
                },
                donated_by:{
                    required:true,
                    AlphaNumericSpace:true
                },
                other_country:{
                    AlphaNumericSpace:true
                },
                duplicate_certificate_qty:{
                    digits:true
                },
                bio_certificate_qty:{
                    digits:true
                },
                bio_certificate_qty:{
                    digits:true
                }
            },
            messages:{
                first_name_one:{
                    required:FIRST_NAME_EMPTY,
                    AlphaNumericSpace:
                    WOH_ERROR_ALPHANUMERICSPACE_FIRST_NAME_ONE
                },
                first_name_two:{
                    required:FIRST_NAME_EMPTY,
                    AlphaNumericSpace:
                    WOH_ERROR_ALPHANUMERICSPACE_FIRST_NAME_TWO
                },
                other_init_one:{
                    required:INITIAL_NAME_EMPTY,
                    AlphaNumericSpace:
                    WOH_ERROR_ALPHANUMERICSPACE_OTHER_INIT_ONE
                },
                other_init_two:{
                    required:INITIAL_NAME_EMPTY,
                    AlphaNumericSpace:
                    WOH_ERROR_ALPHANUMERICSPACE_OTHER_INIT_TWO
                },
                other_name:{
                    required:OTHER_NAME_EMPTY,
                    AlphaNumericSpace:
                    WOH_ERROR_ALPHANUMERICSPACE_OTHER_NAME
                },
                last_name_one:{
                    required:LAST_NAME_EMPTY,
                    AlphaNumericSpace:
                    WOH_ERROR_ALPHANUMERICSPACE_LAST_NAME_ONE
                },
                last_name_two:{
                    required:LAST_NAME_EMPTY,
                    AlphaNumericSpace:
                    WOH_ERROR_ALPHANUMERICSPACE_LAST_NAME_TWO
                },
                first_line:{
                    required:FIRST_LINE_EMPTY,
                    AlphaNumericSpace:
                    WOH_ERROR_ALPHANUMERICSPACE_FIRST_LINE
                },
                second_line:{
                    required:SECOND_LINE_EMPTY,
                    AlphaNumericSpace:
                    WOH_ERROR_ALPHANUMERICSPACE_SECOND_LINE
                },
                donated_by:{
                    required:DONATED_BY_EMPTY,
                    AlphaNumericSpace:WOH_ERROR_ALPHANUMERICSPACE_DONATED_BY
                },
                other_country:{
                    AlphaNumericSpace:WOH_ERROR_ALPHANUMERICSPACE_OTHER_COUNTRY
                },
                amount:{
                    required:AMOUNT_EMPTY
                },
                campaign:{
                    required:CAMPAIGN_EMPTY
                }
            }
        });
        $("#passenger_document").validate({
            submitHandler:function(){
                var dataStr=$(
                    "#passenger_document").serialize()+"&"+$("#add_to_cart").serialize();
                showAjxLoader();
                $.ajax({
                    type:"POST",
                    url:
                    "/add-to-cart-passenger-document",
                    data:dataStr,
                    success:function(responseData){
                        hideAjxLoader();
                        if(!
                            checkUserAuthenticationAjx(responseData))return false;
                        var jsonObj=jQuery.parseJSON(responseData);
                        if(jsonObj.status==
                            "success"){
                            parent.refreshCartData();
                            parent.$.colorbox.close()
                        }else $.each(jsonObj.message,function(i,msg){
                            $("#"+i).after
                            ('<label class="error" style="display:block;">'+msg+"</label>")
                        })
                    }
                })
            },
            rules:{
                passenger_record_qty:{
                    digits:true
                },
                ship_image_qty:{
                    digits:true
                },
                is_first_image:{
                    required:function(){
                        if($("#is_second_image").is(":checked")||$(
                            "#is_first_image").is(":checked") || manifestMandatoryCheck() == true)return false;else return true;
                    }
                }
            },
            messages:{
                is_first_image:{
                    required:FIRST_IMAGE_EMPTY
                }
            }
        });
        var url="/upload-user-fof-image";
        $("#upload_photo").fileupload({
            url:url,
            dataType:"json",
            done:function(e,data){
                hideAjxLoader();
                data=data.result;
                if(data.status=="success"){
                    $("#DivUploadedPhoto").show();
                    $('label[for="upload_photo"]')
                    .remove();
                    $("#upload_photo").removeClass("error");
                    $("#filemessage").addClass("success-msg");
                    $("#filemessage").
                    removeClass("error-msg");
                    $("#image_name").val(data.image_name);
                    $("#fof_image").val(data.filename);
                    $(
                        "#UploadUserFofImageIframe").attr("src","/upload-user-fof-image-iframe/"+data.full_path_pixenate+"/"+data.encrypted_filename);
                    $(
                        "#previewFOF1").attr("src","/upload-user-fof-image-iframe-preview/"+data.preview_path+"/2");
                    $("#peoplePhotoBox").empty()
                    ;
                    $("#people_photo").val("")
                }else{
                    $("#filemessage").removeClass("success-msg");
                    $("#filemessage").addClass("error-msg");
                    $(
                        "#image_name").val("");
                    $("#fof_image").val("");
                    $("#peoplePhotoBox").empty();
                    $("#people_photo").val("")
                }
                $("#filemessage")
                .html(data.message)
            },
            start:function(e,data){
                showAjxLoader()
            }
        }).prop("disabled",!$.support.fileInput).parent().addClass($
            .support.fileInput?undefined:"disabled")
    });
var totalSaveSearch=typeof totalSaveSearch!="undefined"?totalSaveSearch:1;

function manifestMandatoryCheck() {
	var imageCount = $("#mycarousel li").length;
	if(manifestType == "TwoPage") {
			var startIndex = 3; // It means that third image will be the additional image.
		}
	if(manifestType == "OnePage")	{
			var startIndex = 2;     // It means that second image will be additional image.
		}	
	for(var i=startIndex;i<=imageCount; i++) {
		if($("#additional_images_"+i).is(":checked") == true)
			return true;
    }		
}
function addMoreAdditionalContact(currAnchor){
    showAjxLoader();
    $.ajax({
        type:"POST",
        url:"/add-additional-contact",
        data:{
            totalSaveSearch:totalSaveSearch
        },
        success:function(responseData){
            hideAjxLoader();
            if(!checkUserAuthenticationAjx(
                responseData))return false;
            var allDivBlock=$("div[id^='add_more_additional_contact']");
            if(allDivBlock.length==1){
                var 
                parId=$(currAnchor).parents('div[id^="add_more_additional_contact_"]').attr("id"),id=parId.substring(parId.lastIndexOf(
                    "_")+1,parId.length);
                $(currAnchor).parent().append(
                    '<a class="minus" href="javascript:void(0);" onclick="deleteAdditionalContact('+id+')"></a>')
            }
            $(currAnchor).remove();
            $(
                "#add_more_additional_div").append(responseData);
            totalSaveSearch=totalSaveSearch+1
        }
    })
}
function deleteAdditionalContact(
    id){
    $("#add_more_additional_contact_"+id).remove();
    var lastBlock=$("div[id^='add_more_additional_contact']").last();
    if($
        (lastBlock).find(".plus").length==0)$(lastBlock).children("div").last().children("div").last().append(
        '<a id="addMoreAdditionalContact" onclick="addMoreAdditionalContact(this)" href="javascript:void(0);" class="plus m-l-5"></a>'
        );
    var allDivBlock=$("div[id^='add_more_additional_contact']");
    if(allDivBlock.length==1)$(lastBlock).find(".minus").
        remove()
}
function getBioPersonalizeSearch(buttonId){
    showAjxLoader();
    var userId="",transactionId="",panelNo="",name="",
    gridBlockId="";
    userId=$("#user_id").val();
    if(buttonId=="bio_certificate_search"){
        transactionId=$("#bio_certificate_transaction_id").val();
        panelNo=$("#bio_certificate_panel_id").val();
        gridBlockId="bio_certificate_search_data";
        searchType="bio_certificate"
    }else if(buttonId=="duplicate_certificate_search"){
        name=$("#duplicate_certificate_name").val();
        transactionId=$("#duplicate_certificate_transaction_id").val();
        panelNo=$("#duplicate_certificate_panel_id").val();
        gridBlockId="duplicate_certificate_search_data";
        searchType="duplicate_certificate"
    }    
    else{
        transactionId=$("#personalize_panel_transaction_id").val();
        panelNo= $("#personalize_panel_panel_id").val();
        name = $("#personalize_panel_honoree_name").val();
        gridBlockId="personalize_search_data";
        searchType="personalize"
    }
    $.ajax(
    {
            type:"POST",
            url:"/get-wall-of-honor-bio-certificate-search",
            data:{
                user_id:userId,
                name:name,
                transaction_id:transactionId,
                panel_no:panelNo,
                search_type:searchType
            },
            success:function(responseData){
                hideAjxLoader();
                if(!checkUserAuthenticationAjx(responseData))return false;
                $("#"+gridBlockId).html(responseData)
            }
        })
}


function checkCustomValidation(id){
    var flagGl=true,flagCh=true;
    $('input[name="first_name[]"]').each(
        function(){
            $('label[for="'+$(this).attr("id")+'"]').remove();
            var str=$(this).val(),regx=/^([a-zA-Z0-9\s']+)$/;
            if(!str.
                match(regx)&&str.length>0){
                $(this).after('<label class="error" for="'+$(this).attr("id")+'" generated="true">'+
                    WOH_HEADING_ADD_CONTACTS_ERROR_ALPHANUMERICSPACE_FIRST_NAME+"</label>");
                flagGl=false;
                flagCh=false
                }else $('label[for="'+$
                (this).attr("id")+'"]').remove()
                });
    $('input[name="last_name[]"]').each(function(){
        $('label[for="'+$(this).attr("id")+
            '"]').remove();
        var str=$(this).val(),regx=/^([a-zA-Z0-9\s']+)$/;
        if(!str.match(regx)&&str.length>0){
            $(this).after(
                '<label class="error" for="'+$(this).attr("id")+'" generated="true">'+
                WOH_HEADING_ADD_CONTACTS_ERROR_ALPHANUMERICSPACE_LAST_NAME+"</label>");
            flagGl=false;
            flagCh=false
            }else $('label[for="'+$(
            this).attr("id")+'"]').remove()
            });
    $('input[name="email_id[]"]').each(function(){
        $('label[for="'+$(this).attr("id")+'"]')
        .remove();
        var str=$(this).val(),regx=
        /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i
        ;
        if(!str.match(regx)&&str.length>0){
            $(this).after('<label class="error" for="'+$(this).attr("id")+'" generated="true">'+
                WOH_HEADING_ADD_CONTACTS_ERROR_EMAIL_VALIDATION_EMAIL+"</label>");
            flagGl=false;
            flagCh=false
            }else $('label[for="'+$(this)
            .attr("id")+'"]').remove()
            });
    if(flagGl&&flagCh)return true;else return false
        }