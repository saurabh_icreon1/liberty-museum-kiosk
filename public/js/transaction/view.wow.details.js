$(function(){$.validator.addMethod("AlphaNumericSpace",function(alpha_numeric_text){var alpha_number_regex=
/^([a-zA-Z\s-.,']+)$/;if(!alpha_number_regex.test(alpha_numeric_text)&&alpha_numeric_text!="")return false;else
 return true},"");$("#woh_details").validate({submitHandler:function(){var dataStr=$("#woh_details").serialize();$.ajax(
{type:"POST",url:"/view-woh-details/"+woh_id+"/"+woh_transaction_id,data:dataStr,success:function(responseData){if(!
checkUserAuthenticationAjx(responseData))return false;var jsonObj=jQuery.parseJSON(responseData);if(jsonObj.status==
"success"){parent.$("#woh_img"+woh_disp_id).show();parent.location.reload()}else $.each(jsonObj.message,function(i,msg){
$("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")})}})},rules:{first_name_one:{required:{
depends:function(element){if($("#first_name_one").attr("id")==undefined)return false;else return true}},
AlphaNumericSpace:{depends:function(element){if($("#first_name_one").attr("id")==undefined)return false;else return true
}}},other_init_one:{required:{depends:function(element){if($("#other_init_one").attr("id")==undefined)return false;else
 return true}},AlphaNumericSpace:{depends:function(element){if($("#other_init_one").attr("id")==undefined)return false;
else return true}}},first_name_two:{required:{depends:function(element){if($("#first_name_two").attr("id")==undefined)
return false;else return true}},AlphaNumericSpace:{depends:function(element){if($("#first_name_two").attr("id")==
undefined)return false;else return true}}},other_name:{required:{depends:function(element){if($("#other_name").attr("id"
)==undefined)return false;else return true}},AlphaNumericSpace:{depends:function(element){if($("#other_name").attr("id")
==undefined)return false;else return true}}},other_init_two:{required:{depends:function(element){if($("#other_init_two")
.attr("id")==undefined)return false;else return true}},AlphaNumericSpace:{depends:function(element){if($(
"#other_init_two").attr("id")==undefined)return false;else return true}}},last_name_one:{required:{depends:function(
element){if($("#last_name_one").attr("id")==undefined)return false;else return true}},AlphaNumericSpace:{depends:
function(element){if($("#last_name_one").attr("id")==undefined)return false;else return true}}},last_name_two:{required:
{depends:function(element){if($("#last_name_two").attr("id")==undefined)return false;else return true}},
AlphaNumericSpace:{depends:function(element){if($("#last_name_two").attr("id")==undefined)return false;else return true}
}},first_line:{required:{depends:function(element){if($("#first_line").attr("id")==undefined)return false;else
 return true}},AlphaNumericSpace:{depends:function(element){if($("#first_line").attr("id")==undefined)return false;else
 return true}}},second_line:{required:{depends:function(element){if($("#second_line").attr("id")==undefined)return false
;else return true}},AlphaNumericSpace:{depends:function(element){if($("#second_line").attr("id")==undefined)return false
;else return true}}},donated_by:{required:{depends:function(element){if($("#donated_by").attr("id")==undefined)
return false;else return true}},AlphaNumericSpace:{depends:function(element){if($("#donated_by").attr("id")==undefined)
return false;else return true}}},country_origin:{required:{depends:function(element){if($("#country_originUS").prop(
"checked")==false&&$("#country_origin").val()=="")return true;else if($("#country_originUS").prop("checked")==true&&$(
"#country_origin").val()=="")return false;else return false}},AlphaNumericSpace:{depends:function(element){if($(
"#country_originUS").prop("checked")==false&&$("#country_origin").val()!="")return true;else return false}}}},messages:{
first_name_one:{required:WOH_ERROR_EMPTY_FIRST_NAME_ONE,AlphaNumericSpace:WOH_ERROR_ALPHANUMERICSPACE_FIRST_NAME_ONE},
other_init_one:{required:WOH_ERROR_EMPTY_OTHER_INIT_ONE,AlphaNumericSpace:WOH_ERROR_ALPHANUMERICSPACE_OTHER_INIT_ONE},
first_name_two:{required:WOH_ERROR_EMPTY_FIRST_NAME_TWO,AlphaNumericSpace:WOH_ERROR_ALPHANUMERICSPACE_FIRST_NAME_TWO},
other_name:{required:WOH_ERROR_EMPTY_OTHER_NAME,AlphaNumericSpace:WOH_ERROR_ALPHANUMERICSPACE_OTHER_NAME},other_init_two
:{required:WOH_ERROR_EMPTY_OTHER_INIT_TWO,AlphaNumericSpace:WOH_ERROR_ALPHANUMERICSPACE_OTHER_INIT_TWO},last_name_one:{
required:WOH_ERROR_EMPTY_LAST_NAME_ONE,AlphaNumericSpace:WOH_ERROR_ALPHANUMERICSPACE_LAST_NAME_ONE},last_name_two:{
required:WOH_ERROR_EMPTY_LAST_NAME_TWO,AlphaNumericSpace:WOH_ERROR_ALPHANUMERICSPACE_LAST_NAME_TWO},first_line:{required
:WOH_ERROR_EMPTY_FIRST_LINE,AlphaNumericSpace:WOH_ERROR_ALPHANUMERICSPACE_FIRST_LINE},second_line:{required:
WOH_ERROR_EMPTY_SECOND_LINE,AlphaNumericSpace:WOH_ERROR_ALPHANUMERICSPACE_SECOND_LINE},donated_by:{required:
WOH_ERROR_EMPTY_DONATED_BY,AlphaNumericSpace:WOH_ERROR_ALPHANUMERICSPACE_DONATED_BY},country_origin:{required:
WOH_ERROR_EMPTY_OTHER_COUNTRY,AlphaNumericSpace:WOH_ERROR_ALPHANUMERICSPACE_OTHER_COUNTRY}}})})