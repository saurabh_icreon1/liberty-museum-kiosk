$(function(){
    $("#update_qty").validate({
        submitHandler:function(){
            var dataStr=$("#update_qty").serialize();
            if(
                checkCustomValidation("update_qty")){
                showAjxLoader();
                $.ajax({
                    type:"POST",
                    url:"/update-product-qty/"+
                    encrypt_transactionId,
                    data:dataStr,
                    success:function(responseData){
                        var jsonObj=JSON.parse(responseData);
                        if(jsonObj.status
                            =="success")window.location.reload();else $.each(jsonObj.message,function(i,msg){
                            $("#"+i).after(
                                '<label class="error" style="display:block;">'+msg+"</label>")
                            });
                        hideAjxLoader()
                        }
                    })
            }
        }
    })
});
function checkCustomValidation(id){
    var flagGl=true;
    $('#update_qty [name="product_qty[]"]').each(function(index){
        var allInputProductQty=$("input[name^='product_shp_qty']"),allInputProductStu=$("select[name^='product_status']"),shp_qty=$(allInputProductQty[index]).val(),pro_stu=$(allInputProductStu[index]).val(),
        str=$(this).val(),regx=/^[0-9]+$/i;
        alert(str);
        $(this).next().remove("span");
        if(str.match(regx)){
            if(pro_stu==4||pro_stu==5||pro_stu==9)if(str.length<1||str.length==0){
                if($(this).next("span").length==0)$(this).after('<span class="error" for="product_qty[]" generated="true" style="display:block !important;"><span class="redarrow"></span>Please enter a valid quantity</span>');
                flagGl=false
                }else if(str==0){
                if($(this).next("span").length==0)$(this).after('<span class="error" for="product_qty[]" generated="true" style="display:block !important;"><span class="redarrow"></span>Please enter a valid quantity</span>');
                flagGl=false
                }else if(str.length>10&&str!=""){
                if($(this).next("span").length==0)$(this).after('<span class="error" for="product_qty[]" generated="true" style="display:block !important;"><span class="redarrow"></span>Please enter a valid quantity</span>');
                flagGl=false
                }else if(shp_qty<str){
                $(this).after('<span class="error" for="product_qty[]" generated="true" style="display:block !important;"><span class="redarrow"></span>Please enter a valid quantity</span>');
                flagGl=false
                }else $(this).next().remove("span")
                }else{
            if($(this).next("span").length==0)$(this).after('<span class="error" for="product_qty[]" generated="true" style="display:block !important;"><span class="redarrow"></span>Please enter a valid quantity</span>');
            flagGl=false
            }
        });
if(flagGl==false){
    $(".detail-section").css("display","block");
    return false
    }else return true
    }