 $(document).on('change',"#country",function(){
        if($("#country").val()=="228"){
            $("#state").show();
            $("#other_state").hide();
        }else{
            $("#state").hide();
            $("#other_state").show();
        }
    });
	
$(function() {

	if($("#country").val()=="228"){
            $("#state").show();
            $("#other_state").hide();
        }else{
            $("#state").hide();
            $("#other_state").show();
        }

	$("#credit_card_number").validateCreditCard(function(result){
		if(result.card_type) {
                    $("#credit_card_type").val(result.card_type.name) };
                });


 ga('require', 'displayfeatures');
 ga('send', 'pageview',{'page': '/donate-checkout','title': 'Donate Now'});
  if($("#userId").val() ==""){
    $("#confirm_email_id").blur(function() {
        if ($("#confirm_email_id").val() != "")
            if ($("#email_id").val() != $(
                "#confirm_email_id").val()) {
                $("#confrm_email_green").hide();
                $("#confrm_email_red").show()
            } else {
                $("#confrm_email_green").
                show();
                $("#confrm_email_red").hide()
            }
    });
    $("#confirm_email_id").focus(function() {
        if ($("#confirm_email_id").val() != "")
            if ($("#email_id").val() != $("#confirm_email_id").val()) {
                $("#confrm_email_green").hide();
                $("#confrm_email_red").show()
            } else {
                $("#confrm_email_green").show();
                $("#confrm_email_red").hide()
            }
    });
	}
    if($("#userId").val() ==""){
    $("#email_id").blur(function() {
        var filter =
        /^([\w-\.\+]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if ($("#email_id").val() != "" && filter.test($("#email_id").val())) {} 
		else {$("#email_exits_red").show();$("#email_exits_green").hide()}
		});
	}

jQuery.validator.addMethod("lettersandapostropheonly", function(value, element) {
    return this.optional(element) || /^[a-z '\-]+$/i.test(value)}, INVALID_NAME);
jQuery.validator.addMethod("lettersonly", function(value,element) {
    return this.optional(element) || /^[a-z ]+$/i.test(value)
}, INVALID_NAME);
jQuery.validator.addMethod(
    "lettersanddotonly", function(value, element) {
        return this.optional(element) || /^[a-z.']+$/i.test(value)
    }, INVALID_NAME);
jQuery.validator.addMethod("zipcodevalidation", function(value, element) {
    return this.optional(element) || /^[a-z0-9- ]+$/i.
    test(value)
}, INVALID_ZIP);
jQuery.validator.
    addMethod("creditcardexpiry",function(value,element){
        var form=element.form,expiry=form.credit_card_exp_year.value+form.
        credit_card_exp_month.value,date=new Date(),month=date.getMonth()+1,now=""+date.getFullYear()+(month<10?"0"+month:month)
        ;

        return expiry>=now
    });
	
	
$("#loginfrm").validate({
    submitHandler:function(){
        var loginUrl;
        if(refererPath)loginUrl="/login/"+refererPath;else loginUrl="/login";
        var dataStr=$
        ("#loginfrm").serialize();

       
        $.ajax({
            type:"POST",
            url:loginUrl,
            data:dataStr,
            success:function(responseData){
                var jsonObj=jQuery.
                parseJSON(responseData);
                if(jsonObj.status=="success"){
                    if(jsonObj.is_active==false)
                            window.location.href="/verify-email/"+jsonObj.id;
                    else        
                        window.location.href='/checkout-donate';
                 }
            else if(jsonObj.is_email_verified==false)window.location.href="/update-email/"
                +jsonObj.id;else $("#error_message").html(jsonObj.message)
                }
            });
				$("#ajx_loader").css({
				    "z-index":""
				});
				hideAjxLoader()
				    },
				rules:{
				    user_name:{
				        required:true
				    },
				    password:{
				        required:true
				    }
				},
				messages:{
				    user_name:{
				        required:'Please enter Username/Email.'
				    },
				    password:{
				        required:'Please enter Password.'
				    }
				}
})	
	

 $("input[name=credit_card_number]").bind('keyup change',function(){ 
        $("#error_msg").html('');
 });
	
$("#donation_checkout").validate({
submitHandler: function() {
        var dataStr = $("#donation_checkout").serialize();
        $("#error_msg").html('');
        showAjxLoader
        ();
        $.ajax({
            type: "POST", 
            url: "/checkout-donate", 
            data: dataStr, 
            success: function(responseData) {
            
                hideAjxLoader();
                var jsonObj = jQuery.
                parseJSON(responseData);
                if (jsonObj.status == "success") {      
					 parent.window.location.href =SITE_URL+"/donate-confirmation";
                    
                } else if ((jsonObj.status == "error" || jsonObj.status == "payment_error") && jsonObj.message)
                    $("#error_msg").html('<label class="error" style="display:block;font-size:20px !important">' + jsonObj.message + "</label>");
                else
                    $.each(jsonObj.message, function(i, msg) {
                        $('label[for="' + i + '"]').remove();
                        $("#"+i).after('<label for="' + i + '" class="error" style="display:block;">' + msg + "</label>")
                    })
            }
        })
}, 
onchange: function(element) {
         $(element).valid();
    },
rules: {
    first_name: {
        required: true, 
        minlength: 1, 
        maxlength: 50, 
        lettersandapostropheonly: true
    },
	last_name: {
        required: true, 
        minlength: 1, 
        maxlength: 20, 
        lettersandapostropheonly: true
    },
    email_id: {
        required: function(){
                        if($("#userId").val() ==""){return true;}else {return false;}
                    }, 
        email: function(){
                        if($("#userId").val() ==""){return true;}else {return false;}
                    },
    }, 
    confirm_email_id: {
        required: function(){
                        if($("#userId").val() ==""){return true;}else {return false;}
                    }, 
        equalTo: function(){
                        if($("#userId").val() ==""){return "#donation_checkout #email_id";}else {return false;}
                    }
    }, 
	address_1:{
	 required: true,
	},
	address_2:{
	 required: false,
	},
	zipcode: {
		required: true,
        zipcodevalidation: true, 
        minlength: 4, 
        maxlength: 12
    }, 
	city: {
        required: true
    },
	state: {
        required: true
    },
	state_id: {
        required: function(){
                        if($("#country").val()=="228"){return false;}else {return true;}
                    },
    },
	country: {
        required: true
    },
    credit_card_number:{
    required:true,
    digits:true,
    maxlength:18,
    minlength:12
    },
    credit_card_code:{
	 required:true,
     maxlength:4,
     minlength:3,
     digits:true
     }, 
     credit_card_exp_month:{
        required:true,
	     creditcardexpiry:true
     },
     credit_card_exp_year:{
     required:true,
     creditcardexpiry:true
     }
  
    
}, 
messages: {
    first_name: {
        required: 'Please enter First Name.'
    }, 
    last_name: {
        required: 'Please enter Last Name.'
    }, 
    email_id: {
        required: 'Please enter Email.', 
        email: 'Please enter valid Email.'
    }, 
    confirm_email_id: {
        required:'Please enter Confirm Email.',
        equalTo: 'Email and Confirm Email are not same.'
    },
	address_1: {
        required: 'Please enter Address line 1.'
    },
	address_2: {
        required: EMPTY_ADD_LINE_2
    },
    zipcode: {
        required: 'Please enter valid Postal Code.'
    },
	city: {
        required: 'Please enter City.'
    },
	state: {
        required: 'Please select State.'
    },
    state_id: {
        required: 'Please enter State.'
    },
    country: {
        required: 'Please select Country.'
    },
	credit_card_number: {
        required: 'Please enter Credit Card number.'
    },
    credit_card_code: {
        required: 'Please enter Credit Card code.'
    },
	credit_card_exp_month: {
        required: 'Plese select valid Expiry Date.',
		creditcardexpiry: "Plese select valid Expiry Date."
    },
    credit_card_exp_year: {
        required: CC_YEAR,
        creditcardexpiry: "Plese select valid Expiry Date."
    }
 
},
 errorPlacement: function(error, element) {
	            var name = element.attr("name");
	            if (name === "credit_card_exp_month" || name === "credit_card_exp_year") {
	                 $("#anyErrorIndication").html(error);
	            }
	            else {
	                error.insertAfter(element);
	            }
            }
});

})

function updateLoginDetails(){
    $.ajax({
        type:"POST",
        url:"/login-details",
        async:false,
        success:function(
            responseData){
            parent.$("#welcome-div").html(responseData);
            parent.parent.$("#welcome-div").html(responseData);
            parent.$(
                "#menusignup").html('<a href="/profile">My File</a>');
            parent.parent.$("#menusignup").html(
                '<a href="/profile">My File</a>');
            parent.$("#menulogin").remove();
            parent.parent.$("#menulogin").remove()
            }
        });
        }