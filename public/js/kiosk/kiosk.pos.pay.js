$(function(){
$('html').click(function (){ 
	$("#card_details").focus();
});
		/* used to check the slot is available or not. */
		$.ajax({
                    type:"POST",
                    url:"/check-avaliable-kiosk",
                    data:{},
                    success:function(responseData){
                        var jsonObj=jQuery.parseJSON(responseData);
                        if(jsonObj.status=="error")
                        {   
                            $("#slotErrorMessage").show();
                            $("#slotErrorMessage").html(jsonObj.message);
                            $("#submitbutton").hide();
                        }
                        else{
                                          
                            }
                        }
                });


	jQuery.validator.addMethod("lettersandapostropheonly", function(value, element) {
	    return this.
	    optional(element) || /^[a-z '\-]+$/i.test(value)
	}, 'Please enter Valid Name.');

    
    // swap card form validation
/*    
    $("#pay_purchase_session").validate({
        submitHandler:function(){
			var dataStr=$("#pay_purchase_session").serialize();
			showAjxLoader();
			$.ajax({
                    type:"POST",
                    url:"/purchase-session-zipcode",
                    data:dataStr,
                    success:function(responseData){
                        var jsonObj=jQuery.parseJSON(responseData);
                        if(jsonObj.status=="success")
                        {   
                            hideAjxLoader();
                            window.location.href="/purchase-session-zipcode";    
                        }
                        else{
                               
                            }
                        }
                });
			}	
        ,
        rules:{
        },
        messages:{
        }
    });
    */
    
    
$("#credit_card").click(function(){
		$("#step2").hide();
		//$("#swapcard").show();
		$("#card_details").focus();
		$("#creditcard_div").hide();
		$("#payment_mode").val('credit_card');
	});	
$("#show_cred_card_old").click(function(){
	//if($.trim($("#card_details").val())!='') {
		$("#step2").hide();
		$("#swapcard").hide();
		$("#cash_div").hide();
		$("#step3").show();
		$("#creditcard_div").show();
		$("#payment_mode").val('credit_card');
	/*}	
	else {
			$("#swpaCardError").show();
			$("#card_details").focus();
			return false;
	}*/
});
$("#cash,.cash_btn").click(function(){ 
		$("#step2").hide();
		$("#step3").show();
		$("#cash_div").show();
		$("#creditcard_div").hide();
		$("#payment_mode").val('cash');
		$("#credit_card_error").hide();

	});	
$("#cash_confirm2, .credit_card_error").click(function(){
		$("#card_details").val('');
		$("#step2").hide();
		$("#credit_card_error").hide();
		//$("#swapcard").show();
		$("#card_details").focus();
		$("#step3").hide();
		$("#creditcard_div").show();
		$("#cash_div").hide();
		$("#payment_mode").val('credit_card');
	});		
$("#tab1").click(function(){	
		$("#step1").show();
		$("#step2").hide();
		$("#step3").hide();
	});
$("#tab2,#cash_go_back,#credit_card_go_back").click(function(){	
		$("#step1").hide();
		$("#step2").show();
		$("#step3").hide();
		$("#swapcard").hide();
	});	
$("#card_details").on('change input',function(){
showAjxLoader();
	setTimeout("changeFocus()","1000");	
});	
});

function changeFocus(){
	if($.trim($("#card_details").val())!='') {
		var str = $.trim($("#card_details").val()).split(';');
		if(str[0].length>=50) {
					//$("#step2").hide();
					//$("#swapcard").hide();
					//$("#cash_div").hide();
					//$("#step3").show();
					//$("#creditcard_div").show();
					//$("#payment_mode").val('credit_card');
					
		}
		else {
			$("#card_details").val('');
			//$("#zipcode").val('');
			$("#card_details").focus();
			$("#swapcardErrorMessage").show();
		}
	}
		return false;
}
function go_to_back(act){
	window.location.href="/purchase-session/"+act;
	//showAjxLoader();
}
function go_to_cash(act){
	window.location.href="/purchase-session/"+act;
}