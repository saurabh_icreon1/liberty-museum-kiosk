$(function(){
$('input').customInput();

    //showAjxLoader();
    $.ajax({type: "GET",dataType: "json", data: {}, url: "/edit-kiosk-module-details/" + id, success: function(response) {
            if (!checkUserAuthenticationAjx(response))
                return false;
			if (response.kioskModules.kiosk_module_data.length == 0)
                return false;	
				
			   var j=0;
			   $.each(response.kioskModules.kiosk_module_data, function(i, data) {
			  if(data.is_available == 1 || data.is_available == '1'){
			   var rowData = '<div class="view-row"><div class="col1"> <label class="no-padd"><strong>' + data.kiosk_module_name + '</strong></label>Available</div></div>';
			   }else{
			   var textlabel = "available";
					var rowData = '<div class="view-row"><div class="col1"> <label class="no-padd"><strong>' + data.kiosk_module_name + '</strong></label>Not Available</div></div>';
			   }
				$("#kioskmodulesinfo").append(rowData);
				
				 //$.each(response.kioskModules.kiosk_module_child_info, function(k, childdata) {
				 //if(data.kiosk_module_id == childdata.kiosk_module_id){
						//alert("called for "+data.kiosk_module_id);
						$("#kioskmodulesinfo").append(createModuleConfigElement(data.kiosk_module_id,response.kioskModules.kiosk_module_child_info,response.modulesConfig));
					//}
				 
				 //});
			    
				j++;
		   });  hideAjxLoader();
           
        }});

    var LOWER=/[a-z]/,DIGIT=/[0-9]/,DIGITS=/[0-9].*[0-9]/,SPECIAL=/[^a-zA-Z0-9]/,SAME=/^(.)\1+$/;
    function rating(rate,message){
        return{
            rate:rate,
            messageKey:message
        }
    }
	
		 
	function createModuleConfigElement(kiosk_module_id,kioskModulesSettingElement,modulesConfig){ 
  //console.log("called "+ kiosk_module_id);

	var html = '';
	var element= '';
	var settingNameArray = new Array();
	var settingNameValArray = new Array();
	var resultVal = "";
	var str = '';
	var countr = 0;
	var labelCntr = 0;
		$.each(kioskModulesSettingElement, function(l, childElement) {
			 
					if(kiosk_module_id == childElement.kiosk_module_id){
						if(childElement.setting_display_type != ""){//console.log(v.setting_title_name);
								switch(childElement.setting_display_type) {
									case 1:
									case '1':
										//console.log(settingNameArray);
										settingNameArray = childElement.setting_name.split(",");
										settingNameValArray = childElement.setting_value.split(",");
										element = '<div class="view-row"><div class="col1"> <label class="no-padd  m-l-50"><strong>'+childElement.setting_title_name+'</strong></label>';
										for(var j=0;j<settingNameArray.length;j++){
	
										  if(childElement.kiosk_module_setting_value == settingNameValArray[j]){
											element +=''+settingNameArray[j]+'';
										  }
											
										   countr++;
										}
										element += '</div></div>';
										
										html +=element;
										element = "";
										settingNameArray ="";
										settingNameValArray="";
										break;
									case 2:
									case '2':
								
										element = '<div class="view-row"><div class="col1"> <label class="no-padd"><strong>'+childElement.setting_title_name+'<strong></label>'+childElement.kiosk_module_setting_value+'</div></div>';
										html +=element;
										
										countr++;
										break;
									case 3:
									case '3':
										element = '<div class="row"><div class="col1"><input type="checkbox" name="module_settings['+childElement.kiosk_setting_id+']" value="'+childElement.setting_value+'">'+childElement.setting_title_name+'</div></div>';
										html +=element;
										
										countr++;
										break;
									
									default:
										element = '';
										html +=element;
										//html.concat(element);
								}
						}
					
					}
					labelCntr++;
							
			 });
			 //console.log(html1+html2+html3);
			 //html = html1+html2+html3
			 return html;
		 }

$.validator.classRuleSettings.password={
    password:true
};

    $.validator.addMethod("kioskIp",function(value,element){
        return this.optional(element)|| /\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b/.test(value)
        },
    VALID_KIOSK_IP);
 var clicked = 0;
 var vclicked = 0;
$("#edit_kiosk").submit(function(){
 
 timeFormatRe = /^\d{1,2}:\d{2}([ap]m)?$/;
 
 var timeformat= $('#module_settings_40').val();
 var timeBetweenUsers= $('#module_settings_41').val();
 
 if(!timeFormatRe.test(timeformat)){
	clicked++;
	if(clicked == 1){
	$('<label for="module_settings_40" generated="true" class="custom_error" style="display:block">Please enter Valid Time.</label>' ).insertAfter('#module_settings_40');
	}

 }else{
	  if(clicked == 1){
	  $("#module_settings_40").next().remove();
	  }
 }
 if(!timeFormatRe.test(timeBetweenUsers)){
	vclicked++;
	if(vclicked == 1){
	$('<label for="module_settings_41" generated="true" class="custom_error" style="display:block">Please enter Valid Time.</label>' ).insertAfter('#module_settings_41');
	}
	 
 }else{
	if(vclicked == 1){
		$("#module_settings_41").next().remove();
	  }
 }
 
	var dataStr=$("#edit_kiosk").serialize();
            showAjxLoader();
            $.ajax({
                type:"POST",
                url:
                "/edit-kiosk-module",
                data:dataStr,
                success:function(responseData){
                    hideAjxLoader();
                    if(!checkUserAuthenticationAjx(
                        responseData))return false;
                    var jsonObj=jQuery.parseJSON(responseData);
                    if(jsonObj.status=="success")window.location.href=
                        "/kiosk-module-management";else $.each(jsonObj.message,function(i,msg){
                        $("#"+i).after('<label class="error" style="display:block;">'+
                            msg+"</label>")
                        })
                    }
                    });
});

$("#edit_kiosk").validate({

    submitHandler:function(){
	 
        },
highlight:function(label){
    $(".detail-section").css("display","block")
    },
ignore:"",
rules:{
        kiosk_name
        :{
            required:true,
            minlength:1,
            maxlength:50
        },
        
},
messages:{
    kiosk_name:{
        required:EMPTY_KIOSK_NAME
    },
    
}
});

$("#report_to").autocomplete({
    afterAdd:true,
    selectFirst:
    true,
    autoFocus:true,
    source:function(request,response){
        $.ajax({
            url:"/get-crm-contacts",
            method:"POST",
            dataType:"json",
            data
            :{
                crm_full_name:$("#report_to").val()
                },
            success:function(jsonResult){
                if(!checkUserAuthenticationAjx(jsonResult))
                    return false;
                var resultset=[];
                $.each(jsonResult,function(){
                    var id=this.crm_user_id,value=this.crm_full_name;
                    resultset.
                    push({
                        id:this.crm_user_id,
                        value:this.crm_full_name
                        })
                    });
                response(resultset)
                }
            })
    },
open:function(){
    $(".ui-menu").width(300)
    }
,
change:function(event,ui){
    if(ui.item){
        $("#report_to").val(ui.item.value);
        $("#report_to_id").val(ui.item.id)
        }else{
        $(
            "#report_to").val("");
        $("#report_to_id").val("")
        }
        $(".ui-autocomplete").jScrollPane()
    },
focus:function(event,ui){
    $(
        "#report_to_id").val(ui.item.id);
    return false
    },
select:function(event,ui){
    $("#report_to").val(ui.item.value);
    $(
        "#report_to_id").val(ui.item.id);
    return false
    }
});
var url="/upload-crm-user-profile";
$("#virtualface").fileupload({
    url:
    url,
    dataType:"json",
    start:function(e){
        $("#submitbutton").prop("disabled",true);
        showAjxLoader()
        },
    done:function(e,data){
        hideAjxLoader();
        data=data.result;
        if(data.status=="success"){
            $("#filemessage").addClass("success-msg");
            $("#filemessage").
            removeClass("error-msg");
            $("#thumbnail_virtualface").val(data.filename)
            }else{
            $("#filemessage").removeClass("success-msg"
                );
            $("#filemessage").addClass("error-msg");
            $("#thumbnail_virtualface").val("")
            }
            $("#filemessage").html(data.message);
        $(
            "#submitbutton").prop("disabled",false)
        },
    progressall:function(e,data){
        var progress=parseInt(data.loaded/data.total*100,
            10);
        $("#progress .bar").css("width",progress+"%")
        }
    }).prop("disabled",!$.support.fileInput).parent().addClass($.support.
    fileInput?undefined:"disabled");
$("#remove").click(function(){
    $("#upload_old_div").hide();
    $("#upload_new_div").show();
    $(
        "#thumbnail_virtualface").val("")
    })

});
function deleteKiosk(kiosk_id){
    
    $('.delete_contact').colorbox({
        width:"550px",
        height:"200px",
        inline: true
    });
    $("#no_delete").click(function(){
        $.colorbox.close();
        $('#yes_delete').unbind('click');
        return false;
    });
    
    $("#yes_delete").click(function(){
        $('#yes_delete').unbind('click');
        $.ajax
        ({
            type: 'POST',
            data: {
                kioks_id:kiosk_id
            },
            url: '/delete-kiosk',
            success: function(response)
            {
                if(!checkUserAuthenticationAjx(response)){return false;}
                var jsonObj =  jQuery.parseJSON(response); 
                if(jsonObj.status == 'success')
                {
                    $("#success_message").html(jsonObj.message);
                    window.location.href="/kiosk-module-management";;
                }
            }
        });
    });
}
function cancelKiosk(){
    
    $('.cancel_kiosk').colorbox({
        width:"550px",
        height:"200px",
        inline: true
    });
    $("#no_cancel").click(function(){
        $.colorbox.close();
        $('#yes_delete').unbind('click');
        return false;
    });
    
    $("#yes_cancel").click(function(){
        $('#yes_cancel').unbind('click');
         window.location.href="/kiosk-module-management";
    });
}

