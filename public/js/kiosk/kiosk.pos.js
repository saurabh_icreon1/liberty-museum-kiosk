$(function(){
$('html').click(function (){ 
	$("#card_details").focus();
});
		/* used to check the slot is available or not. */
		$.ajax({
                    type:"POST",
                    url:"/check-avaliable-kiosk",
                    data:{},
                    success:function(responseData){
                        var jsonObj=jQuery.parseJSON(responseData);
                        if(jsonObj.status=="error")
                        {   
                            $("#slotErrorMessage").show();
                            $("#slotErrorMessage").html(jsonObj.message);
                            $("#submitbutton").hide();
                        }
                        else{
                                          
                            }
                        }
                });


	//$("#other_state").hide();

	jQuery.validator.addMethod("lettersandapostropheonly", function(value, element) {
	    return this.
	    optional(element) || /^[a-z '\-]+$/i.test(value)
	}, 'Please enter Valid Name.');

        $("#purchase_session").validate({
        submitHandler:function(){
        if($("#step1").is(':visible')==true && kiosk_fee!=0) {
	            $("#step1").hide();
				$("#step2").show();
				$("#user_name_id1").html($("#first_name").val()+' '+$("#last_name").val());
				$("#user_name_id2").html($("#first_name").val()+' '+$("#last_name").val());
				$("#user_name_id3").html($("#first_name").val()+' '+$("#last_name").val());
				$("#user_name_id4").html($("#first_name").val()+' '+$("#last_name").val());
			}
		else {
			var dataStr=$("#purchase_session").serialize();
			$("#confirmMessage").hide();
			$("#creditcard_confirm").hide();
			/*
				if($("#card_details").val()!='') {
					if($("#zipcode").val() == '') {
						hideAjxLoader();
						$("#creditcard_confirm").show();
						return false;
					}
				}*/
				

			if($("#payment_mode").val() == "cash" || kiosk_fee==0) {	
					showAjxLoader();
					var postUrl = "/purchase-session";
						$.ajax({
			                    type:"POST",
			                    url:postUrl,
			                    data:dataStr,
			                    success:function(responseData){
			                        var jsonObj=jQuery.parseJSON(responseData);
			                        if(jsonObj.status=="success")
			                        {   
			                            hideAjxLoader();
			                            if($("#payment_mode").val() == "cash" || kiosk_fee==0) {
			                                    
			                                    var contactId = jsonObj.contact_id;
			                                    $.ajax({
								                    type:"POST",
								                    url:"/add-reservation-contact",
								                    data:{contact_id:jsonObj.contact_id,transaction_id:jsonObj.transaction_id, authorize_transaction_id:jsonObj.authorize_transaction_id,payment_mode:jsonObj.payment_mode, amount:jsonObj.amount, first_name:jsonObj.first_name, last_name:jsonObj.last_name},
								                    success:function(responseData){
								                    
								                        
								                        var jsonObj2=jQuery.parseJSON(responseData);
								                        if(jsonObj2.status=="success")
								                        {   
								                            window.location.href=SITE_URL+"/purchase-session-confirmation/"+jsonObj2.queue_id;
								                            
								                        }
								                        else{
								                                $("#confirmMessage").show();
								                                $("#confirmMessage_cash").show();
									                            $("#confirmMessage").html(jsonObj2.message);
									                            $("#confirmMessage_cash").html(jsonObj2.message);
									                            $("#creditcard_confirm").hide();
								                            }
								                        }
								                });        
			                                    
			                                    
			                            }
			                            else {
			                                window.location.href="/pay-purchase-session";    
			                            }
			                        }
		                        else if(jsonObj.status=='credit_card_error' || jsonObj.status=='payment_error'){
		                                hideAjxLoader();
		                                $("#card_details").val('');
			                                $("#zipcode").val('');
				                            $("#credit_card_error").show();
				                            $("#creditcard_confirm").show();
				                            $("#step3").hide();
			                            }
			                        }
			                });					
					
				}
				else {
					//var postUrl = "/pay-purchase-session";
				}
			}	
        }
        ,
        rules:{
            first_name:{
                required: true,
		        minlength: 1, 
		        maxlength: 50, 
		        lettersandapostropheonly: true
            },
            last_name:{
                required: true,
		        minlength: 1, 
		        maxlength: 20, 
		        lettersandapostropheonly: true
            },
            email:{
                required:function(){
                    if($("#showAddressBlockId").is(':visible'))return false;else return true
	                },
                email:true,
                maxlength: 100 
            },
            address:{
                required:function(){
	                if($("#showAddressBlockId").is(':visible'))return true;else return false
	                }
                },
            city:{
                required:function(){
	                if($("#showAddressBlockId").is(':visible'))return true;else return false
	                }
                },    
            zipcode:{
                required:function(){
                if($("#creditcard_div").is(':visible'))return true;else return false
                },
            maxlength:12,
            minlength:4                    
            
            },
        },
        messages:{
            first_name:{
                required:"Please enter First or Given Name."
            },
            last_name:{
                required:"Please enter Last Name or Family Name."
            },
            email:{
                required:"Please enter Email Address.",
                email:"Please enter Valid Email Address."
            },
            address:{
                required:"Please enter Street Address."
            },
            city:{
                required:"Please enter City."
            },
            zipcode:{
                required :"Please enter Billing Zip or Postal Code."
            }
        }
    });
    
 
    $("#country").change(function(){
        if($("#country").val() != 228) {
	            $("#other_state").show();
	            $("#state").val(' ').select2(); 
                $("#state_div").hide();
                $("#state").hide();
        }
        else {
	            $("#other_state").hide();
	            $("#state_div").show();
                $("#state").show();        
        }    
    });
$("#credit_card,#cash_confirm2").click(function(){
		showAjxLoader();
		//$("#credit_card").hide();
		//$("#cash").hide();
		//$("#tab1").hide();
		$("#processing_1").show();
		//$("#processing_1").html("Processing...");
		//$("#step2").hide();
		//$("#swapcard").show();
		//$("#card_details").focus();
		//$("#creditcard_div").hide();
		$("#payment_mode").val('credit_card');
		document.purchase_session.submit();
	});	
$("#show_cred_card_old").click(function(){
	//if($.trim($("#card_details").val())!='') {
		$("#step2").hide();
		$("#swapcard").hide();
		$("#cash_div").hide();
		$("#step3").show();
		$("#creditcard_div").show();
		$("#payment_mode").val('credit_card');
	/*}	
	else {
			$("#swpaCardError").show();
			$("#card_details").focus();
			return false;
	}*/
});
$("#cash,.cash_btn").click(function(){
		$("#step2").hide();
		$("#step3").show();
		$("#cash_div").show();
		$("#creditcard_div").hide();
		$("#payment_mode").val('cash');
		$("#credit_card_error").hide();

	});	
$("#cash_confirm2, .credit_card_error").click(function(){
		$("#card_details").val('');
		$("#step2").hide();
		$("#credit_card_error").hide();
		//$("#swapcard").show();
		$("#card_details").focus();
		//$("#step3").hide();
		$("#creditcard_div").show();
		//$("#cash_div").hide();
		$("#payment_mode").val('credit_card');
	});		
$("#tab1").click(function(){	
		$("#step1").show();
		$("#step2").hide();
		$("#step3").hide();
	});
$("#tab2,#cash_go_back,#credit_card_go_back").click(function(){	
		$("#step1").hide();
		$("#step2").show();
		$("#step3").hide();
		$("#swapcard").hide();
	});	
$("#card_details").on('change input',function(){
	setTimeout("changeFocus()","1000");	
});	
});

function changeFocus(){
	if($.trim($("#card_details").val())!='') {
		var str = $.trim($("#card_details").val()).split(';');
		if(str[0].length>=50) {
					//$("#step2").hide();
					//$("#swapcard").hide();
					//$("#cash_div").hide();
					//$("#step3").show();
					//$("#creditcard_div").show();
					//$("#payment_mode").val('credit_card');
		}
		else {
			//$("#card_details").val('');
			//$("#zipcode").val('');
			$("#card_details").focus();
			$("#swapcardErrorMessage").show();
		}
	}
		return false;
}
function showAddressBlock(){
		$('#showAddressBlockId').show();
		$('#showEmailBlockBtn').show();
		$("#donotId").hide();
		$("#email_lebel_msg").hide();
		$("#emailDivId").hide();
		$("#email").val('');
		$('#country').val('228').select2();
}
function showEmailBlock(){
	if($("#showAddressBlockId").is(':visible')==true ) {
		$('#showAddressBlockId').hide();
		$('#showEmailBlockBtn').hide();
		$("#email_lebel_msg").show();
		$('#emailDivId').show();
		$("#donotId").show();
		$("#address").val('');
		$("#city").val('');
		$("#state").val('');
		$("#other_state").val('');
		$('#country').val('');
	}
	else {
		window.location.href='/';
	}
}	
function print_html(){
$.ajax({
                    type:"POST",
                    url:"/print-page-content",
                    data:{},
                    success:function(responseData){
                       
                        }
                });
}
function showProcessing(){
	//showAjxLoader();
}
			