$(function(){
//$("#kiosk_shutoff_time").hide();


    var LOWER=/[a-z]/,DIGIT=/[0-9]/,DIGITS=/[0-9].*[0-9]/,SPECIAL=/[^a-zA-Z0-9]/,SAME=/^(.)\1+$/;
    function 
    rating(rate,message){
        return{
            rate:rate,
            messageKey:message
        }
    }


$.validator.classRuleSettings.password={
    password:true
};

    $.validator.addMethod("kioskIp",function(value,element){
        return this.optional(element)|| /\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b/.test(value)
        },
    VALID_KIOSK_IP);
	$.validator.addMethod("kioskShutOffTime",function(value,element){
        return this.optional(element)|| /^(?:1[0-2]|0[0-9]):[0-5][0-9]:[0-5][0-9]$/.test(value)
        },
    VALID_KIOSK_IP);
	
	$.validator.addMethod("KioskRegex", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-]+$/i.test(value);
    }, "Kiosk Name must contain only letters, numbers, or dashes.");

  
    var validator=$("#kiosk").validate({
	submitHandler:function(){
            var dataStr=$("#kiosk").serialize();
            showAjxLoader();
            $.ajax({
                type:"POST",
                url:
                "/add-kiosk",
                data:dataStr,
                success:function(responseData){
                    hideAjxLoader();
                    if(!checkUserAuthenticationAjx(
                        responseData))return false;
                    var jsonObj=jQuery.parseJSON(responseData);
                    if(jsonObj.status=="success") parent.window.location.href=
                        "/kiosk";else $.each(jsonObj.message,function(i,msg){
                        $("#"+i).after('<label class="error" style="display:block;">'+
                            msg+"</label>")
                        })
                    }
                    })
        },
    highlight:function(label){
        $(".detail-section").css("display","block")
        },
    ignore:"",
    rules:{
        kiosk_name
        :{
            required:true,
			KioskRegex: true,
            minlength:1,
            maxlength:50
        },
        kiosk_ip:{
            required:true,
            kioskIp:true,
            minlength:1,
            maxlength:20
        },
		kiosk_shutoff_time:{
		required: function(element){ 
		if($("input:radio[name=kiosk_shutoff]:checked").val() == 1){return true;
		}else{ return false;}},
		kioskShutOffTime:function(element){ 
		if($("input:radio[name=kiosk_shutoff]:checked").val() == 1){return true;
		}else{ return false;}},
		},
        kiosk_capacity:{
            required:true,
			minlength:1,
            maxlength:3
        },
        status:{
            required:true
        },
		kiosk_wheelchair:{
            required:true
        },
		koisk_ads:{
            required:true
        },
		kiosk_note:{
            required:true
        },
},
messages:{
    kiosk_name:{
        required:EMPTY_KIOSK_NAME,
		KioskRegex:'Kiosk Name must contain only letters, numbers, or dashes.',
    },
    kiosk_ip:{
        required:EMPTY_KIOSK_IP
    },
	kiosk_shutoff_time:{
        required:EMPTY_SHUTOFF_TIME,
		kioskShutOffTime: VALID_SHUTOFF_TIME
    },
    status:{
        required:EMPTY_STATUS
    },
	kiosk_capacity:{
		required:EMPTY_CAPACITY
	},
	kiosk_wheelchair:{
        required:EMPTY_KIOSK_WHEELCHAIR
    },
    koisk_ads:{
        required:EMPTY_KIOSK_ADA
    },
    kiosk_note:{
        required:EMPTY_KIOSK_NOTE
    }
}
});

$("#edit_kiosk").validate({
    submitHandler:function(){
            var dataStr=$("#edit_kiosk").serialize();
            showAjxLoader();
            $.ajax({
                type:"POST",
                url:
                "/edit-kiosk",
                data:dataStr,
                success:function(responseData){
                    hideAjxLoader();
                    if(!checkUserAuthenticationAjx(
                        responseData))return false;
                    var jsonObj=jQuery.parseJSON(responseData);
                    if(jsonObj.status=="success")window.location.href=
                        "/kiosk";else $.each(jsonObj.message,function(i,msg){
                        $("#"+i).after('<label class="error" style="display:block;">'+
                            msg+"</label>")
                        })
                    }
                    })
        },
highlight:function(
    label){
    $(".detail-section").css("display","block")
    },
ignore:"",
rules:{
        kiosk_name
        :{
            required:true,
			KioskRegex: true,
            minlength:1,
            maxlength:50
        },
        kiosk_ip:{
            required:true,
            kioskIp:true,
            minlength:1,
            maxlength:20
        },
		kiosk_shutoff_time:{
		required: function(element){ 
		if($("input:radio[name=kiosk_shutoff]:checked").val() == 1){return true;
		}else{ return false;}},
		kioskShutOffTime:function(element){ 
		if($("input:radio[name=kiosk_shutoff]:checked").val() == 1){return true;
		}else{ return false;}},
		},
        kiosk_capacity:{
            required:true,
        },
        status:{
            required:true
        },
		kiosk_wheelchair:{
            required:true
        },
		koisk_ads:{
            required:true
        },
		kiosk_note:{
            required:true
        },
},
messages:{
    kiosk_name:{
        required:EMPTY_KIOSK_NAME,
		KioskRegex:'Kiosk Name must contain only letters, numbers, or dashes.',
    },
    kiosk_ip:{
        required:EMPTY_KIOSK_IP
    },
	kiosk_shutoff_time:{
        required:EMPTY_SHUTOFF_TIME,
		kioskShutOffTime: VALID_SHUTOFF_TIME
    },
    status:{
        required:EMPTY_STATUS
    },
	kiosk_capacity:{
		required:EMPTY_CAPACITY
	},
	kiosk_wheelchair:{
        required:EMPTY_KIOSK_WHEELCHAIR
    },
    koisk_ads:{
        required:EMPTY_KIOSK_ADA
    },
    kiosk_note:{
        required:EMPTY_KIOSK_NOTE
    }
}
});
$("#report_to").autocomplete({
    afterAdd:true,
    selectFirst:
    true,
    autoFocus:true,
    source:function(request,response){
        $.ajax({
            url:"/get-crm-contacts",
            method:"POST",
            dataType:"json",
            data
            :{
                crm_full_name:$("#report_to").val()
                },
            success:function(jsonResult){
                if(!checkUserAuthenticationAjx(jsonResult))
                    return false;
                var resultset=[];
                $.each(jsonResult,function(){
                    var id=this.crm_user_id,value=this.crm_full_name;
                    resultset.
                    push({
                        id:this.crm_user_id,
                        value:this.crm_full_name
                        })
                    });
                response(resultset)
                }
            })
    },
open:function(){
    $(".ui-menu").width(300)
    }
,
change:function(event,ui){
    if(ui.item){
        $("#report_to").val(ui.item.value);
        $("#report_to_id").val(ui.item.id)
        }else{
        $(
            "#report_to").val("");
        $("#report_to_id").val("")
        }
        $(".ui-autocomplete").jScrollPane()
    },
focus:function(event,ui){
    $(
        "#report_to_id").val(ui.item.id);
    return false
    },
select:function(event,ui){
    $("#report_to").val(ui.item.value);
    $(
        "#report_to_id").val(ui.item.id);
    return false
    }
});
var url="/upload-crm-user-profile";
$("#virtualface").fileupload({
    url:
    url,
    dataType:"json",
    start:function(e){
        $("#submitbutton").prop("disabled",true);
        showAjxLoader()
        },
    done:function(e,data){
        hideAjxLoader();
        data=data.result;
        if(data.status=="success"){
            $("#filemessage").addClass("success-msg");
            $("#filemessage").
            removeClass("error-msg");
            $("#thumbnail_virtualface").val(data.filename)
            }else{
            $("#filemessage").removeClass("success-msg"
                );
            $("#filemessage").addClass("error-msg");
            $("#thumbnail_virtualface").val("")
            }
            $("#filemessage").html(data.message);
        $(
            "#submitbutton").prop("disabled",false)
        },
    progressall:function(e,data){
        var progress=parseInt(data.loaded/data.total*100,
            10);
        $("#progress .bar").css("width",progress+"%")
        }
    }).prop("disabled",!$.support.fileInput).parent().addClass($.support.
    fileInput?undefined:"disabled");
$("#remove").click(function(){
    $("#upload_old_div").hide();
    $("#upload_new_div").show();
    $(
        "#thumbnail_virtualface").val("")
    })

});
function deleteKiosk(kiosk_id){
    
    $('.delete_contact').colorbox({
        width:"550px",
        height:"200px",
        inline: true
    });
    $("#no_delete").click(function(){
        $.colorbox.close();
        $('#yes_delete').unbind('click');
        return false;
    });
    
    $("#yes_delete").click(function(){
        $('#yes_delete').unbind('click');
        $.ajax
        ({
            type: 'POST',
            data: {
                kioks_id:kiosk_id
            },
            url: '/delete-kiosk',
            success: function(response)
            {
                if(!checkUserAuthenticationAjx(response)){return false;}
                var jsonObj =  jQuery.parseJSON(response); 
                if(jsonObj.status == 'success')
                {
                    $("#success_message").html(jsonObj.message);
                    window.location.href="/kiosk";;
                }
            }
        });
    });
}
function cancelKiosk(){
    
    $('.cancel_kiosk').colorbox({
        width:"550px",
        height:"200px",
        inline: true
    });
    $("#no_cancel").click(function(){
        $.colorbox.close();
        $('#yes_delete').unbind('click');
        return false;
    });
    
    $("#yes_cancel").click(function(){
        $('#yes_cancel').unbind('click');
         window.location.href="/kiosk";
    });
}
