$(function(){
$('html').click(function (){ 
	$("#card_details").focus();
});
	jQuery.validator.addMethod("lettersandapostropheonly", function(value, element) {
	    return this.
	    optional(element) || /^[a-z '\-]+$/i.test(value)
	}, 'Please enter Valid Name.');

       
    // ziocode form validation    
    
        $("#purchase_session_zipcode").validate({
        submitHandler:function(){
        if($("#step1").is(':visible')==true ) {
	            $("#step1").hide();
				$("#step2").show();
			}
		else {
			var dataStr=$("#purchase_session_zipcode").serialize();
			showAjxLoader();
			$.ajax({
                    type:"POST",
                    url:"/pos-donation",
                    data:dataStr,
                    success:function(responseData){
                        var jsonObj=jQuery.parseJSON(responseData);
                        if(jsonObj.status=="success")
                        {   
                            window.location.href=SITE_URL+"/pos-donation-confirmation/"+jsonObj.transaction_id+"/"+jsonObj.confirmation_code+"/"+jsonObj.user_name;
                        }
                        else{
                                window.location.href="/pay-pos-donation/payment_error";
                            }
                        /*else if(jsonObj.status=='credit_card_error' || jsonObj.status=='payment_error'){
                                hideAjxLoader();
                            	$("#card_details").val('');
                            	$("#zipcode").val('');
	                            $("#credit_card_error").show();
	                            $("#creditcard_confirm").show();
	                            $("#step3").hide();
                            }*/
                        }
                });
			}	
        }
        ,
        rules:{
           zipcode:{
                required: true,
	            maxlength:12,
	            minlength:4  
            }, 
        },
        messages:{
	        zipcode:{
                required :"Please enter Billing Zip or Postal Code."
            }
        }
    });  


});