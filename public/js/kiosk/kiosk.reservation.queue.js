$(function(){
$.jgrid.no_legacy_api=true;
$.jgrid.useJSON=true;
var grid=jQuery("#list"),emptyMsgDiv=$('<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");
$("#list").jqGrid({
    postData:{
        searchString:$("#kioskreservationsearch").serialize()
        },
    mtype:"POST",
    url:"/get-search-kiosk-modules",
    datatype:
    "json",
    sortable:true,
    colNames:["","Contact","Country","Kiosk","Estimated Time","Status","Assign"],
    colModel:[
	{
        name:"id",
        index:"id",
        sortable:true
    },{
        name:"kiosk_name",
        index:"kiosk_name",
        sortable:true
    },{
        name:
        "type",
        index:"type",
        sortable:false
    },{
        name:"status",
        index:"status"
    },{
        name:"seats",
        index:"seats"
    },{
        name:"time_allowed",
        index:"time_allowed",
        sortable:true
    },{
        name:"time_between",
        index:"time_between",
        sortable:false
    }],
    sortname:"kiosk.kiosk_id",
    sortorder:"DESC",
    rowNum:10,
    rowList:[10,20,30],
    pager:"#pcrud",
    multiselect:true,
    viewrecords:true,
    autowidth:true,
    shrinkToFit:true,
    caption:"",
    width:"100%",
    cmTemplate:{
        title:false
    },
    loadComplete:function(data){
        hideAjxLoader();
        var count=grid.getGridParam(),ts=grid[0];
        if(ts.p.reccount===0){
            grid.hide();
            emptyMsgDiv.show();
            $("#pcrud_right div.ui-paging-info").css("display","none")
            }else{
            grid.show();
            emptyMsgDiv.hide();
            $("#pcrud_right div.ui-paging-info").css("display","block")
            }
        var ids=jQuery("#list").jqGrid("getDataIDs");
        for(var i=0;i<ids.length;i++){
            var rowId=ids[i],rowData=jQuery("#list").jqGrid("getRowData",rowId),userRole=rowData.Role;
            if(userRole=="Super Admin"){
                $("#jqg_list_"+rowId).prop("checked",false);
                $("#jqg_list_"+data.rows[i].id).attr("disabled",true)
                }
            }
        },
onSelectAll:function(aRowids,status){
    if(status){
        for(var i=0;i<aRowids.length;i++){
            var rowId=aRowids[i],rowData=jQuery(
                "#list").jqGrid("getRowData",rowId),userRole=rowData.Role;
            if(userRole=="Super Admin")$("#"+rowId).removeClass(
                "ui-state-highlight")
            }
            var cbs=$("tr.jqgrow > td > input.cbox:disabled",grid[0]);
        cbs.removeAttr("checked");
        grid[0].p.
        selarrrow=grid.find("tr.jqgrow:has(td > input.cbox:checked)").map(function(){
            return this.id
            }).get()
        }
    },
onCellSelect: function(rowid,iCol,cellcontent,e){
    var rowData=jQuery("#list").jqGrid("getRowData",rowid),userRole=rowData.Role;
    if(userRole=="Super Admin"){
            $("#"+grid+" tr#"+rowid).css("background","none");
            $("#"+grid+" tr.jqgrow:odd").css("background","#DDDDDC")
        }
    }
});
emptyMsgDiv.insertAfter(grid.parent());
emptyMsgDiv.hide();
$("#list").jqGrid("navGrid","#pcrud",{
    reload:
    true,
    edit:false,
    add:false,
    search:false,
    del:false
});
$("#list").jqGrid("navButtonAdd","#pcrud",{
    caption:"",
    title:"Export",
    id:"exportExcel",
    onClickButton:function(){
        exportExcel("list","/get-search-kiosk")
        },
    position:"last"
});
$("#updateButton").click(function(){
    var ids=jQuery("#list").jqGrid("getGridParam","selarrrow");
    if($("#activity").val()!=""
        &&ids!="")if($("#activity").val()=="delete")$.colorbox({
        width:"900px",
        height:"360px",
        iframe:true,
        href:
        "/change-crm-user-status/"+ids
        });
    else{
        showAjxLoader();
        $.ajax({
            type:"POST",
            url:"/change-crm-user-status",
            data:{
                activity:$
                .trim($("#activity").val()),
                ids:ids
            },
            success:function(responseData){
                var jsonObj=jQuery.parseJSON(responseData);
                if(
                    jsonObj.status=="success")location.reload();else $.each(jsonObj.message,function(i,msg){
                    $("#"+i).after(
                        '<label class="error" style="display:block;">'+msg+"</label>")
                    });
                $("#updateButton").addClass("save-btn");
                $(
                    "#updateButton").removeClass("cancel-btn");
                hideAjxLoader()
                }
            })
    }else{
    $("#updateButton").removeClass("save-btn");
    $("#updateButton").addClass("cancel-btn")
    }
});
});
$("#submitbutton").click(function(){
    $("#success_message").hide();
    $("#successRow").hide();
    showAjxLoader();
    jQuery("#list").jqGrid("setGridParam",{
        postData:{
            searchString:$("#kioskreservationsearch").
            serialize()
            }
        });
jQuery("#list").trigger("reloadGrid",[{
    page:1
}]);
window.location.hash="#save_search";
hideAjxLoader();
    return false
    });


function createKiosk()
{
    showAjxLoader();
    $.ajax({
        type: 'GET',
        data: {},
        url: '/add-kiosk',
        success: function(response)
        {
            if (!checkUserAuthenticationAjx(response)) {
                return false;
            } else {
                hideAjxLoader();
                $.colorbox.close();
                $.colorbox({
                    width: "600px",
                    height: "650px",
                    iframe: true,
                    href: "/add-kiosk"
                });
            }
        }
    });
}