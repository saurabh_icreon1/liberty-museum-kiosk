$(function(){
$('html').click(function (){ 
	$("#card_details").focus();
});
//$("#other_state").hide();
	jQuery.validator.addMethod("lettersandapostropheonly", function(value, element) {
	    return this.
	    optional(element) || /^[a-z '\-]+$/i.test(value)
	}, 'Please enter Valid Name.');

        $("#donation_form").validate({
        submitHandler:function(){
        
		if($("#amount").val() == '' && ($.trim($("#donation_amount").val()) == '' || $("#donation_amount").val() == 0 || $.isNumeric($("#donation_amount").val()) == false)) {
				$("#donation_amount_error").html('<label for="donation_amount" generated="true" class="error">Please select or enter valid donation amount.</label>')
				return false;
			}
		if($("#donation_amount").val()!='')
		{
			$("#amount").val($("#donation_amount").val());
		}        
        if($("#step1").is(':visible')==true ) {
	            $("#step1").hide();
				$("#step2").show();
			}
		else {
				showAjxLoader();
				$("#confirmMessage").hide();
				$("#creditcard_confirm").hide();
				document.donation_form.submit();
                //return false;
			}	
        }
        ,
        rules:{
            first_name:{
                required: true,
		        minlength: 1, 
		        maxlength: 50, 
		        lettersandapostropheonly: true
            },
            last_name:{
                required: true,
		        minlength: 1, 
		        maxlength: 20, 
		        lettersandapostropheonly: true
            },
            email:{
                required:false,
                email:true,
                maxlength: 100
            }   
        },
        messages:{
            first_name:{
                required:"Please enter First or Given Name."
            },
            last_name:{
                required:"Please enter Last Name or Family Name."
            },
            email:{
                email:"Please enter Valid Email Id."
            }
        }
    });
    
$("#btnSubmit_old").click(function(){ 
			if($("#amount").val() == '' && ($("#donation_amount").val() < 0 || $.trim($("#donation_amount").val()) == '' || $("#donation_amount").val() == '0' || $.isNumeric($("#donation_amount").val()) == false)) {
				$("#donation_amount_error").html('<label for="donation_amount" generated="true" class="error">Please select or enter valid donation amount.</label>')
				return false;
			}

		if($("#donation_amount").val()!='')
		{
			$("#amount").val($("#donation_amount").val());
			//document.donation_form.submit();
		}
		
	});	
	
$("#show_cred_card_old").click(function(){
		$("#swapcard").hide();
		$("#step3").show();
		$("#creditcard_div").show();
		$("#payment_mode").val('credit_card');	
});	
/*	
$("#benifit_1").click(function(){
		$("#step2").hide();
		$("#benifit_1_content").show();
		
});
$("#benifit_2").click(function(){
		$("#step2").hide();
		$("#benifit_2_content").show();
		
});
$("#back1").click(function(){
		$("#step2").show();
		$("#benifit_1_content").hide();
		
	});	
	*/
$(".credit_card_error").click(function(){
		$("#card_details").val('');
		$("#step2").hide();
		$("#credit_card_error").hide();
		$("#swapcard").show();
		$("#card_details").focus();
		$("#step3").hide();
		$("#creditcard_div").show();

		$("#payment_mode").val('credit_card');
		});	
		
/*$("#back2").click(function(){
		$("#step2").show();
		$("#benifit_2_content").hide();
		
});		*/

$("#cash_confirm2").click(function(){
		$("#step2").hide();
		$("#step3").show();
		$("#creditcard_div").show();
		$("#cash_div").hide();
		$("#payment_mode").val('credit_card');
	});		
	$("#tab1").click(function(){
			
		$("#step1").show();
		$("#step2").hide();
		$("#step3").hide();
		$("#swapcard").hide();
	});
$("#tab2").click(function(){	
		$("#step1").hide();
		$("#step2").show();
		$("#step3").hide();

	});	
	
$("#submitbutton").click(function(){	
		$("#user_name_id1").html($("#first_name").val()+' '+$("#last_name").val());
		$("#user_name_id2").html($("#first_name").val()+' '+$("#last_name").val());
		$("#user_name_id3").html($("#first_name").val()+' '+$("#last_name").val());

	});
	
$("#amount_1,#amount_2,#amount_3,#amount_4").click(function(){	
		$("#amount_1,#amount_2,#amount_3,#amount_4").parent().removeClass('active');
		$("#otherId").parent().removeClass('active');
		$(this).parent().addClass('active');
		$("#amount").val(this.value);
		$("#donation_amount").val('');
		$("#donation_amount_div").hide();
	});		

$("#go_back").click(function(){
		$("#step1").show();
		$("#step2").hide();
	});
	
$("#otherId").click(function(){
		$("#amount").val('');
		$(this).parent().addClass('active');
		$("#donation_amount_div").show();
		$("#amount_1,#amount_2,#amount_3,#amount_4").parent().removeClass('active');
	});	
$("#backSwap").click(function(){
		$("#swapcard").hide();
		$("#step2").show();
	});	

$("#resetbutton").click(function(){
		$("#first_name").val('');
		$("#last_name").val('');
		$("#email").val('');

	});	
		
$("#card_details").on('change input',function(){ 
		setTimeout("changeFocus()","1000");	
	});			
		
});

function changeFocus(){
	if($.trim($("#card_details").val())!='') {
		var str = $.trim($("#card_details").val()).split(';');
		
		if(str[0].length>=50) {
			$("#swapcard").hide();
			$("#step3").show();
			$("#creditcard_div").show();
			$("#payment_mode").val('credit_card');	
		}
		
		else {
			$("#card_details").val('');
			$("#zipcode").val('');
			$("#card_details").focus();
			$("#swapcardErrorMessage").show();
		}
	}
	return false;
}

function showAddressBlock(){

		$('#showAddressBlockId').show();
		$("#donotId").hide();
		$("#emailDivId").hide();
		$("#email").val('');
		$('#country').val('228');
		

}
function submitFrm(){
	$("#donation_form").submit();
}

function showBenifit(id){
		$("#step2").hide();
		$("#benifit_content_"+id).show();
}
function backMembershipBenifit(id) {
		$("#step2").show();
		$("#benifit_content_"+id).hide();
}
