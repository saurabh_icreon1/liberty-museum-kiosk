$(function(){
 
$('input').customInput();

    //showAjxLoader();
    $.ajax({type: "GET",dataType: "json", data: {}, url: "/edit-kiosk-module-details/" + id, success: function(response) {			
            if (!checkUserAuthenticationAjx(response))
                return false;
			if (response.kioskModules.kiosk_module_data.length == 0)
                return false;	
				
			   var j=0;
			  
			   $.each(response.kioskModules.kiosk_module_data, function(i, data) {
			  if(data.is_available == 1 || data.is_available == '1'){
			  if(data.kiosk_module_id == 1 || data.kiosk_module_id == 2 ){ reservationModule = true;}
			   var rowData = '<div class="row"><div class="col1"> <label>' + data.kiosk_module_name + '</label><span class="radio-label left m-t-5"><div class="custom-radio"><input type="radio" onclick="changeStatus(this)" class="e3" name="modules['+data.kiosk_module_mapping_id+'_'+ data.kiosk_module_id + ']" value="1" id="available_'+j+'" data-modid="'+data.kiosk_module_id+'"><label for="available_'+j+'" class="checked" id="label_available_'+j+'">Available</label></div><div class="custom-radio"><input type="radio" onclick="changeStatus(this)" name="modules['+data.kiosk_module_mapping_id+'_' + data.kiosk_module_id + ']" value="0" class="e3" id="notavailable_'+j+'" data-modid="'+data.kiosk_module_id+'"><label for="notavailable_'+j+'" id="label_notavailable_'+j+'">Not Available</label></div></span></div></div>';
			   }else{
					var textlabel = "available";
					var rowData = '<div class="row"><div class="col1"> <label>' + data.kiosk_module_name + '</label><span class="radio-label left m-t-5"><div class="custom-radio"><input onclick="changeStatus(this)" type="radio" id="available_'+j+'" class="e3" name="modules['+data.kiosk_module_mapping_id+'_' + data.kiosk_module_id + ']" value="1" data-modid="'+data.kiosk_module_id+'"><label for="available_'+j+'" id="label_available_'+j+'">Available</label></div><div class="custom-radio"><input type="radio" onclick="changeStatus(this)" name="modules['+data.kiosk_module_mapping_id+'_' + data.kiosk_module_id + ']" value="0" id="notavailable_'+j+'" class="e3" data-modid="'+data.kiosk_module_id+'"><label for="notavailable_'+j+'" id="label_notavailable_'+j+'" class="checked" >Not Available</label></div></span></div></div>';
			   }
			  $("#module_elements").append(rowData);
			  
			  if(response.kioskModules.kiosk_data.is_status =="" || response.kioskModules.kiosk_data.is_status == 0)
			  { $('#module_elements :input').attr('disabled', true);$("#submitbutton").remove()}
				 //$.each(response.kioskModules.kiosk_module_child_info, function(k, childdata) {
				 //if(data.kiosk_module_id == childdata.kiosk_module_id){
						//alert("called for "+data.kiosk_module_id);
						$("#module_elements").append(createModuleConfigElement(data.kiosk_module_id,response.kioskModules.kiosk_module_child_info,response.modulesConfig));
					//}
				 
				 //});
			    
				j++;
		   });  hideAjxLoader(); $('#cancel_button_id,#cancelbutton,#submitbutton').css('display','block');
           
        }});

    var LOWER=/[a-z]/,DIGIT=/[0-9]/,DIGITS=/[0-9].*[0-9]/,SPECIAL=/[^a-zA-Z0-9]/,SAME=/^(.)\1+$/;
    function rating(rate,message){
        return{
            rate:rate,
            messageKey:message
        }
    }
	
		 
	function createModuleConfigElement(kiosk_module_id,kioskModulesSettingElement,modulesConfig){ 
  //console.log("called "+ kiosk_module_id);

	var html = '';
	var element= '';
	var settingNameArray = new Array();
	var settingNameValArray = new Array();
	var resultVal = "";
	var str = '';
	var countr = 0;
	var labelCntr = 0;
		$.each(kioskModulesSettingElement, function(l, childElement) {
			 
					if(kiosk_module_id == childElement.kiosk_module_id){
						if(childElement.setting_display_type != ""){//console.log(v.setting_title_name);
								switch(childElement.setting_display_type) {
									case 1:
									case '1':
										//console.log(settingNameArray);
										settingNameArray = childElement.setting_name.split(",");
										settingNameValArray = childElement.setting_value.split(",");
										element = '<div class="row"><div class="col1"><label class="m-l-50">'+childElement.setting_title_name+'</label>';
										for(var j=0;j<settingNameArray.length;j++){
										  var checkedStatus = "";
										  if(childElement.kiosk_module_setting_value == settingNameValArray[j]){
										  checkedStatus = "checked";
										  }
											element +='<span class="radio-label left m-t-5"><div class="custom-radio"><input onclick="changeStatusChild(this)"  type="radio" class="e3" name="module_settings['+childElement.kiosk_setting_id+']" id="'+settingNameArray[j]+'_'+countr+'_'+kiosk_module_id+'" value="'+settingNameValArray[j]+'" '+checkedStatus+'  data-modid="'+labelCntr+'"><label for="'+settingNameArray[j]+'_'+countr+'_'+kiosk_module_id+'" class="'+checkedStatus+' label_'+labelCntr+'">'+settingNameArray[j]+'</label></div> </span>';
										   countr++;
										}
										element += '</div></div>';
										html +=element;
										element = "";
										settingNameArray ="";
										settingNameValArray="";
										break;
									case 2:
									case '2':
								
										element = '<div class="row"><div class="col1"><label>'+childElement.setting_title_name+'</label><input type="text" name="module_settings['+childElement.kiosk_setting_id+']" class="timeclass" value="'+childElement.kiosk_module_setting_value+'" id="module_settings_'+childElement.kiosk_module_setting_id+'"></div></div>';
										html +=element;
										
										countr++;
										break;
									case 3:
									case '3':
										element = '<div class="row"><div class="col1"><input type="checkbox" name="module_settings['+childElement.kiosk_setting_id+']" value="'+childElement.setting_value+'">'+childElement.setting_title_name+'</div></div>';
										html +=element;
										
										countr++;
										break;
									
									default:
										element = '';
										html +=element;
										//html.concat(element);
								}
						}
					
					}
					labelCntr++;
							
			 });
			 //console.log(html1+html2+html3);
			 //html = html1+html2+html3
			 return html;
		 }

$.validator.classRuleSettings.password={
    password:true
};

    $.validator.addMethod("kioskIp",function(value,element){
        return this.optional(element)|| /\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b/.test(value)
        },
    VALID_KIOSK_IP);
 var clicked = 0;
 var vclicked = 0;
 var errorFlag = false;
 var elementName = "";
 var nameArr = new Array();
 var nameValArr = [];
 var nameValObjArr = [];
$("#edit_kiosk").submit(function(){
//alert($("input:radio[name='modules[374_1]']").val());
  $($(".e3")).each(function(i){
        if($(this).is("[type=radio]")){
		elementName = $(this).attr('name');
		nameArr = elementName.split("_");
		modudleId = parseInt(nameArr[1].slice(0,-1));
		
		if(modudleId == 1 || modudleId == 2 || modudleId == 3 || modudleId ==4){
		 nameValArr[modudleId] = parseInt($("[name='"+elementName+"']:checked").val());
		 nameValObjArr[modudleId] = $("[name='"+elementName+"']:checked").attr('id');
		 console.log(elementName+"=>"+$("[name='"+elementName+"']:checked").attr('id'));
		}
    }
 });
 if(nameValArr[1] == 0 && nameValArr[2] == 0 ){
	if(nameValArr[3] == 1){
		$('<label for="'+nameValObjArr[3]+'" generated="true" class="custom_error" style="display:block">Please first enable Login or Registration Module.</label>' ).insertAfter("#"+nameValObjArr[3]);
		return false;
	}
 }
 /*console.log(nameValArr);
  console.log(nameValObjArr);
 console.log(nameValArr[1]);
 return false;*/
 //timeFormatRe = /^\d{1,2}:\d{2}([ap]m)?$/;
 timeFormatRe = /^(?:1[0-2]|0[0-9]):[0-5][0-9]:[0-5][0-9]$/;
 
 var timeForSearch= $('#module_settings_3').val();
 var timeBetweenUsers= $('#module_settings_4').val();
 
 if(!timeFormatRe.test(timeForSearch) && nameValArr[4] == 1){
	errorFlag = true;
	clicked++;
	if(clicked == 1){
	$('<label for="module_settings_3" generated="true" class="custom_error" style="display:block">Please enter Valid Time.</label>' ).insertAfter('#module_settings_3');
	}

 }else{
	if(clicked >= 1){
	  errorFlag = false;
	  $("#module_settings_3").next().remove();
	  }
 }
 if(!timeFormatRe.test(timeBetweenUsers) && nameValArr[4] == 1){
	errorFlag = true;
	vclicked++;
	if(vclicked == 1){
	$('<label for="module_settings_4" generated="true" class="custom_error" style="display:block">Please enter Valid Time.</label>' ).insertAfter('#module_settings_4');
	}
	 
 }else{
	
	if(vclicked >= 1){
		errorFlag = false;
		$("#module_settings_4").next().remove();
	  }
 }
 if(errorFlag == true){
  return false;
 }
 
	var dataStr=$("#edit_kiosk").serialize();
            showAjxLoader();
            $.ajax({
                type:"POST",
                url:
                "/edit-kiosk-module",
                data:dataStr,
                success:function(responseData){
                    hideAjxLoader();
                    if(!checkUserAuthenticationAjx(
                        responseData))return false;
                    var jsonObj=jQuery.parseJSON(responseData);
                    if(jsonObj.status=="success")window.location.href=
                        "/kiosk-module-management";else $.each(jsonObj.message,function(i,msg){
                        $("#"+i).after('<label class="error" style="display:block;">'+
                            msg+"</label>")
                        })
                    }
                    });
});

$("#edit_kiosk").validate({

    submitHandler:function(){
	 
        },
highlight:function(label){
    $(".detail-section").css("display","block")
    },
ignore:"",
rules:{
        kiosk_name
        :{
            required:true,
            minlength:1,
            maxlength:50
        },
        
},
messages:{
    kiosk_name:{
        required:EMPTY_KIOSK_NAME
    },
    
}
});

$("#report_to").autocomplete({
    afterAdd:true,
    selectFirst:
    true,
    autoFocus:true,
    source:function(request,response){
        $.ajax({
            url:"/get-crm-contacts",
            method:"POST",
            dataType:"json",
            data
            :{
                crm_full_name:$("#report_to").val()
                },
            success:function(jsonResult){
                if(!checkUserAuthenticationAjx(jsonResult))
                    return false;
                var resultset=[];
                $.each(jsonResult,function(){
                    var id=this.crm_user_id,value=this.crm_full_name;
                    resultset.
                    push({
                        id:this.crm_user_id,
                        value:this.crm_full_name
                        })
                    });
                response(resultset)
                }
            })
    },
open:function(){
    $(".ui-menu").width(300)
    }
,
change:function(event,ui){
    if(ui.item){
        $("#report_to").val(ui.item.value);
        $("#report_to_id").val(ui.item.id)
        }else{
        $(
            "#report_to").val("");
        $("#report_to_id").val("")
        }
        $(".ui-autocomplete").jScrollPane()
    },
focus:function(event,ui){
    $(
        "#report_to_id").val(ui.item.id);
    return false
    },
select:function(event,ui){
    $("#report_to").val(ui.item.value);
    $(
        "#report_to_id").val(ui.item.id);
    return false
    }
});
});
function deleteKiosk(kiosk_id){
    
    $('.delete_contact').colorbox({
        width:"550px",
        height:"200px",
        inline: true
    });
    $("#no_delete").click(function(){
        $.colorbox.close();
        $('#yes_delete').unbind('click');
        return false;
    });
    
    $("#yes_delete").click(function(){
        $('#yes_delete').unbind('click');
        $.ajax
        ({
            type: 'POST',
            data: {
                kioks_id:kiosk_id
            },
            url: '/delete-kiosk',
            success: function(response)
            {
                if(!checkUserAuthenticationAjx(response)){return false;}
                var jsonObj =  jQuery.parseJSON(response); 
                if(jsonObj.status == 'success')
                {
                    $("#success_message").html(jsonObj.message);
                    window.location.href="/kiosk-module-management";;
                }
            }
        });
    });
}
function cancelKiosk(){
    
    $('.cancel_kiosk').colorbox({
        width:"550px",
        height:"200px",
        inline: true
    });
    $("#no_cancel").click(function(){
        $.colorbox.close();
        $('#yes_delete').unbind('click');
        return false;
    });
    
    $("#yes_cancel").click(function(){
        $('#yes_cancel').unbind('click');
         window.location.href="/kiosk-module-management";
    });
}

