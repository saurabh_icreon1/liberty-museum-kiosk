$(function(){
$('html').click(function (){ 
	$("#card_details").focus();
});
		/* used to check the slot is available or not. */
		

	jQuery.validator.addMethod("lettersandapostropheonly", function(value, element) {
	    return this.
	    optional(element) || /^[a-z '\-]+$/i.test(value)
	}, 'Please enter Valid Name.');

       
    // ziocode form validation    
    
    $("#purchase_session_zipcode").validate({
        submitHandler:function(){
			var dataStr=$("#purchase_session_zipcode").serialize();
			showAjxLoader();
				$.ajax({
                    type:"POST",
                    url:"/check-avaliable-kiosk",
                    data:{},
                    success:function(responseData){
                        var jsonObj=jQuery.parseJSON(responseData);
                        if(jsonObj.status=="error")
                        {   
                            $("#timeSlotError").show();
                            $("#creditcard_confirm").hide();
                            hideAjxLoader();
                        }
                        else{
							$.ajax({
			                    type:"POST",
			                    url:"/purchase-session",
			                    data:dataStr,
			                    success:function(responseData){
			                        var jsonObj=jQuery.parseJSON(responseData);
			                        if(jsonObj.status=="success")
			                        {   
			                            hideAjxLoader();
												var contactId = jsonObj.contact_id;
			                                    $.ajax({
								                    type:"POST",
								                    url:"/add-reservation-contact",
								                    data:{contact_id:jsonObj.contact_id,transaction_id:jsonObj.transaction_id, authorize_transaction_id:jsonObj.authorize_transaction_id,payment_mode:jsonObj.payment_mode, amount:jsonObj.amount, first_name:jsonObj.first_name, last_name:jsonObj.last_name},
								                    success:function(responseData){
								                    
								                        
								                        var jsonObj2=jQuery.parseJSON(responseData);
								                        if(jsonObj2.status=="success")
								                        {   
								                            window.location.href=SITE_URL+"/purchase-session-confirmation/"+jsonObj2.queue_id;
								                            
								                        }
								                        else{
								                                $("#confirmMessage").show();
								                                $("#confirmMessage_cash").show();
									                            $("#confirmMessage").html(jsonObj2.message);
									                            $("#confirmMessage_cash").html(jsonObj2.message);
									                            $("#creditcard_confirm").hide();
								                            }
								                        }
								                });
			                        }
			                        else{
			                                window.location.href="/pay-purchase-session/payment_error";
			                            }
			                        }
		                        });                                          
                            }
                        }
                });			
			}	
        ,
        rules:{
           zipcode:{
                required: true,
	            maxlength:12,
	            minlength:4  
            }, 
        },
        messages:{
	        zipcode:{
                required :"Please enter Billing Zip or Postal Code."
            }
        }
    });     
    
    $("#country").change(function(){
        if($("#country").val() != 228) {
	            $("#other_state").show();
	            $("#state").val(' ').select2(); 
                $("#state_div").hide();
                $("#state").hide();
        }
        else {
	            $("#other_state").hide();
	            $("#state_div").show();
                $("#state").show();        
        }    
    });
$("#credit_card").click(function(){
		$("#step2").hide();
		//$("#swapcard").show();
		$("#card_details").focus();
		$("#creditcard_div").hide();
		$("#payment_mode").val('credit_card');
	});	
$("#show_cred_card_old").click(function(){
	//if($.trim($("#card_details").val())!='') {
		$("#step2").hide();
		$("#swapcard").hide();
		$("#cash_div").hide();
		$("#step3").show();
		$("#creditcard_div").show();
		$("#payment_mode").val('credit_card');
	/*}	
	else {
			$("#swpaCardError").show();
			$("#card_details").focus();
			return false;
	}*/
});
$("#cash,.cash_btn").click(function(){
		$("#step2").hide();
		$("#step3").show();
		$("#cash_div").show();
		$("#creditcard_div").hide();
		$("#payment_mode").val('cash');
		$("#credit_card_error").hide();

	});	
$("#cash_confirm2, .credit_card_error").click(function(){
		$("#card_details").val('');
		$("#step2").hide();
		$("#credit_card_error").hide();
		//$("#swapcard").show();
		$("#card_details").focus();
		$("#step3").hide();
		$("#creditcard_div").show();
		$("#cash_div").hide();
		$("#payment_mode").val('credit_card');
	});		
$("#tab1").click(function(){	
		$("#step1").show();
		$("#step2").hide();
		$("#step3").hide();
	});
$("#tab2,#cash_go_back,#credit_card_go_back").click(function(){	
		$("#step1").hide();
		$("#step2").show();
		$("#step3").hide();
		$("#swapcard").hide();
	});	
$("#card_details").on('change input',function(){
	setTimeout("changeFocus()","1000");	
});	
});