$(function(){
        $("#login_frm").validate({
        submitHandler:function(){
			var dataStr=$("#login_frm").serialize();
			$("#error_message").html('&nbsp;');
			
			if(validateLoginForm() == false){
				$("#error_message").show();
				$("#error_message").html('Please enter Confirmation Code or Email Id.');
				return false;
			}
			showAjxLoader();

			$.ajax({
                    type:"POST",
                    url:"/kiosk-login",
                    data:dataStr,
                    success:function(responseData){
                        hideAjxLoader();
                        var jsonObj=jQuery.parseJSON(responseData);
                        if(jsonObj.status=="success")
                        {   
                            window.location.href="/kiosk-search";
                        }
                        else{
                                $("#error_message").show();
	                            $("#error_message").html(jsonObj.message)
                            }
                        }
                });
			
        }
        ,
        rules:{
            confirmation_code:{
                required:true
            },
            email_id:{
                required:true
            },
        },
        messages:{
            confirmation_code:{
                required:"Please enter Confirmation Code."
            },
            email_id:{
                required:"Please enter Email or Last / Family Name.",
            },
        }
    });
    
    $("#email_id").keypress(function(){
		$("#error_message").hide();
	});	
    
});

function validateLoginForm(){
	if($.trim($("#confirmation_code").val())=='' && $.trim($("#email_id").val())=='')
		return false
	else
		return true;			
}