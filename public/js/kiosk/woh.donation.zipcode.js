$(function(){
$('html').click(function (){ 
	$("#card_details").focus();
});
	jQuery.validator.addMethod("lettersandapostropheonly", function(value, element) {
	    return this.
	    optional(element) || /^[a-z '\-]+$/i.test(value)
	}, 'Please enter Valid Name.');
    // ziocode form validation    
        $("#pos-woh_zipcode").validate({
        submitHandler:function(){
        if($("#step1").is(':visible')==true ) {
	            $("#step1").hide();
				$("#step2").show();
			}
		else {
			var dataStr=$("#pos-woh_zipcode").serialize();
			showAjxLoader();
			$.ajax({
                    type:"POST",
                    url:"/woh-donation",
                    data:dataStr,
                    success:function(responseData){
                        var jsonObj=jQuery.parseJSON(responseData);
                        if(jsonObj.status=="success")
                        {   
                            window.location.href=SITE_URL+"/woh-donation-confirmation/"+jsonObj.transaction_id+"/"+jsonObj.confirmation_code+"/"+jsonObj.user_name;
                        }
                        else{
                                window.location.href="/pay-woh-donation/payment_error";
                            }
                        }
                });
			}	
        }
        ,
        rules:{
           zipcode:{
                required: true,
	            maxlength:12,
	            minlength:4  
            }, 
        },
        messages:{
	        zipcode:{
                required :"Please enter Billing Zip or Postal Code."
            }
        }
    });  


});