$(function(){
	jQuery.validator.addMethod("lettersandapostropheonly", function(value, element) {
	    return this.
	    optional(element) || /^[a-z '\-]+$/i.test(value)
	}, 'Please enter Valid Name.');

        $("#individualFrm").validate({
        submitHandler:function(){
			var dataStr=$("#individualFrm").serialize();
			showAjxLoader();
			$.ajax({
                    type:"POST",
                    url:"/woh-visitation-certificate",
                    data:dataStr,
                    success:function(responseData){
                        hideAjxLoader();
                        var jsonObj=jQuery.parseJSON(responseData);
                        if(jsonObj.status=="success")
                        {   
                            window.location.href="/woh-visitation-confirmation/"+jsonObj.emailFlag+"/"+jsonObj.confirmation_code+"/"+jsonObj.username+"/"+jsonObj.password;
                        }
                        else{
	                            
                            }
                        }
                });
			
        }
        ,
        rules:{
            first_name:{
                required: true,
		        minlength: 1, 
		        maxlength: 50, 
		        lettersandapostropheonly: true
            },
            last_name:{
                required: true,
		        minlength: 1, 
		        maxlength: 20, 
		        lettersandapostropheonly: true
            },
            email:{
                required:false,
                email:true,
                maxlength: 100
            },
            country:{
                required:true,
            },
        },
        messages:{
            first_name:{
                required:"Please enter First or Given Name."
            },
            last_name:{
                required:"Please enter Last Name or Family Name."
            },
            email:{
                email:"Please enter Valid Email Address."
            },
            country:{
                required:"Please select Country of Residence."
            },
        }
    });
	
       $("#familyFrm").validate({
        submitHandler:function(){
			var dataStr=$("#familyFrm").serialize();
			showAjxLoader();
			$.ajax({
                    type:"POST",
                    url:"/woh-visitation-certificate",
                    data:dataStr,
                    success:function(responseData){
                        hideAjxLoader();
                        var jsonObj=jQuery.parseJSON(responseData);
                        if(jsonObj.status=="success")
                        {   
                            window.location.href="/woh-visitation-confirmation/"+jsonObj.emailFlag+"/"+jsonObj.confirmation_code+"/"+jsonObj.username+"/"+jsonObj.password;
                        }
                        else{
	                            
                            }
                        }
                });
			
        }
        ,
        rules:{
            family_name:{
                required: true,
		        minlength: 1, 
		        maxlength: 20, 
		        lettersandapostropheonly: true
            },
            country:{
                required:true,
            },
             email:{
                required:false,
                email:true
            },
       },
        messages:{
            family_name:{
                required:"Please enter Family Name."
            },
            country:{
                required:"Please select Country of Residence."
            },
            email:{
                email:"Please enter Valid Email Address."
            },
        },
        
    });	
	
$("#tab11").click(function(){	
		$("#step1").show();
		$("#step2").hide();
		$("#tab1").addClass("active");
		$("#tab22").removeClass("active");		
	});
$("#tab2").click(function(){	
		$("#step1").hide();
		$("#step2").show(); 
		$("#tab1").removeClass("active");
		$("#tab22").addClass("active");
		
		$("#country").select2();
		/*$("#tab22").addClass("active");*/
		/*$("#country").attr("id","newId");
		setTimeout('changeCss()',"1000");*/
	});	
});
function changeCss() {
		//$("#country").attr("id","newId").select2();
		$("#country").select2();$("#tab22").addClass("active");
		
//	$("#country")
}
function showTerms(){
	$("#terms").show();
	$("#step1").hide();
	$("#active_tab").val('individual');
}
function showTerms2(){
	$("#terms").show();
	$("#step2").hide();
	$("#active_tab").val('family');
}
function hideTerms(){
	if($("#active_tab").val() == "individual")
	{
		$("#step1").show();
		$("#terms").hide();
	}
	else {
		$("#step2").show();
		$("#terms").hide();	
	}
}