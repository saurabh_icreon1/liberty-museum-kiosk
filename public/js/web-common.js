function fbShare(linkStr, picStr, nameStr, capStr, descStr, fBAppId) {
    FB.init({appId: fBAppId, status: true, cookie: false});
    FB.ui(
            {method: "feed", link: linkStr, picture: picStr, name: nameStr, caption: capStr, description: descStr}, function(response) {
        if (
                response && response.post_id) {
        } else {
        }
    })
}
function authenticateUserBeforeAjax() {
    $.ajax({type: "POST", url: "/authenticate-user"
        , dataType: "html", cache: true, async: false, success: function(responseData) {
            var jsonObj = jQuery.parseJSON(responseData);
            if (
                    jsonObj.status == "success") {
                if (jsonObj.encryptPath != undefined && jsonObj.encryptPath != "") {
                    checkUserLogin(jsonObj.
                            encryptPath);
                    return jsonObj.status
                }
                return jsonObj.status
            } else
                return true
        }})
}
function showAjxLoader() {
    var scrollheight = $
            (document).height(), scrollwidth = $(document).width(), windowheight = $(window).height(), windowwidth = $(window).width();
    $(
            "#ajx_loader").css({height: scrollheight, width: scrollwidth, display: "block"});
    $("#ajx_loader_image").css({top: windowheight
                /2,left:windowwidth/2, position: "absolute"})
}
$(document).ready(function() {
    $(window).scroll(function() {
        if ($(
                "#ajx_loader_image").css("display") == "block") {
            var windowheight = $(window).height(), scrolltop = $(document).scrollTop(),
                    windowwidth = $(window).width();
            $("#ajx_loader_image").css({top: scrolltop + windowheight / 2, left: windowwidth / 2, position:
                        "absolute"})
        }
    })
});
function hideAjxLoader() {
    $("#ajx_loader").hide()
}
$(document).ready(function() {
    var s = $("#contentArea");
    if (s.position() != undefined) {
        var pos = s.position();
        $(window).scroll(function() {
            var windowpos = $(window).scrollTop();
            if (
                    windowpos >= pos.top)
                s.addClass("stick");
            else
                s.removeClass("stick")
        })
    }
});
function checkUserLogin(url, objValue, ref_url) {
    var auth_url = "/authenticate-user";
    $.ajax({type: "POST", url: auth_url, dataType: "html", async: false, success: function(
                responseData) {
            var jsonObj = jQuery.parseJSON(responseData);
            if (jsonObj.status == "redirect_to_crm")
                window.location.href = "/crm";
            else if (jsonObj.status == "success")
                $.colorbox({width: "920px", height:
                            "545px", href: "/login/" + url + "--showPopup", iframe: true});
            else if (jsonObj.status == "admin")
                window.location.href = "/dashboard"
                        ;
            else if (ref_url != "")
                if (ref_url == "/view-woh-products")
                    $.colorbox({width: "900px", height: "500px", href: ref_url + "/" + objValue, iframe: true});
                else if (ref_url == "/add-to-cart")
                    parent.addtocart(objValue);
                else if (ref_url == "/add-to-wishlist")
                    parent.addtowishlist(objValue);
                else if (ref_url == "/save-search-fof")
                    parent.document.save_fof.submit();
                else if (ref_url =="/oral-history-text")
                    parent.getOralHistoryFile("text", objValue);
                else if (ref_url == "/oral-history-audio")
                    parent.getOralHistoryFile("audio", objValue);
                else
                    window.location.href = ref_url;
            else if (jsonObj.status == "error")
                window.location.href = ref_url
        }})
}
function showAddToCartConfirmation() {
    $.colorbox({width: "700px", height: "285px", inline: true, href:
                "#addedToCart"});
    setTimeout("showCartProducts();", "400")
}
function showAddToWishlistConfirmation() {
    $.colorbox({href:
                "#addedToWishlist", width: "700px", height: "285px", inline: true})
}
function showCartProducts() {
    $.ajax({type: "POST", url:
                "/cart-details", async: false, success: function(responseData) {
			parent.$("#header_card").show();
                    parent.$("#cart-details-div").html(responseData)
                }})
}
function disableCheckoutButton(buttonId) {
    $("#" + buttonId).css("color", "gray");
    $("#" + buttonId).css("cursor", "none");
    $("#" + buttonId).attr("disabled", "disabled");
}
function regisrationConfirm(obj){
	$("#email_id").html('');
	$("#email_id").html(obj.email_id);
	$.colorbox({width:"730px",height:"920px",inline:true,href:".visitation_record_thank"});
    $(".visitation_record_thank").show();
    $("#cboxClose").on("click",function(){$(".visitation_record_thank").hide()});
	$("#close_thanks").click(function(){
        $('#close_thanks').unbind('click');
         window.location.href="/logout";
    });
	$("#passenger_search").click(function(){
		window.location.href="/kiosk-fee";
         
    });
}
(function($) {
    $.fn.extend({
        limiter: function(limit, elem) {
            $(this).on("keyup focus", function() {
                setCount(this, elem);
            });
            function setCount(src, elem) {
                if (src != undefined) {
                    var chars = src.value.length;
                    if (chars >= limit) {
                        src.value = src.value.substr(0, limit);
                        chars = limit;
                    }
                    elem.html(chars);
                }
            }
            setCount($(this)[0], elem);
        }
    });
})(jQuery);