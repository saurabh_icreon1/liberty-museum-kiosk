$(function(){
    $("#save").click(function(){
        $("#user_id").val($("#contact_name_id").val());
        $("#activity_type").val($("#type").val())
        });
    $.validator.addMethod("firstnamealpha",function(value,element){
        return this.optional(element)||/^[0-9a-zA-Z]+$/.test(value)
        },AGE_VALIDATION);
    $.validator.addMethod("gendernamealpha",function(value,element){
        return this.optional(element)||/^[MFU]+$/.test(value)
        },GENDER_VALIDATION);
    $.validator.addMethod("maritalstatusalpha",function(value,element){
        return this.optional(element)||
        /^[SMUDW]+$/.test(value)
        },MARITAL_VALIDATION);
    $("#annotation").validate({
        submitHandler:function(){
            showAjxLoader();
            $.ajax({
                type:"POST",
                data:$("#annotation").serialize(),
                url:"/crm-annotation-correction-information/"+record_id+"/"+act,
                success:function(response){
                    hideAjxLoader();
                    if(!checkUserAuthenticationAjx(response))return false;
                    var jsonObj=jQuery.parseJSON(response);
                    if(jsonObj.status=="success")location.href="/crm-annotation-correction";else $.each(jsonObj.message,function(i,msg){
                        $("#"+i).after('<label class="error" style="display:block;">'+
                            msg+"</label>")
                        })
                    }
                    })
        },
    rules:{
        contact_name:{
            required:true
        },
        arrival_age:{
            required:false,
            maxlength:2,
            firstnamealpha:true
        },
        gender:{
            required:false,
            gendernamealpha:true
        },
        marital_status:{
            required:false,
            maritalstatusalpha:true
        },
        terms:{
            required:true
        }
    },
    messages:{
        contact_name:{
            required:CONTACT_NAME_VALIDATION
        },
        arrival_age:{
            maxlength:AGE_VALIDATION
        },
        gender:{
            required:GENDER_VALIDATION
        },
        marital_status:{
            required:MARITAL_VALIDATION
        },
        terms:{
            required:ACKNOWLEDGE_VALIDATION
        }
    }
});
$("#birth_date").datepicker({
    changeMonth:true,
    changeYear:true
});
$("#death_date").datepicker({
    changeMonth:true,
    changeYear:true
});
$("#arrival_date").datepicker({
    changeMonth:true,
    changeYear:true
});
$("#date_of_marriage").datepicker({
    changeMonth:true,
    changeYear:true
});
$("#ship").autocomplete({
    afterAdd:true,
    selectFirst:true,
    autoFocus:true,
    source:function(request,response){
        $.ajax({
            url:"/get-ship-list",
            method:"POST",
            dataType:"json",
            data:{
                ship:$("#ship").val()
                },
            success:function(jsonResult){
                var resultset=[];
                $.each(jsonResult,function(){
                    var value=this.ship_name;
                    resultset.push({
                        value:this.ship_name
                        })
                    });
                response(resultset)
                }
            })
    },
open:function(){
    $(".ui-menu").width(350)
    },
change:function(event,ui){
    if(ui.item)$("#ship").val(ui.item.value);else $("#ship").val("")
        },
focus:function(event,ui){
    return false
    },
select:function(event,ui){
    $("#ship").val(ui.item.value);
    return false
    }
});
$("#contact_name").autocomplete({
    afterAdd:true,
    selectFirst:true,
    autoFocus:true,
    source:function(request,response){
        $.ajax({
            url:"/get-contacts",
            method:"POST",
            dataType:"json",
            data:{
                user_email:$("#contact_name").val()
                },
            success:function(jsonResult){
                if(!checkUserAuthenticationAjx(jsonResult))return false;
                var resultset=[];
                $.each(jsonResult,function(){
                    var id=this.user_id;
                    var value=this.full_name;
                    resultset.push({
                        id:this.user_id,
                        value:this.full_name
                        })
                    });
                response(resultset)
                }
            })
    },
open:function(){
    $(".ui-menu").width(350)
    },
change:function(event,ui){
    if(ui.item){
        $("#contact_name").val(ui.item.value);
        $("#contact_name_id").val(ui.item.id)
        }else{
        $("#contact_name").val("");
        $("#contact_name_id").val("")
        }
    },
focus:function(event,ui){
    $("#contact_name_id").val(ui.item.id);
    return false
    },
select:function(event,ui){
    $("#contact_name").val(ui.item.value);
    $("#contact_name_id").val(ui.item.id);
    checkForAddedAnnotation(ui.item.id,record_id_orignal);
    return false
    }
});
$("#type").change(function(){
    if($("#type").val()=="2"){
        $("#annotation_form").hide();
        $("#annotation_head").hide();
        for(var k=1;k<=11;k++)$("#recordApprovalDiv"+k).show();
        $("#annotation_msg").hide();
        $("#correction_msg").show()
        }else{
        $("#annotation_form").show();
        $("#annotation_head").show();
        for(var k=1;k<=11;k++)$("#recordApprovalDiv"+
            k).hide();
        $("#annotation_msg").show();
        $("#correction_msg").hide()
        }
    })
});
function checkForAddedAnnotation(userId,annotation_correction_id){
    showAjxLoader();
    if(annotation_correction_id!="")annotation_correction_id="/"+annotation_correction_id;
    $("#duplicate_annotation_msg").hide();
    $("#duplicate_annotation_msg").html("");
    $.ajax({
        url:"/crm-annotation-duplicate-check/"+userId+"/"+$("#passenger_id").val()+"/"+$('#type').val()+annotation_correction_id,
        method:"GET",
        data:{},
        success:function(response){
            hideAjxLoader();
            var jsonObj=jQuery.parseJSON(response);
            if(jsonObj.status=="duplicate"){
                $("#duplicate_annotation_msg").show();
                if($('#type').val() == 1)
                    $("#duplicate_annotation_msg").html('<label class="error">An Annotation for passenger ID '+$("#passenger_id").val()+" is already added by "+$("#contact_name").val()+".</label>");
                if($('#type').val() == 2)
                    $("#duplicate_annotation_msg").html('<label class="error">A Correction for passenger ID '+$("#passenger_id").val()+" is already added by "+$("#contact_name").val()+".</label>");
                $("#contact_name_id").val("");
                $("#contact_name").val("")
                }
            }
    })
};
