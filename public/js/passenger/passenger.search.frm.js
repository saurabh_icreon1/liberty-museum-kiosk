$(document).ready(function(){
    $(".slideshow").cycle({
        fx:"scrollHorz",
        next:".right_arrow",
        prev:".left_arrow",
        speed:400,
        timeout:0,
        pager:"#nav"
    });
    var minBirthYear="0";
    var maxBirthYear="1974";
    var minCurrentAge="40";
    var maxCurrentAge="230";
    var minArrivalAge="0";
    var maxArrivalAge="140";
    var minYearOfArrival="0";
    var maxYearOfArrival="1984";
    var minMonthOfArrival="1";
    var maxMonthOfArrival="12";
    var minDayOfArrival="1";
    var maxDayOfArrival="31";
    var birth_year_from,birth_year_to,current_age_from,current_age_to,
    arrival_age_from,arrival_age_to,year_of_arrival_from,year_of_arrival_to,month_of_arrival_from,month_of_arrival_to,day_of_arrival_from,day_of_arrival_to;
    if($("#birth_year_from").val()!=""){
        birth_year_from=$("#birth_year_from").val();
        birth_year_to=$("#birth_year_to").val()
        }else{
        birth_year_from=minBirthYear;
        birth_year_to=maxBirthYear
        }
        if($("#current_age_from").val()!=""){
        current_age_from=$("#current_age_from").val();
        current_age_to=$("#current_age_to").val()
        }else{
        current_age_from=minCurrentAge;
        current_age_to=
        maxCurrentAge
        }
        if($("#arrival_age_from").val()!=""){
        arrival_age_from=$("#arrival_age_from").val();
        arrival_age_to=$("#arrival_age_to").val()
        }else{
        arrival_age_from=minArrivalAge;
        arrival_age_to=maxArrivalAge
        }
        if($("#year_of_arrival_from").val()!=""){
        year_of_arrival_from=$("#year_of_arrival_from").val();
        year_of_arrival_to=$("#year_of_arrival_to").val()
        }else{
        year_of_arrival_from=minYearOfArrival;
        year_of_arrival_to=maxYearOfArrival
        }
        if($("#month_of_arrival_from").val()!=""){
        month_of_arrival_from=$("#month_of_arrival_from").val();
        month_of_arrival_to=$("#month_of_arrival_to").val()
        }else{
        month_of_arrival_from=minMonthOfArrival;
        month_of_arrival_to=maxMonthOfArrival
        }
        if($("#day_of_arrival_from").val()!=""){
        day_of_arrival_from=$("#day_of_arrival_from").val();
        day_of_arrival_to=$("#day_of_arrival_to").val()
        }else{
        day_of_arrival_from=minDayOfArrival;
        day_of_arrival_to=maxDayOfArrival
        }
        $("#birth_year").noUiSlider({
        range:[minBirthYear,maxBirthYear],
        start:[birth_year_from,birth_year_to],
        handles:2,
        connect:true,
        step:1,
        serialization:{
            to:[$("#birth_year_from"),
            $("#birth_year_to")],
            resolution:1
        }
    });
$("#current_age").noUiSlider({
    range:[minCurrentAge,maxCurrentAge],
    start:[current_age_from,current_age_to],
    handles:2,
    connect:true,
    step:1,
    serialization:{
        to:[$("#current_age_from"),$("#current_age_to")],
        resolution:1
    }
});
$("#arrival_age").noUiSlider({
    range:[minArrivalAge,maxArrivalAge],
    start:[arrival_age_from,arrival_age_to],
    handles:2,
    connect:true,
    step:1,
    serialization:{
        to:[$("#arrival_age_from"),$("#arrival_age_to")],
        resolution:1
    }
});
$("#year_of_arrival").noUiSlider({
    range:[minYearOfArrival,
    maxYearOfArrival],
    start:[year_of_arrival_from,year_of_arrival_to],
    handles:2,
    connect:true,
    step:1,
    serialization:{
        to:[$("#year_of_arrival_from"),$("#year_of_arrival_to")],
        resolution:1
    }
});
$("#month_of_arrival").noUiSlider({
    range:[minMonthOfArrival,maxMonthOfArrival],
    start:[month_of_arrival_from,month_of_arrival_to],
    handles:2,
    connect:true,
    step:1,
    serialization:{
        to:[$("#month_of_arrival_from"),$("#month_of_arrival_to")],
        resolution:1
    }
});
$("#day_of_arrival").noUiSlider({
    range:[minDayOfArrival,maxDayOfArrival],
    start:[day_of_arrival_from,day_of_arrival_to],
    handles:2,
    connect:true,
    step:1,
    serialization:{
        to:[$("#day_of_arrival_from"),$("#day_of_arrival_to")],
        resolution:1
    }
});
$("#refresh").click(function(){
    if($("#step_1").is(":visible")==true){
        $("#search_type1").prop("checked",false);
        $("#search_type2").prop("checked",false);
        $("#search_type3").prop("checked",false);
        $("#search_type4").prop("checked",false);
        $("#search_type5").prop("checked",false);
        $("#search_type6").prop("checked",false);    
    }
    else if($("#step_2").is(":visible")==true){
        $("#male").prop("checked",false);
        $("#female").prop("checked",false);    
        $("#male_label > span").removeClass("active");
        $("#female_label > span").removeClass("active");        
    }
    else if($("#step_3").is(":visible")==true){
        $("#single").prop("checked",false);
        $("#married").prop("checked",false);
        $("#widowed").prop("checked",false);
        $("#divorced").prop("checked",false);  
        $("#single_label > span").removeClass("active");
        $("#married_label > span").removeClass("active");
        $("#widowed_label > span").removeClass("active");
        $("#divorced_label > span").removeClass("active")
          
    }
    else if($("#step_4").is(":visible")==true){
        $("#birth_year").noUiSlider({
            range:[minBirthYear,maxBirthYear],
            start:[minBirthYear,maxBirthYear]
            },true);
        $("#current_age").noUiSlider({
            range:[minCurrentAge,maxCurrentAge],
            start:[minCurrentAge,maxCurrentAge]
            },true);
        $("#arrival_age").noUiSlider({
            range:[minArrivalAge,
            maxArrivalAge],
            start:[minArrivalAge,maxArrivalAge]
            },true);    
            
        $("#birth_year_from").val(minBirthYear);
        $("#birth_year_to").val(maxBirthYear);
        $("#current_age_from").val(minCurrentAge);
        $("#current_age_to").val(maxCurrentAge);
        $("#arrival_age_from").val(minArrivalAge);
        $("#arrival_age_to").val(maxArrivalAge);
            
    }
    else if($("#step_5").is(":visible")==true){
        $("#year_of_arrival").noUiSlider({
            range:[minYearOfArrival,maxYearOfArrival],
            start:[minYearOfArrival,maxYearOfArrival]
            },true);
        $("#month_of_arrival").noUiSlider({
            range:[minMonthOfArrival,maxMonthOfArrival],
            start:[minMonthOfArrival,maxMonthOfArrival]
            },true);
        $("#day_of_arrival").noUiSlider({
            range:[minDayOfArrival,maxDayOfArrival],
            start:[minDayOfArrival,maxDayOfArrival]
            },true);    
        $("#year_of_arrival_from").val(minYearOfArrival);
        $("#year_of_arrival_to").val(maxYearOfArrival);
        $("#month_of_arrival_from").val(minMonthOfArrival);
        $("#month_of_arrival_to").val(maxMonthOfArrival);
        $("#day_of_arrival_from").val(minDayOfArrival);
        $("#day_of_arrival_to").val(maxDayOfArrival)
            
    }
    else if($("#step_6").is(":visible")==true){
        $('#step_6 input[type="text"]').val("");
        $("#step_6 select").val("").select2();    
    }
    else if($("#step_7").is(":visible")==true){
        var checkboxes=document.getElementsByName("ethnicity[]");
        for(var k=0;k<checkboxes.length;k++)checkboxes[k].checked=false;
        $("#ethnicity_search_id").html("");
        $("#ethnicity_search").val("")    
    
    }
    });
if($("#search_div").is(":visible")==true)$("#search_div").hide();
$("#narrowyoursearch").click(function(){
    if($("#search_div").is(":visible")==true){
        $("#search_div").fadeOut(500);
        $("#narrowyoursearch").removeClass("active")
            
        }else{
        $("#search_div").fadeIn(500);
        $("#narrowyoursearch").addClass("active")
        }
    });
$(".right_arrow, .left_arrow, #nav").click(function(){
	setTimeout("scrollPan()","100");
});    
$("#hide_details_match,#cancel_details_match").click(function(){
    $("#narrowyoursearch").click()
    });
$(".right_arrow, .left_arrow, #nav").click(function(){
    if($("#step_3").is(":visible")==true)$(".scroll-pane").jScrollPane()
        });
$("#update_details_search_result,#update_match_result").click(function(){
    $("#searchButton").click()
    });
$(".match-box").click(function(){
    $(".match-box").removeClass("active");
    $(this).addClass("active");
    var $i=$(this).index();
    $(".passenger-search-output .search-results-box").fadeOut(0);
    $(".passenger-search-output .search-results-box:eq("+$i+")").fadeIn(0)
    });
$(".psr-filter").click(function(){});
$("#hide_close_matches,#cancel_button").click(function(){
    var className=$("#filter_area").attr("class");
    if(className=="passenger-inner-bg narrow-dropdown narrowfilter-right")$(".psr-filter").click();else showFilters()
        });
        
$("#search_type5_label").click(function(){
    var className=$("#search_type5_label > span").attr("class");
    if(className=="active")$("#search_type5_label > span").removeClass("active");else $("#male_label > span").addClass("active")
        });        
        
        
$("#male_label").click(function(){
    var className=$("#male_label > span").attr("class");
    if(className=="active")$("#male_label > span").removeClass("active");else $("#male_label > span").addClass("active")
        });
        
        
        
        
$("#female_label").click(function(){
    var className=$("#female_label > span").attr("class");
    if(className=="active")$("#female_label > span").removeClass("active");else $("#female_label > span").addClass("active")
        });
$("#single_label").click(function(){
    var className=$("#single_label > span").attr("class");
    if(className=="active")$("#single_label > span").removeClass("active");else $("#single_label > span").addClass("active")
        });
$("#married_label").click(function(){
    var className=$("#married_label > span").attr("class");
    if(className=="active")$("#married_label > span").removeClass("active");else $("#married_label > span").addClass("active")
        });
$("#widowed_label").click(function(){
    var className=$("#widowed_label > span").attr("class");
    if(className=="active")$("#widowed_label > span").removeClass("active");else $("#widowed_label > span").addClass("active")
        });
$("#divorced_label").click(function(){
    var className=$("#divorced_label > span").attr("class");
    if(className=="active")$("#divorced_label > span").removeClass("active");
    else $("#divorced_label > span").addClass("active")
        });
$("#narrowyoursearch").click(function(){
    var visibleSlide=0;
    if($("#step_1").is(":visible")==true)visibleSlide=0;
    else if($("#step_2").is(":visible")==true)visibleSlide=1;
    else if($("#step_3").is(":visible")==true)visibleSlide=2;
    else if($("#step_4").is(":visible")==true)visibleSlide=3;
    else if($("#step_5").is(":visible")==true)visibleSlide=4;
    else if($("#step_6").is(":visible")==true)visibleSlide=5;
    else if($("#step_7").is(":visible")==true)visibleSlide=6;
    
    setTimeout("$('.slideshow').cycle({timeout:0,startingSlide:"+visibleSlide+"});","100")


    });
$("#filter_2").click(function(){
    var className=$("#filter_area").attr("class");
    if(className=="passenger-inner-bg narrow-dropdown narrowfilter-left"&&
        $("#passenger-filter").is(":visible")==true){
        $("#passenger-filter").fadeOut(200);
        setTimeout("showHideFilter2();","200");
        $("#passenger-filter").fadeIn(1E3);
        $(".psr-filter").addClass("black")
        }else{
        if($("#passenger-filter").is(":visible")==true){
            $("#passenger-filter").fadeOut(400);
            $(".psr-filter").removeClass("black")
            }else{
            $(".psr-filter").addClass("black");
            $("#passenger-filter").fadeIn(600)
            }
            showHideFilter2()
        }
        if($("#search_div").is(":visible")==true){
        $("#search_div").hide();
        $("#narrowyoursearch").removeClass("active")
        }
    })
    
    $("#clearForm").click(function(){
        /*
         call clearSearchPassengerSession action of passenger controller to clear passenger search session data.
         **/
        showAjxLoader();
        $.ajax({
            async: "false",
            type: "POST",
            data: {
            },
            url: "/clear-search-passenger-session",
            success: function(response) {
                hideAjxLoader();

            }
        })
    
        $("#search_type1").prop("checked",false);
        $("#search_type2").prop("checked",false);
        $("#search_type3").prop("checked",false);
        $("#search_type4").prop("checked",false);
        $("#search_type5").prop("checked",false);
        $("#search_type6").prop("checked",false);

        $("#male").prop("checked",false);
        $("#female").prop("checked",false);
        $("#single").prop("checked",false);
        $("#married").prop("checked",false);
        $("#widowed").prop("checked",false);
        $("#divorced").prop("checked",false);
        $("#male_label > span").removeClass("active");
        $("#female_label > span").removeClass("active");
        $("#single_label > span").removeClass("active");
        $("#married_label > span").removeClass("active");
        $("#widowed_label > span").removeClass("active");
        $("#divorced_label > span").removeClass("active")
        
		$("#birth_year").noUiSlider({
            range:[minBirthYear,maxBirthYear],
            start:[minBirthYear,maxBirthYear]
            },true);
        $("#current_age").noUiSlider({
            range:[minCurrentAge,maxCurrentAge],
            start:[minCurrentAge,maxCurrentAge]
            },true);
        $("#arrival_age").noUiSlider({
            range:[minArrivalAge,
            maxArrivalAge],
            start:[minArrivalAge,maxArrivalAge]
            },true);
        $("#year_of_arrival").noUiSlider({
            range:[minYearOfArrival,maxYearOfArrival],
            start:[minYearOfArrival,maxYearOfArrival]
            },true);
        $("#month_of_arrival").noUiSlider({
            range:[minMonthOfArrival,maxMonthOfArrival],
            start:[minMonthOfArrival,maxMonthOfArrival]
            },true);
        $("#day_of_arrival").noUiSlider({
            range:[minDayOfArrival,maxDayOfArrival],
            start:[minDayOfArrival,maxDayOfArrival]
            },true);
        
        $("#last_name").val('');
        $("#initital_name").val('');
        $("#passenger_id").val('');
        
        
        $("#birth_year_from").val(minBirthYear);
        $("#birth_year_to").val(maxBirthYear);
        $("#current_age_from").val(minCurrentAge);
        $("#current_age_to").val(maxCurrentAge);
        $("#arrival_age_from").val(minArrivalAge);
        $("#arrival_age_to").val(maxArrivalAge);
        $("#year_of_arrival_from").val(minYearOfArrival);
        $("#year_of_arrival_to").val(maxYearOfArrival);
        $("#month_of_arrival_from").val(minMonthOfArrival);
        $("#month_of_arrival_to").val(maxMonthOfArrival);
        $("#day_of_arrival_from").val(minDayOfArrival);
        $("#day_of_arrival_to").val(maxDayOfArrival)
        $('#step_3 input[type="text"]').val("");
//      $("#step_3 select").val("").select2();
        var checkboxes=document.getElementsByName("ethnicity[]");
        for(var k=0;k<checkboxes.length;k++)checkboxes[k].checked=false;
        $("#ethnicity_search_id").html("");
        $("#ethnicity_search").val("")      
	});    
    
    
});
function showFilters(){
    var className=$("#filter_area").attr("class");
    if(className=="passenger-inner-bg narrow-dropdown narrowfilter-right"&&$("#passenger-filter").is(":visible")==true){
        $("#passenger-filter").fadeOut(200);
        setTimeout("showHideFilter();","200");
        $("#passenger-filter").fadeIn(1E3);
        $("#filter_1").addClass("Active")
        }else{
        if($("#passenger-filter").is(":visible")==true){
            $("#passenger-filter").fadeOut(400);
            $("#filter_1").removeClass("Active")
            }else{
            $("#passenger-filter").fadeIn(600);
            $("#filter_1").addClass("Active")
            }
            showHideFilter()
        }
        if($("#search_div").is(":visible")==
        true){
        $("#search_div").hide();
        $("#narrowyoursearch").removeClass("active")
        }
    }
function showHideFilter(){
    $("#filter_area").removeClass("narrowfilter-right");
    $(".psr-filter").removeClass("black");
    $("#filter_area").addClass("narrowfilter-left");
    $(".passenger-filter").css("top","197px");
    $(".passenger-filter").css("left","-427px")
    }
function showHideFilter2(){
    $("#filter_area").removeClass("narrowfilter-left");
    $("#filter_area").addClass("narrowfilter-right");
    $(".passenger-filter").css("top","0px");
    $(".passenger-filter").css("left","0px");
    $("#filter_1").removeClass("Active")
    };
function clearForm() {

		//alert("test")
  
}
function scrollPan(){
    $(".scroll-pane").jScrollPane()
    }