$(document).delegate(".frameQtyWohNum","input",function(){
    if(/\D/g.test(this.value))this.value=this.value.replace(/\D/g,"")
        });
$(document).delegate(".frameQtyWohNum","blur",function(){
    if($.trim(this.value)=="")this.value="1"
        });
$(function(){
    $("#continueWithoutFrame1,#continueWithoutFrame2").click(function(){
        parent.showAddToCartConfirmation();
        parent.$("#cboxClose").click()
        });
    $("#submit").click(function(){
        var minAmount=parseInt($("#minAmount").val());
        var amount=$("#amount").val();
        var membershipId=$("#membershipId").val();
        Number(amount);
        if(amount<minAmount||!Number(amount)){
            $("#error_message").html("");
            $("#error_message").html("Enter an amount greater than or equal to $ "+minAmount);
            return false
            }else{
            parent.$.colorbox.close();
            var urlstr="membership_id="+membershipId+"&user_id="+userId+"&minimum_donation_amount="+amount+"&product_mapping_id=5";
            $.ajax({
                type:"POST",
                url:"/add-cart-membership-front",
                data:urlstr,
                async:false,
                beforeSend:function(){
                    showAjxLoader()
                    },
                success:function(responseData){
                    hideAjxLoader();
                    var jsonObj=jQuery.parseJSON(responseData);
                    if(jsonObj.status=="success")parent.location.href="/cart"
                        }
                    })
        }
    })
});
function addtocartmembership(membership_id,minimum_donation_amount,product_mapping_id,user_id){
    if(user_id!="")user_id=user_id;else user_id=userId;
    authenticateUserBeforeAjax();
    var urlstr="membership_id="+membership_id+"&user_id="+user_id+"&minimum_donation_amount="+minimum_donation_amount+"&product_mapping_id="+product_mapping_id;
    $.ajax({
        type:"POST",
        url:"/add-cart-membership-front",
        data:urlstr,
        beforeSend:function(){
            showAjxLoader()
            },
        success:function(responseData){
            hideAjxLoader();
            var jsonObj=jQuery.parseJSON(responseData);
            if(jsonObj.status=="success")location.href="/cart"
                }
            })
}
function addtocartpassdocument(passengerid,quantity,producttype,flag){
    var urlstr="passenger_id="+passengerid+"&user_id="+userId+"&"+$("#ship_form").serialize()+"&flag="+flag;
    if(producttype==5)urlstr+="&is_passenger_record=1&passenger_record_qty=1";
    if(producttype==8)urlstr+="&is_ship_image=1&ship_image_qty=1";
    if(producttype==7)urlstr+="&is_manifest=1&passenger_record_qty=1";
    var shipname=$("#ship_form #ship_name").val();
    $.ajax({
        type:"POST",
        url:"/add-cart-passenger-document-front",
        data:urlstr,
        beforeSend:function(){
            showAjxLoader()
            },
        success:function(responseData){
            hideAjxLoader();
            if(responseData=="session_expired")if(!checkFrontUserAuthenticationAjx(responseData))return false;
            var jsonObj=jQuery.parseJSON(responseData);
            if(jsonObj.status=="success")if(jsonObj.hasReletedProducts=="yes"){
                if(shipname!=""){
                    showCartProductsNew();
                    showRelatedProducts(producttype,shipname)
                    }
                }else showAddToCartConfirmation()
                }
            })
}
function showRelatedProducts(product_type,product){
    $.colorbox({
        width:"1200px",
        height:"800px",
        iframe:true,
        overlayClose:false,
        href:"/get-related-products/"+product_type+"/"+product,
        onLoad:function(){ $("#ajx_loader").css({"z-index":"99999999"}); $('#colorbox').css({"display":"none"}); $('#cboxClose').css({"display":"none"}); },
        onComplete:function(){ $("#ajx_loader").css({"z-index":""}); $('#colorbox').css({"display":"block"}); hideAjxLoader(); }
    });
}
function showCartProductsNew(){
    $("#ajx_loader").css({"z-index":"99999999","display":"block"});
    $.ajax({
        type:"POST",
        url: "/cart-details",
        async:true,
        success:function(responseData){
            parent.$("#header_card").show();
            parent.$("#cart-details-div").html(responseData)
          }
        });
}

function addToCartManifestCheck(passengerid,quantity,producttype,flag)
{
    if($('#item_in_cart').html()*1 != 0 && $('#item_in_cart').html()*1 != '')
    {
        if(manifestType=='TwoPage')
        {    
            //if($("#is_additional").val() == 0)
           // if($('#is_first_image').is(':checked') ==  false || $('#is_second_image').is(':checked') ==  false )
            if($('#ship_manifest_images tr').length == 1)
            {
                $.colorbox({
                    width:"600px",
                    href:"#add_to_card_message",
                    height:"450px",
                    inline: true
                });
                $("#cancel_add").click(function(){
                    $.colorbox.close();
                    $('#continue_add').unbind('click');
                    return false;
                });
				    
                $("#continue_add").click(function(){
                    if(document.getElementById("add_image1").checked == true)
                    {
                        $.colorbox.close();
                        $('#continue_add').unbind('click');
                    }
                    else
                    {
                        addtocartpassdocument(passengerid,quantity,producttype,flag);
                        return;
                    }    
                });
             } else
			{
                addtocartpassdocument(passengerid,quantity,producttype,flag);
                return;
            }
           
        }
        else
        {
            addtocartpassdocument(passengerid,quantity,producttype,flag);
            return;
        }
        
    }
    else
    {
        $("#add_cart_error").show();
        $("#add_cart_error").html("Please add at least manifest image.");
        return false;
    }
}
function showDonationPopup(minAmount,membershipID){
    authenticateUserBeforeAjax();
    $.colorbox({
        width:"500px",
        height:"350px",
        iframe:true,
        href:"/member-benefit-entry/"+minAmount+"/"+membershipID
        })
    }
function submitMembership(){
    $("#error_message").html("");
    var donation=$("#amount").val();
    var obj=$("#jsonValue").val();
    var response=jQuery.parseJSON(obj);
    var flag=0;
    $.each(response,function(key,val){
        if(donation>=parseInt(val.minimun_donation_amount)&&flag==0){
            $("#membership_title").html("");
            $("#membership_title").html("You will get "+val.membership_title+" memebership with this amount ");
            $("#membershipId").html(val.membership_id);
            flag=1
            }else if(flag==0)$("#membership_title").html("")
            })
    };
