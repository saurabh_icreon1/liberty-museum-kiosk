$(window).data("ajaxready", true).scroll(function(e) {
    var spy, container = $("#container");
    if ($(window).data("ajaxready") ==
            false)
        return;
    var WindowHeight = $(window).height();
    if ($(window).scrollTop() >= container.outerHeight() - 600) {
        $(window).data(
                "ajaxready", false);
        var page = parseInt($("#page").val());
        if ($("#loadingDiv" + "-" + page))
            $("#loadingDiv" + "-" + page).html(
                    "Loading.....");
        $("#page").val(page + 1);
        $("#load_more" + page).html("");
        $.ajax({type: "POST", cache: true, data: {ship: $(
                        "#ship").serialize(), page: $("#page").val()}, url: "/ship-line-list-more/" + line_id + "/" + line_name, success: function(response)
            {
                $("#loadingDiv" + "-" + page).html("");
                $("#lastDiv").append(response);
                $(window).data("ajaxready", true);
                $("#load_more" + "-" +
                        page).html("")
            }})
    }
});
function loadMore() {
    var page = parseInt($("#page").val());
    if ($("#loadingDiv" + "-" + page))
        $(
                "#loadingDiv" + "-" + page).html("Loading.....");
    $("#page").val(page + 1);
    $("#load_more" + page).html("");
    $.ajax({type: "POST",
        cache: true, data: $("#ship").serialize(), url: "/ship-line-list-more/" + line_id + "/" + line_name, success: function(response) {
            $(
                    "#loadingDiv" + "-" + page).html("");
            $("#lastDiv").append(response);
            $(window).data("ajaxready", true);
            $("#load_more" + "-" + page
                    ).html("")
        }})
}
sortSearchShip("FILENAME");
function sortSearchShip(fieldName) {
    $("#sort_field").val(fieldName);
    $("#page").val(1);
    if ($("#sort_order").val() == "asc" || $("#sort_order").val() == "")
        $("#sort_order").val("desc");
    else
        $("#sort_order").val("asc");
    $.ajax({type: "POST", cache: true, data: $("#ship").serialize(), url: "/ship-line-list-more/" + line_id + "/" + line_name
        , success: function(response) {
            $("#shipListing").html(response)
        }})
}