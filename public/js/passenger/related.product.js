

$(document).delegate('input[id="hide_add_to_card_related_products"]','click',function(){ 
    var js1 = jQuery.noConflict();
    parent.$('#cboxClose').click();
    js1('#cboxClose').click();
    parent.$('#cboxClose').click();
    js1.colorbox.close();
    parent.$.colorbox.close();    
}); 


$(document).delegate("#continueWithoutFrame1",'click',function(){
        $('#cboxClose').css({"display":"block"});
        parent.$('#cboxClose').css({"display":"block"});
        $('#relatedProductsDiv').css({"display":"none"});
        parent.$.colorbox.resize({innerWidth:"650px",innerHeight:"200px",top: "100px"}); 
        $("html").addClass("overflow-hidden");
        $("body").addClass("overflow-hidden");
        $("html, body").animate({scrollTop:$("#colorbox").offset().top},2000);
        $('#addedToCartRelatedProducts').css({"display":"block"});
        setTimeout("showCartProducts();","400"); 
 });
 
 $(document).delegate("#continueWithoutFrame2",'click',function(){
        $('#cboxClose').css({"display":"block"});
        parent.$('#cboxClose').css({"display":"block"});
        $('#relatedProductsDiv').css({"display":"none"});
        parent.$.colorbox.resize({innerWidth:"650px",innerHeight:"200px",top: "100px"}); 
        $("html").addClass("overflow-hidden");
        $("body").addClass("overflow-hidden");
        $("html, body").animate({scrollTop:$("#colorbox").offset().top},2000);
        $('#addedToCartRelatedProducts').css({"display":"block"});
        setTimeout("showCartProducts();","400"); 
 });
 
    
 
    // n - addtocart - start
    function addtocart(product_id) {

        authenticateUserBeforeAjax();        
        var quantity = $("#quantity_"+product_id).val();
        var status = /^\d+$/.test(quantity)
        if(status == false) {
            $("#quantity_"+product_id).addClass('error');
            $("#quantityerror_"+product_id).show();
        }
        else {     
            $.ajax({
                type: "POST",
                url: '/add-to-cart-inventory-front',
                data: {
                    product_id :product_id,
                    num_quantity:quantity
                },
                beforeSend:function(){
                    showAjxLoader();  
                },
                success: function(responseData){
                    hideAjxLoader();
                    var jsonObj =  jQuery.parseJSON(responseData);
                    $('label.error').remove();
                    if(jsonObj.status =='success'){
                        $('label.error').remove();   
                        $('#cboxClose').css({"display":"block"});
                        parent.$('#cboxClose').css({"display":"block"});
                        $('#relatedProductsDiv').css({"display":"none"});
                        parent.$.colorbox.resize({innerWidth:"650px",innerHeight:"200px",top: "100px"}); 
                        $("html").addClass("overflow-hidden");
                        $("body").addClass("overflow-hidden");
                        $("html, body").animate({scrollTop:$("#colorbox").offset().top},2000);
                        $('#addedToCartRelatedProducts').css({"display":"block"});
                        setTimeout("showCartProducts();","400");   
                    } else {  
                        if(jsonObj.product_id != undefined && jsonObj.message != undefined && jsonObj.product_id != "" && jsonObj.message != "") {
                            $('label[for="quantity_'+jsonObj.product_id+'"]').remove();
                           $("#quantity_"+jsonObj.product_id).after('<label class="error mar-b-10" for="quantity_'+jsonObj.product_id+'" style="display:block;">'+jsonObj.message+'</label>');
                        }
                        $.each(jsonObj.message, function (i, msg) {
                            $("#"+i).after('<label class="error" style="display:block;">'+msg+'</label>');
                        });
                    }
                }
             });     
        }
    }
    // n - addtocart - end 
    
    //    
   function displaypopclick(pid){
        $(document).ready(function() {

        $('.fancybox-thumbs'+pid).fancybox({
                prevEffect : 'none',
                nextEffect : 'none',
                closeBtn  : true,
                arrows    : true,
                nextClick : false,
                mouseWheel : true,                                                    
                afterClose:function(){
                    $(".fancybox-thumbs"+pid).hide();
                }
            });    
          });  

        displaypop(pid);
        $('.fancybox-thumbs'+pid).click(); 
        $("a[rel^='lightbox']").slimbox({});
    }
    
    function displaypop(pid)
    {
        $(".fancybox-thumbs"+pid).show();
    }
    $(document).ready(function() {        
        $(document).on('click',".thumbsdisplay",function(){
            var pid = $(this).attr('id');
            $('.fancybox-thumbs'+pid).click(); 
        });
        
        $("a[rel^='lightbox']").slimbox({});
    });   
    
    
    
$(document).ready(function() {
    hideAjxLoader();
});
