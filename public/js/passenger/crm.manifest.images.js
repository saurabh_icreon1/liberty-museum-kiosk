function showPassengerMoreManifestImage(seriesValue,frame,passengerId){
    showAjxLoader();
    if(passengerId!="")passengerId="/"+passengerId;
    location.href="/show-manifest-more-image/"+seriesValue+"/"+frame+passengerId
    }
function showAjxLoader(){
    var scrollheight=$(document).height();
    var scrollwidth=$(document).width();
    var windowheight=$(window).height();
    var windowwidth=$(window).width();
    $("#ajx_loader").css({
        height:scrollheight,
        width:scrollwidth,
        display:"block"
    });
    $("#ajx_loader_image").css({
        top:windowheight/4,
        left:windowwidth/2,
        position:"absolute"
    })
    }
    function hideAjxLoader(){
    $("#ajx_loader").hide()
    }
function showPassengerListing(filename){
    window.open("/manifest-passenger/"+filename,filename,"width=1000,height=700,resizable=true,scrollbars=1")
    }
$(document).ready(function(){
    
    $(".e2").select2();
    $("#frame_no_plus_1,#frame_no_plus_2,#frame_no_plus_3,#frame_no_plus_4,#frame_no_minus_1,#frame_no_minus_2,#frame_no_minus_3,#frame_no_minus_4").click(function(){
        var frame_no=parseInt($("#frame_no").val())+parseInt(this.value);
        if($("#frame_no").val()<1)return false;
        else{
            if(frame_no<=0)frame_no=1;
            if(frame_no>=$("#frame_no_max").val())frame_no=$("#frame_no_max").val();
            $("#frame_no").val(frame_no)
            }
            $("#display").click()
        });
    $("#roll_no_plus_1,#roll_no_minus_1").click(function(){
        showAjxLoader();
        var roll_no=parseInt($("#roll_no").val())+parseInt(this.value);
        if($("#roll_no").val()<1)return false;
        else{
            if(parseInt(this.value)=="-1")if(roll_no<=$("#roll_no_min").val())roll_no=$("#roll_no_min").val();
            if(parseInt(this.value)=="+1")if(roll_no>=$("#roll_no_max").val())roll_no=$("#roll_no_max").val();
            if(dataSourceVal == 2)
                $("#roll_no").val(pad(roll_no,9))
            if(dataSourceVal == 1)
                $("#roll_no").val(pad(roll_no,4))
            
        }
            $("#frame_no").val("1");
        var passengerId="";
        if($("#passengerId").val()!="")passengerId="/"+$("#passengerId").val();
        window.location.href="/show-manifest-more-image/"+$("#series_no").val()+
        "-"+$("#roll_no").val()+"--showfromfile/"+$("#frame_no").val()+passengerId
        });
    $("#display").click(function(){
        showAjxLoader();
        $("#error_msg").html("");
        $.ajax({
            url:"/check-valid-rollnumber",
            method:"POST",
            async:false,
            data:{
                series_no:$("#series_no").val(),
                roll_no:$("#roll_no").val(),
                frame_no:$("#frame_no").val()
                },
            success:function(response){
                var jsonObj=jQuery.parseJSON(response);
                if(jsonObj.min_frame)$("#f_range").html("Frame Range(1 - "+jsonObj.max_frame+")");else $("#f_range").html("Frame Range(0 - 0)");
                if(jsonObj.roll_nbr_count==0||jsonObj.roll_nbr_count==""){
                    $("#error_msg").html("Roll number does not exists");
                    hideAjxLoader();
                    return false
                    }else if($("#frame_no").val()*1>jsonObj.max_frame||$("#frame_no").val()*1<1){
                    $("#error_msg").html("Invalid frame range");
                    hideAjxLoader();
                    return false
                    }else{
                    var passengerId="";
                    if($("#passengerId").val()!="")passengerId="/"+$("#passengerId").val();
                    window.location.href="/show-manifest-more-image/"+$("#series_no").val()+"-"+$("#roll_no").val()+"--showfromfile/"+$("#frame_no").val()+
                    passengerId
                    }
                    hideAjxLoader()
                }
            })
    });
$("#series_no").change(function(){
    var jsonObj=jQuery.parseJSON($("#roll_no_range").val());
    var roll_no_Array=jsonObj[$("#series_no").val()].split("~");
    $("#roll_no_min").val(roll_no_Array[0]);
    $("#roll_no_max").val(roll_no_Array[1]);
    $("#roll_no").val(pad(roll_no_Array[0],4));
    $("#frame_no").val("1");
    var passengerId="";
    showAjxLoader();
    if($("#passengerId").val()!="")passengerId="/"+$("#passengerId").val();
    window.location.href="/show-manifest-more-image/"+$("#series_no").val()+
    "-"+$("#roll_no").val()+"--showfromfile/"+$("#frame_no").val()+passengerId
    })
});
function pad(num,size){
    var s=num+"";
    while(s.length<size)s="0"+s;
    return s
    };
