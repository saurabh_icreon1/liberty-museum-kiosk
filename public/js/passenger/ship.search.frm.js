$(document).ready(function(){$(".slideshow").cycle({fx:"scrollHorz",next:".right_arrow",prev:".left_arrow",speed:400,
timeout:0,pager:"#nav"});var minYearOfArrival="1879",maxYearOfArrival="1960",minMonthOfArrival="1",maxMonthOfArrival=
"12",minDayOfArrival="1",maxDayOfArrival="31",year_of_arrival_from,year_of_arrival_to,month_of_arrival_from,
month_of_arrival_to,day_of_arrival_from,day_of_arrival_to;if($("#year_of_arrival_from").val()!=""){year_of_arrival_from=
$("#year_of_arrival_from").val();year_of_arrival_to=$("#year_of_arrival_to").val()}else{year_of_arrival_from=
minYearOfArrival;year_of_arrival_to=maxYearOfArrival}if($("#month_of_arrival_from").val()!=""){month_of_arrival_from=$(
"#month_of_arrival_from").val();month_of_arrival_to=$("#month_of_arrival_to").val()}else{month_of_arrival_from=
minMonthOfArrival;month_of_arrival_to=maxMonthOfArrival}if($("#day_of_arrival_from").val()!=""){day_of_arrival_from=$(
"#day_of_arrival_from").val();day_of_arrival_to=$("#day_of_arrival_to").val()}else{day_of_arrival_from=minDayOfArrival;
day_of_arrival_to=maxDayOfArrival}$("#year_of_arrival").noUiSlider({range:[minYearOfArrival,maxYearOfArrival],start:[
year_of_arrival_from,year_of_arrival_to],handles:2,connect:true,step:1,serialization:{to:[$("#year_of_arrival_from"),$(
"#year_of_arrival_to")],resolution:1}});$("#month_of_arrival").noUiSlider({range:[minMonthOfArrival,maxMonthOfArrival],
start:[month_of_arrival_from,month_of_arrival_to],handles:2,connect:true,step:1,serialization:{to:[$(
"#month_of_arrival_from"),$("#month_of_arrival_to")],resolution:1}});$("#day_of_arrival").noUiSlider({range:[
minDayOfArrival,maxDayOfArrival],start:[day_of_arrival_from,day_of_arrival_to],handles:2,connect:true,step:1,
serialization:{to:[$("#day_of_arrival_from"),$("#day_of_arrival_to")],resolution:1}});$("#refresh").click(function(){if(
$("#step_1").is(":visible")==true){$('#step_1 input[type="text"]').val("");$("#step_1 select").val(" ").select2();$(
"#year_of_arrival").noUiSlider({range:[minYearOfArrival,maxYearOfArrival],start:[minYearOfArrival,maxYearOfArrival]},
true);$("#month_of_arrival").noUiSlider({range:[minMonthOfArrival,maxMonthOfArrival],start:[minMonthOfArrival,
maxMonthOfArrival]},true);$("#day_of_arrival").noUiSlider({range:[minDayOfArrival,maxDayOfArrival],start:[
minDayOfArrival,maxDayOfArrival]},true);$("#year_of_arrival_from").val(minYearOfArrival);$("#year_of_arrival_to").val(
maxYearOfArrival);$("#month_of_arrival_from").val(minMonthOfArrival);$("#month_of_arrival_to").val(maxMonthOfArrival);$(
"#day_of_arrival_from").val(minDayOfArrival);$("#day_of_arrival_to").val(maxDayOfArrival)}});if($("#search_div").is(
":visible")==true)$("#search_div").hide();$("#narrowshipsearchid").click(function(){if($("#search_div").is(":visible")==
true){$("#search_div").fadeOut(500);$("#narrowshipsearchid").removeClass("active")}else{$("#search_div").fadeIn(500);$(
"#narrowshipsearchid").addClass("active")}});$("#hide_details_match,#cancel_details_match").click(function(){$(
"#narrowshipsearchid").click()});$(".close").click(function(){});$(".match-box").click(function(){$(".match-box").
removeClass("active");$(this).addClass("active");$i=$(this).index();$(".passenger-search-output .search-results-box").
fadeOut(0);$(".passenger-search-output .search-results-box:eq("+$i+")").fadeIn(0)});$(".psr-filter").click(function(){})
;$("#hide_close_matches,#cancel_button").click(function(){var className=$("#filter_area").attr("class");if(className==
"passenger-inner-bg narrow-dropdown narrowfilter-right")$(".psr-filter").click();else showFilters()});$(
"#update_details_search_result,#update_match_result").click(function(){$("#searchButton").click()});$(
"#narrowshipsearchid").click(function(){setTimeout("$('.slideshow').cycle();","100")})})