$(function(){
    $("#arrival_date").datepicker({
        changeMonth:true,
        changeYear:true,
        showOtherMonths:true
    });
    $("#ship_arrival_date").datepicker({
        changeMonth:true,
        changeYear:true,
        showOtherMonths:true
    });
    $("#img_arrival_date").click(function(){
        jQuery("#arrival_date").datepicker("show")
        });
    $("#img_ship_arrival_date").click(function(){
        $("#ship_arrival_date").datepicker("show")
        });
    $.validator.addMethod("firstnamealpha",function(value,element){
        return this.optional(element)||/^[A-Za-z. ]+$/i.test(value)
        },FIRST_NAME_MSG);
    $.validator.addMethod("lastnamealpha",function(value,element){
        return this.optional(element)||/^[A-Za-z. ]+$/i.test(value)
        },LAST_NAME_MSG);
    $.validator.addMethod("nationalityalpha",function(value,element){
        return this.optional(element)||/^[A-Za-z. ]+$/i.test(value)
        },NATIONALITY_MSG);
    $.validator.addMethod("ethnicityalpha",function(value,element){
        return this.optional(element)||/^[A-Za-z. ]+$/i.test(value)
        },ETHNICITY_MSG);
    $.validator.addMethod("agearrivalnumeric",function(value,element){
        return this.optional(element)||
        /^[0-9]+$/i.test(value)
        },AGE_VALIDATION);
    $("#passenger").validate({
        submitHandler:function(){
            showAjxLoader();
            $.ajax({
                type:"POST",
                data:$("#passenger").serialize(),
                url:"/edit-passenger/"+passenger_id,
                success:function(response){
                    hideAjxLoader();
                    if(!checkUserAuthenticationAjx(response))return false;
                    var jsonObj=jQuery.parseJSON(response);
                    if(jsonObj.status=="success")location.href="/crm-passenger";else $.each(jsonObj.message,function(i,msg){
                        $("#"+i).after('<label class="error" style="display:block;">'+msg+
                            "</label>")
                        })
                    }
                    })
        },
    rules:{
        last_name:{
            required:true,
            maxlength:20,
            lastnamealpha:true
        }
    },
    messages:{
        last_name:{
            required:LAST_NAME_MSG,
            maxlength:LAST_NAME_MSG
        }
    }
});
$("#ship").autocomplete({
    afterAdd:true,
    selectFirst:true,
    autoFocus:true,
    source:function(request,response){
        $.ajax({
            url:"/get-ship-list",
            method:"POST",
            dataType:"json",
            data:{
                ship:$("#ship").val()
                },
            success:function(jsonResult){
                var resultset=[];
                $.each(jsonResult,function(){
                    var value=this.ship_name;
                    resultset.push({
                        value:this.ship_name
                        })
                    });
                response(resultset)
                }
            })
    },
open:function(){
    $(".ui-menu").width(350)
    },
change:function(event,ui){
    if(ui.item)$("#ship").val(ui.item.value);else $("#ship").val("")
        },
focus:function(event,ui){
    return false
    },
select:function(event,ui){
    $("#ship").val(ui.item.value);
    showArrivalsDates(ui.item.value);
    return false
    }
});
//showArrivalsDates($("#ship").val())
});
function showArrivalsDates(ship_name){

    $("#ship_arrival_date option").each(function(){
        $(this).remove()
        });
    $.ajax({
        type:"POST",
        data:{
            ship:ship_name
        },
        url:"/get-ship-arrival-dates",
        success:function(jsonResult){
            var jsonObj=jQuery.parseJSON(jsonResult);
            var html="";
            var len=jsonObj.length;
            for(var i=0;i<len;i++)html+='<option value="'+jsonObj[i].date_arrive+'">'+jsonObj[i].date_arrive+"</option>";
            $("#ship_arrival_date").append(html);
            $("#ship_arrival_date").val($("#ship_arrival_date_value").val());
           // $("#ship_arrival_date").select2()
            }
        })
}
;
