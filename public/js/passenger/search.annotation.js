$(function(){
    var grid=jQuery("#list"),emptyMsgDiv=$('<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");
    $.jgrid.
    no_legacy_api=true;
    $.jgrid.useJSON=true;
    $("#list").jqGrid({
        postData:{
            searchString:$("#annotation_list").serialize()
            },
        mtype:"POST",
        url:"/get-crm-annotation-correction",
        datatype:"json",
        sortable:true,
        colNames:[PASSENGER_ID,PASSENGER_NAME,
        CONTACT_NAME,REQUESTED_ON,LAST_UPDATED,TYPE,STATUS,APPROVED_STATUS,APPROVED_BY,ACTIONS],
        colModel:[{
            name:"PassengerID",
            index:"passenger_id"
        },{
            name:"PassengerName",
            index:"AC_tab.first_name"
        },{
            name:"ContactName",
            index:"USER_tab.first_name"
        },

        {
            name:"RequestedOn",
            index:"added_date"
        },{
            name:"updatedOn",
            index:"modified_date"
        },{
            name:"Type",
            index:"activity_type"
        },{
            name:"Status",
            index:"is_active",
             sortable:false
        },{
            name:"ApprovalRequired",
            index:"is_reauthorization"
        },{
            name:"approvedby",
            index:
            "CRM_USER_tab.crm_first_name"
        },{
            name:"Action(s)",
            index:"action",
            sortable:false
        }],
        viewrecords:true,
        sortname:
        "AC_tab.modified_date",
        sortorder:"DESC",
        rowNum:10,
        rowList:[10,20,30],
        pager:"#pcrud",
        viewrecords:true,
        autowidth:true,
        shrinkToFit:true,
        caption:"",
        width:"100%",
        cmTemplate:{
            title:false
        },
        loadComplete:function(){
            hideAjxLoader();
            var count=grid
            .getGridParam(),ts=grid[0];
            if(ts.p.reccount===0){
                grid.hide();
                emptyMsgDiv.show();
                $("#pcrud_right div.ui-paging-info").css
                ("display","none")
                }else{
                grid.show();
                emptyMsgDiv.hide();
                $("#pcrud_right div.ui-paging-info").css("display","block")
                }
            }
    });
emptyMsgDiv.insertAfter(grid.parent());
    emptyMsgDiv.hide();
    $("#list").jqGrid("navGrid","#pcrud",{
    reload:true,
    edit:false,
    add:false,
    search:false,
    del:false
});
$("#list").jqGrid("navButtonAdd","#pcrud",{
    caption:"",
    title:"Export",
    id:"exportExcel"
    ,
    onClickButton:function(){
        exportExcel("list","/get-crm-annotation-correction")
        },
    position:"last"
});
$("#search").click(
    function(){
        showAjxLoader();
        $("#success_message").hide();
        jQuery("#list").jqGrid("setGridParam",{
            postData:{
                searchString:$(
                    "#annotation_list").serialize()
                }
            });
    jQuery("#list").trigger("reloadGrid",[{
        page:1
    }]);
    window.location.hash="#saved_search"
    ;
    hideAjxLoader();
    return false
    });
$("#from_date").datepicker({
    changeMonth:true,
    changeYear:true
});
$("#to_date").datepicker(

{
    changeMonth:true,
    changeYear:true
});
$("#img_from").click(function(){
    jQuery("#from_date").datepicker("show")
    });
$(
    "#img_to").click(function(){
    jQuery("#to_date").datepicker("show")
    });
$("#requested_on").change(function(){
    var date=$(this
        ).val();
    if(date=="1"){
        $("#requested").show();
        $("#from_date").val("");
        $("#to_date").val("")
        }else $("#requested").hide()
        })
;
$(".basic-text").click(function(){
    $("#advance-seach").slideToggle("slow");
    var text=$(".basic-text").text();
    if(text==
        "Advance Search")$(".basic-text").html("<a href='javascript:void()'>Basic Search");
    else if(text=="Basic Search"){
        $(
            ".basic-text").html("<a href='javascript:void()'>Advance Search");
        $("#first_name").attr("disabled",true)
        }
    });
$("#type").
change(function(){
    if($("#type").val()=="corrections"){
        $("#annotation_form").hide();
        $("#annotation_head").hide()
        }else{
        $(
            "#annotation_form").show();
        $("#annotation_head").show()
        }
    });
$("#search_name").keyup(function(){
    $("#search_msg").html("")
    }
);
$("#saved_search").change(function(){
    showAjxLoader();
    $("#search_msg").hide();
    if($("#saved_search").val()!=""){
        if($(
            "#saved_search").val()!=""){
            $('label[for="search_name"]').hide();
            $("#savebutton").val("Update");
            $("#deletebutton").show(
                )
            }
            $.ajax({
            type:"POST",
            data:{
                search_id:$("#saved_search").val()
                },
            url:"/get-crm-search-annotation-saved-param",
            success:
            function(response){
                hideAjxLoader();
                if(!checkUserAuthenticationAjx(response))return false;
                var searchParamArray=response,
                obj=jQuery.parseJSON(searchParamArray);
                document.getElementById("annotation_list").reset();
                $("#passenger_id").val(obj.
                    passenger_id);
                $("#passenger_name").val(obj.passenger_name);
                $("#contact_name").val(obj.contact_name);
                $("#status").val(obj
                    .status).select2();
                $("#requested_on").val(obj.requested_on).select2();
                $("#type").val(obj.type).select2();
                $("#from_date")
                .val(obj.from_date);
                $("#to_date").val(obj.to_date);
                $("#search_name").val(obj.search_title);
                $("#search_id").val($(
                    "#saved_search").val());
                $("#search").click()
                }
            })
    }else{
    $("#status").select2();
    $("#requested_on").select2();
    $("#type").
    select2();
    $("#search_name").val("");
    $("#search_id").val("");
    $("#savebutton").val("Save");
    $("#deletebutton").hide();
    $(
        "#search").click()
    }
});
$("#save_search").click(function(){
    $("#success_message").hide()
    });
$("#save_search").validate({
    submitHandler:function(){
        showAjxLoader();
        $.ajax({
            type:"POST",
            data:{
                search_name:$.trim($("#search_name").val()),
                searchString:$("#annotation_list").serialize(),
                search_id:$("#search_id").val()
                },
            url:"/save-crm-annotation-search",
            success:function(response){
                hideAjxLoader();
                if(!checkUserAuthenticationAjx(response))return false;
                var jsonObj=jQuery.
                parseJSON(response);
                if(jsonObj.status=="success"){
                    $("#search_name").val("");
                    $("#search_msg").removeClass("error");
                    document.getElementById("annotation_list").reset();
                    location.reload()
                    }else $("#search_msg").html(ERROR_SAVING_SEARCH)
                    }
                })
    }
,
rules:{
    search_name:{
        required:true
    }
},
messages:{
    search_name:{
        required:SEARCH_NAME_MSG
    }
}
})
});
function generateSavedSearch(
    ){
    $("#saved_search option").each(function(){
        $(this).remove()
        });
    $.ajax({
        type:"POST",
        data:{},
        url:
        "/get-saved-crm-annotation-search-select",
        success:function(response){
            if(!checkUserAuthenticationAjx(response))
                return false;
            $("#saved_search").append(response)
            }
        })
}
function deleteAnnotation(id){
    $(".delete_annotation").colorbox({
        width:"700px",
        height:"180px",
        inline:true
    });
    $("#no_delete_search").click(function(){
        $.colorbox.close();
        $(
            "#yes_delete_search").unbind("click");
        return false
        });
    $("#yes_delete_search").click(function(){
        $.ajax({
            type:"POST",
            data:{
                id:id
            },
            url:"/delete-annotation-correction",
            success:function(response){
                if(!checkUserAuthenticationAjx(response))
                    return false;
                var jsonObj=jQuery.parseJSON(response);
                if(jsonObj.status=="success")location.reload()
                    }
                })
    })
}
function 
deleteRecord(){
    $(".delete_annotation_search").colorbox({
        width:"700px",
        height:"180px",
        inline:true
    });
    $("#no_delete").click
    (function(){
        $.colorbox.close();
        $("#yes_delete").unbind("click");
        return false
        });
    $("#yes_delete").click(function(){
        $.ajax(

        {
            type:"POST",
            data:{
                search_id:$("#search_id").val()
                },
            url:"/delete-crm-search-annotation",
            success:function(response){
                if(!
                    checkUserAuthenticationAjx(response))return false;
                var jsonObj=jQuery.parseJSON(response);
                if(jsonObj.status=="success")
                    location.reload()
                    }
                })
    })
}