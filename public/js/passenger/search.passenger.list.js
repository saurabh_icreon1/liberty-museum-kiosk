$(function(){var grid=jQuery("#list"),emptyMsgDiv=$(
'<div class="no-record-msz">Sorry, no records are found. Please refine the search criteria.</div>');$.jgrid.
no_legacy_api=true;$.jgrid.useJSON=true;$("#list").jqGrid({postData:{searchString:$("#passenger").serialize()},mtype:
"POST",url:"/get-search-passenger-list",datatype:"json",sortable:true,colNames:["Last Name","FIRST NAME & MIDDLE",
"LAST PLACE OF RESIDENCE","ARRIVED","PASSENGER RECORD","SHIP IMAGE","SHIP MANIFEST"],colModel:[{name:"PRIN_LAST_NAME",
index:"field3",width:"5%"},{name:"PRIN_FIRST_NAME",index:"field2",width:"5%"},{name:"PRIN_PLACE_RESI",index:"field4",
width:"7%"},{name:"DATE_ARRIVE",index:"field19",width:"4%"},{name:"PASSENGER_RECORD",sortable:false,width:"6%"},{name:
"SHIP_IMAGE",sortable:false,width:"4%"},{name:"MANIFEST_IMAGE",sortable:false,width:"4%"}],viewrecords:true,sortname:
"field19",sortorder:"DESC",rowNum:10,rowList:[10,20,30],pager:"#pcrudpassenger",viewrecords:true,autowidth:true,fixed:
true,shrinkToFit:true,caption:"",width:"60%",cmTemplate:{title:false},loadComplete:function(){hideAjxLoader();var count=
grid.getGridParam(),ts=grid[0];if(ts.p.reccount===0){grid.hide();emptyMsgDiv.show();$(
"#pcrudpassenger_right div.ui-paging-info").css("display","none")}else{grid.show();emptyMsgDiv.hide();$(
"#pcrudpassenger_right div.ui-paging-info").css("display","block")}}});emptyMsgDiv.insertAfter(grid.parent());
emptyMsgDiv.hide();$("#list").jqGrid("navGrid","#pcrudpassenger",{reload:true,edit:false,add:false,search:false,del:
false})})