$(document).ready(function(){
    jQuery.validator.addMethod("alphanumeric",function(value,element){
        return this.optional(
            element)||/^[A-Za-z0-9()\'' .-]+$/i.test(value)
        },VALID_SEARCH_INPUT);
    $("#savebutton").click(function(){
        saveSearch()
        });
    $(
        "#close_confirm").click(function(){
        $("#cboxClose").click()
        });
    $("#close_exceed").click(function(){
        $("#cboxClose").click()
    });
    $("#eth_ok").click(function(){
        var val=[];
        $("input:checkbox:checked.ethnicityCls").each(function(i){
            val[i]=$(this).val
            ()
            });
        var str='<div class="scroll-pane ethni-scroll">',str2="";
        for(var t=0;t<val.length;t++){
            str2=str2+val[t]+",";
            str=str
            +"<span id='eth_"+t+"'><strong class='left'>"+val[t]+'</strong> <a href="javascript:void(0)" onclick="removeEth(\''+val[
            t]+"','"+t+"')\">x</a></span> "
            }
            str=str+"</div>";
        $("#ethnicity_search_id").html(str);
        str2=str2.substring(0,str2.length-1
            );
        $("#ethnicity_search").val(str2);
        $(".scroll-pane").jScrollPane()
        });
    $("#show_advance").click(function(){
        if($(
            "#advsearch").is(":hidden")){
            $("#advsearch").show();
            $("#show_advance h3 a").addClass("active")
            }else{
            $("#advsearch").hide
            ();
            $("#show_advance h3 a").removeClass("active")
            }
        });
if($("#advsearch").is(":hidden"))$("#arrival_port").val("");
    $("#passenger").validate({
    submitHandler:function(){
        showAjxLoader();
        $.ajax({
            async:"false",
            type:"POST",
            data:{
                searchString:$(
                    "#passenger").serialize()
                },
            url:"/get-search-passenger",
            success:function(response){ 
                hideAjxLoader();
                $(
                    "#passengerSearchResult").html(response);
                $("#searchTab"+$("#match_type").val()).click();
                if($("#passenger-filter").is(
                    ":visible")==true){
                    $("#passenger-filter").fadeOut(400);
                    $("#filter_1").removeClass("Active");
                    $(".psr-filter").removeClass
                    ("black")
                    }
                    if($("#search_div").is(":visible")==true)$("#hide_details_match").click()
                    }
                })
    },
rules:{
    last_name:{
        required:
        function(){
            return $("#passenger_id").val()==""
            },
        minlength:2,
        alphanumeric:true
    },
    town:{
        required:false,
        alphanumeric:true
    },
    passenger_id:{
        required:false,
        alphanumeric:true
    },
    first_name:{
        required:false,
        alphanumeric:true
    },
    companion_name:{
        required:
        false,
        alphanumeric:true
    }
},
messages:{
    last_name:{
        required:LAST_NAME_ERROR_MSG,
        minlength:CHAR_VALID_LENGTH,
        alphanumeric:
        VALID_SEARCH_INPUT
    },
    town:{
        alphanumeric:VALID_SEARCH_INPUT
    },
    passenger_id:{
        alphanumeric:VALID_SEARCH_INPUT
    },
    first_name:{
        alphanumeric:VALID_SEARCH_INPUT
    },
    companion_name:{
        alphanumeric:VALID_SEARCH_INPUT
    }
}
});
$("#search_passenger").validate({
    rules:{
        last_name:{
            required:function(){
                return $("#passenger_id").val()==""
                },
            minlength:2,
            alphanumeric:true
        },
        town:{
            required
            :false,
            alphanumeric:true
        },
        passenger_id:{
            required:false,
            alphanumeric:true
        },
        first_name:{
            required:false,
            alphanumeric:true
        },
        companion_name:{
            required:false,
            alphanumeric:true
        }
    },
messages:{
    last_name:{
        required:LAST_NAME_ERROR_MSG,
        minlength:
        CHAR_VALID_LENGTH,
        alphanumeric:VALID_SEARCH_INPUT
    },
    town:{
        alphanumeric:VALID_SEARCH_INPUT
    },
    passenger_id:{
        alphanumeric:
        VALID_SEARCH_INPUT
    },
    first_name:{
        alphanumeric:VALID_SEARCH_INPUT
    },
    companion_name:{
        alphanumeric:VALID_SEARCH_INPUT
    }
}
});
$(
    "#ship_name").autocomplete({
    afterAdd:true,
    selectFirst:true,
    autoFocus:true,
    source:function(request,response){
        $.ajax({
            url:
            "/get-ship-list",
            method:"POST",
            dataType:"json",
            data:{
                ship:$("#ship_name").val()
                },
            success:function(jsonResult){
                var 
                resultset=[];
                $.each(jsonResult,function(){
                    var value=this.ship_name;
                    resultset.push({
                        value:this.ship_name
                        })
                    });
                response(
                    resultset)
                }
            })
    },
open:function(){
    $(".ui-menu").width(350)
    },
change:function(event,ui){
    if(ui.item)$("#ship_name").val(ui.
        item.value);else $("#ship_name").val("")
        },
focus:function(event,ui){
    return false
    },
select:function(event,ui){
    $(
        "#ship_name").val(ui.item.value);
    return false
    }
});
$("#port_of_departure").autocomplete({
    afterAdd:true,
    selectFirst:true,
    autoFocus:true,
    source:function(request,response){
        $.ajax({
            url:"/get-departure-port",
            method:"POST",
            dataType:"json",
            data:{
                maxRows:10,
                port_of_departure:$("#port_of_departure").val()
                },
            success:function(jsonResult){
                var resultset=[];
                $.each(
                    jsonResult,function(){
                        var value=this.port_of_departure;
                        resultset.push({
                            value:this.port_of_departure
                            })
                        });
                response(
                    resultset)
                }
            })
    },
open:function(){
    $(".ui-menu").width(350)
    },
change:function(event,ui){
    if(ui.item)$("#port_of_departure").
        val(ui.item.value);else $("#port_of_departure").val("")
        },
focus:function(event,ui){
    return false
    },
select:function(event,ui
    ){
    $("#port_of_departure").val(ui.item.value);
    return false
    }
});
if(searchId!="")getSaveSearch(searchId);
$(
    "#sort_field,#sort_order").change(function(){
    if($.trim($("#sort_field").val())!=""&&$.trim($("#sort_order").val())!="")$
        ("#passenger").submit()
        })
});
function getSavedSearchListing(page_act){
    var action="getList";
    $.ajax({
        type:"POST",
        data:{
            act:
            action,
            page_act:page_act
        },
        url:"/get-save-passenger-search",
        success:function(response){
            $("#SaveSearchTitleList").html(
                response)
            }
        })
}
function getSaveSearch(search_id,pageact){
    showAjxLoader();
    var returnSearchParam="yes";
    if($("#advsearch").is
        (":hidden"))$("#arrival_port").val("");
    $.ajax({
        type:"POST",
        data:{
            search_id:search_id,
            returnSearchParam:returnSearchParam
        },
        url:"/get-search-passenger",
        success:function(response){
            hideAjxLoader();
            var searchParamArray=response,obj2=jQuery.
            parseJSON(searchParamArray);
            $("#advsearch").show();
            $("#initital_name").val(obj2.initital_name);
            $("#last_name").val(obj2.
                last_name);
            document.getElementById("search_type1").checked=false;
            document.getElementById("search_type2").checked=false;
            document.getElementById("search_type3").checked=false;
            document.getElementById("search_type4").checked=false;
            document.
            getElementById("search_type5").checked=false;
            document.getElementById("search_type6").checked=false;
            if(obj2.search_type1
                ==1)document.getElementById("search_type1").checked=true;
            if(obj2.search_type2==1)document.getElementById("search_type2")
                .checked=true;
            if(obj2.search_type3==1)document.getElementById("search_type3").checked=true;
            if(obj2.search_type4==1)
                document.getElementById("search_type4").checked=true;
            if(obj2.search_type5==1)document.getElementById("search_type5").
                checked=true;
            if(obj2.search_type6==1)document.getElementById("search_type6").checked=true;
            $("#town").val(obj2.town);
            $(
                "#ship_name").val(obj2.ship_name);
            $("#port_of_departure").val(obj2.port_of_departure);
            $("#passenger_id").val(obj2.
                passenger_id);
            $("#first_name").val(obj2.first_name);
            $("#companion_name").val(obj2.companion_name);
            $("#arrival_port").val
            (obj2.arrival_port).select2();
            var gender=obj2.gender,minBirthYear="1680",maxBirthYear="1974",minCurrentAge="40",
            maxCurrentAge="230",minArrivalAge="0",maxArrivalAge="140",minYearOfArrival="1879",maxYearOfArrival="1960",
            minMonthOfArrival="1",maxMonthOfArrival="12",minDayOfArrival="1",maxDayOfArrival="31";
            checkboxes=document.
            getElementsByName("gender[]");
            for(var k=0;k<checkboxes.length;k++)checkboxes[k].checked=false;
            $("#male_label > span").
            removeClass("active");
            $("#female_label > span").removeClass("active");
            if(gender){
                checkboxes=document.getElementsByName(
                    "gender[]");
                for(var k=0;k<checkboxes.length;k++)checkboxes[k].checked=false;
                for(var k=0;k<checkboxes.length;k++){
                    var val
                    =eval(checkboxes[k]).value;
                    if(gender.indexOf(val)!=-1){
                        checkboxes[k].checked=true;
                        if(checkboxes[k].value=="M")$(
                            "#male_label > span").addClass("active");
                        if(checkboxes[k].value=="F")$("#female_label > span").addClass("active")
                            }
                        }
                }
            var 
    marital_status=obj2.marital_status;
    checkboxes=document.getElementsByName("marital_status[]");
        for(var k=0;k<checkboxes.
        length;k++)checkboxes[k].checked=false;
    $("#single_label > span").removeClass("active");
        $("#married_label > span").
        removeClass("active");
        $("#widowed_label > span").removeClass("active");
        $("#divorced_label > span").removeClass("active")
        ;
        if(marital_status)for(var k=0;k<checkboxes.length;k++){
            var val=eval(checkboxes[k]).value;
            if(marital_status.indexOf(val)
                !=-1){
                checkboxes[k].checked=true;
                if(checkboxes[k].value=="S")$("#single_label > span").addClass("active");
                if(checkboxes[
                    k].value=="M")$("#married_label > span").addClass("active");
                if(checkboxes[k].value=="D")$("#divorced_label > span").
                    addClass("active");
                if(checkboxes[k].value=="W")$("#widowed_label > span").addClass("active")
                    }
                }
        ethnicity=obj2.ethnicity;
    if(ethnicity){
        checkboxes=document.getElementsByName("ethnicity[]");
        for(var k=0;k<checkboxes.length;k++)checkboxes[k].
            checked=false;
        for(var k=0;k<checkboxes.length;k++){
            var val=eval(checkboxes[k]).value;
            if(ethnicity.indexOf(val)!=-1)
                checkboxes[k].checked=true
                }
            }
        if(obj2.birth_year_from&&obj2.birth_year_from!=""){
    $("#birth_year_from").val(obj2.
        birth_year_from);
    $("#birth_year_to").val(obj2.birth_year_to);
    $("#birth_year").noUiSlider({
        range:[minBirthYear,
        maxBirthYear],
        start:[obj2.birth_year_from,obj2.birth_year_to]
        },true)
    }
    if(obj2.current_age_from&&obj2.current_age_from!=""
    ){
    $("#current_age").noUiSlider({
        range:[minCurrentAge,maxCurrentAge],
        start:[obj2.current_age_from,obj2.current_age_to]
        },
    true);
    $("#current_age_from").val(obj2.current_age_from);
    $("#current_age_to").val(obj2.current_age_to)
    }
    if(obj2.
    arrival_age_from&&obj2.arrival_age_from!=""){
    $("#arrival_age").noUiSlider({
        range:[minArrivalAge,maxArrivalAge],
        start:[
        obj2.arrival_age_from,obj2.arrival_age_to]
        },true);
    $("#arrival_age_from").val(obj2.arrival_age_from);
    $("#arrival_age_to")
    .val(obj2.arrival_age_to)
    }
    if(obj2.year_of_arrival_from&&obj2.year_of_arrival_from!=""){
    $("#year_of_arrival").noUiSlider(

    {
        range:[minYearOfArrival,maxYearOfArrival],
        start:[obj2.year_of_arrival_from,obj2.year_of_arrival_to]
        },true);
    $(
        "#year_of_arrival_from").val(obj2.year_of_arrival_from);
    $("#year_of_arrival_to").val(obj2.year_of_arrival_to)
    }
    if(obj2.
    month_of_arrival_from&&obj2.month_of_arrival_from!=""){
    $("#month_of_arrival").noUiSlider({
        range:[minMonthOfArrival,
        maxMonthOfArrival],
        start:[obj2.month_of_arrival_from,obj2.month_of_arrival_to]
        },true);
    $("#month_of_arrival_from").val(
        obj2.month_of_arrival_from);
    $("#month_of_arrival_to").val(obj2.month_of_arrival_to)
    }
    if(obj2.day_of_arrival_from&&obj2.
    day_of_arrival_from!=""){
    $("#day_of_arrival").noUiSlider({
        range:[minDayOfArrival,maxDayOfArrival],
        start:[obj2.
        day_of_arrival_from,obj2.day_of_arrival_to]
        },true);
    $("#day_of_arrival_from").val(obj2.day_of_arrival_from);
    $(
        "#day_of_arrival_to").val(obj2.day_of_arrival_to)
    }
    ethnicity=obj2.ethnicity;
var checkboxes=document.getElementsByName(
    "ethnicity[]");
for(var k=0;k<checkboxes.length;k++)checkboxes[k].checked=false;
for(var k=0;k<checkboxes.length;k++){
    var 
    val=eval(checkboxes[k]).value;
    if(ethnicity)if(ethnicity.indexOf(val)!=-1)checkboxes[k].checked=true
        }
        if(ethnicity){
    var 
    str="";
    for(var t=0;t<ethnicity.length;t++)str=str+"<span id='eth_"+t+"'><strong class='left'>"+ethnicity[t]+
        '</strong> <a href="javascript:void(0)" onclick="removeEth(\''+ethnicity[t]+"','"+t+"')\">x</a></span> ";
    $(
        "#ethnicity_search_id").html(str)
    }else $("#ethnicity_search_id").html("");
if(pageact=="indexpage")$("#submitbutton").
    click();else $("#passenger").submit()
    }
})
}
function deleteSaveSearch(search_id,page_act){
    $.colorbox({
        width:"600px",
        href:
        "#delete_image",
        height:"260px",
        inline:true
    });
    $("#no_delete").click(function(){
        $.colorbox.close();
        $("#yes_delete").unbind
        ("click");
        return false
        });
    $("#yes_delete").click(function(){
        $("#yes_delete").unbind("click");
        showAjxLoader();
        $.ajax({
            type
            :"POST",
            data:{
                search_id:search_id
            },
            url:"/delete-search-passenger",
            success:function(response){
                var jsonObj=jQuery.
                parseJSON(response);
                if(jsonObj.status=="success"){
                    hideAjxLoader();
                    getSavedSearchListing(page_act);
                    $("#no_delete").click(
                        )
                    }
                }
        })
    })
}
function searchPassenger(){
    var tabIndex;
    if($("#search_type5").is(":checked")==true)tabIndex=5;
    else if($(
        "#search_type1").is(":checked")==true)tabIndex=1;
    else if($("#search_type2").is(":checked")==true)tabIndex=2;
    else if($(
        "#search_type3").is(":checked")==true)tabIndex=3;
    else if($("#search_type4").is(":checked")==true)tabIndex=4;
    else if($(
        "#search_type6").is(":checked")==true)tabIndex=6;else tabIndex=5;
    $("#sort_field").val("");
    $("#sort_order").val("");
    $(
        "#match_type").val(tabIndex);
    $("#passenger").submit()
    }
    function sortSearchPassenger(fieldName){
    $("#sort_field").val(
        fieldName);
    if($("#sort_order").val()=="asc"||$("#sort_order").val()=="")$("#sort_order").val("desc");else $(
        "#sort_order").val("asc");
    $("#passenger").submit()
    }
    function checkLogin(){
    $("#hidden-login-popup").click();
    return false
    }
function saveSearch(){
    var urlval;
    if($("#is_logged_in").val()!="")urlval="/save-search-passenger";else urlval=
        "/passenger-result";
    $("#last_name").removeClass("error");
    $("#search_name").removeClass("error");
    if($.trim($(
        "#search_name").val())!="")
        if($.trim($("#passenger_id").val())!='' || $.trim($("#last_name").val())!="")
        if($.trim($("#passenger_id").val())!='' || $.trim($("#last_name").val()).length>=2)
        {
        showAjxLoader();
        $("#error_msg").html("");
        $("#save_error_msg").html("");
        $.ajax({
            type:"POST",
            data:{
                searchString:$(
                    "#passenger").serialize()
                },
            url:urlval,
            success:function(response){
                authenticateUserBeforeAjax();
                hideAjxLoader();
                var 
                jsonObj=jQuery.parseJSON(response);
                if(jsonObj.status=="success"){
                    $("#search_name").focus();
                    getSavedSearchListing();
                    $.
                    colorbox({
                        width:"600px",
                        href:"#confirm_message",
                        height:"260px",
                        inline:true
                    })
                    }
                    if(jsonObj.status=="exceed"){
                    $(
                        "#search_name").focus();
                    getSavedSearchListing();
                    $.colorbox({
                        width:"600px",
                        href:"#exceed",
                        height:"320px",
                        inline:true
                    })
                    }
                }
        }
    )
    }else return false;
else{
    $("label[for='last_name']").remove();
    $("#error_msg").html(
        '<label for="last_name" generated="true" class="error">'+LAST_NAME_ERROR_MSG+"</label>");
    $("#last_name").addClass(
        "error");
    return false
    }else{
    $("#save_error_msg").html('<label for="search_name" generated="true" class="error">'+
        SEARCH_NAME_ALERT+"</label>");
    $("#search_name").addClass("error");
    return false
    }
}
function searchPassengerList(id){ alert("searchPassengerList");
    var url
    ="/get-search-passenger";
    $.ajax({
        type:"POST",
        async:"false",
        data:{
            searchType:id,
            searchString:$("#passenger").serialize()
            }
        ,
        url:url,
        success:function(response){
            hideAjxLoader();
            checkFrontUserAuthenticationAjx(response);
            if(id==
                "textPassengerListTab"||id=="shipManifestTab"){
                if($("#record_series").val()!=""){
                    $("#recordSeries").show();
                    $(
                        "#recordSeriesValue").html($("#record_series").val());
                    $("#recordRoll").show();
                    $("#recordRollValue").html($(
                        "#record_roll").val())
                    }
                }else{
            $("#recordSeries").hide();
            $("#recordSeriesValue").html("");
            $("#recordRoll").hide();
            $(
                "#recordRollValue").html("")
            }
        }
    })
}
function setCookie(c_name,value,exdays){
    var expires;
    if(exdays){
        var date=new Date();
        date
        .setTime(date.getTime()+exdays*24*60*60*1000);
        expires="; expires="+date.toGMTString()
        }else expires="";
    document.cookie=
    c_name+"="+value+expires+";path=/"
    }
    function showEthnicity(){
    $.colorbox({
        width:"800px",
        href:"#ethnicity_list",
        height:
        "500px",
        inline:true,
        escKey:false,
        overlayClose:false
    });
    setTimeout("scrollPan()","1000");
    $("#eth_ok").click(function(){
        $(
            "#cboxClose").click()
        });
    var val=[];
    $("input:checkbox.ethnicityCls").each(function(i){
        $("input:checkbox.ethnicityCls").
        prop("checked",false)
        });
    if($("#ethnicity_search").val()!=""){
        var ethArray=$("#ethnicity_search").val().split(",");
        $(
            "input:checkbox.ethnicityCls").each(function(i){
            if(ethArray.indexOf($(this).val())!="-1")$(this).prop("checked",true)
                })
        }
}
function scrollPan(){
    $(".scroll-pane").jScrollPane()
    }
    function removeEth(value,t){
    $('input:checkbox[value="'+value+'"]')
    .attr("checked",false);
    $("#eth_"+t).hide();
    var ethArray=$("#ethnicity_search").val().split(","),index=ethArray.indexOf(
        value);
    if(index>-1)ethArray.splice(index,1);
    $("#ethnicity_search").val(ethArray.join(","))
    }
    function showGridView(){
    $(
        "#result_format").val("grid_view");
    $(".grid-view").addClass("active");
    $(".list-view").removeClass("active");
    $(
        "#searchButton").click()
    }
    function showListView(){
    $("#result_format").val("list_view");
    $(".list-view").addClass("active")
    ;
    $(".grid-view").removeClass("active");
    $("#searchButton").click()
    }
    function showPassengerDetailsSummary(id){
    $(
        "#passengerDetailDiv"+id).fadeIn(1000)
    }
    function hidePassengerDetailsSummary(id){
    $("#passengerDetailDiv"+id).fadeOut(600)
    

    
}

    function openOnePageSerach()
    {
       // window.open('/full-passenger-search','mywindow',"width=1300,height=900,resizable=true,scrollbars=1")
    
		$.colorbox({
		        width: "1300px",
		        height: "900px",
		        iframe: true,
		        href: "/full-passenger-search",
		});      
    }
    
    function showNationalitySlider(){
        setTimeout("scrollPan()","1000");
    }