$(function(){
    $("#filename").keypress(function(){
        $("#filename").next().remove()
        });
    $("#bySeriesSearch").show();
    $("#search_passenger").validate({
        submitHandler:function(){
            var submitFlag1=0;
            var submitFlag2=0;
            if($("#search_type_0").is(":checked")==true){
                if($("#roll_list").val()!=""){
                    if($("#roll").next().attr("id")=="roll_error")$("#roll").next().remove();
                    var jsonObj=jQuery.parseJSON($("#roll_list").val());
                    var sizeof=jsonObj.length;
                    if($("#roll").val()>=jsonObj[0].ROLL_NBR*1&&$("#roll").val()<=jsonObj[sizeof-
                        1].ROLL_NBR*1&&$.isNumeric($("#roll").val())||$("#roll").val()=="")submitFlag1=1;
                    else{
                        $("#roll").after('<label id="roll_error" for="roll" generated="true" class="error" style="display:block">Please enter roll number within the range</label>');
                        $("#roll_error").show();
                        submitFlag1=0;
                        return false
                        }
                    }
                if($("#frame_list").val()!=""){
                if($("#frame").next().attr("id")=="series_error")$("#frame").next().remove();
                var jsonObj=jQuery.parseJSON($("#frame_list").val());
                var sizeof=jsonObj.length;
                if($("#frame").val()>=
                    jsonObj[0].FRAME*1&&$("#frame").val()<=jsonObj[sizeof-1].FRAME*1&&$.isNumeric($("#frame").val())||$("#frame").val()=="")submitFlag2=1;
                else{
                    $("#frame").after('<label id="series_error" for="frame" generated="true" style="display:block" class="error">Please enter image number within the range</label>');
                    $("#series_error").show();
                    submitFlag2=0;
                    return false
                    }
                }
        }
    if((submitFlag1==1||submitFlag2==1)&&$("#search_type_0").is(":checked")==true){
        searchPassenger();
        $("#updateSection").show();
        $("#fileNameManifest").show();
        jQuery("#list").jqGrid("setGridParam",{
            postData:{
                searchString:$("#search_passenger").serialize()
                }
            });
    jQuery("#list").trigger("reloadGrid",[{
        page:1
    }]);
    var isChecked=$("#search_type_0").is(":checked");
    if(isChecked==true&&$("#roll").val()!=""){
        $("#sliderDiv").html("Loading...");
        $.ajax({
            type:"POST",
            data:{
                roll:$("#roll").val(),
                series:$("#series").val(),
                frame:$("#frame").val(),
                showSlider:"show"
            },
            url:"/get-roll-frame",
            success:function(response){
                if(!checkUserAuthenticationAjx(response))return false;
                $("#sliderDiv").html(response)
                }
            })
    }
}else if(submitFlag1==
    0&&submitFlag2==0&&($("#search_type_1").is(":checked")==true||$("#search_type_2").is(":checked")==true)){
    searchPassenger();
    $("#updateSection").show();
    $("#fileNameManifest").show();
    jQuery("#list").jqGrid("setGridParam",{
        postData:{
            searchString:$("#search_passenger").serialize()
            }
        });
jQuery("#list").trigger("reloadGrid",[{
    page:1
}]);
var isChecked1=$("#search_type_0").is(":checked");
var isChecked2=$("#search_type_2").is(":checked");
if(isChecked1==true&&$("#roll").val()!=""){
    $("#sliderDiv").html("Loading...");
    $.ajax({
        type:"POST",
        data:{
            roll:$("#roll").val(),
            series:$("#series").val(),
            frame:$("#frame").val(),
            showSlider:"show"
        },
        url:"/get-roll-frame",
        success:function(response){
            if(!checkUserAuthenticationAjx(response))return false;
            $("#sliderDiv").html(response)
            }
        })
}else if(isChecked2==true){
    $("#sliderDiv").html("Loading...");
    $.ajax({
        type:"POST",
        data:{
            ship_name:$("#ship_name").val(),
            date_arrive:$("#arrival_date").val(),
            showSlider:"show"
        },
        url:"/get-roll-frame",
        success:function(response){
            if(!checkUserAuthenticationAjx(response))return false;
            $("#sliderDiv").html(response)
            }
        })
}
}
},
rules:{
    start_id:{
        required:function(){
            if($("#search_type_1").is(":checked")==true)return true;else return false
                }
            },
end_id:{
    required:function(){
        if($("#search_type_1").is(":checked")==true)return true;else return false
            }
        },
series:{
    required:function(){
        if($("#search_type_0").is(":checked")==true)return true;else return false
            }
        },
ship_name:{
    required:function(){
        if($("#search_type_2").is(":checked")==true)return true;else return false
            }
        }
},
messages:{
    start_id:{
        required:START_ID_INVALID
    },
    end_id:{
        required:END_ID_INVALID
    },
    series:{
        required:SERIES_INVALID
    },
    ship_name:{
        required:SHIP_NAME_INVALID
    }
}
});
$("#arrival_date").datepicker({
    changeMonth:true,
    changeYear:true
});
$("#p_arrival_date").datepicker({
    changeMonth:true,
    changeYear:true
});
$("#previousRoll").hide();
$("#previousFrame").hide();
$("#nextRoll").hide();
$("#nextFrame").hide();
$(".e2").select2();
$("#series").change(function(){
    showAjxLoader();
    $("#previousRoll").hide();
    $("#previousFrame").hide();
    $("#roll_index").val("0");
    $("#frame").val("");
    $("#frame_index").val("0");
    $("#frame_range").html("");
    if($("#roll").next().attr("id")=="roll_error")$("#roll").next().remove();
    if($("#frame").next().attr("id")=="series_error")$("#frame").next().remove();
    $.ajax({
        type:"POST",
        data:{
            series:$("#series").val()
            },
        url:"/get-passenger-roll",
        success:function(response){
            if(!checkUserAuthenticationAjx(response))return false;
            $("#roll_list").val(response);
            var jsonObj=jQuery.parseJSON($("#roll_list").val());
            var sizeof=jsonObj.length;
            $("#roll").val(jsonObj[0]["ROLL_NBR"]);
            $("#roll_range").html("("+jsonObj[0]["ROLL_NBR"]+" to "+jsonObj[sizeof-1]["ROLL_NBR"]+")");
            $("#roll").attr("readonly",false);
            $("#frame").attr("readonly",true);
            $("#nextRoll").show();
            $("#nextFrame").show();
            getRollFrame()
            }
        });
if($("#series").val()==""){
    $("#roll").attr("readonly",true);
    $("#frame").attr("readonly",true)
    }
});
$("#nextRoll").click(function(){
    $("#previousFrame").hide();
    $("#frame_index").val("");
    $("#frame_list").val("");
    $("#frame").val("");
    $("#frame_range").html("");
    $("#nextFrame").show();
    var jsonObj=
    jQuery.parseJSON($("#roll_list").val());
    var index=jsonObj.map(function(d){
        return d["ROLL_NBR"]
        }).indexOf($("#roll").val());
    if(index!="-1")$("#roll_index").val(index);
    var roll_index=$("#roll_index").val()*1+1*1;
    $("#roll_index").val(roll_index);
    $("#roll").val(jsonObj[roll_index]["ROLL_NBR"]);
    $("#previousRoll").show()
    });
$("#previousRoll").click(function(){
    $("#previousFrame").hide();
    $("#frame_index").val("");
    $("#frame_list").val("");
    $("#frame").val("");
    $("#frame_range").html("");
    var jsonObj=jQuery.parseJSON($("#roll_list").val());
    var index=jsonObj.map(function(d){
        return d["ROLL_NBR"]
        }).indexOf($("#roll").val());
    if(index!="-1")$("#roll_index").val(index);
    var roll_index=$("#roll_index").val()*1-1*1;
    if(roll_index>=0){
        $("#roll_index").val(roll_index);
        $("#roll").val(jsonObj[roll_index]["ROLL_NBR"])
        }else $("#previousRoll").hide()
        });
$("#nextFrame").click(function(){
    if($("#frame_index").val()==""){
        showAjxLoader();
        getRollFrame()
        }else{
        $("#previousFrame").show();
        var jsonObj=jQuery.parseJSON($("#frame_list").val());
        var index=jsonObj.map(function(d){
            return d["FRAME"]
            }).indexOf($("#frame").val());
        if(index!="-1")$("#frame_index").val(index);
        var frame_index=$("#frame_index").val()*1+1*1;
        $("#frame_index").val(frame_index);
        if(jsonObj.length<=frame_index)$("#nextFrame").hide();else $("#frame").val(jsonObj[frame_index]["FRAME"])
            }
        });
$("#previousFrame").click(function(){
    var jsonObj=jQuery.parseJSON($("#frame_list").val());
    var index=jsonObj.map(function(d){
        return d["FRAME"]
        }).indexOf($("#frame").val());
    if(index!="-1")$("#frame_index").val(index);
    var frame_index=$("#frame_index").val()*1-1*1;
    if(frame_index>=
        0){
        $("#nextFrame").show();
        $("#frame_index").val(frame_index);
        $("#frame").val(jsonObj[frame_index]["FRAME"])
        }else $("#previousFrame").hide()
        });
$("#submit").click(function(){
    var rows=jQuery("#list").jqGrid("getRowData");
    var ids=[];
    for(var i=0;i<rows.length;i++){
        var row=rows[i];
        if($("#jqg_list_"+row["Id"]).is(":checked"))ids.push(row["Id"])
            }
            if(ids.length<=0){
        $("#errorMsg").show();
        $("#errorMsg").html(L_ATLEAST_ONE_PASSENGER);
        return false
        }else $("#errorMsg").hide();
        
    if($.trim($("#p_roll_no").val())=="" && $.trim($("#p_frame").val())!="" || $.trim($("#p_roll_no").val())!="" && $.trim($("#p_frame").val())==""){
        $("#errorMsg").show();
        $("#errorMsg").html('Please enter Series-Roll Number and Frame');
        return false
        }else $("#errorMsg").hide();        
        
    if($.trim($("#p_roll_no").val())==""&&
        $.trim($("#p_frame").val())==""&&$.trim($("#p_ship_name").val())==""&&$.trim($("#p_arrival_date").val())==""&&$.trim($("#p_last_name").val())==""){
        $("#errorMsg").show();
        $("#errorMsg").html(L_ATLEAST_ONE_VALUE);
        return false
        }else $("#errorMsg").hide();
    $(".update_confirm").colorbox({
        width:"700px",
        height:"180px",
        inline:true
    });
    $("#no_add").click(function(){
        window.location.href="/crm-bulk-search-passenger"
        });
    $("#yes_add").click(function(){
        showAjxLoader();
        $.ajax({
            type:"POST",
            data:{
                ids:ids,
                searchString:$("#update_passenger").serialize()
                },
            url:"/update-bulk-passenger",
            success:function(response){
                hideAjxLoader();
                var jsonObj=jQuery.parseJSON(response);
                if(jsonObj.status=="success")window.location.href="/crm-bulk-search-passenger"
                    }
                })
    })
});
$("#p_ship_name").autocomplete({
    afterAdd:true,
    selectFirst:true,
    autoFocus:true,
    source:function(request,response){
        $.ajax({
            url:"/get-ship-list",
            method:"POST",
            dataType:"json",
            data:{
                ship:$("#p_ship_name").val()
                },
            success:function(jsonResult){
                var resultset=[];
                $.each(jsonResult,function(){
                    var value=this.ship_name;
                    resultset.push({
                        value:this.ship_name
                        })
                    });
                response(resultset)
                }
            })
    },
open:function(){
    $(".ui-menu").width(350)
    },
change:function(event,ui){
    if(ui.item)$("#p_ship_name").val(ui.item.value);else $("#p_ship_name").val("")
        },
focus:function(event,ui){
    return false
    },
select:function(event,ui){
    $("#p_ship_name").val(ui.item.value);
    return false
    }
});
$("#ship_name").autocomplete({
    afterAdd:true,
    selectFirst:true,
    autoFocus:true,
    source:function(request,response){
        $.ajax({
            url:"/get-ship-list",
            method:"POST",
            dataType:"json",
            data:{
                ship:$("#ship_name").val()
                },
            success:function(jsonResult){
                var resultset=[];
                $.each(jsonResult,function(){
                    var value=this.ship_name;
                    resultset.push({
                        value:this.ship_name
                        })
                    });
                response(resultset)
                }
            })
    },
open:function(){
    $(".ui-menu").width(350)
    },
change:function(event,ui){
    if(ui.item)$("#ship_name").val(ui.item.value);else $("#ship_name").val("")
        },
focus:function(event,ui){
    return false
    },
select:function(event,ui){
    $("#ship_name").val(ui.item.value);
    return false
    }
})
});
function showSearchOption(obj){
    if(obj.value==1){
        $("#byTripSearch").hide();
        $("#bySeriesSearch").hide();
        $("#byPassengerSearch").show();
        $("#sliderDiv").hide()
        }
        if(obj.value==2){
        $("#byPassengerSearch").hide();
        $("#byTripSearch").hide();
        $("#bySeriesSearch").show();
        $("#sliderDiv").html("");
        $("#sliderDiv").show()
        }
        if(obj.value==3){
        $("#byPassengerSearch").hide();
        $("#bySeriesSearch").hide();
        $("#byTripSearch").show();
        $("#sliderDiv").html("");
        $("#sliderDiv").show()
        }
    }
function searchPassenger(){
    window.location.hash="#sliderDiv";
    var grid=jQuery("#list");
    var emptyMsgDiv=$('<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");
    $.jgrid.no_legacy_api=true;
    $.jgrid.useJSON=true;
    $("#list").jqGrid({
        postData:{
            searchString:$("#search_passenger").serialize()
            },
        mtype:"POST",
        url:"/get-crm-search-passenger-list",
        datatype:"json",
        sortable:true,
        colNames:[ID,PASSENGER_ID,LAST_NAME,FIRST_NAME_PASSENGER,REF_LINE_NUMBER,SHIP_NAME,S_ARRIVAL_DATE,MANIFEST,GENDER,RESIDENCE,MARITAL_STATUS,
        AGE,ETHNICITY],
        colModel:[{
            name:"Id",
            index:"field0",
            hidden:true,
            key:true
        },{
            name:"ID",
            index:"field0"
        },{
            name:"lastname",
            index:"field3"
        },{
            name:"firstname",
            index:"field2"
        },{
            name:"reflineno",
            index:"field16",
            align:"center"
        },{
            name:"shipname",
            index:"field7"
        },{
            name:"dateofarrival",
            index:"field18",
            align:"center"
        },{
            name:"manifest",
            index:"field32",
            align:"center",
            sortable:false,
            width:"200"
        },{
            name:"Gender",
            index:"field10",
            align:"center"
        },{
            name:"Residence",
            index:"field4"
        },{
            name:"MaritalStatus",
            index:"field11",
            align:"center"
        },

        {
            name:"Age",
            index:"field5",
            align:"center"
        },{
            name:"Ethnicity",
            index:"field36"
        }],
        viewrecords:true,
        sortname:"field0",
        sortorder:"ASC",
        rowNum:100,
        rowList:[30,50,100],
        pager:"#pcrudpassenger",
        viewrecords:true,
        autowidth:true,
        shrinkToFit:true,
        caption:"",
        width:"100%",
        multiselect:true,
        cmTemplate:{
            title:false
        },
        loadComplete:function(){
            hideAjxLoader();
            var count=grid.getGridParam();
            var ts=grid[0];
            if(ts.p.reccount===0){
                grid.hide();
                emptyMsgDiv.show();
                $("#pcrudpassenger_right div.ui-paging-info").css("display","none")
                }else{
                grid.show();
                emptyMsgDiv.hide();
                $("#pcrudpassenger_right div.ui-paging-info").css("display","block")
                }
            }
    });
emptyMsgDiv.insertAfter(grid.parent());
emptyMsgDiv.hide();
$("#list").jqGrid("navGrid","#pcrudpassenger",{
    reload:true,
    edit:false,
    add:false,
    search:false,
    del:false
});
$("#list").jqGrid("navButtonAdd","#pcrudpassenger",{
    caption:"",
    title:"Export",
    id:"exportExcel",
    onClickButton:function(){
        exportExcel("list","/get-crm-search-passenger-list")
        },
    position:"last"
})
}
function getSeries(){
    $("#p_roll_no").val($("#p_series").val()+"-")
    }
function getRollFrame(){
    $.ajax({
        type:"POST",
        data:{
            roll:$("#roll").val(),
            series:$("#series").val()
            },
        url:"/get-roll-frame",
        success:function(response){
            hideAjxLoader();
            if(!checkUserAuthenticationAjx(response))return false;
            $("#frame_list").val(response);
            var jsonObj=jQuery.parseJSON($("#frame_list").val());
            var sizeof=jsonObj.length;
            $("#frame_range").html("("+jsonObj[0]["FRAME"]+" to "+jsonObj[sizeof-1]["FRAME"]+")");
            $("#frame").val(jsonObj[0]["FRAME"]);
            $("#frame_index").val("0");
            $("#previousFrame").hide();
            $("#frame").attr("readonly",false)
            }
        })
}
function showPassengerMoreManifestImage(seriesValue,frame,passengerId){
    if(passengerId!="")passengerId="/"+passengerId;
    window.open("/show-manifest-more-image/"+seriesValue+"/"+frame+passengerId,frame,"width=1200,height=700,resizable=true,scrollbars=1")
    }
function getManifest(pgAct){
    if($("#filename").val()!="")if($("#filename").val().length!=17&&$("#filename").val().length!=19){
        $("#filename").after('<label id="filename_error" for="filename" generated="true" class="error" style="display:block">Please enter valid file name</label>');
        $("#filename_error").show()
        }else{
        $("#filename").next().remove();
        var seriesValue;
        var frame;
        if($("#filename").val().length==17){
            var filename=$("#filename").val();
            seriesValue=filename.substr(0,9)+"--"+pgAct;
            frame=filename.substr(9,
                4)
            }
            if($("#filename").val().length==19){
            var filename=$("#filename").val();
            seriesValue="T715-"+filename.substr(0,9)+"--"+pgAct;
            frame=filename.substr(10,5)
            }
            var passengerId;
        showPassengerMoreManifestImage(seriesValue,frame,passengerId="")
        }else{
        $("#filename").after('<label id="filename_error" for="filename" generated="true" class="error" style="display:block">Please enter file name</label>');
        $("#filename_error").show()
        }
    };
