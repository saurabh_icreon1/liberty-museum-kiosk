$(document).ready(function(){
    $("#contact").validate({
        submitHandler:function(){
            var dataStr=$("#contact").serialize();
            $.ajax({
                type:"POST",
                url:"/email-annotation-contact",
                data:dataStr,
                success:function(responseData){
                    hideAjxLoader();
                    var jsonObj=jQuery.parseJSON(responseData);
                    if(jsonObj.status=="success")showSendMail(0);else $.each(jsonObj.message,function(i,msg){
                        $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                        })
                    }
                    })
        },
    rules:{
        agree:{
            required:true
        }
    },
    messages:{
        agree:{
            required:"Please check the agreement"
        }
    }
});
$("#addAnnotation").click(function(){
    addAnnotation(1)
    });
$("#addCorrection").click(function(){
    addCorrection(1)
    })
});
function saveToFile(){
    authenticateUserBeforeAjax();
    showAjxLoader();
    $.ajax({
        type:"GET",
        data:{},
        url:"/passenger-save-to-file/"+passengerId,
        success:function(response){
            hideAjxLoader();
            var jsonObj=jQuery.parseJSON(response);
            if(jsonObj.status=="success"){
                $.colorbox({
                    width:"700px",
                    height:"285px",
                    inline:true,
                    href:".success_msg"
                });
                $(".success_msg").show();
                $("#cboxClose").on("click",function(){
                    $(".success_msg").hide()
                    })
                }
                if(jsonObj.status=="already"){
                $.colorbox({
                    width:"700px",
                    height:"285px",
                    inline:true,
                    href:".error_msg"
                });
                $(".error_msg").show();
                $("#cboxClose").on("click",function(){
                    $(".error_msg").hide()
                    })
                }
                if(jsonObj.status=="exceed"){
                $.colorbox({
                    width:"700px",
                    height:"320px",
                    inline:true,
                    href:".exceed"
                });
                $(".exceed").show();
                $("#cboxClose").on("click",function(){
                    $(".exceed").hide()
                    })
                }
            }
    })
}
function saveShipToFile(shipId,arrival_date,ship_name){
    authenticateUserBeforeAjax();
    showAjxLoader();
    $.ajax({
        type:"GET",
        data:{},
        url:"/ship-save-to-file/"+shipId+"/"+arrival_date+"/"+ship_name,
        success:function(response){
            hideAjxLoader();
            var jsonObj=jQuery.parseJSON(response);
            if(jsonObj.status=="success"){
                $.colorbox({
                    width:"700px",
                    height:"285px",
                    inline:true,
                    href:".success_msg"
                });
                $(".success_msg").show();
                $("#cboxClose").on("click",function(){
                    $(".success_msg").hide()
                    })
                }
                if(jsonObj.status=="already"){
                $.colorbox({
                    width:"700px",
                    height:"285px",
                    inline:true,
                    href:".error_msg"
                });
                $(".error_msg").show();
                $("#cboxClose").on("click",function(){
                    $(".error_msg").hide()
                    })
                }
                if(jsonObj.status=="exceed"){
                $.colorbox({
                    width:"600px",
                    height:"320px",
                    inline:true,
                    href:".exceed"
                });
                $(".exceed").show();
                $("#cboxClose").on("click",function(){
                    $(".exceed").hide()
                    })
                }
            }
    })
}
function saveToImageFile(dateArrivalVal,shipIdVal,shipName){
    authenticateUserBeforeAjax();
    showAjxLoader();
    $.ajax({
        type:"GET",
        data:{},
        url:"/manifest-save-to-file/"+$("#centerImage").val()+"/"+dateArrivalVal+"/"+shipIdVal+"/"+shipName+"/"+passengerId,
        success:function(response){
            hideAjxLoader();
            var jsonObj=jQuery.parseJSON(response);
            if(jsonObj.status=="success"){
                $.colorbox({
                    width:"700px",
                    height:"285px",
                    inline:true,
                    href:".success_msg"
                });
                $(".success_msg").show();
                $("#cboxClose").on("click",function(){
                    $(".success_msg").hide()
                    })
                }
                if(jsonObj.status==
                "already"){
                $.colorbox({
                    width:"700px",
                    height:"285px",
                    inline:true,
                    href:".error_msg"
                });
                $(".error_msg").show();
                $("#cboxClose").on("click",function(){
                    $(".error_msg").hide()
                    })
                }
                if(jsonObj.status=="exceed"){
                $.colorbox({
                    width:"600px",
                    height:"320px",
                    inline:true,
                    href:".exceed"
                });
                $(".exceed").show();
                $("#cboxClose").on("click",function(){
                    $(".exceed").hide()
                    })
                }
            }
    })
}
function showPassengerDetails(id,annotationId,page,addedBy){
    var url;
    showAjxLoader();
    $("#annotationTab").removeClass("active");
    if(id=="annotationTab"){
        url="/passenger-annotation/";
        $("#PassengerRightPanelSuggest").hide();
        $("#PassengerRightPanelAnnotation").show();
        //ga('require', 'displayfeatures');
        //ga('send', 'pageview',{'page': '/annotation','title': 'Annotations'});
        $.ajax({
            type:"POST",
            cache:true,
            data:{},
            url:"/get-passenger-annotation-listing/"+passengerId,
            success:function(response){
                $("#PassengerRightPanelAnnotation").html(response)
                }
            })
    }else if(id=="shipManifestTab"){
    url="/text-passenger-list/";
    $("#PassengerRightPanelSuggest").show();
    $("#PassengerRightPanelAnnotation").hide();
    //ga('require', 'displayfeatures');
    //ga('send', 'pageview',{'page': '/ship-manifest','title': 'Ship Manifest'});
    var imageValue="all";
    $.ajax({
        type:"POST",
        cache:true,
        data:{
            image_id:imageValue
        },
        url:"/delete-additional-image",
        success:function(response){}
    })
    }else if(id=="passengerRecordTab"){
    url="/passenger-record/";
    $("#PassengerRightPanelSuggest").show();
    $("#PassengerRightPanelAnnotation").hide();
    //ga('require', 'displayfeatures');
    //ga('send', 'pageview',{'page': '/passenger-records','title': 'Passenger Record'});
    }else if(id=="shipInformationTab"){
    url="/passenger-ship-information/";
    $("#PassengerRightPanelSuggest").show();
    $("#PassengerRightPanelAnnotation").hide();
    //ga('require', 'displayfeatures');
    //ga('send', 'pageview',{'page': '/ship-information','title': 'Ship Information'});
    }else if(id=="textPassengerListTab"){
    url="/text-passenger-list/";
    $("#PassengerRightPanelSuggest").show();
    $("#PassengerRightPanelAnnotation").hide();
    //ga('require', 'displayfeatures');
    //ga('send', 'pageview',{'page': '/text-passenger-list','title': 'Text Passenger List'});
    }

   if(passengerId&&passengerId!="")passengerId=passengerId;else passengerId="0";
if(ship_name&&ship_name!="")ship_name=ship_name;else ship_name="0";
if(date_arrive&&date_arrive!="")date_arrive=date_arrive;else date_arrive="0";
if(manifestFile&&manifestFile!="")manifestFile=manifestFile;else manifestFile="0";
if(shipLineId&&shipLineId!="")shipLineId=shipLineId;else shipLineId="0";
if(annotationId){
    annotationId=annotationId;
    $("#annotationTab").addClass("active");
    $("#passengerRecordTab").removeClass("active")
    }else annotationId="0";

$.ajax({
    type:"POST",
    data:{
        page:page,
        typeShipText:id
    },
    url:url+passengerId+"/"+ship_name+"/"+date_arrive+"/"+annotationId+"/"+manifestFile+"/"+shipLineId,
    success:function(response){
        hideAjxLoader();
        if(response!="session_expired")$("#page_content").html(response);
        if(id=="textPassengerListTab"||id=="shipManifestTab"){
            if($("#record_series").val()!="");
            if(id=="textPassengerListTab")window.location.href="#passengerListAnchor"
                }else;
        if(id=="annotationTab")if($("#rec_added_by")&&$.trim($("#rec_added_by").val())!="")if($("#rec_added_by").val()==
            $("#logged_user_id").val()||$("#userAnnotationCount").val()==0)$("#addAnnotation").show();else $("#addAnnotation").hide();else $("#addAnnotation").show()
            }
        })
}
function showAnnotationTab(){
    $("#annotationTab").click();
    $("#annotationTab").addClass("active");
    $("#passengerRecordTab").removeClass("active");
    setTimeout("addAnnotation(0)","600")
    }
function showCorrectionTab(){
    $("#annotationTab").click();
    $("#annotationTab").addClass("active");
    $("#passengerRecordTab").removeClass("active");
    setTimeout("addCorrection(0)","600")
    }
    function addAnnotation(val){
    authenticateUserBeforeAjax();
    if(val==1)showAjxLoader();
    var url="/passenger-add-annotation/";
    $.ajax({
        type:"GET",
        data:{},
        url:url+passengerId,
        success:function(response){
            hideAjxLoader();
            $("#annotationCorrection").html(response)
            }
        })
}
function addCorrection(val){
    authenticateUserBeforeAjax();
    if(val==1)showAjxLoader();
    var url="/passenger-add-correction/";
    $.ajax({
        type:"GET",
        data:{},
        url:url+passengerId,
        success:function(response){
            hideAjxLoader();
            $("#annotationCorrection").html(response)
            }
        })
}
function backToPassenger(){
    location.href="/passenger"
    }
function getCookie(c_name){
    var c_value=document.cookie;
    var c_start=c_value.indexOf(" "+c_name+"=");
    if(c_start==-1)c_start=c_value.indexOf(c_name+"=");
    if(c_start==-1)c_value=null;
    else{
        c_start=c_value.indexOf("=",c_start)+1;
        var c_end=c_value.indexOf(";",c_start);
        if(c_end==-1)c_end=c_value.length;
        c_value=unescape(c_value.substring(c_start,c_end))
        }
        return c_value
    };
    
function sendPassengerRecord() {
	$("#email_address_message").hide();
	var email_address=$("#email_address").val();
	$('label[for=email_address]').remove();
    var pattern= /^\b[A-Z0-9._%-+]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
    if(!pattern.test(email_address) || $.trim(email_address) == ''){ 
        $("#email_address").after('<label class="error" for="email_address" style="display:block;">Please enter valid Email Id.</label>');
            return false
        }
    else showAjxLoader();
    
    $.ajax({
        type:"POST",
        url:"/send-passenger-record",
        data:{
            emailId:$("#email_address").val(),
            passenger_id:passengerId
            },
        success:function(responseData){
            hideAjxLoader();
            var jsonObj=jQuery.parseJSON(responseData);
            if(jsonObj.status=="success"){
            
                $.colorbox({
                    width:"700px",
                    height:"285px",
                    inline:true,
                    href:".email_success_msg"
                });
                $(".email_success_msg").show();
                $("#email_address").val('');;
                $("#cboxClose").on("click",function(){
                    $(".email_success_msg").hide()
                            })
                            
                //$("#email_address_message").show();
               // $("#email_address_message").html(jsonObj.message);
            }
            }
        });        
    }
function requestPrintCertificate() {
    $.ajax({
        type:"POST",
        url:"/print-certificate-send",
        data:{
            emailId:REQUEST_PRINT_CERTIFICATE_EMAIL_ID,
            passenger_id:passengerId,
            mailTo:'admin'
            },
        success:function(responseData){
            hideAjxLoader();
            var jsonObj=jQuery.parseJSON(responseData);
            if(jsonObj.status=="success"){
            
                $.colorbox({
                    width:"700px",
                    height:"285px",
                    inline:true,
                    href:".email_success_msg_free"
                });
                $(".email_success_msg_free").show();
                $("#cboxClose").on("click",function(){
                    $(".email_success_msg_free").hide()
                })
            }
            }
        });    
}    
function sendShipRecord() {
	var email_address=$("#email_address").val();
	$('label[for=email_address]').remove();
    var pattern= /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;
    if(!pattern.test(email_address) || $.trim(email_address) == ''){ 
        $("#email_address").after('<label class="error" for="email_address" style="display:block;">Please enter valid Email Id!</label>');
            return false
        }
    else showAjxLoader();
    
    $.ajax({
        type:"POST",
        url:"/send-ship-record",
        data:{
            emailId:$("#email_address").val(),
            ship_assetvalue:$("#ship_assetvalue").val()
            },
        success:function(responseData){
            hideAjxLoader();
            var jsonObj=jQuery.parseJSON(responseData);
            if(jsonObj.status=="success"){
                $("#email_address_message").show();
                $("#email_address_message").html(jsonObj.message);
            }
            }
        });        
    }
	
	function viewAnnotationLog(passengerId){
		showAjxLoader();
		$.ajax({
		type:"POST",
		cache:true,
		data:{},
		url:"/get-annotation-log/"+passengerId,
		success:function(response){
			hideAjxLoader();
			$("#annotationCorrection").html(response)
			}
		});
	}
	/*This is used to view the passenger record*/
	function viewSampleRecord(){
				showAjxLoader();
				$.colorbox({
                    width:"800px", 
                    height:"700px",
                    iframe : true,
                    href:"/sample-passenger-record/"+passengerId,
                    onComplete:function(){
						hideAjxLoader();
					}
                }); 
	}