window.onerror=function(message,url,line){prompt("A Javascript error occurred!\n "+message+"\n at line "+line,url);
return false};var PXN8=PXN8||{};PXN8.server=document.location.protocol+"/"+"/"+document.location.host;PXN8.root=
"/pixenate/";PXN8.basename="pxn8.pl";PXN8.replaceOnSave=true;PXN8.aspectRatio={width:0,height:0};PXN8.position={x:"-",y:
"-"};PXN8.style={};PXN8.style.notSelected={opacity:0.33,color:"black"};PXN8.style.resizeHandles={color:"white",size:6,
smallsize:4,oldsize:-1};PXN8.ON_IMAGE_LOAD="ON_IMAGE_LOAD";PXN8.ON_IMAGE_CHANGE="ON_IMAGE_CHANGE";PXN8.
BEFORE_IMAGE_CHANGE="BEFORE_IMAGE_CHANGE";PXN8.ON_ZOOM_CHANGE="ON_ZOOM_CHANGE";PXN8.ON_SELECTION_CHANGE=
"ON_SELECTION_CHANGE";PXN8.ON_SELECTION_COMPLETE="ON_SELECTION_COMPLETE";PXN8.ON_IMAGE_ERROR="ON_IMAGE_ERROR";PXN8.
listener={listenersByType:{}};PXN8.listener.listenersByType[PXN8.ON_ZOOM_CHANGE]=[];PXN8.listener.listenersByType[PXN8.
ON_SELECTION_CHANGE]=[];PXN8.listener.listenersByType[PXN8.ON_IMAGE_CHANGE]=[];PXN8.listener.listenersByType[PXN8.
ON_IMAGE_ERROR]=[];PXN8.listener.add=function(eventType,callback){var self=PXN8.listener,callbacks=self.listenersByType[
eventType],found=false;if(!callbacks){callbacks=[];self.listenersByType[eventType]=callbacks}for(var i=0;i<callbacks.
length;i++)if(callbacks[i]==callback){found=true;break}if(!found)callbacks.push(callback);return callback};PXN8.listener
.remove=function(eventType,callback){var self=PXN8.listener,callbacks=self.listenersByType[eventType];if(!callbacks)
return;for(var i=0;i<callbacks.length;i++)if(callbacks[i]==callback){callbacks.splice(i,1);i--}};PXN8.listener.onceOnly=
function(eventType,callback){var self=PXN8.listener;callback.onceOnly=true;return self.add(eventType,callback)};PXN8.
opNumber=0;PXN8.maxOpNumber=0;PXN8.response={status:"",image:"",errorCode:0,errorMessage:""};PXN8.updating=false;PXN8.
resizelimit={width:1600,height:1200};PXN8.imagesBySrc={};PXN8.sx=0;PXN8.sy=0;PXN8.ex=0;PXN8.ey=0;PXN8.initialize=
function(param){PXN8.ready=false;var _=PXN8.dom,image_src,paramType=typeof param;if(paramType=="string")image_src=param;
else image_src=param.url;PXN8.priv.createSelectionRect();var canvas=PXN8.initializeCanvas(),imgContainer=_.id(
"pxn8_image_container");if(!imgContainer){imgContainer=_.ac(canvas,_.ce("div",{id:"pxn8_image_container"}));if(document.
all)imgContainer.style.backgroundColor="black"}if(navigator.userAgent.indexOf("Opera")!=-1)PXN8.style.notSelected.color=
"transparent";try{document.execCommand("BackgroundImageCache",false,true)}catch(e){}var rects=["pxn8_top_rect",
"pxn8_bottom_rect","pxn8_left_rect","pxn8_right_rect","pxn8_topleft_rect","pxn8_topright_rect","pxn8_bottomleft_rect",
"pxn8_bottomright_rect"];for(var i=0;i<rects.length;i++){var rect=_.id(rects[i]);if(!rect)rect=_.ac(canvas,_.ce("div",{
id:rects[i]}));rect.style.fontSize="0px";if(!rect.style.backgroundColor)rect.style.backgroundColor=PXN8.style.
notSelected.color;rect.style.position="absolute";if(!rect.style.opacity)_.opacity(rect,PXN8.style.notSelected.opacity);
rect.style.top="0px";rect.style.left="0px";rect.style.width="0px";rect.style.height="0px";rect.style.display="none";rect
.style.zIndex=1;var antshz="url("+PXN8.server+PXN8.root+"/images/ants_hr.gif)",antsvt="url("+PXN8.server+PXN8.root+
"/images/ants_vt.gif)";if(rects[i]=="pxn8_top_rect"){rect.style.backgroundImage=antshz;rect.style.backgroundPosition=
"bottom left";rect.style.backgroundRepeat="repeat-x"}if(rects[i]=="pxn8_bottom_rect"){rect.style.backgroundImage=antshz;
rect.style.backgroundPosition="top left";rect.style.backgroundRepeat="repeat-x"}if(rects[i]=="pxn8_left_rect"){rect.
style.backgroundImage=antsvt;rect.style.backgroundPosition="top right";rect.style.backgroundRepeat="repeat-y"}if(rects[i
]=="pxn8_right_rect"){rect.style.backgroundImage=antsvt;rect.style.backgroundPosition="top left";rect.style.
backgroundRepeat="repeat-y"}}PXN8.image.location=image_src;PXN8.opNumber=0;PXN8.maxOpNumber=0;PXN8.history=[];PXN8.
offsets=[1];var fetchOp={operation:"fetch",url:PXN8.image.location};fetchOp.pxn8root=PXN8.root;if(paramType=="object")
for(var i in param)fetchOp[i]=param[i];PXN8.history.push(fetchOp);if(PXN8.replaceOnSave)fetchOp.random=PXN8.randomHex();
var gotAbsoluteImageSrc=false,getAbsoluteImageSrc=function(){if(gotAbsoluteImageSrc){getAbsoluteImageSrc=function(){};
return}gotAbsoluteImageSrc=true;var fetchOp=PXN8.getOperation(0),theImage=document.getElementById("pxn8_image");fetchOp.
url=theImage.src;PXN8.ready=true},pxn8image=_.id("pxn8_image"),onImageLoad=function(){var _=PXN8.dom,pxn8image=_.id(
"pxn8_image");PXN8.image.width=pxn8image.width;PXN8.image.height=pxn8image.height;PXN8.priv.addImageToHistory(pxn8image.
src);PXN8.show.size();getAbsoluteImageSrc()};if(!pxn8image){var innerHTML='<img id="pxn8_image" border="0" src="'+PXN8.
image.location+'"/>';try{imgContainer.innerHTML=innerHTML}catch(e){alert(
"An error occurred while adding the <img> tag to the page.\n"+
"This is most likely because the pxn8_canvas DIV has been added to an incorrect element (<table> or <p>).\n"+
"The error message reported was: "+e.message);PXN8.dom.ac(imgContainer,PXN8.dom.ce("img",{id:"pxn8_image",src:PXN8.image
.location}))}pxn8image=_.id("pxn8_image")}else{var imgContainer=_.cl("pxn8_image_container"),innerHTML=
'<img id="pxn8_image" border="0" src="'+PXN8.image.location+'"/>';try{imgContainer.innerHTML=innerHTML}catch(e){alert(
"An error occurred while adding the <img> tag to the page.\n"+
"This is most likely because the pxn8_canvas DIV has been added to an incorrect element (<table> or <p>).\n"+
"The error message reported was: "+e.message);PXN8.dom.ac(imgContainer,PXN8.dom.ce("img",{id:"pxn8_image",src:PXN8.image
.location}))}pxn8image=_.id("pxn8_image")}pxn8image.onload=onImageLoad;PXN8.event.removeListener(pxn8image,"load",PXN8.
imageLoadNotifier);PXN8.event.addListener(pxn8image,"load",PXN8.imageLoadNotifier)};PXN8.select=function(startX,startY,
width,height){var self=PXN8;if(typeof startX=="object"){var sel=startX;startY=sel.top;width=sel.width;height=sel.height;
startX=sel.left}self.sx=startX;self.sy=startY;self.ex=self.sx+width;self.ey=self.sy+height;if(PXN8.select.
constrainToImageBounds==true){if(self.sx<0)self.sx=0;if(self.sy<0)self.sy=0;if(self.ex>PXN8.image.width){self.ex=PXN8.
image.width;self.sx=self.ex-width}if(self.ey>PXN8.image.height){self.ey=PXN8.image.height;self.sy=self.ey-height}}self.
position.x=startX;self.position.y=startY;var selection=self.getSelection();self.listener.notify(PXN8.ON_SELECTION_CHANGE
,selection)};PXN8.select.constrainToImageBounds=true;PXN8.select.disabler=function(eventType,selection){if(selection.
width>0||selection.height>0){PXN8.unselect();return}};PXN8.select.enable=function(enabled,unselect){var canvas=document.
getElementById("pxn8_canvas");if(enabled)PXN8.listener.remove(PXN8.ON_SELECTION_CHANGE,PXN8.select.disabler);else{if(
unselect)PXN8.unselect();PXN8.listener.add(PXN8.ON_SELECTION_CHANGE,PXN8.select.disabler)}};PXN8.select.defaultListener=
function(eventType,selection){var _=PXN8.dom,self=PXN8,theImg=_.id("pxn8_image"),selectRect=_.id("pxn8_select_rect"),
leftRect=_.id("pxn8_left_rect"),rightRect=_.id("pxn8_right_rect"),topRect=_.id("pxn8_top_rect"),bottomRect=_.id(
"pxn8_bottom_rect"),topleftRect=_.id("pxn8_topleft_rect"),toprightRect=_.id("pxn8_topright_rect"),bottomleftRect=_.id(
"pxn8_bottomleft_rect"),bottomrightRect=_.id("pxn8_bottomright_rect");PXN8.show.position();PXN8.show.selection();if(
eventType==PXN8.ON_SELECTION_CHANGE&&selection==null||eventType==PXN8.ON_ZOOM_CHANGE)selection=self.getSelection();if(
selection.width<=0&&selection.height<=0){selectRect.style.display="none";leftRect.style.display="none";rightRect.style.
display="none";topRect.style.display="none";bottomRect.style.display="none";topleftRect.style.display="none";
toprightRect.style.display="none";bottomleftRect.style.display="none";bottomrightRect.style.display="none";return}var 
zoom=PXN8.zoom.value(),sel={};for(var i in selection)if(typeof selection[i]!="function")sel[i]=Math.floor(selection[i]*
zoom);var bh=theImg.height-(sel.top+sel.height);if(bh<0)bh=0;bh=bh+"px";var bt=sel.top+sel.height+"px",th=sel.top+"px",
ll="0px",lw=sel.left+"px",rw=theImg.width-(sel.left+sel.width);if(rw<0)rw=0;rw=rw+"px";var rl=sel.left+sel.width+"px";
topleftRect.style.display="block";topleftRect.style.top="0px";topleftRect.style.left=ll;topleftRect.style.width=lw;
topleftRect.style.height=th;leftRect.style.display="block";leftRect.style.top=sel.top+"px";leftRect.style.left=ll;
leftRect.style.width=lw;leftRect.style.height=sel.height+"px";bottomleftRect.style.display="block";bottomleftRect.style.
top=bt;bottomleftRect.style.left=ll;bottomleftRect.style.width=lw;bottomleftRect.style.height=bh;topRect.style.display=
"block";topRect.style.top="0px";topRect.style.left=sel.left+"px";topRect.style.width=sel.width+"px";topRect.style.height
=th;selectRect.style.top=sel.top+"px";selectRect.style.left=sel.left+"px";selectRect.style.width=sel.width+"px";
selectRect.style.height=sel.height+"px";selectRect.style.display="block";selectRect.style.zIndex=100;bottomRect.style.
display="block";bottomRect.style.top=bt;bottomRect.style.left=sel.left+"px";bottomRect.style.width=sel.width+"px";
bottomRect.style.height=bh;toprightRect.style.display="block";toprightRect.style.top="0px";toprightRect.style.left=rl;
toprightRect.style.width=rw;toprightRect.style.height=th;rightRect.style.display="block";rightRect.style.top=sel.top+
"px";rightRect.style.left=rl;rightRect.style.width=rw;rightRect.style.height=sel.height+"px";bottomrightRect.style.
display="block";bottomrightRect.style.top=bt;bottomrightRect.style.left=rl;bottomrightRect.style.width=rw;
bottomrightRect.style.height=bh};PXN8.listener.add(PXN8.ON_SELECTION_CHANGE,PXN8.select.defaultListener);PXN8.listener.
add(PXN8.ON_ZOOM_CHANGE,PXN8.select.defaultListener);PXN8.getSelection=function(){var rect={},self=PXN8;rect.width=self.
ex>self.sx?self.ex-self.sx:self.sx-self.ex;rect.height=self.ey>self.sy?self.ey-self.sy:self.sy-self.ey;rect.left=self.ex
>self.sx?self.sx:self.ex;rect.top=self.ey>self.sy?self.sy:self.ey;return rect};PXN8.selectByRatio=function(ratio,
override){var _=PXN8.dom,self=PXN8;if(typeof ratio!="string"){alert("Ratio must be expressed as a string e.g. '4x6'");
return}if(!PXN8.ready){PXN8.listener.onceOnly(PXN8.ON_IMAGE_LOAD,function(){PXN8.selectByRatio(ratio,override)});return}
var ih=PXN8.image.height,iw=PXN8.image.width,sepIndex=ratio.indexOf("x");if(sepIndex!=-1){var rw=parseFloat(ratio.
substring(0,sepIndex)),rh=parseFloat(ratio.substring(sepIndex+1));if(override){PXN8.aspectRatio.width=rw;PXN8.
aspectRatio.height=rh}else if(iw>ih)if(rw>rh){PXN8.aspectRatio.width=rw;PXN8.aspectRatio.height=rh}else{PXN8.aspectRatio
.width=rh;PXN8.aspectRatio.height=rw}else if(rw>rh){PXN8.aspectRatio.width=rh;PXN8.aspectRatio.height=rw}else{PXN8.
aspectRatio.width=rw;PXN8.aspectRatio.height=rh}rw=PXN8.aspectRatio.width;rh=PXN8.aspectRatio.height}else{PXN8.
aspectRatio.width=0;PXN8.aspectRatio.height=0;PXN8.resize.enable(["n","s","e","w"],true);return}PXN8.resize.enable(["n",
"s","e","w"],false);var left=0,top=0,width=0,height=0,fitWidth=function(){width=iw;height=Math.round(width/rw*rh);top=
Math.round(ih/2-height/2)},fitHeight=function(){height=ih;width=Math.round(height/rh*rw);left=Math.round(iw/2-width/2)};
if(iw>ih)if(iw/ih>rw/rh)fitHeight();else fitWidth();else if(ih/iw>rh/rw)fitWidth();else fitHeight();self.select(left,top
,width,height)};PXN8.rotateSelection=function(){if(!PXN8.ready){PXN8.listener.onceOnly(PXN8.ON_IMAGE_LOAD,function(){
PXN8.rotateSelection()});return}var sel=PXN8.getSelection(),imgSize=PXN8.getImageSize(),cx=sel.left+sel.width/2,cy=sel.
top+sel.height/2;if(sel.width>imgSize.height){sel.height=sel.height*(imgSize.height/sel.width);sel.width=imgSize.height}
if(sel.height>imgSize.width){sel.width=sel.width*(imgSize.width/sel.height);sel.height=imgSize.width}PXN8.select(cx-sel.
height/2,cy-sel.width/2,sel.height,sel.width);var temp=PXN8.aspectRatio.width;PXN8.aspectRatio.width=PXN8.aspectRatio.
height;PXN8.aspectRatio.height=temp;PXN8.snapToAspectRatio()};PXN8.selectAll=function(){if(!PXN8.ready){PXN8.listener.
onceOnly(PXN8.ON_IMAGE_LOAD,function(){PXN8.selectAll()});return}PXN8.select(0,0,PXN8.image.width,PXN8.image.height)};
PXN8.unselect=function(){PXN8.select(0,0,0,0)};PXN8.getUncompressedImage=function(){if(PXN8.responses[PXN8.opNumber])
return PXN8.responses[PXN8.opNumber].uncompressed;else return false};PXN8.listener.notify=function(eventType,source){var
 self=PXN8.listener,listeners=self.listenersByType[eventType];if(listeners)for(var i=0;i<listeners.length;i++){var 
listener=listeners[i];if(listener!=null){listener(eventType,source);if(listener.onceOnly){PXN8.listener.remove(eventType
,listener);i--}}}};PXN8.imageLoadHousekeeping=function(eventType,theImage){var _=PXN8.dom;theImage=_.id(
"pxn8_buffered_image");if(theImage==null)theImage=_.id("pxn8_image");if(PXN8.log)PXN8.log.trace("image "+theImage.src+
" has loaded");PXN8.image.width=theImage.width;PXN8.image.height=theImage.height;var iw=PXN8.image.width,ih=PXN8.image.
height,zoomFactor=PXN8.zoom.value();theImage.width=iw*zoomFactor;theImage.height=ih*zoomFactor;_.ac(_.cl(
"pxn8_image_container"),theImage);theImage.id="pxn8_image";PXN8.show.size();PXN8.priv.addImageToHistory(theImage.src);
var selection=PXN8.getSelection();if(selection.width>iw||selection.left>iw||selection.height>ih||selection.top>ih)PXN8.
unselect();else{var moved=false;if(selection.left+selection.width>iw){selection.left=iw-selection.width;moved=true}if(
selection.top+selection.height>ih){selection.top=ih-selection.height;moved=true}if(moved)PXN8.select(selection)}PXN8.
listener.notify(PXN8.ON_SELECTION_CHANGE,selection);PXN8.imagesBySrc[theImage.src]=true;PXN8.listener.notify(PXN8.
ON_IMAGE_CHANGE);var timer=_.id("pxn8_timer");if(timer)timer.style.display="none";return theImage};PXN8.listener.add(
PXN8.ON_IMAGE_LOAD,PXN8.imageLoadHousekeeping);if(typeof log4javascript!="undefined")PXN8.log=log4javascript.
getDefaultLogger();else PXN8.log=false;PXN8.getOperation=function(i){if(i>PXN8.opNumber)return null;return PXN8.history[
i]};PXN8.getUserOperation=function(index){var self=PXN8,result=null,lastIndex=0;for(var i=0;i<index;i++)lastIndex+=self.
offsets[i];return self.history[lastIndex]};PXN8.getScript=function(){var self=PXN8,result=[],lastIndex=0;for(var i=0;i<=
self.opNumber;i++)lastIndex+=self.offsets[i];for(var i=0;i<lastIndex;i++){var original=self.history[i],duplicate={};for(
var j in original)duplicate[j]=original[j];result.push(duplicate)}return result};PXN8.isUpdating=function(){return PXN8.
updating};PXN8.curry=function(func,object){return function(){return func(object)}};PXN8.prepareForSubmit=function(msg){
var _=PXN8.dom;if(!msg)msg=PXN8.strings.UPDATING;var timer=_.id("pxn8_timer");if(!timer){timer=_.ce("div",{id:
"pxn8_timer"});_.ac(timer,_.tx(msg));var canvas=_.id("pxn8_canvas");_.ac(canvas,timer)}if(timer){_.ac(_.cl(timer),_.tx(
msg));timer.style.display="block";var theImage=_.id("pxn8_image"),imagePos=_.ep(theImage);timer.style.width=Math.max(200
,theImage.width)+"px"}PXN8.updating=true};PXN8.scrolledPoint=function(x,y){var result={x:x,y:y},canvas=document.
getElementById("pxn8_canvas");if(canvas.parentNode.id=="pxn8_scroller"){var scroller=document.getElementById(
"pxn8_scroller");result.x+=scroller.scrollLeft;result.y+=scroller.scrollTop}return result};PXN8.getWindowScrollPoint=
function(){var scrOfX=0,scrOfY=0;if(typeof window.pageYOffset=="number"){scrOfY=window.pageYOffset;scrOfX=window.
pageXOffset}else if(document.body&&(document.body.scrollLeft||document.body.scrollTop)){scrOfY=document.body.scrollTop;
scrOfX=document.body.scrollLeft}else if(document.documentElement&&(document.documentElement.scrollLeft||document.
documentElement.scrollTop)){scrOfY=document.documentElement.scrollTop;scrOfX=document.documentElement.scrollLeft}return{
x:scrOfX,y:scrOfY}};PXN8.createPin=function(pinId,imgSrc){var pinElement=document.createElement("img");pinElement.id=
pinId;pinElement.className="pin";pinElement.src=imgSrc;pinElement.style.position="absolute";return pinElement};PXN8.
mousePointToElementPoint=function(mx,my){var _=PXN8.dom,result={},canvas=_.id("pxn8_canvas"),imageBounds=_.eb(canvas),
scrolledPoint=PXN8.scrolledPoint(mx,my),zoom=PXN8.zoom.value();result.x=Math.round((scrolledPoint.x-imageBounds.x)/zoom)
;result.y=Math.round((scrolledPoint.y-imageBounds.y)/zoom);if(canvas.style.borderWidth){var borderWidth=parseInt(canvas.
style.borderWidth);result.x-=borderWidth;result.y-=borderWidth;if(result.x<0)result.x=0;if(result.y<0)result.y=0}
return result};PXN8.getImageSize=function(){var imgElement=document.getElementById("pxn8_image"),zoomValue=PXN8.zoom.
value(),realWidth=imgElement.width/zoomValue,realHeight=imgElement.height/zoomValue;return{width:realWidth,height:
realHeight}};PXN8.objectToString=function(obj){var s="",propToString=function(prop){return'"'+prop+'":'},
operationAlwaysFirst=function(a,b){if(a=="operation")return-1;if(b=="operation")return 1;return a>b?1:b>a?-1:0},types={
array:{s:"[",e:"]",indexer:function(o){var result=[];for(var i=0;i<o.length;i++)result.push(i);return result},pusher:
function(array,o,i){array.push(o)}},object:{s:"{",e:"}",indexer:function(o){var result=[];for(var i in o)if(typeof o[i]
!="function")result.push(i);return result.sort(operationAlwaysFirst)},pusher:function(array,o,i){array.push(propToString
(i)+o)}}},type="object";if(PXN8.isArray(obj))type="array";s=types[type].s;var props=[],pusher=types[type].pusher,indexes
=types[type].indexer(obj);for(var j=0;j<indexes.length;j++){var i=indexes[j];if(typeof obj[i]=="function")continue;if(
typeof obj[i]=="string")pusher(props,'"'+obj[i]+'"',i);else if(typeof obj[i]=="object")pusher(props,PXN8.objectToString(
obj[i]),i);else if(typeof obj[i]=="boolean")pusher(props,'"'+obj[i]+'"',i);else pusher(props,obj[i],i)}for(var i=0;i<
props.length;i++){s=s+props[i];if(i<props.length-1)s+=","}s+=types[type].e;return s};PXN8.isArray=function(o){return o&&
typeof o=="object"&&o.constructor==Array};PXN8.randomHex=function(){return Math.round(Math.random()*65535).toString(16)}
;PXN8.getImageBuffer=function(){var _=PXN8.dom,buffer=_.id("pxn8_buffer");if(!buffer){buffer=_.ce("div",{id:
"pxn8_buffer"});_.ac(document.body,buffer);buffer.style.width="1px";buffer.style.height="1px";buffer.style.overflow=
"hidden"}return buffer};PXN8.replaceImage=function(imageurl){var _=PXN8.dom,buffer=PXN8.getImageBuffer();_.cl(buffer);
var theImage=_.ce("img",{id:"pxn8_buffered_image"});_.ac(buffer,theImage);var timer=_.id("pxn8_timer");if(timer){timer.
style.display="block";_.ac(_.cl(timer),_.tx("Loading photo. Please wait..."))}var notified=false,closure={onload:
function(){if(!notified){PXN8.imageLoadNotifier();notified=true}}};PXN8.event.addListener(theImage,"load",closure.onload
);theImage.onload=closure.onload;theImage.src=imageurl;PXN8.show.size()};PXN8.imageUpdateDone=function(jsonResponse){var
 errorMsg=null,_=PXN8.dom,status=null;if(PXN8.log)PXN8.log.trace("PXN8.imageUpdateDone("+jsonResponse+")");var targetDiv
=_.id("pxn8_image_container");PXN8.response=jsonResponse;if(jsonResponse&&jsonResponse.status=="OK"){PXN8.responses[PXN8
.opNumber]=jsonResponse;if(document.all){}var newImageSrc=PXN8.server+PXN8.root+"/"+jsonResponse.image;PXN8.replaceImage
(newImageSrc)}else{status=PXN8.response;if(PXN8.response&&typeof PXN8.response=="object"){status=PXN8.response.status;
errorMsg="An error occurred while updating the image.\n"+"status: "+status+"\n"+"errorMessage: "+PXN8.response.
errorMessage;if(PXN8.log)PXN8.log.error(errorMsg);alert(errorMsg)}else{errorMsg=
"An error occurred while updating the image.\nstatus:"+status;if(PXN8.log)PXN8.log.error(errorMsg);alert(errorMsg)}PXN8.
listener.notify(PXN8.ON_IMAGE_ERROR);PXN8.updating=false;PXN8.opNumber--;var timer=_.id("pxn8_timer");if(timer)timer.
style.display="none"}var theImage=_.id("pxn8_image");theImage.onerror=function(){alert(PXN8.strings.IMAGE_ON_ERROR1+
theImage.src+PXN8.strings.IMAGE_ON_ERROR2);PXN8.listener.notify(PXN8.ON_IMAGE_ERROR)}};PXN8.show={};PXN8.show.selection=
function(){var _=PXN8.dom,selectionField=_.id("pxn8_selection_size");if(selectionField){var text="N/A";if(PXN8.ex-PXN8.
sx>0)text=PXN8.ex-PXN8.sx+","+(PXN8.ey-PXN8.sy);_.ac(_.cl(selectionField),_.tx(text))}};PXN8.show.position=function(){
var _=PXN8.dom,posInfo=_.id("pxn8_mouse_pos");if(posInfo){var text=PXN8.position.x+","+PXN8.position.y;_.ac(_.cl(posInfo
),_.tx(text))}};PXN8.show.zoom=function(t,v){var _=PXN8.dom,zoomInfo=_.id("pxn8_zoom");if(zoomInfo){var text=Math.round(
PXN8.zoom.value()*100)+"%";_.ac(_.cl(zoomInfo),_.tx(text))}};PXN8.show.size=function(){var _=PXN8.dom,sizeInfo=_.id(
"pxn8_image_size");if(sizeInfo){var text=PXN8.image.width+"x"+PXN8.image.height;_.ac(_.cl(sizeInfo),_.tx(text))}};PXN8.
show.alert=function(message,duration){var _=PXN8.dom;duration=duration||1000;var warning=_.id("pxn8_warning");if(!
warning){warning=_.ce("div",{id:"pxn8_warning",className:"warning"});_.ac(_.id("pxn8_canvas"),warning)}else warning.
style.display="block";_.ac(_.cl(warning),_.tx(message));_.opacity(warning,90);warning.style.width=(PXN8.image.width>200?
PXN8.image.width:200)+"px";setTimeout("PXN8.fade.init();PXN8.fade.fadeout('pxn8_warning',false);",duration)};PXN8.fade={
values:[0.99,0.85,0.70,0.55,0.40,0.25,0.10,0],times:[75,75,75,75,75,75,75,75],i:0,stopfadeout:false};PXN8.fade.init=
function(){var self=PXN8.fade;self.i=0;self.stopfadeout=false};PXN8.fade.cancel=function(){var self=PXN8.fade;self.
stopfadeout=true};PXN8.fade.fadeout=function(eltid,destroyOnFade){var _=PXN8.dom,self=PXN8.fade;if(self.stopfadeout)
return;_.opacity(eltid,self.values[self.i]);if(self.i<self.values.length-1){self.i++;setTimeout("PXN8.fade.fadeout('"+
eltid+"',"+destroyOnFade+");",self.times[self.i])}else if(destroyOnFade){var node=_.id(eltid);if(!node)return;else{var 
parent=node.parentNode;parent.removeChild(node)}}else _.id(eltid).style.display="none"};PXN8.fade.fadein=function(eltid)
{var _=PXN8.dom,self=PXN8.fade;try{if(self.i>=self.values.length)self.i=self.values.length-1;_.opacity(eltid,self.values
[self.i]);if(self.i>0){self.i--;setTimeout("PXN8.fade.fadein('"+eltid+"');",self.times[self.i])}}catch(e){alert(e.
message)}};PXN8.offsets=[];PXN8.addOperations=function(operations){var self=PXN8,cachedImage=self.getUncompressedImage()
;self.opNumber++;self.offsets[self.opNumber]=operations.length;var lastIndex=0;for(var i=0;i<self.opNumber;i++)lastIndex
+=self.offsets[i];for(var i=0;i<operations.length;i++)self.history[lastIndex+i]=operations[i];self.maxOpNumber=self.
opNumber;var script=PXN8.getScript();self.prepareForSubmit();self.ajax.submitScript(script,self.imageUpdateDone)};PXN8.
snapToAspectRatio=function(){var sel=PXN8.getSelection();if(PXN8.aspectRatio.width!=0){if(PXN8.aspectRatio.width>PXN8.
aspectRatio.height)sel.height=Math.round(sel.width/PXN8.aspectRatio.width*PXN8.aspectRatio.height);else sel.width=Math.
round(sel.height/PXN8.aspectRatio.height*PXN8.aspectRatio.width);PXN8.select(sel)}};PXN8.zoom={};PXN8.zoom.values=[0.25,
0.5,0.75,1.0,1.25,1.5,2,3,4];PXN8.zoom.index=3;PXN8.zoom.zoomedBy=PXN8.zoom.values[PXN8.zoom.index];PXN8.zoom.value=
function(){return PXN8.zoom.zoomedBy};PXN8.zoom.canZoomIn=function(){var self=PXN8.zoom;return self.zoomedBy<self.values
[self.values.length-1]};PXN8.zoom.canZoomOut=function(){var self=PXN8.zoom;return self.zoomedBy>self.values[0]};PXN8.
zoom.zoomByIndex=function(index){return PXN8.zoom.setIndex(index)};PXN8.zoom.shrinkToWidth=function(width){if(!PXN8.
ready){PXN8.listener.onceOnly(PXN8.ON_IMAGE_LOAD,function(){PXN8.zoom.shrinkToWidth(width)});return}var imgSize=PXN8.
getImageSize();if(imgSize.width>width)PXN8.zoom.toWidth(width)};PXN8.zoom.expandToWidth=function(width){if(!PXN8.ready){
PXN8.listener.onceOnly(PXN8.ON_IMAGE_LOAD,function(){PXN8.zoom.expandToWidth(width)});return}var imgSize=PXN8.
getImageSize();if(imgSize.width<width)PXN8.zoom.toWidth(width)};PXN8.zoom.expandToHeight=function(height){if(!PXN8.ready
){PXN8.listener.onceOnly(PXN8.ON_IMAGE_LOAD,function(){PXN8.zoom.expandToHeight(height)});return}var imgSize=PXN8.
getImageSize();if(imgSize.height<height)PXN8.zoom.toHeight(height)};PXN8.zoom.toWidth=function(width){if(!PXN8.ready){
PXN8.listener.onceOnly(PXN8.ON_IMAGE_LOAD,function(){PXN8.zoom.toWidth(width)});return}var imgSize=PXN8.getImageSize();
PXN8.zoom.setValue(width/imgSize.width)};PXN8.zoom.toHeight=function(height){if(!PXN8.ready){PXN8.listener.onceOnly(PXN8
.ON_IMAGE_LOAD,function(){PXN8.zoom.toHeight(height)});return}var imgSize=PXN8.getImageSize();PXN8.zoom.setValue(height/
imgSize.height)};PXN8.zoom.setIndex=function(i){var self=PXN8.zoom;self.index=i;return self.setValue(self.values[i])};
PXN8.zoom.setValue=function(magnification){if(!PXN8.ready){PXN8.listener.onceOnly(PXN8.ON_IMAGE_LOAD,function(){PXN8.
zoom.setValue(magnification)});return}var self=PXN8.zoom;self.zoomedBy=magnification;var theImg=document.getElementById(
"pxn8_image");theImg.width=PXN8.image.width*magnification;theImg.height=PXN8.image.height*magnification;PXN8.listener.
notify(PXN8.ON_ZOOM_CHANGE,magnification);return magnification};PXN8.zoom.zoomIn=function(){var self=PXN8.zoom;if(self.
canZoomIn()){for(var i=0;i<self.values.length;i++)if(self.values[i]>self.zoomedBy){self.setIndex(i);break}}else PXN8.
show.alert(PXN8.strings.NO_MORE_ZOOMIN,500);return false};PXN8.zoom.zoomOut=function(){var self=PXN8.zoom;if(self.
canZoomOut()){for(var i=self.values.length-1;i>=0;i--)if(self.values[i]<self.zoomedBy){self.setIndex(i);break}}else PXN8
.show.alert(PXN8.strings.NO_MORE_ZOOMOUT,500);return false};PXN8.zoom.toSize=function(width,height){if(!PXN8.ready){PXN8
.listener.onceOnly(PXN8.ON_IMAGE_LOAD,function(){PXN8.zoom.toSize(width,height)});return}var hr=width/PXN8.image.width,
vr=height/PXN8.image.height;PXN8.zoom.setValue(Math.min(vr,hr));return false};PXN8.zoom.zoomByValue=function(
magnification){PXN8.zoom.setValue(magnification)};PXN8.browser={};PXN8.browser.isIE6=function(){return window.navigator.
userAgent.indexOf("MSIE 6")>-1};PXN8.history=[];PXN8.responses=[];PXN8.images=[];PXN8.ready=false;PXN8.image={width:0,
height:0,location:""};PXN8.priv={};PXN8.priv.addImageToHistory=function(imageLocation){var item={location:imageLocation,
width:PXN8.image.width,height:PXN8.image.height};PXN8.images[PXN8.opNumber]=item;PXN8.updating=false};PXN8.priv.
createSelectionRect=function(){var _=PXN8.dom,selectRect=_.id("pxn8_select_rect");if(!selectRect){var canvas=_.id(
"pxn8_canvas");selectRect=_.ac(canvas,_.ce("div",{id:"pxn8_select_rect"}));selectRect.style.backgroundColor="white";_.
opacity(selectRect,0);selectRect.style.cursor="move";selectRect.style.borderWidth="1px";selectRect.style.borderColor=
"red";selectRect.style.borderStyle="dotted";selectRect.style.position="absolute";selectRect.style.zIndex=1;selectRect.
style.fontSize="0px";selectRect.style.display="block";selectRect.style.width="0px";selectRect.style.height="0px"}
selectRect.onmousedown=function(event){if(!event)event=window.event;PXN8.drag.begin(selectRect,event,PXN8.drag.
moveSelectionBoxHandler,PXN8.drag.upSelectionBoxHandler)};return selectRect};PXN8.when=function(condition,callback,
interval){if(!condition())setTimeout(function(){PXN8.when(condition,callback,interval)},interval);else callback()};PXN8.
whenReady=function(callback){var condition=function(){return PXN8.ready};PXN8.when(condition,callback,50)};PXN8.
imageLoadNotifier=function(){PXN8.when(function(){return PXN8.ready},function(){PXN8.listener.notify(PXN8.ON_IMAGE_LOAD)
},50)};PXN8.onCanvasMouseDown=function(event){if(!event)event=window.event;PXN8.drag.begin(document.getElementById(
"pxn8_canvas"),event,PXN8.drag.moveCanvasHandler,PXN8.drag.upCanvasHandler)};PXN8.initializeCanvas=function(){var _=PXN8
.dom,canvas=_.id("pxn8_canvas");canvas.onmousemove=function(event){if(!event)event=window.event;var cursorPos=_.
cursorPos(event),imagePoint=PXN8.mousePointToElementPoint(cursorPos.x,cursorPos.y);PXN8.position.x=imagePoint.x;PXN8.
position.y=imagePoint.y;PXN8.show.position();return true};canvas.onmouseout=function(event){if(!event)event=window.event
;PXN8.position.x="-";PXN8.position.y="-";PXN8.show.position()};canvas.onmousedown=PXN8.onCanvasMouseDown;canvas.ondrag=
function(){return false};var computedCanvasStyle=_.computedStyle("pxn8_canvas"),canvasPosition=null;if(
computedCanvasStyle.getPropertyValue)canvasPosition=computedCanvasStyle.getPropertyValue("position");else if(!
computedCanvasStyle.position)canvasPosition="static";else canvasPosition=computedCanvasStyle.position;if(!canvasPosition
||canvasPosition=="static"){canvas.style.position="relative";canvas.style.top="0px";canvas.style.left="0px"}var 
floatProperty="cssFloat";if(document.all)floatProperty="styleFloat";var floatValue=computedCanvasStyle[floatProperty];
if(!floatValue||floatValue=="none")canvas.style[floatProperty]="left";return canvas};PXN8.listener.add(PXN8.
ON_IMAGE_CHANGE,PXN8.show.zoom);PXN8.listener.add(PXN8.ON_ZOOM_CHANGE,PXN8.show.zoom)