var PXN8=PXN8||{};PXN8.crosshairs={};PXN8.crosshairs.enabled=true;PXN8.crosshairs.image=null;PXN8.crosshairs.setEnabled=
function(enabled){PXN8.crosshairs.enabled=enabled;PXN8.crosshairs.refresh()};PXN8.crosshairs.isEnabled=function(){
return PXN8.crosshairs.enabled};PXN8.crosshairs.getImage=function(){if(PXN8.crosshairs.image==null)PXN8.crosshairs.image
=PXN8.server+PXN8.root+"/images/pxn8_xhairs_white.gif";return PXN8.crosshairs.image};PXN8.crosshairs.setImage=function(
imageURL){PXN8.crosshairs.image=imageURL;PXN8.crosshairs.refresh()};PXN8.crosshairs.refresh=function(){var _=PXN8.dom,
xhairs=_.id("pxn8_crosshairs");if(!PXN8.crosshairs.isEnabled()){if(xhairs)xhairs.style.display="none";return}var sel=
PXN8.getSelection();_=PXN8.dom;var selBounds=_.eb("pxn8_select_rect"),canvas=_.id("pxn8_canvas");if(!xhairs)xhairs=_.ac(
canvas,_.ce("img",{id:"pxn8_crosshairs",src:PXN8.crosshairs.getImage()}));if(selBounds.width<=0){xhairs.style.display=
"none";return}var xhcx=xhairs.width/2,xhcy=xhairs.height/2;xhairs.style.display="inline";xhairs.style.position=
"absolute";xhairs.style.left=Math.floor(selBounds.x+selBounds.width/2-xhcx)+"px";xhairs.style.top=Math.floor(selBounds.y
+selBounds.height/2-xhcy)+"px"};PXN8.listener.add(PXN8.ON_SELECTION_CHANGE,PXN8.crosshairs.refresh);PXN8.listener.add(
PXN8.ON_ZOOM_CHANGE,PXN8.crosshairs.refresh)