var PXN8=PXN8||{};PXN8.dom={cachedComputedStyles:{}};PXN8.dom.cl=function(elt){if(typeof elt=="string")elt=PXN8.dom.id(
elt);if(!elt)return false;while(elt.firstChild)elt.removeChild(elt.firstChild);return elt};PXN8.dom.tx=function(str){
return document.createTextNode(str)};PXN8.dom.id=function(str){return document.getElementById(str)};PXN8.dom.ce=function
(nodeType,attrs){var el=document.createElement(nodeType);for(var i in attrs)el[i]=attrs[i];return el};PXN8.dom.css=
function(elt,attrs){var i;if(typeof elt=="string")elt=PXN8.dom.id(elt);for(i in attrs)elt.style[i]=attrs[i];return elt};
PXN8.dom.ac=function(parent,child){if(typeof parent=="string")parent=PXN8.dom.id(parent);if(typeof child=="string")child
=PXN8.dom.id(child);if(!parent||!child)return false;parent.appendChild(child);return child};PXN8.dom.eb=function(elt){
if(typeof elt=="string")elt=PXN8.dom.id(elt);if(!elt)return false;var x=null,y=null;if(elt.style.position=="absolute"){x
=parseInt(elt.style.left);y=parseInt(elt.style.top)}else{var pos=this.ep(elt);x=pos.x;y=pos.y}return{x:x,y:y,width:elt.
offsetWidth,height:elt.offsetHeight}};PXN8.dom.ep=function(elt){if(typeof elt=="string")elt=PXN8.dom.id(elt);if(!elt)
return false;var tmpElt=elt,posX=parseInt(tmpElt.offsetLeft),posY=parseInt(tmpElt.offsetTop);while(tmpElt.tagName.
toUpperCase()!="BODY"){tmpElt=tmpElt.offsetParent;if(tmpElt==null)break;posX+=parseInt(tmpElt.offsetLeft);posY+=parseInt
(tmpElt.offsetTop)}return{x:posX,y:posY}};PXN8.dom.windowSize=function(){if(document.all)return{width:document.body.
clientWidth,height:document.body.clientHeight};else return{width:window.outerWidth,height:window.outerHeight}};PXN8.dom.
opacity=function(elt,value){if(typeof elt=="string")elt=PXN8.dom.id(elt);if(!elt)return;if(document.all)elt.style.filter
="alpha(opacity:"+value*100+")";else{elt.style.opacity=value;elt.style._moz_opacity=value}};PXN8.dom.clz=function(
className){var links=document.getElementsByTagName("*"),result=[];for(var i=0;i<links.length;i++)if(links[i].className.
match(className))result.push(links[i]);return result};PXN8.dom.removeClass=function(elt,className){if(typeof elt==
"string")elt=PXN8.dom.id(elt);var oldClassname=elt.className,oldClasses=oldClassname.split(/ /),newClasses=[];for(var i=
0;i<oldClasses.length;i++)if(oldClasses[i]!=className)newClasses.push(oldClasses[i]);var newClassname=newClasses.join(
" ");elt.className=newClassname};PXN8.dom.addClass=function(elt,className){if(typeof elt=="string")elt=PXN8.dom.id(elt);
elt.className+=" "+className};PXN8.dom.isClass=function(elt,className){if(typeof elt=="string")elt=PXN8.dom.id(elt);var 
clzs=elt.className.split(/ /);for(var i=0;i<clzs.length;i++)if(clzs[i]==className)return true;return false};PXN8.dom.
computedStyle=function(elementId){var result=null;if(this.cachedComputedStyles[elementId])result=this.
cachedComputedStyles[elementId];else{var element=this.id(elementId);if(document.all)result=element.currentStyle;else if(
window.getComputedStyle)result=window.getComputedStyle(element,null);else result=element.style;if(result==null)result={}
;this.cachedComputedStyles[elementId]=result}return result};PXN8.dom.cursorPos=function(e){e=e||window.event;var cursor=
{x:0,y:0};if(e.pageX||e.pageY){cursor.x=e.pageX;cursor.y=e.pageY}else{var sl=document.documentElement.scrollLeft||
document.body.scrollLeft,st=document.documentElement.scrollTop||document.body.scrollTop;cursor.x=e.clientX+sl-document.
documentElement.clientLeft;cursor.y=e.clientY+st-document.documentElement.clientTop}return cursor};PXN8.dom.addLoadEvent
=function(func){var oldonload=window.onload;if(typeof window.onload!="function")window.onload=func;else window.onload=
function(){if(oldonload)oldonload();func()}};PXN8.dom.addClickEvent=function(elt,func){var oldonclick=elt.onclick;if(
typeof oldonclick!="function")elt.onclick=func;else elt.onclick=function(){if(oldonclick)oldonclick(elt);func(elt)}};
PXN8.dom.onceOnlyClickEvent=function(elt,func){var oldonclick=elt.onclick;PXN8.dom.addClickEvent(elt,function(){func();
elt.onclick=oldonclick})};PXN8.dom.table=function(rows,attributes){var dom=PXN8.dom,result=dom.ce("table",attributes),
tbody=dom.ce("tbody");result.appendChild(tbody);var mostCells=0;for(var i=0;i<rows.length;i++){var row=rows[i];if(row.
length>mostCells)mostCells=row.length}for(var i=0;i<rows.length;i++){var tr=dom.ce("tr");tbody.appendChild(tr);var 
rowData=rows[i],cellsInRow=rowData.length;for(var j=0;j<rows[i].length;j++){var cellData=rows[i][j],td=dom.ce("td");tr.
appendChild(td);if(j==rows[i].length-1&&cellsInRow<mostCells)td.colSpan=mostCells-cellsInRow+1;if(typeof cellData==
"string")td.appendChild(dom.tx(cellData));else if(PXN8.isArray(cellData))for(var k=0;k<cellData.length;k++)if(typeof 
cellData[k]=="string")td.appendChild(dom.tx(cellData[k]));else td.appendChild(cellData[k]);else td.appendChild(cellData)
}}return result};PXN8.dom.createFlag=function(x,y,id){var flag=PXN8.dom.ce("img",{src:PXN8.root+"images/icons/flag.gif",
id:id});document.body.appendChild(flag);flag.style.position="absolute";flag.style.top=y-16+"px";flag.style.left=x-11+
"px";return flag}