var PXN8=PXN8||{};PXN8.save={};PXN8.save.toDisk=function(){var uncompressedImage=PXN8.getUncompressedImage();if(
uncompressedImage){var newURL=PXN8.server+PXN8.root+"/save.pl?";if(typeof pxn8_original_filename=="string")newURL+=
"originalFilename="+pxn8_original_filename+"&";newURL+="image="+uncompressedImage;document.location=newURL}else{document
.location="#";PXN8.show.alert("You have not changed the image !")}};PXN8.save.toServer=function(){var 
relativeFilePathToUncompressedImage=PXN8.getUncompressedImage();if(typeof pxn8_save_image=="function")
return pxn8_save_image(relativeFilePathToUncompressedImage);else{alert("This feature is not available by default.\n"+
"To enable this feature you must create a PHP,ASP or JSP page to save the image to your own server.\n"+
"You must also create a javascript function called 'pxn8_save_image()' - it's first parameter is the URL of the changed image.\n"
+"The path to the changed image (relative to the directory where PXN8 is installed) is "+PXN8.getUncompressedImage());
return false}}