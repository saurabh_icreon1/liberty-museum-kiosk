var PXN8=PXN8||{};PXN8.tools={};PXN8.tools.history=function(offset){if(offset==0)return;if(PXN8.isUpdating()){alert(PXN8
.strings.IMAGE_UPDATING);return}var theImage=PXN8.dom.id("pxn8_image");if(!theImage){alert(
"Please wait for the image to load");return}if(!offset)offset=-1;if(PXN8.opNumber==0&&offset<0){PXN8.show.alert(PXN8.
strings.NO_MORE_UNDO);return}if(PXN8.opNumber==PXN8.maxOpNumber&&offset>0){PXN8.show.alert(PXN8.strings.NO_MORE_REDO);
return}if(offset<0){var userOp=PXN8.getUserOperation(PXN8.opNumber);PXN8.show.alert("- "+userOp.operation,500)}else{var 
userOp=PXN8.getUserOperation(PXN8.opNumber+1);PXN8.show.alert("+ "+userOp.operation,500)}var index=PXN8.opNumber+offset,
currentImageData=PXN8.images[index];if(!currentImageData){alert("Error! PXN8.images["+index+"] is undefined");
return false}PXN8.opNumber=index;PXN8.image.location=currentImageData.location;PXN8.image.width=currentImageData.width;
PXN8.image.height=currentImageData.height;PXN8.listener.notify(PXN8.BEFORE_IMAGE_CHANGE,null);PXN8.unselect();PXN8.
replaceImage(PXN8.image.location);return false};PXN8.tools.undo=function(){PXN8.tools.history(-1);return false};PXN8.
tools.redo=function(){PXN8.tools.history(1);return false};PXN8.tools.undoall=function(){PXN8.tools.history(0-PXN8.
opNumber);return false};PXN8.tools.redoall=function(){PXN8.tools.history(PXN8.maxOpNumber-PXN8.opNumber);return false};
PXN8.tools.updateImage=function(ops){var theImage=PXN8.dom.id("pxn8_image");if(!PXN8.ready){alert(
"Please wait for the image to load");return}if(PXN8.isUpdating()){alert(PXN8.strings.IMAGE_UPDATING);return}PXN8.
listener.notify(PXN8.BEFORE_IMAGE_CHANGE,ops);PXN8.addOperations(ops)};PXN8.tools.startTransaction=function(){PXN8.tools
.transactionCache=[];PXN8.tools.commit=PXN8.tools.updateImage;PXN8.tools.updateImage=PXN8.tools.cacheUpdates};PXN8.tools
.cacheUpdates=function(ops){for(var i=0;i<ops.length;i++)PXN8.tools.transactionCache.push(ops[i])};PXN8.tools.
endTransaction=function(){PXN8.tools.updateImage=PXN8.tools.commit;PXN8.tools.updateImage(PXN8.tools.transactionCache)};
PXN8.tools.enhance=function(){PXN8.tools.updateImage([{operation:"enhance"}])};PXN8.tools.normalize=function(){PXN8.
tools.updateImage([{operation:"normalize"}])};PXN8.tools.instantFix=function(){PXN8.tools.updateImage([{operation:
"normalize"},{operation:"enhance"}])};PXN8.tools.spiritlevel=function(x1,y1,x2,y2){var opposite=y1>y2?y1-y2:y2-y1,
adjacent=x1>x2?x1-x2:x2-x1,hypotenuse=Math.sqrt(opposite*opposite+adjacent*adjacent),sineratio=opposite/hypotenuse,
RAD2DEG=57.2957795,rads=Math.atan2(sineratio,Math.sqrt(1-sineratio*sineratio)),degrees=rads*RAD2DEG;if(y1<y2)degrees=360
-degrees;PXN8.tools.rotate({angle:degrees});return degrees};PXN8.tools.spiritlevel_mode={};PXN8.tools.spiritlevel_mode.
clicks=[];PXN8.tools.spiritlevel_mode.callback=null;PXN8.tools.spiritlevel_mode.start=function(callback){var _=PXN8.dom,
img=_.id("pxn8_image"),iw=img.width/PXN8.zoom.value(),ih=img.height/PXN8.zoom.value(),sel=_.id("pxn8_select_rect");sel.
style.cursor="pointer";PXN8.crosshairs.setEnabled(false);PXN8.resize.enable(["n","s","e","w","ne","se","nw","sw"],false)
;PXN8.select({top:0,left:0,width:iw/2,height:ih});PXN8.event.addListener(sel,"click",this.onclick);this.callback=
callback};PXN8.tools.spiritlevel_mode.end=function(){var _=PXN8.dom,pin1=_.id("pxn8_flag_0");if(pin1)document.body.
removeChild(pin1);var pin2=_.id("pxn8_flag_1");if(pin2)document.body.removeChild(pin2);PXN8.unselect();var sel=_.id(
"pxn8_select_rect");sel.style.cursor="move";PXN8.crosshairs.setEnabled(true);PXN8.resize.enable(["n","s","e","w","ne",
"se","nw","sw"],true);PXN8.event.removeListener(sel,"click",this.onclick);this.clicks=[]};PXN8.tools.spiritlevel_mode.
onclick=function(event){var _=PXN8.dom,self=PXN8.tools.spiritlevel_mode,img=_.id("pxn8_image"),iw=img.width/PXN8.zoom.
value(),ih=img.height/PXN8.zoom.value(),sel=_.id("pxn8_select_rect"),pos=_.cursorPos(event),flag=PXN8.dom.createFlag(pos
.x,pos.y,"pxn8_flag_"+self.clicks.length);self.clicks.push(pos);PXN8.select({top:0,left:iw/2,width:iw/2,height:ih});if(
self.clicks.length==2){PXN8.event.removeListener(sel,"click",self.onclick);PXN8.tools.spiritlevel(self.clicks[0].x,self.
clicks[0].y,self.clicks[1].x,self.clicks[1].y);var oldSize=PXN8.dom.eb("pxn8_image");PXN8.listener.onceOnly(PXN8.
ON_IMAGE_CHANGE,function(){self.end();var newSize=PXN8.dom.id("pxn8_image"),hdiff=newSize.height-oldSize.height,wdiff=
newSize.width-oldSize.width;PXN8.select(wdiff,hdiff,oldSize.width-wdiff,oldSize.height-hdiff);self.callback()})}};PXN8.
tools.rotate=function(params){var operation={operation:"rotate",angle:0,fliphz:false,flipvt:false};if(params)for(var i 
in params)operation[i]=params[i];if(!params)operation.angle=90;if(operation.angle>0||operation.flipvt||operation.fliphz)
PXN8.tools.updateImage([operation])};PXN8.tools.blur=function(params){params.operation="blur";PXN8.tools.updateImage([
params])};PXN8.tools.colors=function(param){if(!param.saturation)param.saturation=100;if(!param.brightness)param.
brightness=100;if(!param.hue)param.hue=100;if(!param.contrast)param.contrast=0;param.operation="colors";PXN8.tools.
updateImage([param])};PXN8.tools.crop=function(params){if(!params)params=PXN8.getSelection();var operation={operation:
"crop"};for(var i in params)operation[i]=params[i];if(operation.width<=0||operation.height<=0){PXN8.show.alert(PXN8.
strings.CROP_SELECT_AREA);return}PXN8.tools.updateImage([operation])};PXN8.tools.previewCrop=function(timeout){timeout=
timeout||3500;var preview=function(borderColor,borderOpacity,handleOpacity){var _=PXN8.dom,rects=["left","right","top",
"bottom","topleft","topright","bottomleft","bottomright"];for(var i=0;i<rects.length;i++){var rect=_.id("pxn8_"+rects[i]
+"_rect");rect.style.backgroundColor=borderColor;_.opacity(rect,borderOpacity)}for(var i in PXN8.resize.handles)if(
typeof PXN8.resize.handles[i]!="function"){var handle=_.id(i+"_handle");_.opacity(handle,handleOpacity)}};preview(
"white",1.00,0);setTimeout(function(){var _=PXN8.style.notSelected;preview(_.color,_.opacity,1.00)},timeout)};PXN8.tools
.preview_crop=PXN8.tools.previewCrop;PXN8.tools.filter=function(params){params.color=params.color;params.operation=
"filter";PXN8.tools.updateImage([params])};PXN8.tools.interlace=function(params){params.color=params.color;params.
operation="interlace";PXN8.tools.updateImage([params])};PXN8.tools.lomo=function(params){params.operation="lomo";PXN8.
tools.updateImage([params])};PXN8.tools.fillFlash=function(opacity){var operation={operation:"fill_flash"};if(opacity)
operation.opacity=Math.max(0,Math.min(100,opacity));else operation.opacity=50;PXN8.tools.updateImage([operation])};PXN8.
tools.fill_flash=PXN8.tools.fillFlash;PXN8.tools.snow=function(){PXN8.tools.overlay({filepath:
"images/overlays/snowflakes.png",tile:"true"})};PXN8.tools.overlay=function(params){params.operation="overlay";PXN8.
tools.updateImage([params])};PXN8.tools.addText=function(params){params.operation="add_text";params.fill=params.fill;
params.text=params.text.replace(/\"/g,'\\"');PXN8.tools.updateImage([params])};PXN8.tools.add_text=PXN8.tools.addText;
PXN8.tools.whiten=function(params){if(!params)params=PXN8.getSelection();if(params.width==0||params.height==0)return;
params.operation="whiten";PXN8.tools.updateImage([params])};PXN8.tools.fixredeye=function(params){if(!params)params=PXN8
.getSelection();if(PXN8.isArray(params)){for(var i=0;i<params.length;i++)params[i].operation="fixredeye";PXN8.tools.
updateImage(params)}else{params.operation="fixredeye";PXN8.tools.updateImage([params])}};PXN8.tools.resize=function(
width,height){PXN8.tools.updateImage([{operation:"resize",width:width,height:height}])};PXN8.tools.roundedCorners=
function(color,radius){PXN8.tools.updateImage([{operation:"roundcorners",color:color,radius:radius}])};PXN8.tools.
roundedcorners=PXN8.tools.roundedCorners;PXN8.tools.sepia=function(color){PXN8.tools.updateImage([{operation:"sepia",
color:color}])};PXN8.tools.grayscale=function(){PXN8.tools.updateImage([{operation:"grayscale"}])};PXN8.tools.charcoal=
function(radius){PXN8.tools.updateImage([{operation:"charcoal",radius:radius}])};PXN8.tools.oilpaint=function(radius){
PXN8.tools.updateImage([{operation:"oilpaint",radius:radius}])};PXN8.tools.unsharpmask=function(params){var operation={
operation:"unsharpmask"};if(params)for(var i in params)operation[i]=params[i];PXN8.tools.updateImage([operation])};PXN8.
tools.fetch=function(params){var operation={operation:"fetch"};if(params)for(var i in params)operation[i]=params[i];PXN8
.tools.updateImage([operation])};PXN8.tools.hi_res=function(hires_params,callback){var script=PXN8.getScript(),fetchOp=
script[0],hires_script=[fetchOp],proxyOp={operation:"proxy"};for(var i in fetchOp)if(i!="operation")proxyOp[i]=fetchOp[i
];proxyOp.timestamp=new Date().getTime();hires_script.push(proxyOp);for(var i in hires_params)fetchOp[i]=hires_params[i]
;for(var i=1;i<script.length;i++)hires_script.push(script[i]);PXN8.prepareForSubmit(PXN8.strings.SAVING_HI_RES);PXN8.
ajax.submitScript(hires_script,function(jsonResponse){var timer=document.getElementById("pxn8_timer");if(timer)timer.
style.display="none";PXN8.updating=false;callback(jsonResponse)})};PXN8.tools.mask=function(params){var operation={
operation:"mask"};if(params)for(var i in params)operation[i]=params[i];PXN8.tools.updateImage([operation])};PXN8.tools.
rearrange=function(params){var operation={operation:"rearrange"};if(!params){alert(PXN8.strings.INVALID_PARAMETER+
" (null) ");return}operation.pieces=params;PXN8.tools.updateImage([operation])};PXN8.tools.transparent=function(params){
params.__extension=".png";params.operation="transparent";PXN8.tools.updateImage([params])}