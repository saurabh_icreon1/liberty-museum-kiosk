var PXN8=PXN8||{};PXN8.ImageMagick=function(){var id=PXN8.ImageMagick.objectCount++;if(id==0)this.handle="_";else this.
handle="im"+id;return this};PXN8.ImageMagick.curry=function(methodName){return function(a){var statement=[];statement.
push(this.handle);statement.push(methodName);for(var i=0;i<arguments.length;i++)statement.push(arguments[i]);PXN8.
ImageMagick.statements.push(statement)}};PXN8.ImageMagick.prototype.Method=function(){var methodName=arguments[0];if(!
PXN8.ImageMagick.prototype[methodName])PXN8.ImageMagick.prototype[methodName]=PXN8.ImageMagick.curry(methodName);var 
statement=[];statement.push(this.handle);for(var i=0;i<arguments.length;i++)statement.push(arguments[i]);PXN8.
ImageMagick.statements.push(statement)};PXN8.ImageMagick.prototype.Crop=function(coords){var _coords={x:coords.left||
coords.x,y:coords.top||coords.y,width:coords.width,height:coords.height};this.Method("Crop",_coords)};PXN8.ImageMagick.
prototype._CropToText=function(textParams){this.Method("_CropToText",textParams)};PXN8.ImageMagick.prototype._ScaleTo=
function(scaleParams){this.Method("_ScaleTo",scaleParams)};PXN8.ImageMagick.prototype.Clone=function(){var clone=new 
PXN8.ImageMagick(),statement=[];statement.push(clone.handle);statement.push("clone_from");statement.push(this.handle);
PXN8.ImageMagick.statements.push(statement);return clone};PXN8.ImageMagick.prototype.Append=function(stack){var appended
=new PXN8.ImageMagick(),statement=[];statement.push(appended.handle);statement.push("append");statement.push([this.
handle,stack]);PXN8.ImageMagick.statements.push(statement);return appended};PXN8.ImageMagick.prototype.push=function(
image){var statement=[];statement.push(this.handle);statement.push("push");statement.push(image.handle);PXN8.ImageMagick
.statements.push(statement);return this};PXN8.ImageMagick.start=function(){PXN8.ImageMagick.objectCount=0;var image=new 
PXN8.ImageMagick();PXN8.ImageMagick.statements=[];return image};PXN8.ImageMagick.end=function(image,callback){var op={
operation:"imagemagick"};op.script=PXN8.ImageMagick.statements;op.script.push([image.handle,"return"]);if(typeof 
callback=="undefined")PXN8.tools.updateImage([op]);else if(typeof callback=="boolean"){if(callback==true)PXN8.tools.
updateImage([op])}else if(typeof callback=="function")PXN8.ajax.submitScript([op],callback);return op};PXN8.ImageMagick.
maskFromPaths=function(size,paths){var result=new PXN8.ImageMagick();result.Set({size:size.width+"x"+size.height});
result.Read("xc:#000000");for(var i=0;i<paths.length;i++){var path=paths[i],ps=PXN8.freehand.getPoints(path);result.Draw
({stroke:"#ffffff",fill:"#00000000",primitive:"path",points:ps,strokewidth:path.width})}result.Set({matte:false});
return result};PXN8.ImageMagick.methods=["AdaptiveBlur","AdaptivelyResize","AdaptiveSharpen","AdaptiveThreshold",
"AddNoise","AffineTransform","Annotate","AutoOrient","Blur","Border","BlackThreshold","Charcoal","Colorize","Contrast",
"Composite","Draw","Edge","Emboss","Enhance","Extent","Flip","Flop","Frame","Gamma","GaussianBlur","Implode","Label",
"Level","Magnify","Mask","Minify","Modulate","MotionBlur","Negate","Normalize","OilPaint","Opaque","Posterize",
"Quantize","Raise","ReduceNoise","Resample","Read",,"Rotate","Resize","Roll","Set","SetPixel","Shadow","Sharpen","Shear"
,"Sketch","Solarize","Spread","Stegano","Stereo","Strip","Swirl","Texture","Thumbnail","Threshold","Tint","Transparent",
"Transpose","Transverse","Trim","UnsharpMask","Vignette","Wave","WhiteThreshold"];for(var i=0;i<PXN8.ImageMagick.methods
.length;i++){var method=PXN8.ImageMagick.methods[i];PXN8.ImageMagick.prototype[method]=PXN8.ImageMagick.curry(method)}