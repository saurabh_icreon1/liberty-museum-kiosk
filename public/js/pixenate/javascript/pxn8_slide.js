var PXN8=PXN8||{};PXN8.slide={};PXN8.slide.bind=function(slideElement,inputElementId,startRange,rangeSize,initValue,
increment,optionalCallback){if(!increment)increment=1;if(typeof slideElement=="string")slideElement=PXN8.dom.id(
slideElement);slideElement.className="pxn8_slide";slideElement.onmousedown=function(event){if(!event)event=window.event;
PXN8.slide.onmousedown(slideElement,event,inputElementId,startRange,rangeSize,increment)};var slider=document.
createElement("span");slider.className="pxn8_slider";slideElement.appendChild(slider);PXN8.slide.refresh_slider(slider,
initValue,startRange,rangeSize);var inputElement=PXN8.dom.id(inputElementId);if(!inputElement){alert(
'ERROR: NO <input/> element was found with id="'+inputElementId+'"');return false}inputElement.value=initValue;
inputElement.onblur=function(){if(isNaN(this.value))this.value=startRange;if(this.value>startRange+rangeSize)this.value=
startRange+rangeSize;if(this.value<startRange)this.value=startRange;PXN8.slide.refresh_slider(slider,this.value,
startRange,rangeSize)};if(typeof optionalCallback=="function"){PXN8.event.addListener(slideElement,"mouseup",
optionalCallback);PXN8.event.addListener(inputElement,"change",optionalCallback)}};PXN8.slide.refresh_slider=function(
slider,value,startRange,rangeSize){slider.style.left=3+(value-startRange)/rangeSize*117+"px"};PXN8.slide.onmousedown=
function(slide,event,inputId,start,size,increment){var kids=slide.getElementsByTagName("*"),slider=undefined;for(var i=0
;i<kids.length;i++)if(kids[i].className=="pxn8_slider"){slider=kids[i];break}slider.onmousemove=null;var inputElement=
document.getElementById(inputId);slide.onmousemove=function(evt){return PXN8.slide.update(slider,inputElement,slide,evt,
start,size,increment)};slide.onmouseup=function(){slide.onmousemove=null};PXN8.slide.update(slider,inputElement,slide,
event,start,size,increment)};PXN8.slide.update=function(slider,inputElement,slide,evt,start,size,increment){evt=evt?evt:
window.event;var px=PXN8.slide.position(slide),nx=evt.clientX-px;if(nx<=120&&nx>=3){var iv=start+(nx-3)/117*size;iv=iv-
iv%increment;PXN8.slide.refresh_slider(slider,iv,start,size);inputElement.value=Math.round(iv,2)}};PXN8.slide.position=
function(obj){var curleft=0;if(obj.offsetParent)while(obj.offsetParent){curleft+=obj.offsetLeft;obj=obj.offsetParent}
else if(obj.x)curleft+=obj.x;return curleft}