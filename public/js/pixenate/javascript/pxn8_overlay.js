var PXN8=PXN8||{};PXN8.overlay={};PXN8.overlay.start=function(overlayURL,dimensions){PXN8.overlay.stop();if(dimensions==
undefined)dimensions=PXN8.getSelection();var rects=["top","bottom","left","right","topright","topleft","bottomleft",
"bottomright"];for(var i=0;i<rects.length;i++)PXN8.dom.opacity("pxn8_"+rects[i]+"_rect",0);var img=document.
getElementById("pxn8_image"),iw=img.width/PXN8.zoom.value(),ih=img.height/PXN8.zoom.value();if(dimensions.top==undefined
&&dimensions.left==undefined){dimensions.top=ih/2-dimensions.height/2;dimensions.left=iw/2-dimensions.width/2}PXN8.
select(dimensions);var canvas=PXN8.dom.id("pxn8_canvas"),overlayEl=PXN8.dom.id("pxn8_overlay");if(overlayURL.indexOf(
".png")>-1&&PXN8.browser.isIE6()){canvas.removeChild(overlayEl);overlayEl=null}if(overlayEl==null){overlayEl=PXN8.dom.ce
("img",{id:"pxn8_overlay"});overlayEl.style.position="absolute";canvas.appendChild(overlayEl)}if(overlayURL.indexOf(
".png")>-1&&PXN8.browser.isIE6())overlayEl.onload=function(){PXN8.overlay.fixPNG(overlayEl)};overlayEl.src=overlayURL;
PXN8.overlay.show();PXN8.listener.add(PXN8.ON_SELECTION_CHANGE,PXN8.overlay.show);PXN8.listener.add(PXN8.ON_ZOOM_CHANGE,
PXN8.overlay.show);PXN8.crosshairs.setEnabled(false)};PXN8.overlay.stop=function(){var rects=["top","bottom","left",
"right","topright","topleft","bottomleft","bottomright"];for(var i=0;i<rects.length;i++)PXN8.dom.opacity("pxn8_"+rects[i
]+"_rect",PXN8.style.notSelected.opacity);PXN8.listener.remove(PXN8.ON_SELECTION_CHANGE,PXN8.overlay.show);PXN8.listener
.remove(PXN8.ON_ZOOM_CHANGE,PXN8.overlay.show);var overlay=document.getElementById("pxn8_overlay");if(overlay)overlay.
style.display="none";PXN8.selectByRatio("free");PXN8.crosshairs.setEnabled(true)};PXN8.overlay.show=function(){var 
overlay=document.getElementById("pxn8_overlay"),sel=PXN8.getSelection(),zoom=PXN8.zoom.value();overlay.style.display=
"block";overlay.style.top=sel.top*zoom+"px";overlay.style.left=sel.left*zoom+"px";overlay.width=sel.width*zoom;overlay.
height=sel.height*zoom;overlay.style.width=sel.width*zoom+"px";overlay.style.height=sel.height*zoom+"px"};PXN8.overlay.
fixPNG=function(img){if(!PXN8.browser.isIE6())return;if(img.src.indexOf(".png")==-1)return;var imgID=img.id?"id='"+img.
id+"' ":"",imgClass=img.className?"class='"+img.className+"' ":"",imgTitle=img.title?"title='"+img.title+"' ":"title='"+
img.alt+"' ",imgStyle="display:inline-block;"+img.style.cssText;if(img.align=="left")imgStyle="float:left;"+imgStyle;if(
img.align=="right")imgStyle="float:right;"+imgStyle;if(img.parentElement.href)imgStyle="cursor:hand;"+imgStyle;var 
strNewHTML="<span "+imgID+imgClass+imgTitle+' style="'+"width:"+img.width+"px; height:"+img.height+"px;"+imgStyle+";";
strNewHTML=strNewHTML+"filter:progid:DXImageTransform.Microsoft.AlphaImageLoader";strNewHTML=strNewHTML+"(src='"+img.src
+"', sizingMethod='scale');\"></span>";img.outerHTML=strNewHTML}