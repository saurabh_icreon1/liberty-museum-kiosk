var PXN8=PXN8||{};PXN8.ajax={};PXN8.ajax.useXHR=true;PXN8.ajax.createRequest=function(){if(typeof XMLHttpRequest!=
"undefined")return new XMLHttpRequest();try{return new ActiveXObject("Msxml2.XMLHTTP")}catch(e){try{return new 
ActiveXObject("Microsoft.XMLHTTP")}catch(e){}}return false};PXN8.ajax.submitScript=function(script,callback){if(PXN8.log
){PXN8.log.trace("PXN8.ajax.submitScript() ----- START -----");var s=PXN8.objectToString(script);PXN8.log.trace(s);PXN8.
log.trace("PXN8.ajax.submitScript() ------ END ------")}var path,suffix,i,op,scriptToText,cachedJSON,urlWithoutScript,
url,scriptSizeLimit,bite,c;if(typeof PXN8.convertToJPEG=="boolean"&&PXN8.convertToJPEG==false){suffix=".jpg";for(i=0;i<
script.length;i++){op=script[i];if(op.operation=="fetch"){path=op.url||op.filepath;c=path.indexOf("?");if(c==-1)c=path.
length;suffix=path.substring(path.lastIndexOf("."),c);break}if(op.operation=="cache"){path=op.image;c=path.indexOf("?");
if(c==-1)c=path.length;suffix=path.substring(path.lastIndexOf("."),c);break}}for(i=0;i<script.length;i++){op=script[i];
if(!op.__extension)op.__extension=suffix;else suffix=op.__extension}}suffix="";for(i=0;i<script.length;i++){op=script[i]
;if(op.__extension)suffix=op.__extension;else if(suffix!="")op.__extension=suffix}script=PXN8.optimizeScript(script);
scriptToText=escape(PXN8.objectToString(script));cachedJSON=PXN8.json.getResponseForScript(scriptToText);if(cachedJSON){
callback(cachedJSON);return}if(PXN8.ajax.useXHR==false){urlWithoutScript=PXN8.server+PXN8.root+"/"+PXN8.basename+
"?callback=pxn8cb&script=";url=urlWithoutScript+scriptToText;scriptSizeLimit=2048-urlWithoutScript.length;if(
scriptToText.length>scriptSizeLimit){bite="[";c=0;i=0;for(;i<script.length;i++){var optext=escape(PXN8.objectToString(
script[i]));if(bite.length+optext.length<scriptSizeLimit){bite=bite+(i>0?",":"");bite=bite+optext}else break}bite=bite+
"]";var remainder=script.slice(i);pxn8cb=function(json){var script=[{operation:"cache",image:json.image}];for(var i=0;i<
remainder.length;i++)script.push(remainder[i]);PXN8.ajax.submitScript(script,callback)};url=urlWithoutScript+bite}else 
pxn8cb=function(json){PXN8.json.setResponseForScript(scriptToText,json);callback(json)};var insertScriptTag=function(){
var scriptTag=document.createElement("script");scriptTag.setAttribute("type","text/javascript");scriptTag.setAttribute(
"src",url);document.getElementsByTagName("head")[0].appendChild(scriptTag)};if(PXN8.browser.isIE6())setTimeout(
insertScriptTag,50);else insertScriptTag()}else{var req=PXN8.ajax.createRequest(),onJSONerror=function(r){alert(unescape
(PXN8.strings.WEB_SERVER_ERROR)+"\n"+r.statusText+"\n"+r.responseText);var timer=document.getElementById("pxn8_timer");
if(timer)timer.style.display="none";PXN8.updating=false};PXN8.json.bindScriptToResponse(req,callback,scriptToText,
onJSONerror);req.open("POST",PXN8.root+"/"+PXN8.basename,true);req.setRequestHeader("Content-Type",
"application/x-www-form-urlencoded");var submission="script="+scriptToText;req.send(submission)}};PXN8.optimizeScript=
function(script){var self=PXN8;for(var i=0;i<self.optimizations.length;i++){var optimize=self.optimizations[i];script=
optimize(script)}return script};PXN8.optimizations=[(function(script){var result=[];for(var i=0;i<script.length;i++){var
 op=script[i],nextop=false;if(i+1<script.length)nextop=script[i+1];if(nextop&&op.operation=="resize"&&nextop.operation==
"resize"){}else result.push(op)}return result}),(function(script){var result=[];for(var i=0;i<script.length;i++){var op=
script[i],nextop=false;if(i+1<script.length)nextop=script[i+1];if(nextop&&nextop.operation=="normalize"&&op.operation==
"normalize"){}else result.push(op)}return result}),(function(script){var result=[],colorsOp=null;for(var i=0;i<script.
length;i++){var op=script[i];if(op.operation!="colors"){if(colorsOp!=null)result.push(colorsOp);result.push(op);colorsOp
=null}else if(colorsOp!=null){colorsOp.saturation=colorsOp.saturation/100*(op.saturation/100)*100;colorsOp.brightness=
colorsOp.brightness/100*(op.brightness/100)*100;colorsOp.hue=colorsOp.hue/100*(op.hue/100)*100;colorsOp.contrast=
colorsOp.contrast+op.contrast}else colorsOp=op}if(colorsOp!=null)result.push(colorsOp);return result}),(function(script)
{var result=[];for(var i=0;i<script.length;i++){var op=script[i],nextop=false;if(i+1<script.length)nextop=script[i+1];
if(nextop&&nextop.operation=="rotate"&&op.operation=="rotate"){var flipping=op.flipvt||op.fliphz||nextop.flipvt||nextop.
fliphz;if(!flipping)nextop.angle=(op.angle+nextop.angle)%360;else if(op.angle==0&&nextop.angle==0&&(op.flipvt==nextop.
flipvt&&op.fliphz==nextop.fliphz))i+=1;else result.push(op)}else if(op.operation=="rotate"){var flipping=op.flipvt||op.
fliphz||nextop.flipvt||nextop.fliphz;if(!flipping&&op.angle==0){}else result.push(op)}else result.push(op)}return result
})];PXN8.json=(function(){var that={},scriptCache={};function getResponseForScript(script){return scriptCache[script]}
function setResponseForScript(script,json){scriptCache[script]=json}function bindScriptToResponse(request,callback,
scriptAsString,onerror){request.onreadystatechange=function(){if(request.readyState==4)if(request.status==200){var json;
try{json=eval("("+request.responseText+")");setResponseForScript(scriptAsString,json)}catch(e){alert(
"An exception occured tring to evaluate server response:\n"+request.responseText+"\nException: "+e);var wnd=window.open(
"","","height=200, width=640, scrollbars=1, resizable=1, location=0, menubar=0, toolbar=0"),doc=wnd.document;doc.open(
"text/plain");doc.writeln("If contacting the site administrator, please copy and paste the following...");doc.writeln(
"============================================================================");doc.writeln("An exception ("+e+
") occured trying to evaluate server response:");doc.writeln(request.responseText);doc.close();return}callback(json)}
else if(onerror)onerror(request);else alert(unescape(PXN8.strings.WEB_SERVER_ERROR)+"\n"+request.statusText+"\n"+request
.responseText)}}that.bindScriptToResponse=bindScriptToResponse;that.getResponseForScript=getResponseForScript;that.
setResponseForScript=setResponseForScript;return that})()