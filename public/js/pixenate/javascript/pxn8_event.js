var PXN8=PXN8||{};PXN8.event={};PXN8.event._added=[];PXN8.event.addListener=function(el,eventstr,func){if(typeof el==
"string")el=PXN8.dom.id(el);if(el.addEventListener)el.addEventListener(eventstr,func,true);else if(el.attachEvent)el.
attachEvent("on"+eventstr,func);var record={element:el,event:eventstr,listener:func};PXN8.event._added.push(record);
return func};PXN8.event.removeListener=function(el,eventstr,func){if(typeof el=="string")el=PXN8.dom.id(el);if(func)PXN8
.event._removeListener(el,eventstr,func);else{var original=PXN8.event._added,removed=false;for(var i=0;i<original.length
;i++){var record=original[i];if(record)if(record.eventstr==eventstr&&record.element==el){PXN8.event._removeListener(el,
eventstr,record.listener);original[i]=null;removed=true}}if(!removed)return;var temp=[];for(var i=0;i<original.length;
i++)if(original[i]!=null)temp.push(original[i]);PXN8.event._added=temp}};PXN8.event._removeListener=function(el,eventstr
,func){if(typeof el=="string")el=PXN8.dom.id(el);if(el.removeEventListener)el.removeEventListener(eventstr,func,true);
else if(el.detachEvent)el.detachEvent("on"+eventstr,func)};PXN8.event.closure=function(object,func){return function(
event){event=event||window.event;var source=window.event?event.srcElement:event.target;func(event,object,source,
arguments.callee)}};PXN8.event.normalize=function(func){return function(event){event=event||window.event;var source=
window.event?event.srcElement:event.target;func(event,source,arguments.callee)}};PXN8.behaviour={bind:function(className
,behaviourObject){var elements=PXN8.dom.clz(className);for(var i=0;i<elements.length;i++)for(var j in behaviourObject)
PXN8.event.addListener(elements[i],j,behaviourObject[j])}}