PXN8=PXN8||{};PXN8.preview={};PXN8.preview._div_to_intervalid={};PXN8.preview.initialize=function(elementId,
preview_method){if(!preview_method)preview_method="crop";if(preview_method=="crop"||preview_method=="resize"){}else{
alert("Invalid preview method provided - use either 'crop' or 'resize'");return}try{document.execCommand(
"BackgroundImageCache",false,true)}catch(e){}PXN8.event.removeListener(elementId,"mousedown");PXN8.event.removeListener(
elementId,"mouseup");PXN8.event.removeListener(elementId,"mousemove");var oldIntervalId=PXN8.preview._div_to_intervalid[
elementId];if(oldIntervalId)clearInterval(oldIntervalId);var result={},element=PXN8.dom.id(elementId);if(!element)
return false;result._preview_method=preview_method;result._element=element;result._sizeX=parseInt(element.style.width);
result._sizeY=parseInt(element.style.height);result._offset={x:0,y:0};result._op=false;result._opQueue=[];result.
_intervalId=0;result._beginDrag={x:0,y:0};result._endDrag={x:0,y:0};result._mouseDownCoords={};result._mouseUpCoords={};
result._mouseDown=PXN8.event.closure(result,PXN8.preview._innerMouseDown);result._mouseUp=PXN8.event.closure(result,PXN8
.preview._innerMouseUp);result._mouseMove=PXN8.event.closure(result,PXN8.preview._innerMouseMove);var img=PXN8.dom.id(
"pxn8_image");if(img){var real_image_width=img.width/PXN8.zoom.value(),real_image_height=img.height/PXN8.zoom.value();
result._fullsize_ratio=Math.ceil(Math.max(real_image_height/result._sizeY,real_image_width/result._sizeX));PXN8.preview.
centerOffset(result)}else result._fullsize_ratio=-1;result._intervalId=setInterval(function(){PXN8.preview._manageQueue(
result)},750);PXN8.preview._div_to_intervalid[elementId]=result._intervalId;PXN8.event.addListener(elementId,"mousedown"
,result._mouseDown);return result};PXN8.preview.setOffset=function(object,x,y){var img=PXN8.dom.id("pxn8_image"),iw=img.
width/PXN8.zoom.value(),ih=img.height/PXN8.zoom.value(),maxY=ih-object._sizeY,maxX=iw-object._sizeX;if(x<0)x=0;if(y<0)y=
0;if(y>maxY)y=maxY;if(x>maxX)x=maxX;object._offset.x=x;object._offset.y=y;PXN8.preview._refresh(object)};PXN8.preview.
centerOffset=function(object){var _=PXN8.dom,image=_.id("pxn8_image"),iw=image.width/PXN8.zoom.value(),ih=image.height/
PXN8.zoom.value(),halfW=object._sizeX/2,halfH=object._sizeY/2;PXN8.preview.setOffset(object,Math.round(iw/2-halfW),Math.
round(ih/2-halfH))};PXN8.preview.show=function(object,op){var _=PXN8.dom;if(!object._element)return;if(object._element.
style.display=="none")object._element.style.display="block";if(op)object._op=op;if(op)PXN8.preview._enqueue(object)};
PXN8.preview.hide=function(object){if(object._element){object._element.innerHTML="";object._element.style.display="none"
}object._op=false;clearInterval(object._intervalId);PXN8.event.removeListener(object._element,"mousedown",object.
_mouseDown);PXN8.event.removeListener(object._element,"mousemove",object._mouseMove);PXN8.event.removeListener(object.
_element,"mouseup",object._mouseUp)};PXN8.preview._enqueue=function(object){object._opQueue.push(object._op)};PXN8.
preview._complete=true;PXN8.preview._getOffset=function(object){return{x:object._offset.x,y:object._offset.y}};PXN8.
preview._manageQueue=function(object){var lastIndex=object._opQueue.length-1;if(lastIndex<0)return;if(!PXN8.preview.
_complete)return;var _=PXN8.dom,script=PXN8.getScript(),image=_.id("pxn8_image");if(!image)return;PXN8.preview._complete
=false;var op=object._opQueue[lastIndex];object._opQueue.length=0;var width=image.width/PXN8.zoom.value(),height=image.
height/PXN8.zoom.value(),top=object._offset.y,left=object._offset.x,gridSize=100,offsetTop=top%gridSize,offsetLeft=left%
gridSize;top=top-offsetTop;left=left-offsetLeft;var right=Math.min(width,left+(object._sizeX+gridSize)),bottom=Math.min(
height,top+(object._sizeY+gridSize)),addedOps=[],prepareOp=null;if(object._preview_method=="crop")prepareOp={operation:
"crop",top:top,left:left,width:right-left,height:bottom-top,__quality:100,__uncompressed:0};else prepareOp={operation:
"resize",width:right-left,height:bottom-top};if(width>object._sizeX&&height>object._sizeY)addedOps.push(prepareOp);var 
opsFromQueue=[];if(PXN8.isArray(op))opsFromQueue=op;else opsFromQueue.push(op);for(var i=0;i<opsFromQueue.length;i++){
var opFromQueue=opsFromQueue[i];if(opFromQueue.__quality==null)opFromQueue.__quality=65;opFromQueue.__uncompressed=0;
addedOps.push(opFromQueue)}object._element.innerHTML='<span class="pxn8_preview_update">Please wait...</span>';var 
cachedImage=PXN8.getUncompressedImage();if(cachedImage)script=[{operation:"cache",image:cachedImage}];for(var i=0;i<
addedOps.length;i++)script.push(addedOps[i]);PXN8.ajax.submitScript(script,function(jsonResponse){PXN8.preview._complete
=true;_.cl(object._element);if(jsonResponse.status=="ERROR"){alert(jsonResponse.errorMessage);return}if(!jsonResponse)
return;var prevImgURL=PXN8.server+PXN8.root+"/"+jsonResponse.image;object._element.style.backgroundImage="url("+
prevImgURL+")";var position=-offsetLeft*1+"px "+-offsetTop*1+"px";object._element.style.backgroundPosition=position})};
PXN8.preview._refresh=function(object){var _=PXN8.dom;if(!object._element)return;var position=-object._offset.x*1+"px "+
-object._offset.y*1+"px",image=_.id("pxn8_image");object._element.style.backgroundPosition=position;object._element.
style.backgroundImage="url("+image.src+")";object._element.style.cursor="move";object._element.style.backgroundRepeat=
"no-repeat"};PXN8.preview._innerMouseMove=function(event,object,source){var _=PXN8.dom;object._endDrag=_.cursorPos(event
);var offset=PXN8.preview._getOffset(object),xdiff=object._beginDrag.x-object._endDrag.x,ydiff=object._beginDrag.y-
object._endDrag.y;if(object._fullsize_ratio==-1){var img=PXN8.dom.id("pxn8_image");if(img){var real_image_width=img.
width/PXN8.zoom.value(),real_image_height=img.height/PXN8.zoom.value();object._fullsize_ratio=Math.ceil(Math.max(
real_image_height/object._sizeY,real_image_width/object._sizeX))}}offset.x+=xdiff*object._fullsize_ratio;offset.y+=ydiff
*object._fullsize_ratio;PXN8.preview.setOffset(object,offset.x,offset.y);object._beginDrag=object._endDrag};PXN8.preview
._innerMouseUp=function(event,object,source){PXN8.event.removeListener(object._element,"mouseup",object._mouseUp);PXN8.
event.removeListener(object._element,"mouseout",object._mouseUp);PXN8.event.removeListener(object._element,"mousemove",
object._mouseMove);PXN8.preview.show(object,object._op)};PXN8.preview._innerMouseDown=function(event,object,source){var 
_=PXN8.dom,img=_.id("pxn8_image");object._element.style.backgroundImage="url("+img.src+")";PXN8.preview._refresh(object)
;object._beginDrag=_.cursorPos(event);object._mouseDownCoords=_.cursorPos(event);PXN8.event.addListener(object._element,
"mouseup",object._mouseUp);PXN8.event.addListener(object._element,"mouseout",object._mouseUp);PXN8.event.addListener(
object._element,"mousemove",object._mouseMove)}