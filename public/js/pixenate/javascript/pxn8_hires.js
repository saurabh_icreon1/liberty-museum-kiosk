var PXN8=PXN8||{};PXN8.ON_HIRES_COMPLETE="ON_HIRES_COMPLETE";PXN8.ON_HIRES_BEGIN="ON_HIRES_BEGIN";PXN8.hires={
originalURL:"",responses:[],jsonCallback:function(jsonResponse){PXN8.listener.notify(PXN8.ON_HIRES_COMPLETE,jsonResponse
)}};PXN8.hires.scaleScript=function(script,ratio){var paramsToScale=["left","width","top","height","radius"];for(var i=0
;i<script.length;i++){var op=script[i];for(var j=0;j<paramsToScale.length;j++){var attr=paramsToScale[j];if(op[attr])op[
attr]=op[attr]*ratio}}};PXN8.hires.doImageChange=function(eventType){var loRes=PXN8.images[0],ratio=PXN8.hires.responses
[0].height/loRes.height,script=PXN8.getScript();PXN8.hires.interpolate(script,PXN8.hires.originalURL,ratio)};PXN8.hires.
interpolate=function(originalScript,hiresImageURL,ratio){originalScript[0].image=hiresImageURL;PXN8.hires.scaleScript(
originalScript,ratio);PXN8.listener.notify(PXN8.ON_HIRES_BEGIN);var opNumberWas=PXN8.opNumber;PXN8.ajax.submitScript(
originalScript,function(jsonResponse){jsonResponse.initOpNumber=opNumberWas;PXN8.hires.jsonCallback(jsonResponse)})};
PXN8.hires.init=function(imageUrl){PXN8.listener.add(PXN8.ON_HIRES_COMPLETE,function(eventType,jsonResponse){PXN8.hires.
responses[jsonResponse.initOpNumber]=jsonResponse});PXN8.hires.originalURL=imageUrl;var fetch={operation:"fetch",image:
imageUrl,pxn8root:PXN8.root,random:Math.random()};PXN8.listener.notify(PXN8.ON_HIRES_BEGIN);var opNumberWas=PXN8.
opNumber;PXN8.ajax.submitScript([fetch],function(jsonResponse){jsonResponse.initOpNumber=opNumberWas;PXN8.hires.
jsonCallback(jsonResponse)});PXN8.listener.add(PXN8.ON_IMAGE_CHANGE,PXN8.hires.doImageChange);PXN8.getUncompressedImage=
PXN8.hires.getUncompressedImage};PXN8.hires.getUncompressedImage=function(){var result=false;if(PXN8.hires.responses[
PXN8.opNumber])result=PXN8.hires.responses[PXN8.opNumber].uncompressed;return result}