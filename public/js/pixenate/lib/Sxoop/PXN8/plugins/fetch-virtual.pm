use strict;
use Sxoop::PXN8 ':all';
#
#
# insert the following new property/line to your pixenate/config.ini file...
#
# VIRTUAL_IMAGE_ROOT = "/js/pixenate/cache/" ,
#
#
sub fetch_from_vpath
{
	 my ($image, $params) = @_;

	 unless (exists $ENV{PIXENATE_VIRTUAL_IMAGE_ROOT} )
	 {
		  die "The VIRTUAL_IMAGE_ROOT configuration property has not been set!\n";
	 }

	 my $path = $ENV{PIXENATE_VIRTUAL_IMAGE_ROOT} . $params->{virtual_path};

	 unless (-e $path){
		  die "File $path does not exist\n";
	 }
	 my $imrc = $image->Read($path);

	 if (is_imagick_error($imrc)){
		  die "ImageMagick cannot open file: $imrc\n";
	 }
	 return $image;
}
AddOperation('vpath', \&fetch_from_vpath);
1;