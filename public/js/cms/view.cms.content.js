$(function() {
    getTimeline("1600");
    getSolTimeline("1600");
    $("#show-details").click(function() {
        $("#details").show();
        $("#gallery").hide();
        $("#video").hide()
    });
    $("#show-gallery").click(function() {
        $("#details").hide();
        $("#gallery").show();
        $("#video").hide()
    });
    $("#show-video").click(function() {
        if ($("#video_flag").val() == "1") {
            $("#details").hide();
            $("#gallery").hide();
            $("#video").show()
        } else
            window.open($("#video_flag").val(), "_blank")
    })
});
function fa_category(id, catId,catName) {
    showAjxLoader();
    $.ajax({type: "POST", url: "/get-fa-data/" + id, success: function(response) {
            hideAjxLoader();
            $("a[id^='cat_']").removeClass("active");
            $("#fa-data").html(response);
            $("#cat_" + catId).addClass("active");
            //ga('require', 'displayfeatures');
            //ga('send', 'pageview',{'page': '/famous-passengers-'+catName.toLowerCase(),'title': 'Famous Passengers'+catName});
        }})
}
function peopling_content(id) {
    $.ajax({type: "POST", url: "/get-peopling-data/" + id, success: function(response) {
            $("#peopling_content").html(response)
        }})
}
function showContent(id) {
    $.ajax({type: "POST", url: "/get-ha-data/" + id, success: function(response) {
            $("li[id^='member_']").removeClass("active");
            $("#member_" + id).addClass("active");
            $("#heritage-content").html(response)
        }})
}
function getTimeline(year) {
    $.ajax({type: "POST", url: "/get-timeline-data/" + year, success: function(response) {
            $("li[id^='timeline_']").removeClass("active");
            $("#timeline_" + year).addClass("active");
            $("#timeline-content_1").html(response);
            $(".slider").each(function() {
                var $this = $(this);
                $this.cycle({fx: "scrollHorz", speed: 400, timeout: 0, next: $this.prev(), prev: $this.prev().prev()})
            })
        }})
}
function getSolTimeline(year) {
    $.ajax({type: "POST", url: "/get-sol-timeline-data/" + year, success: function(response) {
            $("li[id^='timeline_']").removeClass("active");
            $("#timeline_" + year).addClass("active");
            $("#timeline-content_2").html(response);
            $(".slider").each(function() {
                var $this = $(this);
                $this.cycle({fx: "scrollHorz", speed: 400, timeout: 0, next: $this.prev(), prev: $this.prev().prev()})
            })
        }})
}
function viewSponser(id) {
    $.ajax({type: "POST", url: "/get-sponser-data/" + id, success: function(response) {
            $("#sponser_content_main").hide();
            $("#sponser_content").show();
            $("#sponser_content").html(response)
        }})
}
function show_main_content() {
    $("#sponser_content_main").show();
    $("#sponser_content").hide()
}
;
