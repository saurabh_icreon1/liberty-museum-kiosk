$(function() {
    $("#activate_date").removeClass();
    $("#activate_date").addClass("width-120 cal-icon");
    $("#activate_date").datepicker({changeMonth: true, changeYear: true, onClose: function(selectedDate) {
            $("#end_date").datepicker("option", "minDate", selectedDate)
        }});
    $("#end_date").removeClass();
    $("#end_date").addClass("width-120 cal-icon");
    $("#end_date").datepicker({changeMonth: true, changeYear: true, onClose: function(selectedDate) {
            $("#activate_date").datepicker("option", "maxDate", selectedDate)
        }});
    $.jgrid.no_legacy_api =
            true;
    $.jgrid.useJSON = true;
    var gridProAttrib = jQuery("#list");
    var emptyMsgDiv = $('<div class="no-record-msz">' + NO_RECORD_FOUND + "</div>");
    $("#list").jqGrid({mtype: "POST", url: "/list-crm-ads", datatype: "json", sortable: true, colNames: ["Title", "Page URL", "Zone", "Description", "Active Date", "End Date", "Status", "Action(s)",""], colModel: [{name: "Title", index: "ad_title"}, {name: "PageURL", index: "page_url"}, {name: "Zone", index: "ad_zone_name"}, {name: "Description", index: "ad_description"}, {name: "ActivateDate", index: "activate_date"},
            {name: "EndDate", index: "end_date"}, {name: "Status", index: "is_active"}, {name: "Action(s)", index: "action", sortable: false},{name: "expired", index: "expired", sortable: false,hidden:true}], viewrecords: true, sortname: "ads_ads.ad_id", sortorder: "DESC", rowNum: 10, rowList: [10, 20, 30], pager: "#pcrud", autowidth: true, shrinkToFit: true, caption: "", width: "100%", loadComplete: function() {
            var count = gridProAttrib.getGridParam();
            var ts = gridProAttrib[0];
            if (ts.p.reccount === 0) {
                gridProAttrib.hide();
                emptyMsgDiv.show();
                $("#pcrud_right div.ui-paging-info").css("display", "none")
            } else {
                gridProAttrib.show();
                emptyMsgDiv.hide();
                $("#pcrud_right div.ui-paging-info").css("display", "block")
            }
        },
        gridComplete: function()
        {
            var rows = $("#list").getDataIDs(); 
            for (var i = 0; i < rows.length; i++)
            {
                var expired = $("#list").getCell(rows[i],"expired");
                var status = $("#list").getCell(rows[i],"Status");
                if(expired == "1" || status == "Inactive")
                {
                    $("#list").jqGrid('setRowData',rows[i],false, {background:'#b2b2b2'});            
                }
                
            }
        }
   
                      
                     
  
    });

    emptyMsgDiv.insertAfter(gridProAttrib.parent());
    emptyMsgDiv.hide();
    $("#list").jqGrid("navGrid", "#pcrud", {reload: true, edit: false, add: false, search: false, del: false});
    $("#searchbutton").click(function() {
        showAjxLoader();
        $("#search_msg").hide();
        $("#success_message").hide();
        jQuery("#list").jqGrid("setGridParam", {postData: {searchString: $("#search_ads").serialize()}});
        jQuery("#list").trigger("reloadGrid", [{page: 1}]);
        window.location.hash =
                "#publish";
        hideAjxLoader();
        return false
    })
});
function deleteAds(id) {
    $(".deleteads").colorbox({width: "700px", height: "180px", inline: true});
    $("#no").click(function() {
        $.colorbox.close();
        $("#yes").unbind("click");
        return false
    });
    $("#yes").click(function() {
        $.ajax({url: "/delete-crm-ad", method: "POST", dataType: "html", data: {ads_id: id}, success: function(jsonResult) {
                if (!checkUserAuthenticationAjx(jsonResult))
                    return false;
                location.reload()
            }})
    })
}
function viewStatistics(AdIDEncrypt) {
    $.colorbox({href: "/get-ads-statistics/" + AdIDEncrypt, width: "1200px", height: "420px", escKey: true})
}
;
