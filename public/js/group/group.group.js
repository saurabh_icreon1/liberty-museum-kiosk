$(function(){

// start 
    
    $("#name_email_look").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:function(request,response){
            $.ajax({
                url:"/get-contacts",
                method:"POST",
                dataType:"json",
                data:{
                    user_email_with_name:$("#name_email_look").val()
                    },
                success:function(jsonResult){
                    if(!checkUserAuthenticationAjx(jsonResult))return false;
                    var resultset=[];
                    $.each(jsonResult,function(){
                        var id=this.user_id;
                        var value=this.full_name;
                        resultset.push({
                            id:this.user_id,
                            value:this.full_name
                            })
                        });
                    response(resultset)
                    }
                })
        },
    open:function(){
        $(".ui-menu").width(350)
        },
    change:function(event,ui){
        if(ui.item){
            $("#name_email_look").val(ui.item.value);
            $("#name_email_look_id").val(ui.item.id)
            }else{
            $("#name_email_look").val("");
            $("#name_email_look_id").val("")
            }
        },
    select:function(event,ui){
        $("#name_email_look").val(ui.item.value);
        $("#name_email_look_id").val(ui.item.id);
        return false
        }
    });
$("#searchcontacts").click(function(){
    var flag=true;
    /*count of selected filters ends*/
        var countSelectVal = 0;
       
        if($.trim($('#name_email_look_id').val()) !='' )
        {
            countSelectVal = countSelectVal+2;
        }
        if($.trim($('#campaign').val()) !='' )
        {
           countSelectVal = countSelectVal+2;
        }
        if($.trim($('#transaction_amount_from').val()) !='' )
        {
            countSelectVal =countSelectVal+.5;
        }
        
        if($.trim($('#transaction_amount_to').val()) !='' )
        {
            countSelectVal =countSelectVal+.5;
        }
        
        if($('#product_type').val() !='null' && $.trim($('#product_type').val()) !='' )
        {
            countSelectVal = countSelectVal+2;
        }
        
        if($('#product_ids').val() !='null' && $.trim($('#product_ids').val()) !='' )
        {
            countSelectVal = countSelectVal+2;
        }
        
        if($.trim($('#phone').val()) !='' )
        {
            countSelectVal = countSelectVal+2;
        }
        
        if($('#state').val() !='null' && $.trim($('#state').val()) !='' )
        {
            countSelectVal = countSelectVal+2;
        }
        
        if($.trim($('#city').val()) !='' )
        {
            countSelectVal = countSelectVal+2;
        }
        
        if($('#country').val() !='null' && $.trim($('#country').val()) !='')
        {
            if($("#is_exclude_country").prop("checked") ==false)
            {
                countSelectVal = countSelectVal+2;
            }
            else
            {
                countSelectVal = countSelectVal+1;
            }
        }
        
        if($.trim($('#assocation').val()) !='' )
        {
            countSelectVal = countSelectVal+2;
        }
        
        if($('#relationship').val() !='null' && $.trim($('#relationship').val()) !='' )
        {
            countSelectVal = countSelectVal+2;
        }
        
        if($('#privacy').val() !='null' && $.trim($('#privacy').val()) !='' )
        {
            countSelectVal = countSelectVal+2;
        }
        if($('#exclude_group').val() != 'null' && $.trim($('#exclude_group').val()) !='')
        {
            if($("#is_exclude_group").prop("checked") ==false)
            {
                countSelectVal = countSelectVal+2;
            }
            else
            {
                countSelectVal = countSelectVal+1;
            }
            
        }
        if($('#exclude_tags').val() !='null' && $.trim($('#exclude_tags').val()) !='')
        {
            if($("#is_exclude_tags").prop("checked") ==false)
            {
                countSelectVal = countSelectVal+2;
            }
            else
            {
                countSelectVal = countSelectVal+1;
            }
        }
        if($.trim($('#contact_type').val()) !='' )
        {
            countSelectVal = countSelectVal+2;
        }
        
        if($.trim($('#status').val()) !='' )
        {
            countSelectVal =countSelectVal+1;
        }
        if($.trim($('#membership_title').val()) !='' )
        {
            countSelectVal = countSelectVal+2;
        }
        if($.trim($('#transaction_status_id').val()) !='' )
        {
            countSelectVal = countSelectVal+2;
        }
        if($('#item_status').val() !='null' && $.trim($('#item_status').val()) !='' )
        {
           countSelectVal = countSelectVal+2;
        }
        /*count of selected filters ends*/
        if ($("#transaction_status:checked").val() == "1" || $("#signup_status:checked").val() == "1" || $("#web_status:checked").val() == "1")
            if ($.trim($("#transaction_amount_date").val()) == "")
            {
                $("label[for='transaction_amount']").remove();
                $("#transaction_amount_date").after("<label class='error' for='transaction_amount'>" + GROUP_CHOOSE_DATE_RANGE_EMPTY + "</label>");
                flag = false
            }
            else
            {
                $("label[for='transaction_amount']").remove();
                if ($.trim($("#transaction_amount_date").val()) == "1")
                    if ($.trim($("#transaction_amount_date_from").val()) == "" && $.trim($("#transaction_amount_date_to").val()) == "") 
                    {
                        $("label[for='transaction_amount']").remove();
                        $("#transaction_amount_date_from").after("<label class='error' for='transaction_amount'>" +
                                GROUP_ENTER_DATE_EMPTY + "</label>");
                        flag = false
                    }
                    else
                        $("label[for='transaction_amount']").remove()
            }
        else if ($.trim($("#transaction_amount_date").val()) != "")
        {
            $("label[for='transaction_amount']").remove();
            if ($.trim($("#transaction_amount_date").val()) == "1")
                if ($.trim($("#transaction_amount_date_from").val()) == "" && $.trim($("#transaction_amount_date_to").val()) == "")
                {
                    $("label[for='transaction_amount']").remove();
                    $("#transaction_amount_date_from").after("<label class='error' for='transaction_amount'>" + GROUP_ENTER_DATE_EMPTY +
                            "</label>");
                    flag = false
                }
                else
                {
                    if (countSelectVal < 2)
                    {
                        $("html, body").animate({scrollTop:$(".create-contacts").offset().top},2000);
                         $('#search-err-msg_need_two').css({display: "block"});
                        //$("label[for='transaction_amount']").remove();
                        //$("#transaction_status").after("<label class='error' for='transaction_amount'>" + GROUP_DATE_RANGE_EMPTY + "</label>");
                        flag = false
                    }
                    else
                    {
                       flag = true; 
                    }
                }
            else
            {
                if (countSelectVal < 2)
                {
                    $("html, body").animate({scrollTop:$(".create-contacts").offset().top},2000);
                    $('#search-err-msg_need_two').css({display: "block"});
                    //$("label[for='transaction_amount']").remove();
                    //$("#transaction_status").after("<label class='error' for='transaction_amount'>" + GROUP_DATE_RANGE_EMPTY + "</label>");
                    flag = false
                }
                else
                {
                    flag = true; 
                }
            }
        }
        else
        {
                $("label[for='transaction_amount']").remove();
                if ($.trim($("#transaction_amount_date_from").val()) != "" || $.trim($("#transaction_amount_date_to").val()) != "")
                {
                    if ($.trim($("#transaction_amount_date").val()) == "")
                    {
                        $("label[for='transaction_amount']").remove();
                        $("#transaction_amount_date").after("<label class='error' for='transaction_amount'>" + GROUP_CHOOSE_DATE_RANGE_EMPTY + "</label>");
                        flag = false
                    } 
                    else 
                    {
                        $("label[for='transaction_amount']").remove();
                        $("#transaction_status").after("<label class='error' for='transaction_amount'>" + GROUP_DATE_RANGE_EMPTY + "</label>");
                        flag = false
                    }
                }
                else
                {
                    if (countSelectVal < 2)
                    {
                        $("html, body").animate({scrollTop:$(".create-contacts").offset().top},2000);
                        $('#search-err-msg_need_two').css({display: "block"});
                        flag = false;  
                    }
                    else
                    {
                        $("label[for='transaction_amount']").remove();
                    }
                    
                }
                
        }
        if (flag == false)
            return false;
        else
            return true
    });
$("#transaction_amount_date_from").datepicker({
    changeMonth:true,
    changeYear:true,
    showOtherMonths:true,
    onClose:function(selectedDate){
            $("#transaction_amount_date_to").datepicker("option","minDate",selectedDate);
            if(selectedDate != undefined && $.trim(selectedDate) != "") {
              var dateSelected = $('#transaction_amount_date_from').datepicker("getDate"); 
              var rMax = new Date(dateSelected.getFullYear() + 1, dateSelected.getMonth(),dateSelected.getDate() + 2);
              $("#transaction_amount_date_to").datepicker("option","maxDate",$.datepicker.formatDate('mm/dd/yy', new Date(rMax)));  
            }
        }
    });
$("#transaction_amount_date_to").datepicker({
    changeMonth:true,
    changeYear:true,
    showOtherMonths:true,
    onClose:function(selectedDate){
            $("#transaction_amount_date_from").datepicker("option","maxDate",selectedDate);
            if(selectedDate != undefined && $.trim(selectedDate) != "") {
              var dateSelected = $('#transaction_amount_date_to').datepicker("getDate"); 
              var rMin = new Date(dateSelected.getFullYear() - 1, dateSelected.getMonth(),dateSelected.getDate() - 2);
              $("#transaction_amount_date_from").datepicker("option","minDate",$.datepicker.formatDate('mm/dd/yy', new Date(rMin)));
            }
        }
    });
$("#ellis_visit_date_from").datepicker({
    changeMonth:true,
    changeYear:true,
    showOtherMonths:true,
    onClose:function(selectedDate){
        $("#ellis_visit_date_to").datepicker("option","minDate",selectedDate)
        }
    });
$("#ellis_visit_date_to").datepicker({
    changeMonth:true,
    changeYear:true,
    showOtherMonths:true,
    onClose:function(selectedDate){
        $("#ellis_visit_date_from").datepicker("option","maxDate",selectedDate)
        }
    });
$.validator.addMethod("namealphanumspecialchar",function(value,element){
    return this.optional(element)||/^[A-Za-z0-9\n\r ,.-]+$/i.test(value)
    },GROUP_NAME_VALID);
$.validator.addMethod("descalphanumspecialchar",
    function(value,element){
        return this.optional(element)||/^[A-Za-z0-9\n\r ,.-]+$/i.test(value)
        },GROUP_DESCRIPTION_VALID);
jQuery.validator.addMethod("alphanumericspecialcharext",function(value,element){
    return this.optional(element)||/^[A-Za-z0-9 ~@!#$%&*?{}+,.\-_]+$/i.test(value)
    },GROUP_DESCRIPTION_VALID);
$("#continue").click(function(){
    $("#group").validate({
        submitHandler:function(){
            $("#contact_search_div").css("display","block");
            $("#create_group_div").css("display","none");
            $("#group").data("validator",
                null);
            if($("input:radio[name=group_type]:checked").val()==2){
                $("#searchcontacts").val("Save Filters to Group");
                $("#groupTagsBlock").hide()
                }else $("#searchcontacts").val("Search")
                },
        rules:{
            group_name:{
                required:true,
                namealphanumspecialchar:true,
                minlength:2,
                maxlength:50
            },
            group_description:{
                required:true,
                alphanumericspecialcharext:true,
                minlength:2,
                maxlength:200
            }
        },
    messages:{
        group_name:{
            required:GROUP_NAME_EMPTY
        },
        group_description:{
            required:GROUP_DESCRIPTION_EMPTY
        }
    }
    })
});
$("#continuetosurvey").click(function(){
    // n - start
    
      var flag=true;
    if($("#transaction_status:checked").val()=="1"||$("#signup_status:checked").val()=="1"||$("#web_status:checked").val()=="1")if($.trim($("#transaction_amount_date").val())==
        ""){
        $("label[for='transaction_amount']").remove();
        $("#transaction_amount_date").after("<label class='error' for='transaction_amount'>"+GROUP_CHOOSE_DATE_RANGE_EMPTY+"</label>");
        flag=false
        }else{
        $("label[for='transaction_amount']").remove();
        if($.trim($("#transaction_amount_date").val())=="1")if($.trim($("#transaction_amount_date_from").val())==""&&$.trim($("#transaction_amount_date_to").val())==""){
            $("label[for='transaction_amount']").remove();
            $("#transaction_amount_date_from").after("<label class='error' for='transaction_amount'>"+
                GROUP_ENTER_DATE_EMPTY+"</label>");
            flag=false
            }else $("label[for='transaction_amount']").remove()
            }else if($.trim($("#transaction_amount_date").val())!=""){
        $("label[for='transaction_amount']").remove();
        if($.trim($("#transaction_amount_date").val())=="1")if($.trim($("#transaction_amount_date_from").val())==""&&$.trim($("#transaction_amount_date_to").val())==""){
            $("label[for='transaction_amount']").remove();
            $("#transaction_amount_date_from").after("<label class='error' for='transaction_amount'>"+GROUP_ENTER_DATE_EMPTY+
                "</label>");
            flag=false
            }else{
            $("label[for='transaction_amount']").remove();
            $("#transaction_status").after("<label class='error' for='transaction_amount'>"+GROUP_DATE_RANGE_EMPTY+"</label>");
            flag=false
            }else{
            $("label[for='transaction_amount']").remove();
            $("#transaction_status").after("<label class='error' for='transaction_amount'>"+GROUP_DATE_RANGE_EMPTY+"</label>");
            flag=false
            }
        }else{
    $("label[for='transaction_amount']").remove();
    if($.trim($("#transaction_amount_date_from").val())!=""||$.trim($("#transaction_amount_date_to").val())!=
        "")if($.trim($("#transaction_amount_date").val())==""){
        $("label[for='transaction_amount']").remove();
        $("#transaction_amount_date").after("<label class='error' for='transaction_amount'>"+GROUP_CHOOSE_DATE_RANGE_EMPTY+"</label>");
        flag=false
        }else{
        $("label[for='transaction_amount']").remove();
        $("#transaction_status").after("<label class='error' for='transaction_amount'>"+GROUP_DATE_RANGE_EMPTY+"</label>");
        flag=false
        }else $("label[for='transaction_amount']").remove()
        }
        if(flag==false) { 
            return false; 
         } 
        else { 
         $("#survey_search_div").css("display","block");
         $("#contact_search_div").css("display","none");
         return true;
        }
        
    
    // n - end
    

    });
});
function showDate(id,divId){
    if($("#"+id+" option:selected").text()=="Choose Date Range")$("#"+divId).show();else $("#"+divId).hide()
        }
        var selectedUserIds="";
$(function(){
    $("#edit_group").click(function(){
        $("#edit-group").validate({
            submitHandler:function(){
                var dataStr=$("#edit-group").serialize();
                showAjxLoader();
                $.ajax({
                    type:"POST",
                    url:"/edit-group",
                    data:dataStr,
                    success:function(responseData){
                        hideAjxLoader();
                        if(!checkUserAuthenticationAjx(responseData))return false;
                        var jsonObj=jQuery.parseJSON(responseData);
                        if(jsonObj.status=="success")window.location.href="/groups";else $.each(jsonObj.message,function(i,msg){
                            $("#"+i).after('<label class="error" style="display:block;">'+
                                msg+"</label>")
                            })
                        }
                        })
            },
        rules:{
            group_name:{
                required:true,
                namealphanumspecialchar:true,
                minlength:2,
                maxlength:50
            },
            group_description:{
                required:true,
                alphanumericspecialcharext:true,
                minlength:2,
                maxlength:200
            }
        },
        messages:{
            group_name:{
                required:GROUP_NAME_EMPTY
            },
            group_description:{
                required:GROUP_DESCRIPTION_EMPTY
            }
        }
    })
});
if(typeof encrypt_group_id!="undefined"){
    var groupContactRemoveGrid=jQuery("#group_contact_remove");
    var emptyMsgDiv=$('<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");
    $("#group_contact_remove").jqGrid({
        mtype:"POST",
        url:"/group-contact/"+encrypt_group_id,
        datatype:"json",
        sortable:true,
        colNames:["Name","Email","Association","Country","Source","Type","Action"],
        colModel:[{
            name:"Name",
            index:"full_name"
        },{
            name:"Email",
            index:"usr.email_id"
        },{
            name:"Association",
            index:"membership"
        },{
            name:"Country",
            index:"country_name"
        },{
            name:"Source",
            index:"source_name"
        },{
            name:"Type",
            index:"user_type"
        },{
            name:"Action",
            index:"action",
            sortable:false,
            cmTemplate:{
                title:false
            }
        }],
    viewrecords:true,
    sortname:"grp_user.modified_date",
    sortorder:"desc",
    rowNum:numRecPerPage,
    rowList:pagesArr,
    pager:"#grcoremlistcrud",
    viewrecords:true,
    autowidth:true,
    shrinkToFit:true,
    caption:"",
    width:"100%",
    multiselect:true,
    cmTemplate:{
        title:false
    },
    onSelectRow:function(id){
        addDeleteUserIds(id)
        },
    loadComplete:function(){
        $("#cb_group_contact_remove").hide();
        selectCheckBoxOnPageRefresh();
        var count=groupContactRemoveGrid.getGridParam();
        var ts=groupContactRemoveGrid[0];
        if(ts.p.reccount===0){
            groupContactRemoveGrid.hide();
            emptyMsgDiv.show();
            $("#grcoremlistcrud_right div.ui-paging-info").css("display","none")
            }else{
            groupContactRemoveGrid.show();
            emptyMsgDiv.hide();
            $("#grcoremlistcrud_right div.ui-paging-info").css("display","block")
            }
        }
    });
emptyMsgDiv.insertAfter(groupContactRemoveGrid.parent());
emptyMsgDiv.hide();
$("#group_contact_remove").jqGrid("navGrid","#grcoremlistcrud",{
    reload:true,
    edit:false,
    add:false,
    search:false,
    del:false
})
}
});
function addDeleteUserIds(userId){
    var currCheckBoxRef=$("#jqg_group_contact_remove_"+userId);
    var name=$(currCheckBoxRef).attr("name");
    userId=name.substring(name.lastIndexOf("_")+1);
    if($(currCheckBoxRef).is(":checked"))if(selectedUserIds=="")selectedUserIds=userId;else selectedUserIds+=","+userId;
    else{
        var userIdsArr=selectedUserIds.split(",");
        var newSelectedUserIds="";
        for(var i=0;i<userIdsArr.length;i++)if(userIdsArr[i]!=userId)if(newSelectedUserIds=="")newSelectedUserIds=userIdsArr[i];else newSelectedUserIds+=
            ","+userIdsArr[i];selectedUserIds=newSelectedUserIds
        }
    }
function selectCheckBoxOnPageRefresh(){
    var userIdsArr=selectedUserIds.split(",");
    for(var i=0;i<userIdsArr.length;i++)$("#jqg_group_contact_remove_"+userIdsArr[i]).attr("checked","checked")
        }
function removeContactToGroup(group_id,user_id){
    $("#error_message").html("");
    $("#error_message").css("display","none");
    var activityList=new Array;
    $("#group_contact_remove").find("input[type=checkbox]").each(function(){
        if($(this).is(":checked")){
            var userid=$(this).attr("id");
            userid=userid.replace(/jqg_group_contact_remove_/,"");
            activityList.push(userid)
            }
        });
if(activityList.length>0){
    $(".remove_selected_contacts").colorbox({
        width:"700px",
        height:"200px",
        inline:true
    });
    $(".no_delete_contacts").click(function(){
        $.colorbox.close();
        $("#yes_delete_contacts").unbind("click");
        return false
        });
    $(".yes_delete_contacts").click(function(){
        $(".yes_delete_contacts").unbind("click");
        showAjxLoader();
        $.ajax({
            type:"POST",
            dataType:"json",
            data:{
                user_ids:activityList,
                group_id:$("#group_id").val()
                },
            url:"/remove-user-from-group",
            success:function(responseData){
                hideAjxLoader();
                if(!checkUserAuthenticationAjx(responseData))return false;
                var jsonObj=responseData;
                if(jsonObj.status=="success"){
                    $.colorbox.close();
                    window.location.href="/groups"
                    }else $("#error_message").html(jsonObj.message)
                    }
                })
    })
}else{
    $("#error_message").html(L_SELECT_CONTACTS);
    $("#error_message").css("display","block");
    return false
    }
}

// var campaign_preload_data = [
// { id: 'user5', text: 'Spongebob Squarepants'}
//, { id: 'user6', text: 'Planet Bob' }
//, { id: 'user7', text: 'Inigo Montoya' }
//];
$(document).ready(function(){
   $('#campaign').select2({
        minimumInputLength: 2,
        multiple:true,
        ajax: {
           url: "/get-campaign-auto-suggest",dataType: 'json',type: 'POST',
           data: function (term, page) { return { campaign_title: term }; },
           results: function (data, page) { return { results: data }; }
        } 
     });
      $('#campaign').select2('data', campaign_preload_data);
 });
       
$(function(){
    $("#continue_edit_group,#continue").click(function(){
        if(searchQuery!=""){
           var campaignObj=searchQuery.campaign.split(",");
 
           $('#campaign').select2({
                minimumInputLength: 2, multiple:true,
                ajax: {
                    url: "/get-campaign-auto-suggest",dataType: 'json',type: 'POST',
                    data: function (term, page) { return { campaign_title: term }; },
                    results: function (data, page) { return { results: data }; }
                 } 
            });
            $('#campaign').select2('data', campaign_preload_data);
            var membershipObj=searchQuery.membership_title.split(",");
           if(searchQuery.status != undefined) { $("#status").val(searchQuery.status).select2(); }
           if(searchQuery.membership_title != undefined) { $("#membership_title").val(membershipObj).select2(); }
           if(searchQuery.transaction_status_id != undefined) { $("#transaction_status_id").val(searchQuery.transaction_status_id).select2(); }
           if(searchQuery.item_status != undefined) { $("#item_status").val(searchQuery.item_status.split(",")).select2(); }
 
            var campaignProdids=searchQuery.product_ids.split(",");
            $("#product_ids").val(campaignProdids).select2();
            $("#name_email_look").val(searchQuery.name_email_look);
            $("#name_email_look_id").val(searchQuery.name_email_look_id);
            $("#transaction_amount_from").val(searchQuery.transaction_amount_from);
            $("#transaction_amount_to").val(searchQuery.transaction_amount_to);
            $("#transaction_amount_date").val(searchQuery.transaction_amount_date).select2();
            if(searchQuery.transaction_amount_date=="1")$("#transactiondate_div").show();
            $("#transaction_amount_date_from").val(searchQuery.transaction_amount_date_from);
            $("#transaction_amount_date_to").val(searchQuery.transaction_amount_date_to);
            if(searchQuery.ellis_visit=="1"){
                $("#ellis_visit").prop("checked",true);
                $(".e2").customInput()
                }
                if(searchQuery.no_email_address=="1"){
                $("#no_email_address").prop("checked",true);
                $(".e2").customInput()
                }
                $("#transaction_amount_date_to").val(searchQuery.transaction_amount_date_to);
            $("#city").val(searchQuery.city);
            var stateObj=searchQuery.state.split(",");
            $("#state").val(stateObj).select2();
            var countryObj=searchQuery.country.split(",");
            $("#country").val(countryObj).select2();
            if(searchQuery.is_exclude_country=="1"){
                $("#is_exclude_country").prop("checked",true);
                $(".e2").customInput()
                }
                var productTypeObj=searchQuery.product_type.split(",");
            $("#product_type").val(productTypeObj).select2();
            $("#assocation").val(searchQuery.assocation).select2();
            var relationshipObj=searchQuery.relationship.split(",");
            $("#relationship").val(relationshipObj).select2();
            var privacyObj=searchQuery.privacy.split(",");
            $("#privacy").val(privacyObj).select2();
            $("#contact_type").val(searchQuery.contact_type).select2();
            var excludeTagsObj=searchQuery.exclude_tags.split(",");
            $("#exclude_tags").val(excludeTagsObj).select2();
            if(searchQuery.is_exclude_tags=="1"){
                $("#is_exclude_tags").prop("checked",true);
                $(".e2").customInput()
                }
                var excludeGroupObj=searchQuery.exclude_group.split(",");
            $("#exclude_group").val(excludeGroupObj).select2();
            if(searchQuery.is_exclude_group=="1"){
                $("#is_exclude_group").prop("checked",true);
                $(".e2").customInput()
                }
                if(searchQuery.transaction_status=="1"){
                $("#transaction_status").prop("checked",true);
                $(".e2").customInput()
                }
                if(searchQuery.signup_status=="1"){
                $("#signup_status").prop("checked",true);
                $(".e2").customInput()
                }
                if(searchQuery.web_status=="1"){
                $("#web_status").prop("checked",true);
                $(".e2").customInput()
                }
                $("#phone").val(searchQuery.phone)
            }
            if(surveyQueryData!=""){
            var arrayOfStrings=surveyQueryData.split(",");
            for(var i=0;i<arrayOfStrings.length;i++){
                $("#survey_question_"+arrayOfStrings[i]).prop("checked",true);
                $(".e2").customInput()
                }
            }
            var groupName=$("#group_name").val();
        var groupDescription=$("#group_description").val();
        var groupType=$("#group_type").val();
        var valid=1;
        if(groupName==""||!/^[A-Za-z0-9\n\r ,.-]+$/i.test(groupName)||groupDescription==""||!/^[A-Za-z0-9 ~@!#$%&*?{}+,.\-_]+$/i.test(groupDescription)||groupName.length<2||groupName.length>50||groupDescription.length<2||groupDescription.length>
        200)valid=0;
        if(valid)if(groupType==2){
        $("#edit_group_section").css("display","none");
        $("#edit_group_add_contact").click();
        $("#searchcontacts").val("Save Filters to Group")
        }else{
        $("#edit_group_section").css("display","none");
        $("#searchcontacts").val("Search");
        $("#group_remove_contact_section").css("visibility","visible")
        }else $("#edit_group").trigger("click")
        });
$("#edit_group_add_contact").click(function(){
    $("#group_remove_contact_section").css("display","none");
    $("#contact_search_div").css("display",
        "block")
    })
});
$(function(){});
