var selectedUserIds="";
var unSelectedUserIds="";
function addDeleteUserIds(userId){
    var currCheckBoxRef=$("#jqg_contacts_list_group_"+userId);
    var name=$(currCheckBoxRef).attr("name");
    userId=name.substring(name.lastIndexOf("_")+1);
    if($(currCheckBoxRef).is(":checked")){
        if(selectedUserIds=="")selectedUserIds=userId;else selectedUserIds+=","+userId;

        var unSelectUserIdsArr=unSelectedUserIds.split(",");
        var newUnSelectedUserIds="";

        for(var i=0;i<unSelectUserIdsArr.length;i++)if(unSelectUserIdsArr[i]!=userId)if(newUnSelectedUserIds=="")newUnSelectedUserIds=
            unSelectUserIdsArr[i];else newUnSelectedUserIds+=","+unSelectUserIdsArr[i];unSelectedUserIds=newUnSelectedUserIds
    }else{
        if(unSelectedUserIds=="")unSelectedUserIds=userId;else unSelectedUserIds+=","+userId;
        var userIdsArr=selectedUserIds.split(",");
        var newSelectedUserIds="";
        for(var i=0;i<userIdsArr.length;i++)if(userIdsArr[i]!=userId)if(newSelectedUserIds=="")newSelectedUserIds=userIdsArr[i];else newSelectedUserIds+=","+userIdsArr[i];selectedUserIds=newSelectedUserIds
    }
}
function selectCheckBoxOnPageRefresh(){
    if(userIdsExcluded!="")selectedUserIds+=","+userIdsExcluded;
    var userIdsArr=selectedUserIds.split(",");
    for(var i=0;i<userIdsArr.length;i++)$("#jqg_contacts_list_group_"+userIdsArr[i]).attr("checked","checked");
    var unSelectedUserIdsArr=unSelectedUserIds.split(",");
    for(var i=0;i<unSelectedUserIdsArr.length;i++){
        $("#cb_contacts_list_group").removeAttr("checked");
        $("#jqg_contacts_list_group_"+unSelectedUserIdsArr[i]).removeAttr("checked")
    }
}
$(function(){
    var firstTimeLoadedGrid=0;
    if($("#group_type").val()==2)$("#addusertogroup").val("Exclude Contacts And Save");
    $.jgrid.no_legacy_api=true;
    $.jgrid.useJSON=true;
    var contactListGroupGrid=jQuery("#contacts_list_group");
    var emptyMsgDiv=$('<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");
    $("#contacts_list_group").jqGrid({
        postData:{
            searchString:$("#search-group").serialize()
        },
        mtype:"POST",
        url:"/get-contact-search",
        datatype:"json",
        sortable:true,
        colNames:["Name","Email","Association","Country","Source","Type"],
        colModel:[{
            name:"User name",
            index:"first_name"
        },{
            name:"Email",
            index:"email_id"
        },{
            name:"Association",
            index:"membership"
        },{
            name:"Country",
            index:"country_name"
        },{
            name:"Source",
            index:"source_name"
        },{
            name:"User Type",
            index:"user_type"
        }],
        viewrecords:true,
        sortname:"first_name",
        sortorder:"asc",
        rowNum:numRecPerPage,
        rowList:pagesArr,
        pager:"#cscrud",
        autowidth:true,
        shrinkToFit:true,
        caption:"",
        width:"100%",
        autowidth:true,
        multiselect:true,
        cmTemplate:{
            title:false
        },
        onSelectAll:function(rowIds,flag) {
            if(flag == true) {
                selectedUserIds = userIdsNotInGroup;
            } else {
                selectedUserIds = "";
            }
        },
    onSelectRow:function(id){
        addDeleteUserIds(id)
    },
    loadComplete:function(){
        $("#cb_contacts_list_group").hide();
        if($("#group_type").val()==1){
            $("#cb_contacts_list_group").click();
            if(firstTimeLoadedGrid==0){
                selectedUserIds=userIdsNotInGroup;
                firstTimeLoadedGrid=1
            }
        }
        selectCheckBoxOnPageRefresh();
        hideAjxLoader();
        var count=contactListGroupGrid.getGridParam();
        var ts=contactListGroupGrid[0];
        if(ts.p.reccount===0){
            if($("#group_type").val()==2)
                $("#addusertogroup").val("Save");
            contactListGroupGrid.hide();
            emptyMsgDiv.show();
            $("#cscrud_right div.ui-paging-info").css("display","none")
        }else{
            if($("#group_type").val()==2)
                $("#addusertogroup").val("Exclude Contacts And Save");
            contactListGroupGrid.show();
            emptyMsgDiv.hide();
            $("#cscrud_right div.ui-paging-info").css("display","block")
        }
    }
    });
emptyMsgDiv.insertAfter(contactListGroupGrid.parent());
    emptyMsgDiv.hide();
    $("#contacts_list_group").jqGrid("navGrid","#cscrud",{
        reload:true,
        edit:false,
        add:false,
        search:false,
        del:false
    });
    $("#search_more_filter").click(function(){
        showAjxLoader();
        var seltags=getAllMultiSelectCommaSepValue($("#tags").find(":selected"));
        var selingroup=getAllMultiSelectCommaSepValue($("#in_group_name").find(":selected"));
        var searchString=$("#search-group").serialize()+"&in_group_name="+selingroup+"&tags="+seltags;
        jQuery("#contacts_list_group").jqGrid("setGridParam",{
            postData:{
                searchString:searchString
            }
        });
        jQuery("#contacts_list_group").trigger("reloadGrid",[{
            page:1
        }]);
        return false
    });
    $("#addusertogroup").click(function(){
        var all_unselected_user=unSelectedUserIds.split(",");
//        $(all_unselected_user).each(function(){
//            var name=$(this).attr("name");
//            var user_id=name.substring(name.lastIndexOf("_")+1);
//	
//            addDeleteUserIds(user_id)
//        });
        var selected_user=selectedUserIds.split(",");
        selected_user = ($(selected_user).not(all_unselected_user).get());
        selectedUserIds = selected_user.join(',');
        
        if(selected_user.length>0||$("#group_type").val()==2){
            var group_id=$("#group_id").val();
            var group_name=$("#group_name").val();
            var group_description=$("#group_description").val();
            var group_type=$("#group_type").val();
            var save_search_param=$(".save-search-param").serialize();
            var save_survey_param=$("#surveyids").val();
            //ticket 73 private group
            var is_private= 0;
            if($("#is_private").length > 0)
            {
                is_private=$("#is_private").val();
            }
            
            showAjxLoader();
            $.ajax({
                type:"POST",
                dataType:"json",
                data:{
                    group_id:group_id,
                    user_ids:selectedUserIds,
                    group_name:group_name,
                    group_description:group_description,
                    group_type:group_type,
                    save_search_param:save_search_param,
                    save_survey_param:save_survey_param,
                    is_private:is_private
                },
                url:"/add-user-to-group",
                success:function(responseData){
                    hideAjxLoader();
                    if(!checkUserAuthenticationAjx(responseData))return false;
                    var jsonObj=responseData;
                    if(jsonObj.status=="success")window.location.href="/groups";else $("#error_message").html(jsonObj.message)
                }
            })
        }
    })
});
function getAllMultiSelectCommaSepValue(objEleArr){
    var arr_comma_str="";
    $(objEleArr).each(function(){
        arr_comma_str+=$(this).val()+","
    });
    return arr_comma_str.substr(0,arr_comma_str.lastIndexOf(","))
};
