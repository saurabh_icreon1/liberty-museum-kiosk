$(function(){$("#updateemail").validate({submitHandler:function(){var dataStr=$("#updateemail").serialize();$.ajax({type
:"POST",url:"/update-email",data:dataStr,success:function(responseData){var jsonObj=jQuery.parseJSON(responseData);if(
jsonObj.status=="success")window.location.href="/activation/";else $.each(jsonObj.message,function(i,msg){$("#"+i).after
('<label class="error" style="display:block;">'+msg+"</label>")})}})},rules:{email:{required:true,email:true},
confirm_email:{required:true}},messages:{email:{required:EMAIL_EMPTY,email:EMAIL_INVALID},confirm_email:{required:
CONFIRM_EMAIL_EMPTY}}})})