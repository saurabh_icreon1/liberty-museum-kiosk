$(function(){$("#deleteCrmUser").validate({submitHandler:function(){var dataStr=$("#deleteCrmUser").serialize();
showAjxLoader();$.ajax({type:"POST",url:"/change-crm-user-status",data:dataStr,success:function(responseData){var 
jsonObj=jQuery.parseJSON(responseData);if(jsonObj.status=="success")parent.location.reload();else $.each(jsonObj.message
,function(i,msg){$("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")});hideAjxLoader()}})},
rules:{name:{required:true}},messages:{name:{required:NAME_EMPTY}}});$("#name").autocomplete({afterAdd:true,selectFirst:
true,autoFocus:true,source:function(request,response){$.ajax({url:"/get-crm-contacts/"+$("#user_id").val(),method:"POST"
,dataType:"json",data:{crm_full_name:$("#name").val()},success:function(jsonResult){var resultset=[];$.each(jsonResult,
function(){var id=this.crm_user_id,value=this.crm_full_name;resultset.push({id:this.crm_user_id,value:this.crm_full_name
})});response(resultset)}})},open:function(){$(".ui-menu").width(300)},change:function(event,ui){if(ui.item){$("#name").
val(ui.item.value);$("#assigned_to_id").val(ui.item.id)}else{$("#name").val("");$("#assigned_to_id").val("")}},focus:
function(event,ui){$("#assigned_to_id").val(ui.item.id);return false},select:function(event,ui){$("#name").val(ui.item.
value);$("#assigned_to_id").val(ui.item.id);return false}})})