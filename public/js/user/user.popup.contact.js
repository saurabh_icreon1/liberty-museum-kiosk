$(function(){
    jQuery.validator.addMethod("lettersonly",function(value,element){
        return this.optional(element)||/^[a-z]+$/i
        .test(value)
        },VALID_NAME);
    jQuery.validator.addMethod("lettersandapostropheonly",function(value,element){
        return this.
        optional(element)||/^[a-z '\-]+$/i.test(value)},VALID_NAME);jQuery.validator.addMethod("lettersanddotonly",function(value,
    element){
        return this.optional(element)||/^[a-z.']+$/i.test(value)},VALID_NAME);$("#contactpopup").validate({
        submitHandler:function(){
            var dataStr=$("#contactpopup").serialize();
            $.ajax({
                type:"POST",
                url:"/create-popup-contact",
                data
                :dataStr,
                success:function(responseData){
                    if(!checkUserAuthenticationAjx(responseData))return false;
                    var jsonObj=jQuery.
                    parseJSON(responseData);
                    if(jsonObj.status=="success"){
                        parent.$("#contact_name").val(jsonObj.contact_name);
                        parent.$(
                            "#contact_name_id").val(jsonObj.contact_name_id);
                        if(typeof parent.$("#list_transaction_contact")!="undefined")parent.$(
                            "#list_transaction_contact").trigger("reloadGrid",[{
                            page:1
                        }]);
                        parent.$.fn.colorbox.close()
                        }else $.each(jsonObj.message,
                        function(i,msg){
                            $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                            })
                    }
                    })
        },
    rules:{
        first_name:{
            required:true,
            minlength:1,
            maxlength:50,
            lettersandapostropheonly:true
        },
        middle_name:{
            minlength:1,
            maxlength:4,
            lettersanddotonly:true
        },
        last_name:{
            required:true,
            minlength:1,
            maxlength:20,
            lettersandapostropheonly:true
        },
        email_id:{
            required:true,
            email:true
        }
    },
messages:{
    first_name:{
        required:FIRST_NAME_EMPTY
    },
    last_name:{
        required:LAST_NAME_EMPTY
    },
    email_id:{
        required:EMAIL_EMPTY,
        email:EMAIL_INVALID
    }
}
})
})