getmessageCenterInfo();
function getmessageCenterInfo(){
    $.ajax({
        type:"GET",
        data:{},
        url:"/message-center",
        success:function
        (response){
            $("#message_center").html(response)
        }
    })
}
function getBasicInfo(){
    showAjxLoader();
    $.ajax({
        type:"GET",
        data:{},
        url
        :"/edit-basic-info/"+userId,
        success:function(response){
            hideAjxLoader();
            $("#edit_user_content").html(response)
        }
    })
}
function getContactInfo(){
    showAjxLoader();
    $.ajax({
        type:"GET",
        data:{},
        url:"/edit-user-contact-info/"+userId,
        success:
        function(response){
            hideAjxLoader();
            $("#edit_user_content").html(response)
        }
    })
}
function getDemoghraphicInfo(){
    showAjxLoader();
    $.ajax({
        type:"GET",
        data:{},
        url:"/edit-user-demographics-info/"+userId,
        success:function(response){
            hideAjxLoader();
            $("#edit_user_content").html(response)
        }
    })
}
function getCreditCardInfo(){
    showAjxLoader();
    $.ajax({
        type:
        "GET",
        data:{},
        url:"/credit-card-info/"+userId,
        error: function(error) {
            console.log(error);
        },
        success:function(response){
            hideAjxLoader();
            $("#edit_user_content").html(
                response)
        }
    })
}
function getMembershipInfo(){
    showAjxLoader();
    $.ajax({
        type:"GET",
        data:{},
        url:"/user-membership-info/"+userId
        ,
        success:function(response){
            hideAjxLoader();
            $("#edit_user_content").html(response)
        }
    })
}
function getPreferenceInfo(){
    showAjxLoader();
    $.ajax({
        type:"GET",
        data:{},
        url:"/edit-user-preferences/"+userId,
        success:function(response){
            hideAjxLoader
            ();
            $("#edit_user_content").html(response)
        }
    })
}
function getSurveyInfo(){
    showAjxLoader();
    $.ajax({
        type:"GET",
        data:{},
        url:
        "/add-edit-user-survey-info/"+userId,
        success:function(response){
            hideAjxLoader();
            $("#edit_user_content").html(response)
        }
    }
    )
}
function getSocialInfo(){
    showAjxLoader();
    $.ajax({
        type:"GET",
        data:{},
        url:"/user-social-info/"+userId,
        success:function(
            response){
            hideAjxLoader();
            $("#edit_user_content").html(response)
        }
    })
}
$(function(){
    if (mode == "contact") {
        getContactInfo();
        $("#tab-2").addClass("active")
    }
    else if (mode == "token") {
        getCreditCardInfo();
        $("#tab-3").addClass("active")
    } else if (mode == "demographic") {
        getDemoghraphicInfo();
        $("#tab-4").addClass("active")
    }
    else if (mode == "membership") {
        getMembershipInfo();
        $("#tab-5").addClass("active");
    }
    else if (mode == "survey") {
        getSurveyInfo();
        $("#tab-6").addClass("active");
    }
    else if (mode == "preference") {
        getPreferenceInfo();
        $("#tab-7").addClass("active");
    }
    else if (mode == "social") {
        getSocialInfo();
        $("#tab-8").addClass("active");
    }
    else{
        getBasicInfo();
        $("#tab-1").addClass("active")
    }
    $("a[id^='tab-']").click(function(){
        $(
            "a[id^='tab-']").removeClass("active");
        $(this).addClass("active");
        $("[class^='div-tab-']").css("display","none");
        $(
            ".div-"+$(this).attr("id")).css("display","block")
    })
});
$(function(){
    $(document).on("click","#add_new_edu",function(){
        var
        i=$("#edu_count_div").text(),allDivBlock=$("div[id^='education_container_']");
        if(allDivBlock.length==1){
            var parId=$(
                this).parents("div[id^='education_container_']").attr("id"),id=parId.substring(parId.lastIndexOf("_")+1,parId.length);
            $(
                this).parent().append(
                "<input type='button' id='remove' class='button green minus-field' onclick=remove_dynamic_contacts('.education_container_"
                +id+"','"+id+"')>")
        }
        $(this).remove();
        $("<div />",{
            "class":"education_container_"+i,
            id:"education_container_"+i
        }).
        appendTo("#load_education");
        $(".education_container_"+i).load("add-user-education-info/"+i+"/education_container_");
        i++;
        $("#edu_count_div").html(i)
    });
    $(document).on("click","#add_new_job",function(){
        var i=$("#job_count_div").text(),
        allDivBlock=$("div[id^='job_container_']");
        if(allDivBlock.length==1){
            var parId=$(this).parents(
                "div[id^='job_container_']").attr("id"),id=parId.substring(parId.lastIndexOf("_")+1,parId.length);
            $(this).parent().
            append(
                "<input type='button' id='remove' class='button green minus-field' onclick=remove_dynamic_contacts('.job_container_"+id+
                "','"+id+"')>")
        }
        $(this).remove();
        $("<div />",{
            "class":"job_container_"+i,
            id:"job_container_"+i
        }).appendTo("#load_job");
        $
        (".job_container_"+i).load("add-user-job-history/"+i+"/job_container_");
        i++;
        $("#job_count_div").html(i)
    })
})