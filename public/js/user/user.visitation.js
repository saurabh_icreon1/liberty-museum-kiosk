$(function(){var grid=jQuery("#visitation_list"),emptyMsgDiv=$('<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");$
("#visitation_list").jqGrid({postData:{},mtype:"POST",url:"/user-visitation/"+user_id,datatype:"json",colNames:[
L_DATE_VISIT,L_LENGTH],colModel:[{name:L_DATE_VISIT,sortable:false},{name:L_LENGTH,sortable:false}],viewrecords:true,
sortname:"visitation_to_date",sortorder:"desc",rowNum:10,rowList:[10,20,30],pager:"#visitation_pcrud",viewrecords:true,
autowidth:true,shrinkToFit:true,caption:"",width:"100%",cmTemplate:{title:false},loadComplete:function(){var count=grid.
getGridParam(),ts=grid[0];if(ts.p.reccount===0){grid.hide();emptyMsgDiv.show();$(
"#visitation_pcrud_right div.ui-paging-info").css("display","none")}else{grid.show();emptyMsgDiv.hide();$(
"#visitation_pcrud_right div.ui-paging-info").css("display","block")}}});emptyMsgDiv.insertAfter(grid.parent());
emptyMsgDiv.hide();$("#visitation_list").jqGrid("navGrid","#visitation_pcrud",{reload:true,edit:false,add:false,search:
false,del:false})})