$(function(){
    $("#submitbutton").click(function(){
        $(".detail-section").css("display","block")
    });
    $(
        "#corporate_submitbutton").click(function(){
        $(".detail-section").css("display","block")
    });
    $("#demographics_ethnicity").
    change(function(){
        $("div[class^='demographics_ethnicity']").hide();
        if($(".demographics_ethnicity"+this.value))$(
            ".demographics_ethnicity"+this.value).show()
    });
    if($(".demographics_ethnicity"+$("#demographics_ethnicity").val()))$(
        ".demographics_ethnicity"+$("#demographics_ethnicity").val()).show();
    $("#demographics_nationality").change(function(){
        $(
            "div[class^='demographics_nationality']").hide();
        if($(".demographics_nationality"+this.value))$(
            ".demographics_nationality"+this.value).show()
    });
    if($(".demographics_nationality"+$("#demographics_nationality").val()))
        $(".demographics_nationality"+$("#demographics_nationality").val()).show();
    $(document).on("change",
        'select[name="edu_degree_level[]"]',function(){
            $("div[class^="+this.id+"]").hide();
            if(this.value!="")$("."+this.id+this.
                value).show();else $("."+this.id+this.value).hide()
        });
    $(document).on("change",
        'select[name="addressinfo_location_type[]"]',function(){
            $("div[class^="+this.id+"]").hide();
            if(this.value!="")$("."+this
                .id+this.value).show();else $("."+this.id+this.value).hide()
        });
    $(document).on("change",
        'select[name="corporate_addressinfo_location_type[]"]',function(){
            $("div[class^="+this.id+"]").hide();
            if(this.value!="")
                $("."+this.id+this.value).show();else $("."+this.id+this.value).hide()
        });
    $("#tagsandgroups_company_name").tagit({
        availableTags:user_tags,
        singleField:true,
        allowSpaces:true,
        singleFieldNode:$("#tagsandgroups_company_name"),
        beforeTagAdded:function(event,ui){
            var indexObj=-1,contactTag=ui.tagLabel,tags=$("#tag_id").val(),newTag;
            if(tags!="")
                newTag=tags+","+contactTag;else newTag=contactTag;
            $("#tag_id").val(newTag)
        },
        beforeTagRemoved:function(event,ui){
            var 
            indexObj=-1,contactTag=ui.tagLabel,tags=$("#tag_id").val();
            if(tags!=""){
                var newTag=tags.replace(contactTag,"");
                $(
                    "#tag_id").val(newTag)
            }
        }
    });
    $("#corporate_tagsandgroups_company_name").tagit({
        availableTags:user_tags,
        singleField:true,
        singleFieldNode:$("#corporate_tagsandgroups_company_name"),
        beforeTagAdded:function(event,ui){
            var indexObj=-1,contactTag=
            ui.tagLabel,tags=$("#corporate_tag_id").val(),newTag;
            if(tags!="")newTag=tags+","+contactTag;else newTag=contactTag;
            $(
                "#corporate_tag_id").val(newTag)
        },
        beforeTagRemoved:function(event,ui){
            var indexObj=-1,contactTag=ui.tagLabel,tags=$(
                "#corporate_tag_id").val();
            if(tags!=""){
                var newTag=tags.replace(contactTag,"");
                $("#corporate_tag_id").val(newTag)
            }
        }
    });
    var url="/upload-user-thumbnail";
    $("#corporate_upload_profile_image").fileupload({
        url:url,
        dataType:"json",
        done:function(
            e,data){
            data=data.result;
            if(data.status=="success"){
                $("#filemessage_cor").addClass("success-msg");
                $("#filemessage_cor").
                html(data.message);
                $("#filemessage_cor").removeClass("error-msg");
                $("#corporate_upload_profile_image_id").val(data.
                    filename)
            }else{
                $("#filemessage_cor").removeClass("success-msg");
                $("#filemessage_cor").addClass("error-msg");
                $(
                    "#filemessage_cor").html(data.message);
                $("#corporate_upload_profile_image_id").val("")
            }
        },
        start:function(e){
            showAjxLoader
            ()
        },
        stop:function(e){
            hideAjxLoader()
        }
    });
    $("#upload_profile_image").fileupload({
        url:url,
        dataType:"json",
        done:function(e,
            data){
            data=data.result;
            if(data.status=="success"){
                $("#filemessage").addClass("success-msg");
                $("#filemessage").html(data.
                    message);
                $("#filemessage").removeClass("error-msg");
                $("#upload_profile_image_id").val(data.filename)
            }else{
                $(
                    "#filemessage").removeClass("success-msg");
                $("#filemessage").addClass("error-msg");
                $("#filemessage").html(data.message);
                $("#upload_profile_image_id").val("")
            }
        },
        start:function(e){
            showAjxLoader()
        },
        stop:function(e){
            hideAjxLoader()
        }
    });
    $(
        ".fileUploadAddMore").fileupload({
        url:"/upload-user-public-doc",
        dataType:"json",
        done:function(e,data){
            var file_id=$(this
                ).attr("id"),file_number=file_id.substr(file_id.length-1,1);
            data=data.result;
            if(data.status=="success"){
                $(
                    "#filemessage_"+file_number).addClass("success-msg");
                $("#filemessage_"+file_number).html(data.message);
                $("#filemessage_"
                    +file_number).removeClass("error-msg");
                $("#publicdocs_upload_file"+file_number).val(data.filename)
            }else{
                $(
                    "#filemessage_"+file_number).removeClass("success-msg");
                $("#filemessage_"+file_number).addClass("error-msg");
                $(
                    "#filemessage_"+file_number).html(data.message);
                $("#publicdocs_upload_file"+file_number).val("")
            }
        },
        start:function(e){
            showAjxLoader()
        },
        stop:function(e){
            hideAjxLoader()
        }
    });
    $("select.e1").select2();
    $(".e2").customInput();
    $($(".e4")).each(
        function(i){
            if($(this).is("[type=checkbox],[type=radio]")){
                var input=$(this),input_class=input.attr("class").split(" ");
                input.attr({
                    id:input_class["1"]+"_"+i
                });
                var label=$("label[for="+input.attr("id")+"]"),outerLabel=input.parent(),
                outerLabelParent=input.parent().parent(),inputType=input.is("[type=checkbox]")?"checkbox":"radio",oDiv=$(document.
                    createElement("label")).attr({
                    "for":input_class["1"]+"_"+i
                }).html($(outerLabel).text());
                $(outerLabel).remove();
                outerLabelParent.append(input,oDiv)
            }
        });
    $(".e4").customInput();
    $($(".e3")).each(function(i){
        if($(this).is(
            "[type=checkbox],[type=radio]")){
            var input=$(this),input_class=input.attr("class").split(" "),id_name="";
            if(input.attr(
                "id")=="undefined"||input.attr("id")==""||input.attr("id")==undefined||input.attr("id")==null)id_name=input_class["1"]+
                "_"+i;else id_name=input.attr("id");
            input.attr({
                id:id_name
            });
            var label=$("label[for="+input.attr("id")+"]"),outerLabel=
            input.parent(),outerLabelParent=input.parent().parent(),inputType=input.is("[type=checkbox]")?"checkbox":"radio",oDiv=$(
                document.createElement("label")).attr({
                "for":id_name
            }).html($(outerLabel).text());
            $(outerLabel).remove();
            outerLabelParent.append(input,oDiv)
        }
    });
    $(".e3").customInput();
    $($(".address-checkbox")).each(function(i){
        if($(this).is(
            "[type=checkbox],[type=radio]")){
            var input=$(this),input_class=input.attr("class").split(" "),id_name="";
            if(input.attr(
                "id")=="undefined"||input.attr("id")==""||input.attr("id")==undefined||input.attr("id")==null)id_name=input_class["1"]+
                "_"+i;else id_name=input.attr("id");
            input.attr({
                id:id_name
            });
            var label=$("label[for="+input.attr("id")+"]"),outerLabel=
            input.parent(),outerLabelParent=input.parent().parent(),inputType=input.is("[type=checkbox]")?"checkbox":"radio",oDiv=$(
                document.createElement("label")).attr({
                "for":id_name
            }).html($(outerLabel).text());
            $(outerLabel).remove();
            outerLabelParent.append(input,oDiv)
        }
    });
    $(".address-checkbox").customInput();
    $($(".phone-radio")).each(function(i){
        if($(
            this).is("[type=checkbox],[type=radio]")){
            var input=$(this),input_class=input.attr("class").split(" "),id_name="";
            if(
                input.attr("id")=="undefined"||input.attr("id")==""||input.attr("id")==undefined||input.attr("id")==null)id_name=
                input_class["1"]+"_"+i;else id_name=input.attr("id");
            input.attr({
                id:id_name
            });
            var label=$("label[for="+input.attr("id")+
                "]"),outerLabel=input.parent(),outerLabelParent=input.parent().parent(),inputType=input.is("[type=checkbox]")?"checkbox"
            :"radio",oDiv=$(document.createElement("label")).attr({
                "for":id_name
            }).html($(outerLabel).text());
            $(outerLabel).remove()
            ;
            outerLabelParent.append(input,oDiv)
        }
    });
    $(".phone-radio").customInput();
    $("#demographics_dob").datepicker();
    jQuery.
    validator.addMethod("lettersandapostropheonly",function(value,element){
        return this.optional(element)||/^[a-z '\-]+$/i.test
        (value)
    },VALID_NAME);
    jQuery.validator.addMethod("lettersonly",function(value,element){
        return this.optional(element)||
        /^[a-z ]+$/i.test(value)
    },VALID_NAME);
    jQuery.validator.addMethod("alphabetwithdot",function(value,element){
        return this.
        optional(element)||/^[a-z.']+$/i.test(value)},VALID_NAME);$.validator.addMethod("alphanumeric",function(value,element){
        return this.optional(element)||/^[a-z0-9]+$/i.test(value)
    },AFIHC_INVALID);
    jQuery.validator.addMethod(
        "ethinicityalphabetic",function(value,element){
            return this.optional(element)||/^[a-z ]+$/i.test(value)
        },VALID_ETHINICITY
        );
    jQuery.validator.addMethod("nationalityalphabetic",function(value,element){
        return this.optional(element)||/^[a-z ]+$/i
        .test(value)
    },VALID_NATIONALITY);
    jQuery.validator.addMethod("alphanumericspecialchar",function(value,element){
        return this.optional(element)||/^[A-Za-z0-9\r\s\n ,.-]+$/i.test(value)
    },"Enter valid details");
    jQuery.validator.
    addMethod("alphanumericspecialapostrophechar",function(value,element){
        return this.optional(element)||
        /^[A-Za-z0-9'\r\s\n ,.-]+$/i.test(value)},"Enter valid details");$.validator.prototype.checkForm=function(){this.
        prepareForm();
        for(var i=0,elements=this.currentElements=this.elements();elements[i];i++)if(this.findByName(elements[i].
            name).length!=undefined&&this.findByName(elements[i].name).length>1)for(var cnt=0;cnt<this.findByName(elements[i].name).
            length;cnt++)this.check(this.findByName(elements[i].name)[cnt]);else this.check(elements[i]);return this.valid()
    };
    
    $(
        "#contact_individual").validate({
        submitHandler:function(){
            var dataStr=$("#contact_individual").serialize();
            if(
                checkCustomValidation("contact_individual")){
                showAjxLoader();
                $.ajax({
                    type:"POST",
                    url:"/edit-contact/"+$("#contact_id").
                    val(),
                    data:dataStr,
                    success:function(responseData){
                        hideAjxLoader();
                        if(!checkUserAuthenticationAjx(responseData))
                            return false;
                        var jsonObj=jQuery.parseJSON(responseData);
                        if(jsonObj.status=="success"){
                            window.location.href="/contact";
                        }
                        else{
                            $("#success-message").hide();
                            $(".detail-section").css("display","block");
                            if(jsonObj.email_exits)$("#email_id").
                                after('<label for="email_id" generated="true" class="error">'+jsonObj.message+"</label>");else $.each(jsonObj.message,
                                function(i,msg){
                                    $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                                })
                        }
                    }
                })
            }
        },
        rules:{
            first_name:

            {
                required:true,
                minlength:1,
                maxlength:50,
                lettersandapostropheonly:true
            },
            middle_name:{
                minlength:1,
                maxlength:4,
                alphabetwithdot:true
            },
            last_name:{
                required:true,
                minlength:1,
                maxlength:20,
                lettersandapostropheonly:true
            },
            email_id:{
                required:"#no_email_address:unchecked",
                email:true
            },
            afihc:{
                minlength:1,
                maxlength:50,
                alphanumeric:true
            },
            alt_name:{
                minlength:1,
                maxlength:50,
                lettersonly:true
            },
            alt_email:{
                email:true
            },
            alt_phoneno:{
                number:true,
                minlength:2,
                maxlength:10
            },
            alt_relationship:{
                minlength:1,
                maxlength:10,
                lettersonly:true
            },
            communicationpref_custome_email_greetings:{
                minlength:1,
                maxlength:50,
                lettersonly:true
            },
            communicationpref_custome_postal_greetings:{
                minlength:1,
                maxlength:50,
                lettersonly:true
            },
            communicationpref_custome_address_greetings:{
                minlength:1,
                maxlength:50,
                lettersonly:true
            },
            demographics_age:{
                required:true
            }
            ,
            publicdocs_description:{
                minlength:2,
                maxlength:200 //,
            // alphanumericspecialchar:true
            },
            notes:{
                minlength:2,
                maxlength:200 //,
            //alphanumericspecialchar:true
            }
        },
        messages:{
            first_name:{
                required:FIRST_NAME_EMPTY,
                lettersonly:INVALID_NAME
            },
            middle_name:{
                alphabetwithdot:VALID_NAME
            },
            last_name:{
                required:LAST_NAME_EMPTY
            },
            email_id:{
                required:EMAIL_EMPTY,
                email:EMAIL_INVALID
            },
            alt_name:{
                lettersonly:INVALID_NAME
            },
            alt_email:{
                email:EMAIL_INVALID
            },
            alt_phoneno:{
                number:PHONE_VALID
            },
            demographics_age:{
                required:AGE_EMPTY
            }
        },
        highlight:function(label){
            $(".detail-section").css("display","block")
        }
    });
    $(
        "#contact_corporatefoundation").validate({
        submitHandler:function(){
            var dataStr=$("#contact_corporatefoundation").
            serialize();
            if(checkCustomValidation("contact_corporatefoundation")){
                showAjxLoader();
                $.ajax({
                    type:"POST",
                    url:
                    "/edit-contact/"+$("#contact_id").val(),
                    data:dataStr,
                    success:function(responseData){
                        hideAjxLoader();
                        if(!
                            checkUserAuthenticationAjx(responseData))return false;
                        var jsonObj=jQuery.parseJSON(responseData);
                        if(jsonObj.status==
                            "success")window.location.href="/contact";
                        else{
                            $("#success-message").hide();
                            $(".detail-section").css("display","block");
                            if(jsonObj.email_exits)$("#corporate_email_id").after('<label for="corporate_email_id" generated="true" class="error">'+
                                jsonObj.message+"</label>");else $.each(jsonObj.message,function(i,msg){
                                $("#"+i).after(
                                    '<label class="error" style="display:block;">'+msg+"</label>")
                            })
                        }
                    }
                })
            }
        },
        rules:{
            corporate_company_name:{
                required:true,
                minlength:2,
                maxlength:50,
                alphanumericspecialapostrophechar:true
            },
            corporate_legal_name:{
                required:true,
                minlength:2,
                maxlength:40,
                alphanumericspecialapostrophechar:true
            },
            corporate_sic_code:{
                minlength:2,
                maxlength:10,
                alphanumericspecialchar:true
            },
            corporate_email_id:{
                required:"#no_email_address:unchecked",
                email:true
            },
            corporate_alt_email
            :{
                email:true
            },
            corporate_alt_phoneno:{
                number:true
            },
            corporate_alt_relationship:{
                lettersonly:true
            },
            corporate_communicationpref_custome_email_greetings:{
                minlength:1,
                maxlength:50,
                lettersonly:true
            },
            corporate_communicationpref_custome_postal_greetings:{
                minlength:1,
                maxlength:50,
                lettersonly:true
            },
            corporate_communicationpref_custome_address_greetings:{
                minlength:1,
                maxlength:50,
                lettersonly:true
            },
            corporate_publicdocs_description:{
                minlength:1,
                maxlength:200,
                alphanumericspecialchar:true
            },
            corporate_notes:{
                minlength:1,
                maxlength:200  //,
            //alphanumericspecialchar:true
            }
        },
        messages:{
            corporate_company_name:{
                required:COMPANY_NAME_EMPTY
            },
            corporate_legal_name:{
                required:LEGAL_NAME_EMPTY
            },
            corporate_email_id:{
                required:EMAIL_EMPTY,
                email:EMAIL_INVALID
            },
            corporate_alt_email:{
                email:EMAIL_INVALID
            },
            corporate_alt_phoneno:{
                number:PHONE_VALID
            }
        },
        highlight:function(label){
            $(
                ".detail-section").css("display","block")
        }
    });
    $(document).on("click","#new_afihc_div",function(){
        var i=$(
            "#count_afihc_div").text(),allDivBlock=$("div[id^='afhic_container_']");
        if(allDivBlock.length==1){
            var parId=$(this).
            parents("div[id^='afhic_container_']").attr("id"),id=parId.substring(parId.lastIndexOf("_")+1,parId.length);
            $(this).
            parent().append("<a  class='minus' onclick=remove_dynamic_contacts('.afhic_container_"+id+"','"+id+"')>")
        }
        $(this).remove
        ();
        $("<div />",{
            "class":"row afhic_container_"+i,
            id:"afhic_container_"+i
        }).appendTo("#afihc_div");
        $(".afhic_container_"+
            i).load("/afihc/"+i+"/afhic_container_");
        i++;
        $("#count_afihc_div").html(i)
    });
    $(document).on("click","#new_phoneno_div",
        function(){
            var i=$("#count_div").text(),allDivBlock=$("div[id^='container_']");
            if(allDivBlock.length==1){
                var parId=$(
                    this).parents("div[id^='container_']").attr("id"),id=parId.substring(parId.lastIndexOf("_")+1,parId.length);
                $(this).
                parent().append("<a  class='minus' onclick=remove_dynamic_contacts('.container_"+id+"','"+id+"','individual')>")
            }
            $(this)
            .remove();
            $("<div />",{
                "class":"row container_"+i,
                id:"container_"+i
            }).appendTo("#phoneno_div");
            $(".container_"+i).load(
                "/phone-no/"+i+"/container_/primary_Div");
            i++;
            $("#count_div").html(i)
        });
    $(document).on("click","#corp_new_phoneno_div",
        function(){
            var i=$("#cororporate_foundation_count_div").text(),allDivBlock=$("div[id^='corporate_container_']");
            if(
                allDivBlock.length==1){
                var parId=$(this).parents("div[id^='corporate_container_']").attr("id"),id=parId.substring(parId.
                    lastIndexOf("_")+1,parId.length);
                $(this).parent().append(
                    "<a  class='minus' onclick=remove_dynamic_contacts('.corporate_container_"+id+"','"+id+"','corporate')>")
            }
            $(this).remove
            ();
            $("<div />",{
                "class":"row corporate_container_"+i,
                id:"corporate_container_"+i
            }).appendTo(
                "#cororporate_foundation_phoneno_div");
            $(".corporate_container_"+i).load("/corporate-phone-no/"+i+
                "/corporate_container_/corp_primary_Div");
            i++;
            $("#cororporate_foundation_count_div").html(i)
        });
    $(document).on("click",
        "#cororporate_foundation_new_website_div",function(){
            var i=$("#cororporate_foundation_website_count_div").text(),
            allDivBlock=$("div[id^='cororporate_foundation_website_container_']");
            if(allDivBlock.length==1){
                var parId=$(this).
                parents("div[id^='cororporate_foundation_website_container_']").attr("id"),id=parId.substring(parId.lastIndexOf("_")+1,
                    parId.length);
                $(this).parent().append(
                    "<a  class='minus' onclick=remove_dynamic_contacts('.cororporate_foundation_website_container_"+id+"','"+id+"')>")
            }
            $(
                this).remove();
            $("<div />",{
                "class":"row add-website cororporate_foundation_website_container_"+i,
                id:
                "cororporate_foundation_website_container_"+i
            }).appendTo("#cororporate_foundation_website_div");
            $(
                ".cororporate_foundation_website_container_"+i).load("/website/"+i+"/cororporate_foundation_website_container_");
            i++;
            $(
                "#cororporate_foundation_website_count_div").html(i)
        });
    $(document).on("click","#new_address_div",function(){
        var i=$(
            "#count_address_div").text(),allDivBlock=$("div[id^='address_container_']");
        if(allDivBlock.length==1){
            var parId=$(this).
            parents("div[id^='address_container_']").attr("id"),id=parId.substring(parId.lastIndexOf("_")+1,parId.length);
            $(this).
            parent().append("<a  class='minus' onclick=remove_dynamic_address('.address_container_"+id+"','"+id+"')>")
        }
        $(this).
        remove();
        $("<div />",{
            "class":"addressAll no-padd row address_container_"+i,
            id:"address_container_"+i
        }).appendTo(
            "#load_address_section");
        $(".address_container_"+i).load("/address-form/"+i+"/address_container_");
        i++;
        $(
            "#count_address_div").html(i);
        $("#primary_location_count").val(i)
    });
    $(document).on("click","#corp_new_address_div",
        function(){
            var i=$("#corporate_count_address_div").text(),allDivBlock=$("div[id^='corp_address_container_']");
            if(
                allDivBlock.length==1){
                var parId=$(this).parents("div[id^='corp_address_container_']").attr("id"),id=parId.substring(
                    parId.lastIndexOf("_")+1,parId.length);
                $(this).parent().prepend(
                    "<a  class='minus m-r-5' onclick=remove_dynamic_address('.corp_address_container_"+id+"','"+id+"')>")
            }
            $(this).remove();
            $
            ("<div />",{
                "class":"addressAll no-padd row corp_address_container_"+i,
                id:"corp_address_container_"+i
            }).appendTo(
                "#corporate_load_address_section");
            $(".corp_address_container_"+i).load("/corporate-address-form/"+i+
                "/corp_address_container_");
            i++;
            $("#corporate_count_address_div").html(i);
            $("#corporate_primary_location_count").val(i)
        }
        );
    $(document).on("click","#add_new_edu",function(){
        var i=$("#edu_count_div").text(),allDivBlock=$(
            "div[id^='education_container_']");
        if(allDivBlock.length==1){
            var parId=$(this).parents("div[id^='education_container_']"
                ).attr("id"),id=parId.substring(parId.lastIndexOf("_")+1,parId.length);
            $(this).parent().append(
                "<a  class='minus' onclick=remove_dynamic_contacts('.education_container_"+id+"','"+id+"')>")
        }
        $(this).remove();
        $(
            "<div />",{
                "class":"change-form-bg row education_container_"+i,
                id:"education_container_"+i,
                style:
                "width:97.5%; padding:15px"
            }).appendTo("#load_education");
        $(".education_container_"+i).load("/education/"+i+
            "/education_container_");
        i++;
        $("#edu_count_div").html(i)
    });
    $(document).on("click","#add_new_file",function(){
        var i=$(
            "#upload_count_div").text(),allDivBlock=$("div[id^='upload_container_']");
        if(allDivBlock.length==1){
            var parId=$(this).
            parents("div[id^='upload_container_']").attr("id"),id=parId.substring(parId.lastIndexOf("_")+1,parId.length);
            $(this).
            parent().append("<a  class='minus' onclick=remove_dynamic_contacts('.upload_container_"+id+"','"+id+"')>")
        }
        $(this).
        remove();
        $("<div />",{
            "class":"row upload_container_"+i,
            id:"upload_container_"+i
        }).appendTo("#load_upload");
        $(
            ".upload_container_"+i).load("/upload-file/"+i+"/upload_container_");
        i++;
        $("#upload_count_div").html(i);
        $(
            "#publicdocs_upload_count").val(i)
    });
    $(document).on("click","#corporate_foundation_add_new_file",function(){
        var i=$(
            "#corporate_foundation_upload_count_div").text(),allDivBlock=$("div[id^='corporate_foundation_upload_container_']");
        if(
            allDivBlock.length==1){
            var parId=$(this).parents("div[id^='corporate_foundation_upload_container_']").attr("id"),id=
            parId.substring(parId.lastIndexOf("_")+1,parId.length);
            $(this).parent().append(
                "<a  class='minus' onclick=remove_dynamic_contacts('.corporate_foundation_upload_container_"+id+"','"+id+"')>")
        }
        $(this).
        remove();
        $("<div />",{
            "class":"corporate_foundation_upload_container_"+i,
            id:"corporate_foundation_upload_container_"+i
        })
        .appendTo("#corporate_foundation_load_upload");
        $(".corporate_foundation_upload_container_"+i).load(
            "/upload-file-corporate/"+i+"/corporate_foundation_upload_container_");
        i++;
        $("#corporate_foundation_upload_count_div").
        html(i);
        $("#publicdocs_upload_count").val(i)
    });
    $("#corporate_addressinfo_search_contact").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:function(request,response){
            $.ajax({
                url:"/get-contacts",
                method:"POST",
                dataType:
                "json",
                data:{
                    user_email:$("#corporate_addressinfo_search_contact").val()
                },
                success:function(jsonResult){
                    if(!
                        checkUserAuthenticationAjx(jsonResult))return false;
                    var resultset=[];
                    $.each(jsonResult,function(){
                        var id=this.user_id,
                        value=this.full_name,address_one=this.street_address_one,address_two=this.street_address_two,city=this.city,state=this.
                        state,zip_code=this.zip_code,country_id=this.country_id,country_name=this.country_name;
                        resultset.push({
                            id:this.user_id,
                            value:this.full_name,
                            address_one:this.street_address_one,
                            address_two:this.street_address_two,
                            city:this.city,
                            state:this.
                            state,
                            zip_code:this.zip_code,
                            country_id:this.country_id,
                            country_name:this.country_name
                        })
                    });
                    response(resultset)
                }
            })
        },
        open:
        function(){
            $(".ui-menu").width(350)
        },
        change:function(event,ui){
            if(ui.item){
                $("#address_cor_1").show();
                $("#address_cor_2"
                    ).show();
                $("#corporate_addressinfo_search_contact").val("");
                $("#corporate_addressinfo_street_address_1").val(ui.item.
                    address_one);
                $("#corporate_addressinfo_street_address_2").val(ui.item.address_two);
                $("#corporate_addressinfo_city").val(
                    ui.item.city);
                $("#corporate_addressinfo_state_0").val(ui.item.state);
                $("#corporate_address_state_id_0").val(ui.item.
                    state).select2();
                $("#corporate_addressinfo_zip").val(ui.item.zip_code);
                $("#0").val(ui.item.country_id);
                $(
                    "#s2id_0 .select2-chosen").html(ui.item.country_name);
                $("#corp_load_contact_section").hide();
                if(ui.item.country_id==
                    "228"){
                    $("#corporate_state_0").hide();
                    $("#corporate_usa_state_0").show()
                }
            }else $("#addressinfo_search_contact").val("")
        }
        ,
        focus:function(event,ui){
            return false
        },
        select:function(event,ui){
            $("#address_cor_1").show();
            $("#address_cor_2").show();
            $("#corporate_addressinfo_search_contact").val("");
            $("#corporate_addressinfo_street_address_1").val(ui.item.address_one)
            ;
            $("#corporate_addressinfo_street_address_2").val(ui.item.address_two);
            $("#corporate_addressinfo_city").val(ui.item.city
                );
            $("#corporate_addressinfo_state_0").val(ui.item.state);
            $("#corporate_address_state_id_0").val(ui.item.state).select2()
            ;
            $("#corporate_addressinfo_zip").val(ui.item.zip_code);
            $("#0").val(ui.item.country_id);
            $("#s2id_0 .select2-chosen").html
            (ui.item.country_name);
            $("#corp_load_contact_section").hide();
            if(ui.item.country_id=="228"){
                $("#corporate_state_0").hide
                ();
                $("#corporate_usa_state_0").show()
            }
            return false
        }
    });
    $("#addressinfo_search_contact").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:function(request,response){
            $.ajax({
                url:"/get-contacts",
                method:"POST",
                dataType:
                "json",
                data:{
                    user_email:$("#addressinfo_search_contact").val()
                },
                success:function(jsonResult){
                    if(!
                        checkUserAuthenticationAjx(jsonResult))return false;
                    var resultset=[];
                    $.each(jsonResult,function(){
                        var id=this.user_id,
                        value=this.full_name,address_one=this.street_address_one,address_two=this.street_address_two,city=this.city,state=this.
                        state,zip_code=this.zip_code,country_id=this.country_id,country_name=this.country_name;
                        resultset.push({
                            id:this.user_id,
                            value:this.full_name,
                            address_one:this.street_address_one,
                            address_two:this.street_address_two,
                            city:this.city,
                            state:this.
                            state,
                            zip_code:this.zip_code,
                            country_id:this.country_id,
                            country_name:this.country_name
                        })
                    });
                    response(resultset)
                }
            })
        },
        open:
        function(){
            $(".ui-menu").width(350)
        },
        change:function(event,ui){
            if(ui.item){
                $("#address_1").show();
                $("#address_2").show()
                ;
                $("#addressinfo_search_contact").val("");
                $("#addressinfo_street_address_1").val(ui.item.address_one);
                $(
                    "#addressinfo_street_address_2").val(ui.item.address_two);
                $("#addressinfo_city").val(ui.item.city);
                $(
                    "#addressinfo_state_0").val(ui.item.state);
                $("#address_state_id_0").val(ui.item.state).select2();
                $("#addressinfo_zip").
                val(ui.item.zip_code);
                $("#0").val(ui.item.country_id);
                $("#s2id_0 .select2-chosen").html(ui.item.country_name);
                $(
                    "#load_contact_section").hide();
                if(ui.item.country_id=="228"){
                    $("#state_0").hide();
                    $("#usa_state_0").show()
                }
            }else $(
                "#addressinfo_search_contact").val("")
        },
        focus:function(event,ui){
            return false
        },
        select:function(event,ui){
            $("#address_1")
            .show();
            $("#address_2").show();
            $("#addressinfo_search_contact").val("");
            $("#addressinfo_street_address_1").val(ui.item.
                address_one);
            $("#addressinfo_street_address_2").val(ui.item.address_two);
            $("#addressinfo_city").val(ui.item.city);
            $(
                "#addressinfo_state_0").val(ui.item.state);
            $("#address_state_id_0").val(ui.item.state).select2();
            $("#addressinfo_zip").
            val(ui.item.zip_code);
            $("#0").val(ui.item.country_id);
            $("#s2id_0 .select2-chosen").html(ui.item.country_name);
            $(
                "#load_contact_section").hide();
            if(ui.item.country_id=="228"){
                $("#state_0").hide();
                $("#usa_state_0").show()
            }
            return false
        }
    });
    $(document).on("click","input[id^='note']",function(){
        var noteId=this.id.substr(6),val=0;
        if($("#"+this.id).is(
            ":checked"))val="1";else val="0";
        showAjxLoader();
        $.ajax({
            type:"POST",
            url:"/modify-note/"+tableName+"/"+tableField+"/"+
            noteId+"/"+val,
            success:function(responseData){
                var jsonObj=jQuery.parseJSON(responseData);
                if(jsonObj.status=="success"){}
                hideAjxLoader()
            }
        })
    })
    });
function loadRemove(i,classname,primary){
    $("#"+primary).show();
    $("."+classname+""+i).append($(
        "<a style='float: right;' class='minus m-l-5' onclick=remove_dynamic_contacts('."+classname+""+i+"')>",{
            href:
            "javascript:void(0)",
            id:"remove"
        }))
}
function loadForm(){
    var value=user_type;
    if(value=="2"||value=="3"){
        $(
            "#corporate_contact_type").val(value);
        $("#contact_individual_div").hide();
        $("#contact_corp_found_div").show();
        $(
            "#contact_info_div").addClass("active");
        $("#contact_type_div").parent(".detail-section").css("display","block")
    }else{
        $(
            "#contact_type").val(value);
        $("#contact_individual_div").show();
        $("#contact_corp_found_div").hide()
    }
}
show_custome_box(
    "communicationpref_email_greetings","custome_email");
show_custome_box("communicationpref_postal_greetings",
    "custome_postal");
show_custome_box("communicationpref_address","custome_address");
    show_custome_box(
        "corporate_communicationpref_email_greetings","corporate_custome_email");
    show_custome_box(
        "corporate_communicationpref_postal_greetings","corporate_custome_postal");
    show_custome_box(
        "corporate_communicationpref_address","corporate_custome_address");
    function show_custome_box(id,divId){
        var value=$("#"+
            id+" option:selected").text();
        if(value=="Customized")$("#"+divId).show();else $("#"+divId).hide()
    }
    showyeardiv();
    function
    showyeardiv(){
        if($("#demographics_age option:selected ").text()=="<18")$("#year_div").show();else $("#year_div").hide()
    }
    function removeAddress(className,id,address_information_id){
        $.colorbox({
            width:"550px",
            href:"#remove_address",
            height:
            "200px",
            inline:true
        });
        $("#no_delete_address").click(function(){
            $.colorbox.close();
            $("#yes_delete_address").unbind(
                "click");
            return false
        });
        $("#yes_delete_address").click(function(){
            $("#yes_delete_address").unbind("click");
            $.ajax({
                type:
                "POST",
                url:"/delete-user-address-info",
                data:{
                    address_id:address_information_id
                },
                success:function(responseData){
                    var 
                    jsonObj=jQuery.parseJSON(responseData);
                    if(jsonObj.status=="success"){
                        remove_dynamic_address(className,id);
                        var total=$(
                            "#primary_location_count").val();
                        $("#primary_location_count").val(parseInt(total)-1);
                        $("#primary_location"+id).remove()
                    }
                }
            });
            $.colorbox.close()
        })
    }
    function remove_dynamic_contacts(classname,i,userType){
        if(userType=="corporate"){
            var 
            selectClass=i,selectedPrimary=$("#corporate-primary-phone").val();
            if(selectedPrimary==selectClass){
                $(
                    '#contact_corporatefoundation [name="corporate_continfo_primary[]"]').prop("checked",false);
                $(
                    '#contact_corporatefoundation [name="corporate_continfo_primary[]"]').each(function(index){
                    if($(
                        "#corporate_continfo_primary"+index).next().attr("class")=="checked"&&index==i)$("#corporate_continfo_primary"+index).
                        next().removeClass("checked");
                    $("#corporate_continfo_primary0").next().addClass("checked");
                    if(index==0)$(
                        "#corporate_continfo_primary0").prop("checked",true)
                });
                $("#corporate-primary-phone").val("0")
            }else if(selectedPrimary>
                selectClass){
                var assignPrimary=parseInt(selectedPrimary)-parseInt(1);
                $("#corporate-primary-phone").val(assignPrimary)
            }
            else{}
        }else{
            var selectClass=i,selectedPrimary=$("#primary-phone").val();
            if(selectedPrimary==selectClass){
                $(
                    '#contact_individual [name="continfo_primary[]"]').prop("checked",false);
                $(
                    '#contact_individual [name="continfo_primary[]"]').each(function(index){
                    if($("#continfo_primary"+index).next().attr(
                        "class")=="checked"&&index==i)$("#continfo_primary"+index).next().removeClass("checked");
                    $("#continfo_primary0").next().
                    addClass("checked");
                    if(index==0)$("#continfo_primary0").prop("checked",true)
                });
                $("#primary-phone").val("0")
            }else if(
                selectedPrimary>selectClass){
                var assignPrimary=parseInt(selectedPrimary)-parseInt(1);
                $("#primary-phone").val(
                    assignPrimary)
            }else{}
        }
        var plusEle=$(classname).find(".plus");
        $(classname).remove();
        var classDivId=classname.substring(1,
            classname.lastIndexOf("_")+1),lastBlock=$("div[id^='"+classDivId+"']").last();
        if($(lastBlock).find(".plus").length==0){
            var plusId=$(plusEle).attr("id");
            $(lastBlock).find(".addMinus").append('<a href="javascript:void(0);" id="'+plusId+
                '" class="plus"></a>')
        }
        var allDivBlock=$("div[id^='"+classDivId+"']");
        if(allDivBlock.length==1)$(lastBlock).find(
            ".minus").remove()
    }
    function remove_dynamic_address(classname,i){
        var plusEle=$(classname).find(".plus");
        $(classname).
        remove();
        var classDivId=classname.substring(1,classname.lastIndexOf("_")+1),lastBlock=$("div[id^='"+classDivId+"']").
        last();
        if($(lastBlock).find(".plus").length==0){
            var plusId=$(plusEle).attr("id");
            $(lastBlock).find(".addMinus").prepend(
                '<a href="javascript:void(0);" id="'+plusId+'" class="plus right m-r-5"></a>')
        }
        var allDivBlock=$("div[id^='"+classDivId+
            "']");
        if(allDivBlock.length==1)$(lastBlock).find(".minus").remove()
    }
    function loadCorporateContact(val){
        if($(
            "#corporate_addressinfo_another_contact_address").is(":checked")){
            $("#address_cor_1").hide();
            $("#address_cor_2").hide();
            var counter=$("#corporate_count_address_div").html();
            if(counter==1)$("#corp_new_address_div").hide();
            $(
                "#corp_load_contact_section").show()
        }else{
            $("#address_cor_1").show();
            $("#address_cor_2").show();
            $(
                "#corp_new_address_div").show();
            $("#corp_load_contact_section").hide()
        }
    }
    function checkCustomValidation(id){
        var flagCode=
        false,flagArea=false,flagPhone=false,flagextension=false,flagAdd1=false,flagAdd2=false,flagZip=false,flagCity=false,
        flagCountry=false,flagBillingShipping=false,flagBillingShipping2=false,flageMajors=false,flageInstitution=false,flageYear=false,flagWeb=false,
        flagAfihc=false,flagGpa=false;
        if(id=="contact_individual"){
            $('#contact_individual [name="afihc[]"]').each(function(index
                ){
                var str=$(this).val(),regx=/^[a-z0-9]+$/i;
                $(this).next().remove("span");
                if(str.match(regx)||str.length==0){
                    var flag=
                    false;
                    if(str.length<1&&str!=""){
                        if($(this).next("span").length==0)$(this).after(
                            '<span for="edu_major[]" class="error" style="display:block !important;">'+AFIHC_MIN+"</span>");
                        flag=true
                    }else if(str.
                        length>50&&str!=""){
                        if($(this).next("span").length==0)$(this).after(
                            '<span for="edu_major[]" class="error" style="display:block !important;">'+AFIHC_MAX+"</span>");
                        flag=true
                    }else{
                        $(this).
                        next().remove("span");
                        flag=false
                    }
                    flagAfihc=flag
                }else{
                    if($(this).next("span").length==0)$(this).after(
                        '<span class="error" for="edu_major[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'
                        +AFIHC_INVALID+"</span>");
                    flagAfihc=true
                }
                if(flagAfihc)return false
            });
            $('#contact_individual [name="edu_institution[]"]')
            .each(function(index){
                var str=$(this).val(),regx=/^[A-Za-z ]+$/i;
                $(this).next().remove("span");
                if(str.match(regx)||str.
                    length==0){
                    var flag=false;
                    if(str.length<2&&str!=""){
                        if($(this).next("span").length==0)$(this).after(
                            '<span for="edu_institution[]" class="error" style="display:block !important;">'+EDUCATION_INSTITUTION_MIN+"</span>");
                        flag=true
                    }else if(str.length>40&&str!=""){
                        if($(this).next("span").length==0)$(this).after(
                            '<span for="edu_institution[]" class="error" style="display:block !important;">'+EDUCATION_INSTITUTION_MAX+"</span>");
                        flag=true
                    }else{
                        $(this).next().remove("span");
                        flag=false
                    }
                    flageInstitution=flag
                }else{
                    if($(this).next("span").length==0)$(
                        this).after(
                        '<span class="error" for="edu_institution[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'
                        +EDUCATION_INSTITUTION_VALID+"</span>");
                    flageInstitution=true
                }
                if(flageInstitution)return false
            });
            $(
                '#contact_individual [name="edu_attended_year[]"]').each(function(index){
                var str=$(this).val(),regx=/^[0-9]+$/i;
                $(this).
                next().remove("span");
                if(str.match(regx)||str.length==0){
                    var flag=false;
                    if(str.length<2&&str!=""){
                        if($(this).next("span"
                            ).length==0)$(this).after('<span for="edu_attended_year[]" class="error" style="display:block !important;">'+
                            EDUCATION_ATTENDED_YEAR_MIN+"</span>");
                        flag=true
                    }else if(str.length>4&&str!=""){
                        if($(this).next("span").length==0)$(this
                            ).after('<span for="edu_attended_year[]" class="error" style="display:block !important;">'+EDUCATION_ATTENDED_YEAR_MAX+
                            "</span>");
                        flag=true
                    }else{
                        $(this).next().remove("span");
                        flag=false
                    }
                    flageYear=flag
                }else{
                    if($(this).next("span").length==0
                        )$(this).after('<span class="error" for="edu_attended_year[]" generated="true" style="display:block !important;">'+
                        EDUCATION_ATTENDED_YEAR_VALID+"</span>");
                    flageYear=true
                }
                if(flageYear)return false
            });
            $(
                '#contact_individual [name="edu_major[]"]').each(function(index){
                var str=$(this).val(),regx=/^[A-Za-z ,-]+$/i;
                $(this).
                next().remove("span");
                if(str.match(regx)||str.length==0){
                    var flag=false;
                    $(this).next().remove("span");
                    flag=false;
                    flageMajors=flag
                }else{
                    if($(this).next("span").length==0)$(this).after(
                        '<span class="error" for="edu_major[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'
                        +EDUCATION_MAJORS_VALID+"</span>");
                    flageMajors=true
                }
                if(flageMajors)return false
            });
            $(
                '#contact_individual [name="edu_gpa[]"]').each(function(index){
                var str=$(this).val(),regx=/^[0-9.]+$/i;
                $(this).next().
                remove("span");
                if(str.match(regx)||str.length==0){
                    var flag=false;
                    if(str.length<1&&str!=""){
                        if($(this).next("span").
                            length==0)$(this).after(
                            '<span class="error" for="edu_gpa[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'+
                            EDUCATION_GPA_MIN+"</span>");
                        flag=true
                    }else if(str.length>4&&str!=""){
                        if($(this).next("span").length==0)$(this).after(
                            '<span class="error" for="edu_gpa[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'+
                            EDUCATION_GPA_MAX+"</span>");
                        flag=true
                    }else{
                        $(this).next().remove("span");
                        flag=false
                    }
                    flagGpa=flag
                }else{
                    if($(this).next(
                        "span").length==0)$(this).after(
                        '<span class="error" for="edu_gpa[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'+
                        EDUCATION_GPA_VALID+"</span>");
                    flagGpa=true
                }
                if(flagGpa)return false
            });
            $('#contact_individual [name="country_code[]"]').
            each(function(index){
                var str=$(this).val();
                $(this).next().remove("span");
                if($.isNumeric(str)||str==""){
                    flag=false;
                    if(str
                        .length<1&&str!=""){
                        if($(this).next("span").length==0)$(this).after(
                            '<span for="country_code[]" class="error" style="display:block !important;">'+PHONE_NO_COUNTRY_MIN+"</span>");
                        flag=true
                    }
                    else if(str.length>5&&str!=""){
                        if($(this).next("span").length==0)$(this).after(
                            '<span for="country_code[]" class="error" style="display:block !important;">'+PHONE_NO_COUNTRY_MAX+"</span>");
                        flag=true
                    }
                    else{
                        $(this).next().remove("span");
                        flag=false
                    }
                    flagCode=flag
                }else if($(this).next("span").length==0){
                    $(this).after(
                        '<span for="country_code[]" class="error" style="display:block !important;">'+PHONE_VALID+"</span>");
                    flagCode=true
                }
                if(
                    flagCode)return false
            });
            $('#contact_individual [name="extension[]"]').each(function(index){
                var str=$(this).val();
                $(this)
                .next().remove("span");
                if($.isNumeric(str)||str==""){
                    flag=false;
                    if(str.length<1&&str!=""){
                        if($(this).next("span").length
                            ==0)$(this).after('<span for="extension[]" class="error" style="display:block !important;">'+PHONE_NO_COUNTRY_MIN_EXT+
                            "</span>");
                        flag=true
                    }else if(str.length>5&&str!=""){
                        if($(this).next("span").length==0)$(this).after(
                            '<span for="extension[]" class="error" style="display:block !important;">'+PHONE_NO_COUNTRY_MAX_EXT+"</span>");
                        flag=true
                    }else{
                        $(this).next().remove("span");
                        flag=false
                    }
                    flagextension=flag
                }else if($(this).next("span").length==0){
                    $(this).after(
                        '<span for="extension[]" class="error" style="display:block !important;">'+PHONE_VALID+"</span>");
                    flagextension=true
                }
                if(
                    flagextension)return false
            });
        
            $('span[for="area_code[]"]').remove();         
            $('#contact_individual [name="area_code[]"]').each(function(index){
                var str=$(this).val();
                /*if(str==""){
		if($(this).next("span").length==0)$(this).after(
            '<span for="area_code[]" class="error" style="display:block !important;">'+EMPTY_AREA_CODE+'</span>');
			flagArea=true
		}else */ if($.isNumeric(str) && str != "") {
                    flag=false;
                    if(str.length<3&&str!=""){
                        if($(this).next("span").length==0)$(this).after(
                            '<span for="area_code[]" class="error" style="display:block !important;">'+AREA_CODE_MIN+"</span>");
                        flag=true
                    }
                    else if(str.length>5&&str!=""){
                        if($(this).next("span").length==0)$(this).after(
                            '<span for="area_code[]" class="error" style="display:block !important;">'+PHONE_NO_COUNTRY_MAX+"</span>");
                        flag=true
                    }
                    else{
                        $(this).next().remove("span");
                        flag=false
                    }
                    flagArea=flag
                } else if(str != "") {
                    if($(this).next("span").length==0)$(this).after(
                        '<span for="area_code[]" class="error" style="display:block !important;">'+PHONE_NO_VALID+"</span>");
                    flagArea=true
                }
                if(
                    flagArea)return false
            });
        
            $('span[for="phone_no[]"]').remove();        
            $('#contact_individual [name="phone_no[]"]').each(function(index){
                var str=$(this).val();
                /*if(str==""){
				if($(this).next("span").length==0)$(this).after('<span for="phone_no[]" class="error" style="display:block !important;">'+EMPTY_PHONE_NO+'</span>');
				flagPhone=true;
			}  else */
                if($.isNumeric(str) && str != "") {
                    var flag=false;
                    if(str.length<7&&str!=""){
                        if($(this).next("span").length==0)$(this).after(
                            '<span for="phone_no[]" class="error" style="display:block !important;">'+PHONE_NO_MIN+"</span>");
                        flag=true
                    }else if(str.
                        length>10&&str!=""){
                        if($(this).next("span").length==0)$(this).after(
                            '<span for="phone_no[]" class="error" style="display:block !important;">'+PHONE_NO_MAX+"</span>");
                        flag=true
                    }else{
                        $(this)
                        .next().remove("span");
                        flag=false
                    }
                    flagPhone=flag
                } else if(str != "") {
                    if($(this).next("span").length==0)$(this).after(
                        '<span for="phone_no[]" class="error" style="display:block !important;">'+PHONE_VALID+"</span>");
                    flagPhone=true
                }
                if(
                    flagPhone)return false
            });
            $('#contact_individual [name="addressinfo_street_address_1[]"]').each(function(index){
                var str=$
                (this).val(),regx=/^[A-Za-z0-9 ,.-]+$/i;
                $(this).next().remove("span");
                //  if(str.match(regx)||str.length==0){
                var flag=false;
                if(str.length<1&&str!=""){
                    if($(this).next("span").length==0)$(this).after(
                        '<span for="addressinfo_street_address_1[]" class="error" style="display:block !important;">'+MIN_ADDRESS+"</span>");
                    flag=true
                }else if(str.length>50&&str!=""){
                    if($(this).next("span").length==0)$(this).after(
                        '<span for="addressinfo_street_address_1[]" class="error" style="display:block !important;">'+MAX_ADDRESS+"</span>");
                    flag=true
                }else{
                    $(this).next().remove("span");
                    flag=false
                }
                flagAdd1=flag
            /* }else{
                if($(this).next("span").length==0)$(this).
                    after('<span for="addressinfo_street_address_1[]" class="error" style="display:block !important;">'+VALID_ADDRESS+
                        "</span>");
                flagAdd1=true
                }*/
            });
            $('#contact_individual [name="addressinfo_street_address_2[]"]').each(function(index){
                var 
                str=$(this).val(),regx=/^[A-Za-z0-9 ,.-]+$/i;
                $(this).next().remove("span");
                //if(str.match(regx)||str.length==0){
                var flag=
                false;
                if(str.length<1&&str!=""){
                    if($(this).next("span").length==0)$(this).after(
                        '<span for="addressinfo_street_address_2[]" class="error" style="display:block !important;">'+MIN_ADDRESS+"</span>");
                    flag=true
                }else if(str.length>50&&str!=""){
                    if($(this).next("span").length==0)$(this).after(
                        '<span for="addressinfo_street_address_2[]" class="error" style="display:block !important;">'+MAX_ADDRESS+"</span>");
                    flag=true
                }else{
                    $(this).next().remove("span");
                    flag=false
                }
                flagAdd2=flag
            /*}else{
            if($(this).next("span").length==0)$(this).
                after('<span for="addressinfo_street_address_2[]" class="error" style="display:block !important;">'+VALID_ADDRESS+
                    "</span>");
            flagAdd2=true
            }*/
            });
            $('#contact_individual [name="addressinfo_city[]"]').each(function(index){
                var str=$(this).
                val(),regx=/^[a-z .-]+$/i;
                if(str.match(regx)||str.length==0){
                    var flag=false;
                    $(this).next().remove("span");
                    if(str.length<2
                        &&str!=""){
                        if($(this).next("span").length==0)$(this).after(
                            '<span for="addressinfo_city[]" class="error" style="display:block !important;">'+CITY_MIN+"</span>");
                        flag=true
                    }else if(
                        str.length>30&&str!=""){
                        if($(this).next("span").length==0)$(this).after(
                            '<span for="addressinfo_city[]" class="error" style="display:block !important;">'+CITY_MAX+"</span>");
                        flag=true
                    }else{
                        $(
                            this).next().remove("span");
                        flag=false
                    }
                    flagCity=flag
                }else{
                    $(this).next().remove("span");
                    if($(this).next("span").length==
                        0)$(this).after('<span for="addressinfo_city[]" class="error" style="display:block !important;">'+VALID_CITY+"</span>");
                    flagCity=true
                }
            });
            $('#contact_individual [name="addressinfo_zip[]"]').each(function(index){
                var str=$(this).val(),regx=
                /^[a-z0-9- ]+$/i;
                $(this).next().remove("span");
                if(str.match(regx)||str.length==0){
                    var flag=false;
                    if(str.length<4&&str!=
                        ""){
                        if($(this).next("span").length==0)$(this).after(
                            '<span for="addressinfo_zip[]" class="error" style="display:block !important;">'+MIN_ZIP+"</span>");
                        flag=true
                    }else if(
                        str.length>12&&str!=""){
                        if($(this).next("span").length==0)$(this).after(
                            '<span for="addressinfo_zip[]" class="error" style="display:block !important;">'+MAX_ZIP+"</span>");
                        flag=true
                    }else{
                        $(
                            this).next().remove("span");
                        flag=false
                    }
                    flagZip=flag
                }else{
                    if($(this).next("span").length==0)$(this).after(
                        '<span for="addressinfo_zip[]" class="error" style="display:block !important;">'+VALID_ZIP+"</span>");
                    flagZip=true
                }
            });
            $('#contact_individual [name="addressinfo_country[]"]').each(function(index){
                
                var countryDivId = $(this).parent().attr('id');
                var tmpSplitArr = countryDivId.split("_");
                if(tmpSplitArr[1] !== undefined && tmpSplitArr[1] !== index)
                {
                    index=tmpSplitArr[1];
                }
                
            if ($("#address_container_" + index).length !== 0) {
                if ($('#contact_individual  [id="addressinfo_location_shipping' + index + '"]').is(":checked") &&
                        $('#contact_individual  [id="is_apo_po' + index + '"]').is(":checked") != true)
                {
                    flagCountry = false;
                }
                else {
                    var str = $(this).val();
                    if (str ==
                            "") {

                        $(this).after(
                                '<span for="addressinfo_country[]" class="error" style="display:block !important;">' + EMPTY_COUNTRY + "</span>");
                        if ($("#address_container_" + index).length !== 0)
                        {
                            $("html,body").animate({
                                scrollTop: $("#address_container_" + index).offset().top
                            }, "slow")

                        }
                        flagCountry = true
                    } else {
                        $(this).next(
                                ).remove("span");
                        flagCountry = false
                    }
                }
                if (flagCountry)
                    return false
            }
        });
            $('#contact_individual [name="addressinfo_country[]"]'
                ).each(function(index1)
                {
                    
                    var countryDivId = $(this).parent().attr('id');
                    var tmpSplitArr = countryDivId.split("_");
                    if(tmpSplitArr[1] !== undefined && tmpSplitArr[1] !== index1)
                    {
                    index1=tmpSplitArr[1];
                    }
            if ($("#address_container_" + index1).length !== 0)
            {
                if ($("#addressinfo_location_billing" + index1).is(":checked") == false && $(
                        "#addressinfo_location_shipping" + index1).is(":checked") == false) {
                    flagBillingShipping = true;
                    if ($("#addressinfo_location_shipping" + index1).next("span").length == 0) {
                        $("#addressinfo_location_shipping" + index1).after(
                                '<span for="addressinfo_location_shipping[' + index1 + ']" class="error" style="display:block !important;">' +
                                EMPTY_BILLING_SHIPPING + "</span>");

                        $("html,body").animate({
                            scrollTop: $("#address_container_" + index1).offset().top}, "slow")

                    }
                } else {
                    $("#addressinfo_location_shipping" + index1).next().remove("span");
                    flagBillingShipping = false
                }
                if (flagBillingShipping)
                    return false
            }
        })
       
        }else{
            $('#contact_corporatefoundation [name="website[]"]').each(function(index){
                var 
                str=$(this).val(),regx=/[-a-zA-Z0-9@:%_\+.~#?&\/=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&\/=]*)?/gi;
                $(this).next(
                    ).remove("span");
                if(str.match(regx)||str.length==0){
                    var flag=false;
                    if(str.length<6&&str!=""){
                        if($(this).next("span").
                            length==0)$(this).after('<span for="website[]" class="error" style="display:block !important;">'+MAX_WEB+"</span>");
                        flag
                        =true
                    }else{
                        $(this).next().remove("span");
                        flag=false
                    }
                    flagWeb=flag
                }else{
                    if($(this).next("span").length==0)$(this).after(
                        '<span for="website[]" class="error" style="display:block !important;">'+VALID_WEB+"</span>");
                    flagWeb=true
                }
                if(flagWeb)
                    return false
            });
            $('#contact_corporatefoundation [name="corporate_country_code[]"]').each(function(index){
                var str=$(this).
                val();
                $(this).next().remove("span");
                if($.isNumeric(str)||str==""){
                    var flag=false;
                    if(str.length<1&&str!=""){
                        if($(this).
                            next("span").length==0)$(this).after(
                            '<span for="corporate_country_code[]" class="error" style="display:block !important;">'+PHONE_NO_COUNTRY_MIN+"</span>");
                        flag=true
                    }else if(str.length>5&&str!=""){
                        if($(this).next("span").length==0)$(this).after(
                            '<span for="corporate_country_code[]" class="error" style="display:block !important;">'+PHONE_NO_COUNTRY_MAX+"</span>");
                        flag=true
                    }else{
                        $(this).next().remove("span");
                        flag=false
                    }
                    flagCode=flag
                }else{
                    if($(this).next("span").length==0)$(this).
                        after('<span for="corporate_country_code[]" class="error" style="display:block !important;">'+PHONE_VALID+"</span>");
                    flagCode=true
                }
                if(flagCode)return false
            });
            $('#contact_corporatefoundation [name="corporate_area_code[]"]').each(function(
                index){
                var str=$(this).val();
                if(str==""){
                    if($(this).next("span").length==0)$(this).after('<span for="corporate_area_code[]" class="error" style="display:block !important;">'+EMPTY_AREA_CODE+"</span>");
                    flagArea=true;
                }else if($.isNumeric(str)){
                    flag=false;
                    if(str.length<3 && str!=""){
                        if($(this).next("span").
                            length==0)$(this).after('<span for="corporate_area_code[]" class="error" style="display:block !important;">'+
                            AREA_CODE_MIN+"</span>");
                        flag=true
                    }else if(str.length>5&&str!=""){
                        if($(this).next("span").length==0)$(this).after
                            ('<span for="corporate_area_code[]" class="error" style="display:block !important;">'+PHONE_NO_COUNTRY_MAX+"</span>");
                        flag=true
                    }else{
                        $(this).next().remove("span");
                        flag=false
                    }
                    flagArea=flag
                }else{
                    if($(this).next("span").length==0)$(this).
                        after('<span for="corporate_area_code[]" class="error" style="display:block !important;">'+PHONE_VALID+"</span>");
                    flagArea=true
                }
                if(flagArea)return false
            });
            $('#contact_corporatefoundation [name="corporate_phone_no[]"]').each(function(
                index){
                var str=$(this).val();
                if(str==""){
                    if($(this).next(
                        "span").length==0)$(this).after('<span for="corporate_phone_no[]" class="error" style="display:block !important;">'+
                        EMPTY_PHONE_NO+"</span>");
                    flagPhone=true
                }else if($.isNumeric(str)){
                    flag=false;
                    if(str.length<7&&str!=""){
                        if($(this).next("span").
                            length==0)$(this).after('<span for="corporate_phone_no[]" class="error" style="display:block !important;">'+PHONE_NO_MIN
                            +"</span>");
                        flag=true
                    }else if(str.length>10&&str!=""){
                        if($(this).next("span").length==0)$(this).after(
                            '<span for="corporate_phone_no[]" class="error" style="display:block !important;">'+PHONE_NO_MAX+"</span>");
                        flag=true
                    }
                    else{
                        $(this).next().remove("span");
                        flag=false
                    }
                    flagArea=flag
                }else{
                    if($(this).next("span").length==0)$(this).after(
                        '<span for="corporate_phone_no[]" class="error" style="display:block !important;">'+PHONE_NO_VALID+"</span>");
                    flagPhone=
                    true
                }
                if(flagPhone)return false
            });
            $('#contact_corporatefoundation [name="corporate_extension[]"]').each(function(index){
                var str=$(this).val();
                $(this).next().remove("span");
                if($.isNumeric(str)||str==""){
                    flag=false;
                    if(str.length<1&&str!=""){
                        if($(this).next("span").length==0)$(this).after(
                            '<span for="corporate_extension[]" class="error" style="display:block !important;">'+PHONE_NO_COUNTRY_MIN_EXT+"</span>")
                        ;
                        flag=true
                    }else if(str.length>5&&str!=""){
                        if($(this).next("span").length==0)$(this).after(
                            '<span for="corporate_extension[]" class="error" style="display:block !important;">'+PHONE_NO_COUNTRY_MAX_EXT+"</span>")
                        ;
                        flag=true
                    }else{
                        $(this).next().remove("span");
                        flag=false
                    }
                    flagextension=flag
                }else if($(this).next("span").length==0){
                    $(
                        this).after('<span for="corporate_extension[]" class="error" style="display:block !important;">'+PHONE_VALID+"</span>");
                    flagextension=true
                }
                if(flagextension)return false
            });
            $(
                '#contact_corporatefoundation [name="corporate_addressinfo_street_address_1[]"]').each(function(index){
                var str=$(this).
                val(),regx=/^[A-Za-z0-9 ,.-]+$/i;
                if(str.match(regx)||str.length==0){
                    var flag=false;
                    if(str.length<1&&str!=""){
                        if($(this).
                            next("span").length==0)$(this).after(
                            '<span for="corporate_addressinfo_street_address_1[]" class="error" style="display:block !important;">'+VALID_ADDRESS+
                            "</span>");
                        flag=true
                    }else if(str.length>30&&str!=""){
                        if($(this).next("span").length==0)$(this).after(
                            '<span for="corporate_addressinfo_street_address_1[]" class="error" style="display:block !important;">'+VALID_ADDRESS+
                            "</span>");
                        flag=true
                    }else{
                        $(this).next().remove("span");
                        flag=false
                    }
                    flagAdd1=flag
                }else{
                    if($(this).next("span").length==0)
                        $(this).after('<span for="corporate_addressinfo_street_address_1[]" class="error" style="display:block !important;">'+
                            VALID_ADDRESS+"</span>");
                    flagAdd1=true
                }
            });
            $(
                '#contact_corporatefoundation [name="corporate_addressinfo_street_address_2[]"]').each(function(index){
                var str=$(this).
                val(),regx=/^[A-Za-z0-9 ,.-]+$/i;
                if(str.match(regx)||str.length==0){
                    var flag=false;
                    if(str.length<1&&str!=""){
                        if($(this).
                            next("span").length==0)$(this).after(
                            '<span for="corporate_addressinfo_street_address_2[]" class="error" style="display:block !important;">'+VALID_ADDRESS+
                            "</span>");
                        flag=true
                    }else if(str.length>30&&str!=""){
                        if($(this).next("span").length==0)$(this).after(
                            '<span for="corporate_addressinfo_street_address_2[]" class="error" style="display:block !important;">'+VALID_ADDRESS+
                            "</span>");
                        flag=true
                    }else{
                        $(this).next().remove("span");
                        flag=false
                    }
                    flagAdd2=flag
                }else{
                    if($(this).next("span").length==0)
                        $(this).after('<span for="corporate_addressinfo_street_address_2[]" class="error" style="display:block !important;">'+
                            VALID_ADDRESS+"</span>");
                    flagAdd2=true
                }
            });
            $('#contact_corporatefoundation [name="corporate_addressinfo_city[]"]').each(
                function(index){
                    var str=$(this).val(),regx=/^[a-z ]+$/i;
                    $(this).next().remove("span");
                    if(str.match(regx)||str.length==0)

                    {
                        var flag=false;
                        if(str.length<2&&str!=""){
                            if($(this).next("span").length==0)$(this).after(
                                '<span for="corporate_addressinfo_city[]" class="error" style="display:block !important;">'+CITY_MIN+"</span>");
                            flag=
                            true
                        }else if(str.length>15&&str!=""){
                            if($(this).next("span").length==0)$(this).after(
                                '<span for="corporate_addressinfo_city[]" class="error" style="display:block !important;">'+CITY_MAX+"</span>");
                            flag=
                            true
                        }else{
                            $(this).next().remove("span");
                            flag=false
                        }
                        flagCity=flag
                    }else{
                        if($(this).next("span").length==0)$(this).after(
                            '<span for="corporate_addressinfo_city[]" class="error" style="display:block !important;">'+VALID_CITY+"</span>");
                        flagCity=true
                    }
                });
            $('#contact_corporatefoundation [name="corporate_addressinfo_zip[]"]').each(function(index){
                var str=$(
                    this).val(),regx=/^[a-z0-9- ]+$/i;
                $(this).next().remove("span");
                if(str.match(regx)||str.length==0){
                    var flag=false;
                    if(str
                        .length<4&&str!=""){
                        if($(this).next("span").length==0)$(this).after(
                            '<span for="corporate_addressinfo_zip[]" class="error" style="display:block !important;">'+VALID_ZIP+"</span>");
                        flag=
                        true
                    }else if(str.length>8&&str!=""){
                        if($(this).next("span").length==0)$(this).after(
                            '<span for="corporate_addressinfo_zip[]" class="error" style="display:block !important;">'+VALID_ZIP+"</span>");
                        flag=
                        true
                    }else{
                        $(this).next().remove("span");
                        flag=false
                    }
                    flagZip=flag
                }else{
                    if($(this).next("span").length==0)$(this).after(
                        '<span for="corporate_addressinfo_zip[]" class="error" style="display:block !important;">'+VALID_ZIP+"</span>");
                    flagZip=
                    true
                }
            });
            $('#contact_corporatefoundation [name="corporate_addressinfo_country[]"]').each(function(index){
                var countryDivId = $(this).parent().attr('id');
                    var tmpSplitArr = countryDivId.split("_");
                    if(tmpSplitArr[2] !== undefined && tmpSplitArr[2] !== index)
                    {
                        index=tmpSplitArr[2];
                    }
                if($('#contact_corporatefoundation  [id="corporate_addressinfo_location_shipping'+index+'"]').is(":checked")&&$(
                    '#contact_corporatefoundation  [id="corporate_is_apo_po'+index+'"]:checked').val()!=1)flagCountry=false;
                else{
                    var str=$(
                        this).val();
                    if(str==""){
                        if($(this).next("span").length==0){
                            $(this).after(
                                '<span for="corporate_addressinfo_country[]" class="error" style="display:block !important;">'+EMPTY_COUNTRY+"</span>");
                            $("html,body").animate({
                                scrollTop:$("#corp_address_container_"+index).offset().top
                            },"slow")
                        }
                        flagCountry=true
                    }else{
                        $(this
                            ).next().remove("span");
                        flagCountry=false
                    }
                }
                if(flagCountry)return false
            });

            $(
                '#contact_corporatefoundation [name="corporate_addressinfo_country[]"]').each(function(index1){
                    var countryDivId = $(this).parent().attr('id');
                    var tmpSplitArr = countryDivId.split("_");
                    if(tmpSplitArr[2] !== undefined && tmpSplitArr[2] !== index1)
                    {
                    index1=tmpSplitArr[2];
                    }
                if($("#corporate_addressinfo_location_billing"+index1).is(":checked")==false&&$("#corporate_addressinfo_location_shipping"+
                    index1).is(":checked")==false){
                    flagBillingShipping2=true;
                    if($("#corporate_addressinfo_location_shipping"+index1).next(
                        "span").length==0){
                        $("#corporate_addressinfo_location_shipping"+index1).after(
                            '<span for="corporate_addressinfo_location_shipping['+index1+']" class="error" style="display:block !important;">'+
                            EMPTY_BILLING_SHIPPING+"</span>");
                        $("html,body").animate({
                            scrollTop:$("#corp_address_container_"+index1).offset().top
                        },
                        "slow")
                    }
                }else{
                    $("#corporate_addressinfo_location_shipping"+index1).next().remove("span");
                    flagBillingShipping2=false
                }
                if(
                    flagBillingShipping2)return false
            })
        }
        if(flagWeb==true||flagCode==true||flagArea==true||flagPhone==true||flagAdd1==true||
            flagAdd2==true||flagZip==true||flagCity==true||flagCountry==true||flagBillingShipping==true||flagBillingShipping2==true
            ||flageMajors==true||flageInstitution==true||flageYear==true||flagGpa==true||flagAfihc==true||flagextension==true){
            $(
                ".detail-section").css("display","block");
            $(".detail-section").css("display","block");
            return false
        }else return true
    }
    function backToSearch(){
        window.location.href="/contact"
    }
    function removePublicDoc(userId,fileName,key){
        $.colorbox({
            width:
            "500px",
            href:"#delete_contact_doc",
            height:"200px",
            inline:true
        });
        $("#no_delete").click(function(){
            $.colorbox.close();
            $(
                "#yes_delete").unbind("click");
            return false
        });
        $("#yes_delete").click(function(){
            $("#yes_delete").unbind("click");
            $.ajax(

            {
                    type:"POST",
                    data:{
                        userId:userId,
                        fileName:fileName
                    },
                    url:"/delete-contact-doc",
                    success:function(response){
                        $.colorbox.
                        close();
                        $("#doc_"+key).hide()
                    }
                })
        })
    }
    changeStatus();
    changeReason();
    function changeStatus(){
        var status=$("#contact_status")
        .val();
        if(status=="0"){
            $("#div_reason").show();
            $(".e2").customInput();
            //$("#webaccesss_enabled_login").prop("checked", false);
            $("#webaccesss_enabled_login").attr("disabled",true);
            //$("#corporate_webaccesss_enabled_login").prop("checked",false);
            $("#corporate_webaccesss_enabled_login").attr("disabled",true);
            $(".e2").customInput()
        }else{
            $("#div_reason").hide(
                );
            $("#div_other_reason").hide();
            $("#reason_other").val("");
            $("#reason").val("").select2();
            $(".e2").customInput();
            $(
                "#webaccesss_enabled_login").attr("disabled",false);
            $("#corporate_webaccesss_enabled_login").attr("disabled",false);
            $(
                ".e2").customInput()
        }
    }
    function changeReason(){
        var reason=$("#reason").val();
        if(reason=="5")$("#div_other_reason").show()
        ;else $("#div_other_reason").hide()
    }
    $("#no_email_address").click(function(){
        $("#email_id").val("");
        if($(
            "#no_email_address").is(":checked"))$("#email_id").attr("readonly",true);else $("#email_id").attr("readonly",false)
    });
    $(
        "#corporate_no_email_address").click(function(){
        $("#corporate_email_id").val("");
        if($("#corporate_no_email_address").is(
            ":checked"))$("#corporate_email_id").attr("readonly",true);else $("#corporate_email_id").attr("readonly",false)
    });
    $(
        "#don_not_have_website").click(function(){
        if($("#don_not_have_website").is(":checked")){
            $(
                "#cororporate_foundation_new_website_div").hide();
            $(".add-website").remove();
            $(
                '#contact_corporatefoundation [name="website[]"]').val("")
        }else{
            $("#cororporate_foundation_new_website_div").show();
            $(
                '#contact_corporatefoundation [name="website[]"]').attr("readonly",false)
        }
    });
    noWebsite();
    function noWebsite(){
        if($(
            "#don_not_have_website").is(":checked"))$("#cororporate_foundation_new_website_div").hide();else $(
            "#cororporate_foundation_new_website_div").show()
    }
    noEmail();
    function noEmail(){
        if($("#corporate_no_email_address").is(
            ":checked")){
            $("#corporate_email_id").val("");
            $("#corporate_email_id").attr("readonly",true)
        }else $(
            "#corporate_email_id").attr("readonly",false);
        if($("#no_email_address").is(":checked")){
            $("#email_id").val("");
            $(
                "#email_id").attr("readonly",true)
        }else $("#email_id").attr("readonly",false)
    }
    function addressTypePrimary(id,index){
        if($
            ("#addressinfo_location_primary"+index).is(":checked"))$("#address_primary_index").val(index);else $(
            "#address_primary_index").val(1)
    }
    function addressTypeBilling(id,index){
        var primaryLocation=$("#primary_location"+index).
        val();
        if($("#addressinfo_location_billing"+index).is(":checked"))if(primaryLocation!="")$("#primary_location"+index).val
            (primaryLocation+",2");else $("#primary_location"+index).val("2");
        else if(primaryLocation!=""){
            var arr=primaryLocation.
            split(","),i=arr.indexOf("2");
            if(i!=-1)arr.splice(i,1);
            $("#primary_location"+index).val(arr)
        }
    }
    function 
    addressTypeShipping(id,index){
        var primaryLocation=$("#primary_location"+index).val();
        if($(
            "#addressinfo_location_shipping"+index).is(":checked"))if(primaryLocation!="")$("#primary_location"+index).val(
            primaryLocation+",3");else $("#primary_location"+index).val("3");
        else if(primaryLocation!=""){
            var arr=primaryLocation.
            split(","),i=arr.indexOf("3");
            if(i!=-1)arr.splice(i,1);
            $("#primary_location"+index).val(arr)
        }
    }
    function 
    corporateAddressTypePrimary(id,index){
        if($("#corporate_addressinfo_location_primary"+index).is(":checked"))$(
            "#corporate_address_primary_index").val(index);else $("#corporate_address_primary_index").val(1)
    }
    function 
    corporateAddressTypeBilling(id,index){
        var primaryLocation=$("#corporate_primary_location"+index).val();
        if($(
            "#corporate_addressinfo_location_billing"+index).is(":checked"))if(primaryLocation!="")$("#corporate_primary_location"+
            index).val(primaryLocation+",2");else $("#corporate_primary_location"+index).val("2");
        else if(primaryLocation!=""){
            var 
            arr=primaryLocation.split(","),i=arr.indexOf("2");
            if(i!=-1)arr.splice(i,1);
            $("#corporate_primary_location"+index).val(
                arr)
        }
    }
    function corporateAddressTypeShipping(id,index){
        var primaryLocation=$("#corporate_primary_location"+index).val();
        if($("#corporate_addressinfo_location_shipping"+index).is(":checked"))if(primaryLocation!="")$(
            "#corporate_primary_location"+index).val(primaryLocation+",3");else $("#corporate_primary_location"+index).val("3");
        else
        if(primaryLocation!=""){
            var arr=primaryLocation.split(","),i=arr.indexOf("3");
            if(i!=-1)arr.splice(i,1);
            $(
                "#corporate_primary_location"+index).val(arr)
        }
    }
    function loadIndividualContact(eleId,val,userType){
        if($("#"+eleId).is(
            ":checked")){
            if(userType=="corporate"){
                $("#corporateAddressRow"+val).hide();
                $("#corporate_load_contact_section_"+val).
                show()
            }else{
                $("#load_contact_section_"+val).show();
                $("#addressRow"+val).hide()
            }
            commonAutoComplete(val,userType)
        }else if(
            userType=="corporate"){
            $("#corporateAddressRow"+val).show();
            $("#corporate_load_contact_section_"+val).hide()
        }else{
            $(
                "#addressRow"+val).show();
            $("#load_contact_section_"+val).hide()
        }
    }
    function commonAutoComplete(val,userType){
        $(
            ".search-icon").autocomplete({
            afterAdd:true,
            selectFirst:true,
            autoFocus:true,
            source:function(request,response){
                $.ajax({
                    url:"/get-contacts",
                    method:"POST",
                    dataType:"json",
                    data:{
                        user_email:request.term
                    },
                    success:function(jsonResult){
                        if(!
                            checkUserAuthenticationAjx(jsonResult))return false;
                        var resultset=[];
                        $.each(jsonResult,function(){
                            var id=this.user_id,
                            value=this.full_name,address_one=this.street_address_one,address_two=this.street_address_two,city=this.city,state=this.
                            state,zip_code=this.zip_code,country_id=this.country_id,country_name=this.country_name;
                            resultset.push({
                                id:this.user_id,
                                value:this.full_name,
                                address_one:this.street_address_one,
                                address_two:this.street_address_two,
                                city:this.city,
                                state:this.
                                state,
                                zip_code:this.zip_code,
                                country_id:this.country_id,
                                country_name:this.country_name
                            })
                        });
                        response(resultset)
                    }
                })
            },
            open:
            function(){
                $(".ui-menu").width(350)
            },
            change:function(event,ui){
                if(ui.item)if(userType=="corporate"){
                    $(
                        "#corporate_load_contact_section_"+val+"  input#corporate_address_search_contact"+val).val("");
                    $("#corporateAddressRow"+
                        val+" input#corporate_addressinfo_street_address_1").val(ui.item.address_one);
                    $("#corporateAddressRow"+val+
                        " input#corporate_addressinfo_street_address_2").val(ui.item.address_two);
                    $("#corporateAddressRow"+val+
                        " input#corporate_addressinfo_city").val(ui.item.city);
                    $("#corporateAddressRow"+val+
                        " input#corporate_addressinfo_state_"+val).val(ui.item.state);
                    $("#corporateAddressRow"+val+
                        " input#corporate_address_state_id_"+val).val(ui.item.state);
                    $("#corporateAddressRow"+val+
                        " input#corporate_address_state_id_"+val).val(ui.item.state).select2();
                    $("#corporateAddressRow"+val+
                        " input#corporate_addressinfo_zip").val(ui.item.zip_code);
                    $("#corporateAddressRow"+val+" select#"+val).val(ui.item.
                        country_id).select2();
                    $("#corporate_load_contact_section_"+val+"").hide();
                    $("#corporateAddressRow"+val+"").show();
                    if(ui.
                        item.country_id=="228"){
                        $("#corporate_state_"+val).hide();
                        $("#corporate_usa_state_"+val).show()
                    }
                }else{
                    $(
                        "#addressRowSearchContact"+val+"  input#address_search_contact"+val).val("");
                    $("#addressRow"+val+
                        " input#addressinfo_street_address_1").val(ui.item.address_one);
                    $("#addressRow"+val+
                        " input#addressinfo_street_address_2").val(ui.item.address_two);
                    $("#addressRow"+val+" input#addressinfo_city").val(ui.
                        item.city);
                    $("#addressRow"+val+" input#addressinfo_state_"+val).val(ui.item.state);
                    $("#addressRow"+val+
                        " input#address_state_id_"+val).val(ui.item.state);
                    $("#addressRow"+val+" input#address_state_id_"+val).val(ui.item.state
                        ).select2();
                    $("#addressRow"+val+" input#addressinfo_zip").val(ui.item.zip_code);
                    $("#addressRow"+val+" select#"+val).val(
                        ui.item.country_id).select2();
                    $("#load_contact_section_"+val+"").hide();
                    $("#addressRow"+val+"").show();
                    if(ui.item.
                        country_id=="228"){
                        $("#state_"+val).hide();
                        $("#usa_state_"+val).show()
                    }
                }else{
                    $("#addressinfo_search_contact").val("");
                    $(
                        "#corporate_addressinfo_search_contact").val("")
                }
            },
            focus:function(event,ui){
                return false
            },
            select:function(event,ui){
                if(
                    userType=="corporate"){
                    $("#corporate_load_contact_section_"+val+"  input#corporate_address_search_contact"+val).val("");
                    $("#corporateAddressRow"+val+" input#corporate_addressinfo_street_address_1").val(ui.item.address_one);
                    $(
                        "#corporateAddressRow"+val+" input#corporate_addressinfo_street_address_2").val(ui.item.address_two);
                    $(
                        "#corporateAddressRow"+val+" input#corporate_addressinfo_city").val(ui.item.city);
                    $("#corporateAddressRow"+val+
                        " input#corporate_addressinfo_state_"+val).val(ui.item.state);
                    $("#corporateAddressRow"+val+
                        " input#corporate_address_state_id_"+val).val(ui.item.state);
                    $("#corporateAddressRow"+val+
                        " input#corporate_address_state_id_"+val).val(ui.item.state).select2();
                    $("#corporateAddressRow"+val+
                        " input#corporate_addressinfo_zip").val(ui.item.zip_code);
                    $("#corporateAddressRow"+val+" select#"+val).val(ui.item.
                        country_id).select2();
                    $("#corporate_load_contact_section_"+val+"").hide();
                    $("#corporateAddressRow"+val+"").show();
                    if(ui.
                        item.country_id=="228"){
                        $("#corporate_state_"+val).hide();
                        $("#corporate_usa_state_"+val).show()
                    }
                }else{
                    $(
                        "#addressRowSearchContact"+val+"  input#address_search_contact"+val).val("");
                    $("#addressRow"+val+
                        " input#addressinfo_street_address_1").val(ui.item.address_one);
                    $("#addressRow"+val+
                        " input#addressinfo_street_address_2").val(ui.item.address_two);
                    $("#addressRow"+val+" input#addressinfo_city").val(ui.
                        item.city);
                    $("#addressRow"+val+" input#addressinfo_state_"+val).val(ui.item.state);
                    $("#addressRow"+val+
                        " input#address_state_id_"+val).val(ui.item.state);
                    $("#addressRow"+val+" input#address_state_id_"+val).val(ui.item.state
                        ).select2();
                    $("#addressRow"+val+" input#addressinfo_zip").val(ui.item.zip_code);
                    $("#addressRow"+val+" select#"+val).val(
                        ui.item.country_id).select2();
                    $("#load_contact_section_"+val+"").hide();
                    $("#addressRow"+val+"").show();
                    if(ui.item.
                        country_id=="228"){
                        $("#state_"+val).hide();
                        $("#usa_state_"+val).show()
                    }
                }
            }
        })
    }
    function checkPrimary(value){
        $(
            "#primary-phone").val(value)
    }
    function corporateCheckPrimary(value){
        $("#corporate-primary-phone").val(value)
    }
    $(function()

    {
            $("#addbutton").click(function(){
                var tagsandgroups_groups_to=document.getElementById("tagsandgroups_groups_to");
                $(
                    "#tagsandgroups_groups_from option:selected").each(function(){
                    $("#tagsandgroups_groups_to").append("<option value='"+$(
                        this).val()+"'>"+$(this).text()+"</option>");
                    $(this).remove();
                    for(var i=0;i<tagsandgroups_groups_to.options.length;i++)
                        tagsandgroups_groups_to.options[i].selected=true
                })
            });
            $("#removebutton").click(function(){
                var tagsandgroups_groups_to=
                document.getElementById("tagsandgroups_groups_to");
                $("#tagsandgroups_groups_to option:selected").each(function(){
                    $(
                        "#tagsandgroups_groups_from").append("<option value='"+$(this).val()+"'>"+$(this).text()+"</option>");
                    $(this).remove();
                    for(var i=0;i<tagsandgroups_groups_to.options.length;i++)tagsandgroups_groups_to.options[i].selected=true
                })
            });
            $(
                "#cor_addbutton").click(function(){
                var cor_tagsandgroups_groups_to=document.getElementById("cor_tagsandgroups_groups_to"
                    );
                $("#cor_tagsandgroups_groups_from option:selected").each(function(){
                    $("#cor_tagsandgroups_groups_to").append(
                        "<option value='"+$(this).val()+"'>"+$(this).text()+"</option>");
                    $(this).remove();
                    for(var i=0;i<
                        cor_tagsandgroups_groups_to.options.length;i++)cor_tagsandgroups_groups_to.options[i].selected=true
                })
            });
            $(
                "#cor_removebutton").click(function(){
                var cor_tagsandgroups_groups_to=document.getElementById(
                    "cor_tagsandgroups_groups_to");
                $("#cor_tagsandgroups_groups_to option:selected").each(function(){
                    $(
                        "#cor_tagsandgroups_groups_from").append("<option value='"+$(this).val()+"'>"+$(this).text()+"</option>");
                    $(this).remove
                    ();
                    for(var i=0;i<cor_tagsandgroups_groups_to.options.length;i++)cor_tagsandgroups_groups_to.options[i].selected=true
                })
            })
        });
    function selectUsaState(value,id,userType){
        if(userType=="corporate")if(value=="228"){
            $("#corporate_usa_state_"+id).
            show();
            $("#corporate_state_"+id).hide();
            $("#corporate_address_state_id_"+id).val("").select2();
            $(
                "#corporate_addressinfo_state_"+id).val("")
        }else{
            $("#corporate_state_"+id).show();
            $("#corporate_usa_state_"+id).hide();
            $
            ("#corporate_address_state_id_"+id).val("").select2();
            $("#corporate_addressinfo_state_"+id).val("")
        }else if(value=="228"
            ){
            $("#usa_state_"+id).show();
            $("#state_"+id).hide();
            $("#address_state_id_"+id).val("").select2();
            $("#addressinfo_state_"
                +id).val("")
        }else{
            $("#state_"+id).show();
            $("#usa_state_"+id).hide();
            $("#address_state_id_"+id).val("").select2();
            $(
                "#addressinfo_state_"+id).val("")
        }
    }
    function assignStateValue(value,id,userType){
        if(userType=="corporate"){
            if(value){
                var 
                extractStateId=id.split("_");
                $("#corporate_addressinfo_state_"+extractStateId[4]).val(value)
            }
        }else if(value){
            var 
            extractStateId=id.split("_");
            $("#addressinfo_state_"+extractStateId[3]).val(value)
        }
    }
    function enableApoPo(id,value){
        if($(
            "#addressinfo_location_shipping"+id).is(":checked")){
            $("#apoid"+id).show();
            var radios=$('input:radio[name="is_apo_po['+
                id+']"]');
            $.each(radios,function(){
                $(this).next().removeClass("checked")
            });
            if(value==""||value==0)fieldval=1;else 
                fieldval=value;
            radios.filter('[value="'+fieldval+'"]').click();
            radios.filter('[value="'+fieldval+'"]').next().addClass(
                "checked")
        }else{
            $("#address_country_id_"+id).val("").select2();
            // $("#addressinfo_state_"+id).val("");
            $(
                "#address_state_apo_"+id).val("").select2();
            $("#address_state_id_"+id).val("").select2();
            $("#addressinfo_city"+id).
            removeAttr("readOnly");
            $("#apoid"+id).hide();
            $("#apo_country_"+id).hide();
            $("#country_"+id).show();
            $("#state_"+id).show(
                );
            $("#apo_state_"+id).hide();
            $("#usa_state_"+id).hide()
        }
    }
    function setApoPostateCountry(id,value){
        if(value!=1){
            $(
                "#apo_country_"+id).show();
            $("#country_"+id).hide();
            $("#apo_state_"+id).hide();
            $("#usa_state_"+id).hide();
            if(value==2){
                $
                ("#state_"+id).hide();
                $("#usa_state_"+id).show();
                $("#apo_state_"+id).hide();
                if($("#old_addressinfo_city"+id).val()!="")$
                    ("#addressinfo_city"+id).val($("#old_addressinfo_city"+id).val());
                $("#addressinfo_city"+id).removeAttr("readOnly");
                if($(
                    "#old_address_type_"+id).val()==value)if($("#old_addressinfo_state_"+id).val()!="")$("#address_state_id_"+id).val($(
                    "#old_addressinfo_state_"+id).val()).select2();else $("#address_state_id_"+id).val("").select2();else $(
                    "#address_state_id_"+id).val("").select2()
            }else if(value==3){
                $("#apo_state_"+id).show();
                $("#state_"+id).hide();
                $(
                    "#usa_state_"+id).hide();
                $("#addressinfo_city"+id).val("APO");
                $("#addressinfo_city"+id).prop("readOnly","readOnly")
            }
        }
        else{
            if($("#old_addressinfo_state_"+id).val()!="")if($("#old_addressinfo_country_"+id).val()==228){
                $("#usa_state_"+id).
                show();
                $("#state_"+id).hide();
                $("#address_state_id_"+id).val($("#old_addressinfo_state_"+id).val()).select2()
            }else{
                $(
                    "#usa_state_"+id).hide();
                $("#state_"+id).show();
                $("#addressinfo_state_"+id).val($("#old_addressinfo_state_"+id).val())
            }
            else{
                $("#usa_state_"+id).hide();
                $("#state_"+id).show();
                $("#addressinfo_state_"+id).val("")
            }
            $("#address_country_id_"+id).
            val("").select2();
            $("#apo_country_"+id).hide();
            $("#country_"+id).show();
            $("#apo_state_"+id).hide();
            if($(
                "#old_addressinfo_city"+id).val()!="")$("#addressinfo_city"+id).val($("#old_addressinfo_city"+id).val());
            $(
                "#addressinfo_city"+id).removeAttr("readOnly")
        }
    }
    function corporateEnableApoPo(id,value){
        if($(
            "#corporate_addressinfo_location_shipping"+id).is(":checked")){
            $("#corporate_apoid"+id).show();
            var radios=$(
                'input:radio[name="corporate_is_apo_po['+id+']"]');
            $.each(radios,function(){
                $(this).next().removeClass("checked")
            });
            if(
                value==""||value==0)fieldval=1;else fieldval=value;
            radios.filter('[value="'+fieldval+'"]').click();
            radios.filter(
                '[value="'+fieldval+'"]').next().addClass("checked")
        }else{
            $("#corporate_address_country_id_"+id).val("").select2();
            // $("#corporate_addressinfo_state_"+id).val("");
            $("#corporate_address_state_apo_"+id).val("").select2();
            $(
                "#corporate_address_state_id_"+id).val("").select2();
            $("#corporate_addressinfo_city"+id).removeAttr("readOnly");
            $(
                "#corporate_apoid"+id).hide();
            $("#corporate_apo_country_"+id).hide();
            $("#corporate_country_"+id).show();
            $(
                "#corporate_state_"+id).show();
            $("#corporate_apo_state_"+id).hide();
            $("#corporate_usa_state_"+id).hide()
        }
    }
    function 
    corporateSetApoPostateCountry(id,value){
        if(value!=1){
            $("#corporate_apo_country_"+id).show();
            $("#corporate_country_"+id).
            hide();
            $("#corporate_apo_state_"+id).hide();
            $("#corporate_usa_state_"+id).hide();
            if(value==2){
                $("#corporate_state_"+id).
                hide();
                $("#corporate_usa_state_"+id).show();
                $("#corporate_apo_state_"+id).hide();
                if($("#corporate_old_addressinfo_city"+
                    id).val()!="")$("#corporate_addressinfo_city"+id).val($("#corporate_old_addressinfo_city"+id).val());
                $(
                    "#corporate_addressinfo_city"+id).removeAttr("readOnly");
                if($("#corporate_old_address_type_"+id).val()==value)if($(
                    "#corporate_old_addressinfo_state_"+id).val()!="")$("#corporate_address_state_id_"+id).val($(
                    "#corporate_old_addressinfo_state_"+id).val()).select2();else $("#corporate_address_state_id_"+id).val("").select2();
                else $("#corporate_address_state_id_"+id).val("").select2()
            }else if(value==3){
                $("#corporate_apo_state_"+id).show();
                $(
                    "#corporate_state_"+id).hide();
                $("#corporate_usa_state_"+id).hide();
                $("#corporate_addressinfo_city"+id).val("APO");
                $(
                    "#corporate_addressinfo_city"+id).prop("readOnly","readOnly")
            }
        }else{
            if($("#corporate_old_addressinfo_state_"+id).val()!=
                "")if($("#corporate_old_addressinfo_country_"+id).val()==228){
                $("#corporate_usa_state_"+id).show();
                $("#corporate_state_"
                    +id).hide();
                $("#corporate_address_state_id_"+id).val($("#corporate_old_addressinfo_state_"+id).val()).select2()
            }else{
                $(
                    "#corporate_usa_state_"+id).hide();
                $("#corporate_state_"+id).show();
                $("#corporate_addressinfo_state_"+id).val($(
                    "#corporate_old_addressinfo_state_"+id).val())
            }else{
                $("#corporate_usa_state_"+id).hide();
                $("#corporate_state_"+id).show(
                    );
                $("#corporate_addressinfo_state_"+id).val("")
            }
            $("#corporate_address_country_id_"+id).val("").select2();
            $(
                "#corporate_apo_country_"+id).hide();
            $("#corporate_country_"+id).show();
            $("#corporate_apo_state_"+id).hide();
            if($(
                "#corporate_old_addressinfo_city"+id).val()!="")$("#corporate_addressinfo_city"+id).val($(
                "#corporate_old_addressinfo_city"+id).val());
            $("#corporate_addressinfo_city"+id).removeAttr("readOnly")
        }
    }