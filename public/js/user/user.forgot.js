$(function(){$("#cboxClose").click(function(){parent.jQuery.colorbox.close();jQuery.colorbox.close();parent.window.
location.href="/"});$("#forgot").validate({submitHandler:function(){showAjxLoader();var dataStr=$("#forgot").serialize()
;$.ajax({type:"POST",url:"/forgot",data:dataStr,success:function(responseData){var jsonObj=jQuery.parseJSON(responseData
);hideAjxLoader();if(jsonObj.status=="success"){$("#error_message").hide();$("#success_message").show();$(
"#success_message").html(jsonObj.message);$("#submitbutton").attr("disabled","disabled");$("#submitbutton").addClass(
"disable-button");setTimeout(function(){parent.jQuery.colorbox.close();jQuery.colorbox.close()},4000)}else{$(
"#error_message").show();$("#success_message").hide();$("#error_message").html(jsonObj.message);$("#submitbutton").
removeAttr("disabled");$("#submitbutton").removeClass("disable-button")}}})},rules:{email_id:{required:true,email:true}}
,messages:{email_id:{required:EMAIL_EMPTY,email:EMAIL_INVALID}}});$.validator.addMethod("alphanumeric",function(value,
element){return this.optional(element)||/^(?=.*\d)(?=.*[a-z]).{6,25}$/i.test(value)},PASSWORD_INVALID);$(
"#password-reset").validate({submitHandler:function(){showAjxLoader();var dataStr=$("#password-reset").serialize();$.
ajax({type:"POST",url:"/password",data:dataStr,success:function(responseData){var jsonObj=jQuery.parseJSON(responseData)
;hideAjxLoader();if(jsonObj.status=="success"){$("#error_message").hide();$("#success_message").show();$(
"#success_message").html(jsonObj.message);$("#submitbutton").attr("disabled","disabled");$("#submitbutton").addClass(
"disable-button");setTimeout(function(){parent.jQuery.colorbox.close();jQuery.colorbox.close();parent.window.location=
"/"},4000)}else{$("#error_message").show();$("#success_message").hide();$("#error_message").html(jsonObj.message);$(
"#submitbutton").removeAttr("disabled");$("#submitbutton").removeClass("disable-button")}}})},rules:{email_id:{required:
true,email:true},password:{required:true,minlength:6,maxlength:25,alphanumeric:true},password_conf:{required:true,
equalTo:"#password-reset #password"}},messages:{email_id:{required:EMAIL_EMPTY,email:EMAIL_INVALID},password:{required:
PASSWORD_EMPTY},password_conf:{required:PASSWORD_CONFIRM_EMPTY,equalTo:PASSWORD_CONFIRM_NOT_MATCHED}}})})