$(function(){var grid=jQuery("#realationship_list"),emptyMsgDiv=$('<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>"
);$("#realationship_list").jqGrid({postData:{},mtype:"POST",url:"/user-relationships/"+user_id,datatype:"json",colNames:
[L_RELATION,L_EMAIL,L_CITY,L_STATE,L_ACTION],colModel:[{name:L_RELATION,sortable:false},{name:L_EMAIL,sortable:false},{
name:L_CITY,index:"address.city"},{name:L_STATE,index:"address.state"},{name:L_ACTION,sortable:false}],viewrecords:true,
sortname:"usr_relation.added_date",sortorder:"desc",rowNum:10,rowList:[10,20,30],pager:"#realationship_pcrud",
viewrecords:true,autowidth:true,shrinkToFit:true,caption:"",width:"100%",cmTemplate:{title:false},loadComplete:function(
){var count=grid.getGridParam(),ts=grid[0];if(ts.p.reccount===0){grid.hide();emptyMsgDiv.show();$(
"#realationship_pcrud_right div.ui-paging-info").css("display","none")}else{grid.show();emptyMsgDiv.hide();$(
"#realationship_pcrud_right div.ui-paging-info").css("display","block")}}});emptyMsgDiv.insertAfter(grid.parent());
emptyMsgDiv.hide();$("#realationship_list").jqGrid("navGrid","#realationship_pcrud",{reload:true,edit:false,add:false,
search:false,del:false})});function deleteRelationship(relationship_id){$(".delete_relationship").colorbox({width:
"500px",height:"200px",inline:true});$("#no_delete").click(function(){$.colorbox.close();$("#yes_delete").unbind("click"
);return false});$("#yes_delete").click(function(){$("#yes_delete").unbind("click");$.ajax({type:"POST",data:{
relationship_id:relationship_id},url:"/delete-user-relationship",success:function(response){if(!
checkUserAuthenticationAjx(response))return false;var jsonObj=jQuery.parseJSON(response);if(jsonObj.status=="success"){$
("#success_message").html(jsonObj.message);location.reload()}}})})}