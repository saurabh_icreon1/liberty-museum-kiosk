$(function(){$.validator.addMethod("alphabetic",function(value,element){return this.optional(element)||/^[A-Za-z ]+$/i.
test(value)},VALID_DETAILS);$("#user_role").validate({submitHandler:function(){var dataStr=$("#user_role").serialize();
showAjxLoader();$.ajax({type:"POST",url:"/create-crm-user-role",data:dataStr,success:function(responseData){if(!
checkUserAuthenticationAjx(responseData))return false;var jsonObj=jQuery.parseJSON(responseData);if(jsonObj.status==
"success")window.location.href="/create-crm-user-role";else $.each(jsonObj.message,function(i,msg){$("#"+i).after(
'<label class="error" style="display:block;">'+msg+"</label>")});hideAjxLoader()}})},highlight:function(label){$(
".detail-section").css("display","block")},rules:{role_name:{required:true,alphabetic:true,minlength:1,maxlength:50}},
messages:{role_name:{required:EMPTY_ROLE_NAME}}});var grid=jQuery("#list"),emptyMsgDiv=$('<div class="no-record-msz">'+
NO_RECORD_FOUND+"</div>");$("#list").jqGrid({mtype:"POST",url:"/crm-user-roles",datatype:"json",sortable:true,colNames:[
"ID","Role Name","Action(s)"],colModel:[{name:"ID",index:"role_id",hidden:true,key:true},{name:"Role Name",index:
"role_name",sortable:true,editable:true},{name:"Action(s)",index:"action",sortable:false}],viewrecords:true,sortname:
"modified_date",sortorder:"DESC",rowNum:10,rowList:[10,20,30],pager:"#pcrud",autowidth:true,shrinkToFit:true,caption:"",
width:"100%",cmTemplate:{title:false},editurl:"/edit-crm-user-role",loadComplete:function(){var count=grid.getGridParam(
),ts=grid[0];if(ts.p.reccount===0){grid.hide();emptyMsgDiv.show();$("#pcrud_right div.ui-paging-info").css("display",
"none")}else{grid.show();emptyMsgDiv.hide();$("#pcrud_right div.ui-paging-info").css("display","block")}}});emptyMsgDiv.
insertAfter(grid.parent());emptyMsgDiv.hide();$("#list").jqGrid("navGrid","#pcrud",{reload:true,edit:false,add:false,
search:false,del:false});jQuery("#list").jqGrid("inlineNav","#pcrud")});function deleteRole(id){$(".delete_role").
colorbox({width:"700px",height:"180px",inline:true});$("#no_delete_role").click(function(){$.colorbox.close();$(
"#yes_delete_role").unbind("click");return false});$("#yes_delete_role").click(function(){$.ajax({url:
"/delete-crm-user-role",method:"POST",dataType:"json",data:{role_id:id},success:function(jsonResult){if(!
checkUserAuthenticationAjx(jsonResult))return false;location.reload()}})})}