function addNewPhone(currAnchor){
    var allDivBlock=$("div[id^='phonecontainer']"),i=$("#count_div").text();
    $(
        "#new_phoneno_div").remove();
    if(allDivBlock.length==1){
        var param="'.container"+i+"','"+i+
        "','individual','individual_phone'",removeDiv='<div id="phone_addMinus'+i+'"><a onclick="remove_dynamic_contacts('+param
        +')" class="minus m-r-5"></a></div>';
        $(currAnchor).parent().append(removeDiv)
        }
        $("<div />",{
        "class":"row container"+i,
        id:
        "phonecontainer_"+i
        }).appendTo("#phoneno_div");
    $(".container"+i).load("phone-no/"+i+"/container/primary_Div");
    i++;
    $(
        "#count_div").html(i)
    }
    function remove_dynamic_contacts(classname,i,userType,sectionName){
    if(userType=="corporate"){
        var 
        selectClass=i,selectedPrimary=$("#corporate-primary-phone").val();
        if(selectedPrimary==selectClass){
            $(
                '#contact_corporatefoundation [name="corporate_continfo_primary[]"]').prop("checked",false);
            $(
                '#contact_corporatefoundation [name="corporate_continfo_primary[]"]').each(function(index){
                if($(
                    "#corporate_continfo_primary"+index).next().attr("class")=="checked"&&index==i)$("#corporate_continfo_primary"+index).
                    next().removeClass("checked");
                $("#corporate_continfo_primary0").next().addClass("checked");
                if(index==0)$(
                    "#corporate_continfo_primary0").prop("checked",true)
                    });
            $("#corporate-primary-phone").val("0")
            }else if(selectedPrimary>
            selectClass){
            var assignPrimary=parseInt(selectedPrimary)-parseInt(1);
            $("#corporate-primary-phone").val(assignPrimary)
            }
        else{}
    }else{
    var selectClass=i,selectedPrimary=$("#primary-phone").val();
    if(selectedPrimary==selectClass){
        $(
            '#contact_individual [name="continfo_primary[]"]').prop("checked",false);
        $(
            '#contact_individual [name="continfo_primary[]"]').each(function(index){
            if($("#continfo_primary"+index).next().attr(
                "class")=="checked"&&index==i)$("#continfo_primary"+index).next().removeClass("checked");
            $("#continfo_primary0").next().
            addClass("checked");
            if(index==0)$("#continfo_primary0").prop("checked",true)
                });
        $("#primary-phone").val("0")
        }else if(
        selectedPrimary>selectClass){
        var assignPrimary=parseInt(selectedPrimary)-parseInt(1);
        $("#primary-phone").val(
            assignPrimary)
        }else{}
}
var plusEle=$(classname).find(".plus");
$(classname).remove();
var classDivId=classname.substring(1,
    classname.lastIndexOf("_")+1),lastBlock=$("div[id^='"+classDivId+"']").last();
if($(lastBlock).find(".plus").length==0){
    var plusId=$(plusEle).attr("id");
    $(lastBlock).find(".addMinus").append('<a href="javascript:void(0);" id="'+plusId+
        '" class="plus m-l-5"></a>')
    }
    var allDivBlock=$("div[id^='"+classDivId+"']");
if(allDivBlock.length==1)$(lastBlock).find(
    ".minus").remove()
    }
    function remove_dynamic_address(classname,i){
    var plusEle=$(classname).find(".plus");
    $(classname).
    remove();
    var classDivId=classname.substring(1,classname.lastIndexOf("_")+1),lastBlock=$("div[id^='"+classDivId+"']").
    last();
    if($(lastBlock).find(".plus").length==0){
        var plusId=$(plusEle).attr("id");
        $(lastBlock).find(".addMinus").prepend(
            '<a href="javascript:void(0);" id="'+plusId+'" class="plus right m-r-5"></a>')
        }
        var allDivBlock=$("div[id^='"+classDivId+
        "']");
    if(allDivBlock.length==1)$(lastBlock).find(".minus").remove()
        }
        $(function(){
    $("#submitbutton").click(function(){
        $(
            ".detail-section").css("display","block")
        });
    $("#corporate_submitbutton").click(function(){
        $(".detail-section").css(
            "display","block")
        });
    $("#demographics_ethnicity").change(function(){
        $("div[class^='demographics_ethnicity']").hide();
        if(
            $(".demographics_ethnicity"+this.value))$(".demographics_ethnicity"+this.value).show()
            });
    if($(".demographics_ethnicity"+
        $("#demographics_ethnicity").val()))$(".demographics_ethnicity"+$("#demographics_ethnicity").val()).show();
    $(
        "#demographics_nationality").change(function(){
        $("div[class^='demographics_nationality']").hide();
        if($(
            ".demographics_nationality"+this.value))$(".demographics_nationality"+this.value).show()
            });
    if($(
        ".demographics_nationality"+$("#demographics_nationality").val()))$(".demographics_nationality"+$(
        "#demographics_nationality").val()).show();
    $(document).on("change",'select[name="edu_degree_level[]"]',function(){
        $(
            "div[class^="+this.id+"]").hide();
        if(this.value!="")$("."+this.id+this.value).show();else $("."+this.id+this.value).hide
            ()
            });
    $(document).on("change",'select[name="addressinfo_location_type[]"]',function(){
        $("div[class^="+this.id+"]").hide()
        ;
        if(this.value!="")$("."+this.id+this.value).show();else $("."+this.id+this.value).hide()
            });
    $(document).on("change",
        'select[name="corporate_addressinfo_location_type[]"]',function(){
            $("div[class^="+this.id+"]").hide();
            if(this.value!="")
                $("."+this.id+this.value).show();else $("."+this.id+this.value).hide()
                });
    $("#communicationpref_email_greetings").val($(
        "#communicationpref_email_greetings option:eq(1)").val());
    $("#communicationpref_postal_greetings").val($(
        "#communicationpref_postal_greetings option:eq(1)").val());
    $("#communicationpref_address").val($(
        "#communicationpref_address option:eq(1)").val());
    $("#corporate_communicationpref_address").val($(
        "#corporate_communicationpref_address option:eq(1)").val());
    $("#tagsandgroups_company_name").tagit({
        availableTags:
        user_tags,
        singleField:true,
        allowSpaces:true,
        singleFieldNode:$("#tagsandgroups_company_name"),
        beforeTagAdded:function(
            event,ui){
            var indexObj=-1,contactTag=ui.tagLabel,tags=$("#tag_id").val(),newTag;
            if(tags!="")newTag=tags+","+contactTag;
            else newTag=contactTag;
            $("#tag_id").val(newTag)
            },
        beforeTagRemoved:function(event,ui){
            var indexObj=-1,contactTag=ui.
            tagLabel,tags=$("#tag_id").val();
            if(tags!=""){
                var newTag=tags.replace(contactTag,"");
                $("#tag_id").val(newTag)
                }
            }
    });
$(
    "#corporate_tagsandgroups_company_name").tagit({
    availableTags:user_tags,
    singleField:true,
    singleFieldNode:$(
        "#corporate_tagsandgroups_company_name"),
    beforeTagAdded:function(event,ui){
        var indexObj=-1,contactTag=ui.tagLabel,tags=$
        ("#corporate_tag_id").val(),newTag;
        if(tags!="")newTag=tags+","+contactTag;else newTag=contactTag;
        $("#corporate_tag_id").
        val(newTag)
        },
    beforeTagRemoved:function(event,ui){
        var indexObj=-1,contactTag=ui.tagLabel,tags=$("#corporate_tag_id").val(
            );
        if(tags!=""){
            var newTag=tags.replace(contactTag,"");
            $("#corporate_tag_id").val(newTag)
            }
        }
});
var url=
"/upload-user-thumbnail";
$("#corporate_upload_profile_image").fileupload({
    url:url,
    dataType:"json",
    done:function(e,data){
        data=data.result;
        if(data.status=="success"){
            $("#filemessage_cor").addClass("success-msg");
            $("#filemessage_cor").html(
                data.message);
            $("#filemessage_cor").removeClass("error-msg");
            $("#corporate_upload_profile_image_id").val(data.filename)
            }
        else{
            $("#filemessage_cor").removeClass("success-msg");
            $("#filemessage_cor").addClass("error-msg");
            $("#filemessage_cor").
            html(data.message);
            $("#corporate_upload_profile_image_id").val("")
            }
        },
start:function(e){
    showAjxLoader()
    },
stop:function(e)

{
    hideAjxLoader()
    }
});
$("#upload_profile_image").fileupload({
    url:url,
    dataType:"json",
    done:function(e,data){
        data=data.
        result;
        if(data.status=="success"){
            $("#filemessage").addClass("success-msg");
            $("#filemessage").html(data.message);
            $(
                "#filemessage").removeClass("error-msg");
            $("#upload_profile_image_id").val(data.filename)
            }else{
            $("#filemessage").
            removeClass("success-msg");
            $("#filemessage").addClass("error-msg");
            $("#filemessage").html(data.message);
            $(
                "#upload_profile_image_id").val("")
            }
        },
start:function(e){
    showAjxLoader()
    },
stop:function(e){
    hideAjxLoader()
    }
});
$(
    ".fileUploadAddMore").fileupload({
    url:"/upload-user-public-doc",
    dataType:"json",
    done:function(e,data){
        var file_id=$(this
            ).attr("id"),file_number=file_id.substr(file_id.length-1,1);
        data=data.result;
        if(data.status=="success"){
            $(
                "#filemessage_"+file_number).addClass("success-msg");
            $("#filemessage_"+file_number).html(data.message);
            $("#filemessage_"
                +file_number).removeClass("error-msg");
            $("#publicdocs_upload_file"+file_number).val(data.filename)
            }else{
            $(
                "#filemessage_"+file_number).removeClass("success-msg");
            $("#filemessage_"+file_number).addClass("error-msg");
            $(
                "#filemessage_"+file_number).html(data.message);
            $("#publicdocs_upload_file"+file_number).val("")
            }
        },
start:function(e){
    showAjxLoader()
    },
stop:function(e){
    hideAjxLoader()
    }
});
$(".fileUploadAddMoreCor").fileupload({
    url:
    "/upload-user-public-doc",
    dataType:"json",
    done:function(e,data){
        var file_id=$(this).attr("id"),file_number=file_id.
        substr(file_id.length-1,1);
        data=data.result;
        if(data.status=="success"){
            $("#cor_filemessage_"+file_number).addClass(
                "success-msg");
            $("#cor_filemessage_"+file_number).html(data.message);
            $("#cor_filemessage_"+file_number).removeClass(
                "error-msg");
            $("#cor_publicdocs_upload_file"+file_number).val(data.filename)
            }else{
            $("#cor_filemessage_"+file_number).
            removeClass("success-msg");
            $("#cor_filemessage_"+file_number).addClass("error-msg");
            $("#cor_filemessage_"+file_number).
            html(data.message);
            $("#cor_publicdocs_upload_file"+file_number).val("")
            }
        },
start:function(e){
    showAjxLoader()
    },
stop:
function(e){
    hideAjxLoader()
    }
});
jQuery.validator.addMethod("lettersandapostropheonly",function(value,element){
    return this
    .optional(element)||/^[a-z '\-]+$/i.test(value)},VALID_NAME);jQuery.validator.addMethod("lettersonly",function(value,
element){
    return this.optional(element)||/^[a-z ]+$/i.test(value)
    },VALID_NAME);
jQuery.validator.addMethod(
    "alphabetwithdot",function(value,element){
        return this.optional(element)||/^[a-z.']+$/i.test(value)},VALID_NAME);$.
    validator.addMethod("alphanumeric",function(value,element){
        return this.optional(element)||/^[a-z0-9]+$/i.test(value)
        },
    AFIHC_INVALID);
    jQuery.validator.addMethod("ethinicityalphabetic",function(value,element){
        return this.optional(element)||
        /^[a-z ]+$/i.test(value)
        },VALID_ETHINICITY);
    jQuery.validator.addMethod("nationalityalphabetic",function(value,element){
        return this.optional(element)||/^[a-z ]+$/i.test(value)
        },VALID_NATIONALITY);
    jQuery.validator.addMethod(
        "alphanumericspecialchar",function(value,element){
            return this.optional(element)||/^[A-Za-z0-9\r\s\n ,.-]+$/i.test(value)
        },"Enter valid details");
    jQuery.validator.addMethod("alphanumericspecialapostrophechar",function(value,element){
        return this.optional(element)||/^[A-Za-z0-9'\r\s\n ,.-]+$/i.test(value)},"Enter valid details");$.validator.prototype.
    checkForm=function(){
        this.prepareForm();
        for(var i=0,elements=this.currentElements=this.elements();elements[i];i++)if(
            this.findByName(elements[i].name).length!=undefined&&this.findByName(elements[i].name).length>1)for(var cnt=0;cnt<this.
            findByName(elements[i].name).length;cnt++)this.check(this.findByName(elements[i].name)[cnt]);else this.check(elements[i]
            );return this.valid()
        };
        
    var container=$("div.error_message");
        $("#contact_individual").validate({
        submitHandler:function(){
            var dataStr=$("#contact_individual").serialize();
            setTimeout("calltime()","100");
            if(checkCustomValidation(
                "contact_individual")){
                showAjxLoader();
                $.ajax({
                    type:"POST",
                    url:"/create-contact",
                    data:dataStr,
                    success:function(
                        responseData){
                        var jsonObj=jQuery.parseJSON(responseData);
                        hideAjxLoader();
                        if(!checkUserAuthenticationAjx(responseData))
                            return false;
                        if(jsonObj.status=="success")window.location.href="/contact";
                        else{
                            $("#success-message").hide();
                            $(
                                ".detail-section").css("display","block");
                            if(jsonObj.email_exits)$("#email_id").after(
                                '<label for="email_id" generated="true" class="error">'+jsonObj.message+"</label>");else $.each(jsonObj.message,function
                                (i,msg){
                                    $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                                    })
                            }
                            }
                })
        }
    },
    rules:{
        first_name:{
            required:true,
            minlength:1,
            maxlength:50,
            lettersandapostropheonly:true
        },
        middle_name:{
            minlength:1,
            maxlength:4,
            alphabetwithdot:true
        },
        last_name:{
            required:true,
            minlength:1,
            maxlength:20,
            lettersandapostropheonly:true
        },
        email_id:{
            required:function(){
                if($("#no_email_address").is(":checked"))return false;else return true
                    },
            email:true,
            minlength:6
        },
        afihc:{
            minlength:1,
            maxlength:50,
            alphanumeric:true
        },
        alt_name:{
            minlength:1,
            maxlength:50,
            lettersonly:true
        },
        alt_email:{
            email
            :true
        },
        alt_phoneno:{
            minlength:2,
            maxlength:10,
            number:true
        },
        alt_relationship:{
            minlength:1,
            maxlength:10,
            lettersonly:true
        },
        communicationpref_custome_email_greetings:{
            lettersonly:true,
            minlength:1,
            maxlength:50
        },
        communicationpref_custome_postal_greetings:{
            lettersonly:true,
            minlength:1,
            maxlength:50
        },
        communicationpref_custome_address_greetings:{
            lettersonly:true,
            minlength:1,
            maxlength:50
        },
        publicdocs_description:{
            minlength:2,
            maxlength:200 //,
           // alphanumericspecialchar:true
        },
        notes:{
            minlength:2,
            maxlength:200 //,
         //   alphanumericspecialchar:true
        }
    },
messages:{
    first_name:{
        required:FIRST_NAME_EMPTY,
        lettersonly:INVALID_NAME
    },
    last_name:{
        required:LAST_NAME_EMPTY
    },
    email_id:

    {
        required:EMAIL_EMPTY,
        email:EMAIL_INVALID
    },
    alt_name:{
        lettersonly:INVALID_NAME
    },
    alt_email:{
        email:EMAIL_INVALID
    },
    alt_phoneno:{
        number:PHONE_VALID
    }
},
highlight:function(label){
    $(".detail-section").css("display","block")
    }
});
$(
    "#contact_corporatefoundation").validate({
    submitHandler:function(){
        var dataStr=$("#contact_corporatefoundation").
        serialize();
        setTimeout("calltime()","100");
        if(checkCustomValidation("contact_corporatefoundation")){
            showAjxLoader();
            $.
            ajax({
                type:"POST",
                url:"/create-contact",
                data:dataStr,
                success:function(responseData){
                    hideAjxLoader();
                    if(!
                        checkUserAuthenticationAjx(responseData))return false;
                    var jsonObj=jQuery.parseJSON(responseData);
                    if(jsonObj.status==
                        "success")window.location.href="/contact";
                    else{
                        $("#success-message").hide();
                        $(".detail-section").css("display","block");
                        if(jsonObj.email_exits)$("#corporate_email_id").after('<label for="corporate_email_id" generated="true" class="error">'+
                            jsonObj.message+"</label>");else $.each(jsonObj.message,function(i,msg){
                            $("#"+i).after(
                                '<label class="error" style="display:block;">'+msg+"</label>")
                            })
                        }
                        }
            })
    }
},
rules:{
    corporate_company_name:{
        required:true,
        minlength:2,
        maxlength:50,
        alphanumericspecialapostrophechar:true
    },
    corporate_legal_name:{
        required:true,
        minlength:2,
        maxlength:40,
        alphanumericspecialapostrophechar:true
    },
    corporate_sic_code:{
        minlength:2,
        maxlength:10,
        alphanumericspecialchar:true
    },
    corporate_email_id:{
        required:function(){
            if($("#corporate_no_email_address").is(":checked")
                )return false;else return true
                },
        email:true,
        minlength:6
    },
    corporate_alt_email:{
        email:true,
        minlength:6
    },
    corporate_alt_phoneno:{
        number:true,
        minlength:2,
        maxlength:20
    },
    corporate_alt_relationship:{
        lettersonly:true,
        minlength:1,
        maxlength:20
    },
    corporate_communicationpref_custome_email_greetings:{
        lettersonly:true,
        minlength:1,
        maxlength:50
    },
    corporate_communicationpref_custome_postal_greetings:{
        lettersonly:true,
        minlength:1,
        maxlength:50
    },
    corporate_communicationpref_custome_address_greetings:{
        lettersonly:true,
        minlength:1,
        maxlength:50
    },
    corporate_publicdocs_description:{
        minlength:1,
        maxlength:200,
        alphanumericspecialchar:true
    },
    corporate_notes:{
        minlength:1,
        maxlength:200,
        alphanumericspecialchar:true
    }
},
messages:{
    corporate_company_name:{
        required:COMPANY_NAME_EMPTY
    },
    corporate_legal_name:{
        required:LEGAL_NAME_EMPTY
    },
    corporate_email_id:{
        required:EMAIL_EMPTY,
        email:EMAIL_INVALID
    },
    corporate_alt_email:{
        email:EMAIL_INVALID
    },
    corporate_alt_phoneno:{
        number:PHONE_VALID
    }
},
highlight:function(label){
    $(
        ".detail-section").css("display","block")
    }
});
$(document).on("click","#new_phoneno_div",function(){
    var i=$("#count_div").
    text(),allDivBlock=$("div[id^='container_']");
    if(allDivBlock.length==1){
        var parId=$(this).parents(
            "div[id^='container_']").attr("id"),id=parId.substring(parId.lastIndexOf("_")+1,parId.length);
        $(this).parent().append(
            "<a  class='minus' onclick=remove_dynamic_contacts('.container_"+id+"','"+id+"','individual')>")
        }
        $(this).remove();
    $(
        "<div />",{
            "class":"row container_"+i,
            id:"container_"+i
            }).appendTo("#phoneno_div");
    $(".container_"+i).load("phone-no/"+i
        +"/container_/primary_Div");
    i++;
    $("#count_div").html(i)
    });
$(document).on("click","#corp_new_phoneno_div",function(){
    var 
    i=$("#corp_count_div").text(),allDivBlock=$("div[id^='corporate_container_']");
    if(allDivBlock.length==1){
        var parId=$(
            this).parents("div[id^='corporate_container_']").attr("id"),id=parId.substring(parId.lastIndexOf("_")+1,parId.length);
        $(
            this).parent().append("<a  class='minus' onclick=remove_dynamic_contacts('.corporate_container_"+id+"','"+id+
            "','corporate')>")
        }
        $(this).remove();
    $("<div />",{
        "class":"row corporate_container_"+i,
        id:"corporate_container_"+i
        }).
    appendTo("#corp_phoneno_div");
    $(".corporate_container_"+i).load("corporate-phone-no/"+i+
        "/corporate_container_/corp_primary_Div");
    i++;
    $("#corp_count_div").html(i)
    });
$(document).on("click",
    "#cororporate_foundation_new_website_div",function(){
        var i=$("#cororporate_foundation_website_count_div").text(),
        allDivBlock=$("div[id^='cororporate_foundation_website_container_']");
        if(allDivBlock.length==1){
            var parId=$(this).
            parents("div[id^='cororporate_foundation_website_container_']").attr("id"),id=parId.substring(parId.lastIndexOf("_")+1,
                parId.length);
            $(this).parent().append(
                "<a  class='minus' onclick=remove_dynamic_contacts('.cororporate_foundation_website_container_"+id+"','"+id+"')>")
            }
            $(
            this).remove();
        $("<div />",{
            "class":"row add-website cororporate_foundation_website_container_"+i,
            id:
            "cororporate_foundation_website_container_"+i
            }).appendTo("#cororporate_foundation_website_div");
        $(
            ".cororporate_foundation_website_container_"+i).load("website/"+i+"/cororporate_foundation_website_container_");
        i++;
        $(
            "#cororporate_foundation_website_count_div").html(i)
        });
$(document).on("click","#new_address_div",function(){
    var i=$(
        "#count_address_div").text(),allDivBlock=$("div[id^='address_container_']");
    if(allDivBlock.length==1){
        var parId=$(this).
        parents("div[id^='address_container_']").attr("id"),id=parId.substring(parId.lastIndexOf("_")+1,parId.length);
        $(this).
        parent().append("<a  class='minus right' onclick=remove_dynamic_address('.address_container_"+id+"','"+id+"')>")
        }
        $(this)
    .remove();
    $("<div />",{
        "class":"addressAll address_container_"+i,
        id:"address_container_"+i
        }).appendTo(
        "#load_address_section");
    var parentDiv=$("#load_address_section");
    $(".address_container_"+i).load("address-form/"+i+
        "/address_container_");
    i++;
    $("#count_address_div").html(i);
    var location_count=parseInt($("#primary_location_count").val(
        ))+1;
    $("#primary_location_count").val(location_count)
    });
$(document).on("click","#corp_new_address_div",function(){
    var i=
    $("#corp_count_address_div").text(),allDivBlock=$("div[id^='corp_address_container_']");
    if(allDivBlock.length==1){
        var 
        parId=$(this).parents("div[id^='corp_address_container_']").attr("id"),id=parId.substring(parId.lastIndexOf("_")+1,parId
            .length);
        $(this).parent().append("<a  class='minus right' onclick=remove_dynamic_contacts('.corp_address_container_"+id+
            "','"+id+"')>")
        }
        $(this).remove();
    $("<div />",{
        "class":"addressAll corp_address_container_"+i,
        id:
        "corp_address_container_"+i
        }).appendTo("#corp_load_address_section");
    $(".corp_address_container_"+i).load(
        "corporate-address-form/"+i+"/corp_address_container_");
    i++;
    $("#corp_count_address_div").html(i);
    var location_count=
    parseInt($("#corporate_primary_location_count").val())+1;
    $("#corporate_primary_location_count").val(location_count)
    });
$(
    document).on("click","#add_new_edu",function(){
    var i=$("#edu_count_div").text(),allDivBlock=$(
        "div[id^='education_container_']");
    if(allDivBlock.length==1){
        var parId=$(this).parents("div[id^='education_container_']"
            ).attr("id"),id=parId.substring(parId.lastIndexOf("_")+1,parId.length);
        $(this).parent().append(
            "<a  class='minus' onclick=remove_dynamic_contacts('.education_container_"+id+"','"+id+"')>")
        }
        $(this).remove();
    $(
        "<div />",{
            "class":"change-form-bg education_container_"+i,
            id:"education_container_"+i,
            style:"width:97.5%; padding:15px"
        }).appendTo("#load_education");
    $(".education_container_"+i).load("education/"+i+"/education_container_");
    i++;
    $(
        "#edu_count_div").html(i)
    });
$(document).on("click","#add_new_file",function(){
    var i=$("#upload_count_div").text(),
    allDivBlock=$("div[id^='upload_container_']");
    if(allDivBlock.length==1){
        var parId=$(this).parents(
            "div[id^='upload_container_']").attr("id"),id=parId.substring(parId.lastIndexOf("_")+1,parId.length);
        $(this).parent().
        append("<a  class='minus m-l-5' onclick=remove_dynamic_contacts('.upload_container_"+id+"','"+id+"')>")
        }
        $(this).remove()
    ;
    $("<div />",{
        "class":"row no-padd upload_container_"+i,
        id:"upload_container_"+i
        }).appendTo("#load_upload");
    $(
        ".upload_container_"+i).load("upload-file/"+i+"/upload_container_");
    i++;
    $("#upload_count_div").html(i);
    $(
        "#publicdocs_upload_count").val(i)
    });
$(document).on("click","#corporate_foundation_add_new_file",function(){
    var i=$(
        "#corporate_foundation_upload_count_div").text(),allDivBlock=$("div[id^='corporate_foundation_upload_container_']");
    if(
        allDivBlock.length==1){
        var parId=$(this).parents("div[id^='corporate_foundation_upload_container_']").attr("id"),id=
        parId.substring(parId.lastIndexOf("_")+1,parId.length);
        $(this).parent().append(
            "<a  class='minus' onclick=remove_dynamic_contacts('.corporate_foundation_upload_container_"+id+"','"+id+"')>")
        }
        $(this).
    remove();
    $("<div />",{
        "class":"no-padd corporate_foundation_upload_container_"+i,
        id:
        "corporate_foundation_upload_container_"+i
        }).appendTo("#corporate_foundation_load_upload");
    $(
        ".corporate_foundation_upload_container_"+i).load("upload-file-corporate/"+i+"/corporate_foundation_upload_container_");
    i++;
    $("#corporate_foundation_upload_count_div").html(i);
    $("#cor_publicdocs_upload_count").val(i)
    });
$(
    "#addressinfo_search_contact").autocomplete({
    afterAdd:true,
    selectFirst:true,
    autoFocus:true,
    source:function(request,
        response){
        $.ajax({
            url:"/get-contacts",
            method:"POST",
            dataType:"json",
            data:{
                user_email:$("#addressinfo_search_contact").
                val()
                },
            success:function(jsonResult){
                var resultset=[];
                $.each(jsonResult,function(){
                    var id=this.user_id,value=this.
                    full_name,address_one=this.street_address_one,address_two=this.street_address_two,city=this.city,state=this.state,
                    zip_code=this.zip_code,country_id=this.country_id,country_name=this.country_name;
                    resultset.push({
                        id:this.user_id,
                        value:
                        this.full_name,
                        address_one:this.street_address_one,
                        address_two:this.street_address_two,
                        city:this.city,
                        state:this.state,
                        zip_code:this.zip_code,
                        country_id:this.country_id,
                        country_name:this.country_name
                        })
                    });
                response(resultset)
                }
            })
    },
open:
function(){
    $(".ui-menu").width(350)
    },
change:function(event,ui){
    if(ui.item){
        $("#addressinfo_search_contact").val("");
        $(
            "#addressinfo_street_address_1").val(ui.item.address_one);
        $("#addressinfo_street_address_2").val(ui.item.address_two);
        $(
            "#addressinfo_city").val(ui.item.city);
        $("#addressinfo_state").val(ui.item.state);
        $("#address_state_id_0").val(ui.item.
            state).select2();
        $("#addressinfo_zip").val(ui.item.zip_code);
        $("#addressinfo_country").val(ui.item.country_id);
        $(
            "#s2id_addressinfo_country .select2-chosen").html(ui.item.country_name);
        $("#addressinfo_country").val(ui.item.country_id
            );
        $("#s2id_addressinfo_country .select2-chosen").html(ui.item.country_name);
        $("#load_contact_section").hide();
        if(ui.item
            .country_id==228){
            $("#state_0").hide();
            $("#usa_state_0").show()
            }
        }else $("#addressinfo_search_contact").val("")
    },
focus:
function(event,ui){
    return false
    },
select:function(event,ui){
    $("#addressinfo_search_contact").val("");
    $(
        "#addressinfo_street_address_1").val(ui.item.address_one);
    $("#addressinfo_street_address_2").val(ui.item.address_two);
    $(
        "#addressinfo_city").val(ui.item.city);
    $("#addressinfo_state").val(ui.item.state);
    $("#address_state_id_0").val(ui.item.
        state);
    $("#s2id_address_state_id_0 .select2-chosen").html(ui.item.state);
    $("#addressinfo_zip").val(ui.item.zip_code);
    $(
        "#0").val(ui.item.country_id);
    $("#s2id_0 .select2-chosen").html(ui.item.country_name);
    $("#load_contact_section").hide();
    if(ui.item.country_id==228){
        $("#state_0").hide();
        $("#usa_state_0").show()
        }
        return false
    }
});
$("#corporate_addressinfo_search_contact").autocomplete({
    afterAdd:true,
    selectFirst:true,
    autoFocus:true,
    source:function(
        request,response){
        $.ajax({
            url:"/get-contacts",
            method:"POST",
            dataType:"json",
            data:{
                user_email:$(
                    "#corporate_addressinfo_search_contact").val()
                },
            success:function(jsonResult){
                var resultset=[];
                $.each(jsonResult,function
                    (){
                        var id=this.user_id,value=this.full_name,address_one=this.street_address_one,address_two=this.street_address_two,city
                        =this.city,state=this.state,zip_code=this.zip_code,country_id=this.country_id,country_name=this.country_name;
                        resultset.
                        push({
                            id:this.user_id,
                            value:this.full_name,
                            address_one:this.street_address_one,
                            address_two:this.street_address_two,
                            city:
                            this.city,
                            state:this.state,
                            zip_code:this.zip_code,
                            country_id:this.country_id,
                            country_name:this.country_name
                            })
                        });
                response
                (resultset)
                }
            })
    },
open:function(){
    $(".ui-menu").width(350)
    },
change:function(event,ui){
    if(ui.item){
        $(
            "#corporate_addressinfo_search_contact").val("");
        $("#corporate_addressinfo_street_address_1").val(ui.item.address_one);
        $
        ("#corporate_addressinfo_street_address_2").val(ui.item.address_two);
        $("#corporate_addressinfo_city").val(ui.item.city);
        $("#corporate_addressinfo_state_0").val(ui.item.state);
        $("#corporate_address_state_id_0").val(ui.item.state);
        $(
            "#s2id_corporate_address_state_id_0 .select2-chosen").html(ui.item.state);
        $("#corporate_addressinfo_zip").val(ui.item.
            zip_code);
        $("#0").val(ui.item.country_id);
        $("#s2id_0 .select2-chosen").html(ui.item.country_name);
        $(
            "#corp_load_contact_section").hide();
        if(ui.item.country_id==228){
            $("#corporate_state_0").hide();
            $(
                "#corporate_usa_state_0").show()
            }
        }else $("#addressinfo_search_contact").val("")
    },
focus:function(event,ui){
    return false
    },
select:function(event,ui){
    $("#corporate_addressinfo_search_contact").val("");
    $("#corporate_addressinfo_street_address_1"
        ).val(ui.item.address_one);
    $("#corporate_addressinfo_street_address_2").val(ui.item.address_two);
    $(
        "#corporate_addressinfo_city").val(ui.item.city);
    $("#corporate_addressinfo_state_0").val(ui.item.state);
    $(
        "#corporate_address_state_id_0").val(ui.item.state);
    $("#s2id_corporate_address_state_id_0 .select2-chosen").html(ui.item
        .state);
    $("#corporate_addressinfo_zip").val(ui.item.zip_code);
    $("#0").val(ui.item.country_id);
    $(
        "#s2id_0 .select2-chosen").html(ui.item.country_name);
    $("#corp_load_contact_section").hide();
    if(ui.item.country_id==228)

    {
        $("#corporate_state_0").hide();
        $("#corporate_usa_state_0").show()
        }
        return false
    }
})
});
function loadRemove(i,classname,
    primary){
    $("#"+primary).show();
    $("."+classname+""+i).append($("<a  class='minus' onclick=remove_dynamic_contacts('."+
        classname+""+i+"','"+i+"')>",{
            href:"javascript:void(0)",
            id:"remove"
        }))
    }
    function loadForm(id){
    var value=$("#"+id).val(),
    type_value=$("#"+id+" option:selected").text();
    if(value=="2"||value=="3"){
        $("#contact_individual_div").hide();
        $(
            "#contact_corp_found_div").show();
        $("#corporate_contact_type option[value="+value+"]").prop("selected","selected");
        $(
            "#s2id_corporate_contact_type .select2-chosen").html(type_value);
        $("#contact_info_div").addClass("active");
        $(
            "#contact_type_div").parent(".detail-section").css("display","block")
        }else{
        $("#s2id_contact_type .select2-chosen").html(
            type_value);
        $("#contact_type option[value="+value+"]").prop("selected","selected");
        $("#contact_individual_div").show();
        $
        ("#contact_corp_found_div").hide()
        }
    }
function show_custome_box(id,divId){
    var value=$("#"+id+" option:selected").text();
    if(value=="Customized")$("#"+divId).show();else $("#"+divId).hide()
        }
        function loadCorporateContact(id){
    if($("#"+id).is(
        ":checked")){
        $("#address_cor_1").hide();
        $("#address_cor_2").hide();
        var counter=$("#corp_count_address_div").html();
        if(
            counter==1)$("#corp_new_address_div").hide();
        $("#corp_load_contact_section").show()
        }else{
        $("#address_cor_1").show();
        $(
            "#address_cor_2").show();
        $("#corp_new_address_div").show();
        $("#corp_load_contact_section").hide()
        }
    }
function 
checkCustomValidation(id){
    var flagCode=false,flagArea=false,flagPhone=false,flagAdd1=false,flagAdd2=false,flagZip=false,
    flagCity=false,flagextension=false,flagCountry=false,flagBillingShipping=false,flageMajors=false,flageInstitution=false,
    flageYear=false,flagWeb=false,flagGpa=false;
    if(id=="contact_individual"){
        $('#contact_individual [name="edu_gpa[]"]').
        each(function(index){
            var str=$(this).val(),regx=/^[0-9.]+$/i;
            $(this).next().remove("span");
            if(str.match(regx)||str.
                length==0){
                var flag=false;
                if(str.length<1&&str!=""){
                    if($(this).next("span").length==0)$(this).after(
                        '<span class="error" for="edu_gpa[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'+
                        EDUCATION_GAP_MIN+"</span>");
                    flag=true
                    }else if(str.length>4&&str!=""){
                    if($(this).next("span").length==0)$(this).after(
                        '<span class="error" for="edu_gpa[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'+
                        EDUCATION_GAP_MIN+"</span>");
                    flag=true
                    }else{
                    $(this).next().remove("span");
                    flag=false
                    }
                    flagGpa=flag
                }else{
                if($(this).next(
                    "span").length==0)$(this).after(
                    '<span class="error" for="edu_gpa[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'+
                    EDUCATION_GPA_VALID+"</span>");
                flagGpa=true
                }
            });
    $('#contact_individual [name="edu_institution[]"]').each(function(index){
        var str=$(this).val(),regx=/^[A-Za-z ]+$/i;
        $(this).next().remove("span");
        if(str.match(regx)||str.length==0){
            var flag=
            false;
            if(str.length<2&&str!=""){
                if($(this).next("span").length==0)$(this).after(
                    '<span for="edu_institution[]" class="error" style="display:block !important;">'+EDUCATION_INSTITUTION_MIN+"</span>");
                flag=true
                }else if(str.length>40&&str!=""){
                if($(this).next("span").length==0)$(this).after(
                    '<span for="edu_institution[]" class="error" style="display:block !important;">'+EDUCATION_INSTITUTION_MAX+"</span>");
                flag=true
                }else{
                $(this).next().remove("span");
                flag=false
                }
                flageInstitution=flag
            }else{
            if($(this).next("span").length==0)$(
                this).after(
                '<span class="error" for="edu_institution[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'
                +EDUCATION_INSTITUTION_VALID+"</span>");
            flageInstitution=true
            }
        });
$('#contact_individual [name="edu_attended_year[]"]').
each(function(index){
    var str=$(this).val(),regx=/^[0-9]+$/i;
    $(this).next().remove("span");
    if(str.match(regx)||str.length
        ==0){
        var flag=false;
        if(str.length<2&&str!=""){
            if($(this).next("span").length==0)$(this).after(
                '<span for="edu_attended_year[]" class="error" style="display:block !important;">'+EDUCATION_ATTENDED_YEAR_MIN+"</span>"
                );
            flag=true
            }else if(str.length>4&&str!=""){
            if($(this).next("span").length==0)$(this).after(
                '<span for="edu_attended_year[]" class="error" style="display:block !important;">'+EDUCATION_ATTENDED_YEAR_MAX+"</span>"
                );
            flag=true
            }else{
            $(this).next().remove("span");
            flag=false
            }
            flageYear=flag
        }else{
        if($(this).next("span").length==0)$(this).
            after('<span class="error" for="edu_attended_year[]" generated="true" style="display:block !important;">'+
                EDUCATION_ATTENDED_YEAR_VALID+"</span>");
        flageYear=true
        }
    });
$('#contact_individual [name="edu_major[]"]').each(function(
    index){
    var str=$(this).val(),regx=/^[A-Za-z ,-]+$/i;
    if(str.match(regx)||str.length==0){
        var flag=false;
        $(this).next().
        remove("span");
        flag=false;
        flageMajors=flag
        }else{
        if($(this).next("span").length==0)$(this).after(
            '<span class="error" for="edu_major[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'
            +EDUCATION_MAJORS_VALID+"</span>");
        flageMajors=true
        }
    });
$('#contact_individual [name="country_code[]"]').each(function(
    index){
    var str=$(this).val();
    $(this).next().remove("span");
    if($.isNumeric(str)||str==""){
        flag=false;
        if(str.length<1&&str
            !=""){
            if($(this).next("span").length==0)$(this).after(
                '<span for="country_code[]" class="error" style="display:block !important;">'+PHONE_NO_COUNTRY_MIN+"</span>");
            flag=true
            }
        else if(str.length>5&&str!=""){
            if($(this).next("span").length==0)$(this).after(
                '<span for="country_code[]" class="error" style="display:block !important;">'+PHONE_NO_COUNTRY_MAX+"</span>");
            flag=true
            }
        else{
            $(this).next().remove("span");
            flag=false
            }
            flagCode=flag
        }else{
        if($(this).next("span").length==0)$(this).after(
            '<span for="country_code[]" class="error" style="display:block !important;">'+PHONE_NO_VALID+"</span>");
        flagCode=true
        }
    })
;
$('#contact_individual [name="extension[]"]').each(function(index){
    var str=$(this).val();
    $(this).next().remove("span");
    if($.isNumeric(str)||str==""){
        flag=false;
        if(str.length<1&&str!=""){
            if($(this).next("span").length==0)$(this).after(
                '<span for="extension[]" class="error" style="display:block !important;">'+PHONE_NO_COUNTRY_MIN_EXT+"</span>");
            flag=true
        }else if(str.length>5&&str!=""){
            if($(this).next("span").length==0)$(this).after(
                '<span for="extension[]" class="error" style="display:block !important;">'+PHONE_NO_COUNTRY_MAX_EXT+"</span>");
            flag=true
        }else{
            $(this).next().remove("span");
            flag=false
            }
            flagextension=flag
        }else if($(this).next("span").length==0){
        $(this).after(
            '<span for="extension[]" class="error" style="display:block !important;">'+PHONE_VALID+"</span>");
        flagextension=true
        }
        if(
        flagextension)return false
        });
        
$('span[for="area_code[]"]').remove();        
$('#contact_individual [name="area_code[]"]').each(function(index){
    var str=$(this).val();
    $(
        this).next().remove("span");
	/*if(str==""){
		if($(this).next("span").length==0)$(this).after(
            '<span for="area_code[]" class="error" style="display:block !important;">'+EMPTY_AREA_CODE+'</span>');
		flagArea=true
	}else */ if($.isNumeric(str) && str != ""){
        flag=false;
        if(str.length<3&&str!=""){
            if($(this).next("span").
                length==0)$(this).after('<span for="area_code[]" class="error" style="display:block !important;">'+AREA_CODE_MIN+
                "</span>");
            flag=true
            }else if(str.length>5&&str!=""){
            if($(this).next("span").length==0)$(this).after(
                '<span for="area_code[]" class="error" style="display:block !important;">'+PHONE_NO_COUNTRY_MAX+"</span>");
            flag=true
            }
        else{
            $(this).next().remove("span");
            flag=false
            }
            flagArea=flag
        } else if(str != "") {
        if($(this).next("span").length==0)$(this).after(
            '<span for="area_code[]" class="error" style="display:block !important;">'+PHONE_NO_VALID+"</span>");
        flagArea=true
        }
    });
    
$('span[for="phone_no[]"]').remove();      
$(
    '#contact_individual [name="phone_no[]"]').each(function(index){
    var str=$(this).val();
    $(this).next().remove("span");
	/*if(str==""){
		if($(this).next("span").length==0)$(this).after(
            '<span for="phone_no[]" class="error" style="display:block !important;">'+EMPTY_PHONE_NO+'</span>');
		flagPhone=true
	}else */ if($
        .isNumeric(str) && str != ""){
        var flag=false;
        if(str.length<7&&str!=""){
            if($(this).next("span").length==0)$(this).after(
                '<span for="phone_no[]" class="error" style="display:block !important;">'+PHONE_NO_MIN+"</span>");
            flag=true
            }else if(str.
            length>10&&str!=""){
            if($(this).next("span").length==0)$(this).after(
                '<span for="phone_no[]" class="error" style="display:block !important;">'+PHONE_NO_MAX+"</span>");
            flag=true
            }else{
            $(this)
            .next().remove("span");
            flag=false
            }
            flagPhone=flag
        } else if(str != ""){
        if($(this).next("span").length==0)$(this).after(
            '<span for="phone_no[]" class="error" style="display:block !important;">'+PHONE_VALID+"</span>");
        flagPhone=true
        }
    });
$(
    '#contact_individual [name="addressinfo_street_address_1[]"]').each(function(index){
    var str=$(this).val(),regx=
    /^[A-Za-z0-9 ,.-]+$/i;
    $(this).next().remove("span");
   // if(str.match(regx)||str.length==0){
        var flag=false;
        if(str.length<1&&
            str!=""){
            if($(this).next("span").length==0)$(this).after(
                '<span for="addressinfo_street_address_1[]" class="error" style="display:block !important;">'+MIN_ADDRESS+"</span>");
            flag=true
            }else if(str.length>40&&str!=""){
            if($(this).next("span").length==0)$(this).after(
                '<span for="addressinfo_street_address_1[]" class="error" style="display:block !important;">'+MAX_ADDRESS+"</span>");
            flag=true
            }else{
            $(this).next().remove("span");
            flag=false
            }
            flagAdd1=flag
       /* }else{
        if($(this).next("span").length==0)$(this).
            after('<span for="addressinfo_street_address_1[]" class="error" style="display:block !important;">'+VALID_ADDRESS+
                "</span>");
        flagAdd1=true
        }*/
    });
$('#contact_individual [name="addressinfo_street_address_2[]"]').each(function(index){
    var 
    str=$(this).val(),regx=/^[A-Za-z0-9 ,.-]+$/i;
    $(this).next().remove("span");
    //if(str.match(regx)||str.length==0){
        var flag=
        false;
        if(str.length<1&&str!=""){
            if($(this).next("span").length==0)$(this).after(
                '<span for="addressinfo_street_address_2[]" class="error" style="display:block !important;">'+MIN_ADDRESS+"</span>");
            flag=true
            }else if(str.length>40&&str!=""){
            if($(this).next("span").length==0)$(this).after(
                '<span for="addressinfo_street_address_2[]" class="error" style="display:block !important;">'+MAX_ADDRESS+"</span>");
            flag=true
            }else{
            $(this).next().remove("span");
            flag=false
            }
            flagAdd2=flag
       /* }else{
        if($(this).next("span").length==0)$(this).
            after('<span for="addressinfo_street_address_2[]" class="error" style="display:block !important;">'+VALID_ADDRESS+
                "</span>");
        flagAdd2=true
        }*/
    });
$('#contact_individual [name="addressinfo_city[]"]').each(function(index){
    var str=$(this).
    val(),regx=/^[a-z .-]+$/i;
    if(str.match(regx)||str.length==0){
        var flag=false;
        $(this).next().remove("span");
        if(str.length<2
            &&str!=""){
            if($(this).next("span").length==0)$(this).after(
                '<span for="addressinfo_city[]" class="error" style="display:block !important;">'+CITY_MIN+"</span>");
            flag=true
            }else if(
            str.length>25&&str!=""){
            if($(this).next("span").length==0)$(this).after(
                '<span for="addressinfo_city[]" class="error" style="display:block !important;">'+CITY_MAX+"</span>");
            flag=true
            }else{
            $(
                this).next().remove("span");
            flag=false
            }
            flagCity=flag
        }else{
        $(this).next().remove("span");
        if($(this).next("span").length==
            0)$(this).after('<span for="addressinfo_city[]" class="error" style="display:block !important;">'+VALID_CITY+"</span>");
        flagCity=true
        }
    });
$('#contact_individual [name="addressinfo_zip[]"]').each(function(index){
    var str=$(this).val(),regx=
    /^[a-z0-9- ]+$/i;
    $(this).next().remove("span");
    if(str.match(regx)||str.length==0){
        var flag=false;
        if(str.length<4&&str!=
            ""){
            if($(this).next("span").length==0)$(this).after(
                '<span for="addressinfo_zip[]" class="error" style="display:block !important;">'+MIN_ZIP+"</span>");
            flag=true
            }else if(
            str.length>12&&str!=""){
            if($(this).next("span").length==0)$(this).after(
                '<span for="addressinfo_zip[]" class="error" style="display:block !important;">'+MAX_ZIP+"</span>");
            flag=true
            }else{
            $(
                this).next().remove("span");
            flag=false
            }
            flagZip=flag
        }else{
        if($(this).next("span").length==0)$(this).after(
            '<span for="addressinfo_zip[]" class="error" style="display:block !important;">'+VALID_ZIP+"</span>");
        flagZip=true
        }
    });
$(
    '#contact_individual [name="addressinfo_country[]"]').each(function(index){
        var countryDivId = $(this).parent().attr('id');
        var tmpSplitArr = countryDivId.split("_");
        if(tmpSplitArr[1] !== undefined && tmpSplitArr[1] !== index)
        {
            index=tmpSplitArr[1];
        }
    if($('#contact_individual  [id="addressinfo_location_shipping'+index+'"]').is(":checked")&&$(
        '#contact_individual  [id="is_apo_po'+index+'"]:checked').val()!=1)flagCountry=false;
    else{
        var str=$(this).val();
        if(str==
            ""){
            if($(this).next("span").length==0){
                $(this).after(
                    '<span for="addressinfo_country[]" class="error" style="display:block !important;">'+EMPTY_COUNTRY+"</span>");
                $("html,body").animate({scrollTop:$("#address_container_"+index).offset().top},"slow");
                }
                flagCountry=true
            }else{
            $(this).next(
                ).remove("span");
            flagCountry=false
            }
        }
});
$('#contact_individual [name="addressinfo_country[]"]').each(function(index1){
    var countryDivId = $(this).parent().attr('id');
    var tmpSplitArr = countryDivId.split("_");
    if(tmpSplitArr[1] !== undefined && tmpSplitArr[1] !== index1)
    {
        index1=tmpSplitArr[1];
    }
    if($("#addressinfo_location_billing"+index1).is(":checked")==false&&$("#addressinfo_location_shipping"+index1).is(
            ":checked")==false){
        
        flagBillingShipping=true;
        if($("#addressinfo_location_shipping"+index1).next("span").length
            ==0){
            
            $("#addressinfo_location_shipping"+index1).after('<span for="addressinfo_location_shipping['+index1+
                ']" class="error" style="display:block !important;">'+EMPTY_BILLING_SHIPPING+"</span>");
            $("html,body").animate({
                scrollTop:$("#address_container_"+index1).offset().top
                },"slow")
            }
        }else{
    $("#addressinfo_location_shipping"+index1).next().
    remove("span");
    flagBillingShipping=false
    }
})
}else{
    $('#contact_corporatefoundation [name="website[]"]').each(function(
        index){
        var str=$(this).val();
        $(this).next().remove("span");
        var regx=
        /[-a-zA-Z0-9@:%_\+.~#?&\/=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&\/=]*)?/gi;
        if(str.match(regx)||str.length==0){
            var flag=false;
            if(str.length<6&&str!=""){
                if($(this).next("span").length==0)$(this).after(
                    '<span for="website[]" class="error" style="display:block !important;">'+MAX_WEB+"</span>");
                flag=true
                }else{
                $(this).next(
                    ).remove("span");
                flag=false
                }
                flagWeb=flag
            }else{
            if($(this).next("span").length==0)$(this).after(
                '<span for="website[]" class="error" style="display:block !important;">'+VALID_WEB+"</span>");
            flagWeb=true
            }
        });
$(
    '#contact_corporatefoundation [name="corporate_country_code[]"]').each(function(index){
    var str=$(this).val();
    $(this).
    next().remove("span");
    if($.isNumeric(str)||str==""){
        flag=false;
        if(str.length<1&&str!=""){
            if($(this).next("span").length
                ==0)$(this).after('<span for="corporate_country_code[]" class="error" style="display:block !important;">'+
                PHONE_NO_COUNTRY_MIN+"</span>");
            flag=true
            }else if(str.length>5&&str!=""){
            if($(this).next("span").length==0)$(this).after
                ('<span for="corporate_country_code[]" class="error" style="display:block !important;">'+PHONE_NO_COUNTRY_MAX+"</span>")
            ;
            flag=true
            }else{
            $(this).next().remove("span");
            flag=false
            }
            flagCode=flag
        }else{
        if($(this).next("span").length==0)$(this).
            after('<span for="corporate_country_code[]" class="error" style="display:block !important;">'+PHONE_NO_VALID+"</span>");
        flagCode=true
        }
    });
$('#contact_corporatefoundation [name="corporate_area_code[]"]').each(function(index){
    var str=$(this).
    val();
    $(this).next().remove("span");
	if(str==""){
		if($(this).next(
                "span").length==0)$(this).after('<span for="corporate_area_code[]" class="error" style="display:block !important;">'+
                EMPTY_AREA_CODE+"</span>");
		flagArea=true;
	}else if($.isNumeric(str)){
        flag=false;
        if(str.length<3&&str!=""){
            if($(this).next(
                "span").length==0)$(this).after('<span for="corporate_area_code[]" class="error" style="display:block !important;">'+
                AREA_CODE_MIN+"</span>");
            flag=true
            }else if(str.length>5&&str!=""){
            if($(this).next("span").length==0)$(this).after
                ('<span for="corporate_area_code[]" class="error" style="display:block !important;">'+PHONE_NO_COUNTRY_MAX+"</span>");
            flag=true
            }else{
            $(this).next().remove("span");
            flag=false
            }
            flagArea=flag
        }else{
        if($(this).next("span").length==0)$(this).
            after('<span for="corporate_area_code[]" class="error" style="display:block !important;">'+PHONE_NO_VALID+"</span>");
        flagArea=true
        }
    });
$('#contact_corporatefoundation [name="corporate_phone_no[]"]').each(function(index){
    var str=$(this).
    val();
    $(this).next().remove("span");
	if(str==""){
		if($(this).next(
                "span").length==0)$(this).after('<span for="corporate_phone_no[]" class="error" style="display:block !important;">'+
                EMPTY_PHONE_NO+"</span>");
		flagPhone=true
	}else if($.isNumeric(str)){
        flag=false;
        if(str.length<7){
            if($(this).next(
                "span").length==0)$(this).after('<span for="corporate_phone_no[]" class="error" style="display:block !important;">'+
                PHONE_NO_MIN+"</span>");
            flag=true
            }else if(str.length>10&&str!=""){
            if($(this).next("span").length==0)$(this).after(
                '<span for="corporate_phone_no[]" class="error" style="display:block !important;">'+PHONE_NO_MAX+"</span>");
            flag=true
            }
        else{
            $(this).next().remove("span");
            flag=false
            }
            flagPhone=flag
        }else{
        if($(this).next("span").length==0)$(this).after(
            '<span for="corporate_phone_no[]" class="error" style="display:block !important;">'+PHONE_VALID+"</span>");
        flagPhone=
        true
        }
    });
$('#contact_corporatefoundation [name="corporate_extension[]"]').each(function(index){
    var str=$(this).val();
    $(
        this).next().remove("span");
    if($.isNumeric(str)||str==""){
        flag=false;
        if(str.length<1&&str!=""){
            if($(this).next("span").
                length==0)$(this).after('<span for="corporate_extension[]" class="error" style="display:block !important;">'+
                PHONE_NO_COUNTRY_MIN_EXT+"</span>");
            flag=true
            }else if(str.length>5&&str!=""){
            if($(this).next("span").length==0)$(this).
                after('<span for="corporate_extension[]" class="error" style="display:block !important;">'+PHONE_NO_COUNTRY_MAX_EXT+
                    "</span>");
            flag=true
            }else{
            $(this).next().remove("span");
            flag=false
            }
            flagextension=flag
        }else if($(this).next("span").
        length==0){
        $(this).after('<span for="corporate_extension[]" class="error" style="display:block !important;">'+
            PHONE_VALID+"</span>");
        flagextension=true
        }
        if(flagextension)return false
        });
$(
    '#contact_corporatefoundation [name="corporate_addressinfo_street_address_1[]"]').each(function(index){
    var str=$(this).
    val(),regx=/^[A-Za-z0-9 ,.-]+$/i;
    if(str.match(regx)||str.length==0){
        var flag=false;
        if(str.length<1&&str!=""){
            if($(this).
                next("span").length==0)$(this).after(
                '<span for="corporate_addressinfo_street_address_1[]" class="error" style="display:block !important;">'+VALID_ADDRESS+
                "</span>");
            flag=true
            }else if(str.length>40&&str!=""){
            if($(this).next("span").length==0)$(this).after(
                '<span for="corporate_addressinfo_street_address_1[]" class="error" style="display:block !important;">'+VALID_ADDRESS+
                "</span>");
            flag=true
            }else{
            $(this).next().remove("span");
            flag=false
            }
            flagAdd1=flag
        }else{
        if($(this).next("span").length==0)
            $(this).after('<span for="corporate_addressinfo_street_address_1[]" class="error" style="display:block !important;">'+
                VALID_ADDRESS+"</span>");
        flagAdd1=true
        }
    });
$(
    '#contact_corporatefoundation [name="corporate_addressinfo_street_address_2[]"]').each(function(index){
    var str=$(this).
    val(),regx=/^[A-Za-z0-9 ,.-]+$/i;
    if(str.match(regx)||str.length==0){
        var flag=false;
        if(str.length<1&&str!=""){
            if($(this).
                next("span").length==0)$(this).after(
                '<span for="corporate_addressinfo_street_address_2[]" class="error" style="display:block !important;">'+VALID_ADDRESS+
                "</span>");
            flag=true
            }else if(str.length>40&&str!=""){
            if($(this).next("span").length==0)$(this).after(
                '<span for="corporate_addressinfo_street_address_2[]" class="error" style="display:block !important;">'+VALID_ADDRESS+
                "</span>");
            flag=true
            }else{
            $(this).next().remove("span");
            flag=false
            }
            flagAdd2=flag
        }else{
        if($(this).next("span").length==0)
            $(this).after('<span for="corporate_addressinfo_street_address_2[]" class="error" style="display:block !important;">'+
                VALID_ADDRESS+"</span>");
        flagAdd2=true
        }
    });
$('#contact_corporatefoundation [name="corporate_addressinfo_city[]"]').each(
    function(index){
        var str=$(this).val(),regx=/^[a-z ]+$/i;
        $(this).next().remove("span");
        if(str.match(regx)||str.length==0)

        {
            var flag=false;
            if(str.length<2&&str!=""){
                if($(this).next("span").length==0)$(this).after(
                    '<span for="corporate_addressinfo_city[]" class="error" style="display:block !important;">'+CITY_MIN+"</span>");
                flag=
                true
                }else if(str.length>25&&str!=""){
                if($(this).next("span").length==0)$(this).after(
                    '<span for="corporate_addressinfo_city[]" class="error" style="display:block !important;">'+CITY_MAX+"</span>");
                flag=
                true
                }else{
                $(this).next().remove("span");
                flag=false
                }
                flagCity=flag
            }else{
            if($(this).next("span").length==0)$(this).after(
                '<span for="corporate_addressinfo_city[]" class="error" style="display:block !important;">'+VALID_CITY+"</span>");
            flagCity=true
            }
        });
$('#contact_corporatefoundation [name="corporate_addressinfo_zip[]"]').each(function(index){
    var str=$(
        this).val(),regx=/^[a-z0-9- ]+$/i;
    $(this).next().remove("span");
    if(str.match(regx)||str.length==0){
        var flag=false;
        if(str
            .length<4&&str!=""){
            if($(this).next("span").length==0)$(this).after(
                '<span for="corporate_addressinfo_zip[]" class="error" style="display:block !important;">'+VALID_ZIP+"</span>");
            flag=
            true
            }else if(str.length>8&&str!=""){
            if($(this).next("span").length==0)$(this).after(
                '<span for="corporate_addressinfo_zip[]" class="error" style="display:block !important;">'+VALID_ZIP+"</span>");
            flag=
            true
            }else{
            $(this).next().remove("span");
            flag=false
            }
            flagZip=flag
        }else{
        if($(this).next("span").length==0)$(this).after(
            '<span for="corporate_addressinfo_zip[]" class="error" style="display:block !important;">'+VALID_ZIP+"</span>");
        flagZip=
        true
        }
    });
$('#contact_corporatefoundation [name="corporate_addressinfo_country[]"]').each(function(index){
    
    var countryDivId = $(this).parent().attr('id');
    var tmpSplitArr = countryDivId.split("_");
    if(tmpSplitArr[2] !== undefined && tmpSplitArr[2] !== index)
    {
        index=tmpSplitArr[2];
    }
    if($('#contact_corporatefoundation  [id="corporate_addressinfo_location_shipping'+index+'"]').is(":checked")&&$(
        '#contact_corporatefoundation  [id="corporate_is_apo_po'+index+'"]:checked').val()!=1)flagCountry=false;
    else{
        var str=$(
            this).val();
        if(str==""){
            if($(this).next("span").length==0){
                $(this).after(
                    '<span for="corporate_addressinfo_country[]" class="error" style="display:block !important;">'+EMPTY_COUNTRY+"</span>");
                $("html,body").animate({
                    scrollTop:$("#corp_address_container_"+index).offset().top
                    },"slow")
                }
                flagCountry=true
            }else{
            $(this
                ).next().remove("span");
            flagCountry=false
            }
        }
});
$('#contact_corporatefoundation [name="corporate_addressinfo_country[]"]')
.each(function(index1){
    var countryDivId = $(this).parent().attr('id');
    var tmpSplitArr = countryDivId.split("_");
    if(tmpSplitArr[2] !== undefined && tmpSplitArr[2] !== index1)
    {
        index1=tmpSplitArr[2];
    }
    if($("#corporate_addressinfo_location_billing"+index1).is(":checked")==false&&$(
        "#corporate_addressinfo_location_shipping"+index1).is(":checked")==false){
        flagBillingShipping=true;
        if($(
            "#corporate_addressinfo_location_shipping"+index1).next("span").length==0){
            $("#corporate_addressinfo_location_shipping"+
                index1).after('<span for="corporate_addressinfo_location_shipping['+index1+
                ']" class="error" style="display:block !important;">'+EMPTY_BILLING_SHIPPING+"</span>");
            $("html,body").animate({
                scrollTop:$("#corp_address_container_"+index1).offset().top
                },"slow")
            }
        }else{
    $("#corporate_addressinfo_location_shipping"+
        index1).next().remove("span");
    flagBillingShipping=false
    }
})
}
if(flagWeb||flagCode||flagArea||flagPhone||flagAdd1||flagAdd2
    ||flagZip||flagCity||flagCountry||flagBillingShipping||flageMajors||flageInstitution||flageYear||flagGpa||flagextension)
{
    $(".detail-section").css("display","block");
    return false
    }else return true
    }
    function backToSearch(){
    window.location.href=
    "/contact"
    }
    $("#no_email_address").click(function(){
    $("#email_id").val("");
    if($("#no_email_address").is(":checked"))$(
        "#email_id").attr("readonly",true);else $("#email_id").attr("readonly",false)
        });
$("#corporate_no_email_address").click(
    function(){
        $("#corporate_email_id").val("");
        if($("#corporate_no_email_address").is(":checked"))$("#corporate_email_id").
            attr("readonly",true);else $("#corporate_email_id").attr("readonly",false)
            });
$("#don_not_have_website").click(function()

{
    if($("#don_not_have_website").is(":checked")){
        $("#cororporate_foundation_new_website_div").hide();
        $(".add-website").
        remove();
        $('#contact_corporatefoundation [name="website[]"]').attr("readonly",true);
        $(
            '#contact_corporatefoundation [name="website[]"]').val("")
        }else{
        $("#cororporate_foundation_new_website_div").show();
        $(
            '#contact_corporatefoundation [name="website[]"]').attr("readonly",false)
        }
    });
function addressTypePrimary(id,index){
    if($(
        "#addressinfo_location_primary"+index).is(":checked"))$("#address_primary_index").val(index);else $(
        "#address_primary_index").val(1)
        }
        function addressTypeBilling(id,index){
    var primaryLocation=$("#primary_location"+index).
    val();
    if($("#addressinfo_location_billing"+index).is(":checked"))if(primaryLocation!="")$("#primary_location"+index).val
        (primaryLocation+",2");else $("#primary_location"+index).val("2");
    else if(primaryLocation!=""){
        var arr=primaryLocation.
        split(","),i=arr.indexOf("2");
        if(i!=-1)arr.splice(i,1);
        $("#primary_location"+index).val(arr)
        }
    }
function 
addressTypeShipping(id,index){
    var primaryLocation=$("#primary_location"+index).val();
    if($(
        "#addressinfo_location_shipping"+index).is(":checked"))if(primaryLocation!="")$("#primary_location"+index).val(
        primaryLocation+",3");else $("#primary_location"+index).val("3");
    else if(primaryLocation!=""){
        var arr=primaryLocation.
        split(","),i=arr.indexOf("3");
        if(i!=-1)arr.splice(i,1);
        $("#primary_location"+index).val(arr)
        }
    }
function 
corporateAddressTypePrimary(id,index){
    if($("#corporate_addressinfo_location_primary"+index).is(":checked"))$(
        "#corporate_address_primary_index").val(index);else $("#corporate_address_primary_index").val(1)
        }
        function 
corporateAddressTypeBilling(id,index){
    var primaryLocation=$("#corporate_primary_location"+index).val();
    if($(
        "#corporate_addressinfo_location_billing"+index).is(":checked"))if(primaryLocation!="")$("#corporate_primary_location"+
        index).val(primaryLocation+",2");else $("#corporate_primary_location"+index).val("2");
    else if(primaryLocation!=""){
        var 
        arr=primaryLocation.split(","),i=arr.indexOf("2");
        if(i!=-1)arr.splice(i,1);
        $("#corporate_primary_location"+index).val(
            arr)
        }
    }
function corporateAddressTypeShipping(id,index){
    var primaryLocation=$("#corporate_primary_location"+index).val();
    if($("#corporate_addressinfo_location_shipping"+index).is(":checked"))if(primaryLocation!="")$(
        "#corporate_primary_location"+index).val(primaryLocation+",3");else $("#corporate_primary_location"+index).val("3");
    else
    if(primaryLocation!=""){
        var arr=primaryLocation.split(","),i=arr.indexOf("3");
        if(i!=-1)arr.splice(i,1);
        $(
            "#corporate_primary_location"+index).val(arr)
        }
    }
function loadIndividualContact(eleId,val,userType){
    if($("#"+eleId).is(
        ":checked")){
        if(userType=="corporate"){
            $("#corporateAddressRow"+val).hide();
            $("#corporate_load_contact_section_"+val).
            show()
            }else{
            $("#load_contact_section_"+val).show();
            $("#addressRow"+val).hide()
            }
            commonAutoComplete(val,userType)
        }else if(
        userType=="corporate"){
        $("#corporateAddressRow"+val).show();
        $("#corporate_load_contact_section_"+val).hide()
        }else{
        $(
            "#addressRow"+val).show();
        $("#load_contact_section_"+val).hide()
        }
    }
function commonAutoComplete(val,userType){
    $(
        ".search-icon").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:function(request,response){
            $.ajax({
                url:"/get-contacts",
                method:"POST",
                dataType:"json",
                data:{
                    user_email:request.term
                    },
                success:function(jsonResult){
                    var 
                    resultset=[];
                    $.each(jsonResult,function(){
                        var id=this.user_id,value=this.full_name,address_one=this.street_address_one,
                        address_two=this.street_address_two,city=this.city,state=this.state,zip_code=this.zip_code,country_id=this.country_id,
                        country_name=this.country_name;
                        resultset.push({
                            id:this.user_id,
                            value:this.full_name,
                            address_one:this.street_address_one,
                            address_two:this.street_address_two,
                            city:this.city,
                            state:this.state,
                            zip_code:this.zip_code,
                            country_id:this.country_id,
                            country_name:this.country_name
                            })
                        });
                    response(resultset)
                    }
                })
        },
    open:function(){
        $(".ui-menu").width(350)
        },
    change:function(
        event,ui){
        if(ui.item)if(userType=="corporate"){
            $("#corporate_load_contact_section_"+val+
                "  input#corporate_address_search_contact"+val).val("");
            $("#corporateAddressRow"+val+
                " input#corporate_addressinfo_street_address_1").val(ui.item.address_one);
            $("#corporateAddressRow"+val+
                " input#corporate_addressinfo_street_address_2").val(ui.item.address_two);
            $("#corporateAddressRow"+val+
                " input#corporate_addressinfo_city").val(ui.item.city);
            $("#corporateAddressRow"+val+
                " input#corporate_addressinfo_state_"+val).val(ui.item.state);
            $("#corporateAddressRow"+val+
                " input#corporate_address_state_id_"+val).val(ui.item.state);
            $("#corporateAddressRow"+val+
                " input#corporate_address_state_id_"+val).val(ui.item.state).select2();
            $("#corporateAddressRow"+val+
                " input#corporate_addressinfo_zip").val(ui.item.zip_code);
            $("#corporateAddressRow"+val+" select#"+val).val(ui.item.
                country_id).select2();
            $("#corporate_load_contact_section_"+val+"").hide();
            $("#corporateAddressRow"+val+"").show();
            if(ui.
                item.country_id=="228"){
                $("state_"+val).hide();
                $("usa_state_"+val).show()
                }
            }else{
            $("#addressRowSearchContact"+val+
                "  input#address_search_contact"+val).val("");
            $("#addressRow"+val+" input#addressinfo_street_address_1").val(ui.item.
                address_one);
            $("#addressRow"+val+" input#addressinfo_street_address_2").val(ui.item.address_two);
            $("#addressRow"+val+
                " input#addressinfo_city").val(ui.item.city);
            $("#addressRow"+val+" input#addressinfo_state_"+val).val(ui.item.state);
            $(
                "#corporateAddressRow"+val+" input#address_state_id_"+val).val(ui.item.state);
            $("#corporateAddressRow"+val+
                " input#address_state_id_"+val).val(ui.item.state).select2();
            $("#addressRow"+val+" input#addressinfo_zip").val(ui.item.
                zip_code);
            $("#addressRow"+val+" select#"+val).val(ui.item.country_id).select2();
            $("#load_contact_section_"+val+"").hide(
                );
            $("#addressRow"+val+"").show();
            if(ui.item.country_id=="228"){
                $("corporate_state_"+val).hide();
                $("corporate_usa_state_"
                    +val).show()
                }
            }else{
        $("#addressinfo_search_contact").val("");
        $("#corporate_addressinfo_search_contact").val("")
        }
    },
focus:
function(event,ui){
    return false
    },
select:function(event,ui){
    if(userType=="corporate"){
        $(
            "#corporate_load_contact_section_"+val+"  input#corporate_address_search_contact"+val).val("");
        $("#corporateAddressRow"+
            val+" input#corporate_addressinfo_street_address_1").val(ui.item.address_one);
        $("#corporateAddressRow"+val+
            " input#corporate_addressinfo_street_address_2").val(ui.item.address_two);
        $("#corporateAddressRow"+val+
            " input#corporate_addressinfo_city").val(ui.item.city);
        $("#corporateAddressRow"+val+
            " input#corporate_addressinfo_state_"+val).val(ui.item.state);
        $("#corporateAddressRow"+val+
            " input#corporate_address_state_id_"+val).val(ui.item.state);
        $("#corporateAddressRow"+val+
            " input#corporate_address_state_id_"+val).val(ui.item.state).select2();
        $("#corporateAddressRow"+val+
            " input#corporate_addressinfo_zip").val(ui.item.zip_code);
        $("#corporateAddressRow"+val+" select#"+val).val(ui.item.
            country_id).select2();
        $("#corporate_load_contact_section_"+val+"").hide();
        $("#corporateAddressRow"+val+"").show();
        if(ui.
            item.country_id=="228"){
            $("corporate_state_"+val).hide();
            $("corporate_usa_state_"+val).show()
            }
        }else{
    $(
        "#addressRowSearchContact"+val+"  input#address_search_contact"+val).val("");
    $("#addressRow"+val+
        " input#addressinfo_street_address_1").val(ui.item.address_one);
    $("#addressRow"+val+
        " input#addressinfo_street_address_2").val(ui.item.address_two);
    $("#addressRow"+val+" input#addressinfo_city").val(ui.
        item.city);
    $("#addressRow"+val+" input#addressinfo_state_"+val).val(ui.item.state);
    $("#addressRow"+val+
        " input#address_state_id_"+val).val(ui.item.state);
    $("#addressRow"+val+" input#address_state_id_"+val).val(ui.item.state
        ).select2();
    $("#addressRow"+val+" input#addressinfo_zip").val(ui.item.zip_code);
    $("#addressRow"+val+" select#"+val).val(
        ui.item.country_id).select2();
    $("#load_contact_section_"+val+"").hide();
    $("#addressRow"+val+"").show();
    if(ui.item.
        country_id=="228"){
        $("state_"+val).hide();
        $("usa_state_"+val).show()
        }
    }
}
})
}
function checkPrimary(value){
    $(
        "#primary-phone").val(value)
    }
    function corporateCheckPrimary(value){
    $("#corporate-primary-phone").val(value)
    }
    $(function()

    {
    $("#addbutton").click(function(){
        var tagsandgroups_groups_to=document.getElementById("tagsandgroups_groups_to");
        $(
            "#tagsandgroups_groups_from option:selected").each(function(){
            $("#tagsandgroups_groups_to").append("<option value='"+$(
                this).val()+"'>"+$(this).text()+"</option>");
            $(this).remove();
            for(var i=0;i<tagsandgroups_groups_to.options.length;i++)
                tagsandgroups_groups_to.options[i].selected=true
                })
        });
    $("#removebutton").click(function(){
        var tagsandgroups_groups_to=
        document.getElementById("tagsandgroups_groups_to");
        $("#tagsandgroups_groups_to option:selected").each(function(){
            $(
                "#tagsandgroups_groups_from").append("<option value='"+$(this).val()+"'>"+$(this).text()+"</option>");
            $(this).remove();
            for(var i=0;i<tagsandgroups_groups_to.options.length;i++)tagsandgroups_groups_to.options[i].selected=true
                })
        });
    $(
        "#cor_addbutton").click(function(){
        var cor_tagsandgroups_groups_to=document.getElementById("cor_tagsandgroups_groups_to"
            );
        $("#cor_tagsandgroups_groups_from option:selected").each(function(){
            $("#cor_tagsandgroups_groups_to").append(
                "<option value='"+$(this).val()+"'>"+$(this).text()+"</option>");
            $(this).remove();
            for(var i=0;i<
                cor_tagsandgroups_groups_to.options.length;i++)cor_tagsandgroups_groups_to.options[i].selected=true
            })
        });
    $(
        "#cor_removebutton").click(function(){
        var cor_tagsandgroups_groups_to=document.getElementById(
            "cor_tagsandgroups_groups_to");
        $("#cor_tagsandgroups_groups_to option:selected").each(function(){
            $(
                "#cor_tagsandgroups_groups_from").append("<option value='"+$(this).val()+"'>"+$(this).text()+"</option>");
            $(this).remove
            ();
            for(var i=0;i<cor_tagsandgroups_groups_to.options.length;i++)cor_tagsandgroups_groups_to.options[i].selected=true
                })
        })
});
function selectUsaState(value,id,userType){
    if(userType=="corporate")if(value=="228"){
        $("#corporate_usa_state_"+id).
        show();
        $("#corporate_state_"+id).hide();
        $("#corporate_address_state_id_"+id).val("").select2();
        $(
            "#corporate_addressinfo_state_"+id).val("")
        }else{
        $("#corporate_state_"+id).show();
        $("#corporate_usa_state_"+id).hide();
        $
        ("#corporate_address_state_id_"+id).val("").select2();
        $("#corporate_addressinfo_state_"+id).val("")
        }else if(value=="228"
        ){
        $("#usa_state_"+id).show();
        $("#state_"+id).hide();
        $("#address_state_id_"+id).val("").select2();
        $("#addressinfo_state_"
            +id).val("")
        }else{
        $("#state_"+id).show();
        $("#usa_state_"+id).hide();
        $("#address_state_id_"+id).val("").select2();
        $(
            "#addressinfo_state_"+id).val("")
        }
    }
function assignStateValue(value,id,userType){
    if(userType=="corporate"){
        if(value){
            var 
            extractStateId=id.split("_");
            $("#corporate_addressinfo_state_"+extractStateId[4]).val(value)
            }
        }else if(value){
    var 
    extractStateId=id.split("_");
    $("#addressinfo_state_"+extractStateId[3]).val(value)
    }
}
function calltime(){}
function 
enableApoPo(id){
    if($("#addressinfo_location_shipping"+id).is(":checked"))$("#apoid"+id).show();
    else{
        $(
            "#address_country_id_"+id).val("").select2();
        $("#addressinfo_state_"+id).val("");
        $("#address_state_apo_"+id).val("").
        select2();
        $("#address_state_id_"+id).val("").select2();
        $("#apoid"+id).hide();
        $("#apo_country_"+id).hide();
        $("#country_"+
            id).show();
        $("#state_"+id).show();
        $("#apo_state_"+id).hide();
        $("#usa_state_"+id).hide()
        }
    }
function corporateEnableApoPo(
    id){
    if($("#corporate_addressinfo_location_shipping"+id).is(":checked"))$("#corporate_apoid"+id).show();
    else{
        $(
            "#corporate_address_country_id_"+id).val("").select2();
        $("#corporate_addressinfo_state_"+id).val("");
        $(
            "#corporate_address_state_apo_"+id).val("").select2();
        $("#corporate_address_state_id_"+id).val("").select2();
        $(
            "#corporate_apoid"+id).hide();
        $("#corporate_apo_country_"+id).hide();
        $("#corporate_country_"+id).show();
        $(
            "#corporate_state_"+id).show();
        $("#corporate_apo_state_"+id).hide();
        $("#corporate_usa_state_"+id).hide()
        }
    }
function 
setApoPostateCountry(id,value){
    if(value!=1){
        $("#"+id).val("").select2();
        $("#apo_country_"+id).show();
        $("#country_"+id).
        hide();
        $("#apo_state_"+id).hide();
        $("#usa_state_"+id).hide();
        if(value==2){
            $("#state_"+id).hide();
            $("#usa_state_"+id).
            show();
            $("#apo_state_"+id).hide();
            $("#addressinfo_city"+id).removeAttr("readOnly")
            }else if(value==3){
            $("#apo_state_"+id)
            .show();
            $("#state_"+id).hide();
            $("#usa_state_"+id).hide();
            $("#addressinfo_city"+id).val("APO");
            $("#addressinfo_city"+id)
            .prop("readOnly","readOnly")
            }
        }else{
    $("#address_country_id_"+id).val("").select2();
    $("#addressinfo_state_"+id).val("");
    $(
        "#address_state_apo_"+id).val("").select2();
    $("#address_state_id_"+id).val("").select2();
    $("#apo_country_"+id).hide();
    $(
        "#country_"+id).show();
    $("#state_"+id).show();
    $("#apo_state_"+id).hide();
    $("#usa_state_"+id).hide();
    $(
        "#addressinfo_city"+id).removeAttr("readOnly")
    }
}
function corporateSetApoPostateCountry(id,value){
    if(value!=1){
        $("#"+id).
        val("").select2();
        $("#corporate_apo_country_"+id).show();
        $("#corporate_country_"+id).hide();
        $("#corporate_apo_state_"+id
            ).hide();
        $("#corporate_usa_state_"+id).hide();
        if(value==2){
            $("#corporate_state_"+id).hide();
            $("#corporate_usa_state_"+id
                ).show();
            $("#corporate_apo_state_"+id).hide();
            $("#corporate_addressinfo_city"+id).removeAttr("readOnly")
            }else if(value==
            3){
            $("#corporate_apo_state_"+id).show();
            $("#corporate_state_"+id).hide();
            $("#corporate_usa_state_"+id).hide();
            $(
                "#corporate_addressinfo_city"+id).val("APO");
            $("#corporate_addressinfo_city"+id).prop("readOnly","readOnly")
            }
        }else{
    $(
        "#corporate_address_country_id_"+id).val("").select2();
    $("#corporate_addressinfo_state_"+id).val("");
    $(
        "#corporate_address_state_apo_"+id).val("").select2();
    $("#corporate_address_state_id_"+id).val("").select2();
    $(
        "#corporate_apo_country_"+id).hide();
    $("#corporate_country_"+id).show();
    $("#corporate_state_"+id).show();
    $(
        "#corporate_apo_state_"+id).hide();
    $("#corporate_usa_state_"+id).hide();
    $("#corporate_addressinfo_city"+id).removeAttr(
        "readOnly")
    }
}