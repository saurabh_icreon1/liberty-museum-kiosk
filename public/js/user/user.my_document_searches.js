function deletePassengerSearches(value){
    authenticateUserBeforeAjax();
    $.colorbox({
        width:"600px",
        href:
        "#passenger_searches",
        height:"260px",
        inline:true
    });
    $("#no_passenger_search").click(function(){
        $.colorbox.close();
        $(
            "#yes_passenger_search").unbind("click");
        return false
        });
    $("#yes_passenger_search").click(function(){
        $(
            "#yes_passenger_search").unbind("click");
        showAjxLoader();
        $.ajax({
            type:"POST",
            data:{
                search_id:value
            },
            url:
            "/delete-search-passenger",
            success:function(response){
                hideAjxLoader();
                var jsonObj=jQuery.parseJSON(response);
                if(jsonObj.
                    status=="success")$("#passenger_search_"+value).hide();
                $("#document_tab-1").click();
                parent.jQuery.colorbox.close()
                }
            })
    })
}
function deletePassengerSavedSearches(value){
    authenticateUserBeforeAjax();
    $.colorbox({
        width:"600px",
        href:
        "#saved_passenger",
        height:"260px",
        inline:true
    });
    $("#no_saved_passenger").click(function(){
        $.colorbox.close();
        $(
            "#yes_saved_passenger").unbind("click");
        return false
        });
    $("#yes_saved_passenger").click(function(){
        $(
            "#yes_saved_passenger").unbind("click");
        showAjxLoader();
        $.ajax({
            type:"POST",
            data:{
                search_id:value
            },
            url:
            "/delete-saved-passenger",
            success:function(response){
                hideAjxLoader();
                var jsonObj=jQuery.parseJSON(response);
                if(jsonObj.
                    status=="success")$("#saved_passenger_"+value).hide();
                $("#document_tab-1").click();
                parent.jQuery.colorbox.close()
                }
            })
    })
}
function deleteManifestSearch(value){
    authenticateUserBeforeAjax();
    $.colorbox({
        width:"600px",
        href:
        "#saved_manifest_search",
        height:"260px",
        inline:true
    });
    $("#no_manifest_search").click(function(){
        $.colorbox.close();
        $(
            "#yes_manifest_search").unbind("click");
        return false
        });
    $("#yes_manifest_search").click(function(){
        $(
            "#yes_manifest_search").unbind("click");
        showAjxLoader();
        $.ajax({
            type:"POST",
            data:{
                search_id:value
            },
            url:
            "/delete-manifest-search",
            success:function(response){
                hideAjxLoader();
                var jsonObj=jQuery.parseJSON(response);
                if(jsonObj.
                    status=="success")$("#manifest_search_"+value).hide();
                $("#document_tab-2").click();
                parent.jQuery.colorbox.close()
                }
            })
    })
}
function deleteShipSearches(value){
    authenticateUserBeforeAjax();
    $.colorbox({
        width:"600px",
        href:"#ship_searches",
        height:
        "260px",
        inline:true
    });
    $("#no_ship_search").click(function(){
        $.colorbox.close();
        $("#yes_ship_search").unbind("click");
        return false
        });
    $("#yes_ship_search").click(function(){
        $("#yes_ship_search").unbind("click");
        showAjxLoader();
        $.ajax({
            type
            :"POST",
            data:{
                search_id:value
            },
            url:"/delete-search-passenger",
            success:function(response){
                hideAjxLoader();
                var jsonObj=
                jQuery.parseJSON(response);
                if(jsonObj.status=="success")$("#ship_search_"+value).hide();
                $("#document_tab-3").click();
                parent.jQuery.colorbox.close()
                }
            })
    })
}
function deleteSavedShipSearch(value){
    authenticateUserBeforeAjax();
    $.colorbox({
        width
        :"600px",
        href:"#saved_ship",
        height:"260px",
        inline:true
    });
    $("#no_saved_ship").click(function(){
        $.colorbox.close();
        $(
            "#yes_saved_ship").unbind("click");
        return false
        });
    $("#yes_saved_ship").click(function(){
        $("#yes_saved_ship").unbind(
            "click");
        showAjxLoader();
        $.ajax({
            type:"POST",
            data:{
                search_id:value
            },
            url:"/delete-saved-ship",
            success:function(response){
                hideAjxLoader();
                var jsonObj=jQuery.parseJSON(response);
                if(jsonObj.status=="success")$("#saved_ship_"+value).hide();
                $(
                    "#document_tab-3").click();
                parent.jQuery.colorbox.close()
                }
            })
    })
}
function deleteOralHistorySearch(value){
    authenticateUserBeforeAjax();
    $.colorbox({
        width:"600px",
        href:"#oral_history_search",
        height:"260px",
        inline:true
    });
    $(
        "#no_oh_search").click(function(){
        $.colorbox.close();
        $("#yes_oh_search").unbind("click");
        return false
        });
    $(
        "#yes_oh_search").click(function(){
        $("#yes_oh_search").unbind("click");
        showAjxLoader();
        $.ajax({
            type:"POST",
            data:{
                search_id:value
            },
            url:"/delete-user-search",
            success:function(response){
                hideAjxLoader();
                var jsonObj=jQuery.parseJSON(
                    response);
                if(jsonObj.status=="success"){
                    $("#document_tab-4").click();
                    parent.jQuery.colorbox.close()
                    }
                }
        })
    })
}
function 
showRelatedProductsNew(product_type){
    $("#ajx_loader").css({"z-index":"99999999"}); 
  //  showAjxLoader();
   // return false;
    $.colorbox({
        width:"1200px",
        height:"800px",
        iframe:true,
        overlayClose:false,
        href:"/get-related-products/"+product_type+"/null",
        onLoad:function(){ $("#ajx_loader").css({"z-index":"99999999"}); $('#colorbox').css({"display":"none"}); $('#cboxClose').css({"display":"none"}); },
        onComplete:function(){ $("#ajx_loader").css({"z-index":""}); $('#colorbox').css({"display":"block"}); hideAjxLoader(); }
    });
}

function showCartProductsNew(){
    $("#ajx_loader").css({"z-index":"99999999","display":"block"});
    $.ajax({
        type:"POST",
        url: "/cart-details",
        async:true,
        success:function(responseData){
            parent.$("#cart-details-div").html(responseData)
          }
        });
}

$(function(){
    $("#add_to_cart").click(function()

    {
        var total=$(".checkbox_transaction:checked").size();
        if(total==0){
            $("#checkbox_selection").html("Select any transaction"
                );
            return false
            }
            var quantity=$("#quantity").val(),status=/^\d+$/.test(quantity);
        if(status==false){
            $("#quantity").addClass
            ("error");
            $(this).after(
                '<label style="display:block !important" class="error errorlabel" generated="true">Please enter quantity.</label>');
            return false
            }
            var val=[];
        $(".checkbox_transaction:checked").each(function(i){
            val[i]=$(this).val()
            });
        var 
        comma_separated_tra=val.join(",");
        $.ajax({
            type:"POST",
            url:"/get-transaction-addtocart",
            beforeSend:function(){
                showAjxLoader()
                },
            data:{
                transactionids:comma_separated_tra,
                quantity:quantity
            },
            success:function(responseData){
                authenticateUserBeforeAjax();
                var jsonObj=jQuery.parseJSON(responseData);
                if(jsonObj.status=="success"){
                    $.ajax({
                        type:
                        "POST",
                        url:"/check-related-products-exists",
                        data:{
                            product_id_related:$.trim($("#product_id_related").val())
                            },
                        success:
                        function(relatedData){
                            var jsonObj2=jQuery.parseJSON(relatedData);
                            if(jsonObj2.status=="success")if(jsonObj2.
                                hasRelatedProducts=="yes"){
                                
                                showRelatedProductsNew($.trim($("#product_type_id_related").val()));
                                showCartProductsNew();
                                //hideAjxLoader()
                                }else{
                                hideAjxLoader();
                                showAddToCartConfirmation()
                                }else{
                                hideAjxLoader();
                                showAddToCartConfirmation()
                                }
                            }
                    });
            return false
            }
        }
    })
});
$("#add_to_cart_manifest").click(function(){
    var total=$(".checkbox_transaction:checked").size();
    if(
        total==0){
        $("#checkbox_selection").html("Select any Manifest");
        return false
        }
        var quantity=$("#quantity").val(),status=
    /^\d+$/.test(quantity);
    if(status==false){
        $("#quantity").addClass("error");
        $(this).after(
            '<label style="display:block !important" class="error errorlabel" generated="true">Please enter quantity.</label>');
        return false
        }
        var val=[],shipVal=[];
    $(".checkbox_transaction:checked").each(function(i){
        val[i]=$(this).val();
        var ship_img
        =$(this).val(),ship_img_2=ship_img.substring(0,ship_img.length-4);
        shipVal[i]=$("#ship_image_"+ship_img_2).val()
        });
    var 
    comma_separated_tra=val.join(","),comma_separated_tra_ship_id=shipVal.join(",");
    $.ajax({
        type:"POST",
        url:
        "/get-transaction-addtocart",
        beforeSend:function(){
            showAjxLoader()
            },
        data:{
            manifestids:comma_separated_tra,
            quantity:
            quantity,
            is_manifest:1,
            flag:"s",
            is_first_image:1,
            is_second_image:"",
            additional_images:"",
            passenger_id:"",
            ship_id:
            comma_separated_tra_ship_id
        },
        success:function(responseData){
            authenticateUserBeforeAjax();
            var jsonObj=jQuery.parseJSON(
                responseData);
            if(jsonObj.status=="success"){
                $.ajax({
                    type:"POST",
                    url:"/check-related-products-exists",
                    data:{
                        product_id_related:$.trim($("#product_id_related").val())
                        },
                    success:function(relatedData){
                        var jsonObj2=jQuery.parseJSON(
                            relatedData);
                        if(jsonObj2.status=="success")if(jsonObj2.hasRelatedProducts=="yes"){
                            
                            showRelatedProductsNew
                            ($.trim($("#product_type_id_related").val()));
                            showCartProductsNew();
                            //hideAjxLoader()
                            }else{
                            hideAjxLoader();
                            showAddToCartConfirmation()
                            }else{
                            hideAjxLoader();
                            showAddToCartConfirmation()
                            }
                        }
                });
        return false
        }
    }
})
});
$("#add_to_cart_manifest_more").click(function(){
    var 
    total=$(".transaction_manifest:checked").size();
    if(total==0){
        $("#checkbox_selection_manifest").html(
            "Select any Manifest");
        return false
        }
        var val=[];
    $(".transaction_manifest:checked").each(function(i){
        val[i]=$(this).val()
        }
    );
    var comma_separated_tra=val.join(",");
    $.ajax({
        type:"POST",
        url:"/get-transaction-addtocart",
        beforeSend:function(){
            showAjxLoader()
            },
        data:{
            manifestids:comma_separated_tra,
            is_manifest:1,
            flag:"s",
            is_first_image:"",
            is_second_image:"",
            additional_images:"",
            passenger_id:"",
            old_manifest:1
        },
        success:function(responseData){
            authenticateUserBeforeAjax();
            var 
            jsonObj=jQuery.parseJSON(responseData);
            if(jsonObj.status=="success"){
                $.ajax({
                    type:"POST",
                    url:
                    "/check-related-products-exists",
                    data:{
                        product_id_related:$.trim($("#product_id_related").val())
                        },
                    success:function(
                        relatedData){
                        var jsonObj2=jQuery.parseJSON(relatedData);
                        if(jsonObj2.status=="success")if(jsonObj2.hasRelatedProducts==
                            "yes"){
                            
                            showRelatedProductsNew($.trim($("#product_type_id_related").val()));
                            showCartProductsNew();
                            //hideAjxLoader()
                            }else{
                            hideAjxLoader();
                            showAddToCartConfirmation()
                            }else{
                            hideAjxLoader();
                            showAddToCartConfirmation()
                            }
                        }
                });
        return false
        }
    }
})
});
$(
    "#add_to_cart_ship").click(function(){
    var total=$(".transaction_ship:checked").size();
    if(total==0){
        $(
            "#checkbox_selection").html("Select any ship image");
        return false
        }
        var ship_selection=$("#ship_copies").serialize(),val=[
    ];
    $(".transaction_ship:checked").each(function(i){
        val[i]=$(this).val()
        });
    var comma_separated_tra=val.join(","),urlstr=
    ship_selection+"&shipids="+comma_separated_tra;
    $.ajax({
        type:"POST",
        url:"/get-transaction-addtocart",
        beforeSend:function(
            ){
            showAjxLoader()
            },
        data:urlstr,
        success:function(responseData){
            authenticateUserBeforeAjax();
            var jsonObj=jQuery.parseJSON(
                responseData);
            if(jsonObj.status=="success"){
                $.ajax({
                    type:"POST",
                    url:"/check-related-products-exists",
                    data:{
                        product_id_related:$.trim($("#product_id_related").val())
                        },
                    success:function(relatedData){
                        var jsonObj2=jQuery.parseJSON(
                            relatedData);
                        if(jsonObj2.status=="success")if(jsonObj2.hasRelatedProducts=="yes"){
                            
                            showRelatedProductsNew
                            ($.trim($("#product_type_id_related").val()));
                            showCartProductsNew();
                            //hideAjxLoader()
                            }else{
                            hideAjxLoader();
                            showAddToCartConfirmation()
                            }else{
                            hideAjxLoader();
                            showAddToCartConfirmation()
                            }
                        }
                });
        return false
        }
    }
})
})
})