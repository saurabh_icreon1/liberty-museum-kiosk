function refreshCaptcha() {
    showAjxLoader();
    $.ajax({
        type: "POST", 
        url: "/refresh-captcha", 
        data: {}, 
        success: function(
            responseData) {
            hideAjxLoader();
            var jsonObj = jQuery.parseJSON(responseData);
            if (jsonObj.status == "success") {
                $(
                    "#captcha-hidden").val(jsonObj.captchaId);
                $("#captcha-image").attr("src", jsonObj.path + "" + jsonObj.captchaId + ".png")
            }
        }
    })
}
$
    (document).delegate("#password,#password_conf", "input", function() {
        $(this).val($.trim($(this).val().replace(/\s/g, "")))
    })
;
$(function() {
  parent.ga('require', 'displayfeatures');
  parent.ga('send', 'pageview',{'page': '/signup','title': 'Sign Up'});
  $("#email_not").change(function() { 
		if($("#email_not").is(":checked")){
		   var fname = $('input:text[name=first_name]').val();
		   var lname = $('input:text[name=last_name]').val();
		   $("#username").val(fname+"."+lname);
			createUserName(this);
		}
	});
	$("#phone_digit" ).blur(function() {
		var fname = $('input:text[name=first_name]').val();
		var lname = $('input:text[name=last_name]').val();
		var phDigit = $('#phone_digit').val();
		$("#username").val('');
		$("#username").val(fname+"."+lname+"."+phDigit);
	});
    $("#email_id_conf").blur(function() {
        if ($("#email_id_conf").val() != "")
            if ($("#email_id").val() != $(
                "#email_id_conf").val()) {
                $("#confrm_email_green").hide();
                $("#confrm_email_red").show()
            } else {
                $("#confrm_email_green").
                show();
                $("#confrm_email_red").hide()
            }
    });
    $("#email_id_conf").focus(function() {
        if ($("#email_id_conf").val() != "")
            if ($(
                "#email_id").val() != $("#email_id_conf").val()) {
                $("#confrm_email_green").hide();
                $("#confrm_email_red").show()
            } else {
                $(
                    "#confrm_email_green").show();
                $("#confrm_email_red").hide()
            }
    });
    $("#password_conf").blur(function() {
        var filter =
        /^(?=.*\d)(?=.*[a-zA-Z]).{6,25}$/;
        if ($("#password_conf").val() != "" && filter.test($("#password_conf").val()))
            if ($("#password"
                ).val() != $("#password_conf").val()) {
                $("#confrm_passwd_green").hide();
                $("#confrm_passwd_red").show()
            } else {
                $(
                    "#confrm_passwd_green").show();
                $("#confrm_passwd_red").hide()
            }
    });
    $("#password_conf").focus(function() {
        var filter =
        /^(?=.*\d)(?=.*[a-zA-Z]).{6,25}$/;
        if ($("#password_conf").val() != "" && filter.test($("#password_conf").val()))
            if ($("#password"
                ).val() != $("#password_conf").val()) {
                $("#confrm_passwd_green").hide();
                $("#confrm_passwd_red").show()
            } else {
                $(
                    "#confrm_passwd_green").show();
                $("#confrm_passwd_red").hide()
            }
    });
    $("#email_id").blur(function() {
        var filter =
        /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if ($("#email_id")
            .val() != "" && filter.test($("#email_id").val())) {
            showAjxLoader();
            $.ajax({
                type: "POST", 
                url: "/email-exits", 
                data: {
                    email_id: $(
                        "#email_id").val()
                    }, 
                success: function(responseData) {
                    hideAjxLoader();
                    var jsonObj = jQuery.parseJSON(responseData);
                    if (jsonObj
                        .result) {
                        $("#email_exits_msg").show();
                        $("#email_exits_red").show();
                        $("#email_exits_green").hide();
                        $(
                            "#confrm_email_green").hide();
                        $("#confrm_email_red").show()
                    } else {
                        $("#email_exits_msg").hide();
                        $("#email_exits_red").hide
                        ();
                        $("#email_exits_green").show()
                    }
                }
            })
    } else {
        $("#email_exits_red").show();
        $("#email_exits_green").hide()
    }
    });
$.validator.
addMethod("alphanumeric", function(value, element) {
    return this.optional(element) || /^(?=.*\d)(?=.*[a-z]).{6,25}$/i.test(
        value)
}, PASSWORD_VALID);
jQuery.validator.addMethod("lettersandapostropheonly", function(value, element) {
    return this.
    optional(element) || /^[a-z '\-]+$/i.test(value)
}, INVALID_NAME);
jQuery.validator.addMethod("lettersonly", function(value,
    element) {
    return this.optional(element) || /^[a-z ]+$/i.test(value)
}, INVALID_NAME);
jQuery.validator.addMethod(
    "lettersanddotonly", function(value, element) {
        return this.optional(element) || /^[a-z.']+$/i.test(value)
    }, INVALID_NAME);
jQuery.validator.addMethod("zipcodevalidation", function(value, element) {
    return this.optional(element) || /^[a-z0-9- ]+$/i.
    test(value)
}, INVALID_ZIP);
$("#user").validate({
submitHandler: function() {
        var dataStr =  $("#user, #createusername").serialize();
        showAjxLoader
        ();
        $.ajax({
            type: "POST", 
            url: "/signup", 
            data: dataStr, 
            success: function(responseData) {
                hideAjxLoader();
                var jsonObj = jQuery.
                parseJSON(responseData);
                if (jsonObj.status == "success") {                        
					   updateLoginDetails();
					   window.location.href ="/visitation-record/"+jsonObj.data
                      
                    }else if (jsonObj.status == "error" && jsonObj.result)
                    $("#email_id").after(
                        '<label class="error" style="display:block;">' + jsonObj.message + "</label>");
                else
                    $.each(jsonObj.message, function(i, msg) {
                        $('label[for="' + i + '"]').remove();
                        $("#" + i).after('<label for="' + i + '" class="error" style="display:block;">' + msg + "</label>"
                            )
                    })
            }
        })
}, 
rules: {
    first_name: {
        required: true, 
        minlength: 1, 
        maxlength: 50, 
        lettersandapostropheonly: true
    }, 
    middle_name: {
        minlength:
        1, 
        maxlength: 4, 
        lettersanddotonly: true
    }, 
    last_name: {
        required: true, 
        minlength: 1, 
        maxlength: 20, 
        lettersandapostropheonly: true
    },
    email_id: {
        required: function(element){ 
		if($("input:checkbox[name=email_not]:checked").val() != 1){return true;
		}else{ return false;}},
        email: true
    }, 
    email_id_conf: {
        required: function(element){ 
		if($("input:checkbox[name=email_not]:checked").val() != 1){return true;
		}else{ return false;}}, 
        equalTo: "#user #email_id"
    },  
    password: {
        required: true,
        minlength: 6, 
        maxlength: 25, 
        alphanumeric: true
    }, 
    password_conf: {
        required: true, 
        equalTo: "#user #password"
    }, 
    country_id: {
        required
        : true
    }, 
    postal_code: {
        zipcodevalidation: true, 
        minlength: 4, 
        maxlength: 12
    }, 
    security_question_id: {
        required: true
    },
    security_answer: {
        required: true
    }, 
    agree: {
        required: true
    }, 
    captcha: {
        required: true
    },
	phone_digit: {
        required: function(element){ 
		if($("input:checkbox[name=email_not]:checked").val() == 1){return true;
		}else{ return false;}}, 
		minlength: 4, 
        maxlength: 4,
		number: true
    }
}, 
messages: {
    first_name: {
        required: FIRST_NAME
    }, 
    last_name: {
        required: LAST_NAME
    }, 
    email_id: {
        required: EMAIL_ID, 
        email: EMAIL_ID_VALID
    }, 
    email_id_conf: {
        required:
        CONFIRM_EMAIL_ID, 
        equalTo: CONFIRM_EMAIL_ID_NOT_MATCH
    }, 
    password: {
        required: PASSWORD
    }, 
    password_conf: {
        required:
        PASSWORD_CONFIRM, 
        equalTo: PASSWORD_CONFIRM_NOT_MATCHED
    }, 
    country_id: {
        required: COUNTRY
    }, 
    security_question_id: {
        required:
        SECURITY_QUESTION
    }, 
    security_answer: {
        required: SECURITY_ANSWER
    }, 
    agree: {
        required: AGREE_TERMS
    }, 
    captcha: {
        required:
        CAPTCHA_EMPTY
    }
}
})

/*create username*/
$("#createusername").validate({
    submitHandler: function() {
	$(".create_user_name").hide();
    $.colorbox.close();
	
	return false;
	}, 
rules: {
	phone_digit: {
        required: function(element){ 
		if($("input:checkbox[name=email_not]:checked").val() == 1){return true;
		}else{ return false;}}, 
		minlength: 4, 
        maxlength: 4,
		number: true
    }
}, 
messages: {
	phone_digit: {
        required:PHONE_DIGIT_EMPTY
    }
}
})

})

function updateLoginDetails(){
    $.ajax({
        type:"POST",
        url:"/login-details",
        async:false,
        success:function(responseData){
            parent.$("#welcome-div").html(responseData);
            parent.parent.$("#welcome-div").html(responseData);
            parent.$(
                "#menusignup").html('<a href="/profile">My File</a>');
            parent.parent.$("#menusignup").html(
                '<a href="/profile">My File</a>');
            parent.$("#menulogin").remove();
            parent.parent.$("#menulogin").remove()
            }
        });
        }
function createUserName(obj){
	$.colorbox({width:"700px",height:"500px",inline:true,href:".create_user_name"});
    $(".create_user_name").show();
    $("#cboxClose").on("click",function(){$(".create_user_name").hide()})
	
	 $("#close_thanks").click(function(){
        $('#close_thanks').unbind('click');
         window.location.href="/kiosk-reservation-system";
    });
	$("#passenger_search").click(function(){
        $('#passenger_search').unbind('click');
         window.location.href="/kiosk-reservation-system";
    });

}