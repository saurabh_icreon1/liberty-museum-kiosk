function goToProfile(){
    window.location.href="/profile"
    }
    function deleteTokenInfo(paymentProfileId,profileId){
    authenticateUserBeforeAjax();
    $.colorbox({
        width:"640px",
        href:"#delete_token_info",
        height:"260px",
        inline:true,
        onClosed:
        function(){
            location.href="#topOfProfilePage"
            }
        });
$("#no_delete_token").click(function(){
    $.colorbox.close();
    $(
        "#yes_delete_token").unbind("click");
    return false
    });
$("#yes_delete_token").click(function(){
    $("#yes_delete_token").
    unbind("click");
    $.ajax({
        type:"POST",
        data:{
            paymentProfileId:paymentProfileId,
            profileId:profileId
        },
        url:
        "/delete-user-token-info",
        success:function(response){
            var jsonObj=jQuery.parseJSON(response);
            if(jsonObj.status=="success"
                ){
                parent.$("#tab-3").click();
                parent.jQuery.colorbox.close()
                }
            }
    })
})
}
function addNewToken(site_url){
    window.location.href=site_url+"/add-new-token";
    /*var url="/add-token";
    showAjxLoader();
    $.ajax({
        type:"GET",
        data:{},
        url:url,
        success:function(response){
            hideAjxLoader();
            $("#add_token_info_div").html(response);
            $("html, body").animate({scrollTop:$("#add_token_info_div").offset().top},2000)
            }
        })*/
}
function addEditAddressInfo(value,mode){
    var url;
    if(value)url="/add-edit-user-address-info/"+mode+"/"+value;else url=
        "/add-edit-user-address-info/"+mode;
    showAjxLoader();
    $.ajax({
        type:"GET",
        data:{},
        url:url,
        success:function(response){
            hideAjxLoader();
            $("#address_info_div").css({
                display:"block"
            });
            $("#address_info_div").html(response);
            $("html, body").
            animate({
                scrollTop:$("#address_info_div").offset().top
                },2000)
            }
        })
}
function addressTypeBilling(id,index){}
function 
addressTypeShipping(id,index){}
function deleteAddressInfo(value,mode){
    authenticateUserBeforeAjax();
    $.colorbox({
        width:
        "640px",
        href:"#delete_address_info",
        height:"260px",
        inline:true,
        onClosed:function(){
            if(mode!="creditCard")location.href=
                "#topOfProfilePage"
                }
            });
$("#no_delete_address").click(function(){
    $.colorbox.close();
    $("#yes_delete_address").unbind(
        "click");
    return false
    });
$("#yes_delete_address").click(function(){
    $("#yes_delete_address").unbind("click");
    $.ajax({
        type:
        "POST",
        data:{
            address_id:value
        },
        url:"/delete-user-address-info",
        success:function(response){
            var jsonObj=jQuery.parseJSON(
                response);
            if(jsonObj.status=="success"){
                if(mode=="creditCard")$.ajax({
                    type:"POST",
                    url:"/show-user-address-info",
                    data:{},
                    success:function(responseData){
                        $("#address_info_div").empty();
                        $("#userAddressInfoCreditCard").html(responseData);
                        $(
                            "html, body").animate({
                            scrollTop:$("#userAddressInfoCreditCard").offset().top
                            },2000)
                        }
                    });else parent.$("#tab-2").click();
            parent.jQuery.colorbox.close()
            }
        }
    })
})
}