function deleteSavedFh(value) {
    authenticateUserBeforeAjax();
    $.colorbox({width: "600px", href: "#saved_fh", height: "260px",
        inline: true});
    $("#no_saved_fh").click(function() {
        $.colorbox.close();
        $("#yes_saved_fh").unbind("click");
        return false
    });
    $("#yes_saved_fh").click(function() {
        $("#yes_saved_fh").unbind("click");
        showAjxLoader();
        $.ajax({type: "POST", data: {search_id
                        : value}, url: "/delete-search-family-history", success: function(response) {
                hideAjxLoader();
                var jsonObj = jQuery.parseJSON(
                        response);
                if (jsonObj.status == "success")
                    $("#fh_search_" + value).hide();
                parent.jQuery.colorbox.close();
                $(
                        "#success_message1").show();
                $("#success_message1").html(jsonObj.message)
            }})
    })
}
function deleteMyFh(value) {
    authenticateUserBeforeAjax();
    $.colorbox({width: "600px", href: "#my_fh", height: "260px", inline: true});
    $("#no_my_fh").click(
            function() {
                $.colorbox.close();
                $("#yes_my_fh").unbind("click");
                return false
            });
    $("#yes_my_fh").click(function() {
        $(
                "#yes_my_fh").unbind("click");
        showAjxLoader();
        $.ajax({type: "POST", data: {family_history_id: value}, url:
                    "/delete-user-family-history", success: function(response) {
                        hideAjxLoader();
                        var jsonObj = jQuery.parseJSON(response);
                        if (
                                jsonObj.status == "success")
                            $("#my_fh_" + value).hide();
                        parent.jQuery.colorbox.close();
                        $("#success_message2").show();
                        $(
                                "#success_message2").html(jsonObj.message);
                        setTimeout(function() {
                            $("#tab-6").click()
                        }, 2000)
                    }})
    })
}
function addfh(
        isValidMembership) {
    if (isValidMembership == "0") {
        $(".delete_family_search").colorbox({width: "700px", height: "300px", inline:
                    true});
        $("#no_delete").click(function() {
            $.colorbox.close();
            $("#yes_delete").unbind("click");
            return false
        })
    } else if (
            isValidMembership == "x")
        $(".exceed").colorbox({width: "600px", height: "320px", inline: true});
    else {
        $("#fhDisplayDiv").hide();
        $("#fhAddDiv").show()
    }
}
function addMorePassengerRow(currAnchor) {
    showAjxLoader();
    $.ajax({type: "POST", url:
                "/associate-more-passenger", data: {totalPassenger: totalPassenger, maxPassengerAllowed: maxPassengerAllowed,
                    numOfPassengerOnPage: numOfPassengerOnPage}, success: function(responseData) {
            hideAjxLoader();
            var allDivBlock = $(
                    "div[id^='person_in_database_block']");
            if (allDivBlock.length == 1) {
                var parId = $(currAnchor).parent().parent().parent().attr
                        ("id"), id = parId.substring(parId.lastIndexOf("_") + 1, parId.length);
                $(currAnchor).parent().append(
                        '<a class="remove" style="float: left; margin-top: 26px;" href="javascript:void(0);" onclick="deleteMorePassengerRow(' +
                        id + ')"></a>')
            }
            $(currAnchor).remove();
            $("#person_in_database_div").append(responseData);
            totalPassenger = totalPassenger + 1;
            numOfPassengerOnPage = numOfPassengerOnPage + 1
        }})
}
function addMoreMnaualPersonRow(currAnchor) {
    showAjxLoader();
    $.ajax({type:
                "POST", url: "/associate-more-manual-person", data: {totalManualFamilyPerson: totalManualFamilyPerson}, success: function(
                responseData) {
            hideAjxLoader();
            var allDivBlock = $("div[id^='manual_block']");
            if (allDivBlock.length == 1) {
                var parId = $(
                        currAnchor).parent().parent().parent().attr("id"), id = parId.substring(parId.lastIndexOf("_") + 1, parId.length);
                $(
                        "div[id^='manual_block'] > div:nth-child(3)").after(
                        '<a class="remove" style="float: left; margin-top: 26px;" href="javascript:void(0);" onclick="deleteMoreMnaualPersonRow('
                        + id + ')"></a>')
            }
            $(currAnchor).remove();
            $("#manual_div").append(responseData);
            totalManualFamilyPerson =
                    totalManualFamilyPerson + 1
        }})
}
function editFamilyHistory(family_history_id) {
    showAjxLoader();
    $.ajax({type: "POST", url:
                "/edit-user-family-history", data: {id: family_history_id}, success: function(responseData) {
            hideAjxLoader();
            $("#fhDisplayDiv"
                    ).hide();
            $("#tab6").html(responseData)
        }})
}
function deleteUserFamilyPerson(family_person_id, family_history_id) {
    $(
            ".delete_family_person").colorbox({width: "700px", height: "200px", inline: true});
    $("#no_delete_family_person").click(
            function() {
                $.colorbox.close();
                $("#yes_delete_family_person").unbind("click");
                return false
            });
    $(
            "#yes_delete_family_person").click(function() {
        $.colorbox.close();
        $("#yes_delete_family_person").unbind("click");
        showAjxLoader();
        var URL = "/delete-user-family-person", data = "family_person_id=" + family_person_id;
        $.ajax({type: "POST", url:
                    URL, data: {familyPersonId: data}, success: function(responseData) {
                hideAjxLoader();
                var jsonObj = jQuery.parseJSON(responseData)
                        ;
                if (jsonObj.status == "success")
                    editFamilyHistory("" + family_history_id + "")
            }})
    })
}
function viewFamilyHistory(
        family_history_id, msg) {
    showAjxLoader();
    $.ajax({type: "POST", url: "/view-user-family-history-detail", data: {id:
                    family_history_id}, success: function(responseData) {
            hideAjxLoader();
            $("#tab6").html(responseData);
            $("html, body").animate(
                    {scrollTop: 0}, 0);
            $("#success_message").show();
            $("#success_message").html(msg)
        }})
}
function showexpirationNotice() {
    $(".membership_expiration").colorbox({width: "700px", height: "250px", inline: true});
    $("#no_delete_membership_expiration").
            click(function() {
                $.colorbox.close();
                $("#title").attr("disabled", "disabled");
                $("#main_content").attr("disabled","disabled");
                tinymce.activeEditor.getBody().setAttribute("contenteditable", false);
                $("#mce_64-body").hide();
                $("#mce_72-body").hide();
                $("#mce_61").hide();
                $("#location_one_video_type").attr("disabled", "disabled");
                $("#location_one_image_type").attr("disabled", "disabled");
                $("#location_two_video_type").attr("disabled", "disabled");
                $("#location_two_image_type").attr("disabled", "disabled");
                $("#location_three_video_type").attr("disabled", "disabled");
                $("#location_three_image_type").attr("disabled", "disabled");
                $("#location_four_video_type").attr("disabled", "disabled");
                $("#location_four_image_type").attr("disabled", "disabled");
                $("#location_five_video_type").attr("disabled", "disabled");
                $("#location_five_image_type").attr("disabled", "disabled");
                return false
            })
}
function deleteMorePassengerRow(id) {
    $("#person_in_database_block_" + id).remove();
    var lastBlock = $("div[id^='person_in_database_block']").last();
    if ($(lastBlock).find(".add").length == 0)
        $(".remove", $(lastBlock)).after('<a class="add" style="float: left; margin-top: 26px;" href="javascript:void(0);" onclick="addMorePassengerRow(this)" id="addMorePassenger"></a>'
                );
    var allDivBlock = $("div[id^='person_in_database_block']");
    if (allDivBlock.length == 1)
        $("div[id^='person_in_database_block']").find(".remove").remove();
    numOfPassengerOnPage = numOfPassengerOnPage - 1
}
function deleteMoreMnaualPersonRow(id) {
    $("#manual_block_" + id).remove();
    var lastBlock = $("div[id^='manual_block']").last();
    if ($(lastBlock).find(".add").length == 0)
        $(".remove", $(lastBlock)).after(
                '<a class="add" style="float: left; margin-top: 26px;" href="javascript:void(0);" onclick="addMoreMnaualPersonRow(this);" id="addMoreMnaualPerson"></a>'
                );
    var allDivBlock = $("div[id^='manual_block']");
    if (allDivBlock.length == 1)
        $("div[id^='manual_block']").find(".remove").remove()
}
$(function() {
    
        tinymce.init({
            mode: "specific_textareas",
            editor_selector: "myTextEditor",
            plugins: [
                "link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste jbimages"
            ],
            toolbar: "insertfile undo redo | table styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link jbimages",
            file_browser_callback: 'myFileBrowser',
            image_advtab: true,
            height: 300,
            menubar: false,
            width: '100%',
       setup: function(editor) {
        editor.on('change', function(e) {
            tinymce.get("story").save();
            $("#" + editor.id).valid();
        });
    },

        });
     jQuery.validator.addMethod("checkeditorMin", function(value, element) {
          if (getStats("story").chars == "" || getStats("story").chars == "0" ) {
                    return false;
                }else{
                    return true;
                }      
       }, "Please enter min story");
    
    jQuery.validator.addMethod("checkeditorMax", function(value, element) {
        if (getStats("story").chars > numOfCharacterAllowedForStory) {
                    return false;
                }else{
                    return true;
                }
       }, "Please enter max story");
       
    $("#location_one_caption").click(function(){
         if ($("#location_one_video_type").is(":checked") || $("#location_one_image_type").is(":checked")){
             $('input[name="location_one_caption"]').rules('add', {minlength: 5,maxlength:200});
         }else{
             $('input[name="location_one_caption"]').rules('remove', 'minlength');
             $('input[name="location_one_caption"]').rules('remove', 'maxlength');
         }
    });
     $("#location_two_caption").click(function(){
         if ($("#location_two_video_type").is(":checked") || $("#location_two_image_type").is(":checked")){
             $('input[name="location_two_caption"]').rules('add', {minlength: 5,maxlength:200});
         }else{
             $('input[name="location_two_caption"]').rules('remove', 'minlength');
             $('input[name="location_two_caption"]').rules('remove', 'maxlength');
         }
    });
     $("#location_three_caption").click(function(){
         if ($("#location_three_video_type").is(":checked") || $("#location_three_image_type").is(":checked")){
             $('input[name="location_three_caption"]').rules('add', {minlength: 5,maxlength:200});
         }else{
             $('input[name="location_three_caption"]').rules('remove', 'minlength');
             $('input[name="location_three_caption"]').rules('remove', 'maxlength');
         }
    });
     $("#location_four_caption").click(function(){
         if ($("#location_four_video_type").is(":checked") || $("#location_four_image_type").is(":checked")){
             $('input[name="location_four_caption"]').rules('add', {minlength: 5,maxlength:200});
         }else{
             $('input[name="location_four_caption"]').rules('remove', 'minlength');
             $('input[name="location_four_caption"]').rules('remove', 'maxlength');
         }
    });
     $("#location_five_caption").click(function(){
         if ($("#location_five_video_type").is(":checked") || $("#location_five_image_type").is(":checked")){
             $('input[name="location_five_caption"]').rules('add', {minlength: 5,maxlength:200});
         }else{
             $('input[name="location_five_caption"]').rules('remove', 'minlength');
             $('input[name="location_five_caption"]').rules('remove', 'maxlength');
         }
    });
         // $("#addFamilyHistory")
  var validator = $("#addFamilyHistory").submit(function() {
			// update underlying textarea before submit validation
			//tinyMCE.triggerSave();
                        tinymce.get("story").save();
		}).validate({
        ignore:'',
        submitHandler: function() {
            tinymce.get("story").save();
            var dataStr = $("#addFamilyHistory").serialize();
            if (numOfCharacterAllowedForStory != "")
                if (getStats("story").chars >
                        numOfCharacterAllowedForStory) {
                    $("#story_label").show();
                    $("#story_label").text("User can write upto " +
                            numOfCharacterAllowedForStory + " characters story");
                    return false
                } else {
                    $("#story_label").hide();
                    $("#story_label").text("")
                }
            showAjxLoader();
            $.ajax({type: "POST", data: dataStr, url: "/add-user-family-history", success: function(responseData) {
                    var
                            jsonObj = jQuery.parseJSON(responseData);
                    hideAjxLoader();
                    if (jsonObj.status === "success")
                        viewFamilyHistory("" + jsonObj.
                                family_history_id + "", "" + jsonObj.message + "");
                    else
                        $.each(jsonObj.message, function(i, msg) {
                            $("#" + i).after(
                                    '<label class="error" style="display:block;">' + msg + "</label>")
                        })
                }})
        }, rules: {
            title: {required: true, maxlength: 50}, 
            story: {required: true}, 
            is_sharing: {required: true}, 
            is_email_on: {required: function() {
                    if ($("#is_sharing").is(":checked"))
                        return true;
                    else
                        return false
                }},
            location_one_video: {
                required: function() { if ($("#location_one_video_type").is(":checked"))return true;else return false
                },
                youtubevideo: true},
            location_two_video: {required: function() {if ($("#location_two_video_type").is(":checked"))return true;else return false
                }, youtubevideo: true}, 
            location_three_video: { required: function() { var videourl = $("#location_three_video").val();
                    if ($("#location_three_video_type").is(":checked"))
                        return true;
                    else
                        return false
                }, youtubevideo: true},
            location_four_video: {required: function() {
                    if ($("#location_four_video_type").is(":checked"))
                        return true;
                    else
                        return false
                }, youtubevideo: true}, 
            location_five_video: {required: function() {
                    if ($("#location_five_video").is(":checked"))
                        return true;
                    else
                        return false
                }, youtubevideo: true},
        },
        messages: {
            title: {required: TITLE_EMPTY}, 
            story: {required: STORY_EMPTY},
            is_sharing: {required: SHARED_EMPTY}, 
            is_email_on: {required: FEATURED_EMAIL_EMPTY}, 
            location_one_video: {required: VIDEO_EMPTY},
            location_one_caption: {minlength:CAPTION_MIN_LENGHT,maxlength: CAPTION_MAX_LENGHT}, 
            location_two_video: {required: VIDEO_EMPTY},
            location_two_caption: {minlength:CAPTION_MIN_LENGHT,maxlength: CAPTION_MAX_LENGHT},
            location_three_video: {required: VIDEO_EMPTY}, 
            location_three_caption: {minlength:CAPTION_MIN_LENGHT,maxlength: CAPTION_MAX_LENGHT},
            location_four_video: {required: VIDEO_EMPTY},
            location_four_caption: {minlength:CAPTION_MIN_LENGHT,maxlength: CAPTION_MAX_LENGHT},
            location_five_video: {required:VIDEO_EMPTY},
            location_five_caption: {minlength:CAPTION_MIN_LENGHT,maxlength: CAPTION_MAX_LENGHT},
        }
        });
  validator.focusInvalid = function() {
			// put focus on tinymce on submit validation
			if (this.settings.focusInvalid) {
				try {
					var toFocus = $(this.findLastActive() || this.errorList.length && this.errorList[0].element || []);
					if (toFocus.is("story")) {
						tinyMCE.get(toFocus.attr("id")).focus();
					} else {
						toFocus.filter(":visible").focus();
					}
				} catch (e) {
					// ignore IE throwing errors when focusing hidden elements
				}
			}
		}
    $("#updateUserFamilyHistory").validate({submitHandler: function() {
            tinymce.get("story").save();
            if (
                    numOfCharacterAllowedForStory != "")
                if (getStats("story").chars > numOfCharacterAllowedForStory) {
                    $("#story_label").show();
                    $(
                            "#story_label").text("User can write upto " + numOfCharacterAllowedForStory + " characters story");
                    return false
                } else {
                    $(
                            "#story_label").hide();
                    $("#story_label").text("")
                }
            var dataStr = $("#updateUserFamilyHistory").serialize();
            showAjxLoader();
            $.ajax({type: "POST", data: dataStr, url: "/update-user-family-history", success: function(responseData) {
                    var jsonObj = jQuery.
                            parseJSON(responseData);
                    hideAjxLoader();
                    if (jsonObj.status === "success")
                        viewFamilyHistory("" + jsonObj.family_history_id + "",
                                "" + jsonObj.message + "");
                    else {
                        $(".detail-section").css("display", "block");
                        $.each(jsonObj.message, function(i, msg) {
                            $("#" + i).
                                    after('<label class="error" style="display:block;">' + msg + "</label>")
                        })
                    }
                }})
        }, rules: {title: {required: true, maxlength: 50},
            story: {required: true}, is_sharing: {required: true}, is_email_on: {required: function() {
                    if ($("#is_sharing").is(":checked"))
                        return true;
                    else
                        return false
                }}, location_one_video: {required: function() {
                    if ($("#location_one_video_type").is(":checked"))
                        return true;
                    else
                        return false
                }, youtubevideo: true}, location_two_video: {required: function() {
                    if ($(
                            "#location_two_video_type").is(":checked"))
                        return true;
                    else
                        return false
                }, youtubevideo: true}, location_three_video: {
                required: function() {
                    if ($("#location_three_video_type").is(":checked"))
                        return true;
                    else
                        return false
                }, youtubevideo: true},
            location_four_video: {required: function() {
                    if ($("#location_four_video_type").is(":checked"))
                        return true;
                    else
                        return false
                }
                , youtubevideo: true}, location_five_video: {required: function() {
                    if ($("#location_five_video").is(":checked"))
                        return true;
                    else
                        return false
                }, youtubevideo: true}}, messages: {title: {required: TITLE_EMPTY}, story: {required: STORY_EMPTY}, is_sharing: {
                required: SHARED_EMPTY}, is_email_on: {required: FEATURED_EMAIL_EMPTY}, 
			is_email_on: {required: FEATURED_EMAIL_EMPTY}, 
            location_one_video: {required: VIDEO_EMPTY},
            location_one_caption: {minlength:CAPTION_MIN_LENGHT,maxlength: CAPTION_MAX_LENGHT}, 
            location_two_video: {required: VIDEO_EMPTY},
            location_two_caption: {minlength:CAPTION_MIN_LENGHT,maxlength: CAPTION_MAX_LENGHT},
            location_three_video: {required: VIDEO_EMPTY}, 
            location_three_caption: {minlength:CAPTION_MIN_LENGHT,maxlength: CAPTION_MAX_LENGHT},
            location_four_video: {required: VIDEO_EMPTY},
            location_four_caption: {minlength:CAPTION_MIN_LENGHT,maxlength: CAPTION_MAX_LENGHT},
            location_five_video: {required:VIDEO_EMPTY},
            location_five_caption: {minlength:CAPTION_MIN_LENGHT,maxlength: CAPTION_MAX_LENGHT},
						}
						}
						)
})