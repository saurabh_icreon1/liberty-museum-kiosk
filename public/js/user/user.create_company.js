$(function(){$("#create-company").validate({submitHandler:function(){var dataStr=$("#create-company").serialize();$.ajax
({type:"POST",url:"/create-company",data:dataStr,success:function(responseData){if(!checkUserAuthenticationAjx(
responseData))return false;var jsonObj=jQuery.parseJSON(responseData);if(jsonObj.status=="success"){parent.$(
"#comp_name"+jsonObj.key).val(jsonObj.company_name);parent.$("#company_id"+jsonObj.key).val(jsonObj.comapny_id);parent.
jQuery.colorbox.close()}else if(jsonObj.already_exits=="yes")$("#name").after(
'<label class="error" style="display:block;">'+jsonObj.message+"</label>");else $.each(jsonObj.message,function(i,msg){$
("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")})}})},rules:{name:{required:true}},messages
:{name:{required:NAME_EMPTY}}})})