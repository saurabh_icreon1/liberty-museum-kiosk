var flag_record_status=true;
function refreshWohSaveDel(Opr,dataResult){
    $("#save_search_as").val("");
    $("#search_msg").
    removeClass("error");
    document.getElementById("search_woh").reset();
    $("#woh_search_results").css({
        display:"block"
    });
    $(
        "#woh_search_results").html(dataResult);
    $(".scroll-pane").jScrollPane();
    var countSavedSearch;
    if(Opr=="del")
        countSavedSearch=parseInt($.trim($("#count_saved_searches_input").val()))-parseInt(1);
    if(Opr=="add")countSavedSearch=
        parseInt($.trim($("#count_saved_searches_input").val()))+parseInt(1);
    $("#count_saved_searches_input").val(
        countSavedSearch);
    if($.trim(dataResult)=="")$("#woh_search_results").css({
        display:"none"
    });
    $(
        ".common-savesearch .dropdown").hide();
    $("#woh_search_msg").css("display","block");
    if(Opr=="del"){
        $("#woh_search_msg").
        html(WOH_SEARCH_SUCCESS_DELETED_MSG);
        $.colorbox.close()
        }
        if(Opr=="add")$("#woh_search_msg").html(WOH_SEARCH_SUCCESS_MSG);
    return false
    }
    $(function(){
    $("#savesearch").click(function(){
        authenticateUserBeforeAjax();
        $("#save_search").validate({
            submitHandler:function(){
                showAjxLoader();
                $("#woh_search_msg").css("display","none");
                $("#woh_search_msg").html("");
                if($.
                    trim($("#last_name").val())=="Last Name")$("#last_name").val("");
                if($.trim($("#save_search_as").val())==
                    "Save this search")$("#save_search_as").val("");
                if($.trim($("#save_search_as").val())==""||$.trim($("#save_search_as").
                    val())=="Save this search"){
                    hideAjxLoader();
                    $('label[for="save_search_as"]').remove();
                    $("#savesearch").after(
                        '<label class="error" for="save_search_as" generated="true">'+WOH_HEADING_SEARCH_ERROR_EMPTY_NAME+"</label>");
                    return false
                    }else if(($.trim($("#last_name").val())==""||$.trim($("#last_name").val())=="Last Name")&&($.trim($(
                    "#panel_number").val())==""||$.trim($("#panel_number").val())=="Panel #")){
                    hideAjxLoader();
                    $("#error_last_name_search").
                    remove();
                    $("#last_name").after('<div class="error" id="error_last_name_search">'+WOH_SEARCH_ENTER_LAST_NAME+"</div>");
                    return false
                    }else{
                    authenticateUserBeforeAjax();
                    hideAjxLoader();
                    $("#error_last_name_search").remove();
                    if($.trim($(
                        "#last_name").val())=="Last Name")$("#last_name").val("");
                    if($.trim($("#first_name").val())=="First Name")$(
                        "#first_name").val("");
                    if($.trim($("#other_part_name").val())=="Other Part of Name")$("#other_part_name").val("");
                    if($.
                        trim($("#country").val())=="Country of Origin")$("#country").val("");
                    if($.trim($("#donated_by_add").val())=="Donated By"
                        )$("#donated_by_add").val("");
                    if($.trim($("#panel_number").val())=="Panel #")$("#panel_number").val("");
                    $.ajax({
                        type:
                        "POST",
                        data:{
                            search_name:$.trim($("#save_search_as").val()),
                            searchString:$("#search_woh").serialize()+"&donated_by="+$.
                            trim($("#donated_by_add").val())
                            },
                        url:"/save-woh-search",
                        success:function(response){
                            var jsonObj=jQuery.parseJSON(
                                response);
                            if(jsonObj.status=="success")refreshWohSaveDel("add",jsonObj.dataresult);
                            else{
                                if(jsonObj.status=="exceed"){
                                    $.
                                    colorbox({
                                        width:"600px",
                                        height:"320px",
                                        inline:true,
                                        href:".exceed"
                                    });
                                    $(".exceed").show();
                                    $("#cboxClose").on("click",
                                        function(){
                                            $(".exceed").hide()
                                            })
                                    }
                                    $("#search_msg").html("")
                                }
                                if($.trim($("#last_name").val())=="")$("#last_name").val(
                                "Last Name");
                            if($.trim($("#first_name").val())=="")$("#first_name").val("First Name");
                            if($.trim($("#other_part_name").
                                val())=="")$("#other_part_name").val("Other Part of Name");
                            if($.trim($("#country").val())=="")$("#country").val(
                                "Country of Origin");
                            if($.trim($("#donated_by_add").val())=="")$("#donated_by_add").val("Donated By");
                            if($.trim($(
                                "#panel_number").val())=="")$("#panel_number").val("Panel #");
                            hideAjxLoader()
                            }
                        })
                }
            },
        errorPlacement:function(error,element
            ){
            $("#woh_search_msg").css("display","none");
            $("#woh_search_msg").html("");
            if(element.attr("name")=="save_search_as")
                error.insertAfter($("#savesearch"));else error.insertAfter(element)
                },
        rules:{
            save_search_as:{
                required:true
            }
        },
    messages:{
        save_search_as:{
            required:WOH_HEADING_SEARCH_ERROR_EMPTY_NAME
        }
    }
    })
});
$.validator.addMethod("AlphaNumericCommaSpace",
    function(alpha_numeric_text){
        var alpha_number_regex=/^([a-zA-Z0-9,\s]+)$/;
        if(!alpha_number_regex.test(alpha_numeric_text
            )&&alpha_numeric_text!="")return false;else return true
            },"");
$.validator.addMethod("TestDateValidation",function(
    testDate){
    var date_regex=/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
    if(!date_regex.test(testDate))
        return false;else return true
        },"");
$("#bio_certificate_woh").validate({
    submitHandler:function(){
        showAjxLoader();
        $.ajax({
            type:"POST",
            data:$("#bio_certificate_woh").serialize(),
            url:"/add-to-cart-woh-products",
            success:function(response){
                var 
                jsonObj=jQuery.parseJSON(response);
                if(jsonObj.status=="success"){
                    jQuery.noConflict();
                    jQuery.colorbox.close();
                    showAddToCartConfirmation()
                    }
                    hideAjxLoader();
                return false
                }
            })
    },
rules:{
    bio_certificate_qty:{
        required:true,
        number:true
    }
},
messages:{
    bio_certificate_qty:{
        required:WOH_BIO_CERTIFICATE_ERROR_NUMBER_OF_QUANTITY,
        number:
        WOH_BIO_CERTIFICATE_ERROR_NUMBER_OF_QUANTITY_ONLY_NUMBER
    }
}
})
});
function viewType(viewTypeVal,ObjThis){
    try{
        if(viewTypeVal
            !=undefined&&$.trim(viewTypeVal)!=""){
            $("#view_woh_type").val($.trim(viewTypeVal));
            if($.trim(viewTypeVal)=="2"){
                $(
                    "#wohListView").addClass("active");
                $("#wohGridView").removeClass("active");
                $("#limit").val("20")
                }else{
                $("#wohGridView").
                addClass("active");
                $("#wohListView").removeClass("active");
                $("#limit").val("6")
                }
            }else{
        $("#view_woh_type").val("1");
        $(
            "#wohGridView").addClass("active");
        $("#wohListView").removeClass("active");
        $("#limit").val("6")
        }
    }catch(e){
    $(
        "#view_woh_type").val("1");
    $("#wohGridView").addClass("active");
    $("#wohListView").removeClass("active");
    $("#limit").val(
        "6")
    }
    sortSearchWoh($("#sort_field").val())
}
function viewSavedSearch(search_id){
    try{
        $("#woh_search_msg").css({
            display:
            "none"
        });
        $("#woh_search_msg").empty();
        showAjxLoader();
        $.ajax({
            type:"POST",
            data:{
                woh_search_id:search_id
            },
            url:
            "/get-woh-user-saved-search-select",
            success:function(response){
                hideAjxLoader();
                var searchParamArray=response,obj=jQuery.
                parseJSON(searchParamArray);
                document.getElementById("search_woh").reset();
                $("#last_name").val(obj.last_name);
                $(
                    "#first_name").val(obj.first_name);
                $("#other_part_name").val(obj.other_part_name);
                $("#country").val(obj.country);
                $(
                    "#panel_number").val(obj.panel_number);
                $("#donated_by_add").val(obj.donated_by);
                $("#submitsearch1").click()
                }
            })
    }catch(e){
}
}
function deleteSaveSearch(search_id){
    $("#woh_search_msg").empty();
    $("#woh_search_msg").css({
        display:"none"
    });
    $(
        "#wohSearchDelId").val($.trim(search_id));
    var flag_del_woh_search=true;
    $(".deletesearch").colorbox({
        width:"700px",
        height
        :"270px",
        inline:true,
        onOpen:function(){
            flag_del_woh_search=false
            },
        onClosed:function(){
            $("#wohSearchDelId").val("")
            }
        });
$(
    "#no").click(function(){
    $.colorbox.close();
    $("#yes").unbind("click");
    $("#wohSearchDelId").val("");
    return false
    });
$(
    "#yes").click(function(){
    if(search_id!=""&&$.trim(search_id)==$.trim($("#wohSearchDelId").val())&&flag_del_woh_search==
        false)$.ajax({
        type:"POST",
        data:{
            woh_search_id:$.trim($("#wohSearchDelId").val())
            },
        url:"/delete-woh-search",
        success:
        function(response){
            $("#wohSearchDelId").val("");
            var jsonObj=jQuery.parseJSON(response);
            if(jsonObj.status==="success")
                refreshWohSaveDel("del",jsonObj.dataresult)
                }
            })
    })
}
function NextRecords(sort_field,sort_order,limit,start_index,act){
    var page=
    parseInt($("#page").val());
    $("#sort_field").val(sort_field);
    $("#sort_order").val(sort_order);
    $("#limit").val(limit);
    $("#start_index").val(start_index);
    $("#loader-div").html('<img src ="/images/loading.gif">');
    if(act == 'next')
        $("#page").val(page+1);
    else if(act == 'previous')    
        $("#page").val(page-1);
        
    //$("#load_more"+page).html("");
    var dataStr="",last_name_s="",first_name_s="",other_part_name_s="",country_s="",donated_by_s="",panel_number_s="";
    if($.
        trim($("#last_name").val())=="Last Name")last_name_s="";else last_name_s=$.trim($("#last_name").val());
    if($.trim($(
        "#first_name").val())=="First Name")first_name_s="";else first_name_s=$.trim($("#first_name").val());
    if($.trim($(
        "#other_part_name").val())=="Other Part of Name")other_part_name_s="";else other_part_name_s=$.trim($("#other_part_name"
        ).val());
    if($.trim($("#country").val())=="Country of Origin")country_s="";else country_s=$.trim($("#country").val());
    if(
        $.trim($("#donated_by_add").val())=="Donated By")donated_by_s="";else donated_by_s=$.trim($("#donated_by_add").val());
    if($.trim($("#panel_number").val())=="Panel #")panel_number_s="";else panel_number_s=$.trim($("#panel_number").val());
    dataStr="view_woh_type="+$("#view_woh_type").val()+"&"+"page="+$("#page").val()+"&"+"sort_field="+$("#sort_field").val()
    +"&"+"sort_order="+$("#sort_order").val()+"&"+"start_index="+$("#start_index").val()+"&"+"limit="+$("#limit").val()+"&"+
    "last_name="+last_name_s+"&"+"first_name="+first_name_s+"&"+"other_part_name="+other_part_name_s+"&"+"country="+
    country_s+"&"+"donated_by="+donated_by_s+"&"+"panel_number="+panel_number_s;
    $.ajax({
        type:"POST",
        data:{
            searchString:
            dataStr,
            sort_field:sort_field,
            sort_order:sort_order,
            limit:limit
        },
        url:"/more-woh-list",
        success:function(responseData){
            flag_record_status=true;
            //$("#loadingDiv"+"-"+page).html("");
            //$("div.js_load_more").remove();
            $("#searchResult").html(responseData);
            //$("#load_more"+"-"+page).html("");
            //var moveHtml=$(".js_load_more").clone();
            //$(".js_load_more").remove();
            //$("#lastDiv").after(moveHtml);
            $(".scroll-pane").jScrollPane()
            }
        })
}
/*
function loadMore(sort_field,sort_order,limit,start_index){
    var page=
    parseInt($("#page").val());
    $("#sort_field").val(sort_field);
    $("#sort_order").val(sort_order);
    $("#limit").val(limit);
    $(
        "#start_index").val(start_index);
    if($("#loadingDiv"+"-"+page))$("#loadingDiv"+"-"+page).append(
        '<div class="loader-class"><img src ="/images/loading.gif"></div>');
    $("#page").val(page+1);
    $("#load_more"+page).html("")
    ;
    var dataStr="",last_name_s="",first_name_s="",other_part_name_s="",country_s="",donated_by_s="",panel_number_s="";
    if($.
        trim($("#last_name").val())=="Last Name")last_name_s="";else last_name_s=$.trim($("#last_name").val());
    if($.trim($(
        "#first_name").val())=="First Name")first_name_s="";else first_name_s=$.trim($("#first_name").val());
    if($.trim($(
        "#other_part_name").val())=="Other Part of Name")other_part_name_s="";else other_part_name_s=$.trim($("#other_part_name"
        ).val());
    if($.trim($("#country").val())=="Country of Origin")country_s="";else country_s=$.trim($("#country").val());
    if(
        $.trim($("#donated_by_add").val())=="Donated By")donated_by_s="";else donated_by_s=$.trim($("#donated_by_add").val());
    if($.trim($("#panel_number").val())=="Panel #")panel_number_s="";else panel_number_s=$.trim($("#panel_number").val());
    dataStr="view_woh_type="+$("#view_woh_type").val()+"&"+"page="+$("#page").val()+"&"+"sort_field="+$("#sort_field").val()
    +"&"+"sort_order="+$("#sort_order").val()+"&"+"start_index="+$("#start_index").val()+"&"+"limit="+$("#limit").val()+"&"+
    "last_name="+last_name_s+"&"+"first_name="+first_name_s+"&"+"other_part_name="+other_part_name_s+"&"+"country="+
    country_s+"&"+"donated_by="+donated_by_s+"&"+"panel_number="+panel_number_s;
    $.ajax({
        type:"POST",
        data:{
            searchString:
            dataStr,
            sort_field:sort_field,
            sort_order:sort_order,
            limit:limit
        },
        url:"/more-woh-list",
        success:function(responseData){
            flag_record_status=true;
            $("#loadingDiv"+"-"+page).html("");
            $("div.js_load_more").remove();
            $("#lastDiv").append(
                responseData);
            $("#load_more"+"-"+page).html("");
            var moveHtml=$(".js_load_more").clone();
            $(".js_load_more").remove();
            $(
                "#lastDiv").after(moveHtml);
            $(".scroll-pane").jScrollPane()
            }
        })
}*/
$(window).data("ajaxready",true).scroll(function(e){ return false;
    if(($
        .trim($("#lastDiv li").last().html())!=""&&$("#view_woh_type").val()=="1"||$.trim($("#lastDiv div").last().html())!=""&&
        $("#view_woh_type").val()=="2")&&flag_record_status==true){
        var spy,container=$("#container");
        if($(window).data(
            "ajaxready")==false)return;
        var WindowHeight=$(window).height();
        if($(window).scrollTop()>=container.outerHeight()-400){
            $(
                window).data("ajaxready",false);
            var page=parseInt($("#page").val());
            if($("#loadingDiv"+"-"+page))$("#loadingDiv"+"-"+
                page).append('<div class="loader-class"><img src ="/images/loading.gif"></div>');
            page=parseInt($("#page").val());
            $(
                "#page").val(page+1);
            var dataStr="",last_name_s="",first_name_s="",other_part_name_s="",country_s="",donated_by_s="",
            panel_number_s="";
            if($.trim($("#last_name").val())=="Last Name")last_name_s="";else last_name_s=$.trim($("#last_name").
                val());
            if($.trim($("#first_name").val())=="First Name")first_name_s="";else first_name_s=$.trim($("#first_name").val());
            if($.trim($("#other_part_name").val())=="Other Part of Name")other_part_name_s="";else other_part_name_s=$.trim($(
                "#other_part_name").val());
            if($.trim($("#country").val())=="Country of Origin")country_s="";else country_s=$.trim($(
                "#country").val());
            if($.trim($("#donated_by_add").val())=="Donated By")donated_by_s="";else donated_by_s=$.trim($(
                "#donated_by_add").val());
            if($.trim($("#panel_number").val())=="Panel #")panel_number_s="";else panel_number_s=$.trim($(
                "#panel_number").val());
            dataStr="view_woh_type="+$("#view_woh_type").val()+"&"+"page="+$("#page").val()+"&"+
            "sort_field="+$("#sort_field").val()+"&"+"sort_order="+$("#sort_order").val()+"&"+"start_index="+$("#start_index").val()
            +"&"+"limit="+$("#limit").val()+"&"+"last_name="+last_name_s+"&"+"first_name="+first_name_s+"&"+"other_part_name="+
            other_part_name_s+"&"+"country="+country_s+"&"+"donated_by="+donated_by_s+"&"+"panel_number="+panel_number_s;
            $.ajax({
                type:"POST",
                cache:false,
                data:{
                    searchString:dataStr,
                    page:$("#page").val()
                    },
                url:"/more-woh-list",
                success:function(response
                    ){
                    $("#loadingDiv"+"-"+page).html("");
                    $("div.js_load_more").remove();
                    $("#lastDiv").append(response);
                    $(window).data(
                        "ajaxready",true);
                    $("#load_more"+"-"+page).html("");
                    var moveHtml2=$(".js_load_more").clone();
                    $(".js_load_more").remove()
                    ;
                    $("#lastDiv").after(moveHtml2);
                    $(".scroll-pane").jScrollPane();
                    if($.trim(response)=="")flag_record_status=false
                        }
                    })
        }
    }
});
function sortSearchWoh(fieldName){
    if($.trim(fieldName)!="")$("#sort_order").val("asc");else $("#sort_order").val("desc");
    if($.trim($("#lastDiv").html())!=""&&$("#lastDiv").html()!=undefined&&$.trim(fieldName)!=""){
        $("#sort_field").val($.
            trim(fieldName));
        $("#start_index").val("0");
        $("#page").val("1");
        var dataStr="",last_name_s="",first_name_s="",
        other_part_name_s="",country_s="",donated_by_s="",panel_number_s="";
        if($.trim($("#last_name").val())=="Last Name")
            last_name_s="";else last_name_s=$.trim($("#last_name").val());
        if($.trim($("#first_name").val())=="First Name")
            first_name_s="";else first_name_s=$.trim($("#first_name").val());
        if($.trim($("#other_part_name").val())==
            "Other Part of Name")other_part_name_s="";else other_part_name_s=$.trim($("#other_part_name").val());
        if($.trim($(
            "#country").val())=="Country of Origin")country_s="";else country_s=$.trim($("#country").val());
        if($.trim($(
            "#donated_by_add").val())=="Donated By")donated_by_s="";else donated_by_s=$.trim($("#donated_by_add").val());
        if($.trim($
            ("#panel_number").val())=="Panel #")panel_number_s="";else panel_number_s=$.trim($("#panel_number").val());
        dataStr=
        "view_woh_type="+$("#view_woh_type").val()+"&"+"page="+$("#page").val()+"&"+"sort_field="+$("#sort_field").val()+"&"+
        "sort_order="+$("#sort_order").val()+"&"+"start_index="+$("#start_index").val()+"&"+"limit="+$("#limit").val()+"&"+
        "last_name="+last_name_s+"&"+"first_name="+first_name_s+"&"+"other_part_name="+other_part_name_s+"&"+"country="+
        country_s+"&"+"donated_by="+donated_by_s+"&"+"panel_number="+panel_number_s;
        showAjxLoader();
        $.ajax({
            type:"POST",
            data:{
                searchString:dataStr
            },
            url:"/woh-list",
            success:function(responseData){
                hideAjxLoader();
                $("#searchResult").html(
                    responseData);
                $(".scroll-pane").jScrollPane();
                flag_record_status=true
                }
            })
    }
}
function ViewRelatedProducts(wohParam){
    showAjxLoader();
    parent.$("#ajx_loader").css({"z-index":"2147483647"});
    parent.$("#ajx_loader_image").css({"z-index":"2147483647"});
    $.colorbox({
        href:"/view-woh-products/"+wohParam,
        iframe:true,
        width:"900px",
        height:"200px",
        overlayClose:false,
        escKey:false,
        fastIframe: false,
        onOpen:function(){
            $("#cboxClose").css({display:"none"});
       },
       onComplete:function(){
            parent.$("#ajx_loader").css({"z-index":""});
            parent.$("#ajx_loader_image").css({"z-index":""});
            hideAjxLoader();
        }
     });
}
function ViewMoreProducts(wohid){
    showAjxLoader();
    parent.$("#ajx_loader").css({"z-index":"2147483647"});
    parent.$("#ajx_loader_image").css({"z-index":"2147483647"});
    $.colorbox({
        href:"/get-woh-other-products",
        width:"1200px",
        data:{
            wohid:wohid
        },
        onComplete:function(){
            parent.$("#ajx_loader").css({"z-index":""});
            parent.$("#ajx_loader_image").css({"z-index":""});
            hideAjxLoader();
        },
        escKey:false,
        closeButton:true,
        showClose:true
    })
    }
    function addToCartWohProducts(productid,
    wohid,inforeq,prodtype,numQty){
    $('label[for="bio_certificate_qty"]').remove();
    var flag_c=true;
    if(inforeq=="2"&&(prodtype
        =="1"||prodtype=="2"))if($.trim(numQty)==""){
        $('label[for="bio_certificate_qty"]').remove();
        $("#bio_certificate_qty").
        after('<label for="bio_certificate_qty" class="error">'+WOH_BIO_CERTIFICATE_ERROR_NUMBER_OF_QUANTITY+"</label>");
        flag_c=
        false
        }else if(/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(numQty)==false){
        $('label[for="bio_certificate_qty"]').
        remove();
        $("#bio_certificate_qty").after('<label for="bio_certificate_qty" class="error">'+
            WOH_BIO_CERTIFICATE_ERROR_NUMBER_OF_QUANTITY_ONLY_NUMBER+"</label>");
        flag_c=false
        }
        if(flag_c==true)$.ajax({
        type:"POST",
        data:{
            productid:productid,
            wohid:wohid,
            inforeq:inforeq,
            prodtype:prodtype,
            numQty:numQty
        },
        url:"/add-to-cart-woh-products",
        success:function(response){
            var jsonObj=jQuery.parseJSON(response);
            if(jsonObj.status==="success"){
                jQuery.noConflict();
                jQuery.colorbox.close();
                showAddToCartConfirmation()
                }
            }
    })
}
$(document).delegate("#addedToCartWohClose","click",function(){
    parent.jQuery.colorbox.close()
    })
    
    /*this is used to show the woh image*/
    function showWohImage(panelNo) {
    showAjxLoader();
    parent.$("#ajx_loader").css({"z-index":"2147483647"});
    parent.$("#ajx_loader_image").css({"z-index":"2147483647"});
    $.colorbox({
        href:"/view-woh-image/"+panelNo,
        iframe:true,
        width:"670px",
        height:"542px",
        overlayClose:false,
        escKey:true,
        fastIframe: false,
        onOpen:function(){
       },
       onComplete:function(){
            parent.$("#ajx_loader").css({"z-index":""});
            parent.$("#ajx_loader_image").css({"z-index":""});
            hideAjxLoader();
        }
     });
    }
    
function displaypopclick(pid){
        $(document).ready(function() {

        $('.fancybox-thumbs'+pid).fancybox({
                prevEffect : 'none',
                nextEffect : 'none',
                closeBtn  : true,
                arrows    : false,
                nextClick : false,
                mouseWheel : true,                                                    
                afterClose:function(){
                    $(".fancybox-thumbs"+pid).hide();
                }
            });    
          });  

        displaypop(pid);
        $('.fancybox-thumbs'+pid).click(); 
        $("a[rel^='lightbox']").slimbox({});
    }
    
    function displaypop(pid)
    {
        $(".fancybox-thumbs"+pid).show();
    }    
    