$(function(){
    var LOWER=/[a-z]/,DIGIT=/[0-9]/,DIGITS=/[0-9].*[0-9]/,SPECIAL=/[^a-zA-Z0-9]/,SAME=/^(.)\1+$/;
    function 
    rating(rate,message){
        return{
            rate:rate,
            messageKey:message
        }
    }
    function uncapitalize(str){
    return str.substring(0,1).
    toLowerCase()+str.substring(1)
    }
    $.validator.passwordRating=function(password,username){
    if(!password||password.length<6)
        return rating(0,"too-short");
    if(SAME.test(password))return rating(1,"very-weak");
    var lower=LOWER.test(password),digit=
    DIGIT.test(password);
    if(lower&&digit)return rating(4,"strong");
    if(lower)return rating(3,"good");
    return rating(2,"weak")
    }
;
$.validator.passwordRating.messages={
    "too-short":"Too short",
    "very-weak":"Very weak",
    weak:"Weak",
    good:"Good",
    strong:
    "Strong"
};

$.validator.addMethod("passwordstrength",function(value,element,usernameField){
    var password=element.value,
    username=$(typeof usernameField!="boolean"?usernameField:[]),rating=$.validator.passwordRating(password,username.val()),
    meter=$(".password-meter",element.form);
    meter.find(".password-meter-bar").removeClass().addClass("password-meter-bar").
    addClass("password-meter-"+rating.messageKey);
    meter.find(".password-meter-message").removeClass().addClass(
        "password-meter-message").addClass("password-meter-message-"+rating.messageKey).text($.validator.passwordRating.messages
        [rating.messageKey]);
    return rating.rate>2
    },"Please enter valid passwoed");
$.validator.classRuleSettings.password={
    password:true
};

$.validator.addMethod("alphanumeric",function(value,element){
    return this.optional(element)||
    /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,25}$/.test(value)
    },PASSWORD_INVALID);
$.validator.addMethod("alphabeticapostrophe",
    function(value,element){
        return this.optional(element)||/^[A-Za-z ']+$/i.test(value)},VALID_DETAILS);$.validator.
    addMethod("alphabetic",function(value,element){
        return this.optional(element)||/^[A-Za-z ]+$/i.test(value)
        },VALID_DETAILS
    );
    $.validator.addMethod("zipvalid",function(value,element){
        return this.optional(element)||/^[0-9]+$/i.test(value)
        },
    VALID_ZIP);
    $.validator.addMethod("phonevalid",function(value,element){
        return this.optional(element)||/^[0-9-+]+$/i.test(
            value)
        },VALID_PHONE);
    $.validator.addMethod("cityalphabetic",function(value,element){
        return this.optional(element)||
        /^[A-Za-z -]+$/i.test(value)
        },VALID_CITY);
    $.validator.addMethod("statealphabetic",function(value,element){
        return this.
        optional(element)||/^[A-Za-z ]+$/i.test(value)
        },VALID_STATE);
    $.validator.addMethod("validaddress",function(value,element
        ){
        return this.optional(element)||/^[A-Za-z0-9 ,.-]+$/i.test(value)
        },VALID_STATE);
    var validator=$("#crm_user").validate({
        submitHandler:function(){
            var dataStr=$("#crm_user").serialize();
            showAjxLoader();
            $.ajax({
                type:"POST",
                url:
                "/create-crm-user",
                data:dataStr,
                success:function(responseData){
                    hideAjxLoader();
                    if(!checkUserAuthenticationAjx(
                        responseData))return false;
                    var jsonObj=jQuery.parseJSON(responseData);
                    if(jsonObj.status=="success")window.location.href=
                        "/crm-users";else $.each(jsonObj.message,function(i,msg){
                        $("#"+i).after('<label class="error" style="display:block;">'+
                            msg+"</label>")
                        })
                    }
                    })
        },
    highlight:function(label){
        $(".detail-section").css("display","block")
        },
    ignore:"",
    rules:{
        first_name
        :{
            required:true,
            alphabeticapostrophe:true,
            minlength:1,
            maxlength:50
        },
        last_name:{
            required:true,
            alphabeticapostrophe:true,
            minlength:1,
            maxlength:20
        },
        crm_email_id:{
            required:true,
            email:true,
            minlength:6
        },
        password:{
            required:true,
            minlength:6,
            maxlength:25,
            alphanumeric:true,
            passwordstrength:true
        },
        password_conf:{
            required:true,
            equalTo:"#crm_user #password",
            passwordstrength:false
        },
        status:{
            required:true
        },
        "role[]":{
            required:true
        },
        country_id:{
            required:true
        },
        address:{
            required:
            true,
           // validaddress:true,
            minlength:1,
            maxlength:30
        },
        zip:{
            required:true,
            zipvalid:true,
            minlength:4,
            maxlength:8
        },
        city:{
            required:true,
            cityalphabetic:true,
            minlength:2,
            maxlength:15
        },
        state:{
//            required:function(){
//                if($("#country_id").val()==228)
//                    return false;
//                return true
//                },
            statealphabetic:true,
            minlength:2,
            maxlength:25
        },
        state_select:{
            required:function(){
                if($(
                    "#country_id").val()==228)return true;
                return false
                }
            },
    phone_no:{
        phonevalid:true,
        required:true,
        maxlength:20
    }
    },
messages:{
    first_name:{
        required:EMPTY_FIRST_NAME
    },
    last_name:{
        required:EMPTY_LAST_NAME
    },
    crm_email_id:{
        required:EMPTY_EMAIL,
        email:
        EMAIL_VALID
    },
    password:{
        required:EMPTY_PASSWORD
    },
    password_conf:{
        required:EMPTY_CON_PASSWORD,
        equalTo:VALID_CON_PASSWORD
    },
    status:{
        required:EMPTY_STATUS
    },
    "role[]":{
        required:EMPTY_ROLE
    },
    country_id:{
        required:EMPTY_COUNTRY
    },
    address:{
        required:
        EMPTY_ADDRESS
    },
    zip:{
        required:EMPTY_ZIP
    },
    city:{
        required:EMPTY_CITY
    },
    state:{
       // required:EMPTY_STATE
    },
    state_select:{
        required:
        EMPTY_STATE_SELECT
    },
    phone_no:{
        required:PHONE_EMPTY
    }
}
});
$("#edit_crm_user").validate({
    submitHandler:function(){
        var 
        dataStr=$("#edit_crm_user").serialize();
        showAjxLoader();
        $.ajax({
            type:"POST",
            url:"/edit-crm-user",
            data:dataStr,
            success:
            function(responseData){
                hideAjxLoader();
                if(!checkUserAuthenticationAjx(responseData))return false;
                var jsonObj=jQuery.
                parseJSON(responseData);
                if(jsonObj.status=="success")window.location.href="/crm-users";else $.each(jsonObj.message,
                    function(i,msg){
                        $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                        })
                }
                })
    },
highlight:function(
    label){
    $(".detail-section").css("display","block")
    },
ignore:"",
rules:{
    first_name:{
        required:true,
        alphabeticapostrophe:true
        ,
        minlength:1,
        maxlength:50
    },
    last_name:{
        required:true,
        alphabeticapostrophe:true,
        minlength:1,
        maxlength:50
    },
    crm_email_id:{
        required:true,
        email:true,
        minlength:6
    },
    status:{
        required:true
    },
    "role[]":{
        required:true
    },
    country_id:{
        required:true
    },
    address
    :{
        required:true,
       // validaddress:true,
        minlength:1,
        maxlength:30
    },
    zip:{
        required:true,
        zipvalid:true,
        minlength:4,
        maxlength:8
    },
    city:{
        required:true,
        cityalphabetic:true,
        minlength:2,
        maxlength:15
    },
    state:{
//        required:function(){
//            if($("#country_id").val()==
//                228)return false;
//            return true
//            },
        statealphabetic:true,
        minlength:2,
        maxlength:25
    },
    state_select:{
        required:function(){
            if($(
                "#country_id").val()==228)return true;
            return false
            }
        },
phone_no:{
    phonevalid:true,
    required:true,
    maxlength:20
}
},
messages:{
    first_name:{
        required:EMPTY_FIRST_NAME
    },
    last_name:{
        required:EMPTY_LAST_NAME
    },
    crm_email_id:{
        required:EMPTY_EMAIL,
        email:
        EMAIL_VALID
    },
    status:{
        required:EMPTY_STATUS
    },
    "role[]":{
        required:EMPTY_ROLE
    },
    country_id:{
        required:EMPTY_COUNTRY
    },
    address:{
        required:EMPTY_ADDRESS
    },
    zip:{
        required:EMPTY_ZIP
    },
    city:{
        required:EMPTY_CITY
    },
    state:{
       // required:EMPTY_STATE
    },
    state_select:{
        required:EMPTY_STATE_SELECT
    },
    phone_no:{
        required:PHONE_EMPTY
    }
}
});
$("#report_to").autocomplete({
    afterAdd:true,
    selectFirst:
    true,
    autoFocus:true,
    source:function(request,response){
        $.ajax({
            url:"/get-crm-contacts",
            method:"POST",
            dataType:"json",
            data
            :{
                crm_full_name:$("#report_to").val()
                },
            success:function(jsonResult){
                if(!checkUserAuthenticationAjx(jsonResult))
                    return false;
                var resultset=[];
                $.each(jsonResult,function(){
                    var id=this.crm_user_id,value=this.crm_full_name;
                    resultset.
                    push({
                        id:this.crm_user_id,
                        value:this.crm_full_name
                        })
                    });
                response(resultset)
                }
            })
    },
open:function(){
    $(".ui-menu").width(300)
    }
,
change:function(event,ui){
    if(ui.item){
        $("#report_to").val(ui.item.value);
        $("#report_to_id").val(ui.item.id)
        }else{
        $(
            "#report_to").val("");
        $("#report_to_id").val("")
        }
        $(".ui-autocomplete").jScrollPane()
    },
focus:function(event,ui){
    $(
        "#report_to_id").val(ui.item.id);
    return false
    },
select:function(event,ui){
    $("#report_to").val(ui.item.value);
    $(
        "#report_to_id").val(ui.item.id);
    return false
    }
});
var url="/upload-crm-user-profile";
$("#virtualface").fileupload({
    url:
    url,
    dataType:"json",
    start:function(e){
        $("#submitbutton").prop("disabled",true);
        showAjxLoader()
        },
    done:function(e,data){
        hideAjxLoader();
        data=data.result;
        if(data.status=="success"){
            $("#filemessage").addClass("success-msg");
            $("#filemessage").
            removeClass("error-msg");
            $("#thumbnail_virtualface").val(data.filename)
            }else{
            $("#filemessage").removeClass("success-msg"
                );
            $("#filemessage").addClass("error-msg");
            $("#thumbnail_virtualface").val("")
            }
            $("#filemessage").html(data.message);
        $(
            "#submitbutton").prop("disabled",false)
        },
    progressall:function(e,data){
        var progress=parseInt(data.loaded/data.total*100,
            10);
        $("#progress .bar").css("width",progress+"%")
        }
    }).prop("disabled",!$.support.fileInput).parent().addClass($.support.
    fileInput?undefined:"disabled");
$("#remove").click(function(){
    $("#upload_old_div").hide();
    $("#upload_new_div").show();
    $(
        "#thumbnail_virtualface").val("")
    })
});
function passworEdit(){
    var crm_user_id=$("#crm_user_id").val();
    $.colorbox({
        width:
        "900px",
        height:"500px",
        iframe:true,
        href:"/change-crm-user-password/"+crm_user_id
        })
    }
    function onChangeCountry(countryId){
    if(countryId==228){
        $("#state_text_block").hide();
        $("#state_select_block").show()
        }else{
        $("#state_text_block").show();
        $(
            "#state_select_block").hide()
        }
    }