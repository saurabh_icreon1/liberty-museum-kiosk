function viewContact(){
    showAjxLoader();
    $.ajax({
        type:"POST",
        data:{},
        url:"/contact-detail/"+user_id,
        success:function(
            response){
            hideAjxLoader();
            if(!checkUserAuthenticationAjx(response))return false;
            $("#contact_content").html(response)
            }
        })
}
function editContact(){
    showAjxLoader();
    $.ajax({
        type:"GET",
        data:{},
        url:"/edit-contact/"+user_id,
        success:function(response
            ){
            hideAjxLoader();
            if(!checkUserAuthenticationAjx(response))return false;
            $("#contact_content").html(response)
            }
        })
}
function
userTransaction(){
    showAjxLoader();
    $.ajax({
        type:"POST",
        data:{},
        url:"/user-transactions/"+user_id,
        success:function(
            response){
            hideAjxLoader();
            if(!checkUserAuthenticationAjx(response))return false;
            $("#contact_content").html(response)
            }
        })
}
function userCampaign(){
    showAjxLoader();
    $.ajax({
        type:"POST",
        data:{},
        url:"/user-campaigns/"+user_id,
        success:function(
            response){
            hideAjxLoader();
            if(!checkUserAuthenticationAjx(response))return false;
            $("#contact_content").html(response)
            }
        })
}
function userSurvey(){
    showAjxLoader();
    $.ajax({
        type:"GET",
        data:{},
        url:"/user-survey/"+user_id,
        success:function(response){
            hideAjxLoader();
            if(!checkUserAuthenticationAjx(response))return false;
            $("#contact_content").html(response)
            }
        })
}
function 
userSaveSearch(){
    showAjxLoader();
    $.ajax({
        type:"GET",
        data:{},
        url:"/user-searches/"+user_id,
        success:function(response){
            hideAjxLoader();
            if(!checkUserAuthenticationAjx(response))return false;
            $("#contact_content").html(response)
            }
        })
}
function 
userDevelopment(){
    showAjxLoader();
    $.ajax({
        type:"GET",
        data:{},
        url:"/user-development/"+user_id,
        success:function(response)

        {
            hideAjxLoader();
            if(!checkUserAuthenticationAjx(response))return false;
            $("#contact_content").html(response)
            }
        })
}
function 
userRelationship(){
    showAjxLoader();
    $.ajax({
        type:"GET",
        data:{},
        url:"/user-relationships/"+user_id,
        success:function(
            response){
            hideAjxLoader();
            if(!checkUserAuthenticationAjx(response))return false;
            $("#contact_content").html(response)
            }
        })
}
function userMembership(){
    showAjxLoader();
    $.ajax({
        type:"GET",
        data:{},
        url:"/user-memberships/"+user_id,
        success:function(
            response){
            hideAjxLoader();
            if(!checkUserAuthenticationAjx(response))return false;
            $("#contact_content").html(response)
            }
        })
}
function userVisitation(){
    showAjxLoader();
    $.ajax({
        type:"GET",
        data:{},
        url:"/user-visitation/"+user_id,
        success:function(
            response){
            hideAjxLoader();
            if(!checkUserAuthenticationAjx(response))return false;
            $("#contact_content").html(response)
            }
        })
}
function userChangelog(){
    showAjxLoader();
    $.ajax({
        type:"POST",
        url:"/user-change-log/"+moduleName+"/"+user_id,
        success:
        function(response){
            hideAjxLoader();
            if(!checkUserAuthenticationAjx(response))return false;
            $("#contact_content").html(
                response)
            }
        })
}
function editUserRelation(relationUserId){
    showAjxLoader();
    $.ajax({
        type:"GET",
        data:{},
        url:
        "/edit-user-relationships/"+relationUserId,
        success:function(response){
            hideAjxLoader();
            if(!checkUserAuthenticationAjx(
                response))return false;
            $("#contact_content").html(response)
            }
        })
}
function viewUserRelation(relationUserId){
    showAjxLoader()
    ;
    $.ajax({
        type:"GET",
        data:{},
        url:"/user-relationships-information/"+relationUserId,
        success:function(response){
            hideAjxLoader();
            if(!checkUserAuthenticationAjx(response))return false;
            $("#contact_content").html(response)
            }
        })
}
function 
userAddRelation(){
    showAjxLoader();
    $.ajax({
        type:"GET",
        data:{},
        url:"/create-user-relationships/"+user_id,
        success:function(
            response){
            hideAjxLoader();
            if(!checkUserAuthenticationAjx(response))return false;
            $("#contact_content").html(response)
            }
        })
}
$(function(){
	if(mode=="view" && source !="" && source == "soft_credit"){
		userDevelopment();
		$("#tab-8").addClass("active")
	}
    if(mode=="view" && source ==""){
        viewContact();
        $("#tab-1").addClass("active")
        }
        if(mode=="edit"){
        editContact();
        $("#tab-2").
        addClass("active")
        }
        if(mode=="relation"){
        userRelationship();
        $("#tab-12").addClass("active")
        }
        $("a[id^='tab-']").click(
        function(){
            $("a[id^='tab-']").removeClass("active");
            $(this).addClass("active");
            $("[class^='div-tab-']").css("display",
                "none");
            $(".div-"+$(this).attr("id")).css("display","block")
            })
    });
function getContactCases(){
    $.ajax({
        type:"POST",
        data:{
            contact_id:user_id
        },
        url:"/contact-cases",
        success:function(response){
            if(!checkUserAuthenticationAjx(response))
                return false;
            $("#contact_content").html(response)
            }
        })
}
function addToArchieve(case_contact_id){
    $(".add_to_archive").
    colorbox({
        width:"700px",
        height:"170px",
        inline:true
    });
    $("#no_delete_archive").click(function(){
        $.colorbox.close();
        $(
            "#yes_delete_archive").unbind("click");
        return false
        });
    $("#yes_delete_archive").click(function(){
        $("#yes_delete_archive")
        .unbind("click");
        $.ajax({
            type:"POST",
            dataType:"json",
            data:{
                case_contact_id:case_contact_id,
                is_archived:1
            },
            url:
            "/add-case-to-archive",
            success:function(responseData){
                if(!checkUserAuthenticationAjx(responseData))return false;
                var 
                jsonObj=responseData;
                if(jsonObj.status=="success")window.location.href="/cases";else $("#error_message").html(jsonObj.
                    message)
                }
                })
    })
}
function getContactActivityDetail(){
    showAjxLoader();
    $.ajax({
        type:"POST",
        data:{
            userid:
            encrypted_contact_name_id,
            sourceid:encrypted_source_type_id,
            sourceactivityid:encrypted_contact_name_id
        },
        url:
        "/contact-activities",
        success:function(response){
            hideAjxLoader();
            if(!checkUserAuthenticationAjx(response))return false;
            $
            ("#contact_content").html(response)
            }
        })
}
function getContactFamilyHistoryDetail(encrypted_contact_name_id){
    showAjxLoader()
    ;
    $.ajax({
        type:"POST",
        data:{
            userid:encrypted_contact_name_id
        },
        url:"/contact-family-history",
        success:function(response){
            hideAjxLoader();
            if(!checkUserAuthenticationAjx(response))return false;
            $("#contact_content").html(response)
            }
        })
}
function 
getContactLeads(encrypted_contact_name_id){
    $.ajax({
        type:"GET",
        data:{},
        url:"/contact-leads/"+encrypted_contact_name_id,
        success:function(response){
            hideAjxLoader();
            if(!checkUserAuthenticationAjx(response))return false;
            $("#contact_content").
            html(response)
            }
        })
}
function getContactPledge(encrypted_contact_name_id){
    $.ajax({
        type:"GET",
        data:{},
        url:
        "/contact-pledges/"+encrypted_contact_name_id,
        success:function(response){
            if(!checkUserAuthenticationAjx(response))
                return false;
            $("#contact_content").html(response)
            }
        })
}
function viewContactNewWindow(){
    window.open("/view-contact/"+
        encrypted_contact_name_id+"/"+encryptView,"","width=1500, height=800,scrollbars=1")
    }
    function getUserBonus(
    encrypted_contact_name_id){
    showAjxLoader();
    $.ajax({
        type:"POST",
        data:{
            userid:encrypted_contact_name_id
        },
        url:
        "/contact-bonus",
        success:function(response){
            hideAjxLoader();
            if(!checkUserAuthenticationAjx(response))return false;
            $(
                "#contact_content").html(response)
            }
        })
}