function authenticateUserBeforeAjaxFlagOfFaces(){
    $.ajax({
        type:"POST",
        url:"/authenticate-user",
        dataType:"html",
        success:
        function(responseData){
            var jsonObj=jQuery.parseJSON(responseData);
            if(jsonObj.status=="success"){
                window.location.href=
                "/login/"+refererUrl;
                return false
                }
            else if(jsonObj.status=="redirect_to_crm"){
                window.location.href=
                "/crm/";
                return false
                }
            }
    })
}

function showAjxLoaderFof(){
    var scrollheight=$(document).height(),scrollwidth=$(document).width(),windowheight=$(window).height(),windowwidth=$(window).width();
    $("#ajx_loader_fof").css({
            height:scrollheight,
            width:scrollwidth,
            display:"block",
            zIndex:"99999999"
    });
    $("#ajx_loader_image_fof").css({
        top:windowheight/2,
        left:windowwidth/2,
        position:"absolute"
    });
 }
	
function hideAjxLoaderFof(){ $("#ajx_loader_fof").remove(); }

showAjxLoaderFof();
var flagFlagOfFacesData=true;
var carousel = "";
 
$(document).ready(function(){
 
 carousel = $("#carousel").elastislide({onAfterSlide: function(){
            hideAjxLoaderFof();
            if($("ul#carousel img").size()<208) { $("span.elastislide-next").hide(); }
            else {  $("span.elastislide-next").click(); }
     }
 });
 
$("#search_name").focus();
var flag_search_name_empty=true;

 $('form#search_fof').each(function() { 
     $(this).find('input').keypress(function(e) { 
         if(e.which == 13) { 
             showAjxLoader();
             $('#submitsearch').click(); 
             return false; 
         } 
     }); 
 });
        
$("#search_name").live("input",function(){
    if($.trim($(this).val())==""){
        if(flag_search_name_empty==true){
            $(
                "#fofNoRecordFound").css({
                display:"none"
            });
            $("#fofNoRecordFound").html("");
            flagFlagOfFacesData=true;
            showAjxLoader();
            getFlagOfFacesContent(true,true);
            flag_search_name_empty=false
            }
            return false
        }else flag_search_name_empty=true
        });
$("#search_name").keyup(function(){
    if($.trim($(this).val())==""){
        if(flag_search_name_empty==true){
            $("#fofNoRecordFound").
            css({
                display:"none"
            });
            $("#fofNoRecordFound").html("");
            flagFlagOfFacesData=true;
            flag_record_request=true;
            getFlagOfFacesContent(true,true);
            flag_search_name_empty=false
            }
            return false
        }else flag_search_name_empty=true
        });
$("#submitsearch").click(function(){
    showAjxLoader();
    $('label[for="search_name"]').remove();
    if($.trim($("#search_name").val
        ())!=""){
        $('label[for="search_name"]').remove();
        flagFlagOfFacesData=true;
        getFlagOfFacesContent(true,true);
        hideAjxLoader();
        return false
        }else{
        $('label[for="search_name"]').remove();
        $("#search_name").after(
            '<label class="error" for="search_name" generated="true">'+FOF_HEADING_SEARCH_ERROR_SEARCH_NAME_COULD_NOT_EMPTY+
            "</label>");
        $("#fofNoRecordFound").css({
            display:"none"
        });
        $("#fofNoRecordFound").html("")
        }
        hideAjxLoader()
    });
    $("img").bind("contextmenu",function(e){ return false; });
  });
var flag_record_request=true;
function getFlagOfFacesContent(flagHtmlStatus,flagManage){
    $("#fof_search_msg").css({display:"none"});
    $("#fof_search_msg").html("");
    $('label[for="save_search_as"]').remove();
    if(true){
        var start_index_val="0";
        if(flagHtmlStatus==false)start_index_val=$.trim($("#startIndex").val());
        if(flag_record_request==true){ 
            flag_record_request=false;
            $.ajax({
                type:"POST",
                data:{
                    search_name:$.trim($("#search_name").val(
                        )),
                    start_index:start_index_val
                },
                async: false,
                url:"/get-fof-data",
                success:function(response){
                    try{
                        var jsonObj=jQuery.parseJSON(response
                            ),countRecords=jsonObj.data.length,divide13=countRecords/13,divide13Round=Math.round(divide13),divide13RoundLatitudinal=
                        divide13Round/3,divide13RoundLatitudinalRound=7,sclass=$("#carousel li.li2:last").attr("class"),sclass1="",c1=0,c2=0,ul=
                        1;
                        if($("ul#carousel li.li1 > ul.ul1").length!=""&&$("ul#carousel li.li1 > ul.ul1").length!=undefined&&$(
                            "ul#carousel li.li1 > ul.ul1").length>1)ul=$("ul#carousel li.li1 > ul.ul1").length;
                        if($(
                            "ul#carousel > li.li1:last > ul.ul1 > li.li2").length!=undefined&&$("ul#carousel > li.li1:last > ul.ul1 > li.li2").
                            length!="")c1=$("ul#carousel > li.li1:last > ul.ul1 > li.li2").length;
                        if(jsonObj.data.length>0){
                            if($.trim($(
                                "#search_name").val())!=""){
                                $("#fofNoRecordFound").css({
                                    display:"block"
                                });
                                $("#fofNoRecordFound").html('Your search "'+$.
                                    trim($("#search_name").val())+'" Matches '+jsonObj.countFofRes+" Results")
                                }else{
                                $("#fofNoRecordFound").css({
                                    display:
                                    "none"
                                });
                                $("#fofNoRecordFound").html("")
                                }
                                $("#startIndex").val(parseInt($("#startIndex").val())+parseInt(jsonObj.data.
                                length));
                            var htmlData="";
                            if(c1>12){
                                c1=0;
                                htmlData='<li class="li1"><ul class="ul1">'
                                }else{
                                htmlData=
                                '<li class="li1"><ul class="ul1">'+$("ul#carousel > li.li1:last > ul.ul1").html();
                                $("ul#carousel li.li1:last").remove()
                                }
                            if(flagHtmlStatus==true){
                                htmlData='<li class="li1"><ul class="ul1">';
                                ul=1;
                                c1=0;
                                $("#startIndex").val(parseInt(jsonObj.
                                    data.length)-parseInt(1))
                                }
                                for(var i=0;i<jsonObj.data.length;i++){
                                if(ul<divide13RoundLatitudinalRound&&c1<6)sclass="blue"
                                ;
                                else if(c1%2==0)sclass="red";else sclass="gray";
                                if(flagHtmlStatus==false)if(c1%2==0)sclass="red";else sclass="gray";
                                if(
                                    jsonObj.data[i].search_flag=="1")sclass="white";
                                if(c1==0)sclass1="firsttool-tip";
                                htmlData+='<li title="'+jsonObj.data[i]
                                .people_photo+'" onclick="javascript:focusOnImage('+"'fofImg"+jsonObj.data[i].fof_id+"'"+
                                ');"  style="cursor: pointer;" class="li2 '+sclass+" "+sclass1+'">';
                                htmlData+='<span id="span_fofImg'+jsonObj.data[i].
                                fof_id+'"></span>';
                                htmlData+='<a id="fofImg'+jsonObj.data[i].fof_id+'" name="fofImg'+jsonObj.data[i].fof_id+'" value="'+
                                jsonObj.data[i].fof_id+'"  rel="lightbox" >';
                                htmlData+='<img src="'+jsonObj.fofDirThumb+jsonObj.data[i].fof_image+'" />'
                                ;
                                htmlData+="</a>";
                                htmlData+="</li>";
                                c1=c1+1;
                                if(c1>12){
                                    c1=0;
                                    htmlData+='</ul></li><li class="li1"><ul class="ul1">';
                                    ul=ul+
                                    1
                                    }
                                }
                            if(flagHtmlStatus==true){
                            $("#flagOfFacesData").html('<ul id="carousel"  class="elastislide-list">');
                            $("#carousel").
                            html(htmlData);
                            carousel=$("#carousel").elastislide({
                                onAfterSlide:function(){
                                    chkRightArrow();
                                    if($("ul#carousel img").size()<208)$("span.elastislide-next").hide();
                                    if($("ul#carousel > li.li1:last > ul.ul1 > li.li2").length!=undefined&&$(
                                        "ul#carousel > li.li1:last > ul.ul1 > li.li2").length=="0")$("ul#carousel > li.li1:last").remove();
                                    return false
                                    }
                                })
                        }else{
                        $("#carousel").append(htmlData);
                        carousel.add()
                        }
                        jQuery(function($){
                        $("a[rel^='lightbox']").slimbox({})
                        });
                    $("img").bind(
                        "contextmenu",function(e){
                            return false
                            });
                    $(".li2").poshytip({
                        className:"tip-yellowsimple",
                        showOn:"hover",
                        alignTo:
                        "target",
                        alignX:"inner-left",
                        offsetX:0,
                        offsetY:5,
                        showTimeout:100
                    });
                    if($("ul#carousel > li.li1:last > ul.ul1 > li.li2").
                        length!=undefined&&$("ul#carousel > li.li1:last > ul.ul1 > li.li2").length=="0")$("ul#carousel > li.li1:last").remove()
                        }
                else {
                    flagFlagOfFacesData=true;
                    /*if(flagManage == true) { $("span.elastislide-next").css({"display":"block"}); }
                    else { $("span.elastislide-next").css({"display":"block"}); }*/
                }
                }catch(err){
                flagFlagOfFacesData=false
                }
            },
            complete: function() { flag_record_request=true;  hideAjxLoader(); }
    });

}
}
}
function chkRightArrow(){
    if($("ul#carousel > li.li1:last > ul.ul1 > li.li2").length<13)
        return false
        }
        function focusOnImage(imageId){
    $("span").removeClass("border-yellow");
    $("#span_"+imageId).addClass("border-yellow");
    $.colorbox({
        href:"/show-fof-image/"+$('a[id="'+imageId+'"]').attr("value"),
        overlayClose:false,
        width:"600px",
        escKey:false,
        onLoad:function(){
            $("#ajx_loader").css({
                "z-index":"99999999"
            });
            showAjxLoader()
            },
        onComplete:function(){
            $(
                "#ajx_loader").css({
                "z-index":""
            });
            hideAjxLoader()
            }
        });
$("#"+imageId).focus()
}
$(document).ready(function(){
  /* $("span.elastislide-prev").live("click",function(){
        showAjxLoader();
        getFlagOfFacesContent(false,false);
        hideAjxLoader();
        return false;
   });
  */ 
  $("span.elastislide-next").live("click",function(){
        showAjxLoader();
        getFlagOfFacesContent(false,true);
      //  hideAjxLoader();
        return false;
   });
});
    
$(function(){
    $(".li2").poshytip({
        className:"tip-yellowsimple",
        showOn:"hover",
        alignTo:"target",
        alignX:
        "inner-left",
        offsetX:0,
        offsetY:5,
        showTimeout:100
    })
    });
$(document).ready(function(){
    $("[placeholder]").focus(function(){
        var input=$(this);
        if(input.val()==input.attr("placeholder")){
            input.val("");
            input.removeClass("placeholder")
            }
        }).blur(
    function(){
        var input=$(this);
        if(input.val()==""||input.val()==input.attr("placeholder")){
            input.addClass("placeholder");
            input.val(input.attr("placeholder"))
            }
        }).blur().parents("form").submit(function(){
    $(this).find("[placeholder]").each(
        function(){
            var input=$(this);
            if(input.val()==input.attr("placeholder"))input.val("")
                })
    });
$.validator.addMethod(
    "AlphaNumericSpace",function(alpha_numeric_text){
        var alpha_number_regex=/^([a-zA-Z0-9\s]+)$/;
        if(!alpha_number_regex.test
            (alpha_numeric_text)&&alpha_numeric_text!="")return false;else return true
            },"");
$.validator.addMethod("SearchName",
    function(){
        if($.trim($("#search_name").val())==""){
            $("#search_name").focus();
            return false
            }else return true
            },"");
$(
    "#savesearch").click(function(){
    authenticateUserBeforeAjax();
    $('label[for="save_search_as"]').remove();
    if($.trim($(
        "#save_search_as").val())=="Save this search")$("#save_search_as").val("");
    $("#save_fof").validate({
        submitHandler:
        function(){
            authenticateUserBeforeAjax();
            showAjxLoader();
            $.ajax({
                type:"POST",
                data:{
                    search_name:$.trim($("#save_search_as"
                        ).val()),
                    searchString:$("#search_fof").serialize()
                    },
                url:"/save-fof-search",
                success:function(response){
                    var jsonObj=jQuery
                    .parseJSON(response);
                    if(jsonObj.status=="exceed"){
                        $.colorbox({
                            width:"600px",
                            height:"320px",
                            inline:true,
                            href:".exceed"
                        });
                        $(".exceed").show();
                        $("#cboxClose").on("click",function(){
                            $(".exceed").hide()
                            });
                        $("#search_msg").html("")
                        }else if(
                        jsonObj.status=="success"){
                        document.getElementById("save_fof").reset();
                        $("#search_msg").removeClass("error");
                        document.
                        getElementById("search_fof").reset();
                        getFofSearchData(jsonObj.dataresult,jsonObj.countresult,jsonObj.message)
                        }else $(
                        "#search_msg").html("");
                    hideAjxLoader()
                    }
                })
        },
    rules:{
        save_search_as:{
            required:true,
            AlphaNumericSpace:true,
            SearchName:true
        }
    },
    messages:{
        save_search_as:{
            required:FOF_HEADING_SEARCH_ERROR_EMPTY_NAME,
            AlphaNumericSpace:
            FOF_HEADING_SEARCH_ERROR_ALHANUMERIC_SPACE,
            SearchName:FOF_HEADING_SEARCH_ERROR_SEARCH_NAME_COULD_NOT_EMPTY
        }
    }
    })
})
});
function viewSavedSearch(search_id){
    showAjxLoader();
    $.ajax({
        type:"POST",
        data:{
            fof_search_id:search_id
        },
        url:
        "/get-fof-user-saved-search-select",
        success:function(response){
            hideAjxLoader();
            var searchParamArray=response,obj=jQuery.
            parseJSON(searchParamArray);
            document.getElementById("search_fof").reset();
            $("#search_name").val(obj.search_name);
            $(
                "#submitsearch").click()
            }
        })
}
function deleteSaveSearch(search_id){
    $("#fofSearchDelId").val($.trim(search_id));
    var 
    flag_delete_fof=true;
    $(".deletesearch").colorbox({
        width:"700px",
        height:"300px",
        inline:true,
        onOpen:function(){
            flag_delete_fof=false
            },
        onClosed:function(){
            $("#fofSearchDelId").val("")
            }
        });
$("#no").click(function(){
    $(this).colorbox.close();
    $("#yes").unbind("click");
    $("#fofSearchDelId").val("");
    return false
    });
$("#yes").click(function(){
    if(search_id!=""&&$.trim(search_id)==$.
        trim($("#fofSearchDelId").val())&&flag_delete_fof==false)$.ajax({
        type:"POST",
        data:{
            fof_search_id:$.trim($(
                "#fofSearchDelId").val())
            },
        url:"/delete-fof-search",
        success:function(response){
            var jsonObj=jQuery.parseJSON(response);
            if(jsonObj.status==="success"){
                getFofSearchData(jsonObj.dataresult,jsonObj.countresult,jsonObj.message);
                $.colorbox.close
                ()
                }
            }
    })
})
}
function getFofSearchData(Dataresult,Countresult,MessageResult){
    if($.trim(MessageResult)!=""){
        $(
            "#fof_search_msg").css({
            display:"block"
        });
        $("#fof_search_msg").html($.trim(MessageResult))
        }
        $("#fofNoRecordFound").css({
        display:"none"
    });
    $("#fofNoRecordFound").html("");
    $("#fof_search_results").html(Dataresult);
    $(".scroll-pane").jScrollPane
    ();
    $(".common-savesearch .dropdown").hide();
    if($.trim(Dataresult)=="")$("#fof_search_results").css({
        display:"none"
    });
    var
    countSavedSearch=parseInt(Countresult);
    if(countSavedSearch>0)$("#fof_search_results").css({
        display:"block"
    });else $(
        "#fof_search_results").css({
        display:"none"
    });
    $("#count_saved_searches_input").val(countSavedSearch)
    }