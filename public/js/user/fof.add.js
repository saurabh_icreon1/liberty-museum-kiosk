function authenticateUserBeforeAjaxFlagOfFaces(){}
function dataShowPost(){
    $("#previewFOF1").contents().find(
        "#showTagsId").css({
        display:"block"
    });
    $("#previewFOF1").contents().find("#addTagsId").css({
        display:"none"
    });
    $(
        "#previewFOF1").contents().find("#mapper").hide();
    var arrNames=[],arrNamesSerialize=[],htmlNewBox="",countS=0;
    $(
        "#previewFOF1").contents().find("#planetmap > div").each(function(){
        arrNames.push($.trim($(this).find(
            "div.tagged_title > span.tagged_title_text").html()));
        htmlNewBox+='<div class="'+$.trim($(this).attr("class").replace(
            "tagged",""))+' tages" >'+$.trim($(this).find("div.tagged_title > span.tagged_title_text").html())+
        '<a class="removeTag" onclick="javascript:removePeoplePhotoTag(\''+$.trim($(this).attr("class").replace("tagged",""))+
        '\');" delid="'+$.trim($(this).attr("class").replace("tagged",""))+'" >X</a>'+"</div>";
        arrNamesSerialize[countS]={
            name:$
            .trim($(this).find("div.tagged_title > span.tagged_title_text").html()),
            width:$(this).css("width"),
            height:$(this).css(
                "height"),
            top:$(this).css("top"),
            left:$(this).css("left")
            };
            
        countS=parseInt(countS)+parseInt(1)
        });
    if(arrNamesSerialize.
        length>0){
        $("#people_photo").val(JSON.stringify(arrNamesSerialize));
        $("#people_photo_edit").val(JSON.stringify(
            arrNamesSerialize))
        }else{
        $("#people_photo").val("");
        $("#people_photo_edit").val("")
        }
        $("#peoplePhotoBox").html(htmlNewBox
        );
    $("#span_fof_people_in_photo").html(htmlNewBox)
    }
    function removePeoplePhotoTag(delid){
    $("#previewFOF1").contents().find
    ("."+delid).remove();
    dataShowPost()
    }
    $(function(){
    $(document).delegate("#photo_caption","input",function(){
        $(
            'label[for="photo_caption"]').remove();
        $('label[for="photo_caption_edit"]').remove();
        $("#photo_caption_edit").val($(this
            ).val());
        $("#span_fof_caption").html($(this).val())
        });
    $(document).delegate("#photoCaption","mouseover",function(){
        if($(
            "#photoCaption > div[class='edit_photo_details']").is(":visible")==false){
            var main_obj=this;
            $(main_obj).removeClass(
                "content1");
            $(main_obj).addClass("edit-section");
            $("#fof_caption_edit").css({
                display:"block"
            })
            }
        });
$(
    "a[id='fof_caption_edit']").click(function(){
    $("#fof_caption_edit").css({
        display:"none"
    });
    $(
        "#photoCaption > p[class='view_photo_details']").css({
        display:"none"
    });
    $(
        "#photoCaption > div[class='edit_photo_details']").css({
        display:"block"
    })
    });
$("input[id='fof_caption_save']").click(
    function(){
        $('label[for="photo_caption_edit"]').remove();
        $("#photo_caption").val($("#photo_caption_edit").val());
        $(
            "#span_fof_caption").html($("#photo_caption_edit").val());
        $("#photoCaption").addClass("content1");
        $("#photoCaption").
        removeClass("edit-section");
        $("#fof_caption_edit").css({
            display:"none"
        });
        $(
            "#photoCaption > p[class='view_photo_details']").css({
            display:"block"
        });
        $(
            "#photoCaption > div[class='edit_photo_details']").css({
            display:"none"
        })
        });
$("input[id='fof_caption_cancel']").click(
    function(){
        $('label[for="photo_caption_edit"]').remove();
        $("#photo_caption_edit").val($("#photo_caption").val());
        $(
            "#span_fof_caption").html($("#photo_caption").val());
        $("#photoCaption").addClass("content1");
        $("#photoCaption").
        removeClass("edit-section");
        $("#fof_caption_edit").css({
            display:"none"
        });
        $(
            "#photoCaption > p[class='view_photo_details']").css({
            display:"block"
        });
        $(
            "#photoCaption > div[class='edit_photo_details']").css({
            display:"none"
        })
        });
$(document).delegate("#photoCaption",
    "mouseout",function(){
        if($("#photoCaption > div[class='edit_photo_details']").is(":visible")==false){
            $(this).addClass(
                "content1");
            $(this).removeClass("edit-section");
            $(this).find("a[id='fof_caption_edit']").css({
                display:"none"
            });
            $(this).
            find("p[class='view_photo_details']").css({
                display:"block"
            });
            $(this).find("div[class='edit_photo_details']").css({
                display:"none"
            })
            }
        });
$("a[id='fof_peoplephoto_edit']").click(function(){
    $("#step4full").css({
        display:"none"
    });
    $("#step3full").css({
        display:"block"
    })
    });
$(document).delegate("#peoplePhoto","mouseover",function(){
    $(
        "a[id='fof_peoplephoto_edit']").css({
        display:"block"
    });
    $(this).addClass("edit-section")
    });
$(document).delegate(
    "#peoplePhoto","mouseout",function(){
        $("a[id='fof_peoplephoto_edit']").css({
            display:"none"
        });
        $(this).removeClass(
            "edit-section")
        });
$(document).delegate("#donated_by","input",function(){
    $('label[for="donated_by"]').remove();
    $(
        'label[for="donated_by_edit"]').remove();
    $("#donated_by_edit").val($(this).val());
    $("#p_donated_by").html($(this).val())
});
$(document).delegate("#donatedBy","mouseover",function(){
    if($("#donatedBy > div[class='edit_photo_details']").is(
        ":visible")==false){
        var main_obj=this;
        $(main_obj).removeClass("content1");
        $(main_obj).addClass("edit-section");
        $(
            "#fof_donated_by_edit").css({
            display:"block"
        })
        }
    });
$("a[id='fof_donated_by_edit']").click(function(){
    $(
        "#fof_donated_by_edit").css({
        display:"none"
    });
    $("#donatedBy > p[class='view_photo_details']").css({
        display:"none"
    });
    $(
        "#donatedBy > div[class='edit_photo_details']").css({
        display:"block"
    })
    });
$("input[id='fof_donated_by_save']").click(
    function(){
        $('label[for="donated_by_edit"]').remove();
        $("#donated_by").val($("#donated_by_edit").val());
        $(
            "#p_donated_by").html($("#donated_by_edit").val());
        $("#donatedBy").addClass("content1");
        $("#donatedBy").removeClass(
            "edit-section");
        $("#fof_donated_by_edit").css({
            display:"none"
        });
        $("#donatedBy > p[class='view_photo_details']").css({
            display:"block"
        });
        $("#donatedBy > div[class='edit_photo_details']").css({
            display:"none"
        })
        });
$(
    "input[id='fof_donated_by_cancel']").click(function(){
    $('label[for="donated_by_edit"]').remove();
    $("#donated_by_edit").
    val($("#donated_by").val());
    $("#p_donated_by").html($("#donated_by").val());
    $("#donatedBy").addClass("content1");
    $(
        "#donatedBy").removeClass("edit-section");
    $("#fof_donated_by_edit").css({
        display:"none"
    });
    $(
        "#donatedBy > p[class='view_photo_details']").css({
        display:"block"
    });
    $("#donatedBy > div[class='edit_photo_details']").
    css({
        display:"none"
    })
    });
$(document).delegate("#donatedBy","mouseout",function(){
    if($(
        "#donatedBy > div[class='edit_photo_details']").is(":visible")==false){
        $(this).addClass("content1");
        $(this).removeClass(
            "edit-section");
        $(this).find("a[id='fof_donated_by_edit']").css({
            display:"none"
        });
        $(this).find(
            "p[class='view_photo_details']").css({
            display:"block"
        });
        $(this).find("div[class='edit_photo_details']").css({
            display:
            "none"
        })
        }
    });
if(navigator.userAgent.indexOf("MSIE")>0)$("#upload_photo").mousedown(function(){
    $(this).trigger("click")
    });
$("#upload_photo_button").click(function(){
    $("#upload_photo").click()
    });
$("#step3fullBack").click(function(){
    $("#step3full").css({
        display:"none"
    });
    $("#step2Full").css({
        display:"block"
    })
    });
$("#step4fullBack").click(function(){
    $("#step4full").css({
        display:"none"
    });
    $("#step3full").css({
        display:"block"
    })
    });
$("#upload_photo").fileupload({
    url:
    "/upload-user-fof-image",
    dataType:"json",
    done:function(e,data){
        data=data.result;
        if(data.status=="success"){
                $('label[for="upload_photo"]').remove();
                $("#upload_photo").removeClass("error");
                $("#filemessage").addClass("success-msg");
                $("#filemessage").removeClass("error-msg");
                $("#image_name").val(data.image_name);
                $("#fof_image").val(data.filename);  
                $("#UploadUserFofImageIframe").attr("src","/upload-user-fof-image-iframe/"+data.full_path_pixenate+"/"+data.encrypted_filename);
                $("#previewFOF1").attr("src","/upload-user-fof-image-iframe-preview/"+data.preview_path+"/2");
                $("#step3FOFTag").attr("src",data.temp_upload_medium_dir+data.filename);
                $("#stepSmallFOFTag").attr("src","/small-image-view-fof/"+data.temp_upload_small_image_path_dir+"/2/"+data.encryptfilename);
                $("#filemessage").html(data.message);
                $("#step1Full").css({display:"none"});
                $("#step2Full").css({display:"block"});
                //step 2 google code
                //ga('require', 'displayfeatures');
                //ga('send', 'pageview',{'page': '/add-fof-image2','title': 'Add FOF Image2'});
                $("#peoplePhotoBox").empty();
                $("#span_fof_people_in_photo").empty();
                $("#people_photo").val("");
                $("#people_photo_edit").val("");
         } else {
                $('label[for="upload_photo"]').remove();
                $("#filemessage").removeClass("success-msg");
                $("#filemessage").addClass("error");
                $("#image_name").val("");
                $("#fof_image").val("");
                $("#UploadUserFofImageIframe").attr("src","/upload-user-fof-image-iframe/none/none");
                $("#previewFOF1").attr("src","/upload-user-fof-image-iframe-preview/none/3");
                $("#stepSmallFOFTag").attr("src","/small-image-view-fof/none/3/none");
                $("#filemessage").html('<label class="error">'+data.message+"</label>");
                $("#peoplePhotoBox").empty();
                $("#span_fof_people_in_photo").empty();
                $("#people_photo").val("");
                $("#people_photo_edit").val("");
           }
        },
start:function(e){
    showAjxLoader()
    },
stop:function(e){
    hideAjxLoader()
    }
}).prop("disabled",!$.support.fileInput).
parent().addClass($.support.fileInput?undefined:"disabled");
$.validator.addMethod("AlphaNumericComma",function(
    alpha_numeric_text){
    var alpha_number_regex=/^([a-zA-Z0-9,]+)$/;
    if(!alpha_number_regex.test(alpha_numeric_text)&&
        alpha_numeric_text!="")return false;else return true
        },"");
$.validator.addMethod("AlphaNumericSpace",function(
    alpha_numeric_text){
    var alpha_number_regex=/^([a-zA-Z0-9\s]+)$/;
    if(!alpha_number_regex.test(alpha_numeric_text)&&
        alpha_numeric_text!="")return false;else return true
        },"");
$.validator.addMethod("AlphaNumericSpaceComma",function(
    alpha_numeric_text){
    var alpha_number_regex=/^([a-zA-Z0-9,\s']+)$/;
    if(!alpha_number_regex.test(alpha_numeric_text)&&
        alpha_numeric_text!="")return false;else return true
        },"");
$("#add_fof1").validate({
    submitHandler:function(){
        authenticateUserBeforeAjaxFlagOfFaces();
        showAjxLoader();
        $("#step1Full").css({
            display:"none"
        });
        $("#step2Full").css({
            display:"block"
        });
        hideAjxLoader()
        },
    rules:{
        upload_photo:{
            required:{
                depends:function(element){
                    if($.trim($("#fof_image").
                        val())=="")return true;else return false
                        }
                    }
        }
},
messages:{
    upload_photo:{
        required:FOF_IMAGE_UPLOAD_IMAGE_EMPTY
    }
},
errorPlacement:function(error,element){
    var name=element.attr("name");
    if(name==="upload_photo"){
        error.insertAfter(
            "span[id='filemessage']");
        $("#filemessage").html("")
        }else error.insertAfter(element)
        }
    });
$.validator.setDefaults({
    ignore:
    []
});
$("#add_fof3").validate({
    submitHandler:function(){
        authenticateUserBeforeAjaxFlagOfFaces();
        showAjxLoader();
        $("#step3full").css({
            display:"none"
        });
        $("#step4full").css({
            display:"block"
        });
        //add code for step4
        //ga('require', 'displayfeatures');
       // ga('send', 'pageview',{'page': '/add-fof-image4','title': 'Add FOF Image4'});
        hideAjxLoader()
        },
    rules:{
        donated_by:{
            required:
            true,
            AlphaNumericSpaceComma:true
        },
        people_photo:{
            required:true
        },
        photo_caption:{
            required:true,
            AlphaNumericSpaceComma:true
        }
        ,
        is_people_photo_visible:{
            required:true
        },
        is_photo_caption_visible:{
            required:true
        }
    },
messages:{
    donated_by:{
        required:
        FOF_DONATED_BY_REQUIRED,
        AlphaNumericSpaceComma:FOF_ERROR_ADD_TITLE_ALPHANUMERIC
    },
    people_photo:{
        required:
        FOF_PEOPLE_PHOTO_REQUIRED
    },
    photo_caption:{
        required:FOF_PEOPLE_CAPTION_REQUIRED,
        AlphaNumericSpaceComma:
        FOF_ERROR_PEOPLE_CAPTION_ALPHANUMERIC
    },
    is_people_photo_visible:{
        required:FOF_CHECK_IS_PEOPLE_PHOTO_VISIBLE
    },
    is_photo_caption_visible:{
        required:FOF_CHECK_IS_PHOTO_CAPTION_VISIBLE
    }
},
errorPlacement:function(error,element){
    var name=
    element.attr("name");
    if(name==="people_photo")error.insertAfter("div[id='peoplePhotoVisbilty']");
    else if(name===
        "photo_caption")error.insertAfter("div[id='photoCaptionVisbilty']");
    else if(name==="donated_by")error.insertAfter(
        "div[id='donatedByError']");else error.insertAfter(element)
        }
    });
$("#add_fof4").validate({
    submitHandler:function(){
        authenticateUserBeforeAjaxFlagOfFaces();
        showAjxLoader();
        $.ajax({
            type:"POST",
            data:$(
                "#add_fof0,#add_fof1,#add_fof2,#add_fof3,#add_fof4").serialize(),
            url:"/add-fof-image-process",
            success:function(response)

            {
                hideAjxLoader();
                var jsonObj=jQuery.parseJSON(response);
                if(jsonObj.status=="success"){
                    window.location.href="/cart";
                    return false
                    }else{
                    $.each(jsonObj.message,function(i,msg){
                        $('label[for="error_'+i+'"]').remove();
                        $("#"+i).after(
                            '<label class="error" for="error_'+i+'" style="display:block;">'+msg+"</label>")
                        });
                    return false
                    }
                }
        })
},
rules:{
    donated_by_edit:{
        required:true,
        AlphaNumericSpaceComma:true
    },
    people_photo_edit:{
        required:true
    },
    photo_caption_edit:{
        required:true,
        AlphaNumericSpaceComma:true
    },
    is_agree:{
        required:true
    }
},
messages:{
    donated_by_edit:{
        required:
        FOF_DONATED_BY_REQUIRED,
        AlphaNumericSpaceComma:FOF_ERROR_ADD_TITLE_ALPHANUMERIC
    },
    people_photo_edit:{
        required:
        FOF_PEOPLE_PHOTO_REQUIRED
    },
    photo_caption_edit:{
        required:FOF_PEOPLE_CAPTION_REQUIRED,
        AlphaNumericSpaceComma:
        FOF_ERROR_PEOPLE_CAPTION_ALPHANUMERIC
    },
    is_agree:{
        required:FOF_TERMS_AND_CONDITION
    }
},
errorPlacement:function(error,
    element){
    var name=element.attr("name");
    if(name==="is_agree")error.insertAfter(
        "span[id='fof_agree_terms_and_condition_message']");
    else if(name==="donated_by_edit")error.insertAfter(
        "div[id='donatedByVisbilty2']");
    else if(name==="people_photo_edit"){
        $("#step4full").css({
            display:"none"
        });
        $("#step3full"
            ).css({
            display:"block"
        })
        }else if(name==="photo_caption_edit")error.insertAfter("div[id='photoCaptionVisbilty2']");else 
        error.insertAfter(element)
        }
    });
$("#is_photo_uploaded").click(function(){
    if($("#is_photo_uploaded").prop("checked")==true)
         parent.location.href = "/fof-mail";
        //$("#is_photo_uploaded_hidden_href").trigger("click")
        });
$("#people_photo").bind("input",function(){
    if($.trim($(this).val(
        ))=="")$("#span_fof_people_in_photo").html("&#60;"+FOF_PEOPLE_IN_PHOTO+"&#62;");else $("#span_fof_people_in_photo").html
        ($.trim($(this).val()))
        });
$("#photo_caption").bind("input",function(){
    if($.trim($(this).val())=="")$("#span_fof_caption"
        ).html("&#60;"+FOF_CAPTION+"&#62;");else $("#span_fof_caption").html($.trim($(this).val()))
        });
$("#people_photo").change(
    function(){
        if($.trim($(this).val())=="")$("#span_fof_people_in_photo").html("&#60;"+FOF_PEOPLE_IN_PHOTO+"&#62;");else $(
            "#span_fof_people_in_photo").html($.trim($(this).val()))
            });
$("#photo_caption").change(function(){
    if($.trim($(this).val()
        )=="")$("#span_fof_caption").html("&#60;"+FOF_CAPTION+"&#62;");else $("#span_fof_caption").html($.trim($(this).val()))
        })
;
$("#people_photo").keyup(function(){
    if($.trim($(this).val())=="")$("#span_fof_people_in_photo").html("&#60;"+
        FOF_PEOPLE_IN_PHOTO+"&#62;");else $("#span_fof_people_in_photo").html($.trim($(this).val()))
        });
$("#photo_caption").keyup
(function(){
    if($.trim($(this).val())=="")$("#span_fof_caption").html("&#60;"+FOF_CAPTION+"&#62;");else $(
        "#span_fof_caption").html($.trim($(this).val()))
        })
});
function fofByMail(){
    $(".fofbymail").colorbox({
        width:"970px",
        height
        :"640px",
        inline:true,
        iframe:true
    })
    }
    $(function(){
    $('a[id="a_is_people_photo_visible"]').click(function(){});
    $(
        'a[id="a_is_photo_caption_visible"]').click(function(){})
    });
function setPhotoCaptionVisiblilty(pcId,pcVal){
    if(pcId!=
        undefined&&pcId!="")$("#"+pcId).val(pcVal);
    return false
    }
    if(typeof jQuery=="undefined")var iframeBody=document.
    getElementsByTagName("body")[0],jQuery=function(selector){
        return parent.jQuery(selector,iframeBody)
        },$=jQuery