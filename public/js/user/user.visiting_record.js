
$(function() {
 /*Visiting record */
$("#uservisitrecord").validate({
    submitHandler: function() {
        var dataStr = $("#uservisitrecord").serialize();
        showAjxLoader
        ();
        $.ajax({
            type: "POST", 
            url: "/visitation-record", 
            data: dataStr, 
            success: function(responseData) {
                hideAjxLoader();
                var jsonObj = jQuery.
                parseJSON(responseData);
                if (jsonObj.status == "success") { 
					$.colorbox.close(); 
                   parent.regisrationConfirm(jsonObj);
				   
                }else
                    $.each(jsonObj.message, function(i, msg) {
                        $
                        ('label[for="' + i + '"]').remove();
                        $("#" + i).after('<label for="' + i + '" class="error" style="display:block;">' + msg + "</label>"
                            )
                    })
            }
        })
}, 
rules: {
    visiting_with: {
        required: true,
		}, 
   
    country_id: {
        required: true,
    }, 
	ethnicity: {
        required: true,
    },
	nationality: {
        required: true,
    },
    agree: {
        required: true,
    } 
   
}, 
messages: {
    visiting_with: {
        required: SELECT_VISITING_WITH
    }, 
	country_id: {
        required: SELECT_BIRTH_COUNTRY
    }, 
	ethnicity: {
        required: "Please select ethnicity."
    },
	nationality: {
        required: "Please select nationality."
    },
    agree: {
        required: AGREE_TERMS
    } 
    
}
}) 


function regisrationConfirm_(obj){
   $.colorbox.close(); 
   //console.log(obj);
   /*var htmltext ="Name was assign to Kiosk <br/>'"+obj.contact+"' ";
   $('#loadingwindow').dialog('open').html(htmltext);*/
	$("#email_id").html('');

	$("#email_id").html(obj.email_id);
	$.colorbox({width:"730px",height:"920px",inline:true,href:".visitation_record_thank"});
    $(".visitation_record_thank").show();
    $("#cboxClose").on("click",function(){$(".visitation_record_thank").hide()});
	$("#close_thanks").click(function(){
        $('#close_thanks').unbind('click');
         parent.window.location.href="/logout";
    });
	$("#passenger_search").click(function(){
		parent.window.location.href="/kiosk-fee";
		  //addtocart(9);
         //$('#passenger_search').unbind('click');
		 //setTimeout(addtocart(9),1000);
         
    });

}


function addtocart(product_id){
    authenticateUserBeforeAjax();
    var status = false;

    
       var isWheelchair = 0;
        var isNextAvailableTime = 0;
       /* if($("#addMeTime").is(":checked")){
                isNextAvailableTime = 1;
        }
        if($("#wheelchair").is(":checked")){
                isWheelchair = 1;
            }*/
    $.ajax({
        type:"POST",
        url:"/add-to-cart-inventory-front",
        data:{
            product_id:product_id,
            num_quantity:1,
            is_wheelchair:isWheelchair,
            next_available_time:isNextAvailableTime,
        },
        beforeSend:function(){
            showAjxLoader()
        }
        ,
        success:function(responseData){
            hideAjxLoader();
            var jsonObj=jQuery.parseJSON(responseData);
            $("label.error").remove();
            if(jsonObj.status=="success"){
                // added - start
                getCartViewUpdtLS();
				return true;
            // added - end                
            }else{
					return true;
            }
        }
    })
}

function getCartViewUpdtLS() {
    try {
        showAjxLoader();
        $.ajax({
            type:"POST",
            url:"/cart-front-product",
            async:true,
            success:function(responseData){
                hideAjxLoader();
                parent.$("#cartlist").html(responseData);
            }
        });	
        $.ajax({
            type:"POST",
            url:"/cart-details",
            async:true,
            success:function(responseData){
                parent.$("#cart-details-div").html(responseData);
            }
        });
		parent.window.location.href="/cart";
    }
    catch(err) {}
}

})
 