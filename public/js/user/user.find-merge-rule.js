$(function() {
    $(".e1").select2({allowClear: true, placeholder: "Select"});

	$("#email_id1").autocomplete({
           afterAdd : true,
           selectFirst:true,
           autoFocus: true,

           source: function(request, response) {
               $.ajax({
                   url: "/get-contacts",
                   method: 'POST',
                   dataType: "json",
                   data: { name_email: $("#email_id1").val() },
                   success: function(jsonResult) {                       
                       var resultset = [];
                       $.each(jsonResult, function() {
                           var id = this.user_id;
                           var value = this.email_id;
						   if(value!=''){
							   resultset.push({
								   id:this.user_id,
								   value: this.email_id
							   });
						   }
                       });
                       response(resultset);
                   }
               });

           },
           open: function() {
               $('.ui-menu').width(350)
           },

           change: function(event, ui) {
               if(ui.item)
               {
                   $("#email_id1").val(ui.item.value);
                   $("#user_id1").val(ui.item.id);      
               }
               else{
                   $("#email_id1").val('');
                   $("#user_id1").val('');  
               }
           },
           focus: function(event, ui) {
               $("#user_id1").val(ui.item.id);
               return false;
           },
           select: function(event, ui) { 
               $("#email_id1").val(ui.item.value);
               $("#user_id1").val(ui.item.id);                
               return false;
           }           
       });

	   $("#email_id2").autocomplete({
           afterAdd : true,
           selectFirst:true,
           autoFocus: true,

           source: function(request, response) {
               $.ajax({
                   url: "/get-contacts",
                   method: 'POST',
                   dataType: "json",
                   data: { name_email: $("#email_id2").val() },
                   success: function(jsonResult) {                       
                       var resultset = [];
                       $.each(jsonResult, function() {
                           var id = this.user_id;
                           var value = this.email_id;
						   if(value!=''){
							   resultset.push({
								   id:this.user_id,
								   value: this.email_id
							   });
						   }
                           
                       });
                       response(resultset);
                   }
               });

           },
           open: function() {
               $('.ui-menu').width(350)
           },

           change: function(event, ui) {
               if(ui.item)
               {
                   $("#email_id2").val(ui.item.value);
                   $("#user_id2").val(ui.item.id);      
               }
               else{
                   $("#email_id2").val('');
                   $("#user_id2").val('');  
               }
           },
           focus: function(event, ui) {
               $("#user_id2").val(ui.item.id);
               return false;
           },
           select: function(event, ui) { 
               $("#email_id2").val(ui.item.value);
               $("#user_id2").val(ui.item.id);                
               return false;
           }           
       });

	   $("#contact_id1").autocomplete({
           afterAdd : true,
           selectFirst:true,
           autoFocus: true,

           source: function(request, response) {
               $.ajax({
                   url: "/get-contacts",
                   method: 'POST',
                   dataType: "json",
                   data: { contact_id: $("#contact_id1").val() },
                   success: function(jsonResult) {                       
                       var resultset = [];
                       $.each(jsonResult, function() {
                           var id = this.user_id;
                           var value = this.contact_id;
						   if(value!=''){
							   resultset.push({
								   id:this.user_id,
								   value1: this.contact_id,
								   value: this.full_name+" ( "+this.contact_id+" ) "
							   });
						   }
                       });
                       response(resultset);
                   }
               });

           },
           open: function() {
               $('.ui-menu').width(350)
           },

           change: function(event, ui) {
               if(ui.item)
               {
                   $("#contact_id1").val(ui.item.value1);
                   $("#user_contact_id1").val(ui.item.id);      
               }
               else{
                   $("#contact_id1").val('');
                   $("#user_contact_id1").val('');  
               }
           },
           focus: function(event, ui) {
               $("#user_contact_id1").val(ui.item.id);
               return false;
           },
           select: function(event, ui) { 
               $("#contact_id1").val(ui.item.value1);
               $("#user_contact_id1").val(ui.item.id);                
               return false;
           }           
       });

	   $("#contact_id2").autocomplete({
           afterAdd : true,
           selectFirst:true,
           autoFocus: true,

           source: function(request, response) {
               $.ajax({
                   url: "/get-contacts",
                   method: 'POST',
                   dataType: "json",
                   data: { contact_id: $("#contact_id2").val() },
                   success: function(jsonResult) {                       
                       var resultset = [];
                       $.each(jsonResult, function() {
                           var id = this.user_id;
                           var value = this.contact_id;
						   if(value!=''){
							   resultset.push({
								   id:this.user_id,
								   value1: this.contact_id,
								   value: this.full_name+" ( "+this.contact_id+" )"
							   });
						   }
                           
                       });
                       response(resultset);
                   }
               });

           },
           open: function() {
               $('.ui-menu').width(350)
           },

           change: function(event, ui) {
               if(ui.item)
               {
                   $("#contact_id2").val(ui.item.value1);
                   $("#user_contact_id2").val(ui.item.id);      
               }
               else{
                   $("#contact_id2").val('');
                   $("#user_contact_id2").val('');  
               }
           },
           focus: function(event, ui) {
               $("#user_contact_id2").val(ui.item.id);
               return false;
           },
           select: function(event, ui) { 
               $("#contact_id2").val(ui.item.value1);
               $("#user_contact_id2").val(ui.item.id);                
               return false;
           }           
       });

	   $("#yes_merge").click(function() {
        showAjxLoader();
        $("#yes_merge").unbind("click");
		$("#no_merge").unbind("click");
		
        $.ajax({type: "POST", data: {userId: $('#user_id1').val()
                        , duplicateUserId: $('#user_id2').val()}, url: "/merge-contact-record", success: function(response) {
                hideAjxLoader();
                if (!checkUserAuthenticationAjx(response))
                    return false;
                var jsonObj = jQuery.parseJSON(response);
                if (jsonObj.status == "success") {
                    $.colorbox.close();
                    $("#success_message").show();
                    $("#success_message").html(jsonObj.message);
                } else {
                    $("#success_message").hide();
                }
            }});
    });

	$("#no_merge").click(function() {
        $("#yes_merge").unbind("click");
        $("#no_merge").unbind("click");
		
        window.location.href = '/merge-rule';
    });
});
function userRule(rule, userRule) {
    showAjxLoader();
    var group = "";
    if (userRule == "name")
        group = $("#group_id_name").val();
    else if (userRule == "address")
        group = $("#group_id_address").val();
    else
    if (userRule == "last_name_address")
        group = $("#group_id_last_name_address").val();
    else if (userRule == "last_name_phone")
        group =
                $("#group_id_last_name_phone").val();
    else
        group = $("#group_id_phone").val();
    $("#loader").show();
    if (group)
        window.location.
                href = "/duplicate-contact/" + rule + "/" + group;
    else
        window.location.href = "/duplicate-contact/" + rule
}
function
        addDedupeException(userId, duplicateUserId, divId) {
    $.colorbox({width: "650px", href: "#add_dedupe_content", height: "200px",
        inline: true});
    $("#no_delete").click(function() {
        $.colorbox.close();
        $("#yes_delete").unbind("click");
        return false
    });
    $(
            "#yes_delete").click(function() {
        showAjxLoader();
        $("#yes_delete").unbind("click");
        $.ajax({type: "POST", data: {userId: userId
                        , duplicateUserId: duplicateUserId}, url: "/dedupe-exception-contact", success: function(response) {
                hideAjxLoader();
                if (!
                        checkUserAuthenticationAjx(response))
                    return false;
                var jsonObj = jQuery.parseJSON(response);
                if (jsonObj.status == "success") {
                    $
                            .colorbox.close();
                    $("#" + divId).hide();
                    $("#success_message").show();
                    $("#success_message").html(jsonObj.message);
                    $(
                            "#success_message_flash").hide();
                    var count = $("#count_contact").html();
                    count = parseInt(count) - parseInt(1);
                    $(
                            "#count_contact").html(count);
                    if (count == 0)
                        $("#heading").hide()
                } else {
                    $("#success_message").hide();
                    $(
                            "#success_message_flash").hide()
                }
            }})
    })
}
function mergeContact(userId, duplicateUserId) {
    if (group)
        window.location.href =
                "/duplicate-contact-info/" + userId + "/" + duplicateUserId + "/" + rule + "/" + group;
    else
        window.location.href =
                "/duplicate-contact-info/" + userId + "/" + duplicateUserId + "/" + rule
}

	

function userEmailMergeRule(){
	$('#rulegroup').submit();
}

function userContactIdMergeRule(){
	$('#rulegroup').submit();
}