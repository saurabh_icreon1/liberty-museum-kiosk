getmessageCenterInfo();function getmessageCenterInfo(){$.ajax({type:"GET",data:{},url:"/message-center",success:function
(response){$("#message_center").html(response)}})}
function getMyFile()
{
showAjxLoader();

var urlMyfile = "/my-file";
if(typeof userId !== 'undefined' && userId.trim() !='')
{
   urlMyfile +="/"+userId;
}
$.ajax({type:"POST",data:{},
        url:urlMyfile,success:function(response){hideAjxLoader();$("#profile_content").html(response)}})
}
function 
myDocumentSearch(){showAjxLoader();$.ajax({type:"POST",data:{},url:"/my-document-search/"+userId,success:function(
response){hideAjxLoader();$("#profile_content").html(response)}})}function myFofSearch(){showAjxLoader();$.ajax({type:
"POST",data:{},url:"/my-flag-of-faces/"+userId,success:function(response){hideAjxLoader();$("#profile_content").html(
response)}})}function myWohSearch(){showAjxLoader();$.ajax({type:"POST",data:{},url:"/my-wall-of-honor/"+userId,success:
function(response){hideAjxLoader();$("#profile_content").html(response)}})}function myFamilyHistory(){showAjxLoader();$.
ajax({type:"POST",data:{},url:"/user-family-history/"+userId,success:function(response){hideAjxLoader();$(
"#profile_content").html(response)}})}function myQuizes(){showAjxLoader();$.ajax({type:"POST",data:{},url:
"/user-quizes/"+userId,success:function(response){hideAjxLoader();$("#profile_content").html(response)}})}function 
myTransaction(){showAjxLoader();$.ajax({type:"POST",data:{},url:"/user-transaction-list/"+userId,success:function(
response){hideAjxLoader();$("#profile_content").html(response)}})}function getUserTransactionDetail(transactionId){$.
colorbox({width:"1150px",href:"/user-transaction-detail/"+transactionId,height:"550px",opacity:0.50,inline:false})}
function myCases(){showAjxLoader();$.ajax({type:"POST",data:{},url:"/user-cases/"+userId,success:function(response){
hideAjxLoader();$("#profile_content").html(response)}})}function myDigitalCertificate(){showAjxLoader();$.ajax({type:
"POST",data:{},url:"/user-digital-certificate/"+userId,success:function(response){hideAjxLoader();$("#profile_content").
html(response)}})}$(function(){if(mode=="case"){myCases();$("#tab-9").addClass("active");$("#add-woh").click()}else if(
mode=="familyhistory"){myFamilyHistory();$("#tab-6").addClass("active");$("#add-fof").click()}else if(mode=="woh"){
myWohSearch();$("#tab-4").addClass("active")}else{getManifestRecord();$("#tab-1").addClass("active")}$("a[id^='tab-']").click(
function(){$("a[id^='tab-']").removeClass("active");$(this).addClass("active");$("[class^='div-tab-']").css("display",
"none");$(".div-"+$(this).attr("id")).css("display","block")})})