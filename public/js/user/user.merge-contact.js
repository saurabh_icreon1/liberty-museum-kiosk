$(function(){$("#select_all").click(function(){$(".e2").prop("checked",$(this).prop("checked"));$(".e2").customInput()})
;$(".e2").click(function(){if($(".e2").length==$(".e2:checked").length)$("#select_all").attr("checked","checked");else $
("#select_all").removeAttr("checked");$(".e2").customInput()});var relationBreadCrumbAnchor=$(
".breadcrumb a:nth-child(3)");$(".breadcrumb a:nth-child(3)").after($(relationBreadCrumbAnchor).text());$(
relationBreadCrumbAnchor).remove()});function addDedupeException(){$.colorbox({width:"650px",href:"#add_dedupe_content",
height:"200px",inline:true});$("#no_delete").click(function(){$.colorbox.close();$("#yes_delete").unbind("click");
return false});$("#yes_delete").click(function(){$("#yes_delete").unbind("click");showAjxLoader();$.ajax({type:"POST",
data:{userId:$("#user_id").val(),duplicateUserId:$("#duplicate_user_id").val()},url:"/dedupe-exception-contact",success:
function(response){hideAjxLoader();if(!checkUserAuthenticationAjx(response))return false;var jsonObj=jQuery.parseJSON(
response);if(jsonObj.status=="success")if(group)window.location.href="/duplicate-contact/"+rule+"/"+group;else window.
location.href="/duplicate-contact/"+rule;else $("#success_message").hide()}})})}function backToDuplicateUserListing(){
if(group)window.location.href="/duplicate-contact/"+rule+"/"+group;else window.location.href="/duplicate-contact/"+rule}
function mergeAndList(){showAjxLoader();if($(".e2:checked").length)$.ajax({type:"POST",data:$("#merge_duplicate").
serialize(),url:"/merge-contact",success:function(response){hideAjxLoader();if(!checkUserAuthenticationAjx(response))
return false;var jsonObj=jQuery.parseJSON(response);if(jsonObj.status=="success")if(group)window.location.href=
"/duplicate-contact/"+rule+"/"+group;else window.location.href="/duplicate-contact/"+rule}});else{$("#error_message").
show();$("#error_message").html(EMPTY_MERGE);hideAjxLoader()}}function mergeAndNext(){showAjxLoader();var url;if(group)
url="/merge-contact/next/"+rule+"/"+group;else url="/merge-contact/next/"+rule;if($(".e2:checked").length){$(
"#error_message").hide();$.ajax({type:"POST",data:$("#merge_duplicate").serialize(),url:url,success:function(response){
hideAjxLoader();if(!checkUserAuthenticationAjx(response))return false;var jsonObj=jQuery.parseJSON(response);if(jsonObj.
status=="success")if(jsonObj.next)if(group)window.location.href="/duplicate-contact-info/"+jsonObj.user_id+"/"+jsonObj.
duplicate_user_id+"/"+rule+"/"+group;else window.location.href="/duplicate-contact-info/"+jsonObj.user_id+"/"+jsonObj.
duplicate_user_id+"/"+rule;else window.location.href="/merge-rule";else $("#success_message").hide()}})}else{$(
"#error_message").show();$("#error_message").html(EMPTY_MERGE);hideAjxLoader()}}function flipContact(){var userIdTemp=$(
"#user_id").val(),duplicateUserIdTemp=$("#duplicate_user_id").val();$("#user_id").val(duplicateUserIdTemp);$(
"#duplicate_user_id").val(userIdTemp);var userId=$("#user_id").val(),duplicateUserId=$("#duplicate_user_id").val();
showAjxLoader();$.ajax({type:"POST",data:$("#merge_duplicate").serialize(),url:"/flip-contact/"+userId+"/"+
duplicateUserId,success:function(response){hideAjxLoader();if(!checkUserAuthenticationAjx(response))return false;$(
"#flip-contact").html(response)}})}