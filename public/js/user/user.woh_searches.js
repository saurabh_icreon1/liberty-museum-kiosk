function loadMore(divId){$("#"+divId).toggle();$("#plus_"+divId).toggleClass("minus")}function getAdditionalWoh(panelNo,
isFinalize,bioCertificateProdId){if(isFinalize=="")isFinalize=0;if(panelNo=="")panelNo=0;if(bioCertificateProdId=="")
bioCertificateProdId=0;showAjxLoader();$.ajax({type:"GET",url:"/woh-additional-product/"+panelNo+"/"+isFinalize+"/"+
bioCertificateProdId,data:$("#edit_woh").serialize(),success:function(response){hideAjxLoader();$(
"#addition_woh_product").html(response)}})}function deleteWohSearch(value){authenticateUserBeforeAjax();$.colorbox({
width:"600px",href:"#woh_search",height:"260px",inline:true});$("#no_woh_search").click(function(){$.colorbox.close();$(
"#yes_woh_search").unbind("click");return false});$("#yes_woh_search").click(function(){$("#yes_woh_search").unbind(
"click");showAjxLoader();$.ajax({type:"POST",data:{search_id:value},url:"/delete-woh-search-myfile",success:function(
response){hideAjxLoader();var jsonObj=jQuery.parseJSON(response);if(jsonObj.status=="success")$("#woh_search_"+value).
hide();$("#tab-4").click();parent.jQuery.colorbox.close()}})})}function editDonatedBy(value){authenticateUserBeforeAjax(
);$.colorbox({width:"650px",height:"350px",iframe:true,href:"/edit-wall-of-honor/"+value})}function editBioWoh(wohId,
value){authenticateUserBeforeAjax();var url;if(value)url="/edit-bio-wall-of-honor/"+wohId+"/"+value;else url=
"/edit-bio-wall-of-honor/"+wohId;$.colorbox({width:"800px",height:"600px",iframe:true,href:url})}function viewBioWoh(
value){authenticateUserBeforeAjax();$.colorbox({width:"800px",height:"450px",iframe:true,href:"/view-bio-wall-of-honor/"
+value})}$(function(){jQuery.validator.addMethod("lettersonly",function(value,element){return this.optional(element)||
/^[a-z ]+$/i.test(value)},"Please enter valid detail.");jQuery.validator.addMethod("alphanumericspecialchar",function(
value,element){return this.optional(element)||/^[A-Za-z0-9\r\s\n ,.-]+$/i.test(value)},"Enter valid details");$(
"#edit_woh").validate({submitHandler:function(){showAjxLoader();$.ajax({type:"POST",url:"/edit-wall-of-honor/"+$(
"#woh_id").val(),data:$("#edit_woh").serialize(),success:function(response){hideAjxLoader();var jsonObj=jQuery.parseJSON
(response);if(jsonObj.status=="success"){parent.$("#tab-4").click();parent.jQuery.colorbox.close()}}})},rules:{
donated_by:{minlength:1,maxlength:50,required:true,lettersonly:true}},messages:{donated_by:{required:
"Please enter namesad",lettersonly:"Please enter valid name."}}});jQuery.validator.addMethod("lettersonlybio",function(
value,element){return this.optional(element)||/^[A-Za-z0-9,'\s".:]+$/i.test(value)},"Please enter valid detail.");jQuery
.validator.addMethod("alphanumericspecialcharbio",function(value,element){return this.optional(element)||
/^[A-Za-z0-9"'\r\s\n ,.\-:]+$/i.test(value)},"Enter valid details");$("#edit-biowoh").validate({submitHandler:function()
{var url;if($("#woh_id").val()!="")url="/edit-bio-wall-of-honor/"+$("#woh_id").val();else url="/edit-bio-wall-of-honor";
authenticateUserBeforeAjax();showAjxLoader();$.ajax({type:"POST",url:url,data:$("#edit-biowoh").serialize(),success:
function(response){hideAjxLoader();var jsonObj=jQuery.parseJSON(response);if(jsonObj.status=="success"){parent.$(
"#tab-4").click();parent.jQuery.colorbox.close()}}})},rules:{name:{required:true,minlength:1},immigrated_from:{required:
true,minlength:1,maxlength:50,lettersonlybio:true},ship_method:{required:true,minlength:1,maxlength:50,lettersonlybio:
true},port_of_entry:{required:true,minlength:1,maxlength:50,lettersonlybio:true},date_of_entry:{required:true},
additional_info:{minlength:1,maxlength:500,alphanumericspecialcharbio:true}},messages:{name:{required:
"This field cannot be empty"},immigrated_from:{required:"This field cannot be empty",lettersonlybio:
"Please enter valid detail."},ship_method:{required:"This field cannot be empty",lettersonlybio:
"Please enter valid detail."},port_of_entry:{required:"This field cannot be empty",lettersonlybio:
"Please enter valid detail."},date_of_entry:{required:"This field cannot be empty"},additional_info:{
alphanumericspecialcharbio:"Please enter valid detail."}}})})