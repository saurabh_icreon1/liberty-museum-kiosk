function addEditAddressInfo(value,mode){
    var url
    ;
    if(value)url="/add-edit-user-address-info/"+mode+"/"+value;else url="/add-edit-user-address-info/"+mode;
    showAjxLoader()
    ;
    $.ajax({
        type:"GET",
        data:{},
        url:url,
        success:function(response){
            hideAjxLoader();
            $("#address_info_div").show();
            $(
                "#address_info_div").html(response);
            location.href="#address_info_div";
            var $radios=$("input:radio[name=is_apo_po_record]"
                ),typeValue=$("input[name=is_apo_po_record]:checked","#street_address").val();
            if(typeValue!="")setApoPoAddress(typeValue
                );
            if($("#addressinfo_location_billing").is(":checked")==true){
                $("#country_block").show();
                $("#apo_po_country_block").hide
                ();
                if($("#0").val()==228){
                    $("#usa_state_shipping").show();
                    $("#address_state_id").show();
                    $("#address_state_id_0").val($("#us_state_id_value").val());
                    $("#other_state_shipping").hide();
                    $("#addressinfo_state").hide()
                }
                else{$("#usa_state_shipping").hide();
                    $("#other_state_shipping").show();
                    $("#addressinfo_state").show()
                }
            }
        }
    })
}
function deleteAddressInfo(value,mode){
    
    authenticateUserBeforeAjax();
    $.colorbox({
        width:"640px",
        href:"#delete_address_info",
        height:"260px",
        inline:
        true,
        onClosed:function(){
            location.href="#topOfProfilePage"
        }
    });
    $("#no_delete_address").click(function(){
        $.colorbox.close(
            );
        $("#yes_delete_address").unbind("click");
        return false
    });
    $("#yes_delete_address").click(function(){
        $(
            "#yes_delete_address").unbind("click");
        $.ajax({
            type:"POST",
            data:{
                address_id:value
            },
            url:"/delete-user-address-info",
            success:function(response){
                var jsonObj=jQuery.parseJSON(response);
                if(jsonObj.status=="success"){
                    window.location.href="/add-new-token";
                    parent.jQuery.colorbox.close();
                }
            }
        })
    })
}

function addNewToken(){
    var url="/add-token";
    showAjxLoader();
    $.ajax({
        type:"GET",
        data:{},
        url:url,
        success:function(response){
            hideAjxLoader();
            $("#add_token_info_div").html(response);
            $("html, body").animate({scrollTop:$("#add_token_info_div").offset().top},2000)
            }
        })
}
$(function(){
    addNewToken();
    $("#tab-3").addClass("active");
    return false;
});