function getUserTransactionDetail(transactionId) {
    $.colorbox({width: "750px", href: "/user-transaction-detail-crm/" +
        transactionId, height: "550px", opacity: 0.50, inline: false})
}
$(function() {
    $("select.e1").select2({});
    $(".e2").customInput();
    var url = "/upload-development-file/" + $("#user_id").val();
    $(".fileUploadAddMore").fileupload({url: url, dataType: "json", done
                : function(e, data) {
                    var file_id = $(this).attr("id"), file_number = file_id.substr(file_id.length - 1, 1);
                    data = data.result;
                    if (data
                            .status == "success") {
                        $("#filemessage_" + file_number).addClass("success-msg");
                        $("#filemessage_" + file_number).html(data.
                                message);
                        $("#filemessage_" + file_number).removeClass("error-msg");
                        $("#publicdocs_upload_file" + file_number).val(data.
                                filename)
                    } else {
                        $("#filemessage_" + file_number).removeClass("success-msg");
                        $("#filemessage_" + file_number).addClass(
                                "error-msg");
                        $("#filemessage_" + file_number).html(data.message);
                        $("#publicdocs_upload_file" + file_number).val("")
                    }
                }, start:
                function(e) {
                    showAjxLoader()
                }, stop: function(e) {
            hideAjxLoader()
        }});
    url = "/import-user-development/" + $("#user_id").val();
    $(
            "#file_name").fileupload({url: url, dataType: "json", done: function(e, data) {
            data = data.result;
            if (data.status == "success") {
                $(
                        "#import-success").show();
                $("#import-success").html(data.message);
                $("#import-error").hide();
                userDevelopment()
            } else {
                $(
                        "#import-error").show();
                $("#import-error").html(data.message);
                $("#import-success").hide()
            }
        }, start: function(e) {
            showAjxLoader()
        }, stop: function(e) {
            hideAjxLoader()
        }});
    var grid = jQuery("#list"), emptyMsgDiv = $(
            '<div class="no-record-msz">' + NO_RECORD_FOUND + "</div>");
    $("#list").jqGrid({postData: {}, mtype: "POST", url:
                "/user-soleif-gift/" + $("#user_id").val(), datatype: "json", colNames: [L_DATE, L_AMOUNT, L_TRANSACTION, "Product Name",
            L_CAMPAIGN], colModel: [{name: L_DATE, index: "usr_transaction.transaction_date"}, {name: L_AMOUNT, sortable: false}, {name:
                L_TRANSACTION, sortable: false}, {name: "Product Name", sortable: false}, {name: L_CAMPAIGN, sortable: false}], viewrecords: true,
        sortname: "usr_transaction.transaction_date", sortorder: "desc", rowNum: 10, rowList: [10, 20, 30], pager: "#pcrud", viewrecords:
                true, autowidth: true, shrinkToFit: true, caption: "", width: "100%", cmTemplate: {title: false}, loadComplete: function() {
            var count = grid.getGridParam(), ts = grid[0];
            if (ts.p.reccount === 0) {
                grid.hide();
                emptyMsgDiv.show();
                $("#pcrud_right div.ui-paging-info")
                        .css("display", "none")
            } else {
                grid.show();
                emptyMsgDiv.hide();
                $("#pcrud_right div.ui-paging-info").css("display", "block")
            }
        }
    });
    emptyMsgDiv.insertAfter(grid.parent());
    emptyMsgDiv.hide();
    $("#list").jqGrid("navGrid", "#pcrud", {reload: true, edit:
        false, add: false, search: false, del: false});
    $("#last_gift_date").datepicker({changeMonth: true, changeYear: true});
    $("#largest_gift_date").datepicker({changeMonth: true, changeYear: true});
    $("#period_from0").datepicker({changeMonth: true,
        changeYear: true, onClose: function(selectedDate) {
            $("#period_to0").datepicker("option", "minDate", selectedDate)
        }});
    $("#period_to0").datepicker({changeMonth: true, changeYear: true, onClose: function(selectedDate) {
            $("#period_from0").datepicker
                    ("option", "maxDate", selectedDate)
        }});
    jQuery.validator.addMethod("alphanumericspecialchar", function(value, element) {
        return this.optional(element) || /^[A-Za-z0-9\n ,.-]+$/i.test(value)
    }, VALID_DETAILS);
    $("#development").validate({
        submitHandler: function() {
            var dataStr = $("#development").serialize();
            if (checkCustomValidation()) {
                showAjxLoader();
                $.ajax({
                    type: "POST", url: "/user-development/" + $("#user_id").val(), data: dataStr, success: function(responseData) {
                        hideAjxLoader();
                        if (
                                !checkUserAuthenticationAjx(responseData))
                            return false;
                        var jsonObj = jQuery.parseJSON(responseData);
                        if (jsonObj.status ==
                                "success") {
                            $("#success-message").show();
                            $("#success-message").html(jsonObj.message);
                            window.scrollTo(0, 0)
                        } else {
                            $(
                                    "#success-message").hide();
                            $.each(jsonObj.message, function(i, msg) {
                                $("#" + i).after(
                                        '<label class="error" style="display:block;">' + msg + "</label>")
                            })
                        }
                    }})
            }
        }, rules: {total_given_amount: {number: true, minlength:
                1, maxlength: 10}, total_no_of_gifts: {number: true, minlength: 1, maxlength: 10}, last_gift_amount: {number: true, minlength: 1,
                maxlength: 10}, largest_gift_amount: {number: true, minlength: 1, maxlength: 10}, total_income: {number: true, minlength: 1, maxlength
                        : 10}, total_real_estate_value: {number: true, minlength: 1, maxlength: 10}, total_stock_value: {number: true, minlength: 1, maxlength
                        : 10}, total_pension_value: {number: true, minlength: 1, maxlength: 10}, total_political_non_profit_giving_amount: {number: true,
                minlength: 1, maxlength: 10}, income_portion_of_egc_calculation: {number: true, minlength: 1, maxlength: 10},
            pol_non_profit_giving_portion_of_egc_calc: {number: true, minlength: 1, maxlength: 10}, rating_summary_giving_capacity_rating: {
                number: true, minlength: 1, maxlength: 10}, rating_summary_giving_capacity_range: {number: true, minlength: 1, maxlength: 10},
            estimated_giving_capacity: {number: true, minlength: 1, maxlength: 10}, propensity_to_give_score: {number: true, minlength: 1,
                maxlength: 10}, propensity_to_give_score_part_2: {number: true, minlength: 1, maxlength: 10}, combined_propensity_to_give_score: {
                number: true, minlength: 1, maxlength: 10}, planned_giving_bequest: {number: true, minlength: 1, maxlength: 10},
            planned_giving_annuity: {number: true, minlength: 1, maxlength: 10}, planned_giving_trust: {number: true, minlength: 1, maxlength: 10
            }, rating_summary_influence: {number: true, minlength: 1, maxlength: 10}, rating_summary_inclination: {number: true, minlength: 1,
                maxlength: 10}, total_quality_of_match: {number: true, minlength: 1, maxlength: 10}, charitable_donations_of_qom: {number: true,
                minlength: 1, maxlength: 10}, trustees_qom: {number: true, minlength: 1, maxlength: 10}, guidestar_directors_qom: {number: true,
                minlength: 1, maxlength: 10}, guidestar_foundations_qom: {number: true, minlength: 1, maxlength: 10},
            hoover_business_information_qom: {number: true, minlength: 1, maxlength: 10}, household_profile_qom: {number: true, minlength: 1,
                maxlength: 10}, lexis_nexis_qom: {number: true, minlength: 1, maxlength: 10}, major_donor_qom: {number: true, minlength: 1, maxlength:
                10}, philanthropic_donations_qom: {number: true, minlength: 1, maxlength: 10}, volunteers_and_directors_qom: {number: true,
                minlength: 1, maxlength: 10}, wealth_id_securities_qom: {number: true, minlength: 1, maxlength: 10}, bio_information: {
                alphanumericspecialchar: true, minlength: 2, maxlength: 200}, description: {alphanumericspecialchar: true, minlength: 2, maxlength:
                200}}, messages: {total_given_amount: {number: VALID_AMOUNT}, total_no_of_gifts: {number: VALID_AMOUNT}, last_gift_amount: {
                number: VALID_AMOUNT}, largest_gift_amount: {number: VALID_AMOUNT}, total_income: {number: VALID_AMOUNT},
            total_real_estate_value: {number: VALID_AMOUNT}, total_stock_value: {number: VALID_AMOUNT}, total_pension_value: {number:
                VALID_AMOUNT}, total_political_non_profit_giving_amount: {number: VALID_AMOUNT}, income_portion_of_egc_calculation: {number:
                VALID_AMOUNT}, pol_non_profit_giving_portion_of_egc_calc: {number: VALID_AMOUNT}, rating_summary_giving_capacity_rating: {
                number: VALID_AMOUNT}, rating_summary_giving_capacity_range: {number: VALID_AMOUNT}, estimated_giving_capacity: {number:
                VALID_AMOUNT}, propensity_to_give_score: {number: VALID_AMOUNT}, propensity_to_give_score_part_2: {number: VALID_AMOUNT},
            combined_propensity_to_give_score: {number: VALID_AMOUNT}, planned_giving_bequest: {number: VALID_AMOUNT},
            planned_giving_annuity: {number: VALID_AMOUNT}, planned_giving_trust: {number: VALID_AMOUNT}, rating_summary_influence: {number
                        : VALID_AMOUNT}, rating_summary_inclination: {number: VALID_AMOUNT}, total_quality_of_match: {number: VALID_AMOUNT},
            charitable_donations_of_qom: {number: VALID_AMOUNT}, trustees_qom: {number: VALID_AMOUNT}, guidestar_directors_qom: {number:
                VALID_AMOUNT}, guidestar_foundations_qom: {number: VALID_AMOUNT}, hoover_business_information_qom: {number: VALID_AMOUNT},
            household_profile_qom: {number: VALID_AMOUNT}, lexis_nexis_qom: {number: VALID_AMOUNT}, major_donor_qom: {number: VALID_AMOUNT},
            philanthropic_donations_qom: {number: VALID_AMOUNT}, volunteers_and_directors_qom: {number: VALID_AMOUNT},
            wealth_id_securities_qom: {number: VALID_AMOUNT}}, highlight: function(label) {
            $(".detail-section").css("display", "block")
        }});
		
		//start soft credit 
		 var gridDonar = jQuery("#listDonar"), emptyMsgDivDonar = $('<div class="no-record-msz">' + NO_RECORD_FOUND + "</div>");
    $("#listDonar").jqGrid({postData: {}, mtype: "POST", url:
                "/user-softcredit-received/" + $("#user_id").val(), datatype: "json", 
				colNames: ["Date", "Name", "Amount", "Transaction#","Note","Action"], 
				colModel: [{name: "Date", index: "soft_credits.credit_date"}, 
				{name: "name", index: "name",sortable: true}, 
				{name:"total_amount", index: "total_amount",sortable: true},
				{name:"transaction_id", index: "transaction_id",sortable: true},
				{name: "note", index:"note",sortable: false}, 
				{name: '', sortable: false}], viewrecords: true,
        sortname: "soft_credits.credit_date", sortorder: "desc", rowNum: 10, rowList: [10, 20, 30], pager: "#pcrudDonar", viewrecords:
                true, autowidth: true, shrinkToFit: true, caption: "", width: "100%", cmTemplate: {title: false}, loadComplete: function() {
            var count = gridDonar.getGridParam(), ts = gridDonar[0];
            if (ts.p.reccount === 0) {
                gridDonar.hide();
                emptyMsgDivDonar.show();
                $("#pcrud_right div.ui-paging-info").css("display", "none")
            } else {
                gridDonar.show();
                emptyMsgDivDonar.hide();
                $("#pcrud_right div.ui-paging-info").css("display", "block")
            }
        }
    });
	
    emptyMsgDivDonar.insertAfter(gridDonar.parent());
    emptyMsgDivDonar.hide();
    $("#listDonar").jqGrid("navGrid", "#pcrudDonar", {reload: true, edit:
        false, add: false, search: false, del: false});
		//end soft credit 
		
		//start soft credit provider 
		 var gridProvider = jQuery("#listProvider"), emptyMsgDivProvider = $(
            '<div class="no-record-msz">' + NO_RECORD_FOUND + "</div>");
    $("#listProvider").jqGrid({postData: {}, mtype: "POST", url:
                "/user-softcredit-provider/" + $("#user_id").val(), datatype: "json", 
				colNames: ["Date", "Name", "Amount", "Transaction#","Note","Action"], 
				colModel: [{name: "Date", index: "soft_credits.credit_date"}, 
				{name: "name", index: "name",sortable: true}, 
				{name:"total_amount", index: "total_amount",sortable: true},
				{name:"transaction_id", index: "transaction_id",sortable: true},
				{name: "note", index:"note",sortable: false}, 
				{name: '', sortable: false}], 
				viewrecords: true,
        sortname: "soft_credits.credit_date", sortorder: "desc", rowNum: 10, rowList: [10, 20, 30], pager: "#pcrudProvider", viewrecords:
                true, autowidth: true, shrinkToFit: true, caption: "", width: "100%", cmTemplate: {title: false}, loadComplete: function() {
            var count = gridProvider.getGridParam(), ts = gridProvider[0];
            if (ts.p.reccount === 0) {
                gridProvider.hide();
                emptyMsgDivProvider.show();
                $("#pcrud_right div.ui-paging-info")
                        .css("display", "none")
            } else {
                gridProvider.show();
                emptyMsgDivProvider.hide();
                $("#pcrud_right div.ui-paging-info").css("display", "block")
            }
        },
	
    });
    emptyMsgDivProvider.insertAfter(gridProvider.parent());
    emptyMsgDivProvider.hide();
    $("#listProvider").jqGrid("navGrid", "#pcrudProvider", {reload: true, edit:
        false, add: false, search: false, del: false});
		//end soft credit provider
		
    $(document).on("click", "#add_new_file", function() {
        var i = $("#count_upload_div").text(), allDivBlock = $(
                "div[id^='file_container_']");
        if (allDivBlock.length == 1) {
            var parId = $(this).parents("div[id^='file_container_']").attr(
                    "id"), id = parId.substring(parId.lastIndexOf("_") + 1, parId.length);
            $(this).parent().append(
                    "<a  class='minus' onclick=remove_dynamic_contacts('.file_container_" + id + "','" + id + "')>")
        }
        $(this).remove();
        $("<div />", {
            "class": "row file_container_" + i, id: "file_container_" + i}).appendTo("#upload_file_div");
        $(".file_container_" + i).load(
                "/upload-file/" + i + "/file_container_/1");
        i++;
        $("#count_upload_div").html(i);
        $("#publicdocs_upload_count").val(i)
    });
    $(
            document).on("click", "#load_org_div", function() {
        var i = $("#org_counter").text(), allDivBlock = $("div[id^='org_container_']"
                );
        if (allDivBlock.length == 1) {
            var parId = $(this).parents("div[id^='org_container_']").attr("id"), id = parId.substring(parId.
                    lastIndexOf("_") + 1, parId.length);
            $(this).parent().append(
                    "<a  class='minus' onclick=remove_dynamic_contacts('.org_container_" + id + "','" + id + "')>")
        }
        $(this).remove();
        $("<div />", {
            "class": "row change-form-bg org_container_" + i, id: "org_container_" + i, style: "width:97.5%; padding:15px"}).appendTo(
                "#organization_div");
        $(".org_container_" + i).load("/gift/" + i + "/org_container_");
        i++;
        $("#org_counter").html(i)
    });
    $(
            document).on("click", "#load_job_history", function() {
        var i = $("#job_history_counter").text(), allDivBlock = $(
                "div[id^='job_history_container_']");
        if (allDivBlock.length == 1) {
            var parId = $(this).parents(
                    "div[id^='job_history_container_']").attr("id"), id = parId.substring(parId.lastIndexOf("_") + 1, parId.length);
            $(this).parent
                    ().append("<a  class='minus' onclick=remove_dynamic_contacts('.job_history_container_" + id + "','" + id + "')>")
        }
        $(this).remove
                ();
        $("<div />", {"class": "row change-form-bg job_history_container_" + i, id: "job_history_container_" + i, style:
                    "width:97.5%; padding:15px"}).appendTo("#job_history_div");
        $(".job_history_container_" + i).load("/jobhistory/" + i +
                "/job_history_container_");
        i++;
        $("#job_history_counter").html(i)
    });
    $(".company_name_auto_search").autocomplete({afterAdd
                : true, selectFirst: true, autoFocus: true, source: function(request, response) {
                    $.ajax({url: "/get-company", method: "POST",
                        dataType: "json", data: {company_name: $("#comp_name0").val()}, success: function(jsonResult) {
                            if (!checkUserAuthenticationAjx(
                                    jsonResult))
                                return false;
                            var resultset = [];
                            $.each(jsonResult, function() {
                                var id = this.company_id, value = this.company_name;
                                resultset.push({id: this.company_id, value: this.company_name})
                            });
                            response(resultset)
                        }})
                }, open: function() {
            $(".ui-menu").
                    width(350)
        }, change: function(event, ui) {
            if (ui.item) {
                $("#comp_name0").val(ui.item.value);
                $("#company_id0").val(ui.item.id)
            }
            else {
                $("#comp_name0").val("");
                $("#company_id0").val("")
            }
        }, focus: function(event, ui) {
            $("#company_id0").val(ui.item.id);
            return false
        }, select: function(event, ui) {
            $("#comp_name0").val(ui.item.value);
            $("#company_id0").val(ui.item.id);
            return false
        }});
    $("div[id^=industry]").change(function() {
        $("div[class^='industry_desc']").hide();
        $(".industry_desc" + this
                .value).show()
    });
    if ($(".industry_desc" + $("#industry_desc").val()))
        $(".industry_desc" + $("#industry_desc").val()).show()
})
        ;
function loadRemove(i, classname) {
    $("#primary_Div").show();
    $("." + classname + "" + i).append($(
            "<a style='float: right;' class='minus m-l-5' onclick=remove_dynamic_contacts('." + classname + "" + i + "','" + i + "')>", {href:
                        "javascript:void(0)", id: "remove"}))
}
function remove_dynamic_contacts(classname, i) {
    var plusEle = $(classname).find(".plus")
            ;
    $(classname).remove();
    var classDivId = classname.substring(1, classname.lastIndexOf("_") + 1), lastBlock = $("div[id^='" +classDivId + "']").last();
    if ($(lastBlock).find(".plus").length == 0) {
        var plusId = $(plusEle).attr("id");
        $(lastBlock).find(".addMinus").append('<a href="javascript:void(0);" id="' + plusId + '" class="plus"></a>')
    }
    var allDivBlock = $("div[id^='" +
            classDivId + "']");
    if (allDivBlock.length == 1)
        $(lastBlock).find(".minus").remove()
}
function checkCustomValidation() {
    var flagY = false, flagO = false, flagA = false, flagT = false;
    $(".detail-section").css("display", "block");
    $(
            '#development [name="year[]"]').each(function(index) {
        var str = $(this).val(), regx = /^[0-9]+$/i;
        if (str.match(regx) || str.
                length == 0) {
            var flag = false;
            if (str.length < 1 && str != "") {
                if ($(this).next("span").length == 0)
                    $(this).after(
                            '<span for="year[]" class="error" style="display:block !important;">' + VALID_4_NUMBER + "</span>");
                flag = true
            } else if (str.
                    length > 4 && str != "") {
                if ($(this).next("span").length == 0)
                    $(this).after(
                            '<span for="year[]" class="error" style="display:block !important;">' + VALID_4_NUMBER + "</span>");
                flag = true
            } else {
                $(this).
                        next().remove("span");
                flag = false
            }
            flagY = flag
        } else {
            if ($(this).next("span").length == 0)
                $(this).after(
                        '<span for="year[]" class="error" style="display:block !important;">' + VALID_YEAR + "</span>");
            flagY = true
        }
    });
    $(
            '#development [name="organization[]"]').each(function(index) {
        var str = $(this).val(), regx = /^[A-Za-z0-9\n ,.-]+$/i;
        if (str.
                match(regx) || str.length == 0) {
            var flag = false;
            if (str.length < 2 && str != "") {
                if ($(this).next("span").length == 0)
                    $(this).after(
                            '<span for="organization[]" class="error" style="display:block !important;">' + LESS_THAN_2_CHAR + "</span>");
                flag = true
            } else
            if (str.length > 30 && str != "") {
                if ($(this).next("span").length == 0)
                    $(this).after(
                            '<span for="organization[]" class="error" style="display:block !important;">' + VALID_30_CHAR + "</span>");
                flag = true
            } else {
                $(
                        this).next().remove("span");
                flag = false
            }
            flagO = flag
        } else {
            if ($(this).next("span").length == 0)
                $(this).after(
                        '<span for="organization[]" class="error" style="display:block !important;">' + VALID_DETAILS + "</span>");
            flagO = true
        }
    });
    $(
            '#development [name="amount[]"]').each(function(index) {
        var str = $(this).val(), regx = /^[0-9.]+$/i;
        if (str.match(regx) || str.
                length == 0) {
            var flag = false;
            if (str.length < 1 && str != "") {
                if ($(this).next("span").length == 0)
                    $(this).after(
                            '<span for="amount[]" class="error" style="display:block !important;">' + VALID_10_NUMBER + "</span>");
                flag = true
            } else if (str
                    .length > 10 && str != "") {
                if ($(this).next("span").length == 0)
                    $(this).after(
                            '<span for="amount[]" class="error" style="display:block !important;">' + VALID_10_NUMBER + "</span>");
                flag = true
            } else {
                $(this
                        ).next().remove("span");
                flag = false
            }
            flagA = flag
        } else {
            if ($(this).next("span").length == 0)
                $(this).after(
                        '<span for="amount[]" class="error" style="display:block !important;">' + VALID_AMOUNT + "</span>");
            flagA = true
        }
    });
    $('#development [name="job_title[]"]').each(function(index) {
        var str = $(this).val(), regx = /^[a-z ]+$/i;
        if (str.match(regx) ||
                str.length == 0) {
            var flag = false;
            if (str.length < 2 && str != "") {
                if ($(this).next("span").length == 0)
                    $(this).after(
                            '<span for="job_title[]" class="error" style="display:block !important;">' + VALID_16_CHAR + "</span>");
                flag = true
            } else if (
                    str.length > 16 && str != "") {
                if ($(this).next("span").length == 0)
                    $(this).after(
                            '<span for="job_title[]" class="error" style="display:block !important;">' + VALID_16_CHAR + "</span>");
                flag = true
            } else {
                $(
                        this).next().remove("span");
                flag = false
            }
            flagT = flag
        } else {
            if ($(this).next("span").length == 0)
                $(this).after(
                        '<span for="job_title[]" class="error" style="display:block !important;">' + VALID_DETAILS + "</span>");
            flagT = true
        }
    });
    if (
            flagY || flagA || flagO || flagT)
        return false;
    else
        return true
}
function createComapny(key) {
    $.colorbox({width: "900px", height:
                "600px", iframe: true, href: "/create-company/" + key})
}
/*function userDevelopment() {
    $.ajax({type: "GET", data: {}, url:"/user-development/" + $("#user_id").val(), success: function(response) {
            if (!checkUserAuthenticationAjx(response))
                return false;
            $("#contact_content").html(response)
        }})
} */
function removePrivateDoc(userId, fileName, key) {
    $.colorbox({width:
                "500px", href: "#delete_contact_development_doc", height: "200px", inline: true});
    $("#no_delete").click(function() {
        $.colorbox.
                close();
        $("#yes_delete").unbind("click");
        return false
    });
    $("#yes_delete").click(function() {
        $("#yes_delete").unbind(
                "click");
        $.ajax({type: "POST", data: {userId: userId, fileName: fileName}, url: "/delete_contact_development_doc", success:
                    function(response) {
                        if (!checkUserAuthenticationAjx(response))
                            return false;
                        $.colorbox.close();
                        $("#doc_" + key).hide()
                    }})
    })
}
function assignSoftCredit()
{
    showAjxLoader();
    $.ajax({
        type: 'GET',
        data: {},
        url: '/soft-credit',
        success: function(response)
        {
            if (!checkUserAuthenticationAjx(response)) {
                return false;
            } else {
                hideAjxLoader();
                $.colorbox.close();
                $.colorbox({
                    width: "950px",
                    height: "850px",
                    iframe: true,
                    href: "/soft-credit"
                });
            }
        }
    });
}
function deleteUserCreditTransaction(rowId,grid) {
    $.colorbox({width:"500px", href: "#delete_user_credit_transaction", height: "200px", inline: true});
    $("#no_delete_credit").click(function() {
        $.colorbox.close();
        $("#yes_delete_credit").unbind("click");
        return false
    });
	$("#yes_delete_credit").click(function() {
        $("#yes_delete_credit").unbind("click");
        $.ajax({type: "POST", data: {id: rowId}, url: "/delete-user-credit-transaction", 
		success:function(response) {
                        if (!checkUserAuthenticationAjx(response))
                            return false;
							
						$("#"+grid).trigger("reloadGrid", [{current: true}]);
                        $.colorbox.close();
                    }})
    })
}