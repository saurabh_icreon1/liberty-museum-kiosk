$(function(){$("#crm_forgot").validate({submitHandler:function(){var dataStr=$("#crm_forgot").serialize();showAjxLoader(
);$.ajax({type:"POST",url:"/crm-forgot",data:dataStr,success:function(responseData){var jsonObj=jQuery.parseJSON(
responseData);if(jsonObj.status=="success"){$("#error_message").hide();$("#success_message").show();$("#success_message"
).html(jsonObj.message)}else{$("#error_message").show();$("#success_message").hide();$("#error_message").html(jsonObj.
message)}hideAjxLoader()}})},rules:{email_id:{required:true,email:true}},messages:{email_id:{required:EMAIL_EMPTY,email:
EMAIL_INVALID}}});$.validator.addMethod("alplhanumbericwithcapital",function(value,element){return this.optional(element
)||/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,25}$/.test(value)},PASSWORD_INVALID);$("#crm_password_reset").validate({
submitHandler:function(){var dataStr=$("#crm_password_reset").serialize();$.ajax({type:"POST",url:"/crm-password",data:
dataStr,success:function(responseData){var jsonObj=jQuery.parseJSON(responseData);if(jsonObj.status=="success"){$(
"#error_message").hide();$("#success_message").show();$("#success_message").html(jsonObj.message);setTimeout(function(){
window.location.href="/crm"},5000)}else{$("#error_message").show();$("#success_message").hide();$("#error_message").html
(jsonObj.message)}}})},rules:{email_id:{required:true,email:true},password:{required:true,minlength:6,maxlength:25,
alplhanumbericwithcapital:true},password_conf:{required:true,equalTo:"#crm_password_reset #password"}},messages:{
email_id:{required:EMAIL_EMPTY,email:EMAIL_INVALID},password:{required:PASSWORD_EMPTY,minlength:PASSWORD_LENGTH_MIN,
maxlength:PASSWORD_LENGTH_MAX},password_conf:{required:PASSWORD_CONFIRM_EMPTY,equalTo:PASSWORD_CONFIRM_NOT_MATCHED}}})})