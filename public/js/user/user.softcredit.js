$(function(){
 $("#added_date_from").datepicker({
        changeMonth:true,
        changeYear:true,
        showOtherMonths:true,
        onClose:function(
            selectedDate){
            $("#added_date_to").datepicker("option","minDate",selectedDate)
        }
    });
    $("#added_date_to").datepicker({
        changeMonth:true,
        changeYear:true,
        showOtherMonths:true,
        onClose:function(selectedDate){
            $("#added_date_from").datepicker(
                "option","maxDate",selectedDate)
        }
    });
function showResult(){	
$('#transaction_id_note').show();
$('#transaction_id_note_2').show();

$.jgrid.no_legacy_api=true;
$.jgrid.useJSON=true;
var grid=jQuery("#list"),emptyMsgDiv=$('<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");
$("#list").jqGrid({
    postData:{
        searchString:$("#searchCredit").serialize()
        },
    mtype:"POST",
    url:"/get-search-credit-transactions",
    datatype:
    "json",
    sortable:true,
    colNames:["Date","Transaction#","Name","Amount","Actions"],
    colModel:[
	{
        name:"date",
        index:"usr_transaction.transaction_date",
        sortable:true
    },{
        name:"transaction_id",
        index:"transaction_id",
        sortable:true
    },{
        name:"full_name",
        index:"full_name",
        sortable:true
    },{
        name:"transaction_amount",
        index:"transaction_amount",
		sortable:true
    },{
        name:"",
        index:"",
		sortable:false
    }],
    sortname:"usr_transaction.transaction_date",
    sortorder:"DESC",
    rowNum:10,
    rowList:[10,20,30],
    pager:"#pcrud",
    multiselect:false,
    viewrecords:true,
    autowidth:true,
    shrinkToFit:true,
    caption:"",
    width:"100%",
    cmTemplate:{
        title:false
    },
    loadComplete:function(data){
        hideAjxLoader();
        var count=grid.getGridParam(),ts=grid[0];
        if(ts.p.reccount===0){
            grid.hide();
            emptyMsgDiv.show();
            $("#pcrud_right div.ui-paging-info").css("display","none")
            }else{
            grid.show();
            emptyMsgDiv.hide();
            $("#pcrud_right div.ui-paging-info").css("display","block")
            }
        var ids=jQuery("#list").jqGrid("getDataIDs");
        for(var i=0;i<ids.length;i++){
            var rowId=ids[i],rowData=jQuery("#list").jqGrid("getRowData",rowId),userRole=rowData.Role;
            if(userRole=="Super Admin"){
                $("#jqg_list_"+rowId).prop("checked",false);
                $("#jqg_list_"+data.rows[i].id).attr("disabled",true)
                }
            }
        },
onSelectAll:function(aRowids,status){
    if(status){
        for(var i=0;i<aRowids.length;i++){
            var rowId=aRowids[i],rowData=jQuery(
                "#list").jqGrid("getRowData",rowId),userRole=rowData.Role;
            if(userRole=="Super Admin")$("#"+rowId).removeClass(
                "ui-state-highlight")
            }
            var cbs=$("tr.jqgrow > td > input.cbox:disabled",grid[0]);
        cbs.removeAttr("checked");
        grid[0].p.
        selarrrow=grid.find("tr.jqgrow:has(td > input.cbox:checked)").map(function(){
            return this.id
            }).get()
        }
    },
onCellSelect: function(rowid,iCol,cellcontent,e){
    var rowData=jQuery("#list").jqGrid("getRowData",rowid),userRole=rowData.Role;
    if(userRole=="Super Admin"){
            $("#"+grid+" tr#"+rowid).css("background","none");
            $("#"+grid+" tr.jqgrow:odd").css("background","#DDDDDC")
        }
    }
});
emptyMsgDiv.insertAfter(grid.parent());
emptyMsgDiv.hide();
$("#list").jqGrid("navGrid","#pcrud",{
    reload:
    true,
    edit:false,
    add:false,
    search:false,
    del:false
});
}
$("#submitbutton").click(function(){
        $('#tra-err-msg').hide();
        var countSelectVal = 0;
        $('form#searchCredit input,form#searchCredit select,form#searchCredit textarea').each(function(index){
            var input = $(this);
            if(input.attr('name') != undefined && input.attr('name') != 'submitbutton' && input.attr('name') != 'received_date_from' && input.attr('name') != 'received_date_to' && $.trim(input.val()) != undefined && $.trim(input.val()) != "") {
                countSelectVal = parseInt(countSelectVal) + parseInt(1); 
            }
        });
        $('label[for="added_date_from"]').remove();
        
        var diffYear = 0;
        if ($.trim($('#received_date_range').val()) == "1" && ($.trim($('#added_date_from').val()) != "" || $.trim($('#added_date_to').val()) != ""))
        {
            var toDate = new Date();
            var fromDate = new Date();

            var tmp_reg_Date_From = $.trim($('#added_date_from').val());
            var tmp_reg_Date_To = $.trim($('#added_date_to').val());
            if (tmp_reg_Date_From == "")
            {
                toDate = new Date(tmp_reg_Date_To);
            }
            else if (tmp_reg_Date_To == "")
            {
                fromDate = new Date(tmp_reg_Date_From);
            }
            else
            {
                toDate = new Date(tmp_reg_Date_To);
                fromDate = new Date(tmp_reg_Date_From);
            }
            var oneYear = 365 * 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
            var diffYear = Math.abs((toDate.getTime() - fromDate.getTime()) / (oneYear));
        }
       
        if(countSelectVal < 2) {
          $("html, body").animate({scrollTop:$(".accordion-detail").offset().top},2000);
          $('#search-err-msg_need_two').css({display: "none"});
          $('#search-err-msg').css({display:"block"}); 
          return false;  
        }
      
        else if($.trim($('#received_date_range').val()) == "1" && $.trim($('#added_date_from').val()) == "" && $.trim($('#added_date_to').val()) == "") {
           $("html, body").animate({scrollTop:$(".accordion-detail").offset().top},2000); 
           $('#added_date_from').after('<label for="added_date_from" class="error" style="display:block;">Please enter date range.</label>');
           return false;
        }
        else {  
            $('label[for="added_date_from"]').remove();
            $('#search-err-msg').css({display:"none"});
            $('#search-err-msg_need_two').css({display: "none"});
             showAjxLoader();
			 $(".search-results").show();
             showResult();
			 jQuery("#list").jqGrid("setGridParam",{
                 postData:{
                     searchString:$("#searchCredit").serialize()
                 }
             });
             jQuery("#list").trigger("reloadGrid",[{page:1}]);
             window.location.hash="#saved_search";

             hideAjxLoader();
             return false;
        }  
    }
    );

});
function showDate(id,divId){ 
    if($("#"+id+" option:selected").text()=="Choose Date Range") { 
        $("#"+divId).show(); 
    }
    else { 
        $("#"+divId).hide(); 
        if(id == 'received_date_range') {
            $('#added_date_from').val('');
            $('#added_date_to').val('');
        }
    }
}
function creditTransaction(tid){ 
	//hide already credit message error
	$('#tra-err-msg').hide();
     $.ajax({
        type: 'POST',
        data: {transaction_id:tid},
        url: '/check-transaction',
        success: function(response)
        {  
		 var jsonObj = jQuery.parseJSON(response);
            if (!checkUserAuthenticationAjx(response)) {
				//show already credit message error
                return false;
            } else {
				if(jsonObj.status == "success"){
					$.ajax({
					type: 'POST',
					data: {transaction_id:tid,user_id:parent.user_id,note:$("#notes").val()},
					url: '/credit-user-transaction',
					success: function(responseData){
						var jsonCreditObj = jQuery.parseJSON(responseData);
						if(jsonCreditObj.status == "success"){
							parent.$("#listDonar").trigger("reloadGrid", [{current: true}]);
							parent.$.colorbox.close();
							
							//location.reload(); 
						}else{
							return false;
						}
					}
					});
				}else{
					$('#tra-err-msg').show();
					return false;
				}
			}
        }
    });
}
