function goToProfile(){window.location.href="/profile"}function remove_dynamic_contacts(classname,i){var plusEle=$(
classname).find(".add-field");$(classname).remove();var classDivId=classname.substring(1,classname.lastIndexOf("_")+1),
lastBlock=$("div[id^='"+classDivId+"']").last();if($(lastBlock).find(".add-field").length==0){var plusId=$(plusEle).attr
("id");$(lastBlock).find(".addMinus").append('<input type="button" value="+" class="button green add-field" id="'+plusId
+'">')}var allDivBlock=$("div[id^='"+classDivId+"']");if(allDivBlock.length==1)$(lastBlock).find(".minus-field").remove(
)}$(function(){$("#period_from0").datepicker({changeMonth:true,changeYear:true,onClose:function(selectedDate){$(
"#period_to0").datepicker("option","minDate",selectedDate)}});$("#period_to0").datepicker({changeMonth:true,changeYear:
true,onClose:function(selectedDate){$("#period_from0").datepicker("option","maxDate",selectedDate)}});$(
".company_name_auto_search").autocomplete({afterAdd:true,selectFirst:true,autoFocus:true,source:function(request,
response){$.ajax({url:"/get-company",method:"POST",dataType:"json",data:{company_name:$("#comp_name0").val()},success:
function(jsonResult){var resultset=[];$.each(jsonResult,function(){var id=this.company_id,value=this.company_name;
resultset.push({id:this.company_id,value:this.company_name})});response(resultset)}})},open:function(){$(".ui-menu").
width(350)},change:function(event,ui){if(ui.item){$("#comp_name0").val(ui.item.value);$("#company_id0").val(ui.item.id)}
},focus:function(event,ui){$("#company_id0").val(ui.item.id);return false},select:function(event,ui){$("#comp_name0").
val(ui.item.value);$("#company_id0").val(ui.item.id);return false}});$("#user_demographic").validate({submitHandler:
function(){var dataStr=$("#user_demographic").serialize();showAjxLoader();$.ajax({type:"POST",url:
"/edit-user-demographics-info/"+$("#user_id").val(),data:dataStr,success:function(responseData){hideAjxLoader();var 
jsonObj=jQuery.parseJSON(responseData);if(jsonObj.status=="success"){$("#tab-4").click();location.href=
"#topOfProfilePage"}else $.each(jsonObj.message,function(i,msg){$("#"+i).after(
'<label class="error" style="display:block;">'+msg+"</label>")})}})}})})