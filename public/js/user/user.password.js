$(function(){
    var LOWER=/[a-z]/,DIGIT=/[0-9]/,DIGITS=/[0-9].*[0-9]/,SPECIAL=/[^a-zA-Z0-9]/,SAME=/^(.)\1+$/;
    function 
    rating(rate,message){
        return{
            rate:rate,
            messageKey:message
        }
    }
    function uncapitalize(str){
    return str.substring(0,1).
    toLowerCase()+str.substring(1)
    }
    $.validator.passwordRating=function(password,username){
    if(!password||password.length<6)
        return rating(0,"too-short");
    if(SAME.test(password))return rating(1,"very-weak");
    var lower=LOWER.test(password),digit=
    DIGIT.test(password);
    if(lower&&digit)return rating(4,"strong");
    if(lower)return rating(3,"good");
    return rating(2,"weak")
    }
;
$.validator.passwordRating.messages={
    "too-short":"Too short",
    "very-weak":"Very weak",
    weak:"Weak",
    good:"Good",
    strong:
    "Strong"
};

$.validator.addMethod("passwordstrength",function(value,element,usernameField){
    var password=element.value,
    username=$(typeof usernameField!="boolean"?usernameField:[]),rating=$.validator.passwordRating(password,username.val()),
    meter=$(".password-meter",element.form);
    meter.find(".password-meter-bar").removeClass().addClass("password-meter-bar").
    addClass("password-meter-"+rating.messageKey);
    meter.find(".password-meter-message").removeClass().addClass(
        "password-meter-message").addClass("password-meter-message-"+rating.messageKey).text($.validator.passwordRating.messages
        [rating.messageKey]);
    return rating.rate>2
    },"Please enter valid passwoed");
$.validator.classRuleSettings.password={
    password:true
};

$.validator.addMethod("alphanumeric",function(value,element){
    return this.optional(element)||
    /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,25}$/.test(value)
    },PASSWORD_INVALID);
var validator=$("#changePassword").validate({
    submitHandler:function(){
        var dataStr=$("#changePassword").serialize();
        showAjxLoader();
        $.ajax({
            type:"POST",
            url:
            "/change-crm-user-password",
            data:dataStr,
            success:function(responseData){
                var jsonObj=jQuery.parseJSON(responseData);
                if(
                    jsonObj.status=="success")parent.location.reload();else $.each(jsonObj.message,function(i,msg){
                    $("#"+i).after(
                        '<label class="error" style="display:block;">'+msg+"</label>")
                    });
                hideAjxLoader()
                }
            })
    },
ignore:"",
rules:{
    password:{
        required
        :true,
        minlength:6,
        maxlength:25,
        alphanumeric:true,
        passwordstrength:true
    },
    password_conf:{
        required:true,
        passwordstrength:
        false,
        equalTo:"#changePassword #password"
    }
},
messages:{
    password:{
        required:EMPTY_PASSWORD
    },
    password_conf:{
        required:
        EMPTY_CON_PASSWORD,
        equalTo:VALID_CON_PASSWORD
    }
}
})
})