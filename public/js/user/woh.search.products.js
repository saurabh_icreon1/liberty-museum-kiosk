var flag_close=1;
$(document).delegate("#cboxCloseMain","click",function(event){
    flag_close=2;
    jQuery.noConflict();
    parent.
    jQuery.colorbox.close();
    return false
    });
jQuery(document).ready(function(){
    var originalClose=jQuery.colorbox.close;
    jQuery.
    colorbox.close=function(){
        if(flag_close==1)jQuery.ajax({
            type:"POST",
            url:"/cart-details",
            async:false,
            success:function(
                responseData){
                parent.$("#header_card").show();
                parent.jQuery("#cart-details-div").html(responseData)
                }
            });
    originalClose();
    if(flag_close==1)parent.jQuery.
        colorbox({
            width:"700px",
            height:"285px",
            inline:true,
            href:"#addedToCartWoh",
            onOpen:function(){
                showAjxLoader()
                }
            });
if(
    flag_close==2)$(this).colorbox.close();
    hideAjxLoader();
    return false
    }
});
function showLoaderCart(){
    jQuery.noConflict();
    parent.jQuery("#cboxClose").click();
    jQuery.ajax({
        type:"POST",
        url:"/cart-details",
        async:false,
        success:function(
            responseData){
            parent.$("#header_card").show();
            parent.jQuery("#cart-details-div").html(responseData)
            }
        })
}
$(document).delegate("#addedToCartWohClose",
    "click",function(){
        flag_close=2;
        parent.jQuery.colorbox.close()
        });
$(function(){
    $("#bio_certificate_woh").validate({
        submitHandler:function(){
            showAjxLoader();
            $.ajax({
                type:"POST",
                data:$("#bio_certificate_woh").serialize(),
                url:
                "/add-to-cart-woh-products",
                success:function(response){
                    var jsonObj=jQuery.parseJSON(response);
                    if(jsonObj.status==
                        "success"){
                        jQuery.noConflict();
                        jQuery.colorbox.close();
                        showLoaderCart()
                        }
                        hideAjxLoader();
                    return false
                    }
                })
        },
    rules:{
        bio_certificate_qty:{
            required:true,
            number:true
        }
    },
    messages:{
        bio_certificate_qty:{
            required:
            WOH_BIO_CERTIFICATE_ERROR_NUMBER_OF_QUANTITY,
            number:WOH_BIO_CERTIFICATE_ERROR_NUMBER_OF_QUANTITY_ONLY_NUMBER
        }
    }
})
});
function addToCartWohProducts(productid,wohid,inforeq,prodtype,numQty){
    showAjxLoader();
    $(
        'label[for="bio_certificate_qty"]').remove();
    var flag_c=true;
    if(inforeq=="2"&&(prodtype=="1"||prodtype=="2"))if($.trim(
        numQty)==""){
        $('label[for="bio_certificate_qty"]').remove();
        $("#bio_certificate_qty").after(
            '<label for="bio_certificate_qty" class="error">'+WOH_BIO_CERTIFICATE_ERROR_NUMBER_OF_QUANTITY+"</label>");
        flag_c=false;
        hideAjxLoader()
        }else if(/^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(numQty)==false){
        $(
            'label[for="bio_certificate_qty"]').remove();
        $("#bio_certificate_qty").after(
            '<label for="bio_certificate_qty" class="error">'+WOH_BIO_CERTIFICATE_ERROR_NUMBER_OF_QUANTITY_ONLY_NUMBER+"</label>");
        flag_c=false;
        hideAjxLoader()
        }
        if(flag_c==true){
        showAjxLoader();
        $.ajax({
            type:"POST",
            data:{
                productid:productid,
                wohid:wohid,
                inforeq:inforeq,
                prodtype:prodtype,
                numQty:numQty
            },
            url:"/add-to-cart-woh-products",
            success:function(response){
                var jsonObj=
                jQuery.parseJSON(response);
                if(jsonObj.status==="success"){
                    jQuery.noConflict();
                    jQuery.colorbox.close();
                    showLoaderCart();
                    hideAjxLoader()
                    }
                }
        })
}
}
function changeProductQuantity(obj,act){
    if(act=='add') {   
                obj.value = parseInt(obj.value) + 1;
                $("#minusQty1").show();
    }
    else {
        if(parseInt(obj.value) >= 2) {
                obj.value = parseInt(obj.value) - 1;
        }
        if(parseInt(obj.value)==1) {
            $("#minusQty1").hide();
        }
    }
}