$(function(){$("#from").datepicker({changeMonth:true,changeYear:true,onClose:function(selectedDate){$("#to").datepicker(
"option","minDate",selectedDate)}});$("#to").datepicker({changeMonth:true,changeYear:true,onClose:function(selectedDate)
{$("#from").datepicker("option","maxDate",selectedDate)}});$("#save_search").click(function(){$("#success_message").hide
()});$("#save_search").validate({submitHandler:function(){showAjxLoader();$.ajax({type:"POST",data:{search_name:$.trim($
("#search_name").val()),searchString:$("#search").serialize(),search_id:$("#search_id").val()},url:
"/save-crm-user-assignment-search",success:function(response){var jsonObj=jQuery.parseJSON(response);if(jsonObj.status==
"success"){$("#search_name").val("");$("#search_msg").removeClass("error");$("#search_msg").addClass("success-msg clear"
);if($("#search_id").val()==""){$("#search_msg").show();$("#search_msg").html(SAVE_SUCCESS_MSG)}document.getElementById(
"search").reset();if($("#search_id").val()!="")location.reload();else generateSavedSearch()}else{$("#search_msg").show()
;$("#search_msg").html(ERROR_SAVING_SEARCH)}hideAjxLoader()}})},rules:{search_name:{required:true}},messages:{
search_name:{required:"Please enter search name"}}});$.jgrid.no_legacy_api=true;$.jgrid.useJSON=true;var grid=jQuery(
"#assignments-list"),emptyMsgDiv=$('<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");$("#assignments-list").jqGrid
({postData:{searchString:$("#search").serialize()},mtype:"POST",url:"/crm-user-assignment-search",datatype:"json",
sortable:true,colNames:["Source","Type","Subject","With","Scheduled On","Action(s)"],colModel:[{name:"Source",index:
"source"},{name:"Type",index:"activity_type",sortable:true},{name:"Subject",index:"subject"},{name:"With",index:
"user_with"},{name:"Scheduled On",index:"scheduled_date"},{name:"Action(s)",index:"action",sortable:false}],sortname:
"scheduled_date",sortorder:"DESC",rowNum:10,rowList:[10,20,30],pager:"#pcrud",viewrecords:true,autowidth:true,
shrinkToFit:true,caption:"",width:"100%",cmTemplate:{title:false},loadComplete:function(){hideAjxLoader();var count=grid
.getGridParam(),ts=grid[0];if(ts.p.reccount===0){grid.hide();emptyMsgDiv.show();$("#pcrud_right div.ui-paging-info").css
("display","none")}else{grid.show();emptyMsgDiv.hide();$("#pcrud_right div.ui-paging-info").css("display","block")}}});
emptyMsgDiv.insertAfter(grid.parent());emptyMsgDiv.hide();$("#assignments-list").jqGrid("navGrid","#pcrud",{reload:true,
edit:false,add:false,search:false,del:false});$("#assignments-list").jqGrid("navButtonAdd","#pcrud",{caption:"",title:
"Export",id:"exportExcel",onClickButton:function(){exportExcel("assignments-list","/crm-user-assignment-search")},
position:"last"});$("#with").autocomplete({afterAdd:true,selectFirst:true,autoFocus:true,source:function(request,
response){$.ajax({url:"/get-contacts",method:"POST",dataType:"json",data:{user_email:$("#with").val()},success:function(
jsonResult){console.log(jsonResult);var resultset=[];$.each(jsonResult,function(){var id=this.user_id,value=this.
full_name;resultset.push({id:this.user_id,value:this.full_name})});response(resultset)}})},open:function(){$(".ui-menu")
.width(350)},change:function(event,ui){if(ui.item){$("#with").val(ui.item.value);$("#user_id").val(ui.item.id)}else $(
"#with").val("")},focus:function(event,ui){return false},select:function(event,ui){$("#with").val(ui.item.value);$(
"#user_id").val(ui.item.id);return false}});$("#assigned").autocomplete({afterAdd:true,selectFirst:true,autoFocus:true,
source:function(request,response){$.ajax({url:"/get-crm-contacts",method:"POST",dataType:"json",data:{crm_full_name:$(
"#assigned").val()},success:function(jsonResult){var resultset=[];$.each(jsonResult,function(){var id=this.crm_user_id,
value=this.crm_full_name;resultset.push({id:this.crm_user_id,value:this.crm_full_name})});response(resultset)}})},open:
function(){$(".ui-menu").width(300)},change:function(event,ui){if(ui.item){$("#assigned").val(ui.item.value);$(
"#assigned_id").val(ui.item.id)}else{$("#assigned").val("");$("#assigned_id").val("")}$(".ui-autocomplete").jScrollPane(
)},focus:function(event,ui){$("#assigned_id").val(ui.item.id);return false},select:function(event,ui){$("#assigned").val
(ui.item.value);$("#assigned_id").val(ui.item.id);return false}})});$("#submitbutton").click(function(){showAjxLoader();
jQuery("#assignments-list").jqGrid("setGridParam",{postData:{searchString:$("#search").serialize()}});jQuery(
"#assignments-list").trigger("reloadGrid",[{page:1}]);window.location.hash="#save_search";hideAjxLoader();return false})
;function getSavedSearchResult(id){$("#search_msg").hide();if(id!=""){$("#search_id").val(id);$("#savebutton").val(
"Update");$("#deletebutton").show();$('label[for="save_search_as"]').hide();$("#search_msg").addClass(
"success-msg clear");showAjxLoader();$.ajax({type:"POST",data:{search_id:id},url:"/get-crm-user-assignment-search-info",
success:function(response){var searchParamArray=response,obj=jQuery.parseJSON(searchParamArray);document.getElementById(
"search").reset();$("#subject").val(obj.subject);$("#source").val(obj.source).select2({minimumResultsForSearch:-1});$(
"#source").val(obj.source).select2({minimumResultsForSearch:-1});$("#activity_type").val(obj.activity_type).select2({
minimumResultsForSearch:-1});$("#scheduled").val(obj.scheduled).select2({minimumResultsForSearch:-1});$("#assigned").val
(obj.assigned);$("#assigned_id").val(obj.assigned_id);$("#with").val(obj.with);$("#user_id").val(obj.user_id);$("#from")
.val(obj.from);$("#to").val(obj.to);$("#search_name").val(obj.search_title);$("#submitbutton").click();hideAjxLoader()}}
)}else{document.getElementById("search").reset();$("#saved_search").val("");$("#search_id").val("");$("#savebutton").val
("Save");$("#deletebutton").hide()}}function deleteCrmUserSearch(){$(".delete_crm_user_search").colorbox({width:"700px",
height:"180px",inline:true});$("#no_delete_search").click(function(){$.colorbox.close();$("#yes_delete_search").unbind(
"click");return false});$("#yes_delete_search").click(function(){showAjxLoader();$.ajax({type:"POST",data:{search_id:$(
"#search_id").val()},url:"/delete-crm-user-assignment-search",success:function(response){var jsonObj=jQuery.parseJSON(
response);if(jsonObj.status=="success")location.reload();hideAjxLoader()}})})}function generateSavedSearch(){$(
"#saved_search option").each(function(){$(this).remove()});$.ajax({type:"POST",data:{},url:
"/get-crm-user-assignment-save-search",success:function(response){$("#saved_search").append(response)}})}function 
showFromTo(val){if(val=="1")$("#date_div").show();else $("#date_div").hide()}