$(function(){
    $('#description').limiter(1000, $("#description_count"));
    
    $("#contactus").validate({
        submitHandler:function(){ 
			showAjxLoader();
            var dataStr=$("#contactus").serialize();
            $.ajax({
                type:
                "POST",
                data:dataStr,
                url:"/contactus",
                success:function(responseData){
                    var jsonObj=jQuery.parseJSON(responseData);
					hideAjxLoader();
                    if(
                        jsonObj.status==="success-msg"){						
						$("#submitbutton").attr('disabled','disabled');            
                        $("#success-msg").show();
                        $("#error-msg").hide();
                        $("#first_name").val("");
                        $("#last_name")
                        .val("");
                        $("#case_type").val("");
                        $("#email_id").val("");
                        $("#phone").val("");
                        $("#phone").val("");
                        $("#description").val(""
                            );
                        $("#captcha").val("");
                        $("#description_count").html('0');
                        }else if(jsonObj.status==="error-msg"){
                        $("#error-msg").show();
                        $("#success-msg").hide();
                        $(
                            "#emailidmsg").html(jsonObj.email)
                        }else if(jsonObj.status==="error"){
                        $(".detail-section").css("display","block");
                        $.each(
                            jsonObj.message,function(i,msg){
                                $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>");
                                $(
                                    "#error-msg").hide();
                                $("#success-msg").hide()
                                })
                        }
                    }
            })
    },
    rules:{
        first_name:{
            required:true
        },
        last_name:{
            required:true
        },
        case_type:{
            required:true
        },
        email_id:{
            required:true
        },
        description:{
            required:true,
            maxlength:1000
        }
    },
messages:{
    first_name:{
        required:
        "Please enter first name"
    },
    last_name:{
        required:"Please enter last name"
    },
    case_type:{
        required:"Please select category"
    },
    email_id:{
        required:"Please enter email"
    },
    description:{
        required:"Please enter description"
    }
}
})
});
function deleteRole(id){
    $(".delete_role").colorbox({
        width:"700px",
        height:"180px",
        inline:true
    });
    $("#no_delete_role").click(function(){
        $.colorbox.
        close();
        $("#yes_delete_role").unbind("click");
        return false
        });
    $("#yes_delete_role").click(function(){
        $.ajax({
            url:
            "/delete-crm-user-role",
            method:"POST",
            dataType:"json",
            data:{
                role_id:id
            },
            success:function(jsonResult){
                if(!
                    checkUserAuthenticationAjx(jsonResult))return false;
                location.reload()
                }
            })
    })
}
function addUserCase(value){
    $.colorbox({
        width
        :"930px",
        height:"600px",
        iframe:true,
        href:"/add-user-case/"+value
        })
    }
    function refreshCaptcha(){
    showAjxLoader();
    $.ajax({
        type:"POST",
        url:"/refresh-captcha",
        data:{},
        success:function(responseData){
            hideAjxLoader();
            var jsonObj=jQuery.parseJSON(
                responseData);
            if(jsonObj.status=="success"){
                $("#captcha-hidden").val(jsonObj.captchaId);
                $("#captcha-image").attr("src",
                    jsonObj.path+""+jsonObj.captchaId+".png")
                }
            }
    })
}