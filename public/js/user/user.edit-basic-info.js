

/**
* function is used to start ajax loader
* @params void
* @return void
* @author Icreon Tech - NS
*/  
function showEditInfoAjxLoader(target) {   
    var tartop = target.position().top; //$(target).parent().closest('div.row').position().top;
    var tarleft = target.position().left; //$(target).parent().closest('div.row').position().left;
    var tarwidth = parseInt(target.width())+parseInt(20);
    var tarheight = parseInt(target.height())+parseInt(20);
   // alert(tarheight);
    $(".main-loader-editinfo").css({display:'block',top:tartop,left:tarleft,position:'absolute',width:tarwidth+'px',height:tarheight+'px'});
}

/**
* function is used to hide  ajax loader
* @params void
* @return void
* @author Icreon Tech - NS
*/
function hideEditInfoAjxLoader() {
    $(".main-loader-editinfo").css({display:'none'});
}

function goToProfile(){
    window.location.href = '/profile';
}


$(function() {
    $("select").select2();
    var url = '/upload-user-pic/'+$("#contact_id").val();
    $('#upload_profile_image').fileupload({
        url: url,
        dataType: 'json',
        done: function (e, data) {
            data=data.result;
            if(data.status=='success') {
                $("#filemessage").removeClass("");
                $("#filemessage").html("");
                $("#filemessage").removeClass('error_message');
                $("#upload_profile_image_id").val(data.filename);     
                $("#thumbnail").attr('src', data.assets_url+data.filename);                         
                $('input[name="submitbutton1"][type="submit"]').click();
                $('#remove_profile_image').css({'display':'block'});
                $('#upload_profile_image_link').text('Change Photo');
            }
            else
            {
                $("#filemessage").removeClass('success_message');
                $("#filemessage").addClass('error_message');
                $("#filemessage").html('<label class="error">'+data.message+'</label>');
                $("#upload_profile_image_id").val('');
                $('#remove_profile_image').css({'display':'none'});   
                $('#upload_profile_image_link').text('Upload Photo');
            }            
        },
    start : function (e) {
           $("div.field-detail").hide(); 
           showEditInfoAjxLoader($("div.left-side"));
        },
        stop : function(e){
           hideEditInfoAjxLoader();
        }
    });
    

   // newly added - start 
       
       $('.row').mouseover(function(){
           $(this).find(".editprofile").show();
       });
       
       $('.row').mouseout(function(){
           $(this).find(".editprofile").show();
       });
       

        
    
        $('.editprofile').click(function(){
            hideEditInfoAjxLoader();
            $('form')[0].reset();
            $("div.field-detail").hide();
            $(this).hide();
            $(this).parent().closest('div.row').find("div.field-detail").show();
        }); 

        $('#upload_profile_image_link').click(function(){
           $('#upload_profile_image').trigger('click');
        }); 

        $('.cancelsave').click(function(){ 
            $("select").select2('close');
            var objCancelSave = this;
            $('form#'+$(objCancelSave).parent().closest('form').attr("id"))[0].reset();
            $('form#'+$(objCancelSave).parent().closest('form').attr("id")+' label.error').remove();
            showEditInfoAjxLoader($(objCancelSave).parent().closest('div.row'));
            $('div.left-side > div.main-loader-editinfo').css({"display":"none"});
            $('form')[0].reset();
            setTimeout(function(){  
                 $(objCancelSave).parent().closest('div.row').find("div.field-detail").hide(); 
                 hideEditInfoAjxLoader(); 
            },2000);
        }); 

        $('#remove_profile_image').click(function(){
             showEditInfoAjxLoader($("div.left-side"));
            // $('form')[0].reset();
             $("div.field-detail").hide();
             $('#upload_profile_image_id').val('');
             //$("#edit_basic_info").submit();
             $('#remove_profile_image').css({'display':'none'});  
             $("#submitbutton1").click();
             $('#upload_profile_image_link').text('Upload Photo');
        });
   // newly added - end
   

    $( "#conf_email_id" ).blur(function() {
        if($("#conf_email_id").val()!=''){
            if($("#email_id").val()!=$("#conf_email_id").val()){
                $("#confrm_email_green").hide();
                $("#confrm_email_red").show();
            }else{
                $("#confrm_email_green").show();
                $("#confrm_email_red").hide();
            }
        }
    });
    $( "#conf_email_id" ).focus(function() {
        if($("#conf_email_id").val()!=''){
            if($("#email_id").val()!=$("#conf_email_id").val()){
                $("#confrm_email_green").hide();
                $("#confrm_email_red").show();
            }else{
                $("#confrm_email_green").show();
                $("#confrm_email_red").hide();
            }
        }
    });

    jQuery.validator.addMethod("lettersandapostropheonlydot", function(value, element) {
        return this.optional(element) || /^[a-z '.]+$/i.test(value);
    },INVALID_NAME);
    
    jQuery.validator.addMethod("lettersandapostropheonly", function(value, element) {
        return this.optional(element) || /^[a-z '\-]+$/i.test(value);
    },INVALID_NAME);
    
    jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z ]+$/i.test(value);
    },INVALID_NAME);
    
    jQuery.validator.addMethod("alphanumericspecialchar", function(value, element) {
        return this.optional(element) || /^[A-Za-z0-9\r\s\n ,.-]+$/i.test(value);
    },"Enter valid details");
    
   jQuery.validator.addMethod("alphanumericspecialcharandapostrophe", function(value, element) {
        return this.optional(element) || /^[A-Za-z0-9'\r\s\n ,.-]+$/i.test(value);
    },"Enter valid details");
    
    
   $('input[type="submit"]').click(function(){
       
    var objTT = this;
    
    if($(objTT).attr('name') == 'submitbutton1' || $(objTT).attr('name') == 'submitbutton2' || $(objTT).attr('name') == 'submitbutton3' || $(objTT).attr('name') == 'submitbutton4' || $(objTT).attr('name') == 'submitbutton5') {
    
    $("#edit_basic_info").validate({        
        submitHandler: function() {
            var dataStr = $('#edit_basic_info').serialize();
         
             showEditInfoAjxLoader($(objTT).parent().closest('div.row'));
             $('div.left-side > div.main-loader-editinfo').css({"display":"none"});

            $.ajax({
                type: "POST",
                url: '/edit-basic-info/'+$("#contact_id").val(),
                data: dataStr,
                success: function(responseData){
                     hideEditInfoAjxLoader();
                    var jsonObj =  jQuery.parseJSON(responseData);
                    if(jsonObj.status =='success'){  
                        var jsondataObj =  jQuery.parseJSON(jsonObj.jsondata);
                        
                        $('#corporate_company_name').attr("value",jsondataObj.corporate_company_name);
                        $('#corporate_legal_name').attr("value",jsondataObj.corporate_legal_name);
                        $('#corporate_sic_code').attr("value",jsondataObj.corporate_sic_code);
                        $('#upload_profile_image_id').attr("value",jsondataObj.upload_profile_image_id);                        
                        $('#title').attr("value",jsondataObj.title);
                        $('#first_name').attr("value",jsondataObj.first_name);
                        $('#middle_name').attr("value",jsondataObj.middle_name);
                        $('#last_name').attr("value",jsondataObj.last_name);
                        
                        if(jsondataObj.upload_profile_image_id.length > 14) {
                            var split_image = jsondataObj.upload_profile_image_id.split(".");                            
                           $('#viewProfileImageName').html(split_image[0].substring(0,11)+"."+split_image[1]);  
                        }
                        else {
                           $('#viewProfileImageName').html(jsondataObj.upload_profile_image_id);
                        }

        
                     
                        if(jsondataObj.corporate_company_name != "") { 
                          $('#viewCompanyName').html(jsondataObj.corporate_company_name);
                        }
                        else {
                          $('#viewCompanyName').html("&nbsp;");
                        }

                        if(jsondataObj.corporate_legal_name != "") {
                           $('#viewLegalName').html(jsondataObj.corporate_legal_name);
                        }
                        else {
                           $('#viewLegalName').html("&nbsp;"); 
                        }
                        
                        if(jsondataObj.corporate_sic_code != "") {
                           $('#viewSicCode').html(jsondataObj.corporate_sic_code);
                        }
                        else {
                           $('#viewSicCode').html("&nbsp;");  
                        }
                        
                        var nameView = '';
                        if($.trim(jsondataObj.title) != "") { nameView += jsondataObj.title+' '; }
                        if($.trim(jsondataObj.first_name) != "") { nameView += jsondataObj.first_name+' '; }
                        if($.trim(jsondataObj.middle_name) != "") { nameView += jsondataObj.middle_name+' '; }
                        if($.trim(jsondataObj.last_name) != "") { nameView += jsondataObj.last_name+' '; }                        
                        $('#viewInfoName').html(nameView);
                         
                        $('#imageFileName').attr("src",jsondataObj.fileImageName);
                        
                        $("div.field-detail").hide();
                        return false;
                    } else {
                        $.each(jsonObj.message, function (i, msg) {
                            $("#"+i).after('<label class="error" style="display:block;">'+msg+'</label>');                                    
                        });
                    }
                }
            });
        },
        rules: {
            first_name: {
                required: function() {
                    if (userType==1) {
                        return true;
                    } else {
                        return false;
                    }
                },
                minlength: 1,
                maxlength :50,
                lettersandapostropheonly:true
            },
            corporate_company_name: {
                required: function() {
                    if (userType==2) {
                        return true;
                    } else {
                        return false;
                    }
                },
                minlength: 2,
                maxlength :50,
                alphanumericspecialcharandapostrophe:true
            },
            corporate_legal_name: {
                required: function() {
                    if (userType==2) {
                        return true;
                    } else {
                        return false;
                    }
                },
                minlength: 2,
                maxlength :50,
                alphanumericspecialcharandapostrophe:true
            },
            corporate_sic_code: {
                minlength: 2,
                maxlength :50,
                alphanumericspecialcharandapostrophe:true
            },
            middle_name: {
                minlength: 1,
                maxlength :4,
                lettersandapostropheonlydot:true
            },
            last_name: {
                required: function() {
                    if (userType==1) {
                        return true;
                    } else {
                        return false;
                    }
                },
                minlength: 1,
                maxlength :20,
                lettersandapostropheonly:true
            },
            email_id: {
                required: true,
                email: true
            },
            conf_email_id: {
                required: true,
                equalTo: "#edit_basic_info #email_id"
            }
//            country_id: {
//                required: true
//            },
//            postal_code: {
//                digits: true,
//                minlength: 4,
//                maxlength :8
//            }
        },
        messages: {
            first_name: {
                required: EMPTY_FIRST_NAME
            },
            last_name: {
                required: EMPTY_LAST_NAME
            },
            corporate_company_name: {
                required: COMPANY_NAME_EMPTY
            },
            corporate_legal_name: {
                required: LEGAL_NAME_EMPTY
            },
            email_id: {
                required: EMPTY_EMAIL_ID,
                email: INVALID_EMAIL
            },
            email_id_conf: {
                required: EMPTY_CONFIRM_EMAIL_ID,
                equalTo : CONFIRM_EMAIL_ID_NOT_MATCH
            }
//            country_id: {
//                required: EMPTY_COUNTRY
//            }
        }
     });
     }
   });  
});

function editPassword(){
    authenticateUserBeforeAjax();
    $.colorbox({
        width:"700px", 
        height:"450px",
        iframe : true,        
        onClosed:function (){
            location.href="#topOfProfilePage"
        },
        href:"/edit-user-password/"+$("#contact_id").val()
    });
}
function editQuestion(){
    authenticateUserBeforeAjax();
    $.colorbox({
        width:"700px", 
        height:"450px",
        iframe : true,
        onClosed:function (){
            location.href="#topOfProfilePage"
        },
        href:"/edit-security-question/"+$("#contact_id").val()
    });
}
function editEmail(){
    authenticateUserBeforeAjax();
    $.colorbox({
        width:"700px", 
        height:"450px",
        iframe : true,
        onClosed:function (){
            location.href="#topOfProfilePage"
        },
        href:"/edit-email-info/"+$("#contact_id").val()
    });
}


/////////////////////////////////////////////////////////  Start ///////////////////////////////////////////////////////////////////////////////////

$(function() {
          // Email  -start
          
        $('#edituseremailinfo #submitbutton').click(function(){ 
           $('#edituseremailinfo label[for="email_id"]').remove();
        });

        $("#edituseremailinfo").validate({
            submitHandler: function() {
                var dataStr = $('#edituseremailinfo').serialize();                    
              //  showAjxLoader();
              showEditInfoAjxLoader($('#fieldDetailEdituseremailinfo'));
              $('div.left-side > div.main-loader-editinfo').css({"display":"none"});
                $.ajax({
                    type: "POST",
                    url: '/edit-email-info/'+$("#contact_id").val(),
                    data: dataStr,
                    dataType: "html",
                    success: function(responseData){
                      //  hideAjxLoader();
                        var jsonObj =  jQuery.parseJSON(responseData);
                        if(jsonObj.status =='success'){             
                            $('#success_message').css({"display":"block"});
                            $('#success_message > div.success_message').html(jsonObj.message);
                            setTimeout(function() {  $('#success_message').css({"display":"none"}); },8000);
                            $('#edituseremailinfo #email_id').val("");
                            $('#edituseremailinfo #email_id_conf').val("");
                            $('#edituseremailinfo #password').val("");
                            $('#edituseremailinfo #confirm_security_answer').val("");
                             //$("#tab-1").click();
                             //location.href="#topOfProfilePage";
                             $("div.field-detail").hide();
                        }else if (jsonObj.status =='error' && jsonObj.result){
                            $('#edituseremailinfo label[for="email_id"]').remove();
                            $("#edituseremailinfo #email_id").after('<label for="email_id" class="error" style="display:block;">'+jsonObj.message+'</label>');
                        }else{
                            $.each(jsonObj.message, function (i, msg) {
                                $("#edituseremailinfo #"+i).after('<label class="error" style="display:block;">'+msg+'</label>');                                    
                            });
                        }
                        hideEditInfoAjxLoader(); 
                    }
                });
            },
            rules: {
                email_id: {
                    email : true,
                    required: true
                },
                email_id_conf: {
                    required: true,
                    equalTo: "#edituseremailinfo #email_id"
                },
                password: {
                    required: true
                },
                confirm_security_answer: {
                    required: function (){
                        if($.trim($('#jsSecurityQuestion').val()) == '')
                            return false;
                        else
                            return true;    
                    }
                }
            },
            messages: {
                email_id: {
                    email : INVALID_EMAIL,
                    required: EMPTY_EMAIL
                },
                email_id_conf: {
                    required: EMPTY_CONFIRM_EMAIL,
                    equalTo: CONFIRM_EMAIL_ID_NOT_MATCH
                },
                password: {
                    required: EMPTY_PASSWORD
                },
                confirm_security_answer: {
                    required: EMPTY_SECURITY_ANSWER
                }
            }
        });
         // Email  -end
        
        
        // Question -start
        $("#editusersecurityquestion").validate({
            submitHandler: function() {
                var dataStr = $('#editusersecurityquestion').serialize();                    
               // showAjxLoader();
               showEditInfoAjxLoader($('#fieldDetailEditusersecurityquestion'));
               $('div.left-side > div.main-loader-editinfo').css({"display":"none"});
                $.ajax({
                    type: "POST",
                    url: '/edit-security-question/'+$("#contact_id").val(),
                    data: dataStr,
                    success: function(responseData){
                       // hideAjxLoader();
                        var jsonObj =  jQuery.parseJSON(responseData);
                        if(jsonObj.status =='success'){
                            
                            var jsondataObj =  jQuery.parseJSON(jsonObj.jsondata);
                            $('#viewSecurityQuestion').html(jsondataObj.secQName);
                            $('#viewSecurityQuestionAns').html(jsondataObj.secAnsName);
                            
                            $('#jsSecurityQuestion').val(jsondataObj.secQName);
                            $('.textSecurityQuestion').html(jsondataObj.secQName);

                            if($.trim(jsondataObj.secQName) != "") { 
                                $('#secFormSecurityQuestionHeading').html(L_CONFIRM_SECURITY_ANSWER); 
                                $('.displayTextSecurityQuestion').css({"display":"block"}); 
                            }
                            else { 
                                $('#secFormSecurityQuestionHeading').html(L_CONFIRM_PASSWORD_PROCEED);
                                $('.displayTextSecurityQuestion').css({"display":"none"}); 
                            }
                            

                            $("div.field-detail").hide();
                           // $("#tab-1").click();
                           //  location.href="#topOfProfilePage";
                        }else{
                            $.each(jsonObj.message, function (i, msg) {
                                $('label[for="'+i+'"]').remove();
                                $("#editusersecurityquestion #"+i).after('<label class="error" for="'+i+'" style="display:block;">'+msg+'</label>');                                    
                            });
                        }
                         hideEditInfoAjxLoader();
                    }
                });
            },
            rules: {
                security_question: {
                    required: true
                },
                security_answer: {
                    required: true
                },
                password: {
                    required: true
                },
                confirm_security_answer: {
                    required: function (){
                        if($.trim($('#jsSecurityQuestion').val()) == '')
                            return false;
                        else
                            return true;    
                    }
                }
            },
            messages: {
                security_question: {
                    required: EMPTY_SECURITY_QUESTION
                },
                security_answer: {
                    required: EMPTY_SECURITY_ANSWER
                },
                password: {
                    required: EMPTY_PASSWORD
                },
                confirm_security_answer: {
                    required: EMPTY_CONFIRM_SECURITY_ANSWER
                }
            }
        });
          // Question -end
          
          
          // Password - start 
            $.validator.addMethod("password",function(value,element)
                {
                    return this.optional(element) || /^(?=.*\d)(?=.*[a-zA-Z]).{6,25}$/i.test(value);
                },INVALID_PASSWORD_CRITERIA);

                $("#edituserpassword").validate({
                    submitHandler: function() {
                        var dataStr = $('#edituserpassword').serialize();                    
                       // showAjxLoader();
                       showEditInfoAjxLoader($('#fieldDetailEdituserpassword'));
                       $('div.left-side > div.main-loader-editinfo').css({"display":"none"});
                        $.ajax({
                            type: "POST",
                            url: '/edit-user-password/'+$("#contact_id").val(),
                            data: dataStr,
                            success: function(responseData){
                             //   hideAjxLoader();
                                var jsonObj =  jQuery.parseJSON(responseData);
                                if(jsonObj.status =='success'){
                                    
                                  $("#edituserpassword #password").val("");
                                  $("#edituserpassword #password_conf").val("");
                                  $("#edituserpassword #security_answer").val("");
                                   
                                  $("div.field-detail").hide();
                                  //  parent.$("#tab-1").click();
                                  //  parent.jQuery.colorbox.close();
                                    
                                   // $("#tab-1").click();
                                   // location.href="#topOfProfilePage";
                                    //parent.location.hash = "#topOfProfilePage";
                                }else{
                                    $.each(jsonObj.message, function (i, msg) {
                                        $("#edituserpassword #"+i).after('<label class="error" style="display:block;">'+msg+'</label>');                                    
                                    });
                                }
                                
                                  hideEditInfoAjxLoader(); 
                            }
                        });
                    },
                    rules: {
                        password: {
                            required: true,
                            minlength: 6,
                            maxlength :25
                        },
                        password_conf: {
                            required: true,
                            equalTo: "#edituserpassword #password"
                        },
                        security_answer: {
                            required: function (){
                                if($.trim($('#jsSecurityQuestion').val()) == '')
                                    return false;
                                else
                                    return true;    
                            }
                        }
                    },
                    messages: {
                        password: {
                            required: EMPTY_PASSWORD
                        },
                        password_conf: {
                            required: EMPTY_CONFIRM_PASSWORD
                        },
                        security_answer: {
                            required: EMPTY_SECURITY_ANSWER
                        }
                    }
                });
          // Password - end
    });
    
    
/////////////////////////////////////////////////////////  End ///////////////////////////////////////////////////////////////////////////////////    