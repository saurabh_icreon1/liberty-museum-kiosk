function forgotPopup(){
    parent.$.colorbox({
        width:"655px",
        height:"475px",
        iframe:true,
        href:"/forgot"
    })
    }
    function getFBInfo(
    userId,referPath,faceBookId,faceBookEmail,isPopup){
    $("#login-b1").css({
        display:"none"
    });
    $("#login-b2").css({
        display:
        "none"
    });
    $("#login-b3").css({
        display:"none"
    });
    $("#termsAndConditionSection").css({
        display:"block"
    });
    $("form#loginTermsAndCondition input#user_id").val(userId);
    $("form#loginTermsAndCondition input#is_fb").val("1");
    $("form#loginTermsAndCondition input#referpath").val(referPath);
    $("form#loginTermsAndCondition input#facebook_id").val(faceBookId);
    $("form#loginTermsAndCondition input#fb_email_id").val(faceBookEmail);
    $(
        "form#loginTermsAndCondition input#is_popup").val(isPopup)
    }
    $(function(){
        $('input[name="user_name"]').focus();
    if(isLoginPopup=="")$.colorbox({
        width:"920px",
        href:"#login-popup-div",
        height:"540px",
        inline:true,
        onClosed:function(){
            if(isLoginPopup=="")window.history.go(-1)
                }
            });
$("#cboxClose").click(function(){
    $.colorbox.close();
    window.location.href="/"
    });
$("#loginTermsAndCondition").validate({
    submitHandler:function(){
        $("#ajx_loader").css({
            "z-index":"99999999"
        });
        showAjxLoader();
        $.ajax({
            type:"POST",
            url:
            "/update-user-terms-condition",
            data:$("#loginTermsAndCondition").serialize(),
            success:function(responseData){
                var jsonObj=
                jQuery.parseJSON(responseData);
                if(jsonObj.status!=undefined&&$.trim(jsonObj.status)=="success"){
                    $(
                        "form#loginTermsAndCondition input#submitbutton").attr("disabled","disabled");
                    updateLoginDetails();
                    if($.trim($(
                        "form#loginTermsAndCondition input#is_fb").val())=="1")if($.trim($("form#loginTermsAndCondition input#is_popup").val())
                        =="1"){
                        window.parent.$("#closebox").colorbox.close();
                        parent.window.location.href=$.trim($(
                            "form#loginTermsAndCondition input#referpath").val());
                        window.close()
                        }else if($.trim($(
                        "form#loginTermsAndCondition input#is_popup").val())=="2"){
                        updateLoginDetails();
                        window.location.href=$.trim($(
                            "form#loginTermsAndCondition input#referpath").val());
                        window.close()
                        }else parent.window.location.href=$.trim($(
                        "form#loginTermsAndCondition input#referpath").val());else $("#login").submit()
                        }
                    }
        })
}
});
$("#login").validate({
    submitHandler:function(){
        var loginUrl;
        if(refererPath)loginUrl="/login/"+refererPath;else loginUrl="/login";
        var dataStr=$
        ("#login").serialize();
        $.ajax({
            type:"POST",
            url:loginUrl,
            data:dataStr,
            success:function(responseData){
                var jsonObj=jQuery.
                parseJSON(responseData);
                if(jsonObj.status=="success"){
					if(jsonObj.hasOwnProperty('isBookingSystem') && jsonObj.hasOwnProperty('isVisitRecord')){
                               if(jsonObj.isBookingSystem && !jsonObj.isVisitRecord){
                                //window.location.href = "/visitation-record";
                                //$("#login").colorbox.close();
                                parent.$.colorbox({width: "920px", height:"745px", href:"/visitation-record",iframe: true});
								
								parent.$.colorbox.resize({
                                    width: "920px",
                                    height: "900px"
                                });
								 
                               // alert("is visiting record");
                                return ;
                             }
                             if(jsonObj.isBookingSystem && !jsonObj.kioskFeePaid){
                                    parent.window.location.href = "/kiosk-fee";
                                    //alert("kiosk fee");
                                    return ;
                                }
                       }
                    if(jsonObj.flag_status_tacv!=undefined&&jsonObj.flag_status_tacv=="2"){
                       
                        $("#login-b1").css({
                            display:"none"
                        });
                        $("#login-b2").css({
                            display:"none"
                        });
                        $("#login-b3").css({
                            display:"none"
                        });
                        $("#termsAndConditionSection").css({
                            display:"block"
                        });
                        $("form#loginTermsAndCondition input#user_id").val(jsonObj.user);
                        return false
                        }else{
                        $("form#loginTermsAndCondition input#user_id").val("");
                        $("#termsAndConditionSection").css({
                            display:
                            "none"
                        })
                        }
                        $("#ajx_loader").css({
                        display:"block"
                    });
                    $("#ajx_loader").css({
                        "z-index":"99999999"
                    });
                    $("#error_message").html(
                        "");
                    if(jsonObj.path==false)parent.window.location.href=SITE_URL+"/profile";
                    else{
                        updateLoginDetails();
                        var urlArray=jsonObj.path.split("~~");
                        if(urlArray[1]){
                            if(urlArray[0]=="/add-to-cart")parent.addtocart(urlArray[1]);
                            else if(urlArray[0]==
                                "/add-to-wishlist")parent.addtowishlist(urlArray[1])
                                }else if(isRedirectPopup!=""){
                            $.colorbox.resize({
                                width:$(document).
                                width(),
                                height:$(document).height()
                                });
                                
                            window.location.href=urlArray[0];
                            $.colorbox.resize({
                                width:$(document).width(),
                                height:$(document).height()
                                })
                            }else parent.window.location.href=urlArray[0]
                            }
                        }else if(jsonObj.is_active==false)
                            parent.window.location.href="/verify-email/"+jsonObj.id;
            else if(jsonObj.is_email_verified==false)window.location.href="/update-email/"
                +jsonObj.id;else $("#error_message").html(jsonObj.message)
                }
            });
$("#ajx_loader").css({
    "z-index":""
});
hideAjxLoader()
    },
rules:{
    user_name:{
        required:true
    },
    password:{
        required:true
    }
},
messages:{
    user_name:{
        required:USERNAME_EMPTY
    },
    password:{
        required:PASSWORD_EMPTY
    }
}
})
});
function callPopUp(val1,val2,val3,val4,val5){
    parent.ViewRelatedProducts(val1,val2,val3,
        val4,val5)
    }
    function updateLoginDetails(){
    $.ajax({
        type:"POST",
        url:"/login-details",
        async:false,
        success:function(
            responseData){
            parent.$("#welcome-div").html(responseData);
            parent.parent.$("#welcome-div").html(responseData);
            parent.$(
                "#menusignup").html('<a href="/profile">My File</a>');
            parent.parent.$("#menusignup").html(
                '<a href="/profile">My File</a>');
            parent.$("#menulogin").remove();
            parent.parent.$("#menulogin").remove()
            }
        });
$.ajax({
    type
    :"POST",
    url:"/cart-details",
    async:false,
    success:function(responseData){
        parent.$("#cart-details-div").html(responseData);
        parent.parent.$("#cart-details-div").html(responseData);
        parent.$("#menusignup").html('<a href="/profile">My File</a>');
        parent.parent.$("#menusignup").html('<a href="/profile">My File</a>');
        parent.$("#menulogin").remove();
        parent.parent.$(
            "#menulogin").remove()
        }
    })
}