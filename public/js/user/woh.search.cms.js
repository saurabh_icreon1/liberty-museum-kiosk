$(document).ready(function(){$("[placeholder]").focus(function(){var input=$(this);if(input.val()==input.attr(
"placeholder")){input.val("");input.removeClass("placeholder")}}).blur(function(){var input=$(this);if(input.val()==""||
input.val()==input.attr("placeholder")){input.addClass("placeholder");input.val(input.attr("placeholder"))}}).blur().
parents("form").submit(function(){$(this).find("[placeholder]").each(function(){var input=$(this);if(input.val()==input.
attr("placeholder"))input.val("")})});$("form#search_woh").keypress(function(event){if(event.keyCode==13){showAjxLoader(
);$("#submitsearch1").click();return false}});$("#submitsearch1").click(function(){showAjxLoader()})});function 
searchWoh(Url){showAjxLoader();var flag_record_status=true;if($.trim($("#last_name").val())=="Last Name")$("#last_name")
.val("");if($.trim($("#first_name").val())=="First Name")$("#first_name").val("");if($.trim($("#other_part_name").val())
=="Other Part of Name")$("#other_part_name").val("");if($.trim($("#country").val())=="Country of Origin")$("#country").
val("");if($.trim($("#donated_by_add").val())=="Donated By")$("#donated_by_add").val("");if($.trim($("#panel_number").
val())=="Panel #")$("#panel_number").val("");if($.trim($("#last_name").val())==""&&$.trim($("#panel_number").val())=="")
{hideAjxLoader();$("#error_last_name_search").remove();$("#last_name").after(
'<div class="error" id="error_last_name_search">'+WOH_SEARCH_ENTER_LAST_NAME+"</div>");return false}else{$(
"#error_last_name_search").remove();$("#page").val("1");$("#start_index").val("0");if(flag_record_status==true){
flag_record_status=false;if($.trim(Url)!=""&&$.trim(Url)=="search-woh")$.ajax({type:"POST",data:{searchString:
"view_woh_type="+$("#view_woh_type").val()+"&"+"page="+$("#page").val()+"&"+"sort_field="+$("#sort_field").val()+"&"+
"sort_order="+$("#sort_order").val()+"&"+"start_index="+$("#start_index").val()+"&"+"limit="+$("#limit").val()+"&"+
"last_name="+$("#last_name").val()+"&"+"first_name="+$("#first_name").val()+"&"+"other_part_name="+$("#other_part_name")
.val()+"&"+"country="+$("#country").val()+"&"+"donated_by="+$("#donated_by_add").val()+"&"+"panel_number="+$(
"#panel_number").val()},url:"/woh-list",async:false,success:function(responseData){flag_record_status=true;hideAjxLoader
();$("#searchResult").html(responseData);$(".scroll-pane").jScrollPane()}});else $("#search_woh").submit()}hideAjxLoader
();if($.trim($("#last_name").val())=="")$("#last_name").val("Last Name");if($.trim($("#first_name").val())=="")$(
"#first_name").val("First Name");if($.trim($("#other_part_name").val())=="")$("#other_part_name").val(
"Other Part of Name");if($.trim($("#country").val())=="")$("#country").val("Country of Origin");if($.trim($(
"#donated_by_add").val())=="")$("#donated_by_add").val("Donated By");if($.trim($("#panel_number").val())=="")$(
"#panel_number").val("Panel #")}return false}