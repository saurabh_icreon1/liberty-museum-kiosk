$(function(){
    $("#country").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:function(request,response)

        {
            var matcher=new RegExp($.ui.autocomplete.escapeRegex(request.term),"i"),resultset=[];
            $.each(country,function(){
                var id=
                this.id,value=this.value;
                if(this.value&&matcher.test(value))resultset.push({
                    id:this.id,
                    value:this.value
                    });
                return
            });
            response(resultset)
            },
        open:function(){
            $(".ui-menu").width(350)
            },
        change:function(event,ui){
            if(ui.item){
                $("#country").val(
                    ui.item.value);
                $("#country_id").val(ui.item.id)
                }else{
                $("#country").val("");
                $("#country_id").val("")
                }
            },
    focus:function(
        event,ui){
        $("#country_id").val(ui.item.id);
        return false
        },
    select:function(event,ui){
        $("#country").val(ui.item.value);
        $(
            "#country_id").val(ui.item.id);
        return false
        }
    });
$("#company").autocomplete({
    afterAdd:true,
    selectFirst:true,
    autoFocus:true
    ,
    source:function(request,response){
        $.ajax({
            url:"/get-company",
            method:"POST",
            dataType:"json",
            data:{
                company_name:$(
                    "#company").val()
                },
            success:function(jsonResult){
                var resultset=[];
                $.each(jsonResult,function(){
                    var id=this.company_id,
                    value=this.company_name;
                    resultset.push({
                        id:this.company_id,
                        value:this.company_name
                        })
                    });
                response(resultset)
                }
            })
    },
open:
function(){
    $(".ui-menu").width(350)
    },
change:function(event,ui){
    if(ui.item){
        $("#company").val(ui.item.value);
        $(
            "#company_id").val(ui.item.id)
        }else{
        $("#company").val("");
        $("#company_id").val("")
        }
    },
focus:function(event,ui){
    $(
        "#company_id").val(ui.item.id);
    return false
    },
select:function(event,ui){
    $("#company").val(ui.item.value);
    $("#company_id")
    .val(ui.item.id);
    return false
    }
});
$(".basic-text").click(function(){
    $("#contact-hideshow").slideToggle("slow");
    var text=$
    (".basic-text").text();
    if(text=="Advance Search")$(".basic-text").html("<a href='javascript:void(0)'>Basic Search</a>");
    else if(text=="Basic Search")$(".basic-text").html("<a href='javascript:void(0)'>Advance Search</a>");
    setTimeout(
        '$("#registration_date").val("2").select2();',"1000");
    document.getElementById("search").reset();
    $(".select2-chosen").html
    ("");
    $(".select2-search-choice").remove();
    $(".custom-radio label").removeClass("checked");
    $("#company_id").val("");
    $(
        "#country_id").val("");
    $("#association").val("")
    });
$("#from_date").datepicker({
    changeMonth:true,
    changeYear:true,
    onClose:
    function(selectedDate){
        $("#to_date").datepicker("option","minDate",selectedDate)
        }
    });
$("#to_date").datepicker({
    changeMonth:true,
    changeYear:true,
    onClose:function(selectedDate){
        $("#from_date").datepicker("option","maxDate",
            selectedDate)
        }
    });
    
$("#registration_date_from").datepicker({
     changeMonth:true,
     changeYear:true,
     onClose:
     function(selectedDate){
         $("#registration_date_to").datepicker("option","minDate",selectedDate);
     }
 });
 $("#registration_date_to").datepicker({
     changeMonth:true,
     changeYear:true,
     onClose:function(selectedDate){
       $("#registration_date_from").datepicker("option","maxDate",selectedDate);
     }
 });
    
$("#img_from").click(function(){
    jQuery("#from_date").datepicker("show")
    });
$("#img_to").click(function()

{
    jQuery("#to_date").datepicker("show")
    });
$("#last_transaction").change(function(){
    var date=$(this).val();
    if(date=="1"){
        $
        ("#date_selection").show();
        $("#from_date").val("");
        $("#to_date").val("")
        }else $("#date_selection").hide()
        });
        
  $("#registration_date").change(function(){
    var rdate = $(this).val();
    if(rdate=="1") {
        $("#registration_date_selection").show();
        $("#registration_date_from").val("");
        $("#registration_date_to").val("");
     }
     else { 
        $("#registration_date_selection").hide(); 
     }
  });
  
$(
    "#membership_package_shipped").change(function(){
    var date=$(this).val();
    if(date=="1")$("#membership_year_section").show(
        );else $("#membership_year_section").hide()
        });
$("#is_usa").change(function(){
    if($(this).is(":checked")){
        $(
            "#state_text_select").show();
        $("#state_text_block").hide()
        }else{
        $("#state_text_select").hide();
        $("#state_text_block").
        show()
        }
    });
var grid=jQuery("#list"),emptyMsgDiv=$('<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");
$("#list").
jqGrid({
    postData:{
        searchString:$("#search").serialize()
        },
    mtype:"POST",
    url:"/search-membership-package/"+newContact,
    datatype:"json",
    colNames:["ID",L_CONTACT_ID,L_NAME,L_EMAIL,L_TYPE,L_MEMBERSHIP_TITLE,L_COUNTRY,L_LAST_TRANS_NUMBER,
    L_LAST_TRANS_DATE,L_LAST_TRANS_AMOUNT,L_ACTION],
    colModel:[{
        name:"ID",
        index:"users.user_id",
        hidden:true,
        key:true
    },{
        name:
        L_CONTACT_ID,
        index:"users.user_id"
    },{
        name:L_NAME,
        index:"users.first_name"
    },{
        name:L_EMAIL,
        index:"users.email_id",
        width:
        "200"
    },{
        name:L_TYPE,
        index:"users.user_type"
    },{
        name:L_MEMBERSHIP_TITLE,
        index:"mst_membership.membership_title"
    },{
        name:
        L_COUNTRY,
        index:"country.name"
    },{
        name:L_LAST_TRANS_NUMBER,
        index:"transaction_id"
    },{
        name:L_LAST_TRANS_DATE,
        index:
        "transaction_date"
    },{
        name:L_LAST_TRANS_AMOUNT,
        index:"transaction_amount"
    },{
        name:L_ACTION,
        sortable:false
    }],
    viewrecords:
    true,
    sortname:"users.modified_date",
    sortorder:"desc",
    rowNum:10,
    rowList:[10,20,30],
    pager:"#pcrud",
    multiselect:true,
    viewrecords:true,
    autowidth:true,
    shrinkToFit:true,
    caption:"",
    width:"100%",
    cmTemplate:{
        title:false
    },
    loadComplete:function(
        ){
        var count=grid.getGridParam(),ts=grid[0];
        if(ts.p.reccount===0){
            grid.hide();
            emptyMsgDiv.show();
            $(
                "#pcrud_right div.ui-paging-info").css("display","none")
            }else{
            grid.show();
            emptyMsgDiv.hide();
            $(
                "#pcrud_right div.ui-paging-info").css("display","block")
            }
        }
});
emptyMsgDiv.insertAfter(grid.parent());
emptyMsgDiv.hide();
$("#list").jqGrid("navGrid","#pcrud",{
    reload:true,
    edit:false,
    add:false,
    search:false,
    del:false
});
$("#list").jqGrid(
    "navButtonAdd","#pcrud",{
        caption:"",
        title:"Export",
        id:"exportExcel",
        onClickButton:function(){
            exportExcel("list",
                "/search-membership-package")
            },
        position:"last"
    })
});
$("#updateButton").click(function(){
    var ids=jQuery("#list").jqGrid(
        "getGridParam","selarrrow");
    if($.trim($("#membership_package_value").val())!=""&&ids!=""){
        showAjxLoader();
        $.ajax({
            type:
            "POST",
            url:"/update-contact-membership-package-status",
            data:{
                membership_package_value:$.trim($(
                    "#membership_package_value").val()),
                ids:ids
            },
            success:function(responseData){
                var jsonObj=jQuery.parseJSON(responseData);
                if(jsonObj.status=="success")location.reload();else $.each(jsonObj.message,function(i,msg){
                    $("#"+i).after(
                        '<label class="error" style="display:block;">'+msg+"</label>")
                    });
                $("#updateButton").addClass("save-btn");
                $(
                    "#updateButton").removeClass("cancel-btn");
                hideAjxLoader()
                }
            })
    }
});
$("#searchbutton").click(function(){
    showAjxLoader();
    $(
        "#success_message").html("");
    $("#search_success_message").html("");
    $("#search_success_message").hide();
    $(
        "#success_message").hide();
    jQuery("#list").jqGrid("setGridParam",{
        postData:{
            searchString:$("#search").serialize()
            },
        url:
        "/search-membership-package/aTowOw=="
    });
    jQuery("#list").trigger("reloadGrid",[{
        page:1
    }]);
    window.location.hash=
    "#searchResult";
    hideAjxLoader();
    return false
    });
function search_details(){
    $("#success_message").html("");
    $(
        "#search_success_message").html("");
    $("#search_success_message").hide();
    $("#success_message").hide();
    jQuery("#list").
    jqGrid("setGridParam",{
        postData:{
            searchString:$("#search").serialize()
            }
        });
jQuery("#list").trigger("reloadGrid",[{
    page:1
}
])
}
$("#savebutton").click(function(){
    if($.trim($("#search_name").val())!=""){
        showAjxLoader();
        $.ajax({
            type:"POST",
            data:{
                search_name:$.trim($("#search_name").val()),
                searchString:$("#search").serialize(),
                search_id:$("#search_id").val()
                },
            url:
            "/contact-save-search",
            success:function(response){
                hideAjxLoader();
                if(!checkUserAuthenticationAjx(response))return false;
                var jsonObj=jQuery.parseJSON(response);
                $("#search_success_message").hide();
                $("#success_message").html("");
                $(
                    "#success_message").hide();
                $("#search_msg").html("");
                $("#search_msg").hide();
                if(jsonObj.status=="success")if($(
                    "#search_id").val()!="")generateSavedSearch(jsonObj.message);else generateSavedSearch(jsonObj.message);
                else{
                    $(
                        "#search_msg").show();
                    $("#search_msg").html(ERROR_MSG)
                    }
                }
        })
}else{
    $("#search_msg").show();
    $("#search_success_message").
    hide();
    $("#search_msg").html(SEARCH_NAME_EMPTY+'<span class="redarrow"></span>');
    return false
    }
});
function 
generateSavedSearch(message){
    $("#search_success_message").html("");
    $("#search_success_message").hide();
    $(
        "#saved_search option").each(function(){
        $(this).remove()
        });
    $.ajax({
        type:"POST",
        data:{},
        url:"/get-saved-search",
        success:
        function(response){
            if(!checkUserAuthenticationAjx(response))return false;
            $("#saved_search").append(response);
            $(
                "#search_success_message").show();
            $("#search_success_message").html(message)
            }
        })
}
$("#deletebutton").click(function(){
    $.
    colorbox({
        width:"550px",
        href:"#delete_search_contact",
        height:"200px",
        inline:true
    });
    $("#no_delete_search").click(function
        (){
            $.colorbox.close();
            $("#yes_delete_search").unbind("click");
            return false
            });
    $("#yes_delete_search").click(function(){
        $(
            "#yes_delete_search").unbind("click");
        $.ajax({
            type:"POST",
            data:{
                search_id:$("#search_id").val()
                },
            url:"/delete-search",
            success:function(response){
                if(!checkUserAuthenticationAjx(response))return false;
                var jsonObj=jQuery.parseJSON(response);
                if(jsonObj.status=="success"){
                    $("#search_name").val("");
                    $("#search_success_message").html(jsonObj.message);
                    location.
                    reload()
                    }
                }
        })
    })
});
function getSavedSearchResult(){
    if($("#saved_search").val()!=""){
        showAjxLoader();
        $("#savebutton").val(
            "Update");
        $("#deletebutton").show();
        $.ajax({
            type:"POST",
            data:{
                search_id:$("#saved_search").val()
                },
            url:
            "/get-saved-search-param",
            success:function(response){
                hideAjxLoader();
                if(!checkUserAuthenticationAjx(response))
                    return false;
                $("#success_message").html("");
                $("#success_message").hide();
                var searchParamArray=response,obj=jQuery.
                parseJSON(searchParamArray);
                document.getElementById("search").reset();
                $("#afihc").val(obj.afihc);
                $("#user_email").val(
                    obj.user_email);
                $("#transaction").val(obj.transaction);
                $("#phone").val(obj.phone);
                $("#association").val(obj.association)
                .select2();
                $("#last_transaction").val(obj.last_transaction).select2();
                $("#registration_date").val(obj.registration_date).select2();
                
                $("#registration_date_from").val(obj.registration_date_from);
                $("#registration_date_to").val(obj.registration_date_to);
                
                 if((obj.registration_date_from != undefined && $.trim(obj.registration_date_from) != "") || (obj.registration_date_to != undefined && $.trim(obj.registration_date_to) != "")) {
                  $('#registration_date_selection').show();
                }
                else { 
                  $('#registration_date_selection').hide();
                }
                
                $("#source").val(obj.source).select2();
                $(
                    "#membership_package_shipped").val(obj.membership_package_shipped).select2();
                $("#state").val(obj.state);
                $("#from_date").
                val(obj.from_date);
                $("#to_date").val(obj.to_date);
                $("#membership_id").val(obj.membership_id);
                $("#type").val(obj.type).
                select2();
                $("#company").val(obj.company);
                $("#company_id").val(obj.company_id);
                $("#zip").val(obj.zip);
                $("#country_id").
                val(obj.country_id);
                $("#country").val(obj.country);
                $("#search_name").val(obj.search_title);
                $("#search_id").val($(
                    "#saved_search").val());
                $("#membership_title").val(obj.membership_title).select2();
                var groupsObj=Object(obj.groups);
                $(
                    "#groups").val(groupsObj).select2();
                $("#gender").val(obj.gender).select2();
                $("#status").val(obj.status).select2();
                $(
                    "#association").val(obj.association).select2();
                $("#searchbutton").click()
                }
            })
    }else{
    $("#search_name").val("");
    $(
        "#search_id").val("");
    $("#savebutton").val("Save");
    $("#deletebutton").hide()
    }
}
function deleteContact(contact_id){
    $(
        ".delete_contact").colorbox({
        width:"550px",
        height:"200px",
        inline:true
    });
    $("#no_delete").click(function(){
        $.colorbox.
        close();
        $("#yes_delete").unbind("click");
        return false
        });
    $("#yes_delete").click(function(){
        $("#yes_delete").unbind(
            "click");
        $.ajax({
            type:"POST",
            data:{
                conatact_id:contact_id
            },
            url:"/delete-contact",
            success:function(response){
                if(!
                    checkUserAuthenticationAjx(response))return false;
                var jsonObj=jQuery.parseJSON(response);
                if(jsonObj.status=="success"){
                    $
                    ("#success_message").html(jsonObj.message);
                    location.reload()
                    }
                }
        })
    })
}