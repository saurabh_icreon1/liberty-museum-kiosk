function loginPopupNp(Url) {
    parent.$.colorbox({width:"920px",height:"545px",href:Url+"--showPopup",iframe:true});
}

$(function() {
    $("#activation").validate({submitHandler: function() {
            var dataStr = $("#activation").serialize();
            $.ajax({type:
                        "POST", url: "/activation", data: dataStr, success: function(responseData) {
                    var jsonObj = jQuery.parseJSON(responseData);
                    if (
                            jsonObj.status == "success") {
                        $("#verification_not_success").hide();
                        $("#verification_success").show();
                        var login_message = '<a href="/login"> Continue Login</a>';
                        if(jsonObj.referalUrlPN != "" && jsonObj.referalUrlPN != undefined) { 
                            var spStr = jsonObj.referalUrlPN.split("@@");
                            if(spStr[1] != undefined && spStr[1] != "" && $.trim(spStr[1]) != "") {
                              login_message = '<a href="javascript:loginPopupNp(\'/login/'+jsonObj.referalUrlPN+'\');"> Continue Login</a>'; 
                            }
                            else {
                                login_message = '<a href="/login/'+jsonObj.referalUrlPN+'"> Continue Login</a>';
                            }
                        }
                        
                        var success_message =
                                jsonObj.message, message = success_message + login_message;
                        $(
                                "#verification_success").html(message);
                        $("#verify_code").removeClass("error");
                        $("#verify_code").val("")
                    } else {
                        $(
                                "#error_message_server").val("");
                        $("#verify_code").val("");
                        $("#error_message_server").hide();
                        $("#verify_code").addClass(
                                "error");
                        if (jsonObj.message != "") {
                            $("#error_message").html(
                                    '<label for="verify_code" generated="true" class="error" style="display: block;">' + jsonObj.message + "</label>");
                            $(
                                    "#verify_code").val("")
                        } else
                            $("#error_message").html("")
                    }
                }})
        }, rules: {verify_code: {required: true}}, messages: {verify_code
                    : {required: ENTER_VERIFY_CODE}}});
            
        $("#resend_activation_code").validate({submitHandler: function() {
            showAjxLoader();
            var dataStr = $("#resend_activation_code").serialize()
                    ;
            $.ajax({type: "POST", url: "/resend-activation-code", data: dataStr, success: function(responseData) {
                    var jsonObj = jQuery.parseJSON(responseData
                            );
                    hideAjxLoader();
                    if (jsonObj.status == "success") {
                        $("#error_message").hide();
                        $("#success_message").show();
                        $(
                                "#success_message").html(jsonObj.message);
                        $("#submitbutton").attr("disabled", "disabled");
                        $("#submitbutton").addClass(
                                "disable-button");
                        setTimeout(function() {
                            parent.jQuery.colorbox.close();
                            jQuery.colorbox.close()
                        }, 4000)
                    } else {
                        $(
                                "#error_message").show();
                        $("#success_message").hide();
                        $("#error_message").html(jsonObj.message);
                        $("#submitbutton").
                                removeAttr("disabled");
                        $("#submitbutton").removeClass("disable-button")
                    }
                }})
        }, rules: {email_id: {required: true, email: true}}
        , messages: {email_id: {required: EMAIL_EMPTY, email: EMAIL_INVALID}}});
});
function resendMail(user_id) {
    $.ajax({type: "POST", url: "/resend-email", data: {user_id:
            user_id}, success: function(responseData) {
            var jsonObj = jQuery.parseJSON(responseData);
            if (jsonObj.status == "success")
                $(
                        "#resend_mail").html(jsonObj.message)
        }})
}

function resendActivationCode(){
    parent.$.colorbox({
                    width:"650px", 
                    height:"520px",
                    iframe : true,
                    href:"/resend-activation-code"
                });
}
