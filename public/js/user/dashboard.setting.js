$(function(){$("button[id^='cboxClose']").hide();showAjxLoader();$.jgrid.no_legacy_api=true;$.jgrid.useJSON=true;var 
gridProAttrib=jQuery("#list"),emptyMsgDiv=$('<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");$("#list").jqGrid({
mtype:"POST",url:"/list-table-fields/"+dash0+"/"+dash1+"/"+dash2+"/"+visible,datatype:"json",sortable:true,colNames:["",
"","","","","",DISPLAY_COLUMN_NAME,DISPLAY_ORDER],colModel:[{name:"Exist",index:"hidden",hidden:true,key:true},{name:
"Dash Id",index:"dashId",hidden:true,key:true},{name:"Search Id",index:"searchId",hidden:true,key:true},{name:
"Search Type",index:"searchType",hidden:true,key:true},{name:"Visible",index:"visible",hidden:true,key:true},{name:"",
sortable:false,width:"200px"},{name:DISPLAY_COLUMN_NAME,sortable:false,width:"150px"},{name:DISPLAY_ORDER,sortable:false
,width:"130px"}],viewrecords:true,rowNum:200,viewrecords:true,autowidth:true,shrinkToFit:false,caption:"",width:"100%",
cmTemplate:{title:false},loadComplete:function(){hideAjxLoader();var count=gridProAttrib.getGridParam(),ts=gridProAttrib
[0];if(ts.p.reccount===0){gridProAttrib.hide();emptyMsgDiv.show();$("#pcrud_right div.ui-paging-info").css("display",
"none")}else{gridProAttrib.show();emptyMsgDiv.hide();$("#pcrud_right div.ui-paging-info").css("display","block")}},
gridComplete:function(){$("#_empty","#list").addClass("nodrag nodrop");jQuery("#list").tableDnDUpdate()}});emptyMsgDiv.
insertAfter(gridProAttrib.parent());emptyMsgDiv.hide();$("#list").jqGrid("navGrid","#pcrud",{reload:true,edit:false,add:
false,search:false,del:false});jQuery("#list").tableDnD({onDrop:function(table,row){}});$("#custom_dashlet").validate({
submitHandler:function(){var dataStr=$("#custom_dashlet").serialize(),total=$('[name="id[]"]:checked').length;if(total<6
){if(checkCustomValidation("custom_dashlet"))$.ajax({type:"POST",url:"/update-dashlet-table",data:dataStr,success:
function(responseData){var jsonObj=jQuery.parseJSON(responseData);if(jsonObj.status=="success")$(
"button[id^='cboxClose']").click()}})}else $("#check_dash0").after(
"<label class='error' for='id[]'>Please select atmost 5 dashlet.</label>")},rules:{"id[]":{required:true},sort:{required
:true}},messages:{"id[]":{required:"Please select at least one"},sort:{required:"Please select at least one"}}});$(
"#cboxClose").click(function(){})});function clickCheck(id){if($("#check_dash"+id).is(":checked")){$("#name"+id).
removeAttr("disabled");$("#radio"+id).removeAttr("disabled")}else{$("#name"+id).prop("disabled",true);$("#radio"+id).
prop("disabled",true)}}function checkCustomValidation(id){var flagGl=true;$('#custom_dashlet [name="title[]"]').each(
function(index){if(!$("#name"+index).is(":disabled")){var str=$(this).val(),regx=/^[a-zA-z0-9.]+$/i;$(this).next().
remove("span");if(str.match(regx)||str.length==0)if(str.length<1){if($(this).next("span").length==0)$(this).after(
'<span class="error" for="title[]" generated="true" style="display:block !important;"><span class="redarrow"></span>Please enter a valid title</span>'
);flagGl=false}else if(str.length>15&&str!=""){if($(this).next("span").length==0)$(this).after(
'<span class="error" for="title[]" generated="true" style="display:block !important;"><span class="redarrow"></span>Please enter a valid title</span>'
);flagGl=false}else $(this).next().remove("span")}});if(flagGl==false)return false;else{$(".detail-section").css(
"display","block");return true}}function undrag(){if(isDragged==1){$("button[id^='cboxClose']").click();var id=dash0;if(
dash1!=0)id=id+"_"+dash1;if(dash2!=0)id=id+"_"+dash2;parent.$("#"+id+"-href-icon").children().click()}else $(
"button[id^='cboxClose']").click()}