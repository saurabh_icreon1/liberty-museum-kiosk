jQuery(function(){jQuery.validator.addMethod("validate_require_default",function(value,element){if(element.defaultValue
==value||value==""||value==null)return false;return true},"Field should not be empty.")});jQuery(function(){$(
"#login_crm").validate({submitHandler:function(){var loginUrl;if(refererPath)loginUrl="/crm/"+refererPath;else loginUrl=
"/crm";var dataStr=jQuery("#login_crm").serialize();showAjxLoader();jQuery.ajax({type:"POST",url:loginUrl,data:dataStr,
success:function(responseData){var jsonObj=jQuery.parseJSON(responseData);if(jsonObj.status=="success"){jQuery(
"#error_message").css("display","none");jQuery("#error_message").html("");window.location.href=jsonObj.path}else if(
typeof jsonObj.message=="string"){jQuery("#error_message").css("display","block");jQuery("#error_message").html(jsonObj.
message)}else $.each(jsonObj.message,function(i,msg){$("#"+i).after('<label class="error" style="display:block;">'+msg+
"</label>")});hideAjxLoader()}})},rules:{user_name:{validate_require_default:true,email:true},password:{
validate_require_default:true}},messages:{user_name:{validate_require_default:USERNAME_EMPTY,email:INVALID_EMAIL_ADDRESS
},password:{validate_require_default:PASSWORD_EMPTY}}})});$(function(){$("#password").bind("focus",function(){$(this).
attr("type","password");if("Password"==$(this).val())$(this).val("")});$("#password").bind("blur",function(){if(
"Password"==$(this).val())$(this).attr("type","text");else if($(this).val()=="")$(this).val("Password")})})