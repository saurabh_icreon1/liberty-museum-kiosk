$(function(){
    var mode=$("#mode").val();
    var activity_id=$("#activity_id").val();
    if(mode=="edit")viewRelatedSources();
    else if(mode=="create")selectTmpActivity($("#crm_user_id").val(),$("#user_session_id").val(),$("#contact_name_id").val(),$("#activity_source_type_id").val());
    $("#activity_date").datepicker({
        changeMonth:true,
        changeYear:true,
        showOtherMonths:true
    });
    $("#activity_time").timepicker({
        "timeFormat":"h:i A"
    });
    $("#follow_up_activity_date").datepicker({
        changeMonth:true,
        changeYear:true,
        showOtherMonths:true
    });
    $("#follow_up_activity_time").timepicker({
        "timeFormat":"h:i A"
    });
    $("#activity_type_id").change(function(){
        $("div[class^='activity_type_id']").hide();
        if($(".activity_type_id"+this.value))$(".activity_type_id"+this.value).show()
            });
    if($(".activity_type_id"+$("#activity_type_id").val()))$(".activity_type_id"+$("#activity_type_id").val()).show();
    $("#contact_name").blur(function(){
        if($("#contact_name_id").val()!=""&&$("#contact_name").next().attr("id")!="contact_name_id"){
            $("#contact_name").next().remove();
            $("#contact_name").removeClass("error")
            }
        });
$("#contact_name").keyup(function(){
    if($("#activity_source_type_id").val()!=""){
        $("#activity_source_type_id").val("").select2();
        $("div[id^='source_look_']").hide();
        $(".gview_list").html("");
        $(".gview_list").html('<table id="list_case"></table><div id="listcaserud"></div>');
        $(".gview_tmplist").html("");
        $(".gview_tmplist").html('<table id="list_tmpcase"></table><div id="listtmpcaserud"></div>');
        $("#selectcancel").hide()
        }
    });
$("#saveactivity").click(function(){
    if($("#mode").val()==
        "create"){
        var activity_source_type=$("#activity_source_type_id").val();
        if($("#list_tmpcase").children().children().length==0&&activity_source_type!=""){
            var message;
            if(activity_source_type==2)message="Please select an opportunity";
            else if(activity_source_type==3)message="Please select a pledge";
            else if(activity_source_type==4)message="Please select a case";
            $("#activity_source_type_id").after('<label id="emptySource" class="error" style="display:block;">'+message+"</label>");
            $("html,body").animate({
                scrollTop:$("#activity_detail").offset().top
                })
            }else{
            $("#emptySource").remove();
            $("#create_activity").submit()
            }
        }else if($("#mode").val()=="edit"){
    var activity_source_type=$("#activity_source_type_id").val();
    if(activity_source_type!="")if($("#list_case").children().children().length>0){
        var activityList=new Array;
        $("#list_case").find("input[type=checkbox]").each(function(){
            activityList.push($(this).attr("id"))
            });
        if(activityList.length==0){
            $("#emptySource").remove();
            $("#edit_activity").submit()
            }else if(activityList.length>0){
            var message;
            if(activity_source_type==2)message="Please select an opportunity";
            else if(activity_source_type==3)message="Please select a pledge";
            else if(activity_source_type==4)message="Please select a case";
            $("#activity_source_type_id").after('<label id="emptySource" class="error" style="display:block;">'+message+"</label>");
            $("html,body").animate({
                scrollTop:$("#activity_detail").offset().top
                })
            }
        }else if($("#list_tmpcase").children().children().length==0){
        var message;
        if(activity_source_type==2)message="Please select an opportunity";
        else if(activity_source_type==3)message="Please select a pledge";
        else if(activity_source_type==4)message="Please select a case";
        $("#activity_source_type_id").after('<label id="emptySource" class="error" style="display:block;">'+message+"</label>");
        $("html,body").animate({
            scrollTop:$("#activity_detail").offset().top
            })
        }else{
        if($("#list_case").children().children().length==0&&$("#list_tmpcase").children().children().length>0){
            $("#emptySource").remove();
            $("#edit_activity").submit()
            }
        }else{
    $("#emptySource").remove();
    $("#edit_activity").submit()
    }
}
});
jQuery.validator.addMethod("alphanumericspecialchar",
    function(value,element){
        return this.optional(element)||/^[A-Za-z0-9\n\r ,.-]+$/i.test(value)
        },ALPHA_NUMERIC_SPECIALSYMPOL);
jQuery.validator.addMethod("alphanumerichypenchar",function(value,element){
    return this.optional(element)||/^[A-Za-z0-9\n\r -]+$/i.test(value)
    },ALPHA_NUMERIC_SPECIALSYMPOL);
$("#create_activity").validate({
    submitHandler:function(){
        var dataStr=$("#create_activity").serialize();
        var sources=$("#list_tmpcase").children().children();
        var sourceData=new Array;
        sources.each(function(){
            if($(this).attr("id"))sourceData.push($(this).attr("id"))
                });
        showAjxLoader();
        $.ajax({
            type:"POST",
            url:"/create-activity",
            data:{
                dataStr:dataStr,
                sourceData:sourceData
            },
            success:function(responseData){
                hideAjxLoader();
                if(!checkUserAuthenticationAjx(responseData))return false;
                var jsonObj=jQuery.parseJSON(responseData);
                if(jsonObj.status=="success")window.location.href="/activities";else $.each(jsonObj.message,function(i,msg){
                    $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                    })
                }
                })
    },
rules:{
    activity_type_id:{
        required:true
    },
    assigned_by_crm_user:{
        required:true
    },
    contact_name:{
        required:true
    },
    assigned_to_crm_user:{
        required:true
    },
    activity_subject:{
        required:true,
        alphanumerichypenchar:true,
        minlength:2,
        maxlength:50
    },
    activity_location:{
        required:true,
        alphanumericspecialchar:true,
        minlength:2,
        maxlength:50
    },
    activity_date:{
        required:true
    },
    activity_time:{
        required:true
    },
    activity_status_id:{
        required:true
    },
    activity_priority_id:{
        required:true
    },
    activity_detail:{
        //alphanumericspecialchar:true,
        minlength:2,
        maxlength:500
    },
    activity_duration:{
        digits:true
    },
    follow_up_activity_subject:{
        minlength:2,
        maxlength:50
    }
},
messages:{
    activity_type_id:{
        required:ACTIVITY_TYPE_EMPTY
    },
    assigned_by_crm_user:{
        required:ASSIGNED_BY_EMPTY
    },
    contact_name:{
        required:USER_EMPTY
    },
    assigned_to_crm_user:{
        required:ASSIGNED_TO_EMPTY
    },
    activity_subject:{
        required:SUBJECT_EMPTY
    },
    activity_location:{
        required:LOCATION_EMPTY
    },
    activity_date:{
        required:DATE_EMPTY
    },
    activity_time:{
        required:DATE_EMPTY
    },
    activity_status_id:{
        required:STATUS_EMPTY
    },
    activity_priority_id:{
        required:PRIORITY_EMPTY
    },
    activity_duration:{
        digits:DURATION_NUMERIC
    }
}
});
$("#edit_activity").validate({
    submitHandler:function(){
        var dataStr=$("#edit_activity").serialize();
        var sourceData=new Array;
        if($("#list_tmpcase").children().children().length>0){
            var sources=$("#list_tmpcase").children().children();
            sources.each(function(){
                if($(this).attr("id"))sourceData.push($(this).attr("id"))
                    })
            }else{
            var sources=$("#list_case").children().children();
            sources.each(function(){
                if($(this).attr("id"))sourceData.push($(this).attr("id"))
                    })
            }
            showAjxLoader();
        $.ajax({
            type:"POST",
            url:"/edit-activity",
            data:{
                dataStr:dataStr,
                sourceData:sourceData
            },
            success:function(responseData){
                hideAjxLoader();
                if(!checkUserAuthenticationAjx(responseData))return false;
                var jsonObj=jQuery.parseJSON(responseData);
                if(jsonObj.status=="success")window.location.href="/activities";else $.each(jsonObj.message,function(i,msg){
                    $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                    })
                }
                })
    },
rules:{
    activity_type_id:{
        required:true
    },
    assigned_by_crm_user:{
        required:true
    },
    user:{
        required:true
    },
    assigned_to_crm_user:{
        required:true
    },
    activity_subject:{
        required:true,
        alphanumerichypenchar:true,
        minlength:2,
        maxlength:50
    },
    activity_location:{
        required:true,
        alphanumericspecialchar:true,
        minlength:2,
        maxlength:50
    },
    activity_date:{
        required:true
    },
    activity_time:{
        required:true
    },
    activity_status_id:{
        required:true
    },
    activity_priority_id:{
        required:true
    },
    activity_detail:{
        //alphanumericspecialchar:true,
        minlength:2,
        maxlength:500
    },
    activity_duration:{
        digits:true,
        minlength:2,
        maxlength:50
    },
    follow_up_activity_subject:{
        minlength:2,
        maxlength:50
    }
},
messages:{
    activity_type_id:{
        required:ACTIVITY_TYPE_EMPTY
    },
    assigned_by_crm_user:{
        required:ASSIGNED_BY_EMPTY
    },
    user:{
        required:USER_EMPTY
    },
    assigned_to_crm_user:{
        required:ASSIGNED_TO_EMPTY
    },
    activity_subject:{
        required:SUBJECT_EMPTY
    },
    activity_location:{
        required:LOCATION_EMPTY
    },
    activity_date:{
        required:DATE_EMPTY
    },
    activity_time:{
        required:TIME_EMPTY
    },
    activity_status_id:{
        required:STATUS_EMPTY
    },
    activity_priority_id:{
        required:PRIORITY_EMPTY
    },
    activity_duration:{
        digits:DURATION_NUMERIC
    }
}
});
$("#assigned_by_crm_user").autocomplete({
    afterAdd:true,
    selectFirst:true,
    autoFocus:true,
    source:function(request,response){
        $.ajax({
            url:"/get-crm-contacts",
            method:"POST",
            dataType:"json",
            data:{
                crm_full_name:$("#assigned_by_crm_user").val()
                },
            success:function(jsonResult){
                if(!checkUserAuthenticationAjx(jsonResult))return false;
                var resultset=[];
                $.each(jsonResult,function(){
                    var id=this.crm_user_id;
                    var value=this.crm_full_name;
                    resultset.push({
                        id:this.crm_user_id,
                        value:this.crm_full_name
                        })
                    });
                response(resultset)
                }
            })
    },
open:function(){
    $(".ui-menu").width(300)
    },
change:function(event,ui){
    if(ui.item){
        $("#assigned_by_crm_user").val(ui.item.value);
        $("#assigned_by_crm_user_id").val(ui.item.id)
        }else{
        $("#assigned_by_crm_user").val("");
        $("#assigned_by_crm_user_id").val("")
        }
    },
focus:function(event,ui){
    $("#assigned_by_crm_user_id").val(ui.item.id);
    return false
    },
select:function(event,ui){
    $("#assigned_by_crm_user").val(ui.item.value);
    $("#assigned_by_crm_user_id").val(ui.item.id);
    return false
    }
});
$("#contact_name").autocomplete({
    afterAdd:true,
    selectFirst:true,
    autoFocus:true,
    source:function(request,response){
        $.ajax({
            url:"/get-contacts",
            method:"POST",
            dataType:"json",
            data:{
                user_email:$("#contact_name").val()
                },
            success:function(jsonResult){
                if(!checkUserAuthenticationAjx(jsonResult))return false;
                var resultset=[];
                $.each(jsonResult,function(){
                    var id=this.user_id;
                    var value=this.full_name;
                    resultset.push({
                        id:this.user_id,
                        value:this.full_name
                        })
                    });
                response(resultset)
                }
            })
    },
open:function(){
    $(".ui-menu").width(350)
    },
change:function(event,ui){
    if(ui.item){
        $("#contact_name").val(ui.item.value);
        $("#contact_name_id").val(ui.item.id)
        }else{
        $("#contact_name").val("");
        $("#contact_name_id").val("")
        }
    },
focus:function(event,ui){
    $("#contact_name_id").val(ui.item.id);
    return false
    },
select:function(event,ui){
    $("#contact_name").val(ui.item.value);
    $("#contact_name_id").val(ui.item.id);
    return false
    }
});
$("#assigned_to_crm_user").autocomplete({
    afterAdd:true,
    selectFirst:true,
    autoFocus:true,
    source:function(request,response){
        $.ajax({
            url:"/get-crm-contacts",
            method:"POST",
            dataType:"json",
            data:{
                crm_full_name:$("#assigned_to_crm_user").val()
                },
            success:function(jsonResult){
                if(!checkUserAuthenticationAjx(jsonResult))return false;
                var resultset=[];
                $.each(jsonResult,function(){
                    var id=this.crm_user_id;
                    var value=this.crm_full_name;
                    resultset.push({
                        id:this.crm_user_id,
                        value:this.crm_full_name
                        })
                    });
                response(resultset)
                }
            })
    },
open:function(){
    $(".ui-menu").width(300)
    },
change:function(event,ui){
    if(ui.item){
        $("#assigned_to_crm_user").val(ui.item.value);
        $("#assigned_to_crm_user_id").val(ui.item.id)
        }else{
        $("#assigned_to_crm_user").val("");
        $("#assigned_to_crm_user_id").val("")
        }
    },
focus:function(event,ui){
    $("#assigned_to_crm_user_id").val(ui.item.id);
    return false
    },
select:function(event,ui){
    $("#assigned_to_crm_user").val(ui.item.value);
    $("#assigned_to_crm_user_id").val(ui.item.id);
    return false
    }
});
$("#case_subject").autocomplete({
    afterAdd:true,
    selectFirst:true,
    autoFocus:true,
    source:function(request,response){
        $.ajax({
            url:"/get-case-subject",
            method:"POST",
            dataType:"json",
            data:{
                case_subject_name:$("#case_subject").val()
                },
            success:function(jsonResult){
                if(!checkUserAuthenticationAjx(jsonResult))return false;
                var resultset=[];
                $.each(jsonResult,function(){
                    var value=
                    this.subject;
                    var id=this.id;
                    resultset.push({
                        value:value,
                        id:id
                    })
                    });
                response(resultset)
                }
            })
    },
open:function(){
    $(".ui-menu").width(300)
    },
change:function(event,ui){
    if(ui.item){
        $("#case_subject").val(ui.item.value);
        $("#case_subject_id").val(ui.item.id)
        }else{
        $("#case_subject").val("");
        $("#case_subject_id").val("")
        }
    },
select:function(event,ui){
    $("#case_subject").val(ui.item.value);
    $("#case_subject_id").val(ui.item.id);
    return false
    }
});
$("#lead_user_name").autocomplete({
    afterAdd:true,
    selectFirst:true,
    autoFocus:true,
    source:function(request,response){
        $.ajax({
            url:"/get-lead-contact",
            method:"POST",
            dataType:"json",
            data:{
                lead_user_name:$("#lead_user_name").val()
                },
            success:function(jsonResult){
                if(!checkUserAuthenticationAjx(jsonResult))return false;
                var resultset=[];
                $.each(jsonResult,function(){
                    var value=this.full_name_with_id;
                    var id=this.id;
                    resultset.push({
                        value:value,
                        id:id
                    })
                    });
                response(resultset)
                }
            })
    },
open:function(){
    $(".ui-menu").width(300)
    },
change:function(event,ui){
    if(ui.item){
        $("#lead_user_name").val(ui.item.value);
        $("#lead_user_name_id").val(ui.item.id)
        }else{
        $("#lead_user_name").val("");
        $("#lead_user_name_id").val("")
        }
    },
select:function(event,ui){
    $("#lead_user_name").val(ui.item.value);
    $("#lead_user_name_id").val(ui.item.id);
    return false
    }
});
$("#pledge_user_name").autocomplete({
    afterAdd:true,
    selectFirst:true,
    autoFocus:true,
    source:function(request,response){
        $.ajax({
            url:"/get-pledge-contact",
            method:"POST",
            dataType:"json",
            data:{
                pledge_user_name:$("#pledge_user_name").val()
                },
            success:function(jsonResult){
                if(!checkUserAuthenticationAjx(jsonResult))return false;
                var resultset=[];
                $.each(jsonResult,function(){
                    var value=this.full_name_with_id;
                    var id=this.id;
                    resultset.push({
                        value:value,
                        id:id
                    })
                    });
                response(resultset)
                }
            })
    },
open:function(){
    $(".ui-menu").width(300)
    },
change:function(event,ui){
    if(ui.item){
        $("#pledge_user_name").val(ui.item.value);
        $("#pledge_user_name_id").val(ui.item.id)
        }else{
        $("#pledge_user_name").val("");
        $("#pledge_user_name_id").val("")
        }
    },
select:function(event,ui){
    $("#pledge_user_name").val(ui.item.value);
    $("#pledge_user_name_id").val(ui.item.id);
    return false
    }
});
var progressbarid="progress";
$(".attachmentfiles").focus(function(){
    var file_id=$(this).attr("id");
    var file_number=file_id.substr(file_id.length-1,1);
    progressbarid="progress_"+file_number
    });
var url="/upload-attachment-activity";
$(".attachmentfiles").fileupload({
    url:url,
    dataType:"json",
    done:function(e,data){
        var file_id=$(this).attr("id");
        var file_number=file_id.substr(file_id.length-1,1);
        data=data.result;
        if(data.status=="success"){
            $("#file_name_msg_"+file_number).addClass("success-msg");
            $("#file_name_msg_"+file_number).removeClass("error-msg");
            $("#file_name_hidden_"+file_number).val(data.filename)
            }else{
            $("#file_name_msg_"+file_number).removeClass("success-msg");
            $("#file_name_msg_"+file_number).addClass("error-msg");
            $("#file_name_hidden_"+file_number).val("")
            }
            $("#file_name_msg_"+file_number).html(data.message)
        },
    progressall:function(e,data){},
    start:function(e){
        showAjxLoader()
        },
    stop:function(e){
        hideAjxLoader()
        }
    });
$("#cancelsource").click(function(){
    $("#list_case").find("input[type=checkbox]").each(function(){
        if($(this).is(":checked"))$(this).removeAttr("checked")
            });
    if($("#cb_list_case").is(":checked"))$("#cb_list_case").removeAttr("checked")
        });
$("#selectsource").click(function(){
    if($("#emptySource"))$("#emptySource").remove();
    var mode=$("#mode").val();
    var activity_id=$("#activity_id").val();
    var activityList=new Array;
    $("#list_case").find("input[type=checkbox]").each(function(){
        if($(this).is(":checked"))activityList.push($(this).attr("id"))
            });
    var sourceType=$("#activity_source_type_id").val();
    var user_id=$("#contact_name_id").val();
    if(activityList.length>0){
        showAjxLoader();
        $.ajax({
            type:"POST",
            url:"/create-temp-activity",
            data:{
                tmpActivities:activityList,
                sourceType:$("#activity_source_type_id").val(),
                user_id:$("#contact_name_id").val(),
                mode:mode,
                activity_id:activity_id
            },
            success:function(responseData){
                hideAjxLoader();
                if(!checkUserAuthenticationAjx(responseData))return false;
                var jsonObj=jQuery.parseJSON(responseData);
                if(jsonObj.status=="success"){
                    selectTmpActivity(jsonObj.crm_user_id,jsonObj.user_session_id,$("#contact_name_id").val(),$("#activity_source_type_id").val());
                    $("#source_look_"+$("#activity_source_type_id").val()).show();
                    jQuery("#list_tmpcase").trigger("reloadGrid",[{
                        page:1
                    }]);
                    jQuery(".gview_list").html("");
                    jQuery(".selectcancel").hide()
                    }else $.each(jsonObj.message,function(i,msg){
                    $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                    })
                }
                })
    }else $("#gview_list").before('<label id="emptySource" class="error" style="display:block;">Please select source</label>')
    })
});
function selectTmpActivity(crm_user_id,user_session_id,user_id,source_type){
    $(".gview_list").html("");
    $(".gview_list").html('<table id="list_case"></table><div id="listcaserud"></div>');
    $(".gview_tmplist").html("");
    $(".gview_tmplist").html('<table id="list_tmpcase"></table><div id="listtmpcaserud"></div>');
    $("#selectcancel").hide();
    $(".gview_tmplist").show();
    if($(".gview_tmplist").html()=="")$(".gview_tmplist").html('<table id="list_tmpcase"></table><div id="listtmpcaserud"></div>');
    var emptyMsgDiv=
    $('<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");
    $.jgrid.no_legacy_api=true;
    $.jgrid.useJSON=true;
    var source_type=$("#activity_source_type_id").val();
    var user_id=$("#contact_name_id").val();
    var grid=jQuery("#list_tmpcase");
    if($("#activity_source_type_id").val()==2){
        $("#list_tmpcase").jqGrid({
            postData:{
                crm_user_id:crm_user_id,
                user_session_id:user_session_id,
                user_id:$("#contact_name_id").val(),
                source_type:$("#activity_source_type_id").val(),
                searchString:$("#tmpact").serialize()
                },
            mtype:"POST",
            url:"/get-temp-activity",
            datatype:"json",
            colNames:["Id","Lead Type","Lead Source","Opportunity Amount","Status","Action(s)"],
            colModel:[{
                name:"Id",
                index:"id",
                hidden:true,
                key:true
            },{
                name:"Lead Type",
                index:"types.lead_type"
            },{
                name:"Lead Source",
                index:"source.lead_source"
            },{
                name:"Opportunity Amount",
                index:"leads.opportunity_amount"
            },{
                name:"Status",
                index:"leads.status"
            },{
                name:"Action",
                sortable:false
            }],
            viewrecords:true,
            sortname:"tmpact.modified_date",
            sortorder:"DESC",
            rowNum:"",
            pager:false,
            caption:"",
            autowidth:true,
            shrinkToFit:true,
            width:"100%",
            cmTemplate:{
                title:false
            },
            loadComplete:function(){
                var count=grid.getGridParam();
                var ts=grid[0];
                if(ts.p.reccount===0){
                    grid.hide();
                    emptyMsgDiv.show();
                    $("#listtmpcaserud div.ui-paging-info").css("display","none")
                    }else{
                    grid.show();
                    emptyMsgDiv.hide();
                    $("#listtmpcaserud div.ui-paging-info").css("display","block")
                    }
                }
        });
emptyMsgDiv.insertAfter(grid.parent());
emptyMsgDiv.hide();
$("#source_look_22").hide();
$("#list_tmpcase").jqGrid("navGrid","#listtmpcaserud",{
    reload:true,
    edit:false,
    add:false,
    search:false,
    del:false
});
myGrid=$("#list_tmpcase");
if($("#lead_user_name_id").val()!=""||$("#pledge_user_name_id").val()!=""||$("#case_subject_id").val()!="")myGrid.hideCol("Action");
var cm=myGrid[0].p.colModel;
$.each(myGrid[0].grid.headers,function(index,value){
    var cmi=cm[index],colName=cmi.name;
    if(!cmi.sortable&&colName!=="rn"&&colName!=="cb"&&colName!=="subgrid")$("div.ui-jqgrid-sortable",value.el).css({
        cursor:"default"
    })
    });
if($("#list_tmpcase").children().children().length==0)myGrid.hide()
    }else if($("#activity_source_type_id").val()==
    3){
    $("#list_tmpcase").jqGrid({
        postData:{
            crm_user_id:crm_user_id,
            user_session_id:user_session_id,
            user_id:$("#contact_name_id").val(),
            source_type:$("#activity_source_type_id").val(),
            searchString:$("#tmpact").serialize()
            },
        mtype:"POST",
        url:"/get-temp-activity",
        datatype:"json",
        colNames:["Id","Pledge #","Total Amount","Balance Amount","Contribution Type","Date","Status","Action(s)"],
        colModel:[{
            name:"Id",
            index:"id",
            hidden:true,
            key:true
        },{
            name:"Pledge #",
            index:"pledge_id"
        },{
            name:"Total Amount",
            index:"tbl_pledge.pledge_amount"
        },

        {
            name:"Balance Amount",
            index:"",
            sortable:false
        },{
            name:"Contribution Type",
            index:"tbl_pledge.contribution_type_id"
        },{
            name:"Date",
            index:"tbl_pledge.added_date"
        },{
            name:"Status Name",
            index:"pledge_status_id"
        },{
            name:"Action",
            sortable:false
        }],
        viewrecords:true,
        sortname:"tmpact.modified_date",
        sortorder:"DESC",
        rowNum:"",
        pager:false,
        caption:"",
        autowidth:true,
        shrinkToFit:true,
        width:"100%",
        cmTemplate:{
            title:false
        },
        loadComplete:function(){
            var count=grid.getGridParam();
            var ts=grid[0];
            if(ts.p.reccount===0){
                grid.hide();
                emptyMsgDiv.show();
                $("#listtmpcaserud div.ui-paging-info").css("display","none")
                }else{
                grid.show();
                emptyMsgDiv.hide();
                $("#listtmpcaserud div.ui-paging-info").css("display","block")
                }
            }
    });
emptyMsgDiv.insertAfter(grid.parent());
emptyMsgDiv.hide();
$("#source_look_33").hide();
$("#list_tmpcase").jqGrid("navGrid","#listtmpcaserud",{
    reload:true,
    edit:false,
    add:false,
    search:false,
    del:false
});
myGrid=$("#list_tmpcase");
var cm=myGrid[0].p.colModel;
$.each(myGrid[0].grid.headers,function(index,value){
    var cmi=cm[index],
    colName=cmi.name;
    if(!cmi.sortable&&colName!=="rn"&&colName!=="cb"&&colName!=="subgrid")$("div.ui-jqgrid-sortable",value.el).css({
        cursor:"default"
    })
    });
if($("#list_tmpcase").children().children().length==0)myGrid.hide()
    }else if($("#activity_source_type_id").val()==4){
    $("#list_tmpcase").jqGrid({
        postData:{
            crm_user_id:crm_user_id,
            user_session_id:user_session_id,
            user_id:$("#contact_name_id").val(),
            source_type:$("#activity_source_type_id").val(),
            searchString:$("#tmpact").serialize()
            },
        mtype:"POST",
        url:"/get-temp-activity",
        datatype:"json",
        colNames:["Id","Case #","Subject","Transaction #","Date","Type","Status","Action(s)"],
        colModel:[{
            name:"Id",
            index:"id",
            hidden:true,
            key:true
        },{
            name:"Case",
            index:"case_contact_id"
        },{
            name:"Subject",
            index:"case_subject"
        },{
            name:"Transaction",
            index:"user_transaction_id"
        },{
            name:"Date",
            index:"case_start_date"
        },{
            name:"Type",
            index:"case_type"
        },{
            name:"Status",
            index:"case_status"
        },{
            name:"Action",
            sortable:false
        }],
        viewrecords:false,
        sortname:"tmpact.modified_date",
        sortorder:"DESC",
        rowNum:"",
        pager:false,
        caption:"",
        autowidth:true,
        shrinkToFit:true,
        width:"100%",
        cmTemplate:{
            title:false
        },
        loadComplete:function(){
            var count=grid.getGridParam();
            var ts=grid[0];
            if(ts.p.reccount===0){
                grid.hide();
                emptyMsgDiv.show();
                $("#listtmpcaserud div.ui-paging-info").css("display","none")
                }else{
                grid.show();
                emptyMsgDiv.hide();
                $("#listtmpcaserud div.ui-paging-info").css("display","block")
                }
            }
    });
emptyMsgDiv.insertAfter(grid.parent());
emptyMsgDiv.hide();
$("#source_look_44").hide();
$("#list_tmpcase").jqGrid("navGrid","#listtmpcaserud",

{
    reload:true,
    edit:false,
    add:false,
    search:false,
    del:false
});
myGrid=$("#list_tmpcase");
var cm=myGrid[0].p.colModel;
$.each(myGrid[0].grid.headers,function(index,value){
    var cmi=cm[index],colName=cmi.name;
    if(!cmi.sortable&&colName!=="rn"&&colName!=="cb"&&colName!=="subgrid")$("div.ui-jqgrid-sortable",value.el).css({
        cursor:"default"
    })
    });
if($("#list_tmpcase").children().children().length==0)myGrid.hide()
    }
}
function removeAttachment(attachmentId,fileNumber){
    $(".delete_attachment").colorbox({
        width:"700px",
        height:"200px",
        inline:true
    });
    $("#no_delete_attachment").click(function(){
        $.colorbox.close();
        $("#yes_delete_attachment").unbind("click");
        return false
        });
    $("#yes_delete_attachment").click(function(){
        $("#yes_delete_attachment").unbind("click");
        $("#file_block_"+attachmentId).remove();
        $("#file_name_hidden_"+fileNumber).val("");
        $.colorbox.close()
        })
    }
function downloadFile(fileName){
    if(fileName!="")$.ajax({
        type:"GET",
        url:"/download-activity-file/"+fileName
        })
    }
function changeSourceType(val,crm_user_id,user_session_id){
    if($("#emptySource"))$("#emptySource").remove();
    $("div[id^='source_look_']").hide();
    if($("#contact_name_id").val()==""){
        $("#contact_name").focus();
        $("#contact_name").after('<label class="error" style="display:block;">'+USER_EMPTY+"</label>");
        window.scrollTo(0,0);
        $("#activity_source_type_id").val("").select2()
        }else if(val!=""&&$("#contact_name_id").val()!=""){
        $("div[id^='source_look_"+val+"']").show();
        $(".gview_list").html("");
        $(".gview_list").html('<table id="list_case"></table><div id="listcaserud"></div>');
        $(".gview_tmplist").html("");
        $(".gview_tmplist").html('<table id="list_tmpcase"></table><div id="listtmpcaserud"></div>');
        $("#selectcancel").hide();
        var user_id=$("#contact_name_id").val();
        var sourceType=val;
        showAjxLoader();
        $.ajax({
            type:"POST",
            url:"/count-related-temp-activity-sources",
            data:{
                sourceType:$("#activity_source_type_id").val(),
                user_id:$("#contact_name_id").val(),
                crm_user_id:crm_user_id,
                user_session_id:user_session_id
            },
            success:function(responseData){
                hideAjxLoader();
                if(!checkUserAuthenticationAjx(responseData))return false;
                var jsonObj=jQuery.parseJSON(responseData);
                if(jsonObj.status=="success"&&jsonObj.relatedCount>0){
                    selectTmpActivity(crm_user_id,user_session_id,$("#contact_name_id").val(),$("#activity_source_type_id").val());
                    $(".gview_list").html("");
                    $(".gview_list").html('<table id="list_case"></table><div id="listcaserud"></div>');
                    $("#selectcancel").hide()
                    }else{
                    $("div[id^='source_look_"+val+val+"']").show();
                    if($("#mode").val()=="edit")viewRelatedSources()
                        }
                    }
        })
}else if(val==""){
    $(".gview_list").html("");
    $(".gview_list").html('<table id="list_case"></table><div id="listcaserud"></div>');
    $(".gview_tmplist").html("");
    $(".gview_tmplist").html('<table id="list_tmpcase"></table><div id="listtmpcaserud"></div>');
    $("#selectcancel").hide()
    }
}
function getPledges(){
    if($("#contact_name_id").val()==""){
        $("#contact_name").focus();
        $("#contact_name").after('<label class="error" style="display:block;">'+USER_EMPTY+"</label>");
        window.scrollTo(0,0)
        }else{
        var sourceType=$("#activity_source_type_id").val();
        var user_id=$("#contact_name_id").val();
        if($("#activity_source_type_id").val()==""){
            $("#source_look_33").show();
            $(".gview_list").hide();
            $("#selectcancel").hide()
            }else{
            $("div[id^='source_look_']").hide();
            if($("#mode").val()=="create"){
                $(".gview_list").show();
                $(".gview_list").html("");
                $(".gview_list").html('<table id="list_case"></table><div id="listcaserud"></div>');
                $(".gview_tmplist").html("");
                $(".gview_tmplist").html('<table id="list_tmpcase"></table><div id="listtmpcaserud"></div>');
                $("#selectcancel").hide()
                }else $(".gview_list").html('<table id="list_case"></table><div id="listcaserud"></div>');
            $("#selectcancel").show();
            if($("#activity_source_type_id").val()==3){
                $(".gview_tmplist").hide();
                var emptyMsgDiv=$('<div class="no-record-msz">'+NO_RECORD_FOUND+
                    "</div>");
                $.jgrid.no_legacy_api=true;
                $.jgrid.useJSON=true;
                var grid=jQuery("#list_case");
                $("#list_case").jqGrid({
                    postData:{
                        user_id:$("#contact_name_id").val(),
                        sourceType:$("#activity_source_type_id").val(),
                        mode:$("#mode").val(),
                        activity_id:$("#activity_id").val()
                        },
                    mtype:"POST",
                    url:"/get-activity-pledges/",
                    datatype:"json",
                    colNames:["Id","Pledge #","Total Amount","Balance Amount","Contribution Type","Date","Status"],
                    colModel:[{
                        name:"Id",
                        index:"id",
                        hidden:true,
                        key:true
                    },{
                        name:"Pledge #",
                        index:"pledge_id"
                    },

                    {
                        name:"Total Amount",
                        index:"tbl_pledge.pledge_amount"
                    },{
                        name:"Balance Amount",
                        index:"",
                        sortable:false
                    },{
                        name:"Contribution Type",
                        index:"tbl_pledge.contribution_type_id"
                    },{
                        name:"Date",
                        index:"tbl_pledge.added_date"
                    },{
                        name:"Status Name",
                        index:"pledge_status_id"
                    }],
                    viewrecords:true,
                    sortname:"tbl_pledge.modified_date",
                    sortorder:"DESC",
                    rowNum:"",
                    pager:false,
                    caption:"",
                    autowidth:true,
                    shrinkToFit:true,
                    width:"100%",
                    multiselect:true,
                    cmTemplate:{
                        title:false
                    },
                    loadComplete:function(json){
                        var count=grid.getGridParam();
                        var ts=grid[0];
                        $("#listcaserud div.ui-paging-info").css("display","none");
                        if(ts.p.reccount===0){
                            grid.hide();
                            emptyMsgDiv.show()
                            }else{
                            grid.show();
                            emptyMsgDiv.hide();
                            var tmpActs=json.selectedIds;
                            if(tmpActs.length>0)for(var i=0;i<tmpActs.length;i++)$("#jqg_list_case_"+tmpActs[i]).attr("checked","checked")
                                }
                            }
                });
        emptyMsgDiv.insertAfter(grid.parent());
        emptyMsgDiv.hide();
        $("#list_case").jqGrid("navGrid","#listcaserud",{
            reload:true,
            edit:false,
            add:false,
            search:false,
            del:false
        });
        myGrid=$("#list_case");
        var cm=
        myGrid[0].p.colModel;
        $.each(myGrid[0].grid.headers,function(index,value){
            var cmi=cm[index],colName=cmi.name;
            if(!cmi.sortable&&colName!=="rn"&&colName!=="cb"&&colName!=="subgrid")$("div.ui-jqgrid-sortable",value.el).css({
                cursor:"default"
            })
            })
        }
    }
}
}
function getCases(){
    if($("#contact_name_id").val()==""){
        $("#contact_name").focus();
        $("#contact_name").after('<label class="error" style="display:block;">'+USER_EMPTY+"</label>");
        window.scrollTo(0,0)
        }else{
        var sourceType=$("#activity_source_type_id").val();
        var user_id=$("#contact_name_id").val();
        if($("#activity_source_type_id").val()==""){
            $("#source_look_44").show();
            $(".gview_list").hide();
            $("#selectcancel").hide()
            }else{
            $("div[id^='source_look_']").hide();
            if($("#mode").val()=="create"){
                $(".gview_list").show();
                $(".gview_list").html("");
                $(".gview_list").html('<table id="list_case"></table><div id="listcaserud"></div>');
                $(".gview_tmplist").html("");
                $(".gview_tmplist").html('<table id="list_tmpcase"></table><div id="listtmpcaserud"></div>');
                $("#selectcancel").hide()
                }else $(".gview_list").html('<table id="list_case"></table><div id="listcaserud"></div>');
            $("#selectcancel").show();
            if($("#activity_source_type_id").val()==4){
                $(".gview_tmplist").hide();
                var emptyMsgDiv=$('<div class="no-record-msz">'+NO_RECORD_FOUND+
                    "</div>");
                $.jgrid.no_legacy_api=true;
                $.jgrid.useJSON=true;
                var grid=jQuery("#list_case");
                $("#list_case").jqGrid({
                    postData:{
                        user_id:$("#contact_name_id").val(),
                        sourceType:$("#activity_source_type_id").val(),
                        mode:$("#mode").val(),
                        activity_id:$("#activity_id").val()
                        },
                    mtype:"POST",
                    url:"/get-activity-cases/",
                    datatype:"json",
                    colNames:["Id","Case #","Subject","Transaction #","Date","Type","Status"],
                    colModel:[{
                        name:"Id",
                        index:"id",
                        hidden:true,
                        key:true
                    },{
                        name:"Case",
                        index:"case_contact_id"
                    },{
                        name:"Subject",
                        index:"case_subject"
                    },{
                        name:"Transaction",
                        index:"user_transaction_id"
                    },{
                        name:"Date",
                        index:"case_start_date"
                    },{
                        name:"Type",
                        index:"case_type"
                    },{
                        name:"Status",
                        index:"case_status"
                    }],
                    viewrecords:true,
                    sortname:"tbl_case.modified_date",
                    sortorder:"DESC",
                    rowNum:"",
                    pager:false,
                    caption:"",
                    autowidth:true,
                    shrinkToFit:true,
                    width:"100%",
                    multiselect:true,
                    cmTemplate:{
                        title:false
                    },
                    loadComplete:function(json){
                        var count=grid.getGridParam();
                        var ts=grid[0];
                        $("#listcaserud div.ui-paging-info").css("display","none");
                        if(ts.p.reccount===
                            0){
                            grid.hide();
                            emptyMsgDiv.show()
                            }else{
                            grid.show();
                            emptyMsgDiv.hide();
                            var tmpActs=json.selectedIds;
                            if(tmpActs.length>0)for(var i=0;i<tmpActs.length;i++)$("#jqg_list_case_"+tmpActs[i]).attr("checked","checked")
                                }
                            }
                });
        emptyMsgDiv.insertAfter(grid.parent());
        emptyMsgDiv.hide();
        $("#list_case").jqGrid("navGrid","#listcaserud",{
            reload:true,
            edit:false,
            add:false,
            search:false,
            del:false
        });
        myGrid=$("#list_case");
        var cm=myGrid[0].p.colModel;
        $.each(myGrid[0].grid.headers,function(index,value){
            var cmi=cm[index],colName=
            cmi.name;
            if(!cmi.sortable&&colName!=="rn"&&colName!=="cb"&&colName!=="subgrid")$("div.ui-jqgrid-sortable",value.el).css({
                cursor:"default"
            })
            })
        }
    }
}
}
function getOpportunity(){
    var mode=$("#mode").val();
    if($("#contact_name_id").val()==""){
        $("#contact_name").focus();
        $("#contact_name").after('<label class="error" style="display:block;">'+USER_EMPTY+"</label>");
        window.scrollTo(0,0)
        }else{
        var sourceType=$("#activity_source_type_id").val();
        var user_id=$("#contact_name_id").val();
        var sourceType=$("#activity_source_type_id").val();
        var user_id=$("#contact_name_id").val();
        if($("#activity_source_type_id").val()==""){
            $("#source_look_22").show();
            $(".gview_list").hide();
            $("#selectcancel").hide()
            }else{
            $("div[id^='source_look_']").hide();
            if(mode=="create"){
                $(".gview_list").show();
                $(".gview_list").html("");
                $(".gview_list").html('<table id="list_case"></table><div id="listcaserud"></div>');
                $(".gview_tmplist").html("");
                $(".gview_tmplist").html('<table id="list_tmpcase"></table><div id="listtmpcaserud"></div>');
                $("#selectcancel").hide()
                }else $(".gview_list").html('<table id="list_case"></table><div id="listcaserud"></div>');
            $("#selectcancel").show();
            if($("#activity_source_type_id").val()==
                2){
                $(".gview_tmplist").hide();
                var emptyMsgDiv=$('<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");
                $.jgrid.no_legacy_api=true;
                $.jgrid.useJSON=true;
                var grid=jQuery("#list_case");
                $("#list_case").jqGrid({
                    postData:{
                        user_id:$("#contact_name_id").val(),
                        sourceType:$("#activity_source_type_id").val(),
                        mode:$("#mode").val(),
                        activity_id:$("#activity_id").val()
                        },
                    mtype:"POST",
                    url:"/get-activity-leads/",
                    datatype:"json",
                    colNames:["Id","Lead Type","Lead Source","Opportunity Amount","Status"],
                    colModel:[{
                        name:"Id",
                        index:"id",
                        hidden:true,
                        key:true
                    },{
                        name:"Lead Type",
                        index:"types.lead_type"
                    },{
                        name:"Lead Source",
                        index:"source.lead_source"
                    },{
                        name:"Opportunity Amount",
                        index:"leads.opportunity_amount"
                    },{
                        name:"Status",
                        index:"leads.status"
                    }],
                    viewrecords:true,
                    sortname:"leads.modified_date",
                    sortorder:"DESC",
                    rowNum:"",
                    pager:false,
                    caption:"",
                    autowidth:true,
                    shrinkToFit:true,
                    width:"100%",
                    multiselect:true,
                    cmTemplate:{
                        title:false
                    },
                    loadComplete:function(json){
                        var count=grid.getGridParam();
                        var ts=grid[0];
                        $("#listcaserud div.ui-paging-info").css("display",
                            "none");
                        if(ts.p.reccount===0){
                            grid.hide();
                            emptyMsgDiv.show();
                            $("#selectcancel").hide()
                            }else{
                            grid.show();
                            emptyMsgDiv.hide();
                            var tmpActs=json.selectedIds;
                            if(tmpActs.length>0)for(var i=0;i<tmpActs.length;i++)$("#jqg_list_case_"+tmpActs[i]).attr("checked","checked")
                                }
                            }
                });
        emptyMsgDiv.insertAfter(grid.parent());
        emptyMsgDiv.hide();
        $("#list_case").jqGrid("navGrid","#listcaserud",{
            reload:true,
            edit:false,
            add:false,
            search:false,
            del:false
        });
        myGrid=$("#list_case");
        var cm=myGrid[0].p.colModel;
        $.each(myGrid[0].grid.headers,
            function(index,value){
                var cmi=cm[index],colName=cmi.name;
                if(!cmi.sortable&&colName!=="rn"&&colName!=="cb"&&colName!=="subgrid")$("div.ui-jqgrid-sortable",value.el).css({
                    cursor:"default"
                })
                })
        }
    }
}
}
function deleteTempSource(tmpActId){
    $.colorbox({
        width:"600px",
        href:"#delete_temp",
        height:"230px",
        inline:true
    });
    $("#no_delete").click(function(){
        $.colorbox.close();
        $("#yes_delete").unbind("click");
        return false
        });
    $("#yes_delete").click(function(){
        $("#yes_delete").unbind("click");
        showAjxLoader();
        $.ajax({
            type:"POST",
            data:{
                tmp_activity_id:tmpActId
            },
            url:"/delete-temp-activity",
            success:function(response){
                var jsonObj=jQuery.parseJSON(response);
                if(jsonObj.status=="success"){
                    hideAjxLoader();
                    jQuery("#list_tmpcase").trigger("reloadGrid",
                        [{
                            page:1
                        }]);
                    $("#no_delete").click();
                    $("#success_message").show();
                    setTimeout("$('#success_message').hide();",2E3)
                    }
                }
        })
    })
}
function viewRelatedSources(){
    $("div[id^='source_look_']").hide();
    var activity_source_type_id=$("#activity_source_type_id").val();
    var user_id=$("#contact_name_id").val();
    var emptyMsgDiv=$('<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");
    $.jgrid.no_legacy_api=true;
    $.jgrid.useJSON=true;
    var grid=jQuery("#list_case");
    if($("#activity_source_type_id").val()==3){
        $("#source_look_3").show();
        $("#list_case").jqGrid({
            postData:{
                sourceType:$("#activity_source_type_id").val(),
                activity_id:$("#activity_id").val(),
                user_id:$("#contact_name_id").val(),
                mode:$("#mode").val()
                },
            mtype:"POST",
            url:"/view-related-activity-sources",
            datatype:"json",
            colNames:["Pledge #","Total Amount","Balance Amount","Contribution Type","Date","Status","Action(s)"],
            colModel:[{
                name:"Pledge #",
                index:"pledge_id"
            },{
                name:"Total Amount",
                index:"tbl_pledge.pledge_amount"
            },{
                name:"Balance Amount",
                index:"",
                sortable:false
            },{
                name:"Contribution Type",
                index:"tbl_pledge.contribution_type_id"
            },{
                name:"Date",
                index:"tbl_pledge.added_date"
            },{
                name:"Status Name",
                index:"pledge_status_id"
            },{
                name:"Action(s)",
                sortable:false
            }],
            viewrecords:true,
            sortname:"tbl_pledge.modified_date",
            sortorder:"DESC",
            rowNum:"",
            pager:false,
            caption:"",
            autowidth:true,
            shrinkToFit:true,
            width:"100%",
            cmTemplate:{
                title:false
            },
            loadComplete:function(json){
                var count=grid.getGridParam();
                var ts=grid[0];
                if(ts.p.reccount===0){
                    grid.hide();
                    emptyMsgDiv.show();
                    $("#listcaserud div.ui-paging-info").css("display","none")
                    }else{
                    grid.show();
                    emptyMsgDiv.hide();
                    $("#listcaserud div.ui-paging-info").css("display",
                        "block")
                    }
                }
        });
emptyMsgDiv.insertAfter(grid.parent());
emptyMsgDiv.hide();
$("#list_case").jqGrid("navGrid","#listcaserud",{
    reload:true,
    edit:false,
    add:false,
    search:false,
    del:false
});
myGrid=$("#list_case");
var cm=myGrid[0].p.colModel;
$.each(myGrid[0].grid.headers,function(index,value){
    var cmi=cm[index],colName=cmi.name;
    if(!cmi.sortable&&colName!=="rn"&&colName!=="cb"&&colName!=="subgrid")$("div.ui-jqgrid-sortable",value.el).css({
        cursor:"default"
    })
    })
}else if($("#activity_source_type_id").val()==4){
    $("#source_look_4").show();
    $("#list_case").jqGrid({
        postData:{
            sourceType:$("#activity_source_type_id").val(),
            activity_id:$("#activity_id").val(),
            user_id:$("#contact_name_id").val(),
            mode:$("#mode").val()
            },
        mtype:"POST",
        url:"/view-related-activity-sources",
        datatype:"json",
        colNames:["Case #","Subject","Transaction #","Date","Type","Status","Action(s)"],
        colModel:[{
            name:"Case",
            index:"case_contact_id"
        },{
            name:"Subject",
            index:"case_subject"
        },{
            name:"Transaction",
            index:"user_transaction_id"
        },{
            name:"Date",
            index:"case_start_date"
        },{
            name:"Type",
            index:"case_type"
        },{
            name:"Status",
            index:"case_status"
        },{
            name:"Action(s)",
            sortable:false
        }],
        viewrecords:true,
        sortname:"tbl_case.modified_date",
        sortorder:"DESC",
        rowNum:"",
        pager:false,
        caption:"",
        autowidth:true,
        shrinkToFit:true,
        width:"100%",
        cmTemplate:{
            title:false
        },
        loadComplete:function(json){
            var count=grid.getGridParam();
            var ts=grid[0];
            if(ts.p.reccount===0){
                grid.hide();
                emptyMsgDiv.show();
                $("#listcaserud div.ui-paging-info").css("display","none")
                }else{
                grid.show();
                emptyMsgDiv.hide();
                $("#listcaserud div.ui-paging-info").css("display",
                    "block")
                }
            }
    });
emptyMsgDiv.insertAfter(grid.parent());
emptyMsgDiv.hide();
$("#list_case").jqGrid("navGrid","#listcaserud",{
    reload:true,
    edit:false,
    add:false,
    search:false,
    del:false
});
myGrid=$("#list_case");
var cm=myGrid[0].p.colModel;
$.each(myGrid[0].grid.headers,function(index,value){
    var cmi=cm[index],colName=cmi.name;
    if(!cmi.sortable&&colName!=="rn"&&colName!=="cb"&&colName!=="subgrid")$("div.ui-jqgrid-sortable",value.el).css({
        cursor:"default"
    })
    })
}else if($("#activity_source_type_id").val()==2){
    $("#source_look_2").show();
    $("#list_case").jqGrid({
        postData:{
            sourceType:$("#activity_source_type_id").val(),
            activity_id:$("#activity_id").val(),
            user_id:$("#contact_name_id").val(),
            mode:$("#mode").val()
            },
        mtype:"POST",
        url:"/view-related-activity-sources",
        datatype:"json",
        colNames:["Lead Type","Lead Source","Opportunity Amount","Status","Action(s)"],
        colModel:[{
            name:"Lead Type",
            index:"types.lead_type"
        },{
            name:"Lead Source",
            index:"source.lead_source"
        },{
            name:"Opportunity Amount",
            index:"leads.opportunity_amount"
        },{
            name:"Status",
            index:"leads.status"
        },

        {
            name:"Action(s)",
            sortable:false
        }],
        viewrecords:true,
        sortname:"leads.modified_date",
        sortorder:"DESC",
        rowNum:"",
        pager:false,
        caption:"",
        autowidth:true,
        shrinkToFit:true,
        width:"100%",
        cmTemplate:{
            title:false
        },
        loadComplete:function(json){
            var count=grid.getGridParam();
            var ts=grid[0];
            if(ts.p.reccount===0){
                grid.hide();
                emptyMsgDiv.show();
                $("#listcaserud div.ui-paging-info").css("display","none")
                }else{
                grid.show();
                emptyMsgDiv.hide();
                $("#listcaserud div.ui-paging-info").css("display","block")
                }
            }
    });
emptyMsgDiv.insertAfter(grid.parent());
emptyMsgDiv.hide();
$("#list_case").jqGrid("navGrid","#listcaserud",{
    reload:true,
    edit:false,
    add:false,
    search:false,
    del:false
});
myGrid=$("#list_case");
var cm=myGrid[0].p.colModel;
$.each(myGrid[0].grid.headers,function(index,value){
    var cmi=cm[index],colName=cmi.name;
    if(!cmi.sortable&&colName!=="rn"&&colName!=="cb"&&colName!=="subgrid")$("div.ui-jqgrid-sortable",value.el).css({
        cursor:"default"
    })
    })
}
}
function validateSource(){
    if($("#activity_source_type_id").val()!=""){
        var mode=$("#mode").val();
        if(mode=="create");
    }else $("#create_activity").submit()
        }
function deleteRelatedSource(act_source_id){
    $.colorbox({
        width:"600px",
        href:"#delete_temp",
        height:"230px",
        inline:true
    });
    $("#no_delete").click(function(){
        $.colorbox.close();
        $("#yes_delete").unbind("click");
        return false
        });
    $("#yes_delete").click(function(){
        $("#yes_delete").unbind("click");
        showAjxLoader();
        $.ajax({
            type:"POST",
            data:{
                activity_id:$("#activity_id").val(),
                act_source_id:act_source_id
            },
            url:"/delete-related-activity-source",
            success:function(response){
                var jsonObj=jQuery.parseJSON(response);
                if(jsonObj.status==
                    "success"){
                    hideAjxLoader();
                    jQuery("#list_case").trigger("reloadGrid",[{
                        page:1
                    }]);
                    $("#no_delete").click();
                    $("#success_message").show();
                    setTimeout("$('#success_message').hide();",2E3)
                    }
                }
        })
    })
};