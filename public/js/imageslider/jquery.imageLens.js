(function($){$.fn.imageLens=function(options){var defaults={lensSize:100,borderSize:4,borderColor:"#888"};options=$.
extend(defaults,options);var lensStyle=
"background-position: 0px 0px;width: 800px;height: 300px;float: left;display: none;border-radius: "+"0px;border: "+
String(options.borderSize)+"px solid "+options.borderColor+";background-repeat: no-repeat;position: absolute;";
return this.each(function(){obj=$(this);var offset=$(this).offset(),target=$("<div style='"+lensStyle+"' class='"+
options.lensCss+"'>&nbsp;</div>").appendTo($("body")),targetSize=target.size(),imageSrc=options.imageSrc?options.
imageSrc:$(this).attr("src"),imageTag="<img style='display:none;' src='"+imageSrc+"' />",widthRatio=0,heightRatio=0;$(
imageTag).load(function(){widthRatio=$(this).width()/obj.width();heightRatio=$(this).height()/obj.height()}).appendTo($(
this).parent());target.css({backgroundImage:"url('"+imageSrc+"')"});target.mousemove(setPosition);$(this).mousemove(
setPosition);function setPosition(e){var leftPos=parseInt(e.pageX-offset.left),topPos=parseInt(e.pageY-offset.top);if(
leftPos<0||topPos<0||leftPos>obj.width()||topPos>obj.height())target.hide();else{target.show();leftPos=String(((-e.pageX
+offset.left)*widthRatio+target.width()/2)*1);topPos=String(((-e.pageY+offset.top)*heightRatio+target.height()/2)*1);
target.css({backgroundPosition:leftPos+"px "+topPos+"px"});leftPos=String(e.pageX-target.width()/2);topPos=String(e.
pageY-target.height()/2);target.css({left:leftPos+"px",top:topPos+"px"})}}})}})(jQuery)