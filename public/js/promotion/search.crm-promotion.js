$(function() {
    $("#expiry_date_from").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true});
    $(
            "#expiry_date_to").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true});
    var listPromotionGrid = jQuery(
            "#list_promotion"), emptyMsgDiv = $('<div class="no-record-msz">' + NO_RECORD_FOUND + "</div>");
    $("#list_promotion").jqGrid({
        mtype: "POST", url: "/crm-promotions", datatype: "json", sortable: true, postData: {searchString: $("#search_promotion").serialize(
            )}, colNames: [L_PROMOTION_CODE, L_NAME, L_APPLIED_FOR, L_START_DATE, L_EXPIRY_DATE, L_ACTIONS], colModel: [{name:
                        "Coupon Code", index: "coupon_code"}, {name: "Promotion Name", index: "promotion_name"}, {name: "Promotion Applicable", index:
                        "tbl_pro_mst_applied.applicable"}, {name: "Start Date", index: "start_date"}, {name: "Expiry Date", index: "expiry_date"}, {name: "Action", sortable: false, cmTemplate: {title: false}, classes: "none"}], viewrecords: true,
        sortname: "modified_date", sortorder: "desc", rowNum: numRecPerPage, rowList: pagesArr, pager: "#listpromotionrud", viewrecords:
                true, autowidth: true, shrinkToFit: true, caption: "", width: "100%", cmTemplate: {title: false}, loadComplete: function() {
            hideAjxLoader();
            var count = listPromotionGrid.getGridParam(), ts = listPromotionGrid[0];
            if (ts.p.reccount === 0) {
                listPromotionGrid.hide();
                emptyMsgDiv.show();
                $("#listpromotionrud_right div.ui-paging-info").css("display", "none")
            } else {
                listPromotionGrid.show();
                emptyMsgDiv.hide();
                $("#listpromotionrud_right div.ui-paging-info").css("display", "block")
            }
        }});
    emptyMsgDiv.insertAfter(listPromotionGrid.parent());
    emptyMsgDiv.hide();
    $("#list_promotion").jqGrid("navGrid",
            "#listpromotionrud", {reload: true, edit: false, add: false, search: false, del: false});
    $("#searchpromotionbutton").click(
            function() {
                $(".success-msg").hide();
                $(".success-msg").html("");
                showAjxLoader();
                jQuery("#list_promotion").jqGrid(
                        "setGridParam", {postData: {searchString: $("#search_promotion").serialize()}});
                jQuery("#list_promotion").trigger(
                        "reloadGrid", [{page: 1}]);
                window.location.hash = "#saved_search";
                hideAjxLoader();
                return false
            })
});
function showDate(id,
        divId) {
    if ($("#" + id + " option:selected").text() == "Choose Date Range")
        $("#" + divId).show();
    else
        $("#" + divId).hide()
}
function
        getSavedPromotionSearchResult() {
    $("#search_msg").css("display", "none");
    $("#search_msg").html("");
    if ($("#saved_search").
            val() != "") {
        $("#savebutton").val("Update");
        $("#deletebutton").show()
    } else if ($("#saved_search").val() == "") {
        $(
                "#savebutton").val("Save");
        $("#deletebutton").hide();
        $("#search_name").val("");
        return false
    }
    showAjxLoader();
    $.ajax({type
                : "POST", data: {search_id: $("#saved_search").val()}, url: "/get-promotion-search-saved-param", success: function(response) {
            hideAjxLoader();
            if (!checkUserAuthenticationAjx(response))
                return false;
            var searchParamArray = response, obj = jQuery.parseJSON
                    (searchParamArray);
            document.getElementById("search_promotion").reset();
            $("#coupon_code").val(obj.coupon_code);
            $(
                    "#promotion_name").val(obj.promotion_name);
            $("#user").val(obj.user);
            $("#group_name").val(obj.group_name);
            $("#discount").
                    val(obj.discount);
            $("#applicable_id").val(obj.applicable_id).select2();
            $("#expiry_date_range").val(obj.expiry_date_range
                    ).select2();
            $("#expiry_date_from").val(obj.expiry_date_from);
            $("#expiry_date_to").val(obj.expiry_date_to);
            $(
                    "#search_name").val(obj.search_title);
            $("#search_id").val($("#saved_search").val());
            showDate("expiry_date_range",
                    "date_range_div");
            $("#searchpromotionbutton").click()
        }})
}
$("#savebutton").click(function() {
    if ($.trim($("#search_name").
            val()) != "") {
        $("#search_msg").html("");
        $("#search_msg").css("display", "none");
        showAjxLoader();
        $.ajax({type: "POST", data: {
                search_name: $.trim($("#search_name").val()), searchString: $("#search_promotion").serialize(), search_id: $("#search_id").
                        val()}, url: "/save-promotion-search", success: function(response) {
                hideAjxLoader();
                if (!checkUserAuthenticationAjx(response))
                    return false;
                var jsonObj = jQuery.parseJSON(response);
                if (jsonObj.status == "success") {
                    $("#search_name").val("");
                    document.
                            getElementById("search_promotion").reset();
                    location.reload()
                } else {
                    $("#search_msg").html(ERROR_SAVING_SEARCH);
                    $(
                            "#search_msg").addClass("error");
                    $("#search_msg").removeClass("success-msg clear");
                    $("#search_msg").css("display",
                            "block")
                }
            }})
    } else {
        $("#search_msg").html(SEARCH_NAME_MSG);
        $("#search_msg").addClass("error");
        $("#search_msg").removeClass
                ("success-msg clear");
        $("#search_msg").css("display", "block");
        return false
    }
});
function deleteSaveSearch() {
    $(
            ".delete_saved_search").colorbox({width: "700px", height: "200px", inline: true});
    $("#no_delete").click(function() {
        $.colorbox
                .close();
        $("#yes_delete").unbind("click");
        return false
    });
    $("#yes_delete").click(function() {
        $("#yes_delete").unbind(
                "click");
        showAjxLoader();
        $.ajax({type: "POST", data: {search_id: $("#search_id").val()}, url: "/delete-crm-search-promotion",
            success: function(response) {
                hideAjxLoader();
                if (!checkUserAuthenticationAjx(response))
                    return false;
                var jsonObj = jQuery.
                        parseJSON(response);
                if (jsonObj.status == "success")
                    location.reload()
            }})
    })
}
function deletePromotion(promotionId) {
    $(
            ".delete_promotion").colorbox({width: "700px", height: "200px", inline: true});
    $("#no_delete_promotion").click(function() {
        $.
                colorbox.close();
        $("#yes_delete_promotion").unbind("click");
        return false
    });
    $("#yes_delete_promotion").click(function() {
        $
                .colorbox.close();
        $("#yes_delete_promotion").unbind("click");
        showAjxLoader();
        $.ajax({type: "POST", dataType: "html", data: {
                promotionId: promotionId}, url: "/delete-crm-promotion", success: function(responseData) {
                hideAjxLoader();
                if (!
                        checkUserAuthenticationAjx(responseData))
                    return false;
                var jsonObj = JSON.parse(responseData);
                if (jsonObj.status == "success")
                    window.location.href = "/crm-promotions";
                else
                    $("#error_message").html(jsonObj.message)
            }})
    })
}

function deleteProductPromotion(promotionId) {
    $(
            ".delete_promotion").colorbox({width: "700px", height: "200px", inline: true});
    $("#no_delete_promotion").click(function() {
        $.
                colorbox.close();
        $("#yes_delete_promotion").unbind("click");
        return false
    });
    $("#yes_delete_promotion").click(function() {
        $
                .colorbox.close();
        $("#yes_delete_promotion").unbind("click");
        showAjxLoader();
        $.ajax({type: "POST", dataType: "html", data: {
                promotionId: promotionId}, url: "/delete-crm-product-promotion", success: function(responseData) {
                hideAjxLoader();
                if (!
                        checkUserAuthenticationAjx(responseData))
                    return false;
                var jsonObj = JSON.parse(responseData);
                if (jsonObj.status == "success")
                    window.location.href = "/crm-promotions";
                else
                    $("#error_message").html(jsonObj.message)
            }})
    })
}