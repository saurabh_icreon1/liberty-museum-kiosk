$(function(){
    $("#expiry_date").datepicker({
        changeMonth:true,
        changeYear:true,
        onClose:function(selectedDate){
            $(
                "#start_date").datepicker("option","maxDate",selectedDate)
        }
    });
    $("#start_date").datepicker({
        changeMonth:true,
        changeYear:
        true,
        onClose:function(selectedDate){
            $("#expiry_date").datepicker("option","minDate",selectedDate)
        }
    });
    jQuery.validator.
    addMethod("alphanumericunderscore",function(value,element){
        return this.optional(element)||/^[A-Za-z0-9 -]+$/i.test(value
            )
    },ALPHA_NUMERIC_HIFEN);
    jQuery.validator.addMethod("alphanumericspecialchar",function(value,element){
        return this.
        optional(element)|| /^[A-Za-z0-9 ~@!#$%&*?{}+,.\-_]+$/i.test(value)
    },ALPHA_NUMERIC_SPECIALSYMPOL);
    jQuery.validator.addMethod(
        "floating",function(value,element){
            return this.optional(element)||/^-?\d*(\.\d+)?$/.test(value)
        },NUMERIC_VALUE);
    $("#create_promotion").validate({
        submitHandler:function(){
            var dataStr=$("#create_promotion").serialize();
			showAjxLoader();
            $.ajax({
                type:"POST",
                url:"/add-crm-promotion",
                data:dataStr,
                success:function(responseData){
                    hideAjxLoader();
                    if(!
                        checkUserAuthenticationAjx(responseData))return false;
                    var jsonObj=jQuery.parseJSON(responseData);
                    if(jsonObj.status==
                        "success")window.location.href="/crm-promotions";else $.each(jsonObj.message,function(i,msg){
                        $("#"+i).after(
                            '<label class="error" style="display:block;">'+msg+"</label>")
                    })
                }
            })
        },
        rules:{
            promotion_name:{
                required:true,
                alphanumericspecialchar:true,
                minlength:2,
                maxlength:60
            },
            promotion_description:{
                maxlength:500
            },
			promotion_display:{
				required:true,
			},
			usage_limit_type:{
				required: true,
			},
			usage_limit:{
				required: function(){
					if($("input[name='usage_limit_type']:checked").val() == '3'){
						return true;
					}else{
						return false;
					}
				}
			},
			is_shipping_discount:{
				required: function(){
					if(($("#is_shipping_discount").is(":checked") == false) && ($("#is_cart_discount").is(":checked") == false) && ($("#is_free_product").is(":checked") == false)){
						return true;
					}else{
						return false;
					}
				}
			},
			promotion_qualified_by:{
				required: true,
			},
			shipping_discount_type:{
				required: function(){
					if($('#shipping_discount_type_div').find("input[type='radio']:checked").length == '0'){
						return true;
					}else{
						return false;
					}
				},
			},
			shipping_discount_amount:{
				required: function(){
					if($('#shipping_discount_type_div').find("input[type='radio']:checked").val() == '2'){
						return true;
					}else{
						return false;
					}
				}
			},
			'shipping_address_location[]':{
				required: function(){
					if($('#free_shipping_location').find("input[type='checkbox']:checked").length == '0'){
						return true;
					}else{
						return false;
					}
				}
			},
			'pb_shipping_type_id[]':{
				required: function(){
					if($('#free_shipping_methods').find("input[type='checkbox']:checked").length == '0'){
						return true;
					}else{
						return false;
					}
				}
			},
			cart_discount_type:{
				required: function(){
					if($('#cart_discount_type_div').find("input[type='radio']:checked").length == '0'){
						return true;
					}else{
						return false;
					}
				},
			},					
			cart_discount_percent:{
				required: function(){
					if($('#cart_discount_type_div').find("input[type='radio']:checked").val() == '1'){
						return true;
					}else{
						return false;
					}
				}
			},
			cart_discount_amount:{
				required: function(){
					if($('#cart_discount_type_div').find("input[type='radio']:checked").val() == '2'){
						return true;
					}else{
						return false;
					}
				}
			},
			promotion_qualified_by:{
				required: function(){
					if($("input[name='promotion_qualified_by']:checked").length == '0'){
						return true;
					}else{
						return false;
					}
				}
			},
			promotion_qualified_min_amount:{
				required: function(){
					if($("input[name='promotion_qualified_by']:checked").val() == '1'){
						return true;
					}else{
						return false;
					}
				}
			}
        },
        messages:{
            promotion_name:{
                required:PROMOTION_NAME_EMPTY
            },
			promotion_display:{
				required:PROMOTION_DISPLAY_EMPTY,
			},
			usage_limit_type:{
				required: USAGE_LIMIT_DISPLAY_EMPTY,
			},
			usage_limit:{
				required: NUMBER_AVAILABLE_EMPTY,
			},
			is_shipping_discount:{
				required: PROMOTION_PROVIDES_EMPTY,
			},
			promotion_qualified_by:{
				required: PROMOTION_QUALIFIED_BY_EMPTY,
			},
			shipping_discount_type:{
				required: SHIPPING_DISCOUNT_TYPE_EMPTY,
			},
			shipping_discount_amount:{
				required: SHIPPING_DISCOUNT_AMOUNT_EMPTY,
			},
			'shipping_address_location[]':{
				required: SHIPPING_DISCOUNT_LOCATION_EMPTY,
			},
			'pb_shipping_type_id[]':{
				required: DOMESTIC_SHIPPING_METHODS_EMPTY
			},
			cart_discount_type:{
				required: CART_DISCOUNT_TYPE_EMPTY,
			},
			cart_discount_percent:{
				required: CART_DISCOUNT_PERCENT_EMPTY,
			},
			cart_discount_amount:{
				required: CART_DISCOUNT_AMOUNT_EMPTY,
			},
			promotion_qualified_by:{
				required: PROMOTION_QUALIFIED_BY_EMPTY,
			},
			promotion_qualified_min_amount:{
				required: PROMOTION_QUALIFIED_MIN_AMOUNT_EMPTY
			}
        }
    });
    $("#edit_promotion").validate({
        submitHandler:function(){
            /*if($( "#is_domestic:checked").val() != 1 && $("#is_international:checked").val() != 1){
                $("#error_domestic").show();                
                return false;
            }
            else {
                $("#error_domestic").hide();                
            }*/
            var dataStr=$("#edit_promotion").serialize();
            showAjxLoader();
            $.ajax({
                type:"POST",
                url:"/edit-crm-product-promotion",
                data:
                dataStr,
                success:function(responseData){
                    hideAjxLoader();
                    if(!checkUserAuthenticationAjx(responseData))return false;
                    var 
                    jsonObj=jQuery.parseJSON(responseData);
                    if(jsonObj.status=="success")window.location.href="/crm-promotions";else $.each(
                        jsonObj.message,function(i,msg){
                            $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                        })
                }
            })
        },
        rules:{
            promotion_name:{
                required:true,
                alphanumericspecialchar:true,
                minlength:2,
                maxlength:60
            },
            promotion_description:{
                maxlength:500
            },
			promotion_display:{
				required:true,
			},
			usage_limit_type:{
				required: true,
			},
			usage_limit:{
				required: function(){
					if($("input[name='usage_limit_type']:checked").val() == '3'){
						return true;
					}else{
						return false;
					}
				}
			},
			is_shipping_discount:{
				required: function(){
					if(($("#is_shipping_discount").is(":checked") == false) && ($("#is_cart_discount").is(":checked") == false) && ($("#is_free_product").is(":checked") == false)){
						return true;
					}else{
						return false;
					}
				}
			},
			promotion_qualified_by:{
				required: true,
			},
			shipping_discount_type:{
				required: function(){
					if($('#shipping_discount_type_div').find("input[type='radio']:checked").length == '0'){
						return true;
					}else{
						return false;
					}
				},
			},
			shipping_discount_amount:{
				required: function(){
					if($('#shipping_discount_type_div').find("input[type='radio']:checked").val() == '2'){
						return true;
					}else{
						return false;
					}
				}
			},
			'shipping_address_location[]':{
				required: function(){
					if($('#free_shipping_location').find("input[type='checkbox']:checked").length == '0'){
						return true;
					}else{
						return false;
					}
				}
			},
			'pb_shipping_type_id[]':{
				required: function(){
					if($('#free_shipping_methods').find("input[type='checkbox']:checked").length == '0'){
						return true;
					}else{
						return false;
					}
				}
			},
			cart_discount_type:{
				required: function(){
					if($('#cart_discount_type_div').find("input[type='radio']:checked").length == '0'){
						return true;
					}else{
						return false;
					}
				},
			},					
			cart_discount_percent:{
				required: function(){
					if($('#cart_discount_type_div').find("input[type='radio']:checked").val() == '1'){
						return true;
					}else{
						return false;
					}
				}
			},
			cart_discount_amount:{
				required: function(){
					if($('#cart_discount_type_div').find("input[type='radio']:checked").val() == '2'){
						return true;
					}else{
						return false;
					}
				}
			},
			promotion_qualified_by:{
				required: function(){
					if($("input[name='promotion_qualified_by']:checked").length == '0'){
						return true;
					}else{
						return false;
					}
				}
			},
			promotion_qualified_min_amount:{
				required: function(){
					if($("input[name='promotion_qualified_by']:checked").val() == '1'){
						return true;
					}else{
						return false;
					}
				}
			}
        },
        messages:{
            promotion_name:{
                required:PROMOTION_NAME_EMPTY
            },
			promotion_display:{
				required:PROMOTION_DISPLAY_EMPTY,
			},
			usage_limit_type:{
				required: USAGE_LIMIT_DISPLAY_EMPTY,
			},
			usage_limit:{
				required: NUMBER_AVAILABLE_EMPTY,
			},
			is_shipping_discount:{
				required: PROMOTION_PROVIDES_EMPTY,
			},
			promotion_qualified_by:{
				required: PROMOTION_QUALIFIED_BY_EMPTY,
			},
			shipping_discount_type:{
				required: SHIPPING_DISCOUNT_TYPE_EMPTY,
			},
			shipping_discount_amount:{
				required: SHIPPING_DISCOUNT_AMOUNT_EMPTY,
			},
			'shipping_address_location[]':{
				required: SHIPPING_DISCOUNT_LOCATION_EMPTY,
			},
			'pb_shipping_type_id[]':{
				required: DOMESTIC_SHIPPING_METHODS_EMPTY
			},
			cart_discount_type:{
				required: CART_DISCOUNT_TYPE_EMPTY,
			},
			cart_discount_percent:{
				required: CART_DISCOUNT_PERCENT_EMPTY,
			},
			cart_discount_amount:{
				required: CART_DISCOUNT_AMOUNT_EMPTY,
			},
			promotion_qualified_by:{
				required: PROMOTION_QUALIFIED_BY_EMPTY,
			},
			promotion_qualified_min_amount:{
				required: PROMOTION_QUALIFIED_MIN_AMOUNT_EMPTY
			}
        }
    });
    $("#user").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:
        function(request,response){
            $.ajax({
                url:"/get-contacts",
                method:"POST",
                dataType:"json",
                data:{
                    name_email:$("#user").val()
                },
                success:function(jsonResult){
                    if(!checkUserAuthenticationAjx(jsonResult))return false;
                    var resultset=[];
                    $.each(jsonResult,
                        function(){
                            var id=this.user_id,value=this.full_name;
							if(this.email_id!='') 
								var full_name = this.full_name+' ('+this.email_id+')';
							else
								var full_name = this.full_name;
                            resultset.push({
                                id:this.user_id,
                                value:full_name
                            })
                        });
                    response(
                        resultset)
                }
            })
        },
        open:function(){
            $(".ui-menu").width(350)
        },
        change:function(event,ui){
            $("#user").val("")
        },
        select:function(
            event,ui){
            var user=ui.item.value,userId=ui.item.id,size=$("#user"+userId).size();
            if(size==0){
                $("#select_user").append(
                    "<li id='user"+userId+"'>"+user+"<span onclick='removeAutoCompleteValue("+userId+',"user")\'>x</span></li>');
                var 
                userIdsVal=$("#user_ids").val();
                if(userIdsVal=="")$("#user_ids").val(userId);else $("#user_ids").val(userIdsVal+","+
                    userId)
            }
            $("#user").val("");
            return false
        }
    });
    $("#group_name").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:function(request,response){
            $.ajax({
                url:"/get-groups",
                method:"POST",
                dataType:"json",
                data:{
                    group_name:$(
                        "#group_name").val()
                },
                success:function(jsonResult){
                    if(!checkUserAuthenticationAjx(jsonResult))return false;
                    var resultset
                    =[];
                    $.each(jsonResult,function(){
                        resultset.push({
                            id:this.group_id,
                            value:this.group_name
                        })
                    });
                    response(resultset)
                }
            })
        },
        open
        :function(){
            $(".ui-menu").width(350)
        },
        change:function(event,ui){
            $("#group_name").val("")
        },
        select:function(event,ui){
            var 
            group=ui.item.value,groupId=ui.item.id,size=$("#group"+groupId).size();
            if(size==0){
                $("#select_group").append(
                    "<li id='group"+groupId+"'>"+group+"<span onclick='removeAutoCompleteValue("+groupId+',"group")\'>x</span></li>');
                var 
                groupIdsVal=$("#group_name_ids").val();
                if(groupIdsVal=="")$("#group_name_ids").val(groupId);else $("#group_name_ids").
                    val(groupIdsVal+","+groupId)
            }
            $("#group_name").val("");
            return false
        }
    });
    $("#product_name").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:function(request,response){
            $.ajax({
                url:"/get-products",
                method:"POST",
                dataType:
                "json",
                data:{
                    product_name:$("#product_name").val(),
                    is_promotion_check:1
                },
                success:function(jsonResult){
                    if(!
                        checkUserAuthenticationAjx(jsonResult))return false;
                    var resultset=[];
                    $.each(jsonResult,function(){
                        resultset.push({
                            id:
                            this.product_id,
                            value:this.product_name
                        })
                    });
                    response(resultset)
                }
            })
        },
        open:function(){
            $(".ui-menu").width(350)
        },
        change:
        function(event,ui){
            $("#product_name").val("")
        },
        select:function(event,ui){
            var product=ui.item.value,productId=ui.item.id,
            size=$("#product"+productId).size();
            if(size==0){
                $("#select_product").append("<li id='product"+productId+"'>"+product+
                    "<span onclick='removeAutoCompleteValue("+productId+',"product")\'>x</span></li>');
                var productIdsVal=$(
                    "#product_name_ids").val();
                if(productIdsVal=="")$("#product_name_ids").val(productId);else $("#product_name_ids").val(
                    productIdsVal+","+productId)
            }
            $("#product_name").val("");
            return false
        }
    });
    $("#category_name").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:function(request,response){
            $.ajax({
                url:"/get-product-types",
                method:"POST",
                dataType:"json",
                data:{
                    product_type:$("#category_name").val()
                },
                success:function(jsonResult){
                    if(!
                        checkUserAuthenticationAjx(jsonResult))return false;
                    var resultset=[];
                    $.each(jsonResult,function(){
                        resultset.push({
                            id:
                            this.product_type_id,
                            value:this.product_type
                        })
                    });
                    response(resultset)
                }
            })
        },
        open:function(){
            $(".ui-menu").width(350)
        },
        change:function(event,ui){
            $("#category_name").val("")
        },
        select:function(event,ui){
            var category=ui.item.value,categoryId=
            ui.item.id,size=$("#category"+categoryId).size();
            if(size==0){
                $("#select_category").append("<li id='category"+categoryId+
                    "'>"+category+"<span onclick='removeAutoCompleteValue("+categoryId+',"category")\'>x</span></li>');
                var categoryIdsVal=$(
                    "#category_name_ids").val();
                if(categoryIdsVal=="")$("#category_name_ids").val(categoryId);else $("#category_name_ids").
                    val(categoryIdsVal+","+categoryId)
            }
            $("#category_name").val("");
            return false
        }
    });
    $("#is_free_shipping").change(function(){
        if($(this).is(":checked"))$("#freeShipAmount").show();else $("#freeShipAmount").hide()
    });


	$("#free_product_name").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:
        function(request,response){
            $.ajax({
                url:"/get-promotional-products",
                method:"POST",
                dataType:"json",
                data:{
                    product_name:$("#free_product_name").val()
                },
                success:function(jsonResult){
                    if(!checkUserAuthenticationAjx(jsonResult))return false;
                    var resultset=[];
                    $.each(jsonResult,
                        function(){
                            var id=this.product_id,value=this.product_name;
                            resultset.push({
                                id:this.product_id,
                                value:this.product_name
                            })
                        });
                    response(
                        resultset)
                }
            })
        },
        open:function(){
            $(".ui-menu").width(350);
        },
        change:function(event,ui){
            $("#free_product_name").val("");
        },
        select:function(
            event,ui){
            var product=ui.item.value,productId=ui.item.id,size=$("#free_product"+productId).size();
            if(size==0){
                $("#select_free_product").append(
                    "<li id='free_product"+productId+"'>"+product+"<span onclick='removeAutoCompleteValue("+productId+',"free_product")\'>x</span></li>');
                var 
                productIdsVal=$("#free_products").val();
                if(productIdsVal=="")$("#free_products").val(productId);else $("#free_products").val(productIdsVal+","+
                    productId)
            }
            $("#free_product_name").val("");
            return false
        }
    });


	$("#excluded_product_name").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:
        function(request,response){
            $.ajax({
                url:"/get-promotional-products",
                method:"POST",
                dataType:"json",
                data:{
                    product_name:$("#excluded_product_name").val()
                },
                success:function(jsonResult){
                    if(!checkUserAuthenticationAjx(jsonResult))return false;
                    var resultset=[];
                    $.each(jsonResult,
                        function(){
                            var id=this.product_id,value=this.product_name;
                            resultset.push({
                                id:this.product_id,
                                value:this.product_name
                            })
                        });
                    response(
                        resultset)
                }
            })
        },
        open:function(){
            $(".ui-menu").width(350);
        },
        change:function(event,ui){
            $("#excluded_product_name").val("");
        },
        select:function(
            event,ui){
            var product=ui.item.value,productId=ui.item.id,size=$("#excluded_product"+productId).size();
            if(size==0){
                $("#select_excluded_product").append(
                    "<li id='excluded_product"+productId+"'>"+product+"<span onclick='removeAutoCompleteValue("+productId+',"excluded_product")\'>x</span></li>');
                var 
                productIdsVal=$("#excluded_products").val();
                if(productIdsVal=="")$("#excluded_products").val(productId);else $("#excluded_products").val(productIdsVal+","+
                    productId)
            }
            $("#excluded_product_name").val("");
            return false
        }
    });

	$("#excluded_product_type_name").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:
        function(request,response){
            $.ajax({
                url:"/get-product-types",
                method:"POST",
                dataType:"json",
                data:{
                    product_type:$("#excluded_product_type_name").val()
                },
                success:function(jsonResult){
                    if(!checkUserAuthenticationAjx(jsonResult))return false;
                    var resultset=[];
                    $.each(jsonResult,
                        function(){
                            var id=this.product_type_id,value=this.product_type;
                            resultset.push({
                                id:this.product_type_id,
                                value:this.product_type
                            })
                        });
                    response(
                        resultset)
                }
            })
        },
        open:function(){
            $(".ui-menu").width(350);
        },
        change:function(event,ui){
            $("#excluded_product_type_name").val("");
        },
        select:function(
            event,ui){
            var productType=ui.item.value,productTypeId=ui.item.id,size=$("#excluded_product_type"+productTypeId).size();
            if(size==0){
                $("#select_excluded_product_type").append(
                    "<li id='excluded_product_type"+productTypeId+"'>"+productType+"<span onclick='removeAutoCompleteValue("+productTypeId+',"excluded_product_type")\'>x</span></li>');
                var 
                productTypeIdsVal=$("#excluded_product_type").val();
                if(productTypeIdsVal=="")$("#excluded_product_type").val(productTypeId);else $("#excluded_product_type").val(productTypeIdsVal+","+
                    productTypeId)
            }
            $("#excluded_product_type_name").val("");
            return false
        }
    });


	$("#bundle_product_name").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:
        function(request,response){
            $.ajax({
                url:"/get-promotional-products",
                method:"POST",
                dataType:"json",
                data:{
                    product_name:$("#bundle_product_name").val()
                },
                success:function(jsonResult){
                    if(!checkUserAuthenticationAjx(jsonResult))return false;
                    var resultset=[];
                    $.each(jsonResult,
                        function(){
                            var id=this.product_id,value=this.product_name;
                            resultset.push({
                                id:this.product_id,
                                value:this.product_name
                            })
                        });
                    response(
                        resultset)
                }
            })
        },
        open:function(){
            $(".ui-menu").width(350);
        },
        change:function(event,ui){
            $("#bundle_product_name").val("");
        },
        select:function(
            event,ui){
            var product=ui.item.value,productId=ui.item.id,size=$("#bundle_product"+productId).size();
            if(size==0){
                $("#select_bundle_product").append(
                    "<li id='bundle_product"+productId+"'>"+product+"<span onclick='removeAutoCompleteValue("+productId+',"bundle_product")\'>x</span></li>');
                var 
                productIdsVal=$("#bundle_products").val();
                if(productIdsVal=="")$("#bundle_products").val(productId);else $("#bundle_products").val(productIdsVal+","+
                    productId)
            }
            $("#bundle_product_name").val("");
            return false
        }
    });

	$("#bundle_product_type_name").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:
        function(request,response){
            $.ajax({
                url:"/get-product-types",
                method:"POST",
                dataType:"json",
                data:{
                    product_type:$("#bundle_product_type_name").val()
                },
                success:function(jsonResult){
                    if(!checkUserAuthenticationAjx(jsonResult))return false;
                    var resultset=[];
                    $.each(jsonResult,
                        function(){
                            var id=this.product_type_id,value=this.product_type;
                            resultset.push({
                                id:this.product_type_id,
                                value:this.product_type
                            })
                        });
                    response(
                        resultset)
                }
            })
        },
        open:function(){
            $(".ui-menu").width(350);
        },
        change:function(event,ui){
            $("#bundle_product_type_name").val("");
        },
        select:function(
            event,ui){
            var productType=ui.item.value,productTypeId=ui.item.id,size=$("#bundle_product_type"+productTypeId).size();
            if(size==0){
                $("#select_bundle_product_type").append(
                    "<li id='bundle_product_type"+productTypeId+"'>"+productType+"<span onclick='removeAutoCompleteValue("+productTypeId+',"bundle_product_type")\'>x</span></li>');
                var 
                productTypeIdsVal=$("#bundle_product_type").val();
                if(productTypeIdsVal=="")$("#bundle_product_type").val(productTypeId);else $("#bundle_product_type").val(productTypeIdsVal+","+
                    productTypeId)
            }
            $("#bundle_product_type_name").val("");
            return false
        }
    });

	$("#minimum_amount_product_name").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:
        function(request,response){
            $.ajax({
                url:"/get-promotional-products",
                method:"POST",
                dataType:"json",
                data:{
                    product_name:$("#minimum_amount_product_name").val()
                },
                success:function(jsonResult){
                    if(!checkUserAuthenticationAjx(jsonResult))return false;
                    var resultset=[];
                    $.each(jsonResult,
                        function(){
                            var id=this.product_id,value=this.product_name;
                            resultset.push({
                                id:this.product_id,
                                value:this.product_name
                            })
                        });
                    response(
                        resultset)
                }
            })
        },
        open:function(){
            $(".ui-menu").width(350);
        },
        change:function(event,ui){
            $("#minimum_amount_product_name").val("");
        },
        select:function(
            event,ui){
            var product=ui.item.value,productId=ui.item.id,size=$("#minimum_amount_product"+productId).size();
            if(size==0){
                $("#select_minimum_amount_product").append(
                    "<li id='minimum_amount_product"+productId+"'>"+product+"<span onclick='removeAutoCompleteValue("+productId+',"minimum_amount_product")\'>x</span></li>');
                var 
                productIdsVal=$("#minimum_amount_products").val();
                if(productIdsVal=="")$("#minimum_amount_products").val(productId);else $("#minimum_amount_products").val(productIdsVal+","+
                    productId)
            }
            $("#minimum_amount_product_name").val("");
            return false
        }
    });

	$("#minimum_amount_product_type_name").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:
        function(request,response){
            $.ajax({
                url:"/get-product-types",
                method:"POST",
                dataType:"json",
                data:{
                    product_type:$("#minimum_amount_product_type_name").val()
                },
                success:function(jsonResult){
                    if(!checkUserAuthenticationAjx(jsonResult))return false;
                    var resultset=[];
                    $.each(jsonResult,
                        function(){
                            var id=this.product_type_id,value=this.product_type;
                            resultset.push({
                                id:this.product_type_id,
                                value:this.product_type
                            })
                        });
                    response(
                        resultset)
                }
            })
        },
        open:function(){
            $(".ui-menu").width(350);
        },
        change:function(event,ui){
            $("#minimum_amount_product_type_name").val("");
        },
        select:function(
            event,ui){
            var productType=ui.item.value,productTypeId=ui.item.id,size=$("#minimum_amount_product_type"+productTypeId).size();
            if(size==0){
                $("#select_minimum_amount_product_type").append(
                    "<li id='minimum_amount_product_type"+productTypeId+"'>"+productType+"<span onclick='removeAutoCompleteValue("+productTypeId+',"minimum_amount_product_type")\'>x</span></li>');
                var 
                productTypeIdsVal=$("#minimum_amount_product_type").val();
                if(productTypeIdsVal=="")$("#minimum_amount_product_type").val(productTypeId);else $("#minimum_amount_product_type").val(productTypeIdsVal+","+
                    productTypeId)
            }
            $("#minimum_amount_product_type_name").val("");
            return false
        }
    });


	$("#promotion_any_product").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:
        function(request,response){
            $.ajax({
                url:"/get-promotional-products",
                method:"POST",
                dataType:"json",
                data:{
                    product_name:$("#promotion_any_product").val()
                },
                success:function(jsonResult){
                    if(!checkUserAuthenticationAjx(jsonResult))return false;
                    var resultset=[];
                    $.each(jsonResult,
                        function(){
                            var id=this.product_id,value=this.product_name;
                            resultset.push({
                                id:this.product_id,
                                value:this.product_name
                            })
                        });
                    response(
                        resultset)
                }
            })
        },
        open:function(){
            $(".ui-menu").width(350);
        },
        change:function(event,ui){
            $("#promotion_any_product").val("");
        },
        select:function(
            event,ui){
            var product=ui.item.value,productId=ui.item.id,size=$("#promotion_any_product"+productId).size();
            if(size==0){
                $("#select_promotion_any_product").append(
                    "<li id='promotion_any_product"+productId+"'>"+product+"<span onclick='removeAutoCompleteValue("+productId+',"promotion_any_product")\'>x</span></li>');
                var 
                productIdsVal=$("#promotion_any_products").val();
                if(productIdsVal=="")$("#promotion_any_products").val(productId);else $("#promotion_any_products").val(productIdsVal+","+
                    productId)
            }
            $("#promotion_any_product").val("");
            return false
        }
    });

	$("#promotion_any_product_type_name").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:
        function(request,response){
            $.ajax({
                url:"/get-product-types",
                method:"POST",
                dataType:"json",
                data:{
                    product_type:$("#promotion_any_product_type_name").val()
                },
                success:function(jsonResult){
                    if(!checkUserAuthenticationAjx(jsonResult))return false;
                    var resultset=[];
                    $.each(jsonResult,
                        function(){
                            var id=this.product_type_id,value=this.product_type;
                            resultset.push({
                                id:this.product_type_id,
                                value:this.product_type
                            })
                        });
                    response(
                        resultset)
                }
            })
        },
        open:function(){
            $(".ui-menu").width(350);
        },
        change:function(event,ui){
            $("#promotion_any_product_type_name").val("");
        },
        select:function(
            event,ui){
            var productType=ui.item.value,productTypeId=ui.item.id,size=$("#promotion_any_product_type"+productTypeId).size();
            if(size==0){
                $("#select_promotion_any_product_type").append(
                    "<li id='promotion_any_product_type"+productTypeId+"'>"+productType+"<span onclick='removeAutoCompleteValue("+productTypeId+',"promotion_any_product_type")\'>x</span></li>');
                var 
                productTypeIdsVal=$("#promotion_any_product_type").val();
                if(productTypeIdsVal=="")$("#promotion_any_product_type").val(productTypeId);else $("#promotion_any_product_type").val(productTypeIdsVal+","+
                    productTypeId)
            }
            $("#promotion_any_product_type_name").val("");
            return false
        }
    });
});
function onChangeApplicable(
    val){
    if(val==1){
        $("#contact_section,#group_section,#applicable_excluded").css("display","none");
        $(
            ".select_product_category_label2").empty()
    }else if(val==2){
        $("#group_section").css("display","none");
        $(
            "#contact_section").css("display","block");
        $("#applicable_excluded").css("display","block");
        $(
            ".select_product_category_label2").html(L_SELECTED_CONTACT_LIST)
    }else if(val==3){
        $("#group_section").css("display",
            "block");
        $("#contact_section").css("display","none");
        $("#applicable_excluded").css("display","block");
        $(
            ".select_product_category_label2").html(L_SELECTED_GROUP_LIST)
    }
}
function onChangeProductCategory(val){
    $(
        ".select_product_category_label").empty();
    if(val==1){
        $("#category_section,#product_section").css("display","none");
        $(
            ".select_product_category_label").empty()
    }else if(val==2){
        $("#product_section").css("display","none");
        $(
            "#category_section").css("display","block");
        $("#product_category_excluded").css("display","block");
        $(
            ".select_product_category_label").html(L_SELECTED_CATEGORY_LIST)
    }else if(val==3){
        $("#product_section").css("display",
            "block");
        $("#category_section").css("display","none");
        $("#product_category_excluded").css("display","block");
        $(
            ".select_product_category_label").html(L_SELECTED_PRODUCT_LIST)
    }
}
function removeAutoCompleteValue(id,type){
    var module=""
    ,elementId="";
    if(type=="user"){
        module="user";
        elementId="user_ids"
    }else if(type=="group"){
        module="group";
        elementId=
        "group_name_ids"
    }else if(type=="category"){
        module="category";
        elementId="category_name_ids"
    }else if(type=="product"){
        module="product";
        elementId="product_name_ids"
    }else if(type=="free_product"){
        module="free_product";
        elementId="free_products"
    }else if(type=="excluded_product"){
        module="excluded_product";
        elementId="excluded_products"
    }else if(type=="excluded_product_type"){
        module="excluded_product_type";
        elementId="excluded_product_type"
    }else if(type=="bundle_product"){
        module="bundle_product";
        elementId="bundle_products"
    }else if(type=="bundle_product_type"){
        module="bundle_product_type";
        elementId="bundle_product_type"
    }else if(type=="minimum_amount_product"){
        module="minimum_amount_product";
        elementId="minimum_amount_products"
    }else if(type=="minimum_amount_product_type"){
        module="minimum_amount_product_type";
        elementId="minimum_amount_product_type"
    }else if(type=="promotion_any_product"){
        module="promotion_any_product";
        elementId="promotion_any_products"
    }else if(type=="promotion_any_product_type"){
        module="promotion_any_product_type";
        elementId="promotion_any_product_type"
    }
    var idArr=$("#"+elementId).val().split(","),newIdStr="";
    for(i=0;i<idArr.
        length;i++)if(idArr[i]!=id)newIdStr+=idArr[i]+",";newIdStr=newIdStr.substr(0,newIdStr.length-1);
    $("#"+elementId).val(
        newIdStr);
    $("#"+module+""+id).remove()
}
function is_domestic(){
    if($("#is_domestic").val() == 0 && $("#is_international").val() == 0){
        return true;
    }
    else {
        return false;
    }
}

function enableUsageLimit(usageLimitVal){
	if(usageLimitVal=='3'){
		$('#usage_limit').removeAttr('disabled');
		$('#usage_limit_per_contact').removeAttr('disabled');
		$('#usage_limit_display').show();
	}else{
		$('#usage_limit').val('');
		$('#usage_limit').next('label').remove();
		$('#usage_limit').attr('disabled','disabled');
		$('#usage_limit_per_contact').prop('checked', false);
		$('#usage_limit_per_contact').next('label').removeAttr('class');
		$('#usage_limit_per_contact').attr('disabled','disabled');
		$('#usage_limit_display').hide();
	}
}

function enableShippingLocationDiscount(shippingDiscountType){
	if(shippingDiscountType == '2'){
		$('#shipping_discount_amount').removeAttr('disabled');
		
		$('#shipping_address_location_1').prop('checked', false);
		$('#shipping_address_location_1').next('label').removeAttr('class');
		$('#shipping_address_location_2').prop('checked', false);
		$('#shipping_address_location_2').next('label').removeAttr('class');
		
		$('#free_shipping_methods').hide();
		$('#free_shipping_location').hide();
		$('#international_shipping_methods').hide();
		
	}else if(shippingDiscountType == '1'){
		$('#shipping_discount_amount').val('');
		$('#shipping_discount_amount').attr('disabled','disabled');		
		$('#free_shipping_location').show();
	}else{
		$('#shipping_address_location_1').prop('checked', false);
		$('#shipping_address_location_1').next('label').removeAttr('class');

		$('#shipping_address_location_2').prop('checked', false);
		$('#shipping_address_location_2').next('label').removeAttr('class');

		$('#free_shipping_methods').hide();
		$('#international_shipping_methods').hide();
	}
}

function enableShippingMethod(){

	if($('#shipping_address_location_1').is(':checked') == true){		
		if($('#pb_shipping_type_id_d_1').next('label').text() == 'Please select domestic shipping method(s)'){
			$('#pb_shipping_type_id_d_1').next('label').remove();
		}
		$('#free_shipping_methods').show();
	}else if($('#shipping_address_location_1').is(':checked') == false){ 
		$('#free_shipping_methods').find("input[type='checkbox']").next('label').removeAttr('class');
		$('#free_shipping_methods').find("input[type='checkbox']").prop('checked', false);		
		$('#free_shipping_methods').hide();
	}

	if($('#shipping_address_location_2').is(':checked') == true){ 
		//$('#international_shipping_methods').show();
	}else if($('#shipping_address_location_2').is(':checked') == false){ 
		$('#international_shipping_methods').find("input[type='checkbox']").prop('checked', false);
		$('#international_shipping_methods').find("input[type='checkbox']").next('label').removeAttr('class');
		$('#international_shipping_methods').hide();
	}
}

function enableShippingDiscount(){
	if($('#is_shipping_discount').is(':checked') == true){
		$('#shipping_discount_type_div').show();
	}else if($('#is_shipping_discount').is(':checked') == false){
		$("#shipping_discount_type_div").find("input[type='radio']").prop('checked', false);
		$("#shipping_discount_type_div").find("input[type='radio']").next('label').removeAttr('class');
		$('#shipping_discount_amount').val('');
		$('#shipping_discount_amount').attr('disabled','disabled');
		
		
		$('#free_shipping_methods').find("input[type='checkbox']").prop('checked', false);
		$('#free_shipping_methods').find("input[type='checkbox']").next('label').removeAttr('class');
		$('#free_shipping_methods').hide();
		$('#free_shipping_location').hide();
		

		$('#international_shipping_methods').find("input[type='checkbox']").prop('checked', false);
		$('#international_shipping_methods').find("input[type='checkbox']").next('label').removeAttr('class');
		$('#international_shipping_methods').hide();


		$('#shipping_address_location_1').prop('checked', false);
		$('#shipping_address_location_1').next('label').removeAttr('class');

		$('#shipping_address_location_2').prop('checked', false);
		$('#shipping_address_location_2').next('label').removeAttr('class');
		
		
		$('#shipping_discount_type_div').hide();
	}
}

function enableCartDiscount(){
	if($('#is_cart_discount').is(':checked') == true){
		$('#cart_discount_type_div').show();
	}else if($('#is_cart_discount').is(':checked') == false){
		$("#cart_discount_type_div").find("input[type='radio']").prop('checked', false);
		$("#cart_discount_type_div").find("input[type='radio']").next('label').removeAttr('class');

		$('#cart_discount_amount').val('');
		$('#cart_discount_amount').attr('disabled','disabled');

		$('#cart_discount_percent').val('');
		$('#cart_discount_percent').attr('disabled','disabled');

		$('#cart_discount_type_div').hide();
	}
}

function enableCartDiscountAmount(cartDiscountType){
	if(cartDiscountType == '1'){
		$('#cart_discount_percent').removeAttr('disabled');

		$('#cart_discount_amount').val('');
		$('#cart_discount_amount').attr('disabled','disabled');
	}else if(cartDiscountType == '2'){
		$('#cart_discount_percent').val('');
		$('#cart_discount_percent').attr('disabled','disabled');

		$('#cart_discount_amount').removeAttr('disabled');
	}
}

function enableFreeProducts(){
	if($('#is_free_product').is(':checked') == true){
		$('#free_product_section').show();
	}else if($('#is_free_product').is(':checked') == false){
		$('#free_products').val('');
		$('ul#select_free_product').empty();
		$('#free_product_section').hide();
	}
}

function enablePromotionQualifies(promotionQualifiesBy){
	if(promotionQualifiesBy == '1'){
		
		$('#promotion_qualified_min_amount').removeAttr('disabled');

		$('#minimum_amount_product_type_name').removeAttr('disabled');
		$('#minimum_amount_product_name').removeAttr('disabled');

		
		$('#bundle_product_type').val('');
		$('ul#select_bundle_product_type').empty();

		$('#bundle_products').val('');
		$('ul#select_bundle_product').empty();

		$('#bundle_product_section').hide();
		
		
		
		$('#promotion_any_product_type').val('');
		$('ul#select_promotion_any_product_type').empty();

		$('#promotion_any_products').val('');
		$('ul#select_promotion_any_product').empty();

		$('#promotion_any_section').hide();

	}else if(promotionQualifiesBy == '2'){
		$('#promotion_qualified_min_amount').val('');
		$('#promotion_qualified_min_amount').attr('disabled','disabled');

		$('#minimum_amount_product_type').val('');
		$('#minimum_amount_product_type_name').attr('disabled','disabled');

		$('#minimum_amount_products').val('');
		$('#minimum_amount_product_name').attr('disabled','disabled');

		$('ul#select_minimum_amount_product_type').empty();
		$('ul#select_minimum_amount_product').empty();

		$('#bundle_product_section').show();	
		
		
		$('#promotion_any_product_type').val('');
		$('ul#select_promotion_any_product_type').empty();

		$('#promotion_any_products').val('');
		$('ul#select_promotion_any_product').empty();

		$('#promotion_any_section').hide();

	}else if(promotionQualifiesBy == '3'){
		$('#promotion_qualified_min_amount').val('');
		$('#promotion_qualified_min_amount').attr('disabled','disabled');

		$('#minimum_amount_product_type').val('');
		$('#minimum_amount_product_type_name').attr('disabled','disabled');

		$('#minimum_amount_products').val('');
		$('#minimum_amount_product_name').attr('disabled','disabled');

		$('ul#select_minimum_amount_product_type').empty();
		$('ul#select_minimum_amount_product').empty();

		$('#bundle_product_type').val('');
		$('ul#select_bundle_product_type').empty();

		$('#bundle_products').val('');
		$('ul#select_bundle_product').empty();

		$('#bundle_product_section').hide();
		
		
		
		$('#promotion_any_section').show();

	}else if(promotionQualifiesBy == '4' || promotionQualifiesBy == ''){
		$('#promotion_qualified_min_amount').val('');
		$('#promotion_qualified_min_amount').attr('disabled','disabled');

		$('#minimum_amount_product_type').val('');
		$('#minimum_amount_product_type_name').attr('disabled','disabled');

		$('#minimum_amount_products').val('');
		$('#minimum_amount_product_name').attr('disabled','disabled');

		$('ul#select_minimum_amount_product_type').empty();
		$('ul#select_minimum_amount_product').empty();

		$('#bundle_product_type').val('');
		$('ul#select_bundle_product_type').empty();

		$('#bundle_products').val('');
		$('ul#select_bundle_product').empty();

		$('#bundle_product_section').hide();
		
		
		
		$('#promotion_any_product_type').val('');
		$('ul#select_promotion_any_product_type').empty();

		$('#promotion_any_products').val('');
		$('ul#select_promotion_any_product').empty();

		$('#promotion_any_section').hide();
	}
}