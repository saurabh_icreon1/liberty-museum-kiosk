$(function() {
    if (mode == "view")
        getPromotionDetail();
    else
        editPromotionDetails();
    $("a[id^='tab-']").removeClass("active");
    var tab_id_load = mode == "view" ? "1" : "2";
    $("#tab-" + tab_id_load).addClass("active");
    $("a[id^='tab-']").click(function() {
        $(
                "a[id^='tab-']").removeClass("active");
        $(this).addClass("active")
    })
});
function getPromotionDetail() {
    showAjxLoader();
    $.
            ajax({type: "POST", data: {}, url: "/view-crm-promotion-info/" + encryptPromotionId, success: function(response) {
                    hideAjxLoader();
                    if (!checkUserAuthenticationAjx(response))
                        return false;
                    $("#promotionContent").html(response)
                }})
}
function
        editPromotionDetails() {
    showAjxLoader();
    $.ajax({type: "POST", data: {}, url: "/edit-crm-promotion-detail/" + encryptPromotionId,
        success: function(response) {
            hideAjxLoader();
            if (!checkUserAuthenticationAjx(response))
                return false;
            $("#promotionContent").
                    html(response)
        }})
}
function viewChangeLog() {
    showAjxLoader();
    $.ajax({type: "POST", data: {module: moduleName, id:
            encryptPromotionId}, url: "/promotion-change-log/" + moduleName + "/" + encryptPromotionId, success: function(response) {
            hideAjxLoader();
            if (!checkUserAuthenticationAjx(response))
                return false;
            $("#promotionContent").html(response)
        }})
}