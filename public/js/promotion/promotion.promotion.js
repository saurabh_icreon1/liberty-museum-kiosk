$(function(){
    $("#expiry_date").datepicker({
        changeMonth:true,
        changeYear:true,
        onClose:function(selectedDate){
            $(
                "#start_date").datepicker("option","maxDate",selectedDate)
        }
    });
    $("#start_date").datepicker({
        changeMonth:true,
        changeYear:
        true,
        onClose:function(selectedDate){
            $("#expiry_date").datepicker("option","minDate",selectedDate)
        }
    });
    jQuery.validator.
    addMethod("alphanumericunderscore",function(value,element){
        return this.optional(element)||/^[A-Za-z0-9 -]+$/i.test(value
            )
    },ALPHA_NUMERIC_HIFEN);
    jQuery.validator.addMethod("alphanumericspecialchar",function(value,element){
        return this.
        optional(element)||/^[A-Za-z0-9 %,.-]+$/i.test(value)
    },ALPHA_NUMERIC_SPECIALSYMPOL);
    jQuery.validator.addMethod(
        "floating",function(value,element){
            return this.optional(element)||/^-?\d*(\.\d+)?$/.test(value)
        },NUMERIC_VALUE);
    $("#create_promotion").validate({
        submitHandler:function(){
            if($( "#is_domestic:checked").val() != 1 && $("#is_international:checked").val() != 1){
                $("#error_domestic").show();                
                return false;
            }
            else {
                $("#error_domestic").hide();                
            }
            var dataStr=$("#create_promotion").serialize();
            showAjxLoader();
            $.ajax({
                type:"POST",
                url:"/create-crm-promotion",
                data:dataStr,
                success:function(responseData){
                    hideAjxLoader();
                    if(!
                        checkUserAuthenticationAjx(responseData))return false;
                    var jsonObj=jQuery.parseJSON(responseData);
                    if(jsonObj.status==
                        "success")window.location.href="/crm-promotion";else $.each(jsonObj.message,function(i,msg){
                        $("#"+i).after(
                            '<label class="error" style="display:block;">'+msg+"</label>")
                    })
                }
            })
        },
        rules:{
            promotion_name:{
                required:true,
                alphanumericspecialchar:true,
                minlength:2,
                maxlength:60
            },
            promotion_description:{
                maxlength:500
            },
            applicable_id:{
                required:
                true
            },
            product_category:{
                required:true
            },
            discount:{
                minlength:1,
                maxlength:10,
                floating:true
            },
            usage_limit:{
                minlength:1,
                maxlength:10,
                digits:true
            },
            free_shipping_amount:{
                minlength:1,
                maxlength:10,
                floating:true
            }
        },
        messages:{
            promotion_name:{
                required:PROMOTION_NAME_EMPTY
            },
            applicable_id:{
                required:APPLICABLE_BY_EMPTY
            },
            product_category:{
                required:
                PRODUCT_CATEGORY_EMPTY
            },
            usage_limit:{
                digits:USAGE_LIMIT_DIGIT
            }
        }
    });
    $("#edit_promotion").validate({
        submitHandler:function(){
            if($( "#is_domestic:checked").val() != 1 && $("#is_international:checked").val() != 1){
                $("#error_domestic").show();                
                return false;
            }
            else {
                $("#error_domestic").hide();                
            }
            var dataStr=$("#edit_promotion").serialize();
            showAjxLoader();
            $.ajax({
                type:"POST",
                url:"/edit-crm-promotion",
                data:
                dataStr,
                success:function(responseData){
                    hideAjxLoader();
                    if(!checkUserAuthenticationAjx(responseData))return false;
                    var 
                    jsonObj=jQuery.parseJSON(responseData);
                    if(jsonObj.status=="success")window.location.href="/crm-promotion";else $.each(
                        jsonObj.message,function(i,msg){
                            $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                        })
                }
            })
        },
        rules:{
            promotion_name:{
                required:true,
                alphanumericspecialchar:true,
                minlength:2,
                maxlength:60
            },
            promotion_description:{
                maxlength:500
            },
            applicable_id:{
                required:true
            },
            product_category:{
                required:true
            },
            discount:{
                minlength:1,
                maxlength:10,
                floating:true
            },
            usage_limit:{
                minlength:1,
                maxlength:10,
                digits:true
            }
        },
        messages:{
            promotion_name:{
                required:
                PROMOTION_NAME_EMPTY
            },
            applicable_id:{
                required:APPLICABLE_BY_EMPTY
            },
            product_category:{
                required:PRODUCT_CATEGORY_EMPTY
            },
            usage_limit:{
                digits:USAGE_LIMIT_DIGIT
            }
        }
    });
    $("#user").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:
        function(request,response){
            $.ajax({
                url:"/get-contacts",
                method:"POST",
                dataType:"json",
                data:{
                    user_email:$("#user").val()
                },
                success:function(jsonResult){
                    if(!checkUserAuthenticationAjx(jsonResult))return false;
                    var resultset=[];
                    $.each(jsonResult,
                        function(){
                            var id=this.user_id,value=this.full_name;
                            resultset.push({
                                id:this.user_id,
                                value:this.full_name
                            })
                        });
                    response(
                        resultset)
                }
            })
        },
        open:function(){
            $(".ui-menu").width(350)
        },
        change:function(event,ui){
            $("#user").val("")
        },
        select:function(
            event,ui){
            var user=ui.item.value,userId=ui.item.id,size=$("#user"+userId).size();
            if(size==0){
                $("#select_user").append(
                    "<li id='user"+userId+"'>"+user+"<span onclick='removeAutoCompleteValue("+userId+',"user")\'>x</span></li>');
                var 
                userIdsVal=$("#user_ids").val();
                if(userIdsVal=="")$("#user_ids").val(userId);else $("#user_ids").val(userIdsVal+","+
                    userId)
            }
            $("#user").val("");
            return false
        }
    });
    $("#group_name").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:function(request,response){
            $.ajax({
                url:"/get-groups",
                method:"POST",
                dataType:"json",
                data:{
                    group_name:$(
                        "#group_name").val()
                },
                success:function(jsonResult){
                    if(!checkUserAuthenticationAjx(jsonResult))return false;
                    var resultset
                    =[];
                    $.each(jsonResult,function(){
                        resultset.push({
                            id:this.group_id,
                            value:this.group_name
                        })
                    });
                    response(resultset)
                }
            })
        },
        open
        :function(){
            $(".ui-menu").width(350)
        },
        change:function(event,ui){
            $("#group_name").val("")
        },
        select:function(event,ui){
            var 
            group=ui.item.value,groupId=ui.item.id,size=$("#group"+groupId).size();
            if(size==0){
                $("#select_group").append(
                    "<li id='group"+groupId+"'>"+group+"<span onclick='removeAutoCompleteValue("+groupId+',"group")\'>x</span></li>');
                var 
                groupIdsVal=$("#group_name_ids").val();
                if(groupIdsVal=="")$("#group_name_ids").val(groupId);else $("#group_name_ids").
                    val(groupIdsVal+","+groupId)
            }
            $("#group_name").val("");
            return false
        }
    });
    $("#product_name").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:function(request,response){
            $.ajax({
                url:"/get-products",
                method:"POST",
                dataType:
                "json",
                data:{
                    product_name:$("#product_name").val(),
                    is_promotion_check:1
                },
                success:function(jsonResult){
                    if(!
                        checkUserAuthenticationAjx(jsonResult))return false;
                    var resultset=[];
                    $.each(jsonResult,function(){
                        resultset.push({
                            id:
                            this.product_id,
                            value:this.product_name
                        })
                    });
                    response(resultset)
                }
            })
        },
        open:function(){
            $(".ui-menu").width(350)
        },
        change:
        function(event,ui){
            $("#product_name").val("")
        },
        select:function(event,ui){
            var product=ui.item.value,productId=ui.item.id,
            size=$("#product"+productId).size();
            if(size==0){
                $("#select_product").append("<li id='product"+productId+"'>"+product+
                    "<span onclick='removeAutoCompleteValue("+productId+',"product")\'>x</span></li>');
                var productIdsVal=$(
                    "#product_name_ids").val();
                if(productIdsVal=="")$("#product_name_ids").val(productId);else $("#product_name_ids").val(
                    productIdsVal+","+productId)
            }
            $("#product_name").val("");
            return false
        }
    });
    $("#category_name").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:function(request,response){
            $.ajax({
                url:"/get-product-types",
                method:"POST",
                dataType:"json",
                data:{
                    product_type:$("#category_name").val()
                },
                success:function(jsonResult){
                    if(!
                        checkUserAuthenticationAjx(jsonResult))return false;
                    var resultset=[];
                    $.each(jsonResult,function(){
                        resultset.push({
                            id:
                            this.product_type_id,
                            value:this.product_type
                        })
                    });
                    response(resultset)
                }
            })
        },
        open:function(){
            $(".ui-menu").width(350)
        },
        change:function(event,ui){
            $("#category_name").val("")
        },
        select:function(event,ui){
            var category=ui.item.value,categoryId=
            ui.item.id,size=$("#category"+categoryId).size();
            if(size==0){
                $("#select_category").append("<li id='category"+categoryId+
                    "'>"+category+"<span onclick='removeAutoCompleteValue("+categoryId+',"category")\'>x</span></li>');
                var categoryIdsVal=$(
                    "#category_name_ids").val();
                if(categoryIdsVal=="")$("#category_name_ids").val(categoryId);else $("#category_name_ids").
                    val(categoryIdsVal+","+categoryId)
            }
            $("#category_name").val("");
            return false
        }
    });
    $("#is_free_shipping").change(function(){
        if($(this).is(":checked"))$("#freeShipAmount").show();else $("#freeShipAmount").hide()
    })
});
function onChangeApplicable(
    val){
    if(val==1){
        $("#contact_section,#group_section,#applicable_excluded").css("display","none");
        $(
            ".select_product_category_label2").empty()
    }else if(val==2){
        $("#group_section").css("display","none");
        $(
            "#contact_section").css("display","block");
        $("#applicable_excluded").css("display","block");
        $(
            ".select_product_category_label2").html(L_SELECTED_CONTACT_LIST)
    }else if(val==3){
        $("#group_section").css("display",
            "block");
        $("#contact_section").css("display","none");
        $("#applicable_excluded").css("display","block");
        $(
            ".select_product_category_label2").html(L_SELECTED_GROUP_LIST)
    }
}
function onChangeProductCategory(val){
    $(
        ".select_product_category_label").empty();
    if(val==1){
        $("#category_section,#product_section").css("display","none");
        $(
            ".select_product_category_label").empty()
    }else if(val==2){
        $("#product_section").css("display","none");
        $(
            "#category_section").css("display","block");
        $("#product_category_excluded").css("display","block");
        $(
            ".select_product_category_label").html(L_SELECTED_CATEGORY_LIST)
    }else if(val==3){
        $("#product_section").css("display",
            "block");
        $("#category_section").css("display","none");
        $("#product_category_excluded").css("display","block");
        $(
            ".select_product_category_label").html(L_SELECTED_PRODUCT_LIST)
    }
}
function removeAutoCompleteValue(id,type){
    var module=""
    ,elementId="";
    if(type=="user"){
        module="user";
        elementId="user_ids"
    }else if(type=="group"){
        module="group";
        elementId=
        "group_name_ids"
    }else if(type=="category"){
        module="category";
        elementId="category_name_ids"
    }else if(type=="product"){
        module="product";
        elementId="product_name_ids"
    }
    var idArr=$("#"+elementId).val().split(","),newIdStr="";
    for(i=0;i<idArr.
        length;i++)if(idArr[i]!=id)newIdStr+=idArr[i]+",";newIdStr=newIdStr.substr(0,newIdStr.length-1);
    $("#"+elementId).val(
        newIdStr);
    $("#"+module+""+id).remove()
}
function is_domestic(){
    alert($("#is_domestic").val());
    return false;
    if($("#is_domestic").val() == 0 && $("#is_international").val() == 0){
        return true;
    }
    else {
        return false;
    }
}