$(document).ready(function(){jQuery.validator.addMethod("alphanumeric",function(value,element){return this.optional(
element)||/^[A-Za-z0-9()\'' .-]+$/i.test(value)},"Please enter valid input.");$("#homePassengerSearchFrm").validate({
rules:{last_name:{required:true,minlength:2,alphanumeric:true}},messages:{last_name:{required:"Please enter last name.",
minlength:"Last name must contain at least two characters.",alphanumeric:"Please enter valid input."}}})})