$(function() {
    if (mode == "view")
        getProjectDetail();
    else if (mode == "edit")
        editProjectDetails();
    else
        projectChangelog();
    $("a[id^='tab-']").removeClass("active");
    var tab_id_load = mode == "view" ? "1" : "2";
    $("#tab-" + tab_id_load).addClass("active");
    $("a[id^='tab-']").click(function() {
		$("a[id^='tab-']").removeClass("active");
		$(this).addClass("active");
    });
});
function getProjectDetail() {
    showAjxLoader();
    $.ajax({
		type: "POST", 
		data: {}, 
		url: "/crm-project-detail/" + crm_project_id, 
			success:
			function(response) {
				hideAjxLoader();
				if (!checkUserAuthenticationAjx(response))
					return false;
				$("#projectContent").html(response)
			}
	});
}
function editProjectDetails() {
    showAjxLoader();
    $.ajax({
		type: "POST", 
		data: {}, 
		url: "/modify-project/" + crm_project_id, 
		success: function(response) {
            hideAjxLoader();
            if (!checkUserAuthenticationAjx(response))
                return false;
            $("#projectContent").html(response);
        }});
}
function projectChangelog() {
    showAjxLoader();
    $.ajax({
		type: "POST", 
		url:  "/project-change-log/" + moduleName + "/" + crm_project_id, 
		success: function(response) {
            hideAjxLoader();
            if (!checkUserAuthenticationAjx(response))
                return false;
            $("#projectContent").html(response)
       }
	});
}