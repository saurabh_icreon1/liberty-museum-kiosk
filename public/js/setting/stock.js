$(document).ready(function(){var container=$("div.error_message");jQuery.validator.addMethod(
"alphanumericunderscoreatrate",function(value,element){var obj=value.split(","),isValid=true;for(var k=0;k<obj.length;
k++)if(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i.test($.trim(obj[k])))isValid=true;else isValid=false;
return isValid},RECIPIENT_INVALID_ERROR_MSG);jQuery.validator.addMethod("alphanumericunderscore",function(value,element)
{return this.optional(element)||/^[A-Za-z0-9:\[\] -]+$/i.test(value)},SUBJECT_ERROR_MSG);$("#stock").validate({
submitHandler:function(){var dataStr=$("#stock").serialize();$.ajax({type:"POST",url:"/manage-stock-notification",data:
dataStr,success:function(responseData){if(!checkUserAuthenticationAjx(responseData))return false;var jsonObj=jQuery.
parseJSON(responseData);if(jsonObj.status=="success")window.location.href="/manage-stock-notification";else $.each(
jsonObj.message,function(i,msg){$("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")})}})},
rules:{notification_recipient:{required:true,alphanumericunderscoreatrate:true},subject:{required:true,
alphanumericunderscore:true,minlength:2,maxlength:200},message:{required:true}},messages:{notification_recipient:{
required:RECIPIENT_ERROR_MSG},subject:{required:SUBJECT_ERROR_MSG},message:{required:MESSAGE_ERROR_MSG}}})})