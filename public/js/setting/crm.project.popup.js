$(function() {
    var grid = jQuery("#project_list"), emptyMsgDivPass = $('<div class="no-record-msz">' + NO_RECORD_FOUND + "</div>");
    $("#project_list").jqGrid({
		mtype: "POST", 
		url: "/crm-project-popup-list-data", datatype: "json", 
			colNames: [
				L_SNO, L_PROJECT_NAME
			], 
			colModel: [
				{
					name: L_SNO, 
					index: "s_no",
					sortable: false,
					width: '15%',
				},
				{
					name: L_PROJECT_NAME, 
					index: "project_name"
				}
			], 
			viewrecords: true, 
			sortname: "IF(tbl_projects.modified_date IS NOT NULL,tbl_projects.modified_date,tbl_projects.added_date)", 
			sortorder: "asc", 
			rowNum: 50, 
			rowList: [50, 100, 150], 
			pager: "#pcrud_project", 
			viewrecords:true, 
			autowidth: false, 
			shrinkToFit: true, 
			caption: "", 
			width: "570", 
			cmTemplate: {
				title: false
			}, 
			loadComplete: function() {
				var count = grid.getGridParam(), ts = grid[0];
				if (ts.p.reccount === 0) {
					grid.hide();
					emptyMsgDivPass.show();
					$("#pcrud_project_right div.ui-paging-info").css("display", "none")
				} else {
					grid.show();
					emptyMsgDivPass.hide();
					$("#pcrud_project_right div.ui-paging-info").css("display", "block")
				}
        }});
		emptyMsgDivPass.insertAfter(grid.parent());
		emptyMsgDivPass.hide();
		$("#project_list").jqGrid("navGrid", "#pcrud_project", {
			reload: true, 
			edit: false, 
			add: false, 
			search: false, 
			del: false});
});
function selectProject(projectName, projectId) {
    parent.$("#project").val(projectName);
    parent.$("#project_id").val(projectId);
    parent.$.fn.colorbox.close()
}