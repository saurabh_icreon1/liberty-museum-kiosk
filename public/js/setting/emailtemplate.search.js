$(function(){$.jgrid.no_legacy_api=true;$.jgrid.useJSON=true;var grid=jQuery("#list"),emptyMsgDiv=$(
'<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");$("#list").jqGrid({postData:{searchString:$("#search-template").
serialize()},mtype:"POST",url:"/get-crm-email-template",datatype:"json",sortable:true,colNames:[L_NAME,L_DATE_CREATED,
L_DATE_MODIFIED,ACTION],colModel:[{name:"Name",index:"email_title"},{name:"date_created",index:"added_date",align:
"center"},{name:"date_modified",index:"modified_date",align:"center"},{name:"Action",index:"action",sortable:false}],
viewrecords:true,sortname:"modified_date",sortorder:"DESC",rowNum:30,rowList:[10,20,30],pager:"#pcrud",viewrecords:true,
autowidth:true,shrinkToFit:true,caption:"",width:"100%",cmTemplate:{title:false},loadComplete:function(){hideAjxLoader()
;var count=grid.getGridParam(),ts=grid[0];if(ts.p.reccount===0){grid.hide();emptyMsgDiv.show();$(
"#pcrud_right div.ui-paging-info").css("display","none")}else{grid.show();emptyMsgDiv.hide();$(
"#pcrud_right div.ui-paging-info").css("display","block")}}});emptyMsgDiv.insertAfter(grid.parent());emptyMsgDiv.hide();
$("#list").jqGrid("navGrid","#pcrud",{reload:true,edit:false,add:false,search:false,del:false});$("#search").click(
function(){showAjxLoader();jQuery("#list").jqGrid("setGridParam",{postData:{searchString:$("#search-template").serialize
()}});jQuery("#list").trigger("reloadGrid",[{page:1}]);window.location.hash="#search-template";hideAjxLoader();
return false})})