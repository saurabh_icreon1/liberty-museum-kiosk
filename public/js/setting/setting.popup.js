$(document).ready(function(){var postData=true;$.jgrid.no_legacy_api=true;$.jgrid.useJSON=true;var gridDropdown=jQuery(
"#list"),emptyMsgDiv=$('<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");$("#list").jqGrid({mtype:"POST",url:
"/get-dropdown-content/"+MASTER_TABLE,datatype:"json",sortable:true,colNames:["ID","Title","Description","Action(s)"],
colModel:[{name:"ID",index:"Id",hidden:true},{name:FIELD,index:FIELD,editable:true,editoptions:{maxlength:30},editrules:
{required:true}},{name:FIELD2,index:FIELD2,editable:true,editoptions:{maxlength:50}},{name:"Action",index:"action",
sortable:false,formatter:"actions",formatoptions:{keys:true,delbutton:false,afterSave:function(){jQuery("#list").trigger
("reloadGrid");$("#delete_content").css("display","block");$("#success_message").css("display","block");$(
"#success_message").css("padding","4px 0px 2px 25px");$("#success_message").html(EDIT_MSG)}}}],viewrecords:true,sortname
:"modified_date",sortorder:"DESC",rowNum:500,rowList:[10,20,30],pager:"#pcrud",autowidth:true,shrinkToFit:true,caption:
"",width:"100%",cmTemplate:{title:false},loadComplete:function(){var count=gridDropdown.getGridParam(),ts=gridDropdown[0
];if(ts.p.reccount===0){gridDropdown.hide();emptyMsgDiv.show();$("#pcrud_right div.ui-paging-info").css("display","none"
)}else{gridDropdown.show();emptyMsgDiv.hide();$("#pcrud_right div.ui-paging-info").css("display","block")}},gridComplete
:function(){$("#pcrud_center").hide();$("#pcrud_right").hide();$(".ui-icon-pencil").html(
'<a class="edit-icon" onclick="hideIcon(this)"></a>');$(".ui-icon-disk").html(
'<a style="cursor: pointer;" class="save-icon"></a>');$(".ui-icon-cancel").html(
'<a style="cursor: pointer;" class="cancel-icon" onclick="showIcon(this,'+ID+')"></a>');$(".ui-inline-edit").after(
'<span><a href="#delete_content" class="delete_value delete-icon" onclick="delDetails('+ID+')"></a></span>');$(
".ui-icon-disk").removeClass();$(".ui-icon-cancel").removeClass();$(".ui-icon-pencil").removeClass();$(".jqgrow").
mouseover(function(e){var rowId=$(this).attr("id");$("#delId").val(rowId)});$("#_empty","#list").addClass(
"nodrag nodrop");jQuery("#list").tableDnDUpdate()},editurl:"/edit-crm-mst-table-content/"+ID+"/"+FIELD+"/"+FIELD2});
emptyMsgDiv.insertAfter(gridDropdown.parent());emptyMsgDiv.hide();$("#list").jqGrid("navGrid","#pcrud",{reload:true,edit
:false,add:true,search:false,del:false},{url:"yourEditUrl"},{url:"create-crm-mst-table-content/"+MASTER_TABLE+"/"+FIELD+
"/"+FIELD2,closeOnEscape:true,width:500,savekey:[true,13],afterSubmit:function(response,postData){var jsonObj=jQuery.
parseJSON(response.responseText);if(jsonObj.status=="success"){jQuery("#list").trigger("reloadGrid");jQuery("#name").val
("");$("#delete_content").css("display","block");$("#success_message").css("display","block");$("#success_message").css(
"padding","4px 0px 2px 25px");$("#success_message").html(jsonObj.msg)}$("#list").jqGrid("setGridParam",{datatype:"json"}
).trigger("reloadGrid");return[true,"",""]},resize:false,closeAfterAdd:true});jQuery("#list").tableDnD({onDrop:function(
table,row){$(".loading").css("display","block");var count=gridDropdown.getGridParam(),data=gridDropdown.getRowData(),
postData=JSON.stringify(data);$.ajax({type:"POST",data:{table:MASTER_TABLE,id:IDFIELD,data:postData},url:"/update-order"
,success:function(response){$(".loading").css("display","none");var jsonObj=jQuery.parseJSON(response);if(jsonObj.status
=="success")jQuery("#list").trigger("reloadGrid");else $("#success_message").html(ERROR_SAVING_SEARCH)}})}});jQuery.
validator.addMethod("alphanumericunderscore",function(value,element){return this.optional(element)||/^[A-Za-z0-9 -]+$/i.
test(value)});$("#add_dropdown").validate({submitHandler:function(){$.ajax({type:"POST",data:{name:$("#name").val(),
table:MASTER_TABLE,field:FIELD,displayName:DISPLAY_NAME},url:"/create-crm-mst-table-content",success:function(response){
if(!checkUserAuthenticationAjx(response))return false;var jsonObj=jQuery.parseJSON(response);if(jsonObj.status==
"success"){jQuery("#list").trigger("reloadGrid");jQuery("#name").val("");$("#delete_content").css("display","block");$(
"#success_message").css("display","block");$("#success_message").css("padding","4px 0px 2px 25px");$("#success_message")
.html(jsonObj.msg)}else $("#search_msg").html(ERROR_SAVING_SEARCH)}})},rules:{name:{required:true,alphanumericunderscore
:true}},messages:{name:{required:NAME_MSG,alphanumericunderscore:VALID_NAME_MSG}}})});function delDetails(ID){$.colorbox
({width:"700px",href:"#delete_content"+ID,height:"180px",inline:true});$("#no"+ID).click(function(){$.colorbox.close();$
("#yes"+ID).unbind("click");return false});$("#yes"+ID).click(function(){var delId=$("#delId").val();$.ajax({type:"POST"
,data:{table:MASTER_TABLE,displayName:DISPLAY_NAME,id:IDFIELD},url:"/delete-crm-mst-table-content/"+delId,success:
function(response){if(!checkUserAuthenticationAjx(response))return false;var jsonObj=jQuery.parseJSON(response);if(
jsonObj.status=="success"){jQuery("#list").trigger("reloadGrid");$.colorbox.close();$("#delete_content").css("display",
"block");$("#success_message").css("display","block");$("#success_message").css("padding","4px 0px 2px 25px");$(
"#success_message").html(jsonObj.msg)}else $("#success_message").html(ERROR_SAVING_SEARCH)}})})}function hideIcon(
thisref){$(thisref).parents("div").next().children().css("display","none");$(".full").css("display","block");$(
".common-form").css("display","block");$("#delete_content").css("display","block");$("#success_message").css("display",
"none")}function showIcon(thisref,id){$(thisref).parents("div").prev().prev().children().css("display","block");$(
thisref).parents("div").prev().children().css("display","block");$("#delete_content"+id).show();$("#delete_content").css
("display","block");$("#success_message").css("display","none")}