$(function(){
	$("#create_project").validate({
        submitHandler:function(){
            var dataStr=$("#create_project").serialize();
            showAjxLoader();
            $.ajax({
                type:"POST",
                url:"/create-crm-project",
                data:dataStr,
                success:function(responseData){
                    if(!checkUserAuthenticationAjx(responseData))return false;
                    hideAjxLoader();
                    var jsonObj=jQuery.parseJSON(responseData);
                    if(jsonObj.status=="success")window.location.href="/get-crm-projects";else $.each(jsonObj.message,function(i,msg){
                        $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                        })
                    }
                    })
        },
		rules:{
			project_name:{
				required:true
			},
			project_status:{
				required:true
			},
		},
		messages:{
			project_name:{
				required:PROJECT_NAME_EMPTY
			},
			project_status:{
				required:PROJECT_STATUS_EMPTY
			},
		}
	});

	$("#edit_project").validate({
        submitHandler:function(){
            var dataStr=$("#edit_project").serialize();
            showAjxLoader();
            $.ajax({
                type:"POST",
                url:"/update-crm-project",
                data:dataStr,
                success:function(responseData){
                    if(!checkUserAuthenticationAjx(responseData))return false;
                    hideAjxLoader();
                    var jsonObj=jQuery.parseJSON(responseData);
                    if(jsonObj.status=="success")window.location.href="/get-crm-projects";else $.each(jsonObj.message,function(i,msg){
                        $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                        })
                    }
                    })
        },
		rules:{
			project_name:{
				required:true
			},
			project_status:{
				required:true
			},
		},
		messages:{
			project_name:{
				required:PROJECT_NAME_EMPTY
			},
			project_status:{
				required:PROJECT_STATUS_EMPTY
			},
		}
	});

	
	$("#submit_button").click(function(){
		$("#success-message").hide();
		$("#successRow").hide();
		showAjxLoader();
		jQuery("#project_list").jqGrid("setGridParam",{
			postData:{
				searchString:$("#search_projects").serialize()
					}
				});
		jQuery("#project_list").trigger("reloadGrid",[{page:1}]);
		hideAjxLoader();
		return false;
	});
	$('#update').click(function(){
		if($('#tac').val()=='1' && $('#project_status').val()=='0'){
			$.colorbox({
				width:"700px",
				href:
				"#block_update",
				height:"200px",
				inline:true
			});
			$('#no_update').click(function(){
				$.colorbox.close();
			});
		}else{
			$("#edit_project").submit();
		}
	});
	var grid=jQuery("#project_list"),emptyMsgDivPass=$('<div class="no-record-msz">'+ NO_RECORD_FOUND+"</div>");
	$("#project_list").jqGrid({
		postData:{
			searchString:$("#search").serialize()
		},
		mtype:"POST",
		url: "/crm-project-list-data",
		datatype:"json",
		colNames:[L_PROJECT_NAME,L_CAMPAIGNS, L_DATES, L_PROJECT_STATUS,L_ACTION],
		colModel:[
		{
			name:L_PROJECT_NAME,
			index:"project_name"
		},
		{
			name:L_CAMPAIGNS,
			index:"campaigns",
			sortable: false
		},
		{
			name:L_DATES,
			index:"dates",
			sortable: false
		},
		{
			name: L_PROJECT_STATUS,
			index:"project_status"
		},
		{
			name:L_ACTION
		}],
		viewrecords:true,
		sortname:"IF(tbl_projects.modified_date IS NOT NULL,tbl_projects.modified_date,tbl_projects.added_date)",
		sortorder:"desc",
		rowNum:10,
		rowList:[10,20,30],
		pager:"#pcrud_project",
		autowidth:true,
		shrinkToFit:true,
		caption:"",
		width:"100%",
		cmTemplate:{
			title:false
		},
		loadComplete:function(){
				var count=grid.getGridParam(),ts=grid[0];
				if(ts.p.reccount===0){
					grid.hide();emptyMsgDivPass.show();
					$("#pcrud_project_right div.ui-paging-info").css("display","none");
				}else{
					grid.show();emptyMsgDivPass.hide();
					$("#pcrud_project_right div.ui-paging-info").css("display","block");
				}
		}
	});
	emptyMsgDivPass.insertAfter(grid.parent());
	emptyMsgDivPass.hide();
	$("#project_list").jqGrid("navGrid","#pcrud_project",{
		reload:true,
		edit:false,
		add:false,
		search:false,
		del:false
	}
	);
});