$(document).ready(function(){var container=$("div.error_message");jQuery.validator.addMethod("alphanumericspecialchar",
function(value,element){return this.optional(element)||/^[A-Za-z0-9-\n ,.:\\'\\!\/"]+$/i.test(value)});$(
"#edit-email-template").validate({submitHandler:function(){showAjxLoader();var isChecked=$("#email_set_text_0").is(
":checked");if(isChecked==false)tinymce.get("email_message_body").save();var dataStr=$("#edit-email-template").serialize
();$.ajax({type:"POST",url:"/edit-crm-email-template/"+$("#email_id").val(),data:dataStr,success:function(responseData){
hideAjxLoader();if(!checkUserAuthenticationAjx(responseData))return false;var jsonObj=jQuery.parseJSON(responseData);if(
jsonObj.status=="success")window.location.href="/crm-email-templates";else $.each(jsonObj.message,function(i,msg){$("#"+
i).after('<label class="error" style="display:block;">'+msg+"</label>")});hideAjxLoader()}})},rules:{email_title:{
required:true,alphanumericspecialchar:true,minlength:2,maxlength:200},email_description:{required:true,
alphanumericspecialchar:true,minlength:2,maxlength:200},email_subject:{required:true,alphanumericspecialchar:true,
minlength:2,maxlength:200},email_body:{required:true}},messages:{email_title:{required:EMAIL_TITLE_ERROR_MSG,
alphanumericspecialchar:EMAIL_TITLE_INVALID},email_description:{required:EMAIL_DESCRIPTION_ERROR_MSG,
alphanumericspecialchar:EMAIL_DESCRIPTION_INVALID},email_subject:{required:EMAIL_SUBJECT_ERROR_MSG,
alphanumericspecialchar:EMAIL_SUBJECT_INVALID},email_body:{required:EMAIL_MESSAGE_ERROR_MSG}}});$("#email_address").
click(function(){$(".error").hide();$(".success-msg").hide()});$("#send").click(function(){var dataStr=$(
"#edit-email-template").serialize(),userinput=$("#email_address").val(),pattern=
/^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;if(!pattern.test(userinput)){$("#send").after(
'<label class="error" style="display:block;">Please enter a valid Email id!</label>');return false}else showAjxLoader();
$(".success-msg").hide();var isChecked=$("#email_set_text_0").is(":checked");if(isChecked==false)tinymce.get(
"email_message_body").save();$.ajax({type:"POST",url:"/send-email-template",data:{emailId:$("#email_address").val(),
message:$("#email_message_body").val(),subject:$("#email_subject").val()},success:function(responseData){hideAjxLoader()
;if(!checkUserAuthenticationAjx(responseData))return false;var jsonObj=jQuery.parseJSON(responseData);if(jsonObj.status
=="success")$("#send").after('<div class="full"><label>&nbsp;</label><div class="success-msg" style="display:block;">'+
jsonObj.message+"</div></div>");else $("#send").after('<label class="error" style="display:block;">'+jsonObj.message+
"</label>");hideAjxLoader()}})})})