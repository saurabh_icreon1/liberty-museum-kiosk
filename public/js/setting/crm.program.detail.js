$(function(){if(mode=="view")getProgramDetail();else if(mode=="edit")editProgramDetails();else programChangelog();$(
"a[id^='tab-']").removeClass("active");var tab_id_load=mode=="view"?"1":"2";$("#tab-"+tab_id_load).addClass("active");$(
"a[id^='tab-']").click(function(){$("a[id^='tab-']").removeClass("active");$(this).addClass("active")})});function 
getProgramDetail(){showAjxLoader();$.ajax({type:"POST",data:{},url:"/crm-program-detail/"+crm_program_id,success:
function(response){hideAjxLoader();if(!checkUserAuthenticationAjx(response))return false;$("#programContent").html(
response)}})}function editProgramDetails(){showAjxLoader();$.ajax({type:"POST",data:{},url:"/modify-program/"+
crm_program_id,success:function(response){hideAjxLoader();if(!checkUserAuthenticationAjx(response))return false;$(
"#programContent").html(response)}})}function programChangelog(){showAjxLoader();$.ajax({type:"POST",url:
"/program-change-log/"+moduleName+"/"+crm_program_id,success:function(response){hideAjxLoader();if(!
checkUserAuthenticationAjx(response))return false;$("#programContent").html(response)}})}