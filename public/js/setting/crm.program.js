function deleteProgram(programId){$.colorbox({href:"#delete_program",width:"600px",height:"180px",inline:true});$(
"#no_delete").click(function(){$.colorbox.close();$("#no_delete").unbind("click");return false});$("#yes_delete").click(
function(){$.ajax({type:"POST",data:{programId:programId},url:"/remove-program",success:function(response){if(!
checkUserAuthenticationAjx(response))return false;var jsonObj=jQuery.parseJSON(response);if(jsonObj.status=="success"){$
.colorbox.close();hideAjxLoader();window.location.href="/crm-program-list"}}})})}$(function(){$("#submitbutton").click(
function(){$("#success-message").hide();$("#successRow").hide();showAjxLoader();jQuery("#program_list").jqGrid(
"setGridParam",{postData:{searchString:$("#search").serialize()}});jQuery("#program_list").trigger("reloadGrid",[{page:1
}]);hideAjxLoader();return false});var grid=jQuery("#program_list"),emptyMsgDivPass=$('<div class="no-record-msz">'+
NO_RECORD_FOUND+"</div>");$("#program_list").jqGrid({postData:{searchString:$("#search").serialize()},mtype:"POST",url:
"/crm-program-list-data",datatype:"json",colNames:[L_PROGRAM_NAME,L_PROGRAM_CODE,L_IS_STATUS,L_ACTION],
colModel:[{name:L_PROGRAM_NAME,index:"program"},{name:L_PROGRAM_CODE,index:"program_code"},{name:L_IS_STATUS,index:"is_status"},{name:L_ACTION}],
viewrecords:true,sortname:"program.program_id",sortorder:"asc",rowNum:10,rowList:[10,20,30],pager:"#pcrud_program",
viewrecords:true,autowidth:true,shrinkToFit:true,caption:"",width:"100%",cmTemplate:{title:false},loadComplete:function(
){var count=grid.getGridParam(),ts=grid[0];if(ts.p.reccount===0){grid.hide();emptyMsgDivPass.show();$(
"#pcrud_program_right div.ui-paging-info").css("display","none")}else{grid.show();emptyMsgDivPass.hide();$(
"#pcrud_program_right div.ui-paging-info").css("display","block")}}});emptyMsgDivPass.insertAfter(grid.parent());
emptyMsgDivPass.hide();$("#program_list").jqGrid("navGrid","#pcrud_program",{reload:true,edit:false,add:false,search:
false,del:false})});function setIsDonation(){if($("#is_available_for_donation").is(":checked"))$(
"#is_available_for_donation").val(1);else $("#is_available_for_donation").val(0)}