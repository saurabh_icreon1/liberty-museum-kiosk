function setPlainText(thisobj){var isChecked=thisobj.checked;if(isChecked==true)tinymce.execCommand("mceRemoveEditor",
true,"membership_description");else tinymce.execCommand("mceAddEditor",true,"membership_description")}$(document).ready(
function(){jQuery.validator.addMethod("alphanumericspecialchar",function(value,element){return this.optional(element)||
/^[A-Za-z0-9\n\r ,.-]+$/i.test(value)},ALPHA_NUMERIC_SPECIALSYMPOL);jQuery.validator.addMethod("floating",function(value
,element){return this.optional(element)||/^-?\d*(\.\d+)?$/.test(value)},NUMERIC_VALUE);$("#user_membership").validate({
submitHandler:function(){if(!$("#email_set_text").is(":checked"))tinymce.get("membership_description").save();var 
dataStr=$("#user_membership").serialize();showAjxLoader();$.ajax({type:"POST",url:"/create-crm-membership",data:dataStr,
success:function(responseData){hideAjxLoader();if(!checkUserAuthenticationAjx(responseData))return false;var jsonObj=
jQuery.parseJSON(responseData);if(jsonObj.status=="success")window.location.href="/crm-membership";else $.each(jsonObj.
message,function(i,msg){$("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")})}})},rules:{
membership_title:{required:true,alphanumericspecialchar:true},level_color:{required:true},minimun_donation_amount:{
floating:true,minlength:1,maxlength:10},first_alert_before_in_days:{digits:true,minlength:1,maxlength:10},
repeat_alerts_in_days:{digits:true,minlength:1,maxlength:10},repeat_alerts_in_times:{required:function(element){return $
("#repeat_alerts_in_days").val()!=""},digits:true,minlength:1,maxlength:10},renew_alerts_in_days:{digits:true,minlength:
1,maxlength:10},renew_alerts_in_times:{required:function(element){return $("#renew_alerts_in_days").val()!=""},digits:
true,minlength:1,maxlength:10},come_back_request_in_days:{digits:true,minlength:1,maxlength:10},discount:{floating:true,
minlength:1,maxlength:10},saved_search_in_days:{digits:true},saved_search_validity_in_days:{digits:true,minlength:1,
maxlength:10},max_stories:{digits:true,minlength:1,maxlength:3},max_story_characters:{digits:true,minlength:1,maxlength:
6},max_passengers:{digits:true,minlength:1,maxlength:3},max_saved_histories:{digits:true,minlength:1,maxlength:3}},
messages:{membership_title:{required:TITLE_EMPTY},level_color:{required:LEVEL_COLOR_EMPTY},repeat_alerts_in_times:{
required:REPEAT_ALERT_TIME_EMPTY},renew_alerts_in_times:{required:RENEW_ALERT_TIME_EMPTY}}});$("#edit_user_membership").
validate({submitHandler:function(){if(!$("#email_set_text").is(":checked"))tinymce.get("membership_description").save();
var dataStr=$("#edit_user_membership").serialize();showAjxLoader();$.ajax({type:"POST",url:"/edit-crm-membership",data:
dataStr,success:function(responseData){hideAjxLoader();if(!checkUserAuthenticationAjx(responseData))return false;var 
jsonObj=jQuery.parseJSON(responseData);if(jsonObj.status=="success")window.location.href="/crm-membership";else $.each(
jsonObj.message,function(i,msg){$("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")})}})},
rules:{membership_title:{required:true,alphanumericspecialchar:true},level_color:{required:true},minimun_donation_amount
:{floating:true,minlength:1,maxlength:10},first_alert_before_in_days:{digits:true,minlength:1,maxlength:10},
repeat_alerts_in_days:{digits:true,minlength:1,maxlength:10},repeat_alerts_in_times:{required:function(element){return $
("#repeat_alerts_in_days").val()!=""},digits:true,minlength:1,maxlength:10},renew_alerts_in_days:{digits:true,minlength:
1,maxlength:10},renew_alerts_in_times:{required:function(element){return $("#renew_alerts_in_days").val()!=""},digits:
true,minlength:1,maxlength:10},come_back_request_in_days:{digits:true,minlength:1,maxlength:10},discount:{floating:true,
minlength:1,maxlength:10},saved_search_in_days:{digits:true,minlength:1,maxlength:10},saved_search_validity_in_days:{
digits:true,minlength:1,maxlength:10},max_stories:{digits:true,minlength:1,maxlength:3},max_story_characters:{digits:
true,minlength:1,maxlength:6},max_passengers:{digits:true,minlength:1,maxlength:3},max_saved_histories:{digits:true,
minlength:1,maxlength:3}},messages:{membership_title:{required:TITLE_EMPTY},level_color:{required:LEVEL_COLOR_EMPTY},
repeat_alerts_in_times:{required:REPEAT_ALERT_TIME_EMPTY},renew_alerts_in_times:{required:RENEW_ALERT_TIME_EMPTY}}})});
function changeValidityType(val){if(val==1){$("#financial_year_block").show();$("#validity_start_date_block").hide()}
else if(val==2){$("#financial_year_block").hide();$("#validity_start_date_block").show()}else{$("#financial_year_block")
.hide();$("#validity_start_date_block").hide()}}var totalSaveSearch=typeof totalSaveSearch!="undefined"?totalSaveSearch:
1;function addNewSaveSearch(currAnchor){var allSearchSelect=$("select[name^='saved_search_type']"),selectedTypes="";$(
allSearchSelect).each(function(){selectedTypes+=$(this).val()});showAjxLoader();$.ajax({type:"POST",url:
"/add-save-search",data:{totalSaveSearch:totalSaveSearch},success:function(responseData){hideAjxLoader();if(!
checkUserAuthenticationAjx(responseData))return false;var allDivBlock=$("div[id^='save_search_block']");if(allDivBlock.
length==1){var parId=$(currAnchor).parent().attr("id"),id=parId.substring(parId.lastIndexOf("_")+1,parId.length);$(
currAnchor).parent().append('<a class="minus" href="javascript:void(0);" onclick="deleteNewSaveSearch('+id+')"></a>')}$(
currAnchor).remove();$("#saved_search_div").append(responseData);totalSaveSearch=totalSaveSearch+1}})}function 
deleteNewSaveSearch(id){$("#save_search_block_"+id).remove();var lastBlock=$("div[id^='save_search_block']").last();if($
(lastBlock).find(".plus").length==0)$(lastBlock).append(
'<a class="plus plus m-l-5" href="javascript:void(0);" onclick="addNewSaveSearch(this)" id="addMoreSaveSearch"></a>');
var allDivBlock=$("div[id^='save_search_block']");if(allDivBlock.length==1)$(lastBlock).find(".minus").remove()}