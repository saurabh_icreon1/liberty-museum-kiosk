function checkexists(){$("#isusable").val("");$.ajax({type:"POST",data:{programCode:$("#program_code").val(),programId:$
("#programId").val()},url:"/crm-check-program-code",async:false,success:function(response){var jsonObj=jQuery.parseJSON(
response);if(jsonObj.status=="notexists")$("#isusable").val(1);else if(jsonObj.status=="exists")if($.trim($(
"#program_code").val())==$.trim($("#program_code_hidden").val())&&$.trim($("#program_code").val())!="")$("#isusable").
val(1);else $("#isusable").val(0)}})}$(function(){$("#save").click(function(){checkexists()});$(
"#is_available_for_donation").change(function(){if($.trim($("#programId").val())=="")$("#gl_code").val("");if($(
"#is_available_for_donation").is(":checked")){$("#major_glcode_label").show();$("#general_glcode_label").show();$(
"#glcode_label").hide()}else{if($.trim($("#programId").val())=="")$("#major_gl_code").val("");$("#major_glcode_label").
hide();$("#general_glcode_label").hide();$("#glcode_label").show()}});jQuery.validator.addMethod(
"alphanumericunderscore",function(value,element){return this.optional(element)||/^[A-Za-z0-9._,\n -]+$/i.test(value)});
jQuery.validator.addMethod("alphanumeric",function(value,element){return this.optional(element)||/^[A-Za-z0-9\n .?-]+$/i
.test(value)});jQuery.validator.addMethod("programcodeexists",function(value,element){if($("#isusable").val()==1||$(
"#isusable").val()=="")return true;else if($("#isusable").val()==0)return false},P_PROGRAM_CODE_EXISTS);$("#add_program"
).validate({submitHandler:function(){showAjxLoader();$.ajax({type:"POST",data:$("#add_program").serialize(),url:
"/add-program-process",success:function(response){hideAjxLoader();var jsonObj=jQuery.parseJSON(response);if(jsonObj.
status=="success")window.location.href="/crm-program-list";else $.each(jsonObj.message,function(i,msg){$("#"+i).after(
'<label class="error" style="display:block;">'+msg+"</label>")});return false}})},rules:{name:{required:true,
alphanumericunderscore:true},program_code:{required:true,alphanumeric:true,programcodeexists:true},description:{required
:true,alphanumericunderscore:true},gl_code:{digits:true,minlength:1,maxlength:10}},messages:{name:{required:P_NAME_MSG,
alphanumericunderscore:P_NAME_MSG},program_code:{required:P_PROGRAM_CODE_MSG,alphanumeric:P_PROGRAM_CODE_MSG},
description:{required:P_DESC_MSG,alphanumericunderscore:P_DESC_MSG}}})});function setIsDonation(){}