$(function(){var grid=jQuery("#program_list"),emptyMsgDivPass=$('<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");
$("#program_list").jqGrid({mtype:"POST",url:"/crm-program-popup-list-data",datatype:"json",colNames:[L_PROGRAM_NAME,
L_PROGRAM_CODE,L_IS_AVAILABLE_FOR_DONATION],colModel:[{name:L_PROGRAM_NAME,index:"program"},{name:L_PROGRAM_CODE,index:
"program_code"},{name:L_IS_AVAILABLE_FOR_DONATION,index:"is_available_for_donation"}],viewrecords:true,sortname:
"program.program_id",sortorder:"asc",rowNum:50,rowList:[50,100,150],pager:"#pcrud_program",viewrecords:true,autowidth:
false,shrinkToFit:true,caption:"",width:"570",cmTemplate:{title:false},loadComplete:function(){var count=grid.
getGridParam(),ts=grid[0];if(ts.p.reccount===0){grid.hide();emptyMsgDivPass.show();$(
"#pcrud_program_right div.ui-paging-info").css("display","none")}else{grid.show();emptyMsgDivPass.hide();$(
"#pcrud_program_right div.ui-paging-info").css("display","block")}}});emptyMsgDivPass.insertAfter(grid.parent());
emptyMsgDivPass.hide();$("#program_list").jqGrid("navGrid","#pcrud_program",{reload:true,edit:false,add:false,search:
false,del:false})});function selectProgram(programName,programId){parent.$("#program").val(programName);parent.$(
"#program_id").val(programId);parent.$.fn.colorbox.close()}