$(function() {
    $.jgrid.no_legacy_api = true;
    $.jgrid.useJSON = true;
    var grid = jQuery("#list"), emptyMsgDiv = $(
            '<div class="no-record-msz">' + NO_RECORD_FOUND + "</div>");
    $("#list").jqGrid({mtype: "POST", url: "/crm-membership", datatype:
                "json", sortable: true, colNames: [L_TITLE, L_LEVEL_COLOR, L_MINIMUM_DONATION_AMOUNT_GRID, L_STATUS, ACTION], colModel: [{name:
                        "Title", index: "membership_title"}, {name: "Level Color", index: "level_color", align: "center"}, {name: "Minimum Donation", index
                        : "tbl_membership.minimun_donation_amount", align: "center"}, {name: "Status", index: "is_active", align: "center"}, {name: "Action", index:
                        "action", sortable: false}], viewrecords: true, sortname: "tbl_membership.modified_date", sortorder: "DESC", rowNum: numRecPerPage
                , rowList: pagesArr, pager: "#pcrud", viewrecords:true, autowidth: true, shrinkToFit: true, caption: "", width: "100%", cmTemplate: {
            title: false}, loadComplete: function() {
            hideAjxLoader();
            var count = grid.getGridParam(), ts = grid[0];
            if (ts.p.reccount === 0) {
                grid
                        .hide();
                emptyMsgDiv.show();
                $("#pcrud_right div.ui-paging-info").css("display", "none")
            } else {
                grid.show();
                emptyMsgDiv.hide(
                        );
                $("#pcrud_right div.ui-paging-info").css("display", "block")
            }
        }});
    emptyMsgDiv.insertAfter(grid.parent());
    emptyMsgDiv.
            hide();
    $("#list").jqGrid("navGrid", "#pcrud", {reload: true, edit: false, add: false, search: false, del: false})
});
function
        deleteMembership(membershipId) {
    $(".delete_membership").colorbox({width: "700px", height: "200px", inline: true});
    $(
            "#no_delete_membership").click(function() {
        $.colorbox.close();
        $("#yes_delete_membership").unbind("click");
        return false
    });
    $("#yes_delete_membership").click(function() {
        $.colorbox.close();
        $("#yes_delete_membership").unbind("click");
        showAjxLoader();
        $.ajax({type: "POST", dataType: "html", data: {membershipId: membershipId}, url: "/delete-crm-membership",
            success: function(responseData) {
                hideAjxLoader();
                if (!checkUserAuthenticationAjx(responseData))
                    return false;
                var jsonObj =
                        JSON.parse(responseData);
                if (jsonObj.status == "success")
                    window.location.href = "/crm-membership";
                else
                    $("#error_message").
                            html(jsonObj.message)
            }})
    })
}
function changeStatus(membershipId, status) {
    $(".success-msg").css("display", "none");
    showAjxLoader();
    $.ajax({type: "POST", dataType: "json", data: {membershipId: membershipId, status: status}, url:
                "/change-crm-membership-status", success: function(responseData) {
            hideAjxLoader();
            if (!checkUserAuthenticationAjx(
                    responseData))
                return false;
            var jsonObj = responseData;
            if (jsonObj.status == "success") {
                var newStatus = status == 1 ? 0 : 1,
                        newStatusLabel = status == 1 ? "Inactive" : "Active";
                $("#status_" + membershipId).html('<a onclick="changeStatus(' + membershipId +
                        "," + newStatus + ')" href="javascript:void(0);">' + newStatusLabel + '<div class="tooltip">' + newStatusLabel +
                        "<span></span></div></a>")
            } else
                $("#error_message").html(jsonObj.message)
        }})
}