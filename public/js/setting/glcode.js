$(document).ready(function(){$("#glcode").validate({submitHandler:function(){var dataStr=$("#glcode").serialize();
showAjxLoader();$.ajax({type:"POST",url:"/manage-gl-codes",data:dataStr,success:function(responseData){hideAjxLoader();
if(!checkUserAuthenticationAjx(responseData))return false;var jsonObj=jQuery.parseJSON(responseData);if(jsonObj.status==
"success")window.location.href="/manage-gl-codes";else $.each(jsonObj.message,function(i,msg){$("#"+i).after(
'<label class="error" style="display:block;">'+msg+"</label>")})}})},rules:{W_O_H:{digits:true,minlength:1,maxlength:10}
,F_O_F:{digits:true,minlength:1,maxlength:10},INV:{digits:true,minlength:1,maxlength:10},UN_DON:{digits:true,minlength:1
,maxlength:10},UN_DON_M:{digits:true,minlength:1,maxlength:10},KIO_FEE:{digits:true,minlength:1,maxlength:10},PASS_DOC:{
digits:true,minlength:1,maxlength:10},INV_PRO:{digits:true,minlength:1,maxlength:10},SHI_AND_HAND:{digits:true,minlength
:1,maxlength:10},TAX:{digits:true,minlength:1,maxlength:10},REFUND:{digits:true,minlength:1,maxlength:10}}})})