$(function(){$.validator.addMethod("namealphanumspecialchar",function(value,element){return this.optional(element)||
/^[A-Za-z0-9\n\r ,.-]+$/i.test(value)},GROUP_NAME_VALID);$.validator.addMethod("descalphanumspecialchar",function(value,
element){return this.optional(element)||/^[A-Za-z0-9\n\r ,.-]+$/i.test(value)},GROUP_DESCRIPTION_VALID);jQuery.validator
.addMethod("alphanumericspecialcharext",function(value,element){return this.optional(element)||
/^[A-Za-z0-9 ~@!#$%&*?{}+\/,.\-_]+$/i.test(value)},GROUP_DESCRIPTION_VALID);$("#report_contact_group").validate({
submitHandler:function(){var dataStr=$("#report_contact_group").serialize();showAjxLoader();$.ajax({type:"POST",url:
"/add-report-contacts-to-group",data:dataStr,success:function(responseData){hideAjxLoader();if(!
checkUserAuthenticationAjx(responseData))return false;var jsonObj=jQuery.parseJSON(responseData);if(jsonObj.status==
"success"){$("#success_message").show();$("#success_message").html("<div>"+jsonObj.message+"</div>");setTimeout(function
(){parent.$.colorbox.close()},3000)}else{$("#success_message").hide();$("#success_message").html("");$.each(jsonObj.
message,function(i,msg){$("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")})}}})},rules:{
group_id:{required:true},group_name:{required:true,namealphanumspecialchar:true,minlength:2,maxlength:50},
group_description:{required:true,alphanumericspecialcharext:true,minlength:2,maxlength:200}},messages:{group_id:{
required:GROUP_NAME_SELECT},group_name:{required:GROUP_NAME_EMPTY},group_description:{required:GROUP_DESCRIPTION_EMPTY}}
});$("#cancel").click(function(){parent.$.colorbox.close()})});function showCreateGroup(val){if(val==1){$(
"#addToGroupBlock").show();$("#createGroupBlock").hide()}else{$("#addToGroupBlock").hide();$("#createGroupBlock").show()
}}