var listReportGrid;$(function(){listReportGrid=jQuery("#list_report");var emptyMsgDiv=$('<div class="no-record-msz">'+
NO_RECORD_FOUND+"</div>");$(listReportGrid).jqGrid({mtype:"POST",url:redirectUrl,datatype:"json",sortable:true,postData:
{is_main_report:"0"},colNames:[L_NAME,L_DESCRIPTION,L_ACTIONS],colModel:[{name:"Name",index:"tbl_rep.report_name"},{name
:"Description",index:"tbl_rep.description"},{name:"Action",sortable:false,cmTemplate:{title:false},classes:"none"}],
viewrecords:true,sortname:"modified_date",sortorder:"desc",rowNum:numRecPerPage,rowList:pagesArr,pager:"#listreportrud",
viewrecords:true,autowidth:true,shrinkToFit:true,caption:"",width:"100%",cmTemplate:{title:false},loadComplete:function(
){hideAjxLoader();var count=listReportGrid.getGridParam(),ts=listReportGrid[0];if(ts.p.reccount===0){listReportGrid.hide
();emptyMsgDiv.show();$("#listreportrud_right div.ui-paging-info").css("display","none")}else{listReportGrid.show();
emptyMsgDiv.hide();$("#listreportrud_right div.ui-paging-info").css("display","block")}}});emptyMsgDiv.insertAfter(
listReportGrid.parent());emptyMsgDiv.hide();$(listReportGrid).jqGrid("navGrid","#listreportrud",{reload:true,edit:false,
add:false,search:false,del:false});listAddReportGrid=jQuery("#list_add_report");var emptyMsgDiv1=$(
'<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");$(listAddReportGrid).jqGrid({mtype:"POST",url:redirectUrl,
datatype:"json",sortable:true,postData:{is_main_report:"1"},colNames:[L_NAME,L_DESCRIPTION],colModel:[{name:"Name",index
:"tbl_rep.report_name"},{name:"Description",index:"tbl_rep.description"}],viewrecords:true,sortname:"modified_date",
sortorder:"desc",rowNum:numRecPerPage,rowList:pagesArr,pager:"#listaddreportrud",viewrecords:true,autowidth:true,
shrinkToFit:true,caption:"",width:"100%",cmTemplate:{title:false},loadComplete:function(){hideAjxLoader();var count=
listAddReportGrid.getGridParam(),ts=listAddReportGrid[0];if(ts.p.reccount===0){listAddReportGrid.hide();emptyMsgDiv1.
show();$("#listaddreportrud_right div.ui-paging-info").css("display","none")}else{listAddReportGrid.show();emptyMsgDiv1.
hide();$("#listaddreportrud_right div.ui-paging-info").css("display","block")}}});emptyMsgDiv1.insertAfter(
listAddReportGrid.parent());emptyMsgDiv1.hide();$(listAddReportGrid).jqGrid("navGrid","#listaddreportrud",{reload:true,
edit:false,add:false,search:false,del:false})});function viewAllReport(){showAjxLoader();$.ajax({type:"POST",url:
"/generate-all-reports",success:function(responseData){hideAjxLoader();if(!checkUserAuthenticationAjx(responseData))
return false;var jsonObj=jQuery.parseJSON(responseData);if(jsonObj.status=="success")generateAllReportsGrid(jsonObj)}});
return false}function deleteReport(reportId){$(".delete_report").colorbox({width:"700px",height:"200px",inline:true});$(
"#no_delete_report").click(function(){$.colorbox.close();$("#yes_delete_report").unbind("click");return false});$(
"#yes_delete_report").click(function(){$.colorbox.close();$("#yes_delete_report").unbind("click");showAjxLoader();$.ajax
({type:"POST",dataType:"html",data:{reportId:reportId},url:"/delete-report",success:function(responseData){hideAjxLoader
();if(!checkUserAuthenticationAjx(responseData))return false;var jsonObj=JSON.parse(responseData);if(jsonObj.status==
"success")window.location.href=redirectUrl;else $("#error_message").html(jsonObj.message)}})})}