var totalReportOrderBy=typeof totalReportOrderBy!="undefined"?totalReportOrderBy:1;
function addNewOrderBy(currAnchor){
    showAjxLoader();
    $.ajax({
        type:"POST",
        url:"/add-report-order-by",
        data:{
            totalReportOrderBy:totalReportOrderBy,
            reportId:$(
                "#report_id").val()
        },
        success:function(responseData){
            hideAjxLoader();
            if(!checkUserAuthenticationAjx(responseData))
                return false;
            var allDivBlock=$("div[id^='report_order_by_block']");
            if(allDivBlock.length==1){
                var parId=$(currAnchor).
                parents('div[id^="report_order_by_block"]').attr("id"),id=parId.substring(parId.lastIndexOf("_")+1,parId.length);
                $(
                    currAnchor).parent().append('<a class="minus" href="javascript:void(0);" onclick="deleteNewOrderBy('+id+')"></a>')
            }
            $(
                currAnchor).remove();
            $("#report_order_by_div").append(responseData);
            totalReportOrderBy=totalReportOrderBy+1
        }
    })
}
function deleteNewOrderBy(id){
    $("#report_order_by_block_"+id).remove();
    var lastBlock=$("div[id^='report_order_by_block']").last()
    ;
    if($(lastBlock).find(".plus").length==0)$(lastBlock).children("div").last().append(
        '<a class="plus m-l-5" href="javascript:void(0);" onclick="addNewOrderBy(this)" id="addMoreOrderBy"></a>');
    var 
    allDivBlock=$("div[id^='report_order_by_block']");
    if(allDivBlock.length==1)$(lastBlock).find(".minus").remove()
}
function initAllDatePickers(){
    var allCalenderFrom=$(".calenderFrom");
    $(allCalenderFrom).each(function(){
        var currFromId=$(this).
        attr("id"),nameOfCol=currFromId.substr(0,currFromId.lastIndexOf("_")),currToId=nameOfCol+"_to";
        $("#"+currFromId).
        datepicker({
            changeMonth:true,
            changeYear:true,
            showOtherMonths:true,
            onClose:function(selectedDate){
                $("#"+currToId).
                datepicker("option","minDate",selectedDate)
            }
        });
        $("#"+currToId).datepicker({
            changeMonth:true,
            changeYear:true,
            showOtherMonths:true,
            onClose:function(selectedDate){
                $("#"+currFromId).datepicker("option","maxDate",selectedDate)
            }
        })
    })
}
var showFilterValueJson;
$(function(){
    showFilterValueJson=jQuery.parseJSON(isShowValueJson);
    initAllDatePickers();
    reloadGridInfo();
    $("#create_report").validate({
        submitHandler:function(){
            if(dateValidate()){
                if(cutomValidateOrderBy()){
                    var dataStr=$("#create_report").serialize();
                    showAjxLoader();
                    $.ajax({
                        type:"POST",
                        url:"/save-report",
                        data:dataStr,
                        success:
                        function(responseData){
                            hideAjxLoader();
                            if(!checkUserAuthenticationAjx(responseData))return false;
                            var jsonObj=jQuery.
                            parseJSON(responseData);
                            if(jsonObj.status=="success"){
                                
								if(typeof(jsonObj.message)!='undefined'){
									$("#success_message").show();
									$("#success_message").html("<div>"+
                                    jsonObj.message+"</div>");
								}
                                $("#report_id").val(jsonObj.reportId);
                                reloadGridInfo(jsonObj.isRunOnly);
                            }else $.each(jsonObj.message,function(i,
                                msg){
                                $("#success_message").hide();
                                $("#success_message").html("");
                                $("#"+i).after(
                                    '<label class="error" style="display:block;">'+msg+"</label>")
                            })
                        }
                    })
                }
            }else $(".detail-section").show()
        },
        rules:{
            report_title:{
                required:true
            }
        },
        messages:{
            report_title:{
                required:TITLE_EMPTY
            }
        }
    });
    $(document).on("click",
        "#addReportContactToGroup",function(){
            $.colorbox({
                href:"/add-report-contacts-to-group/"+encryptReportId,
                width:"900px",
                height:"500px",
                iframe:true
            })
        })
});
function reloadGridInfo(isRunOnly){
	if(typeof(isRunOnly)=='undefined' || isRunOnly=='0'){
		showAjxLoader();
		$.ajax({
			type:"POST",
			url:
			"/get-reload-grid-info",
			data:{
				report_id:$("#report_id").val()
			},
			success:function(responseData){
				hideAjxLoader();
				if(!
					checkUserAuthenticationAjx(responseData))return false;
				responseData=jQuery.parseJSON(responseData);
				generateAllReportsGrid
				(responseData)
			}
		});
	}else{
        var dataStr = $("#create_report").serialize();
        showAjxLoader();
		$.ajax({type: "POST", url:
					"/get-reload-grid-info", data: {report_id: $("#report_id").val(), data: dataStr, isRunOnly: isRunOnly}, success: function(responseData) {
				hideAjxLoader();
				if (!
						checkUserAuthenticationAjx(responseData))
					return false;
				responseData = jQuery.parseJSON(responseData);
				generateAllReportsGrid
						(responseData)
			}});
		}    
}
function cutomValidateOrderBy(){
    var allOrderColumns=$('select[name="order_columns[]"]'),isValid=true,
    orderColumnsArr=[];
    $(allOrderColumns).each(function(){
        var eleValid=true;
        if($(this).next().attr("class")=="error")$(this)
            .next().remove();
        if($(this).val()!=""){
            for(i=0;i<orderColumnsArr.length;i++)if(orderColumnsArr[i]==$(this).val()){
                eleValid=false;
                isValid=false;
                $(this).after(
                    '<label class="error" style="display:block;">Column already selected.</label>');
                break
            }
            if(eleValid)orderColumnsArr.push($
                (this).val())
        }
    });
    var allRoles=$("#role_id option:selected");
    if(allRoles.length==0){
        $("#role_id").after(
            '<label class="error" style="display:block;">'+ROLE_EMPTY+"</label>");
        isValid=false
    }
    return isValid
}
function changeFilter
(currFilter,columnId){
	
    var filterId=$(currFilter).val(),filterValue=eval("showFilterValueJson.filter_id_"+filterId);
	if(typeof('#filter_value_'+columnId+'_from')!='undefined'){
		$('#filter_value_'+columnId+'_from').val('');
		$('#filter_value_'+columnId+'_to').val('');
	}
		
    if(
        filterValue!=="undefined")if(filterValue=="Y")$(".filterValues"+columnId).show();else $(".filterValues"+columnId).hide()
    ;
    if(filterId==12||filterId==16){
        $("#filter_value_"+columnId+"_between1").hide();
        $("#filter_value_"+columnId+"_between2")
        .show()
    }else{
        $("#filter_value_"+columnId+"_between1").show();
        $("#filter_value_"+columnId+"_between2").hide()
    }

	
}
function dateValidate(){
    $(".error").hide();
    var flag=true,jsonData=jQuery.parseJSON(dateColJson);
    for(i=0;i<jsonData.length;i++){
        var id="filter_columns_"+jsonData[i],valueFrom="filter_value_"+jsonData[i]+"_from",valueTo="filter_value_"+jsonData[i]+
        "_to",val=$("#"+id).val();
        if(val!=""){
            if(val=="17"){
                if($("#"+valueFrom).val()==""){
                    $("#"+valueFrom).after(
                        '<span class="error" for="'+valueFrom+
                        '" generated="true" style="display:block !important;"><span class="redarrow"></span>Select valid from date</span>');
                    flag
                    =false
                }
                if($("#"+valueTo).val()==""){
                    $("#"+valueTo).after('<span class="error date_to_error" for="'+valueTo+
                        '" generated="true" style="display:block !important;"><span class="redarrow"></span>Select valid to date</span>');
                    flag=
                    false
                }
                var start=$("#"+valueFrom).datepicker("getDate"),end=$("#"+valueTo).datepicker("getDate"),days=(end-start)/1000/60
                /60/24;
                if(days>365){
                    $("#"+valueFrom).after('<span class="error" for="'+valueFrom+
                        '" generated="true" style="display:block !important;"><span class="redarrow"></span>Max date range 1 year</span>');
                    flag=false
                }
            }
        }else{
            $("#"+id).after('<span class="error" for="'+id+
                '" generated="true" style="display:block !important;"><span class="redarrow"></span>Select valid option</span>');
            flag=
            false
        }
    }
    return flag
}
function genExcel(){
    showAjxLoader();
    $.ajax({
        type:"POST",
        url:"/create-excel",
        data:{
            report_id:$("#report_id").val()
        },
        success:function(responseData){
            hideAjxLoader();
            if(!
                checkUserAuthenticationAjx(responseData))return false;
            responseData=jQuery.parseJSON(responseData);
            generateAllReportsGrid
            (responseData)
        }
    })
}
