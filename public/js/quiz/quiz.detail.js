$(function(){if(mode=="view")getQuizDetail();else editQuizDetails();$("a[id^='tab-']").removeClass("active");var 
tab_id_load=mode=="view"?"1":"2";$("#tab-"+tab_id_load).addClass("active");$("a[id^='tab-']").click(function(){$(
"a[id^='tab-']").removeClass("active");$(this).addClass("active")})});function editQuizDetails(){showAjxLoader();$.ajax(
{type:"GET",data:{},url:"/edit-crm-quiz/"+encrypt_quiz_id,success:function(response){if(!checkUserAuthenticationAjx(
response))return false;$("#quizContent").html(response);hideAjxLoader()}})}function getQuizDetail(){showAjxLoader();$.
ajax({type:"POST",data:{},url:"/get-crm-quiz-info/"+encrypt_quiz_id,success:function(response){if(!
checkUserAuthenticationAjx(response))return false;$("#quizContent").html(response);hideAjxLoader()}})}function 
quizQuestionsDetails(){showAjxLoader();$.ajax({type:"POST",data:{},url:"/get-quiz-questions-info/"+encrypt_quiz_id,
success:function(response){if(!checkUserAuthenticationAjx(response))return false;$("#quizContent").html(response);
hideAjxLoader()}})}function viewChangeLog(){$.ajax({type:"POST",data:{module:module_name,id:encrypt_quiz_id},url:
"/quiz-change-log/"+module_name+"/"+encrypt_quiz_id,success:function(response){$("#quizContent").html(response)}})}