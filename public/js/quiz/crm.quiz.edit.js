$(function(){$("#start_date").datepicker({changeMonth:true,changeYear:true,onClose:function(selectedDate){$("#end_date")
.datepicker("option","minDate",selectedDate)}});$("#expiration_date").datepicker({changeMonth:true,changeYear:true,
minDate:0});$("#end_date").datepicker({changeMonth:true,changeYear:true,onClose:function(selectedDate){$("#start_date").
datepicker("option","maxDate",selectedDate)}});$("#start_time,#expiration_time").timepicker({timeFormat:"h:i A"});$(
"input").customInput();jQuery.validator.addMethod("alphanemeric",function(value,element){return this.optional(element)||
/^[a-zA-Z0-9 "'?!© ®℠℗™,.-]+$/i.test(value)},"Please enter valid details");jQuery.validator.addMethod(
"alphanemericspecial",function(value,element){return this.optional(element)||/^[a-zA-Z0-9 "'?!© ®℠℗™,.-]+$/i.test(value)
},"Please enter valid details");$("#create_quiz").validate({submitHandler:function(){showAjxLoader();$.ajax({type:"POST"
,data:$("#create_quiz").serialize(),url:"/edit-crm-quiz",success:function(response){var jsonObj=jQuery.parseJSON(
response);if(jsonObj.status=="success")location.href="/crm-quizes";else $.each(jsonObj.message,function(i,msg){$("#"+i).
after('<label class="error" style="display:block;">'+msg+"</label>")});hideAjxLoader()}})},rules:{quiz_title:{required:
true,alphanemeric:true},quiz_tagline:{required:true,alphanemeric:true},main_category_id:{required:true},expiration_date:
{required:true},expiration_time:{required:true},brief_description:{alphanemericspecial:true},full_description:{
alphanemericspecial:true},avail_member:{required:function(elem){var length=$(".availability:checked").length;if(length==
0)return true;else return false}}},messages:{quiz_title:{required:TITLE_MSG},quiz_tagline:{required:QUIZ_TAGLINE_MSG},
main_category_id:{required:QUIZ_CATEGORY_MSG},expiration_date:{required:QUIZ_EXPIRATION_DATE_MSG},expiration_time:{
required:QUIZ_EXPIRATION_TIME_MSG},avail_member:{required:AVAILABILITY_MSG},avail_member_demo:{required:
AVAILABILITY_MSG_DEMO}}});var progressbarid="progress";$(".attachmentfiles").focus(function(){var file_id=$(this).attr(
"id"),file_number=file_id.substr(file_id.length-1,1);progressbarid="progress_"+file_number});var url=
"/upload-quiz-image";$(".attachmentfiles").fileupload({url:url,dataType:"json",done:function(e,data){var file_id=$(this)
.attr("id"),file_number=file_id.substr(file_id.length-1,1);data=data.result;if(data.status=="success"){$(
"#main_image_msg").addClass("success-msg");$("#main_image_msg").removeClass("error-msg");$("#new_image_hidden").val(data
.filename)}else{$("#main_image_msg").removeClass("success-msg");$("#main_image_msg").addClass("error-msg");$(
"#new_image_hidden").val("")}$("#main_image_msg").html(data.message)},progressall:function(e,data){},start:function(e){
showAjxLoader()},stop:function(e){hideAjxLoader()}});$("#main_category_id").change(function(){$(
"div[class^='main_category_id']").hide();if($(".main_category_id"+this.value))$(".main_category_id"+this.value).show()})
;if($(".main_category_id"+$("#main_category_id").val()))$(".main_category_id"+$("#main_category_id").val()).show()});
function testAlphaNumeric(value){return/^[a-zA-Z0-9 ,.-]+$/i.test(value)}