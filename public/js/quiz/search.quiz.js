$(function(){$("#savebutton").click(function(){if($.trim($("#search_name").val())!=""){$("#search_msg").html("");$(
"#search_msg").css("display","none");showAjxLoader();$.ajax({type:"POST",data:{search_name:$.trim($("#search_name").val(
)),searchString:$("#search_quiz").serialize(),search_id:$("#search_id").val()},url:"/add-crm-search-quiz",success:
function(response){var jsonObj=jQuery.parseJSON(response);if(jsonObj.status=="success"){hideAjxLoader();$("#search_name"
).val("");document.getElementById("search_quiz").reset();location.reload()}else{$("#search_msg").html(
ERROR_SAVING_SEARCH);$("#search_msg").addClass("error");$("#search_msg").removeClass("success-msg clear");$(
"#search_msg").css("display","block")}}})}else{$("#search_msg").html(SEARCH_NAME_MSG);$("#search_msg").addClass("error")
;$("#search_msg").removeClass("success-msg clear");$("#search_msg").css("display","block");return false}$(
"#main_category_id").change(function(){$("div[class^='main_category_id']").hide();if($(".main_category_id"+this.value))$
(".main_category_id"+this.value).show()});if($(".main_category_id"+$("#main_category_id").val()))$(".main_category_id"+$
("#main_category_id").val()).show()});function generateSavedSearch(){$("#saved_search option").each(function(){$(this).
remove()});$.ajax({type:"POST",data:{},url:"/get-quiz-saved-search-list",success:function(response){var jsonObj=jQuery.
parseJSON(response);jQuery.each(jsonObj,function(key,value){$("#saved_search").append("<option value='"+key+"'>"+value+
"</option>")})}})}});function getSavedQuizSearchResult(){$("#search_msg").css("display","none");$("#search_msg").html(""
);if($("#saved_search").val()!=""){$("#savebutton").val("Update");$("#deletebutton").show()}else{$("#savebutton").val(
"Save");$("#search_name").val("");$("#deletebutton").hide()}if($("#saved_search").val()!="")$.ajax({type:"POST",data:{
search_id:$("#saved_search").val()},url:"/get-crm-search-saved-quiz",success:function(response){var searchParamArray=
response,obj=jQuery.parseJSON(searchParamArray);document.getElementById("search_quiz").reset();$("#quiz_title").val(obj.
quiz_title);$("#main_category_id").val(obj.main_category_id).select2();$("div[class^='main_category_id']").hide();if($(
".main_category_id"+$("#main_category_id").val()))$(".main_category_id"+$("#main_category_id").val()).show();$(
"#added_date_range").val(obj.added_date_range).select2();$("#start_date").val(obj.start_date);$("#end_date").val(obj.
end_date);$("#avail_member").select2("val",obj.avail_member);if(obj.added_date_range=="1")$("#date_range_div").show();
if(obj.avail_member=="1"){$("#avail_member").val(obj.avail_member);$("#avail_member").attr("checked",true).customInput()
}if(obj.avail_non_member=="1"){$("#avail_non_member").val(obj.avail_non_member);$("#avail_non_member").attr("checked",
true).customInput()}if(obj.avail_anonymous=="1"){$("#avail_anonymous").val(obj.avail_anonymous);$("#avail_anonymous").
attr("checked",true).customInput()}if(obj.is_featured=="1"){$("#is_featured").val(obj.is_featured);$("#is_featured").
attr("checked",true).customInput()}$("#quiz_status").select2("val",obj.quiz_status).select2();$("#is_featured").select2(
"val",obj.is_featured);$("#search_name").val(obj.search_title);$("#search_id").val($("#saved_search").val());$(
"#searchbuttonquiz").click()}})}function deleteSaveSearch(){$(".delete_saved_search").colorbox({width:"700px",height:
"200px",inline:true});$("#no_delete").click(function(){$.colorbox.close();$("#yes_delete").unbind("click");return false}
);$("#yes_delete").click(function(){$("#yes_delete").unbind("click");$.ajax({type:"POST",data:{search_id:$("#search_id")
.val()},url:"/delete-crm-search-quiz",success:function(response){var jsonObj=jQuery.parseJSON(response);if(jsonObj.
status=="success")location.reload()}})})}