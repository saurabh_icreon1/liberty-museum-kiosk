$(function() {
    $("#add_another_question").click(function() {
        $("#create_quiz_questions").validate({submitHandler: function() {
                showAjxLoader();
                $.ajax({type: "POST", data: $("#create_quiz_questions").serialize(), url: "/get-quiz-questions-info", success:
                            function(response) {
                                var jsonObj = jQuery.parseJSON(response);
                                if (jsonObj.status == "success") {
                                    $("#success_message_disp").html(
                                            jsonObj.message);
                                    $("#success_message_ajax").show();
                                    $("html, body").animate({scrollTop: parseInt($("#success_message_ajax"
                                                ).offset().top) - parseInt(10)}, 2000);
                                    document.getElementById("create_quiz_questions").reset();
                                    showQuestionsGridQuestions(
                                            encrypt_quiz_id);
                                    $("div[class^='row1 question_added']").remove()
                                } else
                                    $.each(jsonObj.message, function(i, msg) {
                                        $("#" + i).
                                                after('<label class="error" style="display:block;">' + msg + "</label>")
                                    });
                                hideAjxLoader()
                            }})
            }, rules: {quiz_question_choice: {
                    required: function() {
                        var length = parseInt($("#count_div").text());
                        if (length == 0)
                            return true;
                        else
                            return false
                    }}}, messages: {
                quiz_question_choice: {required: PLEASE_ENTER_QUIZ_QUESTION_CHOICE}}});
        var status = $("#create_quiz_questions").valid(),
                content = tinyMCE.get("quiz_question").getContent(), flag = true;
        if (content == "") {
            $("#quiz_question_error").show();$('#quiz_question_edit_exceed').hide();
            flag = false
        }
        else{
            $("#quiz_question_error").hide();
            if (getStats('quiz_question').chars > 150){
                $('#quiz_question_edit_exceed').show();
                flag = false
            }
        }
        var length = $(".option_selection_quiz:checked").length, length_choices = parseInt($(
                "#count_div").text());
        if (length_choices > 0) {
            if (length == 0) {
                $("#quiz_correct_option_edit").show();
                flag = false
            }
        } else {
            $(
                    "#quiz_question_choice_edit_message").show();
            flag = false
        }
        if (flag == false)
            return false
    })
});
var choicesValues = [];
$(document)
        .on("click", "#add_choices", function() {
    var choice = $.trim($("#quiz_question_choice").val());
    if (choice != "") {
        var count_div =
                parseInt($("#count_div").text()), div_content =
                '<div class="row1 question_added"><label>&nbsp;</label><div class="col11 action"><a href="javascript:void(0)" class="link edit eitd-icon-bt" id="edit_'
                + count_div +
                '"><div class="tooltip">Edit<span></span></div></a>&nbsp;&nbsp;<a class="link delete delete-icon-bt" href="javascript:void(0)"><div class="tooltip">Delete<span></span></div></a> </div><div class="col7 rel-auto" id="choice_'
                + count_div + '"><span id="choicetext_' + count_div + '" class="text-bold">' + choice +
                '</span><input type="hidden" id="question_choice_' + count_div + '" name="question_choice[]" value="' + choice +
                '" /> </div><div class="col11"><span class="text-bold"><input id="correct_choice_count_' + count_div +
                '" name="correct_choices[]" type="radio" class="e3 correct_choice_ans option_selection_quiz" /><label class="redio" for="correct_choice_count_'
                + count_div + '">&nbsp;</label></span></div></div>';
        $("#choice_div").append(div_content);
        $("#quiz_question_choice").val("")
                ;
        count_div = count_div + 1;
        $("#count_div").text(count_div)
    }
    $("input").customInput()
});
$(document).on("click", ".edit",
        function() {
            var id = $(this).attr("id");
            $(this).html('<div class="tooltip">Save<span></span></div>');
            $(this).removeClass(
                    "edit");
            $(this).removeClass("eitd-icon-bt");
            $(this).addClass("savetext save-icon-bt");
            var id_count = id.replace("edit_", ""
                    ), textinbox = $("#choicetext_" + id_count).text();
            $("#choice_" + id_count).html(
                    '<input type="text" class="choicetextval" id="choice_text_' + id_count + '" style="width:95% !important" value="' + textinbox +
                    '" /><label id="choice_text_error_' + id_count + '" for="choice_text_' + id_count +
                    '"  style="display:none;top:auto !important" generated="true" class="custom_error choice_error"><span class="redarrow"></span>Please enter choice</label>'
                    )
        });
$(document).on("click", ".savetext", function() {
    var id = $(this).attr("id"), id_count = id.replace("edit_", ""), textinbox = $.
            trim($("#choice_text_" + id_count).val());
    if (textinbox != "") {
        $("#choice_text_error_" + id_count).hide();
        $(this).html(
                '<div class="tooltip">Edit<span></span></div>');
        $(this).removeClass("savetext");
        $(this).removeClass("save-icon-bt");
        $(
                this).addClass("edit eitd-icon-bt");
        $("#choice_" + id_count).html('<span id="choicetext_' + id_count + '" class="text-bold">' +
                textinbox + '</span><input type="hidden" id="question_choice_' + id_count + '" name="question_choice[]" value="' + textinbox +
                '" />')
    } else
        $("#choice_text_error_" + id_count).show()
});
$(document).on("focus", ".choicetextval", function() {
    $(this).next(
            "label.error").hide()
});
$(document).on("focusout", ".choicetextval", function() {
    if ($(this).val() == "")
        $(this).next(
                "label.error").show()
});
$(document).on("click", ".delete", function() {
    var count_div = parseInt($("#count_div").text());
    $(
            "#count_div").text(count_div - 1);
    $(this).closest("div.row1").remove()
});
$(document).on("click", ".correct_choice_ans",
        function() {
            var id = $(this).attr("id"), selected_index = id.replace("correct_choice_count_", "");
            $("#correct_index").val(
                    selected_index)
        });
function showQuestionsGridQuestions(quizId) {
    $.ajax({type: "POST", data: quizId, url:
                "/get-questions-grid-edit/" + quizId, success: function(response) {
            $("#quizQuestionsAnswers").html(response)
        }})
}
$(document).
        on("click", ".delete_question", function() {
    var id = $(this).attr("id"), selected_question = id.replace("question_", "")
});
function deleteQuizQuestion(questionid) {
    $(".delete_question").colorbox({width: "700px", height: "200px", inline: true});
    $(
            "#no_delete_quiz_question").click(function() {
        $.colorbox.close();
        $("#yes_delete_quiz_question").unbind("click");
        return false
    });
    if ($("#redirect"))
        var redirect = $("#redirect").html();
    else
        var redirect = "";
    $("#yes_delete_quiz_question").
            click(function() {
        $.colorbox.close();
        $("#yes_delete_quiz_question").unbind("click");
        showAjxLoader();
        $.ajax({type: "POST",
            dataType: "json", data: {questionid: questionid}, url: "/delete-quiz-question", success: function(responseData) {
                hideAjxLoader();
                var jsonObj = responseData;
                if (jsonObj.status == "success")
                    if (redirect != "" && redirect == "QuestionEdit")
                        showQuestionsGridQuestions(encrypt_quiz_id);
                    else {
                        var redirecturl = "/create-crm-quiz-questions/" + $(
                                "#quiz_id_encrypted_hidden").val();
                        window.location.href = redirecturl
                    }
                else
                    $("#error_message").html(jsonObj.message)
            }})
    })
}
function editQuizQuestion(quizId, questionid_encrypter, questionid) {
    showAjxLoader();
    $(".onlyphtml").html("").hide();
    $(
            ".questiondisplay").show();
    $.ajax({url: "/create-quiz-edit-question/" + quizId + "/" + questionid_encrypter, success: function(
                response) {
            $("#question_" + questionid).hide();
            $("#question_div_" + questionid).html(response).show();
            hideAjxLoader()
        }})
}
$(
        document).on("click", "#add_choices_question", function() {
    var choice = $.trim($("#quiz_question_choice_edit").val());
    if (
            choice != "") {
        var count_div = parseInt($("#count_div_question").text()), lengthnew = $(".correct_choice_ans_question_new").
                length, div_content =
                '<div class="row1"><label>&nbsp;</label><div class="col11 action"><a href="javascript:void(0)" class="edit_q link eitd-icon-bt" id="edit_'
                + count_div +
                '"><div class="tooltip">Edit<span></span></div></a>&nbsp;&nbsp;<a class="delete_q link delete-icon-bt" href="javascript:void(0)"><div class="tooltip">Delete<span></span></div></a> </div><div class="col7 rel-auto" id="choice_question_'
                + count_div + '"><span id="choicetext_question_' + count_div + '" class="text-bold">' + choice +
                '</span><input type="hidden" id="question_choice_' + count_div + '" name="question_choice_new[]" value="' + choice +
                '" /> </div><div class="col11"><span class="text-bold"><input id="correct_choice_count_edit_' + count_div +
                '" name="correct_choices[]" value="' + lengthnew +
                '" type="radio" class="e3 correct_choice_ans_question_new option_selection" /><label class="redio" for="correct_choice_count_edit_'
                + count_div + '">&nbsp;</label></span></div></div>';
        $("#choice_div_edit").append(div_content);
        $(
                "#quiz_question_choice_edit").val("");
        count_div = count_div + 1;
        $("#count_div_question").text(count_div)
    }
    $("input").
            customInput()
});
$(document).on("click", ".edit_q", function() {
    var id = $(this).attr("id");
    $(this).html(
            '<div class="tooltip">Save<span></span></div>');
    var id_count = id.replace("edit_", "");
    $(this).removeClass("edit_q");
    $(this
            ).removeClass("eitd-icon-bt");
    $(this).addClass("savetext_q save-icon-bt");
    var textinbox = $("#choicetext_question_" +
            id_count).text();
    $("#choice_question_" + id_count).html(
            '<input type="text" class="choicetextval_question" id="choice_text_question_' + id_count +
            '" style="width:95% !important" value="' + textinbox + '" /><label id="choice_question_text_error_' + id_count +
            '" for="choice_question_text_' + id_count +
            '"  style="display:none;top:auto !important" generated="true" class="error choice_error"><span class="redarrow"></span>Please enter choice</label>'
            )
});
$(document).on("click", ".savetext_q", function() {
    var id = $(this).attr("id"), id_count = id.replace("edit_", ""), textinbox =
            $.trim($("#choice_text_question_" + id_count).val());
    if (textinbox != "")
        if (testAlphaNumeric(textinbox)) {
            $(
                    "#choice_question_text_error_" + id_count).hide();
            $(this).html('<div class="tooltip">Edit<span></span></div>');
            $(this).
                    removeClass("savetext_q");
            $(this).removeClass("save-icon-bt");
            $(this).addClass("edit_q eitd-icon-bt");
            if ($(this).
                    hasClass("oldedit"))
                $("#choice_question_" + id_count).html('<span id="choicetext_question_' + id_count +
                        '" class="text-bold">' + textinbox + '</span><input type="hidden" id="question_choice_' + id_count +
                        '" name="question_choice[]" value="' + textinbox + '" />');
            else
                $("#choice_question_" + id_count).html(
                        '<span id="choicetext_question_' + id_count + '" class="text-bold">' + textinbox +
                        '</span><input type="hidden" id="question_choice_' + id_count + '" name="question_choice_new[]" value="' + textinbox + '" />')
        }
        else {
            $("#choice_question_text_error_" + id_count).html("Please enter valid details");
            $("#choice_question_text_error_" +
                    id_count).show();
            return false
        }
    else {
        $("#choice_question_text_error_" + id_count).show().html("Please enter choice value");
        return false
    }
});
$(document).on("click", ".delete_q", function() {
    var count_div = parseInt($("#count_div_question").text());
    $(
            "#count_div_question").text(count_div - 1);
    $(this).closest("div.row1").remove();
    var val = [];
    $(
            ".correct_choice_ans_question").each(function(i) {
        val[i] = $(this).val()
    });
    var comma_separated_choice = val.join(",");
    $(
            "#all_choice_remain").val(comma_separated_choice)
});
function show_choice_popup_edit(classShow) {
    $("." + classShow).colorbox
            ({width: "600px", height: "400px", inline: true})
}
$(document).on("click", "#save_question_edit", function() {
    $(
            "#add_another_question").show();
    $("#save_question").show();
    $("#saveandpublish").show();
    var data = $("#edit_quiz_questions"
            ).serialize();
    tinyMCE.triggerSave();
    var buttonclick = $.trim($(this).attr("id"));
    $("#edit_quiz_questions").validate({
        submitHandler: function() {
            showAjxLoader();
            $.ajax({type: "POST", data: $("#edit_quiz_questions").serialize(), url:
                        "/create-quiz-edit-question", success: function(response) {
                    var jsonObj = jQuery.parseJSON(response);
                    if (jsonObj.status ==
                            "success") {
                        var quizid = $("#quiz_id_encrypted_hidden").val();
                        $("#success_message_ajax").show();
                        $("#success_message_disp").
                                html("Quiz Question Updated Successfully!");
                        $("html, body").animate({scrollTop: parseInt($("#success_message_ajax").
                                    offset().top) - parseInt(10)}, 2000);
                        showQuestionsGridQuestions(quizid)
                    } else
                        $.each(jsonObj.message, function(i, msg) {
                            $("#" + i
                                    ).after('<label class="error" style="display:block;">' + msg + "</label>")
                        });
                    hideAjxLoader()
                }})
        }, rules: {quiz_question_edit: {
                required: true}}, messages: {quiz_question_edit: {required: PLEASE_ENTER_QUIZ_QUESTION}}});
    var status = $(
            "#edit_quiz_questions").valid(), content = tinyMCE.get("quiz_question_edit").getContent(), flag_q = true;
    if (content == "") {
        $(
                "#quiz_question_edit_error").show();$('#quiz_question_edit_exceed').hide();
        flag_q = false
    } else{
        $("#quiz_question_edit_error").hide();        
        if (getStats('quiz_question_edit').chars > 150){
            $('#quiz_question_edit_exceed').show();
            flag_q = false
        }
    }
    var length_op = $(
            ".option_selection:checked").length, length_choices = parseInt($("#count_div_question").text());
    if (length_op == 0) {
        $(
                "#quiz_question_correct_option_edit").show();
        flag_q = false
    }
    if (length_choices > 1)
        if (length_op == 0) {
            $(
                    "#quiz_question_correct_option_edit").show();
            flag_q = false
        } else
            $("#quiz_question_correct_option_edit").hide();
    else {
        $(
                "#quiz_question_choice_edit_message").show();
        flag_q = false
    }
    if (flag_q == false)
        return false
});
$(document).on("click",
        ".correct_choice_ans_question", function() {
    $("#choice_type").val("old");
    var id = $(this).attr("id"), val = $(this).val(),
            selected_index = id.replace("correct_choice_count_", "");
    $("#correct_index_question").val(val)
});
$(document).on("click",
        ".correct_choice_ans_question_new", function() {
    $("#choice_type").val("new");
    var id = $(this).attr("id"), selected_index = id.
            replace("correct_choice_count_", ""), val = $(this).val();
    $("#correct_index_question").val(val)
});
$(document).on("focus",
        ".choicetextval_question", function() {
    $(this).next("label.error").hide()
});
$(document).on("focusout",
        ".choicetextval_question", function() {
    if ($(this).val() == "")
        $(this).next("label.error").show()
});
function testAlphaNumeric
        (value) {
    return/^[a-zA-Z0-9 ,.-]+$/i.test(value)
}
function getStats(id) {
    var body = tinymce.get(id).getBody(), text = tinymce.trim(body.innerText || body.textContent);

    return {
        chars: text.length,
        words: text.split(/[\w\u2019\'-]+/).length
    };
}