function getquizlistFront() {
    showAjxLoader();
    $.ajax({type: "POST", data: {searchString: $("#search_quiz").serialize(), page: $(
                    "#page").val()}, url: "/get-quiz-list", success: function(response) {
            $("#quiz_records").html(response);
            hideAjxLoader()
        }})
}
$(
        function() {
            $("#search_quiz_button").click(function() {
                $("#page").val(1);
                showAjxLoader();
                $.ajax({type: "POST", data: {
                        searchString: $("#search_quiz").serialize(), page: $("#page").val()}, url: "/get-quiz-list", success: function(response) {
                        if ($.
                                trim(response) == "") {
                            $("#quiz_records").html("No record found");
                            $(".top-section").hide()
                        } else {
                            $("#quiz_records").html(
                                    response);
                            $(".top-section").show()
                        }
                        hideAjxLoader()
                    }})
            });
            $("#main_category_id").change(function() {
                $(
                        "div[class^='main_category_id']").hide();
                if ($(".main_category_id" + this.value))
                    $(".main_category_id" + this.value).show()
            })
                    ;
            if ($(".main_category_id" + $("#main_category_id").val()))
                $(".main_category_id" + $("#main_category_id").val()).show();
            /* $("#date_sorting").click(function() {
                $("#page").val(1);
                if ($("#sort_field").val() == "tbl_qui_quizzes.added_date")
                    if ($(
                            "#sort_order").val() == "asc")
                        $("#sort_order").val("desc");
                    else
                        $("#sort_order").val("asc");
                else {
                    $("#sort_field").val(
                            "tbl_qui_quizzes.added_date");
                    $("#sort_order").val("desc")
                }
                showAjxLoader();
                $.ajax({type: "POST", data: {searchString: $(
                                "#search_quiz").serialize(), page: $("#page").val()}, url: "/get-quiz-list", success: function(response) {
                        $("#quiz_records").
                                html(response);
                        hideAjxLoader()
                    }})
            });*/
            $(document).on("click", ".begin_quiz", function() {
                var quiz_id = $(this).attr("id");
                showAjxLoader();
                $.ajax({type: "POST", data: {start_index: $("#start_index").val(), start_quiz: 1}, url: "/quiz-play/" + quiz_id,
                    success: function(response) {
                        $("#quizQuestionSection").html(response);
                        hideAjxLoader();
                        var start_index = parseInt($(
                                "#start_index").val());
                        $("#start_index").val(start_index + 1)
                    }})
            });
            $(".next-question").click(function() {
                var quiz_id = $(this
                        ).attr("id"), checked_option = $(".option_radio:checked").length;
                if (checked_option == 0) {
                    $("#error_message").html(
                            "Please select at least 1 answer to continue");
                    return false
                }
                showAjxLoader();
                $.ajax({type: "POST", data: {start_index: $(
                                "#start_index").val(), questionId: $("#questionId").val(), optionId: $(".option_radio:checked").val(), quizUserId: $(
                                "#quizUserId").val(), correct_option: $("#correct_option").val(), demographic_type: $("#demographic_type").val()}, url:
                            "/quiz-play/" + quiz_id, success: function(response) {
                                $("#quizQuestionSection").html(response);
                                hideAjxLoader();
                                var
                                        start_index = parseInt($("#start_index").val());
                                $("#start_index").val(start_index + 1)
                            }})
            });
            $(".show-result").click(function
                    () {
                var quiz_id = $(this).attr("id"), quizUserId = $("#quizUserIdEncrypt").val(), checked_option = $(".option_radio:checked").
                        length;
                if (checked_option == 0) {
                    $("#error_message").html("Please select at least 1 answer to continue");
                    return false
                }
                showAjxLoader();
                $.ajax({type: "POST", data: {start_index: $("#start_index").val(), questionId: $("#questionId").val(), optionId
                                : $(".option_radio:checked").val(), quizUserId: $("#quizUserId").val(), correct_option: $("#correct_option").val(),
                        demographic_type: $("#demographic_type").val()}, url: "/quiz-play/" + quiz_id, success: function(response) {
                        location.href =
                                "/quiz-result/" + quiz_id + "/" + quizUserId
                    }})
            });
            $(".start_demo").click(function() {
                var quiz_id = $(this).attr("id"), quizUserId =
                        $("#quizUserIdEncrypt").val(), checked_option = $(".option_radio:checked").length;
                if (checked_option == 0) {
                    $("#error_message")
                            .html("Please select at least 1 answer to continue");
                    return false
                }
                showAjxLoader();
                $.ajax({type: "POST", data: {start_index:
                                $("#start_index_demo").val(), questionId: $("#questionId").val(), optionId: $(".option_radio:checked").val(), quizUserId: $(
                                "#quizUserId").val(), correct_option: $("#correct_option").val(), start_demo: 1, demographic_type: $("#demographic_type").val(
                        )}, url: "/quiz-play/" + quiz_id, success: function(response) {
                        $("#quizQuestionSection").html(response);
                        hideAjxLoader();
                        var
                                start_index = parseInt($("#start_index_demo").val());
                        $("#start_index_demo").val(start_index + 1)
                    }})
            });
            $(
                    ".expired,.requirelogin,.requiremember,.requireloginmember,.notrequirelogin,.cannottake").colorbox({width: "700px", height
                        : "300px", inline: true});
            $("#close,#close1,#close2,#close3").click(function() {
                $.colorbox.close();
                return false
            });
            $(
                    "#login,#login1,#login2,#login3").click(function() {
                authenticateUserBeforeAjax()
            });
            $("#register,#register1,#register2").
                    click(function() {
                        signupPopup()
                    });
            $(".continue").click(function() {
                $.colorbox.close();
                $(".notrequirelogin").addClass(
                        "begin_quiz");
                $(".begin_quiz").removeClass("notrequirelogin");
                $(".begin_quiz").click()
            })
        });
function viewquiz(quizid) {
    location.href = "/quiz-info/" + quizid
}
function viewquizresult(quizid) {
    location.href = "/quiz-result/" + quizid
}
function openpop
        (url) {
    window.open("https://www.facebook.com/sharer/sharer.php?u=" + encodeURIComponent(url), "facebook-share-dialog",
            "width=626,height=436");
    return false
}
function sortQuiz(){
                $("#page").val(1);
                if ($("#sort_field").val() == "tbl_qui_quizzes.added_date" || $("#sort_field").val() == "is_featured"){
                        $("#sort_field").val("tbl_qui_quizzes.added_date");
                    if ($("#sort_order").val() == "asc" || $("#sort_order").val() == "")
                        $("#sort_order").val("desc");
                    else
                        $("#sort_order").val("asc");
                }
                else {
                    $("#sort_field").val("tbl_qui_quizzes.added_date");
                    $("#sort_order").val("desc")
                }
                showAjxLoader();
                $.ajax({type: "POST", data: {searchString: $(
                                "#search_quiz").serialize(), page: $("#page").val()}, url: "/get-quiz-list", success: function(response) {
                        $("#quiz_records").
                                html(response);
                        hideAjxLoader()
                    }})
    }