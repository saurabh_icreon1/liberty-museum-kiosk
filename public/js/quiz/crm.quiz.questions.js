$(function(){tinymce.init({mode:"specific_textareas",editor_selector:"myTextEditor",plugins:[
"link charmap print preview anchor","searchreplace visualblocks code fullscreen",
"insertdatetime media table contextmenu paste jbimages"],toolbar:
"insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link jbimages"
,height:300,menubar:false,width:"100%"});$(document).on("click","#add_another_question,#save_question,#saveandpublish",
function(){jQuery.validator.addMethod("alphanemeric",function(value,element){return this.optional(element)||
/^[a-zA-Z0-9 ,.-]+$/i.test(value)},"Please enter valid details");var buttonclick=$.trim($(this).attr("id"));$(
"#create_quiz_questions").validate({submitHandler:function(){showAjxLoader();$.ajax({type:"POST",data:$(
"#create_quiz_questions").serialize(),url:"/create-crm-quiz-questions",success:function(response){var jsonObj=jQuery.
parseJSON(response),redirecturl;if(buttonclick=="save_question"||buttonclick=="saveandpublish")redirecturl="/crm-quizes"
;if(buttonclick=="add_another_question")redirecturl="/create-crm-quiz-questions/"+$("#quiz_id_encrypted_hidden").val();
if(jsonObj.status=="success")location.href=redirecturl;else $.each(jsonObj.message,function(i,msg){$("#"+i).after(
'<label class="error" style="display:block;">'+msg+"</label>")});hideAjxLoader()}})},rules:{quiz_question:{required:true
},quiz_question_choice:{alphanemeric:true}},messages:{quiz_question:{required:PLEASE_ENTER_QUIZ_QUESTION}}});var status=
$("#create_quiz_questions").validate(),content=tinyMCE.get("quiz_question").getContent(),flag=true;if(content==""){$(
"#quiz_question_error").show();flag=false}else $("#quiz_question_error").hide();var length=$(
".option_selection_quiz:checked").length,length_choices=parseInt($("#count_div").text());if(length_choices>0){if(length
==0){$("#quiz_correct_option_edit").show();flag=false}}else{$("#quiz_question_choice_text").show();flag=false}if(flag==
false)return false})});var choicesValues=[];$(document).on("click","#add_choices",function(){var choice=$.trim($(
"#quiz_question_choice").val());if(choice!=""){var count_div=parseInt($("#count_div").text()),div_content=
'<div class="row1"><label>&nbsp;</label><div class="col11 action"><a href="javascript:void(0)" class="edit link eitd-icon-bt" id="edit_'
+count_div+
'"><div class="tooltip">Edit<span></span></div></a>&nbsp;&nbsp;<a class="delete link delete-icon-bt" href="javascript:void(0)"> <div class="tooltip">Delete<span></span></div></a> </div><div class="col7 rel-auto" id="choice_'
+count_div+'"><span id="choicetext_'+count_div+'" class="text-bold">'+choice+
'</span><input type="hidden" id="question_choice_'+count_div+'" name="question_choice[]" value="'+choice+
'" /> </div><div class="col11"><span class="text-bold"><input id="correct_choice_count_'+count_div+
'" name="correct_choices[]" type="radio" class="e3 correct_choice_ans option_selection_quiz" /><label class="redio" for="correct_choice_count_'
+count_div+'">&nbsp;</label></span></div></div>';$("#choice_div").append(div_content);$("#quiz_question_choice").val("")
;count_div=count_div+1;$("#count_div").text(count_div)}$("input").customInput()});$(document).on("click",".edit",
function(){var id=$(this).attr("id");$(this).html('<div class="tooltip">Save<span></span></div>');$(this).removeClass(
"edit");$(this).removeClass("eitd-icon-bt");$(this).addClass("savetext save-icon-bt");var id_count=id.replace("edit_",""
),textinbox=$("#choicetext_"+id_count).text();$("#choice_"+id_count).html(
'<input type="text" class="choicetextval" id="choice_text_'+id_count+'" style="width:95% !important" value="'+textinbox+
'" /><label id="choice_text_error_'+id_count+'" for="choice_text_'+id_count+
'"  style="display:none;top:auto !important" generated="true" class="custom_error choice_error"><span class="redarrow"></span>Please enter choice</label>'
)});$(document).on("click",".savetext",function(){var id=$(this).attr("id"),id_count=id.replace("edit_",""),textinbox=$.
trim($("#choice_text_"+id_count).val());if(textinbox!="")if(testAlphaNumeric(textinbox)){$("#choice_text_error_"+
id_count).hide();$(this).html('<div class="tooltip">Edit<span></span></div>');$(this).removeClass("savetext");$(this).
removeClass("save-icon-bt");$(this).addClass("edit eitd-icon-bt");$("#choice_"+id_count).html('<span id="choicetext_'+
id_count+'" class="text-bold">'+textinbox+'</span><input type="hidden" id="question_choice_'+id_count+
'" name="question_choice[]" value="'+textinbox+'" />')}else{$("#choice_text_error_"+id_count).html(
"Please enter valid details");$("#choice_text_error_"+id_count).show();return false}else{$("#choice_text_error_"+
id_count).show().html("Please enter choice value");return false}});$(document).on("focus",".choicetextval",function(){$(
this).next("label.error").hide()});$(document).on("focusout",".choicetextval",function(){if($(this).val()=="")$(this).
next("label.error").show()});$(document).on("click",".delete",function(){var count_div=parseInt($("#count_div").text());
$("#count_div").text(count_div-1);$(this).closest("div.row1").remove()});$(document).on("click",".correct_choice_ans",
function(){var id=$(this).attr("id"),selected_index=id.replace("correct_choice_count_","");$("#correct_index").val(
selected_index)});function showQuestionsGridQuestions(quizId){$("#quizQuestionsAnswers").load(
"/get-questions-grid-edit/"+quizId)}$(document).on("click",".delete_question",function(){var id=$(this).attr("id"),
selected_question=id.replace("question_","")});function deleteQuizQuestion(questionid){$(".delete_question").colorbox({
width:"700px",height:"200px",inline:true});$("#no_delete_quiz_question").click(function(){$.colorbox.close();$(
"#yes_delete_quiz_question").unbind("click");return false});$("#yes_delete_quiz_question").click(function(){$.colorbox.
close();$("#yes_delete_quiz_question").unbind("click");showAjxLoader();$.ajax({type:"POST",dataType:"json",data:{
questionid:questionid},url:"/delete-quiz-question",success:function(responseData){hideAjxLoader();var jsonObj=
responseData;if(jsonObj.status=="success"){var redirecturl="/create-crm-quiz-questions/"+$("#quiz_id_encrypted_hidden").
val();window.location.href=redirecturl}else $("#error_message").html(jsonObj.message)}})})}function 
show_choice_popup_edit(classShow){$("."+classShow).colorbox({width:"600px",height:"400px",inline:true})}function 
editQuizQuestion(quizId,questionid_encrypter,questionid){$("#add_another_question").hide();$("#save_question").hide();$(
"#saveandpublish").hide();$("#success_message_edit_question").hide();showAjxLoader();$(".onlyphtml").html("").hide();$(
".questiondisplay").show();$.ajax({url:"/create-quiz-edit-question/"+quizId+"/"+questionid_encrypter,success:function(
response){$("#question_"+questionid).hide();$("#question_div_"+questionid).html(response).show();hideAjxLoader()}})}
function testAlphaNumeric(value){return/^[a-zA-Z0-9 ,.-]+$/i.test(value)}