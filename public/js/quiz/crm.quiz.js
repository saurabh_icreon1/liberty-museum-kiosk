$(function(){$("#start_date").datepicker({changeMonth:true,changeYear:true,onClose:function(selectedDate){$("#end_date")
.datepicker("option","minDate",selectedDate)}});$("#expiration_date").datepicker({changeMonth:true,changeYear:true,
minDate:0});$("#main_category_id").change(function(){$("div[class^='main_category_id']").hide();if($(".main_category_id"
+this.value))$(".main_category_id"+this.value).show()});if($(".main_category_id"+$("#main_category_id").val()))$(
".main_category_id"+$("#main_category_id").val()).show();$("#end_date").datepicker({changeMonth:true,changeYear:true,
onClose:function(selectedDate){$("#start_date").datepicker("option","maxDate",selectedDate)}});$(
"#start_time,#expiration_time").timepicker({timeFormat:"h:i A"});$("input").customInput();var listQuizGrid=jQuery(
"#list_quizzes"),emptyMsgDiv=$('<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");$("#list_quizzes").jqGrid({mtype:
"POST",url:"/crm-quizes",datatype:"json",sortable:true,postData:{searchString:$("#search_quiz").serialize()},colNames:[
L_TITLE,CATEGORY,EXPIRATION_DATE,FEATURED,STATUS,ACTIONS],colModel:[{name:"quiz_title",index:"quiz_title"},{name:
"quiz_category_id",index:"quiz_category_id"},{name:"expiration_date",index:"expiration_date"},{name:"is_featured",index:
"is_featured"},{name:"is_published",index:"is_published"},{name:"Action",sortable:false,cmTemplate:{title:false},classes
:"none"}],viewrecords:true,sortname:"tbl_qui_quizzes.modified_date",sortorder:"desc",rowNum:10,rowList:[10,20,30],pager:
"#listquizzerud",viewrecords:true,autowidth:true,shrinkToFit:true,caption:"",width:"100%",cmTemplate:{title:false},
loadComplete:function(){hideAjxLoader();var count=listQuizGrid.getGridParam(),ts=listQuizGrid[0];if(ts.p.reccount===0){
listQuizGrid.hide();emptyMsgDiv.show();$("#listquizzerud_right div.ui-paging-info").css("display","none")}else{
listQuizGrid.show();emptyMsgDiv.hide();$("#listquizzerud_right div.ui-paging-info").css("display","block")}}});
emptyMsgDiv.insertAfter(listQuizGrid.parent());emptyMsgDiv.hide();$("#list_quizzes").jqGrid("navGrid","#listquizzerud",{
reload:true,edit:false,add:false,search:false,del:false});$("#searchbuttonquiz").click(function(){showAjxLoader();jQuery
("#list_quizzes").jqGrid("setGridParam",{postData:{searchString:$("#search_quiz").serialize()}});jQuery("#list_quizzes")
.trigger("reloadGrid",[{page:1}]);window.location.hash="#save_search";return false});$("#savebutton,#saveandaddquestion"
).click(function(){jQuery.validator.addMethod("alphanemeric",function(value,element){return this.optional(element)||
/^[a-zA-Z0-9 "'?!© ®℠℗™,.-]+$/i.test(value)},MESSAGE_ALPHANUMERIC);jQuery.validator.addMethod("alphanemericspecial",
function(value,element){return this.optional(element)||/^[a-zA-Z0-9 "'?!© ®℠℗™,.-]+$/i.test(value)},MESSAGE_ALPHANUMERIC
);var buttonclick=$.trim($(this).val());$("#create_quiz").validate({submitHandler:function(){showAjxLoader();$.ajax({
type:"POST",data:$("#create_quiz").serialize(),url:"/create-crm-quiz",success:function(response){var jsonObj=jQuery.
parseJSON(response),redirecturl;if(buttonclick=="Save")redirecturl="/crm-quizes";else redirecturl=
"/create-crm-quiz-questions/"+jsonObj.quiz_id;if(jsonObj.status=="success")location.href=redirecturl;else $.each(jsonObj
.message,function(i,msg){$("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")});hideAjxLoader()
}})},rules:{quiz_title:{required:true,alphanemeric:true},quiz_tagline:{required:true,alphanemeric:true},main_category_id
:{required:true},expiration_date:{required:true},expiration_time:{required:true},brief_description:{alphanemericspecial:
true},full_description:{alphanemericspecial:true},avail_member:{required:function(elem){var length=$(
".availability:checked").length;if(length==0)return true;else return false}}},messages:{quiz_title:{required:TITLE_MSG},
quiz_tagline:{required:QUIZ_TAGLINE_MSG},main_category_id:{required:QUIZ_CATEGORY_MSG},expiration_date:{required:
QUIZ_EXPIRATION_DATE_MSG},expiration_time:{required:QUIZ_EXPIRATION_TIME_MSG},avail_member:{required:AVAILABILITY_MSG},
avail_member_demo:{required:AVAILABILITY_MSG_DEMO}}})});var progressbarid="progress";$(".attachmentfiles").focus(
function(){var file_id=$(this).attr("id"),file_number=file_id.substr(file_id.length-1,1);progressbarid="progress_"+
file_number});var url="/upload-quiz-image";$(".attachmentfiles").fileupload({url:url,dataType:"json",done:function(e,
data){var file_id=$(this).attr("id"),file_number=file_id.substr(file_id.length-1,1);data=data.result;if(data.status==
"success"){$("#main_image_msg").addClass("success-msg");$("#main_image_msg").removeClass("error-msg");$(
"#main_image_hidden").val(data.filename)}else{$("#main_image_msg").removeClass("success-msg");$("#main_image_msg").
addClass("error-msg");$("#main_image_hidden").val("")}$("#main_image_msg").html(data.message)},progressall:function(e,
data){},start:function(e){showAjxLoader()},stop:function(e){hideAjxLoader()}})});function checkvail(){}function 
deleteQuiz(quizid){$(".delete_quiz").colorbox({width:"700px",height:"200px",inline:true});$("#no_delete_quiz").click(
function(){$.colorbox.close();$("#yes_delete_quiz").unbind("click");return false});$("#yes_delete_quiz").click(function(
){$.colorbox.close();$("#yes_delete_quiz").unbind("click");showAjxLoader();$.ajax({type:"POST",dataType:"html",data:{
quizid:quizid},url:"/delete-quiz",success:function(responseData){if(!checkUserAuthenticationAjx(responseData))
return false;hideAjxLoader();var jsonObj=JSON.parse(responseData);if(jsonObj.status=="success")window.location.href=
"/crm-quizes";else $("#error_message").html(jsonObj.message)}})})}function showDate(id,divId){if($("#"+id+
" option:selected").text()=="Choose Date Range")$("#"+divId).show();else $("#"+divId).hide()}function focusTextBox(
text_box_id){$("#"+text_box_id).focus()}