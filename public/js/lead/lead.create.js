$(function(){
    jQuery.validator.addMethod("alphanumericspecialchar",function(value,element){
        return this.optional(element)||/^[A-Z a-z 0-9\r\s\n ,.-]+$/i.test(value)
        },ALPHA_NUMERIC_SPECIALSYMPOL);
    jQuery.validator.addMethod("alphabetcoma",function(value,element){
        return this.optional(element)||/^[a-z, ]+$/i.test(value)
        },ALPHA_NUMERIC_SPECIALSYMPOL);
    $("#lead_type").change(function(){
        $("div[class^='lead_type']").hide();
        if($(".lead_type"+this.value))$(".lead_type"+this.value).show()
            });
    if($(".lead_type"+$("#lead_type").val()))$(".lead_type"+
        $("#lead_type").val()).show();
    $("#lead_source").change(function(){
        $("div[class^='lead_source']").hide();
        if($(".lead_source"+this.value))$(".lead_source"+this.value).show()
            });
    if($(".lead_source"+$("#lead_source").val()))$(".lead_source"+$("#lead_source").val()).show();
    $("#continue").click(function(){
        $(".detail-section").css("display","block")
        });
    $("#create-lead").validate({
        submitHandler:function(){
            var dataStr=$("#create-lead").serialize();
            if(checkCustomValidation()){
                showAjxLoader();
                $.ajax({
                    type:"POST",
                    url:"/create-lead",
                    data:dataStr,
                    success:function(responseData){
                        var jsonObj=jQuery.parseJSON(responseData);
                        if(jsonObj.status=="success")window.location.href="/leads";else $.each(jsonObj.message,function(i,msg){
                            $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                            })
                        }
                        })
            }else $(".detail-section").css("display","block")
            },
    rules:{
        lead_type:{
            required:true
        },
        lead_source:{
            required:true
        },
        assigned_to:{
            required:true
        },
        contact_name:{
            required:true
        },
        opportunity_amount:{
            number:true
        },
        notes:{
            maxlength:1E3
        }
    },
    messages:{
        lead_type:{
            required:LEAD_TYPE_EMPTY
        },
        lead_source:{
            required:LEAD_SOURCE_EMPTY
        },
        assigned_to:{
            required:ASSIGN_TO_EMPTY
        },
        contact_name:{
            required:CONTACT_NAME_EMPTY
        }
    },
highlight:function(label){
    $(".detail-section").css("display","block")
    }
});
$("#new_phoneno_div").click(function(){
    var i=$("#count_div").text();
    $("#primary_Div").show();
    $("<div />",{
        "class":"row container"+i
        }).appendTo("#phoneno_div");
    $(".container"+i).load("/lead-phone-no/"+i+"/container");
    i++;
    $("#count_div").html(i)
    });
$("#new_address_div").click(function(){
    var i=
    $("#count_address_div").text();
    $("<div />",{
        "class":"addressAll row no-padd address_container"+i
        }).appendTo("#load_address_section");
    $(".address_container"+i).load("/lead-address-form/"+i+"/address_container");
    i++;
    $("#count_address_div").html(i)
    });
$("#addressinfo_search_contact").autocomplete({
    afterAdd:true,
    selectFirst:true,
    autoFocus:true,
    source:function(request,response){
        $.ajax({
            url:"/get-contacts",
            method:"POST",
            dataType:"json",
            data:{
                user_email:$("#addressinfo_search_contact").val()
                },
            success:function(jsonResult){
                var resultset=
                [];
                $.each(jsonResult,function(){
                    var id=this.user_id;
                    var value=this.full_name;
                    var address_one=this.street_address_one;
                    var address_two=this.street_address_two;
                    var city=this.city;
                    var state=this.state;
                    var zip_code=this.zip_code;
                    var country_id=this.country_id;
                    var country_name=this.country_name;
                    resultset.push({
                        id:this.user_id,
                        value:this.full_name,
                        address_one:this.street_address_one,
                        address_two:this.street_address_two,
                        city:this.city,
                        state:this.state,
                        zip_code:this.zip_code,
                        country_id:this.country_id,
                        country_name:this.country_name
                        })
                    });
                response(resultset)
                }
            })
    },
open:function(){
    $(".ui-menu").width(350)
    },
change:function(event,ui){
    if(ui.item){
        $("#addressRow0  input#addressinfo_search_contact").val("");
        $("#addressRow0 input.addressinfo_street_address_1").val(ui.item.address_one);
        $("#addressRow0 input.addressinfo_street_address_2").val(ui.item.address_two);
        $("#addressRow0 input.addressinfo_city").val(ui.item.city);
        $("#addressRow0 input.addressinfo_state").val(ui.item.state);
        $("#addressRow0 input.addressinfo_zip").val(ui.item.zip_code);
        $("#addressRow0 input.addressinfo_country").val(ui.item.country_id).select2();
        $("#load_contact_section_0").hide();
        $("#addressRow0").show()
        }else $("#addressinfo_search_contact").val("")
        },
focus:function(event,ui){
    return false
    },
select:function(event,ui){
    $("#addressRow0  input#addressinfo_search_contact").val("");
    $("#addressRow0 input.addressinfo_street_address_1").val(ui.item.address_one);
    $("#addressRow0 input.addressinfo_street_address_2").val(ui.item.address_two);
    $("#addressRow0 input.addressinfo_city").val(ui.item.city);
    $("#addressRow0 input.addressinfo_state").val(ui.item.state);
    $("#addressRow0 input.addressinfo_zip").val(ui.item.zip_code);
    $("#addressRow0 input.addressinfo_country").val(ui.item.country_id).select2();
    $("#load_contact_section_0").hide();
    $("#addressRow0").show()
    }
});
$("#contact_name").autocomplete({
    afterAdd:true,
    selectFirst:true,
    autoFocus:true,
    source:function(request,response){
        $.ajax({
            url:"/get-contacts",
            method:"POST",
            dataType:"json",
            data:{
                user_email:$("#contact_name").val()
                },
            success:function(jsonResult){
                var resultset=
                [];
                $.each(jsonResult,function(){
                    var id=this.user_id;
                    var value;
                    if(this.email_id!="")value=this.full_name+"("+this.email_id+")";else value=this.full_name;
                    var value1=this.full_name;
                    resultset.push({
                        id:this.user_id,
                        value:value,
                        value1:value1
                    })
                    });
                response(resultset)
                }
            })
    },
open:function(){
    $(".ui-menu").width(350)
    },
change:function(event,ui){
    if(ui.item){
        $("#contact_name").val(ui.item.value1);
        $("#contact_name_id").val(ui.item.id)
        }else{
        $("#contact_name").val("");
        $("#contact_name_id").val("")
        }
    },
focus:function(event,
    ui){
    $("#contact_name_id").val(ui.item.id);
    return false
    },
select:function(event,ui){
    $("#contact_name").val(ui.item.value1);
    $("#contact_name_id").val(ui.item.id);
    $.ajax({
        url:"/user-contact-info",
        method:"POST",
        dataType:"json",
        data:{
            user_id:ui.item.id
            },
        success:function(jsonResult){
            if(jsonResult.user_type==2||jsonResult.user_type==3){
                $("#title").parent().hide();
                $("#first_name").parent().parent().hide();
                $("#last_name").parent().parent().hide()
                }else{
                $("#title").parent().show();
                $("#first_name").parent().parent().show();
                $("#last_name").parent().parent().show();
                $("#title").val(jsonResult.title);
                $("#first_name").val(jsonResult.first_name);
                $("#middle_name").val(jsonResult.middle_name);
                $("#last_name").val(jsonResult.last_name);
                $("#suffix").val(jsonResult.suffix)
                }
                $("#email_address").val(jsonResult.email_id);
            var resultset=new Array;
            if(jsonResult["phoneArr"].length==0)resultset.push({
                norecord:true
            });else $.each(jsonResult["phoneArr"],function(){
                resultset.push({
                    phone_type:this.type,
                    country_code:this.country_code,
                    area_code:this.area_code,
                    phone_no:this.phone_number,
                    extension:this.extension,
                    is_primary:this.is_primary
                    })
                });
            var i=$("#count_div").text();
            if(jsonResult["phoneArr"].length>0)$("#phoneno_div").load("/lead-phone-no/"+i+"/container",{
                "phoneArr":resultset
            });
            else{
                $("#phone_type").val("");
                $("#country_code").val("");
                $("#area_code").val("");
                $("#phone_no").val("");
                $("#extension").val("");
                $("#is_primary").val("")
                }
                var resultAddset=new Array;
            $.each(jsonResult["addressArr"],function(){
                resultAddset.push({
                    location_type:this.location_type,
                    country_id:this.country_id,
                    location_name:this.location_name,
                    country_name:this.country_name,
                    street_address_one:this.street_address_one,
                    street_address_two:this.street_address_two,
                    street_address_three:this.street_address_three,
                    city:this.city,
                    state:this.state,
                    zip_code:this.zip_code
                    })
                });
            var j=$("#count_address_div").text();
            if(jsonResult["addressArr"].length>0)$("#load_address_section").load("/lead-address-form/"+j+"/address_container",{
                "addressArr":resultAddset
            });
            else{
                $("#addressinfo_location_type").val("");
                $("#country_id").val("");
                $("#addressinfo_location").val("");
                $("#addressinfo_country").val("");
                $("#addressinfo_street_address_1").val("");
                $("#addressinfo_city").val("");
                $("#addressinfo_state").val("");
                $("#addressinfo_zip").val("")
                }
            }
    });
return false
}
});
$("#assigned_to").autocomplete({
    afterAdd:true,
    selectFirst:true,
    autoFocus:true,
    source:function(request,response){
        $.ajax({
            url:"/get-crm-contacts",
            method:"POST",
            dataType:"json",
            data:{
                crm_full_name:$("#assigned_to").val()
                },
            success:function(jsonResult){
                var resultset=
                [];
                $.each(jsonResult,function(){
                    var id=this.crm_user_id;
                    var value;
                    if(this.crm_email_id!=""&&this.crm_email_id!=null&&this.crm_email_id!="undefined")value=this.crm_full_name+"("+this.crm_email_id+")";else value=this.crm_full_name;
                    var value1=this.crm_full_name;
                    resultset.push({
                        id:this.crm_user_id,
                        value:value,
                        value1:value1
                    })
                    });
                response(resultset)
                }
            })
    },
open:function(){
    $(".ui-menu").width(350)
    },
change:function(event,ui){
    if(ui.item){
        $("#assigned_to").val(ui.item.value1);
        $("#assigned_to_id").val(ui.item.id)
        }else{
        $("#assigned_to").val("");
        $("#assigned_to_id").val("")
        }
    },
focus:function(event,ui){
    $("#assigned_to_id").val(ui.item.id);
    return false
    },
select:function(event,ui){
    $("#assigned_to").val(ui.item.value1);
    $("#assigned_to_id").val(ui.item.id);
    return false
    }
});
$("#campaign").autocomplete({
    afterAdd:true,
    selectFirst:true,
    autoFocus:true,
    source:function(request,response){
        $.ajax({
            url:"/get-campaigns",
            method:"POST",
            dataType:"json",
            data:{
                campaign_name:$("#campaign").val()
                },
            success:function(jsonResult){
                var resultset=[];
                $.each(jsonResult,function(){
                    var id=
                    this.campaign_id;
                    var value=this.campaign_title;
                    resultset.push({
                        id:this.campaign_id,
                        value:this.campaign_title
                        })
                    });
                response(resultset)
                }
            })
    },
open:function(){
    $(".ui-menu").width(350)
    },
change:function(event,ui){
    if(ui.item){
        $("#campaign").val(ui.item.value);
        $("#campaign_id").val(ui.item.id)
        }else{
        $("#campaign").val("");
        $("#campaign_id").val("")
        }
        $(".ui-autocomplete").jScrollPane()
    },
focus:function(event,ui){
    $("#campaign_id").val(ui.item.id);
    return false
    },
select:function(event,ui){
    $("#campaign").val(ui.item.value);
    $("#campaign_id").val(ui.item.id);
    return false
    }
})
});
$(document).ready(function(){
    if(contact_id)convertContactToLead(contact_id);
    $("#cancel").click(function(){
        window.location.href="/leads"
        })
    });
function convertContactToLead(contact_id){
    $.ajax({
        url:"/user-contact-info",
        method:"POST",
        dataType:"json",
        data:{
            user_id:contact_id
        },
        success:function(jsonResult){
            if(jsonResult.user_type==1){
                var name="";
                if(jsonResult.first_name!=""&&jsonResult.first_name!="undefined"&&jsonResult.first_name!=null&&jsonResult.first_name!=null)name+=jsonResult.first_name+" ";
                if(jsonResult.middle_name!=""&&jsonResult.middle_name!="undefined"&&jsonResult.middle_name!=null&&jsonResult.first_name!=null)name+=jsonResult.middle_name+
                    " ";
                if(jsonResult.last_name!=""&&jsonResult.last_name!="undefined"&&jsonResult.first_name!=null)name+=jsonResult.last_name;
                $("#contact_name").val(name);
                $("#first_name").val(jsonResult.first_name);
                $("#middle_name").val(jsonResult.middle_name);
                $("#last_name").val(jsonResult.last_name);
                $("#suffix").val(jsonResult.suffix);
                $("#title").val(jsonResult.title)
                }else if(jsonResult.user_type==2||jsonResult.user_type==3){
                $("#contact_name").val(jsonResult.company_name);
                $("#title").parent().hide();
                $("#first_name").parent().parent().hide();
                $("#last_name").parent().parent().hide()
                }
                $("#email_address").val(jsonResult.email_id);
            var resultset=new Array;
            $.each(jsonResult["phoneArr"],function(){
                resultset.push({
                    phone_type:this.type,
                    country_code:this.country_code,
                    area_code:this.area_code,
                    phone_no:this.phone_number,
                    extension:this.extension,
                    is_primary:this.is_primary
                    })
                });
            var i=$("#count_div").text();
            if(jsonResult["phoneArr"].length>0)$("#phoneno_div").load("/lead-phone-no/"+i+"/container",{
                "phoneArr":resultset
            });
            var resultAddset=new Array;
            $.each(jsonResult["addressArr"],
                function(){
                    resultAddset.push({
                        location_type:this.location_type,
                        country_id:this.country_id,
                        location_name:this.location_name,
                        country_name:this.country_name,
                        street_address_one:this.street_address_one,
                        street_address_two:this.street_address_two,
                        street_address_three:this.street_address_three,
                        city:this.city,
                        state:this.state,
                        zip_code:this.zip_code
                        })
                    });
            var j=$("#count_address_div").text();
            if(jsonResult["addressArr"].length>0)$("#load_address_section").load("/lead-address-form/"+j+"/address_container",{
                "addressArr":resultAddset
            })
            }
            })
}
$(document).on("click","a#new_phoneno_div_",function(){
    $("#primary_Div").show();
    var i=$("#count_div").text();
    $("<div />",{
        "class":"row container"+i
        }).appendTo("#phoneno_div");
    $(".container"+i).load("/lead-phone-no/"+i+"/container");
    i++;
    $("#count_div").html(i)
    });
function remove_dynamic_contacts(classname,i){
    $(classname).remove();
    $("#count_div").html(i);
    $("#count_address_div").html(i)
    }
function remove_dynamic_contacts2(classname,i){
    $(classname+i).remove();
    $("#count_div").html(i);
    var selectClass=i;
    var selectedPrimary=$("#phone_index").val();
    if(selectedPrimary==selectClass){
        $('#create-lead [name="continfo_primary[]"]').prop("checked",false);
        $('#create-lead [name="continfo_primary[]"]').each(function(index){
            if($("#undefined_"+index).next().attr("class")=="checked"&&index==i)$("#undefined_"+index).next().removeClass("checked");
            $("#undefined_0").next().addClass("checked");
            if(index==
                0)$("#undefined_0").prop("checked",true)
                })
        }else if(selectedPrimary>selectClass){
        var assignPrimary=parseInt(selectedPrimary)-parseInt(1);
        $("#phone_index").val(assignPrimary)
        }else if(selectClass==index){
        $("#undefined_0").prop("checked",true);
        $("#phone_index").val("0")
        }
        $('#create-lead [name="continfo_primary[]"]').prop("checked",false);
    $("#phone_index").val("0")
    }
function loadIndividualContact(eleId,val){
    if($("#"+eleId).is(":checked")){
        $("#addressRow"+val).hide();
        $("#load_contact_section_"+val).show();
        commonAutoComplete(val)
        }else{
        $("#addressRow"+val).show();
        $("#load_contact_section_"+val).hide()
        }
    }
function commonAutoComplete(val){
    $("#address_search_contact"+val).autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:function(request,response){
            $.ajax({
                url:"/get-contacts",
                method:"POST",
                dataType:"json",
                data:{
                    user_email:$("#address_search_contact"+val).val()
                    },
                success:function(jsonResult){
                    var resultset=[];
                    $.each(jsonResult,function(){
                        var id=this.user_id;
                        var value=this.full_name;
                        var address_one=this.street_address_one;
                        var address_two=this.street_address_two;
                        var city=this.city;
                        var state=
                        this.state;
                        var zip_code=this.zip_code;
                        var country_id=this.country_id;
                        var country_name=this.country_name;
                        resultset.push({
                            id:this.user_id,
                            value:this.full_name,
                            address_one:this.street_address_one,
                            address_two:this.street_address_two,
                            city:this.city,
                            state:this.state,
                            zip_code:this.zip_code,
                            country_id:this.country_id,
                            country_name:this.country_name
                            })
                        });
                    response(resultset)
                    }
                })
        },
    open:function(){
        $(".ui-menu").width(350)
        },
    change:function(event,ui){
        if(ui.item){
            $("#addressRow"+val+"  input#addressinfo_search_contact").val("");
            $("#addressRow"+val+" input.addressinfo_street_address_1").val(ui.item.address_one);
            $("#addressRow"+val+" input.addressinfo_street_address_2").val(ui.item.address_two);
            $("#addressRow"+val+" input.addressinfo_city").val(ui.item.city);
            $("#addressRow"+val+" input.addressinfo_state").val(ui.item.state);
            $("#addressRow"+val+" input.addressinfo_zip").val(ui.item.zip_code);
            $("#addressRow"+val+" input.addressinfo_country").val(ui.item.country_id).select2();
            $("#load_contact_section_"+val+"").hide();
            $("#addressRow"+
                val+"").show()
            }else $("#addressinfo_search_contact").val("")
            },
    focus:function(event,ui){
        return false
        },
    select:function(event,ui){
        $("#addressRow"+val+"  input#addressinfo_search_contact").val("");
        $("#addressRow"+val+" input.addressinfo_street_address_1").val(ui.item.address_one);
        $("#addressRow"+val+" input.addressinfo_street_address_2").val(ui.item.address_two);
        $("#addressRow"+val+" input.addressinfo_city").val(ui.item.city);
        $("#addressRow"+val+" input.addressinfo_state").val(ui.item.state);
        $("#addressRow"+
            val+" input.addressinfo_zip").val(ui.item.zip_code);
        $("#addressRow"+val+" input.addressinfo_country").val(ui.item.country_id).select2();
        $("#load_contact_section_"+val+"").hide();
        $("#addressRow"+val+"").show()
        }
    })
}
function changeLeadSource(val){
    if(val==3){
        $("#compaign_div").show();
        $("#compaign").val($("#compaign_id").val())
        }else $("#compaign_div").hide();
    $("#campaign").autocomplete({
        afterAdd:true,
        selectFirst:true,
        autoFocus:true,
        source:function(request,response){
            $.ajax({
                url:"/get-campaigns",
                method:"POST",
                dataType:"json",
                data:{
                    campaign_name:$("#campaign").val()
                    },
                success:function(jsonResult){
                    var resultset=[];
                    $.each(jsonResult,function(){
                        var id=this.campaign_id;
                        var value=this.campaign_title;
                        resultset.push({
                            id:this.campaign_id,
                            value:this.campaign_title
                            })
                        });
                    response(resultset)
                    }
                })
        },
    open:function(){
        $(".ui-menu").width(350)
        },
    change:function(event,ui){
        if(ui.item){
            $("#campaign").val(ui.item.value);
            $("#campaign_id").val(ui.item.id)
            }else{
            $("#campaign").val("");
            $("#campaign_id").val("")
            }
            $(".ui-autocomplete").jScrollPane()
        },
    focus:function(event,ui){
        $("#campaign_id").val(ui.item.id);
        return false
        },
    select:function(event,ui){
        $("#campaign").val(ui.item.value);
        $("#campaign_id").val(ui.item.id);
        return false
        }
    })
}
function checkCustomValidation(){
    var flag=false;
    var flagArea=false;
    var flagCode=false;
    var flagPhone=false;
    var flagAdd1=false;
    var flagAdd2=false;
    var flagCity=false;
    var flagZip=false;
    var campaignFlag=false;
    if($("#lead_source").val()==3&&$("#campaign_id").val()==""){
        campaignFlag=true;
        if($("#campaign").next("span").length==0)$("#campaign").after('<span for="campaign" class="error" style="display:block !important;"><span class="redarrow"></span>'+CAMPAIGN_ERROR+"</span>")
            }
            $('#create-lead [name="country_code[]"]').each(function(index){
        var str=
        $(this).val();
        $(this).next().remove("span");
        if($.isNumeric(str)||str==""){
            flag=false;
            if(str.length>5&&str!=""){
                if($(this).next("span").length==0)$(this).after('<span for="country_code[]" class="error" generated="true" style="display:block !important;"><span class="redarrow"></span>'+COUNTRY_NO_MAX+"</span>");
                flag=true
                }else{
                $(this).next().remove("span");
                flag=false
                }
                flagCode=false
            }else{
            if($(this).next("span").length==0)$(this).after('<span for="country_code[]" class="error" generated="true" style="display:block !important;"><span class="redarrow"></span>'+
                COUNTRY_NO_VALID+"</span>");
            flagCode=true
            }
        });
$('#create-lead [name="area_code[]"]').each(function(index){
    var str=$(this).val();
    $(this).next().remove("span");
    if($.isNumeric(str)||str==""){
        var flag=false;
        if(str.length>5&&str!=""){
            if($(this).next("span").length==0)$(this).after('<span for="area_code[]" class="error" generated="true" style="display:block !important;"><span class="redarrow"></span>'+AREA_NO_MAX+"</span>");
            flag=true
            }else{
            $(this).next().remove("span");
            flag=false
            }
            flagArea=false
        }else{
        if($(this).next("span").length==
            0)$(this).after('<span for="area_code[]" class="error"  style="display:block !important;"><span class="redarrow"></span>'+AREA_NO_VALID+"</span>");
        flagArea=true
        }
    });
$('#create-lead [name="phone_no[]"]').each(function(index){
    $(this).next().remove("span");
    var str=$(this).val();
    if($.isNumeric(str)||str==""){
        var flag=false;
        if(str.length>10&&str!=""){
            if($(this).next("span").length==0)$(this).after('<span for="phone_no[]" class="error" style="display:block !important;"><span class="redarrow"></span>'+PHONE_NO_MAX+
                "</span>");
            flag=true
            }else{
            $(this).next().remove("span");
            flag=false
            }
            flagPhone=false
        }else{
        if($(this).next("span").length==0)$(this).after('<span for="phone_no[]" class="error" style="display:block !important;"><span class="redarrow"></span>'+PHONE_NO_VALID+"</span>");
        flagPhone=true
        }
    });
$('#create-lead [name="addressinfo_street_address_1[]"]').each(function(index){
    var str=$(this).val();
    var regx=/^[A-Za-z0-9 ,.-]+$/i;
    $(this).next().remove("span");
    if(str.match(regx)){
        if(str.length>30&&str!=""){
            if($(this).next("span").length==
                0)$(this).after('<span for="addressinfo_street_address_1[]" class="error" style="display:block !important;"><span class="redarrow"></span>'+ADDRESS_MAX+"</span>");
            flag=true
            }else{
            $(this).next().remove("span");
            flag=false
            }
            flagAdd1=false
        }else if(str!=""){
        if($(this).next("span").length==0)$(this).after('<span class="error" for="addressinfo_street_address_1[]" generated="true" style="display:block !important;"><span class="redarrow"></span>'+VALID_ADDRESS+"</span>");
        flagAdd1=true
        }
    });
$('#create-lead [name="addressinfo_street_address_2[]"]').each(function(index){
    var str=
    $(this).val();
    var regx=/^[A-Za-z0-9 ,.-]+$/i;
    $(this).next().remove("span");
    if(str.match(regx)){
        var flag=false;
        if(str.length>30&&str!=""){
            if($(this).next("span").length==0)$(this).after('<span for="addressinfo_street_address_2[]" class="error" style="display:block !important;"><span class="redarrow"></span>'+ADDRESS_MAX+"</span>");
            flag=true
            }else{
            $(this).next().remove("span");
            flag=false
            }
            flagAdd2=false
        }else if(str!=""){
        if($(this).next("span").length==0)$(this).after('<span for="addressinfo_street_address_2[]" class="error" style="display:block !important;"><span class="redarrow"></span>'+
            VALID_ADDRESS+"</span>");
        flagAdd2=true
        }
    });
$('#create-lead [name="addressinfo_city[]"]').each(function(index){
    var str=$(this).val();
    var regx=/^[a-z ]+$/i;
    $(this).next().remove("span");
    if(str.match(regx)||str.length==0){
        var flag=false;
        if(str.length<2&&str!=""){
            if($(this).next("span").length==0)$(this).after('<span for="addressinfo_city[]" class="error" style="display:block !important;">'+CITY_MIN+"</span>");
            flag=true
            }else if(str.length>30&&str!=""){
            if($(this).next("span").length==0)$(this).after('<span for="addressinfo_city[]" class="error" style="display:block !important;">'+
                CITY_MAX+"</span>");
            flag=true
            }else{
            $(this).next().remove("span");
            flag=false
            }
            flagCity=flag
        }else{
        if($(this).next("span").length==0)$(this).after('<span for="addressinfo_city[]" class="error" style="display:block !important;">'+VALID_CITY+"</span>");
        flagCity=true
        }
    });
$('#create-lead [name="addressinfo_zip[]"]').each(function(index){
    var str=$(this).val();
    var regx=/^[0-9]+$/i;
    $(this).next().remove("span");
    if(str.match(regx)||str.length==0){
        var flag=false;
        if(str.length>8&&str!=""){
            if($(this).next("span").length==
                0)$(this).after('<span for="addressinfo_zip[]" class="error" style="display:block !important;"><span class="redarrow"></span>'+ZIP_MAX+"</span>");
            flag=true
            }else{
            $(this).next().remove("span");
            flag=false
            }
            flagZip=false
        }else{
        if($(this).next("span").length==0)$(this).after('<span for="addressinfo_zip[]" class="error" style="display:block !important;"><span class="redarrow"></span>'+VALID_ZIP+"</span>");
        flagZip=true
        }
    });
if(flagCode||flagArea||flagPhone||flagAdd1||flagAdd2||flagZip||flagCity||flag||campaignFlag){
    $(".detail-section").css("display",
        "block");
    return false
    }else return true
    }
function addressLocationType(eleId,val,counter){
    var preViousVal=$("#hidden_address_type"+counter).val();
    var finder=preViousVal.search(val);
    var finder2=preViousVal.search(val+",");
    if($("#"+eleId).is(":checked")){
        if(finder<0)if(preViousVal.length>0)preViousVal=preViousVal+","+val;else preViousVal=val
            }else if(finder2>=0)if(preViousVal.length==1)preViousVal=preViousVal.replace(val,"");else preViousVal=preViousVal.replace(val+",","");
    else if(finder>-1)preViousVal=preViousVal.replace(val,"");
    $("#hidden_address_type"+
        counter).val(preViousVal)
    }
    function isPrimaryCheck(index){
    $("#phone_index").val(index)
    }
$($(".address-checkbox")).each(function(i){
    if($(this).is("[type=checkbox],[type=radio]")){
        var input=$(this);
        var input_class=input.attr("class").split(" ");
        var id_name="";
        console.log(input.attr("id"));
        if(input.attr("id")=="undefined"||input.attr("id")==""||input.attr("id")==undefined||input.attr("id")==null){
            console.log("in");
            id_name=input_class["1"]+"_"+i
            }else{
            console.log("out");
            id_name=input.attr("id")
            }
            input.attr({
            "id":id_name
        });
        var label=$("label[for="+input.attr("id")+"]");
        var outerLabel=input.parent();
        var outerLabelParent=input.parent().parent();
        var inputType=input.is("[type=checkbox]")?"checkbox":"radio";
        var oDiv=$(document.createElement("label")).attr({
            "for":id_name
        }).html($(outerLabel).text());
        $(outerLabel).remove();
        outerLabelParent.append(input,oDiv)
        }
    });
$(".address-checkbox").customInput();
