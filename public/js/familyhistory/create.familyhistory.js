/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$(function() {


    jQuery.validator.addMethod("youtubevideo", function(value, element) {
        return value.match(/watch\?v=([a-zA-Z0-9\-_]+)/) || value.match(/^http:\/\/(www\.)?vimeo\.com\/.*$/);
    }, VIDEO_EMPTY);
    
      jQuery.validator.addMethod("checkEditor", function(value, element) {
       if(tinyMCE.get('story').getContent()=="")
       { return false;}
    }, "EMpty");
    
    $("#country_of_birth_manual").autocomplete({
        afterAdd: true,
        selectFirst: true,
        autoFocus: true,
        source: function(request, response) {
            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
            var resultset = [];
            $.each(country, function() {
                var id = this.id;
                var value = this.value;
                if (this.value && (matcher.test(value)))
                {
                    resultset.push({
                        id: this.id,
                        value: this.value

                    });
                }
                return;
            });
            response(resultset);
        },
        open: function() {
            $('.ui-menu').width(350);
        },
        change: function(event, ui) {
            if (ui.item)
            {
                $("#country_of_birth_manual").val(ui.item.value);
                $("#country_of_birth_manual_id").val(ui.item.id);
            }
            else {
                $("#country_of_birth_manual").val('');
                $("#country_of_birth_manual_id").val('');
            }
        },
        focus: function(event, ui) {
            $("#country_of_birth_manual_id").val(ui.item.id);
            return false;
        },
        select: function(event, ui) {
            $("#country_of_birth_manual").val(ui.item.value);
            $("#country_of_birth_manual_id").val(ui.item.id);
            return false;
        }
    });
    $("#contact_name").autocomplete({
        afterAdd: true,
        selectFirst: true,
        autoFocus: true,
        source: function(request, response) {
            $.ajax({
                url: "/get-contacts",
                method: 'POST',
                dataType: "json",
                data: {
                    //maxRows: 12,
                    user_email: $("#contact_name").val()
                },
                success: function(jsonResult) {
                    if (!checkUserAuthenticationAjx(jsonResult)) {
                        return false;
                    }
                    var resultset = [];
                    $.each(jsonResult, function() {
                        var id = this.user_id;
                        var value;
                        if (this.email_id != '')
                            value = this.full_name + '(' + this.email_id + ')';
                        else
                            value = this.full_name;
                        var value1 = this.full_name;
                        resultset.push({
                            id: this.user_id,
                            value: value,
                            value1: value1
                        });
                    });
                    response(resultset);
                }
            });

        },
        open: function() {
            $('.ui-menu').width(350);
        },
        change: function(event, ui) {
            var numOfCharacterAllowedForStory, maxPassengerAllowed;
            if (ui.item)
            {
                $("#contact_name").val(ui.item.value1);
                $("#contact_name_id").val(ui.item.id);
                if ($("#case_contact_id").val() != '')
                {
                    $.ajax({
                        url: "/get-user-transaction",
                        method: 'POST',
                        data: {
                            contact_name_id: $("#contact_name_id").val()
                        },
                        success: function(result) {
                            if (!checkUserAuthenticationAjx(result)) {
                                return false;
                            }
                            $('#transaction_id option').each(function() {
                                $(this).remove();
                            });
                            $('#transaction_id').append(result);
                            $(".e1").select2();
                        }
                    });
                }
                if ($("#contact_name_id").val() != '')
                {
                    $.ajax({
                        url: "/get-membership-detail",
                        method: 'POST',
                        data: {
                            contact_name_id: $("#contact_name_id").val()
                        },
                        success: function(result) {
                            if (!checkUserAuthenticationAjx(result)) {
                                return false;
                            }
                            result = JSON.parse(result);
                            numOfCharacterAllowedForStory = result.max_story_characters;
                            maxPassengerAllowed = result.max_passengers;
                            $("#maximumPassengerAdd").html("( You can add upto " + maxPassengerAllowed + " passenger.)");
                        }
                    });
                }
            }
            else {
                numOfCharacterAllowedForStory = '';
                maxPassengerAllowed = '';
                $("#contact_name").val('');
                $("#contact_name_id").val('');
                $("#maximumPassengerAdd").html("");
            }
        },
        focus: function(event, ui) {
            $("#contact_name_id").val(ui.item.id);
            return false;
        },
        select: function(event, ui) {
            $("#contact_name").val(ui.item.value1);
            $("#contact_name_id").val(ui.item.id);

            return false;
        }

    });

    $("#passenger_id").autocomplete({
        afterAdd: true,
        selectFirst: true,
        autoFocus: true,
        minLength: 5,
        source: function(request, response) {
            showAjxLoader();
            $.ajax({
                url: "/get-passenger-id",
                method: 'POST',
                dataType: "json",
                data: {
                    passenger_id: $("#passenger_id").val()
                },
                success: function(jsonResult) {
                    var resultset = [];
                    $.each(jsonResult, function() {

                        var value = this.ID;
                        resultset.push({
                            value: this.ID
                        });
                    });
                    response(resultset);
                    hideAjxLoader();
                }
            });

        },
        open: function() {
            $('.ui-menu').width(100);
        },
        change: function(event, ui) {
            if (ui.item)
            {
                $("#passenger_id").val(ui.item.value);
            }
            else { 
                $("#passenger_id").val('');
            }
        },
        focus: function(event, ui) {
            return false;
        },
        select: function(event, ui) {
            $("#passenger_id").val(ui.item.value);
            return false;
        }

    });
    
    $("#location_one_caption").click(function(){
         if ($("#location_one_video_type").is(":checked") || $("#location_one_image_type").is(":checked")){
             $('input[name="location_one_caption"]').rules('add', {minlength: 5,maxlength:200});
         }else{
             $('input[name="location_one_caption"]').rules('remove', 'minlength');
             $('input[name="location_one_caption"]').rules('remove', 'maxlength');
         }
    });
     $("#location_two_caption").click(function(){
         if ($("#location_two_video_type").is(":checked") || $("#location_two_image_type").is(":checked")){
             $('input[name="location_two_caption"]').rules('add', {minlength: 5,maxlength:200});
         }else{
             $('input[name="location_two_caption"]').rules('remove', 'minlength');
             $('input[name="location_two_caption"]').rules('remove', 'maxlength');
         }
    });
     $("#location_three_caption").click(function(){
         if ($("#location_three_video_type").is(":checked") || $("#location_three_image_type").is(":checked")){
             $('input[name="location_three_caption"]').rules('add', {minlength: 5,maxlength:200});
         }else{
             $('input[name="location_three_caption"]').rules('remove', 'minlength');
             $('input[name="location_three_caption"]').rules('remove', 'maxlength');
         }
    });
     $("#location_four_caption").click(function(){
         if ($("#location_four_video_type").is(":checked") || $("#location_four_image_type").is(":checked")){
             $('input[name="location_four_caption"]').rules('add', {minlength: 5,maxlength:200});
         }else{
             $('input[name="location_four_caption"]').rules('remove', 'minlength');
             $('input[name="location_four_caption"]').rules('remove', 'maxlength');
         }
    });
     $("#location_five_caption").click(function(){
         if ($("#location_five_video_type").is(":checked") || $("#location_five_image_type").is(":checked")){
             $('input[name="location_five_caption"]').rules('add', {minlength: 5,maxlength:200});
         }else{
             $('input[name="location_five_caption"]').rules('remove', 'minlength');
             $('input[name="location_five_caption"]').rules('remove', 'maxlength');
         }
    });

    /*$('#crm_user_family_history_submitbutton').click(function() {
        if(getStats('story').chars == 0 || getStats('story').chars ==""){
            $("#story_label").show();
            $("#story_label").text("Please enter story");
           
        }
        if (numOfCharacterAllowedForStory != '')
            {
                if (getStats('story').chars > numOfCharacterAllowedForStory) {
                    $("#story_label").show();
                    $("#story_label").text("User can write upto " + numOfCharacterAllowedForStory + " characters story");
                    return false;
                }
                else
                {
                    $("#story_label").hide();
                    $("#story_label").text('');
                }
            }
        
    });*/
    
    $("#FamilyHistory").validate({
        submitHandler: function() {
            tinymce.get('story').save();
            var dataStr = $('#FamilyHistory').serialize();
            if (numOfCharacterAllowedForStory != '')
            {
                if (getStats('story').chars > numOfCharacterAllowedForStory) {
                    $("#story_label").show();
                    $("#story_label").text("User can write upto " + numOfCharacterAllowedForStory + " characters story");
                    return false;
                }
                else
                {
                    $("#story_label").hide();
                    $("#story_label").text('');
                }
            }

            showAjxLoader();
            $.ajax
                    ({
                        type: 'POST',
                        data: dataStr,
                        url: '/create-crm-family-history',
                        success: function(responseData)
                        {
                            if (!checkUserAuthenticationAjx(responseData)) {
                                return false;
                            }
                            var jsonObj = jQuery.parseJSON(responseData);
                            //var jsonObj =  responseData;
                            hideAjxLoader();
                            if (jsonObj.status === 'success') {
                                backToSearch();
                            } else {
                                $(".detail-section").css("display", "block");
                                $.each(jsonObj.message, function(i, msg) {
                                    $("#" + i).after('<label class="error" style="display:block;">' + msg + '</label>');
                                });
                            }

                        }
                    });
        },
        rules: {
            title: {
                required: true,
				maxlength:50
            },
            story: {
                required: true,
                //checkEditor:true
            },
            is_sharing: {
                required: true
            },
            is_email_on: {
                required: function() {
                    if ($("#is_sharing").is(':checked')) {
                        return true;
                    } else {
                        return false;
                    }
                }
            },
            location_one_video: {
                required: function() {
                    if ($("#location_one_video_type").is(':checked')) {
                        return true;
                    } else {
                        return false;
                    }
                },
                youtubevideo: true
            },
            location_two_video: {
                required: function() {
                    if ($("#location_two_video_type").is(':checked')) {
                        return true;
                    } else {
                        return false;
                    }
                },
                youtubevideo: true
            },
            location_three_video: {
                required: function() {
                    var videourl = $('#location_three_video').val();
                    if ($("#location_three_video_type").is(':checked')) {
                        return true;
                    } else {
                        return false;
                    }
                },
                youtubevideo: true
            },
            location_four_video: {
                required: function() {
                    if ($("#location_four_video_type").is(':checked')) {
                        return true;
                    } else {
                        return false;
                    }
                },
                youtubevideo: true
            },
            location_five_video: {
                required: function() {
                    if ($("#location_five_video").is(':checked')) {
                        return true;
                    } else {
                        return false;
                    }
                },
                youtubevideo: true
            }
        },
        messages: {
            title: {
                required: TITLE_EMPTY
            },
            story: {
                required: STORY_EMPTY
            },
            is_sharing: {
                required: SHARED_EMPTY
            },
            is_email_on: {
                required: FEATURED_EMAIL_EMPTY
            },
            location_one_video: {
                required: VIDEO_EMPTY
            },
            location_one_caption: {minlength:CAPTION_MIN_LENGHT,maxlength: CAPTION_MAX_LENGHT}, 
            location_two_video: {required: VIDEO_EMPTY},
            location_two_caption: {minlength:CAPTION_MIN_LENGHT,maxlength: CAPTION_MAX_LENGHT},
            location_three_video: {required: VIDEO_EMPTY}, 
            location_three_caption: {minlength:CAPTION_MIN_LENGHT,maxlength: CAPTION_MAX_LENGHT},
            location_four_video: {required: VIDEO_EMPTY},
            location_four_caption: {minlength:CAPTION_MIN_LENGHT,maxlength: CAPTION_MAX_LENGHT},
            location_five_video: {required:VIDEO_EMPTY},
            location_five_caption: {minlength:CAPTION_MIN_LENGHT,maxlength: CAPTION_MAX_LENGHT},
        },
        highlight: function(label) {
            $(".detail-section").css("display", "block");

        }
    });
    $("#updateFamilyHistory").validate({
        submitHandler: function() {
            tinymce.get('story').save();
            if (numOfCharacterAllowedForStory != '')
            {
                if (getStats('story').chars > numOfCharacterAllowedForStory) {

                    $("#story_label").show();
                    $("#story_label").text("User can write upto " + numOfCharacterAllowedForStory + " characters story");
                    return false;
                }
                else
                {
                    $("#story_label").hide();
                    $("#story_label").text('');
                }
            }
            var dataStr = $('#updateFamilyHistory').serialize();
            showAjxLoader();
            $.ajax
                    ({
                        type: 'POST',
                        data: dataStr,
                        url: '/update-crm-family-history',
                        success: function(responseData)
                        {
                            if (!checkUserAuthenticationAjx(responseData)) {
                                return false;
                            }
                            var jsonObj = jQuery.parseJSON(responseData);
                            //var jsonObj =  responseData;
                            hideAjxLoader();
                            if (jsonObj.status === 'success') {
                                backToSearch();
                            } else {
                                $(".detail-section").css("display", "block");
                                $.each(jsonObj.message, function(i, msg) {
                                    $("#" + i).after('<label class="error" style="display:block;">' + msg + '</label>');
                                });
                            }

                        }
                    });
        },
        rules: {
            title: {
                required: true,
				maxlength:50
            },
            story: {
                required: true
            },
            is_sharing: {
                required: true
            },
            is_email_on: {
                required: function() {
                    if ($("#is_sharing").is(':checked')) {
                        return true;
                    } else {
                        return false;
                    }
                }
            },
            location_one_video: {
                required: function() {
                    if ($("#location_one_video_type").is(':checked')) {
                        return true;
                    } else {
                        return false;
                    }
                },
                youtubevideo: true
            },
            location_two_video: {
                required: function() {
                    if ($("#location_two_video_type").is(':checked')) {
                        return true;
                    } else {
                        return false;
                    }
                },
                youtubevideo: true
            },
            location_three_video: {
                required: function() {
                    if ($("#location_three_video_type").is(':checked')) {
                        return true;
                    } else {
                        return false;
                    }
                },
                youtubevideo: true
            },
            location_four_video: {
                required: function() {
                    if ($("#location_four_video_type").is(':checked')) {
                        return true;
                    } else {
                        return false;
                    }
                },
                youtubevideo: true
            },
            location_five_video: {
                required: function() {
                    if ($("#location_five_video").is(':checked')) {
                        return true;
                    } else {
                        return false;
                    }
                },
                youtubevideo: true
            }
        },
        messages: {
            title: {
                required: TITLE_EMPTY
            },
            story: {
                required: STORY_EMPTY
            },
            is_sharing: {
                required: SHARED_EMPTY
            },
            is_email_on: {
                required: FEATURED_EMAIL_EMPTY
            },
            location_one_video: {
                required: VIDEO_EMPTY
            },
            location_one_caption: {minlength:CAPTION_MIN_LENGHT,maxlength: CAPTION_MAX_LENGHT}, 
            location_two_video: {required: VIDEO_EMPTY},
            location_two_caption: {minlength:CAPTION_MIN_LENGHT,maxlength: CAPTION_MAX_LENGHT},
            location_three_video: {required: VIDEO_EMPTY}, 
            location_three_caption: {minlength:CAPTION_MIN_LENGHT,maxlength: CAPTION_MAX_LENGHT},
            location_four_video: {required: VIDEO_EMPTY},
            location_four_caption: {minlength:CAPTION_MIN_LENGHT,maxlength: CAPTION_MAX_LENGHT},
            location_five_video: {required:VIDEO_EMPTY},
            location_five_caption: {minlength:CAPTION_MIN_LENGHT,maxlength: CAPTION_MAX_LENGHT},
        },
        highlight: function(label) {
            $(".detail-section").css("display", "block");

        }
    });
    var url = '/upload-main-image';
    $('#main_content').fileupload({
        url: url,
        dataType: 'json',
        start: function(e) {
            showAjxLoader();
        },
        done: function(e, data) {
            data = data.result;
            hideAjxLoader();
            if (data.status == 'success')
            {
                $("#filemessage").addClass('success-msg');
                $("#filemessage").removeClass('error');
                $("#main_content_id").val(data.filename);
            }
            else
            {
                $("#filemessage").removeClass('success-msg');
                $("#filemessage").addClass('error');
                $("#main_content_id").val('');
            }
            $("#filemessage").html(data.message);
        }
    });

    var url1 = '/upload-image-one';
    $('#location_one_image').fileupload({
        url: url1,
        dataType: 'json',
        start: function(e) {
            showAjxLoader();
        },
        done: function(e, data) {
            data = data.result;
            hideAjxLoader();
            if (data.status == 'success')
            {
                $("#filemessage1").show();
                $("#filemessage1").addClass('success-msg');
                $("#filemessage1").removeClass('error');
                $("#location_one_image_id").val(data.filename);
            }
            else
            {
                $("#filemessage1").removeClass('success-msg');
                $("#filemessage1").addClass('error');
                $("#location_one_image_id").val('');
            }
            $("#filemessage1").html(data.message);
        }
    });

    var url2 = '/upload-image-two';
    $('#location_two_image').fileupload({
        url: url2,
        dataType: 'json',
        start: function(e) {
            showAjxLoader();
        },
        done: function(e, data) {
            data = data.result;
            hideAjxLoader();
            if (data.status == 'success')
            {
                $("#filemessage2").show();
                $("#filemessage2").addClass('success-msg');
                $("#filemessage2").removeClass('error');
                $("#location_two_image_id").val(data.filename);
            }
            else
            {
                $("#filemessage2").removeClass('success-msg');
                $("#filemessage2").addClass('error');
                $("#location_two_image_id").val('');
            }
            $("#filemessage2").html(data.message);
        }
    });

    var url3 = '/upload-image-three';
    $('#location_three_image').fileupload({
        url: url3,
        dataType: 'json',
        start: function(e) {
            showAjxLoader();
        },
        done: function(e, data) {
            data = data.result;
            hideAjxLoader();
            if (data.status == 'success')
            {
                $("#filemessage3").addClass('success-msg');
                $("#filemessage3").removeClass('error');
                $("#location_three_image_id").val(data.filename);
            }
            else
            {
                $("#filemessage3").removeClass('success-msg');
                $("#filemessage3").addClass('error');
                $("#location_three_image_id").val('');
            }
            $("#filemessage3").html(data.message);
        }
    });

    var url4 = '/upload-image-four';
    $('#location_four_image').fileupload({
        url: url4,
        dataType: 'json',
        start: function(e) {
            showAjxLoader();
        },
        done: function(e, data) {
            data = data.result;
            hideAjxLoader();
            if (data.status == 'success')
            {
                $("#filemessage4").addClass('success-msg');
                $("#filemessage4").removeClass('error');
                $("#location_four_image_id").val(data.filename);
            }
            else
            {
                $("#filemessage4").removeClass('success-msg');
                $("#filemessage4").addClass('error');
                $("#location_four_image_id").val('');
            }
            $("#filemessage4").html(data.message);
        }
    });

    var url5 = '/upload-image-five';
    $('#location_five_image').fileupload({
        url: url5,
        dataType: 'json',
        start: function(e) {
            showAjxLoader();
        },
        done: function(e, data) {
            data = data.result;
            hideAjxLoader();
            if (data.status == 'success')
            {
                $("#filemessage5").addClass('success-msg');
                $("#filemessage5").removeClass('error');
                $("#location_five_image_id").val(data.filename);
            }
            else
            {
                $("#filemessage5").removeClass('success-msg');
                $("#filemessage5").addClass('error');
                $("#location_five_image_id").val('');
            }
            $("#filemessage5").html(data.message);
        }
    });



    if ($('#location_one_image').val() === '')
        $('#location_one_image').attr('disabled', 'disabled');
    if ($('#location_one_video').val() === '')
        $('#location_one_video').attr('disabled', 'disabled');
    if ($('#location_two_image').val() === '')
        $('#location_two_image').attr('disabled', 'disabled');
    if ($('#location_two_video').val() === '')
        $('#location_two_video').attr('disabled', 'disabled');
    if ($('#location_three_image').val() === '')
        $('#location_three_image').attr('disabled', 'disabled');
    if ($('#location_three_video').val() === '')
        $('#location_three_video').attr('disabled', 'disabled');
    if ($('#location_four_image').val() === '')
        $('#location_four_image').attr('disabled', 'disabled');
    if ($('#location_four_video').val() === '')
        $('#location_four_video').attr('disabled', 'disabled');
    if ($('#location_five_image').val() === '')
        $('#location_five_image').attr('disabled', 'disabled');
    if ($('#location_five_video').val() === '')
        $('#location_five_video').attr('disabled', 'disabled');
});

function enableVideoImage(elementId, elementNo) {

    if (elementId === 'location_' + elementNo + '_video_type') {
        $('#location_' + elementNo + '_video').removeAttr("disabled");
        $('#location_' + elementNo + '_image_type').attr('checked', false);
        $('#location_' + elementNo + '_image_type').next().attr('class', 'auto');
        $('#location_' + elementNo + '_image').val('');
        $('#location_' + elementNo + '_image').attr("disabled", "disabled");
    } else if (elementId === 'location_' + elementNo + '_image_type') {
        $('#location_' + elementNo + '_image').removeAttr("disabled");
        $('#location_' + elementNo + '_video_type').removeAttr('checked');
        $('#location_' + elementNo + '_video_type').next().attr('class', 'auto');
        $('#location_' + elementNo + '_video').val('');
        $('#location_' + elementNo + '_video').attr("disabled", "disabled");
    }
}
function enableFamilyHistory(ischecked) {
    if (ischecked === '1')
        $('#family_history_email').show();
    else
        $('#family_history_email').hide();
}

function addStatus(status) {
    $('#is_status').val(status);
}
function enableAssociation(Id) {
    if (Id == 'is_manual')
    {
        $("#person_in_database_div").hide();
        $("#manual_div").show();
        $("#is_in_db").removeAttr('checked');
    }
    else
    {
        $("#person_in_database_div").show();
        $("#manual_div").hide();
        $("#is_manual").removeAttr('checked');
    }
}
function backToSearch() {
    window.location.href = '/crm-family-history';
}



var totalPassenger = (typeof totalPassenger != 'undefined') ? totalPassenger : 1;
/**
 * function is used to  add new save search
 * @param void
 * @return void
 * @author Icreon Tech - DT
 */
function addMorePassenger(currAnchor)
{
    showAjxLoader();
    $.ajax({
        type: "POST",
        url: '/add-more-passenger',
        data: {
            totalPassenger: totalPassenger,
            maxPassengerAllowed: maxPassengerAllowed,
            numOfPassengerOnPage: numOfPassengerOnPage
        },
        success: function(responseData) {
            hideAjxLoader();
            if (!checkUserAuthenticationAjx(responseData)) {
                return false;
            }
            var allDivBlock = $("div[id^='person_in_database_block']");
            if (allDivBlock.length == 1)
            {
                var parId = $(currAnchor).parent().attr('id');
                var id = parId.substring(parId.lastIndexOf('_') + 1, parId.length);
                $(currAnchor).parent().append('<a class="minus m-t-35" href="javascript:void(0);" onclick="deleteMorePassenger(' + id + ')"></a>');
            }
            $(currAnchor).remove();
            $("#person_in_database_div").append(responseData);
            totalPassenger = totalPassenger + 1;
            numOfPassengerOnPage = numOfPassengerOnPage + 1;
        }
    });
}

/**
 * function is used to  remove save search
 * @param int
 * @return void
 * @author Icreon Tech - DT
 */
function deleteMorePassenger(id)
{
    $('#person_in_database_block_' + id).remove();
    var lastBlock = $("div[id^='person_in_database_block']").last();
    if ($(lastBlock).find('.plus').length == 0)
    {
        $(lastBlock).append('<a class="plus plus m-l-5 m-t-35" href="javascript:void(0);" onclick="addMorePassenger(this)" id="addMorePassenger"></a>');
    }
    var allDivBlock = $("div[id^='person_in_database_block']");
    if (allDivBlock.length == 1)
    {
        $(lastBlock).find('.minus').remove();
    }
    numOfPassengerOnPage = numOfPassengerOnPage - 1;
}

function editDbPassenger(id) {
    $('#db_passanger_list' + id).hide();
    $('#db_passanger_field' + id).show();
    
    $("#edit_db_passenger_id" + id).autocomplete({
        afterAdd: true,
        selectFirst: true,
        autoFocus: true,
        minLength: 5,
        source: function(request, response) {
            showAjxLoader();
            $.ajax({
                url: "/get-passenger-id",
                method: 'POST',
                dataType: "json",
                data: {
                    passenger_id: $("#edit_db_passenger_id" + id).val()
                },
                success: function(jsonResult) {
                    var resultset = [];
                    $.each(jsonResult, function() {

                        var value = this.ID;
                        resultset.push({
                            value: this.ID
                        });
                    });
                    response(resultset);
                    hideAjxLoader();
                }
            });

        },
        open: function() {
            $('.ui-menu').width(350);
        },
        change: function(event, ui) {
            if (ui.item)
            {
                $("#edit_db_passenger_id" + id).val(ui.item.value);
            }
            else { 
                $("#edit_db_passenger_id" + id).val('');
            }
        },
        focus: function(event, ui) {
            return false;
        },
        select: function(event, ui) {
            $("#edit_db_passenger_id" + id).val(ui.item.value);
            return false;
        }
    });
}
function updateDbPassenger(id) {
    $('#db_passanger_list' + id).show();

    var URL = '/update-crm-family-person';
    var family_person_id = id;
    var passenger_id = $('#edit_db_passenger_id' + id).val();
    var relationship_id = $('#edit_db_relationship_id' + id).val();
    var panel_no = $('#edit_db_panel_no' + id).val();
    var first_name = '';
    var last_name = '';
    var year_of_birth = '';
    var birth_country_id = '';

    var data = 'family_person_id=' + family_person_id + '&passenger_id=' + passenger_id + '&first_name=' + first_name + '&last_name=' + last_name + '&year_of_birth=' + year_of_birth + '&birth_country_id=' + birth_country_id + '&panel_no=' + panel_no + '&relationship_id=' + relationship_id;

    $('#update_passenger_id' + id).html(passenger_id);
    $('#update_relationship_id' + id).html($('#edit_db_relationship_id' + id).find(":selected").text());
    $('#update_panel_no_id' + id).html(panel_no);
    $('#db_passanger_field' + id).hide();

    $.ajax({
        type: "POST",
        url: URL,
        data: {
            familyPersonData: data
        },
        success: function(responseData) {
            if (!checkUserAuthenticationAjx(responseData)) {
                return false;
            }
            var jsonObj = jQuery.parseJSON(responseData);
        }
    });
}

var totalManualFamilyPerson = (typeof totalManualFamilyPerson != 'undefined') ? totalManualFamilyPerson : 1;
/**
 * function is used to  add new manual person
 * @param void
 * @return void
 * @author Icreon Tech - SK
 */
function addMoreMnaualPerson(currAnchor)
{
    showAjxLoader();
    $.ajax({
        type: "POST",
        url: '/add-more-manual-person',
        data: {
            totalManualFamilyPerson: totalManualFamilyPerson
        },
        success: function(responseData) {
            hideAjxLoader();
            if (!checkUserAuthenticationAjx(responseData)) {
                return false;
            }
            var allDivBlock = $("div[id^='manual_block']");
            if (allDivBlock.length == 1)
            {
                var parId = $(currAnchor).parent().attr('id');
                var id = parId.substring(parId.lastIndexOf('_') + 1, parId.length);
                $(currAnchor).parent().append('<a class="minus m-t-35" href="javascript:void(0);" onclick="deleteMoreMnaualPerson(' + id + ')"></a>');
            }
            $(currAnchor).remove();
            $("#manual_div").append(responseData);
            totalManualFamilyPerson = totalManualFamilyPerson + 1;
        }
    });
}

/**
 * function is used to  remove manual person
 * @param int
 * @return void
 * @author Icreon Tech - SK
 */
function deleteMoreMnaualPerson(id)
{
    $('#manual_block_' + id).remove();
    var lastBlock = $("div[id^='manual_block']").last();
    if ($(lastBlock).find('.plus').length == 0)
    {
        $(lastBlock).append('<a class="plus plus m-l-5 m-t-35" href="javascript:void(0);" onclick="addMoreMnaualPerson(this);" id="addMoreMnaualPerson"></a>');
    }
    var allDivBlock = $("div[id^='manual_block']");
    if (allDivBlock.length == 1)
    {
        $(lastBlock).find('.minus').remove();
    }
}

function editManual(id) {
    $('#manual_list_div' + id).hide();
    $('#manual_value_div' + id).show();
    $("select").select2();
    $("#country_of_birth_manual" + id).autocomplete({
        afterAdd: true,
        selectFirst: true,
        autoFocus: true,
        source: function(request, response) {
            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
            var resultset = [];
            $.each(country, function() {
                var id = this.id;
                var value = this.value;
                if (this.value && (matcher.test(value)))
                {
                    resultset.push({
                        id: this.id,
                        value: this.value

                    });
                }
                return;
            });
            response(resultset);
        },
        open: function() {
            $('.ui-menu').width(350);
        },
        change: function(event, ui) {
            if (ui.item)
            {
                $("#country_of_birth_manual" + id).val(ui.item.value);
                $("#country_of_birth_manual_id" + id).val(ui.item.id);
            }
            else {
                $("#country_of_birth_manual" + id).val('');
                $("#country_of_birth_manual_id" + id).val('');
            }
        },
        focus: function(event, ui) {
            $("#country_of_birth_manual_id" + id).val(ui.item.id);
            return false;
        },
        select: function(event, ui) {
            $("#country_of_birth_manual" + id).val(ui.item.value);
            $("#country_of_birth_manual_id" + id).val(ui.item.id);
            return false;
        }
    });
}
function updateManual(id) {
    $('#manual_list_div' + id).show();

    var URL = '/update-crm-family-person';

    var family_person_id = id;
    var passenger_id = null;
    var relationship_id = $('#edit_manual_relationship_id' + id).val();
    var panel_no = $('#edit_db_panel_no' + id).val();
    var first_name = $('#edit_manual_first_name' + id).val();
    var last_name = $('#edit_manual_last_name' + id).val();
    var year_of_birth = $('#edit_manual_year_of_birth' + id).val();
    var birth_country_id = $('#country_of_birth_manual_id' + id).val();

    $('#fname_manual' + id).html(first_name);
    $('#lname_manual' + id).html(last_name);
    $('#relationship_manual' + id).html($('#edit_manual_relationship_id' + id).find(":selected").text());
    $('#birth_year_manual' + id).html(year_of_birth);
    $('#birth_county_manual' + id).html($('#country_of_birth_manual' + id).val());
    $('#panel_manual' + id).html(panel_no);


    var data = 'family_person_id=' + family_person_id + '&passenger_id=' + passenger_id + '&first_name=' + first_name + '&last_name=' + last_name + '&year_of_birth=' + year_of_birth + '&birth_country_id=' + birth_country_id + '&panel_no=' + panel_no + '&relationship_id=' + relationship_id;

    $.ajax({
        type: "POST",
        url: URL,
        data: {
            familyPersonData: data
        },
        success: function(responseData) {
            if (!checkUserAuthenticationAjx(responseData)) {
                return false;
            }
            var jsonObj = jQuery.parseJSON(responseData);
        }
    });
    $('#manual_value_div' + id).hide();
}

function getStats(id) {
    var body = tinymce.get(id).getBody(), text = tinymce.trim(body.innerText || body.textContent);

    return {
        chars: text.length,
        words: text.split(/[\w\u2019\'-]+/).length
    };
}

function setPassenger(currRef,obj)
{   
    if(obj==''){
        $("#passenger_id").val($(currRef).text());
        $("#ui-id-2").remove();
    }else if(obj!=''){
        $("#passenger_id_"+obj).val($(currRef).text());
        $("#ui-id-2").remove();
    }
    
}
function addautofill(value) {
    $("#passenger_id_"+value).autocomplete({
        afterAdd: true,
        selectFirst: true,
        autoFocus: true,
        minLength: 5,
        source: function(request, response) {
            showAjxLoader();
            $.ajax({
                url: "/get-passenger-id",
                method: 'POST',
                dataType: "json",
                data: {

                    passenger_id: $("#passenger_id_"+value).val()

                },
                success: function(jsonResult) {
                    var resultset = [];
                    $.each(jsonResult, function() {

                        var value = this.ID;
                        resultset.push({
                            value: this.ID
                        });
                    });
                    response(resultset);
                    hideAjxLoader();
                }
            });

        },
        open: function() {
            $('.ui-menu').width(350);
        },
        change: function(event, ui) {
            if (ui.item)
            {
                $("#passenger_id_"+value).val(ui.item.value);
            }
            else { 
                $("#passenger_id_"+value).val('');
            }
        },
        focus: function(event, ui) {
            return false;
        },
        select: function(event, ui) {
            $("#passenger_id_"+value).val(ui.item.value);
            return false;
        }

    });


    $("#country_of_birth_manual" + value).autocomplete({
        afterAdd: true,
        selectFirst: true,
        autoFocus: true,
        source: function(request, response) {
            var matcher = new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
            var resultset = [];
            $.each(country, function() {
                var id = this.id;
                var value = this.value;
                if (this.value && (matcher.test(value)))
                {
                    resultset.push({
                        id: this.id,
                        value: this.value

                    });
                }
                return;
            });
            response(resultset);
        },
        open: function() {
            $('.ui-menu').width(350);
        },
        change: function(event, ui) {
            if (ui.item)
            {
                $("#country_of_birth_manual" + value).val(ui.item.value);
                $("#country_of_birth_manual_id" + value).val(ui.item.id);
            }
            else {
                $("#country_of_birth_manual" + value).val('');
                $("#country_of_birth_manual_id" + value).val('');
            }
        },
        focus: function(event, ui) {
            $("#country_of_birth_manual_id" + value).val(ui.item.id);
            return false;
        },
        select: function(event, ui) {
            $("#country_of_birth_manual" + value).val(ui.item.value);
            $("#country_of_birth_manual_id" + value).val(ui.item.id);
            return false;
        }
    });
}

function deleteFamilyPerson(family_person_id) {
    $('.delete_family_person').colorbox({
        width: "700px",
        height: "200px",
        inline: true
    });
    $("#no_delete_family_person").click(function() {
        $.colorbox.close();
        $('#yes_delete_family_person').unbind('click');
        return false;
    });

    $("#yes_delete_family_person").click(function() {
        $.colorbox.close();
        $('#yes_delete_family_person').unbind('click');
        showAjxLoader();

        var URL = '/delete-crm-family-person';
        var data = 'family_person_id=' + family_person_id;
        $.ajax({
            type: "POST",
            url: URL,
            data: {
                familyPersonId: data
            },
            success: function(responseData) {
                hideAjxLoader();
                if (!checkUserAuthenticationAjx(responseData)) {
                    return false;
                }
                var jsonObj = jQuery.parseJSON(responseData);
                backToSearch();
            }
        });

    });
}

function checkNotify(){
    if ($('input#is_notifiy').is(':checked'))
        $("#updateFamilyHistory").submit();
    else{
        $.colorbox({
            width:"700px",
            height:"190px",
            inline:true, 
            href:"#notify_confirm"
        });
        $("#no_refuse_save").click(function() {
            $.colorbox.close();
            $('#no_refuse_save').unbind('click');
            $( '<label for="is_notifiy" generated="true" class="error"><span class="redarrow"></span>Please check Notify Contact</label>' ).insertAfter( "#is_notifiy" );
            $('html,body').animate({scrollTop: $('#is_featured').offset().top}, 0);
            return false;
        });
        $("#yes_allow_save").click(function() {
        $.colorbox.close();
        $('#yes_allow_save').unbind('click');
           $("#updateFamilyHistory").submit();		
        });
    }	
}