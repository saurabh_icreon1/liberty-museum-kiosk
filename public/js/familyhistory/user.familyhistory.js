$(function() {
    $("#SearchUserFamilyHistory").validate({submitHandler: function() {
            $("#page").val("1");
            $("#is_featured").val("");
            var dataStr = $("#SearchUserFamilyHistory").serialize();
            showAjxLoader();
            $.ajax({type: "POST", data: dataStr, url: "/search-user-familyhistory", success: function(responseData) {
                    hideAjxLoader();
					if(document.getElementById('moreInfoId'))
						$('#moreInfoId').show();
                    $("#featured_text").hide();
                    if (document.getElementById("save_search"))
                        document.getElementById("save_search").style.display =
                                "block";
                    $("#family_content").addClass("featured-result-container");
                    $("#family_content").html(responseData)
                }})
        }, rules: {}, messages: {}});
    if (document.getElementById("save_search"))
        $("#save_search").validate({submitHandler: function() {
                showAjxLoader();
                $.ajax({type: "POST", data: {search_name: $.trim($("#save_search_as").val()), searchString: $("#SearchUserFamilyHistory").serialize()}, url: "/save-user-family-history-search", success: function(response) {
                        var jsonObj = jQuery.parseJSON(response);
                        if (jsonObj.status == "success") {
                            $("#save_search_as").val("");
                            $("#search_msg").removeClass("error");
                            document.getElementById("SearchUserFamilyHistory").reset();
                            location.reload()
                        } else
                            $("#search_msg").html("");
                        hideAjxLoader()
                    }})
            }, rules: {save_search_as: {required: true}}, messages: {save_search_as: {required: "Please enter search title"}}});
    $("#savehistory").click(function() {
        $("#savehistoryForm").submit()
    });
    if (document.getElementById("savehistoryForm"))
        $("#savehistoryForm").validate({submitHandler: function() {
                var dataStr = $("#savehistoryForm").serialize();
                $.ajax({type: "POST",
                    data: dataStr, url: "/save-user-histories", success: function(responseData) {
                        window.location.href = window.location.pathname
                    }})
            }, rules: {title: {required: true}}, messages: {title: {required: "Please enter valid title"}}})
});
function sendEmailFh(family_history_id) {
    $.ajax({type: "POST", data: {family_history_id: family_history_id}, url: "/sendemail-familyhistory", success: function(response) {
            var jsonObj = jQuery.parseJSON(response);
            if (jsonObj.status === "success") {
                $("#sendEmailButtons").hide();
                $("#success-msg").show();
                window.parent.window.scrollTo(0, 0)
            }
        }})
}
function deleteSavedFamilyhistories(value) {
    $(".saved_history").colorbox({width: "600px", height: "260px", inline: true});
    $("#no_my_fh").click(function() {
        $.colorbox.close();
        $("#yes_my_fh").unbind("click");
        return false
    });
    $("#yes_my_fh").click(function() {
        $("#yes_my_fh").unbind("click");
        showAjxLoader();
        $.ajax({type: "POST", data: {search_id: value}, url: "/delete-search-family-history", success: function(response) {
                window.location.href = window.location.pathname
            }})
    })
}
function sendEmailRequest(family_history_id, title, user_id, defaultPermission) {
    authenticateUserBeforeAjax();
    if (defaultPermission)
        $.colorbox({width: "930px", height: "900px", iframe: true, href: "/sendemail-familyhistory/" + family_history_id + "/" + title + "/" + defaultPermission + "/" + user_id});
    $("#sendEmailButtons").show();
    $("#no").click(function() {
        $.colorbox.close();
        $("#yes").unbind("click");
        return false
    });
    $("#yes").click(function() {
        $.ajax({type: "POST", data: {family_history_id: family_history_id}, url: "/sendemail-familyhistory",
            success: function(response) {
                var jsonObj = jQuery.parseJSON(response);
                if (jsonObj.status === "success") {
                    $("#sendEmailButtons").hide();
                    $("#success-msg").show();
                    $("html,body").animate({scrollTop: $("#cboxClose").offset().top}, 0)
                }
            }})
    })
}
function viewSavedSearch(fh_search_id) {
    $.ajax({type: "POST", data: {search_id: fh_search_id}, url: "/get-user-search-family-saved-select", success: function(response) {
            hideAjxLoader();
            var searchParamArray = response;
            var obj = jQuery.parseJSON(searchParamArray);
            $("#contact_name").val(obj["contact_name"]);
            $("#searchbutton").click()
        }})
}
function checkuserlogin() {
    $(".loginsignup").colorbox({width: "700px", height: "250px", inline: true});
    $("#cancel").click(function() {
        $.colorbox.close();
        return false
    });
    $("#login").click(function() {
        loginPopup("profile");
        return false
    });
    $("#register").click(function() {
        signupPopup();
        return false
    })
}
function deleteSaveSearch(search_id) {
    $(".deletesearch").colorbox({width: "700px", height: "300px", inline: true});
    $("#no").click(function() {
        $.colorbox.close();
        $("#yes").unbind("click");
        return false
    });
    $("#yes").click(function() {
        $.ajax({type: "POST", data: {search_id: search_id}, url: "/delete-user-search", success: function(response) {
                var jsonObj = jQuery.parseJSON(response);
                if (jsonObj.status === "success") {
                    $.colorbox.close();
                    location.reload()
                }
            }})
    })
}
function loadMore() {
    var page = parseInt($("#page").val());
    if ($("#loadingDiv" + "-" + page))
        $("#loadingDiv" + "-" + page).append('<div class="loader-class"><img src ="/images/loading.gif"></div>');
    $("#page").val(page + 1);
    $("#load_more" + page).html("");
    var dataStr = $("#SearchUserFamilyHistory").serialize();
    $.ajax({type: "POST", data: {dataStr: dataStr, page: $("#page").val()}, url: "/more-user-familyhistory", success: function(responseData) {
            $("#loadingDiv" + "-" + page).html("");
            $("#lastDiv").append(responseData);
            $("#load_more" +
                    "-" + page).html("")
        }})
}
function back() {
    var refere_url = $("#referer").val();
    window.location.href = refere_url
}
function validitycheck(value) {
    if (value == "notallow")
        $(".notallow").colorbox({width: "600px", height: "260px", inline: true});
    else if (value == "exceed")
        $(".exceed").colorbox({width: "600px", height: "320px", inline: true});
    else
        $("#save_search").submit()
}
function showvideo(video_id) {
    $(".video_" + video_id).colorbox({width: "450px", height: "360px", inline: true, onOpen: function() {
            showAjxLoader()
        }, onComplete: function() {
            hideAjxLoader()
        }})
}
;
