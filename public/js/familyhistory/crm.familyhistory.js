$(function() {
    $("#added_date_from").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, onClose: function(selectedDate) {
            $("#added_date_to").datepicker("option", "minDate", selectedDate)
        }});
    $("#added_date_to").datepicker({changeMonth: true, changeYear: true, showOtherMonths: true, onClose: function(selectedDate) {
            $("#added_date_from").datepicker("option", "maxDate", selectedDate)
        }});
    $("#first_name").autocomplete({afterAdd: true, selectFirst: true, autoFocus: true, source: function(request, response) {
            $.ajax({url: "/get-user-family-person",
                method: "POST", dataType: "json", data: {first_name: $("#first_name").val()}, success: function(jsonResult) {
                    var resultset = [];
                    $.each(jsonResult, function() {
                        var value = this.first_name;
                        resultset.push({value: value})
                    });
                    response(resultset)
                }})
        }, open: function() {
            $(".ui-menu").width(300)
        }, change: function(event, ui) {
            if (ui.item)
                $("#first_name").val(ui.item.value);
            else
                $("#first_name").val("");
            $(".ui-autocomplete").jScrollPane()
        }, select: function(event, ui) {
            $("#first_name").val(ui.item.value);
            return false
        }});
    $("#last_name").autocomplete({afterAdd: true,
        selectFirst: true, autoFocus: true, source: function(request, response) {
            $.ajax({url: "/get-user-family-person", method: "POST", dataType: "json", data: {last_name: $("#last_name").val()}, success: function(jsonResult) {
                    var resultset = [];
                    $.each(jsonResult, function() {
                        var value = this.first_name;
                        resultset.push({value: value})
                    });
                    response(resultset)
                }})
        }, open: function() {
            $(".ui-menu").width(300)
        }, change: function(event, ui) {
            if (ui.item)
                $("#last_name").val(ui.item.value);
            else
                $("#last_name").val("");
            $(".ui-autocomplete").jScrollPane()
        },
        select: function(event, ui) {
            $("#last_name").val(ui.item.value);
            return false
        }});
    $("#contact_name").autocomplete({afterAdd: true, selectFirst: true, autoFocus: true, source: function(request, response) {
            $(".ui-menu").hide();
            $.ajax({url: "/get-authorised-associated-family-person", method: "POST", dataType: "json", data: {contact_name: $("#contact_name").val()}, success: function(jsonResult) {
                    if (!checkUserAuthenticationAjx(jsonResult))
                        return false;
                    var resultset = [];
                    $.each(jsonResult, function() {
                        var id = this.user_id;
                        var value;
                        if (this.email_id != "")
                            value = this.full_name + " (" + this.email_id + ")";
                        else
                            value = this.full_name;
                        var value1 = this.full_name;
                        resultset.push({value: value, value1: value1})
                    });
                    response(resultset)
                }})
        }, open: function() {
            $(".ui-menu").width(350)
        }, change: function(event, ui) {
            if (ui.item)
                $("#contact_name").val(ui.item.value1)
        }, select: function(event, ui) {
            $("#contact_name").val(ui.item.value1);
            return false
        }});
    $("#country").autocomplete({afterAdd: true, selectFirst: true, autoFocus: true, source: function(request, response) {
            var matcher =
                    new RegExp($.ui.autocomplete.escapeRegex(request.term), "i");
            var resultset = [];
            $.each(country, function() {
                var id = this.id;
                var value = this.value;
                if (this.value && matcher.test(value))
                    resultset.push({id: this.id, value: this.value});
                return
            });
            response(resultset)
        }, open: function() {
            $(".ui-menu").width(350)
        }, change: function(event, ui) {
            if (ui.item) {
                $("#country").val(ui.item.value);
                $("#country_id").val(ui.item.id)
            } else {
                $("#country").val("");
                $("#country_id").val("")
            }
            $(".ui-autocomplete").jScrollPane()
        }, focus: function(event,
                ui) {
            $("#country_id").val(ui.item.id);
            return false
        }, select: function(event, ui) {
            $("#country").val(ui.item.value);
            $("#country_id").val(ui.item.id);
            return false
        }});
    var grid = jQuery("#list_case");
    var emptyMsgDiv = $('<div class="no-record-msz">' + NO_RECORD_FOUND + "</div>");
    $("#list_case").jqGrid({mtype: "POST", url: "/crm-family-history", datatype: "json", sortable: true, colNames: ["", L_TITLE, L_CONTACT_NAME, L_PUBLISHED_DATE, L_LAST_MODIFIED_DATE, L_SHARED, L_FEATURED, L_STATUS, L_ACTIONS], colModel: [{name: "User Family History Id",
                index: "family_history_id", hidden: true}, {name: "Title", index: "title"}, {name: "Contact Name", index: "tbl_usr_users.first_name"}, {name: "Published Date", index: "publish_date", align: "center"}, {name: "Last Modified Date", index: "modified_date", align: "center"}, {name: "Shared", index: "is_shared", align: "center"}, {name: "Featured", index: "is_fearured", align: "center"}, {name: "Status", index: "is_status", align: "center"}, {name: "Action", sortable: false, cmTemplate: {title: false}}], viewrecords: true, sortname: "tbl_usr_family_histories.modified_date",
        sortorder: "desc", rowNum: "10", rowList: pagesArr, pager: "#listcaserud", viewrecords:true, autowidth: true, shrinkToFit: true, caption: "", width: "100%", cmTemplate: {title: false}, loadComplete: function() {
            hideAjxLoader();
            var count = grid.getGridParam();
            var ts = grid[0];
            if (ts.p.reccount === 0) {
                grid.hide();
                emptyMsgDiv.show();
                $("#listcaserud_right div.ui-paging-info").css("display", "none")
            } else {
                grid.show();
                emptyMsgDiv.hide();
                $("#listcaserud_right div.ui-paging-info").css("display", "block")
            }
        }, afterInsertRow: function(rowid,
                rowdata) {
            if (rowdata.Urgent == 1)
                $(this).jqGrid("setRowData", rowid, false, "urgentRecord")
        }});
    $("#searchbutton").click(function() {
        showAjxLoader();
        $("#search_msg").hide();
        $("#success_message").hide();
        jQuery("#list_case").jqGrid("setGridParam", {postData: {searchString: $("#SearchCrmFamilyHistory").serialize()}});
        jQuery("#list_case").trigger("reloadGrid", [{page: 1}]);
        window.location.hash = "#saved_searches";
        hideAjxLoader();
        return false
    });
    emptyMsgDiv.insertAfter(grid.parent());
    emptyMsgDiv.hide();
    $("#list_case").jqGrid("navGrid",
            "#listcaserud", {reload: true, edit: false, add: false, search: false, del: false});
    $("#list_case").jqGrid("navButtonAdd", "#listcaserud", {caption: "", title: "Export", id: "exportExcel", onClickButton: function() {
            exportExcel("list_case", "/crm-family-history")
        }, position: "last"});
    $("#save_search").validate({submitHandler: function() {
            showAjxLoader();
            $.ajax({type: "POST", data: {search_name: $.trim($("#save_search_as").val()), searchString: $("#SearchCrmFamilyHistory").serialize(), search_id: $("#search_id").val()}, url: "/save-family-history-search",
                success: function(response) {
                    var jsonObj = jQuery.parseJSON(response);
                    if (jsonObj.status == "success") {
                        $("#save_search_as").val("");
                        $("#search_msg").removeClass("error");
                        document.getElementById("SearchCrmFamilyHistory").reset();
                        location.reload()
                    } else
                        $("#search_msg").html(ERROR_SAVING_SEARCH);
                    hideAjxLoader()
                }})
        }, rules: {save_search_as: {required: true}}, messages: {save_search_as: {required: ENTER_SEARCH}}});
    $("#saved_searches").change(function() {
        $("#search_msg").hide();
        if ($("#saved_searches").val() != "") {
            if ($("#saved_searches").val() !=
                    "") {
                $("#savesearch").val("Update");
                $("#deletebutton").show();
                $('label[for="save_search_as"]').hide()
            }
            $.ajax({type: "POST", data: {search_id: $("#saved_searches").val()}, url: "/get-crm-search-family-saved-select", success: function(response) {
                    hideAjxLoader();
                    var searchParamArray = response;
                    var obj = jQuery.parseJSON(searchParamArray);
                    document.getElementById("SearchCrmFamilyHistory").reset();
                    $("#first_name").val(obj["first_name"]);
                    $("#last_name").val(obj["last_name"]);
                    $("#is_status").val(obj["is_status"]);
                    $("#title").val(obj["title"]);
                    $("#added_date_range").val(obj["added_date_range"]).select2();
                    if ($("#added_date_range").val() == "1")
                        $("#date_range_div").show();
                    else
                        $("#date_range_div").hide();
                    $("#added_date_from").val(obj["added_date_from"]);
                    $("#added_date_to").val(obj["added_date_to"]);
                    $("#is_sharing").val(obj["is_sharing"]).select2();
                    $("#is_featured").val(obj["is_featured"]);
                    $("#country").val(obj["country"]);
                    $("#country_id").val(obj["country_id"]);
                    $("#save_search_as").val(obj["search_title"]);
                    $("#search_id").val($("#saved_searches").val());
                    $("#search").click()
                }})
        } else {
            $("#save_search_as").val("");
            $("#search_id").val("");
            $("#savesearch").val("Save");
            $("#deletebutton").hide();
            hideAjxLoader()
        }
    })
});
function showDate(id, divId) {
    if ($("#" + id + " option:selected").text() == "Choose Date Range")
        $("#" + divId).show();
    else
        $("#" + divId).hide()
}
function deleteFamilyhistory(id) {
    $(".delete_familyhistory").colorbox({width: "700px", height: "180px", inline: true});
    $("#no").click(function() {
        $.colorbox.close();
        $("#yes").unbind("click");
        return false
    });
    $("#yes").click(function() {
        $.ajax({url: "/delete-crm-family-history/" + id, method: "POST", dataType: "html", success: function(jsonResult) {
                if (!checkUserAuthenticationAjx(jsonResult))
                    return false;
                location.reload()
            }})
    })
}
function deleteSearch() {
    $(".delete_family_search").colorbox({width: "700px", height: "180px", inline: true});
    $("#no_delete").click(function() {
        $.colorbox.close();
        $("#yes_delete").unbind("click");
        return false
    });
    $("#yes_delete").click(function() {
        $.ajax({type: "POST", data: {search_id: $("#search_id").val()}, url: "/delete-crm-search-familyhistory", success: function(response) {
                var jsonObj = jQuery.parseJSON(response);
                if (jsonObj.status == "success") {
                    $("#search_msg").html(DELETE_CONFIRM);
                    location.reload()
                }
            }})
    })
}
;
