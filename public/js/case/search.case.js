$(function(){
    $("#added_date_from").datepicker({
        changeMonth:true,
        changeYear:true,
        showOtherMonths:true,
        onClose:function(selectedDate){
            $("#added_date_to").datepicker("option","minDate",selectedDate)
            }
        });
$("#added_date_to").datepicker({
    changeMonth:true,
    changeYear:true,
    showOtherMonths:true,
    onClose:function(selectedDate){
        $("#added_date_from").datepicker("option","maxDate",selectedDate)
        }
    });
	$("#start_date_from").datepicker({
        changeMonth:true,
        changeYear:true,
        showOtherMonths:true,
        onClose:function(selectedDate){
            $("#start_date_to").datepicker("option","minDate",selectedDate)
            }
        });
$("#start_date_to").datepicker({
    changeMonth:true,
    changeYear:true,
    showOtherMonths:true,
    onClose:function(selectedDate){
        $("#start_date_from").datepicker("option","maxDate",selectedDate)
        }
    });
$("#case_type").change(function(){
    $("div[class^='case_type']").hide();
    if($(".case_type"+this.value))$(".case_type"+this.value).show()
        });
if($(".case_type"+$("#case_type").val()))$(".case_type"+$("#case_type").val()).show();
    $("#case_status").change(function(){
    $("div[class^='case_status']").hide();
    if($(".case_status"+this.value))$(".case_status"+this.value).show()
        });
if($(".case_status"+$("#case_status").val()))$(".case_status"+$("#case_status").val()).show();
    var firstBreadCrumbAnchor=$(".breadcrumb a").first();
    $(".breadcrumb").prepend($(firstBreadCrumbAnchor).text());
    $(firstBreadCrumbAnchor).remove();
    var grid=jQuery("#list_case");
    var emptyMsgDiv=
    $('<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>");
    $("#list_case").jqGrid({
        mtype:"POST",
        url:"/cases",
        datatype:"json",
        sortable:true,
        colNames:["",L_CASE_HASH,L_SUBJECT,L_MESSAGE,L_TRANSACTION_HASH,L_CREATED_DATE,L_DATE,L_MODIFIED_DATE,L_TYPE,L_STATUS,L_CONTACT_NAME, L_ASSIGNED_TO, L_ACTIONS],
        colModel:[{
            name:"Urgent",
            index:"is_urgent",
            hidden:true
        },{
            name:"Case",
            index:"case_contact_id"
        },{
            name:"Subject",
            index:"case_subject"
        },{
            name:"Message",
            index:"case_message"
        },{
            name:"Transaction",
            index:"user_transaction_id"
        },{
            name:"Created Date",
            index:"tbl_case.added_date"
        },{
            name:"Start Date",
            index:"case_start_date"
        },{
            name:"Modified Date",
            index:"tbl_case.modified_date"
        },{
            name:"Type",
            index:"case_type"
        },{
            name:"Status",
            index:"case_status"
        },{
            name:"Contact Name",
            index:"full_name"
        },{
            name:"Assigned To",
            index:"assigned_to"
        },{
            name:"Action",
            sortable:false,
            cmTemplate:{
                title:false
            }
        }],
    viewrecords:true,
    sortname:"if(tbl_case.modified_date='0000-00-00 00:00:00',tbl_case.added_date,tbl_case.modified_date)",
    sortorder:"desc",
    rowNum:numRecPerPage,
    rowList:pagesArr,
    pager:"#listcaserud",
    viewrecords:true,
    autowidth:true,
    shrinkToFit:true,
    caption:"",
    width:"100%",
    cmTemplate:{
        title:false
    },
	postData:{
			searchString: $('#search_case').serialize()
	},
    loadComplete:function(){
        hideAjxLoader();
        var count=grid.getGridParam();
        var ts=grid[0];
        if(ts.p.reccount===0){
            grid.hide();
            emptyMsgDiv.show();
            $("#listcaserud_right div.ui-paging-info").css("display",
                "none")
            }else{
            grid.show();
            emptyMsgDiv.hide();
            $("#listcaserud_right div.ui-paging-info").css("display","block")
            }
        },
    afterInsertRow:function(rowid,rowdata){
        if(rowdata.Urgent==1)$(this).jqGrid("setRowData",rowid,false,"urgentRecord")
            }
        });
emptyMsgDiv.insertAfter(grid.parent());
emptyMsgDiv.hide();
$("#list_case").jqGrid("navGrid","#listcaserud",{
    reload:true,
    edit:false,
    add:false,
    search:false,
    del:false
});
$("#list_case").jqGrid("navButtonAdd","#listcaserud",{
    caption:"",
    title:"Export",
    id:"exportExcel",
    onClickButton:function(){
        exportExcel("list_case",
            "/cases")
        },
    position:"last"
});
if(typeof contact_id!="undefined"){
    $(".e1").select2();
    $(".e2").customInput();
    var gridContactCaseList=jQuery("#contact_list_case");
    $("#contact_list_case").jqGrid({
        mtype:"POST",
        url:"/contact-cases",
        datatype:"json",
        sortable:true,
        postData:{
            contact_id:contact_id
        },
        colNames:[L_CASE_HASH,L_TYPE,L_CREATED_DATE,L_SUBJECT,L_STATUS,L_ACTIONS],
        colModel:[{
            name:"Case",
            index:"case_contact_id"
        },{
            name:"Type",
            index:"case_type"
        },{
            name:"Date",
            index:"added_date"
        },{
            name:"Subject",
            index:"case_subject"
        },{
            name:"Status",
            index:"case_status"
        },

        {
            name:"Action"
        }],
        viewrecords:true,
        sortname:"modified_date",
        sortorder:"desc",
        rowNum:10,
        rowList:[10,20,30],
        pager:"#contactlistcaserud",
        viewrecords:true,
        autowidth:true,
        shrinkToFit:true,
        caption:"",
        width:"100%",
        cmTemplate:{
            title:false
        },				
        loadComplete:function(){
            hideAjxLoader();
            var count=gridContactCaseList.getGridParam();
            var ts=gridContactCaseList[0];
            if(ts.p.reccount===0){
                gridContactCaseList.hide();
                emptyMsgDiv.show();
                $("#contactlistcaserud_right div.ui-paging-info").css("display","none")
                }else{
                gridContactCaseList.show();
                emptyMsgDiv.hide();
                $("#contactlistcaserud_right div.ui-paging-info").css("display","block")
                }
            }
    });
emptyMsgDiv.insertAfter(gridContactCaseList.parent());
emptyMsgDiv.hide();
$("#contact_list_case").jqGrid("navGrid","#contactlistcaserud",{
    reload:true,
    edit:false,
    add:false,
    search:false,
    del:false
})
}
$("#searchcontactcasebutton").click(function(){
    showAjxLoader();
    $("#success_message").hide();
    $("#success_message").html("");
    jQuery("#contact_list_case").jqGrid("setGridParam",{
        postData:{
            searchString:$("#contact_search_case").serialize()
            }
        });
jQuery("#contact_list_case").trigger("reloadGrid",[{
    page:1
}]);
return false
});
$("#searchcasebutton").click(function(){
    $("#success_message").hide();
    $("#success_message").html("");
    showAjxLoader();
    jQuery("#list_case").jqGrid("setGridParam",{
        postData:{
            searchString:$("#search_case").serialize()
            }
        });
jQuery("#list_case").trigger("reloadGrid",[{
    page:1
}]);
window.location.hash="#saved_search";
return false
});
$("#assigned_to").autocomplete({
    afterAdd:true,
    selectFirst:true,
    autoFocus:true,
    source:function(request,
        response){
        $.ajax({
            url:"/get-crm-contacts",
            method:"POST",
            dataType:"json",
            data:{
                crm_full_name:$("#assigned_to").val()
                },
            success:function(jsonResult){
                if(!checkUserAuthenticationAjx(jsonResult))return false;
                var resultset=[];
                $.each(jsonResult,function(){
                    var id=this.crm_user_id;
                    var value=this.crm_full_name;
                    resultset.push({
                        id:this.crm_user_id,
                        value:this.crm_full_name
                        })
                    });
                response(resultset)
                }
            })
    },
open:function(){
    $(".ui-menu").width(350)
    },
change:function(event,ui){
    if(ui.item){
        $("#assigned_to").val(ui.item.value);
        $("#assigned_to_id").val(ui.item.id)
        }else{
        $("#assigned_to").val("");
        $("#assigned_to_id").val("")
        }
        $(".ui-autocomplete").jScrollPane()
    },
focus:function(event,ui){
    $("#assigned_to_id").val(ui.item.id);
    return false
    },
select:function(event,ui){
    $("#assigned_to").val(ui.item.value);
    $("#assigned_to_id").val(ui.item.id);
    return false
    }
});
	
});
function showDate(id,divId){
    if($("#"+id+" option:selected").text()=="Choose Date Range")$("#"+divId).show();else $("#"+divId).hide()
        }
function getSavedCaseSearchResult(){
    $("#search_msg").css("display","none");
    $("#search_msg").html("");
    if($("#saved_search").val()!=""){
        $("#savebutton").val("Update");
        $("#deletebutton").show()
        }else{
        $("#savebutton").val("Save");
        $("#search_name").val("");
        $("#deletebutton").hide();
        return false
        }
        showAjxLoader();
    $.ajax({
        type:"POST",
        data:{
            search_id:$("#saved_search").val()
            },
        url:"/get-case-search-saved-param",
        success:function(response){
            hideAjxLoader();
            if(!checkUserAuthenticationAjx(response))return false;
            var searchParamArray=
            response;
            var obj=jQuery.parseJSON(searchParamArray);
            document.getElementById("search_case").reset();
			$("#case_status option:selected").removeAttr("selected");
			
            $("#case_subject").val(obj["case_subject"]);
            $("#user_email").val(obj["user_email"]);
            $("#user_transaction_id").val(obj["user_transaction_id"]);
			
			var case_status = obj["case_status"];
			var valArr = case_status.split(",");
			size = valArr.length;		
			
			for(k=0;k<size;k++){
				$("#case_status option[value='" + valArr[k] + "']").prop("selected", "selected");
			}
            //$("#case_status").select2("val",obj["case_status"]);
            if($(".case_status"+$("#case_status").val()))$(".case_status"+$("#case_status").val()).show();
            $("#case_type").select2("val",obj["case_type"]);
            if($(".case_type"+$("#case_type").val()))$(".case_type"+$("#case_type").val()).show();
            if(obj["is_converted"]==1)$("#is_converted").attr("checked","checked").customInput();
            if(obj["is_urgent"]==1)$("#is_urgent").attr("checked","checked").customInput();
            if(obj["is_archive"]==1)$("#is_archive").attr("checked","checked").customInput();
            $("#added_date_range").select2("val",obj["added_date_range"]);
            $("#added_date_to").val(obj["added_date_to"]);
            $("#assigned_to").val(obj["assigned_to"]);
            $("#assigned_to_id").val(obj["assigned_to_id"]);
            $("#search_name").val(obj["search_title"]);
            $("#search_id").val($("#saved_search").val());
            $("#searchcasebutton").click()
            }
        })
}
$("#savebutton").click(function(){
    if($.trim($("#search_name").val())!=""){
        $("#search_msg").html("");
        $("#search_msg").css("display","none");
        showAjxLoader();
        $.ajax({
            type:"POST",
            data:{
                search_name:$.trim($("#search_name").val()),
                searchString:$("#search_case").serialize(),
                search_id:$("#search_id").val()
                },
            url:"/save-case_search",
            success:function(response){
                hideAjxLoader();
                if(!checkUserAuthenticationAjx(response))return false;
                var jsonObj=jQuery.parseJSON(response);
                if(jsonObj.status=="success"){
                    $("#search_name").val("");
                    document.getElementById("search_case").reset();
                    if($("#search_id").val()!="")location.reload();else location.reload()
                        }else{
                    $("#search_msg").html(ERROR_SAVING_SEARCH);
                    $("#search_msg").addClass("error");
                    $("#search_msg").removeClass("success-msg clear");
                    $("#search_msg").css("display","block")
                    }
                }
        })
}else{
    $("#search_msg").html(SEARCH_NAME_MSG);
    $("#search_msg").addClass("error");
    $("#search_msg").removeClass("success-msg clear");
    $("#search_msg").show();
    return false
    }
});
function deleteSaveSearch(){
    $(".delete_saved_search").colorbox({
        width:"700px",
        height:"200px",
        inline:true
    });
    $("#no_delete").click(function(){
        $.colorbox.close();
        $("#yes_delete").unbind("click");
        return false
        });
    $("#yes_delete").click(function(){
        $.colorbox.close();
        $("#yes_delete").unbind("click");
        showAjxLoader();
        $.ajax({
            type:"POST",
            data:{
                search_id:$("#search_id").val()
                },
            url:"/delete-case-search",
            success:function(response){
                hideAjxLoader();
                if(!checkUserAuthenticationAjx(response))return false;
                var jsonObj=
                jQuery.parseJSON(response);
                if(jsonObj.status=="success")location.reload()
                    }
                })
    })
}
function deleteCase(case_contact_id){
    $(".delete_case").colorbox({
        width:"700px",
        height:"200px",
        inline:true
    });
    $("#no_delete_case").click(function(){
        $.colorbox.close();
        $("#yes_delete_case").unbind("click");
        return false
        });
    $("#yes_delete_case").click(function(){
        $.colorbox.close();
        $("#yes_delete_case").unbind("click");
        showAjxLoader();
        $.ajax({
            type:"POST",
            dataType:"html",
            data:{
                case_contact_id:case_contact_id
            },
            url:"/delete-case",
            success:function(responseData){
                hideAjxLoader();
                if(!checkUserAuthenticationAjx(responseData))return false;
                var jsonObj=JSON.parse(responseData);
                if(jsonObj.status=="success")window.location.href="/cases";else $("#error_message").html(jsonObj.message)
                    }
                })
    })
};
