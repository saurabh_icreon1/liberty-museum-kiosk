$(function(){
    $("#cboxClose").click(function(){
        parent.jQuery("#tab-9").click();
        parent.jQuery.colorbox.close();
        jQuery.colorbox.close()
        });
    $("#cancel_add_case").click(function(){
        parent.jQuery("#tab-9").click();
        parent.jQuery.colorbox.close();
        jQuery.colorbox.close()
        });
    jQuery.validator.addMethod("alphanumericspecialchar",function(value,element){
        return this.optional(element)||/^[A-Za-z0-9\n\r \/,.'"$#@!%&*()-:]+$/i.test(value)},ALPHA_NUMERIC_SPECIALSYMPOL);jQuery.validator.addMethod("alphanumerichypenchar",
    function(value,element){
        return this.optional(element)||/^[A-Za-z0-9\n\r -]+$/i.test(value)
        },ALPHA_NUMERIC_SPECIALSYMPOL);

    $('#message').limiter(2000, $("#message_count"));
    $('#reply').limiter(2000, $("#reply_count"));
    
    $("#usercase").validate({
        submitHandler:function(){
            //var dataStr=$("#usercase").serialize();            
            var passDetails = $("#passengerDetails").html();
            showAjxLoader();
            $.ajax({
                type:"POST",
                url:"/add-user-case/"+$("#contact_name_id").val(),
                data:{dataStr:$("#usercase").serialize(), passeneger_details:passDetails},
                success:function(responseData){
                    hideAjxLoader();
                    var jsonObj=jQuery.parseJSON(responseData);
                    if(jsonObj.status=="success"){
						$('#continue').attr('disabled','disabled');
                        var success_msg="Case # : "+jsonObj.caseId+" was opened up.";
                        $("#case_id_success_msg").html(success_msg);
                        parent.$("#cboxClose").hide();
                        $.colorbox({
                            width:"490px",
                            href:"#case_addes",
                            height:"350px",
                            inline:true
                        })
                        }else if(jsonObj.transaction)$("#transaction_id").after('<label class="error" for="transaction_id" style="display:block;">'+jsonObj.message+"</label>");else $.each(jsonObj.message,function(i,msg){
                        $('label[for="'+i+'"]').remove();
                        $("#"+i).after('<label for="'+i+'" class="error" style="display:block;">'+msg+"</label>")
                        })
                    }
                    })
        },
    rules:{
        subject:{
           // alphanumericspecialchar:true,
            required:true,
            minlength:2,
            maxlength:50
        },
        transaction_id:{
            digits:true
        },
        case_type:{
            required:true
        },
        message:{
            required:true,
           // alphanumericspecialchar:true,
            minlength:2,
            maxlength:2000
        }
    },
    messages:{
        subject:{
            required:SUBJECT_EMPTY
        },
        case_type:{
            required:CASE_TYPE_EMPTY
        },
        message:{
            required:MESSAGE_EMPTY
        },
        transaction_id:{
            digits:CASE_DURATION_NUMERIC
        }
    }
});
$("#case_reply").validate({
    submitHandler:function(){
        var dataStr=$("#case_reply").serialize();
        showAjxLoader();
        $.ajax({
            type:"POST",
            url:"/view-user-case",
            data:dataStr+'&case_status='+$.trim($('#case_status').val()),
            success:function(responseData){
                hideAjxLoader();
                var jsonObj=jQuery.parseJSON(responseData);
                if(jsonObj.status=="success"){
                    $("#replydiv").prepend(jsonObj.message);
                    $("#reply").val("");   
                    $("#reply_count").html('0');
                    // - start
                    if(jsonObj.case_msg_disp != undefined && jsonObj.case_msg_disp != "" && $.trim(jsonObj.case_msg_disp) == "2") {
                        $('#case_status').val('7');
                        $('#case_status_text').html('Re-Open');
                        $('#case_status_div').html('<input type="button" class="button" id="close_case" value="Close Case">');
                        parent.jQuery("#tab-9").click();
                    }
                    // - end                    
                    }else $.each(jsonObj.message,function(i,msg){
                    $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                    })
                }
                })
    },
rules:{
    reply:{
        required:true,
        maxlength:1E3
    }
},
messages:{
    reply:{
        required:MESSAGE_EMPTY
    }
}
});
$("#update_edit_usr_case").click(function(){
    var transactionId=$("#transaction_id").val();
    if(transactionId==""){
        $("#transaction_id").addClass("error");
        $("#transaction_label").show();
        $("#transaction_label").html(EMPTY_TRANSACTION)
        }else{
        $("#transaction_id").removeClass("error");
        $("#transaction_label").hide();
        $("#transaction_label").html("");
        $.ajax({
            type:"POST",
            url:"/edit-user-case/"+$("#case_contact_id").val(),
            data:{
                transaction_id:transactionId
            },
            success:function(responseData){
                hideAjxLoader();
                var jsonObj=jQuery.parseJSON(responseData);
                if(jsonObj.status=="success"){
                    parent.jQuery("#tab-9").click();
                    parent.jQuery.colorbox.close()
                    }else{
                    $("#transaction_id").addClass("error");
                    $("#transaction_label").show();
                    $("#transaction_label").html(jsonObj.message)
                    }
                }
        })
}
});

$(document).delegate("#close_case","click",function(){ //$("#close_case").click(function(){
    parent.$("#cboxClose").hide();
    $.colorbox({
        width:"600px",
        href:"#close_case_content",
        height:"250px",
        inline:true
    });
    $("#no_delete").click(function(){
        parent.$("#cboxClose").show();
        $.colorbox.close();
        $("#yes_delete").unbind("click");
        return false
        });
    $("#yes_delete").click(function(){
        $("#yes_delete").unbind("click");
        $.ajax({
            type:"POST",
            url:"/edit-user-case/"+$("#case_contact_id").val(),
            data:{
                case_status:6
            },
            success:function(responseData){
                hideAjxLoader();
                var jsonObj=jQuery.parseJSON(responseData);
                if(jsonObj.status=="success"){
                    parent.jQuery("#tab-9").click();
                    parent.jQuery.colorbox.close()
                    }else;
            }
        })
    })
});
$("#reopen_case").click(function(){
    $.ajax({
        type:"POST",
        url:"/edit-user-case/"+$("#case_contact_id").val(),
        data:{
            case_status:7
        },
        success:function(responseData){
            hideAjxLoader();
            var jsonObj=jQuery.parseJSON(responseData);
            if(jsonObj.status=="success"){
                parent.jQuery("#tab-9").click();
                parent.jQuery.colorbox.close()
                }else;
        }
    })
})
});
