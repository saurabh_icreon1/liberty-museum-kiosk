$(function(){
    if(mode!="edit"){
        var dt=new Date;
        var time=dt.getHours()+":"+dt.getMinutes();
        $("#start_date").datepicker({
            changeMonth:true,
            changeYear:true,
            showOtherMonths:true
        });
			if(mode!='view'){
				$("#start_time").timepicker({
					"timeFormat":"h:i A"
				}).timepicker();
			}else{
				$("#start_time").timepicker({
					"timeFormat":"h:i A"
				});
			}
        }else{
        $("#start_date").datepicker({
            changeMonth:true,
            changeYear:true,
            showOtherMonths:true
        });
        $("#start_time").timepicker({
            "timeFormat":"h:i A"
        })
        }
        $("#case_type").change(function(){
        $("div[class^='case_type']").hide();
        if($(".case_type"+
            this.value))$(".case_type"+this.value).show()
            });
    if($(".case_type"+$("#case_type").val()))$(".case_type"+$("#case_type").val()).show();
    $("#case_status").change(function(){
        $("div[class^='case_status']").hide();
        if($(".case_status"+this.value))$(".case_status"+this.value).show()
            });
    if($(".case_status"+$("#case_status").val()))$(".case_status"+$("#case_status").val()).show();
    jQuery.validator.addMethod("alphanumericspecialchar",function(value,element){
        return this.optional(element)||/^[A-Za-z0-9\n\r ,.'"$#@!%&*()-]+$/i.test(value)},
    ALPHA_NUMERIC_SPECIALSYMPOL);
    jQuery.validator.addMethod("alphanumerichypenchar",function(value,element){
        return this.optional(element)||/^[A-Za-z0-9\n\r'` -]+$/i.test(value)
        },ALPHA_NUMERIC_SPECIALSYMPOL);
    $("#create_contact").validate({
        submitHandler:function(){
            var dataStr=$("#create_contact").serialize();
            showAjxLoader();
            $.ajax({
                type:"POST",
                url:"/add-case",
                data:dataStr,
                success:function(responseData){
                    if(!checkUserAuthenticationAjx(responseData))return false;
                    hideAjxLoader();
                    var jsonObj=jQuery.parseJSON(responseData);
                    if(jsonObj.status=="success")window.location.href="/cases/";else $.each(jsonObj.message,function(i,msg){
                        $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                        })
                    }
                    })
        },
    rules:{
        first_name:{
            required:true
        },
        last_name:{
            required:true
        },
        email_id:{
            required:EMAIL_EMPTY,
            email:true
        }
    },
    messages:{
        first_name:{
            required:FIRST_NAME_EMPTY
        },
        last_name:{
            required:LAST_NAME_EMPTY
        },
        email_id:{
            required:EMAIL_EMPTY,
            email:EMAIL_VALID
        }
    }
});

$('#message').limiter(1000, $("#message_count"));

$("#case").validate({
    submitHandler:function(){
        var dataStr=$("#case").serialize();
        var url = "/create-case";
        if ($("#caseOfTransaction").length != 0)
        {
            url = "/create-transaction-case";
        }
        showAjxLoader();
        
        $.ajax({
            type:"POST",
            url:url,
            data:dataStr,
            success:function(responseData){
                hideAjxLoader();
                if(!checkUserAuthenticationAjx(responseData))return false;
                var jsonObj=jQuery.parseJSON(responseData);
                if(jsonObj.status=="success")
                {
                    if ($("#caseOfTransaction").length != 0)
                    {
                        parent.$.fn.colorbox.close();
                        parent.$("#tab-1").click();
                    }
                    else
                    {
                         window.location.href="/cases";
                    }
                   
                }
                else $.each(jsonObj.message,function(i,msg){
                    $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                    })
                }
                })
    },
rules:{
    contact_name:{
        required:true
    },
    message:{
        required:true,
      //  alphanumericspecialchar:true,
        minlength:2,
        maxlength:1000
    },
    subject:{
      // alphanumericspecialchar:true,
        required:true,
        minlength:2,
        maxlength:50
    },
    assigned_to:{
       // required:true,
        alphanumerichypenchar:true
    },
    case_type:{
        required:true
    },
    case_status:{
        required:true
    },
    reason:{
        alphanumericspecialchar:true,
        minlength:2,
        maxlength:100
    },
    duration:{
        digits:true,
        minlength:1,
        maxlength:4
    },
    start_time:{
        required:function(){
            if($("#start_date").val()!="")return true;
            return false
            }
        }
},
messages:{
    contact_name:{
        required:CONTACT_NAME_EMPTY
    },
    message:{
        required:MESSAGE_EMPTY
    },
    subject:{
        required:SUBJECT_EMPTY
    },
    assigned_to:{
        required:ASSIGNED_TO_EMPTY
    },
    case_type:{
        required:CASE_TYPE_EMPTY
    },
    case_status:{
        required:CASE_STATUS_EMPTY
    },
    duration:{
        digits:CASE_DURATION_NUMERIC
    },
    start_time:{
        required:START_DATE_EMPTY
    }
}
});
$("#contact_name").autocomplete({
    afterAdd:true,
    selectFirst:true,
    autoFocus:true,
    source:function(request,response){
        $.ajax({
            url:"/get-contacts",
            method:"POST",
            dataType:"json",
            data:{
                user_email:$("#contact_name").val()
                },
            success:function(jsonResult){
                if(!checkUserAuthenticationAjx(jsonResult))return false;
                var resultset=
                [];
                $.each(jsonResult,function(){
                    var id=this.user_id;
                    var value=this.full_name;
                    resultset.push({
                        id:this.user_id,
                        value:this.full_name
                        })
                    });
                response(resultset)
                }
            })
    },
open:function(){
    $(".ui-menu").width(350)
    },
change:function(event,ui){
    if(ui.item){
        $("#contact_name").val(ui.item.value);
        $("#contact_name_id").val(ui.item.id);
        if($("#case_contact_id").val()!="")$.ajax({
            url:"/get-user-transaction",
            method:"POST",
            data:{
                contact_name_id:$("#contact_name_id").val()
                },
            success:function(result){
                if(!checkUserAuthenticationAjx(result))return false;
                $("#transaction_id option").each(function(){
                    $(this).remove()
                    });
                $("#transaction_id").append(result);
                /*bug fixed*/
                //$(".e1").select2()
                $("select.form-select").select2();
                $('.select2-container').select2('val', '');
                }
            })
        }else{
    $("#contact_name").val("");
    $("#contact_name_id").val("")
    }
},
focus:function(event,ui){
    $("#contact_name_id").val(ui.item.id);
    return false
    },
select:function(event,ui){
    $("#contact_name").val(ui.item.value);
    $("#contact_name_id").val(ui.item.id);
    return false
    }
});
$("#assigned_to").autocomplete({
    afterAdd:true,
    selectFirst:true,
    autoFocus:true,
    source:function(request,response){
        $.ajax({
            url:"/get-crm-contacts",
            method:"POST",
            dataType:"json",
            data:{
                crm_full_name:$("#assigned_to").val()
                },
            success:function(jsonResult){
                if(!checkUserAuthenticationAjx(jsonResult))return false;
                var resultset=[];
                $.each(jsonResult,function(){
                    var id=this.crm_user_id;
                    var value=this.crm_full_name;
                    resultset.push({
                        id:this.crm_user_id,
                        value:this.crm_full_name
                        })
                    });
                response(resultset)
                }
            })
    },
open:function(){
    $(".ui-menu").width(350)
    },
change:function(event,ui){
    if(ui.item){
        $("#assigned_to").val(ui.item.value);
        $("#assigned_to_id").val(ui.item.id)
        }else{
        $("#assigned_to").val("");
        $("#assigned_to_id").val("")
        }
        $(".ui-autocomplete").jScrollPane()
    },
focus:function(event,ui){
    $("#assigned_to_id").val(ui.item.id);
    return false
    },
select:function(event,ui){
    $("#assigned_to").val(ui.item.value);
    $("#assigned_to_id").val(ui.item.id);
    return false
    }
});
$(document).on("click","#add_new_file",function(){
    showAjxLoader();
    var i=$("#upload_count_div").text();
    var allDivBlock=$("div[id^='upload_container_']");
    if(allDivBlock.length==1){
        var parId=$(this).parents("div[id^='upload_container_']").attr("id");
        var id=parId.substring(parId.lastIndexOf("_")+1,parId.length);
        $(this).parent().append("<a  class='minus m-l-5' onclick=remove_dynamic_contacts('.upload_container_"+id+"','"+id+"')>")
        }
        $(this).remove();
    if(!window.location.origin)window.location.origin=window.location.protocol+"//"+window.location.hostname+(window.location.port?":"+window.location.port:"");
    $("<div />",{
        "class":"row no-padd upload_container_"+i,
        "id":"upload_container_"+i
        }).appendTo("#load_upload");
    $(".upload_container_"+i).load(window.location.origin+
        "/upload-file/"+i+"/upload_container_");
    i++;
    hideAjxLoader();
    $("#upload_count_div").html(i);
    $("#publicdocs_upload_count").val(i)
    });
$(".fileUploadAddMore").fileupload({
    url:"/upload-user-public-doc",
    dataType:"json",
    done:function(e,data){
        var file_id=$(this).attr("id");
        var file_number=file_id.substr(file_id.length-1,1);
        data=data.result;
        if(data.status=="success"){
            $("#publicdocs_upload_count").val(parseInt(file_number)+1);
            $("#filemessage_"+file_number).addClass("success-msg");
            $("#filemessage_"+file_number).html(data.message);
            $("#filemessage_"+file_number).removeClass("error-msg");
            $("#publicdocs_upload_file"+file_number).val(data.filename)
            }else{
            $("#filemessage_"+file_number).removeClass("success-msg");
            $("#filemessage_"+file_number).addClass("error-msg");
            $("#filemessage_"+file_number).html(data.message);
            $("#publicdocs_upload_file"+file_number).val("")
            }
        },
start:function(e){
    showAjxLoader()
    },
stop:function(e){
    hideAjxLoader()
    }
})
});
function showReason(id){
    if($("#"+id+" option:selected ").text()==L_CANOT_REASON)$("#reason_div").show();else $("#reason_div").hide()
        }
function remove_dynamic_contacts(classname,i,userType,sectionName){
    var plusEle=$(classname).find(".plus");
    if(i!=0)$(classname).remove();
    else if(i==0){
        $(classname).next().next().remove();
        $("#filemessage_0").removeClass("success-msg");
        $("#filemessage_0").html("")
        }
        var classDivId=classname.substring(1,classname.lastIndexOf("_")+1);
    var lastBlock=$("div[id^='"+classDivId+"']").last();
    if($(lastBlock).find(".plus").length==0){
        var plusId=$(plusEle).attr("id");
        if(!plusId)plusId="add_new_file";
        $(lastBlock).find(".addMinus").append('<a href="javascript:void(0);" id="'+
            plusId+'" class="plus m-l-5"></a>')
        }
        var allDivBlock=$("div[id^='"+classDivId+"']");
    if(allDivBlock.length==1)$(lastBlock).find(".minus").remove()
        }
function removePublicDoc(case_attachment_id,file_name,case_contact_id,key){
    $.colorbox({
        width:"500px",
        href:"#delete_contact_doc",
        height:"200px",
        inline:true
    });
    $("#no_delete").click(function(){
        $.colorbox.close();
        $("#yes_delete").unbind("click");
        return false
        });
    $("#yes_delete").click(function(){
        $("#yes_delete").unbind("click");
        $.ajax({
            type:"POST",
            data:{
                case_contact_id:case_contact_id,
                case_attachment_id:case_attachment_id,
                file_name:file_name
            },
            url:"/delete-case-file",
            success:function(response){
                $.colorbox.close();
                $("#doc_"+key).hide()
                }
            })
    })
};
