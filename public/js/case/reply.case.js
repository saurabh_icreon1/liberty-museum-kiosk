$(function(){
    $(".postreply").click(function(){
        var click_id=$(this).attr("id");
        if(click_id=="tab-reply"){
            $("#tab-note").removeClass("active");
            $(".div-tab-note").css("display","none");
            $(".div-tab-reply").css("display","block")
            }else{
            $("#tab-reply").removeClass("active");
            $(".div-tab-note").css("display","block");
            $(".div-tab-reply").css("display","none")
            }
            $(this).addClass("active")
        });
        
    $('#reply').limiter(1000, $("#reply_count"));
    $('#note').limiter(1000, $("#note_count"));

    $("#case_reply").validate({
        submitHandler:function(){
            var dataStr=$("#case_reply").serialize();
            showAjxLoader();
            var replydivClass=
            "";
            if($("#replydiv div").first().hasClass("alterbg-none"))replydivClass="alterbg";else replydivClass="alterbg alterbg-none";
            dataStr=dataStr+"&replydivClass="+replydivClass;
            $.ajax({
                type:"POST",
                url:"/get-case-detail",
                data:dataStr+'&case_status='+$.trim($('#case_status_vals').val()),
                success:function(responseData){
                    hideAjxLoader();
                    if(!checkUserAuthenticationAjx(responseData))return false;
                    var jsonObj=jQuery.parseJSON(responseData);
                    if(jsonObj.status=="success"){
                        $("#replydiv").prepend(jsonObj.message);
                        $("#reply").val("");
                        $("#reply_count").html('0');
                        // - start
                        /*if(jsonObj.case_msg_disp != undefined && jsonObj.case_msg_disp != "" && $.trim(jsonObj.case_msg_disp) == "2") {
                            $('#case_status_vals').val('7');
                            $('#case_status_txt').html('Re-Open');
                        }*/
                        // - end

						getCaseDetail();
                        
                        }else $.each(jsonObj.message,function(i,
                        msg){
                        $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                        })
                    }
                    })
        },
    rules:{
        reply:{
            required:true,
            maxlength:1000
        }
    },
    messages:{
        reply:{
            required:MESSAGE_EMPTY
        }
    }
});
$("#case_note").validate({
    submitHandler:function(){
        var dataStr=$("#case_note").serialize();
        showAjxLoader();
        var replydivClass="";
        if($("#notesdiv div").first().hasClass("alterbg-none"))replydivClass="alterbg";else replydivClass="alterbg alterbg-none";
        dataStr=dataStr+"&replydivClass="+replydivClass;
        $.ajax({
            type:"POST",
            url:"/get-case-detail",
            data:dataStr,
            success:function(responseData){
                hideAjxLoader();
                if(!checkUserAuthenticationAjx(responseData))return false;
                var jsonObj=jQuery.parseJSON(responseData);
                if(jsonObj.status=="success"){
                    $("#notesdiv").prepend(jsonObj.message);
                    $("#note").val("");
                    $("#note_count").html('0');
					getCaseDetail();
					$('.note-tab #tab-reply').removeClass('postreply');
					$('.note-tab #tab-note').attr('class','active postreply');
                    }else $.each(jsonObj.message,function(i,msg){
                    $("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")
                    })
                }
                })
    },
rules:{
    note:{
        required:true,
        maxlength:1000
    }
},
messages:{
    note:{
        required:MESSAGE_EMPTY
    }
}
})
});
