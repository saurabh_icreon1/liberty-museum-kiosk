$(function(){var emptyMsgDiv=$('<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>"),grid=jQuery("#list_pledge");$(
"#list_pledge").jqGrid({postData:{searchString:$("#search_pledge").serialize()},mtype:"POST",url:"/pledges",datatype:
"json",sortable:true,colNames:["Pledge #","Name","Total Amount","Balance Amount","Contribution Type","Date","Status",
"Action(s)"],colModel:[{name:"Pledge #",index:"pledge_id"},{name:"Name",index:"tbl_usr.first_name"},{name:"Total Amount",index:
"tbl_pledge.pledge_amount"},{name:"Balance Amount",index:"",sortable:false},{name:"Pledge for",index:
"tbl_pledge.contribution_type_id"},{name:"Date",index:"tbl_pledge.added_date"},{name:"Status Name",index:
"pledge_status_id"},{name:"Action",sortable:false,cmTemplate:{title:false}}],viewrecords:true,sortname:"modified_date",
sortorder:"desc",rowNum:10,rowList:[10,20,30],pager:"#listpledgerud",viewrecords:true,autowidth:true,shrinkToFit:true,
caption:"",width:"100%",cmTemplate:{title:false},loadComplete:function(){var count=grid.getGridParam(),ts=grid[0];if(ts.
p.reccount===0){grid.hide();emptyMsgDiv.show();$("#pcrud div.ui-paging-info").css("display","none")}else{grid.show();
emptyMsgDiv.hide();$("#pcrud div.ui-paging-info").css("display","block")}}});emptyMsgDiv.insertAfter(grid.parent());
emptyMsgDiv.hide();$("#list_pledge").jqGrid("navGrid","#listpledgerud",{reload:true,edit:false,add:false,search:false,
del:false});$("#list_pledge").jqGrid("navButtonAdd","#listpledgerud",{caption:"",title:"Export",id:"exportExcel",
onClickButton:function(){exportExcel("list_pledge","/pledges")},position:"last"});$("#searchpledgebutton").click(
function(){showAjxLoader();$("#success_message").hide();$("#success_message").html("");jQuery("#list_pledge").jqGrid(
"setGridParam",{postData:{searchString:$("#search_pledge").serialize()}});jQuery("#list_pledge").trigger("reloadGrid");
window.location.hash="#saved_search";hideAjxLoader();return false});$("#pledge_status_id").change(function(){$(
"div[class^='pledge_status_id']").hide();if($(".pledge_status_id"+this.value))$(".pledge_status_id"+this.value).show()})
;if($(".pledge_status_id"+$("#pledge_status_id").val()))$(".pledge_status_id"+$("#pledge_status_id").val()).show();$(
"#savebutton").click(function(){if($.trim($("#search_name").val())!=""){$("#search_msg").html("");$("#search_msg").css(
"display","none");showAjxLoader();$.ajax({type:"POST",data:{search_name:$.trim($("#search_name").val()),searchString:$(
"#search_pledge").serialize(),search_id:$("#search_id").val()},url:"/add-crm-search-pledges",success:function(response){
if(!checkUserAuthenticationAjx(response))return false;var jsonObj=jQuery.parseJSON(response);if(jsonObj.status==
"success"){hideAjxLoader();$("#search_name").val("");document.getElementById("search_pledge").reset();location.reload()}
else{$("#search_msg").html(ERROR_SAVING_SEARCH);$("#search_msg").addClass("error");$("#search_msg").removeClass(
"success-msg clear");$("#search_msg").css("display","block")}}})}else{$("#search_msg").html(SEARCH_NAME_MSG);$(
"#search_msg").addClass("error");$("#search_msg").removeClass("success-msg clear");$("#search_msg").css("display",
"block");return false}});function generateSavedSearch(){$("#saved_search option").each(function(){$(this).remove()});$.
ajax({type:"POST",data:{},url:"/get-pledge-saved-search-list",success:function(response){if(!checkUserAuthenticationAjx(
response))return false;var jsonObj=jQuery.parseJSON(response);jQuery.each(jsonObj,function(key,value){$("#saved_search")
.append("<option value='"+key+"'>"+value+"</option>")})}})}});function getSavedPledgeSearchResult(){$("#search_msg").css
("display","none");$("#search_msg").html("");if($("#saved_search").val()!=""){$("#savebutton").val("Update");$(
"#deletebutton").show()}else{$("#savebutton").val("Save");$("#search_name").val("");$("#deletebutton").hide()}if($(
"#saved_search").val()!="")$.ajax({type:"POST",data:{search_id:$("#saved_search").val()},url:
"/get-pledge-search-saved-param",success:function(response){if(!checkUserAuthenticationAjx(response))return false;var 
searchParamArray=response,obj=jQuery.parseJSON(searchParamArray);document.getElementById("search_pledge").reset();$(
"#contact_name").val(obj.contact_name);$("#pledge_id").val(obj.pledge_id);$("#pledge_amount_from").val(obj.
pledge_amount_from);$("#pledge_amount_to").val(obj.pledge_amount_to);$("#payment_scheduled").val(obj.payment_scheduled).
select2();$("#pledge_status_id").val(obj.pledge_status_id).select2();$("div[class^='pledge_status_id']").hide();if($(
".pledge_status_id"+$("#pledge_status_id").val()))$(".pledge_status_id"+$("#pledge_status_id").val()).show();$(
"#search_name").val(obj.search_title);$("#search_id").val($("#saved_search").val());$("#searchpledgebutton").click()}})}
function deleteSaveSearch(){$(".delete_saved_search").colorbox({width:"700px",height:"180px",inline:true});$(
"#no_delete").click(function(){$.colorbox.close();$("#yes_delete").unbind("click");return false});$("#yes_delete").click
(function(){$("#yes_delete").unbind("click");$.ajax({type:"POST",data:{search_id:$("#search_id").val()},url:
"/delete-pledge-search",success:function(response){if(!checkUserAuthenticationAjx(response))return false;var jsonObj=
jQuery.parseJSON(response);if(jsonObj.status=="success")location.reload()}})})}function deletePledge(pledgeId){$(
".delete_pledge").colorbox({width:"700px",height:"180px",inline:true});$("#no_delete_pledge").click(function(){$.
colorbox.close();$("#yes_delete_pledge").unbind("click");return false});$("#yes_delete_pledge").click(function(){$(
"#yes_delete_pledge").unbind("click");$.ajax({type:"POST",dataType:"html",data:{pledgeId:pledgeId},url:"/delete-pledge",
success:function(responseData){if(!checkUserAuthenticationAjx(responseData))return false;var jsonObj=JSON.parse(
responseData);if(jsonObj.status=="success")window.location.href="/pledges";else $("#error_message").html(jsonObj.message
)}})})}function cancelPledgeStatus(pledgeId){$(".cancel_pledge_status").colorbox({width:"700px",height:"180px",inline:
true});$("#no_cancel").click(function(){$.colorbox.close();$("#yes_cancel").unbind("click");return false});$(
"#yes_cancel").click(function(){$("#yes_cancel").unbind("click");$.colorbox.close();$.ajax({type:"POST",dataType:"html",
data:{pledgeId:pledgeId,pledgeStatus:3},url:"/change-pledge-status",success:function(responseData){if(!
checkUserAuthenticationAjx(responseData))return false;var jsonObj=JSON.parse(responseData);if(jsonObj.status=="success")
window.location.href="/pledges";else $("#error_message").html(jsonObj.message)}})})}