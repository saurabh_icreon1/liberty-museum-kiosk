$(function() {
    if (mode == "view")
        getPledgeDetail();
    else
        editPledgeDetails();
    $("a[id^='tab-']").removeClass("active");
    var
            tab_id_load = mode == "view" ? "1" : "2";
    $("#tab-" + tab_id_load).addClass("active");
    $("a[id^='tab-']").click(function() {
        $(
                "a[id^='tab-']").removeClass("active");
        $(this).addClass("active")
    })
});
function getPledgeDetail() {
    showAjxLoader();
    $.ajax(
            {type: "POST", data: {}, url: "/view-pledge-detail/" + encrypt_pledge_id, success: function(response) {
                    if (!
                            checkUserAuthenticationAjx(response))
                        return false;
                    $("#pledgeContent").html(response);
                    hideAjxLoader()
                }})
}
function
        editPledgeDetails() {
    showAjxLoader();
    $.ajax({type: "GET", data: {}, url: "/edit-pledge/" + encrypt_pledge_id, success: function(
                response) {
            if (!checkUserAuthenticationAjx(response))
                return false;
            $("#pledgeContent").html(response);
            hideAjxLoader()
        }})
}
function viewChangeLog() {
    showAjxLoader();
    $.ajax({type: "POST", data: {module: module_name, id: encrypt_pledge_id}, url:
                "/pledge-change-log/" + module_name + "/" + encrypt_pledge_id, success: function(response) {
                    hideAjxLoader();
                    if (!
                            checkUserAuthenticationAjx(response))
                        return false;
                    $("#pledgeContent").html(response)
                }})
}
function getPledgeTransactions()
{
    showAjxLoader();
    $.ajax({type: "GET", data: {}, url: "/get-pledge-transactions/" + encrypt_pledge_id, success: function(response)
        {
            hideAjxLoader();
            if (!checkUserAuthenticationAjx(response))
                return false;
            $("#pledgeContent").html(response)
        }})
}
function
        getContactActivityDetail() {
    showAjxLoader();
    $.ajax({type: "POST", data: {userid: encrypted_contact_name_id, sourceid:
            encrypted_source_type_id, sourceactivityid: encrypt_pledge_id}, url: "/pledge-activities", success: function(response) {
            if (!
                    checkUserAuthenticationAjx(response))
                return false;
            hideAjxLoader();
            $("#pledgeContent").html(response)
        }})
}