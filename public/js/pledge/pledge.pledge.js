var emiFlag=false;function checkValidData(id,val){$("#num_installment").next().remove("label");if($(
"#num_installment_hidden").val()!=$("#num_installment").val()||$("#installment_amount_hidden").val()!=$(
"#installment_amount").val()){var amount=0;if(typeof id=="undefined"||typeof val=="undefined")emiFlag=true;else if($("#"
+id+"_hidden").val()!=val&&paidPledgeAmount>0){$("#is_trans_update").val(1);if(id=="num_installment"||id==
"installment_amount")emiFlag=true;else emiFlag=false}else emiFlag=true;var isChange=$("#is_trans_update").val();if(
pledge_id!="")amount=$("#balance_amount").val();else amount=$("#pledge_amount").val();var num_installment=$(
"#num_installment").val(),installment_amount=$("#installment_amount").val();if(amount!=""&&num_installment!=""&&
installment_amount!=""){var calculatedAmount=eval(num_installment*installment_amount),errorMsg;if(pledge_id)errorMsg=
PLEDGE_EDIT_ERROR;else errorMsg=PLEDGE_DATA_ERROR;if(calculatedAmount!=amount){$("#num_installment").after(
'<label for="num_installment" generated="true" class="error"><span class="redarrow"></span>'+errorMsg+"</label>");
return false}}return true}else return true}function checkPaidData(val){if(paidPledgeAmount>0&&val<=paidPledgeAmount)$(
"#pledge_amount").after('<label for="num_installment" generated="true" class="error"><span class="redarrow"></span>'+
PLEDGE_DATA_ERROR_COMPLETE+"</label>");else $("#pledge_amount").next().remove("label")}function showHonoreeName(obj){var
 in_honoree=obj.value;switch(in_honoree){case"1":$("#honoree_name_block").show();$("#honoree_name_block").css(
"padding-top","0px");break;case"2":$("#honoree_name_block").show();$("#honoree_name_block").css("padding-top","20px");
break;case"3":$("#honoree_name_block").hide();break}}function checkReminder(){var checked=$("#is_reminder").is(
":checked");return checked}function checkHonoreeDetail(){var val=$(".honoree:checked").val();if(val==1||val==2)
return true;else return false}$(function(){if(paidPledgeAmount==0){$("#payment_start_date").datepicker({changeMonth:true
,changeYear:true,showOtherMonths:true});$("#pledge_made_date").datepicker({changeMonth:true,changeYear:true,
showOtherMonths:true});$("#acknowledgment_date").datepicker({changeMonth:true,changeYear:true,showOtherMonths:true})}$(
"#contribution_type_id").change(function(){$("div[class^='contribution_type_id']").hide();if($(".contribution_type_id"+
this.value))$(".contribution_type_id"+this.value).show()});if($(".contribution_type_id"+$("#contribution_type_id").val()
))$(".contribution_type_id"+$("#contribution_type_id").val()).show();if(paidPledgeAmount>0){$("#payment_start_date").
attr("readonly","readonly");$("#payment_start_date").removeClass("hasDatepicker");$("#pledge_made_date").attr("readonly"
,"readonly");$("#pledge_made_date").removeClass("hasDatepicker");$("#acknowledgment_date").attr("readonly","readonly");$
("#acknowledgment_date").removeClass("hasDatepicker");$("#pledge_amount").attr("readonly","readonly");var hidVal2=$(
"#installment_type_id").val();$("#installment_type_id").after(
'<input type="hidden" name="installment_type_id_hid" value="'+hidVal2+'" >')}$(".e1").select2({});$(".e2").customInput()
;$(".e3").each(function(i){if($(this).is("[type=checkbox],[type=radio]")){var input=$(this),input_class=input.attr(
"class").split(" ");input.attr({id:input_class["1"]+"_"+i});var label=$("label[for="+input.attr("id")+"]"),outerLabel=
input.parent(),outerLabelParent=input.parent().parent(),inputType=input.is("[type=checkbox]")?"checkbox":"radio";if(
inputType=="radio"){var oDiv=$(document.createElement("label")).attr({"for":input_class["1"]+"_"+i}).html($(outerLabel).
text());$(outerLabel).remove();outerLabelParent.append(input,oDiv)}}});$(".e3").customInput();$("#reminder_details").
hide();if($("#installment_type_id").val()==1){$("#payment_due_days").val("");$("#payment_due_day").hide()}$(
"#contact_name").autocomplete({afterAdd:true,selectFirst:true,autoFocus:true,source:function(request,response){$.ajax({
url:"/get-contacts",method:"POST",dataType:"json",data:{user_email:$("#contact_name").val()},success:function(jsonResult
){var resultset=[];$.each(jsonResult,function(){var id=this.user_id,value;if(this.email_id!="")value=this.full_name+" ("
+this.email_id+")";else value=this.full_name;var value1=this.full_name;resultset.push({id:this.user_id,value:value,
value1:value1})});response(resultset)}})},open:function(){$(".ui-menu").width(350)},change:function(event,ui){if(ui.item
){$("#contact_name").val(ui.item.value1);$("#contact_name_id").val(ui.item.id)}else{$("#contact_name").val("");$(
"#contact_name_id").val("")}},focus:function(event,ui){$("#contact_name_id").val(ui.item.id);return false},select:
function(event,ui){$("#contact_name").val(ui.item.value1);$("#contact_name_id").val(ui.item.id);var pledgeId=typeof $(
"#pledge_id").val()!="undefined"?$("#pledge_id").val():"";if($("#contact_name").next().attr("class")=="error")$(
"#contact_name").next().remove();$.ajax({url:"/check-user-active-pledge",method:"POST",data:{userId:$("#contact_name_id"
).val(),pledgeId:pledgeId},success:function(result){if(!checkUserAuthenticationAjx(result))return false;var jsonObj=
jQuery.parseJSON(result);if(jsonObj.status=="success"){}else{$.each(jsonObj.message,function(i,msg){$("#"+i).after(
'<label class="error" style="display:block;">'+msg+"</label>")});$("#contact_name").val("");$("#contact_name_id").val(""
)}}})}});$("#assigned_to").autocomplete({afterAdd:true,selectFirst:true,autoFocus:true,source:function(request,response)
{$.ajax({url:"/get-crm-contacts",method:"POST",dataType:"json",data:{crm_full_name:$("#assigned_to").val()},success:
function(jsonResult){var resultset=[];$.each(jsonResult,function(){var id=this.crm_user_id,value=this.crm_full_name;
resultset.push({id:this.crm_user_id,value:this.crm_full_name})});response(resultset)}})},open:function(){$(".ui-menu").
width(350)},change:function(event,ui){if(ui.item){$("#assigned_to").val(ui.item.value);$("#assigned_to_id").val(ui.item.
id)}else{$("#assigned_to").val("");$("#assigned_to_id").val("")}$(".ui-autocomplete").jScrollPane()},focus:function(
event,ui){$("#assigned_to_id").val(ui.item.id);return false},select:function(event,ui){$("#assigned_to").val(ui.item.
value);$("#assigned_to_id").val(ui.item.id);return false}});$("#campaign_name").autocomplete({afterAdd:true,selectFirst:
true,autoFocus:true,source:function(request,response){$.ajax({url:"/get-campaigns",method:"POST",dataType:"json",data:{
campaign_name:$("#campaign_name").val()},success:function(jsonResult){var resultset=[];$.each(jsonResult,function(){var 
id=this.campaign_id,value=this.campaign_title;resultset.push({id:this.campaign_id,value:this.campaign_title})});response
(resultset)}})},open:function(){$(".ui-menu").width(350)},change:function(event,ui){if(ui.item){$("#campaign_name").val(
ui.item.value);$("#campaign_id").val(ui.item.id)}else{$("#campaign_name").val("");$("#campaign_id").val("")}$(
".ui-autocomplete").jScrollPane()},focus:function(event,ui){$("#campaign_id").val(ui.item.id);return false},select:
function(event,ui){$("#campaign_name").val(ui.item.value);$("#campaign_id").val(ui.item.id);return false}});$(
"#is_reminder").click(function(){var checked=$("#is_reminder").is(":checked");if(checked==true)$("#reminder_details").
show();else if(checked==false)$("#reminder_details").hide()});$("#installment_type_id").click(function(){var value=$(
"#installment_type_id").val();if(value==1)$("#payment_due_day").hide();else if(value==3||value==4||value==2)$(
"#payment_due_day").show()});jQuery.validator.addMethod("numericcommachar",function(value,element){return this.optional(
element)||/^[0-9 ,.]+$/i.test(value)},VALID_AMOUNT);jQuery.validator.addMethod("numericonly",function(value,element){
return this.optional(element)||/^[0-9]+$/i.test(value)},VALID_NUMBER);jQuery.validator.addMethod(
"initialReminderValidate",function(value,element){if($("#is_reminder").is(":checked")){if($("#reminder_days").val()!=""
&&(this.optional(element)||/^[0-9]+$/i.test(value)))return true;else return false;return false}else return true},
VALID_NUMBER);jQuery.validator.addMethod("sendUpToReminderValidate",function(value,element){if($("#is_reminder").is(
":checked")){if($("#num_reminder").val()!=""&&(this.optional(element)||/^[0-9]+$/i.test(value)))return true;else
 return false;return false}else return true},VALID_NUMBER);jQuery.validator.addMethod("additionalReminderValidate",
function(value,element){if($("#is_reminder").is(":checked")){if($("#additional_reminder_days").val()!=""&&(this.optional
(element)||/^[0-9]+$/i.test(value)))return true;else return false;return false}else return true},VALID_NUMBER);jQuery.
validator.addMethod("honoreeNameValidate",function(value,element){if($(".honoree:checked").val()==1||$(
".honoree:checked").val()==2)if($("#honoree_name").val()!="")return true;else return false;else return true},
HONOREE_NAME_EMPTY);jQuery.validator.addMethod("paymentDueDays",function(value,element){var valueInstallMentType=$(
"#installment_type_id").val(),valuePaymentDueDyas=$("#payment_due_days").val();if((valueInstallMentType==3||
valueInstallMentType==4||valueInstallMentType==2)&&valuePaymentDueDyas=="")return false;else return true},
PAYMENT_DUE_DATE_EMPTY);$("#save").click(function(){$("#create-Pledge").validate({submitHandler:function(){if(
checkValidData()){var dataStr=$("#create-Pledge").serialize();showAjxLoader();$.ajax({type:"POST",url:"/create-pledge",
data:dataStr,success:function(responseData){hideAjxLoader();if(!checkUserAuthenticationAjx(responseData))return false;
var jsonObj=jQuery.parseJSON(responseData);if(jsonObj.status=="success")window.location.href="/pledges";else $.each(
jsonObj.message,function(i,msg){$("#"+i).after('<label class="error" style="display:block;">'+msg+"</label>")})}})}},
rules:{contact_name:{required:true},pledge_amount:{required:true},num_installment:{required:true,numericonly:true},
installment_amount:{required:true,numericcommachar:true},installment_in:{required:true,numericonly:true},
installment_type_id:{required:true},payment_due_days:{paymentDueDays:true,numericonly:true},pledge_made_date:{required:
true},payment_start_date:{required:true},honoree_name:{honoreeNameValidate:true},reminder_days:{initialReminderValidate:
true},num_reminder:{sendUpToReminderValidate:true},additional_reminder_days:{additionalReminderValidate:true}},messages:
{contact_name:{required:CONTACT_NAME_EMPTY},pledge_amount:{required:PLEDGE_AMOUNT_EMPTY},num_installment:{required:
NUM_INSTALLMENT_EMPTY},installment_amount:{required:INSTALLMENT_AMOUNT_EMPTY},installment_in:{required:
INSTALLMENT_PERIOD_EMPTY},installment_type_id:{required:INSTALLMENT_CYCLE_EMPTY},payment_due_days:{},pledge_made_date:{
required:PLEDGE_MADE_DATE_EMPTY},payment_start_date:{required:PAYMENT_START_DATE_EMPTY},honoree_name:{},reminder_days:{}
,num_reminder:{},additional_reminder_days:{}}})});var checked=$("#is_reminder").is(":checked");if(checked==true)$(
"#reminder_details").show();else if(checked==false)$("#reminder_details").hide();$("#update").click(function(){$(
"#edit-pledge").validate({submitHandler:function(){if(checkValidData()){var dataStr=$("#edit-pledge").serialize();
showAjxLoader();$.ajax({type:"POST",url:"/edit-pledge",data:dataStr,success:function(responseData){hideAjxLoader();if(!
checkUserAuthenticationAjx(responseData))return false;var jsonObj=jQuery.parseJSON(responseData);if(jsonObj.status==
"success")window.location.href="/pledges";else $.each(jsonObj.message,function(i,msg){$("#"+i).after(
'<label class="error" style="display:block;">'+msg+"</label>")})}})}},rules:{contact_name:{required:true},pledge_amount:
{required:true,numericcommachar:true},num_installment:{required:true,numericonly:true},installment_amount:{required:true
,numericcommachar:true},installment_in:{required:true,numericonly:true},installment_type_id:{required:true},
payment_due_days:{required:true,numericonly:true},pledge_made_date:{required:true},payment_start_date:{required:true},
honoree_name:{honoreeNameValidate:true},reminder_days:{initialReminderValidate:true},num_reminder:{
sendUpToReminderValidate:true},additional_reminder_days:{additionalReminderValidate:true}},messages:{contact_name:{
required:CONTACT_NAME_EMPTY},pledge_amount:{required:PLEDGE_AMOUNT_EMPTY},num_installment:{required:
NUM_INSTALLMENT_EMPTY},installment_amount:{required:INSTALLMENT_AMOUNT_EMPTY},installment_in:{required:
INSTALLMENT_PERIOD_EMPTY},installment_type_id:{required:INSTALLMENT_CYCLE_EMPTY},payment_due_days:{required:
PAYMENT_DUE_DATE_EMPTY},pledge_made_date:{required:PLEDGE_MADE_DATE_EMPTY},payment_start_date:{required:
PAYMENT_START_DATE_EMPTY},honoree_name:{},reminder_days:{},num_reminder:{},additional_reminder_days:{}}})})})