$(function(){var emptyMsgDiv=$('<div class="no-record-msz">'+NO_RECORD_FOUND+"</div>"),grid=jQuery("#list_pledge");$(
"#list_pledge").jqGrid({postData:{searchString:$("#search_pledge").serialize()},mtype:"POST",url:
"/search-contact-pledges/"+$("#user_id").val(),datatype:"json",sortable:true,colNames:["Pledge #","Name","Total Amount",
"Balance Amount","Pledge for","Date","Status Name","Action"],colModel:[{name:"Pledge #",index:"pledge_id"},{name:"Name",
index:"user_id"},{name:"Total Amount",index:"tbl_pledge.pledge_amount"},{name:"Balance Amount",index:"",sortable:false},
{name:"Pledge for",index:"tbl_pledge.contribution_type_id"},{name:"Date",index:"tbl_pledge.added_date"},{name:
"Status Name",index:"pledge_status_id"},{name:"Action",sortable:false,cmTemplate:{title:false}}],viewrecords:true,
sortname:"tbl_pledge.modified_date",sortorder:"desc",rowNum:10,rowList:[10,20,30],pager:"#listpledgerud",viewrecords:
true,autowidth:true,shrinkToFit:true,caption:"",width:"100%",cmTemplate:{title:false},loadComplete:function(){var count=
grid.getGridParam(),ts=grid[0];if(ts.p.reccount===0){grid.hide();emptyMsgDiv.show();$("#pcrud div.ui-paging-info").css(
"display","none")}else{grid.show();emptyMsgDiv.hide();$("#pcrud div.ui-paging-info").css("display","block")}}});
emptyMsgDiv.insertAfter(grid.parent());emptyMsgDiv.hide();$("#list_pledge").jqGrid("navGrid","#listpledgerud",{reload:
true,edit:false,add:false,search:false,del:false})})