$(function(){jQuery.validator.addMethod("valid_check",function(value,element){return this.optional(element)||/^[0-9]+$/.
test(value)},CHECK_NUMBER_VALID);if(typeof pledge_id!="undefined"){$.jgrid.no_legacy_api=true;$.jgrid.useJSON=true;$(
"#list").jqGrid({postData:{pledge_id:pledge_id},mtype:"POST",url:"/get-pledge-transactions/"+encrypt_pledge_id,datatype:
"json",sortable:true,colNames:["Due Date","Total Amount","Amount pledged","Remaining Amount","Status","Transaction #"],
colModel:[{name:"Due Date",index:"due_date",sortable:false},{name:"Total Amount",index:"pledge_amount",sortable:false},{
name:"Amount pledged",index:"transaction_amount",sortable:false},{name:"Remaining Amount",index:"remaining_amount",
sortable:false},{name:"Status",index:"status",sortable:false},{name:"Transaction Id",index:"user_transaction_id",
sortable:false}],viewrecords:true,sortname:"due_date",sortorder:"ASC",rowNum:10,rowList:[10,20,30],pager:"#pcrud",
viewrecords:true,autowidth:true,shrinkToFit:true,caption:"",width:"100%",cmTemplate:{title:false},loadComplete:function(
){$("select.e1").select2({})}});$("#list").jqGrid("navGrid","#pcrud",{reload:true,edit:false,add:false,search:false,del:
false})}$("#pledge_cancel,#cboxClose").click(function(){if(typeof parent.$("#pledge_status")!="undefined")parent.$(
"#pledge_status").val("0").select2({});parent.$.colorbox.close()});$("#pledge_transaction").validate({submitHandler:
function(){var dataStr=$("#pledge_transaction").serialize();showAjxLoader();$.ajax({type:"POST",url:
"/get-pledge-payment-detail",data:dataStr,success:function(responseData){hideAjxLoader();if(!checkUserAuthenticationAjx(
responseData))return false;var jsonObj=jQuery.parseJSON(responseData);if(jsonObj.status=="success"){parent.$(
"#success_message").show();parent.$("#success_message").html(jsonObj.message);parent.jQuery("#list").trigger(
"reloadGrid",[{page:1}]);parent.$.colorbox.close()}else $.each(jsonObj.message,function(i,msg){$("#"+i).after(
'<label class="error" style="display:block;">'+msg+"</label>")})}})},rules:{account_number:{required:true},check_number:
{required:true,valid_check:true}},messages:{account_number:{required:ACCOUNT_NUMBER_EMPTY},check_number:{required:
CHECK_NUMBER_EMPTY,valid_check:CHECK_NUMBER_VALID}}})});function payPledgeAmount(currVal,encryptPledgeTransactionId){if(
currVal!=0)$.colorbox({href:"/get-pledge-payment-detail/"+encryptPledgeTransactionId,width:"900px",height:"800px",iframe
:true})}function getPaymentDetail(currVal){if(currVal==""){$("#bank_account_no_block").hide();$("#check_no_block").hide(
);$("#submit_block").hide()}else if(currVal==1){$("#bank_account_no_block").hide();$("#check_no_block").hide();$(
"#submit_block").hide();parent.$.colorbox({href:"/get-credit-card-detail/"+encryptUserId+"/"+encryptPledgeTransactionId,
width:"900px",height:"800px",iframe:true});parent.$.colorbox.close()}else if(currVal==2){$("#bank_account_no_block").
show();$("#check_no_block").hide();$("#submit_block").show()}else if(currVal==3){$("#bank_account_no_block").hide();$(
"#check_no_block").show();$("#submit_block").show()}}