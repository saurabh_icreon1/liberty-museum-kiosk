<?php

namespace Base\Model;

//use Base\Model\SphinxClient;
//include('sphinxapi.php');

class SphinxComponent {

    /**
     * Used for runtime configuration of model 
     */
    private $_defaults = array('port' => 9312);

    /**
     * Spinx client object 
     * 
     * @var SphinxClient 
     */
    private $sphinx = null;
    private $sphinxServer=null;

    public function __construct($serverAdd) {
        $config = '';
        
        $this->sphinxServer=$serverAdd;
        $settings = array_merge((array) $config, $this->_defaults);
        $this->sphinx = new SphinxClient();
        $this->sphinx->SetServer($this->sphinxServer, $settings['port']);
    }

    function initialize($config = array()) {
        $settings = array_merge((array) $config, $this->_defaults);
        $this->sphinx = new SphinxClient();
        $this->sphinx->SetServer($this->sphinxServer, $settings['port']);
    }

    function search($arrSphinxSettings) {
        if (!isset($this->sphinx)) {
            $this->initialize(null);
        }

        if (empty($arrSphinxSettings['limit'])) {
            $query['limit'] = 9999999;
            $query['page'] = 1;
        }

        foreach ($arrSphinxSettings as $key => $setting) {
            switch ($key) {
                case 'filter':
                    foreach ($setting as $key2 => $arg) {
                        $this->sphinx->SetFilter($key2, (array) $arg);
                    }
                    break;
                case 'filterRange':
                    //TODO 
                    break;
                case 'filterFloatRange':
                    $method = 'Set' . $key;
                    foreach ($setting as $arg) {
                        $arg[3] = empty($arg[3]) ? false : $arg[3];
                        $this->sphinx->{$method}($arg[0], (array) $arg[1], $arg[2], $arg[3]);
                    }
                    break;
                case 'matchMode':
                    $this->sphinx->SetMatchMode($setting);
                    break;
                case 'sortMode':
                    $this->sphinx->SetSortMode(key($setting), reset($setting));
                    break;
                case 'fieldWeights':
                    $this->sphinx->SetFieldWeights($setting);
                    break;
                case 'indexdWeights':
                    $this->sphinx->SetIndexWeights($setting);
                    break;
                case 'rankingMode':
                    $this->sphinx->SetRankingMode($setting);
                    break;
                case 'groupby' :
                    foreach ($setting as $args) {

                        if (is_array($args)) {
                            $arg = $args;
                        } else {
                            $arg = array();
                            $arg[0] = $args;
                            $arg[1] = "@count desc";
                        }
                        $this->sphinx->SetGroupBy($arg[0], SPH_GROUPBY_ATTR, $arg[1]);
                    }
                    break;
                case 'groupdistinct':
                    foreach ($setting as $arg) {
                        $this->sphinx->SetGroupDistinct($arg);
                    }
                    break;
                default:
                    break;
            }
        }

        $this->sphinx->SetLimits(($arrSphinxSettings['page'] - 1) * $arrSphinxSettings['limit'], $arrSphinxSettings['limit']);

        $indexes = !empty($arrSphinxSettings['index']) ? implode(',', $arrSphinxSettings['index']) : '*';

        if (!isset($arrSphinxSettings['search'])) {
            $result = $this->sphinx->Query('', $indexes);
        } else {
            $result = $this->sphinx->Query($arrSphinxSettings['search'], $indexes);
        }

        if ($result === false) {
            echo $this->query_info = $this->sphinx->GetLastError();
            $this->resultCount = 0;

            return 0;
            //throw new SphinxException(); 
        }

        return $result;
    }

    function BuildExcerpts($docs, $index, $words, $opts = array()) {
        if (!isset($this->sphinx)) {
            $this->initialize(null);
        }
        return $this->sphinx->BuildExcerpts($docs, $index, $words, $opts);
    }

    function SetFilterRange($attribute, $min, $max, $exclude = false) {
        if (!isset($this->sphinx)) {
            $this->initialize(null);
        }
        return $this->sphinx->SetFilterRange($attribute, $min, $max, $exclude);
    }

    function SetFilter($attribute, $values, $exclude = false) {
        if (!isset($this->sphinx)) {
            $this->initialize(null);
        }
        return $this->sphinx->SetFilter($attribute, $values, $exclude);
    }

    function SetSelect($select) {
        if (!isset($this->sphinx)) {
            $this->initialize(null);
        }
        return $this->sphinx->SetSelect($select);
    }

    function SetFieldWeights($weights) {
        if (!isset($this->sphinx)) {
            $this->initialize(null);
        }
        return $this->sphinx->SetFieldWeights($weights);
    }

    function BuildKeywords($query, $index, $hits) {

        if (!isset($this->sphinx)) {
            $this->initialize(null);
        }
        return $this->sphinx->BuildKeywords($query, $index, $hits);
    }

    function SetIndexWeights($weights) {


        if (!isset($this->sphinx)) {
            $this->initialize(null);
        }
        return $this->sphinx->SetIndexWeights($weights);
    }

    function EscapeString($weights) {
        if (!isset($this->sphinx)) {
            $this->initialize(null);
        }
        return $this->sphinx->EscapeString($weights);
    }

    function SetSortMode($mode, $sortby = "") {
        if (!isset($this->sphinx)) {
            $this->initialize(null);
        }
        return $this->sphinx->SetSortMode($mode, $sortby);
    }

    function SetGroupBy($attribute, $func, $groupsort = "@group desc") {
        if (!isset($this->sphinx)) {
            $this->initialize(null);
        }
        return $this->sphinx->SetGroupBy($attribute, $func, $groupsort = "@group desc");
    }

}

/* class SphinxException extends Exception {

  }
 */
?>
