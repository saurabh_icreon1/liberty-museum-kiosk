<?php

/**
 * This controller is used for Base functions
 * @package    Base_BaseController
 * @author     Icreon Tech - DG
 */

namespace Base\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\AuthenticationService;
use Zend\Session\Container;
use Zend\Session\Container as SessionContainer;

/**
 * Basic action controller
 */
class BaseController extends AbstractActionController {

    public $auth;

    public function __construct() {
//        $auth = new AuthenticationService();
//        $this->auth = $auth;
//        if ($this->auth->hasIdentity()) {
//            return new ViewModel(array('auth' => $this->auth));
//        }
    }

    /**
     * This function is used to call every time before calling any action
     * @return     array
     * @param type of date
     * @author Icreon Tech - DT
     */
    public function onDispatch(\Zend\Mvc\MvcEvent $event) {
        /* User resource validation */
        $auth = new AuthenticationService();
        $sm = $event->getApplication()->getServiceManager();
        $this->_config = $sm->get('Config');
        $this->timeZoneSession = new SessionContainer('timeZone');

        if (!isset($this->timeZoneSession->timeZone) && $this->_config['get_time_zone_url']['get_time_zone_url'] == 1)
        {
            $isTimezoneSessionSet = 0;
        }
        else
        {
            $isTimezoneSessionSet = 1;
        }
        /** common variable assignment to all views for js, image and css* */
        $viewModel = $event->getApplication()->getMvcEvent()->getViewModel();
        $viewModel->setVariable('isTimezoneSessionSet', $isTimezoneSessionSet); // this is used in footer to check if timeZoneSession->timeZone is set 
        $viewModel->setVariable('https_on', $this->_config['https_on']); // '1' for on '0' for off        
        $viewModel->setVariable('https_url', $this->_config['https_url']);
        $httpsUrl = (explode("/", substr($_SERVER['REQUEST_URI'], 1)));
        if ($this->_config['https_on'] == '1'/* && in_array($httpsUrl[0], $this->_config['https_url'])*/) {
            $viewModel->setVariable('httpsPathOn', '1'); // '1' for on '0' for off
            $viewModel->setVariable('jsFilePath', $this->_config['js_file_path_https']['path']);
            $viewModel->setVariables(array('jsFilePath' => $this->_config['js_file_path_https']['path']));
            $viewModel->setVariable('imageFilePath', $this->_config['img_file_path_https']['path']);
            $viewModel->setVariable('cssFilePath', $this->_config['css_file_path_https']['path']);
            $viewModel->setVariable('assetsUrl', $this->_config['file_upload_path']['assets_url_https']);
            $viewModel->setVariable('checkoutUrl', SITE_URL_HTTPS.'/checkout');
        } else {
            $viewModel->setVariable('httpsPathOn', '0'); // '1' for on '0' for off
            $viewModel->setVariable('jsFilePath', $this->_config['js_file_path']['path']);
            $viewModel->setVariables(array('jsFilePath' => $this->_config['js_file_path']['path']));
            $viewModel->setVariable('imageFilePath', $this->_config['img_file_path']['path']);
            $viewModel->setVariable('cssFilePath', $this->_config['css_file_path']['path']);
            $viewModel->setVariable('assetsUrl', $this->_config['file_upload_path']['assets_url']);
            $viewModel->setVariable('checkoutUrl', SITE_URL_HTTPS.'/checkout');
        }
        if($_SERVER['REQUEST_URI']=='/kiosk-login' || $_SERVER['REQUEST_URI']=='/authenticate-user') {
            if(isset($_SERVER['HTTP_SSLSESSIONID']) || $_SERVER['SERVER_PORT'] == '443') // It means from https request
            {
                if ($this->_config['https_on'] == '1') {
                    $viewModel->setVariable('httpsPathOn', '1'); // '1' for on '0' for off
                    $viewModel->setVariable('jsFilePath', $this->_config['js_file_path_https']['path']);
                    $viewModel->setVariables(array('jsFilePath' => $this->_config['js_file_path_https']['path']));
                    $viewModel->setVariable('imageFilePath', $this->_config['img_file_path_https']['path']);
                    $viewModel->setVariable('cssFilePath', $this->_config['css_file_path_https']['path']);
                    $viewModel->setVariable('assetsUrl', $this->_config['file_upload_path']['assets_url_https']); 
                    $viewModel->setVariable('checkoutUrl', SITE_URL_HTTPS.'/checkout');
                }
            }else {
                $viewModel->setVariable('httpsPathOn', '0'); // '1' for on '0' for off
                $viewModel->setVariable('jsFilePath', $this->_config['js_file_path']['path']);
                $viewModel->setVariables(array('jsFilePath' => $this->_config['js_file_path']['path']));
                $viewModel->setVariable('imageFilePath', $this->_config['img_file_path']['path']);
                $viewModel->setVariable('cssFilePath', $this->_config['css_file_path']['path']);
                $viewModel->setVariable('assetsUrl', $this->_config['file_upload_path']['assets_url']);
                $viewModel->setVariable('checkoutUrl', SITE_URL_HTTPS.'/checkout');
            }
        }
        $moduleSession = new Container('moduleSession');
        $viewModel->setVariable('jsVersion', $this->_config['file_versions']['js_version']);
        $viewModel->setVariable('cssVersion', $this->_config['file_versions']['css_version']);
	$viewModel->setVariable('moduleGroup', $moduleSession->moduleGroup);


        /** end of variable assignement * */
        if (isset($auth->getIdentity()->crm_user_id) && $auth->getIdentity()->crm_user_id != '') {
            $checkValidPermission = $this->checkValidUser($event);
            if ($checkValidPermission['message'] == 'notValidUser') {
                $request = $this->getRequest();
                if ($request->isXmlHttpRequest()) {
                    echo "PageHasNotPermission###" . $checkValidPermission['redirectUrl'];
                    die;
                } else {
                    $routeMatch = $event->getRouteMatch();

                    $controller = $routeMatch->getParam('controller');
                    $actionViewName = $routeMatch->getParam('action');
                    $moduleViewName = substr($controller, 0, strpos($controller, '\\'));
                    $controllerViewName = substr($controller, strrpos($controller, '\\') + 1);

                    $moduleInfoArr = array();
                    $moduleInfoArr[] = $moduleViewName;
                    $moduleInfoArr[] = $controllerViewName;
                    $moduleInfoArr[] = $actionViewName;
                    if ($moduleViewName == 'Report') {
                        $url = explode("/", $_SERVER['REQUEST_URI']);
                        $moduleInfoArr[] = "/" . $url[1];
                    }
                    $sm = $event->getApplication()->getServiceManager();
                    $moduleDetail = $sm->get('User\Model\CrmuserTable')->getModuleAction($moduleInfoArr);
                    $userDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getCrmUserDetails(array('crm_user_id' => $auth->getIdentity()->crm_user_id));

                    $roleDetail = $sm->get('User\Model\CrmuserTable')->checkCrmUserPermission($userDetail[0]['role_id']);


                    if ($roleDetail['moduleActionPermissions'][$moduleDetail['module_id']][$moduleDetail['action_id']] == 0 && str_replace($_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['HTTP_HOST'], '', $checkValidPermission['redirectUrl']) == $_SERVER['REQUEST_URI'])
                        return $this->redirect()->toUrl("/dashboard");
                    else
                        return $this->redirect()->toUrl($checkValidPermission['redirectUrl']);
                }
            }
        }
        /* User resource validation end */
        return parent::onDispatch($event);
    }

    /**
     * This function is used to check user is valid or not for current module
     * @return     array
     * @param Object
     * @author Icreon Tech - DT
     */
    public function checkValidUser($event) {
        $returnValidUserArr = array();
        $returnValidUserArr['redirectUrl'] = '';
        $returnValidUserArr['message'] = '';
        $sm = $event->getApplication()->getServiceManager();
        $app = $event->getApplication();
        $eventManager = $app->getEventManager();
        $routeMatch = $event->getRouteMatch();

        $routeMatch = $event->getRouteMatch();
        //asd($_SERVER,2);
        $controller = $routeMatch->getParam('controller');
        $actionViewName = $routeMatch->getParam('action');
        $viewModel = $event->getApplication()->getMvcEvent()->getViewModel();
        $moduleViewName = substr($controller, 0, strpos($controller, '\\'));
        $controllerViewName = substr($controller, strrpos($controller, '\\') + 1);
        $viewModel->moduleViewName = $moduleViewName;
        $viewModel->controllerViewName = $controllerViewName;
        $viewModel->actionViewName = $actionViewName;

        $auth = new AuthenticationService();

        $userDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getCrmUserDetails(array('crm_user_id' => $auth->getIdentity()->crm_user_id));

        $roleDetail = $sm->get('User\Model\CrmuserTable')->checkCrmUserPermission($userDetail[0]['role_id']);

        $viewModel->module_permission = $roleDetail['modulePermissions'];
        $viewModel->module_action_permission = $roleDetail['moduleActionPermissions'];
        $moduleInfoArr = array();
        $moduleInfoArr[] = $moduleViewName;
        $moduleInfoArr[] = $controllerViewName;
        $moduleInfoArr[] = $actionViewName;
        if ($moduleViewName == 'Report') {
            $url = explode("/", $_SERVER['REQUEST_URI']);
            $moduleInfoArr[] = "/" . $url[1];
        }

        $moduleDetail = $sm->get('User\Model\CrmuserTable')->getModuleAction($moduleInfoArr);

        $aclModuleId = isset($moduleDetail['module_id']) ? $moduleDetail['module_id'] : 0;
        $aclActionId = isset($moduleDetail['action_id']) ? $moduleDetail['action_id'] : 0;
        if (isset($_SERVER['HTTP_REFERER'])) {
            $permissionRedirectUrl = $_SERVER['HTTP_REFERER'];
        } else {
            $permissionRedirectUrl = '/dashboard';
        }
        $userNotValid = new Container('userNotValid');
        if (!$this->getRequest()->isXmlHttpRequest()) {
            $userNotValid->previousPageUrl = $permissionRedirectUrl;
        }

        $returnValidUserArr['redirectUrl'] = !empty($userNotValid->previousPageUrl) ? $userNotValid->previousPageUrl : '/dashboard';
        //echo $aclModuleId."  ".$aclActionId."  ".$aclModuleId."  ".$aclActionId;die;
        if ((isset($roleDetail['moduleActionPermissions'][$aclModuleId][$aclActionId]) && $roleDetail['moduleActionPermissions'][$aclModuleId][$aclActionId] == 1 ) || $aclModuleId == 0 || $aclActionId == 0) {
            if (isset($userNotValid->notValidUser)) {
                unset($userNotValid->notValidUser);
                $viewModel->has_page_permission = 0;
            } else {
                $viewModel->has_page_permission = 1;
            }
            $returnValidUserArr['message'] = 'validUser';
        } else if (isset($roleDetail['moduleActionPermissions']) && !empty($roleDetail['moduleActionPermissions'])) {
            if (!isset($roleDetail['moduleActionPermissions'][$aclModuleId][$aclActionId])) {
                $returnValidUserArr['message'] = 'validUser';
                $viewModel->has_page_permission = 1;
            } else {
                $userNotValid->notValidUser = 1;
                $returnValidUserArr['message'] = 'notValidUser';
            }
        }
        return $returnValidUserArr;
    }

    /**
     * Helper function for get the become adlut year
     * @author Icreon Tech - DG
     * @return array
     * @param array
     */
    public function becomeAdult() {
        $become_adult = array();
        $blankYr = ' ';
        $become_adult[$blankYr] = 'Select Year';
        for ($i = date("Y"); $i <= date("Y") + 4; $i++) {
            $become_adult[$i] = $i;
        }
        return $become_adult;
    }

    /**
     * Helper function for encrypt the srting
     * @author Icreon Tech - DG
     * @return array
     * @param array
     */
    public function encrypt($param) {
        try {
            if (isset($param)) {
                return $encryptedData = base64_encode(serialize($param));
            }
        } catch (Exception $exc) {
            return $exc->getTraceAsString();
        }
    }

    /**
     * Helper function for decrypt the srting
     * @author Icreon Tech - DG
     * @return array
     * @param array
     */
    public function decrypt($param) {
        try {
            if (isset($param)) {
                return $decryptedData = @unserialize(base64_decode($param));
            }
        } catch (Exception $exc) {
            return $exc->getTraceAsString();
        }
    }

    /**
     * Function for get random number
     * @author Icreon Tech - DG
     * @return array
     * @param Int
     */
    public function generateRandomString() {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ23456789";
        srand((double) microtime() * 1000000);
        $i = 0;
        $code = '';

        while ($i <= 7) {
            $num = rand() % 33;
            $tmp = substr($chars, $num, 1);
            $code = $code . $tmp;
            $i++;
        }
        $code .= mt_rand().$_SERVER['SERVER_ADDR'];
		$code  = md5(uniqid($code, true));
        return $code;
    }

    /**
     * Function for check user authentication
     * @author Icreon Tech - DT
     * @return void
     * @param void
     */
   public function checkUserAuthentication() {
        $auth = new AuthenticationService();
        $request = $this->getRequest();
        $moduleSession = new Container('moduleSession');
         if(isset($moduleSession->moduleGroup) && $moduleSession->moduleGroup==3) {
         }
         else {
			        if (isset($_SERVER['HTTP_REFERER'])) {
			            $refererUrl = $_SERVER['HTTP_REFERER'];
			        } else {
			            if (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] == '/crm') {
			                $refererUrl = SITE_URL . '/dashboard';
			            } else {
			                $refererUrl = SITE_URL . $_SERVER['REQUEST_URI'];
			            }
			        }
			        if ($auth->hasIdentity() === true) {
			            $user = $auth->getIdentity();
			            if (!isset($user->crm_email_id)) {
			                if ($request->isXmlHttpRequest()) {
			                    echo "session_expired";
			                    die;
			                } else {
			                    return $this->redirect()->toRoute('crm');
			                }
			            }
			        } else {
			            if ($request->isXmlHttpRequest()) {
			                echo "session_expired##icpl" . $this->encrypt($refererUrl);
			                die;
			            } else {
			                if (substr($refererUrl, 0, 5) != "https") {
			                    return $this->redirect()->toUrl('/crm/' . $this->encrypt($refererUrl));
			                } else {
			                    $url = SITE_URL . '/crm';
			                    echo "<script>parent.location.href='$url'</script>";
			                    //header('Location: ' . SITE_URL . '/crm');
			                    die;
			                }
			//return $this->redirect()->toRoute('crm');
			            }
			        }
			}        
    }

    /**
     * Function for check user authentication via ajax
     * @author Icreon Tech - DT
     * @return void
     * @param void
     */
    public function checkUserAuthenticationAjx() {
        $auth = new AuthenticationService();
        $moduleSession = new Container('moduleSession');
        $moduleSession->moduleGroup;
        if(isset($moduleSession->moduleGroup) && $moduleSession->moduleGroup==3) {
         }
         else {
		        if ($auth->hasIdentity() === true) {
		            $user = $auth->getIdentity();
		            if (!isset($user->crm_email_id)) {
		                echo "session_expired";
		                die;
		            }
		        } else {
		            echo "session_expired";
		            die;
		        }
        }
    }

    /**
     * Function for check front end user authentication
     * @author Icreon Tech - DG
     * @return void
     * @param void
     */
    public function checkFrontUserAuthentication() {
        $auth = new AuthenticationService();
        
        $moduleSession = new Container('moduleSession');
        if(isset($moduleSession->moduleGroup) && $moduleSession->moduleGroup==3) {
         }
         else {
			        if ($auth->hasIdentity() === true) {
			            $user = $auth->getIdentity();
			            if (isset($user->crm_email_id)) {
			                return $this->redirect()->toRoute('dashboard');
			            }
			        } else {
			            if (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] != '' && $_SERVER['REQUEST_URI'] != '/login') {
			                $refererUrl = SITE_URL . $_SERVER['REQUEST_URI'];
			                $this->referPathSession = new SessionContainer('referPathSession');
			                $this->referPathSession->path = $refererUrl;
			            } /* else if (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] == '/login') {
			              $refererUrl = SITE_URL . '/profile';
			              } */ else {
			                if (isset($_SERVER['HTTP_REFERER'])) {
			                    $refererUrl = $_SERVER['HTTP_REFERER'];
			                } else {
			                    $refererUrl = SITE_URL;
			                }
			            }
			
			            //return $this->redirect()->toUrl('/login/' . $this->encrypt($refererUrl));
			            return $this->redirect()->toUrl('/kiosk-login');
			        }
			}        
    }
    /**
     * Function for check front end user authentication
     * @author Icreon Tech - DG
     * @return void
     * @param void
     */
    /* public function authenticateUserOnSubmitAjax() {
      $auth = new AuthenticationService();
      $request = $this->getRequest();
      if ($auth->hasIdentity() === true) {
      $user = $auth->getIdentity();
      if (isset($user->crm_email_id)) {
      return $this->redirect()->toRoute('dashboard');
      }
      } else {
      if (isset($_SERVER['HTTP_REFERER'])) {
      $refererUrl = $_SERVER['HTTP_REFERER'];
      } else {
      if (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] == '/login') {
      $refererUrl = SITE_URL . '/profile';
      } else {
      $refererUrl = SITE_URL . $_SERVER['REQUEST_URI'];
      }
      }
      echo "session_expired##icpl" . $this->encrypt($refererUrl);
      die;
      }
      } */

    /**
     * Function for check front user authentication via ajax
     * @author Icreon Tech - DG
     * @return void
     * @param void
     */
    public function checkFrontUserAuthenticationAjx() {
    
        $moduleSession = new Container('moduleSession');
        if(isset($moduleSession->moduleGroup) && $moduleSession->moduleGroup==3) {
         }
         else {   
		        if (isset($_SERVER['HTTP_REFERER'])) {
		            $refererUrl = $_SERVER['HTTP_REFERER'];
		            $this->referPathSession = new SessionContainer('referPathSession');
		            $this->referPathSession->path = $refererUrl;
		        } else {
		            $refererUrl = '';
		        }
		        $auth = new AuthenticationService();
		        if ($auth->hasIdentity() === true) {
		            $user = $auth->getIdentity();
		            if (!isset($user->email_id)) {
		                return $this->redirect()->toUrl('/login/' . $this->encrypt($refererUrl));
		            }
		        } else {
		            return $this->redirect()->toUrl('/login/' . $this->encrypt($refererUrl));
		        }
        }
    }

    /**
     * Function for check front user authentication via ajax
     * @author Icreon Tech - DG
     * @return void
     * @param void
     */
    public function checkFrontUserSessionPopup() {
            $moduleSession = new Container('moduleSession');
        if(isset($moduleSession->moduleGroup) && $moduleSession->moduleGroup==3) {
         }
         else {
        
	        $auth = new AuthenticationService();
	        if ($auth->hasIdentity() === true) {
	            $user = $auth->getIdentity();
	            if (isset($user->email_id)) {
	                echo "<script>
	                parent.window.location.href='/profile';
	                </script>";
	                die;
	            }
	        }
        }
    }

    /**
     * This function is used to get the date range on basic of drop down date range parameter
     * @return     array
     * @param type of date
     * @author Icreon Tech - DT
     */
    public function getDateRange($date) {
//$date=9;
        $date_from = '';
        $date_to = '';
        $today = getdate();
        $format = 'm/d/Y';
        $current_date = date($format);
        $quarter_array = array();
        $quarter_array[1] = array();
        $quarter_array[1]['start'] = "01/01/{$today['year']}";
        $quarter_array[1]['end'] = "03/31/{$today['year']}";
        $quarter_array[2]['start'] = "04/01/{$today['year']}";
        $quarter_array[2]['end'] = "06/30/{$today['year']}";
        $quarter_array[3]['start'] = "07/01/{$today['year']}";
        $quarter_array[3]['end'] = "09/30/{$today['year']}";
        $quarter_array[4]['start'] = "10/01/{$today['year']}";
        $quarter_array[4]['end'] = "12/31/{$today['year']}";

        if($date == 2){
			// Today 

			$date_from = date('m/d/Y', strtotime($current_date));
			$date_to = date('m/d/Y', strtotime($current_date));

		}else if ($date == 3){
			// Previous Day

			$date_from = date('m/d/Y', strtotime('-1 day'));
			$date_to = date('m/d/Y', strtotime('-1 day'));

		}else if ($date == 4) {
//This Year
            $date_from = date("01/01/{$today['year']}");
            $date_to = date("12/31/{$today['year']}");
        } else if ($date == 5) {
//This Quarter
            $currquarter = ($today['mon'] - 1) / 3 + 1;
            $date_from = date($quarter_array[$currquarter]['start']);
            $date_to = date($quarter_array[$currquarter]['end']);
        } else if ($date == 6) {
//This Month
            $num_days = cal_days_in_month(CAL_GREGORIAN, $today['mon'], $today['year']);
            $today['mon'] = sprintf('%02d', $today['mon']);
            $date_from = date("{$today['mon']}/01/{$today['year']}");
            $date_to = date("{$today['mon']}/$num_days/{$today['year']}");
        } else if ($date == 7) {
//This Week

            if (date('N', time()) == 1)
                $start_date = date($format);
            else
                $start_date = date($format, strtotime('last Monday'));
            $date_from = $start_date;
            $date_to = date($format, strtotime('+6 day' . $start_date));
        }
        if ($date == 8) {
//Previous Year
            $prevYear = $today['year'] - 1;
            $date_from = date("01/01/$prevYear");
            $date_to = date("12/31/$prevYear");
        }
        if ($date == 9) {
//Previous Quarter
            $curr_quarter = ($today['mon'] - 1) / 3 + 1;
            $date_from = date($format, strtotime("-3 months" . date($quarter_array[$curr_quarter]['start'])));
            $date_to = date($format, strtotime("-3 months" . date($quarter_array[$curr_quarter]['end'])));
        }
        if ($date == 10) {
//Previous Month
            $num_days = cal_days_in_month(CAL_GREGORIAN, $today['mon'], $today['year']);
            $curr_month = $today['mon'];
            $date_from = date($format, strtotime("-1 months" . date("{$curr_month}/01/{$today['year']}")));
            $date_to = date($format, strtotime("-1 months" . date("{$curr_month}/$num_days/{$today['year']}")));
        }
        if ($date == 11) {
//Previous Week
            if (date('N', time()) == 1)
                $start_date = date($format, strtotime('last Monday'));
            else
                $start_date = date($format, strtotime(date($format, strtotime('last Monday'))) - 7 * 24 * 60 * 60);
            $date_from = $start_date;
            $date_to = date($format, strtotime('+6 day' . $start_date));
        }
        if ($date == 12) {
//last 13 month
            $lastThirteenMonth_start_date = date("m/d/Y", strtotime( date( 'Y-m-01' )." -12 months"));
            $date_from = $lastThirteenMonth_start_date;
            $date_to = date("m/d/Y");
        }if ($date == 13) {
//Weekly
            $date_from =date($format, strtotime('-6 day' . $start_date));
            $date_to =  date("m/d/Y");
        }        
        return array('from' => $date_from, 'to' => $date_to);
    }

    /**
     * This function is used to get days array
     * @return     array
     * @param void 
     * @author Icreon Tech - SR
     */
    public function dayArray() {
        $day = array(
            "" => "Select",
            "1" => "Sunday",
            "2" => "Monday",
            "3" => "Tuesday",
            "4" => "Wednesday",
            "5" => "Thursday",
            "6" => "Friday",
            "7" => "Saturday"
        );
        return $day;
    }

    /**
     * This function is used to get month array
     * @return     array
     * @param void 
     * @author Icreon Tech - SR
     */
    public function monthArray() {
        $month = array(
            "" => "Select",
            "1" => "January",
            "2" => "February",
            "3" => "March",
            "4" => "April",
            "5" => "May",
            "6" => "June",
            "7" => "July",
            "8" => "August",
            "9" => "September",
            "10" => "October",
            "11" => "November",
            "12" => "December"
        );
        return $month;
    }

    /**
     * This function is used to get month array for credit card
     * @return     array
     * @param void
     * @author Icreon Tech - DT
     */
    public function creditMonthArray() {
        $month = array(
            "" => "Select",
            "01" => "January (1)",
            "02" => "February(2)",
            "03" => "March(3)",
            "04" => "April(4)",
            "05" => "May(5)",
            "06" => "June(6)",
            "07" => "July(7)",
            "08" => "August(8)",
            "09" => "September(9)",
            "10" => "October(10)",
            "11" => "November(11)",
            "12" => "December(12)"
        );
        return $month;
    }

    /**
     * This function is used to get year array
     * @return     array
     * @param void
     * @author Icreon Tech - DT
     */
    public function yearArrayRange($numOfYear = '', $minNumYear = '') {
        $numOfYear = (!empty($numOfYear)) ? $numOfYear : 10;
        $minNumYear = (!empty($minNumYear)) ? $minNumYear : 0;
        $fromYear = date('Y') - $minNumYear;
        $toYear = date('Y') + $numOfYear;
        $year = array('' => 'Select');
        for ($i = $fromYear; $i <= $toYear; $i++) {
            $year[$i] = $i;
        }

        return $year;
    }

    /**
     * This function is used to get gender array
     * @return     array
     * @param void 
     * @author Icreon Tech - SR
     */
    public function genderArray() {
        $gender = array(
            'M' => 'Male',
            'F' => 'Female',
            'U' => 'Unknown'
        );
        return $gender;
    }

    /**
     * This function is used to get marital status array
     * @return     array
     * @param void 
     * @author Icreon Tech - SR
     */
    public function maritalStatusArray() {
        $maritalStatus = array(
            'S' => 'Single',
            'M' => 'Married',
            'D' => 'Divorced',
            'W' => 'Widowed',
            'U' => 'Unknown'
        );
        return $maritalStatus;
    }

    /** This function is used to generate physical filename for member media.
     * @param int $intpLength(length of string)
     * @return string
     * @author Icreon Tech - DG
     */
    public static function GenerateMediaFileName($fileExt = '') {
        if ($fileExt != '')
            $strAlphaNumeric = self::GetRandomAlphaNumeric(8) . '-' . self::GetRandomAlphaNumeric(4) . '-' . self::GetRandomAlphaNumeric(12) . "." . $fileExt;
        else
            $strAlphaNumeric = self::GetRandomAlphaNumeric(8) . '-' . self::GetRandomAlphaNumeric(4) . '-' . self::GetRandomAlphaNumeric(12);
        return $strAlphaNumeric;
    }

    /** This function is used to generate random alpha numeric string.
     * @param int $intpLength(length of string)
     * @return string
     * @author Icreon Tech - DG
     * */
    public static function GetRandomAlphaNumeric($intpLength = 16) {
        $arrAlphaNumeric = array();
        $arrAlpha = range('A', 'Z');
        $arrNumeric = range(0, 9);

        $arrAlphaNumeric = array_merge($arrAlphaNumeric, $arrAlpha, $arrNumeric);

        mt_srand((double) microtime() * 1234567);
        shuffle($arrAlphaNumeric);

        $strAlphaNumeric = '';
        for ($x = 0; $x < $intpLength; $x++)
            $strAlphaNumeric .= $arrAlphaNumeric[mt_rand(0, (sizeof($arrAlphaNumeric) - 1))];
        return $strAlphaNumeric;
    }

    /** Following function is used to create folder structure to upload media 
     * @param  int $pintMemberId (member id).
     * @return array $oarrFolder (array of folder structure).
     * @author Icreon Tech - DG
     * */
    public static function folderStructByUserId($pintUserId) {
        $FolderArray = array();
        $strFolder = $pintUserId;

        for ($intCount = 1; $intCount <= 4 - strlen($pintUserId); $intCount++) {
            $strFolder = "0" . $strFolder;
        }

        $FolderArray[0] = (int) substr($strFolder, 0, 1);
        $FolderArray[1] = (int) substr($strFolder, 1, 4);
        $UserPath = $FolderArray[0] . "/" . $FolderArray[1] . "/";
        return $UserPath;
    }

    /**
     * create directory structure for given path
     * @param $pstrBasePath (base path), string $pstrDirPath (dir path to be created)
     * @return string path
     * @author Icreon Tech - DG
     */
    public static function createDirStructure($pstrBasePath, $pstrDirPath) {
        $arrDir = explode('/', $pstrDirPath);
        for ($intCount = 0; $intCount < count($arrDir); $intCount++) {
            if ($arrDir[$intCount] !== '') {
                $pstrBasePath .= '/' . $arrDir[$intCount];
                if (!is_dir($pstrBasePath)) {
                    mkdir($pstrBasePath, 0755, TRUE);
//mkdir($pstrBasePath,0777);
                }
            }
        }
        return $pstrBasePath . "/";
    }

    /**
     * get date format
     * @param date, and date format
     * @return string date format
     * @author Icreon Tech - SR
     */
    public function DateFormat($date, $format = '') {
        $this->timeZoneSession = new SessionContainer('timeZone');
        if (strtolower($format) == 'db_datetime_format') {
            $dateArr = array();
            if ($date != '') {
                $dateArr = explode('/', $date);
                $date = $dateArr[2] . '-' . $dateArr[0] . '-' . $dateArr[1]; // "Y-m-d H:i:s", strtotime($date));
            }
        } else if (strtolower($format) == 'db_date_format') {
            $dateArr = array();
            if ($date != '') {
                $dateArr = explode('/',$date);
                $date = $dateArr[2] . '-' . $dateArr[0] . '-' . $dateArr[1]; // "Y-m-d H:i:s", strtotime($date));
            }           
        }else if (strtolower($format) == 'db_date_format_from') {
            $dateArr = array();
            if ($date != '') {
				$dateArr = explode('/',$date);
                $date = $dateArr[2] . '-' . $dateArr[0] . '-' . $dateArr[1]." 00:00:00"; // "Y-m-d H:i:s", strtotime($date));
                $date = date("Y-m-d H:i:s", strtotime($date) - ($this->timeZoneSession->timeZone * 60));
            }           
        }else if (strtolower($format) == 'db_date_format_to') {
            $dateArr = array();
            if ($date != '') {
                $dateArr = explode('/',$date);
                $date = $dateArr[2] . '-' . $dateArr[0] . '-' . $dateArr[1]." 23:59:59"; // "Y-m-d H:i:s", strtotime($date));
                //asd($date);
                $date = date("Y-m-d H:i:s", strtotime($date) - ($this->timeZoneSession->timeZone * 60));
            }           
        }
        else if (strtolower($format) == 'calender') {
            $dateArr = array();
            if ($date != '') {
                $dateArr = explode(' ', $date);
                $dateArr = explode('-', $dateArr[0]);
                $date = $dateArr[1] . '/' . $dateArr[2] . '/' . $dateArr[0];
            }
        } else if (strtolower($format) == 'displayformat') {
            if (!empty($date)) {
                if (strpos($date, '-') != false) {
                    $dateDataArray = explode('-', substr($date, 0, 10));
                    $date = $dateDataArray[1] . '-' . $dateDataArray[2] . '-' . $dateDataArray[0];
                }
            }

            //if (!empty($date))
            //  $date = date("m-d-Y", strtotime($date));
        } else if (strtolower($format) == 'displayformattime') {
            if (!empty($date)) {
                $this->timeZoneSession = new Container('timeZone');
                if ($this->timeZoneSession->timeZone != '') {
                    $date = date("m-d-Y H:i", strtotime($date) + $this->timeZoneSession->timeZone * 60);
                } else {
                    $date = date("m-d-Y H:i", strtotime($date));
                }
            }
        } else if (strtolower($format) == 'datetomonthformat') {
            $dateArr = array();
            if ($date != '') {
                $dateArr = explode('/', $date);
                $date = $dateArr[1] . ' ' . date("F", mktime(0, 0, 0, $dateArr[0], 10)) . ' ' . $dateArr[2];
            }
        } else if (strtolower($format) == 'datetoshortmonthformat') {
            $dateArr = array();
            if ($date != '') {
                $dateArr = explode('/', $date);
                $date = $dateArr[1] . ' ' . date("M", mktime(0, 0, 0, $dateArr[0], 10)) . ' ' . $dateArr[2];
            }
        } else if (strtolower($format) == 'monthtodateformat') {
            $dateArr = array();
            if ($date != '') {
                $dateArr = explode(' ', $date);
                $date = sprintf("%02s", date("m", strtotime($dateArr[1]))) . '/' . sprintf("%02s", $dateArr[0]) . '/' . $dateArr[2];
            }
        } else if (strtolower($format) == 'displayformatwithtime') {
            $this->timeZoneSession = new Container('timeZone');
            if ($this->timeZoneSession->timeZone != '') {
                $date = date("m-d-Y H:i", strtotime($date) + $this->timeZoneSession->timeZone * 60);
            } else {
                $date = date("m-d-Y H:i", strtotime($date));
            }
        } else if (strtolower($format) == 'displaymonth') {
            $dateArr = array();
            if ($date != '') {
                $dateArr = explode(' ', $date);
                $date = date("d-M-Y", strtotime($dateArr[1]));
            }
        } else if (strtolower($format) == 'displayfullmonth') {
            $dateArr = array();
            if ($date != '') {
                $dateArr = explode(' ', $date);
                $date = date("F d, Y", strtotime($date)+ $this->timeZoneSession->timeZone * 60);
            }
        } else {
            $this->timeZoneSession = new Container('timeZone');
            if ($this->timeZoneSession->timeZone != '') {
                $date = date("Y-m-d H:i", strtotime($date) + $this->timeZoneSession->timeZone * 60);
            } else {
                $date = date("Y-m-d", strtotime($date));
            }
        }

        return $date;
    }

    /**
     * get file extension
     * @param filename
     * @return array
     * @author Icreon Tech - DT
     */
    public function GetFileExt($strpFileName) {

        $arrFile = explode(".", $strpFileName);

        $strFileExt = strtolower($arrFile[count($arrFile) - 1]);

        $strFileName = substr($strpFileName, 0, - (strlen($strFileExt) + 1));

        return array($strFileName, $strFileExt);
    }

    /**
     *  This function is used to create excel header
     * Created By : Dev2
     * Date : 17 Aug,2011
     * @param  
     * @return array
     */
    function downloadSendHeaders($filename) {
// disable caching
        $now = gmdate("D, d M Y H:i:s");
        header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
        header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
        header("Last-Modified: {$now} GMT");

// force download
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");

// disposition / encoding on response body
        header("Content-Disposition: attachment;filename={$filename}");
        header("Content-Transfer-Encoding: binary");
            
        if($path !="")
        {
            $filePath = $path;
        }
        else
        {
            $filePath = $this->_config['file_upload_path']['assets_upload_dir'] . 'export/';
        }        
        
        $completeFilePath = $filePath . $filename;
        readfile($completeFilePath);
    }

    /**
     *  This function is used to convert array to CSV
     * Created By : Dev2
     * Date : 28 Aug,2013
     * @param  
     *
     */
    function arrayToCsv(array &$array, $filename, $page_counter) {
        $filePath = $this->_config['file_upload_path']['assets_upload_dir'] . 'export/';
        $completeFilePath = $filePath . $filename;
        if (count($array) == 0) {
            return null;
        }
        ob_start();
        if ($page_counter == 2) {
            $df = fopen($completeFilePath, 'w');
            //$df = fopen("php://output", 'w');
            fputcsv($df, array_keys(reset($array)));
        } else {
            //$df = fopen("php://output", 'a+');
            $df = fopen($completeFilePath, 'a+');
        }

        foreach ($array as $row) {
            fputcsv($df, $row);
        }
        fclose($df);
        return ob_get_clean();
    }

    /**
     * This function is used to get status array
     * @return     array
     * @param void 
     * @author Icreon Tech - SR
     */
    public function statusArray() {
        $status = array(
            '' => 'Select',
            '1' => 'Active',
            '0' => 'Inactive'
        );
        return $status;
    }

    /**
     * This function is used to get chunk of data from long data
     * @return     string
     * @param     string,Int
     * @author Icreon Tech - DT
     */
    public function chunkData($data, $length = 30) {
        if (strlen($data) > $length) {
            $subData = substr($data, 0, $length);
            if (substr($data, $length, 1) == ' ') {
                return $subData . "...";
            } else {
                return substr($subData, 0, strrpos($subData, ' ')) . "...";
            }
        }
        return $data;
    }

    /**
     * This function is used to get year array
     * @return     array
     * @param void 
     * @author Icreon Tech - SR
     */
    public function yearArray() {
        $year = array();
        $year[''] = 'Select';
        for ($i = 1600; $i <= 2100; $i++) {
            $year[$i] = $i;
        }
        return $year;
    }

    /**
     * This function is used to remove Empty value from Array
     * @return     array
     * @param void 
     * @author Icreon Tech - ST
     */
    public function removeEmptyArray($params) {
        if (is_array($params) && count($params) > 0) {
            foreach ($params as $key => $value) {
                if (is_array($value)) {
                    foreach ($value as $key2 => $value2) {
                        if (empty($value2))
                            unset($params[$key][$key2]);
                    }
                }
                if (empty($params[$key]))
                    unset($params[$key]);
            }
        }
        return $params;
    }

    /**
     * This function is used to get naming convention array
     * @param void
     * @return void
     * @author Icreon Tech - DT
     */
    public function getNamingConventionArr($optionName, $attributeName, $wohDetail = '') {
        /* Logic for generate wall of honor page */
        $optionNameArr = str_word_count($optionName, 1);
        $totalNameOptions = count($optionNameArr);
        $namingConventionArr = array();
        $namingConventionArr['The'] = 0;
        $namingConventionArr['firstName'] = 0;
        $namingConventionArr['firstName2'] = 0;
        $namingConventionArr['otherInit'] = 0;
        $namingConventionArr['otherInit2'] = 0;
        $namingConventionArr['and'] = 0;
        $namingConventionArr['hiphen'] = 0;
        $namingConventionArr['otherName'] = 0;
        $namingConventionArr['otherName2'] = 0;
        $namingConventionArr['lastName'] = 0;
        $namingConventionArr['lastName2'] = 0;
        $namingConventionArr['firstLine'] = 0;
        $namingConventionArr['secondLine'] = 0;
        $namingConventionArr['Family'] = 0;
        if (strpos($optionName, 'Ellis Island') !== false) {
            $namingConventionArr['firstLine'] = 1;
            $namingConventionArr['secondLine'] = 1;
        } else {
            if (isset($optionNameArr[0]) && $optionNameArr[0] == "The") {
                $namingConventionArr['The'] = 1;
                $namingConventionArr['Family'] = 1;
                $optionNameArr = array_splice($optionNameArr, 1);
                unset($optionNameArr[$totalNameOptions - 2]);
            }
            $andIndex = array_search('and', $optionNameArr);
            if ($andIndex) {
                $namingConventionArr['and'] = 1;
                unset($optionNameArr[$andIndex]);
            }

            $totalMiddle = count($optionNameArr);
            if ($totalMiddle == 1) {
                $lastNameArr = explode('-', $optionNameArr[0]);
                if (count($lastNameArr) == 2) {
                    $namingConventionArr['lastName'] = 1;
                    $namingConventionArr['lastName2'] = 1;
                    $namingConventionArr['hiphen'] = 1;
                } else {
                    $namingConventionArr['lastName'] = 1;
                }
            } else if ($totalMiddle == 2) {
                $namingConventionArr['firstName'] = 1;
                $namingConventionArr['lastName'] = 1;
            } else if ($totalMiddle == 3) {
                if (isset($optionNameArr[1]) && strlen($optionNameArr[1]) == 1) {
                    $namingConventionArr['firstName'] = 1;
                    $namingConventionArr['otherInit'] = 1;
                    $namingConventionArr['lastName'] = 1;
                } else {
                    $namingConventionArr['firstName'] = 1;
                    $namingConventionArr['otherName'] = 1;
                    $namingConventionArr['lastName'] = 1;
                }
            } else if ($totalMiddle == 4) {
                $namingConventionArr['firstName'] = 1;
                
                if(isset($optionNameArr[1]) && strlen($optionNameArr[1]) == 1)
                {
                  $namingConventionArr['otherInit'] = 1;
                }
                else
                {
                    $namingConventionArr['firstName2'] = 1;
                }
                if(isset($optionNameArr[3]) && strlen($optionNameArr[3]) == 1)
                {
                  $namingConventionArr['otherInit2'] = 1;
                }
                else
                {
                    $namingConventionArr['otherName'] = 1;
                }
                $namingConventionArr['lastName'] = 1;
            } else if ($totalMiddle == 5) {
                if (strlen($optionNameArr[1]) == 1 && strlen($optionNameArr[4]) == 1) {
                    $namingConventionArr['firstName'] = 1;
                    $namingConventionArr['firstName2'] = 1;
                    $namingConventionArr['otherInit'] = 1;
                    $namingConventionArr['otherInit2'] = 1;
                    $namingConventionArr['lastName'] = 1;
                }
            }
            else if ($totalMiddle == 6) {
                $namingConventionArr['firstName'] = 1;
                
                if (strlen($optionNameArr[1]) == 1 ) 
                {
                    $namingConventionArr['otherInit'] = 1;
                }
                else
                {
                    $namingConventionArr['otherName'] = 1;
                }
                $namingConventionArr['firstName2'] = 1;
                if (strlen($optionNameArr[4]) == 1) 
                {
                    $namingConventionArr['otherInit2'] = 1;
                }
                $namingConventionArr['otherName'] = 1;
                $namingConventionArr['lastName'] = 1;
            }            
        }
        if (!empty($wohDetail)) {
            $namingConventionArr['honoree_name'] = $this->getFinalNameWoh($wohDetail, $namingConventionArr);
        }

        return $namingConventionArr;
        /* End of Logic for generate wall of honor page */
    }

    /**
     * This function is used to get honoree naming 
     * @param void
     * @return void
     * @author Icreon Tech - NS
     */
    public function getFinalNameWoh($wohDetail, $namingConventionArr) {
        $nameString = '';

        if ($namingConventionArr['The'] == 1) {
            $nameString .= 'The ';
        }
        if ($namingConventionArr['firstName'] == 1) {
            if (isset($wohDetail['first_name_one']) and trim($wohDetail['first_name_one']) != "" and $wohDetail['first_name_one'] != NULL) {
                $nameString .= $wohDetail['first_name_one'] . ' ';
            }
        }  //first_name_one
        if ($namingConventionArr['otherInit'] == 1) {
            if (isset($wohDetail['other_init_one']) and trim($wohDetail['other_init_one']) != "" and $wohDetail['other_init_one'] != NULL) {
                $nameString .= $wohDetail['other_init_one'] . ' ';
            }
        } //other_init_one
        if ($namingConventionArr['and'] == 1) {
            $nameString .= 'and ';
        }
        if ($namingConventionArr['firstName2'] == 1) {
            if (isset($wohDetail['first_name_two']) and trim($wohDetail['first_name_two']) != "" and $wohDetail['first_name_two'] != NULL) {
                $nameString .= $wohDetail['first_name_two'] . ' ';
            }
        }  //first_name_two
        if ($namingConventionArr['otherName'] == 1) {
            if (isset($wohDetail['other_name']) and trim($wohDetail['other_name']) != "" and $wohDetail['other_name'] != NULL) {
                $nameString .= $wohDetail['other_name'] . ' ';
            }
        }  //other_name
        if ($namingConventionArr['otherInit2'] == 1) {
            if (isset($wohDetail['other_init_two']) and trim($wohDetail['other_init_two']) != "" and $wohDetail['other_init_two'] != NULL) {
                $nameString .= $wohDetail['other_init_two'] . ' ';
            }
        }  //other_init_two
        if ($namingConventionArr['lastName'] == 1) {
            if (isset($wohDetail['last_name_one']) and trim($wohDetail['last_name_one']) != "" and $wohDetail['last_name_one'] != NULL) {
                $nameString .= $wohDetail['last_name_one'] . ' ';
            }
        }  //last_name_one
        if ($namingConventionArr['hiphen'] == 1) {
            $nameString .= '- ';
        }
        if ($namingConventionArr['lastName2'] == 1) {
            if (isset($wohDetail['last_name_two']) and trim($wohDetail['last_name_two']) != "" and $wohDetail['last_name_two'] != NULL) {
                $nameString .= $wohDetail['last_name_two'] . ' ';
            }
        } //last_name_two
        if ($namingConventionArr['Family'] == 1) {
            $nameString .= 'Family ';
        }
        if ($namingConventionArr['firstLine'] == 1) {
            if (isset($wohDetail['first_line']) and trim($wohDetail['first_line']) != "" and $wohDetail['first_line'] != NULL) {
                $nameString .= $wohDetail['first_line'] . '<br>';
            }
        } //first_line
        if ($namingConventionArr['secondLine'] == 1) {
            if (isset($wohDetail['second_line']) and trim($wohDetail['second_line']) != "" and $wohDetail['second_line'] != NULL) {
                $nameString .= $wohDetail['second_line'] . ' ';
            }
        }   //second_line 
        return $nameString;
    }

    /**
     * This function is used to get the alternate Spelling
     * @param $word string
     * @return array of words
     * @author Icreon Tech - SR
     */
    public function AlternateSpelling($word) {


        $text_to_check = $word;
        // optional. clean text a bit
        $clean_text_to_check = preg_replace('/[^a-z0-9\-\.!;]+/i', ' ', $text_to_check);
        // get a word list
        $word_list = preg_split('/\s+/', $clean_text_to_check);

        $pspell_config = pspell_config_create("en", null, null, 'utf-8');

        // if the aspell dictionaries that you want are not installed,
        // copy the aspell dictionaries and set the path to the dictionaries here
        pspell_config_data_dir($pspell_config, "/usr/lib64/aspell-0.60");
        pspell_config_dict_dir($pspell_config, "/usr/lib64/aspell-0.60");
        $pspell_link = pspell_new_config($pspell_config);
        $wordsArray = array();
        foreach ($word_list as $word) {

            if (!pspell_check($pspell_link, trim($word))) {
                $suggestions = pspell_suggest($pspell_link, trim($word));

                // echo $word . ' misspelled <br />';
                foreach ($suggestions as $suggestion) {
                    $wordsArray[] = $suggestion;
                    //echo "\t Possible spelling: $suggestion <br />";
                }
            } else {
                // correct spelling
            }
        }
        return $wordsArray;
    }

    /**
     * This function is used to format the currency
     * @return     Float
     * @param Float 
     * @author Icreon Tech - SR
     */
    public function Currencyformat($number) {
        return number_format($number, 2, '.', '');
    }
    /**
     * This function is used to round the amount
     * @return     Float
     * @param Float 
     * @author Icreon Tech - SR
     */    
   public function Roundamount($number) {
        return number_format(round($number, 2),2, '.', '');
    }    

    /**
     * This function is used to format the currency without comman
     * @return     Float
     * @param Float
     * @author Icreon Tech - SR
     */
    public function CurrencyCalculateFormat($number) {
        return number_format($number, 2, '.');
    }

    /**
     * This Action is used to get the membership Id.
     * @param $userId, $param
     * @return this will be membership configration
     * @author Icreon Tech -SR
     */
    public function getMembershipId($userId, $param) {
        $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $userId));
        $membershipDetailArr = array();

        $membershipConfig = array();
        $membershipDetailArr = array();
        $savedSearchInDays = '';
        $savedSearchTypeIds = '';
        if ($userDetail['membership_id'] != '') {
            $membershipDetailArr[] = $userDetail['membership_id'];
            $getMembershipDetail = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
            if ($param == 'annotation') {
                $membershipConfig['is_annotation'] = $getMembershipDetail['is_annotation'];
            } else {
                $savedSearchInDays = explode(',', $getMembershipDetail['savedSearchInDays']);
                $savedSearchTypeIds = explode(',', $getMembershipDetail['savedSearchTypeIds']);
                foreach ($savedSearchTypeIds as $key => $val) {
                    $membershipConfig[$val] = $savedSearchInDays[$key];
                }
            }
        }
        return $membershipConfig;
    }

    /**
     * get date format
     * @param date, and date format
     * @return string input date format
     * @author Icreon Tech - SR
     */
    public function InputDateFormat($date, $format = '') {

//        $this->timeZoneSession = new Container('timeZone');
//        if ($this->timeZoneSession->timeZone != '') {
//            $date = date("Y-m-d H:i:s", strtotime($date) - ($this->timeZoneSession->timeZone * 60));
//        }

        if (strtolower($format) == 'db_datetime_format') {
            $dateArr = array();
            if ($date != '') {
                $dateArr = explode('/', $date);
                $date = $dateArr[2] . '-' . $dateArr[0] . '-' . $dateArr[1]; // "Y-m-d H:i:s", strtotime($date));
            }
        } else if (strtolower($format) == 'db_date_format') {
            $dateArr = array();
            if ($date != '') {
                $dateArr = explode('/', $date);
                $date = $dateArr[2] . '-' . $dateArr[0] . '-' . $dateArr[1]; // "Y-m-d H:i:s", strtotime($date));
            }
        } else if (strtolower($format) == 'displayformat') {
            if (!empty($date)) {
                if (strpos($date, '-') != false) {
                    $dateDataArray = explode('-', substr($date, 0, 10));
                    $date = $dateDataArray[1] . '-' . $dateDataArray[2] . '-' . $dateDataArray[0];
                }
            }
        } else if (strtolower($format) == 'datetomonthformat') {
            $dateArr = array();
            if ($date != '') {
                $dateArr = explode('/', $date);
                $date = $dateArr[1] . ' ' . date("F", mktime(0, 0, 0, $dateArr[0], 10)) . ' ' . $dateArr[2];
            }
        } else if (strtolower($format) == 'monthtodateformat') {
            $dateArr = array();
            if ($date != '') {
                $dateArr = explode(' ', $date);
                $date = sprintf("%02s", date("m", strtotime($dateArr[1]))) . '/' . sprintf("%02s", $dateArr[0]) . '/' . $dateArr[2];
            }
        } else if (strtolower($format) == 'dateformatampm') {
            $dateArr = array();
            $this->timeZoneSession = new Container('timeZone');
            if ($date != '') {
                if ($this->timeZoneSession->timeZone != '') {
                    $date = date("Y-m-d H:i:s", strtotime($date) - ($this->timeZoneSession->timeZone * 60));
                } else {
                    $date = date("Y-m-d H:i:s", strtotime($date));
                }
            }
        }
        else
            $date = date("Y-m-d", strtotime($date));
        return $date;
    }

    /**
     * get date format
     * @param date, and date format
     * @return string output date format
     * @author Icreon Tech - SR
     */
    public function OutputDateFormat($date, $format = '') {

//        $this->timeZoneSession = new Container('timeZone');
//        if ($this->timeZoneSession->timeZone != '') {
//            $date = date("Y-m-d H:i:s", strtotime($date) + ($this->timeZoneSession->timeZone * 60));
//        }
        if (strtolower($format) == 'calender') {
            $dateArr = array();
            if ($date != '') {
                $dateArr = explode(' ', $date);
                $dateArr = explode('-', $dateArr[0]);
                $date = $dateArr[1] . '/' . $dateArr[2] . '/' . $dateArr[0];
            }
        } else if (strtolower($format) == 'displayformat') {
            if (!empty($date)) {
                if (strpos($date, '-') != false) {
                    $dateDataArray = explode('-', substr($date, 0, 10));
                    $date = $dateDataArray[1] . '-' . $dateDataArray[2] . '-' . $dateDataArray[0];
                }
            }

            //if (!empty($date))
            //  $date = date("m-d-Y", strtotime($date));
        } else if (strtolower($format) == 'displayformattime') {
            if (!empty($date))
                $date = date("m-d-Y H:i", strtotime($date));
        }
        else if (strtolower($format) == 'datetomonthformat') {
            $dateArr = array();
            if ($date != '') {
                $dateArr = explode('/', $date);
                $date = $dateArr[1] . ' ' . date("F", mktime(0, 0, 0, $dateArr[0], 10)) . ' ' . $dateArr[2];
            }
        } else if (strtolower($format) == 'monthtodateformat') {
            $dateArr = array();
            if ($date != '') {
                $dateArr = explode(' ', $date);
                $date = sprintf("%02s", date("m", strtotime($dateArr[1]))) . '/' . sprintf("%02s", $dateArr[0]) . '/' . $dateArr[2];
            }
        } else if (strtolower($format) == 'displayformatwithtime') {
            $date = date("m-d-Y H:i", strtotime($date));
        } else if (strtolower($format) == 'displaymonthformat') {
            if ($date != '') {
                $date = date("F jS, Y", strtotime($date));
            }
        } else if (strtolower($format) == 'dateformatampm') {
            $dateArr = array();
            $this->timeZoneSession = new Container('timeZone');
            if ($date != '') {
                if ($this->timeZoneSession->timeZone != '') {
                    $date = date("Y-m-d h:i A", strtotime($date) + ($this->timeZoneSession->timeZone * 60));
                }
            }
        } else if (strtolower($format) == 'displaymonthformat2') {
            if (!empty($date)) {
                if (strpos($date, '-') != false) {
                    $dateDataArray = explode('-', substr($date, 0, 10));
                    $date = date("M d", mktime(0, 0, 0, $dateDataArray[1], $dateDataArray[2])) . ', ' . $dateDataArray[0];
                }
            }
        }
        else
            $date = date("Y-m-d", strtotime($date));

        return $date;
    }

    /**
     * get pdf file
     * @param source and destination files
     * @return string pdf file
     * @author Icreon Tech - SR
     */
    public function convertTifToPDF($strSourceFile, $strDestinationFile) {
        if (file_exists($strSourceFile)) {
            exec("/usr/bin/convert -limit memory 0 -limit map 0 $strSourceFile -compress zip -quality 100 $strDestinationFile");
            return true;
        }else
            return false;
    }

    /**
     * This page is used for display manifest image path on view pages
     * @param 
     * @return image path
     * @author Icreon Tech - SR
     */
    public function GetManifestPath($param = array()) {

$assetsPath = $this->_config['file_upload_path']['assets_url'];
        $applicationSourceId = $this->_config['application_source_id'];

        if (!empty($param['fileName'])) {
            if ($param['dataSource'] == 1 || empty($param['dataSource'])) { // 1 == passenger records
                if ($applicationSourceId == 1) { // on Web
                    $fileName = str_replace(array('tif', 'TIF'), array('jpg', 'jpg'), $param['fileName']); // direct replace
                } else {
                    $fileArray = explode('.', $param['fileName']);
                    $fileExt = $fileArray[1];
                    if (strtolower($fileExt) == 'tif') {
                        // will convert .tif to .jpg file
                    }
                }
                $folderName = substr($fileName, 0, 9);
                if ($param['isThumb'] == 1)
                    $assetsPath = $assetsPath . 'manifest/' . $folderName;
                else
                    $assetsPath = $assetsPath . 'manifest/' . $folderName;

                if ($param['dataSource'] == 1) {
                    $fileName = str_replace(array('jpg', 'jpg'), array('tif', 'TIF'), $param['fileName']); // need to remove this line
                    if(isset($param['imageSource']) && $param['imageSource'] == 'frontEnd')
                        $assetsPath = $config['Image_path']['manifestPath_frontEnd'].substr($fileName,0,9); 
                    else                    
                        $assetsPath = $config['Image_path']['manifestPath'] . substr($fileName, 0, 9); 
                    return $filePath = $assetsPath . '/' . $fileName . '&S=.4';
                } else {
                    //$fileName = str_replace(array('jpg', 'jpg'), array('tif', 'TIF'), $param['fileName']); // need to remove this line
                    //$assetsPath = $config['Image_path']['manifestPath'].substr($fileName,0,9); //need to remove this line
                    return $filePath = $assetsPath . '/' . $fileName;
                }
            } else if ($param['dataSource'] == 2) { // 1 == Family records
                $folderName = substr($param['fileName'], 0, 9);

                if ($param['isThumb'] == 1)
                    $assetsPath = $assetsPath . 'manifest/' . $folderName;
                else
                    $assetsPath = $assetsPath . 'manifest/' . $folderName;

                return $filePath = $assetsPath . '/' . $param['fileName'];
            }
        }	
    }

    /**
     * This function is used for transfer type
     * @return     array
     * @param void
     * @author Icreon Tech - DT
     */
    public function transTypeArray() {

        $transType = array();
        $transType[0] = "";
        $transType[1] = "Sold to";
        $transType[2] = "Transferred to";
        $transType[3] = "Returned to";
        $transType[4] = "Laid up";
        $transType[5] = "Torpedoed and sunk";
        $transType[6] = "Scrapped";
        $transType[7] = "Reported missing at sea";
        $transType[8] = "Chartered by";
        $transType[9] = "Wrecked";
        $transType[10] = "Sunk";
        $transType[11] = "Seized by";
        $transType[12] = "Intended to be the";
        $transType[13] = "Given as reparations to";
        $transType[14] = "Renamed";
        return $transType;
    }

    /**
     * This function is used for how ship name changed
     * @return     array
     * @param void
     * @author Icreon Tech - SR
     */
    public function howNameChangeArray() {
        $howNameChangeType = Array();
        $howNameChangeType[0] = "";
        $howNameChangeType[1] = "renamed";
        $howNameChangeType[2] = "reverted to";
        $howNameChangeType[3] = "named";
        $howNameChangeType[4] = "not changed";
        $howNameChangeType[5] = "intended to be the";
        return $howNameChangeType;
    }

    /**
     * This function is used for line less trans type
     * @return     array
     * @param void
     * @author Icreon Tech - SR
     */
    public function lineLessTransTypeArray() {
        $lineLessTransType = Array();
        $lineLessTransType[0] = "";
        $lineLessTransType[1] = "Sold";
        $lineLessTransType[2] = "Transferred";
        $lineLessTransType[3] = "Returned";
        $lineLessTransType[4] = "Laid up";
        $lineLessTransType[5] = "Torpedoed and sunk";
        $lineLessTransType[6] = "Scrapped";
        $lineLessTransType[7] = "Reported missing at sea";
        $lineLessTransType[8] = "Chartered";
        $lineLessTransType[9] = "Wrecked";
        $lineLessTransType[10] = "Sunk";
        $lineLessTransType[11] = "Seized";
        $lineLessTransType[12] = "Intended to be the";
        $lineLessTransType[13] = "Given as reparations";
        $lineLessTransType[14] = "Renamed";
        return $lineLessTransType;
    }

    /**
     * This function is used ship engine type
     * @return     array
     * @param void
     * @author Icreon Tech - SR
     */
    public function engineTypeArray() {
        $engineTypeName = Array();
        $engineTypeName[0] = "";
        $engineTypeName[1] = "Steam triple expansion";
        $engineTypeName[2] = "Compound";
        $engineTypeName[3] = "Steam quadruple expansion";
        $engineTypeName[4] = "Steam turbine";
        $engineTypeName[5] = "Steam turbine and triple expansion";
        $engineTypeName[6] = "Inverted";
        return $engineTypeName;
    }

    /**
     * This function is used for screw name
     * @return     array
     * @param void
     * @author Icreon Tech - SR
     */
    public function screwNameArray() {
        $screwName = Array();
        $screwName[0] = "";
        $screwName[1] = "single screw";
        $screwName[2] = "twin screw";
        $screwName[3] = "triple screw";
        $screwName[4] = "quadruple screw";
        return $screwName;
    }

    /**
     * @author Icreon Tech - SK
     * gets the data from a URL */
    public function get_data($url) {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
    
    /**
     * @author Icreon Tech - SK
     * gets the data from a URL */
    public function removeSpecialChar($str){
        $spclCharArr = array('&');
        $replacementArr = array('and');
        $str = str_replace($spclCharArr,$replacementArr,$str);        
        return $str;
    }
public function convertTifToJpg($imageName){
	                    $fileArray = explode('.', $imageName);
                    $fileExt = $fileArray[1];
                    if (strtolower($fileExt) == 'tif') {
                        $fileName = substr($imageName,0,-4);
                        $fileName = $fileName.'.jpg';    
                    }
                    else {
                        $fileName = $imageName;
                    }
                    return $fileName;
	}

}