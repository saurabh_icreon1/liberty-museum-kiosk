<?php

/**
 * Global Configuration Override
 *
 * You can use this file for overriding configuration values from modules, etc.
 * You would place values in here that are agnostic to the environment and not
 * sensitive to security.
 *
 * @NOTE: In practice, this file will typically be INCLUDED in your source
 * control, so do not include passwords or other sensitive information in this
 * file.
 */
return array('view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'not_found_layout' => 'layout/error.phtml',
        'template_map' => array(
            'layout/layout' => __DIR__ . '/../../module/Common/layout/layout.phtml',
            'error/404' => __DIR__ . '/../../module/Common/error/404.phtml',
            'error/index' => __DIR__ . '/../../module/Common/error/index.phtml',
        ),
    ),
    'css_file_path' => array(
        'path' => '//elliskiosk.ellisisland.org'
    ),
    'img_file_path' => array(
        'path' => '//elliskiosk.ellisisland.org'
    ),
    'js_file_path' => array(
        'path' => '//elliskiosk.ellisisland.org' 
    ),
    'css_file_path_https' => array(
        'path' => 'https://elliskiosk.ellisisland.org'
    ),
    'img_file_path_https' => array(
        'path' => 'https://elliskiosk.ellisisland.org' 
    ),
    'js_file_path_https' => array(
        'path' => 'https://elliskiosk.ellisisland.org' 
    ),
	'file_versions' => array(
		'js_version' => '96',
		'css_version' => '87'
	),
    'https_url' => array(
        'get-crm-cart-products',
        'checkout',
        'get-credit-card-detail',
        'add-product-to-cart-detail',
        'checkout#checkout_section',
        'front-ads',
        'get-flag-of-faces-add-to-cart-detail',
        'upload-user-fof-image-iframe',
        'upload-user-fof-image-iframe-preview',
        'get-passenger-document-add-to-cart-detail',
        'get-donation-add-to-cart-detail',
        'get-kiosk-fee-add-to-cart-detail',
        'get-inventory-add-to-cart-detail',
        'get-membership-add-to-cart-detail',
        'get-wall-of-honor-add-to-cart-detail',
        'get-passenger-record-info',
        'generate-woh-form-by-naming-convention',
        'get-wall-of-honor-bio-certificate-search',
        'select-batch-id',
        'orderreview-cart-front',
	 'pay-purchase-session',
        'get-swipe-data',
        'purchase-session-zipcode',
        'purchase-session',
        'add-reservation-contact',
        'pos-donation',
        'pay-pos-donation',
        'pos-donation-zipcode',
    ),
     'authorize_net_payment_gateway' => array(
       'API_LOGIN_ID' => '5s8fDJ4n',
        'TRANSACTION_KEY' => '3KMdQ8x7m96eJz38',
        'LOGIN_ID' => 'Tolentino1',
        'PASSWORD' => 'L1b3rty123!',
        'ENV' => 'AuthnetXML::USE_PRODUCTION_SERVER'

 	 /*'API_LOGIN_ID' => '7T5jj59A2',
        'TRANSACTION_KEY' => '8Vs89j3UU73F5yx5',
        'LOGIN_ID' => 'SOLEIF0098',
        'PASSWORD' => 'Icreon0098#',
        'ENV' => 'AuthnetXML::USE_DEVELOPMENT_SERVER'*/

    ),
    'file_upload_path' => array(
        'assets_url' => '//elliskioskassets.ellisisland.org/',
        'assets_url_https' => 'https://elliskioskassets.ellisisland.org/',

        /*'assets_url' => 'http://assets.libertyellisfoundation.org/',
        'assets_url_https' => 'https://assets.libertyellisfoundation.org/',*/

        'assets_upload_dir' => dirname($_SERVER['DOCUMENT_ROOT']) . '/assets/',
        'temp_upload_dir' => dirname($_SERVER['DOCUMENT_ROOT']) . '/assets/temp/',
        'temp_data_upload_dir' => dirname($_SERVER['DOCUMENT_ROOT']) . '/data/temp/',
        'temp_upload_thumbnail_dir' => dirname($_SERVER['DOCUMENT_ROOT']) . '/assets/temp/thumbnail/',
        'temp_upload_medium_dir' => dirname($_SERVER['DOCUMENT_ROOT']) . '/assets/temp/medium/',
        'temp_upload_large_dir' => dirname($_SERVER['DOCUMENT_ROOT']) . '/assets/temp/large/',
        'temp_upload_small_dir' => dirname($_SERVER['DOCUMENT_ROOT']) . '/assets/temp/small/',
        'temp_upload_extralarge_dir' => dirname($_SERVER['DOCUMENT_ROOT']) . '/assets/temp/extralarge/',
        'file_types_allowed' => '/\.(gif|jpe?g|png)$/i',
        'doc_file_types_allowed' => '/\.(doc|docx|pdf|jpe?g|csv)$/i',
        'max_allowed_file_size' => ((pow(1024, 2)) * 2), //in byte 2 mb
        'min_allowed_file_size' => ((pow(1024, 1)) * 1), //in byte 1 kb
        'gallery_file_type' => '/\.(jpe?g|png|bmp|mp4)$/i',
        'gallery_file_size' => ((pow(1024, 2)) * 50),
        'user_thumb_height' => 160,
        'user_thumb_width' => 150,
        'user_medium_height' => 240,
        'user_medium_width' => 240,
        'family_history_small_height' => 50,
        'family_history_small_width' => 50,
        'family_history_medium_height' => 159,
        'family_history_medium_width' => 212,
        'family_history_large_height' => 231,
        'family_history_large_width' => 406,
        'family_history_extralarge_height' => 268,
        'family_history_extralarge_width' => 592,
        'user_right_ads_width' => 359,
        'user_right_ads_height' => 299,
        'user_footer_ads_width' => 722,
        'user_footer_ads_height' => 122,
        'user_fof_main_width' => 710,
        'user_fof_main_height' => 710,
        'user_fof_medium_width' => 72,
        'user_fof_medium_height' => 72,
        'user_fof_large_width' => 109,
        'user_fof_large_height' => 117,
		'user_home_slider_main_width' => 1400,
        'user_home_slider_main_height' => 573,
        'user_home_slider_thumbnail_width' => 150,
        'user_home_slider_thumbnail_height' => 61,
        'product_thumb_width' => 166,
        'product_thumb_height' => 126,
        'product_large_width' => 600,
        'product_large_height' => 600,
        'product_small_width' => 55,
        'product_small_height' => 50,        
        'local_ship_image_path' => '//elliskioskassets.ellisisland.org/ship_image',
        'local_manifest_image_path' => '//elliskioskassets.ellisisland.org',
        'local_passenger_manifest_image_path' => '//elliskioskassets.ellisisland.org',
        'local_internet_ip' => '127.0.0.1',
        'oral_history_assets' => '//elliskioskassets.ellisisland.org/oralhistory/',
		'oral_history_assets_upload_dir' => dirname($_SERVER['DOCUMENT_ROOT']) . '/assets/oralhistory/',
    ),
    'application_config_ids' => array(
        'united_state_country_id' => 228,
        'domestic_pb_id' => 1,
        'apo_po_pb_id' => 2,
        'internation_pb_id' => 3,
        'internation_less_than_lbs_id' => 8,
        'internation_greater_than_lbs_id' => 7,
    ),
    'financial_year' => array(
        'srart_day' => '01',
        'srart_month' => '01'
    ),
    'get_time_zone_url' => array(
        'get_time_zone_url' => '1' // 1- from client machine, 2-from IP address
    ),
    'recaptcha' => array(
        'name' => 'recaptcha',
        'privKey' => '6LdgjekSAAAAACvQJcSUt8CMTp9BcqQ7dqHibiQW',
        'pubKey' => '6LdgjekSAAAAAOism0TRjcxZ8YqGLG63uO-62ALM',
    ),
	'user_blocked' => array(
        'block_time_limit' => 30,
        'invalid_attempt' => 3
    ),
    'crm_user_blocked' => array(
        'block_time_limit' => 30,
        'invalid_attempt' => 3
    ),
    'facebook_app' => array(
        'app_id' => '642672242496504',
        'secret' => '36d4dab19e0d83129abe20a06940ace7'
    ),
	 'allowed_ip' => array(
	 'ip' => '127.0.0.1'
	),
    'transaction_source' => array(
        'transaction_source_id' => '1',
        'transaction_source_crm_id' => '2'
    ),
    'application_source_id' => '1', //1 for web, 2 for kiosk
    'imageConvertCommandPath' => 'C:/imageMagic/convert', 
    'custom_frame_id' => '2',
    'terms_content_id' => '3',
    'https_on' => '1',  // '1' for on '0' for off
    'membership_plus' => '60', // number of days
    'request_print_certificate_email_id' => 'sanjeev.rajput@icreon.com', // request print certificate email id
    'grid_config' => array(
        'numRecPerPage' => 10,
        'numPageSelect' => '10,20,30,40'
    ),
   'Image_path' => array( 
        //'manifestPath' => 'http://174.143.111.10/cgi-bin/tif2gif.exe?T=//192.168.100.11\IMAGES/',
        'manifestPath' => '//elliskioskassets.ellisisland.org/manifest-low/',
        //'shipPath' => 'http://174.143.111.10/shipping/shipImages/',
        'shipPath' => '//elliskioskassets.ellisisland.org/ship_image/',
        'manifestPath_frontEnd' => '//174.143.111.10/cgi-bin/tif2gif.exe?T=//192.168.100.11\IMAGES/'
    ),
    'phpSettings' => array(
        'display_startup_errors' => false,
        'display_errors' => false,
        'max_execution_time' => 60,
        'date.timezone' => 'UTC',
        'mbstring.internal_encoding' => 'UTF-8',
    ),
    'SphinxServer' => array(
         /*'serverName'=>'172.16.4.71',
          'passengerIndexes' => array('passenger'),
          'passengerIndexesIndexWeights' => array('passenger' => 10000),*/

        'serverName' => '10.20.108.24',
        'passengerIndexes' => array('passenger1', 'passenger2', 'passenger3', 'passenger4', 'passenger5', 'passenger6', 'passenger7', 'passenger8', 'passenger9', 'passenger10', 'passenger11', 'passenger12', 'passenger13', 'passenger14', 'passenger15', 'passenger16', 'passenger17', 'passenger18', 'passenger19', 'passenger20', 'passenger21', 'passenger22', 'passenger23', 'passenger24', 'passenger25','passenger26','passenger27','passenger28','passenger29','passenger30','passenger31','passenger32','passenger33','passenger34','passenger35','passenger36','passenger37','passenger38','passenger39','passenger40','passenger41','passenger42','passenger43','passenger44','passenger45','passenger46','passenger47','passenger48','passenger49','passenger50','passenger51','passenger52','passenger53','passenger54','passenger55','passenger56','passenger57','passenger58','passenger59','passenger60','passenger61','passenger62','passenger63','passenger64','passenger65','passenger66'),
	 'passengerIndexesIndexWeights' => array('passenger1' => 10000, 'passenger2' => 10000, 'passenger3' => 10000, 'passenger4' => 10000, 'passenger5' => 10000, 'passenger6' => 10000, 'passenger7' => 10000, 'passenger8' => 10000, 'passenger9' => 10000, 'passenger10' => 10000, 'passenger11' => 10000, 'passenger12' => 10000, 'passenger13' => 10000, 'passenger14' => 10000, 'passenger15' => 10000, 'passenger16' => 10000, 'passenger17' => 10000, 'passenger18' => 10000, 'passenger19' => 10000, 'passenger20' => 10000, 'passenger21' => 10000, 'passenger22' => 10000, 'passenger23' => 10000, 'passenger24' => 10000, 'passenger25' => 10000,'passenger26' => 10000,'passenger27' => 10000, 'passenger28' => 10000, 'passenger29' => 10000, 'passenger30' => 10000,'passenger31' => 10000,'passenger32' => 10000, 'passenger33' => 10000, 'passenger34' => 10000, 'passenger35' => 10000,'passenger36' => 10000,'passenger37' => 10000, 'passenger38' => 10000, 'passenger39' => 10000, 'passenger40' => 10000,'passenger41' => 10000,'passenger42' => 10000, 'passenger43' => 10000, 'passenger44' => 10000, 'passenger45' => 10000,'passenger46' => 10000,'passenger47' => 10000, 'passenger48' => 10000, 'passenger49' => 10000, 'passenger50' => 10000,'passenger51' => 10000,'passenger52' => 10000,'passenger53' => 10000,'passenger54' => 10000,'passenger55' => 10000,'passenger56' => 10000,'passenger57' => 10000,'passenger58' => 10000,'passenger59' => 10000,'passenger60' => 10000,'passenger61' => 10000,'passenger62' => 10000,'passenger63' => 10000,'passenger64' => 10000,'passenger65' => 10000,'passenger66' =>10000),        'shipIndexes' => array('ship'),
        'shipIndexesIndexWeights' => array('ship' => 10000),
    ),
    'export' => array(
        'chunk_size' => 3000,
        'export_limit' => 5000
    ),
    'cache' => array(
        'enable' => false,
        'ttl' => '86400'
    ),
    'memcache' => array(
        'adapter' => array(
            'name' => 'memcached',
            'options' => array(
                'namespace' => 'auth-guardian',
                'servers' => array(
                    array('172.16.4.71', 11211)
                )
            ),
        )
    ),
    'allowed_ip' => array(
            'ip' => '108.166.119.79'
    ),
    'csv_file' => array(
            'max_records' => 100000,
            'db_limits' => 10000
    ),
    'kiosk_reservation_system'=> array(
            '192.168.4.106'=>'32',
            '192.168.4.27'=>'31',
            '192.168.4.104'=>'30',
            
  
    ),
      'kiosk-system-settings'=>array(
        '210.7.79.164'=>array('isHotSystem'=>0)
        ),
    
    'user_visit_record'=>array(
        "birth_year"=>range(1950,2014),
        "immagration_year"=>range(1950,2014),
        "occupation"=>array("Doctor","Eng","Teacher"),
    ),
    
    'ellis_default' => array(
	'membership_expiration_days' => '90'
    ),
    'ajax_refresh_time'=>array(
 	"pauseRefreshTime"=>120000,
 	"systemRefreshTime"=>300000,
    ),
    'printBtn' => array(
	'btn_disabled_duration' => 5000 // 5 seconds,
    ),
    'wohKisok' => array(
       'inactive_session_alert_time' => 20000, // 2 minutes
	'timer_start_time' => 10, // 5 seconds
    ),

);
