<?php
/**
 * This is used for setting module configuration details
 * @package    Setting
 * @author     Icreon Tech - AS
 */
return array(
    'controllers' => array(
        'invokables' => array(
            'Setting\Controller\Setting' => 'Setting\Controller\SettingController',
            'Setting\Controller\Program' => 'Setting\Controller\ProgramController'
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'getCrmMstTableList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-mst-table-lists',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Setting',
                        'action' => 'getCrmMstTableList',
                    ),
                ),
            ),
            'setting' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/setting',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Setting',
                        'action' => 'Setting',
                    ),
                ),
            ),
            'getCrmMstTableContentList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-mst-table-content-lists[/:id]',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Setting',
                        'action' => 'getCrmMstTableContentList',
                    ),
                ),
            ),
            'getDropdownContent' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-dropdown-content[/:table]',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Setting',
                        'action' => 'getDropdownContent',
                    ),
                ),
            ),
            'addCrmMstTableContent' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-crm-mst-table-content[/:table][/:field1][/:field2]',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Setting',
                        'action' => 'addCrmMstTableContent',
                    ),
                ),
            ),
            'manageStockNotification' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/manage-stock-notification',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Setting',
                        'action' => 'manageStockNotification',
                    ),
                ),
            ),
            'editCrmMstTableContent' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-mst-table-content[/:id][/:field][/:field2]',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Setting',
                        'action' => 'editCrmMstTableContent',
                    ),
                ),
            ),
            'deleteCrmMstTableContent' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-mst-table-content[/:id]',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Setting',
                        'action' => 'deleteCrmMstTableContent',
                    ),
                ),
            ),
            'getcrmemailtemplates' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-email-templates',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Setting',
                        'action' => 'getCrmEmailTemplates',
                    ),
                ),
            ),
            'crmemailtemplateinfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-email-template-info[/:id][/:mode]',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Setting',
                        'action' => 'crmEmailTemplateInfo',
                    ),
                ),
            ),
            'getcrmemailtemplatelist' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-email-template',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Setting',
                        'action' => 'getCrmEmailTemplateList',
                    ),
                ),
            ),
            'getcrmemailtemplateinfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-email-templateinfo[/:id]',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Setting',
                        'action' => 'getCrmEmailTemplateinfo',
                    ),
                ),
            ),
            'editCrmEmailTemplate' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-email-template[/:id]',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Setting',
                        'action' => 'editCrmEmailTemplate',
                    ),
                ),
            ),
            
            'manageGlCodes' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/manage-gl-codes',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Setting',
                        'action' => 'manageGlCodes',
                    ),
                ),
            ),
            'addCrmMembership' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-crm-membership',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Setting',
                        'action' => 'addCrmMembership',
                    ),
                ),
            ),
            'sendemailtemplate' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/send-email-template',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Setting',
                        'action' => 'sendEmailTemplate',
                    ),
                ),
            ),
            'addSaveSearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-save-search',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Setting',
                        'action' => 'addSaveSearch',
                    ),
                ),
            ),
            'getCrmMembership' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-membership',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Setting',
                        'action' => 'getCrmMembership',
                    ),
                ),
            ),
            'deleteCrmMembership' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-membership',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Setting',
                        'action' => 'deleteCrmMembership',
                    ),
                ),
            ),
            'changeCrmMembershipStatus' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/change-crm-membership-status',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Setting',
                        'action' => 'changeCrmMembershipStatus',
                    ),
                ),
            ),
            'editCrmMembership' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-membership[/:membership_id]',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Setting',
                        'action' => 'editCrmMembership',
                    ),
                ),
            ),
            
            'getMembershipDetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-membership-detail',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Setting',
                        'action' => 'getMembershipDetail',
                    ),
                ),
            ),
            'editDropdownDragDrop' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-dropdown-drag-drop',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Setting',
                        'action' => 'editDropdownDragDrop',
                    ),
                ),
            ),
            'updateOrder' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/update-order',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Setting',
                        'action' => 'updateOrder',
                    ),
                ),
            ),
            
            // program - start
            
             'getProgramPopupList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-program-popup-list',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Program',
                        'action' => 'getProgramPopupList',
                    ),
                ),
            ),
            'getProgramPopupData' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-program-popup-list-data',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Program',
                        'action' => 'getProgramPopupData',
                    ),
                ),
             ),
            
             'getProgramList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-program-list',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Program',
                        'action' => 'getProgramList',
                    ),
                ),
            ),
            'getProgramData' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-program-list-data',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Program',
                        'action' => 'getProgramData',
                    ),
                ),
             ),
            'createCrmProgram' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-crm-program',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Program',
                        'action' => 'createCrmProgram',
                    ),
                ),
             ),
             'checkProgramCode' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-check-program-code',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Program',
                        'action' => 'checkProgramCode',
                    ),
                ),
             ),
            'addProgramProcess' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-program-process',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Program',
                        'action' => 'addProgramProcess',
                    ),
                ),
             ),
            
           'editProgramDetails' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/modify-program[/:program_id]',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Program',
                        'action' => 'editProgramDetails',
                    ),
                ),
             ),
            
           'deleteProgram' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/remove-program',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Program',
                        'action' => 'deleteProgram',
                    ),
                ),
             ),
            
             'getProgram' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-get-program',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Program',
                        'action' => 'getProgram',
                    ),
                ),
             ),
            
             'viewProgram' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-program-detail[/:program_id]',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Program',
                        'action' => 'viewProgram',
                    ),
                ),
             ),
            
             'getProgramDetails' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-program-detail[/:program_id][/:mode]',
                    'defaults' => array(
                        'controller' => 'Setting\Controller\Program',
                        'action' => 'getProgramDetails',
                    ),
                ),
             ),
            
            //
            // program - end
            
        )
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'navigation' => array(
        'default' => array(
             array(
                'label' => 'Program',
                'route' => 'getProgramList',
                'pages' => array(
                    array(
                    'label' => 'Search',
                    'route' => 'getProgramList',
                        'pages' => array(
                                array(
                                    'label' => 'Create',
                                    'route' => 'createCrmProgram',
                                    'action' => 'createCrmProgram',
                                ),
                                array(
                                    'label' => 'View',
                                    'route' => 'viewProgram',
                                    'action' => 'viewProgram',
                                ),
                                array(
                                    'label' => 'Edit',
                                    'route' => 'editProgramDetails',
                                    'action' => 'editProgramDetails',
                                )
                           )
                        )
                    )            
              ),
            array(
                'label' => 'Settings',
                'route' => 'setting',
                'pages' => array(
                    array(
                        'label' => 'Manage Stock Notification',
                        'route' => 'manageStockNotification'
                    ),
                    array(
                        'label' => 'List',
                        'route' => 'getCrmMstTableList'
                    ),
                )
            ),
             array(
                'label' => 'Email Templates',
                'route' => 'getcrmemailtemplates',
                'pages' => array(
                    array(
                        'label' => 'Search',
                        'route' => 'getcrmemailtemplates',
                        'pages' => array(
                            array(
                                'label' => 'View',
                                'route' => 'getcrmemailtemplateinfo',
                                'action' => 'getCrmEmailTemplateinfo',
                            ),
                            array(
                                'label' => 'Edit',
                                'route' => 'editCrmEmailTemplate',
                                'action' => 'editCrmEmailTemplate',
                            )
                        ),
                    )
                ),
            ),
            array(
                'label' => 'Membership',
                'route' => 'getCrmMembership',
                'pages' => array(
                    array(
                        'label' => 'Search',
                        'route' => 'getCrmMembership',
                        'pages' => array(
                            array(
                                'label' => 'Create',
                                'route' => 'addCrmMembership',
                                'action' => 'addCrmMembership',
                            ),
                            array(
                                'label' => 'Edit',
                                'route' => 'editCrmMembership',
                                'action' => 'editCrmMembership',
                            )
                        ),
                    )
                ),
            ),
        )
    ),
    'setting_messages' => array(
        'config' => include __DIR__ . '/../languages/en/language.php',
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
            'navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory'
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Setting' => __DIR__ . '/../view'
        ),
        'template_map' => array(
            'layout/custom' => __DIR__ . '/../../module/Common/layout/crm.phtml',
            'error/404' => __DIR__ . '/../../module/Common/error/404.phtml',
            'error/index' => __DIR__ . '/../../module/Common/error/index.phtml',
        ),
    ),
    'global_config' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
    'table_config' => array(
        1 => array(
            'id' => '',
            'field1' => ''
        ),
        2 => array(
            'id' => 'campaign_status_id',
            'field1' => 'campaign_status'
        ),
        3 => array(
            'id' => 'campaign_type_id',
            'field1' => 'campaign_type'
        ),
        4 => array(
            'id' => 'case_status_id',
            'field1' => 'case_status'
        ),
        5 => array(
            'id' => 'case_type_id',
            'field1' => 'case_type'
        ),
        6 => array(
            'id' => 'source_id',
            'field1' => 'source_name'
        ),
        7 => array(
            'id' => 'contribution_type_id',
            'field1' => 'contribution_type'
        ),
        8 => array(
            'id' => 'education_id',
            'field1' => 'education'
        ),
        9 => array(
            'id' => 'lead_source_id',
            'field1' => 'lead_source'
        ),
        10 => array(
            'id' => 'lead_type_id',
            'field1' => 'lead_type'
        ),
        11 => array(
            'id' => 'location_id',
            'field1' => 'location'
        ),
        12 => array(
            'id' => 'nationality_id',
            'field1' => 'nationality'
        ),
        13 => array(
            'id' => '',
            'field1' => ''
        ),
        14 => array(
            'id' => 'pledge_status_id',
            'field1' => 'pledge_status'
        ),
        15 => array(
            'id' => '',
            'field1' => ''
        ),
        16 => array(
            'id' => 'relationship_id',
            'field1' => 'relationship'
        ),
        17 => array(
            'id' => '',
            'field1' => ''
        ),
        18 => array(
            'id' => 'channel_id',
            'field1' => 'channel'
        ),
        19 => array(
            'id' => 'quiz_category_id',
            'field1' => 'category_name'
        ),
        20 => array(
            'id' => 'category_id',
            'field1' => 'category_name'
        ),
        21 => array(
            'id' => 'ethnicity_id',
            'field1' => 'ethnicity'
        )
    ),
    'membership_settings' => array(
        'numOfYears' => 5,
        'savedSearchDays' => 20
    )
);