<?php

/**
 * This file is  used for create form for membership saved search
 * @package    Setting
 * @author     Icreon Tech - DT.
 */

namespace Setting\Form;

use Zend\Form\Form;

/**
 * This class is  used for create form for membership saved search
 * @package    Setting
 * @author     Icreon Tech - DT.
 */
class MembershipSavedSearch extends Form {

    public function __construct($name = null) {
        parent::__construct('membership_save_search');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'saved_search_in_days[]',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'class' => 'e1 select-w-150 m-r-10'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'saved_search_type[]',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'class' => 'e1 left m-r-10'
            )
        ));
    }
}