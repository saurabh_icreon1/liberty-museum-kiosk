<?php

/**
 * This file is  used for create form for create and edit membership.
 * @package    Setting
 * @author     Icreon Tech - DT.
 */

namespace Setting\Form;

use Zend\Form\Form;

/**
 * This class is  used for create form for create and edit membership.
 * @package    Setting
 * @author     Icreon Tech - DT.
 */
class MembershipForm extends Form {

    public function __construct($name = null) {
        parent::__construct('user_membership');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'membership_id',
            'attributes' => array(
                'type' => 'hidden',
            ),
        ));
        $this->add(array(
            'name' => 'membership_title',
            'attributes' => array(
                'type' => 'text',
                'id' => 'membership_title'
               
            ),
        ));
        $this->add(array(
            'name' => 'email_set_text',
            'type' => 'Checkbox',
            'attributes' => array(
                'value' => '0',
                'id' => 'email_set_text',
                'class' => 'e2',
                'onclick' => 'setPlainText(this)'
            ),
            'options' => array(
                'value_options' => array(
                    '0' => 'Checkbox',
                    '1' => 'Checkbox',
                ),
            ),
        ));
        
       $this->add(array(
            'name' => 'membership_description',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'membership_description',
                'class' => 'myTextEditor width-250'
            ),
        ));
        $this->add(array(
            'name' => 'level_color',
            'attributes' => array(
                'type' => 'text',
                'id' => 'level_color',
                'class' => 'color-pic',
                'readonly' => 'readonly'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'validity_type',
            'options' => array(
                'value_options' => array(
                    '' => 'Select One',
                    '1' => 'Calendar Year',
                    '2' => 'Start Date',
                ),
            ),
            'attributes' => array(
                'id' => 'validity_type',
                'class' => 'e1',
                'onChange' => 'changeValidityType(this.value)'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'validity_time_financial',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'validity_time_financial',
                'class' => 'e1'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'validity_time_year',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'validity_time_year',
                'class' => 'e1 '
            )
        ));
        $this->add(array(
            'name' => 'minimun_donation_amount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'minimun_donation_amount'
              
            ),
        ));
        $this->add(array(
            'name' => 'first_alert_before_in_days',
            'attributes' => array(
                'type' => 'text',
                'id' => 'first_alert_before_in_days',
				'class' => 'width-386'
               
            ),
        ));
        $this->add(array(
            'name' => 'repeat_alerts_in_days',
            'attributes' => array(
                'type' => 'text',
                'id' => 'repeat_alerts_in_days',
                'class' => 'width-155'
            ),
        ));
        $this->add(array(
            'name' => 'repeat_alerts_in_times',
            'attributes' => array(
                'type' => 'text',
                'id' => 'repeat_alerts_in_times',
                'class' => 'width-155'
            ),
        ));
        $this->add(array(
            'name' => 'renew_alerts_in_days',
            'attributes' => array(
                'type' => 'text',
                'id' => 'renew_alerts_in_days',
                'class' => 'width-155'
            ),
        ));
        $this->add(array(
            'name' => 'renew_alerts_in_times',
            'attributes' => array(
                'type' => 'text',
                'id' => 'renew_alerts_in_times',
                'class' => 'width-155'
            ),
        ));
        $this->add(array(
            'name' => 'come_back_request_in_days',
            'attributes' => array(
                'type' => 'text',
                'id' => 'come_back_request_in_days',
                'class' => 'width-310',
            ),
        ));
        $this->add(array(
            'name' => 'discount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'discount',
                'class' => 'width-134'
            ),
        ));
        $this->add(array(
            'name' => 'saved_search_validity_in_days',
            'attributes' => array(
                'type' => 'text',
                'id' => 'saved_search_validity_in_days',
                'class' => 'width-134 m-r-10'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'saved_search_validity_type',
            'options' => array(
                'value_options' => array(
                    '1' => "Last Login",
                    '2' => "Creation Date"
                ),
            ),
            'attributes' => array(
                'id' => 'saved_search_validity_type',
                'class' => 'e1'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_annotation',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'class' => 'checkbox',
                'id' => 'is_annotation',
                'class' => 'e2'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_correction',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'class' => 'checkbox',
                'id' => 'is_correction',
                'class' => 'e2'
            )
        ));    
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_active',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'class' => 'checkbox',
                'id' => 'is_active',
                'class' => 'e2'
            )
        ));  
        $this->add(array(
            'name' => 'max_stories',
            'attributes' => array(
                'type' => 'text',
                'id' => 'max_stories'
              
            ),
        ));
        $this->add(array(
            'name' => 'max_story_characters',
            'attributes' => array(
                'type' => 'text',
                'id' => 'max_story_characters',
               ),
        ));
        $this->add(array(
            'name' => 'max_passengers',
            'attributes' => array(
                'type' => 'text',
                'id' => 'max_passengers',
                
            ),
        ));
        $this->add(array(
            'name' => 'max_saved_histories',
            'attributes' => array(
                'type' => 'text',
                'id' => 'max_saved_histories',
            
            ),
        ));

        $this->add(array(
            'name' => 'save',
            'attributes' => array(
                'type' => 'Submit',
                'id' => 'save',
                'class' => 'save-btn',
                'value' => 'SAVE'
            ),
        ));
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'id' => 'cancel',
                'class' => 'cancel-btn m-l-10',
                'value' => 'CANCEL',
                'onclick' => 'window.location.href="/crm-membership"'
            )
        ));
    }

}