<?php

/**
 * This form is used to create search program form
 * @package    User
 * @author     Icreon Tech - NS
 */

namespace Setting\Form;

use Zend\Form\Form;

/**
 * This clss is used to create search crm users form elemets
 * @package    User
 * @author     Icreon Tech - NS
 */
class SearchCrmProgramForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('search');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'program',
            'attributes' => array(
                'type' => 'text',
                'id' => 'program',
            )
        ));
        $this->add(array(
            'name' => 'program_code',
            'attributes' => array(
                'type' => 'text',
                'id' => 'program_code'
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'is_available_for_donation',
            'options' => array(
                'value_options' => array(
                    '' => 'All',
                    '1' => 'Yes',
                    '0' => 'No'
                ),
            ),
            'attributes' => array(
                'id' => 'is_available_for_donation',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Search',
                'id' => 'submitbutton',
                'class' => 'search-btn'
            ),
        ));

    }

}