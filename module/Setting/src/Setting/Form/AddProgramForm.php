<?php

/**
 * This form is used to create a program
 * @package    Document
 * @author     Icreon Tech - NS
 */

namespace Setting\Form;

use Zend\Form\Form;

class AddProgramForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('add_program');
        $this->setAttribute('method', 'post');

         $this->add(array(
            'type' => 'hidden',
            'name' => 'program_code_hidden',
            'options' => array(),
            'attributes' => array(
                'id' => 'program_code_hidden',
                'value' => ''
            )
        ));
         
         $this->add(array(
            'type' => 'hidden',
            'name' => 'programId',
            'options' => array(),
            'attributes' => array(
                'id' => 'programId',
                'value' => ''
            )
        ));
            
       $this->add(array(
            'type' => 'hidden',
            'name' => 'isusable',
            'options' => array(),
            'attributes' => array(
                'id' => 'isusable',
                'value' => ''
            )
        ));
                
        $this->add(array(
            'type' => 'text',
            'name' => 'name',
            'options' => array(),
            'attributes' => array(
                'id' => 'name',
                'value' => ''
            )
        ));

        $this->add(array(
            'type' => 'text',
            'name' => 'program_code',
            'options' => array(),
            'attributes' => array(
                'id' => 'program_code',
                'value' => ''
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_available_for_donation',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'options' => array('use_hidden_element' => false ),
            'attributes' => array(
                'id' => 'is_available_for_donation',
                'class' => 'e2',
                'onChange' => 'setIsDonation();'
            )
        ));

        $this->add(array(
            'type' => 'textarea',
            'name' => 'description',
            'options' => array(),
            'attributes' => array(
                'id' => 'program_description',
                'class' => 'width-65'
            )
        ));
        $this->add(array(
            'type' => 'text',
            'name' => 'gl_code',
            'options' => array(),
            'attributes' => array(
                'id' => 'gl_code',
                'value' => ''
            )
        ));
        $this->add(array(
            'type' => 'text',
            'name' => 'major_gl_code',
            'options' => array(),
            'attributes' => array(
                'id' => 'major_gl_code',
                'value' => ''
            )
        ));
        $this->add(array(
            'type' => 'text',
            'name' => 'department',
            'options' => array(),
            'attributes' => array(
                'id' => 'department',
                'value' => ''
            )
        ));
        $this->add(array(
            'type' => 'text',
            'name' => 'designation',
            'options' => array(),
            'attributes' => array(
                'id' => 'designation',
                'value' => ''
            )
        ));
        $this->add(array(
            'type' => 'text',
            'name' => 'allocation',
            'options' => array(),
            'attributes' => array(
                'id' => 'allocation',
                'value' => ''
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_restriction',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'options' => array('use_hidden_element' => false ),
            'attributes' => array(
                'id' => 'is_restriction',
                'class' => 'e2'
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'status',
            'options' => array(
                'value_options' => array(
                    '1' => 'Active',
                    '0' => 'Inactive',
                    '2' => 'Archive'
                ),
            ),
            'attributes' => array(
                'id' => 'status',
                'class' => 'e1',
            )
        ));
        $this->add(array(
            'type' => 'submit',
            'name' => 'save',
            'options' => array(),
            'attributes' => array(
                'id' => 'save',
                'class' => 'save-btn',
                'value' => 'SAVE'
            )
        ));
        
           $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Cancel',
                'id' => 'cancel',
                'class' => 'cancel-btn m-l-20',
                'onClick' => "javascript:window.location.href='/crm-program-list'",
            ),
        ));
 
        $this->add(array(
            'name' => 'cancel_view',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Cancel',
                'id' => 'cancel_view',
                'class' => 'cancel-btn',
                'onClick' => "javascript:window.location.href='/crm-program-list'",
            ),
        ));
    }

}

