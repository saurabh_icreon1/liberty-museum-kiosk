<?php

/**
 * This file is  used for edit email template form.
 * @package    Product
 * @author     Icreon Tech - SR.
 */

namespace Setting\Form;

use Zend\Form\Form;

/**
 * This class is used for edit email template form.
 * @author     Icreon Tech - SR
 */
class EditMailTemplateForm extends Form {

    public function __construct($name = null) {
        parent::__construct('edit-email-template');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'email_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'email_id',
                'class'=>'width-500'
            ),
        ));
        $this->add(array(
            'name' => 'email_title',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email_title',
                'class'=>'width-500'
            ),
        ));
        $this->add(array(
            'name' => 'email_description',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'email_description',
                'class'=> 'width-90'
            ),
        ));
       
       
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'email_module_id',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'email_module_id',
                'value' => '', //set selected to 'blank'
                'class' => 'e2 select-w-270'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'module_variable',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'module_variable',
                'value' => '', //set selected to 'blank'
                'class' => 'e2 select-w-320'
            )
        ));
        $this->add(array(
            'name' => 'placeholder_variable',
            'attributes' => array(
                'type' => 'text',
                'readonly' => 'readonly',
                'id' => 'placeholder_variable',
                'width'=>'width-500'
            ),
        ));
        $this->add(array(
            'name' => 'email_subject',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email_subject',
                'width'=>'width-500'
            ),
        ));
         $this->add(array(
            'name' => 'email_message_body',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'email_message_body',
                'class'=> 'myTextEditor width-250 textareWidthFull'
            ),
        ));
        $this->add(array(
            'name' => 'email_address',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email_address',
                'width'=>'width-500'
            ),
        ));
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'email_set_text',
            'attributes' => array(
                'class' => 'e4 email_set_text',
                'id' => 'email_set_text',
                'onClick'=>'setPlainText()'
            ),
        ));

        $this->add(array(
            'name' => 'insert_variable',
            'attributes' => array(
                'type' => 'button',
                'id' => 'insert_variable',
                'class' => 'save-btn',
                'value' => 'Insert'
            ),
        ));
        $this->add(array(
            'name' => 'send',
            'attributes' => array(
                'type' => 'button',
                'id' => 'send',
                'class' => 'save-btn m-l-10',
                'value' => 'Send'
            ),
        ));
        $this->add(array(
            'name' => 'save',
            'attributes' => array(
                'type' => 'Submit',
                'id' => 'save',
                'class' => 'save-btn',
                'value' => 'Save'
            ),
        ));
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'id' => 'cancel',
                'class' => 'cancel-btn  m-l-10',
                'value' => 'Cancel',
                'onClick' => "javascript:window.location.href='/crm-email-templates'",
            ),
        ));

        
       
    }

}