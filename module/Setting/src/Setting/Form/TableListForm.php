<?php

/**
 * This form is used to list all dropdown element
 * @package    Setting
 * @author     Icreon Tech - AS
 */

namespace Setting\Form;

use Zend\Form\Form;

/**
 * This class is used to add a dropdown element
 * @author     Icreon Tech - AS
 */
class TableListForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('table_list');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'type' => 'Select',
            'name' => 'dropdown',
            'attributes' => array(
                'id' => 'dropdown',
                'class' => 'e1',
                'onChange' => 'select(this.value)'
            )
        ));
    }

}