<?php

/**
 * This file is  used for create form of GL code manage.
 * @package    Setting
 * @author     Icreon Tech - DT.
 */

namespace Setting\Form;

use Zend\Form\Form;

/**
 * This class is used for create form of GL code manage.
 * @package    Setting
 * @author     Icreon Tech - DT.
 */
class GLCodeForm extends Form {

    public function __construct($name = null) {
        parent::__construct('glcode');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'W_O_H',
            'attributes' => array(
                'type' => 'text',
                'id' => 'W_O_H',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'W_O_H_department',
            'attributes' => array(
                'type' => 'text',
                'id' => 'W_O_H_department',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'W_O_H_designation',
            'attributes' => array(
                'type' => 'text',
                'id' => 'W_O_H_designation',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'W_O_H_allocation',
            'attributes' => array(
                'type' => 'text',
                'id' => 'W_O_H_allocation',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'W_O_H_restriction',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'W_O_H_restriction',
                'class' => 'checkbox e2'
            )
        ));
        $this->add(array(
            'name' => 'F_O_F',
            'attributes' => array(
                'type' => 'text',
                'id' => 'F_O_F',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'F_O_F_department',
            'attributes' => array(
                'type' => 'text',
                'id' => 'F_O_F_department',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'F_O_F_designation',
            'attributes' => array(
                'type' => 'text',
                'id' => 'F_O_F_designation',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'F_O_F_allocation',
            'attributes' => array(
                'type' => 'text',
                'id' => 'F_O_F_allocation',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'F_O_F_restriction',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'F_O_F_restriction',
                'class' => 'checkbox e2'
            )
        ));
        $this->add(array(
            'name' => 'INV',
            'attributes' => array(
                'type' => 'text',
                'id' => 'INV',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'INV_department',
            'attributes' => array(
                'type' => 'text',
                'id' => 'INV_department',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'INV_designation',
            'attributes' => array(
                'type' => 'text',
                'id' => 'INV_designation',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'INV_allocation',
            'attributes' => array(
                'type' => 'text',
                'id' => 'INV_allocation',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'INV_restriction',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'INV_restriction',
                'class' => 'checkbox e2'
            )
        ));
        $this->add(array(
            'name' => 'UN_DON',
            'attributes' => array(
                'type' => 'text',
                'id' => 'UN_DON',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'UN_DON_department',
            'attributes' => array(
                'type' => 'text',
                'id' => 'UN_DON_department',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'UN_DON_designation',
            'attributes' => array(
                'type' => 'text',
                'id' => 'UN_DON_designation',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'UN_DON_allocation',
            'attributes' => array(
                'type' => 'text',
                'id' => 'UN_DON_allocation',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'UN_DON_restriction',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'UN_DON_restriction',
                'class' => 'checkbox e2'
            )
        ));
        $this->add(array(
            'name' => 'UN_DON_M',
            'attributes' => array(
                'type' => 'text',
                'id' => 'UN_DON_M',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'UN_DON_M_department',
            'attributes' => array(
                'type' => 'text',
                'id' => 'UN_DON_M_department',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'UN_DON_M_designation',
            'attributes' => array(
                'type' => 'text',
                'id' => 'UN_DON_M_designation',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'UN_DON_M_allocation',
            'attributes' => array(
                'type' => 'text',
                'id' => 'UN_DON_M_allocation',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'UN_DON_M_restriction',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'UN_DON_M_restriction',
                'class' => 'checkbox e2'
            )
        ));
        $this->add(array(
            'name' => 'KIO_FEE',
            'attributes' => array(
                'type' => 'text',
                'id' => 'KIO_FEE',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'KIO_FEE_department',
            'attributes' => array(
                'type' => 'text',
                'id' => 'KIO_FEE_department',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'KIO_FEE_designation',
            'attributes' => array(
                'type' => 'text',
                'id' => 'KIO_FEE_designation',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'KIO_FEE_allocation',
            'attributes' => array(
                'type' => 'text',
                'id' => 'KIO_FEE_allocation',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'KIO_FEE_restriction',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'KIO_FEE_restriction',
                'class' => 'checkbox e2'
            )
        ));
        $this->add(array(
            'name' => 'PASS_DOC',
            'attributes' => array(
                'type' => 'text',
                'id' => 'PASS_DOC',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'PASS_DOC_department',
            'attributes' => array(
                'type' => 'text',
                'id' => 'PASS_DOC_department',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'PASS_DOC_designation',
            'attributes' => array(
                'type' => 'text',
                'id' => 'PASS_DOC_designation',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'PASS_DOC_allocation',
            'attributes' => array(
                'type' => 'text',
                'id' => 'PASS_DOC_allocation',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'PASS_DOC_restriction',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'PASS_DOC_restriction',
                'class' => 'checkbox e2'
            )
        ));
        /* $this->add(array(
          'name' => 'INV_PRO',
          'attributes' => array(
          'type' => 'text',
          'id' => 'INV_PRO',
          'class'=>'width-500'
          ),
          )); */
        $this->add(array(
            'name' => 'SHI_AND_HAND',
            'attributes' => array(
                'type' => 'text',
                'id' => 'SHI_AND_HAND',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'SHI_AND_HAND_department',
            'attributes' => array(
                'type' => 'text',
                'id' => 'SHI_AND_HAND_department',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'SHI_AND_HAND_designation',
            'attributes' => array(
                'type' => 'text',
                'id' => 'SHI_AND_HAND_designation',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'SHI_AND_HAND_allocation',
            'attributes' => array(
                'type' => 'text',
                'id' => 'SHI_AND_HAND_allocation',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'SHI_AND_HAND_restriction',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'SHI_AND_HAND_restriction',
                'class' => 'checkbox e2'
            )
        ));
        $this->add(array(
            'name' => 'TAX',
            'attributes' => array(
                'type' => 'text',
                'id' => 'TAX',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'TAX_department',
            'attributes' => array(
                'type' => 'text',
                'id' => 'TAX_department',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'TAX_designation',
            'attributes' => array(
                'type' => 'text',
                'id' => 'TAX_designation',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'TAX_allocation',
            'attributes' => array(
                'type' => 'text',
                'id' => 'TAX_allocation',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'TAX_restriction',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'TAX_restriction',
                'class' => 'checkbox e2'
            )
        ));
        $this->add(array(
            'name' => 'REFUND',
            'attributes' => array(
                'type' => 'text',
                'id' => 'REFUND',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'REFUND_department',
            'attributes' => array(
                'type' => 'text',
                'id' => 'REFUND_department',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'REFUND_designation',
            'attributes' => array(
                'type' => 'text',
                'id' => 'REFUND_designation',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'REFUND_allocation',
            'attributes' => array(
                'type' => 'text',
                'id' => 'REFUND_allocation',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'REFUND_restriction',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'REFUND_restriction',
                'class' => 'checkbox e2'
            )
        ));
        $this->add(array(
            'name' => 'DISCOUNT',
            'attributes' => array(
                'type' => 'text',
                'id' => 'DISCOUNT',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'DISCOUNT_department',
            'attributes' => array(
                'type' => 'text',
                'id' => 'DISCOUNT_department',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'DISCOUNT_designation',
            'attributes' => array(
                'type' => 'text',
                'id' => 'DISCOUNT_designation',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'name' => 'DISCOUNT_allocation',
            'attributes' => array(
                'type' => 'text',
                'id' => 'DISCOUNT_allocation',
                'class' => 'width-120'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'DISCOUNT_restriction',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'DISCOUNT_restriction',
                'class' => 'checkbox e2'
            )
        ));
        $this->add(array(
            'name' => 'save',
            'attributes' => array(
                'type' => 'Submit',
                'id' => 'save',
                'class' => 'save-btn',
                'value' => 'SAVE'
            ),
        ));
        $this->add(array('type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 600
                )
            )
        ));
    }

}