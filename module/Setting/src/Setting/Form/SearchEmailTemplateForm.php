<?php

/**
 * This form is used for Seach Email Template.
 * @package    Setting
 * @author     Icreon Tech - SR
 */

namespace Setting\Form;

use Zend\Form\Form;

/**
 * This form is used for Seach Email Template.
 * @package    Setting
 * @author     Icreon Tech - SR
 */
class SearchEmailTemplateForm extends Form {

    public function __construct($name = null) {

        parent::__construct('search-template');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'name'
            )
        ));
        
        $this->add(array(
            'name' => 'search',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Search',
                'id' => 'search',
                'class' => 'save-btn',
            ),
        ));

    }

}
