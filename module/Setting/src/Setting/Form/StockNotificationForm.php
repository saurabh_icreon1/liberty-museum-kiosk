<?php

/**
 * This file is  used for Stock management form.
 * @package    Setting
 * @author     Icreon Tech - SR.
 */

namespace Setting\Form;

use Zend\Form\Form;

/**
 * This class is used for Stock management form.
 * @package    Setting
 * @author     Icreon Tech - SR.
 */
class StockNotificationForm extends Form {

    public function __construct($name = null) {
        parent::__construct('stock');
        $this->setAttribute('method', 'post');
        
        $this->add(array(
            'name' => 'notification_recipient',
            'attributes' => array(
                'type' => 'text',
                'id' => 'notification_recipient',
                'class'=>'width-500'
            ),
        ));
        $this->add(array(
            'name' => 'subject',
            'attributes' => array(
                'type' => 'text',
                'id' => 'subject',
                'class'=>'width-500'
            ),
        ));
        $this->add(array(
            'name' => 'message',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'message',
                'class'=>'width-87'
            ),
        ));

       
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'is_send',
            'attributes' => array(
                'class' => 'e2',
                'id' => 'check-1',
            ),

        ));

        $this->add(array(
            'name' => 'save',
            'attributes' => array(
                'type' => 'Submit',
                'id' => 'save',
                'class' => 'save-btn',
                'value' => 'SAVE CONFIGURATION'
            ),
        ));
    }

}