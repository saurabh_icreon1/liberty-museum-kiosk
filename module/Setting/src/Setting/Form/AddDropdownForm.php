<?php

/**
 * This form is used to add a dropdown element
 * @package    Setting
 * @author     Icreon Tech - AS
 */

namespace Setting\Form;

use Zend\Form\Form;

/**
 * This class is used to add a dropdown element
 * @author     Icreon Tech - AS
 */
class AddDropdownForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('add_dropdown');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'type' => 'text',
            'name' => 'name',
            'options' => array(),
            'attributes' => array(
                'id' => 'name',
                'value' => '',
                'class' => 'width-190'
            )
        ));
        $this->add(array(
            'type' => 'submit',
            'name' => 'save',
            'options' => array(),
            'attributes' => array(
                'id' => 'save',
                'class' => 'save-btn m-r-10',
                'value' => 'ADD'
            )
        ));
        $this->add(array(
            'name' => 'cancel',
            'options' => array(),
            'attributes' => array(
                'type' => 'button',
                'id' => 'cancel',
                'class' => 'cancel-btn m-1-10',
                'value' => 'cancel',
                'onclick' => "hideDiv()"
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'delId',
            'options' => array(),
            'attributes' => array(
                'id' => 'delId'
            )
        ));
    }
}