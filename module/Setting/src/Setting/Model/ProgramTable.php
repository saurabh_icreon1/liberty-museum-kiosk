<?php

/**
 * This is used for setting module database related work.
 * @package    setting
 * @author     Icreon Tech - NS
 */

namespace Setting\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This class is used for creating a table gateway for Setting
 * @package    Setting
 * @author     Icreon Tech - NS
 */
class ProgramTable {

    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
    }

    
    
    /**
     * Function for to check existence of program code
     * @author Icreon Tech - NS
     */
    public function chekProgramCodeExists($data = array()) {
        $program_id = (isset($data['programId']) && $data['programId'] != '') ? $data['programId'] : '';
        $program_code = (isset($data['programCode']) && $data['programCode'] != '') ? $data['programCode'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_cam_checkProgramCodeExists(?,?)');
        $stmt->getResource()->bindParam(1, $program_id);
        $stmt->getResource()->bindParam(2, $program_code);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet)) {
            return $resultSet;
        } else {
            return false;
        }
    }
    
    /**
     * Function for fetching program list
     * @return $resultSet as array or false as boolean 
     * @param $dataArr as an array
     * @author Icreon Tech - NS
     */
    public function getProgram($dataArr = null) {
        $program = isset($dataArr['program']) ? $dataArr['program'] : '';
        $program_id = isset($dataArr['program_id']) ? $dataArr['program_id'] : '';
        $is_status = isset($dataArr['is_status']) ? $dataArr['is_status'] : '';
        $start_index = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $record_limit = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $sort_field = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $sort_order = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        
         
        $program_code = (isset($dataArr['program_code']) && $dataArr['program_code'] != '') ? $dataArr['program_code'] : '';
        $is_available_for_donation = (isset($dataArr['is_available_for_donation']) && $dataArr['is_available_for_donation'] != '') ? $dataArr['is_available_for_donation'] : '';
        
        
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_com_getPrograms(?,?,?,?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $program_id);
        $stmt->getResource()->bindParam(2, $program);
        $stmt->getResource()->bindParam(3, $is_status);
        $stmt->getResource()->bindParam(4, $start_index);
        $stmt->getResource()->bindParam(5, $record_limit);
        $stmt->getResource()->bindParam(6, $sort_field);
        $stmt->getResource()->bindParam(7, $sort_order);
        $stmt->getResource()->bindParam(8, $program_code);
        $stmt->getResource()->bindParam(9, $is_available_for_donation);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }
    
    /**
     * This function will add a new program
     * @param $params as array
     * @return this will return a boolean confirmation
     * @author Icreon Tech - NS
     */
    public function addProgramDetails($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_crm_addProgram(?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['name']);
            $stmt->getResource()->bindParam(2, $params['program_code']);
            $stmt->getResource()->bindParam(3, $params['is_available_for_donation']);
            $stmt->getResource()->bindParam(4, $params['gl_code']);
            $stmt->getResource()->bindParam(5, $params['major_gl_code']);
            $stmt->getResource()->bindParam(6, $params['department']);
            $stmt->getResource()->bindParam(7, $params['designation']);
            $stmt->getResource()->bindParam(8, $params['allocation']);
            $stmt->getResource()->bindParam(9, $params['is_restriction']);
            $stmt->getResource()->bindParam(10, $params['description']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    
     /**
     * Function for to for delete master program
     * @author Icreon Tech - NS
     * @return Array
     * @param Void
     */
    public function deleteProgram($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_com_deleteProgram(?)');
            $stmt->getResource()->bindParam(1, $params['programId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
        } catch (Exception $e) {
            return false;
        }
    }
    
     /**
     * Function for to for update master program
     * @author Icreon Tech - NS
     * @return Array
     * @param Void
     */
     public function updateProgram($data = array()) {
        try {
            $program_id = (isset($data['program_id']) && $data['program_id'] != '') ? $data['program_id'] : '';
            $program = (isset($data['name']) && $data['name'] != '') ? $data['name'] : '';
            $program_code = (isset($data['program_code']) && $data['program_code'] != '') ? $data['program_code'] : '';
            $is_available_for_donation = (isset($data['is_available_for_donation']) && $data['is_available_for_donation'] != '') ? $data['is_available_for_donation'] : '0';
            $gl_code = (isset($data['gl_code']) && $data['gl_code'] != '') ? $data['gl_code'] : '';
            $major_gl_code = (isset($data['major_gl_code']) && $data['major_gl_code'] != '') ? $data['major_gl_code'] : '';
            $department = (isset($data['department']) && $data['department'] != '') ? $data['department'] : '';
            $designation = (isset($data['designation']) && $data['designation'] != '') ? $data['designation'] : '';
            $allocation = (isset($data['allocation']) && $data['allocation'] != '') ? $data['allocation'] : '';
            $is_restriction = (isset($data['is_restriction']) && $data['is_restriction'] != '') ? $data['is_restriction'] : '';
            $program_description = (isset($data['description']) && $data['description'] != '') ? $data['description'] : '';
            $status = $data['status'];
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_com_updateProgram(?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $program_id);
            $stmt->getResource()->bindParam(2, $program);
            $stmt->getResource()->bindParam(3, $program_code);
            $stmt->getResource()->bindParam(4, $is_available_for_donation);
            $stmt->getResource()->bindParam(5, $gl_code);
            $stmt->getResource()->bindParam(6, $major_gl_code);
            $stmt->getResource()->bindParam(7, $department);
            $stmt->getResource()->bindParam(8, $designation);
            $stmt->getResource()->bindParam(9, $allocation);
            $stmt->getResource()->bindParam(10, $is_restriction);
            $stmt->getResource()->bindParam(11, $program_description);
            $stmt->getResource()->bindParam(12, $status);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet)) {
                return $resultSet;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }
}
