<?php

/**
 * This is used for setting module database related work.
 * @package    setting
 * @author     Icreon Tech - AS
 */

namespace Setting\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This class is used for creating a table gateway for Setting
 * @package    Setting
 * @author     Icreon Tech - AS
 */
class SettingTable {

    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
    }

    /**
     * Function to get dropdown values
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function getDropdownContent($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_set_getMstTableContentList(?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['startIndex']);
            $stmt->getResource()->bindParam(2, $param['recordLimit']);
            $stmt->getResource()->bindParam(3, $param['sortField']);
            $stmt->getResource()->bindParam(4, $param['sortOrder']);
            $stmt->getResource()->bindParam(5, $param['table']);
            $stmt->getResource()->bindParam(6, $param['name']);
            $stmt->getResource()->bindParam(7, $param['field']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if ($resultSet) {
                return $resultSet;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update the stock notification mail template
     * @param this will be an array.
     * @return this will return a confirmation
     * @author Icreon Tech -SR
     */
    public function updateStockNotification($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_set_updateCrmStockNotification(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['notification_recipients']);
            $stmt->getResource()->bindParam(2, $param['message_subject']);
            $stmt->getResource()->bindParam(3, $param['message_text']);
            $stmt->getResource()->bindParam(4, $param['is_email_send']);
            $stmt->getResource()->bindParam(5, $param['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the saved notification mail template
     * @param this will be an array.
     * @return this will return a confirmation
     * @author Icreon Tech -SR
     */
    public function getStockNotification($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_set_getCrmStockNotification()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to add dropdown values
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function addDropdownContent($param = array()) {
        try {            
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_set_insertMstTableContent(?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['table']);
            $stmt->getResource()->bindParam(2, $param['name']);
            $stmt->getResource()->bindParam(3, $param['desc']);
            $stmt->getResource()->bindParam(4, $param['field']);
            $stmt->getResource()->bindParam(5, $param['field2']);
            $stmt->getResource()->bindParam(6, $param['added_by']);
            $stmt->getResource()->bindParam(7, $param['added_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            //$resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to edit dropdown values
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function editDropdownContent($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_set_updateMstTableContent(?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['table']);
            $stmt->getResource()->bindParam(2, $param['name']);
            $stmt->getResource()->bindParam(3, $param['desc']);
            $stmt->getResource()->bindParam(4, $param['field1']);
            $stmt->getResource()->bindParam(5, $param['field2']);
            $stmt->getResource()->bindParam(6, $param['field3']);
            $stmt->getResource()->bindParam(7, $param['id']);
            $stmt->getResource()->bindParam(8, $param['mod_by']);
            $stmt->getResource()->bindParam(9, $param['mod_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to fetch dropdowm lists available
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function getMstTableList($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_set_getMstTableList(?)');
            $stmt->getResource()->bindParam(1, $param['id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to fetch dropdowm lists available
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function deleteMstTableContent($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_set_deleteMstTableContent(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['table']);
            $stmt->getResource()->bindParam(2, $param['id']);
            $stmt->getResource()->bindParam(3, $param['delField']);
            $stmt->getResource()->bindParam(4, $param['mod_by']);
            $stmt->getResource()->bindParam(5, $param['mod_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get all email templates on the bases of search parameters
     * @param this will be an array having the search params.
     * @return this will return records in array
     * @author Icreon Tech -SR
     */
    public function searchEmailTemplate($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_set_getCrmEmailTemplate(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['name']);
            $stmt->getResource()->bindParam(2, $param['startIndex']);
            $stmt->getResource()->bindParam(3, $param['recordLimit']);
            $stmt->getResource()->bindParam(4, $param['sortField']);
            $stmt->getResource()->bindParam(5, $param['sortOrder']);
            $stmt->getResource()->bindParam(6, $param['email_id']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();
            if ($resultSet) {
                return $resultSet;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to get GL Codes
     * @author Icreon Tech - DT
     * @return array
     * @param void
     */
    public function getGLCodes() {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_set_getCrmGlCodes()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if ($resultSet) {
                return $resultSet;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update the gl code setting
     * @param this will be an array.
     * @return boolean
     * @author Icreon Tech -DT
     */
    public function updateGlCode($param = array()) {
        try {
            $glCode=(isset($param['gl_code']))?$param['gl_code']:'';
            $glCodeValue=(isset($param['gl_code_value']))?$param['gl_code_value']:'';
            $productTypeId=(isset($param['product_type_id']))?$param['product_type_id']:'';
            $department=(isset($param['department']))?$param['department']:'';
            $designation=(isset($param['designation']))?$param['designation']:'';
            $allocation=(isset($param['allocation']))?$param['allocation']:'';
            $isRestriction=(isset($param['is_restriction']))?$param['is_restriction']:'';
            $addedDate=(isset($param['added_date']))?$param['added_date']:'';
            $modifiedDate=(isset($param['modified_date']))?$param['modified_date']:'';
            
            
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_set_updateCrmGlCode(?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $glCode);
            $stmt->getResource()->bindParam(2, $glCodeValue);
            $stmt->getResource()->bindParam(3, $productTypeId);
            $stmt->getResource()->bindParam(4, $department);
            $stmt->getResource()->bindParam(5, $designation);
            $stmt->getResource()->bindParam(6, $allocation);
            $stmt->getResource()->bindParam(7, $isRestriction);
            $stmt->getResource()->bindParam(8, $addedDate);
            $stmt->getResource()->bindParam(9, $modifiedDate);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update the email template
     * @param this will be an array.
     * @return boolean
     * @author Icreon Tech - SR
     */
    public function updateEmailTemplate($param = array()) {
        try { 
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_set_updateCrmEmailTemplate(?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['email_id']);
            $stmt->getResource()->bindParam(2, $param['email_title']);
            $stmt->getResource()->bindParam(3, $param['email_description']);
            $stmt->getResource()->bindParam(4, $param['email_module_id']);
            $stmt->getResource()->bindParam(5, $param['email_subject']);
            $stmt->getResource()->bindParam(6, $param['email_message_body']);
            $stmt->getResource()->bindParam(7, $param['email_set_text']);
            $stmt->getResource()->bindParam(8, $param['modified_by']);
            $stmt->getResource()->bindParam(9, $param['modified_date']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    

    /**
     * Function to email template module variables
     * @return array
     * @param void
     * @author Icreon Tech - SR
     */
    
    public function getModuleVariable() {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_set_getEmailTemplateModuleVariable()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if ($resultSet) {
                return $resultSet;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

     /**
     * Function for insert membership data
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function saveMembership($param) {
        $procquesmarkapp=$this->appendQuestionMars(count($param));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_set_insertCrmMembership(".$procquesmarkapp.")");
        $stmt->getResource()->bindParam(1, $param[0]);
        $stmt->getResource()->bindParam(2, $param[1]);
        $stmt->getResource()->bindParam(3, $param[2]);
        $stmt->getResource()->bindParam(4, $param[3]);
        $stmt->getResource()->bindParam(5, $param[4]);
        $stmt->getResource()->bindParam(6, $param[5]);
        $stmt->getResource()->bindParam(7, $param[6]);
        $stmt->getResource()->bindParam(8, $param[7]);
        $stmt->getResource()->bindParam(9, $param[8]);
        $stmt->getResource()->bindParam(10, $param[9]);
        $stmt->getResource()->bindParam(11, $param[10]);
        $stmt->getResource()->bindParam(12, $param[11]);
        $stmt->getResource()->bindParam(13, $param[12]);
        $stmt->getResource()->bindParam(14, $param[13]);
        $stmt->getResource()->bindParam(15, $param[14]);
        $stmt->getResource()->bindParam(16, $param[15]);
        $stmt->getResource()->bindParam(17, $param[16]);
        $stmt->getResource()->bindParam(18, $param[17]);
        $stmt->getResource()->bindParam(19, $param[18]);
        $stmt->getResource()->bindParam(20, $param[19]);
        $stmt->getResource()->bindParam(21, $param[20]);
        $stmt->getResource()->bindParam(22, $param[21]);
        $stmt->getResource()->bindParam(23, $param[22]);
        $stmt->getResource()->bindParam(24, $param[23]);
        $stmt->getResource()->bindParam(25, $param[24]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for update membership data
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function editMembership($param) {
        $procquesmarkapp=$this->appendQuestionMars(count($param));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_set_updateCrmMembership(".$procquesmarkapp.")");
        $stmt->getResource()->bindParam(1, $param[0]);
        $stmt->getResource()->bindParam(2, $param[1]);
        $stmt->getResource()->bindParam(3, $param[2]);
        $stmt->getResource()->bindParam(4, $param[3]);
        $stmt->getResource()->bindParam(5, $param[4]);
        $stmt->getResource()->bindParam(6, $param[5]);
        $stmt->getResource()->bindParam(7, $param[6]);
        $stmt->getResource()->bindParam(8, $param[7]);
        $stmt->getResource()->bindParam(9, $param[8]);
        $stmt->getResource()->bindParam(10, $param[9]);
        $stmt->getResource()->bindParam(11, $param[10]);
        $stmt->getResource()->bindParam(12, $param[11]);
        $stmt->getResource()->bindParam(13, $param[12]);
        $stmt->getResource()->bindParam(14, $param[13]);
        $stmt->getResource()->bindParam(15, $param[14]);
        $stmt->getResource()->bindParam(16, $param[15]);
        $stmt->getResource()->bindParam(17, $param[16]);
        $stmt->getResource()->bindParam(18, $param[17]);
        $stmt->getResource()->bindParam(19, $param[18]);
        $stmt->getResource()->bindParam(20, $param[19]);
        $stmt->getResource()->bindParam(21, $param[20]);
        $stmt->getResource()->bindParam(22, $param[21]);
        $stmt->getResource()->bindParam(23, $param[22]);
        $stmt->getResource()->bindParam(24, $param[23]);
        $stmt->getResource()->bindParam(25, $param[24]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * This function will used to get the membership search type
     * @param this will be an array.
     * @return boolean
     * @author Icreon Tech - DT
     */
    public function getMembershipSaveSearch($param = array()) {
        $procquesmarkapp=$this->appendQuestionMars(count($param));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_set_getMembershipSaveSearch(".$procquesmarkapp.")");
        $stmt->getResource()->bindParam(1, $param[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
    }

    /**
     * Function for insert membership data
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function saveMembershipSaveSearchType($param) {
        $procquesmarkapp=$this->appendQuestionMars(count($param));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_set_insertCrmMembershipSearch(".$procquesmarkapp.")");
        $stmt->getResource()->bindParam(1, $param[0]);
        $stmt->getResource()->bindParam(2, $param[1]);
        $stmt->getResource()->bindParam(3, $param[2]);
        $stmt->getResource()->bindParam(4, $param[3]);
        $stmt->getResource()->bindParam(5, $param[4]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }
    /**
     * Function for get all membership
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function getAllMembership($membershipData) {
        
        $membershipData['currentYear'] = isset($membershipData['currentYear'])?$membershipData['currentYear']:'';
        $membershipData['isActive'] = isset($membershipData['isActive'])?$membershipData['isActive']:'';
        $procquesmarkapp=$this->appendQuestionMars(count($membershipData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_set_getCrmMembership(?,?,?,?,?,?)");
        $stmt->getResource()->bindParam(1, $membershipData[0]);
        $stmt->getResource()->bindParam(2, $membershipData[1]);
        $stmt->getResource()->bindParam(3, $membershipData[2]);
        $stmt->getResource()->bindParam(4, $membershipData[3]);
        $stmt->getResource()->bindParam(5, $membershipData['currentYear']);
        $stmt->getResource()->bindParam(6, $membershipData['isActive']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for remove membership
     * @author Icreon Tech - DT
     * @return Void
     * @param Array
     */
    public function removeMembershipById($membershipData) {
        $procquesmarkapp=$this->appendQuestionMars(count($membershipData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_set_deleteCrmMembership(".$procquesmarkapp.")");
        $stmt->getResource()->bindParam(1, $membershipData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for remove membership save search by save search id
     * @author Icreon Tech - DT
     * @return Void
     * @param Array
     */
    public function deleteMembershipSaveSearchType($membershipData) {
        $procquesmarkapp=$this->appendQuestionMars(count($membershipData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_set_deleteCrmMembershipSaveSearch(".$procquesmarkapp.")");
        $stmt->getResource()->bindParam(1, $membershipData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement = $result->getResource();
        $statement->closeCursor();
    }
    
    /**
     * Function for change membership status
     * @author Icreon Tech - DT
     * @return Void
     * @param Array
     */
    public function changeMembershipStatusById($membershipData) {
        $procquesmarkapp=$this->appendQuestionMars(count($membershipData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_set_changeCrmMembershipStatus(".$procquesmarkapp.")");
        $stmt->getResource()->bindParam(1, $membershipData[0]);
        $stmt->getResource()->bindParam(2, $membershipData[1]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

     /**
     * Function for get membership data on the base of id
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function getMembershipById($membershipData)
    {
        $procquesmarkapp=$this->appendQuestionMars(count($membershipData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_set_getMembershipDetails(".$procquesmarkapp.")");
        $stmt->getResource()->bindParam(1, $membershipData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet[0];
        else
            return false;
    }

     /**
     * Function for check membership exist
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function checkMembershipExist($membershipData)
    {
        $procquesmarkapp=$this->appendQuestionMars(count($membershipData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_set_checkMembershipExist(".$procquesmarkapp.")");
        $stmt->getResource()->bindParam(1, $membershipData[0]);
        $stmt->getResource()->bindParam(2, $membershipData[1]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet[0];
        else
            return false;
    }
    
    
    

    /**
     * Function for append number of questions marks for stored procedure
     * @author Icreon Tech - DT
     * @return String
     * @param Int
     */
    public function appendQuestionMars($count) {
        $returnstr='';
        for($i=1;$i<=$count;$i++)
        {
            $returnstr.='?,';
        }
        return rtrim($returnstr,',');
    }
    
    public function updateSortOrder($param){
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_set_updateSortOrder(?,?,?)');
            $stmt->getResource()->bindParam(1, $param['table']);
            $stmt->getResource()->bindParam(2, $param['field']);
            $stmt->getResource()->bindParam(3, $param['list']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

	/**
     * Function for get all master membership
     * @author Icreon Tech - SK
     * @return Array
     * @param Array
     */
    public function getMasterMembership($membershipData) {
        
        $membershipData['currentYear'] = isset($membershipData['currentYear'])?$membershipData['currentYear']:'';
        $membershipData['isActive'] = isset($membershipData['isActive'])?$membershipData['isActive']:'';
        $procquesmarkapp=$this->appendQuestionMars(count($membershipData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_set_getMstMembership(?,?,?,?,?,?)");
        $stmt->getResource()->bindParam(1, $membershipData[0]);
        $stmt->getResource()->bindParam(2, $membershipData[1]);
        $stmt->getResource()->bindParam(3, $membershipData[2]);
        $stmt->getResource()->bindParam(4, $membershipData[3]);
        $stmt->getResource()->bindParam(5, $membershipData['currentYear']);
        $stmt->getResource()->bindParam(6, $membershipData['isActive']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }
}
