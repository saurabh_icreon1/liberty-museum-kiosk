<?php

/**
 * This is used for Setting module validation.
 * @package    Setting
 * @author     Icreon Tech - AS
 */

namespace Setting\Model;

use Setting\Module;
// Add these import statements
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\Adapter\Adapter;
use Base\Model\BaseModel;

/**
 * This class is used for Setting module validation.
 * @package    Setting
 * @author     Icreon Tech - AS
 */
class Setting extends BaseModel {

    protected $inputFilter;
    protected $adapter;
    public $setting_id;
    public $setting_title;
    public $setting_type;
    public $setting_description;
    public $program;
    public $program_id;
    public $thumbnail;
    public $groups;
    public $start_date;
    public $end_date;
    public $setting_status;
    public $setting_goal;
    public $revenue_goal;
    public $coupon_code;
    public $gl_code;
    public $designation;
    public $departmrnt;
    public $channel;
    public $channel_cost;
    public $is_active;
    public $restricted;
    public $user_id;
    public $channel_id;

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    /**
     * This method is used to convert form post array of create setting to Object of class type Campaign
     * @param $data
     * @return Void
     * @author Icreon Tech - AS
     */
    public function exchangeArray($data) {

        $this->title = (isset($data['title']) && !empty($data['title'])) ? $data['title'] : '';
        $this->campaign_type = (isset($data['campaign_type']) && !empty($data['campaign_type'])) ? $data['campaign_type'] : '';
        $this->description = (isset($data['description']) && !empty($data['description'])) ? $data['description'] : '';
        $this->program = (isset($data['program']) && !empty($data['program'])) ? $data['program'] : '';
        $this->program_id = (isset($data['program_id']) && !empty($data['program_id'])) ? $data['program_id'] : '';
        $this->thumbnail = (isset($data['thumbnail']) && !empty($data['thumbnail'])) ? $data['thumbnail'] : '';
        $this->groups = (isset($data['groups']) && !empty($data['groups'])) ? $data['groups'] : '';
        $this->start_date = (isset($data['start_date']) && !empty($data['start_date'])) ? $data['start_date'] : '';
        $this->start_time = (isset($data['start_time']) && !empty($data['start_time'])) ? $data['start_time'] : '';
        $this->end_date = (isset($data['end_date']) && !empty($data['end_date'])) ? $data['end_date'] : '';
        $this->end_time = (isset($data['end_time']) && !empty($data['end_time'])) ? $data['end_time'] : '';
        $this->campaign_status = (isset($data['campaign_status']) && !empty($data['campaign_status'])) ? $data['campaign_status'] : '';
        $this->campaign_goal = (isset($data['campaign_goal']) && !empty($data['campaign_goal'])) ? $data['campaign_goal'] : '';
        $this->revenue_goal = (isset($data['revenue_goal']) && !empty($data['revenue_goal'])) ? $data['revenue_goal'] : '';
        $this->coupon_code = (isset($data['coupon_code']) && !empty($data['coupon_code'])) ? $data['coupon_code'] : '';
        $this->gl_code = (isset($data['gl_code']) && !empty($data['gl_code'])) ? $data['gl_code'] : '';
        $this->designation = (isset($data['designation']) && !empty($data['designation'])) ? $data['designation'] : '';
        $this->department = (isset($data['department']) && !empty($data['department'])) ? $data['department'] : '';
        $this->restricted = (isset($data['restricted']) && !empty($data['restricted'])) ? $data['restricted'] : '';
        $this->channel = (isset($data['channel']) && !empty($data['channel'])) ? 'aaaaa' : 'cccc';
        $this->channel_cost = (isset($data['channel_cost']) && !empty($data['channel_cost'])) ? 'bbbb' : 'dddd';
        $this->is_active = (isset($data['is_active']) && !empty($data['is_active'])) ? $data['is_active'] : '';
    }

    /**
     * Function used to check validation for stock notification
     * @author Icreon Tech - SR
     * @return boolean
     * @param Array
     */
    public function getInputFilterStockSetting() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_send',
                        'required' => false
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'notification_recipient',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'RECIPIENT_ERROR_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'subject',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'SUBJECT_ERROR_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'message',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'MESSAGE_ERROR_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));


            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    /**
     * Function used to pass for input filter
     * @author Icreon Tech - AS
     * @return array
     * @param Array
     */
    public function getInputFilterCrmSearch($filterData) {
        $inputFilterData = array();
        if (is_array($filterData)) {
            foreach ($filterData as $key => $val) {
                $inputFilterData[$key] = trim(addslashes(strip_tags($val)));
            }
        } else {
            $inputFilterData = trim(addslashes(strip_tags($filterData)));
        }
        return $this->inputFilterCrmSearch = $inputFilterData;
    }

    /**
     * This method is used to convert form post data array for setting
     * @param $data
     * @return Void
     * @author Icreon Tech - SR
     */
    public function exchangeStockNotificationArray($data) {
        $this->notificationRecipients = (isset($data['notification_recipient']) && !empty($data['notification_recipient'])) ? $data['notification_recipient'] : '';
        $this->messageSubject = (isset($data['subject']) && !empty($data['subject'])) ? $data['subject'] : '';
        $this->messageText = (isset($data['message']) && !empty($data['message'])) ? $data['message'] : '';
        $this->isEmailSend = (isset($data['is_send']) && !empty($data['is_send'])) ? $data['is_send'] : '';
    }

    /**
     * Function used to check validation for gl code
     * @author Icreon Tech - DT
     * @return boolean
     * @param Array
     */
    public function getInputFilterGLCodeSetting() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                        'name' => 'W_O_H',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'VALID_DIGITS'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'W_O_H_department',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'W_O_H_designation',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'W_O_H_allocation',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'W_O_H_restriction',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'F_O_F',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'VALID_DIGITS'
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'F_O_F_department',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'F_O_F_designation',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'F_O_F_allocation',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'F_O_F_restriction',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'INV',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'VALID_DIGITS'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'INV_department',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'INV_designation',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'INV_allocation',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'INV_restriction',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'UN_DON',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'VALID_DIGITS'
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'UN_DON_department',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'UN_DON_designation',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'UN_DON_allocation',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'UN_DON_restriction',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'UN_DON_M',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'VALID_DIGITS',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'VALID_DIGITS'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'UN_DON_M_department',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'UN_DON_M_designation',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'UN_DON_M_allocation',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'UN_DON_M_restriction',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'KIO_FEE',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'VALID_DIGITS'
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'KIO_FEE_department',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'KIO_FEE_designation',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'KIO_FEE_allocation',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'KIO_FEE_restriction',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'PASS_DOC',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'VALID_DIGITS'
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'PASS_DOC_department',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'PASS_DOC_designation',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'PASS_DOC_allocation',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'PASS_DOC_restriction',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            /* $inputFilter->add($factory->createInput(array(
              'name' => 'INV_PRO',
              'required' => false,
              'filters' => array(
              array('name' => 'StripTags'),
              array('name' => 'StringTrim'),
              ),
              'validators' => array(
              array(
              'name' => 'regex', false,
              'options' => array(
              'pattern' => '/^[0-9]+$/',
              'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'VALID_DIGITS'
              ),
              ),
              ),
              ),
              ))); */
            $inputFilter->add($factory->createInput(array(
                        'name' => 'SHI_AND_HAND',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'VALID_DIGITS'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'SHI_AND_HAND_department',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'SHI_AND_HAND_designation',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'SHI_AND_HAND_allocation',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'SHI_AND_HAND_restriction',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'TAX',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'VALID_DIGITS'
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'TAX_department',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'TAX_designation',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'TAX_allocation',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'TAX_restriction',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'REFUND',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'VALID_DIGITS'
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'REFUND_department',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'REFUND_designation',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'REFUND_allocation',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'REFUND_restriction',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'DISCOUNT',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'VALID_DIGITS'
                                    ),
                                ),
                            ),
                        ),
                    )));


            $inputFilter->add($factory->createInput(array(
                        'name' => 'DISCOUNT_department',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'DISCOUNT_designation',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'DISCOUNT_allocation',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'DISCOUNT_restriction',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'csrf',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'csrf',
                                'options' => array(
                                    'messages' => array(
                                        'notSame' => 'NOT_SAME',
                                    ),
                                ),
                            ),
                        ),
                    )));


            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * Function used to check validation for filter membership
     * @author Icreon Tech - DT
     * @return boolean
     * @param Array
     */
    public function getInputFilterMembership() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                        'name' => 'validity_type',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'validity_type',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'validity_type',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'validity_type',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_active',
                        'required' => false                        
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'validity_type',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'membership_title',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'TITLE_EMPTY',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => ' /^[A-Za-z0-9\n\r ,.-]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'ALPHA_NUMERIC_SPECIALSYMPOL'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'Db\NoRecordExists',
                                'options' => array(
                                    'table' => 'tbl_set_memberships',
                                    'field' => 'membership_title',
                                    'adapter' => $this->adapter,
                                    'exclude' => array(
                                        'field' => 'is_deleted',
                                        'value' => '1'
                                    ),
                                    'messages' => array(
                                        'recordFound' => 'MEMBERSHIP_TITLE_EXIST',
                                    ),
                                ),
                            )
                        ),
                    )));
            
            
            $inputFilter->add($factory->createInput(array(
                     'name' => 'membership_description',
                     'required' => false
                   )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'minimun_donation_amount',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^-?\d*(\.\d+)?$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'NUMERIC_VALUE'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'first_alert_before_in_days',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'NUMERIC_VALUE'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'repeat_alerts_in_days',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'NUMERIC_VALUE'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'repeat_alerts_in_times',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'NUMERIC_VALUE'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'renew_alerts_in_days',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'NUMERIC_VALUE'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'renew_alerts_in_times',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'NUMERIC_VALUE'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'come_back_request_in_days',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'NUMERIC_VALUE'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'discount',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^-?\d*(\.\d+)?$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'NUMERIC_VALUE'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array('name' => 'membership_save_search')))->add(
                    array(
                        'name' => 'saved_search_in_days[]',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'NUMERIC_VALUE'
                                    ),
                                ),
                            ),
                        ),
            ));
            $inputFilter->add($factory->createInput(array('name' => 'membership_save_search')))->add(
                    array(
                        'name' => 'saved_search_type[]',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
            ));


            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * Function used to check validation for filter for edit membership
     * @author Icreon Tech - DT
     * @return boolean
     * @param Array
     */
    public function getInputFilterEditMembership() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                        'name' => 'validity_type',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'validity_type',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'validity_type',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'validity_type',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'validity_type',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'membership_title',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'TITLE_EMPTY',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => ' /^[A-Za-z0-9\n\r ,.-]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'ALPHA_NUMERIC_SPECIALSYMPOL'
                                    ),
                                ),
                            )
                        ),
                    )));
            
            $inputFilter->add($factory->createInput(array(
                'name' => 'membership_description',
                'required' => false
              )));
                 
            $inputFilter->add($factory->createInput(array(
                        'name' => 'minimun_donation_amount',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^-?\d*(\.\d+)?$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'NUMERIC_VALUE'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'first_alert_before_in_days',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'NUMERIC_VALUE'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'repeat_alerts_in_days',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'NUMERIC_VALUE'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'repeat_alerts_in_times',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'NUMERIC_VALUE'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'renew_alerts_in_days',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'NUMERIC_VALUE'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'renew_alerts_in_times',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'NUMERIC_VALUE'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'come_back_request_in_days',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'NUMERIC_VALUE'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'discount',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^-?\d*(\.\d+)?$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'NUMERIC_VALUE'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array('name' => 'membership_save_search')))->add(
                    array(
                        'name' => 'saved_search_in_days[]',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'NUMERIC_VALUE'
                                    ),
                                ),
                            ),
                        ),
            ));
            $inputFilter->add($factory->createInput(array('name' => 'membership_save_search')))->add(
                    array(
                        'name' => 'saved_search_type[]',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
            ));


            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * Function used to check validation for email template
     * @author Icreon Tech - SR
     * @return boolean
     * @param Array
     */
    public function getInputFilterEmailTemplate() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'email_title',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMAIL_TITLE_ERROR_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'email_description',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMAIL_DESCRIPTION_ERROR_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'email_subject',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMAIL_SUBJECT_ERROR_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'email_message_body',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMAIL_MESSAGE_ERROR_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'module_variable',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'email_module_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'email_set_text',
                        'required' => false,
                    )));

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * This method is used to return the stored procedure array for create membershp
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getCreateMembershipArr($dataArr = array()) {

        $returnArr = array();
        $returnArr[] = (isset($dataArr['membership_title']) && $dataArr['membership_title'] != '') ? $dataArr['membership_title'] : '';
        $returnArr[] = (isset($dataArr['level_color']) && $dataArr['level_color'] != '') ? $dataArr['level_color'] : '';
        $returnArr[] = (isset($dataArr['validity_type']) && $dataArr['validity_type'] != '') ? $dataArr['validity_type'] : '';
        $returnArr[] = (isset($dataArr['validity_time']) && $dataArr['validity_time'] != '') ? $dataArr['validity_time'] : '';
        $returnArr[] = (isset($dataArr['minimun_donation_amount']) && $dataArr['minimun_donation_amount'] != '') ? $dataArr['minimun_donation_amount'] : '';
        $returnArr[] = (isset($dataArr['first_alert_before_in_days']) && $dataArr['first_alert_before_in_days'] != '') ? $dataArr['first_alert_before_in_days'] : '';
        $returnArr[] = (isset($dataArr['repeat_alerts_in_days']) && $dataArr['repeat_alerts_in_days'] != '') ? $dataArr['repeat_alerts_in_days'] : '';
        $returnArr[] = (isset($dataArr['repeat_alerts_in_times']) && $dataArr['repeat_alerts_in_times'] != '') ? $dataArr['repeat_alerts_in_times'] : '';
        $returnArr[] = (isset($dataArr['renew_alerts_in_days']) && $dataArr['renew_alerts_in_days'] != '') ? $dataArr['renew_alerts_in_days'] : '';
        $returnArr[] = (isset($dataArr['renew_alerts_in_times']) && $dataArr['renew_alerts_in_times'] != '') ? $dataArr['renew_alerts_in_times'] : '';
        $returnArr[] = (isset($dataArr['come_back_request_in_days']) && $dataArr['come_back_request_in_days'] != '') ? $dataArr['come_back_request_in_days'] : '';
        $returnArr[] = (isset($dataArr['discount']) && $dataArr['discount'] != '') ? $dataArr['discount'] : '';
        $returnArr[] = (isset($dataArr['saved_search_validity_in_days']) && $dataArr['saved_search_validity_in_days'] != '') ? $dataArr['saved_search_validity_in_days'] : NULL;
        $returnArr[] = (isset($dataArr['saved_search_validity_type']) && $dataArr['saved_search_validity_type'] != '') ? $dataArr['saved_search_validity_type'] : '';
        $returnArr[] = (isset($dataArr['is_annotation']) && $dataArr['is_annotation'] != '') ? $dataArr['is_annotation'] : '';
        $returnArr[] = (isset($dataArr['max_stories']) && $dataArr['max_stories'] != '') ? $dataArr['max_stories'] : NULL;
        $returnArr[] = (isset($dataArr['max_story_characters']) && $dataArr['max_story_characters'] != '') ? $dataArr['max_story_characters'] : NULL;
        $returnArr[] = (isset($dataArr['max_passengers']) && $dataArr['max_passengers'] != '') ? $dataArr['max_passengers'] : NULL;
        $returnArr[] = (isset($dataArr['max_saved_histories']) && $dataArr['max_saved_histories'] != '') ? $dataArr['max_saved_histories'] : NULL;
        $returnArr[] = (isset($dataArr['added_by']) && $dataArr['added_by'] != '') ? $dataArr['added_by'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : '';
        $returnArr[] = (isset($dataArr['modified_by']) && $dataArr['modified_by'] != '') ? $dataArr['modified_by'] : '';
        $returnArr[] = (isset($dataArr['modified_date']) && $dataArr['modified_date'] != '') ? $dataArr['modified_date'] : '';
        $returnArr[] = (isset($dataArr['is_correction']) && $dataArr['is_correction'] != '') ? $dataArr['is_correction'] : '';
        $returnArr[] = (isset($dataArr['membership_description']) && $dataArr['membership_description'] != '') ? $dataArr['membership_description'] : '';
        
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for edit membership
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getEditMembershipArr($dataArr = array()) {

        $returnArr = array();
        $returnArr[] = (isset($dataArr['membership_id']) && $dataArr['membership_id'] != '') ? $dataArr['membership_id'] : '';
        $returnArr[] = (isset($dataArr['membership_title']) && $dataArr['membership_title'] != '') ? $dataArr['membership_title'] : '';
        $returnArr[] = (isset($dataArr['level_color']) && $dataArr['level_color'] != '') ? $dataArr['level_color'] : '';
        $returnArr[] = (isset($dataArr['validity_type']) && $dataArr['validity_type'] != '') ? $dataArr['validity_type'] : '';
        $returnArr[] = (isset($dataArr['validity_time']) && $dataArr['validity_time'] != '') ? $dataArr['validity_time'] : '';
        $returnArr[] = (isset($dataArr['minimun_donation_amount']) && $dataArr['minimun_donation_amount'] != '') ? $dataArr['minimun_donation_amount'] : '';
        $returnArr[] = (isset($dataArr['first_alert_before_in_days']) && $dataArr['first_alert_before_in_days'] != '') ? $dataArr['first_alert_before_in_days'] : '';
        $returnArr[] = (isset($dataArr['repeat_alerts_in_days']) && $dataArr['repeat_alerts_in_days'] != '') ? $dataArr['repeat_alerts_in_days'] : '';
        $returnArr[] = (isset($dataArr['repeat_alerts_in_times']) && $dataArr['repeat_alerts_in_times'] != '') ? $dataArr['repeat_alerts_in_times'] : '';
        $returnArr[] = (isset($dataArr['renew_alerts_in_days']) && $dataArr['renew_alerts_in_days'] != '') ? $dataArr['renew_alerts_in_days'] : '';
        $returnArr[] = (isset($dataArr['renew_alerts_in_times']) && $dataArr['renew_alerts_in_times'] != '') ? $dataArr['renew_alerts_in_times'] : '';
        $returnArr[] = (isset($dataArr['come_back_request_in_days']) && $dataArr['come_back_request_in_days'] != '') ? $dataArr['come_back_request_in_days'] : '';
        $returnArr[] = (isset($dataArr['discount']) && $dataArr['discount'] != '') ? $dataArr['discount'] : '';
        $returnArr[] = (isset($dataArr['saved_search_validity_in_days']) && $dataArr['saved_search_validity_in_days'] != '') ? $dataArr['saved_search_validity_in_days'] : NULL;
        $returnArr[] = (isset($dataArr['saved_search_validity_type']) && $dataArr['saved_search_validity_type'] != '') ? $dataArr['saved_search_validity_type'] : '';
        $returnArr[] = (isset($dataArr['is_annotation']) && $dataArr['is_annotation'] != '') ? $dataArr['is_annotation'] : '';
        $returnArr[] = (isset($dataArr['max_stories']) && $dataArr['max_stories'] != '') ? $dataArr['max_stories'] : NULL;
        $returnArr[] = (isset($dataArr['max_story_characters']) && $dataArr['max_story_characters'] != '') ? $dataArr['max_story_characters'] : NULL;
        $returnArr[] = (isset($dataArr['max_passengers']) && $dataArr['max_passengers'] != '') ? $dataArr['max_passengers'] : NULL;
        $returnArr[] = (isset($dataArr['max_saved_histories']) && $dataArr['max_saved_histories'] != '') ? $dataArr['max_saved_histories'] : NULL;
        $returnArr[] = (isset($dataArr['modified_by']) && $dataArr['modified_by'] != '') ? $dataArr['modified_by'] : '';
        $returnArr[] = (isset($dataArr['modified_date']) && $dataArr['modified_date'] != '') ? $dataArr['modified_date'] : '';
        $returnArr[] = (isset($dataArr['is_correction']) && $dataArr['is_correction'] != '') ? $dataArr['is_correction'] : '';
        $returnArr[] = (isset($dataArr['is_active']) && $dataArr['is_active'] != '') ? $dataArr['is_active'] : '';
        $returnArr[] = (isset($dataArr['membership_description']) && $dataArr['membership_description'] != '') ? $dataArr['membership_description'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for search membership
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getMembershipSearchArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $returnArr[] = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $returnArr[] = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for delete membership
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getArrayForRemoveMembership($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['membershipId']) && $dataArr['membershipId'] != '') ? $dataArr['membershipId'] : '0';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for change membership status
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getArrayForChangeMembershipStatus($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['membership_id']) && $dataArr['membership_id'] != '') ? $dataArr['membership_id'] : '0';
        $returnArr[] = (isset($dataArr['is_active']) && $dataArr['is_active'] != '') ? $dataArr['is_active'] : '0';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for create membershp search type array
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getCreateMembershipSearchSaveTypeArr($dataArr = array()) {

        $returnArr = array();
        $returnArr[] = (isset($dataArr['membership_id']) && $dataArr['membership_id'] != '') ? $dataArr['membership_id'] : '';
        $returnArr[] = (isset($dataArr['saved_search_in_days']) && $dataArr['saved_search_in_days'] != '') ? $dataArr['saved_search_in_days'] : '';
        $returnArr[] = (isset($dataArr['saved_search_type_id']) && $dataArr['saved_search_type_id'] != '') ? $dataArr['saved_search_type_id'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : '';
        $returnArr[] = (isset($dataArr['modified_date']) && $dataArr['modified_date'] != '') ? $dataArr['modified_date'] : '';

        return $returnArr;
    }

    /**
     * This method is used to exchange the email templates posted data
     * @param posted email template form data
     * @return Void
     * @author Icreon Tech - SR
     */
    public function exchangeEmailTemplateArray($data) {

        $this->email_title = (isset($data['email_title']) && !empty($data['email_title'])) ? $data['email_title'] : '';
        $this->email_description = (isset($data['email_description']) && !empty($data['email_description'])) ? $data['email_description'] : '';
        $this->email_subject = (isset($data['email_subject']) && !empty($data['email_subject'])) ? $data['email_subject'] : '';
        $this->email_message_body = (isset($data['email_message_body']) && !empty($data['email_message_body'])) ? $data['email_message_body'] : '';
        $this->email_module_id = (isset($data['email_module_id']) && !empty($data['email_module_id'])) ? $data['email_module_id'] : '';
        $this->email_set_text = (isset($data['email_set_text']) && !empty($data['email_set_text'])) ? $data['email_set_text'] : '';
    }

}

