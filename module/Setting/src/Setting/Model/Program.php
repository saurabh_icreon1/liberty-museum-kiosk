<?php

/**
 * This is used for Setting module validation.
 * @package    Setting
 * @author     Icreon Tech - NS
 */

namespace Setting\Model;

use Setting\Module;
// Add these import statements
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\Adapter\Adapter;
use Base\Model\BaseModel;

/**
 * This class is used for Setting module validation.
 * @package    Setting
 * @author     Icreon Tech - NS
 */
class Program extends BaseModel {

    protected $inputFilter;
    protected $adapter;
    

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    /**
     * This method is used to convert form post array of create setting to Object of class type Program
     * @param $data
     * @return Void
     * @author Icreon Tech - NS
     */
    public function exchangeArray($param = array()) {
         
        $parameter = array();
        $parameter['programId'] = (isset($param['programId'])) ? $param['programId'] : '';
        $parameter['name'] = (isset($param['name'])) ? $param['name'] : '';
        $parameter['program_code'] = (isset($param['program_code'])) ? $param['program_code'] : '';
        $parameter['is_available_for_donation'] = (isset($param['is_available_for_donation'])) ? $param['is_available_for_donation'] : '0';
        $parameter['gl_code'] = (isset($param['gl_code'])) ? $param['gl_code'] : '';
        $parameter['major_gl_code'] = (isset($param['major_gl_code'])) ? $param['major_gl_code'] : '';
        $parameter['department'] = (isset($param['department'])) ? $param['department'] : '';       
        $parameter['designation'] = (isset($param['designation'])) ? $param['designation'] : '';        
        $parameter['allocation'] = (isset($param['allocation'])) ? $param['allocation'] : '';       
        $parameter['is_restriction'] = (isset($param['is_restriction'])) ? $param['is_restriction'] : '0';       
        $parameter['description'] = (isset($param['description'])) ? $param['description'] : '';       
 	  
       return $parameter;
    }
 

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

     
}

