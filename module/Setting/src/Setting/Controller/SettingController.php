<?php

/**
 * This controller is used in setting modules
 * @package    Setting
 * @author     Icreon Tech - AS
 */

namespace Setting\Controller;

use Base\Controller\BaseController;
use Base\Model\UploadHandler;
use Zend\View\Model\ViewModel;
use Setting\Model\Setting;
use Setting\Form\AddDropdownForm;
use Setting\Form\TableListForm;
use Setting\Form\SearchEmailTemplateForm;
use Setting\Form\EditMailTemplateForm;
use Setting\Form\StockNotificationForm;
use Setting\Form\GLCodeForm;
use Setting\Form\MembershipSavedSearch;
use Setting\Form\MembershipForm;
use Zend\Http\Request;
use Zend\Authentication\AuthenticationService;

/**
 * This class is used in Setting modules
 * @package    Setting
 * @author     Icreon Tech - AS
 */
class SettingController extends BaseController {

    protected $_SettingTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_flashMessage = null;

    function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        parent::__construct();
        $auth = new AuthenticationService();
        $this->auth = $auth;
    }

    /**
     * This function is used to get table and config file object
     * @return     array
     * @author Icreon Tech - AS
     */
    public function getSettingTable() {
        if (!$this->_SettingTable) {
            $sm = $this->getServiceLocator();
            $this->_SettingTable = $sm->get('Setting\Model\SettingTable');
            $this->_translator = $sm->get('translator');
            $this->_config = $sm->get('Config');
            $this->_adapter = $sm->get('dbAdapter');
        }
        return $this->_SettingTable;
    }

    /**
     * This action is used for listing dropdown menu
     * @param $getSearchArray
     * @return $viewModel
     * @author Icreon Tech - AS
     */
    public function getCrmMstTableListAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $tableListForm = new TableListForm();
        $messages = $this->_flashMessage->getMessages();
        $this->getSettingTable();
        $request = $this->getRequest();
        $msgArr = array_merge($this->_config['setting_messages']['config']['common'], $this->_config['setting_messages']['config']['get_dropdown'], $this->_config['setting_messages']['config']['pop_up']);
        $viewModel = new ViewModel();
        $searchParam['id'] = NULL;
        $searchResult = $this->getSettingTable()->getMstTableList($searchParam);
        $dropdownArray = array();
        $dropdownArray[] = 'Select';
        foreach ($searchResult as $key => $val) {
            $dropdownArray[$val['table_id']] = $val['table_name'];
        }
        $tableListForm->get('dropdown')->setAttribute('options', $dropdownArray);
        $viewModel->setVariables(array(
            'jsLangTranslate' => $msgArr,
            'messages' => $messages,
            'mstTableList' => $searchResult,
            'tableListForm' => $tableListForm
        ));
        return $viewModel;
    }

    /**
     * This action is used for dropdown menu popup
     * @param $getSearchArray
     * @return $viewModel
     * @author Icreon Tech - AS
     */
    public function getCrmMstTableContentListAction() {
        $this->checkUserAuthentication();
        $this->getSettingTable();
        $addDropdownForm = new AddDropdownForm();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $response = $this->getResponse();
        $msgArr = array_merge($this->_config['setting_messages']['config']['common'], $this->_config['setting_messages']['config']['pop_up'], $this->_config['setting_messages']['config']['get_dropdown']);
        $viewModel = new ViewModel();
        $table = '';
        $displayName = '';
        $searchResult = $this->getSettingTable()->getMstTableList($params);
        $viewModel->setTerminal(true);
        $dispName = '';
        $mstTable = '';
        $field1 = '';
        $field2 = '';
        $field3 = '';
        if (isset($searchResult[0]['table_name']) && !empty($searchResult[0]['table_name'])) {
            $dispName = $searchResult[0]['table_name'];
        }
        if (isset($searchResult[0]['table_orignal_name']) && !empty($searchResult[0]['table_orignal_name'])) {
            $mstTable = $searchResult[0]['table_orignal_name'];
        }
        if (isset($searchResult[0]['field1']) && !empty($searchResult[0]['field1'])) {
            $field1 = $searchResult[0]['field1'];
        }
        if (isset($searchResult[0]['field2']) && !empty($searchResult[0]['field2'])) {
            $field2 = $searchResult[0]['field2'];
        }
        if (isset($searchResult[0]['field3']) && !empty($searchResult[0]['field3'])) {
            $field3 = $searchResult[0]['field3'];
        }
        $viewModel->setVariables(array(
            'jsLangTranslate' => $msgArr,
            'displayName' => $dispName,
            'mstTable' => $mstTable,
            'id' => $params['id'],
            'idField' => $field1,
            'field' => $field2,
            'field2' => $field3,
            'addDropdown' => $addDropdownForm
        ));
        return $viewModel;
    }

    /**
     * This action is used for listing dropdown menu content
     * @param $getSearchArray
     * @return $viewModel
     * @author Icreon Tech - AS
     */
    public function getDropdownContentAction() {
        $this->checkUserAuthentication();
        $this->getSettingTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        parse_str($request->getPost('searchString'), $searchParam);
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        $searchParam['startIndex'] = $start;
        $searchParam['recordLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');
        $params = $this->params()->fromRoute();
        $searchParam['table'] = $params['table'];
        $searchParam['name'] = NULL;
        $searchParam['field'] = NULL;
        $searchResult = $this->getSettingTable()->getDropdownContent($searchParam);
        $dropdownArray = array();
        if (!empty($searchResult)) {
            for ($i = 0; $i < sizeof($searchResult); $i++) {
                $dropdownArray[$i] = array_values($searchResult[$i]);
            }
        }
        $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
        $jsonResult = array();
        $arrCell = array();
        $subArrCell = array();
        $jsonResult['page'] = $page;
        $jsonResult['records'] = $countResult[0]->RecordCount;
        $jsonResult['total'] = @ceil($countResult[0]->RecordCount / $limit);
        if (count($dropdownArray) > 0) {
            foreach ($dropdownArray as $val) {
                $arrCell['id'] = $val[0];
                $arrCell['cell'] = array($val[0], stripslashes($val[1]), stripslashes($val[2]), '');
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
        }
        return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    /**
     * This action is used for adding new value to  dropdown
     * @param $getSearchArray
     * @return $viewModel
     * @author Icreon Tech - AS
     */
    public function addCrmMstTableContentAction() {
        $this->checkUserAuthentication();
        $this->getSettingTable();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $response = $this->getResponse();
        $search = $request->getPost();
        $msgArr = array_merge($this->_config['setting_messages']['config']['common'], $this->_config['setting_messages']['config']['get_dropdown'], $this->_config['setting_messages']['config']['pop_up']);
        $searchParam['startIndex'] = NULL;
        $searchParam['recordLimit'] = NULL;
        $searchParam['sortField'] = NULL;
        $searchParam['sortOrder'] = NULL;
        $searchParam['table'] = $params['table'];
        if ($searchParam['table'] == 'tbl_mst_channels') {
            $searchParam['name'] = str_replace(' ','',$search->$params['field1']);
        } else {
            $searchParam['name'] = $search->$params['field1'];
        }
        $searchParam['field'] = $params['field1'];
        $searchResult = $this->getSettingTable()->getDropdownContent($searchParam);
        if ($searchResult[0]['total'] == 1) {
            $retArr = true;
            $msg = $msgArr['OPTION_EXIST'] . $search->displayName . $msgArr['DROPDOWN'];
        } else {
            $addParam['table'] = $searchParam['table'];
            $addParam['name'] = $searchParam['name'];
            $addParam['desc'] = $search->$params['field2'];
            $addParam['field'] = $searchParam['field'];
            $addParam['field2'] = $params['field2'];
            $addParam['added_by'] = $this->auth->getIdentity()->crm_user_id;
            $addParam['added_date'] = DATE_TIME_FORMAT;
            $retArr = $this->getSettingTable()->addDropdownContent($addParam);
            $msg = $msgArr['OPTION_ADDED'] . $msgArr['DROPDOWN_ADD'];
        }
        if ($retArr == true) {
            $messages = array('status' => "success", 'msg' => $msg);
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        } else {
            $messages = array('status' => "error");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This action is used to manage the stock notification
     * @return void
     * @author Icreon Tech - SR
     */
    public function manageStockNotificationAction() {
        $this->checkUserAuthentication();
        $this->getSettingTable();
        $msgArr = array_merge($this->_config['setting_messages']['config']['common'], $this->_config['setting_messages']['config']['stock']);
        $this->layout('crm');
        $stockNotificationForm = new StockNotificationForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $settingObj = new Setting($this->_adapter);
        $stockNotificationData = array();
        $stockNotificationData = $this->getSettingTable()->getStockNotification();
        if (!empty($stockNotificationData)) {
            $stockNotificationForm->get('notification_recipient')->setAttribute('value', $stockNotificationData[0]['notification_recipients']);
            $stockNotificationForm->get('subject')->setAttribute('value', $stockNotificationData[0]['message_subject']);
            $stockNotificationForm->get('message')->setAttribute('value', $stockNotificationData[0]['message_text']);
            $stockNotificationForm->get('is_send')->setAttribute('value', $stockNotificationData[0]['is_email_send']);
        }


        if ($request->isPost()) {
            $stockNotificationForm->setData($request->getPost());
            $stockNotificationForm->setInputFilter($settingObj->getInputFilterStockSetting());


            if (!$stockNotificationForm->isValid()) {
                $errors = $stockNotificationForm->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            $msg [$key] = $msgArr[$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {

                $settingObj->exchangeStockNotificationArray($stockNotificationForm->getData());
                $postData = array();
                $postData['notification_recipients'] = $settingObj->notificationRecipients;
                $postData['message_subject'] = $settingObj->messageSubject;
                $postData['message_text'] = $settingObj->messageText;
                $postData['is_email_send'] = $settingObj->isEmailSend;
                $postData['modified_date'] = DATE_TIME_FORMAT;
                $returnData = $this->getSettingTable()->updateStockNotification($postData);
                if ($returnData) {
                    $this->flashMessenger()->addMessage($msgArr['STOCK_UPDATE_SUCCESS']);
                    $messages = array('status' => "success");
                } else {
                    $this->flashMessenger()->addMessage($msgArr['STOCK_UPDATE_FAIL']);
                    $messages = array('status' => "error");
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }

        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'messages' => $messages,
            'jsLangTranslate' => $msgArr,
            'stockNotificationForm' => $stockNotificationForm
        ));

        return $viewModel;
    }

    /**
     * This action is used for edit value of  dropdown
     * @param $getSearchArray
     * @return $viewModel
     * @author Icreon Tech - AS
     */
    public function editCrmMstTableContentAction() {
        $this->checkUserAuthentication();
        $this->getSettingTable();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $response = $this->getResponse();
        $search = $request->getPost();
        $msgArr = array_merge($this->_config['setting_messages']['config']['common'], $this->_config['setting_messages']['config']['get_dropdown'], $this->_config['setting_messages']['config']['pop_up']);
        $searchResult = $this->getSettingTable()->getMstTableList($params);
        $searchParam['startIndex'] = NULL;
        $searchParam['recordLimit'] = NULL;
        $searchParam['sortField'] = NULL;
        $searchParam['sortOrder'] = NULL;
        $searchParam['table'] = $searchResult[0]['table_orignal_name'];
        $searchParam['name'] = addslashes($search->$params['field']);
        $searchParam['field'] = addslashes($params['field']);
        $searchCheck = $this->getSettingTable()->getDropdownContent($searchParam);
        if (isset($searchCheck[0]['total'])) {
            $editParam['table'] = $searchResult[0]['table_orignal_name'];
            $editParam['name'] = addslashes($search->$params['field']);
            $editParam['desc'] = addslashes($search->$params['field2']);
            $editParam['field1'] = $searchResult[0]['field1'];
            $editParam['field2'] = $searchResult[0]['field2'];
            $editParam['field3'] = $searchResult[0]['field3'];
            $editParam['id'] = $search->id;
            $editParam['mod_by'] = $this->auth->getIdentity()->crm_user_id;
            $editParam['mod_date'] = DATE_TIME_FORMAT;
            if (isset($editParam['name']) && !empty($editParam['name'])) {
                $retArr = $this->getSettingTable()->editDropdownContent($editParam);
                $msg = $msgArr['OPTION_ADDED'] . $searchResult[0]['table_name'] . $msgArr['DROPDOWN_UPDATE'];
            }
        }
        $messages = array('status' => "success", 'msg' => $msg);
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This action is used for delete value of  dropdown
     * @param $getSearchArray
     * @return $viewModel
     * @author Icreon Tech - AS
     */
    public function deleteCrmMstTableContentAction() {
        $this->checkUserAuthentication();
        $this->getSettingTable();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $response = $this->getResponse();
        $search = $request->getPost();
        $delParam['table'] = $search['table'];
        $delParam['id'] = $params['id'];
        $delParam['delField'] = $search['id'];
        $delParam['mod_by'] = $this->auth->getIdentity()->crm_user_id;
        $delParam['mod_date'] = DATE_TIME_FORMAT;
        $msgArr = array_merge($this->_config['setting_messages']['config']['common'], $this->_config['setting_messages']['config']['get_dropdown'], $this->_config['setting_messages']['config']['pop_up']);
        $searchResult = $this->getSettingTable()->deleteMstTableContent($delParam);
        $msg = $msgArr['OPTION_DELETED'] . $search['displayName'] . $msgArr['SUCCESSFULLY'];
        //$this->_flashMessage->addMessage($msgArr['OPTION_DELETED'] . $search['displayName'] . $msgArr['SUCCESSFULLY']);
        $messages = array('status' => "success", 'msg' => $msg);
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This action is used for email tempaltes listing
     * @return email teampltes listing
     * @author Icreon Tech - SR
     */
    public function getCrmEmailTemplatesAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getSettingTable();
        $msgArr = array_merge($this->_config['setting_messages']['config']['common'], $this->_config['setting_messages']['config']['searchEmailTemplate']);
        $request = $this->getRequest();
        $searchEmailTemplateForm = new SearchEmailTemplateForm();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        $viewModel->setVariables(array(
            'searchEmailTemplateForm' => $searchEmailTemplateForm,
            'mode' => '',
            'messages' => $messages,
            'jsLangTranslate' => $msgArr
        ));
        return $viewModel;
    }

    /**
     * This Action is used for the email template listing
     * @return this will be result array all email .
     * @author Icreon Tech - SR
     */
    public function getCrmEmailTemplateListAction() {
        $this->checkUserAuthentication();
        $this->getSettingTable();
        $msgArr = array_merge($this->_config['setting_messages']['config']['common'], $this->_config['setting_messages']['config']['searchEmailTemplate']);
        $request = $this->getRequest();
        $response = $this->getResponse();
        parse_str($request->getPost('searchString'), $searchParam);
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;

        $searchParam['email_id'] = (isset($searchParam['email_id']) && $searchParam['email_id'] != '') ? $searchParam['email_id'] : '';
        $searchParam['name'] = (isset($searchParam['name']) && $searchParam['name'] != '') ? trim($searchParam['name']) : '';
        $searchParam['startIndex'] = $start;
        $searchParam['recordLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');
        $searchResult = $this->_SettingTable->searchEmailTemplate($searchParam);
        $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();

        $jsonResult = array();
        $arrCell = array();
        $subArrCell = array();
        $jsonResult['page'] = $page;
        $jsonResult['records'] = $countResult[0]->RecordCount;
        $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
        if ($jsonResult['records'] > 0) {
            foreach ($searchResult as $val) {
                $arrCell['id'] = $val['email_id'];
                $viewLabel = $msgArr['VIEW'];
                $editLabel = $msgArr['EDIT'];
                $deleteLabel = $msgArr['DELETE'];
                $statusArray = $this->statusArray();
                $view = "<a class='view-icon' href='/crm-email-template-info/" . $this->encrypt($val['email_id']) . "/" . $this->encrypt('view') . "'><div class='tooltip'>" . $viewLabel . "<span></span></div></a>";
                $edit = "<a class='edit-icon' href='/crm-email-template-info/" . $this->encrypt($val['email_id']) . "/" . $this->encrypt('edit') . "'><div class='tooltip'>" . $editLabel . "<span></span></div></a>";
                $arrCell['cell'] = array(stripslashes($val['email_title']), $this->OutputDateFormat($val['added_date'], 'dateformatampm'), $this->OutputDateFormat($val['modified_date'], 'dateformatampm'), $view . $edit);

                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
        }
        return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    /**
     * This action is used for manage gl code
     * @param void
     * @return array of json
     * @author Icreon Tech - DT
     */
    public function manageGlCodesAction() {
        $this->checkUserAuthentication();
        $this->getSettingTable();
        $msgArr = array_merge($this->_config['setting_messages']['config']['common'], $this->_config['setting_messages']['config']['gl_code']);
        $this->layout('crm');
        $glCodeForm = new GLCodeForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $settingObj = new Setting($this->_adapter);
        $glCodeData = array();
        $glCodeData = $this->getSettingTable()->getGLCodes();
        $productTypeArr = array();
        $productTypeArr['F_O_F'] = 4;
        $productTypeArr['INV'] = 1;
        //$productTypeArr['INV_PRO'] = '';
        $productTypeArr['KIO_FEE'] = 3;
        $productTypeArr['PASS_DOC'] = 5;
        $productTypeArr['REFUND'] = '';
        $productTypeArr['SHI_AND_HAND'] = '';
        $productTypeArr['TAX'] = '';
        $productTypeArr['UN_DON'] = 2;
        $productTypeArr['UN_DON_M'] = 2;
        $productTypeArr['W_O_H'] = 6;
        $productTypeArr['DISCOUNT'] = '';
        if (!empty($glCodeData)) {
            foreach ($glCodeData as $glData) {
                $glCodeForm->get($glData['gl_code'])->setValue($glData['gl_code_value']);
                $glCodeForm->get($glData['gl_code'] . '_department')->setValue($glData['department']);
                $glCodeForm->get($glData['gl_code'] . '_designation')->setValue($glData['designation']);
                $glCodeForm->get($glData['gl_code'] . '_allocation')->setValue($glData['allocation']);
                $glCodeForm->get($glData['gl_code'] . '_restriction')->setValue($glData['is_restriction']);
            }
        }
        if ($request->isPost()) {
            $glCodeForm->setData($request->getPost());
            $glCodeForm->setInputFilter($settingObj->getInputFilterGLCodeSetting());


            if (!$glCodeForm->isValid()) {
                $errors = $glCodeForm->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            $msg [$key] = $msgArr[$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $postFormArr = $glCodeForm->getData();
                foreach ($postFormArr as $key => $value) {
                    if ($key != 'save' && $key != 'csrf' && !strpos($key, '_department') && !strpos($key, '_designation') && !strpos($key, '_allocation') && !strpos($key, '_restriction')) {
                        $params = array();
                        $params['gl_code'] = $key;
                        $params['gl_code_value'] = $value;
                        $params['product_type_id'] = $productTypeArr[$key];
                        $params['department'] = $postFormArr[$key . '_department'];
                        $params['designation'] = $postFormArr[$key . '_designation'];
                        $params['allocation'] = $postFormArr[$key . '_allocation'];
                        $params['is_restriction'] = $postFormArr[$key . '_restriction'];
                        $params['added_date'] = DATE_TIME_FORMAT;
                        $params['modified_date'] = DATE_TIME_FORMAT;
                        $returnData = $this->getSettingTable()->updateGlCode($params);
                        unset($params);
                    }
                }
                if ($returnData) {
                    $this->flashMessenger()->addMessage($msgArr['GL_CODE_UPDATED']);
                    $messages = array('status' => "success");
                } else {
                    $this->flashMessenger()->addMessage($msgArr['GL_CODE_UPDATE_FAIL']);
                    $messages = array('status' => "error");
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }

        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'messages' => $messages,
            'jsLangTranslate' => $msgArr,
            'glCodeForm' => $glCodeForm,
        ));

        return $viewModel;
    }

    /**
     * This action is used for view the details of the mail template
     * @param id of the email template
     * @return templates details
     * @author Icreon Tech - SR
     */
    public function getCrmEmailTemplateinfoAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getSettingTable();
        $msgArr = array_merge($this->_config['setting_messages']['config']['common'], $this->_config['setting_messages']['config']['searchEmailTemplate']);

        $params = $this->params()->fromRoute();
        $searchParam['email_id'] = $this->decrypt($params['id']);
        $searchParam['startIndex'] = '';
        $searchParam['recordLimit'] = '';
        $searchParam['sortField'] = '';
        $searchParam['sortOrder'] = '';
        $searchResult = $this->getSettingTable()->searchEmailTemplate($searchParam);
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => $msgArr,
            'searchResult' => $searchResult[0]
        ));
        return $viewModel;
    }

    /**
     * This action is used for view the details of the mail template
     * @param id of the email template
     * @return templates details
     * @author Icreon Tech - SR
     */
    public function editCrmEmailTemplateAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getSettingTable();
        $editMailTemplateForm = new EditMailTemplateForm();
        $msgArr = array_merge($this->_config['setting_messages']['config']['common'], $this->_config['setting_messages']['config']['searchEmailTemplate']);
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $settingObj = new Setting($this->_adapter);
        $searchParam['email_id'] = $this->decrypt($params['id']);
        $searchParam['startIndex'] = '';
        $searchParam['recordLimit'] = '';
        $searchParam['sortField'] = '';
        $searchParam['sortOrder'] = '';
        $searchResult = $this->getSettingTable()->searchEmailTemplate($searchParam);
        if ($request->getPost('module_variable') != '')
            $editMailTemplateForm->get('module_variable')->setAttribute('options', array($request->getPost('module_variable') => $request->getPost('module_variable')));
        else
            $editMailTemplateForm->get('module_variable')->setAttribute('value', '');
        $editMailTemplateForm->get('email_title')->setAttribute('value', stripslashes($searchResult[0]['email_title']));
        $editMailTemplateForm->get('email_id')->setAttribute('value', $this->encrypt(stripslashes($searchResult[0]['email_id'])));
        $editMailTemplateForm->get('email_description')->setAttribute('value', stripslashes($searchResult[0]['email_description']));
        $editMailTemplateForm->get('email_subject')->setAttribute('value', stripslashes($searchResult[0]['email_subject']));

        //$email_message_body = str_replace("{--SiteHomeURL--}",SITE_URL,$searchResult[0]['email_message_body']);
        $email_message_body = $searchResult[0]['email_message_body'];
        $editMailTemplateForm->get('email_message_body')->setAttribute('value', $email_message_body);


        $editMailTemplateForm->get('email_set_text')->setAttribute('value', stripslashes($searchResult[0]['email_set_text']));
        $editMailTemplateForm->get('email_module_id')->setAttribute('value', stripslashes($searchResult[0]['email_module_id']));

        $moduleVariableResult = $this->getSettingTable()->getModuleVariable($searchParam);
        $moduleArray = array();
        $moduleArray[''] = 'Select';
        foreach ($moduleVariableResult as $val) {
            $moduleArray[$val['module_id']] = $val['module_name'];
            $moduleVariableArray[$val['module_id']][] = trim($val['attribute_name'] . '~' . $val['attribute_variable']);
        }
        $editMailTemplateForm->get('email_module_id')->setAttribute('options', $moduleArray);
        if ($request->isPost()) {
            $flag = 0;
            $editMailTemplateForm->setData($request->getPost());
            $editMailTemplateForm->setInputFilter($settingObj->getInputFilterEmailTemplate());

            if (!$editMailTemplateForm->isValid()) {
                $errors = $editMailTemplateForm->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            $msg [$key] = $msgArr[$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $settingObj->exchangeEmailTemplateArray($editMailTemplateForm->getData());
                $postData = array();
                $postData['email_id'] = $this->decrypt($params['id']);
                $postData['email_title'] = $settingObj->email_title;
                $postData['email_description'] = $settingObj->email_description;
                $postData['email_subject'] = $settingObj->email_subject;
                $postData['email_message_body'] = $settingObj->email_message_body;
                $postData['email_module_id'] = $settingObj->email_module_id;
                $postData['email_set_text'] = $settingObj->email_set_text;
                $postData['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                $postData['modified_date'] = DATE_TIME_FORMAT;
                $flag = $this->getSettingTable()->updateEmailTemplate($postData);
                if ($flag) {
                    $this->flashMessenger()->addMessage($msgArr['EMAIL_SUCCESS_MSG']);
                    $messages = array('status' => "success");
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                }
            }
        }
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => $msgArr,
            'editMailTemplateForm' => $editMailTemplateForm,
            'moduleVariableArray' => \Zend\Json\Json::encode($moduleVariableArray)
        ));
        return $viewModel;
    }

    /**
     * This action is used for add membership
     * @param void
     * @return void
     * @author Icreon Tech - DT
     */
    public function addCrmMembershipAction() {
        $this->checkUserAuthentication();
        $this->getSettingTable();
        $msgArr = array_merge($this->_config['setting_messages']['config']['common'], $this->_config['setting_messages']['config']['membership']);
        $this->layout('crm');
        $membershipForm = new MembershipForm();
        $membershipSavedSearch = new MembershipSavedSearch();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $settingObj = new Setting($this->_adapter);
        $membserhipSettings = $this->_config['membership_settings'];
        /* Set value of financial year from current year to next 5 years */
        $yearArr = array();
        for ($i = date('Y'); $i <= date('Y') - 1 + $membserhipSettings['numOfYears']; $i++) {
            $yearArr[$i] = $i;
        }
        $membershipForm->get('validity_time_financial')->setAttribute('options', $yearArr);
        /* End set value of financial year from current year to next 5 years */

        /* Set value of start date from current year to next 5 years */
        $yearArr = array();
        for ($i = 1; $i <= $membserhipSettings['numOfYears']; $i++) {
            $yearArr[$i] = $i . " +";
        }
        $membershipForm->get('validity_time_year')->setAttribute('options', $yearArr);
        /* End set value of start date from current year to next 5 years */

        /* Set value search save days */
        $searchSaveDays = array();
        for ($i = 0; $i <= $membserhipSettings['savedSearchDays']; $i++) {
            $searchSaveDays[$i] = $i;
        }
        $membershipSavedSearch->get('saved_search_in_days[]')->setAttributes(array('options' => $searchSaveDays, 'id' => 'saved_search_in_days_1'));

        /* End set value search save days */

        /* Set value search save types */
        $searchSaveTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getSearchSaveTypes();
        $searchSaveTypesArr = array('' => 'Select');
        if (!empty($searchSaveTypes)) {
            foreach ($searchSaveTypes as $searchSave) {
                $searchSaveTypesArr[$searchSave['saved_search_type_id']] = $searchSave['saved_search_type'];
            }
        }
        $membershipSavedSearch->get('saved_search_type[]')->setAttributes(array('options' => $searchSaveTypesArr, 'id' => 'saved_search_type_1'));
        /* End set value search save types */

        $membershipForm->add($membershipSavedSearch);
        if ($request->isPost()) {
            $membershipForm->setData($request->getPost());

            $membershipForm->setInputFilter($settingObj->getInputFilterMembership());


            if (!$membershipForm->isValid()) {
                $errors = $membershipForm->getMessages();
                if (!empty($errors)) {
                    $msg = array();
                    foreach ($errors as $key => $row) {
                        if (!empty($row) && $key != 'submit') {
                            foreach ($row as $rower) {
                                $msg [$key] = $msgArr[$rower];
                            }
                        }
                    }
                    $messages = array('status' => "error", 'message' => $msg);
                }
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                //$postFormArr = $membershipForm->getData();
                $postFormArr = $request->getPost()->toArray();
                //for add membership
                $postFormArr['added_by'] = $this->auth->getIdentity()->crm_user_id;
                $postFormArr['added_date'] = DATE_TIME_FORMAT;
                $postFormArr['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                $postFormArr['modified_date'] = DATE_TIME_FORMAT;

                $postFormArr['validity_time'] = ($postFormArr['validity_type'] == 1) ? $postFormArr['validity_time_financial'] : $postFormArr['validity_time_year'];

                $createMembershipArr = $settingObj->getCreateMembershipArr($postFormArr);
                $membershipId = $this->_SettingTable->saveMembership($createMembershipArr);
                if (!empty($postFormArr['saved_search_type'])) {
                    $savedSearchTypes = array_unique($postFormArr['saved_search_type']);
                    foreach ($savedSearchTypes as $key => $searchType) {
                        if ($postFormArr['saved_search_in_days'][$key] != '' && $postFormArr['saved_search_in_days'][$key] != 0) {
                            $searchFormArr = array();
                            $searchFormArr['membership_id'] = $membershipId;
                            $searchFormArr['saved_search_in_days'] = $postFormArr['saved_search_in_days'][$key];
                            $searchFormArr['saved_search_type_id'] = $searchType;
                            $searchFormArr['added_date'] = DATE_TIME_FORMAT;
                            $searchFormArr['modified_date'] = DATE_TIME_FORMAT;
                            $createMembershipSearchSaveTypeArr = $settingObj->getCreateMembershipSearchSaveTypeArr($searchFormArr);
                            $membershipSaveSearchTypeId = $this->_SettingTable->saveMembershipSaveSearchType($createMembershipSearchSaveTypeArr);
                        }
                    }
                }
                $this->flashMessenger()->addMessage($msgArr['MEMBERSHIP_CREATED']);
                $messages = array('status' => "success");
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'jsLangTranslate' => $msgArr,
            'membershipForm' => $membershipForm,
        ));
        return $viewModel;
    }

    /**
     * This action is used for searching of membership
     * @return     array
     * @author Icreon Tech - DT
     */
    public function getCrmMembershipAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $this->getSettingTable();
        $msgArr = array_merge($this->_config['setting_messages']['config']['common'], $this->_config['setting_messages']['config']['membership']);
        $setting = new Setting($this->_adapter);
        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $response = $this->getResponse();
            $postArray = $request->getPost()->toArray();
            parse_str($request->getPost('searchString'), $searchParam);
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit; // do not put $limit*($page - 1)
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            $searchMembershipArr = $setting->getMembershipSearchArr($searchParam);
            $seachResult = $this->_SettingTable->getAllMembership($searchMembershipArr);
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $jsonResult['page'] = $request->getPost('page');
            $jsonResult['records'] = $countResult[0]->RecordCount;
            $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
            if (!empty($seachResult)) {
                foreach ($seachResult as $val) {
                    $arrCell['id'] = $val['membership_id'];
                    $encryptId = $this->encrypt($val['membership_id']);
                    $editLink = '<a href="/edit-crm-membership/' . $encryptId . '" class="edit-icon"><div class="tooltip">Edit<span></span></div></a>';
                    if ($val['membership_id'] != 1) {
                        $deleteLink = '<a onclick="deleteMembership(\'' . $encryptId . '\')" href="#delete_membership_block" class="delete_membership delete-icon"><div class="tooltip">Delete<span></span></div></a>';
                    } else {
                        $deleteLink = '';
                    }
                    $actions = $editLink . $deleteLink;
                    if (!empty($val['level_color'])) {
                        $divColor = "<div style='width:100px;height:20px;border:2px solid #000000;background-color:" . $val['level_color'] . "'></div>";
                    } else {
                        $divColor = 'N/A';
                    }
                    $statusLink = '<div id="status_' . $val['membership_id'] . '"><a onclick="changeStatus(' . $val['membership_id'] . ',' . $val['is_active'] . ')" href="javascript:void(0);">' . $val['active_status'] . '<div class="tooltip">' . $val['active_status'] . '<span></span></div></a></div>';
                    $arrCell['cell'] = array($this->chunkData($val['membership_title'], 20), $divColor, $val['minimun_donation_amount'], $statusLink, $actions);
                    $subArrCell[] = $arrCell;
                    $jsonResult['rows'] = $subArrCell;
                }
            } else {
                $jsonResult['rows'] = array();
            }
            return $response->setContent(\Zend\Json\Json::encode($jsonResult));
        } else {
            $this->layout('crm');
            $messages = array();

            if ($this->_flashMessage->hasMessages()) {
                $messages = $this->_flashMessage->getMessages();
            }
            $viewModel = new ViewModel();

            $response = $this->getResponse();
            $viewModel->setVariables(array(
                'messages' => $messages,
                'jsLangTranslate' => $msgArr
                    )
            );
            return $viewModel;
        }
    }

    /**
     * This action is used to delete membership
     * @param  void
     * @return     json format string
     * @author Icreon Tech - DT
     */
    public function deleteCrmMembershipAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $membershipId = $request->getPost('membershipId');
            if ($membershipId != '') {
                $membershipId = $this->decrypt($membershipId);
                $postArr = $request->getPost();
                $postArr['membershipId'] = $membershipId;
                $this->getSettingTable();
                $msgArr = array_merge($this->_config['setting_messages']['config']['common'], $this->_config['setting_messages']['config']['membership'], $this->_config['grid_config']);
                $setting = new Setting($this->_adapter);

                $response = $this->getResponse();
                $membershipArr = $setting->getArrayForRemoveMembership($postArr);
                $this->_SettingTable->removeMembershipById($membershipArr);
                $this->flashMessenger()->addMessage($msgArr['MEMBERSHIP_DELETED_SUCCESSFULLY']);
                $messages = array('status' => "success", 'message' => $msgArr['MEMBERSHIP_DELETED_SUCCESSFULLY']);
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                return $this->redirect()->toRoute('getCrmMembership');
            }
        } else {
            return $this->redirect()->toRoute('getCrmMembership');
        }
    }

    /**
     * This action is used to change membership status
     * @param  void
     * @return json format string
     * @author Icreon Tech - DT
     */
    public function changeCrmMembershipStatusAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $membershipId = $request->getPost('membershipId');
            if ($membershipId != '') {
                $postArr = $request->getPost()->toArray();
                $postArr['membership_id'] = $membershipId;
                $postArr['is_active'] = ($request->getPost('status') == 1) ? '0' : '1';
                $this->getSettingTable();
                $msgArr = array_merge($this->_config['setting_messages']['config']['common'], $this->_config['setting_messages']['config']['membership'], $this->_config['grid_config']);
                $setting = new Setting($this->_adapter);

                $response = $this->getResponse();
                $membershipArr = $setting->getArrayForChangeMembershipStatus($postArr);
                $this->_SettingTable->changeMembershipStatusById($membershipArr);
                $messages = array('status' => "success");
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                return $this->redirect()->toRoute('getCrmMembership');
            }
        } else {
            return $this->redirect()->toRoute('getCrmMembership');
        }
    }

    /**
     * This action is used to add more save search
     * @param void
     * @return void
     * @author Icreon Tech - DT
     */
    public function addSaveSearchAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $totalSaveSearch = $request->getPost('totalSaveSearch');
            $this->checkUserAuthenticationAjx();
            $this->getSettingTable();
            $msgArr = array_merge($this->_config['setting_messages']['config']['common'], $this->_config['setting_messages']['config']['membership']);
            $membershipSavedSearch = new MembershipSavedSearch();
            $membserhipSettings = $this->_config['membership_settings'];
            /* Set value search save days */
            $searchSaveDays = array();
            for ($i = 0; $i <= $membserhipSettings['savedSearchDays']; $i++) {
                $searchSaveDays[$i] = $i;
            }
            $savedSearchDayId = 'saved_search_in_days_' . ($totalSaveSearch + 1);
            $membershipSavedSearch->get('saved_search_in_days[]')->setAttributes(array('options' => $searchSaveDays, 'id' => $savedSearchDayId));
            /* End set value search save days */

            /* Set value search save types */
            $searchSaveTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getSearchSaveTypes();
            $searchSaveTypesArr = array('' => 'Select');
            if (!empty($searchSaveTypes)) {
                foreach ($searchSaveTypes as $searchSave) {
                    $searchSaveTypesArr[$searchSave['saved_search_type_id']] = $searchSave['saved_search_type'];
                }
            }
            $savedSearchTypeId = 'saved_search_type_' . ($totalSaveSearch + 1);
            $membershipSavedSearch->get('saved_search_type[]')->setAttributes(array('options' => $searchSaveTypesArr, 'id' => $savedSearchTypeId));
            /* End set value search save types */
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => $msgArr,
                'membershipSavedSearch' => $membershipSavedSearch,
                'totalSaveSearch' => $totalSaveSearch
            ));
            return $viewModel;
        } else {
            return $this->redirect()->toRoute('getCrmMembership');
        }
    }

    /**
     * This action is used for edit user membership
     * @param void
     * @return void
     * @author Icreon Tech - DT
     */
    public function editCrmMembershipAction() {
        $this->checkUserAuthentication();
        $this->getSettingTable();
        $msgArr = array_merge($this->_config['setting_messages']['config']['common'], $this->_config['setting_messages']['config']['membership']);
        $this->layout('crm');
        $membershipForm = new MembershipForm();
        $membershipSavedSearch = new MembershipSavedSearch();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $settingObj = new Setting($this->_adapter);
        $membserhipSettings = $this->_config['membership_settings'];
        /* Set value of financial year from current year to next 5 years */
        $yearArr = array();
        for ($i = date('Y'); $i <= date('Y') - 1 + $membserhipSettings['numOfYears']; $i++) {
            $yearArr[$i] = $i;
        }
        $membershipForm->get('validity_time_financial')->setAttribute('options', $yearArr);
        /* End set value of financial year from current year to next 5 years */

        /* Set value of start date from current year to next 5 years */
        $yearArr = array();
        for ($i = 1; $i <= $membserhipSettings['numOfYears']; $i++) {
            $yearArr[$i] = $i . " +";
        }
        $membershipForm->get('validity_time_year')->setAttribute('options', $yearArr);
        /* End set value of start date from current year to next 5 years */

        /* Set value search save days */
        $searchSaveDays = array();
        for ($i = 0; $i <= $membserhipSettings['savedSearchDays']; $i++) {
            $searchSaveDays[$i] = $i;
        }
        $membershipSavedSearch->get('saved_search_in_days[]')->setAttributes(array('options' => $searchSaveDays, 'id' => 'saved_search_in_days_1'));
        /* End set value search save days */

        /* Set value search save types */
        $searchSaveTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getSearchSaveTypes();
        $searchSaveTypesArr = array('' => 'Select');
        if (!empty($searchSaveTypes)) {
            foreach ($searchSaveTypes as $searchSave) {
                $searchSaveTypesArr[$searchSave['saved_search_type_id']] = $searchSave['saved_search_type'];
            }
        }
        $membershipSavedSearch->get('saved_search_type[]')->setAttributes(array('options' => $searchSaveTypesArr, 'id' => 'saved_search_type_1'));
        /* End set value search save types */

        $membershipForm->add($membershipSavedSearch);
        $glCodeData = array();
        if ($request->isPost()) {
            $membershipForm->setData($request->getPost());

            $membershipForm->setInputFilter($settingObj->getInputFilterEditMembership());
            $msg = array();
            if (!$membershipForm->isValid()) {
                $errors = $membershipForm->getMessages();
                if (!empty($errors)) {
                    foreach ($errors as $key => $row) {
                        if (!empty($row) && $key != 'submit') {
                            foreach ($row as $rower) {
                                $msg [$key] = $msgArr[$rower];
                            }
                        }
                    }
                    $messages = array('status' => "error", 'message' => $msg);
                }
            }
            $postFormArr = $request->getPost()->toArray();
            $membershipId = $postFormArr['membership_id'];
            $membershipDetailArr = array();
            $membershipDetailArr[] = $membershipId;
            $membershipDetailArr[] = $postFormArr['membership_title'];
            $getMembershipCheckDetail = $this->_SettingTable->checkMembershipExist($membershipDetailArr);
            if ($getMembershipCheckDetail['totalRecord'] > 0) {
                $msg ['membership_title'] = $msgArr['MEMBERSHIP_TITLE_EXIST'];
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                //for edit membership
                $postFormArr['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                $postFormArr['modified_date'] = DATE_TIME_FORMAT;

                $postFormArr['validity_time'] = ($postFormArr['validity_type'] == 1) ? $postFormArr['validity_time_financial'] : $postFormArr['validity_time_year'];
                $editMembershipArr = $settingObj->getEditMembershipArr($postFormArr);
                $membershipId = $this->_SettingTable->editMembership($editMembershipArr);

                /* remove member search */
                $deleteSearchArr = array();
                $deleteSearchArr[] = $membershipId;
                $this->_SettingTable->deleteMembershipSaveSearchType($deleteSearchArr);
                /* end remove member search */
                /* add new member search */
                if (!empty($postFormArr['saved_search_type'])) {
                    $savedSearchTypes = array_unique($postFormArr['saved_search_type']);
                    foreach ($savedSearchTypes as $key => $searchType) {
                        if ($postFormArr['saved_search_in_days'][$key] != '' && $postFormArr['saved_search_in_days'][$key] != 0) {
                            $searchFormArr = array();
                            $searchFormArr['membership_id'] = $membershipId;
                            $searchFormArr['saved_search_in_days'] = $postFormArr['saved_search_in_days'][$key];
                            $searchFormArr['saved_search_type_id'] = $searchType;
                            $searchFormArr['added_date'] = DATE_TIME_FORMAT;
                            $searchFormArr['modified_date'] = DATE_TIME_FORMAT;
                            $createMembershipSearchSaveTypeArr = $settingObj->getCreateMembershipSearchSaveTypeArr($searchFormArr);
                            $membershipSaveSearchTypeId = $this->_SettingTable->saveMembershipSaveSearchType($createMembershipSearchSaveTypeArr);
                        }
                    }
                }
                /* end add new member search */
                $this->flashMessenger()->addMessage($msgArr['MEMBERSHIP_UPDATED']);
                $messages = array('status' => "success");
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        } else {
            $params = $this->params()->fromRoute();
            if (isset($params['membership_id']) && $params['membership_id'] != '') {
                $membershipId = $this->decrypt($params['membership_id']);
                $membershipDetailArr = array();
                $membershipDetailArr[] = $membershipId;
                $getMembershipDetail = $this->_SettingTable->getMembershipById($membershipDetailArr);
                $this->setValueForElement($getMembershipDetail, $membershipForm);

                if ($getMembershipDetail['validity_type'] == 1) {
                    $membershipForm->get('validity_time_financial')->setValue($getMembershipDetail['validity_time']);
                } else if ($getMembershipDetail['validity_type'] == 2) {
                    $membershipForm->get('validity_time_year')->setValue($getMembershipDetail['validity_time']);
                }
                $membershipForm->get('membership_id')->setValue($membershipId);
                if ($membershipId == 1) {
                    $membershipForm->get('minimun_donation_amount')->setAttribute('readonly', 'readonly');
                }
                $membershipForm->get('save')->setValue('UPDATE');
            }
            $viewModel = new ViewModel();
            $viewModel->setVariables(array(
                'jsLangTranslate' => $msgArr,
                'membershipForm' => $membershipForm,
                'membershipDetail' => $getMembershipDetail
            ));
            return $viewModel;
        }
    }

    /**
     * This Action is used to set post array values to elements value of form
     * @param $postArr Array,$form Array
     * @return void
     * @author Icreon Tech -DT
     */
    public function setValueForElement($postArr = array(), $form = array()) {
        if (!is_array($postArr)) {
            $postArr = $postArr->toArray();
        }
        $elements = $form->getElements();
        foreach ($elements as $elementName => $elementObject) {

            if (array_key_exists($elementName, $postArr)) {
                if (is_array($postArr[$elementName])) {
                    $value = implode(',', $postArr[$elementName]);
                } else {

                    $value = $postArr[$elementName];
                }
                $elementObject->setValue($value);
            }
        }
    }

    /**
     * This action is used check the mail template by sending the email
     * @param void
     * @return void
     * @author Icreon Tech - DT
     */
    public function sendEmailTemplateAction() {
        $this->checkUserAuthentication();
        $this->getSettingTable();
        $msgArr = array_merge($this->_config['setting_messages']['config']['common'], $this->_config['setting_messages']['config']['searchEmailTemplate']);
        $request = $this->getRequest();
        $response = $this->getResponse();
        $editMailTemplateForm = new EditMailTemplateForm();
        $flag = 0;
        $emailArray = array();
        $emailArray['emailId'] = $request->getPost('emailId');
        $emailArray['subject'] = stripslashes($request->getPost('subject'));
        $emailArray['message'] = stripslashes($request->getPost('message'));
        $emailArray['admin_email'] = $this->_config['soleif_email']['email'];
        $emailArray['admin_name'] = $this->_config['soleif_email']['name'];
        $flag = $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmailTemplate($emailArray, $this->_config['email_options']);
        if ($flag) {
            $messages = array('status' => "success", 'message' => $msgArr['EMAIL_SEND_SUCCESS_MSG']);
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        } else {
            $messages = array('status' => "error", 'message' => $msgArr['EMAIL_SEND_ERROR_MSG']);
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This action is used to get membership detail for familiy history on change on contact name
     * @return     array
     * @author Icreon Tech - DT
     */
    public function getMembershipDetailAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $this->getSettingTable();
        $msgArr = array_merge($this->_config['setting_messages']['config']['common'], $this->_config['setting_messages']['config']['membership']);
        $setting = new Setting($this->_adapter);
        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $response = $this->getResponse();
            /* Get membership detail for contact */
            $form_data = $request->getPost();
            if (!empty($form_data->contact_name_id)) {
                $searchParam['user_id'] = $form_data->contact_name_id;
                $get_contact_arr = $this->getServiceLocator()->get('User\Model\ContactTable')->getContact($searchParam);

                $membershipId = (!empty($get_contact_arr[0]['membership_id'])) ? $get_contact_arr[0]['membership_id'] : 1;

                $membershipDetailArr = array();
                $membershipDetailArr[] = $membershipId;
                $getMembershipDetail = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);

                /* Get user bonus family history */
                $searchParam = array();
                $searchParam['user_id'] = $form_data->contact_name_id;
                $userFamilyHistoryBonusData = $this->getServiceLocator()->get('Passenger\Model\FamilyhistoryTable')->getUserFamilyHistoryBonus($searchParam);
                if (!empty($userFamilyHistoryBonusData)) {
                    $getMembershipDetail['max_stories'] = $getMembershipDetail['max_stories'] + $userFamilyHistoryBonusData[0]['max_stories'];
                    $getMembershipDetail['max_story_characters'] = $getMembershipDetail['max_story_characters'] + $userFamilyHistoryBonusData[0]['max_story_characters'];
                    $getMembershipDetail['max_passengers'] = $getMembershipDetail['max_passengers'] + $userFamilyHistoryBonusData[0]['max_passengers'];
                    $getMembershipDetail['max_saved_histories'] = $getMembershipDetail['max_saved_histories'] + $userFamilyHistoryBonusData[0]['max_saved_histories'];
                }
                /* End get user bonus family history */
            }
            /* End get membership detail for contact */
            return $response->setContent(\Zend\Json\Json::encode($getMembershipDetail));
        }
    }

    function buildTreeArray(array $elements) {
        $branch = array();
        foreach ($elements as $element) {
            $element['id'] = $element['id'];
            $element['name'] = $element['name'];
            $element['description'] = $element['description'];
            $branch[] = $element;
        }

        return $branch;
    }

    /**
     * This action is used for listing dropdown menu content
     * @param $getSearchArray
     * @return $viewModel
     * @author Icreon Tech - AS
     */
    public function editDropdownDragDropAction() {
        $this->checkUserAuthentication();
        $this->getSettingTable();
        $msgArr = array_merge($this->_config['setting_messages']['config']['common'], $this->_config['setting_messages']['config']['get_dropdown'], $this->_config['setting_messages']['config']['pop_up']);
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $parentId = $request->getPost('parentId');
            $sibling = $request->getPost('siblings');
            $childCatArr = explode(',', $request->getPost('siblings'));
            $order = 1;
            foreach ($childCatArr as $key) {
                $param = array();
                $param['parentId'] = $request->getPost('parentId');
                $param['menuId'] = $key;
                $param['sortOrder'] = $order++;
                $param['modifiedBy'] = $this->_auth->getIdentity()->crm_user_id;
                $param['modifiedDate'] = DATE_TIME_FORMAT;
                $this->getCmsTable()->changeOrderMenu($param);
            }
            $messages = array('status' => "success", 'message' => $cmsListPagesMessages['MENU_UPDATE_SUCCESS']);
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This action is used for sort dropdown list
     * @param $getSearchArray
     * @return $viewModel
     * @author Icreon Tech - AS
     */
    public function updateOrderAction() {
        $this->checkUserAuthentication();
        $this->getSettingTable();
        $msgArr = array_merge($this->_config['setting_messages']['config']['common'], $this->_config['setting_messages']['config']['get_dropdown'], $this->_config['setting_messages']['config']['pop_up']);
        $request = $this->getRequest();
        $response = $this->getResponse();
        $search = $request->getPost();
        $content = (\Zend\Json\Json::decode($search['data']));
        $param = array();
        $param['table'] = $search['table'];
        $param['field'] = $search['id'];
        $param['list'] = '';
        foreach ($content as $key => $val) {
            $param['list'] = $param['list'] . $val->ID . ",";
        }
        $param['list'] = trim($param['list'], ",");
        $this->getSettingTable()->updateSortOrder($param);
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $response->setContent(\Zend\Json\Json::encode(array('status' => 'success')));
        return $response;
    }

    public function crmEmailTemplateInfoAction() {
        $this->checkUserAuthentication();
        $this->getSettingTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->layout('crm');
        $viewModel = new ViewModel();

        $params = $this->params()->fromRoute();
        $translator = array_merge($this->_config['setting_messages']['config']['common'], $this->_config['setting_messages']['config']['searchEmailTemplate']);
        $email_id = $this->decrypt($params['id']);
        $mode = (isset($params['mode']) && $params['mode'] != '') ? $this->decrypt($params['mode']) : 'view';
        $searchParam['email_id'] = $email_id;
        $searchParam['startIndex'] = '';
        $searchParam['recordLimit'] = '';
        $searchParam['sortField'] = '';
        $searchParam['sortOrder'] = '';
        $searchResult = $this->getSettingTable()->searchEmailTemplate($searchParam);

        $viewModel->setVariables(array(
            'jsLangTranslate' => $translator,
            'email_id' => $this->encrypt($email_id),
            'searchResult' => $searchResult,
            'mode' => $mode
        ));
        return $viewModel;
    }

}