<?php

/**
 * This controller is used in setting modules
 * @package    Setting
 * @author     Icreon Tech - NS
 */

namespace Setting\Controller;

use Base\Controller\BaseController;
use Base\Model\UploadHandler;
use Zend\View\Model\ViewModel;
use Setting\Form\SearchCrmProgramForm;
use Setting\Form\AddProgramForm;
use Zend\Http\Request;
use Zend\Authentication\AuthenticationService;
use Setting\Model\Program;

/**
 * This class is used in Setting modules
 * @package    Program
 * @author     Icreon Tech - NS
 */
class ProgramController extends BaseController {

    protected $_ProgramTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_flashMessage = null;

    function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        parent::__construct();
        $auth = new AuthenticationService();
        $this->auth = $auth;
    }

    /**
     * This function is used to get table and config file object
     * @return     array
     * @author Icreon Tech - NS
     */
    public function getProgramTable() {
        if (!$this->_ProgramTable) {
            $sm = $this->getServiceLocator();
            $this->_ProgramTable = $sm->get('Setting\Model\ProgramTable');
            $this->_translator = $sm->get('translator');
            $this->_config = $sm->get('Config');
            $this->_adapter = $sm->get('dbAdapter');
        }
        return $this->_ProgramTable;
    }
    
        /**
        * This function is used to get Program Popup List
        * @return     array
        * @author Icreon Tech - NS
        */
      public function getProgramPopupListAction() {
        $this->checkUserAuthentication();
        $this->getProgramTable();
       // $this->layout('popup');
        $translator = $this->_config['setting_messages']['config']['program'];
        $searchCrmProgramForm = new SearchCrmProgramForm();
        
        $viewModel = new ViewModel();
       $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'searchCrmProgramForm' => $searchCrmProgramForm,
            'jsLangTranslate' =>  $translator
          ));
        return $viewModel;
    }
    
    /**
    * This function is used to get Program Popup Data
    * @return     array
    * @author Icreon Tech - NS
    */
    public function getProgramPopupDataAction() {
            $this->getProgramTable();
            $request = $this->getRequest();
            $params = $this->params()->fromRoute();
            $response = $this->getResponse();
            $messages = array();
            $searchParam = array();
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit;
            $searchParam['is_status'] = 1;
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'program.program_id') ? 'program.program_id' : $searchParam['sort_field'];
            
            $programArray = $this->getProgramTable()->getProgram($searchParam);
            
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();

            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $jsonResult['page'] = $request->getPost('page');
            $jsonResult['records'] = $countResult[0]->RecordCount;
            $jsonResult['page'] = $request->getPost('page');
            $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
            if (!empty($programArray)) {
                foreach ($programArray as $val) {
                    $arrCell['id'] = $val['program_id'];
                    $program_name = '<a class="txt-decoration-underline"href="javascript:void(0);" onclick="selectProgram(\'' . $val['program'] . '\',\'' . $val['program_id'] . '\');"><div class="tooltip">Select<span></span></div>' . $val['program'] . '</a>';
 
                    $is_available_for_donation = ($val['is_available_for_donation'] == 0) ? 'No' : 'Yes';
                    $arrCell['cell'] = array($program_name, $val['program_code'], $is_available_for_donation);
                    $subArrCell[] = $arrCell;
                    $jsonResult['rows'] = $subArrCell;
                }
            }
            $response->setContent(\Zend\Json\Json::encode($jsonResult));
            return $response;
    }
    
    
    
    /**
    * This function is used to get Program List
    * @return     array
    * @author Icreon Tech - NS
    */
    public function getProgramListAction() {
        $this->checkUserAuthentication();
        $this->getProgramTable();
        $this->layout('crm');
        $translator = $this->_config['setting_messages']['config']['program'];
        $searchCrmProgramForm = new SearchCrmProgramForm();
        
            $messages = array();
            if ($this->_flashMessage->hasMessages()) {
                 $messages = $this->_flashMessage->getMessages();
            }

        $viewModel = new ViewModel();

        $viewModel->setVariables(array(
            'searchCrmProgramForm' => $searchCrmProgramForm,
            'jsLangTranslate' =>  $translator,
            'messages' => $messages
          ));
        return $viewModel;
    }
    
     /**
    * This function is used to get Program Data
    * @return     array
    * @author Icreon Tech - NS
    */
    public function getProgramDataAction() {
            $this->getProgramTable();
            $request = $this->getRequest();
            $params = $this->params()->fromRoute();
            $response = $this->getResponse();
            $messages = array();
            $searchParam = array();
            parse_str($request->getPost('searchString'), $searchParam);
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit;
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'program.program_id') ? 'program.program_id' : $searchParam['sort_field'];
            

            $programArray = $this->getProgramTable()->getProgram($searchParam);
            
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();

            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $jsonResult['page'] = $request->getPost('page');
            $jsonResult['records'] = $countResult[0]->RecordCount;
            $jsonResult['page'] = $request->getPost('page');
            $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
            if (!empty($programArray)) {
                foreach ($programArray as $val) {
                    $arrCell['id'] = $val['program_id'];

                     if ($val['program_code'] != 0001) {
                        
                      
                        $delete = '<a class="delete-icon delete_campaign" href="#delete_campaign" onclick="deleteProgram(\'' . $this->encrypt($val['program_id']) . '\');">Delete</a>';
                     } else {
                       $view = $edit = $delete = '';
                     }
                       $edit = '<a class="edit-icon" href="/get-program-detail/'.$this->encrypt($val['program_id']).'/'.$this->encrypt('edit').'" >Edit</a>';
                     $view = '<a class="view-icon" href="/get-program-detail/'.$this->encrypt($val['program_id']).'/'.$this->encrypt('view').'" >View</a>'; 
                    $action = $view . " " . $edit . " " . $delete;
                    $is_available_for_donation = ($val['is_available_for_donation'] == 0) ? 'No' : 'Yes';
					if($val['is_status'] == 1 )
						$status = 'Active';
					elseif($val['is_status'] == 2 )
						$status = 'Archive';
					elseif($val['is_status'] == 0 )
						$status = 'Inactive';
                    $arrCell['cell'] = array($val['program'], $val['program_code'], $is_available_for_donation,$status,$action);
                    $subArrCell[] = $arrCell;
                    $jsonResult['rows'] = $subArrCell;
                }
            }
            $response->setContent(\Zend\Json\Json::encode($jsonResult));
            return $response;
    }
    
      /**
    * This function is used to create Crm Program
    * @return     array
    * @author Icreon Tech - NS
    */
    public function createCrmProgramAction() {
        $this->checkUserAuthentication();
        $this->getProgramTable();
        $this->layout('crm');
        $translator = $this->_config['setting_messages']['config']['program'];

        $addProgramForm = new AddProgramForm();
        $viewModel = new ViewModel();

        $viewModel->setVariables(array(
            'addProgramForm' => $addProgramForm,
            'jsLangTranslate' =>  $translator
          ));
        return $viewModel;
    }
    
    
     /**
    * This function is used to add Program Process
    * @return     array
    * @author Icreon Tech - NS
    */
    public function addProgramProcessAction() {
    
        $this->getProgramTable();
        $translator = $this->_config['setting_messages']['config']['program'];        
        $response = $this->getResponse();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $viewModel = new ViewModel();
        $program = $request->getPost();
        $Obj = new Program($this->_adapter);
        $param = $Obj->exchangeArray($program);
        
        $param['name'] = $program['name'];
        $param['program_code'] = $program['program_code'];
        $param['is_available_for_donation'] = isset($program['is_available_for_donation'])?$program['is_available_for_donation']:"0";
        $param['gl_code'] = $program['gl_code'];
        $param['major_gl_code'] = $program['major_gl_code'];
        $param['department'] = $program['department'];
        $param['designation'] = $program['designation'];
        $param['allocation'] = $program['allocation'];
        $param['is_restriction'] = isset($program['is_restriction'])?$program['is_restriction']:"0";
        $param['description'] = $program['description'];
        $param['status'] = $program['status'];
   
        
        if (isset($program['programId']) && $program['programId'] == '') {
            $searchResult = $this->getProgramTable()->addProgramDetails($param);
            $lastId = $searchResult[0]['LAST_INSERT_ID'];
            
            /** insert into the change log */
            $changeLogArray = array();
            $changeLogArray['table_name'] = 'tbl_mst_program_change_logs';
            $changeLogArray['activity'] = '1'; /** 1 == for insert */
            $changeLogArray['id'] = $lastId;
            $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
            $changeLogArray['added_date'] = DATE_TIME_FORMAT;
            $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
            /** end insert into the change log */

            if ($lastId != False) {
                $this->flashMessenger()->addMessage($translator['PROGRAM_MSG_SAVED']);
                $messages = array('status' => "success", 'message' => 'Program Added Successfully.', 'program_id' => $lastId, 'program' => $program['name'], 'associate' => 1);
            }
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        } else {
            $param['program_id'] = $program['programId']; 
            $programArray = $this->getProgramTable()->updateProgram($param);
            if ($programArray[0]['program_id'] != '') {  
                
                /** insert into the change log */
                $changeLogArray = array();
                $changeLogArray['table_name'] = 'tbl_mst_program_change_logs';
                $changeLogArray['activity'] = '2'; /** 2 == for update */
                $changeLogArray['id'] = $programArray[0]['program_id'];
                $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                /** end insert into the change log */

                $this->flashMessenger()->addMessage($translator['PROGRAM_MSG_MODIFIED']);
                $messages = array('status' => "success", "message" => "Program Updated Successfully.", 'program_id' => $programArray[0]['program_id'], 'program' => $program['name'], 'associate' => 0);
            }
            else {   
                $messages = array('status' => "failure");
             }
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }
    

    /**
    * This function is used to edit Program Details
    * @return     array
    * @author Icreon Tech - NS
    */
    public function getProgramDetailsAction() {
            $this->checkUserAuthentication();
            $this->getProgramTable();
            $request = $this->getRequest();
            $response = $this->getResponse();
            $this->layout('crm');
            $viewModel = new ViewModel();
            
            $params = $this->params()->fromRoute();
            $translator = $this->_config['setting_messages']['config']['program'];
            $programId = $this->decrypt($params['program_id']);  
            $mode = (isset($params['mode']) && $params['mode'] != '') ? $this->decrypt($params['mode']) : 'view';
            $programArray = $this->getProgramTable()->getProgram(array('program_id' => $programId));
            
            $viewModel->setVariables(array(
                'jsLangTranslate' => $translator,
                'programId' => $this->encrypt($programId),
                'programArray' => $programArray,
                'mode' => $mode,
                'module_name' => $this->encrypt('program')
            ));
            return $viewModel;
    }   
    
    /**
    * This function is used to edit Program Details
    * @return     array
    * @author Icreon Tech - NS
    */
    public function editProgramDetailsAction() {
        $this->checkUserAuthentication();
        $this->getProgramTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->layout('crm');
        $viewModel = new ViewModel();
        $addProgramForm = new AddProgramForm();
        
        $params = $this->params()->fromRoute();
 
        $translator = $this->_config['setting_messages']['config']['program'];
        $programId = $this->decrypt($params['program_id']);
        $programArray = $this->getProgramTable()->getProgram(array('program_id' => $programId));
 
        $addProgramForm->get("save")->setAttribute('value', 'update');
         
        if(isset($programArray[0]) and is_array($programArray[0])) {
            if(isset($programArray[0]['program_id']) and trim($programArray[0]['program_id']) != "") { $addProgramForm->get("programId")->setAttribute('value', $programArray[0]['program_id']); }
            if(isset($programArray[0]['program']) and trim($programArray[0]['program']) != "") { $addProgramForm->get("name")->setAttribute('value', $programArray[0]['program']); }
            if(isset($programArray[0]['program_code']) and trim($programArray[0]['program_code']) != "") { $addProgramForm->get("program_code_hidden")->setAttribute('value', $programArray[0]['program_code']); }
            if(isset($programArray[0]['program_code']) and trim($programArray[0]['program_code']) != "") { $addProgramForm->get("program_code")->setAttribute('value', $programArray[0]['program_code']); $addProgramForm->get("program_code")->setAttribute('readonly', 'readonly;');  }
            if(isset($programArray[0]['is_available_for_donation']) and trim($programArray[0]['is_available_for_donation']) != "") { $addProgramForm->get("is_available_for_donation")->setAttribute('value', $programArray[0]['is_available_for_donation']); }
            if(isset($programArray[0]['program_description']) and trim($programArray[0]['program_description']) != "") { $addProgramForm->get("description")->setAttribute('value', $programArray[0]['program_description']); }
            if(isset($programArray[0]['gl_code']) and trim($programArray[0]['gl_code']) != "") { $addProgramForm->get("gl_code")->setAttribute('value', $programArray[0]['gl_code']); }
            if(isset($programArray[0]['major_gl_code']) and trim($programArray[0]['major_gl_code']) != "") { $addProgramForm->get("major_gl_code")->setAttribute('value', $programArray[0]['major_gl_code']); }        
            if(isset($programArray[0]['department']) and trim($programArray[0]['department']) != "") { $addProgramForm->get("department")->setAttribute('value', $programArray[0]['department']);   }
            if(isset($programArray[0]['designation']) and trim($programArray[0]['designation']) != "") { $addProgramForm->get("designation")->setAttribute('value', $programArray[0]['designation']);   }
            if(isset($programArray[0]['allocation']) and trim($programArray[0]['allocation']) != "") { $addProgramForm->get("allocation")->setAttribute('value', $programArray[0]['allocation']);   }
            if(isset($programArray[0]['is_restriction']) and trim($programArray[0]['is_restriction']) != "") { $addProgramForm->get("is_restriction")->setAttribute('value', $programArray[0]['is_restriction']);  }
            if(isset($programArray[0]['is_status']) and trim($programArray[0]['is_status']) != "") { $addProgramForm->get("status")->setAttribute('value', $programArray[0]['is_status']); }

            $programArrayV = $programArray[0];
        }
        else {
            $programArrayV = array();
        }
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'addProgramForm' => $addProgramForm,
            'jsLangTranslate' =>  $translator,
            'programArray' => $programArrayV
          ));
        return $viewModel;
    }
    
     /**
    * This function is used to view Program
    * @return     array
    * @author Icreon Tech - NS
    */
    public function viewProgramAction() {
        $this->checkUserAuthentication();
        $this->getProgramTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->layout('crm');
        $viewModel = new ViewModel();
        $addProgramForm = new AddProgramForm();
        
        $params = $this->params()->fromRoute();
 
        $translator = $this->_config['setting_messages']['config']['program'];
        $programId = $this->decrypt($params['program_id']);
        $programArrayV = $this->getProgramTable()->getProgram(array('program_id' => $programId));
 
 
        if(isset($programArrayV[0]) and is_array($programArrayV[0])) {
       
            $programArray = $programArrayV[0];
        }
        else {
            $programArray = array();
        }
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'addProgramForm' => $addProgramForm,
            'jsLangTranslate' =>  $translator,
            'programArray' => $programArray
          ));
        return $viewModel;
    }
    
    
    /**
    * This function is used to delete Program
    * @return     array
    * @author Icreon Tech - NS
    */
    public function deleteProgramAction() {
        $this->getProgramTable();
        $request = $this->getRequest();
        $translator = $this->_config['setting_messages']['config']['program'];
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam['programId'] = $this->decrypt($request->getPost('programId'));
            
            /** insert into the change log */
            $changeLogArray = array();
            $changeLogArray['table_name'] = 'tbl_mst_program_change_logs';
            $changeLogArray['activity'] = '3'; /** 3 == for delete */
            $changeLogArray['id'] = $dataParam['programId'];
            $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
            $changeLogArray['added_date'] = DATE_TIME_FORMAT;
            $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
            /** end insert into the change log */

            $this->getProgramTable()->deleteProgram($dataParam);
            $this->flashMessenger()->addMessage($translator['PROGRAM_MSG_DELETED']);
            $messages = array('status' => "success");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }
    
    
    /**
    * This function is used to check Program Code
    * @return     array
    * @author Icreon Tech - NS
    */
    public function checkProgramCodeAction() {
        $this->getProgramTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
                 
        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $data = $request->getPost()->toArray();
            $data['programId'] = ($data['programId'] != '') ? $this->decrypt($data['programId']) : '';
            if ($data['programCode'] != '') {
                $checkCode = $this->getProgramTable()->chekProgramCodeExists($data);
                if ($checkCode[0]['program_code_exists'] == '1') {
                    $messages = array('status' => "exists");
                } elseif ($checkCode[0]['program_code_exists'] == '0') {
                    $messages = array('status' => "notexists");
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
            else {
                $response->setContent(\Zend\Json\Json::encode(array('status' => "notexists")));
                return $response;
            }
        }
        else {
                $response->setContent(\Zend\Json\Json::encode(array('status' => "notexists")));
                return $response;
        }
        exit();
    }
 
    
        /**
     * This Action is used to get programs by autosuggest
     * @return Array of search result in json format
     * @param $getSearchArray
     * @author Icreon Tech-NS
     */
    public function getProgramAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getProgramTable();
        if ($request->isPost()) {
            $getSearchArray['program'] = $request->getPost('program');
			$getSearchArray['is_status'] = 1;
            $programArray = $this->getProgramTable()->getProgram($getSearchArray);
            $program_list = array();
            if (!empty($programArray)) {
                $i = 0;
                foreach ($programArray as $key => $val) {
                    $program_list[$val['program_id']] = $val['program'];
                    $program_list[$i] = array();
                    $program_list[$i]['program_id'] = $val['program_id'];
                    $program_list[$i]['program'] = $val['program'];
                    $i++;
                }
            }
            return $response->setContent(\Zend\Json\Json::encode($program_list));
        }
    }
    
}