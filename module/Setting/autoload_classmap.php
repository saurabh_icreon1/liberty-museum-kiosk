<?php

/**
 * This is used for setting autoload class maping
 * @package    Setting
 * @author     Icreon Tech - AS
 */
return array(
    'Setting\Module' => __DIR__ . '/Module.php',
    'Setting\Controller\SettingController' => __DIR__ . '/src/Setting/Controller/SettingController.php',
    'Setting\Model\Setting' => __DIR__ . '/src/Setting/Model/Setting.php',
    'Setting\Model\SettingTable' => __DIR__ . '/src/Setting/Model/SettingTable.php',
    'Setting\Model\AuthStorage' => __DIR__ . '/src/Setting/Model/AuthStorage.php',
);
?>