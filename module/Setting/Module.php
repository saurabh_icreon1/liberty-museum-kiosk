<?php
/**
 * This is used for setting module in CRM.
 * @package    Setting
 * @author     Icreon Tech - AS
 */
namespace Setting;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\I18n\Translator\Translator;
use Zend\Validator\AbstractValidator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Setting\Model\Setting;
use Setting\Model\SettingTable;
use Setting\Model\Program;
use Setting\Model\ProgramTable;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;

/**
 * This class is used for setting module in CRM.
 * @author     Icreon Tech - AS
 */
class Module implements AutoloaderProviderInterface, ConfigProviderInterface, ViewHelperProviderInterface {

    public function onBootstrap(MvcEvent $e) {
        $translator = $e->getApplication()->getServiceManager()->get('translator');
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $translator->addTranslationFile(
                'phpArray', './module/Setting/languages/en/language.php', 'default', 'en_US'
        );
        //AbstractValidator::setDefaultTranslator($translator);
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Get Autoloader Configuration
     * @return array
     */
    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    /**
     * Get Service Configuration
     *
     * @return array
     */
    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Setting\Model\SettingTable' => function($sm) {
                    $tableGateway = $sm->get('SettingTableGateway');
                    $table = new SettingTable($tableGateway);
                    return $table;
                },
                'SettingTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Setting($dbAdapter));
                    return new TableGateway('tbl_cam_campaigns', $dbAdapter, null, $resultSetPrototype);
                },
                'Setting\Model\ProgramTable' => function($sm) {
                    $tableGateway = $sm->get('ProgramTableGateway');
                    $table = new ProgramTable($tableGateway);
                    return $table;
                },
                'ProgramTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Program($dbAdapter));
                    return new TableGateway('tbl_mst_programs', $dbAdapter, null, $resultSetPrototype);
                },
                'dbAdapter' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    return $dbAdapter;
                },
            ),
        );
    }

    /**
     * Get View Helper Configuration
     *
     * @return array
     */
    public function getViewHelperConfig() {
        return array(
            'factories' => array(
            // the array key here is the name you will call the view helper by in your view scripts
//                'absoluteUrl' => function($sm) {
//                    $locator = $sm->getServiceLocator(); // $sm is the view helper manager, so we need to fetch the main service manager
//                    return new AbsoluteUrl($locator->get('Request'));
//                },
            ),
        );
    }

    /**
     * Get Controller Configuration
     *
     * @return array
     */
    public function getControllerConfig() {
        return array();
    }

    public function loadConfiguration(MvcEvent $e) {
        $application = $e->getApplication();
        $serviceManager = $application->getServiceManager();
        $sharedManager = $application->getEventManager()->getSharedManager();
        $router = $serviceManager->get('router');
        $request = $serviceManager->get('request');

//        $matchedRoute = $router->match($request);
//        if (null !== $matchedRoute) {
//            $sharedManager->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function($e) use ($serviceManager) {
//                        $serviceManager->get('ControllerPluginManager')->get('AclPlugin')
//                                ->doAuthorization($e);
//                    }, 2
//            );
//        }
    }

    public function loadCommonViewVars(MvcEvent $e) {
        $e->getViewModel()->setVariables(array(
            'auth' => $e->getApplication()->getServiceManager()->get('AuthService')
        ));
    }

}