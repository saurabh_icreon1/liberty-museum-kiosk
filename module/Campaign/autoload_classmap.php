<?php 
return array(
    'Campaign\Module'                       => __DIR__ . '/Module.php',
    'Campaign\Controller\CampaignController'   => __DIR__ . '/src/Campaign/Controller/CampaignController.php',
    'Campaign\Model\Campaign'            => __DIR__ . '/src/Campaign/Model/Campaign.php',
    'Campaign\Model\CampaignTable'                   => __DIR__ . '/src/Campaign/Model/CampaignTable.php',
    'Campaign\Model\AuthStorage'                   => __DIR__ . '/src/Campaign/Model/AuthStorage.php',
);
?>