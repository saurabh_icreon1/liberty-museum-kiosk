<?php

/**
 * This page is used for campaign module configuration details.
 * @package    Campaign
 * @author     Icreon Tech - AS
 */
return array(
    'controllers' => array(
        'invokables' => array(
            'Campaign\Controller\Campaign' => 'Campaign\Controller\CampaignController'
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'addCampaign' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-campaign',
                    'defaults' => array(
                        'controller' => 'Campaign\Controller\Campaign',
                        'action' => 'addCampaign',
                    ),
                ),
            ),
            'viewCampaign' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/campaign[/:id][/:mode]',
                    'defaults' => array(
                        'controller' => 'Campaign\Controller\Campaign',
                        'action' => 'viewCampaign',
                    ),
                ),
            ),
            'addCampaignUsers' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-campaign-user',
                    'defaults' => array(
                        'controller' => 'Campaign\Controller\Campaign',
                        'action' => 'addCampaignUsers',
                    ),
                ),
            ),
            'updateUserBadRecipient' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-update-bad-recipient',
                    'defaults' => array(
                        'controller' => 'Campaign\Controller\Campaign',
                        'action' => 'updateUserBadRecipient',
                    ),
                ),
            ),
            'importBadRecipient' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/import-bad-recipient',
                    'defaults' => array(
                        'controller' => 'Campaign\Controller\Campaign',
                        'action' => 'importBadRecipient',
                    ),
                ),
            ),
            'getUserBadRecipient' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-bad-recipient[/:id]',
                    'defaults' => array(
                        'controller' => 'Campaign\Controller\Campaign',
                        'action' => 'getUserBadRecipient',
                    ),
                ),
            ),
            'listBadRecepient' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/list-bad-recepient[/:id]',
                    'defaults' => array(
                        'controller' => 'Campaign\Controller\Campaign',
                        'action' => 'listBadRecepient',
                    ),
                ),
            ),
            'showChannelsCodes' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/show-channels-codes[/:id]',
                    'defaults' => array(
                        'controller' => 'Campaign\Controller\Campaign',
                        'action' => 'showChannelsCodes',
                    ),
                ),
            ),
            'channelCampaign' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/channel-campaign',
                    'defaults' => array(
                        'controller' => 'Campaign\Controller\Campaign',
                        'action' => 'channelCampaign',
                    ),
                ),
            ),
            'searchCampaigns' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/campaigns',
                    'defaults' => array(
                        'controller' => 'Campaign\Controller\Campaign',
                        'action' => 'getCampaigns',
                    ),
                ),
            ),
            'saveCampaignSearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/save-campaign-search',
                    'defaults' => array(
                        'controller' => 'Campaign\Controller\Campaign',
                        'action' => 'saveCampaignSearch',
                    ),
                ),
            ),
            'getSavedCampaignSearchSelect' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-saved-campaign-search-select',
                    'defaults' => array(
                        'controller' => 'Campaign\Controller\Campaign',
                        'action' => 'getSavedCampaignSearchSelect',
                    ),
                ),
            ),
            'getCrmSearchCampaignSavedParam' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-search-campaign-saved-select',
                    'defaults' => array(
                        'controller' => 'Campaign\Controller\Campaign',
                        'action' => 'getCrmSearchCampaignSavedParam',
                    ),
                ),
            ),
            'deleteCrmSearchCampaign' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-search-campaign',
                    'defaults' => array(
                        'controller' => 'Campaign\Controller\Campaign',
                        'action' => 'deleteCrmSearchCampaign',
                    ),
                ),
            ),
            'deleteCampaign' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-campaign',
                    'defaults' => array(
                        'controller' => 'Campaign\Controller\Campaign',
                        'action' => 'deleteCampaign',
                    ),
                ),
            ),
            'getSearchCampaign' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-search-campaign',
                    'defaults' => array(
                        'controller' => 'Campaign\Controller\Campaign',
                        'action' => 'getSearchCampaign',
                    ),
                ),
            ),
            'getUserCampaigns' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-campaigns[/:id]',
                    'defaults' => array(
                        'controller' => 'Campaign\Controller\Campaign',
                        'action' => 'get-user-campaigns',
                    ),
                ),
            ),
            'getcampaignCode' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-campaign-code',
                    'defaults' => array(
                        'controller' => 'Campaign\Controller\Campaign',
                        'action' => 'get-campaign-code',
                    ),
                ),
            ),
           
            'usersearchcampaigns' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-search-campaigns',
                    'defaults' => array(
                        'controller' => 'Campaign\Controller\Campaign',
                        'action' => 'user-search-campaigns',
                    ),
                ),
            ),
            'userUpdateCampaign' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-update-campaign[/][:campaign_id][/:user_id]',
                    'defaults' => array(
                        'controller' => 'Campaign\Controller\Campaign',
                        'action' => 'user-update-campaign',
                    ),
                ),
            ),
            'viewCampaignDetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-campaign-details[/][/:id]',
                    'defaults' => array(
                        'controller' => 'Campaign\Controller\Campaign',
                        'action' => 'viewCampaignDetail',
                    ),
                ),
            ),
            'editCampaignDetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-campaign-detail[/][/:id]',
                    'defaults' => array(
                        'controller' => 'Campaign\Controller\Campaign',
                        'action' => 'editCampaignDetail',
                    ),
                ),
            ),
            'uploadCampaignThumbnail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-campaign-thumbnail',
                    'defaults' => array(
                        'controller' => 'Campaign\Controller\Campaign',
                        'action' => 'uploadCampaignThumbnail',
                    ),
                ),
            ),

            'campaigntouchpoints' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/campaign-touch-points[/:campaign_id]',
                    'defaults' => array(
                        'controller' => 'Campaign\Controller\Campaign',
                        'action' => 'getCampaignTouchPoints',
                    ),
                ),
            ),
            'uploadcampaigntouchpoints' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-campaign-touch-points[/:touch_id][/:campaign_id]',
                    'defaults' => array(
                        'controller' => 'Campaign\Controller\Campaign',
                        'action' => 'uploadCampaignTouchPoints',
                    ),
                ),
            ),
            'editcampaigntouchpoints' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-campaign-touch-points',
                    'defaults' => array(
                        'controller' => 'Campaign\Controller\Campaign',
                        'action' => 'editCampaignTouchPoints',
                    ),
                ),
            ),
            'addtouchpoint' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-touch-point[/:campaign_id]',
                    'defaults' => array(
                        'controller' => 'Campaign\Controller\Campaign',
                        'action' => 'addTouchPoint',
                    ),
                ),
            ),
            'exportcampaigncontact' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/export-campaign-contact[/:touch_id][/:campaign_id][/:touch_date]',
                    'defaults' => array(
                        'controller' => 'Campaign\Controller\Campaign',
                        'action' => 'exportCampaignContact',
                    ),
                ),
            ),
        )
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'navigation' => array(
        'default' => array(
            array(
                'label' => 'Campaigns',
                'route' => 'searchCampaigns',
                'pages' => array(
                    array(
                        'label' => 'Search',
                        'route' => 'searchCampaigns',
                        'pages' => array(
                            array(
                                'label' => 'Touch Points',
                                'route' => 'campaigntouchpoints',
                                'action' => 'getCampaignTouchPoints',
                            ),
                            array(
                                'label' => 'View',
                                'route' => 'viewCampaignDetail',
                                'action' => 'viewCampaignDetail',
                            ),
                            array(
                                'label' => 'Edit',
                                'route' => 'editCampaignDetail',
                                'action' => 'editCampaignDetail',
                            ),
                            array(
                                'label' => 'Bad Recepients',
                                'route' => 'getUserBadRecipient',
                                'action' => 'getUserBadRecipient'
                            ),
                            array(
                                'label' => 'Change Log',
                                'route' => 'campaignchangelog',
                                'action' => 'change-log'
                            )
                        ),
                    ),
                    array(
                        'label' => 'Create',
                        'route' => 'addCampaign',
                        'pages' => array(),
                    ),
                    array(
                        'label' => 'Import',
                        'route' => 'updateUserBadRecipient',
                        'pages' => array(),
                    )
                ),
            )
        )
    ),
    'campaign_messages' => array(
        'config' => include __DIR__ . '/../languages/en/language.php',
        'navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory'
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'campaign' => __DIR__ . '/../view'
        ),
        'template_map' => array(
            'layout/custom' => __DIR__ . '/../../module/Common/layout/crm.phtml',
            'error/404' => __DIR__ . '/../../module/Common/error/404.phtml',
            'error/index' => __DIR__ . '/../../module/Common/error/index.phtml',
        ),
    ),
    'global_config' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
);
