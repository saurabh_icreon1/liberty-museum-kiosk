<?php

/**
 * This is used for campaign module validation.
 * @package    Campaign
 * @author     Icreon Tech - AS
 */

namespace Campaign\Model;

use Campaign\Module;
// Add these import statements
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\Adapter\Adapter;
use Base\Model\BaseModel;

/**
 * This class is used for campaign module validation.
 * @package    Campaign
 * @author     Icreon Tech - AS
 */
class Campaign extends BaseModel {

    protected $inputFilter;
    protected $adapter;
    public $campaign_id;
    public $campaign_title;
    public $campaign_type;
    public $campaign_description;
    public $program;
    public $program_id;
    public $thumbnail;
    public $groups;
    public $start_date;
    public $end_date;
    public $campaign_status;
    public $campaign_goal;
    public $revenue_goal;
    public $coupon_code;
    public $gl_code;
    public $designation;
    public $departmrnt;
    public $channel;
    public $channel_cost;
    public $is_active;
    public $restricted;
    public $user_id;
    public $channel_id;

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    /**
     * This method is used to convert form post array of create Campaign to Object of class type Campaign
     * @param $data
     * @return Void
     * @author Icreon Tech - AS
     */
    public function exchangeArray($data) {

        $this->title = (isset($data['title']) && !empty($data['title'])) ? $data['title'] : '';
        $this->campaign_type = (isset($data['campaign_type']) && !empty($data['campaign_type'])) ? $data['campaign_type'] : '';
        $this->description = (isset($data['description']) && !empty($data['description'])) ? $data['description'] : '';
        $this->program = (isset($data['program']) && !empty($data['program'])) ? $data['program'] : '';
        $this->program_id = (isset($data['program_id']) && !empty($data['program_id'])) ? $data['program_id'] : '';
        $this->thumbnail = (isset($data['thumbnail']) && !empty($data['thumbnail'])) ? $data['thumbnail'] : '';
        $this->groups = (isset($data['groups']) && !empty($data['groups'])) ? $data['groups'] : '';
        $this->start_date = (isset($data['start_date']) && !empty($data['start_date'])) ? $data['start_date'] : '';
        $this->start_time = (isset($data['start_time']) && !empty($data['start_time'])) ? $data['start_time'] : '';
        $this->end_date = (isset($data['end_date']) && !empty($data['end_date'])) ? $data['end_date'] : '';
        $this->end_time = (isset($data['end_time']) && !empty($data['end_time'])) ? $data['end_time'] : '';
        $this->campaign_status = (isset($data['campaign_status']) && !empty($data['campaign_status'])) ? $data['campaign_status'] : '';
        $this->campaign_goal = (isset($data['campaign_goal']) && !empty($data['campaign_goal'])) ? $data['campaign_goal'] : '';
        $this->revenue_goal = (isset($data['revenue_goal']) && !empty($data['revenue_goal'])) ? $data['revenue_goal'] : '';
        $this->coupon_code = (isset($data['coupon_code']) && !empty($data['coupon_code'])) ? $data['coupon_code'] : '';
        $this->gl_code = (isset($data['gl_code']) && !empty($data['gl_code'])) ? $data['gl_code'] : '';
        $this->designation = (isset($data['designation']) && !empty($data['designation'])) ? $data['designation'] : '';
        $this->department = (isset($data['department']) && !empty($data['department'])) ? $data['department'] : '';
        $this->restricted = (isset($data['restricted']) && !empty($data['restricted'])) ? $data['restricted'] : '';
        $this->channel = (isset($data['channel[]']) && !empty($data['channel[]'])) ? $data['channel[]'] : '';
        $this->channel_cost = (isset($data['channel_cost[]']) && !empty($data['channel_cost[]'])) ? $data['channel_cost[]'] : '';
        $this->is_active = (isset($data['is_active']) && !empty($data['is_active'])) ? $data['is_active'] : '';
    }

    /**
     * This method is used to add filtering and validation to the form elements of create Campaign
     * @param code
     * @return Array
     * @author Icreon Tech - AS
     */
    public function getInputFilterCreateCampaign() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                        'name' => 'title',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'TITLE_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'description',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'DESCRIPTION_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'coupon_code',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'program',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'PROGRAM_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'program_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'PROGRAM_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'thumbnail',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'start_date',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'START_DATE',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'start_time',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'START_TIME',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'end_date',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'END_DATE',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'end_time',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'END_TIME',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'campaign_goal',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'revenue_goal',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'REVENUE_GOAL',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'gl_code',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'designation',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'department',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'campaign_type',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'CAMPAIGN_TYPE_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'group_from',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'group_to',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'groups',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'GROUPS_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'campaign_status',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'CAMPAIGN_STATUS',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_active',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'restricted',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'channel[]',
                        'required' => false
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'channel_cost[]',
                        'required' => false
                    )));



            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    /**
     * Function used to check variable for user update campaign form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function exchangeArrayUserUpdateCampaign($data) {
        $this->channel_id = (isset($data['channel_id'])) ? $data['channel_id'] : null;
        $this->user_id = (isset($data['user_id'])) ? $data['user_id'] : null;
        $this->campaign_id = (isset($data['campaign_id'])) ? $data['campaign_id'] : null;
    }

    /**
     * Function used to check validation for user update campaign form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function getInputFilterUserUpdateCampaign() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'channel_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'CHANNEL_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'user_id',
                        'required' => true,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'campaign_id',
                        'required' => true,
                    )));
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * Function used to pass for input filter
     * @author Icreon Tech - AS
     * @return array
     * @param Array
     */
    public function getInputFilterCrmSearch($filterData) {
        $inputFilterData = array();
        if (is_array($filterData)) {
            foreach ($filterData as $key => $val) {
                $inputFilterData[$key] = @trim(addslashes(strip_tags($val)));
            }
        } else {
            $inputFilterData = @trim(addslashes(strip_tags($filterData)));
        }
        return $this->inputFilterCrmSearch = $inputFilterData;
    }

}