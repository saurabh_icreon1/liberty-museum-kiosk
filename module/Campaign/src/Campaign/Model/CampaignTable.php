<?php

/**
 * This is used for Campaign module database related work.
 * @package    Campaign
 * @author     Icreon Tech - AS
 */

namespace Campaign\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This class is used for creating a table gateway for campaign
 * @package    Campaign
 * @author     Icreon Tech - AS
 */
class CampaignTable {

    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
    }

    /**
     * Function for to get campaign
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getCampaign($dataArr = null) {
        $campaign_code = isset($dataArr['campaign_code']) ? $dataArr['campaign_code'] : null;
        $campaignId = isset($dataArr['campaign_id']) ? $dataArr['campaign_id'] : null;
		$current_date_time = isset($dataArr['current_date_time']) ? $dataArr['current_date_time'] : null;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_com_getCampaignCode(?,?,?)');
        $stmt->getResource()->bindParam(1, $campaign_code);
        $stmt->getResource()->bindParam(2, $campaignId);
		$stmt->getResource()->bindParam(3, $current_date_time);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for fetching program list
     * @return $resultSet as array or false as boolean 
     * @param $dataArr as an array
     * @author Icreon Tech - AS
     */
    public function getProgram($dataArr = null) {
        $program = isset($dataArr['program']) ? $dataArr['program'] : '';
        $program_id = isset($dataArr['program_id']) ? $dataArr['program_id'] : '';
        $start_index = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $record_limit = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $sort_field = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $sort_order = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_com_getPrograms(?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $program_id);
        $stmt->getResource()->bindParam(2, $program);
        $stmt->getResource()->bindParam(3, $start_index);
        $stmt->getResource()->bindParam(4, $record_limit);
        $stmt->getResource()->bindParam(5, $sort_field);
        $stmt->getResource()->bindParam(6, $sort_order);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for adding a new campaign
     * @return $resultSet as array or false as boolean 
     * @param $params as an array
     * @author Icreon Tech - AS
     */
    public function addCampaign($params = array()) {

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cam_insertCampaign(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['title']);
            $stmt->getResource()->bindParam(2, $params['campaign_type']);
            $stmt->getResource()->bindParam(3, $params['description']);
            $stmt->getResource()->bindParam(4, $params['thumbnail']);
            $stmt->getResource()->bindParam(5, $params['program_id']);
            $stmt->getResource()->bindParam(6, $params['start_date']);
            $stmt->getResource()->bindParam(7, $params['end_date']);
            $stmt->getResource()->bindParam(8, $params['campaign_status']);
            $stmt->getResource()->bindParam(9, $params['campaign_goal']);
            $stmt->getResource()->bindParam(10, $params['revenue_goal']);
            $stmt->getResource()->bindParam(11, $params['revenue_received']);
//            $stmt->getResource()->bindParam(12, $params['coupon_code']);
//            $stmt->getResource()->bindParam(13, $params['gl_code']);
            $stmt->getResource()->bindParam(12, $params['department']);
            $stmt->getResource()->bindParam(13, $params['designation']);
            $stmt->getResource()->bindParam(14, $params['is_active']);
            $stmt->getResource()->bindParam(15, $params['restricted']);
            $stmt->getResource()->bindParam(16, $params['is_deleted']);
            $stmt->getResource()->bindParam(17, $params['added_date']);
            $stmt->getResource()->bindParam(18, $params['modified_date']);
            $stmt->getResource()->bindParam(19, $params['campaign_code']);
            $stmt->getResource()->bindParam(20, $params['addedBy']);
            $stmt->getResource()->bindParam(21, $params['modifiedBy']);
            $stmt->getResource()->bindParam(22, $params['is_global']);
			$stmt->getResource()->bindParam(23, $params['project_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if ($resultSet) {
                return $resultSet;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for updating a campaign
     * @return boolean value
     * @param $params as an array
     * @author Icreon Tech - AS
     */
    public function updateCampaign($params = array()) {

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cam_updateCampaign(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['title']);
            $stmt->getResource()->bindParam(2, $params['campaign_type']);
            $stmt->getResource()->bindParam(3, $params['description']);
            $stmt->getResource()->bindParam(4, $params['thumbnail']);
            $stmt->getResource()->bindParam(5, $params['program_id']);
            $stmt->getResource()->bindParam(6, $params['start_date']);
            $stmt->getResource()->bindParam(7, $params['end_date']);
            $stmt->getResource()->bindParam(8, $params['campaign_status']);
            $stmt->getResource()->bindParam(9, $params['campaign_goal']);
            $stmt->getResource()->bindParam(10, $params['revenue_goal']);
            $stmt->getResource()->bindParam(11, $params['is_active']);
            $stmt->getResource()->bindParam(12, $params['modified_date']);
            $stmt->getResource()->bindParam(13, $params['campaign_id']);
            $stmt->getResource()->bindParam(14, $params['modifiedBy']);
            $stmt->getResource()->bindParam(15, $params['is_global']);
			$stmt->getResource()->bindParam(16, $params['project_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            //$resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            //asd($resultSet);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for deleating a campaign
     * @return boolean value
     * @param $params as an array
     * @author Icreon Tech - AS
     */
    public function deleteCampaignDetails($params = array()) {
        try {
            if(!isset($params['channel_camp_id']) || empty($params['channel_camp_id'])){
                $params['channel_camp_id'] = 0;
            }
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cam_deleteCampaignDetails(?,?)');
            $stmt->getResource()->bindParam(1, $params['campaign_id']);
            $stmt->getResource()->bindParam(2, $params['channel_camp_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get campaign
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getUserCampaigns($dataArr = null) {
        $campaign_id = isset($dataArr['campaign_id']) ? $dataArr['campaign_id'] : null;
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        $channel_id = isset($dataArr['channel_id']) ? $dataArr['channel_id'] : null;

        $start_index = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $record_limit = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $sort_field = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $sort_order = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_cam_getUserCampaigns(?,?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $campaign_id);
        $stmt->getResource()->bindParam(2, $channel_id);
        $stmt->getResource()->bindParam(3, $user_id);
        $stmt->getResource()->bindParam(4, $start_index);
        $stmt->getResource()->bindParam(5, $record_limit);
        $stmt->getResource()->bindParam(6, $sort_field);
        $stmt->getResource()->bindParam(7, $sort_order);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to update user campaign
     * @author Icreon Tech - DG
     * @return bollean
     * @param Array
     */
    public function userUpdateCampaign($dataArr = null) {
        try {
            $campaign_id = isset($dataArr['campaign_id']) ? $dataArr['campaign_id'] : null;
            $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
            $channel_id = isset($dataArr['channel_id']) ? $dataArr['channel_id'] : null;

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cam_updateUserCampaignStatus(?,?,?)');
            $stmt->getResource()->bindParam(1, $campaign_id);
            $stmt->getResource()->bindParam(2, $channel_id);
            $stmt->getResource()->bindParam(3, $user_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for adding a campaign group(s)
     * @return boolean value
     * @param $params as an array
     * @author Icreon Tech - AS
     */
    public function addCampaignGroup($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cam_insertCamGroups(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['campaign_id']);
            $stmt->getResource()->bindParam(2, $params['group_id']);
            $stmt->getResource()->bindParam(3, $params['added_date']);
            $stmt->getResource()->bindParam(4, $params['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for adding a campaign channel(s)
     * @return boolean value
     * @param $params as an array
     * @author Icreon Tech - AS
     */
    public function addCampaignChannel($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cam_insertCamChannels(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['campaign_id']);
            $stmt->getResource()->bindParam(2, $params['channel_id']);
            $stmt->getResource()->bindParam(3, $params['channel_cost']);
            $stmt->getResource()->bindParam(4, $params['coupon_code']);
            // $stmt->getResource()->bindParam(5, $params['gl_code']);
            $stmt->getResource()->bindParam(5, $params['added_date']);
            $stmt->getResource()->bindParam(6, $params['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for adding a campaign user(s)
     * @return boolean value
     * @param $params as an array
     * @author Icreon Tech - AS
     */
    public function addCampaignUser($params = array()) {
        try {
            $response_channel_id = isset($params['response_channel_id'])? $params['response_channel_id']:'';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cam_insertCamUser(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['campaign_id']);
            $stmt->getResource()->bindParam(2, $params['user_id']);
            $stmt->getResource()->bindParam(3, $params['added_date']);
            $stmt->getResource()->bindParam(4, $params['group_type']);
            $stmt->getResource()->bindParam(5, $params['group_id']);
			$stmt->getResource()->bindParam(6, $response_channel_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the campaign records
     * @param this will be an array.
     * @return $resultSet as array or boolean
     * @author Icreon Tech - AS
     */
    public function getCampaignId($params = array()) {
        try {
	     $is_ellis_default = isset($params['is_ellis_default'])?$params['is_ellis_default']:'0'; 
	     $campaign_code = isset($params['campaign_code'])?$params['campaign_code']:'';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_com_getCampaignId(?,?)');
            $stmt->getResource()->bindParam(1, $campaign_code);
	     $stmt->getResource()->bindParam(2, $is_ellis_default);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return !empty($resultSet[0]) ? $resultSet[0] : array();
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the campaign records
     * @param this will be an array.
     * @return $resultSet as array or boolean
     * @author Icreon Tech - AS
     */
    public function searchCampaign($params = array()) {

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cam_getCampaigns(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['campaign_code']);
            $stmt->getResource()->bindParam(2, $params['campaign_type']);
            $stmt->getResource()->bindParam(3, $params['program_id']);
            $stmt->getResource()->bindParam(4, $params['start_date']);
            $stmt->getResource()->bindParam(5, $params['end_date']);
            $stmt->getResource()->bindParam(6, $params['campaign_status']);
            $stmt->getResource()->bindParam(7, $params['startIndex']);
            $stmt->getResource()->bindParam(8, $params['recordLimit']);
            $stmt->getResource()->bindParam(9, $params['sortField']);
            $stmt->getResource()->bindParam(10, $params['sortOrder']);
            $stmt->getResource()->bindParam(11, (isset($params['name_email']) and trim($params['name_email']) != "")?addslashes($params['name_email']):"");
            $stmt->getResource()->bindParam(12, $params['channel']);
            $stmt->getResource()->bindParam(13, $params['title']);
            $stmt->getResource()->bindParam(14, $params['group_id']);
			$stmt->getResource()->bindParam(15, $params['project_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get a particular campaign details
     * @param $params as array.
     * @return $resultSet as array or boolean
     * @author Icreon Tech - AS
     */
    public function searchCampaignDetails($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cam_getCampaignDetails(?)');
            $stmt->getResource()->bindParam(1, $params['campaign_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will save the search for campaign 
     * @param $params as array
     * @return boolean value
     * @author Icreon Tech - AS
     */
    public function saveCrmCampaignSearch($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cam_saveSearch(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['crm_user_id']);
            $stmt->getResource()->bindParam(2, $params['search_name']);
            $stmt->getResource()->bindParam(3, $params['search_query']);
            $stmt->getResource()->bindParam(4, $params['isActive']);
            $stmt->getResource()->bindParam(5, $params['isDelete']);
            $stmt->getResource()->bindParam(6, $params['added_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will check for existing of same search name
     * @param $params as array
     * @return $resultSet as array or boolean
     * @author Icreon Tech -AS
     */
    public function saveCrmCampaignSearchValid($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cam_saveSearchValid(?,?)');
            $stmt->getResource()->bindParam(1, $params['crm_user_id']);
            $stmt->getResource()->bindParam(2, $params['search_name']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update the save search 
     * @param $params as array
     * @return boolean value
     * @author Icreon Tech - AS
     */
    public function updateCrmCampaignSearch($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cam_updateSearch(?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['crm_user_id']);
            $stmt->getResource()->bindParam(2, $params['search_name']);
            $stmt->getResource()->bindParam(3, $params['search_query']);
            $stmt->getResource()->bindParam(4, $params['isActive']);
            $stmt->getResource()->bindParam(5, $params['isDelete']);
            $stmt->getResource()->bindParam(6, $params['modifiend_date']);
            $stmt->getResource()->bindParam(7, $params['search_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the saved search listing for campaign
     * @param this will be an array.
     * @return $resultSet as array or boolean
     * @author Icreon Tech -AS
     */
    public function getCrmCampaignSavedSearch($params = array()) {

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_getCrmCampaignpSavedSearch(?,?,?)');
            $stmt->getResource()->bindParam(1, $params['crm_user_id']);
            $stmt->getResource()->bindParam(2, $params['isActive']);
            $stmt->getResource()->bindParam(3, $params['campaignSearchId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will delete the saved search titles
     * @param $params as array
     * @return boolean value
     * @author Icreon Tech -AS
     */
    public function deleteCrmCampaignSearch($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cam_deleteCrmCampaignSearch(?,?,?)');
            $stmt->getResource()->bindParam(1, $params['search_id']);
            $stmt->getResource()->bindParam(2, $params['isDelete']);
            $stmt->getResource()->bindParam(3, $params['modifiend_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            //$resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will delete the crm campaign
     * @param $params as array
     * @return this will return a boolean confirmation
     * @author Icreon Tech -AS
     */
    public function deleteCrmCampaign($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cam_deleteCampaign(?,?,?)');
            $stmt->getResource()->bindParam(1, $params['campaign_id']);
            $stmt->getResource()->bindParam(2, $params['isDelete']);
            $stmt->getResource()->bindParam(3, $params['modifiend_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            //$resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

   

    /**
     * This function will fetch the channel o campaign
     * @param $params as array
     * @return $resultSet as array or boolean value
     * @author Icreon Tech -AS
     */
    public function getChannelDetails($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cam_channelcodes(?)');
            $stmt->getResource()->bindParam(1, $params);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get user campaign count
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function getUserCampaignCount($dataArr) {
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_usr_getUserCampaignCount(?)");
        $stmt->getResource()->bindParam(1, $user_id);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet[0]['total'];
    }

    /**
     * Function for to for campaign code length
     * @author Icreon Tech - AS
     * @return Array
     * @param Void
     */
    public function lastCampaignId() {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_cam_cmapaignId()");
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
    }

    /**
     * Function for to for setting bad recepient
     * @author Icreon Tech - AS
     * @return Array
     * @param Void
     */
    public function updateBadRecipientStatus($params = array()) {
        try {
            //asd($params);
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cam_updateBadRecipientStatus(?,?)');
            $stmt->getResource()->bindParam(1, $params['campaignCode']);
            $stmt->getResource()->bindParam(2, $params['userEmail']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            //$resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to for getting bad recepient list
     * @author Icreon Tech - AS
     * @return Array
     * @param Array
     */
    public function getBadRecipientUsers($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cam_getBadRecipientUsers(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['startIndex']);
            $stmt->getResource()->bindParam(2, $params['recordLimit']);
            $stmt->getResource()->bindParam(3, $params['sortField']);
            $stmt->getResource()->bindParam(4, $params['sortOrder']);
            $stmt->getResource()->bindParam(5, $params['campaign_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to for getting available campaign code
     * @author Icreon Tech - AS
     * @return Array
     * @param Void
     */
    public function campCodeExist($params = array()) {
        try {
            $campaignId = null;
			$current_date_time = isset($params['current_date_time']) ? $params['current_date_time'] : null;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_com_getCampaignCode(?,?,?)');
            $stmt->getResource()->bindParam(1, $params);
            $stmt->getResource()->bindParam(2, $campaignId);
			$stmt->getResource()->bindParam(3, $current_date_time);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

  

    /**
     * Function for to for get touch points
     * @author Icreon Tech - DG
     * @return Array
     * @param Void
     */
    public function getTouchPoints($dataArr = array()) {
        $campaignId = isset($dataArr['campaign_id']) ? $dataArr['campaign_id'] : null;
        $start_index = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $record_limit = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $sort_field = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $sort_order = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_cam_getTouchPoints(?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $campaignId);
        $stmt->getResource()->bindParam(2, $start_index);
        $stmt->getResource()->bindParam(3, $record_limit);
        $stmt->getResource()->bindParam(4, $sort_field);
        $stmt->getResource()->bindParam(5, $sort_order);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to for get touch points
     * @author Icreon Tech - DG
     * @return Array
     * @param Void
     */
    public function insertTouchPoint($dataArr = array()) {
        $campaignId = isset($dataArr['campaign_id']) ? $dataArr['campaign_id'] : null;
        $numSent = (isset($dataArr['num_sent']) && $dataArr['num_sent'] != '') ? $dataArr['num_sent'] : '';
        $modifiedBy = (isset($dataArr['modified_by']) && $dataArr['modified_by'] != '') ? $dataArr['modified_by'] : '';
        $dateAdded = DATE_TIME_FORMAT;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_cam_insertTouchPoints(?,?,?,?)');
        $stmt->getResource()->bindParam(1, $campaignId);
        $stmt->getResource()->bindParam(2, $numSent);
        $stmt->getResource()->bindParam(3, $modifiedBy);
        $stmt->getResource()->bindParam(4, $dateAdded);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            return false;
        }
    }

     /**
     * Function for to for get touch points
     * @author Icreon Tech - NS
     * @return Array
     * @param Void
     */
    public function insertTouchPointCampaign($dataArr = array()) {
        $campaignId = isset($dataArr['campaign_id']) ? $dataArr['campaign_id'] : null;
        $modifiedBy = (isset($dataArr['modified_by']) && $dataArr['modified_by'] != '') ? $dataArr['modified_by'] : '';
        $dateAdded = DATE_TIME_FORMAT;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_cam_insertTouchPointsCampaign(?,?,?)');
        $stmt->getResource()->bindParam(1, $campaignId);
        $stmt->getResource()->bindParam(2, $modifiedBy);
        $stmt->getResource()->bindParam(3, $dateAdded);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            return false;
        }
    }
    
    
     /**
     * Function for to for add touch points contact
     * @author Icreon Tech - NS
     * @return Array
     * @param Void
     */
    public function getSelectInsertTouchPointContact($dataArr = array()) {
        $campaignId = (isset($dataArr['campaign_id']) && $dataArr['campaign_id'] != '') ? $dataArr['campaign_id'] : '';
        $touchPointId = (isset($dataArr['touch_point_id']) && $dataArr['touch_point_id'] != '') ? $dataArr['touch_point_id'] : '';
        $dateAdded = DATE_TIME_FORMAT;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_cam_getContactToSentCampaign(?,?,?)');
        $stmt->getResource()->bindParam(1, $campaignId);
        $stmt->getResource()->bindParam(2, $touchPointId);
        $stmt->getResource()->bindParam(3, $dateAdded);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }
    
    /**
     * Function for to for add touch points contact
     * @author Icreon Tech - DG
     * @return Array
     * @param Void
     */
    public function addTouchPointContact($dataArr = array()) {
        $touchPointId = isset($dataArr['touch_point_id']) ? $dataArr['touch_point_id'] : null;
        $userId = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '';
        $dateAdded = DATE_TIME_FORMAT;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_cam_insertTouchPointContact(?,?,?)');
        $stmt->getResource()->bindParam(1, $touchPointId);
        $stmt->getResource()->bindParam(2, $userId);
        $stmt->getResource()->bindParam(3, $dateAdded);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for to for get contact to sent
     * @author Icreon Tech - DG
     * @return Array
     * @param Void
     */
    public function getTouchPointContact($dataArr = array()) {
        $touchId = isset($dataArr['touch_id']) ? $dataArr['touch_id'] : null;
        $offset = isset($dataArr['offset']) ? $dataArr['offset'] : 0;
        $rec_limit = isset($dataArr['rec_limit']) ? $dataArr['rec_limit'] : 10000;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_cam_getTouchPointContact(?,?,?)');
        $stmt->getResource()->bindParam(1, $touchId);
        $stmt->getResource()->bindParam(2, $offset);
        $stmt->getResource()->bindParam(3, $rec_limit);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet)) {
            return $resultSet;
        } else {
            return false;
        }
    }
    /**
     * Function to get contact count to sent
     * @author Icreon Tech - DG
     * @return integer/false
     * @param data array
     */
    public function getTouchPointContactCount($dataArr = array()) {
        $touchId = isset($dataArr['touch_id']) ? $dataArr['touch_id'] : null;
        
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_cam_getTouchPointContactCount(?)');
        $stmt->getResource()->bindParam(1, $touchId);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (isset($resultSet[0]['totalcount'])) {
            return $resultSet[0]['totalcount'];
        } else {
            return false;
        }
    }

    /**
     * Function for to for get contact to sent
     * @author Icreon Tech - DG
     * @return Array
     * @param Void
     */
    public function getContactToSent($dataArr = array()) {
        $campaignId = isset($dataArr['campaign_id']) ? $dataArr['campaign_id'] : null;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_cam_getContactToSent(?)');
        $stmt->getResource()->bindParam(1, $campaignId);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet)) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to for update touch points
     * @author Icreon Tech - DG
     * @return Array
     * @param Void
     */
    public function updateTouchPoint($dataArr = array()) {
        $touchId = isset($dataArr['touch_id']) ? $dataArr['touch_id'] : null;
        $campaignId = isset($dataArr['campaign_id']) ? $dataArr['campaign_id'] : null;
        $openRate = isset($dataArr['open_rate']) ? $dataArr['open_rate'] : null;
        $bounceRate = isset($dataArr['bounce_rate']) ? $dataArr['bounce_rate'] : null;
        $clickedRate = isset($dataArr['clicked_rate']) ? $dataArr['clicked_rate'] : null;
        $modifiedBy = isset($dataArr['modified_by']) ? $dataArr['modified_by'] : null;
        $modifiedDate = isset($dataArr['modified_date']) ? $dataArr['modified_date'] : null;

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_cam_updateTouchPoints(?,?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $touchId);
        $stmt->getResource()->bindParam(2, $campaignId);
        $stmt->getResource()->bindParam(3, $openRate);
        $stmt->getResource()->bindParam(4, $bounceRate);
        $stmt->getResource()->bindParam(5, $clickedRate);
        $stmt->getResource()->bindParam(6, $modifiedBy);
        $stmt->getResource()->bindParam(7, $modifiedDate);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for to for get responded contact
     * @author Icreon Tech - DG
     * @return Array
     * @param Void
     */
    public function getRespondedContact($dataArr = array()) {
        $campaignId = isset($dataArr['campaign_id']) ? $dataArr['campaign_id'] : null;

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_cam_getRespondedContact(?)');
        $stmt->getResource()->bindParam(1, $campaignId);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet)) {
            return $resultSet[0]['total'];
        } else {
            return false;
        }
    }

    /**
     * Function for to for get group and contact from campaign
     * @author Icreon Tech - DG
     * @return Array
     * @param Void
     */
    public function getGroupByCampaign($dataArr = array()) {
        $campaignId = isset($dataArr['campaign_id']) ? $dataArr['campaign_id'] : null;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_cam_getGroupByCampaign(?)');
        $stmt->getResource()->bindParam(1, $campaignId);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet)) {
            return $resultSet;
        } else {
            return false;
        }
    }


    /**
     * Function for to Update revenue Goal
     * @author Icreon Tech - KK
     */
    public function updateRevenueGoal($dataArr) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_camp_UpdateRevenueGoal(?,?)');
        $campaign_id = isset($dataArr['campaign_id']) ? $dataArr['campaign_id'] : null;
        $revenue_recieved = isset($dataArr['revenue_recieved']) ? $dataArr['revenue_recieved'] : 0;
        $stmt->getResource()->bindParam(1, $campaign_id);
        $stmt->getResource()->bindParam(2, $revenue_recieved);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }
    
    /**
     * Function for adding a campaign channel(s)
     * @return boolean value
     * @param $params as an array
     * @author Icreon Tech - AS
     */
    public function updateCampaignChannel($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cam_updateCamChannels(?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['campaign_id']);
            $stmt->getResource()->bindParam(2, $params['channel_id']);
            $stmt->getResource()->bindParam(3, $params['channel_cost']);
            $stmt->getResource()->bindParam(4, $params['coupon_code']);
            $stmt->getResource()->bindParam(5, $params['channel_camp_id']);
            $stmt->getResource()->bindParam(6, $params['added_date']);
            $stmt->getResource()->bindParam(7, $params['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }


}
