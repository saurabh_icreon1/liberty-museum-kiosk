<?php

/**
 * This form is used to save search campaign for admin
 * @package    Campaign
 * @author     Icreon Tech - AS
 */

namespace Campaign\Form;

use Zend\Form\Form;

class SaveSearchCampaignForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('save_search');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'save_search_as',
            'attributes' => array(
                'type' => 'text',
                'id' => 'save_search_as'
            )
        ));


        $this->add(array(
            'name' => 'savesearch',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'SAVE',
                'id' => 'savesearch',
                'class' => 'save-btn m-l-15'
            ),
        ));
        $this->add(array(
            'name' => 'search_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'search_id',
                'class' => 'save-btn m-l-5'
            ),
        ));
        $this->add(array(
            'name' => 'deletebutton',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Delete',
                'id' => 'deletebutton',
                'class' => 'save-btn m-l-5',
                'style' => 'display:none;',
            ),
        ));
    }

}