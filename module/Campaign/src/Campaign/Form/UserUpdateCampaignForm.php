<?php

/**
 * This form is used to search user campaign record
 * @package    Campain
 * @author     Icreon Tech - DG
 */

namespace Campaign\Form;

use Zend\Form\Form;

class UserUpdateCampaignForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('user_update_campaign');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'user_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'user_id'
            )
        ));
        $this->add(array(
            'name' => 'campaign_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'campaign_id'
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'channel_id',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                ),
            ),
            'attributes' => array(
                'id' => 'channel_id',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));

        $this->add(array(
            'name' => 'save',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'id' => 'save',
                'class' => 'save-btn'
            ),
        ));
    }

}