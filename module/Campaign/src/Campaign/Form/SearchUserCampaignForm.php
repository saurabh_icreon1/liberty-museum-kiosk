<?php

/**
 * This form is used to search user campaign record
 * @package    Camapign
 * @author     Icreon Tech - DG
 */

namespace Campaign\Form;

use Zend\Form\Form;

class SearchUserCampaignForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('user_search_campaign');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'user_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'user_id'
            )
        ));
        $this->add(array(
            'name' => 'campaign_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'campaign_id'
            )
        ));
        
        $this->add(array(
            'name' => 'campaign_code',
            'attributes' => array(
                'type' => 'text',
                'id' => 'campaign_code',
                'class' => 'search-icon'
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'channel_id',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                ),
            ),
            'attributes' => array(
                'id' => 'channel_id',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));

        $this->add(array(
            'name' => 'searchbutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Search',
                'id' => 'searchbutton',
                'class' => 'save-btn'
            ),
        ));
    }

}