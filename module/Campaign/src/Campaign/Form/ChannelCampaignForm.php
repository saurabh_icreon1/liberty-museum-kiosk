<?php

/**
 * This form is used to create channel sub part
 * @package    Campaign
 * @author     Icreon Tech - AS
 */

namespace Campaign\Form;

use Zend\Form\Form;

class ChannelCampaignForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('add_channel_campaign');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'type' => 'Select',
            'name' => 'channel[]',
            'options' => array(),
            'attributes' => array(
                'id' => 'channel',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));

        $this->add(array(
            'name' => 'gl_code[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'gl_code'
            )
        ));

        $this->add(array(
            'name' => 'coupon_code[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'coupon_code'
            )
        ));
        $this->add(array(
            'name' => 'channel_cost[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'channel_cost'
            )
        ));
        $this->add(array(
            'name' => 'channel_id[]',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'channel_id'
            )
        ));
    }

}