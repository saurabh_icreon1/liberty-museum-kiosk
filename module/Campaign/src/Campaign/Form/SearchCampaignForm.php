<?php

/**
 * This form is used to search campaign record
 * @package    Campaign
 * @author     Icreon Tech - AS
 */

namespace Campaign\Form;

use Zend\Form\Form;

class SearchCampaignForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('search_campaign');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type' => 'text',
                'id' => 'title'
            )
        ));
        $this->add(array(
            'name' => 'name_email',
            'attributes' => array(
                'type' => 'text',
                'id' => 'name_email'
            )
        ));

        $this->add(array(
            'name' => 'campaign_code',
            'attributes' => array(
                'type' => 'text',
                'id' => 'campaign_code',
                'class' => 'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'campaign_code_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'campaign_code_id'
            )
        ));

        $this->add(array(
            'name' => 'program',
            'attributes' => array(
                'type' => 'text',
                'id' => 'program',
                'class' => 'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'program_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'program_id'
            )
        ));

        $this->add(array(
            'name' => 'start_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'start_date',
                'class' => 'width-120 cal-icon'
            )
        ));
        $this->add(array(
            'name' => 'start_time',
            'attributes' => array(
                'type' => 'text',
                'id' => 'start_time',
                'class' => 'width-120 time-icon'
            )
        ));


        $this->add(array(
            'name' => 'end_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'end_date',
                'class' => 'width-120 cal-icon'
            )
        ));
        $this->add(array(
            'name' => 'end_time',
            'attributes' => array(
                'type' => 'text',
                'id' => 'end_time',
                'class' => 'width-120 time-icon'
            )
        ));



        $this->add(array(
            'type' => 'Select',
            'name' => 'campaign_type',
            'options' => array(
                'value_options' => array(),
            ),
            'attributes' => array(
                'id' => 'campaign_type',
                'class' => 'e1 select-w-320',
                'value' => '',
                'readonly' => 'true'
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'campaign_status',
            'options' => array(
                'value_options' => array(),
            ),
            'attributes' => array(
                'id' => 'campaign_status',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));
        
        $this->add(array(
            'type' => 'Select',
            'name' => 'channel',
            'options' => array(
                'value_options' => array(),
            ),
            'attributes' => array(
                'id' => 'channel',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));


        $this->add(array(
            'name' => 'searchbutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'SEARCH',
                'id' => 'searchbutton',
                'class' => 'search-btn'
            ),
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'saved_search',
            'options' => array(),
            'attributes' => array(
                'id' => 'saved_search',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));
        $this->add(array(
            'name' => 'sendMail',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Add User',
                'id' => 'sendMail',
                'class' => 'save-btn'
                 ),
        ));
        
        $this->add(array(
            'type' => 'Select',
            'name' => 'group_from',
            'options' => array(
                'value_options' => array(),
            ),
            'attributes' => array(
                'id' => 'group_from',
                'multiple' => 'multiple',
                'value' => ''
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'group_to',
            'options' => array(
                'value_options' => array(),
            ),
            'attributes' => array(
                'id' => 'group_to',
                'multiple' => 'multiple',
                'value' => ''
            )
        ));

        $this->add(array(
            'type' => 'Hidden',
            'name' => 'groups',
            'options' => array(
                'value_options' => array(),
            ),
            'attributes' => array(
                'id' => 'groups'
            )
        ));
        
        $this->add(array(
            'name' => 'addbutton',
            'attributes' => array(
                'type' => 'button',
                'value' => 'ADD >>',
                'id' => 'addbutton',
                'class' => 'save-btn m-l-10'
            ),
        ));

        $this->add(array(
            'name' => 'removebutton',
            'attributes' => array(
                'type' => 'button',
                'value' => '<< REMOVE',
                'id' => 'removebutton',
                'class' => 'cancel-btn m-l-10'
            ),
        ));

		$this->add(array(
            'name' => 'project',
            'attributes' => array(
                'type' => 'text',
                'id' => 'project',
                'class' => 'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'project_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'project_id'
            )
        ));
    }

}