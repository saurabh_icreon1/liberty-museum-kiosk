<?php

/**
 * This form is used to upload bad recipient
 * @package    Campaign
 * @author     Icreon Tech - AS
 */

namespace Campaign\Form;

use Zend\Form\Form;

class BadRecipientForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('bad_recipient');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'bad_recipient',
            'attributes' => array(
                'type' => 'file',
                'id' => 'bad_recipient'
            )
        ));
    }

}