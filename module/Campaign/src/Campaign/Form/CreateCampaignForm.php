<?php

/**
 * This form is used to create new campaign 
 * @package    Campaign
 * @author     Icreon Tech - AS
 */

namespace Campaign\Form;

use Zend\Form\Form;

class CreateCampaignForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('add_campaign');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'campaign_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'campaign_id'
            )
        ));
        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type' => 'text',
                'id' => 'title'
            )
        ));
        $this->add(array(
            'name' => 'description',
            'attributes' => array(
                'type' => 'text',
                'id' => 'description'
            )
        ));
//        $this->add(array(
//            'name' => 'coupon_code',
//            'attributes' => array(
//                'type' => 'text',
//                'id' => 'coupon_code'
//            )
//        ));

        $this->add(array(
            'name' => 'program',
            'attributes' => array(
                'type' => 'text',
                'id' => 'program'
            )
        ));

        $this->add(array(
            'name' => 'program_id',
            'attributes' => array(
                'type' => 'Hidden',
                'id' => 'program_id'
            )
        ));
        $this->add(array(
            'name' => 'thumbnail_image',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 'thumbnail_image'
            ),
        ));
        $this->add(array(
            'name' => 'thumbnail',
            'type' => 'file',
            'attributes' => array(
                'id' => 'thumbnail'
            ),
        ));

        $this->add(array(
            'name' => 'start_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'start_date',
                'class' => 'width-155 cal-icon'
            )
        ));
        $this->add(array(
            'name' => 'start_time',
            'attributes' => array(
                'type' => 'text',
                'id' => 'start_time',
                'class' => 'width-108 m-l-20 time-icon'
            )
        ));


        $this->add(array(
            'name' => 'end_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'end_date',
                'class' => 'width-155 cal-icon'
            )
        ));
        $this->add(array(
            'name' => 'end_time',
            'attributes' => array(
                'type' => 'text',
                'id' => 'end_time',
                'class' => 'width-108 m-l-20 time-icon'
            )
        ));
        $this->add(array(
            'name' => 'campaign_goal',
            'attributes' => array(
                'type' => 'text',
                'id' => 'campaign_goal'
            )
        ));
        $this->add(array(
            'name' => 'revenue_goal',
            'attributes' => array(
                'type' => 'text',
                'id' => 'revenue_goal'
            )
        ));
//        $this->add(array(
//            'name' => 'gl_code',
//            'attributes' => array(
//                'type' => 'text',
//                'id' => 'gl_code'
//            )
//        ));
        $this->add(array(
            'name' => 'designation',
            'attributes' => array(
                'type' => 'text',
                'id' => 'designation'
            )
        ));
        $this->add(array(
            'name' => 'department',
            'attributes' => array(
                'type' => 'text',
                'id' => 'department'
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'campaign_type',
            'options' => array(
                'value_options' => array(),
            ),
            'attributes' => array(
                'id' => 'campaign_type',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'group_from',
            'options' => array(
                'value_options' => array(),
            ),
            'attributes' => array(
                'id' => 'group_from',
                'multiple' => 'multiple',
                'value' => ''
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'group_to',
            'options' => array(
                'value_options' => array(),
            ),
            'attributes' => array(
                'id' => 'group_to',
                'multiple' => 'multiple',
                'value' => ''
            )
        ));

        $this->add(array(
            'type' => 'Hidden',
            'name' => 'groups',
            'options' => array(
                'value_options' => array(),
            ),
            'attributes' => array(
                'id' => 'groups'
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'campaign_status',
            'options' => array(
                'value_options' => array(),
            ),
            'attributes' => array(
                'id' => 'campaign_status',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));


        $this->add(array(
            'name' => 'is_active',
            'type' => 'Checkbox',
            'attributes' => array(
                'value' => '0',
                'id' => 'is_active',
                'class' => 'e2',
            ),
            'options' => array(
                'value_options' => array(
                    '0' => 'Checkbox',
                    '1' => 'Checkbox',
                ),
            ),
        ));

        $this->add(array(
            'name' => 'is_global',
            'type' => 'Checkbox',
            'attributes' => array(
                'value' => '0',
                'id' => 'is_global',
                'class' => 'e2',
            ),
            'options' => array(
                'value_options' => array(
                    '0' => 'Checkbox',
                    '1' => 'Checkbox',
                ),
            ),
        ));
        
        $this->add(array(
            'name' => 'restricted',
            'type' => 'Checkbox',
            'attributes' => array(
                'value' => '0',
                'id' => 'restricted',
                'class' => 'e2',
            ),
            'options' => array(
                'value_options' => array(
                    '0' => 'Checkbox',
                    '1' => 'Checkbox',
                ),
            ),
        ));
        
        
        $this->add(array(
            'name' => 'addbutton',
            'attributes' => array(
                'type' => 'button',
                'value' => 'ADD >>',
                'id' => 'addbutton',
                'class' => 'save-btn m-l-10'
            ),
        ));

        $this->add(array(
            'name' => 'removebutton',
            'attributes' => array(
                'type' => 'button',
                'value' => '<< REMOVE',
                'id' => 'removebutton',
                'class' => 'cancel-btn m-l-10'
            ),
        ));

        $this->add(array(
            'name' => 'savebutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'id' => 'savebutton',
                'class' => 'save-btn m-l-10'
            ),
        ));

        $this->add(array(
            'name' => 'cancelbutton',
            'attributes' => array(
                'type' => 'button',
                'value' => 'CANCEL',
                'id' => 'cancelbutton',
                'class' => 'cancel-btn m-l-10',
                'onclick' => 'window.location.href="/campaigns"'
            ),
        ));

		$this->add(array(
            'name' => 'project',
            'attributes' => array(
                'type' => 'text',
                'id' => 'project',
				'class' => 'search-icon',
            )
        ));

        $this->add(array(
            'name' => 'project_id',
            'attributes' => array(
                'type' => 'Hidden',
                'id' => 'project_id'
            )
        ));
    }

}