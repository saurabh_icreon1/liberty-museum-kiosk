<?php 

/**
 * This controller is used in campaign modules
 * @package    Campaign
 * @author     Icreon Tech - AS
 */

namespace Campaign\Controller;

use Base\Controller\BaseController;
use Base\Model\UploadHandler;
use Zend\View\Model\ViewModel;
use Campaign\Form\SearchCampaignForm;
use Campaign\Form\SearchUserCampaignForm;
use Campaign\Form\SaveSearchCampaignForm;
use Campaign\Form\CreateCampaignForm;
use Campaign\Form\ChannelCampaignForm;
use Campaign\Form\UserUpdateCampaignForm;
use Campaign\Form\BadRecipientForm;
use Campaign\Model\Campaign;
use Group\Model\Group;
use Zend\Http\Request;
use Zend\Authentication\AuthenticationService;
use Base\Model\SpreadsheetExcelReader;
use ZipArchive;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;

/**
 * This class is used in campaign modules
 * @package    Campaign
 * @author     Icreon Tech - AS
 */
class CampaignController extends BaseController {

    protected $_campaignTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_flashMessage = null;

    function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        parent::__construct();
        $auth = new AuthenticationService();
        $this->auth = $auth;
    }

    /**
     * This function is used to get table and config file object
     * @return     array
     * @author Icreon Tech - AS
     */
    public function getCampaignTable() {
        if (!$this->_campaignTable) {
            $sm = $this->getServiceLocator();
            $this->_campaignTable = $sm->get('Campaign\Model\CampaignTable');
            $this->_translator = $sm->get('translator');
            $this->_config = $sm->get('Config');
            $this->_adapter = $sm->get('dbAdapter');
            $this->_auth = new AuthenticationService();
        }
        return $this->_campaignTable;
    }

    /**
     * This action is used for creating view of Campaign search
     * @param $getSearchArray
     * @return $viewModel
     * @author Icreon Tech - AS
     */
    public function getCampaignsAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getCampaignTable();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $viewModel = new ViewModel();
        $response = $this->getResponse();
        $searchCampaignForm = new SearchCampaignForm();
        $saveSearchCampaignForm = new SaveSearchCampaignForm();
        $response = $this->getResponse();
        $getSearchArray = array();
        $getSearchArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
        $getSearchArray['isActive'] = '1';
        $getSearchArray['searchId'] = '';
        $crmSearchArray = $this->getCampaignTable()->getCrmCampaignSavedSearch($getSearchArray);
        $crmSearchList = array();
        $crmSearchList[''] = 'Select';
        foreach ($crmSearchArray as $key => $val) {
            $crmSearchList[$val['campaign_search_id']] = stripslashes($val['title']);
        }
        $searchCampaignForm->get('saved_search')->setAttribute('options', $crmSearchList);
        $campaignResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCampaignType(" ");
        $campaignList = array();
        $campTypeDesc = array();
        $campaignList[''] = 'Select';
        foreach ($campaignResult as $key => $val) {
            $campaignList[$val['campaign_type_id']] = stripslashes($val['campaign_type']);
            $campTypeDesc[$val['campaign_type_id']] = stripslashes($val['type_description']);
        }
        $searchCampaignForm->get('campaign_type')->setAttribute('options', $campaignList);

        $groupResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getGroups();
        sort($groupResult);
        $group_list = array();
        foreach ($groupResult as $key => $val) {
            $group_list[$val['group_id']] = stripslashes($val['name']);
        }
        $searchCampaignForm->get('group_from')->setAttribute('options', $group_list);
        $campaignStatusResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCampaignStatus(" ");
        $campaignStatus = array();
        $campStatDesc = array();
        $campaignStatus[''] = 'All';
        foreach ($campaignStatusResult as $key => $val) {
            $campaignStatus[$val['campaign_status_id']] = stripslashes($val['campaign_status']);
            $campStatDesc[$val['campaign_status_id']] = stripslashes($val['status_description']);
        }
        $searchCampaignForm->get('campaign_status')->setAttribute('options', $campaignStatus);

        $channelResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getChannelType();
        $channel_list = array();
        $channelDesc = array();
        $channel_list[''] = 'Select';
        foreach ($channelResult as $key => $val) {
            $channel_list[$val['channel_id']] = stripslashes($val['channel']);
            $channelDesc[$val['channel_id']] = stripslashes($val['description']);
        }
        $searchCampaignForm->get('channel')->setAttribute('options', $channel_list);

        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        $viewModel->setVariables(array(
            'campTypeDesc' => $campTypeDesc,
            'campStatDesc' => $campStatDesc,
            'channelDesc' => $channelDesc,
            'search_campaign_form' => $searchCampaignForm,
            'save_search_campaign_form' => $saveSearchCampaignForm,
            'messages' => $messages,
            'jsLangTranslate' => array_merge($this->_config['campaign_messages']['config']['common'], $this->_config['campaign_messages']['config']['create_campaign'], $this->_config['campaign_messages']['config']['search_campaign'])
        ));
        return $viewModel;
    }

    /**
     * This action is used for adding a new of Campaign
     * @param $postData
     * @return $viewModel
     * @author Icreon Tech - AS
     */
    public function addCampaignAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getCampaignTable();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();

        $viewModel = new ViewModel();
        $campaign = new Campaign($this->_adapter);
        $response = $this->getResponse();

        $createCampaignForm = new CreateCampaignForm();
        $channel_campaign_form = new ChannelCampaignForm();
        $createCampaignForm->add($channel_campaign_form);

        $campaignResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCampaignType(" ");
        $campaignList = array();
        $campTypeDesc = array();
        $campaignList[''] = 'Select';
        foreach ($campaignResult as $key => $val) {
            $campaignList[$val['campaign_type_id']] = stripslashes($val['campaign_type']);
            $campTypeDesc[$val['campaign_type_id']] = stripslashes($val['type_description']);
        }
        $createCampaignForm->get('campaign_type')->setAttribute('options', $campaignList);

        $groupResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getGroups();
        sort($groupResult);
        $group_list = array();
        foreach ($groupResult as $key => $val) {
            $group_list[$val['group_id']] = stripslashes($val['name']);
        }
        $createCampaignForm->get('group_from')->setAttribute('options', $group_list);

        $campaignStatusResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCampaignStatus(" ");
        $campaignStatus = array();
        $campaignStatus[''] = 'Select';
        $campaignStatusDescripton = array();
        foreach ($campaignStatusResult as $key => $val) {
            $campaignStatus[$val['campaign_status_id']] = stripslashes($val['campaign_status']);
            $campaignStatusDescripton[$val['campaign_status_id']] = stripslashes($val['status_description']);
        }
        $createCampaignForm->get('campaign_status')->setAttribute('options', $campaignStatus);

        $channelResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getChannelType();
        $channel_list = array();
        $channelDesc = array();
        $channel_list[''] = 'Select';
        foreach ($channelResult as $key => $val) {
            $channel_list[$val['channel_id']] = stripslashes($val['channel']);
            $channelDesc[$val['channel_id']] = stripslashes($val['description']);
        }
        $channel_campaign_form->get('channel[]')->setAttribute('options', $channel_list);

        $response = $this->getResponse();


        if ($request->isPost()) {

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $campaign = $request->getPost();
                $postData = array();
                $postData['campaign_id'] = $campaign->campaign_id;
                $postData['title'] = $campaign->title;
                $postData['description'] = $campaign->description;
                //$postData['coupon_code'] = $campaign->coupon_code;
                $postData['program_id'] = $campaign->program_id;
                $postData['thumbnail'] = $campaign->thumbnail_image;
                $postData['groups'] = $campaign->group_to;
                $postData['start_date'] = $this->InputDateFormat($campaign->start_date . " " . $campaign->start_time, 'dateformatampm');
                $postData['end_date'] = $this->InputDateFormat($campaign->end_date . " " . $campaign->end_time, 'dateformatampm');
                $postData['campaign_goal'] = $campaign->campaign_goal;
                $postData['revenue_goal'] = $campaign->revenue_goal;
                $postData['revenue_received'] = '0';
                $postData['designation'] = $campaign->designation;
                $postData['department'] = $campaign->department;
                $postData['campaign_type'] = $campaign->campaign_type;
                $postData['campaign_status'] = $campaign->campaign_status;
                $postData['is_active'] = $campaign->is_active;
                $postData['restricted'] = $campaign->restricted;
                $postData['is_deleted'] = '0';
                $postData['addedBy'] = $this->auth->getIdentity()->crm_user_id;
                $postData['added_date'] = DATE_TIME_FORMAT;
                $postData['modifiedBy'] = $this->auth->getIdentity()->crm_user_id;
                $postData['modified_date'] = DATE_TIME_FORMAT;
                $postData['is_global'] = $campaign->is_global;
				$postData['project_id'] = $campaign->project_id;
                $myArray = $postData['groups'];
                if (isset($postData['campaign_id']) && !empty($postData['campaign_id'])) {
                    /*
                     * For editing any campaign
                     */
                    $searchParam = array();
                    $searchParam['campaign_id'] = $postData['campaign_id'];
                    $searchResult = $this->getCampaignTable()->searchCampaignDetails($searchParam);
                    $this->moveFileFromTempToCampaign($campaign->thumbnail_image, 2, $searchResult[0]['campaign_image']);
                    $retArr = $this->getCampaignTable()->updateCampaign($postData);
                    $campaign_id = $postData['campaign_id'];
                    $arr = array();
                    foreach ($campaign['channel_id'] as $key => $val) {
                        if ($val != 0)
                            $arr[] = $val;
                    }
                    $channel_camp_id = implode(',', $arr);
                    $delete['campaign_id'] = $campaign_id;
                    $delete['channel_camp_id'] = $channel_camp_id;
                    $retArr = $this->getCampaignTable()->deleteCampaignDetails($delete);
                    $changeLogArray = array();
                    $changeLogArray['table_name'] = 'tbl_cam_change_logs';
                    $changeLogArray['activity'] = '2';/** 2 == for update */
                    $changeLogArray['id'] = $campaign_id;
                    $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                    $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                    $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                    $this->flashMessenger()->addMessage($this->_config['campaign_messages']['config']['search_campaign']['CAMP_UPDATED']);
                } else {
                    /*
                     * For adding a new campaign
                     */


                    $camp_code = "CAMP" . sprintf("%05s", mt_rand(1, 99999));
                    $camp = NULL;
                    $camp = $this->removeEmptyArray($this->getCampaignTable()->campCodeExist($camp));
                    foreach ($camp as $key) {
                        $campNew[] = $key['campaign_code'];
                    }
                    while (in_array($camp_code, $campNew)) {
                        $camp_code = "CAMP" . sprintf("%05s", mt_rand(1, 99999));
                    }
                    $postData['campaign_code'] = $camp_code;
					$postData['project_id'] = $campaign->project_id;
                    $this->moveFileFromTempToCampaign($campaign->thumbnail_image);
                    $retArr = $this->getCampaignTable()->addCampaign($postData);
                    $campaign_id = $retArr[0]['LAST_INSERT_ID'];
                    $changeLogArray['table_name'] = 'tbl_cam_change_logs';
                    $changeLogArray['activity'] = '1';/** 1 == for add */
                    $changeLogArray['id'] = $campaign_id;
                    $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                    $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                    $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                    $this->flashMessenger()->addMessage($this->_config['campaign_messages']['config']['search_campaign']['CAMP_ADDED']);
                }
                for ($i = 0; $i < count($campaign['channel']); $i++) {
                    /*
                     * For adding campaign channel(s)
                     */
                    $postChannelData = array();
                    $postChannelData['campaign_id'] = $campaign_id;
                    $postChannelData['channel_id'] = (isset($campaign['channel'][$i]) && !empty($campaign['channel'][$i])) ? $campaign['channel'][$i] : '';
                    $postChannelData['channel_cost'] = (isset($campaign['channel_cost'][$i]) && !empty($campaign['channel_cost'][$i])) ? $campaign['channel_cost'][$i] : '';
                    $postChannelData['coupon_code'] = (isset($campaign['coupon_code'][$i]) && !empty($campaign['coupon_code'][$i])) ? $campaign['coupon_code'][$i] : '';
                    $postChannelData['channel_camp_id'] = (isset($campaign['channel_id'][$i]) && !empty($campaign['channel_id'][$i])) ? $campaign['channel_id'][$i] : '';
                    // $postChannelData['gl_code'] = $campaign['gl_code'][$i];
                    $postChannelData['added_date'] = DATE_TIME_FORMAT;
                    $postChannelData['modified_date'] = DATE_TIME_FORMAT;
                    if (isset($postChannelData['channel_camp_id']) && !empty($postChannelData['channel_camp_id'])) {
                        $this->getCampaignTable()->updateCampaignChannel($postChannelData);
                    } else {
                        $this->getCampaignTable()->addCampaignChannel($postChannelData);
                    }
                }

                foreach ($myArray as $key => $val) {
                    /*
                     * For adding campaign group(s) 
                     */
                    $postGroupData = array();
                    $postGroupData['campaign_id'] = $campaign_id;
                    $postGroupData['group_id'] = $val;
                    $postGroupData['added_date'] = DATE_TIME_FORMAT;
                    $postGroupData['modified_date'] = DATE_TIME_FORMAT;
                    $this->getCampaignTable()->addCampaignGroup($postGroupData);
                }

                if ($retArr == true) {
                    $messages = array('status' => "success", 'message' => $this->_config['campaign_messages']['config']['create_campaign']['ADD_CONFIRM_MSG']);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                } else {
                    $messages = array('status' => "error", 'message' => $this->_config['campaign_messages']['config']['create_campaign']['CAMPAIGN_ERROR_MSG']);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                }
            }
        }

        $viewModel->setVariables(array(
            'create_campaign_form' => $createCampaignForm,
            'jsLangTranslate' => array_merge($this->_config['campaign_messages']['config']['common'], $this->_config['campaign_messages']['config']['create_campaign'], $this->_config['campaign_messages']['config']['view_campaign']),
            'campaignStatusDescripton' => $campaignStatusDescripton,
            'campTypeDesc' => $campTypeDesc,
            'channelDesc' => $channelDesc
        ));
        return $viewModel;
    }

    /**
     * This action is used for adding a channel list
     * @param $channel_list
     * @return $viewModel
     * @author Icreon Tech - AS
     */
    public function channelCampaignAction() {
        $this->checkUserAuthentication();
        $channel_campaign_form = new ChannelCampaignForm();
        $this->getCampaignTable();
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $channelResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getChannelType();
        $channel_list = array();
        $channelDesc = array();
        $channel_list[''] = 'Select';
        foreach ($channelResult as $key => $val) {
            $channel_list[$val['channel_id']] = stripslashes($val['channel']);
            $channelDesc[$val['channel_id']] = stripslashes($val['description']);
        }
        $channel_campaign_form->get('channel[]')->setAttribute('options', $channel_list);
        $viewModel->setVariables(array(
            'channel_campaign_form' => $channel_campaign_form,
            'channelDesc' => $channelDesc,
            'jsLangTranslate' => array_merge($this->_config['campaign_messages']['config']['common'], $this->_config['campaign_messages']['config']['search_campaign'])
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to get user campains
     * @author Icreon Tech-DG
     * @return Array
     * @param userId
     */
    public function getUserCampaignsAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getCampaignTable();
        $form = new SearchUserCampaignForm();

        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $response = $this->getResponse();
        $messages = array();

        $params = $this->params()->fromRoute();
        $channelResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getChannelType();
        $channel_list = array();
        $channel_list[''] = 'Any';
        foreach ($channelResult as $key => $val) {
            $channel_list[$val['channel_id']] = stripslashes($val['channel']);
        }
        $form->get('channel_id')->setAttribute('options', $channel_list);
        $form->get('user_id')->setValue($params['id']);

        $viewModel->setVariables(array(
            'form' => $form,
            'jsLangTranslate' => $this->_config['campaign_messages']['config']['user_search_campaign'])
        );
        return $viewModel;
    }

    /**
     * This Action is used to get user search campaign
     * @author Icreon Tech-DG
     * @return Array
     * @param Array
     */
    public function userSearchCampaignsAction() {
        try {
            $this->checkUserAuthentication();
            $request = $this->getRequest();
            $response = $this->getResponse();
            $messages = array();
            parse_str($request->getPost('searchString'), $searchParam);
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit;
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['user_id'] = $this->decrypt($searchParam['user_id']);
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'added_date') ? 'usr_campaign.added_date' : $searchParam['sort_field'];

            $campaign_arr = $this->getCampaignTable()->getUserCampaigns($searchParam);
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $jsonResult['page'] = $request->getPost('page');
            $jsonResult['records'] = $countResult[0]->RecordCount;
            $jsonResult['page'] = $request->getPost('page');
            $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
            if (!empty($campaign_arr)) {
                foreach ($campaign_arr as $val) {
                    if ($val['channel_res_name'] != '') {
                        $channel_res_name = $val['channel_res_name'];
                    } else {
                        $channel_res_name = '<a href="javascript:void(0);" onclick="updateResponse(\'' . $val['campaign_id'] . '\',\'' . $this->encrypt($val['user_id']) . '\');">No</a>';
                    }
                    $arrCell['id'] = $val['campaign_id'];
                    $arrCell['cell'] = array($val['campaign_code'], $val['campaign_title'], $val['channel_rcv_name'], $val['program'], $channel_res_name);
                    $subArrCell[] = $arrCell;
                    $jsonResult['rows'] = $subArrCell;
                }
            }
            $response->setContent(\Zend\Json\Json::encode($jsonResult));
            return $response;
        } catch (Exception $e) {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * This action is used for showing search data in the grid
     * @param $searchParam
     * @return $viewModel
     * @author Icreon Tech - AS
     */
    public function getSearchCampaignAction() {
        $this->checkUserAuthentication();
        $this->getCampaignTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        parse_str($request->getPost('searchString'), $searchParam);
        $dashlet = $request->getPost('crm_dashlet');
        $dashletSearchId = $request->getPost('searchId');
        $dashletId = $request->getPost('dashletId');
        $dashletSearchType = $request->getPost('searchType');
        $dashletSearchString = $request->getPost('dashletSearchString');
        $groups = (isset($searchParam['group_to']) && $searchParam['group_to'] != '') ? "'" . implode("','", $searchParam['group_to']) . "'" : '';
        $searchParam['group_id'] = $groups;
        if (isset($dashletSearchString) && $dashletSearchString != '') {
            $searchParam = unserialize($dashletSearchString);
        }

        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $msg = $this->_config['campaign_messages']['config']['common'];
        $isExport = $request->getPost('export');
        if ($isExport != "" && $isExport == "excel") {
            $chunksize = $this->_config['export']['chunk_size'];
            $export_limit = $this->_config['export']['export_limit'];

            $searchParam['recordLimit'] = $chunksize;
        } else {
            $searchParam['recordLimit'] = $limit;
        }

        $start = $limit * $page - $limit;
        $searchParam['startIndex'] = $start;

        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');
        if (isset($searchParam['start_date']) && !empty($searchParam['start_date'])) {
            if (isset($searchParam['start_time']) && !empty($searchParam['start_time'])) {
                $searchParam['start_date'] = $this->InputDateFormat($searchParam['start_date'] . " " . $searchParam['start_time'], 'dateformatampm');
                // $searchParam['start_date'] = date("Y-m-d", strtotime($searchParam['start_date'])) . " " . $searchParam['start_time'];
            } else {
                $searchParam['start_date'] = date("Y-m-d H:m:s", strtotime($searchParam['start_date']));
            }
        }
        if (isset($searchParam['end_date']) && !empty($searchParam['end_date'])) {
            if (isset($searchParam['end_time']) && !empty($searchParam['end_time'])) {
                $searchParam['end_date'] = $this->InputDateFormat($searchParam['end_date'] . " " . $searchParam['end_time'], 'dateformatampm');
                //$searchParam['end_date'] = date("Y-m-d", strtotime($searchParam['end_date'])) . " " . $searchParam['end_time'];
            } else {
                $searchParam['end_date'] = date("Y-m-d H:m:s", strtotime($searchParam['end_date']));
            }
        }
        $page_counter = 1;
        do {
			$searchResult = $this->getCampaignTable()->searchCampaign($searchParam);
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $dashletResult = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getTableStruct(array('user_id' => $this->_auth->getIdentity()->crm_user_id, 'search_id' => $dashletSearchId, 'dash_id' => $dashletId, 'search_type' => $dashletSearchType));
            $dashletColumnName = array();
            if (!empty($dashletResult)) {
                $dashletColumn = explode(",", $dashletResult[0]['table_column_names']);
                foreach ($dashletColumn as $val) {
                    if (strpos($val, ".")) {
                        $dashletColumnName[] = trim(strstr($val, ".", false), ".");
                    } else {
                        $dashletColumnName[] = trim($val);
                    }
                }
            }
            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $total = $countResult[0]->RecordCount;
            if ($isExport == "excel") {

                /* if ($total > $export_limit) {
                  $totalExportedFiles = ceil($total / $export_limit);
                  $exportzip = 1;
                  }
                 * */

                if ($total > $chunksize) {
                    $number_of_pages = ceil($total / $chunksize);
//echo $number_of_pages .'--'.$page_counter;
//echo "<br>";
                    $page_counter++;
                    $start = $chunksize * $page_counter - $chunksize;
                    $searchParam['start_index'] = $start;
                    $searchParam['page'] = $page_counter;
                } else {
                    $number_of_pages = 1;
                    $page_counter++;
                }
            } else {
                $page_counter = $page;
                $number_of_pages = ceil($total / $limit);
                $jsonResult['records'] = $countResult[0]->RecordCount;
                $jsonResult['page'] = $request->getPost('page');
                $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
            }
            if (count($searchResult) > 0) {
                $arrExport = array();
                foreach ($searchResult as $val) {
                    $dashletCell = array();
                    $arrCell['id'] = $val['campaign_id'];
                    if (false !== $dashletColumnKey = array_search('campaign_code', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = "<a href='/campaign/" . $this->encrypt($val['campaign_id']) . "/" . $this->encrypt('view') . "' class='txt-decoration-underline'>" . $val['campaign_code'] . "</a>";
                    if (false !== $dashletColumnKey = array_search('campaign_title', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = stripslashes($val['campaign_title']);
                    if (false !== $dashletColumnKey = array_search('campaign_start_date', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $this->OutputDateFormat($val['campaign_start_date'], 'dateformatampm');
                    if (false !== $dashletColumnKey = array_search('campaign_end_date', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $this->OutputDateFormat($val['campaign_end_date'], 'dateformatampm');

                    $pop_up = "<a onclick=camp_channel('" . $val['campaign_id'] . "'); href='Javascript: void(0)'><img src='img/g-icon.png'></a>  " . $val['campaign_code'];
                    $view = "<a class='view-icon' href='/campaign/" . $this->encrypt($val['campaign_id']) . "/" . $this->encrypt('view') . "'><div class='tooltip'>" . $msg['L_VIEW'] . "<span></span></div></a>";
                    $edit = "<a class='edit-icon' href='/campaign/" . $this->encrypt($val['campaign_id']) . "/" . $this->encrypt('edit') . "'><div class='tooltip'>" . $msg['L_EDIT'] . "<span></span></div></a>";
                    $delete = "<a class='delete-icon delete_campaign' href='#delete_campaign' onclick=deleteCampaign('" . $this->encrypt($val['campaign_id']) . "')><div class='tooltip'>" . $msg['L_DELETE'] . "<span></span></div></a>";
                    if (isset($dashlet) && $dashlet == 'dashlet') {
                        $arrCell['cell'] = $dashletCell;
                    } else if ($isExport == "excel") {

                        $arrExport[] = array('Campaign code' => $val['campaign_code'], 'Title' => $val['campaign_title'], 'Program' => $val['program'],'Project' => $val['project_name'], 'Group(s)' => $val['grpname'], 'Start Date' => $this->OutputDateFormat($val['campaign_start_date'], 'dateformatampm'), 'End Date' => $this->OutputDateFormat($val['campaign_end_date'], 'dateformatampm'), 'Received / Goal' => '$' . $val['campaign_revenue_received'] . ' / $' . $val['campaign_revenue_goal'], 'Status' => stripslashes($val['campaign_status']));
                    } else {
                        $arrCell['cell'] = array($arrCell['id'], $pop_up, stripslashes($val['campaign_title']), stripslashes($val['program']), stripslashes($val['project_name']), str_replace(",", "\n", $val['grpname']), $this->OutputDateFormat($val['campaign_start_date'], 'dateformatampm'), $this->OutputDateFormat($val['campaign_end_date'], 'dateformatampm'), "$" . stripslashes($val['campaign_revenue_received']) . "/ $" . stripslashes($val['campaign_revenue_goal']), stripslashes($val['campaign_status']), $val['is_user_added'], $view . $edit . $delete);
                    }
                    $subArrCell[] = $arrCell;
                    $jsonResult['rows'] = $subArrCell;
                }
                if ($isExport == "excel") {
                    $filename = "campaign_export_" . date("Y-m-d") . ".csv";
                    $this->arrayToCsv($arrExport, $filename, $page_counter);
                }
            }
        } while ($page_counter <= $number_of_pages && $isExport == "excel");
        if ($isExport == "excel") {
            $this->downloadSendHeaders("campaign_export_" . date("Y-m-d") . ".csv");
            die;
        } else {
            $response->setContent(\Zend\Json\Json::encode($jsonResult));
            return $response;
        }
//return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    /**
     * This Action is used to get campains code by autosuggest
     * @author Icreon Tech-DG
     * @return Array
     * @param keyword
     */
    public function getCampaignCodeAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getCampaignTable();
        $getSearchArray['active'] = '';
        if($request->isPost('active')){
            $getSearchArray['active'] = $request->isPost('active');
        }
        
        if ($request->isPost()) {
			$getSearchArray['campaign_code'] = $request->getPost('campaign_code');
            if($request->getPost('active')){
				$current_date_time = DATE_TIME_FORMAT;
				$getSearchArray['current_date_time'] = $current_date_time;
			}
            $campaignArray = $this->removeEmptyArray($this->getCampaignTable()->getCampaign($getSearchArray));
            $campaignList = array();
            if (!empty($campaignArray)) {
                $i = 0;
                foreach ($campaignArray as $key => $val) {
//$campaignList[$val['campaign_code']] = $val['campaign_code'];
                    $campaignList[$i] = array();
                    $campaignList[$i]['campaign_code'] = $val['campaign_code'];
                    $campaignList[$i]['campaign_id'] = $val['campaign_id'];
                    $i++;
                }
            }
            return $response->setContent(\Zend\Json\Json::encode($campaignList));
        }
    }

    /**
     * This Action is used to update campaign in user module
     * @author Icreon Tech-DG
     * @return Array
     * @param keyword
     */
    public function userUpdateCampaignAction() {
        $this->checkUserAuthentication();
        $this->layout('popup');
        $this->getCampaignTable();
        $form = new UserUpdateCampaignForm();

        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $response = $this->getResponse();
        $messages = array();

        $params = $this->params()->fromRoute();
        $channelResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getChannelType();
        $channel_list = array();
        $channel_list[''] = 'Please Select';
        foreach ($channelResult as $key => $val) {
            $channel_list[$val['channel_id']] = stripslashes($val['channel']);
        }
        $form->get('channel_id')->setAttribute('options', $channel_list);
        if (isset($params['user_id']))
            $form->get('user_id')->setValue($params['user_id']);

        if (isset($params['campaign_id']))
            $form->get('campaign_id')->setValue($params['campaign_id']);

        $campaign = new Campaign($this->_adapter);
        if ($request->isPost()) {
            $form->setInputFilter($campaign->getInputFilterUserUpdateCampaign());
            $form->setData($request->getPost());
            if (!$form->isValid()) {
                $msg = array();
                $errors = $form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            $msg [$key] = $this->_config['campaign_messages']['config']['user_search_campaign'][$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $campaign->exchangeArrayUserUpdateCampaign($form->getData());
                $postData = array();
                $postData['user_id'] = $this->decrypt($campaign->user_id);
                $postData['campaign_id'] = $campaign->campaign_id;
                $postData['channel_id'] = $campaign->channel_id;
                $last_update = $this->getCampaignTable()->userUpdateCampaign($postData);
                if ($last_update)
                    $messages = array('status' => "success", 'message' => $this->_config['campaign_messages']['config']['user_search_campaign']['UPDATED_SUCCESSFULLY']);
                else {
                    $messages = array('status' => "error", 'message' => $this->_config['campaign_messages']['config']['user_search_campaign']['UPDATED_ERROR']);
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $viewModel->setVariables(array(
            'form' => $form,
            'jsLangTranslate' => $this->_config['campaign_messages']['config']['user_search_campaign'])
        );
        return $viewModel;
    }

    /**
     * This Action is used to view a particular campaign
     * @return $viewModel
     * @param $searchParam
     * @author Icreon Tech-AS
     */
    public function viewCampaignAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getCampaignTable();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $searchParam['campaign_id'] = $this->decrypt($params['id']);
        $searchResult = $this->getCampaignTable()->searchCampaignDetails($searchParam);
        $mode = (isset($params['mode']) && $params['mode'] != '') ? $this->decrypt($params['mode']) : 'view';
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'campaign_id' => $params['id'],
            'moduleName' => $this->encrypt('campaign'),
            'searchResult' => $searchResult[0],
            'mode' => $mode,
            'module_name' => $this->encrypt('campaign'),
            'jsLangTranslate' => $this->_config['campaign_messages']['config']['view_campaign'],
            'params' => $params
        ));
        return $viewModel;
    }

    /**
     * This Action is used to save a particular campaign search
     * @return message in json format
     * @param $searchParam
     * @author Icreon Tech-AS
     */
    public function saveCampaignSearchAction() {
        $this->checkUserAuthentication();
        $this->getCampaignTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $campaign = new Campaign($this->_adapter);
        if ($request->isPost()) {


            $search_name = $request->getPost('search_name');
            $messages = array();
            parse_str($request->getPost('searchString'), $searchParam);
            //asd($searchParam);
            $searchParam['search_title'] = $search_name;
            $groups = (isset($searchParam['group_to']) && $searchParam['group_to'] != '') ? implode(",", $searchParam['group_to']) : '';
            $searchParam['group_id'] = $groups;
            $searchParam = $campaign->getInputFilterCrmSearch($searchParam);
            $search_name = $campaign->getInputFilterCrmSearch($search_name);

            if (trim($search_name) != '') {

                $saveSearchParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $saveSearchParam['isActive'] = 1;
                $saveSearchParam['isDelete'] = 0;
                $saveSearchParam['search_name'] = $search_name;

                $saveSearchParam['search_query'] = serialize($searchParam);
                if ($request->getPost('search_id') != '') {
                    $saveSearchParam['modifiend_date'] = DATE_TIME_FORMAT;
                    $saveSearchParam['search_id'] = $request->getPost('search_id');
                    if ($this->getCampaignTable()->updateCrmCampaignSearch($saveSearchParam) == true) {
                        $this->flashMessenger()->addMessage($this->_config['campaign_messages']['config']['search_campaign']['SEARCH_UPDATED']);
                        $messages = array('status' => "success");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                } else {
                    $saveSearchParam['added_date'] = DATE_TIME_FORMAT;
                    $count = $this->getCampaignTable()->saveCrmCampaignSearchValid($saveSearchParam);
                    if ($count[0]['id'] == 0) {
                        if ($this->getCampaignTable()->saveCrmCampaignSearch($saveSearchParam) == true) {
                            $this->flashMessenger()->addMessage($this->_config['campaign_messages']['config']['search_campaign']['SEARCH_SAVE']);
                            $messages = array('status' => "success");
                            return $response->setContent(\Zend\Json\Json::encode($messages));
                        }
                    } else {
                        $messages = array('status' => "exists", 'message' => $this->_config['campaign_messages']['config']['search_campaign']['SEARCH_EXIST']);
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                }
            }
        }
    }

    /**
     * This Action is used to generte the select option after saving the new search title
     * @param this will pass an array $dataParam
     * @return this will be a string having the all options
     * @author Icreon Tech - AS
     */
    public function getSavedCampaignSearchSelectAction() {
        $this->checkUserAuthentication();
        $this->getCampaignTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam = array();
            $dataParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
            $dataParam['isActive'] = '1';
            $dataParam['campaignSearchId'] = '';

            $crmSearchArray = $this->getCampaignTable()->getCrmCampaignSavedSearch($dataParam);
            $options = '<option value="">Select</option>';
            foreach ($crmSearchArray as $key => $val) {
                $options .="<option value='" . $val['campaign_search_id'] . "'>" . $val['title'] . "</option>";
            }
            return $response->setContent($options);
        }
    }

    /**
     * This Action is used to get the saved crm campaign search
     * @param this will pass an array $dataParam
     * @return this will be json format 
     * @author Icreon Tech - AS
     */
    public function getCrmSearchCampaignSavedParamAction() {
        $this->checkUserAuthentication();
        $this->getCampaignTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            if ($request->getPost('search_id') != '') {
                $dataParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $dataParam['isActive'] = '1';
                $dataParam['campaignSearchId'] = $request->getPost('search_id');
                $searchResult = $this->getCampaignTable()->getCrmCampaignSavedSearch($dataParam);
                $paramArray = unserialize($searchResult[0]['search_query']);
                $groupResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getGroups();
                $group_list = array();
                $groupArray = (isset($paramArray['group_id']) && $paramArray['group_id'] != '') ? explode(",", $paramArray['group_id']) : array();
                $option1 = $option2 = '';
                sort($groupResult);
                foreach ($groupResult as $key => $val) {
                    if (!empty($groupArray) && in_array($val['group_id'], $groupArray))
                        $option1.= '<option value="' . $val['group_id'] . '">' . stripslashes($val['name']) . '</option>';
                    else
                        $option2.= '<option value="' . $val['group_id'] . '">' . stripslashes($val['name']) . '</option>';
                }
                $paramArray['group_from'] = $option2;
                $paramArray['group_to'] = $option1;
                $searchResult = json_encode($paramArray);
                return $response->setContent($searchResult);
            }
        }
    }

    /**
     * This Action is used to delete the the saved crm campaign search
     * @param this will pass an array $dataParam
     * @return this will be a confirmation message in json
     * @author Icreon Tech - AS
     */
    public function deleteCrmSearchCampaignAction() {
        $this->checkUserAuthentication();
        $this->getCampaignTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam['search_id'] = $request->getPost('search_id');
            $dataParam['isDelete'] = 1;
            $dataParam['modifiend_date'] = DATE_TIME_FORMAT;
            $this->getCampaignTable()->deleteCrmCampaignSearch($dataParam);
            $this->flashMessenger()->addMessage($this->_config['campaign_messages']['config']['search_campaign']['SEARCH_DELETED']);
            $messages = array('status' => "success");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This Action is used to delete a campaign
     * @param this will pass an array $dataParam
     * @return this will be a confirmation message in json
     * @author Icreon Tech - AS
     */
    public function deleteCampaignAction() {
        $this->checkUserAuthentication();
        $this->getCampaignTable();
        $request = $this->getRequest();

        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam['campaign_id'] = $this->decrypt($request->getPost('campaign_id'));
            $dataParam['isDelete'] = 1;
            $dataParam['modifiend_date'] = DATE_TIME_FORMAT;
            $this->getCampaignTable()->deleteCrmCampaign($dataParam);
            $this->flashMessenger()->addMessage($this->_config['campaign_messages']['config']['search_campaign']['CAMP_DELETE']);
            $messages = array('status' => "success");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This action is used to show view campaign detail on ajax call
     * @return     array
     * @author Icreon Tech - DT
     */
    public function viewCampaignDetailAction() {
        $this->checkUserAuthentication();
        $this->getCampaignTable();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $searchParam['campaign_id'] = $this->decrypt($params['id']);
        $searchResult = $this->getCampaignTable()->searchCampaignDetails($searchParam);
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'campaign_id' => $params['id'],
            'moduleName' => $this->encrypt('campaign'),
            'searchResult' => $searchResult[0],
            'jsLangTranslate' => $this->_config['campaign_messages']['config']['view_campaign']
        ));
        return $viewModel;
    }

    /**
     * This action is used to get edit campaign detail page
     * @return string
     * @param void
     * @author Icreon Tech - DT
     */
    public function editCampaignDetailAction() {
        $this->checkUserAuthentication();
        $this->getCampaignTable();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $viewModel = new ViewModel();
        $campaign = new Campaign($this->_adapter);
        $response = $this->getResponse();
        $createCampaignForm = new CreateCampaignForm();
        $channel_campaign_form = new ChannelCampaignForm();
        $createCampaignForm->add($channel_campaign_form);
        $createCampaignForm->get('savebutton')->setAttribute('value', 'Update');
        $groupResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getGroups();
        $group_list = array();
        foreach ($groupResult as $key => $val) {
            $group_list[$val['group_id']] = stripslashes($val['name']);
        }
        $createCampaignForm->get('group_from')->setAttribute('options', $group_list);
        $campaign_image = '';
        if (isset($params['id']) && !empty($params['id'])) {
            /*
             * For editing a campaign
             */
            $searchParam['campaign_id'] = $this->decrypt($params['id']);
            $searchResult = $this->getCampaignTable()->searchCampaignDetails($searchParam);
            $channelIds = '';
            if (isset($searchResult[0]['channel_id']) && $searchResult[0]['channel_id'] != '') {
                $channelIds = $searchResult[0]['channel_id'];
            }
            $channelResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getChannelType(array('channel_ids' => $channelIds));

            $channel_list = array();
            $channelDesc = array();
            $channel_list[''] = 'Select';
            foreach ($channelResult as $key => $val) {
                $channel_list[$val['channel_id']] = stripslashes($val['channel']);
                $channelDesc[$val['channel_id']] = stripslashes($val['description']);
            }
            $channel_campaign_form->get('channel[]')->setAttribute('options', $channel_list);
            if (!empty($searchResult[0]['channel_id']) && isset($searchResult[0]['channel_id'])) {
                $channelId = (explode(',', $searchResult[0]['channel_id']));
                $channelCost = (explode(',', $searchResult[0]['channel_cost']));
                $couponCode = (explode(',', $searchResult[0]['coupon_code']));
                $revenueReceived = (explode(',', $searchResult[0]['channel_received']));
                $channelCampId = (explode(',', $searchResult[0]['channel_campaign_id']));
            } else {
                $channelId = NULL;
                $channelCost = NULL;
                $couponCode = NULL;
                $revenueReceived = NULL;
                $channelCampId = NULL;
            }
            $campaignResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCampaignType($searchResult[0]['campaign_type_id']);
            $campaignList = array();
            $campTypeDesc = array();
            $campaignList[''] = 'Select';
            foreach ($campaignResult as $key => $val) {
                $campaignList[$val['campaign_type_id']] = stripslashes($val['campaign_type']);
                $campTypeDesc[$val['campaign_type_id']] = stripslashes($val['type_description']);
            }
            $createCampaignForm->get('campaign_type')->setAttribute('options', $campaignList);

            $campaignStatusResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCampaignStatus($searchResult[0]['campaign_type_id']);
            $campaignStatus = array();
            $campStatDesc = array();
            $campaignStatus[''] = 'Select';
            foreach ($campaignStatusResult as $key => $val) {
                $campaignStatus[$val['campaign_status_id']] = stripslashes($val['campaign_status']);
                $campStatDesc[$val['campaign_status_id']] = stripslashes($val['status_description']);
            }
            $createCampaignForm->get('campaign_status')->setAttribute('options', $campaignStatus);
//For image 
            if ($searchResult[0]['campaign_image'] != '') {
                $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration

                $campaign_image = file_exists($upload_file_path['assets_upload_dir'] . "campaign/thumbnail/" . $searchResult[0]['campaign_image']) ? $upload_file_path['assets_url'] . "campaign/thumbnail/" . $searchResult[0]['campaign_image'] : '';
            }
//End For image
            $createCampaignForm->get('campaign_id')->setAttribute('value', $searchParam['campaign_id']);

            $start_date_arr = $this->OutputDateFormat($searchResult[0]['campaign_start_date'], 'dateformatampm');
            $start_date_arr = explode(' ', $start_date_arr);
            $searchResult[0]['start_date'] = str_replace('-', '/', $start_date_arr[0]);
            $searchResult[0]['start_time'] = $start_date_arr[1] . " " . $start_date_arr[2];

            if (isset($searchResult[0]['start_date']) && !empty($searchResult[0]['start_date'])) {
                $createCampaignForm->get('start_date')->setAttribute('value', $searchResult[0]['start_date']);
            }
            if (isset($searchResult[0]['start_time']) && !empty($searchResult[0]['start_time'])) {
                $createCampaignForm->get('start_time')->setAttribute('value', $searchResult[0]['start_time']);
            }
            $end_date_arr = $this->OutputDateFormat($searchResult[0]['campaign_end_date'], 'dateformatampm');
            $end_date_arr = explode(' ', $end_date_arr);
            $searchResult[0]['end_date'] = str_replace('-', '/', $end_date_arr[0]);
            $searchResult[0]['end_time'] = $end_date_arr[1] . " " . $end_date_arr[2];
            if (isset($searchResult[0]['end_date']) && !empty($searchResult[0]['end_date'])) {
                $createCampaignForm->get('end_date')->setAttribute('value', $searchResult[0]['end_date']);
            }
            if (isset($searchResult[0]['end_time']) && !empty($searchResult[0]['end_time'])) {
                $createCampaignForm->get('end_time')->setAttribute('value', $searchResult[0]['end_time']);
            }
            $group_to_id = explode(',', $searchResult[0]['group_id']);
            $group_to = explode(',', $searchResult[0]['group_name']);
            $group_list_to = array();

            for ($i = 0; $i < count($group_to_id); $i++) {
                $group_list_to[$group_to_id[$i]] = stripslashes($group_to[$i]);
            }
            $group_list_from = array_diff($group_list, $group_list_to);
            $createCampaignForm->get('group_to')->setAttribute('options', $group_list_to);
            $createCampaignForm->get('group_from')->setAttribute('options', $group_list_from);
            if (isset($searchResult[0]['campaign_title']) && !empty($searchResult[0]['campaign_title'])) {
                $createCampaignForm->get('title')->setAttribute('value', $searchResult[0]['campaign_title']);
            }
            if (isset($searchResult[0]['campaign_title']) && !empty($searchResult[0]['campaign_title'])) {
                $createCampaignForm->get('title')->setAttribute('value', $searchResult[0]['campaign_title']);
            }
            if (isset($searchResult[0]['campaign_type_id']) && !empty($searchResult[0]['campaign_type_id'])) {
                $createCampaignForm->get('campaign_type')->setAttribute('value', $searchResult[0]['campaign_type_id']);
            }
            if (isset($searchResult[0]['campaign_type']) && !empty($searchResult[0]['campaign_type'])) {
                $createCampaignForm->get('campaign_type')->setAttribute('select', $searchResult[0]['campaign_type']);
            }
            if (isset($searchResult[0]['campaign_description']) && !empty($searchResult[0]['campaign_description'])) {
                $createCampaignForm->get('description')->setAttribute('value', $searchResult[0]['campaign_description']);
            }
            if (isset($searchResult[0]['campaign_status_id']) && !empty($searchResult[0]['campaign_status_id'])) {
                $createCampaignForm->get('campaign_status')->setAttribute('value', $searchResult[0]['campaign_status_id']);
            }
            if (isset($searchResult[0]['campaign_goal']) && !empty($searchResult[0]['campaign_goal'])) {
                $createCampaignForm->get('campaign_goal')->setAttribute('value', $searchResult[0]['campaign_goal']);
            }
            if (isset($searchResult[0]['campaign_revenue_goal']) && !empty($searchResult[0]['campaign_revenue_goal'])) {
                $createCampaignForm->get('revenue_goal')->setAttribute('value', $searchResult[0]['campaign_revenue_goal']);
            }
//            if (isset($searchResult[0]['channel_received']) && !empty($searchResult[0]['channel_received'])) {
//                $searchResult[0]['channel_received'];
//            }
//            if (isset($searchResult[0]['campaign_coupon_code']) && !empty($searchResult[0]['campaign_coupon_code'])) {
//                $createCampaignForm->get('coupon_code')->setAttribute('value', $searchResult[0]['campaign_coupon_code']);
//            }
//            if (isset($searchResult[0]['campaign_gl_code']) && !empty($searchResult[0]['campaign_gl_code'])) {
//                $createCampaignForm->get('gl_code')->setAttribute('value', $searchResult[0]['campaign_gl_code']);
//            }
            if (isset($searchResult[0]['campaign_designation']) && !empty($searchResult[0]['campaign_designation'])) {
                $createCampaignForm->get('designation')->setAttribute('value', $searchResult[0]['campaign_designation']);
            }
            if (isset($searchResult[0]['campaign_department']) && !empty($searchResult[0]['campaign_department'])) {
                $createCampaignForm->get('department')->setAttribute('value', $searchResult[0]['campaign_department']);
            }
            if (isset($searchResult[0]['is_active']) && !empty($searchResult[0]['is_active'])) {
                $createCampaignForm->get('is_active')->setAttribute('value', $searchResult[0]['is_active']);
            }
            if (isset($searchResult[0]['is_global']) && !empty($searchResult[0]['is_global'])) {
                $createCampaignForm->get('is_global')->setAttribute('value', $searchResult[0]['is_global']);
            }
            if (isset($searchResult[0]['is_restricted']) && !empty($searchResult[0]['is_restricted'])) {
                $createCampaignForm->get('restricted')->setAttribute('value', $searchResult[0]['is_restricted']);
            }
            if (isset($searchResult[0]['program']) && !empty($searchResult[0]['program'])) {
                $createCampaignForm->get('program')->setAttribute('value', $searchResult[0]['program']);
            }
            if (isset($searchResult[0]['program_id']) && !empty($searchResult[0]['program_id'])) {
                $createCampaignForm->get('program_id')->setAttribute('value', $searchResult[0]['program_id']);
            }
            if (isset($searchResult[0]['is_user_added']) && !empty($searchResult[0]['is_user_added']) && $searchResult[0]['is_user_added'] == 1) {
//$createCampaignForm->get('group_from')->setAttribute('disabled', 'disabled');
//$createCampaignForm->get('group_to')->setAttribute('disabled', 'disabled');
                $createCampaignForm->get('addbutton')->setAttribute('disabled', 'disabled');
                $createCampaignForm->get('removebutton')->setAttribute('disabled', 'disabled');
            }
			if (isset($searchResult[0]['project_name']) && !empty($searchResult[0]['project_name'])) {
                $createCampaignForm->get('project')->setAttribute('value', $searchResult[0]['project_name']);
            }
				
            if (isset($searchResult[0]['project_id']) && !empty($searchResult[0]['project_id'])) {
                $createCampaignForm->get('project_id')->setAttribute('value', $searchResult[0]['project_id']);
            }
        }
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'create_campaign_form' => $createCampaignForm,
            'campaign_image' => $campaign_image,
            'channelId' => $channelId,
            'channelCost' => $channelCost,
            'couponCode' => $couponCode,
            'revenueReceived' => $revenueReceived,
            'channelCampId' => $channelCampId,
            'campStatDesc' => $campStatDesc,
            'campTypeDesc' => $campTypeDesc,
            'channelDesc' => $channelDesc,
            'channel_list' => $channel_list,
            'jsLangTranslate' => array_merge($this->_config['campaign_messages']['config']['common'], $this->_config['campaign_messages']['config']['create_campaign'], $this->_config['campaign_messages']['config']['view_campaign'])
        ));
        return $viewModel;
    }

    /**
     * This action is used to upload file for campaign
     * @param void
     * @return json
     * @author Icreon Tech - DT
     */
    public function uploadCampaignThumbnailAction() {
        $fileExt = $this->GetFileExt($_FILES['thumbnail']['name']);
        $filename = $this->GenerateMediaFileName($fileExt[1]);    //Generate file name
        $_FILES['thumbnail']['name'] = $filename;                 // assign name to file variable
        $this->getCampaignTable();
        $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration
        $error_messages = array(
            'post_max_size' => 'The uploaded file exceeds the post_max_size directive in php.ini', // Set error messages
            'max_file_size' => 'File is too big',
            'min_file_size' => 'File is too small',
            'accept_file_types' => 'Filetype not allowed',
        );
        $options = array(
            'upload_dir' => $upload_file_path['temp_upload_dir'],
            'param_name' => 'thumbnail', //file input name                      // Set configuration
            'inline_file_types' => $upload_file_path['file_types_allowed'],
            'accept_file_types' => $upload_file_path['file_types_allowed'],
            'max_file_size' => $upload_file_path['max_allowed_file_size'],
            'min_file_size' => $upload_file_path['min_allowed_file_size']
        );
        $upload_handler = new UploadHandler($options, true, $error_messages);
        $file_response = $upload_handler->jsonResponceData;
        if (isset($file_response['thumbnail'][0]->error)) {
            $messages = array('status' => "error", 'message' => $file_response['thumbnail'][0]->error);
        } else {
            $messages = array('status' => "success", 'message' => 'Image uploaded', 'filename' => $file_response['thumbnail'][0]->name);
        }
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This action is used to move file from temp to campaign folder
     * @param 1 for add , 2 for edit
     * @return json
     * @author Icreon Tech - DT
     */
    public function moveFileFromTempToCampaign($filename, $operation = 1, $oldfilename = '') {
        $this->getCampaignTable();
        $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration
        $temp_dir = $upload_file_path['temp_upload_dir'];
        $temp_thumbnail_dir = $upload_file_path['temp_upload_thumbnail_dir'];
        $campaign_dir = $upload_file_path['assets_upload_dir'] . "campaign/";
        $campaign_thumbnail_dir = $upload_file_path['assets_upload_dir'] . "campaign/thumbnail/";
        $temp_file = $temp_dir . $filename;

        $temp_thumbnail_file = $temp_thumbnail_dir . $filename;
        $campaign_filename = $campaign_dir . $filename;
        $campaign_thumbnail_filename = $campaign_thumbnail_dir . $filename;
        if (!empty($filename) && isset($filename)) {
            if (file_exists($temp_file)) {
                if (copy($temp_file, $campaign_filename)) {
                    unlink($temp_file);
                }
            }
            if (file_exists($temp_thumbnail_file)) {
                if (copy($temp_thumbnail_file, $campaign_thumbnail_filename)) {
                    unlink($temp_thumbnail_file);
                }
            }
            if ($operation == 2) {
                if ($oldfilename != $filename) {
                    $campaign_old_filename = $campaign_dir . $oldfilename;
                    $campaign_old_thumbnail_filename = $campaign_thumbnail_dir . $oldfilename;
                    if ($oldfilename != '') {
                        if (file_exists($campaign_old_filename)) {
                            unlink($campaign_old_filename);
                        }
                        if (file_exists($campaign_old_thumbnail_filename)) {
                            unlink($campaign_old_thumbnail_filename);
                        }
                    }
                }
            }
        }
    }

    /**
     * This action is used for showing channels associated with a campaign
     * @param $param
     * @return $viewModel 
     * @author Icreon Tech - AS
     */
    public function showChannelsCodesAction() {
        $this->checkUserAuthentication();
        $this->layout('popup');
        $this->getCampaignTable();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $viewModel = new ViewModel();
        $response = $this->getResponse();
        $searchResult = $this->getCampaignTable()->getChannelDetails($params['id']);
        $viewModel->setVariables(array(
            'searchResult' => $searchResult,
            'jsLangTranslate' => array_merge($this->_config['campaign_messages']['config']['common'], $this->_config['campaign_messages']['config']['create_campaign'], $this->_config['campaign_messages']['config']['search_campaign'])
        ));
        return $viewModel;
    }

    /**
     * This action is used for showing importing bad recipients list
     * @param $param
     * @return $viewModel 
     * @author Icreon Tech - AS
     */
    public function updateUserBadRecipientAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getCampaignTable();
        $badRecipientForm = new BadRecipientForm();
        $viewModel = new ViewModel();
        $msg = array_merge($this->_config['campaign_messages']['config']['common'], $this->_config['campaign_messages']['config']['create_campaign'], $this->_config['campaign_messages']['config']['search_campaign']);
        $viewModel->setVariables(array(
            'badRecipientForm' => $badRecipientForm,
            'jsLangTranslate' => $msg
        ));
        return $viewModel;
    }

    /**
     * Function  to add user in campaigns
     * @param $param
     * @return $viewModel 
     * @author Icreon Tech - AS
     */
    public function addCampaignUsersAction() {
        $this->checkUserAuthentication();
        $this->getCampaignTable();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $campaignId = $request->getPost();
        if (isset($campaignId->ids) && !empty($campaignId->ids)) {
            foreach ($campaignId->ids as $key) {
                $postUserData = array();
                $postUserData['campaign_id'] = $key;
                $groupDetail = $this->getCampaignTable()->getGroupByCampaign(array('campaign_id' => $key));
                $group = new Group($this->_adapter);
                if (!empty($groupDetail)) {
                    foreach ($groupDetail as $val) {
                        if ($val['group_type'] == 2) {
                            parse_str($val['search_query'], $searchParam);
							
							$searchParam['transaction_amount_date_from'] = (isset($searchParam['transaction_amount_date_from']) and trim($searchParam['transaction_amount_date_from']) != "") ? $this->DateFormat($searchParam['transaction_amount_date_from'], 'db_date_format_from') : "";
								
							$searchParam['transaction_amount_date_to'] = (isset($searchParam['transaction_amount_date_to']) and trim($searchParam['transaction_amount_date_to']) != "") ? $this->DateFormat($searchParam['transaction_amount_date_to'], 'db_date_format_to') : ""; 

							$searchParam['userId_exclude'] = '';
							$excludedContact = $this->getServiceLocator()->get('Group\Model\GroupTable')->getExcludedContactInGroup(array('groupId' => $val['group_id']));
							if (!empty($excludedContact)) {
								$userIds = array();
								foreach ($excludedContact as $val) {
									$userIds[] = $val['user_id'];
								}
								$searchParam['userId_exclude'] = implode(",", $userIds);
							}
                            
							$searchParam['startIndex'] = '';
                            $searchParam['recordLimit'] = '';
                            $searchParam['sortField'] = '';
                            $searchParam['sortOrder'] = '';
                            $groupContactSearchArray = $group->getArrayForSearchCotactsGroup($searchParam);
                            $seachResult = $this->getServiceLocator()->get('Group\Model\GroupTable')->getContactSearch($groupContactSearchArray);
                            if (!empty($seachResult)) {
                                foreach ($seachResult as $k=>$grpUsers) {
									$users = (array)$grpUsers;
                                    $postUserData['added_date'] = DATE_TIME_FORMAT;
                                    $postUserData['user_id'] = $users['user_id'];
                                    $postUserData['group_type'] = 2;
                                    $postUserData['group_id'] = $val['group_id'];
                                    $this->getCampaignTable()->addCampaignUser($postUserData);
                                }
                            }
                        } else {
                            $postUserData['added_date'] = DATE_TIME_FORMAT;
                            $postUserData['group_type'] = 1;
                            $postUserData['user_id'] = null;
                            $postUserData['group_id'] = null;
                            $this->getCampaignTable()->addCampaignUser($postUserData);
                        }
                    }
                }
            }
            $messages = array('status' => "success", 'message' => $this->_config['campaign_messages']['config']['search_campaign']['CONTACTS_ADDED_MSG']);
            $this->flashMessenger()->addMessage($this->_config['campaign_messages']['config']['search_campaign']['CONTACTS_ADDED_MSG']);
        } else {
            $messages = array('status' => "success", 'message' => $this->_config['campaign_messages']['config']['search_campaign']['ERROR_CONTACTS_ADDED_MSG']);
            $this->flashMessenger()->addMessage($this->_config['campaign_messages']['config']['search_campaign']['ERROR_CONTACTS_ADDED_MSG']);
        }
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This Action is used to import bad recepient
     * @param $file
     * @return $response
     * @author Icreon Tech-AS
     */
    public function importBadRecipientAction() {
        $this->checkUserAuthentication();
        $this->getCampaignTable();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $fileExt = $this->GetFileExt($_FILES['bad_recipient']['name']);
        if ($fileExt[1] != 'xls') {
            $messages = array('status' => "error", 'message' => $this->_config['campaign_messages']['config']['common']['CHECK_EXCEL']);
        } else {
            $import_handler = new SpreadsheetExcelReader($_FILES['bad_recipient']['tmp_name']);
            $imported_data = $import_handler->dumpToArray();
            $data_param = array();
            if (!empty($imported_data)) {
                $flag = 0;
                foreach ($imported_data as $key => $val) {
                    if ($key > 0) {
                        $postUserData['campaignCode'] = substr($val[1], strrpos($val[1], '.') + 1);
                        $postUserData['userEmail'] = $val[2];
                        $result = $this->getCampaignTable()->updateBadRecipientStatus($postUserData);
                        if ($result == false) {
                            $flag = 1;
                        }
                    }
                }
            }
            if ($flag == 0) {
                $messages = array('status' => "success", 'message' => $this->_config['campaign_messages']['config']['common']['EXCEL_IMPORTED']);
            } else if ($flag == 1) {
                $messages = array('status' => "error", 'message' => $this->_config['campaign_messages']['config']['common']['EXCEL_NOT_IMPORTED']);
            }
        }
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This Action is used to import bad recepient
     * @param $param
     * @return $viewModel 
     * @author Icreon Tech-AS
     */
    public function getUserBadRecipientAction() {
        $this->checkUserAuthentication();
        $this->getCampaignTable();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $searchParam['campaign_id'] = $this->decrypt($params['id']);
        //$searchResult = $this->getCampaignTable()->getBadRecipientUsers($searchParam);
        //asd($searchResult);
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'campaignId' => $searchParam['campaign_id'],
            'jsLangTranslate' => $this->_config['campaign_messages']['config']['view_campaign']
        ));
        return $viewModel;
    }

    /**
     * This Action is used to show bad recepient list
     * @param $param
     * @return $viewModel 
     * @author Icreon Tech-AS
     */
    public function listBadRecepientAction() {
        $this->checkUserAuthentication();
        $this->getCampaignTable();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $this->getCampaignTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $msg = $this->_config['campaign_messages']['config']['common'];
        parse_str($request->getPost('searchString'), $searchParam);
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $msg = $this->_config['campaign_messages']['config']['common'];
        $start = $limit * $page - $limit;
        $searchParam['startIndex'] = $start;
        $searchParam['recordLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');
        $searchParam['campaign_id'] = $this->decrypt($params['id']);
        $searchResult = $this->getCampaignTable()->getBadRecipientUsers($searchParam);
        $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
        $jsonResult = array();
        $arrCell = array();
        $subArrCell = array();
        $jsonResult['page'] = $page;
        $jsonResult['records'] = $countResult[0]->RecordCount;
        $jsonResult['total'] = @ceil($countResult[0]->RecordCount / $limit);
        if (count($searchResult) > 0) {
            foreach ($searchResult as $val) {
                $arrCell['id'] = $val['user_id'];
                $view = "<a class = 'view-icon' href = '/view-contact/" . $this->encrypt($val['user_id']) . "/" . $this->encrypt('view') . "'><div class = 'tooltip'>" . $msg['L_VIEW'] . "<span></span></div></a>";
                $arrCell['cell'] = array(stripslashes($val['first_name']), stripslashes($val['email_id']), $view);
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
        }
        return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    /**
     * This Action is used to show campaign touch point
     * @param $param
     * @return $viewModel 
     * @author Icreon Tech-DG
     */
    public function getCampaignTouchPointsAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getCampaignTable();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $viewModel = new ViewModel();
        $response = $this->getResponse();
        $viewModel->setTerminal(true);
        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $messages = array();

            $searchParam = array();
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit;
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'program.program') ? 'program.program' : $searchParam['sort_field'];
            $searchParam['campaign_id'] = $this->decrypt($params['campaign_id']);

            $touchPointsArr = $this->getCampaignTable()->getTouchPoints($searchParam);
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();

            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $jsonResult['page'] = $request->getPost('page');
            $jsonResult['records'] = $countResult[0]->RecordCount;
            $jsonResult['total'] = @ceil($countResult[0]->RecordCount / $limit);
            if (!empty($touchPointsArr)) {
                foreach ($touchPointsArr as $val) {
                    $numResponded = 0;
                    $action = '';
                    $numResponded = $this->getCampaignTable()->getRespondedContact($val['campaign_id']);
                    $arrCell['id'] = $val['touch_point_id'];
                    if ($val['num_sent'] > 0)
                        $action = '<a href="/export-campaign-contact/' . $this->encrypt($val['touch_point_id']) . '/' . $this->encrypt($val['campaign_id']) . '/' . $this->encrypt($val['touch_point_date']) .'" onClick=showAjxLoader();>Download</a> || <a href="/upload-campaign-touch-points/' . $this->encrypt($val['touch_point_id']) . '/' . $this->encrypt($val['campaign_id']) . '">Upload</a>';
                    $arrCell['cell'] = array($val['touch_point_id'], $val['campaign_id'], $val['touch_point'], $this->OutputDateFormat($val['touch_point_date'], 'dateformatampm'), $val['num_sent'], $numResponded, $val['num_opened'], $val['num_bounced'], $val['num_clicked'], $action, '');
                    $subArrCell[] = $arrCell;
                    $jsonResult['rows'] = $subArrCell;
                }
            }
            $response->setContent(\Zend\Json\Json::encode($jsonResult));
            return $response;
        } else {
            $viewModel->setVariables(array(
                'campaignId' => $params['campaign_id'],
                'jsLangTranslate' => array_merge($this->_config['campaign_messages']['config']['campaign_touch_point'])
            ));
            return $viewModel;
        }
    }

    /**
     * This Action is used to upload touch point
     * @param $param
     * @return $viewModel 
     * @author Icreon Tech-DG
     */
    public function uploadCampaignTouchPointsAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getCampaignTable();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $viewModel = new ViewModel();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $fileExt = $this->GetFileExt($_FILES['file_name']['name']);
            if ($fileExt[1] != 'xls') {
                $messages = array('status' => "error", 'message' => $this->_config['campaign_messages']['config']['campaign_touch_point']['INVALID_EXCEL_EXTENSION']);
            } else {
                $import_handler = new SpreadsheetExcelReader($_FILES['file_name']['tmp_name']);
                $imported_data = $import_handler->dumpToArray();
                $data_param = array();
                if (!empty($imported_data)) {
                    $dataArr = array();
                    $dataArr['touch_id'] = $this->decrypt($params['touch_id']);
                    $dataArr['open_rate'] = $imported_data[1][2];
                    $dataArr['bounce_rate'] = $imported_data[2][2];
                    $dataArr['clicked_rate'] = $imported_data[3][2];
                    $dataArr['campaign_id'] = $this->decrypt($params['campaign_id']);
                    $dataArr['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                    $dataArr['modified_date'] = DATE_TIME_FORMAT;
                    $this->getCampaignTable()->updateTouchPoint($dataArr);
                    $messages = array('status' => "success", 'message' => $this->_config['campaign_messages']['config']['campaign_touch_point']['IMPORT_SUCCESS']);
                } else {
                    $messages = array('status' => "error", 'message' => $this->_config['campaign_messages']['config']['campaign_touch_point']['INVALID_DATA']);
                }
            }
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
        $viewModel->setVariables(array(
            'touchId' => $params['touch_id'],
            'campaignId' => $params['campaign_id'],
            'jsLangTranslate' => $this->_config['campaign_messages']['config']['campaign_touch_point'],
                )
        );

        return $viewModel;
    }

    /**
     * This Action is used to edit touch point
     * @param $param
     * @return $viewModel 
     * @author Icreon Tech-DG
     */
    public function editCampaignTouchPointsAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getCampaignTable();
        if ($request->isPost()) {
            $postData = array();
            $formData = $request->getPost();
            $postData['touch_id'] = $formData['id'];
            $postData['open_rate'] = $formData['Open_Rate'];
            $postData['bounce_rate'] = $formData['Bounced_Rate'];
            $postData['clicked_rate'] = $formData['Click_Rate'];
            $postData['campaign_id'] = $formData['CAMPAIGN_ID'];
            $postData['modified_by'] = $this->auth->getIdentity()->crm_user_id;
            $postData['modified_date'] = DATE_TIME_FORMAT;
            $this->getCampaignTable()->updateTouchPoint($postData);
            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This function is used to add touch point
     * @param $param
     * @author Icreon Tech-DG
     */
    public function addTouchPointAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getCampaignTable();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $viewModel = new ViewModel();
        $response = $this->getResponse();
        $viewModel->setTerminal(true);
        if ($request->isXmlHttpRequest() && $request->isPost()) {
            /*
              $postData['campaign_id'] = $this->decrypt($params['campaign_id']);
              $getContactToSent = $this->getCampaignTable()->getContactToSent($postData);

              $postData['num_sent'] = '0';
              if (!empty($getContactToSent)) {
              $postData['num_sent'] = count($getContactToSent);
              }
              $postData['modified_by'] = $this->auth->getIdentity()->crm_user_id;
              $touchPointId = $this->getCampaignTable()->insertTouchPoint($postData);
              $addSentContact['touch_point_id'] = $touchPointId;
              foreach ($getContactToSent as $val) {
              $addSentContact['user_id'] = $val['user_id'];
              $this->getCampaignTable()->addTouchPointContact($addSentContact);
              }
             */
            $postData['campaign_id'] = $this->decrypt($params['campaign_id']);
            $getContactToSent = $this->getCampaignTable()->getContactToSent($postData);
            if ($getContactToSent[0]['totalRecord'] > 0) {
                $postData = array();
                $postData['campaign_id'] = $this->decrypt($params['campaign_id']);
                $postData['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                $touchPointId = $this->getCampaignTable()->insertTouchPointCampaign($postData);

                //print_r($postData);

                $addSentContact = array();
                $addSentContact['campaign_id'] = $this->decrypt($params['campaign_id']);
                $addSentContact['touch_point_id'] = $touchPointId;

                // print_r($addSentContact);
                $getContactToSent = $this->getCampaignTable()->getSelectInsertTouchPointContact($addSentContact);
                $messages = array('status' => "success");
            } else {
                $messages = array('status' => "error", "msg" => "No contacts available");
            }
        }

        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This function is used to get all campaign conatct in excel sheet
     * @param $param
     * @author Icreon Tech-DG
     */
    public function exportCampaignContactAction() {
        ini_set('max_execution_time', 0); // Set max execution time explicitly.
        $this->checkUserAuthentication();
        $this->getCampaignTable();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();

        $viewModel = new ViewModel();
        $response = $this->getResponse();
        $viewModel->setTerminal(true);
        $postData['touch_id'] = $this->decrypt($params['touch_id']);
        $touchPointDate = $this->decrypt($params['touch_date']);
        $campaignId = $this->decrypt($params['campaign_id']);
        
        $campaignDetail = $this->getCampaignTable()->getCampaign(array('campaign_id' => $campaignId));
        $fileName = "";

        $fileRecordCounter = 0;
        $fileCounter = 1;
        if (isset($campaignDetail[0]['campaign_code']))
        {
            $fileName .= $campaignDetail[0]['campaign_code'] . "_";
        }
        if (isset($postData['touch_id']))
        {
            $fileName .= "TP" . $postData['touch_id'] . "_";
        }
        $fileName .= $touchPointDate;
        $fileName = str_replace(array(" ",":"), "_", $fileName);

        //number of records per file
        $fileLimit = $this->_config['csv_file']['max_records'];

        //temp folder where file will be created
        $tempfilePath = $this->_config['file_upload_path']['temp_data_upload_dir'] . 'touchpoint/';

        //path to create final zip file
        $zipFilePath = $this->_config['file_upload_path']['assets_upload_dir'] . 'touchpoint/';
        //zip file name
        $zipFileName = $fileName . ".zip";
        //if file already exist then no need to create it again
        if (!file_exists($zipFilePath . $zipFileName))
        {
            $getContactToSentCount = $this->getCampaignTable()->getTouchPointContactCount($postData);
            //echo "<br/>Total records:".$getContactToSentCount;
            if ($getContactToSentCount > 0)
            {
                //header for csv file
                $header = array("Campaign Code", "Title", "Contact ID", "Contact Name", "Email Address", "Phone Number", "Address1", "Address2", "City", "State", "Country", "Postal Code", "OMX ID");
                $zip = new ZipArchive();
                if ($zip->open($zipFilePath . $zipFileName, ZIPARCHIVE::CREATE) !== TRUE)//create new zip file
                {
                    die("Could not open zip");
                }
                 //starting name for all csv file 
                $fileName .= "_";

                $fp = fopen($tempfilePath . $fileName . $fileCounter . ".csv", 'w'); //create first csv file
                fputcsv($fp, $header); //write header row into csv file
                $rec_limit = $this->_config['csv_file']['db_limits'];
                $noOfPages = ceil($getContactToSentCount / $rec_limit);
                //echo "<br/>Total hits to db 10000 each:".$noOfPages;
                for ($i = 0; $i < $noOfPages; $i++)
                {
                    if($i > 0)
                    {
                        $offset = ($rec_limit * $i) + 1;
                    }
                    else
                    {
                        $offset = 0;
                    }
                    $postData['rec_limit'] = $rec_limit;
                    $postData['offset'] = $offset;
                    //echo "<br/>post data to fetch records:";print_r($postData);
                    
                    $getContactToSent = $this->getCampaignTable()->getTouchPointContact($postData);
                    //echo "<br/>no of data in result set:".count($getContactToSent);
                    //echo "<pre>";print_r($getContactToSent);
                    
                    if (!empty($getContactToSent))
                    {
                        if (count($getContactToSent) > 0)
                        {
                            foreach ($getContactToSent as $val)
                            {
                                //echo "<br/>---record counter".$fileRecordCounter;
                                //check if record count is greater then file limit
                               
                                if (($fileRecordCounter % $fileLimit) == 0 && $fileRecordCounter !=0)
                                {
                                    //echo "<br/>---closing old one and creating new csv file";
                                    $zip->addFile($tempfilePath . $fileName . $fileCounter . '.csv', $fileName . $fileCounter . '.csv') or die("ERROR: Could not add file: " . $fileName . $fileCounter . '.csv');
                                    fclose($fp); //close file
                                    $fileCounter = $fileCounter + 1; //increase counter to create new file
                                    $fp = fopen($tempfilePath . $fileName . $fileCounter . '.csv', 'w'); //open new file
                                    fputcsv($fp, $header); //write header row into csv file
                                }
                                //filter records
                                $phoneNo = array();
                                if (isset($val['country_code']))
                                {
                                    $phoneNo[] = $val['country_code'];
                                }
                                if (isset($val['area_code']))
                                {
                                    $phoneNo[] = $val['area_code'];
                                }
                                if (isset($val['phone_number']))
                                {
                                    $phoneNo[] = $val['phone_number'];
                                }
                                if (isset($val['extension']))
                                {
                                    $phoneNo[] = $val['extension'];
                                }
                                if (!empty($phoneNo))
                                {
                                    $phoneNUmber = trim(implode("-", $phoneNo),"-");
                                }
                                else
                                {
                                    $phoneNUmber = '';
                                }

                                $contactId = ($val['contact_id'] != '' && !is_null($val['contact_id'])) ? '"' . $val['contact_id'] . '"' : '';
                                //create array of records
                                $val['full_name']= preg_replace('/\s+/', ' ', $val['full_name']);
                               
                                $dataArr = array($campaignDetail[0]['campaign_code'], $val['title'], $contactId, $val['full_name'], trim($val['email_id'], '^'), $phoneNUmber, $val['street_address_one'], $val['street_address_two'], $val['city'], $val['state'], $val['country_name'], $val['zip_code'], $val['omx_customer_number']);
                                 //echo "<br/>---writing record to csv file";
                                $isWritten =fputcsv($fp, $dataArr); //write data row into csv file
                                $fileRecordCounter = $fileRecordCounter + 1; //increase counter of records fetched from db
                            }
                        }
                    }
                    else
                    {
                        echo "No data found";
                    }
                }
                //add current file into zip file
                $zip->addFile($tempfilePath . $fileName . $fileCounter . '.csv', $fileName . $fileCounter . '.csv') or die("ERROR: Could not add file: " . $fileName . $fileCounter . '.csv');
                fclose($fp);
                $zip->close();
                //removing temp files
                $tempFiles = glob($tempfilePath . '/{,.}*', GLOB_BRACE);
                foreach ($tempFiles as $file)
                {
                    if (is_file($file))
                    {
                        unlink($file);
                    }
                }
            }
        }
        
        //download file if exist
        if (file_exists($zipFilePath . $zipFileName))
        {
            header('Content-Type: application/zip');
            header('Content-disposition: attachment; filename='.$zipFileName);
            header('Content-Length: ' . filesize($zipFilePath . $zipFileName));
            readfile($zipFilePath . $zipFileName);
        }
        else
        {
            echo "File not available to download";
        }
        die;
    }

}
