<?php

/**
 * This module file is used for initailize all obj of model and configuration setting
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace Kiosk;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\I18n\Translator\Translator;
use Zend\Validator\AbstractValidator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Kiosk\Model\KioskManagement;
use Kiosk\Model\KioskManagementTable;
use Kiosk\Model\KioskModule;
use Kiosk\Model\KioskModuleTable;
use Kiosk\Model\KioskReservation;
use Kiosk\Model\KioskReservationTable;
use Kiosk\Model\PosManagement;
use Kiosk\Model\PosTable;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;

/**
 * This module file is used for initailize all obj of model and configuration setting
 * @package    User
 * @author     Icreon Tech - DG
 */
class Module implements AutoloaderProviderInterface, ConfigProviderInterface, ViewHelperProviderInterface {

    public function onBootstrap(MvcEvent $env) {
	   //error_reporting(E_ALL);
	
	
        $translator = $env->getApplication()->getServiceManager()->get('translator');
        $eventManager = $env->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $translator->addTranslationFile(
                'phpArray', './module/Kiosk/languages/en/language.php', 'default', 'en_US'
        );
        
        $viewModel = $env->getApplication()->getMvcEvent()->getViewModel();
        $sm = $env->getApplication()->getServiceManager();        
        $this->_config = $sm->get('Config');
        $viewModel->imageFilePath = $imageFilePath = $this->_config['img_file_path']['path'];
        //AbstractValidator::setDefaultTranslator($translator);
    }

    public function authPreDispatch($event) {
               
    }
    
    
    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Get Autoloader Configuration
     * @return array
     */
    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    /**
     * Get Service Configuration
     *
     * @return array
     */
    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Kiosk\Model\KioskManagementTable' => function($sm) {
                    $tableGateway = $sm->get('KioskManagementTableGateway');
                    $table = new KioskManagementTable($tableGateway);
                    return $table;
                },
                'KioskManagementTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new KioskManagement($dbAdapter));
                    return new TableGateway('tbl_ksk_kiosks', $dbAdapter, null, $resultSetPrototype);
                },
                'Kiosk\Model\PosTable' => function($sm) {
                    $tableGateway = $sm->get('PosTableGateway');
                    $table = new PosTable($tableGateway);
                    return $table;
                },
                'PosTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PosManagement($dbAdapter));
                    return new TableGateway('tbl_ksk_kiosks', $dbAdapter, null, $resultSetPrototype);
                },
                'Kiosk\Model\KioskModuleTable' => function($sm) {
                    $tableGateway = $sm->get('KioskModuleTableGateway');
                    $table = new KioskModuleTable($tableGateway);
                    return $table;
                },
                'KioskModuleTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new KioskModule($dbAdapter));
                    return new TableGateway('tbl_ksk_kiosk_modules', $dbAdapter, null, $resultSetPrototype);
                },
                'Kiosk\Model\KioskReservationTable' => function($sm) {
                    $tableGateway = $sm->get('KioskReservationTableGateway');
                    $table = new KioskReservationTable($tableGateway);
                    return $table;
                },
                'KioskReservationTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new KioskReservation($dbAdapter));
                    return new TableGateway('tbl_ksk_kiosks', $dbAdapter, null, $resultSetPrototype);
                },
				'KioskModules' => function ($sm) {
                   return new Service\KioskModule();
                },
                'dbAdapter' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    return $dbAdapter;
                },
            ),
        );
    }

    /**
     * Get View Helper Configuration
     *
     * @return array
     */
    public function getViewHelperConfig() {
        return array(
            'factories' => array(
            // the array key here is the name you will call the view helper by in your view scripts
//                'absoluteUrl' => function($sm) {
//                    $locator = $sm->getServiceLocator(); // $sm is the view helper manager, so we need to fetch the main service manager
//                    return new AbsoluteUrl($locator->get('Request'));
//                },
            ),
        );
    }

    /**
     * Get Controller Configuration
     *
     * @return array
     */
    public function getControllerConfig() {
        return array();
    }

    public function loadConfiguration(MvcEvent $env) {
        $application = $env->getApplication();
        $serviceManager = $application->getServiceManager();
        $sharedManager = $application->getEventManager()->getSharedManager();
        $router = $serviceManager->get('router');
        $request = $serviceManager->get('request');

//        $matchedRoute = $router->match($request);
//        if (null !== $matchedRoute) {
//            $sharedManager->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function($e) use ($serviceManager) {
//                        $serviceManager->get('ControllerPluginManager')->get('AclPlugin')
//                                ->doAuthorization($e);
//                    }, 2
//            );
//        }
    }

    public function loadCommonViewVars(MvcEvent $env) {
        $env->getViewModel()->setVariables(array(
            'auth' => $env->getApplication()->getServiceManager()->get('AuthService')
        ));
    }

}
