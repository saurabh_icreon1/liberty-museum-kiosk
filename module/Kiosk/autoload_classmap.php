<?php

/**
 * This file defines all the controller and model
 * @package    Kiosk
 * @author     Icreon Tech - DG
 */

return array(
    'Kiosk\Module' => __DIR__ . '/Module.php',
    'Kiosk\Controller\KioskController' => __DIR__ . '/src/Kiosk/Controller/KioskController.php',
    'Kiosk\Model\KioskManagement' => __DIR__ . '/src/Kiosk/Model/KioskManagement.php',
    'Kiosk\Model\KioskManagementTable' => __DIR__ . '/src/Kiosk/Model/KioskManagementTable.php'
);
?>