<?php

/**
 * This page is used for activity module configuration details.
 * @package    Activity
 * @author     Icreon Tech - DT
 */
return array(
    'controllers' => array(
        'invokables' => array(
            'Kiosk\Controller\Kiosk' => 'Kiosk\Controller\KioskController',
            'Kiosk\Controller\Kioskmodule' => 'Kiosk\Controller\KioskmoduleController',
            'Kiosk\Controller\Kioskreservation' => 'Kiosk\Controller\KioskreservationController',
			'Kiosk\Controller\Pos' => 'Kiosk\Controller\PosController'
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'user' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/kiosk[/][:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Kiosk\Controller\Kiosk',
                        'action' => 'index',
                    ),
                ),
            ),
            'kiosk' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/kiosk',
                    'defaults' => array(
                        'controller' => 'Kiosk\Controller\Kiosk',
                        'action' => 'kiosk',
                    ),
                ),
            ),
            'addKiosk' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-kiosk',
                    'defaults' => array(
                        'controller' => 'Kiosk\Controller\Kiosk',
                        'action' => 'addKiosk',
                    ),
                ),
            ),
            'getsearchkiosk' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-search-kiosk',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Kiosk',
                        'action' => 'getSearchKiosk',
                    ),
                ),
            ),
            'getKioskDetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-kiosk-detail[/:id][/:mode]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Kiosk',
                        'action' => 'getKioskDetail',
                    ),
                ),
            ),
            'editKiosk' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-kiosk[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Kiosk',
                        'action' => 'edit',
                    ),
                ),
            ),
            'deleteKiosk' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-kiosk[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Kiosk',
                        'action' => 'delete',
                    ),
                ),
            ),
             'getkioskinfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-kiosk-info[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Kiosk',
                        'action' => 'getKioskInfo',
                    ),
                ),
            ),
            
            'kioskmodule' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/kiosk-module-management',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Kioskmodule',
                        'action' => 'kioskModule',
                    ),
                ),
            ),
            'kioskSearchModule' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-search-kiosk-modules',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Kioskmodule',
                        'action' => 'getSearchKioskModule',
                    ),
                ),
            ),
            'editKioskModules' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-kiosk-module[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Kioskmodule',
                        'action' => 'editKiosk',
                    ),
                ),
            ),
           'getKioskModuleDetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-kiosk-module-detail[/:id][/:mode]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Kioskmodule',
                        'action' => 'getKioskModuleDetail',
                    ),
                ),
            ),
            'editKioskModulesDetails' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-kiosk-module-details[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Kioskmodule',
                        'action' => 'editKioskDetails',
                    ),
                ),
            ),
            'getKioskModuleInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-kiosk-modules-info[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Kioskmodule',
                        'action' => 'getKioskModulesInfo',
                    ),
                ),
            ),
             'kioskReservation' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/kiosk-reservation-system',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Kioskreservation',
                        'action' => 'kioskReservationSystem',
                    ),
                ),
            ),
             'addContactQueue' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-contact-queue',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Kioskreservation',
                        'action' => 'addContactQueue',
                    ),
                ),
            ),
             'reservationQueue' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-reservation-queue',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Kioskreservation',
                        'action' => 'reservationQueue',
                    ),
                ),
            ),
            'addKioskFeeToCart' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-to-cart-kiosk-fee',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Kioskreservation',
                        'action' => 'addKioskFeeToCart',
                    ),
                ),
            ),
            'deleteKioskFeeToCart' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-kart-product-kiosk',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Kioskreservation',
                        'action' => 'deleteKioskCart',
                    ),
                ),
            ),
            'getKioskSlotFrontend' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-kiosk-data',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Kioskreservation',
                        'action' => 'getKioskSlotFrontend',
                    ),
                ),
            ),
            'addContactQueueFrontend' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-reservation-contact',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Kioskreservation',
                        'action' => 'addContactQueueFrontend',
                    ),
                ),
            ),
            'purchaseSession' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/purchase-session[/:page_action]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Pos',
                        'action' => 'purchaseSession',
                    ),
                ),
            ), 
            'payPurchaseSession' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/pay-purchase-session[/:status]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Pos',
                        'action' => 'payPurchaseSession',
                    ),
                ),
            ),
			'purchaseSessionZipcode' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/purchase-session-zipcode',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Pos',
                        'action' => 'purchaseSessionZipcode',
                    ),
                ),
            ),            
            'payPosDonation' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/pay-pos-donation[/:status]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Pos',
                        'action' => 'payPosDonation',
                    ),
                ),
            ), 
			'PosDonationZipcode' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/pos-donation-zipcode',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Pos',
                        'action' => 'PosDonationZipcode',
                    ),
                ),
            ),
            'getSwipeData' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-swipe-data',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Pos',
                        'action' => 'getSwipeData',
                    ),
                ),
            ),                         
            'purchaseSessionConfirmation' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/purchase-session-confirmation[/:id][/:user_name]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Pos',
                        'action' => 'purchaseSessionConfirmation',
                    ),
                ),
              ),
              
            'saveSession' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/save-session',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Pos',
                        'action' => 'saveSession',
                    ),
                ),
            ),   
            'visitationCertificate' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/visitation-certificate',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Pos',
                        'action' => 'visitationCertificate',
                    ),
                ),
            ),
            'visitationConfirmation' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/visitation-confirmation[/:email_flag][/:confirmation_code][/:username][/:password]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Pos',
                        'action' => 'visitationConfirmation',
                    ),
                ),
            ),
            'downloadVisitation' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/ellisisland[/:confirmation_code]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Pos',
                        'action' => 'downloadVisitation',
                    ),
                ),
            ),    
            'posDonation' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/pos-donation[/:first_name][/:last_name][/:email_id][/:page_action]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Pos',
                        'action' => 'posDonation',
                    ),
                ),
            ),
            'posDonationConfirmation' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/pos-donation-confirmation[/:id][/:confirmation_code][/:user_name]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Pos',
                        'action' => 'posDonationConfirmation',
                    ),
                ),
            ),
            'kioskLogin' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/kiosk-login',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Kiosk',
                        'action' => 'kioskLogin',
                    ),
                ),
            ),                                    
            'kioskSearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/kiosk-search',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Kiosk',
                        'action' => 'kioskSearch',
                    ),
                ),
            ),
            'checkAvaliableKiosk' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/check-avaliable-kiosk',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Kioskreservation',
                        'action' => 'checkAvaliableKiosk',
                    ),
                ),
            ),
			'printPageContent' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/print-page-content',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Pos',
                        'action' => 'printPageContent',
                    ),
                ),
            ),  
			'checkPauseFile' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/check-pause-file',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Pos',
                        'action' => 'checkPauseFile',
                    ),
                ),
            ), 
			'kioskPauseMessage' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/kiosk-pause-info',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Pos',
                        'action' => 'kioskPauseMessage',
                    ),
                ),
            ),
			'checkKioskStatus' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/check-kiosk-status',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Pos',
                        'action' => 'checkKioskStatus',
                    ),
                ),
            ),
			'systemTurnOffessage' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/system-turnoff-info',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Pos',
                        'action' => 'systemTurnoffMessage',
                    ),
                ),
            ),
			'userAddWoh' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-add-woh',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Kiosk',
                        'action' => 'userAddWoh',
                    ),
                ),
            ),
            'wohDonation' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/woh-donation[/:first_name][/:last_name][/:email_id][/:page_action]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Pos',
                        'action' => 'wohDonation',
                    ),
                ),
            ),
            'wohDonationConfirmation' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/woh-donation-confirmation[/:id][/:confirmation_code][/:user_name]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Pos',
                        'action' => 'wohDonationConfirmation',
                    ),
                ),
            ),                                                
            'payWohDonation' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/pay-woh-donation[/:status]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Pos',
                        'action' => 'payWohDonation',
                    ),
                ),
            ),
			'wohDonationZipcode' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/woh-donation-zipcode',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Pos',
                        'action' => 'wohDonationZipcode',
                    ),
                ),
            ),
			
			'wohSpecialPromotions' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/woh-special-promotions',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Pos',
                        'action' => 'wohSpecialPromotions',
                    ),
                ),
            ),
			'wohEllisMap' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/ellis-map',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Pos',
                        'action' => 'wohEllisMap',
                    ),
                ),
            ),   
            'wohvisitationCertificate' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/woh-visitation-certificate',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Pos',
                        'action' => 'wohVisitationCertificate',
                    ),
                ),
            ),
            'wohvisitationConfirmation' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/woh-visitation-confirmation[/:email_flag][/:confirmation_code][/:username][/:password]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Pos',
                        'action' => 'wohVisitationConfirmation',
                    ),
                ),
            ),
            'kioskNextLoginSession' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/kiosk-next-login-session',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Kiosk\Controller',
                        'controller' => 'Kiosk\Controller\Kiosk',
                        'action' => 'kioskNextLoginSession',
                    ),
                ),
            ),
            ),
        ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
      'navigation' => array(
        'default' => array(
            array(
                'label' => 'Kiosk',
                'route' => 'kiosk',
                'pages' => array(
                    array(
                        'label' => 'Search',
                        'route' => 'kiosk',
                        'action' => 'kiosk',
                    ),
                    array(
                        'label' => 'Add Kiosk',
                        'route' => 'addKiosk',
                        'action' => 'addKiosk',
                    ),
                    array(
                        'label' => 'Edit',
                        'route' => 'editKiosk',
                        'action' => 'edit',
                    ),
                    array(
                        'label' => 'View ',
                        'route' => 'getkioskinfo',
                        'action' => 'getKioskInfo',
                    ),
                    array(
                        'label' => 'Change Log ',
                        'route' => 'getkiosklog',
                        'action' => 'get-change-log',
                    ),
                ),
            ),
           array(
                'label' => 'Kiosk Module',
                'route' => 'kioskmodule',
                'pages' => array(
                    array(
                        'label' => 'Search',
                        'route' => 'kioskmodule',
                        'action' => 'kioskModule',
                    ),
                     array(
                        'label' => 'Edit',
                        'route' => 'editKioskModules',
                        'action' => 'editKiosk',
                    ),
                     array(
                        'label' => 'View ',
                        'route' => 'getKioskModuleInfo',
                        'action' => 'getKioskModulesInfo',
                    ),
                    array(
                        'label' => 'Change Log ',
                        'route' => 'getKioskModuleChangeLog',
                        'action' => 'get-change-log',
                    ),
                ),
            ),
            array(
                'label' => 'Unsubscribe Users',
                'route' => 'unsubscribeusers'
            )
        ),
    ),
    'kiosk_messages' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
            'navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'kiosk' => __DIR__ . '/../view'
        ),
        'template_map' => array(
            'layout/custom' => __DIR__ . '/../../module/Common/layout/crm.phtml',
            'error/404' => __DIR__ . '/../../module/Common/error/404.phtml',
            'error/index' => __DIR__ . '/../../module/Common/error/index.phtml',
        ),
    ),
    'global_config' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
);
