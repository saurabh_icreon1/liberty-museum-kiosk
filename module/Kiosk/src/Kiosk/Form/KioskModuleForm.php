<?php

/**
 * This page is used for create crm users form
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace Kiosk\Form;

use Zend\Form\Form;

/**
 * This class is used for create crm users form elements
 * @package    User_CrmUserForm
 * @author     Icreon Tech - AP
 */
class KioskModuleForm extends Form {

    public function __construct($param = null) {
        /* we want to ignore the name passed */
        parent::__construct('kioskmodule');
        $this->setAttribute('method', 'post');
        //$this->setAttribute('enctype', 'multipart/form-data');
       $this->add(array(
            'name' => 'kiosk_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'kiosk_id',
            )
        ));
         $this->add(array(
            'name' => 'kiosk_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'kiosk_name',
                'readonly'=>true
            )
        ));
          $this->add(array(
            'name' => 'kiosk_shut_off',
            'attributes' => array(
                'type' => 'text',
                'id' => 'kiosk_shut_off',
            )
        ));
           $this->add(array(
            'name' => 'kiosk_shutoff',
            'attributes' => array(
                'type' => 'text',
                'id' => 'kiosk_shutoff',
                'readonly'=>true
            )
        ));
//        $this->add(array(
//            'type' => 'Zend\Form\Element\Radio',
//            'name' => 'module_login',
//            'options' => array(
//                'value_options' => array(
//                    '1' => 'Available',
//                    '0' => 'Not Available',
//                ),
//            ),
//            'attributes' => array(
//                'value' => '', //set checked to '1'
//                'class' => 'e3'
//            )
//        ));
//    
//        $this->add(array(
//            'type' => 'Zend\Form\Element\Radio',
//            'name' => 'module_registration',
//            'options' => array(
//                'value_options' => array(
//                    '1' => 'Available',
//                    '0' => 'Not Available',
//                ),
//            ),
//            'attributes' => array(
//                'value' => '', //set checked to '1'
//                'class' => 'e3'
//            )
//        ));
//        
//        $this->add(array(
//            'type' => 'Zend\Form\Element\Radio',
//            'name' => 'module_reservation',
//            'options' => array(
//                'value_options' => array(
//                    '1' => 'Available',
//                    '0' => 'Not Available',
//                ),
//            ),
//            'attributes' => array(
//                'value' => '', //set checked to '1'
//                'class' => 'e3'
//            )
//        ));
//         $this->add(array(
//            'type' => 'Zend\Form\Element\Radio',
//            'name' => 'module_passenger_search',
//            'options' => array(
//                'value_options' => array(
//                    '1' => 'Available',
//                    '0' => 'Not Available',
//                ),
//            ),
//            'attributes' => array(
//                'value' => '', //set checked to '1'
//                'class' => 'e3'
//            )
//        ));
//         $this->add(array(
//            'type' => 'Zend\Form\Element\Radio',
//            'name' => 'module_fun_facts',
//            'options' => array(
//                'value_options' => array(
//                    '1' => 'Available',
//                    '0' => 'Not Available',
//                ),
//            ),
//            'attributes' => array(
//                'value' => '', //set checked to '1'
//                'class' => 'e3'
//            )
//        ));
//         $this->add(array(
//            'type' => 'Zend\Form\Element\Radio',
//            'name' => 'module_fof_wall',
//            'options' => array(
//                'value_options' => array(
//                    '1' => 'Available',
//                    '0' => 'Not Available',
//                ),
//            ),
//            'attributes' => array(
//                'value' => '', //set checked to '1'
//                'class' => 'e3'
//            )
//        ));
//          $this->add(array(
//            'type' => 'Zend\Form\Element\Radio',
//            'name' => 'module_fof_search',
//            'options' => array(
//                'value_options' => array(
//                    '1' => 'Available',
//                    '0' => 'Not Available',
//                ),
//            ),
//            'attributes' => array(
//                'value' => '', //set checked to '1'
//                'class' => 'e3'
//            )
//        ));
//          $this->add(array(
//            'type' => 'Zend\Form\Element\Radio',
//            'name' => 'module_woh',
//            'options' => array(
//                'value_options' => array(
//                    '1' => 'Available',
//                    '0' => 'Not Available',
//                ),
//            ),
//            'attributes' => array(
//                'value' => '', //set checked to '1'
//                'class' => 'e3'
//            )
//        ));
//          $this->add(array(
//            'type' => 'Zend\Form\Element\Radio',
//            'name' => 'module_gift_shop',
//            'options' => array(
//                'value_options' => array(
//                    '1' => 'Available',
//                    '0' => 'Not Available',
//                ),
//            ),
//            'attributes' => array(
//                'value' => '', //set checked to '1'
//                'class' => 'e3'
//            )
//        ));
//         $this->add(array(
//            'type' => 'Zend\Form\Element\Radio',
//            'name' => 'module_gift_help',
//            'options' => array(
//                'value_options' => array(
//                    '1' => 'Available',
//                    '0' => 'Not Available',
//                ),
//            ),
//            'attributes' => array(
//                'value' => '', //set checked to '1'
//                'class' => 'e3'
//            )
//        ));
//         $this->add(array(
//            'type' => 'Zend\Form\Element\Radio',
//            'name' => 'module_wayfinding',
//            'options' => array(
//                'value_options' => array(
//                    '1' => 'Available',
//                    '0' => 'Not Available',
//                ),
//            ),
//            'attributes' => array(
//                'value' => '', //set checked to '1'
//                'class' => 'e3'
//            )
//        ));
//         $this->add(array(
//            'type' => 'Zend\Form\Element\Radio',
//            'name' => 'module_promo',
//            'options' => array(
//                'value_options' => array(
//                    '1' => 'Available',
//                    '0' => 'Not Available',
//                ),
//            ),
//            'attributes' => array(
//                'value' => '', //set checked to '1'
//                'class' => 'e3'
//            )
//        ));
        
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Search',
                'id' => 'submitbutton',
                'class' => 'save-btn'
            ),
        ));
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Cancle',
                'id' => 'cancelbutton',
                'class' => 'save-btn'
            ),
        ));
        $this->add(array(
            'name' => 'delete',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Delete',
                'id' => 'deletebutton',
                'class' => 'save-btn'
            ),
        ));
//        $this->add(array(
//            'name' => 'cancel',
//            'attributes' => array(
//                'type' => 'button',
//                'value' => 'Cancel',
//                'id' => 'cancel',
//                'class' => 'cancel-btn m-l-20',
//                'onClick' => "javascript:window.location.href='/crm-users'",
//            ),
//        ));
        
         $this->add(array('type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 100
                )
            )
        ));
       
    }

}