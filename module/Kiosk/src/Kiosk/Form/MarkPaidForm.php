<?php

/**
 * This page is used for create crm users form
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace Kiosk\Form;

use Zend\Form\Form;

/**
 * This class is used for create crm users form elements
 * @package    User_CrmUserForm
 * @author     Icreon Tech - AP
 */
class MarkPaidForm extends Form {

    public function __construct($param = null) {
        /* we want to ignore the name passed */
        parent::__construct('markpaid');
        $this->setAttribute('method', 'post');
        //$this->setAttribute('enctype', 'multipart/form-data');
       
       $this->add(array(
            'name' => 'pending_transaction_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'pending_transaction_id',
            )
        ));
       
         
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Search',
                'id' => 'submitbutton',
                'class' => 'save-btn'
            ),
        ));
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Cancle',
                'id' => 'cancelbutton',
                'class' => 'save-btn'
            ),
        ));
       
       
    }

}