<?php

/**
 * This page is used for create crm users form
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace Kiosk\Form;

use Zend\Form\Form;

/**
 * This class is used for create crm users form elements
 * @package    User_CrmUserForm
 * @author     Icreon Tech - AP
 */
class PurchaseSessionForm extends Form {

    public function __construct($param = null) {
        /* we want to ignore the name passed */
        parent::__construct('kiosk');
        $this->setAttribute('method', 'post');
        //$this->setAttribute('enctype', 'multipart/form-data');
         $this->add(array(
            'name' => 'first_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'first_name',
                'placeholder'=>'Enter Your First or Given Name*',
                'ng-model'=>' required',
				'class'=>'text-capitalize'
                
            )
        ));
         $this->add(array(
            'name' => 'last_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_name',
                'placeholder'=>'Enter Your Last or Family Name*',
				'class'=>'text-capitalize'
                
            )
        ));
         $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email',
                'placeholder'=>'Enter Email Address*'
                
            )
        ));         
         $this->add(array(
            'name' => 'address',
            'attributes' => array(
                'type' => 'text',
                'id' => 'address',
                'placeholder'=>'Enter Street Address *'
            )
        ));
         $this->add(array(
            'name' => 'city',
            'attributes' => array(
                'type' => 'text',
                'id' => 'city',
                'placeholder'=>'Enter City*',
            )
        ));         
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'status',
            'options' => array(
                'value_options' => array(
                    '1' => 'Available',
                    '0' => 'Not Available',
                ),
            ),
            'attributes' => array(
                'value' => '', //set checked to '1'
                'class' => 'e3'
            )
        ));
        
         $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'country',
            'options' => array(
            ),
            'attributes' => array(
                'id'=>'country',
				'class'=>'devkit'
            )
        ));
         $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'state',
            'options' => array(
            ),
            'attributes' => array(
                'id'=>'state',
				'class'=>'devkit'
           
            )
        ));  
         $this->add(array(
            'name' => 'other_state',
            'attributes' => array(
                'type' => 'text',
                'id' => 'other_state',
                'placeholder'=>'Other State',
                'style'=>'display:none;'
               
            )
        ));          
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'button',
                'value' => 'NEXT',
                'id' => 'submitbutton',
                'class' => 'save-btn'
            ),
        ));
       
//        $this->add(array(
//            'name' => 'cancel',
//            'attributes' => array(
//                'type' => 'button',
//                'value' => 'Cancel',
//                'id' => 'cancel',
//                'class' => 'cancel-btn m-l-20',
//                'onClick' => "javascript:window.location.href='/crm-users'",
//            ),
//        ));
        
        
       
    }

}