<?php

/**
 * This page is used for create crm users form
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace Kiosk\Form;

use Zend\Form\Form;

/**
 * This class is used for create crm users form elements
 * @package    User_CrmUserForm
 * @author     Icreon Tech - AP
 */
class KioskLogin extends Form {

    public function __construct($param = null) {
        /* we want to ignore the name passed */
        parent::__construct('kiosk');
        $this->setAttribute('method', 'post');
        //$this->setAttribute('enctype', 'multipart/form-data');
         $this->add(array(
            'name' => 'confirmation_code',
            'attributes' => array(
                'type' => 'text',
                'id' => 'confirmation_code',
                'placeholder'=>'Enter Confirmation Code'
            )
        ));
         $this->add(array(
            'name' => 'email_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email_id',
                'placeholder'=>'Enter Email or Last / Family Name'
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'button',
                'value' => 'NEXT',
                'id' => 'submitbutton',
                'class' => 'save-btn'
            ),
        ));
       
    }

}