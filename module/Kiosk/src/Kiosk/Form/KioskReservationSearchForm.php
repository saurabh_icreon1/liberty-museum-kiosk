<?php

/**
 * This page is used for create crm users form
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace Kiosk\Form;

use Zend\Form\Form;

/**
 * This class is used for create crm users form elements
 * @package    User_CrmUserForm
 * @author     Icreon Tech - AP
 */
class KioskReservationSearchForm extends Form {

    public function __construct($param = null) {
        /* we want to ignore the name passed */
        parent::__construct('kioskreservationsearch');
        $this->setAttribute('method', 'post');
        //$this->setAttribute('enctype', 'multipart/form-data');
       

        $this->add(array(
            'type' => 'MultiCheckbox',
            'name' => 'status',
            'options' => array(
                //'label' => $this->translate('notification.roles_type_list'),
                'use_hidden_element' => true,
                'value_options' => array(
                             '0' => 'Availbale',
                             '1' => 'Reserved',
                             '2' => 'Not Available',
                             '3' => 'In Use',
                             '4' => 'Not Reserved',
                             '5' => 'Paused',
                     ),
            ),
            'attributes' => array(
                'id' => 'role_type_list',
                'class' => 'scrollPanChkBox',
            ),
        ));
        
         $this->add(array(
            'type' => 'Zend\Form\Element\radio',
            'name' => 'type',
            'options' => array(
                'value_options' => array(
                    '2' => 'Both',
                    '1' => 'Automatic',
                    '0' => 'Manual',
                ),
            ),
            'attributes' => array(
                'value' => '2', //set checked to '1'
                'class' => 'e3'
            )
        ));
         $this->add(array(
            'type' => 'Zend\Form\Element\radio',
            'name' => 'wheelchair',
            'options' => array(
                'value_options' => array(
                    '2' => 'Both',
                    '1' => 'Wheelchair',
                    '0' => 'Non-Wheelchair',
                ),
            ),
            'attributes' => array(
                'value' => '2', //set checked to '1'
                'class' => 'e3'
            )
        ));
         $this->add(array(
            'type' => 'Zend\Form\Element\radio',
            'name' => 'history',
            'options' => array(
                'value_options' => array(
                    '2' => 'Both',
                    '1' => 'Current Records',
                    '0' => 'Past Records',
                ),
            ),
            'attributes' => array(
                'value' => '2', //set checked to '1'
                'class' => 'e3'
            )
        ));
         $this->add(array(
            'name' => 'kiosk',
            'attributes' => array(
                'type' => 'text',
                'id' => 'kiosk',
                //'placeholder' => 'Name Or IP',
            )
        ));
        $this->add(array(
            'name' => 'contact',
            'attributes' => array(
                'type' => 'text',
                'id' => 'contact',
                //'placeholder' => 'Name Or IP',
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Search',
                'id' => 'submitbutton',
                'class' => 'search-btn'
            ),
        ));
//        $this->add(array(
//            'name' => 'cancel',
//            'attributes' => array(
//                'type' => 'button',
//                'value' => 'Cancel',
//                'id' => 'cancel',
//                'class' => 'cancel-btn m-l-20',
//                'onClick' => "javascript:window.location.href='/crm-users'",
//            ),
//        ));
        
         $this->add(array('type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 100
                )
            )
        ));
       
    }

}