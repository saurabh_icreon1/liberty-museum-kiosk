<?php

/**
 * This page is used for create crm users form
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace Kiosk\Form;

use Zend\Form\Form;

/**
 * This class is used for create crm users form elements
 * @package    User_CrmUserForm
 * @author     Icreon Tech - AP
 */
class WohDonationForm extends Form {

    public function __construct($param = null) {
        /* we want to ignore the name passed */
        parent::__construct('kiosk');
        $this->setAttribute('method', 'post');
        //$this->setAttribute('enctype', 'multipart/form-data');
         $this->add(array(
            'name' => 'first_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'first_name',
                'placeholder'=>'Enter First or Given Name*',
                'ng-model'=>' required',
				'class'=>'text-capitalize'
				
                
            )
        ));
         $this->add(array(
            'name' => 'last_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_name',
                'placeholder'=>'Enter Last or Family Name*',
				'class'=>'text-capitalize'
				
                
            )
        ));
         $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email',
                'placeholder'=>'Enter Email Address',
				
                
            )
        ));         
         
              
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'button',
                'value' => 'NEXT',
                'id' => 'submitbutton',
                'class' => 'save-btn'
            ),
        ));
        $this->add(array(
            'name' => 'amount_1',
            'attributes' => array(
                'type' => 'button',
                'value' => '10.00',
                'id' => 'amount_1',
                'class' => 'save-btn'
            ),
        ));        
        $this->add(array(
            'name' => 'amount_2',
            'attributes' => array(
                'type' => 'button',
                'value' => '25.00',
                'id' => 'amount_2',
                'class' => 'save-btn'
            ),
        ));        
        $this->add(array(
            'name' => 'amount_3',
            'attributes' => array(
                'type' => 'button',
                'value' => '50',
                'id' => 'amount_3',
                'class' => 'save-btn'
            ),
        ));        
        $this->add(array(
            'name' => 'amount_4',
            'attributes' => array(
                'type' => 'button',
                'value' => '150',
                'id' => 'amount_4',
                'class' => 'save-btn'
            ),
        ));        
        $this->add(array(
            'name' => 'donation_amount',
            'attributes' => array(
                'type' => 'text',
                'value' => '',
                'id' => 'donation_amount',
                'class' => 'billing',
                'placeholder'=>'Enter your own amount',
                'maxlength' => '10',
            ),
        ));        
        $this->add(array(
            'name' => 'amount',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'amount',
                'class' => 'save-btn'
            ),
        ));        
       
//        $this->add(array(
//            'name' => 'cancel',
//            'attributes' => array(
//                'type' => 'button',
//                'value' => 'Cancel',
//                'id' => 'cancel',
//                'class' => 'cancel-btn m-l-20',
//                'onClick' => "javascript:window.location.href='/crm-users'",
//            ),
//        ));
        
        
       
    }

}