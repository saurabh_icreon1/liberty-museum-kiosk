<?php

/**
 * This page is used for create crm users form
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace Kiosk\Form;

use Zend\Form\Form;

/**
 * This class is used for create crm users form elements
 * @package    User_CrmUserForm
 * @author     Icreon Tech - AP
 */
class KioskForm extends Form {

    public function __construct($param = null) {
        /* we want to ignore the name passed */
        parent::__construct('kiosk');
        $this->setAttribute('method', 'post');
        //$this->setAttribute('enctype', 'multipart/form-data');
       $this->add(array(
            'name' => 'kiosk_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'kiosk_id',
            )
        ));
         $this->add(array(
            'name' => 'kiosk_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'kiosk_name',
            )
        ));
        $this->add(array(
            'name' => 'kiosk_ip',
            'attributes' => array(
                'type' => 'text',
                'id' => 'kiosk_ip',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'status',
            'options' => array(
                'value_options' => array(
                    '1' => 'Available',
                    '0' => 'Not Available',
                ),
            ),
            'attributes' => array(
                'value' => '', //set checked to '1'
                'class' => 'e3'
            )
        ));
         $this->add(array(
            'name' => 'kiosk_capacity',
            'attributes' => array(
                'type' => 'text',
                'id' => 'kiosk_capacity',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'kiosk_wheelchair',
            'options' => array(
                'value_options' => array(
                    '1' => 'Available',
                    '0' => 'Not Available',
                ),
            ),
            'attributes' => array(
                'value' => '', //set checked to '1'
                'class' => 'e3'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'koisk_ads',
            'options' => array(
                'value_options' => array(
                    '1' => 'Available',
                    '0' => 'Not Available',
                ),
            ),
            'attributes' => array(
                'value' => '', //set checked to '1'
                'class' => 'e3'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'kiosk_note',
            'attributes' => array(
                'class' => 'e3 textarea',
                'id'=>'kiosk_note'
            )
        ));
         $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'kiosk_shutoff',
            'options' => array(
                'value_options' => array(
                    '0' => 'Never',
                    '1' => 'Time',
                ),
            ),
            'attributes' => array(
                'value' => '', //set checked to '1'
                'class' => 'e3',
                'id' => 'kiosk_shutoff',
            )
        ));
         $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'kiosk_shutoff_time_list',
            'options' => array(
                'value_options' => array(
                    '1' => 'AM',
                    '2' => 'PM',
					
                ),
            ),
            'attributes' => array(
                'value' => '1', //set checked to '1'
                'class' => 'e1 width45 select-w-320',
                'id'=>'kiosk_shutoff_time_list'
            )
        ));
       $this->add(array(
            'name' => 'kiosk_shutoff_time',
            'attributes' => array(
                'type' => 'text',
                'id' => 'kiosk_shutoff_time',
				'class' => 'width45'
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Search',
                'id' => 'submitbutton',
                'class' => 'save-btn'
            ),
        ));
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Cancle',
                'id' => 'cancelbutton',
                'class' => 'save-btn'
            ),
        ));
        $this->add(array(
            'name' => 'delete',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Delete',
                'id' => 'deletebutton',
                'class' => 'save-btn'
            ),
        ));
//        $this->add(array(
//            'name' => 'cancel',
//            'attributes' => array(
//                'type' => 'button',
//                'value' => 'Cancel',
//                'id' => 'cancel',
//                'class' => 'cancel-btn m-l-20',
//                'onClick' => "javascript:window.location.href='/crm-users'",
//            ),
//        ));
        
         $this->add(array('type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 100
                )
            )
        ));
       
    }

}