<?php

/**
 * This page is used for create crm users form
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace Kiosk\Form;

use Zend\Form\Form;

/**
 * This class is used for create crm users form elements
 * @package    User_CrmUserForm
 * @author     Icreon Tech - AP
 */
class VisitationCertificateForm extends Form {

    public function __construct($param = null) {
        /* we want to ignore the name passed */
        parent::__construct('kiosk');
        $this->setAttribute('method', 'post');
        //$this->setAttribute('enctype', 'multipart/form-data');
         $this->add(array(
            'name' => 'first_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'first_name',
                'placeholder'=>'Enter First or Given Name*',
                'ng-model'=>' required',
				'class'=>'text-capitalize'
                
            )
        ));
         $this->add(array(
            'name' => 'last_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_name',
                'placeholder'=>'Enter Last or Family Name*',
				'class'=>'text-capitalize'
               
            )
        ));
         $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email',
                'placeholder'=>'Enter Email Address'
              
            )
        ));         
         $this->add(array(
            'name' => 'family_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'family_name',
                'placeholder'=>'Enter Family Name*',
				'class'=>'text-capitalize'
                
            )
        ));         
         $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'country',
            'options' => array(
            ),
            'attributes' => array(
                'id'=>'country',
                'value'=>'228',
				'class'=>'devkit'
            )
        ));
           
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'button',
                'value' => 'NEXT',
                'id' => 'submitbutton',
                'class' => 'save-btn'
            ),
        ));
       
//        $this->add(array(
//            'name' => 'cancel',
//            'attributes' => array(
//                'type' => 'button',
//                'value' => 'Cancel',
//                'id' => 'cancel',
//                'class' => 'cancel-btn m-l-20',
//                'onClick' => "javascript:window.location.href='/crm-users'",
//            ),
//        ));
        
        
       
    }

}