<?php

/**
 * This model is used for find and merge duplicate contact
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace Kiosk\Model;

use Kiosk\Module;
// Add these import statements
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\Adapter\Adapter;

/**
 * This model is used for find and merge duplicate contact
 * @package    User
 * @author     Icreon Tech - DG
 */
class PosManagement implements InputFilterAwareInterface {

    protected $inputFilter;
    protected $adapter;

    public function __construct(Adapter $adapter) {

        $this->adapter = $adapter;
    }
    // Add content to these methods:
    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    // Add the following method:
    public function getArrayCopy() {
        return get_object_vars($this);
    }
    /**
     * Function used to check variable for user create contact form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function exchangeArray($data) {
        if (!empty($data['first_name'])) {
            $this->first_name = (isset($data['first_name'])) ? $data['first_name'] : null;
        }
    }
     /**
     * Function used to check validation for crm create user form
     * @param Array
     * @return array
     * @author Icreon Tech - AP
     */
    public function getInputFilter() {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
             $inputFilter->add($factory->createInput(array(
                        'name' => 'first_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'last_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
      
    
            $inputFilter->add($factory->createInput(array(
                        'name' => 'email',
                        'required' => true,
                    )));
             
           

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
    
    /**
     * Function used to check variable for crm user 
     * @param Array
     * @return array
     * @author Icreon Tech - AP
     */
    public function filterParam($data) {
        $parameter = array();

        $parameter['no_email'] = (isset($data['email']) && !empty($data['email'])) ? '0' : '1';
        $parameter['login_enable'] = (isset($data['login_enable'])) ? $data['login_enable'] : '0';
        $parameter['username'] = (isset($data['username'])) ? trim(strtolower($data['username'])) : '0';
        $parameter['contact_type'] = (isset($data['contact_type'])) ? $data['contact_type'] : null;
        $parameter['first_name'] = (isset($data['first_name'])) ? trim($data['first_name']) : null;
        $parameter['last_name'] = (isset($data['last_name'])) ? trim($data['last_name']) : null;
        $parameter['email'] = (isset($data['email'])) ? trim($data['email']) : null;
        $parameter['address'] = (isset($data['address'])) ? $data['address'] : null;
        if($data['country'] == 228)
            $parameter['state'] = (isset($data['state'])) ? $data['state'] : null;
        else
            $parameter['state'] = (isset($data['other_state'])) ? $data['other_state'] : null;
        $parameter['country'] = (isset($data['country'])) ? $data['country'] : null;
        $parameter['city'] = (isset($data['city'])) ? $data['city'] : null;
        $parameter['other_state'] = (isset($data['other_state'])) ? $data['other_state'] : null;
        $parameter['zipcode'] = (isset($data['zipcode'])) ? $data['zipcode'] : null;
        $parameter['contact_type'] = (isset($data['contact_type'])) ? $data['contact_type'] : null;
        
        $parameter['salt'] = $this->generateRandomString();
        $parameter['verify_code'] = $this->generateRandomString();
        $password = $this->generateRandomString();
        $parameter['password'] = md5($password . $parameter['salt']);
        $parameter['genrated_password'] = $password; 
        $parameter['source_id'] = (isset($data['source_id'])) ? $data['source_id'] : null; 
        $parameter['add_date'] = $data['add_date'];
        $parameter['modified_date'] = $data['modified_date'];        
        $parameter['payment_mode'] = $data['payment_mode'];        
        $parameter['amount'] = $data['amount']; 
         $parameter['card_details'] = (isset($data['card_details'])) ? $data['card_details'] : '';
        
        return $parameter;
    }
    /**
     * Function used to check variable for crm user 
     * @param Array
     * @return array
     * @author Icreon Tech - AP
     */
    public function filterParamVisit($data) {
        $parameter = array();

        $parameter['no_email'] = (isset($data['email']) && !empty($data['email'])) ? '0' : '1';
        $parameter['login_enable'] = (isset($data['login_enable'])) ? $data['login_enable'] : '0';
        $parameter['username'] = (isset($data['username'])) ? trim(strtolower($data['username'])) : '0';
        $parameter['contact_type'] = (isset($data['contact_type'])) ? $data['contact_type'] : null;
        $parameter['first_name'] = (isset($data['first_name'])) ? trim($data['first_name']) : null;
        $parameter['last_name'] = (isset($data['last_name'])) ? trim($data['last_name']) : null;
        $parameter['family_name'] = (isset($data['family_name'])) ? trim($data['family_name']) : null;
        $parameter['email'] = (isset($data['email'])) ? trim($data['email']) : null;
        $parameter['address'] = (isset($data['address'])) ? $data['address'] : null;
        if($data['country'] == 228)
            $parameter['state'] = (isset($data['state'])) ? $data['state'] : null;
        else
            $parameter['state'] = (isset($data['other_state'])) ? $data['other_state'] : null;
        $parameter['country'] = (isset($data['country'])) ? $data['country'] : null;
        $parameter['city'] = (isset($data['city'])) ? $data['city'] : null;
        $parameter['other_state'] = (isset($data['other_state'])) ? $data['other_state'] : null;
        $parameter['zipcode'] = (isset($data['zipcode'])) ? $data['zipcode'] : null;
        $parameter['contact_type'] = (isset($data['contact_type'])) ? $data['contact_type'] : null;
        
        $parameter['salt'] = $this->generateRandomString();
        $parameter['verify_code'] = $this->generateRandomString();
        $password = $this->generateRandomString();
        $parameter['password'] = md5($password . $parameter['salt']);
        $parameter['genrated_password'] = $password; 
        $parameter['source_id'] = (isset($data['source_id'])) ? $data['source_id'] : null; 
        $parameter['add_date'] = $data['add_date'];
        $parameter['modified_date'] = $data['modified_date'];        
        $parameter['payment_mode'] = $data['payment_mode'];        
        $parameter['amount'] = $data['amount'];        
        
        return $parameter;
    }
    /**
     * This method is used to return the stored procedure array for final transaction
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getFinalTransactionArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : NULL;
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $returnArr[] = (isset($dataArr['transaction_source_id']) && $dataArr['transaction_source_id'] != '') ? $dataArr['transaction_source_id'] : '';
        $returnArr[] = (isset($dataArr['num_items']) && $dataArr['num_items'] != '') ? $dataArr['num_items'] : 0;
        $returnArr[] = (isset($dataArr['sub_total_amount']) && $dataArr['sub_total_amount'] != '') ? $dataArr['sub_total_amount'] : 0;
        $returnArr[] = (isset($dataArr['total_discount']) && $dataArr['total_discount'] != '') ? $dataArr['total_discount'] : 0;
        $returnArr[] = (isset($dataArr['shipping_amount']) && $dataArr['shipping_amount'] != '') ? $dataArr['shipping_amount'] : 0;
        $returnArr[] = (isset($dataArr['handling_amount']) && $dataArr['handling_amount'] != '') ? $dataArr['handling_amount'] : 0;
        $returnArr[] = (isset($dataArr['total_tax']) && $dataArr['total_tax'] != '') ? $dataArr['total_tax'] : 0;
        $returnArr[] = (isset($dataArr['transaction_amount']) && $dataArr['transaction_amount'] != '') ? $dataArr['transaction_amount'] : 0;
        $returnArr[] = (isset($dataArr['transaction_type']) && $dataArr['transaction_type'] != '') ? $dataArr['transaction_type'] : '2';
        $returnArr[] = (isset($dataArr['transaction_status_id']) && $dataArr['transaction_status_id'] != '') ? $dataArr['transaction_status_id'] : '1';
        $returnArr[] = (isset($dataArr['transaction_date']) && $dataArr['transaction_date'] != '') ? $dataArr['transaction_date'] : '';
        $returnArr[] = (isset($dataArr['shipping_type_id']) && $dataArr['shipping_type_id'] != '') ? $dataArr['shipping_type_id'] : '';
        $returnArr[] = (isset($dataArr['pick_up']) && $dataArr['pick_up'] != '') ? $dataArr['pick_up'] : '0';
        $returnArr[] = (isset($dataArr['coupon_code']) && $dataArr['coupon_code'] != '') ? $dataArr['coupon_code'] : '';
        $returnArr[] = (isset($dataArr['is_free_shipping']) && $dataArr['is_free_shipping'] != '') ? $dataArr['is_free_shipping'] : '0';
        $returnArr[] = (isset($dataArr['added_by']) && $dataArr['added_by'] != '') ? $dataArr['added_by'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : '';
        $returnArr[] = (isset($dataArr['modify_by']) && $dataArr['modify_by'] != '') ? $dataArr['modify_by'] : '';
        $returnArr[] = (isset($dataArr['modify_date']) && $dataArr['modify_date'] != '') ? $dataArr['modify_date'] : '';
        $returnArr[] = (isset($dataArr['campaign_code']) && $dataArr['campaign_code'] != '') ? $dataArr['campaign_code'] : '';
        $returnArr[] = (isset($dataArr['campaign_id']) && $dataArr['campaign_id'] != '') ? $dataArr['campaign_id'] : '';
        $returnArr[] = (isset($dataArr['channel_id']) && $dataArr['channel_id'] != '') ? $dataArr['channel_id'] : '';
        $returnArr[] = (isset($dataArr['don_amount']) && $dataArr['don_amount'] != '') ? $dataArr['don_amount'] : 0;
        $returnArr[] = (isset($dataArr['group_id']) && $dataArr['group_id'] != '') ? $dataArr['group_id'] : '';
        $returnArr[] = (isset($dataArr['total_product_revenue']) && $dataArr['total_product_revenue'] != '') ? $dataArr['total_product_revenue'] : '';
		$returnArr[] = (isset($dataArr['invoice_number']) && $dataArr['invoice_number'] != '') ? $dataArr['invoice_number'] : '';
        return $returnArr;
    } 
    
    /**
     * This method is used to return the stored procedure array for final transaction payment info
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getTransactionPaymentArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $returnArr[] = (isset($dataArr['payment_mode_id']) && $dataArr['payment_mode_id'] != '') ? $dataArr['payment_mode_id'] : '';
        $returnArr[] = (isset($dataArr['amount']) && $dataArr['amount'] != '') ? $dataArr['amount'] : '';
        $returnArr[] = (isset($dataArr['cheque_type_id']) && $dataArr['cheque_type_id'] != '') ? $dataArr['cheque_type_id'] : '';
        $returnArr[] = (isset($dataArr['cheque_number']) && $dataArr['cheque_number'] != '') ? $dataArr['cheque_number'] : '';
        $returnArr[] = (isset($dataArr['id_type_id']) && $dataArr['id_type_id'] != '') ? $dataArr['id_type_id'] : '';
        $returnArr[] = (isset($dataArr['id_number']) && $dataArr['id_number'] != '') ? $dataArr['id_number'] : '';
        $returnArr[] = (isset($dataArr['profile_id']) && $dataArr['profile_id'] != '') ? $dataArr['profile_id'] : '';
        $returnArr[] = (isset($dataArr['payment_id']) && $dataArr['payment_id'] != '') ? $dataArr['payment_id'] : '';
        $returnArr[] = (isset($dataArr['billing_id']) && $dataArr['billing_id'] != '') ? $dataArr['billing_id'] : '';
        $returnArr[] = (isset($dataArr['address']) && $dataArr['address'] != '') ? $dataArr['address'] : '';
        $returnArr[] = (isset($dataArr['city']) && $dataArr['city'] != '') ? $dataArr['city'] : '';
        $returnArr[] = (isset($dataArr['state']) && $dataArr['state'] != '') ? $dataArr['state'] : '';
        $returnArr[] = (isset($dataArr['country_id']) && $dataArr['country_id'] != '') ? $dataArr['country_id'] : '';
        $returnArr[] = (isset($dataArr['postal_code']) && $dataArr['postal_code'] != '') ? $dataArr['postal_code'] : '';
        $returnArr[] = (isset($dataArr['authorize_transaction_id']) && $dataArr['authorize_transaction_id'] != '') ? $dataArr['authorize_transaction_id'] : '';
        $returnArr[] = (isset($dataArr['cheque_date']) && $dataArr['cheque_date'] != '') ? $dataArr['cheque_date'] : '';
        $returnArr[] = (isset($dataArr['account_no']) && $dataArr['account_no'] != '') ? $dataArr['account_no'] : '';
        $returnArr[] = (isset($dataArr['added_by']) && $dataArr['added_by'] != '') ? $dataArr['added_by'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : DATE_TIME_FORMAT;
        $returnArr[] = (isset($dataArr['modify_by']) && $dataArr['modify_by'] != '') ? $dataArr['modify_by'] : '';
        $returnArr[] = (isset($dataArr['modify_date']) && $dataArr['modify_date'] != '') ? $dataArr['modify_date'] : DATE_TIME_FORMAT;
	$returnArr[] = (isset($dataArr['credit_card_type_id']) && $dataArr['credit_card_type_id'] != '') ? $dataArr['credit_card_type_id'] : '';
	$returnArr[] = (isset($dataArr['payment_profile_id']) && $dataArr['payment_profile_id'] != '') ? $dataArr['payment_profile_id'] : '';
        return $returnArr;
    }    
    
    /**
     * This method is used to return the stored procedure array for receive payment by credit array
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getTransactionPaymentReceiveArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['transaction_payment_id']) && $dataArr['transaction_payment_id'] != '') ? $dataArr['transaction_payment_id'] : '';
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $returnArr[] = (isset($dataArr['amount']) && $dataArr['amount'] != '') ? $dataArr['amount'] : 0;
        $returnArr[] = (isset($dataArr['id']) && $dataArr['id'] != '') ? $dataArr['id'] : '';
        $returnArr[] = (isset($dataArr['authorize_transaction_id']) && $dataArr['authorize_transaction_id'] != '') ? $dataArr['authorize_transaction_id'] : '';
        $returnArr[] = (isset($dataArr['added_by']) && $dataArr['added_by'] != '') ? $dataArr['added_by'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : DATE_TIME_FORMAT;
        $returnArr[] = (isset($dataArr['modify_by']) && $dataArr['modify_by'] != '') ? $dataArr['modify_by'] : '';
        $returnArr[] = (isset($dataArr['modify_date']) && $dataArr['modify_date'] != '') ? $dataArr['modify_date'] : DATE_TIME_FORMAT;
        return $returnArr;
    }    
    /**
     * Function for get random number
     * @author Icreon Tech - DG
     * @return array
     * @param Int
     */
    public function generateRandomString() {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ23456789";
        srand((double) microtime() * 1000000);
        $i = 0;
        $code = '';

        while ($i <= 7) {
            $num = rand() % 33;
            $tmp = substr($chars, $num, 1);
            $code = $code . $tmp;
            $i++;
        }
        return $code;
    }
    
    /**
     * This method is used to return the stored procedure array for update user membership
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getTotalUserPurchaseProductAmountArr($dataArr = array()) {
        $returnArr = array();
        $returnArr['user_id'] = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '';
        $returnArr['from_date'] = (isset($dataArr['from_date']) && $dataArr['from_date'] != '') ? $dataArr['from_date'] : '';
        $returnArr['to_date'] = (isset($dataArr['to_date']) && $dataArr['to_date'] != '') ? $dataArr['to_date'] : '';
        return $returnArr;
    }    
    
    /**
     * This method is used to return the stored procedure array for update user memberhsip
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getMatchingMembershipArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['minimun_donation_amount']) && $dataArr['minimun_donation_amount'] != '') ? $dataArr['minimun_donation_amount'] : '';
        return $returnArr;
    }    
     /**
     * This method is used to return the stored procedure array for check active pledge for user
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getCheckUserActivePledgeArr($dataArr=array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['pledgeId']) && $dataArr['pledgeId'] != '') ? $dataArr['pledgeId'] : '';
        $returnArr[] = (isset($dataArr['userId']) && $dataArr['userId'] != '') ? $dataArr['userId'] : '';
        return $returnArr;
    }   
    /**
     * This method is used to return the stored procedure array for get the pledge's transactions
     * @param code
     * @return Void
     * @author Icreon Tech - AG
     */
    public function getPledgeTransactionsSearchArr($dataArr=array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['pledge_id']) && $dataArr['pledge_id'] != '') ? $dataArr['pledge_id'] : '';
        $returnArr[] = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : 'due_date';
        $returnArr[] = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : 'ASC';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : 0;
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }    
     /**
     * This method is used to return the stored procedure array for get the pledge's transactions
     * @param code
     * @return Void
     * @author Icreon Tech - AG
     */
    public function updatePledgeTransactionStatusArr($dataArr=array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['pledge_transaction_id']) && $dataArr['pledge_transaction_id'] != '') ? $dataArr['pledge_transaction_id'] : '';
        $returnArr[] = (isset($dataArr['status']) && $dataArr['status'] != '') ? $dataArr['status'] : '0';
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $returnArr[] = (isset($dataArr['modify_by']) && $dataArr['modify_by'] != '') ? $dataArr['modify_by'] : '';
        $returnArr[] = (isset($dataArr['modify_date']) && $dataArr['modify_date'] != '') ? $dataArr['modify_date'] : '';
        return $returnArr;
    }    
    
    /**
     * This method is used to return the stored procedure array for update pledge transaction amount
     * @param code
     * @return Void
     * @author Icreon Tech - AG
     */
    public function updatePledgeTransactionAmountArr($dataArr=array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['pledge_transaction_id']) && $dataArr['pledge_transaction_id'] != '') ? $dataArr['pledge_transaction_id'] : '';
        $returnArr[] = (isset($dataArr['amount_pledge']) && $dataArr['amount_pledge'] != '') ? $dataArr['amount_pledge'] : '';
        $returnArr[] = (isset($dataArr['remaining_amount_pledge']) && $dataArr['remaining_amount_pledge'] != '') ? $dataArr['remaining_amount_pledge'] : '';
        $returnArr[] = (isset($dataArr['status']) && $dataArr['status'] != '') ? $dataArr['status'] : '';
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $returnArr[] = (isset($dataArr['modify_by']) && $dataArr['modify_by'] != '') ? $dataArr['modify_by'] : '';
        $returnArr[] = (isset($dataArr['modify_date']) && $dataArr['modify_date'] != '') ? $dataArr['modify_date'] : '';
        return $returnArr;
    }    
    
    /**
     * This method is used to return the stored procedure array for update user membership
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getUpdateUserMembershipArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '';
        $returnArr[] = (isset($dataArr['membership_id']) && $dataArr['membership_id'] != '') ? $dataArr['membership_id'] : '';
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $returnArr[] = (isset($dataArr['transaction_date']) && $dataArr['transaction_date'] != '') ? $dataArr['transaction_date'] : '';
        $returnArr[] = (isset($dataArr['membership_type']) && $dataArr['membership_type'] != '') ? $dataArr['membership_type'] : '';
        $returnArr[] = (isset($dataArr['transaction_amount']) && $dataArr['transaction_amount'] != '') ? $dataArr['transaction_amount'] : '';
        $returnArr[] = (isset($dataArr['membership_date_from']) && $dataArr['membership_date_from'] != '') ? $dataArr['membership_date_from'] : '';
        $returnArr[] = (isset($dataArr['membership_date_to']) && $dataArr['membership_date_to'] != '') ? $dataArr['membership_date_to'] : '';
        $returnArr[] = (isset($dataArr['membership_shipped']) && $dataArr['membership_shipped'] != '') ? $dataArr['membership_shipped'] : '';
        return $returnArr;
    }    
    
    /**
     * This method is used to return the stored procedure array for final transaction billing shipping address info
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getTransactionBillingShipping($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $returnArr[] = (isset($dataArr['billing_title']) && $dataArr['billing_title'] != '') ? $dataArr['billing_title'] : '';
        $returnArr[] = (isset($dataArr['billing_first_name']) && $dataArr['billing_first_name'] != '') ? $dataArr['billing_first_name'] : '';
        $returnArr[] = (isset($dataArr['billing_last_name']) && $dataArr['billing_last_name'] != '') ? $dataArr['billing_last_name'] : '';
        $returnArr[] = (isset($dataArr['billing_company']) && $dataArr['billing_company'] != '') ? $dataArr['billing_company'] : '';
        $returnArr[] = (isset($dataArr['billing_address']) && $dataArr['billing_address'] != '') ? $dataArr['billing_address'] : '';
        $returnArr[] = (isset($dataArr['billing_city']) && $dataArr['billing_city'] != '') ? $dataArr['billing_city'] : '';
        $returnArr[] = (isset($dataArr['billing_state']) && $dataArr['billing_state'] != '') ? $dataArr['billing_state'] : '';
        $returnArr[] = (isset($dataArr['billing_zip_code']) && $dataArr['billing_zip_code'] != '') ? $dataArr['billing_zip_code'] : '';
        $returnArr[] = (isset($dataArr['billing_country']) && $dataArr['billing_country'] != '') ? $dataArr['billing_country'] : '';
        $returnArr[] = (isset($dataArr['billing_phone']) && $dataArr['billing_phone'] != '') ? $dataArr['billing_phone'] : '';
        $returnArr[] = (isset($dataArr['is_apo_po']) && $dataArr['is_apo_po'] != '') ? $dataArr['is_apo_po'] : '';
        $returnArr[] = (isset($dataArr['shipping_title']) && $dataArr['shipping_title'] != '') ? $dataArr['shipping_title'] : '';
        $returnArr[] = (isset($dataArr['shipping_first_name']) && $dataArr['shipping_first_name'] != '') ? $dataArr['shipping_first_name'] : '';
        $returnArr[] = (isset($dataArr['shipping_last_name']) && $dataArr['shipping_last_name'] != '') ? $dataArr['shipping_last_name'] : '';
        $returnArr[] = (isset($dataArr['shipping_company']) && $dataArr['shipping_company'] != '') ? $dataArr['shipping_company'] : '';
        $returnArr[] = (isset($dataArr['shipping_address']) && $dataArr['shipping_address'] != '') ? $dataArr['shipping_address'] : '';
        $returnArr[] = (isset($dataArr['shipping_city']) && $dataArr['shipping_city'] != '') ? $dataArr['shipping_city'] : '';
        $returnArr[] = (isset($dataArr['shipping_state']) && $dataArr['shipping_state'] != '') ? $dataArr['shipping_state'] : '';
        $returnArr[] = (isset($dataArr['shipping_zip_code']) && $dataArr['shipping_zip_code'] != '') ? $dataArr['shipping_zip_code'] : '';
        $returnArr[] = (isset($dataArr['shipping_country']) && $dataArr['shipping_country'] != '') ? $dataArr['shipping_country'] : '';
        $returnArr[] = (isset($dataArr['shipping_phone']) && $dataArr['shipping_phone'] != '') ? $dataArr['shipping_phone'] : '';
        $returnArr[] = (isset($dataArr['added_by']) && $dataArr['added_by'] != '') ? $dataArr['added_by'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : DATE_TIME_FORMAT;
        $returnArr[] = (isset($dataArr['modify_by']) && $dataArr['modify_by'] != '') ? $dataArr['modify_by'] : '';
        $returnArr[] = (isset($dataArr['modify_date']) && $dataArr['modify_date'] != '') ? $dataArr['modify_date'] : DATE_TIME_FORMAT;
        return $returnArr;
    }    
    
    /**
     * This method is used to return the stored procedure array to save bulk donations
     * @param Array
     * @return Array
     * @author Icreon Tech - SK
     */
    public function getDonationProductArray($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['honoree_type']) && $dataArr['honoree_type'] != '') ? $dataArr['honoree_type'] : '';
        $returnArr[] = (isset($dataArr['honoree_name']) && $dataArr['honoree_name'] != '') ? $dataArr['honoree_name'] : '';
        $returnArr[] = (isset($dataArr['campaign_id']) && $dataArr['campaign_id'] != '') ? $dataArr['campaign_id'] : '';
        $returnArr[] = (isset($dataArr['pledge_transaction_id']) && $dataArr['pledge_transaction_id'] != '') ? $dataArr['pledge_transaction_id'] : '';
        $returnArr[] = (isset($dataArr['is_pledge_transaction']) && $dataArr['is_pledge_transaction'] != '') ? $dataArr['is_pledge_transaction'] : '0';
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $returnArr[] = (isset($dataArr['batch_group_id']) && $dataArr['batch_group_id'] != '') ? $dataArr['batch_group_id'] : '';
        $returnArr[] = (isset($dataArr['product_mapping_id']) && $dataArr['product_mapping_id'] != '') ? $dataArr['product_mapping_id'] : '';
        $returnArr[] = (isset($dataArr['product_type_id']) && $dataArr['product_type_id'] != '') ? $dataArr['product_type_id'] : '';
        $returnArr[] = (isset($dataArr['product_id']) && $dataArr['product_id'] != '') ? $dataArr['product_id'] : '';
        $returnArr[] = (isset($dataArr['product_name']) && $dataArr['product_name'] != '') ? $dataArr['product_name'] : '';
        $returnArr[] = (isset($dataArr['product_attribute_option_id']) && $dataArr['product_attribute_option_id'] != '') ? $dataArr['product_attribute_option_id'] : '';
        $returnArr[] = (isset($dataArr['product_sku']) && $dataArr['product_sku'] != '') ? $dataArr['product_sku'] : '';
        $returnArr[] = (isset($dataArr['product_price']) && $dataArr['product_price'] != '') ? $dataArr['product_price'] : '0';
        $returnArr[] = (isset($dataArr['product_sub_total']) && $dataArr['product_sub_total'] != '') ? $dataArr['product_sub_total'] : '0';
        $returnArr[] = (isset($dataArr['product_discount']) && $dataArr['product_discount'] != '') ? $dataArr['product_discount'] : '0';
        $returnArr[] = (isset($dataArr['product_tax']) && $dataArr['product_tax'] != '') ? $dataArr['product_tax'] : '0';
        $returnArr[] = (isset($dataArr['product_total']) && $dataArr['product_total'] != '') ? $dataArr['product_total'] : '';
        $returnArr[] = (isset($dataArr['product_status_id']) && $dataArr['product_status_id'] != '') ? $dataArr['product_status_id'] : '';
        $returnArr[] = (isset($dataArr['purchase_date']) && $dataArr['purchase_date'] != '') ? $dataArr['purchase_date'] : '';
        $returnArr[] = (isset($dataArr['is_deleted']) && $dataArr['is_deleted'] != '') ? $dataArr['is_deleted'] : '0';
        $returnArr[] = (isset($dataArr['added_by']) && $dataArr['added_by'] != '') ? $dataArr['added_by'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : '';
        $returnArr[] = (isset($dataArr['modified_date']) && $dataArr['modified_date'] != '') ? $dataArr['modified_date'] : '';
        $returnArr[] = (isset($dataArr['modified_by']) && $dataArr['modified_by'] != '') ? $dataArr['modified_by'] : '';
        $returnArr[] = (isset($dataArr['company_id']) && $dataArr['company_id'] != '') ? $dataArr['company_id'] : '';
        $returnArr[] = (isset($dataArr['company_name']) && $dataArr['company_name'] != '') ? $dataArr['company_name'] : '';
        $returnArr[] = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '';
        $returnArr[] = (isset($dataArr['num_quantity']) && $dataArr['num_quantity'] != '') ? $dataArr['num_quantity'] : '';
        $returnArr[] = (isset($dataArr['confirmation_code']) && $dataArr['confirmation_code'] != '') ? $dataArr['confirmation_code'] : '';
        return $returnArr;
    }    
    
    /**
     * This method is used to get all country lookup array
     * @param code
     * @return Void
     * @author Icreon Tech - DT
     */
    public function getCountryArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['country']) && $dataArr['country'] != '') ? $dataArr['country'] : '';
        $returnArr[] = (isset($dataArr['country_id']) && $dataArr['country_id'] != '') ? $dataArr['country_id'] : '';
        return $returnArr;
    }    
    
    public function getMembershipSearchArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $returnArr[] = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $returnArr[] = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        return $returnArr;
    }
    
}