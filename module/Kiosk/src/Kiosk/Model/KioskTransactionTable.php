<?php

/**
 * This model is used for user. Transaction
 * @package    Transaction
 * @author     Icreon Tech - DG
 */

namespace Kiosk\Model;

use Authorize\lib\AuthnetXML;
use Zend\Db\TableGateway\TableGateway;

/**
 * This model is used for user. Transaction
 * @package    Transaction
 * @author     Icreon Tech - DG
 */
class KioskTransactionTable {

    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
    }

    
    /**
     * Function for get all transactions
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function getAllTransactions($dataArr) {
        
        $emailId = (isset($dataArr['email_id']) && $dataArr['email_id'] != '') ? $dataArr['email_id'] : '';
        $firstName = (isset($dataArr['first_name']) && $dataArr['first_name'] != '') ? $dataArr['first_name'] : '';
        $lastName = (isset($dataArr['last_name']) && $dataArr['last_name'] != '') ? $dataArr['last_name'] : '';
        $startIndex = (isset($dataArr['start_Index']) && $dataArr['start_Index'] != '') ? $dataArr['start_Index'] : '';
        $recordLimit = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $sortField = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $sortOrder = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        $transactionId = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';


        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_kiosk_tra_getCrmTransactions(?,?,?,?,?,?,?,?)");
        $stmt->getResource()->bindParam(1, $emailId);
        $stmt->getResource()->bindParam(2, $firstName);
        $stmt->getResource()->bindParam(3, $lastName);
        $stmt->getResource()->bindParam(4, $startIndex);
        $stmt->getResource()->bindParam(5, $recordLimit);
        $stmt->getResource()->bindParam(6, $sortField);
        $stmt->getResource()->bindParam(7, $sortOrder);
        $stmt->getResource()->bindParam(8, $transactionId);

        
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }
    
   /**
     * This function will add final transaction record
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function saveFinalTransaction($transactionData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_ksk_tra_insertTransactionDetail(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $stmt->getResource()->bindParam(7, $transactionData[6]);
            $stmt->getResource()->bindParam(8, $transactionData[7]);
            $stmt->getResource()->bindParam(9, $transactionData[8]);
            $stmt->getResource()->bindParam(10, $transactionData[9]);
            $stmt->getResource()->bindParam(11, $transactionData[10]);
            $stmt->getResource()->bindParam(12, $transactionData[11]);
            $stmt->getResource()->bindParam(13, $transactionData[12]);
            $stmt->getResource()->bindParam(14, $transactionData[13]);
            $stmt->getResource()->bindParam(15, $transactionData[14]);
            $stmt->getResource()->bindParam(16, $transactionData[15]);
            $stmt->getResource()->bindParam(17, $transactionData[16]);
            $stmt->getResource()->bindParam(18, $transactionData[17]);
            $stmt->getResource()->bindParam(19, $transactionData[18]);
            $stmt->getResource()->bindParam(20, $transactionData[19]);
            $stmt->getResource()->bindParam(21, $transactionData[20]);
            $stmt->getResource()->bindParam(22, $transactionData[21]);
            $stmt->getResource()->bindParam(23, $transactionData[22]);
            $stmt->getResource()->bindParam(24, $transactionData[23]);
            $stmt->getResource()->bindParam(25, $transactionData[24]);
            $stmt->getResource()->bindParam(26, $transactionData[25]);
            $stmt->getResource()->bindParam(27, $transactionData[26]);
            $stmt->getResource()->bindParam(28, $transactionData[27]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }    
    
    /**
     * Function for fetching shipping methods detail
     * @return $resultSet as array or false as boolean 
     * @param $dataArr as an array
     * @author Icreon Tech - AS
     */
    public function getShippingMethod($dataArr) {
        $procquesmarkapp = $this->appendQuestionMars(count($dataArr));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_tra_getShippingMethods(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $dataArr[0]);
        $stmt->getResource()->bindParam(2, $dataArr[1]);
        $stmt->getResource()->bindParam(3, $dataArr[2]);
        $stmt->getResource()->bindParam(4, $dataArr[3]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return $resultSet;
    }    
    
    /**
     * This function will add final transaction record
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function saveCartToTransaction($transactionData = array()) {
        try {

            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_kiosk_tra_insertCartToTransaction(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $stmt->getResource()->bindParam(7, $transactionData[6]);
            $stmt->getResource()->bindParam(8, $transactionData[7]);
            $stmt->getResource()->bindParam(9, $transactionData[8]);
            $stmt->getResource()->bindParam(10, $transactionData[9]);
            $stmt->getResource()->bindParam(11, $transactionData[10]);
            $stmt->getResource()->bindParam(12, $transactionData[11]);
            $stmt->getResource()->bindParam(13, $transactionData[12]);
            $stmt->getResource()->bindParam(14, $transactionData[13]);
            $stmt->getResource()->bindParam(15, $transactionData[14]);
            $stmt->getResource()->bindParam(16, $transactionData[15]);
            $stmt->getResource()->bindParam(17, $transactionData[16]);
            $stmt->getResource()->bindParam(18, $transactionData[17]);
            $stmt->getResource()->bindParam(19, $transactionData[18]);
            $stmt->getResource()->bindParam(20, $transactionData[19]);
            $stmt->getResource()->bindParam(21, $transactionData[20]);
            $stmt->getResource()->bindParam(22, $transactionData[21]);
            $stmt->getResource()->bindParam(23, $transactionData[22]);
            $stmt->getResource()->bindParam(24, $transactionData[23]);
            $stmt->getResource()->bindParam(25, $transactionData[24]);
            $stmt->getResource()->bindParam(26, $transactionData[25]);
            $stmt->getResource()->bindParam(27, $transactionData[26]);
            $stmt->getResource()->bindParam(28, $transactionData[27]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }    
    
    /**
     * Function for append number of questions marks for stored procedure
     * @author Icreon Tech - DG
     * @return String
     * @param Array
     */
    public function appendQuestionMars($count) {
        $returnstr = '';
        for ($i = 1; $i <= $count; $i++) {
            $returnstr.='?,';
        }
        return rtrim($returnstr, ',');
    }    
    
    public function deletePendingTransaction($params = array()){
        try { 
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_kiosk_pendingTransaction(?)');
            $stmt->getResource()->bindParam(1, $params['pending_transaction_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }    
    }
    
    /**
     * Function for to get Cart products
     * @author Icreon Tech - DT
     * @return Arr
     * @param Array
     */
    public function getCartProducts($dataArr = null) {
        $user_id = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '-1';
        $pending_transaction_id = (isset($dataArr['pending_transaction_id']) && $dataArr['pending_transaction_id'] != '') ? $dataArr['pending_transaction_id'] : '-1';
        $membership_percent_discount = (isset($dataArr['membership_percent_discount']) && $dataArr['membership_percent_discount'] != '' && $dataArr['membership_percent_discount'] != 0) ? $dataArr['membership_percent_discount'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_kiosk_front_tra_getCartProducts(?,?,?)");
        $stmt->getResource()->bindParam(1, $user_id);
        $stmt->getResource()->bindParam(2, $pending_transaction_id);
        $stmt->getResource()->bindParam(3, $membership_percent_discount);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }    
    
    /**
     * Function for fetching holiday detail
     * @return $resultSet as array or false as boolean 
     * @param $dataArr as an array
     * @author Icreon Tech - AS
     */
    public function getHoliday($dataArr) {
        $procquesmarkapp = $this->appendQuestionMars(count($dataArr));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_kiosk_tra_getHoliday(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $dataArr[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return $resultSet;
    }    
    
   /**
     * This function will add transaction billilng shipping address detail
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function saveTransactionPaymentDetail($transactionData = array()) {
        try {

            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_insertTransactionPaymentInfo(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $stmt->getResource()->bindParam(7, $transactionData[6]);
            $stmt->getResource()->bindParam(8, $transactionData[7]);
            $stmt->getResource()->bindParam(9, $transactionData[8]);
            $stmt->getResource()->bindParam(10, $transactionData[9]);
            $stmt->getResource()->bindParam(11, $transactionData[10]);
            $stmt->getResource()->bindParam(12, $transactionData[11]);
            $stmt->getResource()->bindParam(13, $transactionData[12]);
            $stmt->getResource()->bindParam(14, $transactionData[13]);
            $stmt->getResource()->bindParam(15, $transactionData[14]);
            $stmt->getResource()->bindParam(16, $transactionData[15]);
            $stmt->getResource()->bindParam(17, $transactionData[16]);
            $stmt->getResource()->bindParam(18, $transactionData[17]);
            $stmt->getResource()->bindParam(19, $transactionData[18]);
            $stmt->getResource()->bindParam(20, $transactionData[19]);
            $stmt->getResource()->bindParam(21, $transactionData[20]);
            $stmt->getResource()->bindParam(22, $transactionData[21]);
            $stmt->getResource()->bindParam(23, $transactionData[22]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }    
    
    /**
     * This function will add credit card receive payment
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function saveTransactionPaymentReceive($transactionData = array()) {
        try {

            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_insertCreditPayReceive(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $stmt->getResource()->bindParam(7, $transactionData[6]);
            $stmt->getResource()->bindParam(8, $transactionData[7]);
            $stmt->getResource()->bindParam(9, $transactionData[8]);
            $stmt->getResource()->bindParam(10, $transactionData[9]);
            $stmt->getResource()->bindParam(11, $transactionData[10]);
            $stmt->getResource()->bindParam(12, $transactionData[11]);
            $stmt->getResource()->bindParam(13, $transactionData[12]);
            $stmt->getResource()->bindParam(14, $transactionData[13]);
            $stmt->getResource()->bindParam(15, $transactionData[14]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }    
    
    /**
     * This function is used to remove the authorize transation id.
     * @author Icreon Tech - SR
     * @return array
     * @param Array
     */
    public function removeAuthorizeTransactionId($params = array()) {
        try {
            $transactionId = (isset($params['transaction_id']) && $params['transaction_id'] != '') ? $params['transaction_id'] : '';

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_removePaymentAuthTransactionId(?)');
            $stmt->getResource()->bindParam(1, $transactionId);
            $result = $stmt->execute();
            $statement = $result->getResource();
            // $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC); 

            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }    
    
   /**
     * This function will add transaction billilng shipping address detail
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function saveTransactionBillingShippingDetail($transactionData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_ksk_tra_insertTransactionBillingShipping(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $stmt->getResource()->bindParam(7, $transactionData[6]);
            $stmt->getResource()->bindParam(8, $transactionData[7]);
            $stmt->getResource()->bindParam(9, $transactionData[8]);
            $stmt->getResource()->bindParam(10, $transactionData[9]);
            $stmt->getResource()->bindParam(11, $transactionData[10]);
            $stmt->getResource()->bindParam(12, $transactionData[11]);
            $stmt->getResource()->bindParam(13, $transactionData[12]);
            $stmt->getResource()->bindParam(14, $transactionData[13]);
            $stmt->getResource()->bindParam(15, $transactionData[14]);
            $stmt->getResource()->bindParam(16, $transactionData[15]);
            $stmt->getResource()->bindParam(17, $transactionData[16]);
            $stmt->getResource()->bindParam(18, $transactionData[17]);
            $stmt->getResource()->bindParam(19, $transactionData[18]);
            $stmt->getResource()->bindParam(20, $transactionData[19]);
            $stmt->getResource()->bindParam(21, $transactionData[20]);
            $stmt->getResource()->bindParam(22, $transactionData[21]);
            $stmt->getResource()->bindParam(23, $transactionData[22]);
            $stmt->getResource()->bindParam(24, $transactionData[23]);
            $stmt->getResource()->bindParam(25, $transactionData[24]);
            $stmt->getResource()->bindParam(26, $transactionData[25]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }    
    
    /**
     * Function for remove transaction
     * @author Icreon Tech - DT
     * @return Void
     * @param Array
     */
    public function removeTransactionById($transactionData) {

        $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_kiosk_tra_deleteTransaction(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $transactionData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        
        $statement->closeCursor();
    }    
}
