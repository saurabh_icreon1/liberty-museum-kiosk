<?php

/**
 * This model is used for kiosk management
 * @package    Kiosk
 * @author     Icreon Tech - SK
 */

namespace Kiosk\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This model is used for kiosk management
 * @package    User
 * @author     Icreon Tech - SK
 */
class KioskModuleTable {

    protected $tableGateway;
    protected $dbAdapter;
    protected $connection;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
        $this->connection = $this->tableGateway->getAdapter()->getDriver()->getConnection();
    }
    
    /**
     * Function for to insert crm users
     * @param array
     * @return boolean
     * @author Icreon Tech - AP
     */
    public function insertKiosk($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_kiosk_insertKiosk(?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['kiosk_name']);
            $stmt->getResource()->bindParam(2, $param['kiosk_ip']);
            $stmt->getResource()->bindParam(3, $param['status']);
            $stmt->getResource()->bindParam(4, $param['kiosk_capacity']);
            $stmt->getResource()->bindParam(5, $param['is_wheelchair']);
            $stmt->getResource()->bindParam(6, $param['is_ada']);
            $stmt->getResource()->bindParam(7, $param['kiosk_notes']);
            $stmt->getResource()->bindParam(8, $param['added_by']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * This function is used to get the kiosk seached records
     * @param this will be an array.
     * @return this will return search result of kiosk
     * @author Icreon Tech -AP
     */
    public function searchKioskModule($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ksk_getKioskModuleSearch(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, (isset($param['name_or_ip']) and trim($param['name_or_ip']) != "") ? addslashes($param['name_or_ip']) : "");
            $stmt->getResource()->bindParam(2, $param['status']);
            $stmt->getResource()->bindParam(3, $param['startIndex']);
            $stmt->getResource()->bindParam(4, $param['recordLimit']);
            $stmt->getResource()->bindParam(5, $param['sortField']);
            $stmt->getResource()->bindParam(6, $param['sortOrder']);
            //$stmt->getResource()->bindParam(8, $param['currentTime']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    /**
     * Function for to get kiosk deatails
     * @param Array
     * @return Array
     * @author Icreon Tech - AP 
     */
    public function getKioskModuleDetails($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            //$stmt->prepare('CALL usp_ksk_getKioskModuleInfo(?)');
            $stmt->prepare('CALL usp_ksk_getKioskModuleDetailInfo(?)');
            $stmt->getResource()->bindParam(1, $param['kiosk_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }
     /**
     * Function for to delete contact
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function deleteKiosk($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_kiosk_deleteKiosk(?)');
            $stmt->getResource()->bindParam(1, $params['kiosk_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    /**
     * Function for to update crm users
     * @param Array
     * @return Array
     * @author Icreon Tech - AP 
     */
    public function insertKioskModuleInfo($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ksk_insertKioskModuleInfo(?,?,?)');
            $stmt->getResource()->bindParam(1, $param['kiosk_id']);
            $stmt->getResource()->bindParam(2, $param['kiosk_module_id']);
            $stmt->getResource()->bindParam(3, $param['added_by']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }
     /**
     * Function for to update kiosk module mapping table for mapping status
     * @param Array
     * @return Array
     * @author Icreon Tech - AP 
     */
    public function updateKioskModuleInfo($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ksk_updateKioskModuleInfo(?,?,?)');
            $stmt->getResource()->bindParam(1, $param['kiosk_module_mapping_id']);
            $stmt->getResource()->bindParam(2, $param['is_available']);
            $stmt->getResource()->bindParam(3, $param['modified_by']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
     /**
     * Function for to update kiosk module mapping table for mapping status
     * @param Array
     * @return Array
     * @author Icreon Tech - AP 
     */
    public function updateKioskModuleConfigInfo($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ksk_updateKioskModuleConfigInfo(?,?,?)');
            $stmt->getResource()->bindParam(1, $param['kiosk_setting_id']);
            $stmt->getResource()->bindParam(2, $param['setting_value']);
            $stmt->getResource()->bindParam(3, $param['modified_by']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    /**
     * Function for to update crm users
     * @param Array
     * @return Array
     * @author Icreon Tech - AP 
     */
    public function insertKioskModuleSettingsInfo($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ksk_insertKioskModuleSettingInfo(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['kiosk_id']);
            $stmt->getResource()->bindParam(2, $param['kiosk_module_id']);
            $stmt->getResource()->bindParam(3, $param['kiosk_module_setting_id']);
            $stmt->getResource()->bindParam(4, $param['setting_value']);
            $stmt->getResource()->bindParam(5, $param['added_by']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }
    
    
    /**
     * Function for get All Country
     * @author Icreon Tech - DG
     * @return array
     */
    public function getKioskModules() {
        //$stmt = $this->dbAdapter->createStatement();
        $result = $this->connection->execute('CALL usp_com_getKioskModules');
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();

        if (!$resultSet) {
            throw new \Exception("Could not find Modules record.");
        }
        return $resultSet;
    }
    /**
     * Function for get All Country
     * @author Icreon Tech - DG
     * @return array
     */
    public function getKioskModulesConfig() {
        //$stmt = $this->dbAdapter->createStatement();
        $result = $this->connection->execute('CALL usp_ksk_kioskModuleConfigInfo');
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();

        if (!$resultSet) {
            throw new \Exception("Could not find Modules record.");
        }
        return $resultSet;
    }
    
    /**
     * Function for to delete Kiosk Module Info
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function deleteKioskModuleInfo($kiosk_id) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ksk_deleteKioskModuleInfo(?)');
            $stmt->getResource()->bindParam(1, $kiosk_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    /**
     * Function for to get kiosk deatails
     * @param Array
     * @return Array
     * @author Icreon Tech - AP 
     */
    public function searchKioskSubModule($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ksk_getKioskSubmoduleSetting(?,?,?)');
            $stmt->getResource()->bindParam(1, $param['kiosk_id']);
            $stmt->getResource()->bindParam(2, $param['module_id']);
            $stmt->getResource()->bindParam(3, $param['sub_module_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }
}