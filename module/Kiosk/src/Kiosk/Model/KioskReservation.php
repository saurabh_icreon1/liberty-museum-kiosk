<?php

/**
 * This model is used for find and merge duplicate contact
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace Kiosk\Model;

use Kiosk\Module;
// Add these import statements
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\Adapter\Adapter;

/**
 * This model is used for find and merge duplicate contact
 * @package    User
 * @author     Icreon Tech - DG
 */
class KioskReservation implements InputFilterAwareInterface {

    protected $inputFilter;
    protected $adapter;

    public function __construct(Adapter $adapter) {

        $this->adapter = $adapter;
    }

    // Add content to these methods:
    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    // Add the following method:
    public function getArrayCopy() {
        return get_object_vars($this);
    }

    /**
     * Function used to check variable for user create contact form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function exchangeArray($data) {
        if (!empty($data['kiosk_id'])) {
            $this->email_id = (isset($data['kiosk_id'])) ? $data['kiosk_id'] : null;
        }
    }

    /**
     * Function used to check validation for crm create user form
     * @param Array
     * @return array
     * @author Icreon Tech - AP
     */
    public function getInputFilter($data = array()) {
        $contactFlag = true;
        $emailFlag = false;
        if ($data['email_id'] != "") {
            $contactFlag = false;
            $emailFlag = true;
        }
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                        'name' => 'contact',
                        'required' => $contactFlag,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_CONTACT',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'email_id',
                        'required' => $emailFlag,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMAIL_EMPTY',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'EmailAddress',
                                'options' => array(
                                    'messages' => array(
                                        \Zend\Validator\EmailAddress::INVALID_FORMAT => "EMAIL_INVALID",
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'Db\NoRecordExists',
                                'options' => array(
                                    'table' => 'tbl_usr_users',
                                    'field' => 'email_id',
                                    'adapter' => $this->adapter,
                                    'exclude' => array(
                                        'field' => 'is_delete',
                                        'value' => '1'
                                    ),
                                    'messages' => array(
                                        'recordFound' => 'EMAIL_ID_ALREADY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'csrf',
                        'required' => false,
                        'validators' => array(
                            array(
                                'name' => 'csrf',
                                'options' => array(
                                    'messages' => array(
                                        'notSame' => 'NOT_SAME',
                                    ),
                                ),
                            ),
                        ),
                    )));


            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * Function used to check validation for assign contact form
     * @param Array	 
     * @return array
     * @author Icreon Tech - AP
     */
    public function getAssignContactInputFilter() {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'contact_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_CONTACT',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * Function used to check variable for crm user 
     * @param Array
     * @return array
     * @author Icreon Tech - AP
     */
    public function filterParam($data) {
        $parameter = array();
        $parameter['kiosk_id'] = (isset($data['kiosk_list'])) ? $data['kiosk_list'] : 0;
        $parameter['contact_id'] = (isset($data['contact_id'])) ? $data['contact_id'] : null;
        $parameter['time_allocated'] = (isset($data['time_allocated'])) ? $data['time_allocated'] : null;
        $parameter['time_started'] = (isset($data['time_started'])) ? $data['time_started'] : 0;
        $parameter['time_end'] = (isset($data['time_end'])) ? $data['time_end'] : 0;
        $parameter['add_date'] = (isset($data['date'])) ? $data['date'] : null;
        $parameter['time_left'] = "00:30:00";
        return $parameter;
    }

    /**
     * Function used to check variable for user create popup contact form
     * @author Icreon Tech - DT
     * @return boolean
     * @param Array
     */
    public function filterParamPopup($data) {
        $parameter = array();
        $parameter['title'] = (isset($data['title'])) ? $data['title'] : null;
        $parameter['first_name'] = (isset($data['first_name'])) ? $data['first_name'] : null;
        $parameter['middle_name'] = (isset($data['middle_name'])) ? $data['middle_name'] : null;
        $parameter['last_name'] = (isset($data['last_name'])) ? $data['last_name'] : null;
        $parameter['suffix'] = (isset($data['suffix'])) ? $data['suffix'] : null;
        $parameter['email_id'] = (isset($data['email_id'])) ? $data['email_id'] : null;
        $parameter['verify_code'] = ''; //$this->generateRandomString();
        $parameter['salt'] = $this->generateRandomString();
        $password = $this->generateRandomString();
        $parameter['password'] = md5($password . $parameter['salt']);
        $parameter['genrated_password'] = $password;
        $parameter['source_id'] = 3;
        $parameter['webaccesss_enabled_login'] = '1';
        $parameter['is_active'] = '1';
        $parameter['add_date'] = $data['add_date'];
        $parameter['modified_date'] = $data['modified_date'];
        return $parameter;
    }

    /**
     * Function for get random number
     * @author Icreon Tech - DG
     * @return array
     * @param Int
     */
    public function generateRandomString() {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ23456789";
        srand((double) microtime() * 1000000);
        $i = 0;
        $code = '';

        while ($i <= 7) {
            $num = rand() % 33;
            $tmp = substr($chars, $num, 1);
            $code = $code . $tmp;
            $i++;
        }
        return $code;
    }

    /**
     * Function used to check variable for reservation kiosk
     * @author Icreon Tech - DT
     * @return boolean
     * @param Array
     */
    function getReservationArr($data = array()) {
        $parameter = array();
        $parameter['available'] = (!empty($data['available'])) ? $data['available'] : "";
        $parameter['reserved'] = (!empty($data['reserved'])) ? $data['reserved'] : "";
        $parameter['not_available'] = (!empty($data['not_available'])) ? $data['not_available'] : "";
        $parameter['in_use'] = (!empty($data['in_use'])) ? $data['in_use'] : "";
        $parameter['paused'] = (!empty($data['paused'])) ? $data['paused'] : "";
        $parameter['reservation_method'] = (isset($data['reservation_method'])) ? $data['reservation_method'] : "";
        $parameter['is_wheelchair'] = (isset($data['is_wheelchair'])) ? $data['is_wheelchair'] : "";
        $parameter['history'] = (isset($data['history'])) ? $data['history'] : "";
        $parameter['kiosk_name'] = (isset($data['kiosk_name'])) ? $data['kiosk_name'] : "";
        $parameter['contact_name'] = (isset($data['contact_name'])) ? $data['contact_name'] : "";
        $parameter['curr_time'] = (isset($data['curr_time'])) ? $data['curr_time'] : "";
        $parameter['curr_date'] = (isset($data['curr_date'])) ? $data['curr_date'] : "";
        $parameter['recOffset'] = (isset($data['recOffset'])) ? $data['recOffset'] : "";
        $parameter['recLimit'] = (isset($data['recLimit'])) ? $data['recLimit'] : "";
        $parameter['sortField'] = (isset($data['sortField'])) ? $data['sortField'] : "";
        $parameter['sortOrder'] = (isset($data['sortOrder'])) ? $data['sortOrder'] : "";
        $parameter['grid_main_view'] = (isset($data['grid_main_view'])) ? $data['grid_main_view'] : "";
        return $parameter;
    }

    /**
     * Function used to check variable for reservation params
     * @return Array
     * @param Array
     */
    function getAutoReservationParamArr($data = array()) {
        $parameter = array();
        $parameter['is_wheelchair'] = (isset($data['is_wheelchair'])) ? $data['is_wheelchair'] : "";
        $parameter['kiosk_id'] = (!empty($data['kiosk_id'])) ? $data['kiosk_id'] : "";
        $parameter['curr_time'] = (!empty($data['curr_time'])) ? $data['curr_time'] : "";
        $parameter['curr_date'] = (!empty($data['curr_date'])) ? $data['curr_date'] : "";
        $parameter['auto_reservation'] = (isset($data['auto_reservation'])) ? $data['auto_reservation'] : "";
        $parameter['is_delete'] = (isset($data['is_delete'])) ? $data['is_delete'] : "";
        return $parameter;
    }

    /**
     * Function used to check variable for update reservation params
     * @return Array
     * @param Array
     */
    function getReservationUpdateDataArr($data = array()) {
        $parameter = array();
        $parameter['queue_id'] = (!empty($data['queue_id'])) ? $data['queue_id'] : "";
        $parameter['time_left'] = (isset($data['time_left'])) ? $data['time_left'] : "";
        $parameter['pause_resume'] = (!empty($data['pause_resume'])) ? $data['pause_resume'] : "";
        $parameter['pause_at'] = (!empty($data['pause_at'])) ? $data['pause_at'] : "";
        $parameter['time_started'] = (!empty($data['time_started'])) ? $data['time_started'] : "";
        $parameter['time_end'] = (isset($data['time_end'])) ? $data['time_end'] : "";
        $parameter['time_allocated'] = (isset($data['time_allocated'])) ? $data['time_allocated'] : "";
        $parameter['is_delete'] = (isset($data['is_delete'])) ? $data['is_delete'] : "";
        $parameter['contact_id'] = (isset($data['contact_id'])) ? $data['contact_id'] : "";
        
        $parameter['transaction_id'] = (isset($data['transaction_id'])) ? $data['transaction_id'] : "";
        $parameter['authorize_transaction_id'] = (isset($data['authorize_transaction_id'])) ? $data['authorize_transaction_id'] : "";
        $parameter['payment_mode_id'] = (isset($data['payment_mode_id'])) ? $data['payment_mode_id'] : "";
        $parameter['amount'] = (isset($data['amount'])) ? $data['amount'] : "";
        $parameter['confirmation_code'] = (isset($data['confirmation_code'])) ? $data['confirmation_code'] : "";
        return $parameter;
    }

    /**
     * Function used to check variable for insert reservation params
     * @return Array
     * @param Array
     */
    function getReservationInsertDataArr($data = array()) {
        $parameter = array();
        $parameter['time_allocated'] = (!empty($data['time_allocated'])) ? $data['time_allocated'] : "";
        $parameter['time_end'] = (!empty($data['time_end'])) ? $data['time_end'] : "";
        $parameter['time_left'] = (!empty($data['time_left'])) ? $data['time_left'] : "";
        $parameter['payment_status'] = (!empty($data['payment_status'])) ? $data['payment_status'] : "";
        $parameter['kiosk_id'] = (!empty($data['kiosk_id'])) ? $data['kiosk_id'] : "";
        return $parameter;
    }

    /**
     * Function used to check validation for crm create user form
     * @param Array
     * @return array
     * @author Icreon Tech - AP
     */
    public function getComplimentryInputFilter($data = array()) {
        /* $contactFlag = true;
          $emailFlag = false;
          if($data['email_id'] !=""){
          $contactFlag = false;
          $emailFlag = true;
          } */
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                        'name' => 'first_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_CONTACT',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'kiosk_list',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'payment_mode_cash',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'payment_mode_chk',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'payment_mode_credit',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'last_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_CONTACT',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'email_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMAIL_EMPTY',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'EmailAddress',
                                'options' => array(
                                    'messages' => array(
                                        \Zend\Validator\EmailAddress::INVALID_FORMAT => "EMAIL_INVALID",
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'Db\NoRecordExists',
                                'options' => array(
                                    'table' => 'tbl_usr_users',
                                    'field' => 'email_id',
                                    'adapter' => $this->adapter,
                                    'exclude' => array(
                                        'field' => 'is_delete',
                                        'value' => '1'
                                    ),
                                    'messages' => array(
                                        'recordFound' => 'EMAIL_ID_ALREADY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'csrf',
                        'required' => false,
                        'validators' => array(
                            array(
                                'name' => 'csrf',
                                'options' => array(
                                    'messages' => array(
                                        'notSame' => 'NOT_SAME',
                                    ),
                                ),
                            ),
                        ),
                    )));


            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

}