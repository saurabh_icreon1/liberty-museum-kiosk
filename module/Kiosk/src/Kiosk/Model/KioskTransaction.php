<?php

/**
 * This model is used for find and merge duplicate contact
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace Kiosk\Model;

use Kiosk\Module;
// Add these import statements
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\Adapter\Adapter;

/**
 * This model is used for find and merge duplicate contact
 * @package    User
 * @author     Icreon Tech - DG
 */
class KioskTransaction implements InputFilterAwareInterface {

    protected $inputFilter;
    protected $adapter;

    public function __construct(Adapter $adapter) {

        $this->adapter = $adapter;
    }
    // Add content to these methods:
    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    // Add the following method:
    public function getArrayCopy() {
        return get_object_vars($this);
    }
    /**
     * Function used to check variable for user create contact form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function exchangeArray($data) {
        
    }
	/*****Start Transaction related function*******/
	/**
     * This method is used to add filtering and validation to the form elements of create 
     * @param void
     * @return Array
     * @author Icreon Tech - DG
     */
    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

/**
     * This method is used to return the stored procedure array for final transaction
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getFinalTransactionArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : NULL;
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $returnArr[] = (isset($dataArr['transaction_source_id']) && $dataArr['transaction_source_id'] != '') ? $dataArr['transaction_source_id'] : '';
        $returnArr[] = (isset($dataArr['num_items']) && $dataArr['num_items'] != '') ? $dataArr['num_items'] : 0;
        $returnArr[] = (isset($dataArr['sub_total_amount']) && $dataArr['sub_total_amount'] != '') ? $dataArr['sub_total_amount'] : 0;
        $returnArr[] = (isset($dataArr['total_discount']) && $dataArr['total_discount'] != '') ? $dataArr['total_discount'] : 0;
        $returnArr[] = (isset($dataArr['shipping_amount']) && $dataArr['shipping_amount'] != '') ? $dataArr['shipping_amount'] : 0;
        $returnArr[] = (isset($dataArr['handling_amount']) && $dataArr['handling_amount'] != '') ? $dataArr['handling_amount'] : 0;
        $returnArr[] = (isset($dataArr['total_tax']) && $dataArr['total_tax'] != '') ? $dataArr['total_tax'] : 0;
        $returnArr[] = (isset($dataArr['transaction_amount']) && $dataArr['transaction_amount'] != '') ? $dataArr['transaction_amount'] : 0;
        $returnArr[] = (isset($dataArr['transaction_type']) && $dataArr['transaction_type'] != '') ? $dataArr['transaction_type'] : '2';
        $returnArr[] = (isset($dataArr['transaction_status_id']) && $dataArr['transaction_status_id'] != '') ? $dataArr['transaction_status_id'] : '1';
        $returnArr[] = (isset($dataArr['transaction_date']) && $dataArr['transaction_date'] != '') ? $dataArr['transaction_date'] : '';
        $returnArr[] = (isset($dataArr['shipping_type_id']) && $dataArr['shipping_type_id'] != '') ? $dataArr['shipping_type_id'] : '';
        $returnArr[] = (isset($dataArr['pick_up']) && $dataArr['pick_up'] != '') ? $dataArr['pick_up'] : '0';
        $returnArr[] = (isset($dataArr['coupon_code']) && $dataArr['coupon_code'] != '') ? $dataArr['coupon_code'] : '';
        $returnArr[] = (isset($dataArr['is_free_shipping']) && $dataArr['is_free_shipping'] != '') ? $dataArr['is_free_shipping'] : '0';
        $returnArr[] = (isset($dataArr['added_by']) && $dataArr['added_by'] != '') ? $dataArr['added_by'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : '';
        $returnArr[] = (isset($dataArr['modify_by']) && $dataArr['modify_by'] != '') ? $dataArr['modify_by'] : '';
        $returnArr[] = (isset($dataArr['modify_date']) && $dataArr['modify_date'] != '') ? $dataArr['modify_date'] : '';
        $returnArr[] = (isset($dataArr['campaign_code']) && $dataArr['campaign_code'] != '') ? $dataArr['campaign_code'] : '';
        $returnArr[] = (isset($dataArr['campaign_id']) && $dataArr['campaign_id'] != '') ? $dataArr['campaign_id'] : '';
        $returnArr[] = (isset($dataArr['channel_id']) && $dataArr['channel_id'] != '') ? $dataArr['channel_id'] : '';
        $returnArr[] = (isset($dataArr['don_amount']) && $dataArr['don_amount'] != '') ? $dataArr['don_amount'] : 0;
        $returnArr[] = (isset($dataArr['group_id']) && $dataArr['group_id'] != '') ? $dataArr['group_id'] : '';
        $returnArr[] = (isset($dataArr['total_product_revenue']) && $dataArr['total_product_revenue'] != '') ? $dataArr['total_product_revenue'] : '';
	$returnArr[] = (isset($dataArr['invoice_number']) && $dataArr['invoice_number'] != '') ? $dataArr['invoice_number'] : '';
        return $returnArr;
    }  
    
public function getInsertCartToTransactionArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['cart_id']) && $dataArr['cart_id'] != '') ? $dataArr['cart_id'] : NULL;
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $returnArr[] = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '';
        $returnArr[] = (isset($dataArr['product_type_id']) && $dataArr['product_type_id'] != '') ? $dataArr['product_type_id'] : '';
        $returnArr[] = (isset($dataArr['product_mapping_id']) && $dataArr['product_mapping_id'] != '') ? $dataArr['product_mapping_id'] : '';
        $returnArr[] = (isset($dataArr['product_id']) && $dataArr['product_id'] != '') ? $dataArr['product_id'] : '';
        $returnArr[] = (isset($dataArr['product_name']) && $dataArr['product_name'] != '') ? $dataArr['product_name'] : '';
        $returnArr[] = (isset($dataArr['product_option_name']) && $dataArr['product_option_name'] != '') ? $dataArr['product_option_name'] : '';
        $returnArr[] = (isset($dataArr['product_qty']) && $dataArr['product_qty'] != '') ? $dataArr['product_qty'] : 1;
        $returnArr[] = (isset($dataArr['product_amount']) && $dataArr['product_amount'] != '') ? $dataArr['product_amount']-$dataArr['membership_indi_discount']: 0;
        $returnArr[] = (isset($dataArr['product_subtotal']) && $dataArr['product_subtotal'] != '') ? $dataArr['product_subtotal']-$dataArr['membership_discount'] : 0;
        $returnArr[] = (isset($dataArr['product_discount']) && $dataArr['product_discount'] != '') ? $dataArr['product_discount'] : 0;
        $returnArr[] = (isset($dataArr['product_taxable_amount']) && $dataArr['product_taxable_amount'] != '') ? $dataArr['product_taxable_amount'] : 0;
		if(isset($dataArr['shipping_discount_in_amount']) && $dataArr['shipping_discount_in_amount']!='')
		{
        $returnArr[] = (isset($dataArr['shipping_price']) && $dataArr['shipping_price'] != '') ? $dataArr['shipping_price']-$dataArr['shipping_discount_in_amount'] : 0;
		}else{
		$returnArr[] = (isset($dataArr['shipping_price']) && $dataArr['shipping_price'] != '') ? $dataArr['shipping_price'] : 0;
		}
        $returnArr[] = (isset($dataArr['shipping_handling_price']) && $dataArr['shipping_handling_price'] != '') ? $dataArr['shipping_handling_price']+$dataArr['surcharge_price'] : 0;
        $returnArr[] = (isset($dataArr['product_total']) && $dataArr['product_total'] != '') ? $dataArr['product_total'] : 0;
        $returnArr[] = (isset($dataArr['purchase_date']) && $dataArr['purchase_date'] != '') ? $dataArr['purchase_date'] : DATE_TIME_FORMAT;
        $returnArr[] = (isset($dataArr['is_free_shipping']) && $dataArr['is_free_shipping'] != '') ? $dataArr['is_free_shipping'] : '';
        if ($dataArr['product_type_id'] != 4) {
            $returnArr[] = (isset($dataArr['is_shippable']) && $dataArr['is_shippable'] == '1') ? '1' : '10';
        } else {
            $returnArr[] = '1';
        }
        $returnArr[] = (isset($dataArr['added_by']) && $dataArr['added_by'] != '') ? $dataArr['added_by'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : DATE_TIME_FORMAT;
        $returnArr[] = (isset($dataArr['modify_by']) && $dataArr['modify_by'] != '') ? $dataArr['modify_by'] : '';
        $returnArr[] = (isset($dataArr['modify_date']) && $dataArr['modify_date'] != '') ? $dataArr['modify_date'] : DATE_TIME_FORMAT;
        $returnArr[] = (isset($dataArr['type_of_product']) && $dataArr['type_of_product'] != '') ? $dataArr['type_of_product'] : '';
        $returnArr[] = (isset($dataArr['item_name']) && $dataArr['item_name'] != '') ? $dataArr['item_name'] : '';
        $returnArr[] = (isset($dataArr['item_info']) && $dataArr['item_info'] != '') ? $dataArr['item_info'] : '';
        $returnArr[] = (isset($dataArr['manifest_info']) && $dataArr['manifest_info'] != '') ? $dataArr['manifest_info'] : '';
        $returnArr[] = (isset($dataArr['manifest_type']) && $dataArr['manifest_type'] != '') ? $dataArr['manifest_type'] : '';
        return $returnArr;
    }    
    
/**
     * This method is used to return the stored procedure array for final transaction billing shipping address info
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getTransactionBillingShipping($dataArr = array()) {
        $dataArr['source_type'] = !empty($dataArr['source_type']) ? $dataArr['source_type'] : '';
        $returnArr = array();
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $returnArr[] = (isset($dataArr['billing_title']) && $dataArr['billing_title'] != '') ? $dataArr['billing_title'] : '';
        $returnArr[] = (isset($dataArr['billing_first_name']) && $dataArr['billing_first_name'] != '') ? $dataArr['billing_first_name'] : '';
        $returnArr[] = (isset($dataArr['billing_last_name']) && $dataArr['billing_last_name'] != '') ? $dataArr['billing_last_name'] : '';
        $returnArr[] = (isset($dataArr['billing_company']) && $dataArr['billing_company'] != '') ? $dataArr['billing_company'] : '';
        if ($dataArr['source_type'] == 'frontend') {
            $address1 = !empty($dataArr['billing_address_1']) ? $dataArr['billing_address_1'] : '';
            $address2 = !empty($dataArr['billing_address_2']) ? "," . $dataArr['billing_address_2'] : '';

            $returnArr[] = $address1 . ' ' . $address2;
        } else {
            $returnArr[] = (isset($dataArr['billing_address']) && $dataArr['billing_address'] != '') ? $dataArr['billing_address'] : '';
        }
        $returnArr[] = (isset($dataArr['billing_city']) && $dataArr['billing_city'] != '') ? $dataArr['billing_city'] : '';
        $returnArr[] = (isset($dataArr['billing_state']) && $dataArr['billing_state'] != '') ? $dataArr['billing_state'] : '';
        $returnArr[] = (isset($dataArr['billing_zip_code']) && $dataArr['billing_zip_code'] != '') ? $dataArr['billing_zip_code'] : '';
        $returnArr[] = (isset($dataArr['billing_country']) && $dataArr['billing_country'] != '') ? $dataArr['billing_country'] : '';
        $returnArr[] = (isset($dataArr['billing_phone']) && $dataArr['billing_phone'] != '') ? $dataArr['billing_phone'] : '';
        $returnArr[] = (isset($dataArr['is_apo_po']) && $dataArr['is_apo_po'] != '') ? $dataArr['is_apo_po'] : '';
        $returnArr[] = (isset($dataArr['shipping_title']) && $dataArr['shipping_title'] != '') ? $dataArr['shipping_title'] : '';
        $returnArr[] = (isset($dataArr['shipping_first_name']) && $dataArr['shipping_first_name'] != '') ? $dataArr['shipping_first_name'] : '';
        $returnArr[] = (isset($dataArr['shipping_last_name']) && $dataArr['shipping_last_name'] != '') ? $dataArr['shipping_last_name'] : '';
        $returnArr[] = (isset($dataArr['shipping_company']) && $dataArr['shipping_company'] != '') ? $dataArr['shipping_company'] : '';
        if ($dataArr['source_type'] == 'frontend') {
            $address1ship = !empty($dataArr['shipping_address_1']) ? $dataArr['shipping_address_1'] : '';
            $address2ship = !empty($dataArr['shipping_address_2']) ? "," . $dataArr['shipping_address_2'] : '';
            $returnArr[] = $address1ship . ' ' . $address2ship;
        } else {
            $returnArr[] = (isset($dataArr['shipping_address']) && $dataArr['shipping_address'] != '') ? $dataArr['shipping_address'] : '';
        }
        $returnArr[] = (isset($dataArr['shipping_city']) && $dataArr['shipping_city'] != '') ? $dataArr['shipping_city'] : '';
        $returnArr[] = (isset($dataArr['shipping_state']) && $dataArr['shipping_state'] != '') ? $dataArr['shipping_state'] : '';
        $returnArr[] = (isset($dataArr['shipping_zip_code']) && $dataArr['shipping_zip_code'] != '') ? $dataArr['shipping_zip_code'] : '';
        $returnArr[] = (isset($dataArr['shipping_country']) && $dataArr['shipping_country'] != '') ? $dataArr['shipping_country'] : '';
        $returnArr[] = (isset($dataArr['shipping_phone']) && $dataArr['shipping_phone'] != '') ? $dataArr['shipping_phone'] : '';
        $returnArr[] = (isset($dataArr['added_by']) && $dataArr['added_by'] != '') ? $dataArr['added_by'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : DATE_TIME_FORMAT;
        $returnArr[] = (isset($dataArr['modify_by']) && $dataArr['modify_by'] != '') ? $dataArr['modify_by'] : '';
        $returnArr[] = (isset($dataArr['modify_date']) && $dataArr['modify_date'] != '') ? $dataArr['modify_date'] : DATE_TIME_FORMAT;
        return $returnArr;
    }    
     /**
     * This method is used to get shipping method 
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getShippingMethodArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['pb_shipping_id']) && $dataArr['pb_shipping_id'] != '') ? $dataArr['pb_shipping_id'] : '1';
        $returnArr[] = (isset($dataArr['pb_shipping_type_id']) && $dataArr['pb_shipping_type_id'] != '') ? $dataArr['pb_shipping_type_id'] : '';
        $returnArr[] = (isset($dataArr['carrier_id']) && $dataArr['carrier_id'] != '') ? $dataArr['carrier_id'] : '';
        $returnArr[] = (isset($dataArr['service_type']) && $dataArr['service_type'] != '') ? $dataArr['service_type'] : '';
        return $returnArr;
    }   
    
    /**
     * This method is used to return the stored procedure array for update user membership
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getTotalUserPurchaseProductAmountArr($dataArr = array()) {
        $returnArr = array();
        $returnArr['user_id'] = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '';
        $returnArr['from_date'] = (isset($dataArr['from_date']) && $dataArr['from_date'] != '') ? $dataArr['from_date'] : '';
        $returnArr['to_date'] = (isset($dataArr['to_date']) && $dataArr['to_date'] != '') ? $dataArr['to_date'] : '';
        return $returnArr;
    }    
    /**
     * This method is used to check holiday date
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getHolidayArr($dataArr = array()) {

        $returnArr = array();
        $returnArr[] = (isset($dataArr['holiday_date']) && $dataArr['holiday_date'] != '') ? $dataArr['holiday_date'] : '';
        return $returnArr;
    }    
    
    /**
     * This method is used to return the stored procedure array for final transaction payment info
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getTransactionPaymentArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $returnArr[] = (isset($dataArr['payment_mode_id']) && $dataArr['payment_mode_id'] != '') ? $dataArr['payment_mode_id'] : '';
        $returnArr[] = (isset($dataArr['amount']) && $dataArr['amount'] != '') ? $dataArr['amount'] : '';
        $returnArr[] = (isset($dataArr['cheque_type_id']) && $dataArr['cheque_type_id'] != '') ? $dataArr['cheque_type_id'] : '';
        $returnArr[] = (isset($dataArr['cheque_number']) && $dataArr['cheque_number'] != '') ? $dataArr['cheque_number'] : '';
        $returnArr[] = (isset($dataArr['id_type_id']) && $dataArr['id_type_id'] != '') ? $dataArr['id_type_id'] : '';
        $returnArr[] = (isset($dataArr['id_number']) && $dataArr['id_number'] != '') ? $dataArr['id_number'] : '';
        $returnArr[] = (isset($dataArr['profile_id']) && $dataArr['profile_id'] != '') ? $dataArr['profile_id'] : '';
        $returnArr[] = (isset($dataArr['payment_id']) && $dataArr['payment_id'] != '') ? $dataArr['payment_id'] : '';
        $returnArr[] = (isset($dataArr['billing_id']) && $dataArr['billing_id'] != '') ? $dataArr['billing_id'] : '';
        $returnArr[] = (isset($dataArr['address']) && $dataArr['address'] != '') ? $dataArr['address'] : '';
        $returnArr[] = (isset($dataArr['city']) && $dataArr['city'] != '') ? $dataArr['city'] : '';
        $returnArr[] = (isset($dataArr['state']) && $dataArr['state'] != '') ? $dataArr['state'] : '';
        $returnArr[] = (isset($dataArr['country_id']) && $dataArr['country_id'] != '') ? $dataArr['country_id'] : '';
        $returnArr[] = (isset($dataArr['postal_code']) && $dataArr['postal_code'] != '') ? $dataArr['postal_code'] : '';
        $returnArr[] = (isset($dataArr['authorize_transaction_id']) && $dataArr['authorize_transaction_id'] != '') ? $dataArr['authorize_transaction_id'] : '';
        $returnArr[] = (isset($dataArr['cheque_date']) && $dataArr['cheque_date'] != '') ? $dataArr['cheque_date'] : '';
        $returnArr[] = (isset($dataArr['account_no']) && $dataArr['account_no'] != '') ? $dataArr['account_no'] : '';
        $returnArr[] = (isset($dataArr['added_by']) && $dataArr['added_by'] != '') ? $dataArr['added_by'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : DATE_TIME_FORMAT;
        $returnArr[] = (isset($dataArr['modify_by']) && $dataArr['modify_by'] != '') ? $dataArr['modify_by'] : '';
        $returnArr[] = (isset($dataArr['modify_date']) && $dataArr['modify_date'] != '') ? $dataArr['modify_date'] : DATE_TIME_FORMAT;
		$returnArr[] = (isset($dataArr['credit_card_type_id']) && $dataArr['credit_card_type_id'] != '') ? $dataArr['credit_card_type_id'] : '';
        return $returnArr;
    }    
    
    /**
     * This method is used to return the stored procedure array for receive payment by credit array
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getTransactionPaymentReceiveArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['transaction_payment_id']) && $dataArr['transaction_payment_id'] != '') ? $dataArr['transaction_payment_id'] : '';
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $returnArr[] = (isset($dataArr['amount']) && $dataArr['amount'] != '') ? $dataArr['amount'] : 0;
        $returnArr[] = (isset($dataArr['id']) && $dataArr['id'] != '') ? $dataArr['id'] : '';
        $returnArr[] = (isset($dataArr['authorize_transaction_id']) && $dataArr['authorize_transaction_id'] != '') ? $dataArr['authorize_transaction_id'] : '';
        $returnArr[] = (isset($dataArr['added_by']) && $dataArr['added_by'] != '') ? $dataArr['added_by'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : DATE_TIME_FORMAT;
        $returnArr[] = (isset($dataArr['modify_by']) && $dataArr['modify_by'] != '') ? $dataArr['modify_by'] : '';
        $returnArr[] = (isset($dataArr['modify_date']) && $dataArr['modify_date'] != '') ? $dataArr['modify_date'] : DATE_TIME_FORMAT;
        return $returnArr;
    }    
    
    /**
     * Function used to get array of elements need to call remove transaction
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getArrayForRemoveTransaction($dataArr) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        return $returnArr;
    }    
}