<?php

/**
 * This model is used for kiosk management
 * @package    Kiosk
 * @author     Icreon Tech - SK
 */

namespace Kiosk\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This model is used for kiosk management
 * @package    User
 * @author     Icreon Tech - SK
 */
class KioskManagementTable {

    protected $tableGateway;
    protected $dbAdapter;
    protected $connection;
    
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
        $this->connection = $this->tableGateway->getAdapter()->getDriver()->getConnection();
    }
    
    /**
     * Function for to insert crm users
     * @param array
     * @return boolean
     * @author Icreon Tech - AP
     */
    public function insertKiosk($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_kiosk_insertKiosk(?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['kiosk_name']);
            $stmt->getResource()->bindParam(2, $param['kiosk_ip']);
            $stmt->getResource()->bindParam(3, $param['is_status']);
            $stmt->getResource()->bindParam(4, $param['kiosk_capacity']);
            $stmt->getResource()->bindParam(5, $param['is_wheelchair']);
            $stmt->getResource()->bindParam(6, $param['is_ada']);
            $stmt->getResource()->bindParam(7, $param['kiosk_notes']);
            $stmt->getResource()->bindParam(8, $param['kiosk_shutoff']);
            $stmt->getResource()->bindParam(9, $param['added_by']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * This function is used to get the kiosk seached records
     * @param this will be an array.
     * @return this will return search result of kiosk
     * @author Icreon Tech -AP
     */
    public function searchKiosk($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_kiosk_getKioskSearch(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, (isset($param['name_or_ip']) and trim($param['name_or_ip']) != "") ? addslashes($param['name_or_ip']) : "");
            $stmt->getResource()->bindParam(2, $param['status']);
            $stmt->getResource()->bindParam(3, $param['startIndex']);
            $stmt->getResource()->bindParam(4, $param['recordLimit']);
            $stmt->getResource()->bindParam(5, $param['sortField']);
            $stmt->getResource()->bindParam(6, $param['sortOrder']);
            //$stmt->getResource()->bindParam(8, $param['currentTime']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            //asd($resultSet);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    /**
     * Function for to get kiosk deatails
     * @param Array
     * @return Array
     * @author Icreon Tech - AP 
     */
    public function getkioskDetails($param = array()) {
         $isWheelchair = isset($param['is_wheelchair'])?$param['is_wheelchair']:"";
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_kiosk_getKioskInfo(?,?)');
            $stmt->getResource()->bindParam(1, $param['kiosk_id']);
            $stmt->getResource()->bindParam(2, $isWheelchair);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }
     /**
     * Function for to delete contact
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function deleteKiosk($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_kiosk_deleteKiosk(?)');
            $stmt->getResource()->bindParam(1, $params['kiosk_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    /**
     * Function for to update crm users
     * @param Array
     * @return Array
     * @author Icreon Tech - AP 
     */
    public function updateKiosk($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_kiosk_updateKiosk(?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['kiosk_id']);
            $stmt->getResource()->bindParam(2, $param['kiosk_name']);
            $stmt->getResource()->bindParam(3, $param['kiosk_ip']);
            $stmt->getResource()->bindParam(4, $param['is_status']);
            $stmt->getResource()->bindParam(5, $param['kiosk_capacity']);
            $stmt->getResource()->bindParam(6, $param['is_wheelchair']);
            $stmt->getResource()->bindParam(7, $param['is_ada']);
            $stmt->getResource()->bindParam(8, $param['kiosk_notes']);
            $stmt->getResource()->bindParam(9, $param['kiosk_shutoff']);
            $stmt->getResource()->bindParam(10, $param['modified_by']);
            $stmt->getResource()->bindParam(11, $param['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    /**
     * Function for get All Module
     * @author Icreon Tech - DG
     * @return array
     */
    public function getKioskModules() {
        //$stmt = $this->dbAdapter->createStatement();
        $result = $this->connection->execute('CALL usp_com_getKioskModules');
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();

        if (!$resultSet) {
            throw new \Exception("Could not find Modules record.");
        }
        return $resultSet;
    }
    /**
     * Function for get All Config of Modules
     * @author Icreon Tech - DG
     * @return array
     */
    public function getKioskModulesConfig() {
        //$stmt = $this->dbAdapter->createStatement();
        $result = $this->connection->execute('CALL usp_ksk_kioskModuleConfigInfo');
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();

        if (!$resultSet) {
            throw new \Exception("Could not find Modules record.");
        }
        return $resultSet;
    }
     /**
     * Function for inserting defualt valuse in table
     * @param Array
     * @return Array
     * @author Icreon Tech - AP 
     */
    public function insertKioskModuleInfo($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ksk_insertKioskModuleInfo(?,?,?)');
            $stmt->getResource()->bindParam(1, $param['kiosk_id']);
            $stmt->getResource()->bindParam(2, $param['kiosk_module_id']);
            $stmt->getResource()->bindParam(3, $param['added_by']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }
    /**
     * Function for inserting defualt valuse in table
     * @param Array
     * @return Array
     * @author Icreon Tech - AP 
     */
    public function insertKioskModuleConfigInfo($param = array()) { //asd($param);
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ksk_insertKioskModuleConfigInfo(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['kiosk_module_mapping_id']);
            $stmt->getResource()->bindParam(2, $param['kiosk_module_id']);
            $stmt->getResource()->bindParam(3, $param['kiosk_module_setting_id']);
            $stmt->getResource()->bindParam(4, $param['added_by']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * This function wused to check the valid login for kiosk system
     * @param array
     * @return return true/false
     * @author Icreon Tech - SK
     */    
    public function getLoginKioskId($params = array()){
    try {
            $ipAddress = isset($params['ip_address']) ? $params['ip_address'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_kiosk_getKioskId(?)");
            $stmt->getResource()->bindParam(1, $ipAddress);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }               
    }
    
   /**
     * This function wused to check the valid login for kiosk system
     * @param array
     * @return return true/false
     * @author Icreon Tech - SK
     */
    
    public function checkValidLogin($params = array()){
        try { 
            
            $bookingDate = date("Y-m-d",strtotime(DATE_TIME_FORMAT));
            $confirmationCode = isset($params['confirmation_code']) ? $params['confirmation_code'] : '';
            $emailId = isset($params['email_id']) ? $params['email_id'] : '';
            $loginTime = isset($params['login_time']) ? $params['login_time'] : '';
            $kioskId = isset($params['kiosk_id']) ? $params['kiosk_id'] : '';
            
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_kiosk_check_login(?,?,?,?,?)");
            $stmt->getResource()->bindParam(1, $confirmationCode);
            $stmt->getResource()->bindParam(2, $emailId);
            $stmt->getResource()->bindParam(3, $loginTime);
            $stmt->getResource()->bindParam(4, $kioskId);
            $stmt->getResource()->bindParam(5, $bookingDate);
            
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }       
    }    
    
   /**
     * Function for get user data 
     * @author Icreon Tech - DG
     * @return Int
     * @param Array
     */
    public function getUser($userArr) {
        if (isset($userArr)) {

            if (isset($userArr['email_id']) && $userArr['email_id'] != '') {
                $email_id = $userArr['email_id'];
            } else {
                $email_id = '';
            }

            if (isset($userArr['username']) && $userArr['username'] != '') {
                $username = $userArr['username'];
            } else {
                $username = '';
            }

            if (isset($userArr['user_id']) && $userArr['user_id'] != '') {
                $id = (int) $userArr['user_id'];
            } else {
                $id = '';
            }

            if (isset($userArr['verify_code']) && $userArr['verify_code'] != '') {
                $verify_code = $userArr['verify_code'];
            } else {
                $verify_code = '';
            }

			if (isset($userArr['include_deleted']) && $userArr['include_deleted'] != '') {
                $include_deleted = $userArr['include_deleted'];
            } else {
                $include_deleted = '';
            }

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pos_usr_getUser(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $username);
            $stmt->getResource()->bindParam(2, $email_id);
            $stmt->getResource()->bindParam(3, $id);
            $stmt->getResource()->bindParam(4, $verify_code);
			$stmt->getResource()->bindParam(5, $include_deleted);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if ($resultSet) {
                return $resultSet[0];
            } else {
                return false;
            }
        }
    }     
    
    public function getUserKioskDetails($params = array()){
       try { 
            
            $bookingDate = date("Y-m-d",strtotime(DATE_TIME_FORMAT));
            $confirmationCode = isset($params['confirmation_code']) ? $params['confirmation_code'] : '';
            $emailId = isset($params['email_id']) ? $params['email_id'] : '';

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_kiosk_get_user_kiosk_details(?,?,?)");
            $stmt->getResource()->bindParam(1, $confirmationCode);
            $stmt->getResource()->bindParam(2, $emailId);
            $stmt->getResource()->bindParam(3, $bookingDate);
            
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }     
    }
    
    /**
     * Function for insert login log
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function insertLogins($loginsArr) {
        if (!empty($loginsArr)) {
            if (isset($loginsArr['user_id'])) {
                $user_id = $loginsArr['user_id'];
            } else {
                $user_id = '';
            }
            if (isset($loginsArr['email_id'])) {
                $email_id = $loginsArr['email_id'];
            } else {
                $email_id = '';
            }
            if (isset($loginsArr['login_date'])) {
                $login_date = $loginsArr['login_date'];
            } else {
                $login_date = '';
            }
            if (isset($loginsArr['login_state'])) {
                $login_state = $loginsArr['login_state'];
            } else {
                $login_state = '';
            }
            if (isset($loginsArr['active'])) {
                $active = $loginsArr['active'];
            } else {
                $active = 0;
            }
            if (isset($loginsArr['ip'])) {
                $ip = $loginsArr['ip'];
            } else {
                $ip = '';
            }
            if (isset($loginsArr['last_update_date'])) {
                $last_update_date = $loginsArr['last_update_date'];
            } else {
                $last_update_date = '';
            }
            if (isset($loginsArr['logout_date'])) {
                $logout_date = $loginsArr['logout_date'];
            } else {
                $logout_date = '';
            }
            if (isset($loginsArr['source_id'])) {
                $source_id = $loginsArr['source_id'];
            } else {
                $source_id = '';
            }

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ksk_usr_insertUserLog(?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $user_id);
            $stmt->getResource()->bindParam(2, $email_id);
            $stmt->getResource()->bindParam(3, $login_date);
            $stmt->getResource()->bindParam(4, $login_state);
            $stmt->getResource()->bindParam(5, $active);
            $stmt->getResource()->bindParam(6, $ip);
            $stmt->getResource()->bindParam(7, $last_update_date);
            $stmt->getResource()->bindParam(8, $logout_date);
            $stmt->getResource()->bindParam(9, $source_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if ($resultSet) {
                return $resultSet[0];
            } else {
                return false;
            }
        }
    }
    public function getKioskNextLoginTime($params = array()){
    try {
            $ipAddress = isset($params['ip_address']) ? $params['ip_address'] : '';
            $loginTime = isset($params['login_time']) ? $params['login_time'] : '';
            $bookingDate = date("Y-m-d",strtotime(DATE_TIME_FORMAT));
            
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_kiosk_getKioskNextLoginTime(?,?,?)");
            $stmt->getResource()->bindParam(1, $ipAddress);
            $stmt->getResource()->bindParam(2, $loginTime);
            $stmt->getResource()->bindParam(3, $bookingDate);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            //asd($resultSet);
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }               
    }
}