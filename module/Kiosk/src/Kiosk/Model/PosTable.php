<?php

/**
 * This model is used for kiosk management
 * @package    Kiosk
 * @author     Icreon Tech - SK
 */

namespace Kiosk\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This model is used for kiosk management
 * @package    User
 * @author     Icreon Tech - SK
 */
class PosTable {

    protected $tableGateway;
    protected $dbAdapter;
    protected $connection;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
        $this->connection = $this->tableGateway->getAdapter()->getDriver()->getConnection();
    }

    /**
     * Function for to insert crm users
     * @param array
     * @return boolean
     * @author Icreon Tech - AP
     */
    public function insertKiosk($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_kiosk_insertKiosk(?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['kiosk_name']);
            $stmt->getResource()->bindParam(2, $param['kiosk_ip']);
            $stmt->getResource()->bindParam(3, $param['status']);
            $stmt->getResource()->bindParam(4, $param['kiosk_capacity']);
            $stmt->getResource()->bindParam(5, $param['is_wheelchair']);
            $stmt->getResource()->bindParam(6, $param['is_ada']);
            $stmt->getResource()->bindParam(7, $param['kiosk_notes']);
            $stmt->getResource()->bindParam(8, $param['added_by']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the kiosk seached records
     * @param this will be an array.
     * @return this will return search result of kiosk
     * @author Icreon Tech -AP
     */
    public function searchKioskModule($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ksk_getKioskModuleSearch(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, (isset($param['name_or_ip']) and trim($param['name_or_ip']) != "") ? addslashes($param['name_or_ip']) : "");
            $stmt->getResource()->bindParam(2, $param['status']);
            $stmt->getResource()->bindParam(3, $param['startIndex']);
            $stmt->getResource()->bindParam(4, $param['recordLimit']);
            $stmt->getResource()->bindParam(5, $param['sortField']);
            $stmt->getResource()->bindParam(6, $param['sortOrder']);
            //$stmt->getResource()->bindParam(8, $param['currentTime']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    public function insertPosContact($param = array()) {
        try {
            $isActive = '1';
            $isActiveted = '1';
            $emailVerified = '1';            
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pos_insertContact(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['contact_type']);
            $stmt->getResource()->bindParam(2, $param['first_name']);
            $stmt->getResource()->bindParam(3, $param['last_name']);
            $stmt->getResource()->bindParam(4, $param['email']);
            $stmt->getResource()->bindParam(5, $param['no_email']);
            $stmt->getResource()->bindParam(6, $param['password']);
            $stmt->getResource()->bindParam(7, $param['salt']);            
            $stmt->getResource()->bindParam(8, $param['login_enable']); 
            $stmt->getResource()->bindParam(9, $param['add_date']);
            $stmt->getResource()->bindParam(10, $param['modified_date']);                        
            $stmt->getResource()->bindParam(11, $param['username']);
            $stmt->getResource()->bindParam(12, $param['verify_code']); 
            $stmt->getResource()->bindParam(13, $isActive);
            $stmt->getResource()->bindParam(14, $isActiveted);
            $stmt->getResource()->bindParam(15, $emailVerified);   
            $stmt->getResource()->bindParam(16, $param['source_id']);   

            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }
/**
     * Function for to insert contact 
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     *//*
    public function insertContact($params = array()) {
        $isActive = '1';
        $isActiveted = '1';
        $emailVerified = '1';

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_insertContact(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $params['contact_type']);
        $stmt->getResource()->bindParam(2, $params['title']);
        $stmt->getResource()->bindParam(3, $params['first_name']);
        $stmt->getResource()->bindParam(4, $params['middle_name']);
        $stmt->getResource()->bindParam(5, $params['last_name']);
        $stmt->getResource()->bindParam(6, $params['suffix']);
        $stmt->getResource()->bindParam(7, $params['email_id']);
        $stmt->getResource()->bindParam(8, $params['no_email_address']);
        $stmt->getResource()->bindParam(9, $params['password']);
        $stmt->getResource()->bindParam(10, $params['salt']);
        $stmt->getResource()->bindParam(11, $params['demographics_gender']);
        $stmt->getResource()->bindParam(12, $params['upload_profile_image_id']);
        $stmt->getResource()->bindParam(13, $params['demographics_marital_status']);
        $stmt->getResource()->bindParam(14, $params['demographics_age_limit']);
        $stmt->getResource()->bindParam(15, $params['demographics_income_range']);
        $stmt->getResource()->bindParam(16, $params['demographics_ethnicity']);
        $stmt->getResource()->bindParam(17, $params['demographics_nationality']);
        $stmt->getResource()->bindParam(18, $params['source']);
        $stmt->getResource()->bindParam(19, $params['webaccesss_security_question']);
        $stmt->getResource()->bindParam(20, $params['webaccesss_security_answer']);
        $stmt->getResource()->bindParam(21, $params['webaccesss_enabled_login']);
        $stmt->getResource()->bindParam(22, $params['add_date']);
        $stmt->getResource()->bindParam(23, $params['modified_date']);
        $stmt->getResource()->bindParam(24, $params['corporate_company_name']);
        $stmt->getResource()->bindParam(25, $params['corporate_legal_name']);
        $stmt->getResource()->bindParam(26, $params['corporate_sic_code']);
        $stmt->getResource()->bindParam(27, $params['username']);
        $stmt->getResource()->bindParam(28, $params['don_not_have_website']);
        $stmt->getResource()->bindParam(29, $params['verify_code']);
        $stmt->getResource()->bindParam(30, $isActive);
        $stmt->getResource()->bindParam(31, $isActiveted);
        $stmt->getResource()->bindParam(32, $emailVerified);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            return false;
        }
    }*/
    /**
     * This function will check email exits
     * @param array
     * @return this will return boolean
     * @author Icreon Tech -DG
     */
    public function isEmailExits($params = array()) {
        try {
            $emailId = isset($params['email_id']) ? $params['email_id'] : null;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pos_usr_isEmailExits(?)');
            $stmt->getResource()->bindParam(1, $emailId);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function used to get user searches
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function updateContactId($params = array()) {
        $userId = isset($params['user_id']) ? $params['user_id'] : null;
        $curDate = date("Ym");
        $contactId = $curDate . "" . str_pad($userId, 10, 0, STR_PAD_LEFT);
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_pos_usr_updateContactId(?,?)');
        $stmt->getResource()->bindParam(1, $userId);
        $stmt->getResource()->bindParam(2, $contactId);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for get membership data on the base of id
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function getMembershipById($membershipData) {
      //  asd($membershipData,2);
    //   echo $procquesmarkapp = $this->appendQuestionMars(count($membershipData));
	    $is_ellis_default =  (isset($membershipData[1]) && $membershipData[1]!='')? $membershipData[1]:'0';  
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_pos_set_getMembershipDetails(?,?)");
        $stmt->getResource()->bindParam(1, $membershipData[0]);
		$stmt->getResource()->bindParam(2, $membershipData[1]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet[0];
        else
            return false;
    }


    /**
     * Function for insert user membership data
     * @author Icreon Tech - DG
     * @return Int
     * @param Array
     */
    public function saveUserMembership($params) {
        $userId = isset($params['user_id']) ? $params['user_id'] : null;
        $membershipId = isset($params['membership_id']) ? $params['membership_id'] : null;
        $membershipDateFrom = isset($params['membership_date_from']) ? $params['membership_date_from'] : null;
        $membershipDateTo = isset($params['membership_date_to']) ? $params['membership_date_to'] : null;
        $stmt = $this->dbAdapter->createStatement();
        
        $stmt->prepare("CALL usp_pos_usr_insertUserMembership(?,?,?,?)");
        $stmt->getResource()->bindParam(1, $userId);
        $stmt->getResource()->bindParam(2, $membershipId);
        $stmt->getResource()->bindParam(3, $membershipDateFrom);
        $stmt->getResource()->bindParam(4, $membershipDateTo);
        $result = $stmt->execute();
        $statement = $result->getResource();
        
        // - start
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            $lastInsertedId = $resultSet['0']['LAST_INSERT_MEMBERSHIP_ID'];
        else 
            $lastInsertedId = "";

        if(isset($lastInsertedId) and trim($lastInsertedId) != "") { 
            $this->insertUserMembershipCards(trim($lastInsertedId),$params);
        }
        // - end
    }
    /**
     * Function for to insert contact address info
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function insertAddressInfo($params = array()) {
        try {

            $stmt = $this->dbAdapter->createStatement();
            $title = isset($params['title']) && !is_null($params['title']) ? $params['title'] : '';
            $firstName = isset($params['first_name']) && !is_null($params['first_name']) ? addslashes($params['first_name']) : '';
          //  $middleName = isset($params['middle_name']) && !is_null($params['middle_name']) ? $params['middle_name'] : '';
            $lastName = isset($params['last_name']) && !is_null($params['last_name']) ? addslashes($params['last_name']) : '';
            
            $addressinfo_location_type = isset($params['addressinfo_location_type']) && !is_null($params['addressinfo_location_type']) ? $params['addressinfo_location_type'] : '';
            $addressinfo_location = isset($params['addressinfo_location']) && !is_null($params['addressinfo_location']) ? $params['addressinfo_location'] : '';
            $addressinfo_street_address_1 = isset($params['addressinfo_street_address_1']) && !is_null($params['addressinfo_street_address_1']) ? rtrim($params['addressinfo_street_address_1'], ',') : '';
            $addressinfo_street_address_2 = isset($params['addressinfo_street_address_2']) && !is_null($params['addressinfo_street_address_2']) ? rtrim($params['addressinfo_street_address_2'], ',') : '';
            $addressinfo_city = isset($params['addressinfo_city']) && !is_null($params['addressinfo_city']) ? $params['addressinfo_city'] : '';
            $addressinfo_state = isset($params['addressinfo_state']) && !is_null($params['addressinfo_state']) ? $params['addressinfo_state'] : '';
            $addressinfo_zip = isset($params['addressinfo_zip']) && !is_null($params['addressinfo_zip']) ? $params['addressinfo_zip'] : '';
            $addressinfo_country = isset($params['addressinfo_country']) && !is_null($params['addressinfo_country']) ? $params['addressinfo_country'] : '';
            $address_type = isset($params['address_type']) && !is_null($params['address_type']) ? $params['address_type'] : '';
            $params['add_date'] = DATE_TIME_FORMAT;
            $params['modified_date'] = DATE_TIME_FORMAT;
            $stmt->prepare('CALL usp_pos_usr_insertAddressInformations(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['user_id']);
            $stmt->getResource()->bindParam(2, $addressinfo_location_type);
            $stmt->getResource()->bindParam(3, $addressinfo_location);
            $stmt->getResource()->bindParam(4, $addressinfo_street_address_1);
            $stmt->getResource()->bindParam(5, $addressinfo_street_address_2);
            $stmt->getResource()->bindParam(6, $addressinfo_city);
            $stmt->getResource()->bindParam(7, $addressinfo_state);
            $stmt->getResource()->bindParam(8, $addressinfo_zip);
            $stmt->getResource()->bindParam(9, $addressinfo_country);
            $stmt->getResource()->bindParam(10, $params['add_date']);
            $stmt->getResource()->bindParam(11, $params['modified_date']);
            $stmt->getResource()->bindParam(12, $address_type);
            $stmt->getResource()->bindParam(13, $title);
            $stmt->getResource()->bindParam(14, $firstName);
            $stmt->getResource()->bindParam(15, $lastName);
            //$stmt->getResource()->bindParam(16, $lastName);
            $result = $stmt->execute();
            $statement = $result->getResource();
            //$resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();            
            return true;
        } catch (Exception $e) {
            return false;
        }
    } 
    
    /**
    * This function is used to insert the records into the change log table
    * @params this will be an array.
    * @return this will return true
    * @author Icreon Tech -SR
    */ 
    public function insertChangeLog($params = array())
    {
		try {
	            $stmt = $this->dbAdapter->createStatement();
	            $stmt->prepare('CALL usp_pos_pas_insertChangeLog(?,?,?,?,?)');
	            $stmt->getResource()->bindParam(1, $params['table_name']);
	            $stmt->getResource()->bindParam(2, $params['activity']);
	            $stmt->getResource()->bindParam(3, $params['id']);
	            $stmt->getResource()->bindParam(4, $params['crm_user_id']);
	            $stmt->getResource()->bindParam(5, $params['added_date']);
	            $result = $stmt->execute();
	            $statement = $result->getResource();
	          //  $resultSet = $statement->fetchAll(\PDO::FETCH_OBJ);
	            $statement->closeCursor();

            return true;
            } catch (Exception $e) {
            return false;
            }
        }      
        
        
    /**
     * This function will update the user membership
     * @param array.
     * @return boolean
     * @author Icreon Tech 
     */    
    private function insertUserMembershipCards($userMembershipId,$transactionData = array()) {
        try {
            if(isset($transactionData['membership_date_from']) and trim($transactionData['membership_date_from']) != "" and isset($transactionData['membership_date_to']) and trim($transactionData['membership_date_to']) != "") {
                $dateFrom = trim($transactionData['membership_date_from']);
                $dateTo = trim($transactionData['membership_date_to']);
                $yearFrom = date("Y",  strtotime($dateFrom));
                $yearTo = date("Y",  strtotime($dateTo));
                $diffYear = $yearTo - $yearFrom;
                
                $memCardArr = array();
                $memCardArr['user_id'] = (isset($transactionData['user_id']) and trim($transactionData['user_id']) != "") ? $transactionData['user_id'] : "";
                $memCardArr['user_membership_id'] = $userMembershipId;
                $memCardArr['membership_card_date_from'] = $dateFrom;
                $memCardArr['membership_card_date_to'] = $dateTo;
                
                
                if(isset($diffYear) and trim($diffYear) != "" and $diffYear > 0) {
                     // first entry - start
                     $memCardArr['membership_card_date_from'] = $dateFrom;
                     $memCardArr['membership_card_date_to'] = $yearFrom."-12-31";
                     $this->insertMemberShipCardsRecords($memCardArr);
                     // first entry - end
                     
                     for($yr=1; $yr <= trim($diffYear); $yr++) {
                         if($yearFrom + $yr < $yearTo) {
                             // multi entry - start
                             $mYear = $yearFrom + $yr;
                             $memCardArr['membership_card_date_from'] = $mYear."-01-01";;
                             $memCardArr['membership_card_date_to'] = $mYear."-12-31";
                             $this->insertMemberShipCardsRecords($memCardArr);
                            // multi entry - end 
                         }
                     }
                     
                     // end entry - start
                     $memCardArr['membership_card_date_from'] = $yearTo."-01-01";
                     $memCardArr['membership_card_date_to'] = $dateTo;
                     $this->insertMemberShipCardsRecords($memCardArr);
                     // end entry - end
                }
                else {
                     $this->insertMemberShipCardsRecords($memCardArr);
                }
            }
        }
         catch (Exception $e) {
            return false;
        }
    }
    
     /**
     * Function to insert MemberShip Card Records
     * @author Icreon Tech - NS
     * @return boolean
     * @param Array
     */
    private function insertMemberShipCardsRecords($params = array()) {
        try {
            $params['user_id'] = (isset($params['user_id']) and trim($params['user_id']) != "") ? $params['user_id'] : "";
            $params['user_membership_id'] = (isset($params['user_membership_id']) and trim($params['user_membership_id']) != "") ? $params['user_membership_id'] : "";
            $params['membership_card_date_from'] = (isset($params['membership_card_date_from']) and trim($params['membership_card_date_from']) != "") ? $params['membership_card_date_from'] : "";
            $params['membership_card_date_to'] = (isset($params['membership_card_date_to']) and trim($params['membership_card_date_to']) != "") ? $params['membership_card_date_to'] : "";
            
           if($params['user_id'] != "" and $params['user_membership_id'] != "" and $params['membership_card_date_from'] != "" and $params['membership_card_date_to'] != "" and $params['membership_card_date_from'] != $params['membership_card_date_to']) { 
                $stmt = $this->dbAdapter->createStatement();
                $stmt->prepare('CALL usp_pos_usr_insertContactMembershipCards(?,?,?,?)');
                $stmt->getResource()->bindParam(1, $params['user_id']);
                $stmt->getResource()->bindParam(2, $params['user_membership_id']);
                $stmt->getResource()->bindParam(3, $params['membership_card_date_from']);
                $stmt->getResource()->bindParam(4, $params['membership_card_date_to']);
                $result = $stmt->execute();
            }            
            return true;
        } catch (Exception $e) {
                return false;
        }
    }  
    
    
    /**
     * This function will add final transaction record
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function saveFinalTransaction($transactionData = array()) {
        try {
            
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_pos_tra_insertTransactionDetail(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $stmt->getResource()->bindParam(7, $transactionData[6]);
            $stmt->getResource()->bindParam(8, $transactionData[7]);
            $stmt->getResource()->bindParam(9, $transactionData[8]);
            $stmt->getResource()->bindParam(10, $transactionData[9]);
            $stmt->getResource()->bindParam(11, $transactionData[10]);
            $stmt->getResource()->bindParam(12, $transactionData[11]);
            $stmt->getResource()->bindParam(13, $transactionData[12]);
            $stmt->getResource()->bindParam(14, $transactionData[13]);
            $stmt->getResource()->bindParam(15, $transactionData[14]);
            $stmt->getResource()->bindParam(16, $transactionData[15]);
            $stmt->getResource()->bindParam(17, $transactionData[16]);
            $stmt->getResource()->bindParam(18, $transactionData[17]);
            $stmt->getResource()->bindParam(19, $transactionData[18]);
            $stmt->getResource()->bindParam(20, $transactionData[19]);
            $stmt->getResource()->bindParam(21, $transactionData[20]);
            $stmt->getResource()->bindParam(22, $transactionData[21]);
            $stmt->getResource()->bindParam(23, $transactionData[22]);
            $stmt->getResource()->bindParam(24, $transactionData[23]);
            $stmt->getResource()->bindParam(25, $transactionData[24]);
            $stmt->getResource()->bindParam(26, $transactionData[25]);
            $stmt->getResource()->bindParam(27, $transactionData[26]);
	    $stmt->getResource()->bindParam(28, $transactionData[27]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }    
    public function saveTransactionProduct($transactionData = array()) {
        try {  
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_pos_tra_insertTransactionProduct(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $stmt->getResource()->bindParam(1, $transactionData['user_id']);
            $stmt->getResource()->bindParam(2, $transactionData['transaction_id']);
            $stmt->getResource()->bindParam(3, $transactionData['product_mapping_id']);
            $stmt->getResource()->bindParam(4, $transactionData['product_type_id']);
            $stmt->getResource()->bindParam(5, $transactionData['product_id']);
            $stmt->getResource()->bindParam(6, $transactionData['product_name']);
            $stmt->getResource()->bindParam(7, $transactionData['product_attribute_option_id']);
            $stmt->getResource()->bindParam(8, $transactionData['item_id']);
            $stmt->getResource()->bindParam(9, $transactionData['item_type']);
            $stmt->getResource()->bindParam(10, $transactionData['num_quantity']);
            $stmt->getResource()->bindParam(11, $transactionData['product_price']);
            $stmt->getResource()->bindParam(12, $transactionData['product_sub_total']);
            $stmt->getResource()->bindParam(13, $transactionData['product_discount']);
            $stmt->getResource()->bindParam(14, $transactionData['product_tax']);
            $stmt->getResource()->bindParam(15, $transactionData['product_shipping']);
            $stmt->getResource()->bindParam(16, $transactionData['product_handling']);
            $stmt->getResource()->bindParam(17, $transactionData['product_total']);
            $stmt->getResource()->bindParam(18, $transactionData['product_status_id']);
            $stmt->getResource()->bindParam(19, $transactionData['purchase_date']);
            $stmt->getResource()->bindParam(20, $transactionData['is_free_shipping']);
            $stmt->getResource()->bindParam(21, $transactionData['is_deleted']);
            $stmt->getResource()->bindParam(22, $transactionData['added_by']);
            $stmt->getResource()->bindParam(23, $transactionData['added_date']);
            $stmt->getResource()->bindParam(24, $transactionData['modified_by']);
            $stmt->getResource()->bindParam(25, $transactionData['modified_date']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }        
        
    }
    /**
     * This function is used to get General Campaign
     * @author Icreon Tech - AS
     * @return array
     * @param Array
     */
    public function getGeneralCampaign($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $year = date('Y');
            $stmt->prepare('CALL usp_pos_tra_getGeneralCampaign(?)');
            $stmt->getResource()->bindParam(1, $year);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }    
    
    /**
     * Function for get profile id and credit card id
     * @return array
     * @param $dataArr as an array
     * @author Icreon Tech - DT
     */
    public function getTransactionProfileCreditCartId($dataArr) {
        $transactionId = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $authorizeTransactionId = (isset($dataArr['authorize_transaction_id']) && $dataArr['authorize_transaction_id'] != '') ? $dataArr['authorize_transaction_id'] : '';

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_pos_tra_getTransactionProfileCreditCartId(?,?)');
        $stmt->getResource()->bindParam(1, $transactionId);
        $stmt->getResource()->bindParam(2, $authorizeTransactionId);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return $resultSet;
    }    
    
   /**
     * This function will add credit card receive payment
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function saveTransactionPaymentReceive($transactionData = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_pos_tra_insertCreditPayReceive(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $stmt->getResource()->bindParam(7, $transactionData[6]);
            $stmt->getResource()->bindParam(8, $transactionData[7]);
            $stmt->getResource()->bindParam(9, $transactionData[8]);
            $stmt->getResource()->bindParam(10, $transactionData[9]);
            $stmt->getResource()->bindParam(11, $transactionData[10]);
            $stmt->getResource()->bindParam(12, $transactionData[11]);
            $stmt->getResource()->bindParam(13, $transactionData[12]);
            $stmt->getResource()->bindParam(14, $transactionData[13]);
            $stmt->getResource()->bindParam(15, $transactionData[14]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }    
    /**
     * This function is used to remove the authorize transation id.
     * @author Icreon Tech - SR
     * @return array
     * @param Array
     */
    public function removeAuthorizeTransactionId($params = array()) {
        try {
            $transactionId = (isset($params['transaction_id']) && $params['transaction_id'] != '') ? $params['transaction_id'] : '';

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pos_tra_removePaymentAuthTransactionId(?)');
            $stmt->getResource()->bindParam(1, $transactionId);
            $result = $stmt->execute();
            $statement = $result->getResource();
            // $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC); 

            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
public function saveTransactionPaymentDetail($transactionData = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_pos_tra_insertTransactionPaymentInfo(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $stmt->getResource()->bindParam(7, $transactionData[6]);
            $stmt->getResource()->bindParam(8, $transactionData[7]);
            $stmt->getResource()->bindParam(9, $transactionData[8]);
            $stmt->getResource()->bindParam(10, $transactionData[9]);
            $stmt->getResource()->bindParam(11, $transactionData[10]);
            $stmt->getResource()->bindParam(12, $transactionData[11]);
            $stmt->getResource()->bindParam(13, $transactionData[12]);
            $stmt->getResource()->bindParam(14, $transactionData[13]);
            $stmt->getResource()->bindParam(15, $transactionData[14]);
            $stmt->getResource()->bindParam(16, $transactionData[15]);
            $stmt->getResource()->bindParam(17, $transactionData[16]);
            $stmt->getResource()->bindParam(18, $transactionData[17]);
            $stmt->getResource()->bindParam(19, $transactionData[18]);
            $stmt->getResource()->bindParam(20, $transactionData[19]);
            $stmt->getResource()->bindParam(21, $transactionData[20]);
            $stmt->getResource()->bindParam(22, $transactionData[21]);
            $stmt->getResource()->bindParam(23, $transactionData[22]);
            $stmt->getResource()->bindParam(24, $transactionData[23]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }    
    
    
   /**
     * Function for get user data 
     * @author Icreon Tech - DG
     * @return Int
     * @param Array
     */
    public function getUser($userArr) {
        if (isset($userArr)) {

            if (isset($userArr['email_id']) && $userArr['email_id'] != '') {
                $email_id = $userArr['email_id'];
            } else {
                $email_id = '';
            }

            if (isset($userArr['username']) && $userArr['username'] != '') {
                $username = $userArr['username'];
            } else {
                $username = '';
            }

            if (isset($userArr['user_id']) && $userArr['user_id'] != '') {
                $id = (int) $userArr['user_id'];
            } else {
                $id = '';
            }

            if (isset($userArr['verify_code']) && $userArr['verify_code'] != '') {
                $verify_code = $userArr['verify_code'];
            } else {
                $verify_code = '';
            }

			if (isset($userArr['include_deleted']) && $userArr['include_deleted'] != '') {
                $include_deleted = $userArr['include_deleted'];
            } else {
                $include_deleted = '';
            }

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pos_usr_getUser(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $username);
            $stmt->getResource()->bindParam(2, $email_id);
            $stmt->getResource()->bindParam(3, $id);
            $stmt->getResource()->bindParam(4, $verify_code);
			$stmt->getResource()->bindParam(5, $include_deleted);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if ($resultSet) {
                return $resultSet[0];
            } else {
                return false;
            }
        }
    }    
    
    /**
     * This function will get amount user purchased product till date
     * @param array.
     * @return boolean
     * @author Icreon Tech -DT
     */
    public function getTotalUserPurchased($membershipData) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_pos_usr_getTotalAmountUserPurchased(?,?,?)');
        $stmt->getResource()->bindParam(1, $membershipData['user_id']);
        $stmt->getResource()->bindParam(2, $membershipData['from_date']);
        $stmt->getResource()->bindParam(3, $membershipData['to_date']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet[0]['total_amount'];
        else
            return false;
    }    
    
    /**
     * Function for get matching membership
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function getMatchingMembership($membershipData) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_pos_set_getMatchingMembership(?)");
        $stmt->getResource()->bindParam(1, $membershipData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet[0];
        else
            return false;
    }    
    public function getMatchingMembershipKiosk($membershipData) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_set_getKioskMatchingMembership(?)");
        $stmt->getResource()->bindParam(1, $membershipData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet[0];
        else
            return false;
    }    
    
    /**
     * Function for to get user Afihc
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getUserDetail($params = array()) {
        try {
            $user_id = isset($params['user_id']) ? $params['user_id'] : null;
            $currentTime = DATE_TIME_FORMAT;
            $front_end = (isset($params['front_end']) && !empty($params['front_end'])) ? $params['front_end'] : null;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pos_usr_getUserDetail(?,?,?)');
            $stmt->getResource()->bindParam(1, $user_id);
            $stmt->getResource()->bindParam(2, $currentTime);
            $stmt->getResource()->bindParam(3, $front_end);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet[0];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }
    /**
     * This function is used to update the campaign revenue goal
     * @author Icreon Tech - AS
     * @return array
     * @param Array
     */
    public function updateRevenueGoal($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pos_tra_updateRevenueGoal(?,?)');
            $stmt->getResource()->bindParam(1, $params[26]);
            $stmt->getResource()->bindParam(2, $params[22]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            if ($result) {
                return $result;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * This function is used to update the channel revenue received
     * @author Icreon Tech - AS
     * @return array
     * @param Array
     */
    public function updateChannelRevenue($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pos_tra_updateChannelRevenue(?,?,?)');
            $stmt->getResource()->bindParam(1, $params[26]);
            $stmt->getResource()->bindParam(2, $params[22]);
            $stmt->getResource()->bindParam(3, $params[23]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            if ($result) {
                return $result;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }    
     /**
     * Function for to insert default commu peferences
     * 
     * 
     * @author Icreon Tech - DG
     * @return Int
     * @param Array
     */
    public function insertDefaultContactCommunicationPreferences($dataArr) {
        $addDate = $dataArr['add_date'];
        $privacy = null;
        $annotationPrivacy = 1;
        $familyHistoryPrivacy = 1;

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_pos_usr_insertDefaultCommunicationPreferences(?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $dataArr['user_id']);
        $stmt->getResource()->bindParam(2, $privacy);
        $stmt->getResource()->bindParam(3, $annotationPrivacy);
        $stmt->getResource()->bindParam(4, $familyHistoryPrivacy);
        $stmt->getResource()->bindParam(5, $addDate);
        $stmt->getResource()->bindParam(6, $addDate);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }   
 
    /**
     * This function will fetch the product attributes
     * @param this will be an array
     * @return this will return product attributes array
     * @author Icreon Tech -SR
     */
    public function getProductAttributeDetails($param = array()) {
        try {
            $productId = isset($param['productId']) ? $param['productId'] : '';
            $productType = isset($param['productType']) ? $param['productType'] : '';
            $isOptions = isset($param['isOptions']) ? $param['isOptions'] : '';
            $categoryId = isset($param['categoryId']) ? $param['categoryId'] : '';

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pro_getProductAttributesDetails(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $productId);
            $stmt->getResource()->bindParam(2, $productType);
            $stmt->getResource()->bindParam(3, $isOptions);
            $stmt->getResource()->bindParam(4, $categoryId);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
 
    /**
     * This function will get transaction product details
     * @param array
     * @return return last id data
     * @author Icreon Tech - AS
     */
    public function searchTransactionProducts($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pos_tra_getTransactionProducts(?)');
            $stmt->getResource()->bindParam(1, $params['transactionId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }    
    
    /**
     * This function will search the transaction details
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function searchTransactionDetails($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pos_tra_viewCrmTransaction(?)');
            $stmt->getResource()->bindParam(1, $params['transactionId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }    
    
    /**
     * Function for to get user address info
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getUserAddressInformations($params = array()) {
        try {
            
            $user_id = (isset($params['user_id']) and trim($params['user_id']) != "") ? $params['user_id'] : null;
            $address_information_id = (isset($params['address_information_id']) and trim($params['address_information_id']) != "") ? $params['address_information_id'] : null;
            $addressLocation = (isset($params['address_location']) and trim($params['address_location']) != "") ? $params['address_location'] : null;
            
            if($user_id != null or $address_information_id != null or $addressLocation != null) {
                
                $stmt = $this->dbAdapter->createStatement();
                $stmt->prepare('CALL usp_pos_usr_getAddressInformations(?,?,?)');
                $stmt->getResource()->bindParam(1, $user_id);
                $stmt->getResource()->bindParam(2, $address_information_id);
                $stmt->getResource()->bindParam(3, $addressLocation);
                $result = $stmt->execute();
                $statement = $result->getResource();
                $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
                $statement->closeCursor();
                if (!empty($resultSet))
                    return $resultSet;
                else
                    return false;
                
            } 
            else {
                 return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }    
    
    /**
     * This function use for check user already has active pledge or not
     * @param array
     * @return this will return a confirmation
     * @author Icreon Tech - DT
     */
    public function checkUserActivePledge($pledgeData = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_ple_getTotalUserActivePledge(?,?)");
            $stmt->getResource()->bindParam(1, $pledgeData[0]);
            $stmt->getResource()->bindParam(2, $pledgeData[1]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }    
    
    /**
     * Function for get pledge transactions data on the base of id
     * @author Icreon Tech - AG
     * @return Int
     * @param Array
     */
    public function getPledgeTransactions($pledgeData) {

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_ple_getPledgeTransactions(?,?,?,?,?)");
        $stmt->getResource()->bindParam(1, $pledgeData[0]);
        $stmt->getResource()->bindParam(2, $pledgeData[1]);
        $stmt->getResource()->bindParam(3, $pledgeData[2]);
        $stmt->getResource()->bindParam(4, $pledgeData[3]);
        $stmt->getResource()->bindParam(5, $pledgeData[4]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        //asd($resultSet);
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }    
    
    /**
     * Function for get pledge data on the base of id
     * @author Icreon Tech - AG
     * @return Int
     * @param Array
     */
    public function getPledgeById($pledgeData) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_ple_getPledgeinfo(?)");
        $stmt->getResource()->bindParam(1, $pledgeData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet[0];
        else
            return false;
    }    
    
    /**
     * This function will update the pledge transaction status
     * @param array
     * @return this will return a confirmation
     * @author Icreon Tech - DT
     */
    public function updatePledgeTransactionStatus($pledgeData = array()) {
        try {

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_ple_updatePledgeTransactionStatus(?,?,?,?,?)");
            $stmt->getResource()->bindParam(1, $pledgeData[0]);
            $stmt->getResource()->bindParam(2, $pledgeData[1]);
            $stmt->getResource()->bindParam(3, $pledgeData[2]);
            $stmt->getResource()->bindParam(4, $pledgeData[3]);
            $stmt->getResource()->bindParam(5, $pledgeData[4]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }    
    
    /**
     * This function will update the pledge transaction amount
     * @param array
     * @return this will return a confirmation
     * @author Icreon Tech - DT
     */
    public function updatePledgeTransactionAmount($pledgeData = array()) {
        try {

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_ple_updatePledgeTransactionAmount(?,?,?,?,?,?,?)");
            $stmt->getResource()->bindParam(1, $pledgeData[0]);
            $stmt->getResource()->bindParam(2, $pledgeData[1]);
            $stmt->getResource()->bindParam(3, $pledgeData[2]);
            $stmt->getResource()->bindParam(4, $pledgeData[3]);
            $stmt->getResource()->bindParam(5, $pledgeData[4]);
            $stmt->getResource()->bindParam(6, $pledgeData[5]);
            $stmt->getResource()->bindParam(7, $pledgeData[6]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }    
    
    /**
     * This function will update the user membership
     * @param array.
     * @return boolean
     * @author Icreon Tech -DT
     */
    public function updateUserMembership($transactionData = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_pos_updateContactMembership(?,?,?,?,?,?,?,?,?)");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $stmt->getResource()->bindParam(7, $transactionData[6]);
            $stmt->getResource()->bindParam(8, $transactionData[7]);
            $stmt->getResource()->bindParam(9, $transactionData[8]);
            $result = $stmt->execute();

            // - start
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();
            if (!empty($resultSet))
                $lastInsertedId = $resultSet['0']['LAST_INSERT_ID'];
            else
                $lastInsertedId = "";

            if (isset($lastInsertedId) and trim($lastInsertedId) != "") {
                $this->insertUserMembershipCards(trim($lastInsertedId), $transactionData);
            }
            // - end

            return true;
        } catch (Exception $e) {
            return false;
        }
    }    
    
    
     /**
     * Function for insert user data in table for signup
     * @author Icreon Tech - DG
     * @return Int
     * @param Array
     */
    public function saveUserVisitationRecord($param) {

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_pos_saveUserVisitationRecord(?,?,?,?,?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $param['user_id']);
        $stmt->getResource()->bindParam(2, $param['first_name']);
        $stmt->getResource()->bindParam(3, $param['last_name']);
        $stmt->getResource()->bindParam(4, $param['visiting_with']);
        $stmt->getResource()->bindParam(5, $param['arrival_date']);        
        $stmt->getResource()->bindParam(6, $param['birth_country']);
        $stmt->getResource()->bindParam(7, $param['created_at']);
        $stmt->getResource()->bindParam(8, $param['confirmation_code']);
        $stmt->getResource()->bindParam(9, $param['family_name']);
        $stmt->getResource()->bindParam(10, $param['certificate_file_name']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            return false;
        }
    }
     /**
     * This function will add transaction billilng shipping address detail
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function saveTransactionBillingShippingDetail($transactionData = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_insertPosTransactionBillingShipping(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $stmt->getResource()->bindParam(7, $transactionData[6]);
            $stmt->getResource()->bindParam(8, $transactionData[7]);
            $stmt->getResource()->bindParam(9, $transactionData[8]);
            $stmt->getResource()->bindParam(10, $transactionData[9]);
            $stmt->getResource()->bindParam(11, $transactionData[10]);
            $stmt->getResource()->bindParam(12, $transactionData[11]);
            $stmt->getResource()->bindParam(13, $transactionData[12]);
            $stmt->getResource()->bindParam(14, $transactionData[13]);
            $stmt->getResource()->bindParam(15, $transactionData[14]);
            $stmt->getResource()->bindParam(16, $transactionData[15]);
            $stmt->getResource()->bindParam(17, $transactionData[16]);
            $stmt->getResource()->bindParam(18, $transactionData[17]);
            $stmt->getResource()->bindParam(19, $transactionData[18]);
            $stmt->getResource()->bindParam(20, $transactionData[19]);
            $stmt->getResource()->bindParam(21, $transactionData[20]);
            $stmt->getResource()->bindParam(22, $transactionData[21]);
            $stmt->getResource()->bindParam(23, $transactionData[22]);
            $stmt->getResource()->bindParam(24, $transactionData[23]);
            $stmt->getResource()->bindParam(25, $transactionData[24]);
            $stmt->getResource()->bindParam(26, $transactionData[25]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }   
    
    /**
     * Function for to get user PhoneInformations
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getUserPhoneInformations($params = array()) {
        try {
            $userId = isset($params['user_id']) ? $params['user_id'] : null;
            $phoneId = isset($params['phone_id']) ? $params['phone_id'] : null;

            if (isset($params['is_primary']) and trim($params['is_primary']) != "" and is_numeric(trim($params['is_primary']))) {
                $isPrimary = trim($params['is_primary']);
            } else {
                $isPrimary = '';
            }

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getPhoneInformations(?,?,?)');
            $stmt->getResource()->bindParam(1, $userId);
            $stmt->getResource()->bindParam(2, $phoneId);
            $stmt->getResource()->bindParam(3, $isPrimary);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }    
    
    /**
     * This function will add save bulk donations
     * @param array
     * @return return last id data
     * @author Icreon Tech - SK
     */
    public function saveTransactionDonationProdcts($transactionData = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_pos_tra_insertDonationProducts(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $stmt->getResource()->bindParam(7, $transactionData[6]);
            $stmt->getResource()->bindParam(8, $transactionData[7]);
            $stmt->getResource()->bindParam(9, $transactionData[8]);
            $stmt->getResource()->bindParam(10, $transactionData[9]);
            $stmt->getResource()->bindParam(11, $transactionData[10]);
            $stmt->getResource()->bindParam(12, $transactionData[11]);
            $stmt->getResource()->bindParam(13, $transactionData[12]);
            $stmt->getResource()->bindParam(14, $transactionData[13]);
            $stmt->getResource()->bindParam(15, $transactionData[14]);
            $stmt->getResource()->bindParam(16, $transactionData[15]);
            $stmt->getResource()->bindParam(17, $transactionData[16]);
            $stmt->getResource()->bindParam(18, $transactionData[17]);
            $stmt->getResource()->bindParam(19, $transactionData[18]);
            $stmt->getResource()->bindParam(20, $transactionData[19]);
            $stmt->getResource()->bindParam(21, $transactionData[20]);
            $stmt->getResource()->bindParam(22, $transactionData[21]);
            $stmt->getResource()->bindParam(23, $transactionData[22]);
            $stmt->getResource()->bindParam(24, $transactionData[23]);
            $stmt->getResource()->bindParam(25, $transactionData[24]);
            $stmt->getResource()->bindParam(26, $transactionData[25]);
            $stmt->getResource()->bindParam(27, $transactionData[26]);
            $stmt->getResource()->bindParam(28, $transactionData[27]);
            $stmt->getResource()->bindParam(29, $transactionData[28]);
            $stmt->getResource()->bindParam(30, $transactionData[29]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }    
    
    /**
     * Function for get user Country
     * @author Icreon Tech - SR
     * @return Int
     * @param Array
     */
    public function getCountryUser($countryArr) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getContactCountry(?,?)");
        $stmt->getResource()->bindParam(1, $countryArr[0]);
        $stmt->getResource()->bindParam(2, $countryArr[1]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }    

    /**
     * Function for get visitation details
     * @author Icreon Tech - SR
     * @return Int
     * @param Array
     */    
    public function getVisitationUserDetails($params = array()){ 
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getVisitationUserDetails(?)");
        $stmt->getResource()->bindParam(1, $params['confirmation_code']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }        
    }
	/**
     * Function for get all master membership
     * @author Icreon Tech - SK
     * @return Array
     * @param Array
     */
    public function getMasterMembership($membershipData) {
        
        $membershipData['currentYear'] = isset($membershipData['currentYear'])?$membershipData['currentYear']:'';
        $membershipData['isActive'] = isset($membershipData['isActive'])?$membershipData['isActive']:'1';

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_set_getPosMembership(?,?,?,?,?,?)");
        $stmt->getResource()->bindParam(1, $membershipData[0]);
        $stmt->getResource()->bindParam(2, $membershipData[1]);
        $stmt->getResource()->bindParam(3, $membershipData[2]);
        $stmt->getResource()->bindParam(4, $membershipData[3]);
        $stmt->getResource()->bindParam(5, $membershipData['currentYear']);
        $stmt->getResource()->bindParam(6, $membershipData['isActive']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }    
    
    /**
     * Function for to get the cred card type
     * @author Icreon Tech - SR
     * @return Int
     * @param $user_id
     */
    public function getCreditCards($param = array()) {
        
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_get_poscreditCart()');
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
             return $resultSet;
        } else {
                return false;
        }
    }    
    
    /**
     * Function used to insert token info
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function insertTokenInfo($params = array()) {
        try { 
            $userId = isset($params['user_id']) ? $params['user_id'] : null;
            $profileId = isset($params['profile_id']) ? $params['profile_id'] : null;
            $addedBy = isset($params['added_by']) ? $params['added_by'] : null;
            $modifiedBy = isset($params['modified_by']) ? $params['modified_by'] : null;
            $billingAddressId = isset($params['billing_address_id']) ? $params['billing_address_id'] : null;
            $creditCardType = isset($params['credit_card_type_id']) ? $params['credit_card_type_id'] : null;
            $creditCardNo = isset($params['credit_card_no']) ? $params['credit_card_no'] : null;
            
            $addedDate = DATE_TIME_FORMAT;
            $modifiedDate = DATE_TIME_FORMAT;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_insertPosToken(?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $userId);
            $stmt->getResource()->bindParam(2, $profileId);
            $stmt->getResource()->bindParam(3, $addedBy);
            $stmt->getResource()->bindParam(4, $addedDate);
            $stmt->getResource()->bindParam(5, $modifiedBy);
            $stmt->getResource()->bindParam(6, $modifiedDate);
            $stmt->getResource()->bindParam(7, $billingAddressId);
            $stmt->getResource()->bindParam(8, $creditCardType);
            $stmt->getResource()->bindParam(9, $creditCardNo);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }    
    
    /**
     * Function for get users in Group
     * @author Icreon Tech - SR
     * @return Int
     * @param void
     */
    public function getUserInGroup($params = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_pos_prm_getUserGroup(?)");
        $stmt->getResource()->bindParam(1, $params['user_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
    }    
    
    
    /**
     * Function for updateion of visitation record
     * @author Icreon Tech - DG
     * @return Int
     * @param Array
     */
    public function updateUserVisitationRecord($param) {

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_pos_UpdateUserVisitationRecord(?,?)');
        $stmt->getResource()->bindParam(1, $param['visit_id']);
        $stmt->getResource()->bindParam(2, $param['certificate_file_name']);

        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }    
    /**
     * Function is used to insert in user table
     * @author Icreon Tech - DG
     * @return Int
     * @param Array
     */    
	public function insertWohContact($param = array()) {
        try {
            $isActive = '1';
            $isActiveted = '1';
            $emailVerified = '1';            
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ksk_woh_insertContact(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['contact_type']);
            $stmt->getResource()->bindParam(2, $param['first_name']);
            $stmt->getResource()->bindParam(3, $param['last_name']);
            $stmt->getResource()->bindParam(4, $param['email']);
            $stmt->getResource()->bindParam(5, $param['no_email']);
            $stmt->getResource()->bindParam(6, $param['password']);
            $stmt->getResource()->bindParam(7, $param['salt']);            
            $stmt->getResource()->bindParam(8, $param['login_enable']); 
            $stmt->getResource()->bindParam(9, $param['add_date']);
            $stmt->getResource()->bindParam(10, $param['modified_date']);                        
            $stmt->getResource()->bindParam(11, $param['username']);
            $stmt->getResource()->bindParam(12, $param['verify_code']); 
            $stmt->getResource()->bindParam(13, $isActive);
            $stmt->getResource()->bindParam(14, $isActiveted);
            $stmt->getResource()->bindParam(15, $emailVerified);   
            $stmt->getResource()->bindParam(16, $param['source_id']);   
            $stmt->getResource()->bindParam(17, $param['title']);   

            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }
	
	public function getMatchingMembershipWOHDon($membershipData) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_set_getMatchingMembershipWOHDon(?)");
        $stmt->getResource()->bindParam(1, $membershipData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet[0];
        else
            return false;
    }
}