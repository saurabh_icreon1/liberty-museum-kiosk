<?php

/**
 * This model is used for kiosk management
 * @package    Kiosk
 * @author     Icreon Tech - SK
 */

namespace Kiosk\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This model is used for kiosk management
 * @package    User
 * @author     Icreon Tech - SK
 */
class KioskReservationTable {

    protected $tableGateway;
    protected $dbAdapter;
    protected $connection;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
        $this->connection = $this->tableGateway->getAdapter()->getDriver()->getConnection();
    }
    
    /**
     * Function for to insert crm users
     * @param array
     * @return boolean
     * @author Icreon Tech - AP
     */
    public function insertContact($param = array()) {
        try {
			$is_enable_reassign = isset($param['is_enable_reassign'])?$param['is_enable_reassign']:'';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ksk_insertReservationContact(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['kiosk_id']);
            $stmt->getResource()->bindParam(2, $param['contact_id']);
            $stmt->getResource()->bindParam(3, $param['time_allocated']);
            $stmt->getResource()->bindParam(4, $param['time_started']);
            $stmt->getResource()->bindParam(5, $param['time_end']);
            $stmt->getResource()->bindParam(6, $param['booking_date']);
	        $stmt->getResource()->bindParam(7, $param['time_left']);
	        $stmt->getResource()->bindParam(8, $param['is_wheelchair']);
            $stmt->getResource()->bindParam(9, $param['payment_status']);
            $stmt->getResource()->bindParam(10, $param['transaction_id']);
            $stmt->getResource()->bindParam(11, $param['payment_mode_id']);
            $stmt->getResource()->bindParam(12, $param['authorize_transaction_id']);
            $stmt->getResource()->bindParam(13, $param['cheque_no']);
            $stmt->getResource()->bindParam(14, $param['cheque_date']);
            $stmt->getResource()->bindParam(15, $param['amount']);
            $stmt->getResource()->bindParam(16, $param['confirmation_code']);
            $stmt->getResource()->bindParam(17, $param['first_name']);
            $stmt->getResource()->bindParam(18, $param['last_name']);
			$stmt->getResource()->bindParam(19, $is_enable_reassign);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * This function is used to get the kiosk seached records
     * @param this will be an array.
     * @return this will return search result of kiosk
     * @author Icreon Tech -AP
     */
    public function searchKioskReservation($param = array()) {//asd($param);usp_ksk_getKioskReservationDetailsSearch
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ksk_getKioskReservationSearch(?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['current_status']);
            $stmt->getResource()->bindParam(2, $param['is_wheelchair']);
            $stmt->getResource()->bindParam(3, $param['history']);
            $stmt->getResource()->bindParam(4, $param['kiosk_name']);
            $stmt->getResource()->bindParam(5, $param['contact_name']);
            $stmt->getResource()->bindParam(6, $param['reservation_method']);
            $stmt->getResource()->bindParam(7, $param['grid_main_view']);
            $stmt->getResource()->bindParam(8, $param['startIndex']);
            $stmt->getResource()->bindParam(9, $param['recordLimit']);
            $stmt->getResource()->bindParam(10, $param['sortField']);
            $stmt->getResource()->bindParam(11, $param['sortOrder']);
            $stmt->getResource()->bindParam(12, $param['kiosk_id']);
            $stmt->getResource()->bindParam(13, $param['available']);
            //$stmt->getResource()->bindParam(8, $param['currentTime']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            //asd($resultSet);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    /**
     * This function is used to get the kiosk seached records
     * @param this will be an array.
     * @return this will return search result of kiosk
     * @author Icreon Tech -AP
     */
    public function kioskReservationDetailsSearch($param = array()) {//asd($param);
        
        $inUse = isset($param['in_use'])?(!empty($param['in_use'])?$param['in_use']:""):"";
        $reserved = isset($param['reserved'])?(!empty($param['reserved'])?$param['reserved']:""):"";
        $paused = isset($param['paused'])?(!empty($param['paused'])?$param['paused']:""):"";


        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ksk_getKioskReservationDetailsSearch(?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['kiosk_id']);
            $stmt->getResource()->bindParam(2, $param['history']);
            $stmt->getResource()->bindParam(3, $param['grid_main_view']);
            $stmt->getResource()->bindParam(4, $param['startIndex']);
            $stmt->getResource()->bindParam(5, $param['recordLimit']);
            $stmt->getResource()->bindParam(6, $param['sortField']);
            $stmt->getResource()->bindParam(7, $param['sortOrder']);
            $stmt->getResource()->bindParam(8, $param['contact_name']);
            $stmt->getResource()->bindParam(9, $param['curr_time']);
            $stmt->getResource()->bindParam(10, $param['curr_date']);
            $stmt->getResource()->bindParam(11, $inUse);
            $stmt->getResource()->bindParam(12, $reserved);
            $stmt->getResource()->bindParam(13, $paused);
            //$stmt->getResource()->bindParam(8, $param['currentTime']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            //asd($param,0);echo $inUse.$reserved.$paused;
            //asd($resultSet);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    /**
     * This function is used to get the kiosk reservation queued
     * @param this will be an array.
     * @return this will return search result of kiosk
     * @author Icreon Tech -AP
     */
    public function searchReservationQueued($param = array()) {//asd($param);
        $data['queue_id'] = isset($param['queue_id'])?$param['queue_id']:"";
        $data['kiosk_id'] = isset($param['kiosk_id'])?$param['kiosk_id']:"";
        $data['contact'] = isset($param['contact'])?$param['contact']:"";
        $data['startIndex'] = isset($param['startIndex'])?$param['startIndex']:"";
        $data['recordLimit'] = isset($param['recordLimit'])?$param['recordLimit']:"";
        $data['sortField'] = isset($param['sortField'])?$param['sortField']:"";
        $data['sortOrder'] = isset($param['sortOrder'])?$param['sortOrder']:"";
        $data['payment_status'] = isset($param['payment_status'])?$param['payment_status']:"";
        $data['assigned_customer'] = isset($param['assigned_customer'])?$param['assigned_customer']:"";
        $data['curr_date'] = isset($param['curr_date'])?$param['curr_date']:"";
        $data['user_id'] = isset($param['user_id'])?$param['user_id']:"";
        $data['join_kiosk'] = isset($param['join_kiosk'])?$param['join_kiosk']:"";
        $data['is_wheelchair'] = isset($param['is_wheelchair'])?$param['is_wheelchair']:"";
        $data['kiosk_name'] = isset($param['kiosk_name'])?$param['kiosk_name']:"";
        $data['curr_time'] = isset($param['curr_time'])?$param['curr_time']:"";
        $data['pause_resume'] = isset($param['pause_resume'])?$param['pause_resume']:"";
        
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pos_getKioskReservationQueuedSearch(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $data['queue_id']);
            $stmt->getResource()->bindParam(2, $data['kiosk_id']);
            $stmt->getResource()->bindParam(3, $data['contact']);
            $stmt->getResource()->bindParam(4, $data['startIndex']);
            $stmt->getResource()->bindParam(5, $data['recordLimit']);
            $stmt->getResource()->bindParam(6, $data['sortField']);
            $stmt->getResource()->bindParam(7, $data['sortOrder']);
            $stmt->getResource()->bindParam(8, $data['payment_status']);
            $stmt->getResource()->bindParam(9,  $data['assigned_customer']);
            $stmt->getResource()->bindParam(10, $data['curr_date']);
			$stmt->getResource()->bindParam(11, $data['user_id']);
			$stmt->getResource()->bindParam(12, $data['join_kiosk']);
			$stmt->getResource()->bindParam(13, $data['is_wheelchair']);
            $stmt->getResource()->bindParam(14, $data['kiosk_name']);
            $stmt->getResource()->bindParam(15, $data['curr_time']);
            $stmt->getResource()->bindParam(16, $data['pause_resume']);
            
            //$stmt->getResource()->bindParam(8, $param['currentTime']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            //asd($resultSet);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    /**
     * Function for to get kiosk deatails
     * @param Array
     * @return Array
     * @author Icreon Tech - AP 
     */
    public function getkioskDetails($param = array()) {
        $isWheelchair = isset($param['is_wheelchair'])?$param['is_wheelchair']:"";
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_kiosk_getKioskInfo(?,?)');
            $stmt->getResource()->bindParam(1, $param['kiosk_id']);
            $stmt->getResource()->bindParam(2, $isWheelchair);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            //asd($resultSet);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }
    /**
     * Function for to get reservation contact list
     * @param Array
     * @return Array
     * @author Icreon Tech - AP 
     */
    public function getReservationContactListSearch($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ksk_getReservationContactListSearch(?)');
            $stmt->getResource()->bindParam(1, $param['contact']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }
     /**
     * Function for to get reservation contact list
     * @param Array
     * @return Array
     * @author Icreon Tech - AP 
     */
    public function updateReservationInfo($param = array()) {

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ksk_updateReservationInfo(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['queue_id']);
            $stmt->getResource()->bindParam(2, $param['time_left']);
            $stmt->getResource()->bindParam(3, $param['pause_resume']);
            $stmt->getResource()->bindParam(4, $param['pause_at']);
            $stmt->getResource()->bindParam(5, $param['time_started']);
            $stmt->getResource()->bindParam(6, $param['time_end']);
            $stmt->getResource()->bindParam(7, $param['time_allocated']);
            $stmt->getResource()->bindParam(8, $param['is_delete']);
            $stmt->getResource()->bindParam(9, $param['contact_id']);
            $stmt->getResource()->bindParam(10, $param['transaction_id']);
            $stmt->getResource()->bindParam(11, $param['payment_mode_id']);
            $stmt->getResource()->bindParam(12, $param['authorize_transaction_id']);
            $stmt->getResource()->bindParam(13, $param['cheque_no']);
            $stmt->getResource()->bindParam(14, $param['cheque_date']);
            $stmt->getResource()->bindParam(15, $param['amount']);
            $stmt->getResource()->bindParam(16, $param['confirmation_code']);
            $stmt->getResource()->bindParam(17, $param['kiosk_id']);
            $stmt->getResource()->bindParam(18, $param['payment_status']);
            $stmt->getResource()->bindParam(19, $param['first_name']);
            $stmt->getResource()->bindParam(20, $param['last_name']);
            
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
       
    }
     /**
     * Function for to get next available time
     * @param Array
     * @return Array
     * @author Icreon Tech - AP 
     */
    public function nextAvailableTime($param = array()) {
        $isWheelchair= isset($param['is_wheelchair'])?$param['is_wheelchair']:"";
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ksk_getNextKioskSlot(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['kiosk_id']);
            $stmt->getResource()->bindParam(2, $param['curr_time']);
            $stmt->getResource()->bindParam(3, $param['curr_date']);
            $stmt->getResource()->bindParam(4, $isWheelchair);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();//asd($resultSet);
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * Function for get next Availavle Kiosk
     * @author Icreon Tech - DG
     * @return array
     */
    public function getNextKiosk($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_kiosk_nextKiosk(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['current_status']);
            $stmt->getResource()->bindParam(2, $param['is_wheelchair']);
            $stmt->getResource()->bindParam(3, $param['kiosk_id']);
            $stmt->getResource()->bindParam(4, $param['curr_time']);
            $stmt->getResource()->bindParam(5, $param['curr_date']);
            $stmt->getResource()->bindParam(6, $param['next']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }
    /**
     * Function for get next Availavle Kiosk
     * @author Icreon Tech - DG
     * @return array
     */
    public function getReservationQueueStation() {
        $result = $this->connection->execute('CALL usp_ksk_getReservationQueueStatus');
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();

        if (!$resultSet) {
            throw new \Exception("Could not find Modules record.");
        }
        return $resultSet;
    }
     /**
     * Function for to get reservation contact list
     * @param Array
     * @return Array
     * @author Icreon Tech - AP 
     */
    public function assignKiosk($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ksk_assignKiosk(?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['queue_id']);
            $stmt->getResource()->bindParam(2, $param['kiosk_id']);
            $stmt->getResource()->bindParam(3, $param['time_allocated']);
            $stmt->getResource()->bindParam(4, $param['time_end']);
            $stmt->getResource()->bindParam(5, $param['payment_status']);
            $stmt->getResource()->bindParam(6, $param['time_left']);
            $stmt->getResource()->bindParam(7, $param['is_wheelchair']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    /**
     * This function is used to get the contact details of current kiosk
     * @param this will be an array.
     * @return this will return search result of kiosk
     * @author Icreon Tech -AP
     */
    public function getKioskReservationContacts($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ksk_getKioskReservationContactsSearch(?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['kiosk_id']);
            $stmt->getResource()->bindParam(2, $param['history']);
            $stmt->getResource()->bindParam(3, $param['grid_main_view']);
            $stmt->getResource()->bindParam(4, $param['startIndex']);
            $stmt->getResource()->bindParam(5, $param['recordLimit']);
            $stmt->getResource()->bindParam(6, $param['sortField']);
            $stmt->getResource()->bindParam(7, $param['sortOrder']);
            $stmt->getResource()->bindParam(8, $param['contact_name']);
            $stmt->getResource()->bindParam(9, $param['curr_time']);
            $stmt->getResource()->bindParam(10, $param['curr_date']);
            //$stmt->getResource()->bindParam(8, $param['currentTime']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            //asd($resultSet);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
	 /**
     * This function is used to get the kiosk max time
     * @param this will be an array.
     * @return this will return search result of kiosk
     * @author Icreon Tech -AP
     */
    public function kioskNextMaxTime($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_kiosk_getKiosksMaxTimeInfo(?,?)');
            $stmt->getResource()->bindParam(1, $param['kiosk_ids']);
            $stmt->getResource()->bindParam(2, $param['curr_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
	
	/**
     * This function is used to get the kiosk seached records
     * @param this will be an array.
     * @return this will return search result of kiosk
     * @author Icreon Tech -AP
     */
    public function searchKioskReservationInfo($param = array()) {//asd($param);usp_ksk_getKioskReservationDetailsSearch
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ksk_getKioskReservationSearchCRM(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['available']);
            $stmt->getResource()->bindParam(2, $param['reserved']);
            $stmt->getResource()->bindParam(3, $param['not_available']);
            $stmt->getResource()->bindParam(4, $param['in_use']);
            $stmt->getResource()->bindParam(5, $param['paused']);
            $stmt->getResource()->bindParam(6, $param['reservation_method']);
            $stmt->getResource()->bindParam(7, $param['is_wheelchair']);
            $stmt->getResource()->bindParam(8, $param['history']);
            $stmt->getResource()->bindParam(9, $param['kiosk_name']);
            $stmt->getResource()->bindParam(10, $param['contact_name']);
            $stmt->getResource()->bindParam(11, $param['curr_time']);
            $stmt->getResource()->bindParam(12, $param['curr_date']);
            $stmt->getResource()->bindParam(13, $param['recOffset']);
            $stmt->getResource()->bindParam(14, $param['recLimit']);
            $stmt->getResource()->bindParam(15, $param['sortField']);
            $stmt->getResource()->bindParam(16, $param['sortOrder']);
            $stmt->getResource()->bindParam(17, $param['grid_main_view']);

            //$stmt->getResource()->bindParam(8, $param['currentTime']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            //asd($resultSet);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
	/**
     * This function is used to get the contact details of current kiosk
     * @param this will be an array.
     * @return this will return search result of kiosk
     * @author Icreon Tech -AP
     */
    public function getKioskReservationContactsInfo($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ksk_getKioskReservationContacts(?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['kiosk_id']);
            $stmt->getResource()->bindParam(2, $param['history']);
            $stmt->getResource()->bindParam(3, $param['grid_main_view']);
            $stmt->getResource()->bindParam(4, $param['startIndex']);
            $stmt->getResource()->bindParam(5, $param['recordLimit']);
            $stmt->getResource()->bindParam(6, $param['sortField']);
            $stmt->getResource()->bindParam(7, $param['sortOrder']);
            $stmt->getResource()->bindParam(8, $param['contact_name']);
            $stmt->getResource()->bindParam(9, $param['curr_time']);
            $stmt->getResource()->bindParam(10, $param['curr_date']);
            //$stmt->getResource()->bindParam(8, $param['currentTime']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            //asd($resultSet);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
	/**
     * Function for get next Availavle Kiosk details
     * @author Icreon Tech - DG
     * @return array
     */
    public function getNextKioskDetails($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_kiosk_nextKioskDetails(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['is_wheelchair']);
            $stmt->getResource()->bindParam(2, $param['kiosk_id']);
            $stmt->getResource()->bindParam(3, $param['curr_time']);
            $stmt->getResource()->bindParam(4, $param['curr_date']);
            $stmt->getResource()->bindParam(5, $param['auto_reservation']);
            $stmt->getResource()->bindParam(6, $param['is_delete']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

	function deleteReservationQueue($data = array()){
	    $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_ksk_deleteResevationQueue(?)');
		$stmt->getResource()->bindParam(1, $data['queue_id']);
		$result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
        return true;
	}
   

}