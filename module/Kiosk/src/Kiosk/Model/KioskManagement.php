<?php

/**
 * This model is used for find and merge duplicate contact
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace Kiosk\Model;

use Kiosk\Module;
// Add these import statements
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\Adapter\Adapter;

/**
 * This model is used for find and merge duplicate contact
 * @package    User
 * @author     Icreon Tech - DG
 */
class KioskManagement implements InputFilterAwareInterface {

    protected $inputFilter;
    protected $adapter;

    public function __construct(Adapter $adapter) {

        $this->adapter = $adapter;
    }
    // Add content to these methods:
    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    // Add the following method:
    public function getArrayCopy() {
        return get_object_vars($this);
    }
    /**
     * Function used to check variable for user create contact form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function exchangeArray($data) {
        if (!empty($data['kiosk_id'])) {
            $this->email_id = (isset($data['kiosk_id'])) ? $data['kiosk_id'] : null;
        }
    }
     /**
     * Function used to check validation for crm create user form
     * @param Array
     * @return array
     * @author Icreon Tech - AP
     */
    public function getInputFilter() {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
             $inputFilter->add($factory->createInput(array(
                        'name' => 'kiosk_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_KIOSK_NAME',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'kiosk_ip',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_KIOSK_IP',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'Db\NoRecordExists',
                                'options' => array(
                                    'table' => 'tbl_ksk_kiosks',
                                    'field' => 'kiosk_ip',
                                    'adapter' => $this->adapter,
                                    'messages' => array(
                                        'recordFound' => 'IP_EXIST_ALREADY',
                                    ),
                                ),
                            ),
                        ),
                    )));
      
    
            $inputFilter->add($factory->createInput(array(
                        'name' => 'status',
                        'required' => true,
                    )));
             $inputFilter->add($factory->createInput(array(
                        'name' => 'kiosk_wheelchair',
                        'required' => true,
                    )));
            
            $inputFilter->add($factory->createInput(array(
                        'name' => 'kiosk_capacity',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_CAPACITY',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'csrf',
                'required' => false,
                'validators' => array(
                            array(
                                'name' => 'csrf',
                                'options' => array(
                                    'messages' => array(
                                        'notSame' => 'NOT_SAME',
                                    ),
                                ),
                            ),
                        ),
            )));


            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
     /**
     * Function used to check validation for crm edit user form
     * @param Array	 
     * @return array
     * @author Icreon Tech - AP
     */
    public function getEditFromInputFilter($kiosk_id='') {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                        'name' => 'kiosk_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_KIOSK_NAME',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'kiosk_ip',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_KIOSK_IP',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'Db\NoRecordExists',
                                'options' => array(
                                    'table' => 'tbl_ksk_kiosks',
                                    'field' => 'kiosk_id',
                                    'adapter' => $this->adapter,
                                    'exclude' => array('field' => 'kiosk_id', 'value' => $kiosk_id),
                                    'messages' => array(
                                        'recordFound' => 'IP_EXIST_ALREADY',
                                    ),
                                ),
                            ),
                        ),
                    )));
      
    
            $inputFilter->add($factory->createInput(array(
                        'name' => 'status',
                        'required' => true,
                    )));
             $inputFilter->add($factory->createInput(array(
                        'name' => 'kiosk_wheelchair',
                        'required' => true,
                    )));
            
            $inputFilter->add($factory->createInput(array(
                        'name' => 'kiosk_capacity',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_CAPACITY',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                'name'     => 'csrf',
                'required' => false,
                'validators' => array(
                            array(
                                'name' => 'csrf',
                                'options' => array(
                                    'messages' => array(
                                        'notSame' => 'NOT_SAME',
                                    ),
                                ),
                            ),
                        ),
            )));
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
    /**
     * Function used to check variable for crm user 
     * @param Array
     * @return array
     * @author Icreon Tech - AP
     */
    public function filterParam($data) {
        $parameter = array();
        $parameter['kiosk_id'] = (isset($data['kiosk_id'])) ? $data['kiosk_id'] : null;
        $parameter['kiosk_name'] = (isset($data['kiosk_name'])) ? $data['kiosk_name'] : null;
        $parameter['kiosk_ip'] = (isset($data['kiosk_ip'])) ? $data['kiosk_ip'] : null;
        //$parameter['is_status'] = (isset($data['status'])) ? $data['status'] : 0;
        $parameter['is_status'] = $data['status'];
        $parameter['kiosk_capacity'] = (isset($data['kiosk_capacity'])) ? $data['kiosk_capacity'] : 0;
        $parameter['is_wheelchair'] = (isset($data['kiosk_wheelchair'])) ? $data['kiosk_wheelchair'] : 0;
        $parameter['is_ada'] = (isset($data['koisk_ads'])) ? $data['koisk_ads'] : 0;
        $parameter['kiosk_notes'] = (isset($data['kiosk_note'])) ? $data['kiosk_note'] : null;
        $parameter['added_by'] = (isset($data['added_by'])) ? $data['added_by'] : null;
        $parameter['modified_date'] = isset($data['modified_date']) ? $data['modified_date'] : null;
        $parameter['modified_by'] = isset($data['modified_by']) ? $data['modified_by'] : null;
        $parameter['kiosk_shutoff'] = (empty($data['kiosk_shutoff_time'])) ? 0 : 1;
        if(!empty($data['kiosk_shutoff_time'])){
            $parameter['kiosk_shutoff'] = implode(",", array($data['kiosk_shutoff_time'],$data['kiosk_shutoff_time_list']));
        }
        return $parameter;
    }


}