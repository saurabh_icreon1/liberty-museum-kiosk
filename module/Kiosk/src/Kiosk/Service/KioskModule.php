<?php

namespace Kiosk\Service;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class KioskModule implements ServiceLocatorAwareInterface {

    protected $serviceLocator;
    protected static $serviceLocatorStatic;

    /**
     * Retrieve service manager instance
     *
     * @return ServiceManager
     */
    public function getServiceLocator() {

        if ($this->serviceLocator === null && self::$serviceLocatorStatic !== null) {
            $this->setServiceLocator(self::$serviceLocatorStatic);
        }

        return $this->serviceLocator;
    }

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {

        $this->serviceLocator = $serviceLocator;

        self::$serviceLocatorStatic = $serviceLocator;

        return $this;
    }
    public function getKioskModules(){
        //error_reporting(1);
        $searchParam['kiosk_name'] = "";
        $searchParam['current_status'] = "";
        $searchParam['is_wheelchair'] = "";
        $searchParam['history'] = "";
        $searchParam['contact_name'] = "";
        $searchParam['grid_main_view'] = '1';
        $searchParam['recordLimit'] = "";
        $searchParam['startIndex'] = "";
        $searchParam['recordLimit'] = "";
        $searchParam['sortField'] = "";
        $searchParam['sortOrder'] = "";
        $searchParam['currentTime'] = DATE_TIME_FORMAT;
        $searchParam['kiosk_id'] = "30";
        $searchParam['curr_date'] = date("Y-m-d",strtotime(DATE_TIME_FORMAT));
        $searchParam['curr_time'] = date("H:i:s",strtotime(DATE_TIME_FORMAT));
        $searchResult = $this->getServiceLocator()->get('Kiosk\Model\KioskReservationTable')->searchKioskReservation($searchParam);
        return $searchResult[0];
        
    }
    public function getModuleStatus($data)
    { //asd($data);
        $result = array();
        $module = @explode(',',$data);
        if(empty($module))
            return false;
        
        foreach($module as $val){
             $module = explode('=>',$val);
             $result[$module[0]] = $module[1];
        }
        return $result;
        
    }
    public function getModuleCofigVal($data)
    { //asd($data);
        $result = array();
        $module = @explode(',',$data);
        if(empty($module))
            return false;
        
        foreach($module as $val){
             $module = explode('=>',$val);
             $result[$module[0]] = $module[1];
        }
        return $result;
        
    }
	 /*Function to get Requesting Machine IP
     */
    public function getRemoteIP(){
        
        if (isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
            $ip = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
        else if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ipList = explode(", ", $_SERVER['HTTP_X_FORWARDED_FOR']);
            $ip = trim($ipList[0]);
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
       return $ip;
    }

}
