<?php

/**
 * This controller is used for kiosk management
 * @package    Kiosk
 * @author     Icreon Tech - SR
 */

namespace Kiosk\Controller;
use Common\Model\Common;
use Base\Model\UploadHandler;
use Authorize\lib\AuthnetXML;
use Base\Controller\BaseController;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Zend\Session\Container as SessionContainer;
use Zend\View\Model\ViewModel;
use Kiosk\Model\KioskReservation;
use Kiosk\Form\KioskForm;
use Group\Model\Group;
use Kiosk\Form\KioskSearchForm;
use Kiosk\Form\MarkPaidForm;
use Kiosk\Model\KioskManagement;
use Kiosk\Model\KioskTransaction;
use Product\Model\Category;
use Kiosk\Model\KioskTransactionTable;
use Zend\Mvc\Controller\Plugin\Params;
use Zend\Http\Request;
use Pledge\Model\Pledge;
use SimpleXMLElement;


/**
 * This controller is used to display user detail
 * @package    User
 * @author     Icreon Tech - DG
 */
class KiosktransactionController extends BaseController {

    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_authservice = null;
    protected $_storage = null;
    protected $_transactionTable = null;

    //protected $auth = null;

    public function __construct() { 
       // error_reporting(E_ALL);
        $this->_flashMessage = $this->flashMessenger();
        $auth = new AuthenticationService();
        $this->_flashMessage = $this->flashMessenger();
        $this->auth = $auth;
    }

    public function indexAction() {
        asd("kiosk index action");
    }

    /**
     * This functikon is used to get table connections
     * @return     array 
     * @author Icreon Tech - DG
     */
    public function KioskTransactionTable() {
        $sm = $this->getServiceLocator();
        $this->_transactionTable = $sm->get('Kiosk\Model\KioskTransactionTable');
        $this->_adapter = $sm->get('dbAdapter');
        $this->_config = $sm->get('Config');
        return $this->_transactionTable;
    }

    /**
     * This functikon is used to get config variables
     * @return     array 
     * @author Icreon Tech - DG
     */
    public function getConfig() {
        $sm = $this->getServiceLocator();
        $this->_translator = $sm->get('translator');
        $this->_config = $sm->get('Config');
    }

    /**
     * This function is used to get auth object
     * @return     array 
     * @param array
     * @author  Icreon Tech - DG
     */
    public function getSessionStorage() {
        if (!$this->_storage) {
            $this->_storage = $this->getServiceLocator()
                    ->get('Kiosk\Model\AuthStorage');
        }
        return $this->_storage;
    }
    
    /**
     * This function is used to list the cash transactions
     * @return     array 
     * @param array
     * @author  Icreon Tech - SR
     */    
    public function kioskTransactionAction(){
        $this->checkUserAuthentication();
        $this->getConfig();
        //$this->getTables();
        $this->layout('crm');
        if ($this->_flashMessage->hasMessages()) {
            $flashMessages = $this->_flashMessage->getMessages();
        }
        $messages = array();
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'messages' => $flashMessages,
            'jsLangTranslate' => $this->_config['kiosk_messages']['config']['kiosk-reservation-system'],
        
        ));
        return $viewModel;
    }
    
    /**
     * This function is used to list the cash transactions list
     * @return     array 
     * @param array
     * @author  Icreon Tech - SR
     */     
    public function getKioskTransactionAction(){
      //  error_reporting(E_ALL);
        $this->checkUserAuthentication();
        $this->KioskTransactionTable();
        $params = $this->params()->fromRoute();
        $searchTransactionMessage = $this->_config['transaction_messages']['config']['search_crm_transaction'];
        $transaction = new KioskTransaction($this->_adapter);
        //$searchTransactionForm = new SearchTransactionForm();
       // $saveSearchForm = new SaveSearchForm();
        $request = $this->getRequest();
        /* end  Saved search data */
        if (($request->isXmlHttpRequest() && $request->isPost()) || $request->getPost('export') == 'excel') {
            $response = $this->getResponse();
            $postArray = $request->getPost()->toArray();
            parse_str($request->getPost('searchString'), $searchParam);
            $dashlet = $request->getPost('crm_dashlet');
            $dashletSearchId = $request->getPost('searchId');
            $dashletId = $request->getPost('dashletId');
            $dashletSearchType = $request->getPost('searchType');
            $dashletSearchString = $request->getPost('dashletSearchString');
            if (isset($dashletSearchString) && $dashletSearchString != '') {
                $searchParam = unserialize($dashletSearchString);
            }
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit; // do not put $limit*($page - 1)
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            $isExport = $request->getPost('export');
            if ($isExport != "" && $isExport == "excel") {
                ini_set('max_execution_time', 0);
                $chunksize = $this->_config['export']['chunk_size'];
                $export_limit = $this->_config['export']['export_limit'];

                $searchParam['record_limit'] = $chunksize;
            }

            if (isset($searchParam['received_date_range']) && $searchParam['received_date_range'] != 1 && $searchParam['received_date_range'] != '') {
                $dateRange = $this->getDateRange($searchParam['received_date_range']);
                $searchParam['received_date_from'] = $this->DateFormat($dateRange['from'], 'db_date_format_from');
                $searchParam['received_date_to'] = $this->DateFormat($dateRange['to'], 'db_date_format_to');
            } else if (isset($searchParam['received_date_range']) && $searchParam['received_date_range'] == '') {
                $searchParam['received_date_from'] = '';
                $searchParam['received_date_to'] = '';
            } else {
                if (isset($searchParam['received_date_from']) && $searchParam['received_date_from'] != '') {
                    $searchParam['received_date_from'] = $this->DateFormat($searchParam['received_date_from'], 'db_date_format_from');
                }
                if (isset($searchParam['received_date_to']) && $searchParam['received_date_to'] != '') {
                    $searchParam['received_date_to'] = $this->DateFormat($searchParam['received_date_to'], 'db_date_format_to');
                }
            }
            $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'modified_date') ? 'usr_transaction.modified_date' : $searchParam['sort_field'];
            if (!empty($searchParam['item_status'])) {

                if (is_array($searchParam['item_status'])) {
                    $searchParam['item_status'] = trim(implode(',', $searchParam['item_status']), ",");
                }
            }

            if (!empty($searchParam['product_type_id'])) {

                if (is_array($searchParam['product_type_id'])) {
                    $searchParam['product_type_id'] = trim(implode(',', $searchParam['product_type_id']), ",");
                }
            }

            if (!empty($searchParam['product_ids'])) {

                if (is_array($searchParam['product_ids'])) {
                    $searchParam['product_ids'] = trim(implode(',', $searchParam['product_ids']), ",");
                }
            }
            $page_counter = 1;
            //ticket:976 Export on CSV
            $fileCounter = 1;
            $filename = "transaction_export_" . EXPORT_DATE_TIME_FORMAT;
            do { 
               // $searchTransactionArr = $transaction->getTransactionSearchArr($searchParam);

                $seachResult = $this->_transactionTable->getAllTransactions($searchParam);
                
                $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
                $dashletResult = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getTableStruct(array('user_id' => $this->auth->getIdentity()->crm_user_id, 'search_id' => $dashletSearchId, 'dash_id' => $dashletId, 'search_type' => $dashletSearchType));
                $dashletColumnName = array();
                if (!empty($dashletResult)) {
                    $dashletColumn = explode(",", $dashletResult[0]['table_column_names']);
                    foreach ($dashletColumn as $val) {
                        if (strpos($val, ".")) {
                            $dashletColumnName[] = trim(strstr($val, ".", false), ".");
                        } else {
                            $dashletColumnName[] = trim($val);
                        }
                    }
                }
                $jsonResult = array();
                $arrCell = array();
                $subArrCell = array();
                $total = $countResult[0]->RecordCount;
                if ($isExport == "excel") {

                    if ($total > $chunksize) {
                        $number_of_pages = ceil($total / $chunksize);


                        $page_counter++;
                        $start = $chunksize * $page_counter - $chunksize;
                        $searchParam['start_index'] = $start;
                        $searchProductParam['page'] = $page_counter;
                    } else {
                        $number_of_pages = 1;
                        $page_counter++;
                    }
                } else {
                    $page_counter = $page;
                    $number_of_pages = ceil($total / $limit);
                    $jsonResult['records'] = $countResult[0]->RecordCount;
                    $jsonResult['page'] = $page;
                    $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
                }

                if (!empty($seachResult)) {
                    $arrExport = array();
                    foreach ($seachResult as $val) {
                        $dashletCell = array();
                        $arrCell['id'] = $val['transaction_id'];
                        $encryptId = $this->encrypt($val['transaction_id']);
                        $viewLink = '&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascript:void(0);" onClick="markPaidTransaction(\'' . $this->encrypt($val['pending_transaction_id']) . '\')" alt="view transaction">Mark Paid</a>&nbsp;';
                        $deleteLink = '<a onclick="deleteTransaction(\'' . $this->encrypt($val['pending_transaction_id']) . '\')" href="#delete_transaction_content" class="delete_transaction delete-icon" alt="delete transaction"><div class="tooltip">Delete<span></span></div></a>';
                        $actions = '<div class="action-col">' . $deleteLink.$viewLink.'</div>';
                        $transactionDate = ($val['transaction_date'] == 0) ? 'N/A' : $this->OutputDateFormat($val['transaction_date'], 'dateformatampm');
                        $fullName = trim($val['full_name']);
                        $fullName = (!empty($fullName)) ? $val['full_name'] : $val['company_name'];
                        $addActivityLink = '';
                        $createRmaLink = '';
                        $viewRmaLink = '';
                        
                        
                        if ($isExport == "excel") {
                            $invoiceNumber = (!empty($val['invoice_number'])) ? $val['invoice_number'] : 'N/A';
                            $val['batch_id'] = str_replace('<br>', '', $val['batch_id']);
                            $batch_id = strpos($val['batch_id'], ",") ? str_replace(",", ", ", $val['batch_id']) : $val['batch_id'];
                            $arrExport[] = array('Name' => $val['full_name'], 'Contact Id' => "'".$val['contact_id'], 'Amount' => $val['amount'],  'Transaction Date' => $transactionDate);
                        } else {
                            $invoiceNumber = (!empty($val['invoice_number'])) ? $val['invoice_number'] : 'N/A';
                            $arrCell['cell'] = array($val['full_name'], $val['contact_id'], $val['amount'], $transactionDate, $actions);
                        }

                        $subArrCell[] = $arrCell;
                        $jsonResult['rows'] = $subArrCell;
                    }
                    if ($isExport == "excel") {
                        $fileLimit = $this->_config['csv_file']['max_records'];
                        $fileCounter = $this->exportCSV($arrExport, $filename, $fileLimit, $fileCounter);
                    }
                } else {
                    $jsonResult['rows'] = array();
                }
            } while ($page_counter <= $number_of_pages && $isExport == "excel");
            if ($isExport == "excel") {
                $this->downloadSendHeaders($filename . ".zip", $this->_config['file_upload_path']['temp_data_upload_dir'] . 'export/');

                $tempfilePath = $this->_config['file_upload_path']['temp_data_upload_dir'] . 'export/';

                $tempFiles = glob($tempfilePath . $filename . '*.csv', GLOB_BRACE);
                foreach ($tempFiles as $file) {
                    if (is_file($file)) {
                        unlink($file);
                    }
                }
                die;
            } else {
                $response->setContent(\Zend\Json\Json::encode($jsonResult));
                return $response;
            }
// return $response->setContent(\Zend\Json\Json::encode($jsonResult));
        } else {
            $this->layout('crm');
            $messages = array();
            if ($this->_flashMessage->hasMessages()) {
                $messages = $this->_flashMessage->getMessages();
            }
            $viewModel = new ViewModel();
            $response = $this->getResponse();
            $viewModel->setVariables(array(
                'form' => $searchTransactionForm,
                'search_form' => $saveSearchForm,
                'messages' => $messages,
                'jsLangTranslate' => array_merge($searchTransactionMessage, $this->_config['transaction_messages']['config']['common_message']),
            ));
            return $viewModel;
        }        
        
    }

    /**
     * This function is used to mark th ecash payment
     * @return     array 
     * @param array
     * @author  Icreon Tech - SR
     */     
public function markPaidKioskTransactionAction(){
        //error_reporting(E_ALL); 
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->KioskTransactionTable();
        $this->layout('popup');
        $request = $this->getRequest();
        $markPaidForm = new MArkPaidForm();
        $kioskMessages = array_merge($this->_config['kiosk_messages']['config']['kiosk-reservation-system'],$this->_config['user_messages']['config']['create_popup_contact']);
        $viewModel = new ViewModel();
        $response = $this->getResponse();
        $flashMessages = array();
        $param = $this->params()->fromRoute();

       // $formData = $request->getPost()->toArray();
       if(!empty($param['pending_transaction_id'])) {
	        $pendingTransactionId = $this->decrypt($param['pending_transaction_id']);        
	        $searchParam['transaction_id'] = $pendingTransactionId;
	        $seachResult = $this->_transactionTable->getAllTransactions($searchParam);
        }

        if ($request->isPost()) {
						                $formData = $request->getPost()->toArray();
						                $searchParam = array();
						                $searchParam['transaction_id'] = $this->decrypt($formData['pending_transaction_id']);
						                $transactionResult = $this->_transactionTable->getAllTransactions($searchParam);
				                        $applicationConfigIds = $this->_config['application_config_ids'];
				                        $uSId = $applicationConfigIds['united_state_country_id'];
				                            $addressInformationId = 0;
				                            $creditCardNo = '';
				                            $postData = $transactionResult[0];
				
				
				                            // If billing addres is not selected, Shipping address will be the billing address
				                            /*if (empty($postData['billing_address_information_id'])) {
				                                $postData['billing_title'] = $postData['shipping_title'];
				                                $postData['billing_first_name'] = $postData['shipping_first_name'];
				                                $postData['billing_last_name'] = $postData['shipping_last_name'];
				                                $postData['billing_address_1'] = $postData['shipping_address'];
				                                $postData['billing_address_2'] = $postData['shipping_address_2'];
				                                $postData['billing_country'] = $postData['shipping_country_id'];
				                                $postData['billing_country_text'] = $postData['shipping_country_text'];
				                                $postData['billing_state'] = $postData['shipping_state'];
				                                $postData['billing_state_id'] = $postData['shipping_state'];
				                                $postData['billing_city'] = $postData['shipping_city'];
				                                $postData['billing_zip_code'] = $postData['shipping_postal_code'];
				                                $postData['billing_phone_info_id'] = $postData['shipping_phone_info_id'];
				                                $postData['billing_country_code'] = $postData['shipping_country_code'];
				                                $postData['billing_area_code'] = $postData['shipping_area_code'];
				                                $postData['billing_phone'] = $postData['shipping_phone_no'];
				                                $postData['billing_company'] = $postData['shipping_company'];
				                            }*/
				
				                            // End If billing addres is not selected, Shipping address will be the billing address            
				                            //asd($postData);
				
				                            $arrPostData = $postData;
				
				                            $shipcode = isset($postData['shipping_postal_code']) ? $postData['shipping_postal_code'] : '';
				                            $user_id = $transactionResult[0]['user_id'];
				                            $cartParam['user_id'] = $user_id;
				                            $cartParam['source_type'] = 'frontend';
				
				                            $paramArray['postData'] = $arrPostData;
				                            if (isset($arrPostData['shipping_method']) && !empty($arrPostData['shipping_method']))
				                                $paramArray['shipping_method'] = $arrPostData['shipping_method'];
				                            else
				                                $paramArray['shipping_method'] = '';
				                            $paramArray['source_type'] = 'frontend';
				                            $paramArray['userid'] = $user_id;
				                            $paramArray['shipcode'] = $shipcode;
				                            $paramArray['couponcode'] = !empty($postData['couponcode']) ? $postData['couponcode'] : '';

				                            $itemsTotal = $this->cartTotalWithTax($paramArray);
				                       
				
				                        $postData['promotion_id'] = $promotionId;
				                        $postData['source_type'] = 'frontend';
				                        $postData['source_all'] = 'all';
				                        //  asd($postData);
				
				                        $postData['pick_up'] = isset($postData['pick_up']) ? $postData['pick_up'] : '0';
				
				                        if (isset($postData['cash_payment']) && $postData['cash_payment'] == '1') {
				                            $postData['payment_mode_cash'] = 1;
				                            $postData['payment_mode_cash_amount'] = $itemsTotal['finaltotal'];
				                        } else {
				                            $postData['payment_mode_credit'] = 1;
				                            $postData['payment_mode_credit_card_amount'] = $itemsTotal['finaltotal'];
				                        }
				
				
				                        $postData['transaction_ids'] = 'IN' . time();
				                        $postData['transaction_amounts'] = $paymentInfo['amount'];
				                        //$postData['customer_profile_ids'] = $responseMessage['customerProfileId'];
				                        $postData['customer_profile_ids'] = $postData['credit_card_exist']; //$responseMessage['customerProfileId'];
				                        $postData['coupon_code'] = $paramArray['couponcode'];
				
				                        if (isset($postData['is_apo_po']) && $postData['is_apo_po'] == '3') {
				                            $postData['shipping_state'] = $postData['apo_po_state'];
				                            $postData['shipping_country'] = $postData['apo_po_shipping_country'];
				                        } else if (isset($postData['shipping_country']) && $postData['shipping_country'] == $uSId) {
				                            $postData['shipping_state'] = $postData['shipping_state_id'];
				                            if ($postData['is_apo_po'] == '2') {
				                                $postData['shipping_country'] = $postData['apo_po_shipping_country'];
				                            }
				                        }

                //asd($postData);

                            $res = $this->saveFinalTransaction($postData);
                            
                            if($res['status'] == 'success') {
                                    $this->flashMessenger()->addMessage("Status has been changed successfully.");
									$response->setContent(\Zend\Json\Json::encode(array('status'=>'success','message'=>'Status has been changed successfully.')));
                                    return $response;                            
                            }
                            else {
                                    $this->flashMessenger()->addMessage("Status has not been changed.");
									$response->setContent(\Zend\Json\Json::encode(array('status'=>'error','message'=>'Status has not been changed.')));
                                    return $response;                                                        
                            }
                        }
                
               
            
        
        if ($this->_flashMessage->hasMessages()) {
            $flashMessages = $this->_flashMessage->getMessages();
        }
        $viewModel->setVariables(array(
            'form'=>$markPaidForm,
            'jsLangTranslate' => $kioskMessages,
            'messages' => $flashMessages,
            'seachResult' =>$seachResult 
        ));
        return $viewModel;        
    
    
}    
    

/**
     * This action is used to save transaction from cart
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function saveFinalTransaction($postArr) { // by dev 3
       // error_reporting(E_ALL);
        $response = $this->getResponse();
        $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter')->getDriver()->getConnection()->beginTransaction();
        $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->offAutoCommit();

        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $postArr['shipping_method'] = (!empty($postArr['shipping_method']) && (empty($postArr['pick_up']) || $postArr['pick_up'] == 0)) ? $postArr['shipping_method'] : '';
        $postArr['is_free_shipping'] = '0';
        $responseArr = array();
        $cartProductArr = array();
        $cartProductArr['user_id'] = $postArr['user_id'];
        $cartProductArr['userid'] = $postArr['user_id'];
        $cartProductArr['source_type'] = !empty($postArr['source_type']) ? $postArr['source_type'] : 'backend';
        $cartProductArr['shipcode'] = isset($postArr['shipping_zip_code']) ? $postArr['shipping_zip_code'] : '';
        $cartProductArr['source_all'] = !empty($postArr['source_all']) ? $postArr['source_all'] : '';
        $cartProductArr['shipping_method'] = $postArr['shipping_method'];
        $cartProductArr['couponcode'] = !empty($postArr['couponcode']) ? $postArr['couponcode'] : '';
        $cartProductArr['postData'] = $postArr;
        $cartProductArr['free_shipping'] = false;
        $isFreeShipping = false;
        $postArr['is_free_shipping'] = '0';
        $freeShippingAmount = '';


       $transaction = new KioskTransaction($this->_adapter);


        //$this->campaignSession = new Container('campaign');
        $cartProductData = $this->cartTotalWithTax($cartProductArr);
      //  asd($cartProductData);

        $postArr['promotion_id'] = $cartProductData['promotionData']['promotion_id'];

        if (isset($cartProductData['cartData'])) {
            $cartProducts = $cartProductData['cartData'];
        }
        $responseArr['status'] = 'error';
        if (!empty($cartProducts)) {
            $totalSubtotalAmount = 0;
            $totalDiscountAmount = 0;
            $totalTaxableAmount = 0;
            $totalCartAmount = 0;
            $totalShippingAmount = 0;
            $totalShippingHandlingAmount = 0;

            $pledgeDonationAmount = 0;
            $shipCompleteStatus = 0;
            $userId = 0;
            $i = 0;
            $donationTotalAmount = 0;
            $unshipProductTotalAmount = 0;
            $arrUnshipProductTotalAmount = array();
            $arrAllProductTotalAmount = array();
            $transactionTypeDonation = '';
            $transactionTypePurchase = '';
            $productQtyErrorArr = array();
            $totalProductsInCart = count($cartProducts);

            $isSaveShippingAddress = 0;
            $totalCashAmount = 0;
            $totalCheckAmount = 0;
            $totalCreditAmount = 0;
            $shippableProductsAmount = 0;
            $transactionStatus = 1;
            $countProStatus = 0;
            $totalProductRevenue = 0;
            $donAmt = 0;
            $paymentArray = array();


##################################################################################################################################
            $sendThankyouMail = 0;
            foreach ($cartProducts as $cartKey => $cartProduct) {
                $productLabel = '';
                $totalSubtotalAmount+= $cartProduct['product_subtotal'] - $cartProduct['membership_discount'];
                $totalDiscountAmount+= $cartProduct['product_discount'];
                $totalTaxableAmount+= $cartProduct['product_taxable_amount'];
                $totalCartAmount+=$cartProduct['product_total'];
                $totalProductRevenue+=$cartProduct['total_product_revenue'];


                if (isset($cartProduct['shipping_discount_in_amount']))
                    $totalShippingAmount+=$cartProduct['shipping_price'] - $cartProduct['shipping_discount_in_amount'];
                else
                    $totalShippingAmount+=$cartProduct['shipping_price'];

                $totalShippingHandlingAmount+=$cartProduct['surcharge_price'] + $cartProduct['shipping_handling_price'];
                $productTotal = $cartProduct['product_total'];

                if ($cartProduct['product_type_id'] == '2') {
                    $pledgeDonationAmount+=$cartProduct['product_total'];
                    $sendThankyouMail = 1;
                }
                if ($cartProduct['product_transaction_type'] == '1') {
                    $transactionTypeDonation = '1';
                } else {
                    $transactionTypePurchase = '2';
                }

                if ($cartProduct['product_type_id'] != '4') {
                    if ($cartProduct['is_shippable'] == '0' || empty($cartProduct['is_shippable'])) {
                        $shipCompleteStatus++;
                        $unshipProductTotalAmount = $unshipProductTotalAmount + $productTotal;
                        //$arrUnshipProductTotalAmount['unshipped_product_amount'][] = $productTotal;
                        $arrUnshipProductTotalAmount['product_type_id'][] = $cartProduct['product_type_id'];
                        $paymentArray[$cartKey]['cart_product_amount'] = round($productTotal, 2);
                        $paymentArray[$cartKey]['cart_product_type_id'] = $cartProduct['product_type_id'];
                        //$paymentArray[$cartKey]['unshipped_product_amount'] = round($productTotal, 2);
                        $paymentArray[$cartKey]['product_type_id'] = $cartProduct['product_type_id'];
                        $paymentArray[$cartKey]['payment_recieve'] = '1';
                    } else {
                        $shippableProductsAmount+=$shippableProductsAmount + ($cartProduct['product_subtotal'] - $cartProduct['membership_discount']);
                        $isSaveShippingAddress = 1;
                        $paymentArray[$cartKey]['cart_product_amount'] = round($productTotal, 2);
                        $paymentArray[$cartKey]['cart_product_type_id'] = $cartProduct['product_type_id'];
                        //$paymentArray[$cartKey]['unshipped_product_amount'] = round($productTotal, 2);
                        $paymentArray[$cartKey]['product_type_id'] = $cartProduct['product_type_id'];
                        $paymentArray[$cartKey]['payment_recieve'] = '2';
                    }
                } else {
                    $paymentArray[$cartKey]['cart_product_amount'] = round($productTotal, 2);
                    $paymentArray[$cartKey]['cart_product_type_id'] = $cartProduct['product_type_id'];
                    //$paymentArray[$cartKey]['unshipped_product_amount'] = round($productTotal, 2);
                    $paymentArray[$cartKey]['product_type_id'] = $cartProduct['product_type_id'];
                    $paymentArray[$cartKey]['payment_recieve'] = '2';
                }
                if ($cartProduct['product_mapping_id'] == '1' || $cartProduct['product_mapping_id'] == '3' || $cartProduct['product_mapping_id'] == '5') {
                    $transactionStatus = 9;
                    $countProStatus++;
                }
                if ($cartProduct['product_transaction_type'] == 1) {
                    $donAmt = $donAmt + ($cartProduct['product_subtotal'] - $cartProduct['membership_discount']);
                }

                if ($cartProduct['product_type_id'] == 1 && $cartProduct['product_qty'] > $cartProduct['total_in_stock']) {
                    $productQtyErrorArr['message'] = sprintf($msgArr['QUANTITY_EXCEED'], $cartProduct['total_in_stock'], $cartProduct['product_name']);
                }
                $i++;
            }
            $userId = $postArr['user_id'];

            if (!empty($productQtyErrorArr)) {
                $productQtyErrorArr['quantity_error'] = 1;
                $productQtyErrorArr['status'] = 'quantity_error';
                return $productQtyErrorArr; //vvvvvvvvvvvvvvvv
            }

            $transactionType = trim($transactionTypeDonation . ',' . $transactionTypePurchase, ',');

            if ($countProStatus == count($cartProducts)) {
                $transactionStatus = '10';
            }


            /* Add transaction record */
            $finalTransaction = $postArr;
            $dateTime = DATE_TIME_FORMAT;

            $crm_user_id = !(empty($this->auth->getIdentity()->crm_user_id)) ? $this->auth->getIdentity()->crm_user_id : '';
            $transactionSourceId = ($cartProductArr['source_type'] == 'backend') ? $this->_config['transaction_source']['transaction_source_crm_id'] : $this->_config['transaction_source']['transaction_source_id'];
            $finalTransaction['transaction_source_id'] = $transactionSourceId = 2;
            $finalTransaction['num_items'] = $totalProductsInCart;
            $finalTransaction['sub_total_amount'] = $totalSubtotalAmount;
            $finalTransaction['total_discount'] = $totalDiscountAmount;
            $finalTransaction['shipping_amount'] = $totalShippingAmount;
            $finalTransaction['handling_amount'] = $totalShippingHandlingAmount;
            $finalTransaction['total_tax'] = $totalTaxableAmount;
            $finalTransaction['transaction_amount'] = $totalCartAmount;
            $finalTransaction['transaction_date'] = $dateTime;
            $finalTransaction['transaction_type'] = $transactionType;
            $finalTransaction['transaction_status_id'] = $transactionStatus=10;
            $finalTransaction['added_by'] = $crm_user_id;
            $finalTransaction['added_date'] = $dateTime;
            $finalTransaction['modify_by'] = $crm_user_id;
            $finalTransaction['modify_date'] = $dateTime;
            $finalTransaction['campaign_code'] = '';
            $finalTransaction['campaign_id'] = '';
            $finalTransaction['channel_id'] = '';
            $finalTransaction['don_amount'] = $donAmt;
            $finalTransaction['total_product_revenue'] = $totalProductRevenue;
            if (isset($postArr['payment_mode_invoice']) && $postArr['payment_mode_invoice'] == 1) {
                $finalTransaction['transaction_status_id'] = 10;
            }


            /* General Campaign Code */
            if (isset($this->auth->getIdentity()->crm_user_id) && !empty($this->auth->getIdentity()->crm_user_id))
                $transactionChannelId = 2; // Direct mail
            else
                $transactionChannelId = 5; // Web

            if (!isset($postArr['campaign_code_id']) || empty($postArr['campaign_code_id'])) {
                $generalCampaign = $this->getServiceLocator()->get('Kiosk\Model\KioskreservationTable')->getGeneralCampaign();

                if (isset($generalCampaign) && !empty($generalCampaign)) {
                    $finalTransaction['campaign_code'] = $generalCampaign[0]['campaign_code'];
                    $finalTransaction['campaign_id'] = $generalCampaign[0]['campaign_id'];
                    $finalTransaction['channel_id'] = $transactionChannelId;
                }

                $finalTransaction['campaign_code'] = (isset($this->campaignSession->campaign_code) && !empty($this->campaignSession->campaign_code) ? $this->campaignSession->campaign_code : $finalTransaction['campaign_code']);
                $finalTransaction['campaign_id'] = (isset($this->campaignSession->campaign_id) && !empty($this->campaignSession->campaign_id) ? $this->campaignSession->campaign_id : $finalTransaction['campaign_id']);
                $finalTransaction['channel_id'] = (isset($this->campaignSession->channel_id) && !empty($this->campaignSession->channel_id) ? $this->campaignSession->channel_id : $finalTransaction['channel_id']);
            } else {
                $finalTransaction['campaign_code'] = (isset($postArr['campaign_code']) && !empty($postArr['campaign_code']) ? $postArr['campaign_code'] : $finalTransaction['campaign_code']);
                $finalTransaction['campaign_id'] = (isset($postArr['campaign_code_id']) && !empty($postArr['campaign_code_id']) ? $postArr['campaign_code_id'] : $finalTransaction['campaign_id']);
                $searchParam['campaign_id'] = $finalTransaction['campaign_id'];
                $searchResultCamp = $this->getServiceLocator()->get('Campaign\Model\CampaignTable')->searchCampaignDetails($searchParam);
                if (isset($searchResultCamp[0]['channel_id']) && !empty($searchResultCamp[0]['channel_id'])) {
                    $arr = explode(",", $searchResultCamp[0]['channel_id']);
                    $finalTransaction['channel_id'] = $transactionChannelId;
                    //if (is_array($arr) && count($arr) > 0 && in_array(5 , $arr))
                    if (is_array($arr) && count($arr) > 0 && in_array($transactionChannelId, $arr)) {
                        //update user campaign channel
                        $finalTransaction['user_id'] = (isset($finalTransaction['user_id'])
                                && !empty($finalTransaction['user_id']) ? $finalTransaction['user_id'] : '');
                        $updatedResponse = $this->getServiceLocator()->get('Campaign\Model\CampaignTable')->userUpdateCampaign(array(
                            'user_id' => $finalTransaction['user_id'], 'campaign_id' => $finalTransaction['campaign_id'],
                            'channel_id' => $transactionChannelId));
                    }
                } else {
                    $finalTransaction['channel_id'] = $transactionChannelId;
                }
            }
            $group_id = $this->getServiceLocator()->get('Kiosk\Model\KioskreservationTable')->getGroupId($finalTransaction);
            $finalTransaction['group_id'] = isset($group_id[0]['group_id']) && !empty($group_id[0]['group_id']) ? $group_id[0]['group_id'] : "";
            $pbShippingTypeId = '';
            if (!empty($finalTransaction['shipping_method'])) {
                $shippingMetArr = explode(',', $finalTransaction['shipping_method']);
                $pbShippingTypeId = (!empty($shippingMetArr[2])) ? $shippingMetArr[2] : '';
            }
            $finalTransaction['shipping_type_id'] = $pbShippingTypeId;
            $transaction = new KioskTransaction($this->_adapter);
            if ($totalProductsInCart == $shipCompleteStatus || $postArr['invoice_number'] != '') {
                $finalTransaction['transaction_status_id'] = 10;
            } elseif (!empty($shipCompleteStatus) && $shipCompleteStatus < $totalProductsInCart) {
                $finalTransaction['transaction_status_id'] = 11;
            }
            $finalTransactionArr = $transaction->getFinalTransactionArr($finalTransaction);
            $transactionId = $this->getServiceLocator()->get('Kiosk\Model\KioskreservationTable')->saveFinalTransaction($finalTransactionArr);


            $this->getServiceLocator()->get('Kiosk\Model\KioskreservationTable')->updateRevenueGoal($finalTransactionArr);
            $this->getServiceLocator()->get('Kiosk\Model\KioskreservationTable')->updateChannelRevenue($finalTransactionArr);
			$this->getServiceLocator()->get('Transaction\Model\TransactionTable')->updateTransctionCustomerIP(array($transactionId,$_SERVER['REMOTE_ADDR']));
            /* end add transaction record */

            $finalTransaction['transaction_id'] = $transactionId;

            $isPromotionApplied = '0';
            /* Code start for save coupon used by user */
            if (!empty($postArr['promotion_id'])) {
                $isPromotionApplied = '1';
                $promotionDetailArr = array();
                $promotionDetailArr[] = $postArr['promotion_id'];
                $promotionTable = $this->getServiceLocator()->get('Promotions\Model\PromotionTable');
                $getPromotionDetail = $promotionTable->getPromotionByPromotionId($promotionDetailArr);

                if ($getPromotionDetail['usage_limit_type'] != '1') {
                    $couponUsedArr = $transaction->getCouponUsedArr($finalTransaction);
                    $couponLogId = $this->getServiceLocator()->get('Kiosk\Model\KioskreservationTable')->saveCouponUsedArr($couponUsedArr);
                }
            }
            /* End  Code for save coupon used by user */
            /* Add payment information  of transaction */

            $amount_total = $totalCartAmount;
            $amountIdArray = array();
            /* 
            $crm_user_id = !(empty($this->auth->getIdentity()->crm_user_id)) ? $this->auth->getIdentity()->crm_user_id : '';
              if (isset($postArr['payment_mode_cash']) && $postArr['payment_mode_cash'] == 1) {
              $paymentTransactionData = array();
              $paymentTransactionData['amount'] = $totalCartAmount;
              $paymentTransactionData['transaction_id'] = $transactionId;
              $paymentTransactionData['payment_mode_id'] = 4;
              $paymentTransactionData['added_by'] = $crm_user_id;
              $paymentTransactionData['added_date'] = $dateTime;
              $paymentTransactionData['modify_by'] = $crm_user_id;
              $paymentTransactionData['modify_date'] = $dateTime;
              $paymentTransactionData = $transaction->getTransactionPaymentArr($paymentTransactionData);
              $paymentTransactionData['22'] = '';
              $transactionPaymentId = $this->_transactionTable->saveTransactionPaymentDetail($paymentTransactionData);
           
                    $paymentReceivedTransactionData = array();
                    $paymentReceivedTransactionData['table_auto_id'] = $productLastInsertedId;
                    $paymentReceivedTransactionData['table_type'] = 4; // transaction product
                    $paymentReceivedTransactionData['authorize_transaction_id'] = '';
                    $paymentReceivedTransactionData['profile_id'] = ''; //$customerProfileId;
                    $paymentReceivedTransactionData['amount'] = totalCartAmount;
                    $paymentReceivedTransactionData['product_type_id'] = 3;
                    $paymentReceivedTransactionData['transaction_payment_id'] = $transactionPaymentId;
                    $paymentReceivedTransactionData['transaction_id'] = $transactionId;
                    $paymentReceivedTransactionData['payment_source'] = 'card';
                    $paymentReceivedTransactionData['isPaymentReceived'] = '1';
                    $paymentReceivedTransactionData['add_date'] = $postParam['add_date'];
                    $this->insertIntoPaymentReceive($paymentReceivedTransactionData);          
            */
            
            $postArr['source_type'] = $cartProductArr['source_type'];
            $postArr['address_type_status'] = "0";
            if (!empty($postArr['save_shipping_address']) && $postArr['save_shipping_address'] == '1' && isset($postArr['shipping_phone']) && trim($postArr['shipping_phone']) != "" && is_numeric(trim($postArr['shipping_phone']))) {
                $postArr['address_type_status'] = "1";
                $searchArrayN1 = array();
                $searchArrayN1['user_id'] = $postArr['user_id'];
                $phoneDetail1 = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserPhoneInformations($searchArrayN1);
                $postArr['shipping_phone_info_id'] = (!empty($phoneDetail1[0]['phone_contact_id'])) ? $phoneDetail1[0]['phone_contact_id'] : "";
            } elseif (!empty($postArr['save_billing_address']) && $postArr['save_billing_address'] == '1' && isset($postArr['billing_phone']) && trim($postArr['billing_phone']) != "" && is_numeric(trim($postArr['billing_phone']))) {
                $postArr['address_type_status'] = "2";
                $searchArrayN1 = array();
                $searchArrayN1['user_id'] = $postArr['user_id'];
                $phoneDetail1 = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserPhoneInformations($searchArrayN1);
                $postArr['billing_phone_info_id'] = (!empty($phoneDetail1[0]['phone_contact_id'])) ? $phoneDetail1[0]['phone_contact_id'] : "";
            }

           // if (!empty($postArr['save_billing_address']) && $postArr['save_billing_address'] == '1') {
            //    $postArr['address_type'] = 'billing';
            //    $this->insertUpdateBillingShipping($postArr);
           // }
           // if (!empty($postArr['save_shipping_address']) && $postArr['save_shipping_address'] == '1') {
           //     $postArr['address_type'] = 'shipping';
          //      $this->insertUpdateBillingShipping($postArr);
           // }
            /* End add payment information  of transaction */
            /* Add transaction note info */
            if (!empty($finalTransaction['notes']) && trim($finalTransaction['notes']) != '') {
                $transactionNoteData = array();
                $transactionNoteData['transaction_id'] = $transactionId;
                $transactionNoteData['note'] = $finalTransaction['notes'];
                $transactionNoteData['added_by'] = $crm_user_id;
                $transactionNoteData['added_date'] = $dateTime;
                $transactionNoteData['modify_date'] = $dateTime;
                //$transactionNoteArr = $transaction->getTransactionNoteArr($transactionNoteData);
                //$transactionNoteId = $this->_transactionTable->saveTransactionNote($transactionNoteArr);
            }
            /* Add transaction note info */
            /* Add transaction billing shipping address info */
            $finalTransaction['transaction_id'] = $transactionId;
            $pickUp = !empty($finalTransaction['pick_up']) ? $finalTransaction['pick_up'] : '';
            $phoneNumFinal = "";
            if (isset($finalTransaction['shipping_country_code']) and trim($finalTransaction['shipping_country_code']) != "" and is_numeric(trim($finalTransaction['shipping_country_code']))) {
                $phoneNumFinal .= trim($finalTransaction['shipping_country_code']);
            }
            if (isset($finalTransaction['shipping_area_code']) and trim($finalTransaction['shipping_area_code']) != "" and is_numeric(trim($finalTransaction['shipping_area_code']))) {
                $phoneNumFinal .= "-" . trim($finalTransaction['shipping_area_code']);
            }
            if (isset($finalTransaction['shipping_phone']) and trim($finalTransaction['shipping_phone']) != "" and is_numeric(trim($finalTransaction['shipping_phone']))) {
                $phoneNumFinal .= "-" . trim($finalTransaction['shipping_phone']);
            }

            $phoneNumFinal2 = "";
            if (isset($finalTransaction['billing_country_code']) and trim($finalTransaction['billing_country_code']) != "" and is_numeric(trim($finalTransaction['billing_country_code']))) {
                $phoneNumFinal2 .= trim($finalTransaction['billing_country_code']);
            }
            if (isset($finalTransaction['billing_area_code']) and trim($finalTransaction['billing_area_code']) != "" and is_numeric(trim($finalTransaction['billing_area_code']))) {
                $phoneNumFinal2 .= "-" . trim($finalTransaction['billing_area_code']);
            }
            if (isset($finalTransaction['billing_phone']) and trim($finalTransaction['billing_phone']) != "" and is_numeric(trim($finalTransaction['billing_phone']))) {
                $phoneNumFinal2 .= "-" . trim($finalTransaction['billing_phone']);
            }

            $finalTransaction['shipping_phone'] = $phoneNumFinal;
            $finalTransaction['billing_phone'] = $phoneNumFinal2;
            if ((!empty($isSaveShippingAddress) && $isSaveShippingAddress == 0) ||
                    ($postArr['totalProducts'] > 0 && $postArr['totalProducts'] == $postArr['shipCompleteStatus'])) {
                $finalTransaction['shipping_title'] = '';
                $finalTransaction['shipping_first_name'] = '';
                $finalTransaction['shipping_last_name'] = '';
                $finalTransaction['shipping_company'] = '';
                $finalTransaction['shipping_address'] = '';
                $finalTransaction['shipping_city'] = '';
                $finalTransaction['shipping_state'] = '';
                $finalTransaction['shipping_zip_code'] = '';
                $finalTransaction['shipping_country'] = '';
                $finalTransaction['shipping_phone'] = '';
            }

            if (isset($postArr['pick_up']) && $postArr['pick_up'] == 1) {
                $finalTransaction['shipping_title'] = '';
                $finalTransaction['shipping_first_name'] = '';
                $finalTransaction['shipping_last_name'] = '';
                $finalTransaction['shipping_company'] = '';
                $finalTransaction['shipping_address'] = '';
                $finalTransaction['shipping_city'] = '';
                $finalTransaction['shipping_state'] = '';
                $finalTransaction['shipping_zip_code'] = '';
                $finalTransaction['shipping_country'] = '';
                $finalTransaction['shipping_phone'] = '';
            }
			$finalTransaction['shipping_country'] = $finalTransaction['shipping_country_id'];
			$finalTransaction['billing_phone'] = $finalTransaction['shipping_phone_no'];
			$finalTransaction['billing_country'] = $finalTransaction['billing_country_id'];
			$finalTransaction['shipping_phone'] = $finalTransaction['shipping_phone_no'];
			$finalTransaction['shipping_zip_code'] = $finalTransaction['shipping_postal_code'];
			$finalTransaction['shipping_address'] = $finalTransaction['shipping_address'];
  //           asd($finalTransaction,2);
           $transactionBillingShippingArr = $transaction->getTransactionBillingShipping($finalTransaction);
           $transactionBillingShippingId = $this->_transactionTable->saveTransactionBillingShippingDetail($transactionBillingShippingArr);

            /* End add transaction billing shipping address info */
            /* Add cart product to transaction table */

            $isShipping = 0;
//asd($cartProducts);
            if (!empty($cartProducts)) {

                $arrDuplicateCertificate = array();
                foreach ($cartProducts as $cartKey => $cartProduct) {
                    if ($cartProduct['is_shippable'] == '1') {
                        $isShipping = 1;
                    }
                    $insertCartDataArr = $cartProduct;
                    $insertCartDataArr['transaction_id'] = $transactionId;
                    $insertCartDataArr['added_by'] = $crm_user_id;
                    $insertCartDataArr['added_date'] = $dateTime;
                    $insertCartDataArr['modify_by'] = $crm_user_id;
                    $insertCartDataArr['modify_date'] = $dateTime;
                    $insertCartDataArr['type_of_product'] = $cartProduct['type_of_product'];

                    if (!empty($cartProduct['item_name']))
                        $insertCartDataArr['item_name'] = $cartProduct['item_name'];

                    if (!empty($cartProduct['manifest_info']))
                        $insertCartDataArr['manifest_info'] = $cartProduct['manifest_info'];

                    if (!empty($cartProduct['manifest_type']))
                        $insertCartDataArr['manifest_type'] = $cartProduct['manifest_type'];


                    if ($cartProduct['product_type_id'] == 4) {
                        $search = array();
                        $search['fof_id'] = $cartProduct['cart_id'];
                        $searchResult = $this->getServiceLocator()->get('User\Model\UserTable')->getFofCart($search);
                        $fofImage = $searchResult[0]['fof_image'];
                        $this->moveFileFromTempToFOF($fofImage); // ?????????????????????????
                    }

                    
                    if ($cartProduct['product_type_id'] == '2') {
                        if (!empty($cartProduct['campaign_id'])) {
                            $donation_amount = $cartProduct['product_amount'];
                            $campaign_id_backend = $cartProduct['campaign_id'];
                            $campArrayBack['campaign_id'] = $campaign_id_backend;
                            $campArrayBack['revenue_recieved'] = $donation_amount;
                            $this->getServiceLocator()->get('Campaign\Model\CampaignTable')->updateRevenueGoal($campArrayBack);
                        }
                        $searchDonNotify = $this->_transactionTable->getDonNotify($cartProduct);
                        foreach ($searchDonNotify as $key => $val) {
                            if ($val['email_id'] != 'Email') {
                                $donationNotifyArr['donarName'] = $val['name'];
                                $name = explode(" ", $val['notify_name']);
                                $donationNotifyArr['first_name'] = $name[0];
                                $donationNotifyArr['last_name'] = $name[1];
                                $donationNotifyArr['name'] = $val['notify_name'];
                                $donationNotifyArr['email_id'] = $val['email_id'];
                                $donationNotifyArr['amount'] = $cartProduct['product_amount'];
                                $donationNotifyArr['program_name'] = $val['program'];
                                $email_id_template_int = 15;
                                $donationNotifyArr['admin_email'] = $this->_config['soleif_email']['email'];
                                $donationNotifyArr['admin_name'] = $this->_config['soleif_email']['name'];
                                $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($donationNotifyArr, $email_id_template_int, $this->_config['email_options']);
                            }
                        }
					}

                    if (isset($cartProduct['type_of_product']) and isset($cartProduct['item_info']) and trim($cartProduct['type_of_product']) != "" and trim($cartProduct['item_info']) != "" and trim($cartProduct['type_of_product']) == "personalizeduplicate" and trim($cartProduct['item_info']) == "1") {
                        $insertCartDataArr['item_info'] = "2";

                        $insertCartArr = $transaction->getInsertCartToTransactionArr($insertCartDataArr);
                        array_push($arrDuplicateCertificate, array('arr1' => $insertCartArr, 'cartkey' => $cartKey));
                    } else {
                        $insertCartArr = $transaction->getInsertCartToTransactionArr($insertCartDataArr);

                        $productTransactionId = $this->_transactionTable->saveCartToTransaction($insertCartArr);


                        $paymentArray[$cartKey]['table_auto_id'] = $productTransactionId;
                        $paymentArray[$cartKey]['transaction_id'] = $transactionId;
                        $arrUnshipProductTotalAmount['table_auto_id'][] = $productTransactionId;
                        if ($cartProduct['product_type_id'] == 2) { //Donation
                            if ($cartProduct['product_mapping_id'] == 5)
                                $paymentArray[$cartKey]['table_type'] = 4;
                            else
                                $paymentArray[$cartKey]['table_type'] = 2;
                        }
                        else if ($cartProduct['product_type_id'] == 4) //FOF
                            $paymentArray[$cartKey]['table_type'] = 3;
                        else if ($cartProduct['product_type_id'] == 6) { //WOH
                            if ($cartProduct['type_of_product'] == 'biocertificate')
                                $paymentArray[$cartKey]['table_type'] = 1;
                            elseif ($cartProduct['type_of_product'] == 'woh')
                                $paymentArray[$cartKey]['table_type'] = 5;
                            else
                                $paymentArray[$cartKey]['table_type'] = 4;
                        }
                        else {
                            $paymentArray[$cartKey]['table_type'] = 4; // for transaction
                        }
// }
//}
                    }
                }

                if (isset($arrDuplicateCertificate) and count($arrDuplicateCertificate) > 0) { //????????????????????????????????????????????
                    foreach ($arrDuplicateCertificate as $valDC) {
                        $productTransactionId = $this->getServiceLocator()->get('Kiosk\Model\KioskreservationTable')->saveCartToTransaction($valDC['arr1']);
                        $paymentArray[$valDC['cartkey']]['table_auto_id'] = $productTransactionId; // for transaction
                        $paymentArray[$valDC['cartkey']]['table_type'] = 4; // for transaction
                        $paymentArray[$valDC['cartkey']]['transaction_id'] = $transactionId; // for transaction
                    }
                }
            }
            
            if ($cartProductArr['source_type'] == 'frontend') {
                $paymentFrontArray = array();
                $paymentFrontArray['profile_id'] = $postArr['credit_card_exist'];
                $paymentFrontArray['transaction_amounts'] = $totalCartAmount;
                $paymentFrontArray['transaction_id'] = $transactionId;
                $paymentSuccessStatus = $this->capturePaymentFront($paymentArray, $paymentFrontArray, $isPromotionApplied); // by dev 3
            }
            
            
//asd($paymentArray);

            // Ticket 645
            
            if ((!empty($transactionId)) && $postArr['invoice_number'] != '') {
                $updateInvoiceParams = array();
                $updateInvoiceParams['transactionId'] = $transactionId;
                $updateInvoiceParams['status'] = 10;
                $this->_transactionTable->updateInvoiceAllProduct($updateInvoiceParams);

                $data = array();
                $data['modified_by'] = $crm_user_id;
                $data['modified_date'] = $dateTime;
                $data['transactionId'] = $transactionId;
                $data['shippingChannelId'] = '2';
                $data['shipmentId'] = $shipmentId = $this->_transactionTable->insertShipment($data);

                $searchProductShipment = $this->_transactionTable->searchTransactionProducts(array('transactionId' => $transactionId));
                if ($searchProductShipment) {
                    foreach ($searchProductShipment as $key => $value) {
                        $shipmentPro = array();
                        $shipmentPro['productType'] = $value['item_type'];
                        $shipmentPro['productId'] = $value['id'];
                        $shipmentPro['productQty'] = $value['product_quantity'];
                        $shipmentPro['modified_by'] = $crm_user_id;
                        $shipmentPro['modified_date'] = $dateTime;
                        $shipmentPro['transactionId'] = $transactionId;
                        $shipmentPro['shipmentId'] = $shipmentId;
                        $shipmentPro['shippingChannelId'] = '2';
                        $shipmentPro['is_default'] = '0';

                        $this->getTransactionTable()->insertShipmentProduct($shipmentPro);
                    }
                }
            }
       

         
            //    $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter')->getDriver()->getConnection()->rollback();
           //     $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->onAutoCommit();
                
             

// if ($donationTotalAmount > 0) {
            $associationArr = array();
            $associationArr['user_id'] = $userId;
            $associationArr['association'] = '1';
            $this->getServiceLocator()->get('User\Model\UserTable')->updateUserAssociation($associationArr);
            $userPurchasedProductArr = array();
            $userPurchasedProductArr['user_id'] = $userId;

            $date_range = $this->getDateRange(4);
            $userPurchasedProductArr['from_date'] = $this->DateFormat($date_range['from'], 'db_date_format_from');
            $userPurchasedProductArr['to_date'] = $this->DateFormat($date_range['to'], 'db_date_format_to');
            $getTotalUserPurchaseProductAmountArr = $transaction->getTotalUserPurchaseProductAmountArr($userPurchasedProductArr);
            $totalAmountPurchased = $this->getServiceLocator()->get('Kiosk\Model\KioskreservationTable')->getTotalUserPurchased($getTotalUserPurchaseProductAmountArr);
            $userMembershipArr = array();
            $userMembershipArr['transaction_amount'] = $totalAmountPurchased;
            $userMembershipArr['donation_amount'] = $donAmt;
            $userMembershipArr['user_id'] = $userId;
            $userMembershipArr['transaction_id'] = $transactionId;
            $userMembershipArr['transaction_date'] = $dateTime;

            $this->updateUserMembership($userMembershipArr);
// }
         //   $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter')->getDriver()->getConnection()->commit();
         //   $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->onAutoCommit();   
            if (isset($pledgeDonationAmount) && !empty($pledgeDonationAmount)) {
                $userPledgeArr = array();
                $userPledgeArr['userId'] = $userId;
                $userPledgeArr['transaction_id'] = $transactionId;
                $userPledgeArr['donation_amount'] = $pledgeDonationAmount;
                $this->updateUserPledgeTransaction($userPledgeArr);
            }
            $creditAmountArr = array();



            //$paymentSuccessStatus =  $response->setContent(\Zend\Json\Json::encode($paymentSuccessStatus));
			
			/* this is used to delete the pending transaction */
			$delTransParam = array();
			$delTransParam['pending_transaction_id'] = $postArr['pending_transaction_id'];
			$this->_transactionTable->deletePendingTransaction($delTransParam);			
			/* End used to delete the pending transaction */

            $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter')->getDriver()->getConnection()->commit();
            $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->onAutoCommit();

            /* End send stock notification */
            $responseArr['status'] = 'success';

            /* end add cart product to transaction table */
            if ($cartProductArr['source_type'] == 'backend') {
                $postArr['billing_address_1'] = $postArr['billing_address'];
                $postArr['billing_address_2'] = '';
                $postArr['shipping_address_1'] = (!empty($postArr['shipping_address'])) ? $postArr['shipping_address'] : 'N/A';
                $postArr['shipping_address_2'] = '';
            }
            $isEmailConfirmation = !empty($postArr['is_email_confirmation']) ? $postArr['is_email_confirmation'] : '';
            if (($cartProductArr['source_type'] == 'backend' && $isEmailConfirmation != 0 && !empty($isEmailConfirmation)) || ($cartProductArr['source_type'] == 'frontend')) {
                /*  send email  */
                $email_id_template_int = 10;
                $viewLink = '/profile';
                $param['user_id'] = $userId;
                $userDataArr = $this->getServiceLocator()->get('User\Model\UserTable')->getUser(array('user_id' => $userId));

                $postArr['shipping_first_name'] = !empty($postArr['shipping_first_name']) ? $postArr['shipping_first_name'] : '';
                $postArr['shipping_last_name'] = !empty($postArr['shipping_last_name']) ? $postArr['shipping_last_name'] : '';
                $postArr['shipping_address_1'] = !empty($postArr['shipping_address_1']) ? $postArr['shipping_address_1'] : '';
                $postArr['shipping_address_2'] = !empty($postArr['shipping_address_2']) ? $postArr['shipping_address_2'] : '';
                $postArr['shipping_city'] = !empty($postArr['shipping_city']) ? $postArr['shipping_city'] : '';
                $postArr['shipping_state'] = !empty($postArr['shipping_state']) ? $postArr['shipping_state'] : '';
                $postArr['shipping_zip_code'] = !empty($postArr['shipping_zip_code']) ? $postArr['shipping_zip_code'] : '';
                $postArr['shipping_country_text'] = !empty($postArr['shipping_country_text']) ? $postArr['shipping_country_text'] : '';


                $shippingAddressDetail = '';

//if (!((empty($postArr['shipping_first_name']) || $postArr['shipping_first_name'] != 'N/A') && (empty($postArr['shipping_last_name']) || $postArr['shipping_last_name'] != 'N/A') && (empty($postArr['shipping_address_1']) || $postArr['shipping_address_1'] != 'N/A') && (empty($postArr['shipping_address_2']) || $postArr['shipping_address_2'] != 'N/A') && (empty($postArr['shipping_city']) || $postArr['shipping_city'] != 'N/A') && (empty($postArr['shipping_state']) || $postArr['shipping_state'] != 'N/A') && (empty($postArr['shipping_zip_code']) || $postArr['shipping_zip_code'] != 'N/A'))) {
                if ($isShipping == 1) {
                    $address = $postArr['shipping_address_1'];
                    $address.= (!empty($postArr['shipping_address_2'])) ? '<br>' . $postArr['shipping_address_2'] : "";
                    $shippingStreetAddress = ((!empty($postArr['shipping_title'])) ? $postArr['shipping_title'] . ' ' : '') . $postArr['shipping_first_name'] . ' ' . $postArr['shipping_last_name'] . '<br>' . $address;
                    $shippingAddressDetail = '<td style="50%">  <p style="margin: 0px; padding-left: 0pt; padding-right: 0pt; font: 20px/36px arial; color: #000;  padding-top: 20px; ">Shipping Address :<br />
        <strong style="color: #7E8183; font-weight:normal; font: 16px/30px arial;">' . $shippingStreetAddress . '</strong><br />
        <strong style="color: #7E8183; font-weight:normal; font: 16px/30px arial;">' . $postArr['shipping_city'] . ',&nbsp;' . $postArr['shipping_state'] . '</strong><br />
        <strong style="color: #7E8183; font-weight:normal; font: 16px/30px arial;">' . $postArr['shipping_country_text'] . '&nbsp;-&nbsp;' . $postArr['shipping_zip_code'] . '</strong><br />
        <strong style="color: #7E8183; font-weight:normal; font: 16px/30px arial;"></strong></p></td>';
                }

                $userDataArr = array_merge($userDataArr, $postArr);
                $userDataArr['transactionId'] = $transactionId;
                $userDataArr['transaction_date'] = $this->OutputDateFormat($dateTime, 'dateformatampm');
                $userDataArr['transaction_amount_total'] = $amount_total;

                if ($userDataArr['user_type'] != 1) {
                    $userDataArr['first_name'] = $userDataArr['company_name'];
                    $userDataArr['last_name'] = '';
                } else {
                    $userDataArr['billing_first_name'] = ((!empty($postArr['billing_title'])) ? $postArr['billing_title'] . ' ' : '') . $userDataArr['billing_first_name'];
                }
                $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                $userDataArr['shippingAddressDetail'] = $shippingAddressDetail;
                /** Order Details Template */
                $searchParam['transactionId'] = $transactionId;
                $searchResult = $this->getServiceLocator()->get('Kiosk\Model\KioskreservationTable')->searchTransactionDetails($searchParam);
                $searchProduct = $this->getServiceLocator()->get('Kiosk\Model\KioskreservationTable')->searchTransactionProducts($searchParam);
                foreach ($searchProduct as $key => $val) {
                    switch ($val['tra_pro']) {
                        case 'fof':
                            $fofResult = $this->getServiceLocator()->get('Kiosk\Model\KioskreservationTable')->searchFofDetails($val);
                            array_push($searchProduct[$key], $fofResult[0]);
                            break;
                        case 'pro':
                            $passengerResult = $this->getServiceLocator()->get('Kiosk\Model\KioskreservationTable')->searchDocPass($val);
                            array_push($searchProduct[$key], $passengerResult[0]);
                            break;
                    }
                    $proParam['transactionId'] = $searchParam['transactionId'];
                    $proParam['productId'] = $val['id'];
                    $proResult = $this->getServiceLocator()->get('Kiosk\Model\KioskreservationTable')->searchProductsShipped($proParam);
                    if (isset($proResult[0]) && !empty($proResult[0])) {
                        $searchProduct[$key]['shippedQty'] = $proResult[0]['shippped_qty'];
                    } else {
                        $searchProduct[$key]['shippedQty'] = 0;
                    }
                }

//n-woh- start 
                $arrFinalWallOfHonorId = array();
                $arrFinalWallOfHonorIdContacts = array();
//n-woh- end

                $orderDetailsArr = '<div><table width="100%" border="0" style="border-bottom:1px dotted #ccc;" cellspacing="5" cellpadding="10" class="table-bh tra-view">
                    <tr>
                    <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:left; padding-left:0;">Sr. No</th>
                    <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:left">Product</th>
                    <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:left">SKU</th>
                    <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:left">Qty.</th>
                    <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:center">Price</th>
                    <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:center">SubTotal</th></tr>';
                /* '<th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:center">Tax</th>
                  <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:center">Shipping & Handling</th>
                  <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:center">Discount</th>
                  <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:right">Total</th>
                  </tr>'; */
                $i = 1;

                foreach ($searchProduct as $key => $val) {
                    $orderDetailsArr.= '<tr><td style="width: 48px; font: 16px/20px arial; color: #7e8183; text-align:left; vertical-align:top;">' . $i++ . '</td>
                        <td style="font: 16px/20px arial; color: #7e8183; text-align:left">';
                    switch ($val['tra_pro']) {
                        case 'don' :
                            $orderDetailsArr.=$val['product_name'] . ' <label>' . $val['option_name'] . '</label>';
                            break;
                        case 'bio' :
                            $orderDetailsArr.=$val['product_name'] . '<br> <label>' . $val['item_name'] . '</label>';
                            break;
                        case 'fof' :
                            $orderDetailsArr.=$val['product_name'] . '<br> <label>' . $val[0]['donated_by'] . '</label>';
                            break;
                        case 'pro' :
                            switch ($val['product_type']) {

                                case '5' :
                                    $orderDetailsArr.=$val['product_name'] . '<label> :</label> ' . $val['option_name'] . '<br><label>Passenger Name : ' . $val[0]['NAME'] . '</label><br/><label>Passenger Id : </label> ' . $val[0]['item_id'];
                                    break;
                                case '6' :
                                    $orderDetailsArr.=$val['product_name'] . '<br><label> ' . $val[0]['final_name'] . '</label>';
                                    break;
                                case '7' :
                                    $itemName = '';
                                    if (strlen($val[0]['item_id']) < 12)
                                        $itemName = "Ship Name : " . $val['item_name'];
                                    else
                                        $itemName = "Passenger Name : " . $val['item_name'];

                                    if (!empty($val['item_info']) && $val['item_info'] != '0000-00-00')
                                        $arrivalDate = $this->OutputDateFormat($val['item_info'], 'calender');
                                    else
                                        $arrivalDate = '';

                                    $productName = $val['product_name'];
                                    if (isset($val['manifest_info']) && !empty($val['manifest_info']))
                                        $productName.= ' : ' . $val['manifest_info'];

                                    $orderDetailsArr.=$productName . '<br>  ' . $itemName . '<br/><label>Arrival Date : </label>' . $arrivalDate;
                                    break;
                                case '8' :
                                    if (!empty($val['item_info']) && $val['item_info'] != '0000-00-00')
                                        $arrivalDate = $this->OutputDateFormat($val['item_info'], 'calender');
                                    else
                                        $arrivalDate = '';
                                    $orderDetailsArr.=$val['product_name'] . '<label> : </label>' . $val['option_name'] . '<br/><label>Ship Name : </label>' . $val['item_name'] . '<br/><label>Arrival Date : </label>' . $arrivalDate;
                                    break;
                                default :
                                    $orderDetailsArr.=$val['product_name'] . ' <label>' . $val['option_name'] . '</label>';
                                    break;
                            }
                            break;
                        case 'woh' :

//n-woh- start 
                            if (isset($val['id']) and trim($val['id']) != "" and !in_array(trim($val['id']), $arrFinalWallOfHonorId)) {
                                array_push($arrFinalWallOfHonorId, trim($val['id']));
                            }
//n-woh- end
                            $orderDetailsArr.=$val['product_name'] . '<br> <label>' . utf8_decode($val['option_name']) . '</label>';
                            break;
                    }
                    $orderDetailsArr .='</td><td style="font: 16px/20px arial; color: #7e8183; text-align:left">';
                    if (isset($val['product_sku']) && !empty($val['product_sku'])) {
                        $orderDetailsArr .=$val['product_sku'];
                    } else {
                        $orderDetailsArr .='N/A';
                    }
                    $orderDetailsArr.='</td><td style="font: 16px/20px arial; color: #7e8183; text-align:left">' . $val['product_quantity'] . '</td>
                        <td style="font: 16px/20px arial; color: #7e8183; text-align:center">$' . $this->Currencyformat($val['product_price']) . '</td>
                        <td style="font: 16px/20px arial; color: #7e8183; text-align:right">$' . $this->Currencyformat($val['product_sub_total']) . '</td></tr>';
                    /* '<td style="font: 16px/20px arial; color: #7e8183; text-align:right">$' . $this->Currencyformat($val['product_tax']) . '</td>
                      <td style="font: 16px/20px arial; color: #7e8183; text-align:right">$' . $this->Currencyformat($val['shipping'] + $val['handling']) . '</td>
                      <td style="font: 16px/20px arial; color: #7e8183; text-align:right">($' . $this->Currencyformat($val['product_discount']) . ')</td>
                      <td style="font: 16px/20px arial; color: #7e8183; text-align:right">$' . $this->Currencyformat($val['product_total']) . '</td></tr>'; */
                }
                /* $orderDetailsArr .='</table></div><table width="100%" cellpadding="0" cellspacing="0" align="right">
                  <tr><td width="92%" align="right"><span style="text-align:right; font: 16px/28px arial; color: #7e8183;">Sub Total :</span></td>
                  <td width="8%" align="right"><strong style="text-align:right; font: 16px/28px arial; color: #7e8183; font-weight:bold;">$ ' . $this->Currencyformat($searchResult[0]['sub_total_amount']) . '</strong></td></tr>';
                  $orderDetailsArr .='<tr>
                  <td align="right"><span style="text-align:right; font: 16px/28px arial; color: #7e8183;">Discount : </span></td>
                  <td align="right"><strong style="text-align:right; font: 16px/28px arial; color: #7e8183; font-weight:bold;">$ ' . $this->Currencyformat($searchResult[0]['total_discount']) . '</strong></td></tr>';
                  $orderDetailsArr .='<tr>
                  <td align="right"><span style="text-align:right; font: 16px/28px arial; color: #7e8183;">Shipping & Handling Charge : </span></td>
                  <td align="right"><strong style="text-align:right; font: 16px/28px arial; color: #7e8183; font-weight:bold;">$ ' . $this->Currencyformat($searchResult[0]['shipping_amount'] + $searchResult[0]['handling_amount']) . '</strong></td></tr>
                  <tr><td align="right"> <span style="text-align:right; font: 16px/28px arial; color: #7e8183;">Global Tax :</span></td>
                  <td align="right"><strong style="text-align:right; font: 16px/28px arial; color: #7e8183; font-weight:bold;">$ ' . $this->Currencyformat($searchResult[0]['total_tax']) . '</strong></td></tr>'; */
                $orderDetailsArr .= '<tr>
                    <td align="right" colspan="5"><span style="text-align:right; font: 16px/28px arial; color: #7e8183;">Sub Total :</span></td>
                    <td align="right">
                    <strong style="text-align:right; font: 16px/28px arial; color: #7e8183; font-weight:bold;">$' . $this->Currencyformat($searchResult[0]['sub_total_amount']) . '
                    </strong></td>
                    </tr>
                    <tr>
                    <td align="right" colspan="5"><span style="text-align:right; font: 16px/28px arial; color: #7e8183;">Shipping & Handling :</span></td>
                    <td align="right">
                    <strong style="text-align:right; font: 16px/28px arial; color: #7e8183; font-weight:bold;">$' . $this->Currencyformat($searchResult[0]['shipping_amount'] + $searchResult[0]['handling_amount']) . '
                    </strong></td>
                    </tr>
                    <tr>
                    <td align="right" colspan="5"><span style="text-align:right; font: 16px/28px arial; color: #7e8183;">Tax :</span></td>
                    <td align="right">
                    <strong style="text-align:right; font: 16px/28px arial; color: #7e8183; font-weight:bold;">$' . $this->Currencyformat($searchResult[0]['total_tax']) . '
                    </strong></td>
                    </tr>
                    <tr>
                    <td align="right" colspan="5"><span style="text-align:right; font: 16px/28px arial; color: #7e8183;">Discount :</span></td>
                    <td align="right">
                    <strong style="text-align:right; font: 16px/28px arial; color: #7e8183; font-weight:bold;">($' . $this->Currencyformat($searchResult[0]['total_discount']) . ')
                    </strong></td>
                    </tr>
                    <tr>
                    <td align="right" colspan="5"><span style="text-align:right; font: 16px/28px arial; color: #7e8183;">Purchase Total :</span></td>
                    <td align="right">
                    <strong style="text-align:right; font: 16px/28px arial; color: #7e8183; font-weight:bold;">$' . $this->Currencyformat($searchResult[0]['transaction_amount'] - ($searchResult[0]['cancel_amount'] + $searchResult[0]['refund_amount'])) . '
                    </strong></td>
                    </tr></table>';
                $userDataArr['order_summary'] = $orderDetailsArr;
                $userDataArr['invoice_number'] = $searchResult[0]['invoice_number'];
                $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $email_id_template_int, $this->_config['email_options']);



                // Thank you mail dev 4
                if ($sendThankyouMail == 1) {

                    $userDataThankArr = array();
                    $userDataThankArr['email_id'] = $userDataArr['email_id'];
                    if ($userDataArr['user_type'] != 1) {
                        $userDataThankArr['first_name'] = $userDataArr['company_name'];
                    } else {
                        $userDataThankArr['first_name'] = $userDataArr['first_name'];
                    }

                    $userDataThankArr['amount'] = $this->Currencyformat($pledgeDonationAmount);
                    $userDataThankArr['admin_email'] = $this->_config['soleif_email']['email'];
                    $userDataThankArr['admin_name'] = $this->_config['soleif_email']['name'];
                    $email_id_template_int = '8'; // Thank you mail 
                    $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataThankArr, $email_id_template_int, $this->_config['email_options']);
                }
                // End Thank you mail                
//n-woh- start 

                if (is_array($arrFinalWallOfHonorId) and count($arrFinalWallOfHonorId) > 0) {
                    foreach ($arrFinalWallOfHonorId as $valWoh) {
                        $arrContacts = $this->getServiceLocator()->get('Kiosk\Model\KioskreservationTable')->getUserWohContact(array('woh_id' => $valWoh));
                        if (is_array($arrContacts) and count($arrContacts) > 0) {
                            foreach ($arrContacts as $valWoh2) {
                                if (!in_array(array("firstNameWoh" => $valWoh2['firstNameWoh'], "lastNameWoh" => $valWoh2['lastNameWoh'], "emailIdWoh" => $valWoh2['emailIdWoh']), $arrFinalWallOfHonorIdContacts)) {
                                    array_push($arrFinalWallOfHonorIdContacts, array("firstNameWoh" => $valWoh2['firstNameWoh'], "lastNameWoh" => $valWoh2['lastNameWoh'], "emailIdWoh" => $valWoh2['emailIdWoh']));
                                }
                            }
                        }
                    }
                }


                if (is_array($arrFinalWallOfHonorIdContacts) and count($arrFinalWallOfHonorIdContacts) > 0) {
                    foreach ($arrFinalWallOfHonorIdContacts as $valWoh3) {
                        if (isset($valWoh3['emailIdWoh']) and trim($valWoh3['emailIdWoh']) != "") {
                            $userDataArr['first_name'] = $valWoh3['firstNameWoh'];
                            $userDataArr['last_name'] = $valWoh3['lastNameWoh'];
                            $userDataArr['email_id'] = $valWoh3['emailIdWoh'];
                            $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $email_id_template_int, $this->_config['email_options']);
                        }
                    }
                }
//n-woh- end
            }
            /* send email end */


            /* Send stock notification */

            if (!empty($inventoryProductQtyDetail)) {
                $stockNotificationData = array();
                $stockNotificationData = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getStockNotification();
                if (!empty($stockNotificationData)) {
                    $notificationReceiptsArr = explode(',', $stockNotificationData[0]['notification_recipients']);
                    $isEmailSend = $stockNotificationData[0]['is_email_send'];
                    foreach ($inventoryProductQtyDetail as $inventoryPro) {
                        $remainingQty = $inventoryPro['product_qty_in_stock'] - $inventoryPro['product_qty_in_cart'];
                        if ($isEmailSend == 1 && !empty($notificationReceiptsArr) && $remainingQty <= $inventoryPro['product_threshold']) {
                            $productViewLink = SITE_URL . '/view-crm-product/' . $this->encrypt($inventoryPro['product_id']) . '/' . $this->encrypt('view');
                            $productViewLink = '<a href="' . $productViewLink . '">' . $productViewLink . '</a>';
                            $messageSubject = str_replace('[store: name]', $inventoryPro['product_name'], $stockNotificationData[0]['message_subject']);
                            $messageText = str_replace('[node:title]', $inventoryPro['product_name'], $stockNotificationData[0]['message_text']);
                            $messageText = str_replace('[uc_stock:model]', $inventoryPro['product_sku'], $messageText);
                            $messageText = str_replace('[uc_stock:level]', $remainingQty, $messageText);
                            $messageText = str_replace('#[uc_purchase:link]', $productViewLink, $messageText);
                            $emailDataArr = array();
                            $emailDataArr['subject'] = $messageSubject;
                            $emailDataArr['message'] = $messageText;
                            $emailDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                            $emailDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                            foreach ($notificationReceiptsArr as $receipt) {
                                $emailDataArr['emailId'] = $receipt;
                                $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmailTemplate($emailDataArr, $this->_config['email_options']);
                            }
                        }
                    }
                }
            }
        }
        return $responseArr;
    }
    	/**
     * This action is used to update user membership
     * @return json
     * @param void
     */
    public function updateUserMembership($userMembershipArr) {
        $transaction = new Kioskreservation($this->_adapter);
        $userMembershipUpdateArr = array();
        $userMembershipArr['transaction_amount'] = (isset($userMembershipArr['transaction_amount']) && $userMembershipArr['transaction_amount'] == 0 &&
                !empty($userMembershipArr['donation_amount'])) ? $userMembershipArr['donation_amount'] : $userMembershipArr['transaction_amount'];
        $userMembershipUpdateArr['minimun_donation_amount'] = isset($userMembershipArr['transaction_amount']) ? $userMembershipArr['transaction_amount'] : '';
        $getMatchingMembershipArr = $transaction->getMatchingMembershipArr($userMembershipUpdateArr);
        $matchingMembership = $this->getServiceLocator()->get('Kiosk\Model\KioskreservationTable')->getMatchingMembership($getMatchingMembershipArr);
        $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $userMembershipArr['user_id']));
        //if (!empty($matchingMembership) && $matchingMembership['membership_id'] != $userDetail['membership_id']) {
        $gracePeriodStatus = false;
        $membershipExpiredStatus = false;
        $todayTime = time();
        $memberExpired = date("Y-m-d", strtotime($userDetail['membership_expire']));
        $leftTime = strtotime($memberExpired) - $todayTime;
        if ($leftTime > 0) {
            if (floor($leftTime / (60 * 60 * 24)) < $this->_config['membership_plus'])
                $gracePeriodStatus = true; //allow update 
        }

        if ($gracePeriodStatus) {
            $userMembershipUpdateArr = array();
            $userMembershipUpdateArr['minimun_donation_amount'] = isset($userMembershipArr['donation_amount']) ? $userMembershipArr['donation_amount'] : '';
            $getMatchingMembershipArr = $transaction->getMatchingMembershipArr($userMembershipUpdateArr);
            $matchingMembership = $this->getServiceLocator()->get('Kiosk\Model\KioskreservationTable')->getMatchingMembership($getMatchingMembershipArr);
        }
        if ($leftTime < 1 || ( $gracePeriodStatus == true || (!empty($matchingMembership) && $matchingMembership['membership_id'] != $userDetail['membership_id'] && ($userMembershipUpdateArr['minimun_donation_amount'] >= $userDetail['donation_amount'])) )) {
            $startDate = date("Y-m-d", strtotime(DATE_TIME_FORMAT));
            $membershipDateFrom = '';
            $membershipDateTo = '';

            if ($matchingMembership['validity_type'] == 1) {
                $membershipDateFrom = $startDate;
                $startDay = $this->_config['financial_year']['srart_day'];
                $startMonth = $this->_config['financial_year']['srart_month'];
                $startYear = date("Y", strtotime(DATE_TIME_FORMAT));
                $startDate = $startYear . "-" . $startMonth . "-" . $startDay;
//$year = 1;

                $addYear = ($matchingMembership['validity_time'] - $startYear) + 1;
                $futureDate = date('Y-m-d', strtotime('+' . $addYear . ' year -1 day', strtotime($startDate)));
                $your_date = $futureDate;
                $now = time();
                $datediff = strtotime($your_date) - $now;
                if ((isset($this->_config['membership_plus']) && !empty($this->_config['membership_plus'])) && (floor($datediff / (60 * 60 * 24)) < $this->_config['membership_plus'])) {
                    $addYear = ($matchingMembership['validity_time'] - $startYear) + 2;
                    $futureDate = date('Y-m-d', strtotime('+' . $addYear . ' year', strtotime($startDate)));
                    $futureDate = date('Y-m-d', strtotime('-1 day', strtotime($futureDate)));
                }
                $membershipDateTo = $futureDate;
            } else if ($matchingMembership['validity_type'] == 2) {
                $membershipDateFrom = $startDate;
                $futureDate = date('Y-m-d', strtotime('+' . $matchingMembership['validity_time'] . ' year -1 day', strtotime($startDate)));
                $membershipDateTo = $futureDate;
            }
            $userMembershipUpdateArr = array();
            $userMembershipUpdateArr['user_id'] = $userMembershipArr['user_id'];
            $userMembershipUpdateArr['membership_id'] = $matchingMembership['membership_id'];
            $userMembershipUpdateArr['transaction_date'] = isset($userMembershipArr['transaction_date']) ? $userMembershipArr['transaction_date'] : '';
            $userMembershipUpdateArr['transaction_amount'] = isset($userMembershipArr['transaction_amount']) ? $userMembershipArr['transaction_amount'] : '';
            $userMembershipUpdateArr['membership_date_from'] = $membershipDateFrom;
            $userMembershipUpdateArr['membership_date_to'] = $membershipDateTo;
            if (empty($membershipDateFrom) || empty($membershipDateTo)) {
                return false;
            }
            $getUpdateUserMembershipArr = $transaction->getUpdateUserMembershipArr($userMembershipUpdateArr);
            $userMembershipId =$this->getServiceLocator()->get('Kiosk\Model\KioskreservationTable')->updateUserMembership($getUpdateUserMembershipArr);
        }
    }
    
	function cartTotalWithTax($paramArray) {
	
        $user_id = $paramArray['userid'];
        $shippingcode = !empty($paramArray['shipcode']) ? $paramArray['shipcode'] : '';
        $couponcode = !empty($paramArray['couponcode']) ? $paramArray['couponcode'] : '';
        $source_all = !empty($paramArray['source_all']) ? $paramArray['source_all'] : '';
        $cartParam['user_id'] = $user_id;
        $cartParam['source_type'] = !empty($paramArray['source_type']) ? $paramArray['source_type'] : 'frontend';
        if (defined('MEMBERSHIP_DISCOUNT'))
            $cartParam['membership_percent_discount'] = MEMBERSHIP_DISCOUNT;
        else
            $cartParam['membership_percent_discount'] = '';
            
        $cartParam['pending_transaction_id'] = $paramArray['postData']['pending_transaction_id'];
        $paramArray['postData']['shipping_zip_code'] = $paramArray['postData']['shipping_postal_code'];
        $paramArray['postData']['shipping_country'] = $paramArray['postData']['shipping_country_id'];
            
        $cartData = $this->_transactionTable->getCartProducts($cartParam);

        $totalCartAmount = 0;
        $totalShippableProductWeight = 0;
        $totalPromotionalCartAmount = 0;
        $totalPromotionalShippableProductWeight = 0;
        foreach ($cartData as $key => $val) {
            $totalCartAmount = $totalCartAmount + $val['product_subtotal'] - $val['membership_discount'];
            if ($val['is_shippable'] == 1 && $val['shipping_cost'] == 1 && $val['is_free_shipping'] == 0 && $val['product_weight'] > 0) {
                $totalShippableProductWeight+= ($val['product_weight'] * $val['product_qty']);
                if ($val['is_promotional'] == 1) {
                    $totalPromotionalShippableProductWeight+= ($val['product_weight'] * $val['product_qty']);
                    $totalPromotionalCartAmount+= $val['product_subtotal'] - $val['membership_discount'];
                }
            }
        }

        $subTotal = 0;
        $authorizeAmountTotal = 0;

        /** TAX * */
        $taxDetail = array();

        if (($paramArray['shipping_country'] == $paramArray['apo_po_shipping_country'] || $paramArray['shipping_country'] == 228) || ($paramArray['postData']['shipping_country'] == $paramArray['postData']['apo_po_shipping_country'] || $paramArray['postData']['shipping_country'] == 228)) {

            if (!empty($shippingcode)) {
                $postal_code = substr($shippingcode, 0, 5);
                $postal_code = explode('-', $postal_code);
                $postArray['postal_code'] = $postal_code[0];

                if (isset($paramArray['shipping_state']))
                    $postArray['shipping_state'] = $paramArray['shipping_state'];
                else if (isset($paramArray['postData']['shipping_state']))
                    $postArray['shipping_state'] = $paramArray['postData']['shipping_state'];

                $taxDetail = $this->getServiceLocator()->get('Kiosk\Model\KioskreservationTable')->getTaxDetail($postArray);//done 
            }
        }

        if (isset($taxDetail[0]['CombinedRate']) && !empty($taxDetail[0]['CombinedRate'])) {
            $combined_sales_tax = $taxDetail[0]['CombinedRate'];
        }
        $paramArray['combined_sales_tax'] = $combined_sales_tax;
        /* end of tax* */

        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $application_source_id = $this->_config['application_source_id'];
        /** code for promotion * */
        $promotionTable = $this->getServiceLocator()->get('Promotions\Model\PromotionTable');
        $couponCode = isset($paramArray['postData']['coupon_code']) ? $paramArray['postData']['coupon_code'] : $paramArray['couponcode'];
        if (!empty($couponCode)) {
            $getPromotionDetail = $promotionTable->getPromotionDetailByCode($couponCode, 'code');

            $getPromotionDetail['promotionbundledbroducts'] = array();
            if ((isset($getPromotionDetail['bundle_product_type']) && $getPromotionDetail['bundle_product_type'] != '') || (isset($getPromotionDetail['bundle_products']) && $getPromotionDetail['bundle_products'] != '')) {
                $arrBundleProduct = $promotionTable->getPromotionBundledProducts($getPromotionDetail['bundle_product_type'], $getPromotionDetail['bundle_products']);
                foreach ($arrBundleProduct as $bundleProduct) {
                    $getPromotionDetail['promotionbundledbroducts'][] = $bundleProduct['product_id'];
                }
            }

            $getPromotionDetail['promotionMinimumAmountProducts'] = array();
            if ((isset($getPromotionDetail['minimum_amount_product_type']) && $getPromotionDetail['minimum_amount_product_type'] != '') || (isset($getPromotionDetail['minimum_amount_products']) && $getPromotionDetail['minimum_amount_products'] != '')) {
                $arrMinimumAmountProduct = $promotionTable->getPromotionMinimumAmountProducts($getPromotionDetail['minimum_amount_product_type'], $getPromotionDetail['minimum_amount_products']);
                foreach ($arrMinimumAmountProduct as $minimumAmountProduct) {
                    $getPromotionDetail['promotionMinimumAmountProducts'][] = $minimumAmountProduct['product_id'];
                }
            }

            $getPromotionDetail['promotionAnyProducts'] = array();
            if ((isset($getPromotionDetail['promotion_any_product_type']) && $getPromotionDetail['promotion_any_product_type'] != '') || (isset($getPromotionDetail['promotion_any_products']) && $getPromotionDetail['promotion_any_products'] != '')) {
                $arrAnyProduct = $promotionTable->getPromotionMinimumAmountProducts($getPromotionDetail['promotion_any_product_type'], $getPromotionDetail['promotion_any_products']);
                foreach ($arrAnyProduct as $anyProduct) {
                    $getPromotionDetail['promotionAnyProducts'][] = $anyProduct['product_id'];
                }
            }

            if (!empty($getPromotionDetail)) {
                $promotionId = $getPromotionDetail['promotion_id'];
                // By Dev 2 NEWPROMOCODE.

                $returnArray = $this->isValidCouponCheck($selectedShipping_method, $user_id, $paramArray['postData']['shipping_country'], $getPromotionDetail, $cartData);

                $totalProductCartAmount = isset($returnArray['totalProductCartAmount']) ? $returnArray['totalProductCartAmount'] : 0;
                if ($returnArray['status'] == 'error') {
                    $getPromotionDetail['is_valid_coupon'] = 0;
                } else {
                    $getPromotionDetail['is_valid_coupon'] = 1;
                    // By Dev 2 NEWPROMOCODE

                    $inValidShippingMethod = 0;

                    if (($getPromotionDetail['applicable_to'] == 1 || $getPromotionDetail['applicable_to'] == 3) && (isset($paramArray['postData']['shipping_method']) && $paramArray['postData']['shipping_method'] != '') && $paramArray['postData']['shipping_country'] == '228') {

                        $shipping_method_array = explode(",", $paramArray['postData']['shipping_method']);
                        $selectedShipping_method = trim($shipping_method_array[2]);

                        $selectedPromoShippingMethod = $getPromotionDetail['pb_shipping_type_id'];
                        if ($selectedPromoShippingMethod != '' && !is_null($selectedPromoShippingMethod)) {
                            $shipMethodPromoArray = explode(",", $selectedPromoShippingMethod);
                        }
                        $shipingM = array();
                        if ($selectedShipping_method != '' && !in_array($selectedShipping_method, $shipMethodPromoArray)) {
                            $returnArray['status'] = "error";
                            $transaction = new Kioskreservation($this->_adapter);
                            foreach ($shipMethodPromoArray as $shippingPromoId) {
                                $shippingMetArr = array();
                                $shippingMetArr['pb_shipping_type_id'] = $shippingPromoId;
                                $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                                $getShippingMethods = $this->_transactionTable->getShippingMethod($shippingMethodArr);//doneaa

                                $shipingM[] = $getShippingMethods[0]['web_selection'];
                            }

                            $ship_method_name = '"' . implode('" OR "', $shipingM) . '"';

                            $returnArray['message'] = "Please select " . $ship_method_name . " shipping method for free shipping";
                            $inValidShippingMethod = 1;
                            //$getPromotionDetail['is_valid_coupon'] = 0;
                        }
                    }
                    if ($inValidShippingMethod == 0) {

                        $cartData = $this->applyCouponDiscountInCart($getPromotionDetail, $cartData, $paramArray['postData']['shipping_country'], $totalPromotionalCartAmount, $totalProductCartAmount, $totalPromotionalShippableProductWeight);
                    }
                }
            } else {
                $returnArray['status'] = "error";
                $returnArray['message'] = $msgArr['COUPON_CODE_NOT_EXIST'];
                $getPromotionDetail['is_valid_coupon'] = 0;
            }
        }
        /** end of promotion code * */
        $totalProductWeightWeight = 0;
        $isInventory = 0;

        foreach ($cartData as $key => $value) {
            if (($this->_config['application_source_id'] == 1) && ($value['is_shippable'] == '0' || empty($value['is_shippable']) || $value['shipping_cost'] == '0')) {
                $isInventory++;
            }

            if ($value['is_free_shipping'] != 1 && $value['product_weight'] > 0 && $value['is_shippable'] == 1 && $value['shipping_cost'] == 1) {
                $totalProductWeightWeight+= ($value['product_weight'] * $value['product_qty']);
            }
        }
        if (empty($cartData)) {
            $totalproduct = 0;
        } else {
            $totalproduct = count($cartData);
        }
        //asd($cartData);
        //asd($paramArray);
        
        $cartDataArray = $this->addShippingCostToProducts($cartData, $paramArray, $totalProductWeightWeight);
        $cartData = $cartDataArray['cartdata'];
        $cartDataTotal = $cartDataArray['cartdataTotal'];
		
        //asd($cartDataArray);
        $totalWeight = $totalProductWeightforPB;
        $returnArray['isAnyNotPickupField'] = 0;
        $returnArray['isInventory'] = $isInventory;
        // $returnArray['isInventory'] = 0;

        $returnArray['totalproduct'] = $totalproduct;
        $returnArray['totalweight'] = $totalProductWeightWeight;
        $returnArray['itemtotal'] = $this->Currencyformat($cartDataTotal['subTotalAmount']);
        $returnArray['itemtotalwoformat'] = round($cartDataTotal['subTotalAmount'] + $cartDataTotal['shipping_charge'], 2);
        //$returnArray['coupondiscount'] = $this->Roundamount($coupondis);
        $returnArray['coupondiscount'] = $this->Currencyformat($cartDataTotal['total_discount']);
        $returnArray['shipping'] = $this->Currencyformat($cartDataTotal['shipping_charge']);
        $returnArray['totalaftershipping'] = $this->Currencyformat($cartDataTotal['totalaftershipping']);
        $returnArray['estimatedtax'] = $this->Currencyformat($cartDataTotal['totalTaxAmount']);
        $returnArray['finaltotal'] = $this->Currencyformat($cartDataTotal['order_total']);
        $returnArray['finaltotaldiscounted'] = $this->Currencyformat($cartDataTotal['total_discount']);
        //$returnArray['finaltotaldonationamount'] = ($donationTotalAmount != 0) ? $this->Roundamount($donationTotalAmount + $shipping + $estimatedtax) : 0;
        $returnArray['finaltotaldonationamount'] = 0;
        //$returnArray['isOnlyDonation'] = $isOnlyDonation;
        $returnArray['isOnlyDonation'] = 0;
        $returnArray['membershipDiscountAmount'] = $cartDataTotal['total_membership_discount'];
        $returnArray['validPromotionsForUser'] = $validPromotionsForUser;
        $returnArray['application_source_id'] = $application_source_id;
        $returnArray['cartData'] = $cartData;

        return $returnArray;
    }    
    public function applyCouponDiscountInCart($getPromotionDetail, $cartData, $shipping_country, $totalCartAmount, $totalProductCartAmount, $totalShippableProductWeight) {
        $returnData = array();
        $applicationConfigIds = $this->_config['application_config_ids'];
        $uSId = $applicationConfigIds['united_state_country_id'];
        $serviceManager = $this->getServiceLocator();
        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $bundleProductTypeArray = array();
        $bundleProductArray = array();
        $anyProductTypeArray = array();
        $anyProductArray = array();
        $excludeProductTypeArray = array();
        $excludeProductArray = array();

        if (!empty($getPromotionDetail['bundle_product_type'])) { // Buldle products
            $bundleProductTypeArray = explode(',', $getPromotionDetail['bundle_product_type']);
        }
        if (!empty($getPromotionDetail['bundle_products'])) {
            $bundleProductArray = explode(',', $getPromotionDetail['bundle_products']);
        }

        if (!empty($getPromotionDetail['promotion_any_product_type'])) { // Any product
            $anyProductTypeArray = explode(',', $getPromotionDetail['promotion_any_product_type']);
        }
        if (!empty($getPromotionDetail['promotion_any_products'])) {
            $anyProductArray = explode(',', $getPromotionDetail['promotion_any_products']);
        }

        if (!empty($getPromotionDetail['excluded_product_type'])) { // Exclude product
            $excludeProductTypeArray = explode(',', $getPromotionDetail['excluded_product_type']);
        }
        if (!empty($getPromotionDetail['excluded_products'])) {
            $excludeProductArray = explode(',', $getPromotionDetail['excluded_products']);
        }

        if (!empty($getPromotionDetail['minimum_amount_product_type'])) { // Minimum amount products
            $minimumAmountProductTypeArray = explode(',', $getPromotionDetail['minimum_amount_product_type']);
        }
        if (!empty($getPromotionDetail['minimum_amount_products'])) {
            $minimumAmountProductArray = explode(',', $getPromotionDetail['minimum_amount_products']);
        }

        $arrCartProductId = array();
        $totalBundleProductAmount = 0;
        $totalMinimumAmountProductsAmount = 0;
        foreach ($cartData as $cData) {
            $arrCartProductId[] = $cData['product_id'];
            if (in_array($cData['product_id'], $getPromotionDetail['promotionbundledbroducts']))
                $totalBundleProductAmount+=$cData['product_subtotal'] - $cData['membership_discount'];

            if ($getPromotionDetail['promotion_qualified_by'] == 1) {
                if (!empty($getPromotionDetail['promotionMinimumAmountProducts']) && in_array($cData['product_id'], $getPromotionDetail['promotionMinimumAmountProducts'])) {
                    if ((in_array($cData['product_type_id'], $minimumAmountProductTypeArray)) || (in_array($cData['product_id'], $minimumAmountProductArray)))
                        $totalMinimumAmountProductsAmount+=$cData['product_subtotal'] - $cData['membership_discount'];
                }
            }elseif ($getPromotionDetail['promotion_qualified_by'] == 3) {
                if (!empty($getPromotionDetail['promotionAnyProducts'])) {
                    if ((in_array($cData['product_type_id'], $anyProductTypeArray)) || (in_array($cData['product_id'], $anyProductArray)))
                        $totalAnyProductsAmount+=$cData['product_subtotal'] - $cData['membership_discount'];
                }
            }
        }
        $arrCartProductId = array_unique($arrCartProductId);

        $z = 0;
        $totalProductTax = 0;
        $totalMembershipDiscount = 0;

        foreach ($cartData as $index => $item) {
            if ($item['is_promotional'] == 1 && (!in_array($item['product_type_id'], $excludeProductTypeArray)) && (!in_array($item['product_id'], $excludeProductArray))) {
                if ($getPromotionDetail['promotion_qualified_by'] == 4) {
                    // global
                    // Shipping Discount
                    if ($getPromotionDetail['is_shipping_discount'] == 1) {

                        if ($getPromotionDetail['shipping_discount_type'] == 1) {
                            $totalShippableProductWeight-= ($item['product_weight'] * $item['product_qty']);
                        }
                    }
                } elseif ($getPromotionDetail['promotion_qualified_by'] == 1) {
                    // min product cart amount

                    if (!empty($getPromotionDetail['promotionMinimumAmountProducts'])) {

                        if (in_array($item['product_type_id'], $minimumAmountProductTypeArray) || in_array($item['product_id'], $minimumAmountProductArray)) {

                            // Shipping Discount
                            if ($getPromotionDetail['is_shipping_discount'] == 1) {

                                if ($getPromotionDetail['shipping_discount_type'] == 1) {
                                    $totalShippableProductWeight-= ($item['product_weight'] * $item['product_qty']);
                                }
                            }
                            // end of shippin discount                        
                        }
                    } else {
                        if ($totalProductCartAmount >= $getPromotionDetail['promotion_qualified_min_amount']) {
                            // Shipping Discount
                            if ($getPromotionDetail['is_shipping_discount'] == 1) {

                                if ($getPromotionDetail['shipping_discount_type'] == 1) {
                                    $totalShippableProductWeight-= ($item['product_weight'] * $item['product_qty']);
                                }
                            }
                            // end of shippin discount							
                        }
                    }
                } elseif ($getPromotionDetail['promotion_qualified_by'] == 2) {
                    // all bundled products
                    sort($arrCartProductId);
                    sort($getPromotionDetail['promotionbundledbroducts']);
                    $arrComanProduct = array_intersect($arrCartProductId, $getPromotionDetail['promotionbundledbroducts']);
                    sort($arrComanProduct);
                    if ($arrComanProduct == $getPromotionDetail['promotionbundledbroducts']) {
                        // Shipping Discount
                        if ($getPromotionDetail['is_shipping_discount'] == 1 && in_array($item['product_id'], $getPromotionDetail['promotionbundledbroducts'])) {

                            if ($getPromotionDetail['shipping_discount_type'] == 1) {
                                $totalShippableProductWeight-= ($item['product_weight'] * $item['product_qty']);
                            }
                        }
                        // end of shippin discount                        
                    }
                } elseif ($getPromotionDetail['promotion_qualified_by'] == 3) {
                    //any product in list
                    if ((in_array($item['product_type_id'], $anyProductTypeArray) || in_array($item['product_id'], $anyProductArray)) && in_array($item['product_id'], $getPromotionDetail['promotionAnyProducts'])) {
                        // Shipping Discount
                        if ($getPromotionDetail['is_shipping_discount'] == 1) {

                            if ($getPromotionDetail['shipping_discount_type'] == 1) {
                                $totalShippableProductWeight-= ($item['product_weight'] * $item['product_qty']);
                            }
                        }
                        // end of shippin discount                        
                    }
                }
            }
        }

        foreach ($cartData as $key => $val) {
            if ($val['is_promotional'] == 1 && (!in_array($val['product_type_id'], $excludeProductTypeArray)) && (!in_array($val['product_id'], $excludeProductArray))) {
                if ($getPromotionDetail['promotion_qualified_by'] == 4) {
                    // global
                    // Shipping Discount
                    if ($getPromotionDetail['is_shipping_discount'] == 1) {

                        if ($getPromotionDetail['shipping_discount_type'] == 1) {
                            $cartData[$key]['is_free_shipping'] = 1;
                            $cartData[$key]['shipping_discount_amount'] = 0;
                        } elseif ($getPromotionDetail['shipping_discount_type'] == 2) {
                            if ($val['is_free_shipping'] == 0 && $val['is_shippable'] == 1 && $val['shipping_cost'] == 1) {
                                $shipping_discount_amount = $this->calculateProductShippingDiscount(($val['product_weight'] * $val['product_qty']), $totalShippableProductWeight, $getPromotionDetail['free_shipping_amount']);
                                $cartData[$key]['is_free_shipping'] = 0;
                                $cartData[$key]['shipping_discount_amount'] = $shipping_discount_amount;
                            }
                        }
                    }
                    // end of shippin discount
                    // cart discount
                    if ($getPromotionDetail['is_cart_discount'] == 1) {
                        if ($getPromotionDetail['cart_discount_type'] == 1) { // percent
                            $cartDiscountAmountOff = ($totalCartAmount * $getPromotionDetail['cart_discount_percent']) / 100;
                        } elseif ($getPromotionDetail['cart_discount_type'] == 2) { // amount off
                            $cartDiscountAmountOff = $getPromotionDetail['cart_discount_amount'];
                        }

                        $productDiscount = $this->calculateProductCartDiscount($val['product_subtotal'] - $val['membership_discount'], $totalProductCartAmount, $cartDiscountAmountOff);

                        $cartData[$key]['product_discount'] = $productDiscount;
                    }
                    // end of cart discount
                    // free products
                    if ($getPromotionDetail['is_free_product'] == 1) {
                        $freeProductsArray = explode(',', $getPromotionDetail['free_products']);

                        if (in_array($cartData[$key]['product_id'], $freeProductsArray)) {
                            //asd($val);
                            $productDiscount = $this->calculateFreeProductDiscount($val['product_qty'], ($val['product_subtotal'] - $val['membership_discount']));
                            $cartData[$key]['product_discount'] = $productDiscount;
                            $cartData[$key]['is_taxable'] = 0;
                        }
                    }
                    // end of free products discount
                } elseif ($getPromotionDetail['promotion_qualified_by'] == 1) {
                    // min product cart amount

                    if (!empty($getPromotionDetail['promotionMinimumAmountProducts'])) {

                        sort($arrCartProductId);
                        sort($getPromotionDetail['promotionMinimumAmountProducts']);
                        $arrComanProduct = array_intersect($arrCartProductId, $getPromotionDetail['promotionMinimumAmountProducts']);
                        sort($arrComanProduct);

                        if ($totalProductCartAmount >= $getPromotionDetail['promotion_qualified_min_amount']) {
                            // Shipping Discount
                            if ($getPromotionDetail['is_shipping_discount'] == 1 && ((in_array($val['product_type_id'], $minimumAmountProductTypeArray)) || (in_array($val['product_id'], $minimumAmountProductArray)))) {

                                if ($getPromotionDetail['shipping_discount_type'] == 1) {
                                    $cartData[$key]['is_free_shipping'] = 1;
                                    $cartData[$key]['shipping_discount_amount'] = 0;
                                } elseif ($getPromotionDetail['shipping_discount_type'] == 2) {
                                    if ($val['is_free_shipping'] == 0 && $val['is_shippable'] == 1 && $val['shipping_cost'] == 1) {
                                        $shipping_discount_amount = $this->calculateProductShippingDiscount(($val['product_weight'] * $val['product_qty']), $totalShippableProductWeight, $getPromotionDetail['free_shipping_amount']);
                                        $cartData[$key]['is_free_shipping'] = 0;
                                        $cartData[$key]['shipping_discount_amount'] = $shipping_discount_amount;
                                    }
                                }
                            }
                            // end of shippin discount
                            // cart discount

                            if ($getPromotionDetail['is_cart_discount'] == 1 && ((in_array($val['product_type_id'], $minimumAmountProductTypeArray)) || (in_array($val['product_id'], $minimumAmountProductArray)))) {
                                if ($getPromotionDetail['cart_discount_type'] == 1) { // percent
                                    $cartDiscountAmountOff = ($totalProductCartAmount * $getPromotionDetail['cart_discount_percent']) / 100;
                                } elseif ($getPromotionDetail['cart_discount_type'] == 2) { // amount off
                                    $cartDiscountAmountOff = $getPromotionDetail['cart_discount_amount'];
                                }

                                $productDiscount = $this->calculateProductCartDiscount($val['product_subtotal'] - $val['membership_discount'], $totalProductCartAmount, $cartDiscountAmountOff);

                                $cartData[$key]['product_discount'] = $productDiscount;
                            }
                            // end of cart discount
                            // free products
                            if ($getPromotionDetail['is_free_product'] == 1 && ((in_array($val['product_type_id'], $minimumAmountProductTypeArray)) || (in_array($val['product_id'], $minimumAmountProductArray)))) {
                                $freeProductsArray = explode(',', $getPromotionDetail['free_products']);

                                if (in_array($cartData[$key]['product_id'], $freeProductsArray)) {
                                    $productDiscount = $this->calculateFreeProductDiscount($val['product_qty'], ($val['product_subtotal'] - $val['membership_discount']));
                                    $cartData[$key]['product_discount'] = $productDiscount;
                                }
                            }
                            // end of free products discount
                        }
                    } else {
                        if ($totalProductCartAmount >= $getPromotionDetail['promotion_qualified_min_amount']) {
                            // Shipping Discount
                            if ($getPromotionDetail['is_shipping_discount'] == 1) {

                                if ($getPromotionDetail['shipping_discount_type'] == 1) {
                                    $cartData[$key]['is_free_shipping'] = 1;
                                    $cartData[$key]['shipping_discount_amount'] = 0;
                                } elseif ($getPromotionDetail['shipping_discount_type'] == 2) {
                                    if ($val['is_free_shipping'] == 0 && $val['is_shippable'] == 1 && $val['shipping_cost'] == 1) {
                                        $shipping_discount_amount = $this->calculateProductShippingDiscount(($val['product_weight'] * $val['product_qty']), $totalShippableProductWeight, $getPromotionDetail['free_shipping_amount']);
                                        $cartData[$key]['is_free_shipping'] = 0;
                                        $cartData[$key]['shipping_discount_amount'] = $shipping_discount_amount;
                                    }
                                }
                            }
                            // end of shippin discount
                            // cart discount
                            if ($getPromotionDetail['is_cart_discount'] == 1) {
                                if ($getPromotionDetail['cart_discount_type'] == 1) { // percent
                                    $cartDiscountAmountOff = ($totalCartAmount * $getPromotionDetail['cart_discount_percent']) / 100;
                                } elseif ($getPromotionDetail['cart_discount_type'] == 2) { // amount off
                                    $cartDiscountAmountOff = $getPromotionDetail['cart_discount_amount'];
                                }


                                $productDiscount = $this->calculateProductCartDiscount($val['product_subtotal'] - $val['membership_discount'], $totalProductCartAmount, $cartDiscountAmountOff);

                                $cartData[$key]['product_discount'] = $productDiscount;
                            }
                            // end of cart discount
                            // free products
                            if ($getPromotionDetail['is_free_product'] == 1) {
                                $freeProductsArray = explode(',', $getPromotionDetail['free_products']);

                                if (in_array($val['product_id'], $freeProductsArray)) {
                                    $productDiscount = $this->calculateFreeProductDiscount($val['product_qty'], ($val['product_subtotal'] - $val['membership_discount']));
                                    $cartData[$key]['product_discount'] = $productDiscount;
                                    $cartData[$key]['is_taxable'] = 0;
                                }
                            }
                            // end of free products discount
                        }
                    }
                } elseif ($getPromotionDetail['promotion_qualified_by'] == 2) {
                    // all bundled products
                    sort($arrCartProductId);
                    sort($getPromotionDetail['promotionbundledbroducts']);
                    $arrComanProduct = array_intersect($arrCartProductId, $getPromotionDetail['promotionbundledbroducts']);
                    sort($arrComanProduct);
                    if ($arrComanProduct == $getPromotionDetail['promotionbundledbroducts']) {
                        // Shipping Discount
                        if ($getPromotionDetail['is_shipping_discount'] == 1 && in_array($val['product_id'], $getPromotionDetail['promotionbundledbroducts'])) {

                            if ($getPromotionDetail['shipping_discount_type'] == 1) {
                                $cartData[$key]['is_free_shipping'] = 1;
                                $cartData[$key]['shipping_discount_amount'] = 0;
                            } elseif ($getPromotionDetail['shipping_discount_type'] == 2) {
                                if ($val['is_free_shipping'] == 0 && $val['is_shippable'] == 1 && $val['shipping_cost'] == 1) {
                                    $shipping_discount_amount = $this->calculateProductShippingDiscount(($val['product_weight'] * $val['product_qty']), $totalShippableProductWeight, $getPromotionDetail['free_shipping_amount']);
                                    $cartData[$key]['is_free_shipping'] = 0;
                                    $cartData[$key]['shipping_discount_amount'] = $shipping_discount_amount;
                                }
                            }
                        }
                        // end of shippin discount
                        // cart discount

                        if ($getPromotionDetail['is_cart_discount'] == 1 && in_array($val['product_id'], $getPromotionDetail['promotionbundledbroducts'])) {
                            if ($getPromotionDetail['cart_discount_type'] == 1) { // percent
                                $cartDiscountAmountOff = ($totalProductCartAmount * $getPromotionDetail['cart_discount_percent']) / 100;
                            } elseif ($getPromotionDetail['cart_discount_type'] == 2) { // amount off
                                $cartDiscountAmountOff = $getPromotionDetail['cart_discount_amount'];
                            }

                            $productDiscount = $this->calculateProductCartDiscount($val['product_subtotal'] - $val['membership_discount'], $totalProductCartAmount, $cartDiscountAmountOff);

                            $cartData[$key]['product_discount'] = $productDiscount;
                        }
                        // end of cart discount
                        // free products
                        if ($getPromotionDetail['is_free_product'] == 1 && in_array($val['product_id'], $getPromotionDetail['promotionbundledbroducts'])) {
                            $freeProductsArray = explode(',', $getPromotionDetail['free_products']);

                            if (in_array($cartData[$key]['product_id'], $freeProductsArray)) {
                                $productDiscount = $this->calculateFreeProductDiscount($val['product_qty'], ($val['product_subtotal'] - $val['membership_discount']));
                                $cartData[$key]['product_discount'] = $productDiscount;
                                $cartData[$key]['is_taxable'] = 0;
                            }
                        }
                        // end of free products discount
                    }
                } elseif ($getPromotionDetail['promotion_qualified_by'] == 3) {
                    //any product in list
                    if ((in_array($val['product_type_id'], $anyProductTypeArray) || in_array($val['product_id'], $anyProductArray)) && in_array($val['product_id'], $getPromotionDetail['promotionAnyProducts'])) {
                        // Shipping Discount
                        if ($getPromotionDetail['is_shipping_discount'] == 1) {

                            if ($getPromotionDetail['shipping_discount_type'] == 1) {
                                $cartData[$key]['is_free_shipping'] = 1;
                                $cartData[$key]['shipping_discount_amount'] = 0;
                            } elseif ($getPromotionDetail['shipping_discount_type'] == 2) {
                                if ($val['is_free_shipping'] == 0 && $val['is_shippable'] == 1 && $val['shipping_cost'] == 1) {
                                    $shipping_discount_amount = $this->calculateProductShippingDiscount(($val['product_weight'] * $val['product_qty']), $totalShippableProductWeight, $getPromotionDetail['free_shipping_amount']);
                                    $cartData[$key]['is_free_shipping'] = 0;
                                    $cartData[$key]['shipping_discount_amount'] = $shipping_discount_amount;
                                }
                            }
                        }
                        // end of shippin discount
                        // cart discount
                        if ($getPromotionDetail['is_cart_discount'] == 1) {
                            if ($getPromotionDetail['cart_discount_type'] == 1) { // percent
                                $cartDiscountAmountOff = ($totalProductCartAmount * $getPromotionDetail['cart_discount_percent']) / 100;
                            } elseif ($getPromotionDetail['cart_discount_type'] == 2) { // amount off
                                $cartDiscountAmountOff = $getPromotionDetail['cart_discount_amount'];
                            }

                            $productDiscount = $this->calculateProductCartDiscount($val['product_subtotal'] - $val['membership_discount'], $totalProductCartAmount, $cartDiscountAmountOff);

                            $cartData[$key]['product_discount'] = $productDiscount;
                        }
                        // end of cart discount
                        // free products
                        if ($getPromotionDetail['is_free_product'] == 1) {
                            $freeProductsArray = explode(',', $getPromotionDetail['free_products']);

                            if (in_array($cartData[$key]['product_id'], $freeProductsArray)) {
                                $productDiscount = $this->calculateFreeProductDiscount($val['product_qty'], ($val['product_subtotal'] - $val['membership_discount']));
                                $cartData[$key]['product_discount'] = $productDiscount;
                                $cartData[$key]['is_taxable'] = 0;
                            }
                        }
                        // end of free products discount
                    }
                }
            } else {
                $cartData[$key]['shipping_discount_amount'] = '0.00';
            }
        }
        return $cartData;
    }   
    
	function calculateProductShippingDiscount($indShippableProductWeight, $totalShippableProductWeight, $shippingDiscountAmountOff) {
        $percentWeight = ($indShippableProductWeight * 100) / $totalShippableProductWeight;
        $shippingDiscountAmount = ($percentWeight * $shippingDiscountAmountOff) / 100;
        return $shippingDiscountAmount;
    }

    function calculateProductCartDiscount($indProductAmount, $totalCartAmount, $cartDiscountAmountOff) {
        $percentAmount = ($indProductAmount * 100) / $totalCartAmount;
        $productDiscount = ($percentAmount * $cartDiscountAmountOff) / 100;
        return $productDiscount;
    }

    function calculateFreeProductDiscount($productQty, $productAmount) {
        /* if ($productQty > 1 && $productDiscountWithQty > 0) {
          $singleProductDiscount = $productDiscountWithQty / $productQty;
          $productDiscount = (($productQty - 1) * $singleProductDiscount) + $productAmount;
          } else {
          $productDiscount = $productAmount / $productQty;
          } */

        $productDiscount = $productAmount / $productQty;
        return $productDiscount;
    }
	 function subval_sort($a, $subkey) {
        foreach ($a as $k => $v) {
            $b[$k] = strtolower($v[$subkey]);
        }
        asort($b);
        foreach ($b as $key => $val) {
            $c[] = $a[$key];
        }
        return $c;
    }    
    
// By Dev 2 used to validate the coupon code NEWPROMOCODE
    public function isValidCouponCheck($selectedShipping_method, $userId, $shipping_country = '0', $getPromotionDetail, $cartData) {
        $returnData = array();
        $applicationConfigIds = $this->_config['application_config_ids'];
        $uSId = $applicationConfigIds['united_state_country_id'];

        $serviceManager = $this->getServiceLocator();
        $promotionTable = $serviceManager->get('Promotions\Model\PromotionTable');

        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        if (count($getPromotionDetail) > 0) {
            $promotionUsedByUserRecord = array();

            /** Expiry date check * */
            if (($getPromotionDetail['expiry_date'] != '' && $getPromotionDetail['expiry_date'] != 0 && substr(DATE_TIME_FORMAT, 0, 10) > substr($getPromotionDetail['expiry_date'], 0, 10)) || ($getPromotionDetail['start_date'] != '' && $getPromotionDetail['start_date'] != 0 && substr(DATE_TIME_FORMAT, 0, 10) < substr($getPromotionDetail['start_date'], 0, 10))) {
                $returnData['status'] = "error";
                $returnData['message'] = $msgArr['COUPON_CODE_EXPIRED'];
                return $returnData;
            }
            /** end of expiry chaek * */
            /** Usage Limit Check * */
            if ($getPromotionDetail['usage_limit_type'] == 2 || $getPromotionDetail['usage_limit_type'] == 3) { // One Time Per Contact Limit Condition
                $promoUsedSearchParam = array();
                $promoUsedSearchParam['userId'] = $userId;
                $promoUsedSearchParam['promotionId'] = $getPromotionDetail['promotion_id'];
                $promotionUsedDetail = $promotionTable->checkUserUsedCoupon($promoUsedSearchParam); // used to check that coupon is used by user or not
            }

            if (($getPromotionDetail['usage_limit_type'] == 1) || ($getPromotionDetail['usage_limit_type'] == 2 && $promotionUsedDetail[0]['userUsedCount'] == 0) || (($getPromotionDetail['usage_limit_type'] == 3 && $getPromotionDetail['usage_limit_per_contact'] != 1) && ($getPromotionDetail['usage_limit'] > 0 && $getPromotionDetail['usage_limit'] > $promotionUsedDetail[0]['promotionUsedCount']) ) || (($getPromotionDetail['usage_limit_type'] == 3 && $getPromotionDetail['usage_limit_per_contact'] == 1) && ($getPromotionDetail['usage_limit'] > 0 && $promotionUsedDetail[0]['userUsedCount'] == 0) && ($getPromotionDetail['usage_limit'] > $promotionUsedDetail[0]['promotionUsedCount']))) {

                if ($getPromotionDetail['is_shipping_discount'] == 1 && $getPromotionDetail['shipping_discount_type'] == 1) { // shipping_discount
                    if (!empty($getPromotionDetail['applicable_to'])) { // domestic/international
                        $invalidFl = 0;
                        if ($getPromotionDetail['applicable_to'] == 1 && $shipping_country != '228') { // Domestic
                            $invalidFl = 1;
                        } else if ($getPromotionDetail['applicable_to'] == 2 && $shipping_country == '228') { // International
                            $invalidFl = 1;
                        } else if ($getPromotionDetail['applicable_to'] == 3) {
                            $invalidFl = 0;
                        }



                        if ($invalidFl == 1) {
                            $returnData['status'] = "error";
                            $returnData['message'] = $msgArr['COUPON_NOT_VALID'];
                            return $returnData;
                        }
                    }
                }

                if ($getPromotionDetail['applicable_id'] == 2) { // Applied for check in contact check
                    $promotionUserSearchParam = array();
                    $promotionUserSearchParam['promotion_id'] = $getPromotionDetail['promotion_id'];
                    $promotionUserSearchParam['user_id'] = $userId;
                    $promotionUserSearchParam['applicable_id'] = 2; // 2=for user, 3=Group        

                    $promotionUsersRecords = $promotionTable->getUsersInPromotion($promotionUserSearchParam);
                } else if ($getPromotionDetail['applicable_id'] == 3) { // Applied for check in group check
                    $promotionUserSearchParam = array();
                    $promotionUserSearchParam['promotion_id'] = $getPromotionDetail['promotion_id'];
                    $promotionUserSearchParam['user_id'] = $userId;
                    $promotionUserSearchParam['applicable_id'] = 3; // 2=for user, 3=Group 					

                    $promotionUsersRecords = $promotionTable->getUsersInPromotion($promotionUserSearchParam);

                    if (empty($promotionUsersRecords) || ($promotionUsersRecords[0]['totalApplied'] == 0)) {
                        $appliedSearchParam = array();
                        $availableUsers = array();
                        $appliedSearchParam[] = $getPromotionDetail['promotion_id'];
                        $appliedSearchParam[] = 'applied';
                        $appliedSearchParam[] = 'Group';
                        $appliedData = $promotionTable->getProductPromotionAppliedByPromotionId($appliedSearchParam);

                        foreach ($appliedData as $appliedRecord) {
                            if ($appliedRecord['group_type'] == 2) {

                                parse_str($appliedRecord['search_query'], $searchParam);

                                $searchParam['transaction_amount_date_from'] = (isset($searchParam['transaction_amount_date_from']) and trim($searchParam['transaction_amount_date_from']) != "") ? $this->DateFormat($searchParam['transaction_amount_date_from'], 'db_date_format_from') : "";

                                $searchParam['transaction_amount_date_to'] = (isset($searchParam['transaction_amount_date_to']) and trim($searchParam['transaction_amount_date_to']) != "") ? $this->DateFormat($searchParam['transaction_amount_date_to'], 'db_date_format_to') : "";

                                $searchParam['userId_exclude'] = '';
                                $excludedContact = $this->getServiceLocator()->get('Group\Model\GroupTable')->getExcludedContactInGroup(array('groupId' => $appliedRecord['applied_id']));
                                if (!empty($excludedContact)) {
                                    $userIds = array();
                                    foreach ($excludedContact as $val) {
                                        $userIds[] = $val['user_id'];
                                    }
                                    $searchParam['userId_exclude'] = implode(",", $userIds);
                                }

                                $searchParam['startIndex'] = '';
                                $searchParam['recordLimit'] = '';
                                $searchParam['sortField'] = '';
                                $searchParam['sortOrder'] = '';



                                $group = new Group($this->_adapter);

                                $groupContactSearchArray = $group->getArrayForSearchCotactsGroup($searchParam);

                                $groupContactsData = $this->getServiceLocator()->get('Group\Model\GroupTable')->getContactSearch($groupContactSearchArray);

                                foreach ($groupContactsData as $k => $grpUsers) {
                                    $users = (array) $grpUsers;
                                    if ($users['user_id'] == $userId) {
                                        $promotionUsersRecords[0]['totalApplied'] = 1;
                                        break;
                                    }else
                                        continue;
                                }
                            }
                        }
                    }
                }


                if (($getPromotionDetail['applicable_id'] == 2 || $getPromotionDetail['applicable_id'] == 3) && $promotionUsersRecords[0]['totalApplied'] == 0) {
                    $returnData['status'] = "error";
                    $returnData['message'] = $msgArr['COUPON_NOT_VALID'];
                    return $returnData;
                }
                $promoQualifyCondition = $this->promoQualifyCondition($getPromotionDetail, $cartData);
                $isValidPromotion = $promoQualifyCondition['isValidPromotion'];
                unset($promoQualifyCondition['isValidPromotion']);

                if ($isValidPromotion == 1) { // checking for qualify filter parameters NEWPROMOCODE
                    $returnData['status'] = "success";
                    $returnData['message'] = $msgArr['COUPON_CODE_APPLIED'];
                    $returnData['promotionData'] = $getPromotionDetail;
                    $returnData['totalProductCartAmount'] = $promoQualifyCondition['totalProductCartAmount'];
                    return $returnData;
                } else {
                    $returnData['status'] = "error";
                    $returnData['message'] = $msgArr['COUPON_NOT_VALID'];
                    return $returnData;
                }
            } else {
                $returnData['status'] = "error";
                $returnData['message'] = $msgArr['COUPON_NOT_VALID'];
                return $returnData;
            }
            // End of Usage Limit check
        } else {
            $returnData['status'] = "error";
            $returnData['message'] = 'This promo code does not exists.';
            return $returnData;
        }
    }    
    
 function promoQualifyCondition($getPromotionDetail, $cartDataRecord) {
        $bundleProductTypeArray = array();
        $bundleProductArray = array();
        $anyProductTypeArray = array();
        $anyProductArray = array();
        $excludeProductTypeArray = array();
        $excludeProductArray = array();
        $promoQualifiedArray = array();
        if (!empty($getPromotionDetail['bundle_product_type'])) { // Buldle products
            $bundleProductTypeArray = explode(',', $getPromotionDetail['bundle_product_type']);
        }
        if (!empty($getPromotionDetail['bundle_products'])) {
            $bundleProductArray = explode(',', $getPromotionDetail['bundle_products']);
        }
        if (!empty($getPromotionDetail['promotion_any_product_type'])) { // Any product
            $anyProductTypeArray = explode(',', $getPromotionDetail['promotion_any_product_type']);
        }
        if (!empty($getPromotionDetail['promotion_any_products'])) {
            $anyProductArray = explode(',', $getPromotionDetail['promotion_any_products']);
        }

        if (!empty($getPromotionDetail['excluded_product_type'])) { // Exclude product
            $excludeProductTypeArray = explode(',', $getPromotionDetail['excluded_product_type']);
        }
        if (!empty($getPromotionDetail['excluded_products'])) {
            $excludeProductArray = explode(',', $getPromotionDetail['excluded_products']);
        }

        if (!empty($getPromotionDetail['minimum_amount_product_type'])) { // Minimum amount products
            $minimumAmountProductTypeArray = explode(',', $getPromotionDetail['minimum_amount_product_type']);
        }
        if (!empty($getPromotionDetail['minimum_amount_products'])) {
            $minimumAmountProductArray = explode(',', $getPromotionDetail['minimum_amount_products']);
        }

        if (!empty($getPromotionDetail['free_products']) && $getPromotionDetail['is_free_product'] == 1) {
            $freeProductsArray = explode(',', $getPromotionDetail['free_products']);
        }

        $arrCartProductId = array();
        foreach ($cartDataRecord as $cartData) {
            $arrCartProductId[] = $cartData['product_id'];
        }
        $arrCartProductId = array_unique($arrCartProductId);

        $totalCartAmount = 0; // total cart amount with membership discount
        $totalProductCartAmount = 0;
        $isValid = 0;
        $totalMinimumProductCartAmount = 0;

        foreach ($cartDataRecord as $cartData) {
            if ($cartData['is_promotional'] == 1) {
                if (!in_array($cartData['product_type_id'], $excludeProductTypeArray) && !in_array($cartData['product_id'], $excludeProductArray)) { // exclude condition
                    if ($getPromotionDetail['promotion_qualified_by'] == 2) { // All bundle products conditions
                        sort($arrCartProductId);
                        sort($getPromotionDetail['promotionbundledbroducts']);
                        $arrComanProduct = array_intersect($arrCartProductId, $getPromotionDetail['promotionbundledbroducts']);
                        sort($arrComanProduct);
                        if ($arrComanProduct == $getPromotionDetail['promotionbundledbroducts']) {
                            $isValid = 1;
                            //$totalProductCartAmount+= $cartData['product_subtotal'] - $cartData['membership_discount'];
                            if (in_array($cartData['product_id'], $getPromotionDetail['promotionbundledbroducts'])) {
                                $totalProductCartAmount+= $cartData['product_subtotal'] - $cartData['membership_discount'];
                            }
                            if (in_array($cartData['product_id'], $freeProductsArray)) {
                                $totalProductCartAmount-= ($cartData['product_amount'] - $cartData['membership_indi_discount']);
                            }
                        }
                    } else if ($getPromotionDetail['promotion_qualified_by'] == 3) { // Any Product
                        if (in_array($cartData['product_type_id'], $anyProductTypeArray) || in_array($cartData['product_id'], $anyProductArray)) {
                            if (in_array($cartData['product_id'], $getPromotionDetail['promotionAnyProducts'])) {
                                $totalProductCartAmount+= $cartData['product_subtotal'] - $cartData['membership_discount'];
                            }
                            $isValid = 1;
                            if (in_array($cartData['product_id'], $freeProductsArray)) {
                                $totalProductCartAmount-= ($cartData['product_amount'] - $cartData['membership_indi_discount']);
                            }
                        }
                    } else if ($getPromotionDetail['promotion_qualified_by'] == 4) { // Global coupon
                        $isValid = 1;
                        $totalProductCartAmount+= $cartData['product_subtotal'] - $cartData['membership_discount'];
                        if (in_array($cartData['product_id'], $freeProductsArray)) {
                            $totalProductCartAmount-= ($cartData['product_amount'] - $cartData['membership_indi_discount']);
                        }
                    } else if ($getPromotionDetail['promotion_qualified_by'] == 1) { // Min amount
                        if (!empty($getPromotionDetail['promotionMinimumAmountProducts'])) {
                            if ((in_array($cartData['product_type_id'], $minimumAmountProductTypeArray) || in_array($cartData['product_id'], $minimumAmountProductArray)) && in_array($cartData['product_id'], $getPromotionDetail['promotionMinimumAmountProducts'])) {
                                $totalProductCartAmount+= ($cartData['product_amount'] - $cartData['membership_indi_discount']);
                                $isValid = 1;
                            }
                            if (in_array($cartData['product_id'], $freeProductsArray)) {
                                $totalProductCartAmount-= ($cartData['product_amount'] - $cartData['membership_indi_discount']);
                            }
                        } else {
                            $isValid = 1;
                        }
                    }
                }
                /* if ($isValid == 1) {
                  if(in_array($cartData['product_id'],$freeProductsArray)){
                  $totalProductCartAmount-= ($cartData['product_amount']-$cartData['membership_indi_discount']);
                  }

                  } */
            }
        }



        if ($getPromotionDetail['promotion_qualified_by'] == 1) { // Min amount conditions
            if ($isValid == 1 && $totalProductCartAmount >= $getPromotionDetail['promotion_qualified_min_amount']) {
                $isValid = 1;
            } else {
                $isValid = 0;
            }
        }

        $promoQualifiedArray['totalProductCartAmount'] = $totalProductCartAmount;
        $promoQualifiedArray['isValidPromotion'] = $isValid;
        return $promoQualifiedArray;
    }   
    
    
   /**
     * This function is used to get shipping cost of product
     * @param void
     * @return void
     * @author Icreon Tech - DT
     */
    public function addShippingCostToProducts($cartProducts, $postArr = array(), $totalProductWeightAmountForShipping) { // dev 2 NEWPROMOCODE 
        //echo $totalProductWeightAmountForShipping;die;
        if (!empty($cartProducts) && !empty($postArr)) {
            $postData = $postArr['postData'];
            $countryName = '';
            if (!empty($postData['shipping_country'])) {
                $countryArr = array('country_id' => $postData['shipping_country']);
                $common = new Common($this->_adapter);
                $countryArr = $common->getCountryArr($countryArr);
                $countryData = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCountryUser($countryArr);
                $countryName = $countryData[0]['country_code'];
            }
            $residential = 'true';
            if (isset($postArr['is_apo_po']) && $postArr['is_apo_po'] != '0' && $postArr['is_apo_po'] != '1') {
                $residential = 'false';
            }
            $postArr['residential'] = $residential;
            $postArr['postData']['shipping_country'] = $countryName;


            if (isset($postArr['shipping_method']) && trim($postArr['shipping_method']) != 'undefined' && trim($postArr['shipping_method']) != '') {
                $pbSettingInfo = $this->getServiceLocator()->get('Common\Model\CommonTable')->getPBSetting();
                $shipApiUrl = $pbSettingInfo['pb_api_url'];
                $shipApiUsername = $pbSettingInfo['pb_api_username'];
                $shipApiRequestUsername = $pbSettingInfo['request_username'];
                $shipApiPassword = $pbSettingInfo['pb_api_password'];
                $packageType = $pbSettingInfo['package_type'];
                $carrShipArr = explode(',', $postArr['shipping_method']);
                $carrierId = $carrShipArr[0];
                $serviceType = $carrShipArr[1];
                $shipDate = DATE_TIME_FORMAT;
                $holidayDate = $shipDate;
                $validDate = 0;
                $transaction = new Kioskreservation($this->_adapter);
                while (!$validDate) {
                    $holidArr = array('holiday_date' => $holidayDate);
                    $holidayArr = $transaction->getHolidayArr($holidArr);
                    $holidayData = $this->_transactionTable->getHoliday($holidayArr);
                    if (empty($holidayData)) {
                        $validDate = 1;
                        $shipDate = date('Y-m-d h:i:s', strtotime($holidayDate));
                    }
                    $holidayDate = date('Y-m-d', strtotime('+1 day', strtotime($holidayDate)));
                }
                $currentDay = date('N', strtotime($shipDate));
                if ($currentDay > 5) {
                    $nextShipDate = 7 - ($currentDay - 1);
                    $shipDate = date('m-d-Y h:i:s A', strtotime('+' . $nextShipDate . ' day', strtotime($shipDate)));
                }
                $xmlRateRequest = '<?xml version = "1.0" encoding = "utf-8"?><PierbridgeRateRequest>
    <TransactionIdentifier />
    <Orders>
        <Order>
            <OrderID />
            <PickLists>
                <PickList>
                    <PickListID />
                </PickList>
            </PickLists>
        </Order>
    </Orders>
    <InsuranceProvider />
    <Live />
    <CloseShipment />
    <Carrier>' . $carrierId . '</Carrier>
    <ServiceType>' . $serviceType . '</ServiceType>
    <RateType />
    <ShipDate>' . $shipDate . '</ShipDate>
    <RequiredDate />
    <SaturdayDelivery />
    <Location />
    <AccountID />
    <Truckload />
    <ShipmentDescription />
    <FreightAllKinds />
    <Interline />
    <PreferredCarrier />
    <PreferredCarrierCode />
    <AccessorialCodes />
    <LoadingDockDelivery />
    <ConstructionSitePickup />
    <ConstructionSiteDelivery />
    <TradeShowPickup />
    <TradeShowDelivery />
    <InsidePickup />
    <InsideDelivery />
    <LiftGateDelivery />
    <LiftGatePickup />
    <AppointmentDelivery />
    <AppointmentPickup />
    <UnloadFreightAtDelivery />
    <LoadFreightAtPickup />
    <WhiteGlove />
    <TwoManDelivery />
    <DefaultWeightUOM />
    <DefaultDimensionsUOM />
    <ConsolidatedShipmentID />
    <DefaultCurrency />
    <Sender>
        <SentBy />
        <CompanyName />
        <Street />
        <Locale />
        <Other />
        <City />
        <Region />
        <PostalCode />
        <Country />
        <Phone />
        <Email />
        <JobTitle />
        <DepartmentName />
        <LocationDescription />
        <Residential>true</Residential>
        <OverrideType />
    </Sender>
    <Receiver>
        <CompanyName>' . $postData['shipping_company'] . '</CompanyName>
        <Street>' . $postData['shipping_address'] . '</Street>
        <Locale />
        <Other />
        <City>' . $postData['shipping_city'] . '</City>
        <State>' . $postData['shipping_state'] . '</State>
        <PostalCode>' . $postData['shipping_zip_code'] . '</PostalCode>
        <Country>' . $countryName . '</Country>
        <Residential />
        <ReceiverIdentifier />
    </Receiver>
    <ReturnTo>
        <Name />
        <CompanyName />
        <Street />
        <Locale />
        <Other />
        <City />
        <Region />
        <PostalCode />
        <Country />
        <Phone />
    </ReturnTo>
    <Billing>
        <PayerType />
        <AccountNumber />
        <CompanyName />
        <Street />
        <Locale />
        <Other />
        <City />
        <Region />
        <PostalCode />
        <Country />
    </Billing>
    <International>
        <AESTransactionNumber />
        <TermsOfSale />
        <SED>
            <Method />
            <ExemptNumber />
        </SED>
    </International>
    <Packages>';
                $xmlRateRequest.='<Package>
            <Copies />
            <PackItemID />
            <ShipperReference/>
            <ReceiverName>' . $postData['shipping_first_name'] . $postData['shipping_last_name'] . '</ReceiverName>
            <ReceiverPhone />
            <ReceiverEmail />
            <PackageType>' . $packageType . '</PackageType>
            <Weight>' . $totalProductWeightAmountForShipping . '</Weight>
            <WeightUOM />
            <Length/>
            <Width/>
            <Height/>
            <DimensionsUOM />
            <ContentDescription>' . $cartProducts[0]['product_name'] . '</ContentDescription>
            <Insurance>
                <Type />
                <Value />
                <ValueCurrency />
            </Insurance>
            <COD>
                <Type />
                <Value />
                <ValueCurrency />
            </COD>
            <Hold />
            <Holder>
                <Name />
                <CompanyName />
                <Street />
                <Locale />
                <Other />
                <City />
                <Region />
                <PostalCode />
                <Country />
                <Phone />
            </Holder>
            <AdditionalHandling />
            <Oversize />
            <LargePackage />
            <DeliveryConfirmation />
            <FreightClass />
            <NMFC />
            <ItemsOnPallet />
            <NonStandardContainer />
            <HomeDeliveryType />
            <EmailNotification />
            <NonFlatMachinable />
            <NonMachinable />
            <NonRectangular />
            <Flat />
            <Stackable />
            <Registered />
            <Certified />
            <DryIceWeight />
            <DryIceWeightUOM />
            <ShipperRelease />
            <FreezeProtect />
            <PalletExchange />
            <SortAndSegregate />
            <International>
                <DocumentsOnly />
                <Contents>
                    <Content>
                        <Code />
                        <Quantity >' . $cartProducts[0]['product_qty'] . '</Quantity>
                        <OrderedQuantity />
                        <BackOrderedQuantity />
                        <ContentLineValue />
                        <Value />
                        <ValueCurrency />
                        <Weight />
                        <WeightUOM />
                        <Description />
                        <OriginCountry />
                        <PurchaseOrderNumber />
                        <SalesOrderNumber />
                        <ItemCode />
                        <ItemDescription />
                        <UnitsOfMeasure />
                        <CustomerCode />
                        <PartNumber />
                        <BinNumber />
                        <LotNumber />
                        <SerialNumber />
                        <Hazardous />
                        <HazardousExemptionID />
                        <HazardousType />
                        <HazardousUnits />
                        <HazardousUnitsUOM />
                        <HazardousDescription />
                        <HazardousProperShippingName />
                        <HazardousIdentifier />
                        <HazardousClass />
                        <HazardousSubClass />
                        <HazardousPackingGroup />
                        <HazardousTechnicalName />
                        <HazardousTotalQuantity />
                        <HazardousTotalQuantityUOM />
                        <HazardousLabelCodes />
                        <HazardousLabelCodesMask />
                        <HazardousAccessible />
                        <HazardousPassengerAircraft />
                        <HazardousCargoAircraftOnly />
                        <HazardousRequiredInformation />
                        <HazardousConsumerCommodity />
                        <HazardousPackingInstructions />
                        <HazardousIsSpecialProvisionA1A2A51A109 />
                    </Content>
                </Contents>
            </International>
        </Package>';
//    }
//}

                $xmlRateRequest.='</Packages>
    <UserName>' . $shipApiRequestUsername . '</UserName>
</PierbridgeRateRequest>';

                $isAnyProduct = ($totalProductWeightAmountForShipping > 0) ? 1 : 0;

                $totalShipping = 0;

                if ($isAnyProduct == 1) {
                    $curl = curl_init();
                    curl_setopt($curl, CURLOPT_URL, $shipApiUrl);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($curl, CURLOPT_TIMEOUT, 120);
                    curl_setopt($curl, CURLOPT_USERPWD, "$shipApiUsername:$shipApiPassword");
                    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                    curl_setopt($curl, CURLOPT_POST, 1);
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $this->removeSpecialChar($xmlRateRequest));
                    $response = $this->getResponse();
                    $createTransactionMessage = $this->_config['transaction_messages']['config']['create_crm_transaction'];

                    if (($result = curl_exec($curl)) === false) {
                        $msg = array();
                        $msg ['shipping_method'] = curl_errno($curl);
                        $messages = array('status' => "shipping_error", 'message' => $msg);
                        echo \Zend\Json\Json::encode($messages);

                        if (isset($postArr['source_type']) && $postArr['source_type'] == 'frontend') {
                            die;
                        }
                    }
                    curl_close($curl);
                    $xml = new SimpleXMLElement($result);
                    //asd($xml);
                    $shipItemPrices = $xml->Packages->Package;

                    $j = 0;
                    $applicationConfigIds = $this->_config['application_config_ids'];
                    $uSId = $applicationConfigIds['united_state_country_id'];
                    if ($shipItemPrices->Status->Code == 0 && $carrierId != 'custom') { // commented on 2 july 2014
//if ($shipItemPrices->Status->Code == '9') { // we need to remove this line and uncomment the above line
                        $errorDescription = $shipItemPrices->Status->Description;
                        $errorType = 3;

                        if (strpos($errorDescription, 'country code for Consignee') > 0 || strpos($errorDescription, 'country is not valid') > 0 || strpos($errorDescription, 'country not served') > 0) {
                            $errorType = 1;
                            $errorMessage = $createTransactionMessage['L_INVALID_COUNTRY'];
                        } else if (strpos($errorDescription, 'CONSIGNEE') > 0) {
                            $errorType = 2;
                            $errorMessage = $createTransactionMessage['L_INVALID_POSTAL'];
                        } else {
                            $errorType = 3;
                            $errorMessage = $createTransactionMessage['INVALID_ADDRESS'];
                        }

                        $msg = array();
                        if ($postData['shipping_zip_code'] == '') {
                            $msg ['shipping_zip_code'] = $createTransactionMessage['SHIPPING_ZIP_CODE_EMPTY'];
                        } else if ($countryName == '') {
                            $msg ['shipping_country'] = $createTransactionMessage['SHIPPING_COUNTRY_EMPTY'];
                        } else {
                            if ($errorType == 1) {
                                $msg ['shipping_country'] = $errorMessage;
                            } else if ($errorType == 2) {
                                $msg ['shipping_zip_code'] = $errorMessage;
                            } else {
                                $msg ['shipping_method'] = $errorMessage;
                            }
                        }
                        $messages = array('status' => "shipping_error", 'message' => $msg);
                        echo \Zend\Json\Json::encode($messages);
                        if (isset($postArr['source_type']) && $postArr['source_type'] == 'frontend') {
                            die;
                        }
                    } else {
                        $lbsCheck = 0;
                        $surchangeUnder = 0;
                        $surchangeAbove = 0;
                        $shippingHandlingUnder = 0;
                        $shippingHandlingAbove = 0;
                        if (isset($postData['is_apo_po']) && $postData['is_apo_po'] != '0' && $postData['is_apo_po'] != '1') {
                            $shippingMetArr = array();
                            $shippingMetArr['pb_shipping_id'] = $applicationConfigIds['apo_po_pb_id'];
                            $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                            $getShippingMethods = $this->_transactionTable->getShippingMethod($shippingMethodArr);
                            $lbsCheck = $getShippingMethods[0]['lbs_check'];
                            $surchangeUnder = $getShippingMethods[0]['surchange_under_5'];
                            $surchangeAbove = $getShippingMethods[0]['surchange_above_5'];
                            $shippingHandlingUnder = $getShippingMethods[0]['shipping_handling_under_5'];
                            $shippingHandlingAbove = $getShippingMethods[0]['shipping_handling_above_5'];
                        } else if (isset($postData['shipping_country']) && $postData['shipping_country'] == $uSId && $postData['is_apo_po'] == '1') {
                            $shippingMetArr = array();
                            $shippingMetArr['pb_shipping_id'] = $applicationConfigIds['domestic_pb_id'];
                            $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                            $getShippingMethods = $this->_transactionTable->getShippingMethod($shippingMethodArr);
                            $lbsCheck = $getShippingMethods[0]['lbs_check'];
                            $surchangeUnder = $getShippingMethods[0]['surchange_under_5'];
                            $surchangeAbove = $getShippingMethods[0]['surchange_above_5'];
                            $shippingHandlingUnder = $getShippingMethods[0]['shipping_handling_under_5'];
                            $shippingHandlingAbove = $getShippingMethods[0]['shipping_handling_above_5'];
                        } else {

                            $shippingMetArr = array();
                            $shippingMetArr['pb_shipping_id'] = $applicationConfigIds['internation_pb_id'];
                            $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                            $getShippingMethodsInter = $this->_transactionTable->getShippingMethod($shippingMethodArr);
                            $lbsCheck = $getShippingMethodsInter[0]['lbs_check'];
                            if ($totalProductWeightAmountForShipping <= $lbsCheck) {
                                foreach ($getShippingMethodsInter as $shipMethod) {
                                    if ($shipMethod['pb_shipping_type_id'] == $applicationConfigIds['internation_less_than_lbs_id']) {
                                        $getShippingMethods[] = $shipMethod;
                                    }
                                }
                            } else {
                                foreach ($getShippingMethodsInter as $shipMethod) {
                                    if ($shipMethod['pb_shipping_type_id'] == $applicationConfigIds['internation_greater_than_lbs_id']) {
                                        $getShippingMethods[] = $shipMethod;
                                    }
                                }
                            }
                            $lbsCheck = $getShippingMethods[0]['lbs_check'];
                            $surchangeUnder = $getShippingMethods[0]['surchange_under_5'];
                            $surchangeAbove = $getShippingMethods[0]['surchange_above_5'];
                            $shippingHandlingUnder = $getShippingMethods[0]['shipping_handling_under_5'];
                            $shippingHandlingAbove = $getShippingMethods[0]['shipping_handling_above_5'];
                        }
                    }
                }
            }
//foreach ($shipItemPrices as $itemPrice) {
            $category = new Category($this->_adapter);
            $catArr = array();
            $categoryArr = $category->getCategoriesArr($catArr);
            $allCategories = $this->getServiceLocator()->get('Category\Model\CategoryTable')->getCategories($categoryArr);
            $categoryData = array();
            if (!empty($allCategories)) {
                foreach ($allCategories as $category) {
                    $categoryData[$category['category_id']] = $category['parent_id'];
                }
            }
            $dataArr = array();
            $dataArr['categoryData'] = $categoryData;

            $totalShipping = 0;
            $j = 0;
            $z = 1;

            $productCnt = count($cartProducts);

            $totalProductDiscount = 0;
            $totalMembershipDiscount = 0;
            $totalShippingDiscount = 0;
            $totalShipping = 0;
            $totalTaxAmount = 0;
            $subTotal = 0;
            $subtotalAmount = 0;
            $totalAfterShipping = 0;
            $totalShippingPercentage = 0;
            //

            $orderTotal = 0; // final amount

            $totalShippingCostFromPB = (isset($shipItemPrices->TotalCharge)) ? ((string) $shipItemPrices->TotalCharge) : 0;
            //asd($cartProducts);
            foreach ($cartProducts as $index => $value) {

                $totalProductDiscount+=$value['product_discount'];
                $totalMembershipDiscount+=$value['membership_discount'];
                $totalShippingDiscount+=$value['shipping_discount_amount'];
                $productShippingPrice = 0;

                if ($value['is_shippable'] == '1' && $value['product_weight'] > 0 && $value['is_free_shipping'] != '1' && $value['shipping_cost'] == '1') {

                    $percentShipping = ($value['product_weight'] * $value['product_qty'] * 100) / $totalProductWeightAmountForShipping;

                    $totalShippingPercentage = $totalShippingPercentage + $percentShipping;

                    //....... to be set above ...../
                    if ($z == count($cartProducts)) {
                        if ($totalShippingPercentage < 100)
                            $percentShipping = $percentShipping + ( 100 - $totalShippingPercentage);
                    }
                    // .................... to set above .........................//
                    //echo $percentShipping."=>".$totalShippingCostFromPB;die;
                    $productShippingPrice = ($percentShipping * $totalShippingCostFromPB) / 100;

                    //
                    /* if ($carrierId == 'custom') {
                      $productShippingPrice = ($postArr['postData']['custom_shipping'] * $percentShipping) / 100;
                      $productShippingPrice = $productShippingPrice - $value['shipping_discount_amount'];
                      } */
                    // .......................//

                    $dataArr['category_id'] = $value['category_id'];
                    $isCustomFrame = $this->isCustomFrame($dataArr);

                    if ($isCustomFrame) {

                        $cartProducts[$index]['shipping_price'] = $productShippingPrice;
                        $surchangeTax = ($cartProducts[$index]['product_weight'] <= $getShippingMethods[0]['custom_frames_lbs_check']) ? $getShippingMethods[0]['surchange_under_15'] : $getShippingMethods[0]['surchange_above_15'];
                        $shippingHandlineTax = ($cartProducts[$index]['product_weight'] <= $getShippingMethods[0]['custom_frames_lbs_check']) ? $getShippingMethods[0]['shipping_handling_under_15'] : $getShippingMethods[0]['shipping_handling_above_15'];
                        if ($productShippingPrice == 0)
                            $shippingHandlineTax = 0;
                        $cartProducts[$index]['surcharge_price'] = ((($productShippingPrice) * $surchangeTax) / 100);
                        $cartProducts[$index]['shipping_handling_price'] = $shippingHandlineTax;
                    } else {
                        $cartProducts[$index]['shipping_price'] = $productShippingPrice;
                        $surchangeTax = ($cartProducts[$index]['product_weight'] <= $lbsCheck) ? $surchangeUnder : $surchangeAbove;
                        $shippingHandlineTax = ($cartProducts[$index]['product_weight'] <= $lbsCheck) ? $shippingHandlingUnder : $shippingHandlingAbove;
                        if ($productShippingPrice == 0)
                            $shippingHandlineTax = 0;
                        $cartProducts[$index]['surcharge_price'] = ($productShippingPrice * $surchangeTax / 100);
                        $cartProducts[$index]['shipping_handling_price'] = $shippingHandlineTax;
                    }
                }else {
                    $cartProducts[$index]['shipping_price'] = '0.00';
                    $cartProducts[$index]['surcharge_price'] = '0.00';
                    $cartProducts[$index]['shipping_handling_price'] = '0.00';
                }

                if ($carrierId == 'custom') {

                    $productShippingPrice = ($postArr['postData']['custom_shipping'] * $percentShipping) / 100;
                    $productShippingPrice = $productShippingPrice - $value['shipping_discount_amount'];
                    $totalShipping+=$this->Roundamount($productShippingPrice);
                    $cartProducts[$index]['shipping_price'] = $this->Roundamount($productShippingPrice);
                    $cartProducts[$index]['surcharge_price'] = '0.00';
                    $cartProducts[$index]['shipping_handling_price'] = '0.00';
                } else {
                    if ($productShippingPrice != 0) {
                        $productShippingPrice = $productShippingPrice + $cartProducts[$index]['surcharge_price'] + $cartProducts[$index]['shipping_handling_price'];

                        $productShippingPrice = $this->Roundamount($productShippingPrice - $value['shipping_discount_amount']);
                        if ($productShippingPrice < 0)
                            $productShippingPrice = '0.00';
                        $totalShipping+=$productShippingPrice;
                    }
                }
                $subtotalAmount = ($value['product_amount'] * $value['product_qty']) - $value['membership_discount'];
                //.... tax ...//

                $product_amount_for_tax = $subtotalAmount - $value['product_discount'] + $productShippingPrice;
                $productTotalForRevenue = $subtotalAmount - $value['product_discount'];
                if ($product_amount_for_tax < 0)
                    $product_amount_for_tax = 0;
                $totalAfterShipping+= $this->Roundamount($product_amount_for_tax);


                if (isset($cartProducts[$index]['is_taxable']) && $cartProducts[$index]['is_taxable'] == 1 && (isset($postData['shipping_country']) && $postData['shipping_country'] == '228') && (((isset($postData['shipping_state']) && $postData['shipping_state'] == 'NY') || (isset($postData['shipping_state_id']) && $postData['shipping_state_id'] == 'NY') || (isset($postData['apo_po_state']) && $postData['apo_po_state'] == 'NY')))) {

                    $product_tax = $this->Roundamount($product_amount_for_tax) * $postArr['combined_sales_tax'];
                    $cartProducts[$index]['product_taxable_amount'] = $this->Roundamount($product_tax);
                } else {
                    $cartProducts[$index]['product_taxable_amount'] = '0.00';
                }
                $totalTaxAmount+=$cartProducts[$index]['product_taxable_amount'];

                //.... end of tax //


                $cartProducts[$index]['product_total'] = $this->Roundamount($product_amount_for_tax) + $cartProducts[$index]['product_taxable_amount'];
                $cartProducts[$index]['total_product_revenue'] = $this->Roundamount($productTotalForRevenue);

                $orderTotal+= $cartProducts[$index]['product_total'];

                $subTotal+= ($value['product_amount'] * $value['product_qty']) - $value['membership_discount'];

                $cartProducts[$index]['product_discount'] = $this->Roundamount($cartProducts[$index]['product_discount']);
                $cartProducts[$index]['shipping_price'] = $this->Roundamount($cartProducts[$index]['shipping_price']);
                $cartProducts[$index]['surcharge_price'] = $this->Roundamount($cartProducts[$index]['surcharge_price']);
                $cartProducts[$index]['shipping_handling_price'] = $this->Roundamount($cartProducts[$index]['shipping_handling_price']);
                $cartProducts[$index]['product_taxable_amount'] = $this->Roundamount($cartProducts[$index]['product_taxable_amount']);
                $cartProducts[$index]['product_total'] = $this->Roundamount($cartProducts[$index]['product_total']);
                $j++;
                $z++;
            } //echo "=>".$totalShippingDiscount;die;
            //}
            //}
//asd($cartProducts);
            $cartProductsTotalArray['totalTaxAmount'] = $this->Roundamount($totalTaxAmount);
            $cartProductsTotalArray['subTotalAmount'] = $this->Roundamount($subTotal);
            $cartProductsTotalArray['totalaftershipping'] = $this->Roundamount($totalAfterShipping);
            $cartProductsTotalArray['shipping_charge'] = $this->Roundamount($totalShipping);
            $cartProductsTotalArray['total_discount'] = $this->Roundamount($totalProductDiscount);
            $cartProductsTotalArray['total_membership_discount'] = $this->Roundamount($totalMembershipDiscount);
            $cartProductsTotalArray['order_total'] = $this->Roundamount($orderTotal);
            $cartProductsTotalArray['shipping_discount_in_amount'] = $this->Roundamount($totalShippingDiscount);


            $retArray = array();
            $retArray['cartdata'] = $cartProducts;
            $retArray['cartdataTotal'] = $cartProductsTotalArray;
           // asd($retArray);

            return $retArray;
        }
    }    
    /**
     * This function is used to find the categories for shipping charges
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function isCustomFrame($dataArr = array()) {
        /* Get all Categories code for */
        $categoryId = $dataArr['category_id'];
        $isCustomFrame = false;
        while (1) {
            if (isset($categoryData[$categoryId])) {
                if ($categoryData[$categoryId] == 0) {
                    break;
                } else {
                    $categoryId = $categoryData[$categoryId];
                }
            } else {
                break;
            }
        }
        if ($categoryId == 2) {
            $isCustomFrame = true;
        }
        return $isCustomFrame;
    }    
    
    /**
     * This function is used to capture the payment from front side
     * @param      $paymentArray array(), $paymentFrontArray array()
     * @return     json
     * @author     Icreon Tech - SR by dev 3
     */
    function capturePaymentFront($paymentArray, $paymentFrontArray, $isPromotionApplied) {
			$this->KioskTransactionTable();
            $paymentAmount = $paymentFrontArray['transaction_amounts'];

             $transaction = new Kioskreservation($this->_adapter);

            $paymentTransactionData = array();
            $paymentTransactionData['amount'] = $paymentFrontArray['transaction_amounts'];
            $paymentTransactionData['transaction_id'] = $paymentFrontArray['transaction_id'];
            $paymentTransactionData['payment_mode_id'] = 4;
            $paymentTransactionData['authorize_transaction_id'] = '';

            $paymentTransactionData['profile_id'] = '';
            $paymentTransactionData['added_by'] = $this->auth->getIdentity()->crm_user_id;;
            $paymentTransactionData['added_date'] = DATE_TIME_FORMAT;
            $paymentTransactionData['modify_by'] = $this->auth->getIdentity()->crm_user_id;;
            $paymentTransactionData['modify_date'] = DATE_TIME_FORMAT;
            $paymentTransactionData = $transaction->getTransactionPaymentArr($paymentTransactionData);

            $profileDataParam = array();

            $paymentTransactionData['22'] = '';
            $transactionPaymentId = $this->_transactionTable->saveTransactionPaymentDetail($paymentTransactionData);

            foreach ($paymentArray as $key => $val) {
                $paymentReceivedTransactionData = array();
                $paymentReceivedTransactionData['table_auto_id'] = $val['table_auto_id'];
                $paymentReceivedTransactionData['table_type'] = $val['table_type'];
                $paymentReceivedTransactionData['authorize_transaction_id'] = '';
                $paymentReceivedTransactionData['profile_id'] = '';
                $paymentReceivedTransactionData['amount'] = $val['cart_product_amount'];
                $paymentReceivedTransactionData['product_type_id'] = $val['cart_product_type_id'];
                $paymentReceivedTransactionData['transaction_payment_id'] = $transactionPaymentId;
                $paymentReceivedTransactionData['transaction_id'] = $val['transaction_id'];
                $paymentReceivedTransactionData['payment_source'] = 'cash';
                $paymentReceivedTransactionData['isPaymentReceived'] = '1';
                $this->insertIntoPaymentReceive($paymentReceivedTransactionData);
            }

       
    }    
    
   /**
     * This function is used to insert the record in payment receive table.
     * @param array
     * @author Icreon Tech - SR
     */
    public function insertIntoPaymentReceive($dataArray = array()) {
        $this->KioskTransactionTable();

        $receivedPaymentIdArray = array();
        $totalCreditCardAmount = 0;

         $transaction = new Kioskreservation($this->_adapter);
        $paymentTransactionData = array();
        $paymentTransactionData['amount'] = round($dataArray['amount'], 2);
        $paymentTransactionData['transaction_id'] = $dataArray['transaction_id'];
        $paymentTransactionData['authorize_transaction_id'] = $dataArray['authorize_transaction_id'];

        $paymentTransactionData['transaction_payment_id'] = $dataArray['transaction_payment_id'];
        $paymentTransactionData['added_by'] = isset($this->auth->getIdentity()->crm_user_id) ? $this->auth->getIdentity()->crm_user_id : '';
        $paymentTransactionData['added_date'] = DATE_TIME_FORMAT;
        $paymentTransactionData['modify_by'] = isset($this->auth->getIdentity()->crm_user_id) ? $this->auth->getIdentity()->crm_user_id : '';
        $paymentTransactionData['modify_date'] = DATE_TIME_FORMAT;
        $paymentTransactionDataArr['0'] = $dataArray['transaction_id'];
        //$creditTransactionPayments = $this->_transactionTable->getCreditTransactionPayment($paymentTransactionDataArr);
// $paymentTransactionData['transaction_payment_id'] = $creditTransactionPayments[0]['transaction_payment_id'];

        $paymentTransactionData = $transaction->getTransactionPaymentReceiveArr($paymentTransactionData);

        $paymentTransactionData['10'] = $dataArray['product_type_id']; // $dataArray['UnshipProductType'];
        $paymentTransactionData['11'] = @$dataArray['table_auto_id'];
        $paymentTransactionData['12'] = @$dataArray['table_type'];
        $paymentTransactionData['13'] = @$dataArray['isPaymentReceived'];
        $paymentTransactionData['14'] = @$dataArray['batch_id'];
        $paymentTransactionData['9'] = '';


            $paymentTransactionData['9'] = ''; // credit_card_type_id
            $paymentTransactionData['13'] = 1; // is_payment_received field
            return $this->_transactionTable->saveTransactionPaymentReceive($paymentTransactionData);
        
    }

     /**
     * This action is used to update user membership
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function updateUserPledgeTransaction($userPledgeArr = array()) {
        /* Update active pledge for user */
        $pledge = new Pledge($this->_adapter);
        $this->getTransactionTable();
        $postedData = array();
        $postedData['userId'] = $userPledgeArr['userId'];
        $checkUserActivePledgeArr = $pledge->getCheckUserActivePledgeArr($postedData);
        $activePledges = $this->getServiceLocator()->get('Pledge\Model\PledgeTable')->checkUserActivePledge($checkUserActivePledgeArr);
        if (!empty($activePledges)) {
            $searchParam = array();
            $pledgeId = $activePledges[0]['pledge_id'];
            $searchParam['pledge_id'] = $pledgeId;
            $pledgeDetailArr = array();
            $searchPledgeTransactionsArr = $pledge->getPledgeTransactionsSearchArr($searchParam);

            $pledgeData = $this->getServiceLocator()->get('Pledge\Model\PledgeTable')->getPledgeTransactions($searchPledgeTransactionsArr);
            if (!empty($pledgeData)) {
                $pledgeDetailArr = array();
                $pledgeDetailArr[] = $pledgeId;
                $pledgeDataResult = $this->getServiceLocator()->get('Pledge\Model\PledgeTable')->getPledgeById($pledgeDetailArr);
                $pledgeDonationAmount = $userPledgeArr['donation_amount'];
                foreach ($pledgeData as $pleData) {
                    if ($pledgeDonationAmount == 0) {
                        break;
                    }
                    if ($pleData['status'] == 0 && $pledgeDataResult['pledge_status_id'] != 3) {
                        if ($pledgeDonationAmount >= $pleData['amount_pledge']) {
                            $pledgeDonationAmount = $pledgeDonationAmount - $pleData['amount_pledge'];
                            /* Update pledge transaction status */
                            $updatePledgeStatusArray = array();
                            $updatePledgeStatusArray['pledge_transaction_id'] = $pleData['pledge_transaction_id'];
                            $updatePledgeStatusArray['status'] = '1';
                            $updatePledgeStatusArray['transaction_id'] = $userPledgeArr['transaction_id'];
                            $updatePledgeStatusArray['modify_by'] = $this->auth->getIdentity()->crm_user_id;
                            $updatePledgeStatusArray['modify_date'] = DATE_TIME_FORMAT;
                            $updatePledgeTransactionStatusArr = $pledge->updatePledgeTransactionStatusArr($updatePledgeStatusArray);
                            $pledgeTransactionId = $this->getServiceLocator()->get('Pledge\Model\PledgeTable')->updatePledgeTransactionStatus($updatePledgeTransactionStatusArr);
                            /* End update pledge transaction status */
                        } else {
                            $updatedAmountToBePaid = $pleData['amount_pledge'] - $pledgeDonationAmount;
                            $updatePledgeAmountArray = array();
                            $updatePledgeAmountArray['pledge_transaction_id'] = $pleData['pledge_transaction_id'];
                            $updatePledgeAmountArray['amount_pledge'] = $pledgeDonationAmount;
                            $updatePledgeAmountArray['remaining_amount_pledge'] = $updatedAmountToBePaid;
                            $updatePledgeAmountArray['status'] = '1';
                            $updatePledgeAmountArray['transaction_id'] = $userPledgeArr['transaction_id'];
                            $updatePledgeAmountArray['modify_by'] = $this->auth->getIdentity()->crm_user_id;
                            $updatePledgeAmountArray['modify_date'] = DATE_TIME_FORMAT;
                            $updatePledgeTransactionAmountArr = $pledge->updatePledgeTransactionAmountArr($updatePledgeAmountArray);

                            $pledgeTransactionId = $this->getServiceLocator()->get('Pledge\Model\PledgeTable')->updatePledgeTransactionAmount($updatePledgeTransactionAmountArr);
//Update pledge amount to complete
                            break;
                        }
                    }
                }
            }
        }
        /* end  Update activie pledge for user */
    }   
    
    
    /**
     * This action is used to delete transaction
     * @param  void
     * @return json format string
     * @author Icreon Tech - DT
     */
    public function deletePendingKioskTransactionAction() {
        $request = $this->getRequest();
        $this->KioskTransactionTable();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
             $transactionId = $request->getPost('transaction_id');
            if ($transactionId != '') {
                $transactionId = $this->decrypt($transactionId);
                $postArr = array();
                $postArr['transaction_id'] = $transactionId;
                
                
                $transaction = new KioskTransaction($this->_adapter);

                $response = $this->getResponse();
               
                $transactionArr = $transaction->getArrayForRemoveTransaction($postArr);
                $this->_transactionTable->removeTransactionById($transactionArr);
                
                $changeLogArray = array();
                $changeLogArray['table_name'] = 'tbl_tra_change_logs';
                $changeLogArray['activity'] = '3';
                $changeLogArray['id'] = $transactionId;
                $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                /** end insert into the change log */
                $this->flashMessenger()->addMessage('Transaction has been deleted successfully.');
                $messages = array('status' => "success", 'message' => 'Transaction has been deleted successfully.');

                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                return $this->redirect()->toRoute('get-crm-transactions');
            }
        } else {
            return $this->redirect()->toRoute('get-crm-transactions');
        }
    }    
    
}