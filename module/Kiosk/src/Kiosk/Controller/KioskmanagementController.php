<?php

/**
 * This controller is used for kiosk management
 * @package    Kiosk
 * @author     Icreon Tech - SK
 */

namespace Kiosk\Controller;

use Base\Model\UploadHandler;
use Authorize\lib\AuthnetXML;
use Base\Controller\BaseController;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Zend\Session\Container as SessionContainer;
use Zend\View\Model\ViewModel;
use Kiosk\Model\KioskManagement;
use Zend\Mvc\Controller\Plugin\Params;
use Zend\Http\Request;


/**
 * This controller is used to display user detail
 * @package    User
 * @author     Icreon Tech - DG
 */
class KioskmanagementController extends BaseController {

    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_authservice = null;
    protected $_storage = null;
    protected $_kioskmanagementTable = null;

    //protected $auth = null;

    public function __construct() {
        $this->_flashMessage = $this->flashMessenger();
    }

    public function indexAction() {
        
    }

    /**
     * This functikon is used to get table connections
     * @return     array 
     * @author Icreon Tech - DG
     */
    public function getTables() {
        $sm = $this->getServiceLocator();
        $this->_userTable = $sm->get('Kiosk\Model\KioskManagementTable');
        $this->_adapter = $sm->get('dbAdapter');
        $this->_config = $sm->get('Config');
        return $this->_userTable;
    }

    /**
     * This functikon is used to get config variables
     * @return     array 
     * @author Icreon Tech - DG
     */
    public function getConfig() {
        $sm = $this->getServiceLocator();
        $this->_translator = $sm->get('translator');
        $this->_config = $sm->get('Config');
    }

    /**
     * This function is used to get auth object
     * @return     array 
     * @param array
     * @author  Icreon Tech - DG
     */
    public function getSessionStorage() {
        if (!$this->_storage) {
            $this->_storage = $this->getServiceLocator()
                    ->get('User\Model\AuthStorage');
        }
        return $this->_storage;
    }    

}