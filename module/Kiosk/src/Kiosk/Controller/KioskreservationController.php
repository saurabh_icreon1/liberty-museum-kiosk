<?php

/**
 * This controller is used for kiosk management
 * @package    Kiosk
 * @author     Icreon Tech - SK
 */

namespace Kiosk\Controller;

use Base\Model\UploadHandler;
use Authorize\lib\AuthnetXML;
use Base\Controller\BaseController;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Zend\Session\Container as SessionContainer;
use Zend\View\Model\ViewModel;
use Kiosk\Model\KioskManagement;
use Kiosk\Model\KioskReservation;
use Kiosk\Form\KioskReservationSearchForm;
use Kiosk\Form\KioskContactSearchForm;
use Kiosk\Form\AddContactForm;
use Kiosk\Form\AssignContactForm;
use Kiosk\Form\KioskAssignUserForm;
use Zend\Mvc\Controller\Plugin\Params;
use Zend\Http\Request;
use Transaction\Form\PaymentModeCheck;
use Transaction\Form\PaymentModeCredit;

/**
 * This controller is used to display user detail
 * @package    User
 * @author     Icreon Tech - DG
 */
class KioskreservationController extends BaseController {

    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_authservice = null;
    protected $_storage = null;
    protected $_kioskmanagementTable = null;

    //protected $auth = null;

    public function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        $auth = new AuthenticationService();
        $this->auth = $auth;
        if ($this->auth->hasIdentity() === false) {
            //return $this->redirect()->toRoute('crm');
        }
    }

    public function indexAction() {
        
    }

    /**
     * This functikon is used to get table connections
     * @return     array 
     * @author Icreon Tech - SK
     */
    public function getTables() {
        $sm = $this->getServiceLocator();
        $this->_userTable = $sm->get('Kiosk\Model\KioskReservationTable');
        $this->_adapter = $sm->get('dbAdapter');
        $this->_config = $sm->get('Config');
        return $this->_userTable;
    }

    /**
     * This functikon is used to get config variables
     * @return     array 
     * @author Icreon Tech - SK
     */
    public function getConfig() {
        $sm = $this->getServiceLocator();
        $this->_translator = $sm->get('translator');
        $this->_config = $sm->get('Config');
    }

    /**
     * This function is used to get auth object
     * @return     array 
     * @param array
     * @author  Icreon Tech - SK
     */
    public function getSessionStorage() {
        if (!$this->_storage) {
            $this->_storage = $this->getServiceLocator()
                    ->get('User\Model\AuthStorage');
        }
        return $this->_storage;
    }

    /**
     * This action is used to search queued people
     * @param void
     * @return json
     * @author Icreon Tech - AP
     */
    public function getReservationContactAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $searchParam['contact'] = $request->getPost('contact');
        $resultArr = array();
        $tempArr = array();
        $searchResult = $this->getTables()->getReservationContactListSearch($searchParam);
        if (empty($searchResult)) {
            $msg['contact'] = 'Record Not Found';
            $messages = array('status' => "error", 'message' => $msg);
            $data = json_encode($messages);
            echo $data;
            die;
        }

        foreach ($searchResult as $key => $val) {
            //$resultArr[$val['user_id']] = $val['first_name']." ".$val['last_name'];
            $tempArr['id'] = $val['queue_id'];
            $tempArr['value'] = $val['first_name'] . " " . $val['last_name'];
            $resultArr[] = $tempArr;
        }
        //asd($resultArr);
        $data = json_encode($resultArr);
        echo $data;
        die;
        //return $response->setContent(\Zend\Json\Json::encode($searchResult));
    }

    /**
     * This action is used to get next Available Kiosk
     * @param array
     * @return array
     */
   protected function getNextKioskAutoReservation($param){
     // $this->checkUserAuthentication();
         $this->getConfig();
        $this->getTables();
        $request = $this->getRequest();
        $resultMasterArr = array();
        $resultChildArr = array();
        /*$searchResult = $this->getTables()->getNextKiosk($param);
        update$searchKioskResult = $this->getTables()->getkioskDetails($param);
        $updatedResult = $this->getProcessedResult($searchKioskResult,$searchResult,false);*/
		$dateRange = $this->getDateRange(2);
       /*$searchParam['booked_date_from'] = $this->DateFormat($dateRange['from'], 'db_date_format_from');
        $searchParam['booked_date_to'] = $this->DateFormat($dateRange['to'], 'db_date_format_to');*/
		$param['curr_date'] = $this->DateFormat($dateRange['from'], 'db_date_format_from');
		$searchResult = $this->getTables()->getNextKioskDetails($param);//asd($searchResult);//As per new
		$searchResult = $this->processKioskData($searchResult,false);//asd($searchResult,0);
		$searchResult = $this->getKioskAvlData($searchResult);//asd($searchResult);
  
		if(!empty($searchResult))
		$searchResult = $this->customArraySort($searchResult,'compare_ind','asc');
		
		$param['is_delete'] = '1';
		$searchDelResult = $this->getTables()->getNextKioskDetails($param);//As per new 
		
		$searchDelResult = $this->processKioskData($searchDelResult,true);
		//$searchDelResult = $this->getKioskAvlData($searchDelResult);
		if(!empty($searchDelResult))
		$searchDelResult = $this->customArraySort($searchDelResult,'compare_ind','asc');
	
		$kioskSystemId  = false;
		if(!empty($searchResult)){
			$kioskSystemId = $this->getRandomSystem($searchResult);
		}
		$kioskDelSystemId  = false;
		if(!empty($searchDelResult)){
			$kioskDelSystemId = $this->getRandomSystem($searchDelResult);
		}
		if($kioskSystemId === false && $kioskDelSystemId === false){
		//asd("1",0);
				return $resultChildArr;
	
		}else if($kioskSystemId === false && $kioskDelSystemId !== false){
					//asd("2",0);
				 $resultChildArr['queue_id'] = $searchDelResult[$kioskDelSystemId]['queue_id'];//get queue only
		}else if($kioskSystemId !== false && $kioskDelSystemId === false){
				//asd("s3",0);
				//asd($kioskSystemId,0);
				$resultChildArr = $this->getCalculateReserveTime($searchResult[$kioskSystemId]);
		}else if($kioskSystemId !== false && $kioskDelSystemId !== false){
			//asd("4",0);
			// both arr has element compare now
			$resultChildArr = $this->getNextKioskInformation($searchResult[$kioskSystemId],$searchDelResult[$kioskDelSystemId]);
		}
		//asd($searchResult,0);
		//asd($searchDelResult,0);
		//asd($resultChildArr);
		return $resultChildArr;
	}

        
	/*
 * function return a kiosk data 
 */
 function processKioskData($data = array(),$delRec = false){
		if(empty($data))
			return;
		//prepare list
		$nextCalTime = "";
		
		
		foreach($data as $key => $val){
			$nextTimeComp ="";
			$currTime = date('H:i:s',strtotime(DATE_TIME_FORMAT));
			if(!$delRec){
			$timeBetweenUsers = (int) date('i',strtotime($val['time_between_users']));
			$timeForSearchMin = date('i',strtotime($val['time_for_search']));
			$timeForSearchHour = date('H',strtotime($val['time_for_search']));
			$timeForSearchSec = date('s',strtotime($val['time_for_search']));
			
			$timeForSearch = floor($timeForSearchMin + (int)$timeForSearchHour*60 + (int)$timeForSearchSec/60);
			
			if(empty($val['nextTime']) || $val['nextTime'] =="00:00:00"){
			if(empty($val['kiosk_shutoff']) || $val['kiosk_shutoff']=="0000-00-00 00:00:00"){
			//check kiosk start time 
			 $kioskStartTime = date('H:i:s',strtotime($this->OutputDateFormat($val['kiosk_start_time'], 'dateformatampm')));
			 $currTimeOut = date('H:i:s',strtotime($this->OutputDateFormat($currTime, 'dateformatampm')));
			 $startTime = $currTime;
			 if(strtotime($kioskStartTime) > strtotime($currTimeOut)){
				$startTime = date('H:i:s',strtotime($val['kiosk_start_time']));
			 }
			 $nextAvailableTime = date('H:i:s',strtotime('+' . $timeBetweenUsers . ' minutes',strtotime($startTime)));
			
			$nextCalAvlTime = $nextAvailableTime;
			$nextTimeComp = date('H:i:s',strtotime($this->OutputDateFormat($startTime, 'dateformatampm')));
			}else{
			 $processShutOffTime = strtotime(date('H:i:s',strtotime($this->OutputDateFormat($val['kiosk_shutoff'], 'dateformatampm'))));
			 
			$kioskStartTime = date('H:i:s',strtotime($this->OutputDateFormat($val['kiosk_start_time'], 'dateformatampm')));
			$currTimeOut = date('H:i:s',strtotime($this->OutputDateFormat($currTime, 'dateformatampm')));
			 //check kiosk start time 
			if(strtotime($kioskStartTime) > strtotime($currTimeOut)){ 
				$currTime = date('H:i:s',strtotime($val['kiosk_start_time']));
			}
			$timeBetTimeSrch = $timeBetweenUsers + $timeForSearch;
			$nextEndTime = strtotime('+' . $timeBetTimeSrch . ' minutes',strtotime($currTimeOut));
                 
                //if calculated time is not grt than shutoff time
                 $nextAvailableTime = 0;
				 $nextTimeComp = 0;
				 if($processShutOffTime >= $nextEndTime){ 			 
					$nextTimeComp = date('H:i:s',strtotime($this->OutputDateFormat($currTime, 'dateformatampm')));
					$nextCalAvlTime = date('H:i:s',strtotime('+' . $timeBetweenUsers . ' minutes',strtotime($currTime)));
                }else{
					$nextCalAvlTime = 0;
				}
			
			}
		  }
		  else{
			//check if next available time is grt than curr time
			$currTimeOut = date('H:i:s',strtotime($this->OutputDateFormat($currTime, 'dateformatampm')));
			$processShutOffTime = strtotime(date('H:i:s',strtotime($this->OutputDateFormat($val['kiosk_shutoff'], 'dateformatampm'))));
			
			$timeBetweenUsers = (int) date('i',strtotime($val['time_between_users']));
			
			$timeForSearchMin = date('i',strtotime($val['time_for_search']));
			$timeForSearchHour = date('H',strtotime($val['time_for_search']));
			$timeForSearchSec = date('s',strtotime($val['time_for_search']));
			
			$timeForSearch = floor($timeForSearchMin + (int)$timeForSearchHour*60 + (int)$timeForSearchSec/60);
			//decide next end time 
			$nextTime = $currTime;
			if(strtotime($val['nextTime']) > strtotime($currTime)){
				$nextTime = $val['nextTime'];
			}
			$timeBetTimeSrch = $timeBetweenUsers + $timeForSearch;
			$nextEndTime = strtotime('+' . $timeBetTimeSrch . ' minutes',strtotime($this->OutputDateFormat($nextTime, 'dateformatampm')));
			
			//if calculated time is not grt than shutoff time 
			//added 9:27
				$nextTimeComp = 0;
                 if( ($processShutOffTime >= $nextEndTime) || empty($val['kiosk_shutoff'])){ 
					$nextTimeComp = date('H:i:s',strtotime($this->OutputDateFormat($nextTime, 'dateformatampm')));
                   $nextCalAvlTime = date('H:i:s',strtotime('+' . $timeBetweenUsers . ' minutes',strtotime($nextTime)));
                }else{
					$nextCalAvlTime = 0;
				}
		    //check shutoff time with call
		  
		  
		  }
			$data[$key]['next_avl_cal_time'] = $nextCalAvlTime;
			$data[$key]['next_time_sec'] = strtotime($nextCalAvlTime);
			$data[$key]['compare_ind'] = strtotime($nextTimeComp);
		  }else{
			$nextTimeComp = date('H:i:s',strtotime($this->OutputDateFormat($val['time_allocated'], 'dateformatampm')));
		   $data[$key]['next_avl_cal_time'] = $val['time_allocated'];
		   $data[$key]['next_time_sec'] = strtotime($val['time_allocated']);
		   $data[$key]['compare_ind'] = strtotime($nextTimeComp);
		  
		  }
		}
		//asd($data);
		return $data;
	}
	//Remove those are not available
	function getKioskAvlData($data){
		foreach($data as $key => $val){
			if(empty($val['next_avl_cal_time']) || empty($val['compare_ind'])){
				unset($data[$key]);
			}else{
				$currTime = date('H:i:s',strtotime(DATE_TIME_FORMAT));
				$kioskStartTime = date('H:i:s',strtotime($val['kiosk_start_time']));
				/*if(strtotime($currTime) < strtotime($kioskStartTime)){
				 unset($data[$key]);
				}*/
			}
			if($this->getRowIdsKioskModule($val['kiosk_module_status'])){
				unset($data[$key]);
			}
		}
		return $data;
	}
	protected function getRowIdsKioskModule($data){
		$mappingIdKioskModuleId = array();
		$rowIdModule = @explode(",",$data);
		foreach($rowIdModule as $key => $val){
			$mappingIdModule = @explode("=>",$val);
			$mappingIdKioskModuleId[$mappingIdModule[0]]= $mappingIdModule[1];
		}
	 if(isset($mappingIdKioskModuleId[3]) && !empty($mappingIdKioskModuleId[3])){
		return true;
	  }else
		return false;
	}	
	
	function getRandomSystem($data = array()){ 
		if(empty($data))
			return;

		 $indexArr = 0;
		return $indexArr;
	}        
        
    /*
     * function to get
     */

    function getNextKioskInformation($kioskNextSlotData, $kioskDelData) {
		$result = array();
		$nextTime = strtotime($kioskNextSlotData['next_avl_cal_time']);
		$nextDelTime = strtotime($kioskDelData['next_avl_cal_time']);
		if($nextTime < $nextDelTime){
		  $result = $this->getCalculateReserveTime($kioskNextSlotData);
		}else{
		 $result['queue_id'] = $kioskDelData['queue_id'];
		}
		return $result;
    }

    /**
     * This action is used to get next Available Kiosk
     * @param array
     * @return array
     * @author Icreon Tech-AP
     */
    protected function getNextKioskTime($searchParam, $ajaxCall = true) {
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $request = $this->getRequest();
        $kioskReservationObj = new KioskReservation($this->_adapter);

        $searchResult = $this->getTables()->nextAvailableTime($searchParam);
        //Pending do request to delete contact search and match with result time
        $param['is_delete'] = '1';
        $param['curr_date'] = date("Y-m-d", strtotime(DATE_TIME_FORMAT));
        $param['curr_time'] = date("H:i:s", strtotime(DATE_TIME_FORMAT));
        $filterParams = $kioskReservationObj->getAutoReservationParamArr($param);

        $searchDelResult = $this->getTables()->getNextKioskDetails($param);
        $currDate = date("Y-m-d", strtotime(DATE_TIME_FORMAT));
        $currTime = date("H:i:s", strtotime(DATE_TIME_FORMAT));
        $detailsArr = array();
        //No slot added yet 
        if (empty($searchResult)) {
            $kioskDetails = $this->getTables()->getkioskDetails($searchParam);
            $kioskDetails = $kioskDetails[0];
            //Shut off Never
            if (empty($kioskDetails['kiosk_shutoff']) || $kioskDetails['kiosk_shutoff'] == "00:00:00") {
                //asd("time set yes");
                $timeBetweenUsers = date('i', strtotime($this->OutputDateFormat($kioskDetails['time_between_users'], 'dateformatampm')));
                $timeForSearch = date('i', strtotime($this->OutputDateFormat($kioskDetails['time_for_search'], 'dateformatampm')));

                $currTimeOut = date('H:i:s', strtotime($this->OutputDateFormat($currTime, 'dateformatampm')));

                $nextAvailableTime = date('h:i:s A', strtotime('+' . $timeBetweenUsers . ' minutes', strtotime($currTimeOut)));

                $detailsArr['time_allocated'] = date('H:i:s', strtotime($currTime));
                $detailsArr['time_end'] = date('H:i:s', strtotime('+' . $timeForSearch . ' minutes', strtotime($currTime)));
                $detailsArr['time_left'] = date('H:i:s', strtotime('+' . $timeForSearch . ' minutes', strtotime($detailsArr['time_end'])));
            } else {
                //asd("time set no");
                $processShutOffTime = strtotime(date('H:i:s', strtotime($this->OutputDateFormat($kioskDetails['kiosk_shutoff'], 'dateformatampm'))));

                $timeForSearch = date('i', strtotime($this->OutputDateFormat($kioskDetails['time_for_search'], 'dateformatampm')));

                $currTimeOut = date('H:i:s', strtotime($this->OutputDateFormat($currTime, 'dateformatampm')));

                $nextEndTime = strtotime('+' . $timeForSearch . ' minutes', strtotime($currTimeOut));

                //if calculated time is not grt than shutoff time
                $nextAvailableTime = 0;
                if ($processShutOffTime > $nextEndTime) {
                    $timeBetweenUsers = date('i', strtotime($this->OutputDateFormat($kioskDetails['time_between_users'], 'dateformatampm')));

                    $nextAvailableTime = date('h:i:s A', strtotime('+' . $timeBetweenUsers . ' minutes', strtotime($currTimeOut)));

                    $detailsArr['time_allocated'] = date('H:i:s', strtotime('+' . $timeBetweenUsers . ' minutes', strtotime($currTime)));
                    $detailsArr['time_end'] = date('H:i:s', strtotime('+' . $timeForSearch . ' minutes', strtotime($detailsArr['time_allocated'])));
                    $detailsArr['time_left'] = date('H:i:s', strtotime('+' . $timeForSearch . ' minutes', strtotime($detailsArr['time_end'])));
                }
            }
            //If any deleted slot is present 
            if (!empty($searchDelResult)) {
                $val = $searchDelResult[0];
                if (strtotime($val['time_allocated']) < strtotim($detailsArr['time_allocated'])) {
                    $detailsArr['queue_id'] = $val['queue_id'];
                    $timeBetweenUsers = date('i', strtotime($this->OutputDateFormat($val['time_between_users'], 'dateformatampm')));
                    $currTimeOut = date('H:i:s', strtotime($this->OutputDateFormat($val['time_allocated'], 'dateformatampm')));
                    $nextAvailableTime = date('h:i:s A', strtotime('+' . $timeBetweenUsers . ' minutes', strtotime($currTimeOut)));
                }
            }
            //Internally called by any function
            if (!$ajaxCall && $nextAvailableTime != "0") {
                return $detailsArr;
            }

            return $nextAvailableTime;
        } else {
            //If this kiosk has been assigned to some kiosk 
            $nextTime = $searchResult[0]['nexttime'];
            //Check shutoff time of kiosk if provided 
            if (!empty($searchResult[0]['kiosk_shutoff']) && $searchResult[0]['kiosk_shutoff'] != "00:00:00") {
                // $this->InputDateFormat($postData['activate_date'], 'dateformatampm');
                $processShutOffTime = strtotime(date('H:i:s', strtotime($this->OutputDateFormat($searchResult[0]['kiosk_shutoff'], 'dateformatampm'))));
                $timeForSearch = date('i', strtotime($this->OutputDateFormat($searchResult[0]['time_for_search'], 'dateformatampm')));

                $nextEndTime = strtotime('+' . $timeForSearch . ' minutes', strtotime($this->OutputDateFormat($searchResult[0]['nexttime'], 'dateformatampm')));
                $nextEndTimeOut = $this->OutputDateFormat($searchResult[0]['nexttime'], 'dateformatampm');

                //Current time 
                $currTimeOut = strtotime('+' . $timeForSearch . ' minutes', strtotime($this->OutputDateFormat($currTime, 'dateformatampm')));

                //kiosk last slot end time is less than current time then next time 4 calculation is current time
                if ($currTimeOut > $nextEndTime) {
                    $nextEndTime = $currTimeOut;
                    $nextTimeOut = $nextEndTime;
                    $nextTime = date('H:i:s', strtotime(DATE_TIME_FORMAT));
                    $nextEndTimeOut = date('H:i:s', strtotime($this->OutputDateFormat($currTime, 'dateformatampm')));
                }
                //asd($searchResult[0]['nexttime'],0);
                $nextAvailableTime = 0;
                if ($processShutOffTime > $nextEndTime) {
                    $timeBetweenUsers = date('i', strtotime($this->OutputDateFormat($searchResult[0]['time_between_users'], 'dateformatampm')));

                    $nextAvailableTime = date('h:i:s A', strtotime('+' . $timeBetweenUsers . ' minutes', strtotime($nextEndTimeOut)));

                    $detailsArr['time_allocated'] = date('H:i:s', strtotime('+' . $timeBetweenUsers . ' minutes', strtotime($nextTime)));

                    $detailsArr['time_end'] = date('H:i:s', strtotime('+' . $timeForSearch . ' minutes', strtotime($detailsArr['time_allocated'])));

                    $detailsArr['time_left'] = date('H:i:s', strtotime('+' . $timeForSearch . ' minutes', strtotime($detailsArr['time_end'])));
                }
            } else {
                //If kiosk shutoff time set to never
                //$nextAvailableTime =  date('h:i:s A',strtotime('+5 minutes',strtotime($nextTime)));
                $timeBetweenUsers = date('i', strtotime($this->OutputDateFormat($searchResult[0]['time_between_users'], 'dateformatampm')));
                $timeForSearch = date('i', strtotime($this->OutputDateFormat($searchResult[0]['time_for_search'], 'dateformatampm')));
                $nextTime = $searchResult[0]['nexttime'];
                $nextEndTime = strtotime('+' . $timeForSearch . ' minutes', strtotime($this->OutputDateFormat($searchResult[0]['nexttime'], 'dateformatampm')));
                $nextEndTimeOut = $this->OutputDateFormat($searchResult[0]['nexttime'], 'dateformatampm');

                //Current time 
                $currTimeOut = strtotime(date('H:i:s', strtotime($this->OutputDateFormat($currTime, 'dateformatampm'))));

                //kiosk last slot end time is less than current time then next time 4 calculation is current time
                if ($currTimeOut > $nextEndTime) {
                    $nextEndTime = $currTimeOut;
                    $nextTimeOut = $nextEndTime;
                    $nextTime = date('H:i:s', strtotime(DATE_TIME_FORMAT));
                    $nextEndTimeOut = date('H:i:s', strtotime($this->OutputDateFormat($currTime, 'dateformatampm')));
                }
                $nextAvailableTime = date('h:i:s A', strtotime('+' . $timeBetweenUsers . ' minutes', strtotime($nextEndTimeOut)));
                $detailsArr['time_allocated'] = date('H:i:s', strtotime('+' . $timeBetweenUsers . ' minutes', strtotime($nextTime)));
                $detailsArr['time_end'] = date('H:i:s', strtotime('+' . $timeForSearch . ' minutes', strtotime($nextTime)));
                $detailsArr['time_left'] = date('H:i:s', strtotime('+' . $timeForSearch . ' minutes', strtotime($detailsArr['time_end'])));
            }
            //If any deleted slot is present 
            if (!empty($searchDelResult)) {
                $val = $searchDelResult[0];
                if (strtotime($val['time_allocated']) < strtotime($detailsArr['time_allocated'])) {
                    $detailsArr['queue_id'] = $val['queue_id'];
                    $timeBetweenUsers = date('i', strtotime($this->OutputDateFormat($val['time_between_users'], 'dateformatampm')));
                    $currTimeOut = date('H:i:s', strtotime($this->OutputDateFormat($val['time_allocated'], 'dateformatampm')));
                    $nextAvailableTime = date('h:i:s A', strtotime('+' . $timeBetweenUsers . ' minutes', strtotime($currTimeOut)));
                }
            }
            //Internally called by any function
            if (!$ajaxCall && $nextAvailableTime != "0") {
                return $detailsArr;
            }
            return $nextAvailableTime;
        }
    }


    /* function to get contact details
     */

    protected function getReservationContactDetails($queueId) {
        $contactParams = array();
        $contactParams['queue_id'] = $queueId;
        $contactParams['kiosk_id'] = "";
        $contactParams['contact'] = "";
        $contactParams['startIndex'] = "";
        $contactParams['recordLimit'] = "";
        $contactParams['sortField'] = "";
        $contactParams['sortOrder'] = "";
        $contactParams['payment_status'] = "";
        $contactDetails = $this->getTables()->searchReservationQueued($contactParams);
        return $contactDetails;
    }

    /*
     * Return kiosk ids
     */

    protected function getKioskIds($kioskData) {
        $result = array();
        foreach ($kioskData as $key => $val) {
            $result[] = $val['kiosk_id'];
        }
        //$kioskIds = implode("','",$result);
        $kioskIds = '"' . @implode('","', $result) . '"';
        return $kioskIds;
    }


    /**
     * This action is used to add contact from fornt end side 
     * @param array
     * @return array
     * @author Icreon Tech-AP
     */
    public function addContactQueueFrontendAction() {
        //$auth = new AuthenticationService();
        //$userDetails = $auth->getIdentity();
        //$this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $kioskReservationObj = new KioskReservation($this->_adapter);
        $request = $this->getRequest();

        $kioskMessages = $this->_config['kiosk_messages']['config']['kiosk-reservation-system'];
        $viewModel = new ViewModel();
        $response = $this->getResponse();
        $saveParam = array();
        $contactId = "";
        if ($request->isPost()) {
            $contactDetails = 0;
            $searchParam = array();
            //first get cart information 


            $saveParam['payment_mode_id'] = $this->decrypt($request->getPost('payment_mode'));
            $saveParam['amount'] = $this->decrypt($request->getPost('amount'));
            $saveParam['first_name'] = $this->decrypt($request->getPost('first_name'));
            $saveParam['last_name'] = $this->decrypt($request->getPost('last_name'));

            $param['curr_date'] = date("Y-m-d", strtotime(DATE_TIME_FORMAT));
            $param['curr_time'] = date("H:i:s", strtotime(DATE_TIME_FORMAT));
            $param['auto_reservation'] = '1';
            $param['is_delete'] = '0';
            $filterParams = $kioskReservationObj->getAutoReservationParamArr($param);

// 

            $result = $this->getNextKioskAutoReservation($filterParams); // get kiosk id
            if ($saveParam['payment_mode_id'] == 1) {
                if (empty($result)) {
                    $messages = array('status' => "error", 'message' => "Time Slot is not available.");
                    return $response->setContent(\Zend\Json\Json::encode($messages));
                   
                }
            }
            if ($saveParam['payment_mode_id'] == 1) { // credit card
                $saveParam['kiosk_id'] = isset($result['kiosk_id']) ? $result['kiosk_id'] : "";
                $saveParam['payment_status'] = "1";
                $saveParam['time_end'] = isset($result['time_end']) ? $result['time_end'] : "";
                $saveParam['time_left'] = isset($result['time_left']) ? $result['time_left'] : "";
                $saveParam['time_allocated'] = isset($result['time_allocated']) ? $result['time_allocated'] : "";
                $saveParam['transaction_id'] = $this->decrypt($request->getPost('transaction_id'));
                $saveParam['authorize_transaction_id'] = $this->decrypt($request->getPost('authorize_transaction_id'));
            } else {
                $saveParam['kiosk_id'] = "";
                $saveParam['payment_status'] = "0";
                $saveParam['time_end'] = "";
                $saveParam['time_left'] = "";
                $saveParam['time_allocated'] = "";
                $saveParam['transaction_id'] = '';
                $saveParam['authorize_transaction_id'] = '';
            }
			$saveParam['contact_id'] = $this->decrypt($request->getPost('contact_id')); //contact id ;
            $saveParam['is_wheelchair'] = '';
            $saveParam['booking_date'] = DATE_TIME_FORMAT;

            
            $saveParam['queue_id'] = (!empty($result['queue_id'])) ? $result['queue_id'] : "";


            if (!empty($saveParam['queue_id'])) {
                $updateparam['payment_mode_id'] = $saveParam['payment_mode_id'];
                $updateparam['queue_id'] = $saveParam['queue_id'];
                $updateparam['is_delete'] = 0;
                $updateparam['contact_id'] = $this->decrypt($request->getPost('contact_id')); //contact id ;
                $updateparam['confirmation_code'] = $this->GetRandomAlphaNumeric(6);
                $removeOption = $request->getPost('remove_option');
                $filterParams = $kioskReservationObj->getReservationUpdateDataArr($updateparam);
                
                $filterParams['kiosk_id'] = isset($result['kiosk_id']) ? $result['kiosk_id'] : "";
                if ($saveParam['payment_mode_id'] == 1) {
                    $filterParams['payment_status'] = "1";
                }
                else {
                    $filterParams['payment_status'] = "0";
                }
                $filterParams['time_end'] = isset($result['time_end']) ? $result['time_end'] : "";
                $filterParams['time_left'] = isset($result['time_left']) ? $result['time_left'] : "";
                $filterParams['time_allocated'] = isset($result['time_allocated']) ? $result['time_allocated'] : "";
                $filterParams['transaction_id'] = $this->decrypt($request->getPost('transaction_id'));
                $filterParams['authorize_transaction_id'] = $this->decrypt($request->getPost('authorize_transaction_id'));
	            $filterParams['payment_mode_id'] = $this->decrypt($request->getPost('payment_mode'));
	            $filterParams['amount'] = $this->decrypt($request->getPost('amount'));
	            $filterParams['first_name'] = $this->decrypt($request->getPost('first_name'));
	            $filterParams['last_name'] = $this->decrypt($request->getPost('last_name'));
	            $filterParams['pause_resume'] = '1';
	            $filterParams['booking_date'] =  DATE_TIME_FORMAT;
                                
                              //  asd($filterParams);
                //echo "aaaaa";
//asd($filterParams);
                $returnStatus = $contactId = $this->getTables()->updateReservationInfo($filterParams);
                $queueId = $saveParam['queue_id'];
            } else {
                $saveParam['confirmation_code'] = $this->GetRandomAlphaNumeric(6);
                //echo "bbbb";
               // asd($saveParam);
                $queueId = $this->getTables()->insertContact($saveParam);
            }

            if ($queueId) {
                $messages = array(
                    'status' => "success",
                    'queue_id' => $this->encrypt($queueId)
                );
            } else {
                $messages = array(
                    'status' => "error",
                    'message' => $kioskMessages['CONTACT_ADD_FAIL']
                );
            }
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

	 //this function is used to sort multidimension array based on key
   function customArraySort($array, $on='next_time_sec', $order='asc'){
    $new_array = array();
    $sortable_array = array();

    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case 'asc':
                \asort($sortable_array);
                break;
            case 'desc':
                \arsort($sortable_array);
                break;
				
			default:
				\asort($sortable_array);
                break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }
	$new_array = array_values($new_array);
    return $new_array;
	}
    
 	function getCalculateReserveTime($data = array()){ 
	        $timeBetweenUsers = (int) date('i',strtotime($data['time_between_users']));
			$timeForSearchMin = date('i',strtotime($data['time_for_search']));
			$timeForSearchHour = date('H',strtotime($data['time_for_search']));
			$timeForSearchSec = date('s',strtotime($data['time_for_search']));
			
			$timeForSearch = floor($timeForSearchMin + (int)$timeForSearchHour*60 + (int)$timeForSearchSec/60);
		$timeForSearch = floor($timeForSearchMin + (int)$timeForSearchHour*60 + (int)$timeForSearchSec/60);
		$result = array();
		$result['kiosk_id'] = $data['kiosk_id'];
		$result['time_allocated'] = date('H:i',strtotime($data['next_avl_cal_time']));
		$result['time_end'] = date('H:i',strtotime('+' . $timeForSearch . ' minutes',strtotime($result['time_allocated'])));

		return $result;
	 
	}   
        
    /**
     * This action is used to check the valiablity of the kiosk machine
     * @param array
     * @return array
     * @author Icreon Tech-AP
     */
    public function checkAvaliableKioskAction() {
        
        $this->getConfig();
        $this->getTables();
        $kioskReservationObj = new KioskReservation($this->_adapter);
        $request = $this->getRequest();

        $viewModel = new ViewModel();
        $response = $this->getResponse();
        $saveParam = array();
        $contactId = "";
            $contactDetails = 0;

            $param['curr_date'] = date("Y-m-d", strtotime(DATE_TIME_FORMAT));
            $param['curr_time'] = date("H:i:s", strtotime(DATE_TIME_FORMAT));
            $param['auto_reservation'] = '1';
            $param['is_delete'] = '0';
            $filterParams = $kioskReservationObj->getAutoReservationParamArr($param);

            $result = $this->getNextKioskAutoReservation($filterParams); // get kiosk id
			if (empty($result)) {
                    $messages = array('status' => "error", 'message' => "Time Slot is not available.");
                    return $response->setContent(\Zend\Json\Json::encode($messages));
                }else {
                    $messages = array('status' => "success", 'message' => "success");
                    return $response->setContent(\Zend\Json\Json::encode($messages));                
            }
    }
}