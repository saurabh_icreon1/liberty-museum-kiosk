<?php

/**
 * This controller is used for kiosk management
 * @package    Kiosk
 * @author     Icreon Tech - SK
 */

namespace Kiosk\Controller;

use Base\Model\UploadHandler;
use Authorize\lib\AuthnetXML;
use Base\Controller\BaseController;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Zend\Session\Container as SessionContainer;
use Zend\View\Model\ViewModel;
use Kiosk\Form\KioskForm;
use Kiosk\Form\KioskSearchForm;
use Kiosk\Form\KioskLogin;
use Kiosk\Model\KioskManagement;
use Kiosk\Model\KioskManagementTable;
use Kiosk\Model\KioskReservation;
use Zend\Mvc\Controller\Plugin\Params;
use Zend\Http\Request;
use Kiosk\Model\PosManagement;


/**
 * This controller is used to display user detail
 * @package    User
 * @author     Icreon Tech - DG
 */
class KioskController extends BaseController {

    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_authservice = null;
    protected $_storage = null;
    protected $_kioskTable = null;

    //protected $auth = null;

    public function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        $auth = new AuthenticationService();
        $this->_flashMessage = $this->flashMessenger();
        $this->auth = $auth;
    }

    public function indexAction() {
        asd("kiosk index action");
    }

    /**
     * This function is used to get auth object
     * @return     array 
     * @param array
     * @author  Icreon Tech - DG
     */
    public function getAuthServiceByEmail() {
        if (!$this->_authservice) {
            $this->_authservice = $this->getServiceLocator()
                    ->get('AuthServiceByEmail');
        }
        return $this->_authservice;
    }    
    
    /**
     * This functikon is used to get table connections
     * @return     array 
     * @author Icreon Tech - DG
     */
    public function getTables() {
        $sm = $this->getServiceLocator();
        $this->_kioskTable = $sm->get('Kiosk\Model\KioskManagementTable');
        $this->_adapter = $sm->get('dbAdapter');
        $this->_config = $sm->get('Config');
        return $this->_kioskTable;
    }

    /**
     * This functikon is used to get config variables
     * @return     array 
     * @author Icreon Tech - DG
     */
    public function getConfig() {
        $sm = $this->getServiceLocator();
        $this->_translator = $sm->get('translator');
        $this->_config = $sm->get('Config');
    }

    /**
     * This function is used to get auth object
     * @return     array 
     * @param array
     * @author  Icreon Tech - DG
     */
    public function getSessionStorage() {
        if (!$this->_storage) {
            $this->_storage = $this->getServiceLocator()
                    ->get('Kiosk\Model\AuthStorage');
        }
        return $this->_storage;
    }
    /**
     * This function is used to get kiosks
     * @return     array 
     * @param array
     * @author  Icreon Tech - DG
     */
    public function kioskAction() {
        //asd("kiosk action");
        $this->checkUserAuthentication();
        $this->getConfig();
        //$this->getTables();
        $this->layout('crm');
        $kioskForm = new KioskSearchForm();
        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'kioskForm' => $kioskForm,
            'messages' => $messages,
            'jsLangTranslate' => $this->_config['kiosk_messages']['config']['kiosk_kiosk-management']
        ));
        return $viewModel;
        
    }
    /**
     * This action is used to search crm users 
     * @param void
     * @return json
     * @author Icreon Tech - AP
     */
    public function getSearchKioskAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        parse_str($request->getPost('searchString'), $searchParam);
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        $crmMessages = $this->_config['kiosk_messages']['config']['kisok-create-kiosk'];
        $searchParam['name_or_ip'] = isset($searchParam['name_or_ip']) ? $searchParam['name_or_ip'] : '';
        $searchParam['status'] = $searchParam['status'];

        $isExport = $request->getPost('export');
        if ($isExport != "" && $isExport == "excel") {
            $chunksize = $this->_config['export']['chunk_size'];
            $export_limit = $this->_config['export']['export_limit'];
			$searchParam['recordLimit'] = $chunksize;
        }

        $searchParam['startIndex'] = $start;
        $searchParam['recordLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');
        $searchParam['currentTime'] = DATE_TIME_FORMAT;

        $page_counter = 1;
        $number_of_pages = 1;
        do {
            $searchResult = $this->getTables()->searchKiosk($searchParam);
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $total = $countResult[0]->RecordCount;

            if ($isExport == "excel") {
                $filename = "crm_user_export_" . EXPORT_DATE_TIME_FORMAT . ".csv";
                if ($total > $chunksize) {
                    $number_of_pages = ceil($total / $chunksize);
                    $page_counter++;
                    $start = $chunksize * $page_counter - $chunksize;
                    $searchParam['startIndex'] = $start;
                    $searchParam['page'] = $page_counter;
                } else {
                    $number_of_pages = 1;
                    $page_counter++;
                }
            } else {
                $jsonResult['page'] = $page;
                $jsonResult['records'] = $countResult[0]->RecordCount;
                $jsonResult['total'] = @ceil($countResult[0]->RecordCount / $limit);
            }
        } while ($page_counter <= $number_of_pages && $isExport == "excel");



        if (count($searchResult) > 0) {
            $arrExport = array();
            foreach ($searchResult as $val) {
                //$dateDiff = $val['lastlogin'];

                $arrCell['id'] = $val['kiosk_id'];
                $encrypt_id = $this->encrypt($val['kiosk_id']);
                $view = '<a href="/get-crmuser-detail/' . $encrypt_id . '/' . $this->encrypt('view') . '" class="view-icon"><div class="tooltip">View<span></span></div></a>';

                $edit = '<a href="/get-kiosk-detail/' . $encrypt_id . '/' . $this->encrypt('edit') . '">'.$val['kiosk_name'].'</a>';
          
                if ($isExport == "excel") {
                    $arrExport[] = array("ID" => $val['kiosk_id'], "Name" => (!empty($val['kiosk_name']) ? stripslashes($val['kiosk_name']) : 'N/A'), "Kiosk IP"=>$val['kiosk_ip'],"Status" => $val['is_status'],"Kiosk capacity"=>$val['kiosk_capacity'],
                       "WheelChair"=>$val['is_wheelchair'],"ADA"=>$val['is_ada'],"Kiosk Notes"=>$val['kiosk_notes']);
                }
                $status = (!empty($val['is_status']))?'<img src="img/available.png" alt="Available">':'<img src="img/not-available.png" alt="Not Available">';
                $wheelchair = (!empty($val['is_wheelchair']))?'<img src="img/available.png" alt="Wheelchair Available">':'<img src="img/not-available.png" alt="Wheelchair Not Available">';
                $ada = (!empty($val['is_ada']))?'<img src="img/available.png" alt="ADA Available">':'<img src="img/not-available.png" alt="ADA Not Available">';
                
                $arrCell['cell'] = array($edit,$val['kiosk_ip'], $status, !empty($val['kiosk_capacity']) ? $val['kiosk_capacity'] : 'N/A', $wheelchair,$ada,$val['kiosk_notes']);
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
            if ($isExport == "excel") {
                $this->arrayToCsv($arrExport, $filename, $page_counter);
            }
        } else {
            $jsonResult['rows'] = array();
        }
        if ($isExport == "excel") {
            $this->downloadSendHeaders($filename);
            die;
        } else {
            return $response->setContent(\Zend\Json\Json::encode($jsonResult));
        }
    }
    /**
     * This action is used to add kiosk 
     * @param array
     * @return array
     * @author Icreon Tech-AP
     */
    public function addKioskAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        //$this->layout('crm');
        $this->layout('popup');
        $request = $this->getRequest();
        $kioskForm = new KioskForm();
        $kioskMessages = $this->_config['kiosk_messages']['config']['kisok-create-kiosk'];
        $viewModel = new ViewModel();
        $response = $this->getResponse();
        $kioskManagementObj = new KioskManagement($this->_adapter);
        if ($request->isPost()) {
            $kioskForm->setInputFilter($kioskManagementObj->getInputFilter());
            $kioskForm->setData($request->getPost());
            if (!$kioskForm->isValid()) {
                $errors = $kioskForm->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'savecontact') {
                        foreach ($row as $rower) {
                            $msg [$key] = $kioskMessages[$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $formData = $kioskForm->getData();
                $date = DATE_TIME_FORMAT;
                $formData['add_date'] = $date;
                $formData['added_by'] = $this->auth->getIdentity()->crm_user_id;
                $filterParam = $kioskManagementObj->filterParam($formData);
                $kioskId = $this->getTables()->insertKiosk($filterParam);
                
                //Now Insert Default entry in kiosk assign module
                $kioskModules =  $this->getTables()->getKioskModules();//ALL KIOSK Modules
                $kioskModulesConfigInfo =  $this->getTables()->getKioskModulesConfig();//ALL KIOSK Modules config
                
                $paramModData = array();
                $mappingArr = array(3=>"3",4=>"4",8=>"8");
                $lastInsertIdArr = array();
                foreach($kioskModules as $rowId => $value){
                 $paramModData['kiosk_id'] = $kioskId;
                 $paramModData['kiosk_module_id'] = $value['kiosk_module_id'];
                 $paramModData['added_by'] = $this->auth->getIdentity()->crm_user_id;
                 $lastInsertId = $this->getTables()->insertKioskModuleInfo($paramModData);
                 $lastInsertIdArr[$value['kiosk_module_id']]=$lastInsertId;
//                 if(array_key_exists($value['kiosk_module_id'],$mappingArr)){
//                    $mappingArr[$value['kiosk_module_id']] = $lastInsertId;
//                 }
                 
                 }
       
                 $paramModConfigData = array();
                 foreach($kioskModulesConfigInfo as $key => $val){
                    $paramModConfigData['kiosk_module_mapping_id'] = isset($lastInsertIdArr[$val['kiosk_module_id']])?$lastInsertIdArr[$val['kiosk_module_id']]:0;
                    $paramModConfigData['kiosk_module_id'] = $val['kiosk_module_id'];
                    $paramModConfigData['kiosk_module_setting_id'] = $val['kiosk_module_setting_id'];
                    $paramModConfigData['added_by'] = $this->auth->getIdentity()->crm_user_id;
                    $this->getTables()->insertKioskModuleConfigInfo($paramModConfigData);
                 }
                 
                
                if ($kioskId) {
                    $data['activity'] = '1';
                    $data['id'] = $kioskId;
                    $this->changeLog($data);
                    
                    $this->flashMessenger()->addMessage($kioskMessages['ADD_KIOSK_SUCCESS']);

                    $messages = array(
                        'status' => "success",
                        'message' => $kioskMessages['ADD_KIOSK_SUCCESS']
                    );
                } else {
                    $this->flashMessenger()->addMessage($kioskMessages['ADD_KIOSK_FAIL']);

                    $messages = array(
                        'status' => "error",
                        'message' => $kioskMessages['ADD_KIOSK_FAIL']
                    );
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $flashMessages = array();
        if ($this->_flashMessage->hasMessages()) {
            $flashMessages = $this->_flashMessage->getMessages();
        }
        $viewModel->setVariables(array(
            'form' => $kioskForm,
            'jsLangTranslate' => $kioskMessages,
            'messages' => $flashMessages
        ));
        return $viewModel;
    }
    /**
     * This action is used to show all crm user detail
     * @return     array
     * @author Icreon Tech - AP
     */
    public function getKioskDetailAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getConfig();
        $this->getTables();
        //$crmUserMsg = array_merge($this->_config['user_messages']['config']['common'], $this->_config['user_messages']['config']['crm-create-user']);
        $kioskMessages = $this->_config['kiosk_messages']['config']['kisok-create-kiosk'];
        $params = $this->params()->fromRoute();
        $searchParam['kiosk_id'] = $this->decrypt($params['id']);
        
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'kiosk_id' => $searchParam['kiosk_id'],
            'mode' => $this->decrypt($params['mode']),
            'id' => $params['id'],
            'moduleName' => $this->encrypt('kiosk'),
            'jsLangTranslate' => $kioskMessages
        ));
        return $viewModel;
    }
    /**
     * This function is used to edit kiosk
     * @return     array 
     * @param array
     * @author  Icreon Tech - DG
     */
  
    public function editAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        //$crmMessages = array_merge($this->_config['user_messages']['config']['common'], $this->_config['file_upload_path'], $this->_config['user_messages']['config']['crm-create-user']);
        $kioskMessages = $this->_config['kiosk_messages']['config']['kisok-create-kiosk'];
        $applicationConfigIds = $this->_config['application_config_ids'];
        $uSId = $applicationConfigIds['united_state_country_id'];
        $param = $this->params()->fromRoute();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $response = $this->getResponse();
        $messages = array();
        $kioskId = "";
        $form = new KioskForm();
        $kioskManagementObj = new KioskManagement($this->_adapter);

        if (isset($param['id']) && !empty($param['id'])) {
            $param['id'];
            $searchParam['kiosk_id'] = $this->decrypt($param['id']);
            $kioskId = $this->decrypt($param['id']);
            if ($searchParam['kiosk_id'] == '') {
                return $this->redirect()->toRoute('kiosk');
            }
            $searchResult = $this->getTables()->getKioskDetails($searchParam);
            //asd($searchResult);
            if (isset($searchResult[0]['kiosk_name']) && !empty($searchResult[0]['kiosk_name'])) {
                $form->get('kiosk_name')->setAttribute('value', $searchResult[0]['kiosk_name']);
            }
            if (isset($searchResult[0]['kiosk_ip']) && !empty($searchResult[0]['kiosk_ip'])) {
                $form->get('kiosk_ip')->setAttribute('value', $searchResult[0]['kiosk_ip']);
            }
            if (isset($searchResult[0]['is_status']) && !empty($searchResult[0]['is_status'])) {
                $form->get('status')->setAttribute('value', $searchResult[0]['is_status']);
            }
            if (isset($searchResult[0]['kiosk_capacity']) && !empty($searchResult[0]['kiosk_capacity'])) {
                $form->get('kiosk_capacity')->setAttribute('value', $searchResult[0]['kiosk_ip']);
            }
            if (isset($searchResult[0]['is_wheelchair']) && !empty($searchResult[0]['is_wheelchair'])) {
                $form->get('kiosk_wheelchair')->setAttribute('value', $searchResult[0]['is_wheelchair']);
            }
            if (isset($searchResult[0]['is_ada']) && !empty($searchResult[0]['is_ada'])) {
                $form->get('koisk_ads')->setAttribute('value', $searchResult[0]['is_ada']);
            }
            if (isset($searchResult[0]['kiosk_notes']) && !empty($searchResult[0]['kiosk_notes'])) {
                $form->get('kiosk_note')->setAttribute('value', $searchResult[0]['kiosk_notes']);
            }
            if (isset($searchResult[0]['kiosk_shutoff']) && !empty($searchResult[0]['kiosk_shutoff'])) {
                 $form->get('kiosk_shutoff')->setAttribute('value', 0); 
                 $form->get('kiosk_shutoff_time')->setAttribute('value', ''); 
                 $form->get('kiosk_shutoff_time_list')->setAttribute('value', ''); 
                $seppos = strpos($searchResult[0]['kiosk_shutoff'], ',');
                if($seppos){
                   $form->get('kiosk_shutoff')->setAttribute('value', 1); 
                   $timeFormat = explode(",", $searchResult[0]['kiosk_shutoff']);
                   $form->get('kiosk_shutoff_time')->setAttribute('value', $timeFormat[0]); 
                   $form->get('kiosk_shutoff_time_list')->setAttribute('value', $timeFormat[1]); 
                   
                }
                
            }
            if (isset($searchResult[0]['kiosk_id']) && !empty($searchResult[0]['kiosk_id'])) {
                $form->get('kiosk_id')->setAttribute('value', $searchResult[0]['kiosk_id']);
            }
        }
        if ($request->isPost()) {
            $form->setData($request->getPost());
            $form->setInputFilter($kioskManagementObj->getEditFromInputFilter($request->getPost('kiosk_id')));
            if (!$form->isValid()) {
                $errors = $form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'savekiosk') {
                        foreach ($row as $rower) {
                            $msg [$key] = $kioskMessages[$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $formData = $form->getData();
                $date = DATE_TIME_FORMAT;
                $formData['modified_date'] = $date;
                $formData['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                $filterParam = $kioskManagementObj->filterParam($formData);
                $lastUpdatedId = $this->getTables()->updateKiosk($filterParam);
                if ($lastUpdatedId) {
                    $data['activity'] = '2';
                    //$data['id'] = $lastUpdatedId;
                    $data['id'] = $kioskId;
                    $this->changeLog($data);
                    $this->flashMessenger()->addMessage($kioskMessages['UPDATE_KIOSK_SUCCESS']);
                    $messages = array('status' => "success", 'message' => $kioskMessages['UPDATE_KIOSK_SUCCESS']);
                } else {
                    $this->flashMessenger()->addMessage($kioskMessages['UPDATE_KIOSK_FAIL']);
                    $messages = array('status' => "error", 'message' => $kioskMessages['UPDATE_KIOSK_FAIL']);
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }

        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'form' => $form,
            'jsLangTranslate' => array_merge($kioskMessages, $applicationConfigIds),
            'kiosk_id' => $searchParam['kiosk_id'],
            'searchResult' => $searchResult
        ));
        return $viewModel;
    }
     /**
     * This function is used to view kiosk
     * @return array 
     * @param @id
     * @author  Icreon Tech - DG
     */
    public function viewAction($param = array()) {
       
    }
    /**
     * This function is used to delete kiosk
     * @return boolean 
     * @param @id
     * @author  Icreon Tech - DG
     */
    public function deleteAction($param = array()) {
       $this->checkUserAuthentication();
        $this->getTables();
        $this->getConfig();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam['kiosk_id'] = $request->getPost('kioks_id');
            $this->getTables()->deleteKiosk($dataParam);
            $data['activity'] = '3';
            $data['id'] = $request->getPost('kioks_id');
            $this->changeLog($data);
            $this->flashMessenger()->addMessage($this->_config['kiosk_messages']['config']['kisok-create-kiosk']['RECORD_DELETED']);
            $messages = array('status' => "success", 'message' => $this->_config['kiosk_messages']['config']['kisok-create-kiosk']['RECORD_DELETED']);
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }
    /**
     * This action is used to get crm user details
     * @param void
     * @return json
     * @author Icreon Tech - AP
     */
    public function getKioskInfoAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $param = $this->params()->fromRoute();
        $searchParam['kiosk_id'] = $this->decrypt($param['id']);
        if ($searchParam['kiosk_id'] == '') {
            return $this->redirect()->toRoute('kiosk');
        }
        $searchResult = $this->getTables()->getKioskDetails($searchParam);
        $seppos = strpos($searchResult[0]['kiosk_shutoff'], ',');
        $timeFormat = 0;
        if($seppos){
                   $timeFormat = explode(",", $searchResult[0]['kiosk_shutoff']);
         }
              
            
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            //'campaign_id' => $param['id'],
            'kiosk_id' => $searchParam['kiosk_id'],
            'searchResult' => $searchResult[0],
            'jsLangTranslate' => $kioskMessages = $this->_config['kiosk_messages']['config']['kisok-create-kiosk'],
            'kioskShutoff'=>$timeFormat,
           
        ));
        return $viewModel;
    }
    /**
     * This function is used to insert into the change log
     * @param  array
     * @return void
     * @author Icreon Tech - AP
     */
    public function changeLOg($param = array()) {
        $changeLogArray = array();
        $changeLogArray['table_name'] = 'tbl_ksk_kisok_change_logs';
        $changeLogArray['activity'] = $param['activity'];/** 1 == for insert,2 == for update, 3 == for delete */
        $changeLogArray['id'] = $param['id'];
        $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
        $changeLogArray['added_date'] = DATE_TIME_FORMAT;
        $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
    }
    
    /**
     * This function is used for pos login
     * @return array 
     * @param array
     * @author Icreon Tech - SR
     */
    public function kioskLoginAction() {
       //error_reporting(E_ALL);
        $this->getConfig();
        $this->getTables();
     //   $this->layout('kiosk');
         $this->layout()->setVariable('pauseRefreshTime', $this->_config['ajax_refresh_time']['pauseRefreshTime']);
         $this->layout()->setVariable('systemRefreshTime', $this->_config['ajax_refresh_time']['systemRefreshTime']);
        $modulePassengerStatus = $this->checkIsModuleActive(4); // 4 =  passenger search
        $moduleWohStatus = $this->checkIsModuleActive(8); // 8 =  woh  
        $moduleStatus = $this->checkIsModuleActive(1); // 1 =  login

        if ($moduleStatus == 0 && ($modulePassengerStatus == 0 && $moduleWohStatus == 0)) { 
            header('Location: ' . SITE_URL);
            die;
        }        
        $reqParam['ip_address'] = $_SERVER['REMOTE_ADDR'];
        $curDate = DATE_TIME_FORMAT;
        $reqParam['login_time'] = date('H:i:s', strtotime($curDate));
        $kioskResult = $this->getTables()->getKioskNextLoginTime($reqParam);
        $kiosk_next_login_time="";
        if(isset($kioskResult[0]['time_started']))
        {
            $kiosk_next_login_time=$kioskResult[0]['time_started'];
        } 
        
        $kioskLogin = new KioskLogin();
        $request = $this->getRequest();

        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $groupId = 0;
            $form_data = $request->getPost();
            $curDate = DATE_TIME_FORMAT;
            $loginTime = date('H:i:s', strtotime($curDate));
            $searchParam = array();
            $searchParam['ip_address'] = $_SERVER['REMOTE_ADDR'];
            $kioskId = '';
            $user_id = '';
            $kioskResult = $this->getTables()->getLoginKioskId($searchParam);
            
            if ($kioskResult) {
                $kioskId = $kioskResult[0]['kiosk_id'];
                $kioskName = $kioskResult[0]['kiosk_name'];
            }
            $loginParam = array();
            $loginParam['confirmation_code'] = $form_data['confirmation_code'];
            $loginParam['email_id'] = $form_data['email_id'];
            $loginParam['login_time'] = $loginTime;
            $loginParam['kiosk_id'] = $kioskId;
            
            $reservationArray = $this->getTables()->checkValidLogin($loginParam);

            if (!empty($reservationArray)) {
                if ($reservationArray[0]['contact_id'] != '')
                    $userDataArr = $this->getTables()->getUser(array('user_id' => $reservationArray[0]['contact_id']));
                if ($userDataArr) {
                    $userDataArr['kiosk_end_time'] = $reservationArray[0]['time_end'];
                    $userDataArr['kiosk_id'] = $reservationArray[0]['kiosk_id'];
					$userDataArr['queue_id'] = $reservationArray[0]['queue_id'];
					
					$userDataArr['first_name'] = $reservationArray[0]['first_name'];
					$userDataArr['last_name'] = $reservationArray[0]['last_name'];
					$userDataArr['full_name'] = trim($reservationArray[0]['first_name'].' '.$reservationArray[0]['last_name']);
					
                    $this->getAuthServiceByEmail()->getStorage()->write((object)$userDataArr);
                }
                $this->kioskSession = new SessionContainer('kioskSession');
                $this->kioskSession->time_end = $reservationArray[0]['time_end'];
				$this->kioskSession->queue_id = $reservationArray[0]['queue_id'];
                
                $updateparam['queue_id'] = $reservationArray[0]['queue_id'];
                $updateparam['is_delete'] = 0;
                $updateparam['time_started'] = $loginTime;
                $updateparam['contact_id'] = $reservationArray[0]['contact_id'];
				$updateparam['pause_resume'] = 1;
                $kioskReservationObj = new KioskReservation($this->_adapter);
                $filterParams = $kioskReservationObj->getReservationUpdateDataArr($updateparam);
                $this->getServiceLocator()->get('Kiosk\Model\KioskReservationTable')->updateReservationInfo($filterParams);
                $active = 1;
                $login_state = 'success';
				$this->insertUserLogins($reservationArray[0]['contact_id'], $loginParam['confirmation_code'], $loginParam['email_id'], $active, $login_state);                
                
                
                $messages = array('status' => "success");
            } else {
                    // code to get the valid time and kiosk for the user
                    $searchParam = array();
		            $searchParam['confirmation_code'] = $form_data['confirmation_code'];
		            $searchParam['email_id'] = $form_data['email_id'];
                    
                    $kioskResult = $this->getTables()->getUserKioskDetails($searchParam);
                    if(!empty($kioskResult) && count($kioskResult)>0) {
                        $todayDateArray = explode(' ',DATE_TIME_FORMAT);
                        $todayDate = $todayDateArray[0];
                        $startTime = $todayDate.' '.$kioskResult[0]['time_allocated'];
                        $startTime = (!empty($startTime) && $kioskResult[0]['time_allocated'] !="00:00:00")? date('h:i A',strtotime($this->OutputDateFormat($startTime, 'dateformatampm'))):"";
                        
                        $endTime = $todayDate.' '.$kioskResult[0]['time_end'];
                        $endTime = (!empty($endTime) && $endTime !="00:00:00")? date('h:i A',strtotime($this->OutputDateFormat($endTime, 'dateformatampm'))):"";
                        
                        $fullEndTime = (!empty($endTime) && $kioskResult[0]['time_end']!="00:00:00")? date('H:i',strtotime($this->OutputDateFormat($todayDate.' '.$kioskResult[0]['time_end'], 'dateformatampm'))):"";
                        $currentTime = date('H:i',strtotime($this->OutputDateFormat(DATE_TIME_FORMAT, 'dateformatampm')));
                        
                        if(strtotime($fullEndTime)<strtotime($currentTime)) {
                            $messages = array('status' => "error", "message" => "Your session has ended. If you need help, please ask for assistance.");                      
                        }
                        else if($kioskResult['0']['pause_resume'] == 0 || trim($kioskResult['0']['pause_resume']) == '') {
                            $messages = array('status' => "error", "message" => "Your kiosk station has been paused. If you need help, please ask for assistance.");       
                        }
                        else if($kioskResult['0']['pause_resume'] == 1) {
                            if($kioskId!=$kioskResult[0]['kiosk_id'])
                            {
                                $messages = array('status' => "error", "message" => "You are sitting at ".$kioskName.". Your search kiosk is scheduled for ".$kioskResult[0]['kiosk_name']." and your time slot is from ".$startTime." to ".$endTime.". If you need help, please ask for assistance.");                                
                            }
                            else {
                                $messages = array('status' => "error", "message" => "Your time slot is from ".$startTime." to ".$endTime.". If you need help, please ask for assistance.");
                            }
                        }
                            
                        
                    }
                    else {        
                        $messages = array('status' => "error", "message" => "Please enter valid details. If you need help, please ask for assistance.");
                    }
            }
            $response = $this->getResponse();
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'kioskLogin' => $kioskLogin,
            'jsLangTranslate' => $this->_config['kiosk_messages']['config']['kiosk_kiosk-management'],
            'kiosk_next_login_time' => $kiosk_next_login_time
        ));
        return $viewModel;
    }
    
     /**
     * This function ised for search kiosk
     * @return array 
     * @param array
     * @author Icreon Tech - SR
     */   
    public function kioskSearchAction() {       
        $this->getConfig();
        $this->getTables();
		$this->layout()->setVariable('pauseRefreshTime', $this->_config['ajax_refresh_time']['pauseRefreshTime']);
        $this->layout()->setVariable('systemRefreshTime', $this->_config['ajax_refresh_time']['systemRefreshTime']);
        
        $searchParam = array();
        $searchParam['currentTime'] = DATE_TIME_FORMAT;
        $searchParam['status'] = '1';
        $kioskResult = $this->getServiceLocator()->get('Kiosk\Model\KioskManagementTable')->searchKiosk($searchParam);        
			
			foreach ($kioskResult as $key => $val) {
	            $kioskIPArray[] = $val['kiosk_ip'];
	        }
	        $pageLayoutFlag = '';
	        
	        $passengerModuleStatus = 0;
	        $shopModuleStatus = 0;
	        $modulePromoStatus = 0;
	        $moduleEllisMapStatus = 0;
	        if (in_array($_SERVER['REMOTE_ADDR'], $kioskIPArray)) {
	            $kioskModuleParam = array();
	            $kioskModuleParam['name_or_ip'] = $_SERVER['REMOTE_ADDR'];
	            $kioskModuleResult = $this->getServiceLocator()->get('Kiosk\Model\KioskModuleTable')->searchKioskModule($kioskModuleParam);
	
	            $moduleArray = explode(',', $kioskModuleResult['0']['modules']);
	            $moduleStatusArray = explode(',', $kioskModuleResult['0']['module_status']);
	            $moduleKey = '';   
	            $moduleId = 4; //  4 =  passenger search
	            if(in_array(4,$moduleArray)) {
		            $moduleKey = array_search($moduleId, $moduleArray);
		            if ($moduleStatusArray[$moduleKey] == 1)
		                $passengerModuleStatus = 1;
	            }
	            
	            $moduleId = 9;    // 9 =  gift shop
	            if(in_array(9,$moduleArray)) {
		            $moduleKey = array_search($moduleId, $moduleArray);
		            if ($moduleStatusArray[$moduleKey] == 1)
		                $shopModuleStatus = 1;
	             }
	            
	            /*$moduleKey = '';   
	            $moduleId = 8;    // 8 =  WOH
	            if(in_array(8,$moduleArray)) {
		            $moduleKey = array_search($moduleId, $moduleArray);
		            if ($moduleStatusArray[$moduleKey] == 1)
		                $wohModuleStatus = 1;
	             }
	             
	            $moduleKey = '';    
	            $moduleId = 12;   // 12 =  Promo  
	            if(in_array(12,$moduleArray)) {
		            $moduleKey = array_search($moduleId, $moduleArray);
		            if ($moduleStatusArray[$moduleKey] == 1)
		                $modulePromoStatus = 1;
	            }
	            $moduleKey = '';   
	            $moduleId = 11;    // 11 =  Ellis Map
	            if(in_array(11,$moduleArray)) {
		            $moduleKey = array_search($moduleId, $moduleArray);
		            if ($moduleStatusArray[$moduleKey] == 1)
		                $moduleEllisMapStatus = 1;
	            }  */  
	        }
        
       if(empty($this->auth->getIdentity()->user_id)) {
           header('Location: '.SITE_URL.'/kiosk-login');
           exit;
       }
        $assetsPath = $this->_config['file_upload_path']['assets_url'];
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'first_name' => stripslashes($this->auth->getIdentity()->first_name),
            'last_name' => stripslashes($this->auth->getIdentity()->last_name),
            'jsLangTranslate' => $this->_config['kiosk_messages']['config']['kiosk_kiosk-management'],
            'passengerModuleStatus'=>$passengerModuleStatus,
            'wohModuleStatus'=>$wohModuleStatus,
            'modulePromoStatus'=>$modulePromoStatus,
            'moduleEllisMapStatus'=>$moduleEllisMapStatus,
            'shopModuleStatus'=>$shopModuleStatus,
            'assetsPath'=>$assetsPath
            
        ));
        return $viewModel;
    }
    
    
/**
     * This function is used to check if mudule is active or Not.
     * @return     json
     * @author     Icreon Tech - SR 
     */
    public function checkIsModuleActive($moduleId) {
        $pos = new PosManagement($this->_adapter);
        $this->getTables();
        $searchParam = array();
        $searchParam['currentTime'] = DATE_TIME_FORMAT;
        $searchParam['status'] = '1';
        $kioskResult = $this->getServiceLocator()->get('Kiosk\Model\KioskManagementTable')->searchKiosk($searchParam);


        foreach ($kioskResult as $key => $val) {
            $kioskIPArray[] = $val['kiosk_ip'];
        }
        $pageLayoutFlag = '';
        $moduleStatus = 0;
        if (in_array($_SERVER['REMOTE_ADDR'], $kioskIPArray)) {
            $kioskModuleParam = array();
            $kioskModuleParam['name_or_ip'] = $_SERVER['REMOTE_ADDR'];
            $kioskModuleResult = $this->getServiceLocator()->get('Kiosk\Model\KioskModuleTable')->searchKioskModule($kioskModuleParam);

            $moduleArray = explode(',', $kioskModuleResult['0']['modules']);
            $moduleStatusArray = explode(',', $kioskModuleResult['0']['module_status']);
            $visitationKey = array_search($moduleId, $moduleArray);
            if ($moduleStatusArray[$visitationKey] == 1)
                $moduleStatus = 1;
        }
        return $moduleStatus;
    }    
    
	/**
     * This function is used for wall of honor
     * @return     json
     * @author     Icreon Tech - SR 
     */    
    public function userAddWohAction(){
	    $this->getTables();
	    $viewModel = new ViewModel();
	    $this->layout('wohlayout');
        //$wohModuleStatus = $this->checkIsModuleActive(8); // 8 =  WOH
        $wohModuleStatus = 0;
        $addNameOnWall = 0;
        $searchParam = array();
        $searchParam['currentTime'] = DATE_TIME_FORMAT;
        $searchParam['status'] = '1';
        $kioskResult = $this->getServiceLocator()->get('Kiosk\Model\KioskManagementTable')->searchKiosk($searchParam);    
		
        foreach($kioskResult as $key=>$val){
            $kioskIPArray[] = $val['kiosk_ip'];
        }    
		if(in_array($_SERVER['REMOTE_ADDR'], $kioskIPArray)) {
            $kioskModuleParam = array();
            $kioskModuleParam['name_or_ip'] = $_SERVER['REMOTE_ADDR'];
            $kioskModuleResult = $this->getServiceLocator()->get('Kiosk\Model\KioskModuleTable')->searchKioskModule($kioskModuleParam);

            $moduleArray = explode(',', $kioskModuleResult['0']['modules']);
            $moduleStatusArray = explode(',', $kioskModuleResult['0']['module_status']);
			
            $wohKey = array_search('8', $moduleArray); // 8 = WOH;
            if($moduleStatusArray[$wohKey] == 1)
                $wohModuleStatus = 1;
                
            $kioskSubModuleParam = array();    
            $kioskKey = array_search($_SERVER['REMOTE_ADDR'], $kioskIPArray); // 8 = WOH;
            
            $kioskSubModuleParam['kiosk_id'] = $kioskResult[$kioskKey]['kiosk_id'];
            $kioskSubModuleParam['module_id'] = 8; // WOH 
            $kioskSubModuleParam['sub_module_id'] = 6; // Add name
            $kioskSubModuleResult = $this->getServiceLocator()->get('Kiosk\Model\KioskModuleTable')->searchKioskSubModule($kioskSubModuleParam);    
            if(!empty($kioskSubModuleResult))
                $addNameOnWall = $kioskSubModuleResult[0]['setting_value'];    
            /* check if Add name on wall is active or not. */    
        }    

    if ($wohModuleStatus == 0) { 
        header('Location: ' . SITE_URL);
        die;
    } 
            
        $viewModel->setVariables(array(
            'wohModuleStatus'=>$wohModuleStatus,
            'addNameOnWall'=>$addNameOnWall
        ));
        return $viewModel;
    }
    
    /**
     * This function is used to store user login logs
     * @param array Array
     * @author Icreon Tech - DG
     */
    public function insertUserLogins($user_id, $confirmation_code, $email_id, $active, $login_state) {
        $loginsArr['user_id'] = $user_id;
        $userDataArr = $this->getTables()->getUser(array('user_id' => $user_id));

        if(!empty($userDataArr['email_id']))
            $loginsArr['email_id'] = $userDataArr['email_id'];
        else if(!empty($userDataArr['username']))
            $loginsArr['email_id'] = $userDataArr['username'];    

        $loginsArr['login_date'] = DATE_TIME_FORMAT;
        $loginsArr['active'] = $active;
        $loginsArr['ip'] = $_SERVER['REMOTE_ADDR'];
        $loginsArr['last_update_date'] = DATE_TIME_FORMAT;
        $loginsArr['login_state'] = $login_state;
        $loginsArr['source_id'] = $this->_config['transaction_source']['transaction_source_id'];
        $this->getTables()->insertLogins($loginsArr);
        
    }    
    public function kioskNextLoginSessionAction() {
        
        $this->getConfig();
        $this->getTables();
        $this->layout()->setVariable('pauseRefreshTime', $this->_config['ajax_refresh_time']['pauseRefreshTime']);
        $this->layout()->setVariable('systemRefreshTime', $this->_config['ajax_refresh_time']['systemRefreshTime']);
        $modulePassengerStatus = $this->checkIsModuleActive(4); // 4 =  passenger search
        $moduleWohStatus = $this->checkIsModuleActive(8); // 8 =  woh  
        $moduleStatus = $this->checkIsModuleActive(1); // 1 =  login

        if ($moduleStatus == 0 && ($modulePassengerStatus == 0 && $moduleWohStatus == 0)) { 
            header('Location: ' . SITE_URL);
            die;
        }
        $reqParam['ip_address'] = $_SERVER['REMOTE_ADDR'];
        $curDate = DATE_TIME_FORMAT;
        $reqParam['login_time'] = date('H:i:s', strtotime($curDate));
        $kioskResult = $this->getTables()->getKioskNextLoginTime($reqParam);
        $loginTimeLeft = 0;
        $isloginSessionAvailable = false;
        if(isset($kioskResult[0]['time_started']))
        {
            $isloginSessionAvailable = true;
            $now = date("H:i:s");
            $timeDiff = strtotime($kioskResult[0]['time_started']) - strtotime($now);
            if($timeDiff > 0)
            {
                $loginTimeLeft = ceil($timeDiff);
            }
        }
        $messages = array('status' => "success", "isloginSessionAvailable" => $isloginSessionAvailable,"loginTimeLeft"=>$loginTimeLeft); 
        $response = $this->getResponse();
        return $response->setContent(\Zend\Json\Json::encode($messages));
    }
}