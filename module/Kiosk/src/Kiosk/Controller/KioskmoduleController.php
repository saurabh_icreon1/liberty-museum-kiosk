<?php

/**
 * This controller is used for kiosk management
 * @package    Kiosk
 * @author     Icreon Tech - SK
 */

namespace Kiosk\Controller;

use Base\Model\UploadHandler;
use Authorize\lib\AuthnetXML;
use Base\Controller\BaseController;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Zend\Session\Container as SessionContainer;
use Zend\View\Model\ViewModel;
use Kiosk\Form\KioskModuleForm;
use Kiosk\Form\KioskModuleSearchForm;
use Kiosk\Model\KioskModule;
use Kiosk\Model\KioskModuleTable;
use Zend\Mvc\Controller\Plugin\Params;
use Zend\Http\Request;


/**
 * This controller is used to display user detail
 * @package    User
 * @author     Icreon Tech - DG
 */
class KioskmoduleController extends BaseController {

    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_authservice = null;
    protected $_storage = null;
    protected $_kioskTable = null;

    //protected $auth = null;

    public function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        $auth = new AuthenticationService();
        $this->_flashMessage = $this->flashMessenger();
        $this->auth = $auth;
    }

    public function indexAction() {
        asd("kiosk index action");
    }

    /**
     * This functikon is used to get table connections
     * @return     array 
     * @author Icreon Tech - DG
     */
    public function getTables() {
        $sm = $this->getServiceLocator();
        $this->_kioskTable = $sm->get('Kiosk\Model\KioskModuleTable');
        $this->_adapter = $sm->get('dbAdapter');
        $this->_config = $sm->get('Config');
        return $this->_kioskTable;
    }

    /**
     * This functikon is used to get config variables
     * @return     array 
     * @author Icreon Tech - DG
     */
    public function getConfig() {
        $sm = $this->getServiceLocator();
        $this->_translator = $sm->get('translator');
        $this->_config = $sm->get('Config');
    }

    /**
     * This function is used to get auth object
     * @return     array 
     * @param array
     * @author  Icreon Tech - DG
     */
    public function getSessionStorage() {
        if (!$this->_storage) {
            $this->_storage = $this->getServiceLocator()
                    ->get('Kiosk\Model\AuthStorage');
        }
        return $this->_storage;
    }
    /**
     * This function is used to get kiosks
     * @return     array 
     * @param array
     * @author  Icreon Tech - DG
     */
    public function kioskModuleAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        //$this->getTables();
        $this->layout('crm');
        $kioskModuleForm = new KioskModuleSearchForm();
        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        $colName = "'Kiosk Name','Status','Shutoff Time',";
        $colModelName = "{name:'kiosk_name',index:'kiosk_name',sortable:true},{name:'is_status',index:'is_status',sortable:false},{name:'kiosk_shutoff',index:'kiosk_shutoff'},";
        $kioskModules =  $this->getTables()->getKioskModules();
        foreach($kioskModules as $modulesName){
             $wdith = 100;
            if($modulesName['kiosk_module_id'] == 4)
                $wdith = 150;
            
            $modulesName = $modulesName['kiosk_module_name'];
            $colName .= "'$modulesName'".",";
            $colModelName .="{"
                    ."name:'$modulesName'".","
                    ."index:'$modulesName'".","
                    ."sortable:'false'".","
                    ."width:'$wdith'".","
                    ."},";
        }
        $colName = trim($colName,',');
        $colModelName =  trim($colModelName,',');
        
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'kioskForm' => $kioskModuleForm,
            'messages' => $messages,
            'colName'=>$colName,
            'colModelNames'=>$colModelName,
            'jsLangTranslate' => $this->_config['kiosk_messages']['config']['kiosk-module-management'],
            //'jsLangTranslate' => $this->_config['kiosk_messages']['config']['kiosk_kiosk-management']
        ));
        return $viewModel;
        
    }
    /**
     * This action is used to search Kiosk 
     * @param void
     * @return json
     * @author Icreon Tech - AP
     */
    public function getSearchKioskModuleAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        parse_str($request->getPost('searchString'), $searchParam);
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        $crmMessages = $this->_config['kiosk_messages']['config']['kiosk-module-management'];
        $searchParam['name_or_ip'] = isset($searchParam['name_or_ip']) ? $searchParam['name_or_ip'] : '';
        $searchParam['status'] = $searchParam['status'];
        //$country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
        $kioskModules =  $this->getTables()->getKioskModules();
        $isExport = $request->getPost('export');
        if ($isExport != "" && $isExport == "excel") {
            $chunksize = $this->_config['export']['chunk_size'];
            $export_limit = $this->_config['export']['export_limit'];
			$searchParam['recordLimit'] = $chunksize;
        }

        $searchParam['startIndex'] = $start;
        $searchParam['recordLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');
        $searchParam['currentTime'] = DATE_TIME_FORMAT;

        $page_counter = 1;
        $number_of_pages = 1;
        do {
            $searchResult = $this->getTables()->searchKioskModule($searchParam);
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $total = $countResult[0]->RecordCount;

            if ($isExport == "excel") {
                $filename = "crm_user_export_" . EXPORT_DATE_TIME_FORMAT . ".csv";
                if ($total > $chunksize) {
                    $number_of_pages = ceil($total / $chunksize);
                    $page_counter++;
                    $start = $chunksize * $page_counter - $chunksize;
                    $searchParam['startIndex'] = $start;
                    $searchParam['page'] = $page_counter;
                } else {
                    $number_of_pages = 1;
                    $page_counter++;
                }
            } else {
                $jsonResult['page'] = $page;
                $jsonResult['records'] = $countResult[0]->RecordCount;
                $jsonResult['total'] = @ceil($countResult[0]->RecordCount / $limit);
            }
        } while ($page_counter <= $number_of_pages && $isExport == "excel");



        if (count($searchResult) > 0) {
            $arrExport = array();
            foreach ($searchResult as $val) {
                //$dateDiff = $val['lastlogin'];

                $arrCell['id'] = $val['kiosk_id'];
                $encrypt_id = $this->encrypt($val['kiosk_id']);
                $view = '<a href="/get-crmuser-detail/' . $encrypt_id . '/' . $this->encrypt('view') . '" class="view-icon"><div class="tooltip">View<span></span></div></a>';

                $edit = '<a href="/get-kiosk-module-detail/' . $encrypt_id . '/' . $this->encrypt('edit') . '" >'.$val['kiosk_name'].'</a>';
          
                if ($isExport == "excel") {
                    $arrExport[] = array("ID" => $val['kiosk_id'], "Name" => (!empty($val['kiosk_name']) ? stripslashes($val['kiosk_name']) : 'N/A'), "Kiosk IP"=>$val['kiosk_ip'],"Status" => $val['is_status'],"Kiosk capacity"=>$val['kiosk_capacity'],
                       "WheelChair"=>$val['is_wheelchair'],"ADA"=>$val['is_ada'],"Kiosk Notes"=>$val['kiosk_notes']);
                }
                $timeFormatArr = array(1=>"AM",2=>"PM");
                $kioskShutoff = "N/A";
                if($val['kiosk_shutoff'] !="" && strpos($val['kiosk_shutoff'],',')){
                    
                    $timeFormat = @explode(",", $val['kiosk_shutoff']);
                    $kioskShutoff = $timeFormat[0]." ".$timeFormatArr[$timeFormat[1]];
                }
                $status = (!empty($val['is_status']))?'<img src="img/available.png" alt="Available">':'<img src="img/not-available.png" alt="Not Available">';
                
                //$assignModuls = $this->getAssignModuleInfo($val['modules'], $val['module_status'],$kioskModules);
//                if($val['kiosk_id'] == 7)
//                asd($val['modules']);
                
                $modules = explode(',',$val['modules']);
                $modulesStatus = explode(',',$val['module_status']);
                $moduleAndModuleStatus = array_combine($modules,$modulesStatus);
                $moduleNameArr = array();
                $i= 0;
                foreach($kioskModules as $key => $val){
        
                          $moduleNameArr['varName'][$i] = @implode('_',explode(' ',$val['kiosk_module_name']));
                          $moduleNameArr['varValue'][$i] = '<img src="img/not-available.png" alt="Not Available">';
                          if(array_key_exists($val['kiosk_module_id'],$moduleAndModuleStatus)){
                                if($moduleAndModuleStatus[$val['kiosk_module_id']]){
                                    $result[$val['kiosk_module_id']] = 1;
                                    $moduleNameArr['varValue'][$i] = '<img src="img/available.png" alt="Available">';
                                }
                                
                           }
                           $i++;
                }
                $defaultValArr = array($edit,$status,$kioskShutoff);
                $arrCell['cell'] = array_merge($defaultValArr,$moduleNameArr['varValue']);
//                        array($edit,$status,'',
//                        $isRegistration,$isReservation,$isPassangerSearch,$isFunFacts,
//                    $isFOFWall,$isFOFSearch,$isWOH,$isGiftShop,$isHelp,$isWayFinding,$isPromo,$isPromo);
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
            if ($isExport == "excel") {
                $this->arrayToCsv($arrExport, $filename, $page_counter);
            }
        } else {
            $jsonResult['rows'] = array();
        }
        if ($isExport == "excel") {
            $this->downloadSendHeaders($filename);
            die;
        } else {
            return $response->setContent(\Zend\Json\Json::encode($jsonResult));
        }
    }
    /**
     * This action is used to add kiosk 
     * @param array
     * @return array
     * @author Icreon Tech-AP
     */
    public function addKioskModuleAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $this->layout('crm');
        $request = $this->getRequest();
        $kioskForm = new KioskForm();
        $kioskMessages = $this->_config['kiosk_messages']['config']['kisok-create-kiosk'];
        $viewModel = new ViewModel();
        $response = $this->getResponse();
        $kioskManagementObj = new KioskManagement($this->_adapter);
        if ($request->isPost()) {
            $kioskForm->setInputFilter($kioskManagementObj->getInputFilter());
            $kioskForm->setData($request->getPost());
            if (!$kioskForm->isValid()) {
                $errors = $kioskForm->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'savecontact') {
                        foreach ($row as $rower) {
                            $msg [$key] = $kioskMessages[$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $formData = $kioskForm->getData();
                $date = DATE_TIME_FORMAT;
                $formData['add_date'] = $date;
                $formData['added_by'] = $this->auth->getIdentity()->crm_user_id;
                $filterParam = $kioskManagementObj->filterParam($formData);
                $kioskId = $this->getTables()->insertKiosk($filterParam);
                if ($kioskId) {
                    $data['activity'] = '1';
                    $data['id'] = $kioskId;
                    $this->changeLog($data);
                    
                    $this->flashMessenger()->addMessage($kioskMessages['ADD_KIOSK_SUCCESS']);

                    $messages = array(
                        'status' => "success",
                        'message' => $kioskMessages['ADD_KIOSK_SUCCESS']
                    );
                } else {
                    $this->flashMessenger()->addMessage($kioskMessages['ADD_KIOSK_FAIL']);

                    $messages = array(
                        'status' => "error",
                        'message' => $kioskMessages['ADD_KIOSK_FAIL']
                    );
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $flashMessages = array();
        if ($this->_flashMessage->hasMessages()) {
            $flashMessages = $this->_flashMessage->getMessages();
        }
        $viewModel->setVariables(array(
            'form' => $kioskForm,
            'jsLangTranslate' => $kioskMessages,
            'messages' => $flashMessages
        ));
        return $viewModel;
    }
    /**
     * This action is used to show kiosk details
     * @return     array
     * @author Icreon Tech - AP
     */
    public function getKioskModuleDetailAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getConfig();
        $this->getTables();
        //$crmUserMsg = array_merge($this->_config['user_messages']['config']['common'], $this->_config['user_messages']['config']['crm-create-user']);
        $kioskMessages = $this->_config['kiosk_messages']['config']['kisok-create-kiosk'];
        $params = $this->params()->fromRoute();
        $searchParam['kiosk_id'] = $this->decrypt($params['id']);
        
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'kiosk_id' => $searchParam['kiosk_id'],
            'mode' => $this->decrypt($params['mode']),
            'id' => $params['id'],
            'moduleName' => $this->encrypt('kiosk'),
            'jsLangTranslate' => $kioskMessages
        ));
        return $viewModel;
    }
    /**
     * This function is used to edit kiosk
     * @return     array 
     * @param array
     * @author  Icreon Tech - DG
     */
  
    public function editKioskAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        //$crmMessages = array_merge($this->_config['user_messages']['config']['common'], $this->_config['file_upload_path'], $this->_config['user_messages']['config']['crm-create-user']);
        $kioskMessages = array_merge($this->_config['kiosk_messages']['config']['kisok-create-kiosk'],$this->_config['kiosk_messages']['config']['kiosk-module-management']);
        $applicationConfigIds = $this->_config['application_config_ids'];
        $uSId = $applicationConfigIds['united_state_country_id'];
        $param = $this->params()->fromRoute();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $response = $this->getResponse();
        $messages = array();
        $timeFormatArr = array(1=>"AM",2=>"PM");
        $form = new KioskModuleForm();
        $kioskModuleObj = new KioskModule($this->_adapter);
        $kioskModules =  $this->getTables()->getKioskModules();
        if (isset($param['id']) && !empty($param['id'])) {
            $param['id'];
            $searchParam['kiosk_id'] = $this->decrypt($param['id']);
            if ($searchParam['kiosk_id'] == '') {
                return $this->redirect()->toRoute('kiosk');
            }
            $searchResult = $this->getTables()->getKioskModuleDetails($searchParam);
            //asd($searchParam);
            //$assignModuls = $this->getAssignModuleInfo($searchResult[0]['modules'],$searchResult[0]['module_status'],$kioskModules);//asd($assignModuls);
            if (isset($searchResult[0]['kiosk_name']) && !empty($searchResult[0]['kiosk_name'])) {
                $form->get('kiosk_name')->setAttribute('value', $searchResult[0]['kiosk_name']);
            }
            if (isset($searchResult[0]['kiosk_shutoff']) && !empty($searchResult[0]['kiosk_shutoff'])) {
                 $form->get('kiosk_shutoff')->setAttribute('value', "Never"); 
                $seppos = strpos($searchResult[0]['kiosk_shutoff'], ',');
                if($seppos){
                   $timeFormat = explode(",", $searchResult[0]['kiosk_shutoff']);
                   $format = $timeFormat[0]." ".$timeFormatArr[$timeFormat[1]];
                   $form->get('kiosk_shutoff')->setAttribute('value', $format); 
                   
                }
                
            }
            if (isset($searchResult[0]['kiosk_id']) && !empty($searchResult[0]['kiosk_id'])) {
                $form->get('kiosk_id')->setAttribute('value', $searchResult[0]['kiosk_id']);
            }
            
//          $response->setContent(\Zend\Json\Json::encode($assignModuls));
//          return $response;
        }
        if ($request->isPost()) {
            $form->setData($request->getPost());
            $form->setInputFilter($kioskModuleObj->getEditFromInputFilter($request->getPost('kiosk_id')));
            if (!$form->isValid()) {
                $errors = $form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'savekiosk') {
                        foreach ($row as $rower) {
                            $msg [$key] = $kioskMessages[$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
//            $msg['']="Invalid Time";
//            $messages = array('status' => "error", 'message' => $msg);
            $response->setContent(\Zend\Json\Json::encode($messages));
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $formData = $form->getData();//asd($formData);
                $date = DATE_TIME_FORMAT;
                $formData['modified_date'] = $date;
                $formData['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                //$this->getTables()->deleteKioskModuleInfo($request->getPost('kiosk_id'));
                //$filterParam = $kioskModuleObj->filterParam($formData);
                $formModules = $request->getPost('modules');
                $formModulesSettings = $request->getPost('module_settings');
                $formAllData = $request->getPost();
                $kioskId = $request->getPost('kiosk_id');
                $paramModData = array();
                foreach($formModulesSettings as $rowId => $value){
                //asd($module);//Auto Resrvation_1
                 $paramModData['kiosk_setting_id'] = $rowId;
                 $paramModData['setting_value'] = $value;
                 $paramModData['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                 $lastUpdatedId = $this->getTables()->updateKioskModuleConfigInfo($paramModData);
                 //$lastUpdatedId = $this->getTables()->insertKioskModuleSettingsInfo($paramData);
                }
                
                //$this->getTables()->deleteKioskModuleInfo($kioskId);
                $kioskModuleMapId = array();
                
                if(!empty($formModules)){
                foreach($formModules as $moduleId => $status){
                    $paramData = array();
                 $kioskModuleMapId = explode('_',$moduleId);
                 if($kioskModuleMapId['0'] =="undefine"){
                      continue;
                 }
                 $paramData['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                 $paramData['kiosk_module_mapping_id'] = (int)$kioskModuleMapId['0'];
                 $paramData['is_available'] = $status;
                 //asd(var_dump($paramData['is_available']));
                 //asd($paramData);
                 //$lastUpdatedId = $this->getTables()->insertKioskModuleInfo($paramData);
                $lastUpdatedId = $this->getTables()->updateKioskModuleInfo($paramData);
                }
                }
                
                if ($lastUpdatedId) {
                    $data['activity'] = '2';
                    $data['id'] = $kioskId;
                    $this->changeLog($data);
                    $this->flashMessenger()->addMessage($kioskMessages['UPDATE_KIOSK_SUCCESS']);
                    $messages = array('status' => "success", 'message' => $kioskMessages['UPDATE_KIOSK_SUCCESS']);
                } else {
                    $this->flashMessenger()->addMessage($kioskMessages['UPDATE_KIOSK_FAIL']);
                    $messages = array('status' => "error", 'message' => $kioskMessages['UPDATE_KIOSK_FAIL']);
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }

        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'form' => $form,
            'jsLangTranslate' => array_merge($kioskMessages, $applicationConfigIds),
            
            //'kiosk_id' => $searchParam['kiosk_id'],
            'searchResult' => $searchResult
        ));
        return $viewModel;
    }
     /**
     * This function is used to view kiosk
     * @return array 
     * @param @id
     * @author  Icreon Tech - DG
     */
    public function viewAction($param = array()) {
       
    }
    /**
     * This function is used to delete kiosk
     * @return boolean 
     * @param @id
     * @author  Icreon Tech - DG
     */
    public function deleteAction($param = array()) {
       $this->checkUserAuthentication();
        $this->getTables();
        $this->getConfig();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam['kiosk_id'] = $request->getPost('kioks_id');
            //$this->getTables()->deleteKiosk($dataParam);
            $data['activity'] = '3';
            $data['id'] = $request->getPost('kioks_id');
            $this->changeLog($data);
            $this->flashMessenger()->addMessage($this->_config['kiosk_messages']['config']['kisok-create-kiosk']['RECORD_DELETED']);
            $messages = array('status' => "success", 'message' => $this->_config['kiosk_messages']['config']['kisok-create-kiosk']['RECORD_DELETED']);
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }
    /**
     * This action is used to get Kiosk Module Details
     * @param void
     * @return json
     * @author Icreon Tech - AP
     */
    public function getKioskModulesInfoAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $param = $this->params()->fromRoute();
        $searchParam['kiosk_id'] = $this->decrypt($param['id']);
        if ($searchParam['kiosk_id'] == '') {
            return $this->redirect()->toRoute('kiosk');
        }
        $searchResult = $this->getTables()->getKioskModuleDetails($searchParam);
         $seppos = strpos($searchResult[0]['kiosk_shutoff'], ',');
        $timeFormat = 0;
        if($seppos){
                   $timeFormat = explode(",", $searchResult[0]['kiosk_shutoff']);
         }
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            //'campaign_id' => $param['id'],
            'kiosk_id' => $searchParam['kiosk_id'],
            'searchResult' => $searchResult[0],
            'jsLangTranslate' => $kioskMessages = $this->_config['kiosk_messages']['config']['kisok-create-kiosk'],
            'kioskShutoff'=>$timeFormat,
           
        ));
        return $viewModel;
    }
    /**
     * This function is used to insert into the change log
     * @param  array
     * @return void
     * @author Icreon Tech - AP
     */
    public function changeLOg($param = array()) {
        $changeLogArray = array();
        $changeLogArray['table_name'] = 'tbl_ksk_kisok_change_logs';
        $changeLogArray['activity'] = $param['activity'];/** 1 == for insert,2 == for update, 3 == for delete */
        $changeLogArray['id'] = $param['id'];
        $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
        $changeLogArray['added_date'] = DATE_TIME_FORMAT;
        $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
    }
    protected function getAssignModuleInfo($currentModule,$moduleStatus,$kioskModules){
        $modules = explode(',',$currentModule);
        $modulesStatus = explode(',',$moduleStatus);
        $moduleAndModuleStatus = array_combine($modules,$modulesStatus);
        
        $result = array();
        foreach($kioskModules as $key => $val){
//            if(in_array($val['kiosk_module_id'],$modules)){
//               $result[$val['kiosk_module_id']] = 1;
//            }
         $result[$val['kiosk_module_id']] = 0;
        if(array_key_exists($val['kiosk_module_id'],$moduleAndModuleStatus)){
            if($moduleAndModuleStatus[$val['kiosk_module_id']]){
                $result[$val['kiosk_module_id']] = 1;
            }
            
           }
        }
        return $result;
    }
     /**
     * This function is used to edit kiosk
     * @return     array 
     * @param array
     * @author  Icreon Tech - DG
     */
  
    public function editKioskDetailsAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        //$crmMessages = array_merge($this->_config['user_messages']['config']['common'], $this->_config['file_upload_path'], $this->_config['user_messages']['config']['crm-create-user']);
        $kioskMessages = array_merge($this->_config['kiosk_messages']['config']['kisok-create-kiosk'],$this->_config['kiosk_messages']['config']['kiosk-module-management']);
        $applicationConfigIds = $this->_config['application_config_ids'];
        $uSId = $applicationConfigIds['united_state_country_id'];
        $param = $this->params()->fromRoute();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $response = $this->getResponse();
        $messages = array();

        $form = new KioskModuleForm();
        $kioskModuleObj = new KioskModule($this->_adapter);
        
        $kioskModules =  $this->getTables()->getKioskModules();//ALL KIOSK Modules
        $kioskModulesConfigInfo =  $this->getTables()->getKioskModulesConfig();//ALL KIOSK Modules
        
        //asd($kioskModules);
        if (isset($param['id']) && !empty($param['id'])) {
            $param['id'];
            $searchParam['kiosk_id'] = $this->decrypt($param['id']);
             $searchResult = $this->getTables()->getKioskModuleDetails($searchParam); // Current KIOSK MODULE settings
             //$dataResult = $this->processModuleData($searchResult);
             $dataResult = $this->processModuleSettings($searchResult);
             foreach($dataResult['kiosk_module_child_info'] as $key => $val){
                 foreach($kioskModulesConfigInfo as $childkey => $childval){
                     if($val['kiosk_module_setting_id'] == $childval['kiosk_module_setting_id'] && $val['kiosk_module_id'] == $childval['kiosk_module_id']){
                         $dataResult['kiosk_module_child_info'][$key]['setting_title_name'] = $childval['setting_title_name'];
                         $dataResult['kiosk_module_child_info'][$key]['setting_name'] =$childval['setting_name'];
                         $dataResult['kiosk_module_child_info'][$key]['setting_value'] =$childval['setting_value'];
                         $dataResult['kiosk_module_child_info'][$key]['setting_display_type'] =$childval['setting_display_type'];
          
                     }
                     
                  }
             }
             //asd($dataResult);
            //$assignModuls = $this->getAssignModuleInfo($searchResult[0]['modules'],$kioskModules);//asd($assignModuls);
           
            //$result = array("kioskModules"=>$dataResult,"Modules"=>$kioskModules);
             //$dataResult['kiosk_module_data'] = array_values($dataResult['kiosk_module_data']);
             $result = array("kioskModules"=>$dataResult);
             
            $response->setContent(\Zend\Json\Json::encode($result));
          return $response;
        }
        
       }
      /**
     * This function is used to  process kioks module data 
     * @return     array 
     * @param array
     * @author  Icreon Tech - DG
     */
       
       protected function processModuleSettings($searchResult){
     
           $kioskData = array();
           $i = 0;
           $k = 0;
           foreach($searchResult as $key => $val){
               if(empty($kioskData['kiosk_data'])){
                   $kioskData['kiosk_data'] = $this->getKioskData($val);
               }
               if(empty($kioskModuleData['kiosk_module_info'][$val['kiosk_module_id']])){
                   $kioskData['kiosk_module_data'][$val['kiosk_module_id']] = $this->getKioskModuleInfo($val);
               }
               if(empty($kioskModuleData['kiosk_module_child_info'][$i])){
                   $kioskData['kiosk_module_child_info'][$i] = $this->getKioskModuleChildInfo($val);
        
               }
               if(empty($kioskModuleData['kiosk_module_data'][$i])){
                   //$kioskData['kiosk_module_data'][$i] = $this->getKioskModuleData($val);
               }
               foreach($kioskData['kiosk_module_child_info'] as $key => $val){
                   if(empty($val)){
                       unset($kioskData['kiosk_module_child_info'][$key]);
                   }
               }
             $i++;  
           }
           return $kioskData;
       }
      /**
     * This function is used to prepare kiosk module status 
     * @return     array 
     * @param array
     * @author  Icreon Tech - DG
     */
       protected function getKioskModuleInfo($data){
           $getKioskDataKeys = array('kiosk_module_name', 'kiosk_module_id','is_available','kiosk_module_mapping_id');
           $kioskData = array();
            foreach ($getKioskDataKeys as $key) {
                $kioskData[$key] = isset($data[$key]) ? $data[$key] : "";
            }
             return $kioskData;
      }
      /**
     * This function is used to prepare kiosk module config elements
     * @return     array 
     * @param array
     * @author  Icreon Tech - DG
     */
      protected function getKioskModuleChildInfo($data){
           $getKioskDataKeys = array('kiosk_module_id','kiosk_setting_id','kiosk_module_setting_id','kiosk_module_setting_value','setting_title_name','setting_name','setting_value','setting_display_type');
           $kioskData = array();
            foreach ($getKioskDataKeys as $key) {
                if(empty($data['kiosk_setting_id']))
                    continue;
                $kioskData[$key] = isset($data[$key]) ? $data[$key] : "";
            }
             return $kioskData;
      }
     /**
     * This function is used to prepare kiosk information
     * @return     array 
     * @param array
     * @author  Icreon Tech - DG
     */
     protected function getKioskData($data)
    {
        $getKioskDataKeys = array('kiosk_name', 'is_status', 'kiosk_shutoff', 'kiosk_id');
        $kioskData = array();
        foreach ($getKioskDataKeys as $key) {
            $kioskData[$key] = isset($data[$key]) ? $data[$key] : "";
        }
        return $kioskData;
    }
    
    protected function getKioskModuleData($data)
    {
        $kioskModuleDataKeys = array('kiosk_module_id', 'kiosk_module_mapping_id','is_available','setting_title_name', 'setting_name', 'setting_value', 'setting_display_type', 'kiosk_module_setting_id','kiosk_module_setting_value');
        $kioskModuleData = array();
        foreach ($kioskModuleDataKeys as $key) {
            $kioskModuleData[$key] = isset($data[$key]) ? $data[$key] : "";
            
        }
        return $kioskModuleData;
    }

}