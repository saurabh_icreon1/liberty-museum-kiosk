<?php

/**
 * This File define all the text label and messages
 * @package    Home
 * @author     Icreon Tech - DG
 */
return array(
    'home_page' => array(
        "H_PASSENGER_RECORD_SEARCH" => "Passenger Record Search",
        "H_FEATURED_STORY" => "Featured Story",
        "H_BECOME_A_MEMBER" => "Become a member",
        "H_VISIT_ELLISLAND" => "Visit The Islands",
    )
);
?>