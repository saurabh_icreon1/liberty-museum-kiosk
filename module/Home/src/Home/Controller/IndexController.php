<?php

/**
 * This controller is used for Site Home Page
 * @package    Home
 * @author     Icreon Tech - DG
 */

namespace Home\Controller;
use Zend\Http\Client as HttpClient;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Base\Controller\BaseController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\AuthenticationService;
use Zend\Session\Container;

/**
 * This controller is used for Site Home Page
 * @package    Home
 * @author     Icreon Tech - DG
 */
class IndexController extends BaseController {

    function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        parent::__construct();
        $auth = new AuthenticationService();
        $this->auth = $auth;
    }

    /**
     * This functikon is used to get config variables
     * @return     array
     * @author Icreon Tech - DG
     */
    public function getConfig() {
        $serviceManager = $this->getServiceLocator();
        $this->_translator = $serviceManager->get('translator');
        $this->_config = $serviceManager->get('Config');
    }

    /**
     * This Action is used to display all content of home page
     * @author Icreon Tech-DG
     * @return Data
     */
    public function indexAction() {

        $this->layout()->setVariable('pauseRefreshTime', $this->_config['ajax_refresh_time']['pauseRefreshTime']);
         $this->layout()->setVariable('systemRefreshTime', $this->_config['ajax_refresh_time']['systemRefreshTime']);
        
        $moduleSession = new Container('moduleSession');
        $moduleSession->moduleGroup = array(); 
        
        $this->getConfig();
        $viewModel = new ViewModel();
        $this->layout()->setVariable('stack', 'overflow');
        $searchParam = array();
        $searchParam['currentTime'] = DATE_TIME_FORMAT;
        $searchParam['status'] = '1';
        $kioskResult = $this->getServiceLocator()->get('Kiosk\Model\KioskManagementTable')->searchKiosk($searchParam);

        $purchaseSession = new Container('purchaseSession');
        $purchaseSession->purchaseSessionData = array();

        $donationSession = new Container('donationSession');
        $donationSession->donationData = array(); 
         
		$posTransactionSession = new Container('posTransactionSession'); /* pos session */
        $posTransactionSession->posTransactionData = array();         

        foreach($kioskResult as $key=>$val){
            $kioskIPArray[] = $val['kiosk_ip'];
        }
        $pageLayoutFlag = '';     
        $visitationModule = 0;
        $purchaseSessionModule = 0;
        $isMachineAssigned = 0;
        $isAnyModuleAssign = 0;

        if(in_array($_SERVER['REMOTE_ADDR'], $kioskIPArray)) {
            $kioskModuleParam = array();
            $kioskModuleParam['name_or_ip'] = $_SERVER['REMOTE_ADDR'];
            $kioskModuleResult = $this->getServiceLocator()->get('Kiosk\Model\KioskModuleTable')->searchKioskModule($kioskModuleParam);

            $moduleArray = explode(',', $kioskModuleResult['0']['modules']);
            $moduleStatusArray = explode(',', $kioskModuleResult['0']['module_status']);
			
			if(in_array('1',$moduleStatusArray)) //if any module is assigned
				$isAnyModuleAssign = 1;
            
            $visitationKey = array_search('15', $moduleArray); // 15 = visitation;
            if($moduleStatusArray[$visitationKey] == 1)
                $visitationModule = 1;
                
            $posKey = array_search('3', $moduleArray); // 3 = purchase session;
            if($moduleStatusArray[$posKey] == 1)
                 $purchaseSessionModule = 1;
                 
            $posKey = array_search('12', $moduleArray); // 12 = Promo;
            if($moduleStatusArray[$posKey] == 1)
                $modulePromoStatus = 1; 
            
            $posKey = array_search('11', $moduleArray); // 11 =  Ellis Map
            if($moduleStatusArray[$posKey] == 1)
                $moduleEllisMapStatus = 1;  
                                               
                                                      
                               
            $wohKey = array_search('8', $moduleArray); // 8 = WOH;
            if($moduleStatusArray[$wohKey] == 1) {
                $wohModule = 1;
                
                $moduleSession = new Container('moduleSession');
                $moduleSession->moduleGroup = 3;             
           
            }
                                               
           
            if(count($kioskModuleResult) >0 ) {
                $kioskModuleArray = explode(',', $kioskModuleResult[0]['modules']);
                $kioskModuleStatusArray = explode(',', $kioskModuleResult[0]['module_status']);
                $moduleKey = array_search('3',$kioskModuleArray); // 3 for reservation (Purchase session)
                if($kioskModuleStatusArray[$moduleKey] == 1) {
                   $pageLayoutFlag = 'POS_LAYOUT';     
                }
            }
            $isMachineAssigned = 1;
        }
        else {
            $isMachineAssigned = 0;
        }

        $getBanner = $this->getServiceLocator()->get('Cms\Model\HomeSliderTable')->getCrmHomeSlider(array('startIndex' => '', 'recordLimit' => '', 'sortField' => '', 'sortOrder' => '','isActive'=> '1'));
        
        $getProduct = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProduct(array('is_available' => '1', 'featured' => '1', 'offset' => '3', 'total_in_stock' => '1', 'rand_order_by' => '1','restricted_category_id' => $this->_config['custom_frame_id']));

        $discount = '';


        if (!empty($this->auth->getIdentity()->user_id)) {

            $user_id = $this->auth->getIdentity()->user_id;
            $userArr = array('user_id' => $user_id);
            $contactDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getContact($userArr);
            if ($contactDetail[0]['membership_id'] != '1') {
                $discount = $contactDetail[0]['discount'];
            }
        }


        $jsLangTranslate = $this->_config['home_messages']['config']['home_page'];
        $familyHistoryParam = array(
            'is_featured' => '1',
            'sort_field' => 'modified_date',
            'sort_order' => 'DESC',
            'start_index' => '0',
            'record_limit' => '1'
        );
        $familyHostory = $this->getServiceLocator()->get('Passenger\Model\FamilyhistoryTable')->getFeaturedFamilyHistories($familyHistoryParam);
        
        $kioskModules = $this->getServiceLocator()->get('KioskModules')->getKioskModules();
        $kioskModuleStatus = $this->getServiceLocator()->get('KioskModules')->getModuleStatus($kioskModules['kiosk_module_status']);
        $kioskModuleConfigArr = $this->getServiceLocator()->get('KioskModules')->getModuleCofigVal($kioskModules['kiosk_settings_module_id_values']);
        
        $uploadedPathProduct = $this->_config['file_upload_path']['assets_url'] . 'product/thumbnail';
        $uploadedPathStory = $this->_config['file_upload_path']['assets_url'] . 'familyhistory/extralarge';
        $assetsDirProduct = $this->_config['file_upload_path']['assets_upload_dir'] . 'product/thumbnail';
        $assetsUploadDirStory = $this->_config['file_upload_path']['assets_upload_dir'] . 'familyhistory/extralarge';
       
        $assetsPath = $this->_config['file_upload_path']['assets_url'];
        $viewModel->setVariables(array(
            'getProduct' => $getProduct,
            'discount' => $discount,
            'assetsUploadDirProduct' => $assetsDirProduct,
            'assetsUploadDirStory' => $assetsUploadDirStory,
            'familyHostory' => $familyHostory,
            'uploadedFilePathProduct' => $uploadedPathProduct,
            'uploadedFilePathStory' => $uploadedPathStory,
            'jsLangTranslate' => $jsLangTranslate,
            'arrBanner' => $getBanner,
            'pathBanner' => $this->_config['file_upload_path']['assets_url'].'homeslider',
            'rootPathBanner' => $this->_config['file_upload_path']['assets_upload_dir'].'homeslider',
            'visitationModule'=>$visitationModule,
            'purchaseSessionModule'=>$purchaseSessionModule,
            'isPassenger'=>(!empty($kioskModuleStatus[4]))?true:false,
            'wohModule'=>$wohModule,
            'moduleEllisMapStatus'=>$moduleEllisMapStatus,
            'modulePromoStatus'=>$modulePromoStatus,
            'assetsPath'=>$assetsPath
                )
        );
        if($visitationModule == 1 || $purchaseSessionModule==1) {
                $this->layout('pos');
                $viewModel->setTemplate('home/index/posindex.phtml'); // path to phtml file under view folder
        } 
        else if ($isMachineAssigned==0 || $isAnyModuleAssign==0){
                $this->layout('pos');
                $viewModel->setTemplate('home/index/invalidPage.phtml'); // Machine IP is not assigned
        }
        else if($wohModule == 1 || $moduleEllisMapStatus == 1 || $modulePromoStatus==1 || $moduleWohDonationStatus == 1 || $moduleWohVisitationCertificateStatus == 1) { 
                header('Location: ' . '/wall-of-honor');
                die;
        }
        return $viewModel;
    }
	public function wallOfHonorAction() {
        $this->layout()->setVariable('pauseRefreshTime', $this->_config['ajax_refresh_time']['pauseRefreshTime']);
         $this->layout()->setVariable('systemRefreshTime', $this->_config['ajax_refresh_time']['systemRefreshTime']);
        $this->layout('wohlayout');
        $this->getConfig();
        $viewModel = new ViewModel();
        $this->layout()->setVariable('stack', 'overflow');
        $searchParam = array();
        $searchParam['currentTime'] = DATE_TIME_FORMAT;
        $searchParam['status'] = '1';
        $kioskResult = $this->getServiceLocator()->get('Kiosk\Model\KioskManagementTable')->searchKiosk($searchParam);

        $purchaseSession = new Container('purchaseSession');
        $purchaseSession->purchaseSessionData = array();

        $donationSession = new Container('donationSession');
        $donationSession->donationData = array(); 
         
		$posTransactionSession = new Container('posTransactionSession'); /* pos session */
        $posTransactionSession->posTransactionData = array();         

        foreach($kioskResult as $key=>$val){
            $kioskIPArray[] = $val['kiosk_ip'];
        }
        $pageLayoutFlag = '';     
        $visitationModule = 0;
        $purchaseSessionModule = 0;
        $isMachineAssigned = 0;
        $isAnyModuleAssign = 0;

        if(in_array($_SERVER['REMOTE_ADDR'], $kioskIPArray)) {
            $kioskModuleParam = array();
            $kioskModuleParam['name_or_ip'] = $_SERVER['REMOTE_ADDR'];
            $kioskModuleResult = $this->getServiceLocator()->get('Kiosk\Model\KioskModuleTable')->searchKioskModule($kioskModuleParam);

            $moduleArray = explode(',', $kioskModuleResult['0']['modules']);
            $moduleStatusArray = explode(',', $kioskModuleResult['0']['module_status']);


                 
            $posKey = array_search('12', $moduleArray); // 12 = Promo;
            if($moduleStatusArray[$posKey] == 1)
                $modulePromoStatus = 1; 
            
            $posKey = array_search('11', $moduleArray); // 11 =  Ellis Map
            if($moduleStatusArray[$posKey] == 1)
                $moduleEllisMapStatus = 1;                                 
                               
            $wohKey = array_search('8', $moduleArray); // 8 = WOH;
            if($moduleStatusArray[$wohKey] == 1) {
                $wohModule = 1;
                
                $moduleSession = new Container('moduleSession');
                $moduleSession->moduleGroup = 3;             
           
            }
            
            $posKey = array_search('16', $moduleArray); // 16 =  Woh Donation
            if($moduleStatusArray[$posKey] == 1)
                $moduleWohDonationStatus = 1;
			
			$posKey = array_search('17', $moduleArray); // 17 =  Woh Visitation Certificate
            if($moduleStatusArray[$posKey] == 1)
                $moduleWohVisitationCertificateStatus = 1;
           
            if(count($kioskModuleResult) >0 ) {
                $kioskModuleArray = explode(',', $kioskModuleResult[0]['modules']);
                $kioskModuleStatusArray = explode(',', $kioskModuleResult[0]['module_status']);
                $moduleKey = array_search('3',$kioskModuleArray); // 3 for reservation (Purchase session)
                if($kioskModuleStatusArray[$moduleKey] == 1) {
                   $pageLayoutFlag = 'POS_LAYOUT';     
                }
            }
            $isMachineAssigned = 1;
        }
        else {
            $isMachineAssigned = 0;
        }
        
        $kioskModules = $this->getServiceLocator()->get('KioskModules')->getKioskModules();
        $kioskModuleStatus = $this->getServiceLocator()->get('KioskModules')->getModuleStatus($kioskModules['kiosk_module_status']);
        $kioskModuleConfigArr = $this->getServiceLocator()->get('KioskModules')->getModuleCofigVal($kioskModules['kiosk_settings_module_id_values']);
        
        $uploadedPathProduct = $this->_config['file_upload_path']['assets_url'] . 'product/thumbnail';
        $uploadedPathStory = $this->_config['file_upload_path']['assets_url'] . 'familyhistory/extralarge';
        $assetsDirProduct = $this->_config['file_upload_path']['assets_upload_dir'] . 'product/thumbnail';
        $assetsUploadDirStory = $this->_config['file_upload_path']['assets_upload_dir'] . 'familyhistory/extralarge';
       
        $assetsPath = $this->_config['file_upload_path']['assets_url'];
        $viewModel->setVariables(array(
            'assetsUploadDirProduct' => $assetsDirProduct,
            'assetsUploadDirStory' => $assetsUploadDirStory,
            'uploadedFilePathProduct' => $uploadedPathProduct,
            'uploadedFilePathStory' => $uploadedPathStory,
            'jsLangTranslate' => $jsLangTranslate,
            'wohModule'=>$wohModule,
            'moduleEllisMapStatus'=>$moduleEllisMapStatus,
            'modulePromoStatus'=>$modulePromoStatus,
            'assetsPath'=>$assetsPath,
            'moduleWohDonationStatus'=>$moduleWohDonationStatus,
			'moduleWohVisitationCertificateStatus' => $moduleWohVisitationCertificateStatus
                )
        );
        return $viewModel;	
	}
}
