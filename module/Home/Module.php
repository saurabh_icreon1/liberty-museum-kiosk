<?php

/**
 * This module file is used for initailize all obj of model and configuration setting
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace Home;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\I18n\Translator\Translator;
use Zend\Validator\AbstractValidator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;

/**
 * This module file is used for initailize all obj of model and configuration setting
 * @package    User
 * @author     Icreon Tech - DG
 */
class Module implements AutoloaderProviderInterface, ConfigProviderInterface, ViewHelperProviderInterface {

    public function onBootstrap(MvcEvent $env) {
        $translator = $env->getApplication()->getServiceManager()->get('translator');
        $eventManager = $env->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $translator->addTranslationFile(
                'phpArray', './module/Home/languages/en/language.php', 'default', 'en_US'
        );
        //AbstractValidator::setDefaultTranslator($translator);
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Get Autoloader Configuration
     * @return array
     */
    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    /**
     * Get Service Configuration
     *
     * @return array
     */
    public function getServiceConfig() {
        return array(
            'factories' => array(
                
            ),
        );
    }

    /**
     * Get View Helper Configuration
     *
     * @return array
     */
    public function getViewHelperConfig() {
        return array(
            'factories' => array(
            // the array key here is the name you will call the view helper by in your view scripts
//                'absoluteUrl' => function($sm) {
//                    $locator = $sm->getServiceLocator(); // $sm is the view helper manager, so we need to fetch the main service manager
//                    return new AbsoluteUrl($locator->get('Request'));
//                },
            ),
        );
    }

    /**
     * Get Controller Configuration
     *
     * @return array
     */
    public function getControllerConfig() {
        return array();
    }

    public function loadConfiguration(MvcEvent $env) {
        $application = $env->getApplication();
        $serviceManager = $application->getServiceManager();
    }

    public function loadCommonViewVars(MvcEvent $env) {
        $env->getViewModel()->setVariables(array(
            'auth' => $env->getApplication()->getServiceManager()->get('AuthService')
        ));
    }

}
