<?php

/**
 * This page is used for cases module configuration details.
 * @package    Cases
 * @author     Icreon Tech - DT
 */
return array(
    'controllers' => array(
        'invokables' => array(
            'Cases\Controller\Case' => 'Cases\Controller\CaseController'
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'addCase' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-case[/:contact_id][/:transaction_id]',
                    'defaults' => array(
                        'controller' => 'Cases\Controller\Case',
                        'action' => 'addCase',
                    ),
                ),
            ),
            'addTransactionCase' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-transaction-case[/:contact_id][/:transaction_id]',
                    'defaults' => array(
                        'controller' => 'Cases\Controller\Case',
                        'action' => 'addTransactionCase',
                    ),
                ),
            ),
            'cases' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/cases',
                    'defaults' => array(
                        'controller' => 'Cases\Controller\Case',
                        'action' => 'getCases',
                    ),
                ),
            ),
            'saveCaseSearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/save-case_search',
                    'defaults' => array(
                        'controller' => 'Cases\Controller\Case',
                        'action' => 'saveCaseSearch',
                    ),
                ),
            ),
            'getSavedCaseSearchSelect' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-saved-case-search-select',
                    'defaults' => array(
                        'controller' => 'Cases\Controller\Case',
                        'action' => 'getSavedCaseSearchSelect',
                    ),
                ),
            ),
            'getCaseSearchSavedParam' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-case-search-saved-param',
                    'defaults' => array(
                        'controller' => 'Cases\Controller\Case',
                        'action' => 'getCaseSearchSavedParam',
                    ),
                ),
            ),
            'deleteCaseSearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-case-search',
                    'defaults' => array(
                        'controller' => 'Cases\Controller\Case',
                        'action' => 'deleteCaseSearch',
                    ),
                ),
            ),
            'deleteCase' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-case',
                    'defaults' => array(
                        'controller' => 'Cases\Controller\Case',
                        'action' => 'deleteCase',
                    ),
                ),
            ),
            'addCaseToArchive' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-case-to-archive',
                    'defaults' => array(
                        'controller' => 'Cases\Controller\Case',
                        'action' => 'addCaseToArchive',
                    ),
                ),
            ),
            'getCaseDetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-case-detail[/][:case_contact_id][/][:mode]',
                    'defaults' => array(
                        'controller' => 'Cases\Controller\Case',
                        'action' => 'getCaseDetail',
                    ),
                ),
            ),
            'viewCaseDetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-case-detail/:case_contact_id',
                    'defaults' => array(
                        'controller' => 'Cases\Controller\Case',
                        'action' => 'viewCaseDetail',
                    ),
                ),
            ),
            'editCaseDetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-case-detail/:case_contact_id',
                    'defaults' => array(
                        'controller' => 'Cases\Controller\Case',
                        'action' => 'editCaseDetail',
                    ),
                ),
            ),
            'getContactCases' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/contact-cases',
                    'defaults' => array(
                        'controller' => 'Cases\Controller\Case',
                        'action' => 'getContactCases',
                    ),
                ),
            ),
            'getUserTransaction' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-user-transaction',
                    'defaults' => array(
                        'controller' => 'Cases\Controller\Case',
                        'action' => 'getUserTransaction',
                    ),
                ),
            ),
            'getCaseSubject' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-case-subject',
                    'defaults' => array(
                        'controller' => 'Cases\Controller\Case',
                        'action' => 'getCaseSubject',
                    ),
                ),
            ),
            'usercases' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-cases[/:user_id]',
                    'defaults' => array(
                        'controller' => 'Cases\Controller\Case',
                        'action' => 'userCases',
                    ),
                ),
            ),
            'addusercase' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-user-case[/:user_id][/:passenger_id]',
                    'defaults' => array(
                        'controller' => 'Cases\Controller\Case',
                        'action' => 'addUserCase',
                    ),
                ),
            ),
            'viewusercase' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-user-case[/:case_id]',
                    'defaults' => array(
                        'controller' => 'Cases\Controller\Case',
                        'action' => 'viewUserCase',
                    ),
                ),
            ),
            'editusercase' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-user-case[/:case_id]',
                    'defaults' => array(
                        'controller' => 'Cases\Controller\Case',
                        'action' => 'editUserCase',
                    ),
                ),
            ),
            'getCaseActivities' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/case-activities[/]',
                    'defaults' => array(
                        'controller' => 'Activity\Controller\Activity',
                        'action' => 'getContactActivities',
                    ),
                ),
            ),
            'deletecasefile' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-case-file',
                    'defaults' => array(
                        'controller' => 'Cases\Controller\Case',
                        'action' => 'deleteCaseFile',
                    ),
                ),
            ),			
            'uploadcasefile' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-case-file[/:counter][/:container][/:flag]',
                    'defaults' => array(
                        'controller' => 'Cases\Controller\Case',
                        'action' => 'uploadCaseFile',
                    ),
                ),
            ),			
            'getTransactionCases' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/transaction-cases',
                    'defaults' => array(
                        'controller' => 'Cases\Controller\Case',
                        'action' => 'getTransactionCases',
                    ),
                ),
            ),
        )
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'navigation' => array(
        'default' => array(
            array(
                'label' => 'Case',
                'route' => 'cases',
                'pages' => array(
                    array(
                        'label' => 'Search',
                        'route' => 'cases',
                        'pages' => array(
                            array(
                                'label' => 'Create',
                                'route' => 'addCase',
                                'action' => 'addCase',
                            ),
                            array(
                                'label' => 'View',
                                'route' => 'getCaseDetail',
                                'action' => 'getCaseDetail',
                            ),
                            array(
                                'label' => 'View',
                                'route' => 'viewCaseDetail',
                                'action' => 'viewCaseDetail',
                            ),
                            array(
                                'label' => 'Edit',
                                'route' => 'editCaseDetail',
                                'action' => 'editCaseDetail',
                            ),
                            array(
                                'label' => 'Change Log',
                                'route' => 'casechangelog',
                                'action' => 'change-log'
                            ),
                            array(
                                'label' => 'Activities',
                                'route' => 'getCaseActivities',
                                'action' => 'getContactActivities'
                            ),
                        ),
                    )
                ),
            )
        )
    ),
    'case_messages' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
            'navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'cases' => __DIR__ . '/../view'
        ),
        'template_map' => array(
            'layout/custom' => __DIR__ . '/../../module/Common/layout/crm.phtml',
            'error/404' => __DIR__ . '/../../module/Common/error/404.phtml',
            'error/index' => __DIR__ . '/../../module/Common/error/index.phtml',
        ),
    ),
    'global_config' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
);
