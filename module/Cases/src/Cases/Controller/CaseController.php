<?php

/**
 * This page is used for case module
 * @package    Cases_CaseController
 * @author     Icreon Tech - DT
 */

namespace Cases\Controller;

use Base\Controller\BaseController;
use Zend\View\Model\ViewModel;
use Cases\Form\CaseForm;
use Cases\Form\SearchCaseForm;
use Cases\Form\ContactSearchCaseForm;
use Cases\Form\SaveSearchForm;
use Cases\Form\CaseNotesForm;
use Cases\Form\CaseReplyForm;
use Cases\Form\UserCaseForm;
use User\Form\ContactUploadForm;
use Cases\Model\Cases;
use Zend\Http\Request;
use Zend\Authentication\AuthenticationService;

/**
 * This class is used for case module
 * @package    CaseController
 * @author     Icreon Tech - DT
 */
class CaseController extends BaseController {

    protected $_caseTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_flashMessage = null;

    function __construct() {
        $auth = new AuthenticationService();
        $this->auth = $auth;
        $this->_flashMessage = $this->flashMessenger();
        parent::__construct();
    }

    public function getConfig() {
        $sm = $this->getServiceLocator();
        $this->_translator = $sm->get('translator');
        $this->_config = $sm->get('Config');
    }

    /**
     * This function is used to get table and config file object
     * @return     array
     * @param void
     * @author Icreon Tech - DT
     */
    public function getCaseTable() {
        //$this->checkUserAuthentication();
        if (!$this->_caseTable) {
            $sm = $this->getServiceLocator();
            $this->_caseTable = $sm->get('Cases\Model\CasesTable');
            $this->_translator = $sm->get('translator');
            $this->_config = $sm->get('Config');
            $this->_adapter = $sm->get('dbAdapter');
            $this->_auth = new AuthenticationService();
        }
        return $this->_caseTable;
    }

    /**
     * This action is used to listing of cases
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function getCasesAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $this->getCaseTable();
        $caseSearchMessages = $this->_config['case_messages']['config']['case_search_message'];
        $searchCaseForm = new SearchCaseForm();
        $saveSearchForm = new SaveSearchForm();
        $cases = new Cases($this->_adapter);
        /* Case type */
        $caseTypeArr = array();
        $caseTypeArr[] = '';
        $getCaseType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCaseTypes($caseTypeArr);
        $caseType = array();
        $caseTypeDesc = array();
        $caseType[''] = 'Any';
        if ($getCaseType !== false) {
            foreach ($getCaseType as $key => $val) {
                $caseType[$val['case_type_id']] = $val['case_type'];
                $caseTypeDesc[$val['case_type_id']] = stripslashes($val['type_description']);
            }
        }
        $searchCaseForm->get('case_type')->setAttribute('options', $caseType);
        /* End  Case type */

        /* Case status */
        $caseStatusArr = array();
        $caseStatusArr[] = '';
        $getCaseStatus = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCaseStatus($caseStatusArr);
		$caseStatus = array();
        $caseStatusDesc = array();
		$selectedCaseStatus = array();
        $caseStatus[''] = 'Any';
        if ($getCaseStatus !== false) {
            foreach ($getCaseStatus as $key => $val) {
                $caseStatus[$val['case_status_id']] = $val['case_status'];
                $caseStatusDesc[$val['case_status_id']] = stripslashes($val['status_description']);

				if($val['case_status_id']!='6'){
					$selectedCaseStatus[] = $val['case_status_id'];
				}
            }
        }
		
        $searchCaseForm->get('case_status')->setAttribute('options', $caseStatus)->setValue($selectedCaseStatus);
		
        /* End  Case status */
        /* Get date range */
        $caseDateRange = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTransactionRange();
        $dateRange = array("" => "Select One");
        foreach ($caseDateRange as $key => $val) {
            $dateRange[$val['range_id']] = $val['range'];
        }
        $searchCaseForm->get('added_date_range')->setAttribute('options', $dateRange);
        $searchCaseForm->get('start_date_range')->setAttribute('options', $dateRange);
        /* end Get date range */


        /* Saved search data */
        $getSearchArray = array();
        $getSearchArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
        $getSearchArray['case_search_id'] = '';
        $getSearchArray['is_active'] = '1';
        $caseSavedSearchArray = $cases->getCaseSavedSearchArr($getSearchArray);
        $caseSearchArray = $this->_caseTable->getCaseSavedSearch($caseSavedSearchArray);
        $caseSearchList = array();
        $caseSearchList[''] = 'Select';
        foreach ($caseSearchArray as $key => $val) {
            $caseSearchList[$val['case_search_id']] = stripslashes($val['title']);
        }
        $searchCaseForm->get('saved_search')->setAttribute('options', $caseSearchList);

        /* end  Saved search data */
        if (($request->isXmlHttpRequest() && $request->isPost()) || $request->getPost('export') == 'excel') {
            $response = $this->getResponse();
            $postArray = $request->getPost()->toArray();
            parse_str($request->getPost('searchString'), $searchParam);
			$dashlet = $request->getPost('crm_dashlet');

            $dashletSearchId = $request->getPost('searchId');
            $dashletId = $request->getPost('dashletId');
            $dashletSearchType = $request->getPost('searchType');
            $searchNew = $request->getPost('searchNew');
            $dashletSearchString = $request->getPost('dashletSearchString');
            if (isset($dashletSearchString) && $dashletSearchString != '') {
                $searchParam = unserialize($dashletSearchString);
            }
            $isExport = $request->getPost('export');
            if ($isExport != "" && $isExport == "excel") {
                $chunksize = $this->_config['export']['chunk_size'];
                $export_limit = $this->_config['export']['export_limit'];
                $searchParam['recordLimit'] = $chunksize;
            }
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit;
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');

            if (isset($searchNew) && !empty($searchNew)) {
                $searchParam['searchNew'] = DATE_TIME_FORMAT;
            }
            
            if (isset($searchParam['added_date_range']) && $searchParam['added_date_range'] != 1 && $searchParam['added_date_range'] != '') {
                $dateRange = $this->getDateRange($searchParam['added_date_range']);
                $searchParam['added_date_from'] = $this->DateFormat($dateRange['from'], 'db_date_format_from');
                $searchParam['added_date_to'] = $this->DateFormat($dateRange['to'], 'db_date_format_to');
            } else if (isset($searchParam['added_date_range']) && $searchParam['added_date_range'] == '') {
                $searchParam['added_date_from'] = '';
                $searchParam['added_date_to'] = '';
            } else {
                if (isset($searchParam['added_date_from']) && $searchParam['added_date_from'] != '') {
                    $searchParam['added_date_from'] = $searchParam['added_date_from'];
                    $searchParam['added_date_from'] = $this->DateFormat($searchParam['added_date_from'], 'db_date_format_from');
                }
                if (isset($searchParam['added_date_to']) && $searchParam['added_date_to'] != '') {
                    $searchParam['added_date_to'] = $searchParam['added_date_to'];
                    $searchParam['added_date_to'] = $this->DateFormat($searchParam['added_date_to'], 'db_date_format_to');
                }
            }
            
            if (isset($searchParam['start_date_range']) && $searchParam['start_date_range'] != 1 && $searchParam['start_date_range'] != '') {
                $dateRange = $this->getDateRange($searchParam['start_date_range']);
                $searchParam['start_date_from'] = $this->DateFormat($dateRange['from'], 'db_date_format_from');
                $searchParam['start_date_to'] = $this->DateFormat($dateRange['to'], 'db_date_format_to');
            } else if (isset($searchParam['start_date_range']) && $searchParam['start_date_range'] == '') {
                $searchParam['start_date_from'] = '';
                $searchParam['start_date_to'] = '';
            } else {
                if (isset($searchParam['start_date_from']) && $searchParam['start_date_from'] != '') {
                    $searchParam['start_date_from'] = $searchParam['start_date_from'];
                    $searchParam['start_date_from'] = $this->DateFormat($searchParam['start_date_from'], 'db_date_format_from');
                }
                if (isset($searchParam['start_date_to']) && $searchParam['start_date_to'] != '') {
                    $searchParam['start_date_to'] = $searchParam['start_date_to'];
                    $searchParam['start_date_to'] = $this->DateFormat($searchParam['start_date_to'], 'db_date_format_to');
                }
            }
            
            $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'modified_date') ? 'tbl_case.modified_date' : $searchParam['sort_field'];


            $page_counter = 1;
            do {
                $dashletResult = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getTableStruct(array('user_id' => $this->auth->getIdentity()->crm_user_id, 'search_id' => $dashletSearchId, 'dash_id' => $dashletId, 'search_type' => $dashletSearchType));
                $newExplodedColumnName = array();
                if (!empty($dashletResult)) {
                    $explodedColumnName = explode(",", $dashletResult[0]['table_column_names']);
                    foreach ($explodedColumnName as $val) {
                        if (strpos($val, ".")) {
                            $newExplodedColumnName[] = trim(strstr($val, ".", false), ".");
                        } else {
                            $newExplodedColumnName[] = trim($val);
                        }
                    }
                }
                if(isset($searchParam['case_status']) && is_array($searchParam['case_status'])){
					$searchParam['case_status'] = implode(",",$searchParam['case_status']);
				}
				$searchCaseArr = $cases->getCaseSearchArr($searchParam);
                
                $seachResult = $this->_caseTable->getAllCase($searchCaseArr);

				$countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
                $jsonResult = array();
                $arrCell = array();
                $subArrCell = array();
                $total = $countResult[0]->RecordCount;
                if ($isExport == "excel") {

                    /* if ($total > $export_limit) {
                      $totalExportedFiles = ceil($total / $export_limit);
                      $exportzip = 1;
                      }
                     * */

                    if ($total > $chunksize) {
                        $number_of_pages = ceil($total / $chunksize);
                        //echo $number_of_pages .'--'.$page_counter;
                        //echo "<br>";
                        $page_counter++;
                        $start = $chunksize * $page_counter - $chunksize;
                        $searchParam['start_index'] = $start;
                        $searchParam['page'] = $page_counter;
                    } else {
                        $number_of_pages = 1;
                        $page_counter++;
                    }
                } else {
                    $page_counter = $page;
                    $number_of_pages = ceil($total / $limit);
                    $jsonResult['records'] = $countResult[0]->RecordCount;
                    $jsonResult['page'] = $request->getPost('page');
                    $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
                }



                //$jsonResult['page'] = $request->getPost('page');
                // $jsonResult['records'] = $countResult[0]->RecordCount;
                //$jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
                if (!empty($seachResult)) {
                    $arrExport = array();

					foreach ($seachResult as $val) {
                        $dashletCell = array();
                        $arrCell['id'] = $val['case_contact_id'];
                        $encryptId = $this->encrypt($val['case_contact_id']);
                        $viewLink = '<a href="/get-case-detail/' . $encryptId . '/' . $this->encrypt('view') . '" class="view-icon"><div class="tooltip">View<span></span></div></a>';
                        $editLink = '<a href="/get-case-detail/' . $encryptId . '/' . $this->encrypt('edit') . '" class="edit-icon"><div class="tooltip">Edit<span></span></div></a>';
                        $deleteLink = '<a onclick="deleteCase(\'' . $encryptId . '\')" href="#delete_case_content" class="delete_case delete-icon"><div class="tooltip">Delete<span></span></div></a>';
                        $actions = '<div class="action-col">' . $viewLink . $editLink . $deleteLink . '</div>';
                        $caseContactId = '';
                        $caseContactId = '<span class="plus-sign"><a href="/create-activity/' . $this->encrypt($val['user_id']) . '/' . $this->encrypt('4') . '/' . $this->encrypt($val['case_contact_id']) . '"><div class="tooltip">Add activity<span></span></div></a></span>';
                        if ($val['is_viewed'] == 0) {
                            $caseContactId.= '<span class="red">*</span><span class="left">' . $val['case_contact_id'] . '</span>';
                        } else {
                            $caseContactId.= '<span class="left">' . $val['case_contact_id'] . '</span>';
                        }
                        if ($val['is_archive'] == 1) {
                            $caseContactId.= '<span class="archive"></span>';
                        }

                        $caseStartDate = ($val['case_start_date'] == 0) ? 'N/A' : $this->OutputDateFormat($val['case_start_date'], 'dateformatampm');

                        $caseCreatedDate = ($val['added_date'] == 0) ? 'N/A' : $this->OutputDateFormat($val['added_date'], 'dateformatampm');

                        $caseModifiedDate = ($val['modified_date'] == 0) ? 'N/A' : $this->OutputDateFormat($val['modified_date'], 'dateformatampm');

                        $fullName = trim($val['full_name']);
                        $fullName = (!empty($fullName)) ? $val['full_name'] : $val['company_name'];

						$assigned_to = (!is_null($val['assigned_to']) && $val['assigned_to'])?$val['assigned_to']:'N/A';

                        $dashletStartDate = array_search('case_start_date', $newExplodedColumnName);
                        if ($dashletStartDate !== false)
                            $dashletCell[$dashletStartDate] = $caseStartDate;

                        $dashletUserName = array_search('full_name', $newExplodedColumnName);
                        if ($dashletUserName !== false)
                            $dashletCell[$dashletUserName] = $fullName;

                        $dashletCaseContactId = array_search('case_contact_id', $newExplodedColumnName);
                        if ($dashletCaseContactId !== false)
                            $dashletCell[$dashletCaseContactId] = '<a href="get-case-detail/' . $this->encrypt($val['case_contact_id']) . '/' . $this->encrypt('view') . '" class="txt-decoration-underline">' . $val['case_contact_id'] . '</a>';

                        $dashletTransId = array_search('user_transaction_id', $newExplodedColumnName);
                        if ($dashletTransId !== false)
                            $dashletCell[$dashletTransId] = $val['user_transaction_id'];

                        $dashletTransId = array_search('user_transaction_id', $newExplodedColumnName);
                        if ($dashletTransId !== false)
                            $dashletCell[$dashletTransId] = $val['user_transaction_id'];

                        $dashletCaseStatus = array_search('case_status', $newExplodedColumnName);
                        if ($dashletCaseStatus !== false)
                            $dashletCell[$dashletCaseStatus] = $val['case_status'];

                        $dashletCaseType = array_search('case_type', $newExplodedColumnName);
                        if ($dashletCaseType !== false)
                            $dashletCell[$dashletCaseType] = $val['case_type'];

                        $dashletCaseSub = array_search('case_subject', $newExplodedColumnName);
                        if ($dashletCaseSub !== false)
                            $dashletCell[$dashletCaseSub] = $val['case_subject'];

                        if (isset($dashlet) && $dashlet == 'dashlet') {
                            $view = $this->encrypt('view');
                            $name = "<a href='/view-contact/" . $this->encrypt($val['user_id']) . "/" . $view . "' class='view-icon'>$fullName</a>";
                            // $arrCell['cell'] = array($val['case_contact_id'], $val['case_type'], $fullName, $caseStartDate);
                            $arrCell['cell'] = $dashletCell;
                        } else if ($isExport == "excel") {
                            $arrExport[] = array('Case #' => $val['case_contact_id'], 'Subject' => $val['case_subject'], 'Message' => $val['case_message'], 'Transaction #' => $val['user_transaction_id'],"Created Date" => $caseCreatedDate, 'Start Date' => $caseStartDate,  "Modified Date" => $caseModifiedDate, 'Type' => $val['case_type'], 'Status' => $val['case_status'], 'Contact Name' => $fullName, 'Assigned To' => $assigned_to);
                        } else {
                            $name = '<a class="txt-decoration-underline" onclick=window.open(\'/view-contact/' . $this->encrypt($val['user_id']) . '/' . $this->encrypt('view') . '\',\'popUpWindow,resizable=yes,scrollbars=yes\') href="javascript:void(0)">' . $this->chunkData($fullName, 20) . '</a>';
                            $arrCell['cell'] = array($val['is_urgent'], $caseContactId, $this->chunkData($val['case_subject'], 20), $this->chunkData($val['case_message'], 120), $val['user_transaction_id'],$caseCreatedDate, $caseStartDate,  $caseModifiedDate, $val['case_type'], $val['case_status'], $name, $assigned_to, $actions);
                        }
                        $subArrCell[] = $arrCell;
                        $jsonResult['rows'] = $subArrCell;
                    }
                    if ($isExport == "excel") {
                        $filename = "case_export_" . date("Y-m-d") . ".csv";
                        $this->arrayToCsv($arrExport, $filename, $page_counter);
                    }
                } else {
                    $jsonResult['rows'] = array();
                }
            } while ($page_counter <= $number_of_pages && $isExport == "excel");

            if ($isExport == "excel") {
                $this->downloadSendHeaders("case_export_" . date("Y-m-d") . ".csv");
                die;
            } else {
                $response->setContent(\Zend\Json\Json::encode($jsonResult));
                return $response;
            }
        } else {
            $this->layout('crm');
            $messages = array();
            if ($this->_flashMessage->hasMessages()) {
                $messages = $this->_flashMessage->getMessages();
            }
            $viewModel = new ViewModel();

            $response = $this->getResponse();
            $viewModel->setVariables(array(
                'form' => $searchCaseForm,
                'search_form' => $saveSearchForm,
                'messages' => $messages,
                'jsLangTranslate' => array_merge($caseSearchMessages, $this->_config['grid_config']),
                'caseTypeDesc' => $caseTypeDesc,
                'caseStatusDesc' => $caseStatusDesc
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to create case
     * @return json 
     * @param void
     * @author Icreon Tech - DT
     */
    public function addCaseAction() {
        $this->checkUserAuthentication();
        $this->getCaseTable();
        $caseCreateMessages = $this->_config['case_messages']['config']['case_create_message'];
        $cases = new Cases($this->_adapter);

        $createCaseForm = new CaseForm();
        $request = $this->getRequest();
        $response = $this->getResponse();


        /* Case mode type */
        $getCaseModeType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCaseModeType();
        $modeType = array();
        if ($getCaseModeType !== false) {
            foreach ($getCaseModeType as $key => $val) {
                $modeType[$val['case_mode_type_id']] = $val['case_mode_type'];
            }
        }
        $createCaseForm->get('mode')->setAttribute('options', $modeType);
        $createCaseForm->get('mode')->setValue(6);
        /* End  Case mode type */

        /* Case type */
        $caseTypeArr = array();
        $caseTypeArr[] = '';
        $getCaseType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCaseTypes($caseTypeArr);
        $caseType = array();
        $caseTypeDesc = array();
        $caseType[''] = 'Select';
        if ($getCaseType !== false) {
            foreach ($getCaseType as $key => $val) {
                $caseType[$val['case_type_id']] = $val['case_type'];
                $caseTypeDesc[$val['case_type_id']] = stripslashes($val['type_description']);
            }
        }
        $createCaseForm->get('case_type')->setAttribute('options', $caseType);
        /* End  Case type */

        /* Case status */
        $caseStatusArr = array();
        $caseStatusArr[] = '';
        $getCaseStatus = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCaseStatus($caseStatusArr);
        $caseStatus = array();
        $caseStatusDesc = array();
        $caseStatus[''] = 'Select';
        if ($getCaseStatus !== false) {
            foreach ($getCaseStatus as $key => $val) {
                $caseStatus[$val['case_status_id']] = $val['case_status'];
                $caseStatusDesc[$val['case_status_id']] = stripslashes($val['status_description']);
            }
        }
        $createCaseForm->get('case_status')->setAttribute('options', $caseStatus);
        /* End  Case status */

        /* user transaction */
        $params = $this->params()->fromRoute();
		$postTransactionId = $request->getPost("transaction_id");
		
		$transaction_id = (isset($params['transaction_id']) && $params['transaction_id']!='')?$this->decrypt($params['transaction_id']):'';
        //$postTransactionId = $transaction_id;
		if (((isset($params['case_contact_id']) && $params['case_contact_id'] != '') || $postTransactionId != '') && $transaction_id=='') {
            $searchParam = array();
            $searchParam['user_id'] = ((isset($postTransactionId) && $postTransactionId != '')) ? $postTransactionId : ($this->decrypt($params['case_contact_id']));
            $searchParam['sort_field'] = 'usr_transaction.transaction_date';
            $transactionArr = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getUserTransactions($searchParam);
            $userTransactionArr = array();
            $userTransactionArr[''] = 'Select';
            if ($transactionArr !== false) {
                foreach ($transactionArr as $key => $val) {
                    $userTransactionArr[$val['transaction_id']] = $val['transaction_id'];
                }
            }
            $createCaseForm->get('transaction_id')->setAttribute('options', $userTransactionArr);
        }else if($transaction_id!=''){
			$createCaseForm->get('transaction_id')->setAttribute('options', array($transaction_id => $transaction_id));
		}
        /* End  user transaction  */

        $form_upload = new ContactUploadForm();
        $createCaseForm->add($form_upload);

        if ($request->isPost()) {
            $form_data = $request->getPost();
			$createCaseForm->setData($request->getPost());
            $createCaseForm->setInputFilter($cases->getInputFilterCreateCase());
            if (!$createCaseForm->isValid()) {
                $errors = $createCaseForm->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'continue' && $key!= 'is_send_email') {
                        foreach ($row as $typeError => $rower) {
                            if ($typeError != 'notInArray') {
                                $msg [$key] = $caseCreateMessages[$rower];
                            }
                        }
                    }
                }
            }

            if (!empty($msg)) {
                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $createCaseFormArr = $createCaseForm->getData();

                if (isset($createCaseFormArr['case_contact_id']) && $createCaseFormArr['case_contact_id'] != '' && $createCaseFormArr['case_contact_id'] != '0') {
                    //for edit
                    $createCaseFormArr['modify_by'] = $this->_auth->getIdentity()->crm_user_id;
                    $createCaseFormArr['modify_date'] = DATE_TIME_FORMAT;
                    $editCaseArr = $cases->getCreateEditArr($createCaseFormArr);
                    $editCaseArr[8] = $this->InputDateFormat($editCaseArr[8], 'dateformatampm');
                    $this->_caseTable->editCase($editCaseArr);

                    if (isset($form_data['publicdocs_upload_count']) && ($form_data['publicdocs_upload_count'] != '' || $form_data['publicdocs_upload_count'] != '0')) {
                        $uploadcount = $form_data['publicdocs_upload_count'];

                        for ($count = 0; $count < $uploadcount; $count++) {
                            $key = 'publicdocs_upload_file' . $count;
                            $filesName = $form_data[$key];
                            if ($filesName != '') {

                                $this->moveCaseFileFromTempToCase($filesName, $createCaseFormArr['case_contact_id']);
                                $filedata['case_contact_id'] = $createCaseFormArr['case_contact_id'];
                                $filedata['file_name'] = $filesName;
                                $filedata['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                                $filedata['added_date'] = DATE_TIME_FORMAT;
                                $filedata['modified_date'] = DATE_TIME_FORMAT;
                                $this->_caseTable->addCaseFile($filedata);
                            }
                        }
                    }
                    /** insert into the change log */
                    $changeLogArray = array();
                    $changeLogArray['table_name'] = 'tbl_cas_change_logs';
                    $changeLogArray['activity'] = '2';/** 2 == for edit */
                    $changeLogArray['id'] = $createCaseFormArr['case_contact_id'];
                    $changeLogArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                    $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                    $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                    /** end insert into the change log */
                    $this->flashMessenger()->addMessage($caseCreateMessages['CASE_UPDATED_SUCCESSFULLY']);
                    $messages = array('status' => "success", 'message' => $caseCreateMessages['CASE_UPDATED_SUCCESSFULLY']);
                } else {
                    //for add
                    $createCaseFormArr['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                    $createCaseFormArr['added_date'] = DATE_TIME_FORMAT;

					$createCaseArr = $cases->getCreateCaseArr($createCaseFormArr);
                    $createCaseArr[7] = $this->InputDateFormat($createCaseArr[7], 'dateformatampm');
					
				$caseId = $this->_caseTable->saveCase($createCaseArr);
				if(!empty($caseId) && $createCaseFormArr['is_send_email']=='1'){
				$searchContactParams = array();
				$searchContactParams['contact_user_id'] = $request->getPost('contact_name_id');
				$contactDetails = $this->getServiceLocator()->get('User\Model\UserTable')->getUserContactDetails($searchContactParams);
                $caseReplyDataArr = array();
                $caseReplyDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                $caseReplyDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                $caseReplyDataArr['contact_case_id'] = $caseId;
                $caseReplyDataArr['contact_name'] = $createCaseFormArr['contact_name'];
                $caseReplyDataArr['email_id'] = $contactDetails['email_id'];
                $caseReplyDataArr['first_name'] = $contactDetails['first_name'];
                $caseReplyDataArr['last_name'] = $contactDetails['last_name'];
                $caseReplyDataArr['message'] = $request->getPost('message');
				$startDate = $request->getPost('start_date');
				$startTime = $request->getPost('start_time');
				
				$caseReplyDataArr['case_start_time'] = $startDate." ".$startTime;
				$email_id_template_int = 38;
                $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($caseReplyDataArr, $email_id_template_int, $this->_config['email_options']);
				}
                    if (isset($form_data['publicdocs_upload_count']) && ($form_data['publicdocs_upload_count'] != '' || $form_data['publicdocs_upload_count'] != '0')) {
                        $uploadcount = $form_data['publicdocs_upload_count'];

                        for ($count = 0; $count < $uploadcount; $count++) {
                            $key = 'publicdocs_upload_file' . $count;
                            $filesName = $form_data[$key];
                            if ($filesName != '') {

                                $this->moveCaseFileFromTempToCase($filesName, $caseId);
                                $filedata['case_contact_id'] = $caseId;
                                $filedata['file_name'] = $filesName;
                                $filedata['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                                $filedata['added_date'] = DATE_TIME_FORMAT;
                                $filedata['modified_date'] = DATE_TIME_FORMAT;
                                $this->_caseTable->addCaseFile($filedata);
                            }
                        }
                    }
                    /** insert into the change log */
                    $changeLogArray = array();
                    $changeLogArray['table_name'] = 'tbl_cas_change_logs';
                    $changeLogArray['activity'] = '1';
                    $changeLogArray['id'] = $caseId;
                    $changeLogArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                    $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                    $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                    /** end insert into the change log */
                    $this->flashMessenger()->addMessage($caseCreateMessages['CASE_CREATED_SUCCESSFULLY']);
                    $messages = array('status' => "success", 'message' => $caseCreateMessages['CASE_CREATED_SUCCESSFULLY']);
                }

                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        } else {
            $this->layout('crm');
            $caseContactId = '';
            $params = $this->params()->fromRoute();
			if (isset($params['contact_id']) && $params['contact_id'] != '') {
                $contactId = $this->decrypt($params['contact_id']);

                $contactDetailArr = array();
				$contactDetailArr['user_id'] = $contactId;
				$contactDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getContact($contactDetailArr);
				$createCaseForm->get('contact_name_id')->setValue($contactId);
				$createCaseForm->get('contact_name')->setValue($contactDetail[0]['full_name']);
            }
            $encryptedCaseContactId = (isset($params['case_contact_id']) && $params['case_contact_id'] != '') ? $params['case_contact_id'] : '';

			$encryptedContactId = (isset($params['contact_id']) && $params['contact_id'] != '') ? $params['contact_id'] : '';

            $viewModel = new ViewModel();
            $viewModel->setVariables(array(
                'jsLangTranslate' => $caseCreateMessages,
                'create_case_form' => $createCaseForm,
                'case_contact_id' => $caseContactId,
                'encrypt_case_contact_id' => $encryptedCaseContactId,
				'encryptedContactId' => $encryptedContactId,
                'caseTypeDesc' => $caseTypeDesc,
                'caseStatusDesc' => $caseStatusDesc,
				'contactDetail' => $contactDetail,
				'transaction_id' => $transaction_id
            ));

            return $viewModel;
        }
    }
    /**
     * This action is used to create case for a transaction from ajax call
     * @return json 
     * @param void
     * @author Icreon Tech - DT
     */
    public function addTransactionCaseAction()
    {
        $this->checkUserAuthentication();
        $this->getCaseTable();
        $this->layout('popup');
        $caseCreateMessages = $this->_config['case_messages']['config']['case_create_message'];
        $cases = new Cases($this->_adapter);

        $createCaseForm = new CaseForm();
        $request = $this->getRequest();
        $response = $this->getResponse();


        /* Case mode type */
        $getCaseModeType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCaseModeType();
        $modeType = array();
        if ($getCaseModeType !== false)
        {
            foreach ($getCaseModeType as $key => $val)
            {
                $modeType[$val['case_mode_type_id']] = $val['case_mode_type'];
            }
        }
        $createCaseForm->get('mode')->setAttribute('options' , $modeType);
        $createCaseForm->get('mode')->setValue(6);
        $createCaseForm->get('cancel')->setAttribute('onclick' , 'parent.$.fn.colorbox.close();parent.$("#tab-1").click();');
        $createCaseForm->get('contact_name')->setAttribute('class' , '');
        $createCaseForm->get('contact_name')->setAttribute('readonly' , 'readonly');
        /* End  Case mode type */

        /* Case type */
        $caseTypeArr = array();
        $caseTypeArr[] = '';
        $getCaseType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCaseTypes($caseTypeArr);
        $caseType = array();
        $caseTypeDesc = array();
        $caseType[''] = 'Select';
        if ($getCaseType !== false)
        {
            foreach ($getCaseType as $key => $val)
            {
                $caseType[$val['case_type_id']] = $val['case_type'];
                $caseTypeDesc[$val['case_type_id']] = stripslashes($val['type_description']);
            }
        }
        $createCaseForm->get('case_type')->setAttribute('options' , $caseType);
        /* End  Case type */

        /* Case status */
        $caseStatusArr = array();
        $caseStatusArr[] = '';
        $getCaseStatus = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCaseStatus($caseStatusArr);
        $caseStatus = array();
        $caseStatusDesc = array();
        $caseStatus[''] = 'Select';
        if ($getCaseStatus !== false)
        {
            foreach ($getCaseStatus as $key => $val)
            {
                $caseStatus[$val['case_status_id']] = $val['case_status'];
                $caseStatusDesc[$val['case_status_id']] = stripslashes($val['status_description']);
            }
        }
        $createCaseForm->get('case_status')->setAttribute('options' ,
                $caseStatus);
        /* End  Case status */

        /* user transaction */
        $params = $this->params()->fromRoute();
        $postTransactionId = $request->getPost("transaction_id");

        $transaction_id = (isset($params['transaction_id']) && $params['transaction_id']
                != '') ? $this->decrypt($params['transaction_id']) : '';
        //$postTransactionId = $transaction_id;
        if (((isset($params['case_contact_id']) && $params['case_contact_id'] != '')
                || $postTransactionId != '') && $transaction_id == '')
        {
            $searchParam = array();
            $searchParam['user_id'] = ((isset($postTransactionId) && $postTransactionId
                    != '')) ? $postTransactionId : ($this->decrypt($params['case_contact_id']));
            $searchParam['sort_field'] = 'usr_transaction.transaction_date';
            $transactionArr = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getUserTransactions($searchParam);
            $userTransactionArr = array();
            $userTransactionArr[''] = 'Select';
            if ($transactionArr !== false)
            {
                foreach ($transactionArr as $key => $val)
                {
                    $userTransactionArr[$val['transaction_id']] = $val['transaction_id'];
                }
            }
            $createCaseForm->get('transaction_id')->setAttribute('options' ,
                    $userTransactionArr);
        }
        else if ($transaction_id != '')
        {
            $createCaseForm->get('transaction_id')->setAttribute('options' ,
                    array($transaction_id => $transaction_id));
        }
        /* End  user transaction  */

        $form_upload = new ContactUploadForm();
        $createCaseForm->add($form_upload);

        if ($request->isPost())
        {
            $form_data = $request->getPost();
            $createCaseForm->setData($request->getPost());
            $createCaseForm->setInputFilter($cases->getInputFilterCreateCase());
            if (!$createCaseForm->isValid())
            {
                $errors = $createCaseForm->getMessages();
                $msg = array();
                foreach ($errors as $key => $row)
                {
                    if (!empty($row) && $key != 'continue' && $key != 'is_send_email')
                    {
                        foreach ($row as $typeError => $rower)
                        {
                            if ($typeError != 'notInArray')
                            {
                                $msg [$key] = $caseCreateMessages[$rower];
                            }
                        }
                    }
                }
            }

            if (!empty($msg))
            {
                $messages = array('status' => "error" , 'message' => $msg);
            }

            if (!empty($messages))
            {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
            else
            {
                $createCaseFormArr = $createCaseForm->getData();

                if (isset($createCaseFormArr['case_contact_id']) && $createCaseFormArr['case_contact_id']
                        != '' && $createCaseFormArr['case_contact_id'] != '0')
                {
                    //for edit
                    $createCaseFormArr['modify_by'] = $this->_auth->getIdentity()->crm_user_id;
                    $createCaseFormArr['modify_date'] = DATE_TIME_FORMAT;
                    $editCaseArr = $cases->getCreateEditArr($createCaseFormArr);
                    $editCaseArr[8] = $this->InputDateFormat($editCaseArr[8] ,
                            'dateformatampm');
                    $this->_caseTable->editCase($editCaseArr);

                    if (isset($form_data['publicdocs_upload_count']) && ($form_data['publicdocs_upload_count']
                            != '' || $form_data['publicdocs_upload_count'] != '0'))
                    {
                        $uploadcount = $form_data['publicdocs_upload_count'];

                        for ($count = 0; $count < $uploadcount; $count++)
                        {
                            $key = 'publicdocs_upload_file' . $count;
                            $filesName = $form_data[$key];
                            if ($filesName != '')
                            {

                                $this->moveCaseFileFromTempToCase($filesName ,
                                        $createCaseFormArr['case_contact_id']);
                                $filedata['case_contact_id'] = $createCaseFormArr['case_contact_id'];
                                $filedata['file_name'] = $filesName;
                                $filedata['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                                $filedata['added_date'] = DATE_TIME_FORMAT;
                                $filedata['modified_date'] = DATE_TIME_FORMAT;
                                $this->_caseTable->addCaseFile($filedata);
                            }
                        }
                    }
                    /** insert into the change log */
                    $changeLogArray = array();
                    $changeLogArray['table_name'] = 'tbl_cas_change_logs';
                    $changeLogArray['activity'] = '2';/** 2 == for edit */
                    $changeLogArray['id'] = $createCaseFormArr['case_contact_id'];
                    $changeLogArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                    $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                    $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                    /** end insert into the change log */
                    //$this->flashMessenger()->addMessage($caseCreateMessages['CASE_UPDATED_SUCCESSFULLY']);
                    $messages = array('status' => "success" , 'message' => $caseCreateMessages['CASE_UPDATED_SUCCESSFULLY']);
                }
                else
                {
                    //for add
                    $createCaseFormArr['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                    $createCaseFormArr['added_date'] = DATE_TIME_FORMAT;
                    #$createCaseFormArr['modify_by'] = $this->_auth->getIdentity()->crm_user_id;
                    #$createCaseFormArr['modify_date'] = DATE_TIME_FORMAT;
                    #$createCaseFormArr['start_date'] = (!is_null($createCaseFormArr['start_date']) && $createCaseFormArr['start_date']!='')?date("Y-m-d", strtotime($createCaseFormArr['start_date'])):'';
                    #$createCaseFormArr['start_time'] = (!is_null($createCaseFormArr['start_time']) && $createCaseFormArr['start_time']!='')?date("H:i:s", strtotime($createCaseFormArr['start_time'])):'';

                    $createCaseArr = $cases->getCreateCaseArr($createCaseFormArr);
                    $createCaseArr[7] = $this->InputDateFormat($createCaseArr[7] ,
                            'dateformatampm');
                    $caseId = $this->_caseTable->saveCase($createCaseArr);

                    if (isset($form_data['publicdocs_upload_count']) && ($form_data['publicdocs_upload_count']
                            != '' || $form_data['publicdocs_upload_count'] != '0'))
                    {
                        $uploadcount = $form_data['publicdocs_upload_count'];

                        for ($count = 0; $count < $uploadcount; $count++)
                        {
                            $key = 'publicdocs_upload_file' . $count;
                            $filesName = $form_data[$key];
                            if ($filesName != '')
                            {

                                $this->moveCaseFileFromTempToCase($filesName ,
                                        $caseId);
                                $filedata['case_contact_id'] = $caseId;
                                $filedata['file_name'] = $filesName;
                                $filedata['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                                $filedata['added_date'] = DATE_TIME_FORMAT;
                                $filedata['modified_date'] = DATE_TIME_FORMAT;
                                $this->_caseTable->addCaseFile($filedata);
                            }
                        }
                    }
                    /** insert into the change log */
                    $changeLogArray = array();
                    $changeLogArray['table_name'] = 'tbl_cas_change_logs';
                    $changeLogArray['activity'] = '1';
                    $changeLogArray['id'] = $caseId;
                    $changeLogArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                    $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                    $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                    /** end insert into the change log */
                    //$this->flashMessenger()->addMessage($caseCreateMessages['CASE_CREATED_SUCCESSFULLY']);
                    $messages = array('status' => "success" , 'message' => $caseCreateMessages['CASE_CREATED_SUCCESSFULLY']);
                }

                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        else
        {
            //$this->layout('crm');
            $caseContactId = '';
            $params = $this->params()->fromRoute();
            if (isset($params['contact_id']) && $params['contact_id'] != '')
            {
                $contactId = $this->decrypt($params['contact_id']);

                $contactDetailArr = array();
                $contactDetailArr['user_id'] = $contactId;
                $contactDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getContact($contactDetailArr);
                $createCaseForm->get('contact_name_id')->setValue($contactId);
                $createCaseForm->get('contact_name')->setValue($contactDetail[0]['full_name']);

                //asd($contactDetail);
                /* $caseDetailArr = array();
                  $caseDetailArr[] = $caseContactId;
                  $getCaseDetail = $this->_caseTable->getCaseById($caseDetailArr);
                  $startDateArr = explode(' ', $getCaseDetail['start_date']);
                  $getCaseDetail['start_date'] = $startDateArr[0];
                  $getCaseDetail['start_time'] = $startDateArr[1];
                  $this->setValueForElement($getCaseDetail, $createCaseForm);
                  $createCaseForm->get('case_contact_id')->setValue($caseContactId); */
            }
            $encryptedCaseContactId = (isset($params['case_contact_id']) && $params['case_contact_id']
                    != '') ? $params['case_contact_id'] : '';

            $encryptedContactId = (isset($params['contact_id']) && $params['contact_id']
                    != '') ? $params['contact_id'] : '';

            $viewModel = new ViewModel();
            $viewModel->setVariables(array(
                'jsLangTranslate' => $caseCreateMessages ,
                'create_case_form' => $createCaseForm ,
                'case_contact_id' => $caseContactId ,
                'encrypt_case_contact_id' => $encryptedCaseContactId ,
                'encryptedContactId' => $encryptedContactId ,
                'caseTypeDesc' => $caseTypeDesc ,
                'caseStatusDesc' => $caseStatusDesc ,
                'contactDetail' => $contactDetail ,
                'transaction_id' => $transaction_id
            ));

            return $viewModel;
        }
    }

    public function moveCaseFileFromTempToCase($filename, $caseid) {
        $this->getConfig();
        $temp_dir = $this->_config['file_upload_path']['temp_upload_dir'];
        $case_dir = $this->_config['file_upload_path']['assets_upload_dir'] . "case/";
        $case_doc_dir = $case_dir . $caseid . "/";
        if (!is_dir($case_doc_dir)) {
            mkdir($case_doc_dir, 0777, TRUE);
        }
        $temp_file = $temp_dir . $filename;
        $case_filename = $case_doc_dir . $filename;

        if (file_exists($temp_file)) {
            if (copy($temp_file, $case_filename)) {
                unlink($temp_file);
            }
        }
    }

    /**
     * This Action is used to save the case search
     * @param void
     * @return this will be json format with comfirmation message
     * @author Icreon Tech -DT
     */
    public function saveCaseSearchAction() {
        $this->checkUserAuthentication();
        $this->getCaseTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $cases = new Cases($this->_adapter);
        $caseSearchMessages = $this->_config['case_messages']['config']['case_search_message'];
        if ($request->isPost()) {
            parse_str($request->getPost('searchString'), $searchParam);
			
            $searchName = $request->getPost('search_name');
            $messages = array();
            parse_str($request->getPost('searchString'), $searchParam);
            $searchParam['search_title'] = $searchName;
			$searchParam['case_status'] = implode(",",$searchParam['case_status']);
            $searchParam = $cases->getInputFilterCaseSearch($searchParam);
			
			$searchName = $cases->getInputFilterCaseSearch($searchName);

            if (trim($searchName) != '') {
                $saveSearchParam['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $saveSearchParam['title'] = $searchName;
                $saveSearchParam['search_query'] = serialize($searchParam);
                $saveSearchParam['is_active'] = 1;
                $saveSearchParam['is_delete'] = '0';

                if ($request->getPost('search_id') != '') {
                    $saveSearchParam['modified_date'] = DATE_TIME_FORMAT;
                    $saveSearchParam['case_search_id'] = $request->getPost('search_id');
                    $caseUpdateArray = $cases->getAddCaseSearchArr($saveSearchParam);
                    if ($this->_caseTable->updateCaseSearch($caseUpdateArray) == true) {
                        $this->flashMessenger()->addMessage($caseSearchMessages['SAVE_UPDATED_MSG']);
                        $messages = array('status' => "success");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                } else {
                    $saveSearchParam['added_date'] = DATE_TIME_FORMAT;
                    $caseAddArray = $cases->getAddCaseSearchArr($saveSearchParam);
                    if ($this->_caseTable->saveCaseSearch($caseAddArray) == true) {
                        $this->flashMessenger()->addMessage($caseSearchMessages['SAVE_SUCCESS_MSG']);
                        $messages = array('status' => "success");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                }
            }
        }
    }

    /**
     * This Action is used to generte the select option after saving the new search title
     * @param void
     * @return this will be a string having the all options
     * @author Icreon Tech - DT
     */
    public function getSavedCaseSearchSelectAction() {
        $this->checkUserAuthentication();
        $this->getCaseTable();
        $cases = new Cases($this->_adapter);
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $searchArray = array();
            $searchArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
            $searchArray['case_search_id'] = '';
            $searchArray['is_active'] = '1';
            $caseSavedSearchArray = $cases->getCaseSavedSearchArr($searchArray);
            $caseSearchArray = $this->_caseTable->getCaseSavedSearch($caseSavedSearchArray);
            $options = '<option value="">Select</option>';
            foreach ($caseSearchArray as $key => $val) {
                $options .="<option value='" . $val['case_search_id'] . "'>" . $val['title'] . "</option>";
            }
            return $response->setContent($options);
        }
    }

    /**
     * This Action is used to get the saved case search
     * @param void
     * @return this will be json format
     * @author Icreon Tech - DT
     */
    public function getCaseSearchSavedParamAction() {
        $this->checkUserAuthentication();
        $this->getCaseTable();
        $cases = new Cases($this->_adapter);
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            if ($request->getPost('search_id') != '') {

                $searchArray = array();
                $searchArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $searchArray['case_search_id'] = $request->getPost('search_id');
                $searchArray['is_active'] = '1';
                $caseSavedSearchArray = $cases->getCaseSavedSearchArr($searchArray);
                $caseSearchArray = $this->_caseTable->getCaseSavedSearch($caseSavedSearchArray);
				
                $searchResult = json_encode(unserialize($caseSearchArray[0]['search_query']));
                return $response->setContent($searchResult);
            }
        }
    }

    /**
     * This Action is used to delete the the saved case search
     * @param void
     * @return this will be a confirmation message in json
     * @author Icreon Tech - DT
     */
    public function deleteCaseSearchAction() {
        $this->checkUserAuthentication();
        $this->getCaseTable();
        $cases = new Cases($this->_adapter);
        $request = $this->getRequest();
        $response = $this->getResponse();
        $caseSearchMessages = $this->_config['case_messages']['config']['case_search_message'];
        if ($request->isPost()) {
            $deleteArray = array();
            $deleteArray['is_deleted'] = 1;
            $deleteArray['modified_date'] = DATE_TIME_FORMAT;
            $deleteArray['case_search_id'] = $request->getPost('search_id');
            $case_delete_saved_search_array = $cases->getDeleteCaseSavedSearchArr($deleteArray);
            $this->_caseTable->deleteSavedSearch($case_delete_saved_search_array);
            $messages = array('status' => "success");
            $this->flashMessenger()->addMessage($caseSearchMessages['SEARCH_DELETE_CONFIRM']);
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This action is used to delete case
     * @param  void
     * @return     json format string
     * @author Icreon Tech - DT
     */
    public function deleteCaseAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $caseContactId = $request->getPost('case_contact_id');
            if ($caseContactId != '') {
                $caseContactId = $this->decrypt($caseContactId);
                $postArr = $request->getPost();
                $postArr['case_contact_id'] = $caseContactId;
                $this->getCaseTable();
                $caseSearchMessage = $this->_config['case_messages']['config']['case_search_message'];
                $cases = new Cases($this->_adapter);

                $response = $this->getResponse();
                /* Contact in group grid data */

                $caseArr = $cases->getArrayForRemoveCase($postArr);
                $this->_caseTable->removeCaseById($caseArr);
                /** insert into the change log */
                $changeLogArray = array();
                $changeLogArray['table_name'] = 'tbl_cas_change_logs';
                $changeLogArray['activity'] = '3';/** 1 == for insert */
                $changeLogArray['id'] = $caseContactId;
                $changeLogArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                /** end insert into the change log */
                $this->flashMessenger()->addMessage($caseSearchMessage['CASE_DELETED_SUCCESSFULLY']);
                $messages = array('status' => "success", 'message' => $caseSearchMessage['CASE_DELETED_SUCCESSFULLY']);

                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                return $this->redirect()->toRoute('cases');
            }
        } else {
            return $this->redirect()->toRoute('cases');
        }
    }

    /**
     * This Action is used to set post array values to elements value of form
     * @param $postArr Array,$form Array
     * @return void
     * @author Icreon Tech -DT
     */
    public function setValueForElement($postArr = array(), $form = array()) {
        if (!is_array($postArr)) {
            $postArr = $postArr->toArray();
        }
        $elements = $form->getElements();
        if ($postArr['contact_name'] == '' && $postArr['company_name'] != '')
            $postArr['contact_name'] = $postArr['company_name'];
        foreach ($elements as $elementName => $elementObject) {

            if (array_key_exists($elementName, $postArr)) {
                if (is_array($postArr[$elementName])) {
                    $value = implode(',', $postArr[$elementName]);
                } else {					
                    $value = $postArr[$elementName];
                }
                $elementObject->setValue($value);
            }
        }
    }

    /**
     * This Action is used to add case to archive
     * @param void
     * @return this will be json format
     * @author Icreon Tech -DT
     */
    public function addCaseToArchiveAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();

        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $postArray = $request->getPost()->toArray();
            if (isset($postArray['case_contact_id']) && $postArray['case_contact_id'] != '') {

                $caseContactId = $this->decrypt($postArray['case_contact_id']);
                $postArray['case_contact_id'] = $caseContactId;
                $caseArr = array();
                $caseArr['case_contact_id'] = $caseContactId;
                $this->getCaseTable();

                $create_messages = $this->_config['case_messages']['config']['case_create_message'];

                $cases = new Cases($this->_adapter);

                $response = $this->getResponse();

                $addToArchiveArr = $cases->getArrayForAddToArchiveCase($postArray);

                $this->_caseTable->addToArchive($addToArchiveArr);

                $this->flashMessenger()->addMessage($create_messages['CASE_UPDATED_ARCHIVE']);
                $messages = array('status' => "success", 'message' => $create_messages['CASE_UPDATED_ARCHIVE']);

                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                return $this->redirect()->toRoute('addCase');
            }
        } else {
            return $this->redirect()->toRoute('addCase');
        }
    }

    /**
     * This action is used to show all case detail
     * @return     array
     * @author Icreon Tech - DT
     */
    public function getCaseDetailAction() {
        $this->checkUserAuthentication();
        $this->getCaseTable();
        $caseSearchMessages = $this->_config['case_messages']['config']['case_search_message'];
        $caseNoteForm = new CaseNotesForm();
        $caseReplyForm = new CaseReplyForm();
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $this->checkUserAuthenticationAjx();
            $cases = new Cases($this->_adapter);
            $postreply = $request->getPost('postreply');
            $response = $this->getResponse();
            if (!empty($postreply)) {
                //For reply
                $caseReplyForm->setInputFilter($cases->getInputFilterReplyCase());
                $caseReplyForm->setData($request->getPost());
                if (!$caseReplyForm->isValid()) {
                    $errors = $caseReplyForm->getMessages();
                    $messages = $msg = array();
                    foreach ($errors as $key => $row) {
                        if (!empty($row) && $key != 'continue' && $key!='case_status_reply') {
                            foreach ($row as $typeError => $rower) {
                                $msg [$key] = $caseSearchMessages[$rower];
                            }
                        }
                    }
					if(!empty($msg))
						$messages = array('status' => "error", 'message' => $msg);
                }

                if (!empty($messages)) {
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                } else {
                    $replyCaseFormArr = $caseReplyForm->getData();
					/*******/
					$caseDetailArray = array();
					$caseDetailArray[] = $replyCaseFormArr['case_contact_id'];
					$caseDetailArray[] = '';
					
					//asd($replyCaseFormArr);
					/*if(empty($caseReplies)){*/
						
						$caseDetailArr = array();
						$caseDetailArr[] = $replyCaseFormArr['case_contact_id'];
						$caseData = $this->_caseTable->getCaseById($caseDetailArr);

						$to_update = 0;
						
						if($request->getPost('case_status_reply')!=$caseData['case_status']){
							$caseData['modify_by'] = $this->_auth->getIdentity()->crm_user_id;
							$caseData['modify_date'] = DATE_TIME_FORMAT;
							$caseData['case_status'] = $request->getPost('case_status_reply');
							$to_update = 1;
						}
						


						if($caseData['start_date']=='0000-00-00 00:00:00' || $caseData['start_date']=='' || is_null($caseData['start_date'])){
							$caseReplies = $this->_caseTable->getCaseReplies($caseDetailArray);
							if(empty($caseReplies)){
								$caseData['start_date'] = date("Y-m-d");
								$caseData['start_time'] = date("H:i:s");
								$caseData['modify_by'] = $this->_auth->getIdentity()->crm_user_id;
								$caseData['modify_date'] = DATE_TIME_FORMAT;
								$to_update = 1;
							}
						}						
						if($to_update == 1){
							$editCaseArr = $cases->getCreateEditArr($caseData);
							$this->_caseTable->editCase($editCaseArr);
						}
					//}
					/*******/

                    //for add reply
                    $replyCaseFormArr['is_admin_reply'] = 1;
                    $replyCaseFormArr['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                    $replyCaseFormArr['added_date'] = DATE_TIME_FORMAT;
                    $replyCaseFormArr['modify_date'] = DATE_TIME_FORMAT;
                    $replyCaseArr = $cases->getCaseReplyArr($replyCaseFormArr);
                    $replyId = $this->_caseTable->saveReply($replyCaseArr);

					
                    $replyArr = array();
                    $replyArr[] = '';
                    $replyArr[] = $replyId;
                    $replyData = $this->_caseTable->getCaseReplies($replyArr);
                    $loginUserDetail = $this->_auth->getIdentity();

                    // n - start
                    /*$case_status_n = $request->getPost('case_status');
                    $case_status_n = (isset($case_status_n) and trim($case_status_n) != "") ? trim($case_status_n) : "";
                    $case_msg_disp = "1";
                    if (isset($replyCaseFormArr['case_contact_id']) and trim($replyCaseFormArr['case_contact_id']) != "" and $case_status_n != "" and $case_status_n == "6") {
                        $caseDataUpdt = array();
                        $caseDataUpdt['transaction_id'] = "";
                        $caseDataUpdt['case_status'] = "7";
                        $caseDataUpdt['case_id'] = (isset($replyCaseFormArr['case_contact_id']) and trim($replyCaseFormArr['case_contact_id']) != "") ? trim($replyCaseFormArr['case_contact_id']) : "";
                        $this->_caseTable->updateUserCase($caseDataUpdt);
                        $case_msg_disp = "2";
                    }*/
                    // n - end

                    $replydivClass = $request->getPost('replydivClass');
                    $replydivClass = isset($replydivClass) ? $replydivClass : 'alterbg alterbg-none';
                    $fullname = (isset($replyData[0]['is_admin_reply']) && $replyData[0]['is_admin_reply'] == 1) ? ($loginUserDetail->crm_first_name . " " . $loginUserDetail->crm_last_name) : $replyData[0]['fullname'];
                    $appendText = '<div class="row ' . $replydivClass . '"><p class="text">' . nl2br($replyData[0]['reply']) . '</p><p class="text right no-padd"><strong>' . $caseSearchMessages['L_DATE_CREATED'] . ':</strong> ' . $this->OutputDateFormat($replyData[0]['added_date'], 'dateformatampm') . ' ' . $caseSearchMessages['L_BY'] . ' ' . $fullname . '</p></div>';
                    $messages = array('status' => "success", 'message' => $appendText, 'case_msg_disp' => $case_msg_disp);
                }
            } else {
                //For note
                $caseNoteForm->setInputFilter($cases->getInputFilterNoteCase());
                $caseNoteForm->setData($request->getPost());
                if (!$caseNoteForm->isValid()) {
                    $errors = $caseNoteForm->getMessages();
                    $msg = array();
                    foreach ($errors as $key => $row) {
                        if (!empty($row) && $key != 'continue') {
                            foreach ($row as $typeError => $rower) {
                                $msg [$key] = $caseSearchMessages[$rower];
                            }
                        }
                    }
                    $messages = array('status' => "error", 'message' => $msg);
                }

                if (!empty($messages)) {
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                } else {
                    $noteCaseFormArr = $caseNoteForm->getData();
                    //for add note
                    $noteCaseFormArr['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                    $noteCaseFormArr['added_date'] = DATE_TIME_FORMAT;
                    $noteCaseFormArr['modify_date'] = DATE_TIME_FORMAT;
                    $noteCaseArr = $cases->getCaseNoteArr($noteCaseFormArr);
                    $noteId = $this->_caseTable->saveNote($noteCaseArr);
                    $noteArr = array();
                    $noteArr[] = '';
                    $noteArr[] = $noteId;
                    $noteData = $this->_caseTable->getCaseNotes($noteArr);

                    $replydivClass = $request->getPost('replydivClass');
                    $replydivClass = isset($replydivClass) ? $replydivClass : 'alterbg alterbg-none';

                    $appendText = '<div class="row ' . $replydivClass . '"><p class="text">' . nl2br($noteData[0]['note']) . '</p><p class="text right no-padd"><strong>' . $caseSearchMessages['L_DATE_CREATED'] . ':</strong> ' . $this->OutputDateFormat($noteData[0]['added_date'], 'dateformatampm') . ' ' . $caseSearchMessages['L_BY'] . ' ' . $noteData[0]['fullname'] . '</p></div>';
                    //$this->flashMessenger()->addMessage($caseSearchMessages['NOTE_ADD_SUCCESSFULLY']);
                    $messages = array('status' => "success", 'message' => $appendText);
                }
            }
            $caseContactId = $request->getPost('case_contact_id');
            $caseDetailArr = array();
            $caseDetailArr[] = $caseContactId;
            $caseData = $this->_caseTable->getCaseById($caseDetailArr);

            if ((isset($replyCaseFormArr['notify']) && $replyCaseFormArr['notify'] == 1) && $caseData['email_id'] != '') {
                $caseReplyDataArr = array();
                $caseReplyDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                $caseReplyDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                $caseReplyDataArr['contact_case_id'] = $request->getPost('case_contact_id');
                $caseReplyDataArr['contact_name'] = $caseData['contact_name'];
                $caseReplyDataArr['email_id'] = $caseData['email_id'];
                $caseReplyDataArr['first_name'] = $caseData['first_name'];
                $caseReplyDataArr['last_name'] = $caseData['last_name'];
                $caseReplyDataArr['message'] = $request->getPost('reply');
                $caseReplyDataArr['contact_case_reply_note_link'] = "<a href='" . SITE_URL . "/get-case-detail/" . $this->encrypt($request->getPost('case_contact_id')) . "/" . $this->encrypt('view') . "'>" . SITE_URL . "/get-case-detail/" . $this->encrypt($request->getPost('case_contact_id')) . "/" . $this->encrypt('view') . "</a>";
                $email_id_template_int = 22;
                $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($caseReplyDataArr, $email_id_template_int, $this->_config['email_options']);
            }
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        } else if (isset($params['case_contact_id']) && $params['case_contact_id'] != '') {
            $this->checkUserAuthentication();
            $this->layout('crm');
            $this->getCaseTable();
            $caseContactId = $this->decrypt($params['case_contact_id']);
            $caseDetailArr = array();
            $caseDetailArr[] = $caseContactId;
            $caseData = $this->_caseTable->getCaseById($caseDetailArr);
            $replyArr = array();
            $replyArr[] = $caseContactId;
            $replyArr[] = '';
            $replyData = $this->_caseTable->getCaseReplies($replyArr);
            $noteArr = array();
            $noteArr[] = $caseContactId;
            $noteArr[] = '';
            $noteData = $this->_caseTable->getCaseNotes($noteArr);

            $caseNoteForm->get('case_contact_id')->setValue($caseContactId);
            $caseReplyForm->get('case_contact_id')->setValue($caseContactId);
            $mode = (isset($params['mode']) && $params['mode'] != '') ? $this->decrypt($params['mode']) : 'view';
            //echo "<pre>";print_r($noteData);die;
            /* Get number of activity in user id in this particular case */
            $activityArr = array();
            $activityArr['sourceType'] = 4;
            $activityArr['activitySourceId'] = $caseData['case_contact_id'];
            $activityArr['userId'] = '';
            $activityCount = $this->getServiceLocator()->get('Activity\Model\ActivityTable')->getActivityCount($activityArr);
            /* End get number of activity in user id in this particular case */
            $viewModel = new ViewModel();
            $viewModel->setVariables(array(
                'jsLangTranslate' => $caseSearchMessages,
                'case_note_form' => $caseNoteForm,
                'case_reply_form' => $caseReplyForm,
                'case_contact_id' => $caseContactId,
                'encrypt_case_contact_id' => $params['case_contact_id'],
                'case_data' => $caseData,
                'encrypted_contact_name_id' => $this->encrypt($caseData['contact_name_id']),
                'case_replies' => $replyData,
                'case_notes' => $noteData,
                'module_name' => $this->encrypt('case'),
                'mode' => $mode,
                'totalActivityCount' => $activityCount,
                'login_user_detail' => $this->_auth->getIdentity(),
                'encrypted_source_type_id' => $this->encrypt('4')
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to show view case detail on ajax call
     * @return     array
     * @author Icreon Tech - DT
     */
    public function viewCaseDetailAction() {
        $this->checkUserAuthentication();
        $this->getCaseTable();
        $caseSearchMessages = $this->_config['case_messages']['config']['case_search_message'];
        $caseNoteForm = new CaseNotesForm();
        $caseReplyForm = new CaseReplyForm();
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();
        if (isset($params['case_contact_id']) && $params['case_contact_id'] != '') {
            $this->getCaseTable();
            $caseContactId = $this->decrypt($params['case_contact_id']);
            $caseDetailArr = array();
            $caseDetailArr[] = $caseContactId;
            $caseData = $this->_caseTable->getCaseById($caseDetailArr);
			if (isset($caseData['is_viewed']) && $caseData['is_viewed'] == 0) {
                $this->_caseTable->updateUserViewCase($caseDetailArr);
            }
            $fileParam = array();
            $fileParam['case_contact_id'] = $caseContactId;
            $caseFileData = $this->_caseTable->getCaseFiles($fileParam);

            $replyArr = array();
            $replyArr[] = $caseContactId;
            $replyArr[] = '';
            $replyData = $this->_caseTable->getCaseReplies($replyArr);
            $adminReplyData = $this->_caseTable->getUserCaseReplies($replyArr); // for

            if (isset($replyData) && !empty($replyData)) {
                foreach ($replyData as $key => $val) {
                    if ($val['is_admin_reply'] == 1 && $val['reply_id'] == $adminReplyData[$key]['reply_id'])
                        $replyData[$key]['fullname'] = $adminReplyData[$key]['fullname'];
                }
            }
            $noteArr = array();
            $noteArr[] = $caseContactId;
            $noteArr[] = '';
            $noteData = $this->_caseTable->getCaseNotes($noteArr);

            $caseNoteForm->get('case_contact_id')->setValue($caseContactId);
            $caseReplyForm->get('case_contact_id')->setValue($caseContactId);

			$caseStatusArr = array();
			$caseStatusArr[] = '';
			$getCaseStatus = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCaseStatus($caseStatusArr);
			$caseStatus = array();
			
			$caseStatus[''] = 'Any';
			if ($getCaseStatus !== false) {
				foreach ($getCaseStatus as $key => $val) {
					$caseStatus[$val['case_status_id']] = $val['case_status'];
				}
			}
		
			$caseReplyForm->get('case_status_reply')->setAttribute('options', $caseStatus);

			$case_status = !is_null($caseData['case_status'])?$caseData['case_status']:'';

			$caseReplyForm->get('case_status_reply')->setValue($caseData['case_status']);
			

            /** add private note* */
            $moduleInfoArr = array();
            $moduleInfoArr[] = 'Cases';
            $moduleInfoArr[] = 'Case';
            $moduleInfoArr[] = 'create-case-notes';
            $moduleDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getModuleAction($moduleInfoArr);

            $auth = new AuthenticationService();

            $userDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getCrmUserDetails(array('crm_user_id' => $auth->getIdentity()->crm_user_id));

            $roleDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->checkCrmUserPermission($userDetail[0]['role_id']);

            $addNotePermission = isset($roleDetail['moduleActionPermissions'][$moduleDetail['module_id']][$moduleDetail['action_id']]) ? $roleDetail['moduleActionPermissions'][$moduleDetail['module_id']][$moduleDetail['action_id']] : 1;
            /** end of add private note * */
            /** view private note* */
            $moduleInfoArr = array();
            $moduleInfoArr[] = 'Cases';
            $moduleInfoArr[] = 'Case';
            $moduleInfoArr[] = 'view-case-notes';
            $moduleDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getModuleAction($moduleInfoArr);

            $auth = new AuthenticationService();

            $userDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getCrmUserDetails(array('crm_user_id' => $auth->getIdentity()->crm_user_id));

            $roleDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->checkCrmUserPermission($userDetail[0]['role_id']);

            $viewNotePermission = isset($roleDetail['moduleActionPermissions'][$moduleDetail['module_id']][$moduleDetail['action_id']]) ? $roleDetail['moduleActionPermissions'][$moduleDetail['module_id']][$moduleDetail['action_id']] : 1;
            /** end of view private note * */
            $case_dir = $this->_config['file_upload_path']['assets_url'] . "case/";
            $case_file_dir = $case_dir . $caseContactId . "/";
			
			$replyNoteCount = $this->_caseTable->getCaseReplyNoteCount($caseContactId);
			
			$viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => $caseSearchMessages,
                'case_note_form' => $caseNoteForm,
                'case_reply_form' => $caseReplyForm,
                'case_contact_id' => $caseContactId,
                'encrypt_case_contact_id' => $params['case_contact_id'],
                'case_data' => $caseData,
                'case_replies' => $replyData,
                'case_notes' => $noteData,
                'login_user_detail' => $this->_auth->getIdentity(),
                'addNotePermission' => $addNotePermission,
                'viewNotePermission' => $viewNotePermission,
                'caseFileData' => $caseFileData,
                'case_file_dir' => $case_file_dir,
				'replyNoteCount' => $replyNoteCount
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to get edit case detail page
     * @return string
     * @param void
     * @author Icreon Tech - DT
     */
    public function editCaseDetailAction() {
        $this->checkUserAuthentication();
        $this->getCaseTable();
        $caseCreateMessages = $this->_config['case_messages']['config']['case_create_message'];
        $cases = new Cases($this->_adapter);

        $createCaseForm = new CaseForm();
        $request = $this->getRequest();
        $response = $this->getResponse();

        $form_upload = new ContactUploadForm();
        $createCaseForm->add($form_upload);

        /* Case mode type */
        $getCaseModeType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCaseModeType();
        $modeType = array();
        if ($getCaseModeType !== false) {
            foreach ($getCaseModeType as $key => $val) {
                $modeType[$val['case_mode_type_id']] = $val['case_mode_type'];
            }
        }
        $createCaseForm->get('mode')->setAttribute('options', $modeType);
        /* End  Case mode type */


        $createCaseForm->get('continue')->setValue('UPDATE');

        $caseContactId = '';
        $params = $this->params()->fromRoute();
        if (isset($params['case_contact_id']) && $params['case_contact_id'] != '') {
            $caseContactId = $this->decrypt($params['case_contact_id']);
            $caseDetailArr = array();
            $caseDetailArr[] = $caseContactId;
            $getCaseDetail = $this->_caseTable->getCaseById($caseDetailArr);
            /* Case type */
            $caseTypeArr = array();
            $caseTypeArr[] = $getCaseDetail['case_type'];
            $getCaseType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCaseTypes($caseTypeArr);
            $caseType = array();
            $caseTypeDesc = array();
            $caseType[''] = 'Select';
            if ($getCaseType !== false) {
                foreach ($getCaseType as $key => $val) {
                    $caseType[$val['case_type_id']] = $val['case_type'];
                    $caseTypeDesc[$val['case_type_id']] = stripslashes($val['type_description']);
                }
            }
            $createCaseForm->get('case_type')->setAttribute('options', $caseType);
            /* End  Case type */

            /* Case status */
            $caseStatusArr = array();
            $caseStatusArr[] = $getCaseDetail['case_status'];
            $getCaseStatus = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCaseStatus($caseStatusArr);
            $caseStatus = array();
            $caseStatusDesc = array();
            $caseStatus[''] = 'Select';
            if ($getCaseStatus !== false) {
                foreach ($getCaseStatus as $key => $val) {
                    $caseStatus[$val['case_status_id']] = $val['case_status'];
                    $caseStatusDesc[$val['case_status_id']] = stripslashes($val['status_description']);
                }
            }
            $createCaseForm->get('case_status')->setAttribute('options', $caseStatus);
            /* End  Case status */
            /* user transaction */
            $searchParam = array();
            $searchParam['user_id'] = $getCaseDetail['contact_name_id'];
            $searchParam['sort_field'] = 'usr_transaction.transaction_date';
            $transactionArr = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getUserTransactions($searchParam);
            $userTransactionArr = array();
            $userTransactionArr[''] = 'Select';
            if ($transactionArr !== false) {
                foreach ($transactionArr as $key => $val) {
                    $userTransactionArr[$val['transaction_id']] = $val['transaction_id'];
                }
            }
            $createCaseForm->get('transaction_id')->setAttribute('options', $userTransactionArr);
            /* end of user transaction */
            if ($getCaseDetail['start_date'] != 0) {
                $startDateArr = explode(' ', $this->OutputDateFormat($getCaseDetail['start_date'], 'dateformatampm'));
				
                $getCaseDetail['start_date'] = $startDateArr[0];
				$getCaseDetail['start_time'] = $startDateArr[1] . " " . $startDateArr[2];
            } else {
                $getCaseDetail['start_date'] = '';
                $getCaseDetail['start_time'] = '';
            } 
			$this->setValueForElement($getCaseDetail, $createCaseForm);
            $createCaseForm->get('case_contact_id')->setValue($caseContactId);

            $fileParam = array();
            $fileParam['case_contact_id'] = $caseContactId;
            $caseFileData = $this->_caseTable->getCaseFiles($fileParam);
            $case_dir = $this->_config['file_upload_path']['assets_url'] . "case/";
            $case_file_dir = $case_dir . $caseContactId . "/";
        }
        $encryptedCaseContactId = (isset($params['case_contact_id']) && $params['case_contact_id'] != '') ? $params['case_contact_id'] : '';
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => $caseCreateMessages,
            'create_case_form' => $createCaseForm,
            'case_contact_id' => $caseContactId,
            'encrypt_case_contact_id' => $encryptedCaseContactId,
            'caseTypeDesc' => $caseTypeDesc,
            'caseStatusDesc' => $caseStatusDesc,
            'caseFileData' => $caseFileData,
            'case_file_dir' => $case_file_dir
        ));

        return $viewModel;
    }

    /**
     * This action is used to listing of contact cases
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function getContactCasesAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $this->getCaseTable();
        $caseSearchMessages = $this->_config['case_messages']['config']['case_search_message'];
        $searchCaseForm = new ContactSearchCaseForm();
        $cases = new Cases($this->_adapter);
        /* Case type */
        $caseTypeArr = array();
        $caseTypeArr[] = '';
        $getCaseType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCaseTypes($caseTypeArr);
        $caseType = array();
        $caseType[''] = 'Any';
        if ($getCaseType !== false) {
            foreach ($getCaseType as $key => $val) {
                $caseType[$val['case_type_id']] = $val['case_type'];
            }
        }
        $searchCaseForm->get('case_type')->setAttribute('options', $caseType);
        /* End  Case type */

        /* Case status */
        $caseStatusArr = array();
        $caseStatusArr[] = '';
        $getCaseStatus = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCaseStatus($caseStatusArr);
        $caseStatus = array();
        $caseStatus[''] = 'Any';
        if ($getCaseStatus !== false) {
            foreach ($getCaseStatus as $key => $val) {
                $caseStatus[$val['case_status_id']] = $val['case_status'];
            }
        }
        $searchCaseForm->get('case_status')->setAttribute('options', $caseStatus);
        /* End  Case status */
        /* Get date range */
        $caseDateRange = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTransactionRange();
        $dateRange = array("" => "Select One");
        foreach ($caseDateRange as $key => $val) {
            $dateRange[$val['range_id']] = $val['range'];
        }
        $searchCaseForm->get('added_date_range')->setAttribute('options', $dateRange);
        /* end Get date range */
        $sortorder = $request->getPost('sord');
        if ($request->isXmlHttpRequest() && $request->isPost() && !empty($sortorder)) {
            $response = $this->getResponse();
            $postArray = $request->getPost()->toArray();
            parse_str($request->getPost('searchString'), $searchParam);
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit; // do not put $limit*($page - 1)
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            $searchParam['contact_id'] = $request->getPost('contact_id');
            if (isset($searchParam['added_date_range']) && $searchParam['added_date_range'] != 1 && $searchParam['added_date_range'] != '') {
                $dateRange = $this->getDateRange($searchParam['transaction_amount_date']);
                $searchParam['added_date_from'] = $dateRange['from'];
                $searchParam['added_date_to'] = $dateRange['to'];
            }
            $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'modified_date') ? 'tbl_case.modified_date' : $searchParam['sort_field'];
            $searchCaseArr = $cases->getCaseSearchArr($searchParam);
            $seachResult = $this->_caseTable->getAllCase($searchCaseArr);
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $jsonResult['page'] = $request->getPost('page');
            $jsonResult['records'] = $countResult[0]->RecordCount;
            $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
            if (!empty($seachResult)) {
                foreach ($seachResult as $val) {
                    $arrCell['id'] = $val['case_contact_id'];
                    $encryptId = $this->encrypt($val['case_contact_id']);
                    $viewLink = '<a href="/get-case-detail/' . $encryptId . '/' . $this->encrypt('view') . '" class="view-icon"><div class="tooltip">View<span></span></div></a>';
                    $editLink = '<a href="/get-case-detail/' . $encryptId . '/' . $this->encrypt('edit') . '" class="edit-icon"><div class="tooltip">Edit<span></span></div></a>';
                    $deleteLink = '<a onclick="deleteCase(\'' . $encryptId . '\')" href="#delete_case_content" class="delete_case delete-icon"><div class="tooltip">Delete<span></span></div></a>';
                    if ($val['is_archive'] == 0)
                        $archive_link = '<a onclick="addToArchieve(\'' . $encryptId . '\')" href="#add_to_archive_block" class="add_to_archive archiv-icon"><div class="tooltip">Archive<span></span></div></a>';
                    else
                        $archive_link = '';
                    $actions = '<div class="action-col">' . $viewLink . $editLink . $deleteLink . $archive_link . '</div>';
                    $arrCell['cell'] = array($val['case_contact_id'], $val['case_type'], $this->OutputDateFormat($val['added_date'], 'dateformatampm'), $val['case_subject'], $val['case_status'], $actions);
                    $subArrCell[] = $arrCell;
                    $jsonResult['rows'] = $subArrCell;
                }
            } else {
                $jsonResult['rows'] = array();
            }
            return $response->setContent(\Zend\Json\Json::encode($jsonResult));
        } else {
            $viewModel = new ViewModel();
            $contact_id = $request->getPost('contact_id');
            $response = $this->getResponse();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'form' => $searchCaseForm,
                'contact_id' => $this->decrypt($contact_id),
                'jsLangTranslate' => array_merge($caseSearchMessages, $this->_config['grid_config'])
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to get user transaction on change of user in autocomplete
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function getUserTransactionAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest() && $request->isPost()) {
            /* user transaction */
            $searchParam = array();
            $searchParam['user_id'] = $this->request->getPost('contact_name_id');
            $searchParam['sort_field'] = 'usr_transaction.transaction_date';
            $transactionArr = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getUserTransactions($searchParam);
            $options = "<option value=''>Select</option>";
            if ($transactionArr !== false) {
                foreach ($transactionArr as $key => $val) {
                    $options .="<option value='" . $val['transaction_id'] . "'>" . $val['transaction_id'] . "</option>";
                }
            }
            $response = $this->getResponse();
            return $response->setContent($options);
            /* end of user transaction */
        } else {
            return $this->redirect()->toRoute('cases');
        }
    }

    /**
     * This function is used to get subject autocomplete values
     * @return     json
     * @param void
     * @author Icreon Tech - DT
     */
    public function getCaseSubjectAction() {
        $this->checkUserAuthentication();
        $this->getCaseTable();
        $cases = new Cases($this->_adapter);
        $response = $this->getResponse();
        /* Crm User */
        $request = $this->getRequest();
        $postArr = $request->getPost()->toArray();
        $postArr['sort_field'] = 'tbl_case.modified_date';
        $postArr['sort_order'] = 'desc';
        $searchCaseArr = $cases->getCaseSearchArr($postArr);
        $seachResult = $this->_caseTable->getAllCase($searchCaseArr);
        $allSubjects = array();
        $i = 0;
        if ($seachResult !== false) {
            foreach ($seachResult as $subject) {
                $allSubjects[$i] = array();
                $allSubjects[$i]['id'] = $subject['case_contact_id'];
                $allSubjects[$i]['subject'] = $subject['case_subject'] . "( " . $subject['case_contact_id'] . " )";
                $i++;
            }
        }
        /* End Crm User */
        $response->setContent(\Zend\Json\Json::encode($allSubjects));
        return $response;
    }

    /**
     * This function is used to get user cases
     * @return     array
     * @param void
     * @author Icreon Tech - DG
     */
    public function userCasesAction() {
        $this->checkFrontUserAuthentication();
        $sm = $this->getServiceLocator();
        $this->_config = $sm->get('Config');
        $this->_adapter = $sm->get('dbAdapter');
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $this->layout('layout');
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $params = $this->params()->fromRoute();


        $cases = new Cases($this->_adapter);
        $searchParam['contact_id'] = $this->decrypt($params['user_id']);
        $searchParam['sort_field'] = "if(tbl_case.modified_date='0000-00-00 00:00:00',tbl_case.added_date,tbl_case.modified_date)";
        $searchParam['sort_order'] = 'desc';
        $searchCaseArr = $cases->getCaseSearchArr($searchParam);

        $userCases = $this->getServiceLocator()->get('Cases\Model\CasesTable')->getAllCase($searchCaseArr);

        $viewModel->setVariables(array(
            'userCases' => $userCases,
            'userId' => $params['user_id'],
            'jsLangTranslate' => $this->_config['case_messages']['config']['user_case']
                )
        );
        return $viewModel;
    }

    /**
     * This function is used to add user cases
     * @return     array
     * @param void
     * @author Icreon Tech - DG
     */
    public function addUserCaseAction() {
        $this->checkFrontUserAuthentication();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $this->layout('web-popup');
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $sm = $this->getServiceLocator();
        $this->_config = $sm->get('Config');
        $this->_adapter = $sm->get('dbAdapter');
        $this->_auth = new AuthenticationService();
        $caseCreateMessages = $this->_config['case_messages']['config']['case_create_message'];

        $params = $this->params()->fromRoute();
        $passengerDetails = '';
        $report_an_issue = '';
        if (isset($params['passenger_id']) && !empty($params['passenger_id'])) {
            $searchParam['passenger_id'] = $this->decrypt($params['passenger_id']);
            $passengerResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getPassengerRecord($searchParam);
            $passengerManifestResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getPassengerManifest($searchParam);
            $passengerDetails.=$caseCreateMessages['L_PASSENGER_ID'] . ': ' . $passengerResult[0]['ID'] . "\r\n";
            $passengerDetails.=$caseCreateMessages['L_FIRST_NAME'] . ': ' . stripslashes($passengerResult[0]['PRIN_FIRST_NAME']) . "\r\n";
            $passengerDetails.=$caseCreateMessages['L_LAST_NAME'] . ': ' . stripslashes($passengerResult[0]['PRIN_LAST_NAME']) . "\r\n";
            $passengerDetails.=$caseCreateMessages['L_SHIP_NAME'] . ': ' . stripslashes($passengerResult[0]['SHIP_NAME']) . "\r\n";
            $passengerDetails.=$caseCreateMessages['L_ARRIVAL_DATE'] . ': ' . $this->DateFormat($passengerResult[0]['DATE_ARRIVE'], 'calender') . "\r\n";
            $passengerDetails.=$caseCreateMessages['L_FRAME'] . ': ' . $passengerManifestResult[0]['FRAME'] . "\r\n";
            $passengerDetails.=$caseCreateMessages['L_LINE_NUMBER'] . ': ' . $passengerResult[0]['REF_PAGE_LINE_NBR'] . "\r\n";
            $passengerDetails.=$caseCreateMessages['L_PORT_OF_DEPARTURE'] . ': ' . stripslashes($passengerResult[0]['DEP_PORT_NAME']) . "\r\n";
            $report_an_issue = 1;
        }


        $cases = new Cases($this->_adapter);
        $createCaseForm = new UserCaseForm();

        /* Case type */
        $caseTypeArr = array();
        $caseTypeArr[] = '';
        $getCaseType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCaseTypes($caseTypeArr);
        $caseType = array();
        $caseType[''] = 'Select';
        if ($getCaseType !== false) {
            foreach ($getCaseType as $key => $val) {
                $caseType[$val['case_type_id']] = $val['case_type'];
            }
        }
        $createCaseForm->get('case_type')->setAttribute('options', $caseType);
        $createCaseForm->get('contact_name_id')->setValue($this->decrypt($params['user_id']));
        //$createCaseForm->get('message')->setValue($passengerDetails);
        /* End  Case type */

        if ($request->isPost()) {
            parse_str($request->getPost('dataStr'), $caseParam);
            $createCaseForm->setData($caseParam);
            $createCaseForm->setInputFilter($cases->getInputFilterUserCreateCase());
            if (!$createCaseForm->isValid()) {
                $errors = $createCaseForm->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'continue') {
                        foreach ($row as $typeError => $rower) {
                            if ($typeError != 'notInArray') {
                                $msg [$key] = $caseCreateMessages[$rower];
                            }
                        }
                    }
                }
            }

            if (!empty($msg)) {
                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $createCaseFormArr = $createCaseForm->getData();
                $userid = is_numeric($params['user_id']) ? $params['user_id'] : $this->decrypt($params['user_id']);
                $usrTransactionArr = $sm->get('Transaction\Model\TransactionTable')->getUserTransactions(array('user_id' => $userid));
                if (!empty($usrTransactionArr) && $createCaseFormArr['transaction_id'] != '') {
                    foreach ($usrTransactionArr as $val) {
                        $userTransactions[] = $val['transaction_id'];
                    }

                    if (!in_array($createCaseFormArr['transaction_id'], $userTransactions)) {
                        $messages = array('status' => "error", 'message' => $caseCreateMessages['INVALID_TRANSACTION_ID'], 'transaction' => true);
                        $response->setContent(\Zend\Json\Json::encode($messages));
                        return $response;
                    }
                }
                //for add
                $createCaseFormArr['added_by'] = $this->_auth->getIdentity()->user_id;
                $createCaseFormArr['added_date'] = DATE_TIME_FORMAT;
                #$createCaseFormArr['modify_by'] = $this->_auth->getIdentity()->user_id;
                #$createCaseFormArr['modify_date'] = DATE_TIME_FORMAT;
                $createCaseFormArr['start_date'] = '';
                $createCaseFormArr['start_time'] = '';
                $createCaseFormArr['case_status'] = 1; //Open
                
                if($request->getPost('passeneger_details')!='') {
                    $passenegerDetailsData = str_replace('<br>','',$request->getPost('passeneger_details'));
                    $createCaseFormArr['message'] = $passenegerDetailsData."\n".$caseParam['message']; 
                }
                else {
                    $createCaseFormArr['message'] = $caseParam['message']; 
                }
                $createCaseArr = $cases->getCreateCaseArr($createCaseFormArr);

                $caseId = $sm->get('Cases\Model\CasesTable')->saveCase($createCaseArr);
                $messages = array('status' => "success", 'message' => $caseCreateMessages['CASE_CREATED_SUCCESSFULLY'], 'caseId' => $caseId);
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $viewModel->setVariables(array(
            'create_case_form' => $createCaseForm,
            'jsLangTranslate' => $caseCreateMessages,
            'report_an_issue' => $report_an_issue,
            'passengerDetails' => $passengerDetails
                )
        );
        return $viewModel;
    }

    /**
     * This function is used to view user cases
     * @return     array
     * @param void
     * @author Icreon Tech - DG
     */
    public function viewUserCaseAction() {
        $this->checkFrontUserAuthentication();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $this->layout('web-popup');
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $sm = $this->getServiceLocator();
        $this->_config = $sm->get('Config');
        $this->_adapter = $sm->get('dbAdapter');
        $this->_auth = new AuthenticationService();
        $params = $this->params()->fromRoute();

        $caseCreateMessages = $this->_config['case_messages']['config']['case_create_message'];
        $cases = new Cases($this->_adapter);
        $caseReplyForm = new CaseReplyForm();

        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $cases = new Cases($this->_adapter);
            $postreply = $request->getPost('postreply');
            $response = $this->getResponse();
            if (!empty($postreply)) {
                $caseReplyForm->setInputFilter($cases->getInputFilterReplyCase());
                $caseReplyForm->setData($request->getPost());
                if (!$caseReplyForm->isValid()) {
                    $errors = $caseReplyForm->getMessages();
                    $msg = array();
                    foreach ($errors as $key => $row) {
                        if (!empty($row) && $key != 'continue' && $key!='case_status_reply') {
                            foreach ($row as $typeError => $rower) {
                                $msg [$key] = $caseSearchMessages[$rower];
                            }
                        }
                    }
					if(!empty($msg))
                    $messages = array('status' => "error", 'message' => $msg);
                }
                if (!empty($messages)) {
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                } else {
                    $replyCaseFormArr = $caseReplyForm->getData();
					
                    $replyCaseFormArr['is_admin_reply'] = 0;
                    $replyCaseFormArr['added_by'] = $this->_auth->getIdentity()->user_id;
                    $replyCaseFormArr['added_date'] = DATE_TIME_FORMAT;
                    $replyCaseFormArr['modify_date'] = DATE_TIME_FORMAT;
                    $replyCaseArr = $cases->getCaseReplyArr($replyCaseFormArr);

					// n - start
                    $case_status_n = $request->getPost('case_status');
                    $case_status_n = (isset($case_status_n) and trim($case_status_n) != "") ? trim($case_status_n) : "";
                    $case_msg_disp = "1";
                    if (isset($replyCaseFormArr['case_contact_id']) and trim($replyCaseFormArr['case_contact_id']) != "" and $case_status_n != "" and $case_status_n == "6") {
                        $caseDataUpdt = array();
                        $caseDataUpdt['transaction_id'] = "";
                        $caseDataUpdt['case_status'] = "7";
                        $caseDataUpdt['case_id'] = (isset($replyCaseFormArr['case_contact_id']) and trim($replyCaseFormArr['case_contact_id']) != "") ? trim($replyCaseFormArr['case_contact_id']) : "";
						$caseDataUpdt['modified_by'] = $this->_auth->getIdentity()->user_id;
						$caseDataUpdt['modified_date'] = DATE_TIME_FORMAT;
                        $sm->get('Cases\Model\CasesTable')->updateUserCase($caseDataUpdt);
                        $case_msg_disp = "2";
                    }
                    // n - end
					
					$replyArray = array();
					$replyArray[] = $replyCaseFormArr['case_contact_id'];
					$replyArray[] = '';
					$replyData = $sm->get('Cases\Model\CasesTable')->getUserCaseReplies($replyArray);

                    $replyId = $this->getServiceLocator()->get('Cases\Model\CasesTable')->saveReply($replyCaseArr);

					$caseDetailArr = array();
					$caseDetailArr[] = $replyCaseFormArr['case_contact_id'];
					$getCaseDetail = $sm->get('Cases\Model\CasesTable')->getCaseById($caseDetailArr);

					if($getCaseDetail['start_date']=='0000-00-00 00:00:00' || $getCaseDetail['start_date']=='' || is_null($getCaseDetail['start_date'])){	
						if(empty($replyData)){
							$updateData = array();
							$updateData['start_datetime'] = DATE_TIME_FORMAT;
							$updateData['case_id'] = $replyCaseFormArr['case_contact_id'];
							$updateData['modified_by'] = $this->_auth->getIdentity()->user_id;
							$updateData['modified_date'] = DATE_TIME_FORMAT;
							$sm->get('Cases\Model\CasesTable')->updateUserCase($updateData);
						}
					}

                    $replyArr[] = '';
                    $replyArr[] = $replyId;
                    $replyData = $this->getServiceLocator()->get('Cases\Model\CasesTable')->getUserCaseReplies($replyArr);
                    $loginUserDetail = $this->_auth->getIdentity();
                    if ($loginUserDetail->user_type == 2)
                        $fullname = $loginUserDetail->company_name;
                    else
                        $fullname = $loginUserDetail->first_name . " " . $loginUserDetail->last_name;

                    $appendText = '<div class="form-sep"><div class="full"><p>' . nl2br($replyData[0]['reply']) . '</p></div><div class="full"><p class="right no-margin"><strong>' . $caseCreateMessages['L_DATE_CREATED'] . ':</strong> ' . $this->OutputDateFormat($replyData[0]['added_date'], 'dateformatampm') . ' ' . $caseCreateMessages['L_BY'] . ' <strong>Customer Service</strong></p></div></div>'; //' . $fullname . '
                    $messages = array('status' => "success", 'message' => $appendText, 'case_msg_disp' => $case_msg_disp);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                }
            }
        } else {
            $caseDetailArr = array();
            $caseDetailArr[] = $params['case_id'];
            $getCaseDetail = $sm->get('Cases\Model\CasesTable')->getCaseById($caseDetailArr);
            $caseReplyForm->get('case_contact_id')->setValue($getCaseDetail['case_contact_id']);
            $replyArr = array();
            $replyArr[] = $params['case_id'];
            $replyArr[] = '';
            $replyData = $sm->get('Cases\Model\CasesTable')->getUserCaseReplies($replyArr);
            $viewModel->setVariables(array(
                'caseReplyForm' => $caseReplyForm,
                'getCaseDetail' => $getCaseDetail,
                'replyData' => $replyData,
                'jsLangTranslate' => $caseCreateMessages,
                'loginUserDetail' => $this->_auth->getIdentity()
                    )
            );
            return $viewModel;
        }
    }

    /**
     * This function is used to edit user cases
     * @return     array
     * @param void
     * @author Icreon Tech - DG
     */
    public function editUserCaseAction() {
        $this->checkFrontUserAuthentication();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $this->layout('web-popup');
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $sm = $this->getServiceLocator();
        $this->_config = $sm->get('Config');
        $this->_adapter = $sm->get('dbAdapter');
        $this->_auth = new AuthenticationService();
        $params = $this->params()->fromRoute();

        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $caseId = $params['case_id'];
            $transactionId = $request->getPost('transaction_id');
            $auth = new AuthenticationService();
            $this->auth = $auth;
            $userDetail = (array) $this->auth->getIdentity();
            $usrTransactionArr = $sm->get('Transaction\Model\TransactionTable')->getUserTransactions(array('user_id' => $userDetail['user_id']));
            if (!empty($usrTransactionArr) && $transactionId != '') {
                foreach ($usrTransactionArr as $val) {
                    $userTransactions[] = $val['transaction_id'];
                }
                if (!in_array($transactionId, $userTransactions)) {
                    $messages = array('status' => "error", 'message' => $this->_config['case_messages']['config']['case_create_message']['INVALID_TRANSACTION_ID'], 'transaction' => true);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                }
            }

            $caseData = array();
            $caseData['transaction_id'] = $request->getPost('transaction_id');
            $caseData['case_status'] = $request->getPost('case_status');
            $caseData['case_id'] = $caseId;
			$caseData['modified_by'] = $this->_auth->getIdentity()->user_id;
			$caseData['modified_date'] = DATE_TIME_FORMAT;
            $sm->get('Cases\Model\CasesTable')->updateUserCase($caseData);
            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    public function deleteCaseFileAction() {
        $this->getConfig();
        $this->getCaseTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $caseContactId = $this->decrypt($request->getPost('case_contact_id'));
            $case_attachment_id = $this->decrypt($request->getPost('case_attachment_id'));
            $file_name = $request->getPost('file_name');
            $case_dir = $this->_config['file_upload_path']['assets_upload_dir'] . "case/";
            $case_doc_dir = $case_dir . $caseContactId . "/";
            $casefilename = $case_doc_dir . $file_name;
            if (file_exists($casefilename)) {
                unlink($casefilename);
                $this->_caseTable->deleteCaseFiles($case_attachment_id);
                $messages = array('status' => "success");
                return $response->setContent(\Zend\Json\Json::encode($messages));
            }
        }
        $messages = array('status' => "error");
        return $response->setContent(\Zend\Json\Json::encode($messages));
    }

    public function uploadCaseFileAction() {
        $this->checkUserAuthentication();
        $form_upload_file = new ContactUploadForm();
        $this->getConfig();
        $params = $this->params()->fromRoute();
        $data['counter'] = $params['counter'];
        $data['container'] = $params['container'];
        $fieldName = 'publicdocs_upload' . $data['counter'];
        $fieldFileName = 'publicdocs_upload_file' . $data['counter'];

        $form_upload_file->get('publicdocs_upload_count')->setValue($data['counter']);
        $form_upload_file->get('publicdocs_upload0')->setName($fieldName);
        $form_upload_file->get('publicdocs_upload0')->setAttribute('id', $fieldName);

        $form_upload_file->get('publicdocs_upload_file0')->setName($fieldFileName);
        $form_upload_file->get('publicdocs_upload_file0')->setAttribute('id', $fieldFileName);

        if (isset($params['flag'])) {
            $data['flag'] = $params['flag'];
        }
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'form_upload_form' => $form_upload_file,
            'jsLangTranslate' => $this->_config['user_messages']['config']['create_contact'],
            'params' => $data,
                )
        );
        return $viewModel;
    }

}