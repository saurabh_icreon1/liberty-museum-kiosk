<?php

/**
 * This page is used for Create Case reply form.
 * @package    Cases_CaseReplyForm
 * @author     Icreon Tech - DT
 */

namespace Cases\Form;

use Zend\Form\Form;

/**
 * This page is used for Create Case reply form.
 * @package    CaseReplyForm
 * @author     Icreon Tech - DT
 */
class CaseReplyForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('case_reply');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'case_contact_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'case_contact_id'
            )
        ));
        $this->add(array(
            'name' => 'reply',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'reply',
                'class' => 'width-90'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'notify',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'class' => 'checkbox',
                'id' => 'notify',
                'class' => 'e2',
                'checked'=>'checked'
            )
        ));
        $this->add(array(
            'name' => 'postreply',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'postreply',
                'class' => 'save-btn right button',
                'value' => 'Submit'
            )
        ));

		$this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'case_status_reply',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
				'value'=>'',
                'id' => 'case_status_reply',
                'class' => 'e1 select-w-320'
            )
        ));
    }

}
