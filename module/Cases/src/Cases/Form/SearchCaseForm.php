<?php

/**
 * This form is used for create Search case.
 * @category   Zend
 * @package    Cases
 * @author     Icreon Tech - DT
 */

namespace Cases\Form;

use Zend\Form\Form;
use Cases\Form\SearchCaseForm;

/**
 * This class is used for create Search case.
 * @category   Zend
 * @package    Cases_SearchCaseForm
 * @author     Icreon Tech - DT
 */
class SearchCaseForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('search_case');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'case_subject',
            'attributes' => array(
                'type' => 'text',
                'id' => 'case_subject'
            )
        ));

		$this->add(array(
            'name' => 'case_subject_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'case_subject_name'
            )
        ));

        $this->add(array(
            'name' => 'user_email',
            'attributes' => array(
                'type' => 'text',
                'id' => 'user_email'
            )
        ));
        $this->add(array(
            'name' => 'user_transaction_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'user_transaction_id'
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'case_status',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'case_status',
                'class' => 'select-w-320',
				'multiple' => 'multiple',
				'size' => '7'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'case_type',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'case_type',
                'class' => 'e1 select-w-320'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_converted',
            'checked_value' => '1',
            'unchecked_value' => '0',
             'attributes' => array(
                'id' => 'is_converted',
                'class' => 'checkbox e2'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_urgent',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'is_urgent',
                'class' => 'checkbox e2'
            )
            
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_archive',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'is_archive',
                'class' => 'checkbox e2'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'added_date_range',
            'attributes' => array(
                'id' => 'added_date_range',
                'class' => 'e1 select-w-320',
                'onChange' => 'showDate(this.id,"date_range_div_created")'
            )
        ));
        $this->add(array(
            'name' => 'added_date_from',
            'attributes' => array(
                'type' => 'text',
                'class' => 'calendar-input width-120 cal-icon',
                'id' => 'added_date_from',
            )
        ));
        $this->add(array(
            'name' => 'added_date_to',
            'attributes' => array(
                'type' => 'text',
                'class' => 'calendar-input width-120 cal-icon',
                'id' => 'added_date_to',
            )
        ));


		$this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'start_date_range',
            'attributes' => array(
                'id' => 'start_date_range',
                'class' => 'e1 select-w-320',
                'onChange' => 'showDate(this.id,"date_range_div")'
            )
        ));
        $this->add(array(
            'name' => 'start_date_from',
            'attributes' => array(
                'type' => 'text',
                'class' => 'calendar-input width-120 cal-icon',
                'id' => 'start_date_from',
            )
        ));
        $this->add(array(
            'name' => 'start_date_to',
            'attributes' => array(
                'type' => 'text',
                'class' => 'calendar-input width-120 cal-icon',
                'id' => 'start_date_to',
            )
        ));

        $this->add(array(
            'name' => 'assigned_to',
            'attributes' => array(
                'type' => 'text',
                'id' => 'assigned_to',
                'class' => 'search-icon'
            )
        ));
            
        $this->add(array(
            'name' => 'assigned_to_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'assigned_to_id',
            )
        ));
          
        $this->add(array(
            'name' => 'searchcasebutton',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'searchcasebutton',
                'class' => 'search-btn',
                'value' => 'SEARCH'
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'saved_search',
            'options' => array(
                'value_options' => array(

                ),
            ),
            'attributes' => array(
                'value' => '',
                'class' => 'e1 select-w-320',
                'id' => 'saved_search',
                'onChange' =>'getSavedCaseSearchResult();'
            ),
        ));
    }

}

