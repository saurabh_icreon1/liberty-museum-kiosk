<?php

/**
 * This page is used to contact search case form
 * @package    Cases_ContactSearchCaseForm
 * @author     Icreon Tech - DT
 */

namespace Cases\Form;

use Zend\Form\Form;
use Cases\Form\ContactSearchCaseForm;

/**
 * This class is used to contact search case form
 * @package    ContactSearchCaseForm
 * @author     Icreon Tech - DT
 */
class ContactSearchCaseForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('contact_search_case');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'case_subject',
            'attributes' => array(
                'type' => 'text',
                'id' => 'case_subject'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'case_status',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'case_status',
                'class' => 'e1 select-w-320'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'case_type',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'case_type',
                'class' => 'e1 select-w-320'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_converted',
            'checked_value' => '1',
            'unchecked_value' => '0',
             'attributes' => array(
                'id' => 'is_converted',
                'class' => 'checkbox e2'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_urgent',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'is_urgent',
                'class' => 'checkbox e2'
            )
            
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_archive',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'is_archive',
                'class' => 'checkbox e2'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'added_date_range',
            'attributes' => array(
                'id' => 'added_date_range',
                'class' => 'e1 select-w-320',
                'onChange' => 'showDate(this.id,"date_range_div")'
            )
        ));
        $this->add(array(
            'name' => 'added_date_from',
            'attributes' => array(
                'type' => 'text',
                'class' => 'calendar-input width-120 cal-icon',
                'id' => 'added_date_from',
            )
        ));
        $this->add(array(
            'name' => 'added_date_to',
            'attributes' => array(
                'type' => 'text',
                'class' => 'calendar-input width-120 cal-icon',
                'id' => 'added_date_to',
            )
        ));
         $this->add(array(
            'name' => 'searchcontactcasebutton',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'searchcontactcasebutton',
                'class' => 'save-btn',
                'value' => 'SEARCH'
            )
        ));
    }

}

