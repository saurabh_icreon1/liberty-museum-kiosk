<?php

/**
 * This page is used for Create Case form.
 * @package    Cases_CaseController
 * @author     Icreon Tech - DT
 */

namespace Cases\Form;

use Zend\Form\Form;

/**
 * This class is used for Create Case form.
 * @package    Cases_CaseController
 * @author     Icreon Tech - DT
 */
class CaseForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('case');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'case_contact_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'case_contact_id'
            )
        ));
        $this->add(array(
            'name' => 'contact_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'contact_name',
                'class' => 'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'contact_name_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'contact_name_id'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'transaction_id',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'transaction_id',
                'class' => 'e1'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'mode',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'mode',
                'class' => 'e1'
            )
        ));
        $this->add(array(
            'name' => 'message',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'message',
                "class" => "width-60"
            )
        ));
        $this->add(array(
            'name' => 'subject',
            'attributes' => array(
                'type' => 'text',
                'id' => 'subject',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'case_type',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'case_type',
                'class' => 'e1'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'case_status',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'case_status',
                'onChange' => 'showReason(this.id)',
                'class' => 'e1'
            )
        ));

        $this->add(array(
            'name' => 'reason',
            'attributes' => array(
                'type' => 'text',
                'id' => 'reason',
            )
        ));
        $this->add(array(
            'name' => 'start_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'start_date',
                'class' => 'width-128 cal-icon',
            )
        ));
        $this->add(array(
            'name' => 'start_time',
            'attributes' => array(
                'type' => 'text',
                'id' => 'start_time',
                'class' => 'width-128 time-icon',
            )
        ));
        $this->add(array(
            'name' => 'duration',
            'attributes' => array(
                'type' => 'text',
                'id' => 'duration',
            )
        ));
        $this->add(array(
            'name' => 'assigned_to',
            'attributes' => array(
                'type' => 'text',
                'id' => 'assigned_to',
                'class' => 'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'assigned_to_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'assigned_to_id',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'urgent',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'class' => 'checkbox',
                'id' => 'urgent',
                'class' => 'e2'
            )
        ));

        $this->add(array(
            'name' => 'continue',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'continue',
                'class' => 'save-btn',
                'value' => 'SAVE'
            )
        ));
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'id' => 'cancel',
                'class' => 'cancel-btn m-l-20',
                'value' => 'CANCEL',
                'onclick' => 'window.location.href="/cases"'
            )
        ));
    }

}
