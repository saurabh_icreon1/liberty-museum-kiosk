<?php

/**
 * This page is used for Create USer Case form.
 * @package    Cases_CaseController
 * @author     Icreon Tech - DG
 */

namespace Cases\Form;

use Zend\Form\Form;

/**
 * This class is used for Create user Case form.
 * @package    Cases
 * @author     Icreon Tech - DG
 */
class UserCaseForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('usercase');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'case_contact_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'case_contact_id'
            )
        ));

        $this->add(array(
            'name' => 'contact_name_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'contact_name_id'
            )
        ));
        $this->add(array(
            'name' => 'transaction_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'transaction_id'
            )
        ));
        $this->add(array(
            'name' => 'message',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'message',
                "class" => "width-60",
                'style'=> 'height:110px'
            )
        ));
        $this->add(array(
            'name' => 'subject',
            'attributes' => array(
                'type' => 'text',
                'id' => 'subject',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'case_type',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'case_type',
                'class' => 'e1 select-case'
            )
        ));

        $this->add(array('type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 600
                )
            )
        ));
        $this->add(array(
            'name' => 'continue',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'continue',
                'class' => 'button semibold',
                'value' => 'SAVE'
            )
        ));
    }

}
