<?php

/**
 * This page is used for case module validation.
 * @package    Cases_Cases
 * @author     Icreon Tech - DT
 */

namespace Cases\Model;

use Cases\Module;
// Add these import statements
use Base\Model\BaseModel;
use Zend\Db\Adapter\Adapter;
// Add these import statements
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 * This model is used for case module validation.
 * @package    Cases
 * @author     Icreon Tech - DT
 */
class Cases extends BaseModel {

    protected $inputFilter;
    protected $adapter;

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    /**
     * This method is used to call add case procedure parameter
     * @param array
     * @return array
     * @author Icreon Tech - DT
     */
    public function getCreateCaseArr($data) {
        $returnArr = array();
        $returnArr[] = (isset($data['contact_name_id']) && !empty($data['contact_name_id'])) ? $data['contact_name_id'] : '';
        $returnArr[] = (isset($data['mode']) && !empty($data['mode'])) ? $data['mode'] : '';
        $returnArr[] = (isset($data['message']) && !empty($data['message'])) ? $data['message'] : '';
        $returnArr[] = (isset($data['subject']) && !empty($data['subject'])) ? $data['subject'] : '';
        $returnArr[] = (isset($data['case_type']) && !empty($data['case_type'])) ? $data['case_type'] : '';
        $returnArr[] = (isset($data['case_status']) && !empty($data['case_status'])) ? $data['case_status'] : '';
        $returnArr[] = (isset($data['reason']) && !empty($data['reason'])) ? $data['reason'] : '';
        if (isset($data['start_date']) && !empty($data['start_date'])) {

            $returnArr[] = $data['start_date'] . " " . $data['start_time'];
        } else {
            $returnArr[] = '';
        }
        $returnArr[] = (isset($data['duration']) && !empty($data['duration'])) ? $data['duration'] : '';
        $returnArr[] = (isset($data['urgent']) && !empty($data['urgent'])) ? $data['urgent'] : '';
        $returnArr[] = (isset($data['assigned_to_id']) && !empty($data['assigned_to_id'])) ? $data['assigned_to_id'] : '';
        $returnArr[] = (isset($data['added_by']) && !empty($data['added_by'])) ? $data['added_by'] : '';
        $returnArr[] = (isset($data['added_date']) && !empty($data['added_date'])) ? $data['added_date'] : '';
        $returnArr[] = (isset($data['modify_by']) && !empty($data['modify_by'])) ? $data['modify_by'] : '';
        $returnArr[] = (isset($data['modify_date']) && !empty($data['modify_date'])) ? $data['modify_date'] : '';
        $returnArr[] = (isset($data['transaction_id']) && !empty($data['transaction_id'])) ? $data['transaction_id'] : NULL;
		$returnArr[] = (isset($data['is_send_email']) && !empty($data['is_send_email'])) ? $data['is_send_email'] : '0';
        return $returnArr;
    }

    /**
     * This method is used to call edit case procedure parameter
     * @param array
     * @return array
     * @author Icreon Tech - DT
     */
    public function getCreateEditArr($data) {
        $returnArr = array();
        $returnArr[] = (isset($data['transaction_id']) && !empty($data['transaction_id'])) ? $data['transaction_id'] : NULL;
        $returnArr[] = (isset($data['contact_name_id']) && !empty($data['contact_name_id'])) ? $data['contact_name_id'] : '';
        $returnArr[] = (isset($data['mode']) && !empty($data['mode'])) ? $data['mode'] : '';
        $returnArr[] = (isset($data['message']) && !empty($data['message'])) ? $data['message'] : '';
        $returnArr[] = (isset($data['subject']) && !empty($data['subject'])) ? $data['subject'] : '';
        $returnArr[] = (isset($data['case_type']) && !empty($data['case_type'])) ? $data['case_type'] : '';
        $returnArr[] = (isset($data['case_status']) && !empty($data['case_status'])) ? $data['case_status'] : '';
        $returnArr[] = (isset($data['reason']) && !empty($data['reason'])) ? $data['reason'] : '';
        if (isset($data['start_date']) && !empty($data['start_date'])) {

            $returnArr[] = $data['start_date'] . " " . $data['start_time'];
        } else {
            $returnArr[] = '';
        }
        $returnArr[] = (isset($data['duration']) && !empty($data['duration'])) ? $data['duration'] : '';
        $returnArr[] = (isset($data['urgent']) && !empty($data['urgent'])) ? $data['urgent'] : '';
        $returnArr[] = (isset($data['assigned_to_id']) && !empty($data['assigned_to_id'])) ? $data['assigned_to_id'] : '';
        $returnArr[] = (isset($data['modify_by']) && !empty($data['modify_by'])) ? $data['modify_by'] : '';
        $returnArr[] = (isset($data['modify_date']) && !empty($data['modify_date'])) ? $data['modify_date'] : '';
        $returnArr[] = (isset($data['case_contact_id']) && !empty($data['case_contact_id'])) ? $data['case_contact_id'] : '';
        return $returnArr;
    }

    /**
     * This method is used to add filtering and validation to the form elements of create case
     * @param void
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getInputFilterCreateCase() {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'contact_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'CONTACT_NAME_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'message',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'MESSAGE_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'transaction_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'subject',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'SUBJECT_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'mode',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'CASE_TYPE_EMPTY',
                                    ),
                                    'inarrayvalidator' => false,
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'case_type',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'CASE_TYPE_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));


            $inputFilter->add($factory->createInput(array(
                        'name' => 'case_status',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'CASE_STATUS_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'reason',
                        'required' => false,
                    )));


            $inputFilter->add($factory->createInput(array(
                        'name' => 'start_date',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'assigned_to',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'ASSIGNED_TO_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'duration',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    /**
     * This method is used to add filtering and validation to the form elements of create case
     * @param void
     * @return Array
     * @author Icreon Tech - DG
     */
    public function getInputFilterUserCreateCase() {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();


            $inputFilter->add($factory->createInput(array(
                        'name' => 'csrf',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'csrf',
                                'options' => array(
                                    'messages' => array(
                                        'notSame' => 'CSRF_NOT_SAME',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'message',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'MESSAGE_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'transaction_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'subject',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'SUBJECT_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'case_type',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'CASE_TYPE_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    /**
     * This method is used to return the stored procedure array for search case
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getCaseSearchArr($dataArr = array()) {

        $returnArr = array();
        $returnArr[] = (isset($dataArr['case_subject']) && $dataArr['case_subject'] != '') ? $dataArr['case_subject'] : '';   //This is used for case id
        $returnArr[] = (isset($dataArr['case_subject_name']) && $dataArr['case_subject_name'] != '') ? $dataArr['case_subject_name'] : '';
        $returnArr[] = (isset($dataArr['user_email']) && $dataArr['user_email'] != '') ? $dataArr['user_email'] : '';
        $returnArr[] = (isset($dataArr['user_transaction_id']) && $dataArr['user_transaction_id'] != '') ? $dataArr['user_transaction_id'] : '';
        $returnArr[] = (isset($dataArr['case_status']) && $dataArr['case_status'] != '') ? $dataArr['case_status'] : '';
        $returnArr[] = (isset($dataArr['case_type']) && $dataArr['case_type'] != '') ? $dataArr['case_type'] : '';
        $returnArr[] = (isset($dataArr['is_converted']) && $dataArr['is_converted'] != '') ? $dataArr['is_converted'] : '';
        $returnArr[] = (isset($dataArr['is_urgent']) && $dataArr['is_urgent'] != '') ? $dataArr['is_urgent'] : '';
        $returnArr[] = (isset($dataArr['is_archive']) && $dataArr['is_archive'] != '') ? $dataArr['is_archive'] : '0';
        $returnArr[] = (isset($dataArr['added_date_from']) && $dataArr['added_date_from'] != '') ? $dataArr['added_date_from'] : '';
        $returnArr[] = (isset($dataArr['added_date_to']) && $dataArr['added_date_to'] != '') ? $dataArr['added_date_to'] : '';
        $returnArr[] = (isset($dataArr['contact_id']) && $dataArr['contact_id'] != '') ? $dataArr['contact_id'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $returnArr[] = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $returnArr[] = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        $returnArr[] = (isset($dataArr['assigned_to_id']) && $dataArr['assigned_to_id'] != '') ? $dataArr['assigned_to_id'] : '';
        $returnArr[] = (isset($dataArr['searchNew']) && $dataArr['searchNew'] != '') ? $dataArr['searchNew'] : '';
		$returnArr[] = (isset($dataArr['start_date_from']) && $dataArr['start_date_from'] != '') ? $dataArr['start_date_from'] : '';
		$returnArr[] = (isset($dataArr['start_date_to']) && $dataArr['start_date_to'] != '') ? $dataArr['start_date_to'] : '';
		//asd($returnArr);
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for create search result
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getAddCaseSearchArr($dataArr = array()) {

        $returnArr = array();
        $returnArr[] = (isset($dataArr['crm_user_id']) && $dataArr['crm_user_id'] != '') ? $dataArr['crm_user_id'] : '';
        $returnArr[] = (isset($dataArr['title']) && $dataArr['title'] != '') ? $dataArr['title'] : '';
        $returnArr[] = (isset($dataArr['search_query']) && $dataArr['search_query'] != '') ? $dataArr['search_query'] : '';
        $returnArr[] = (isset($dataArr['is_active']) && $dataArr['is_active'] != '') ? $dataArr['is_active'] : '';
        $returnArr[] = (isset($dataArr['is_delete']) && $dataArr['is_delete'] != '') ? $dataArr['is_delete'] : '';
        if (isset($dataArr['added_date'])) {
            $returnArr[] = $dataArr['added_date'];
        } else {
            $returnArr[] = $dataArr['modified_date'];
        }
        if (isset($dataArr['case_search_id']) && $dataArr['case_search_id'] != '') {
            $returnArr[] = $dataArr['case_search_id'];
        }
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for saved search
     * @param Array
     * @return Array
     * @author Icreon Tech - DT

     */
    public function getCaseSavedSearchArr($dataArr = array()) {

        $returnArr = array();
        $returnArr[] = (isset($dataArr['crm_user_id']) && $dataArr['crm_user_id'] != '') ? $dataArr['crm_user_id'] : '';
        $returnArr[] = (isset($dataArr['case_search_id']) && $dataArr['case_search_id'] != '') ? $dataArr['case_search_id'] : '';
        $returnArr[] = (isset($dataArr['is_active'])) ? $dataArr['is_active'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for delete saved search
     * @param Array
     * @return Array
     * @author Icreon Tech - DT

     */
    public function getDeleteCaseSavedSearchArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['is_deleted']) && $dataArr['is_deleted'] != '') ? $dataArr['is_deleted'] : '';
        $returnArr[] = (isset($dataArr['modified_date']) && $dataArr['modified_date'] != '') ? $dataArr['modified_date'] : '';
        $returnArr[] = (isset($dataArr['case_search_id']) && $dataArr['case_search_id'] != '') ? $dataArr['case_search_id'] : '';
        return $returnArr;
    }

    /**
     * This method is used to add filtering and validation to the form elements of reply case
     * @param void
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getInputFilterReplyCase() {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'case_contact_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'CASE_CONTACT_ID_MESSAGE',
                                    ),
                                ),
                            ),
                        ),
                    )));
			$inputFilter->add($factory->createInput(array(
                        'name' => 'case_status_reply',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'CASE_STATUS_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'reply',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'MESSAGE_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'notify',
                        'required' => false
                    )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    /**
     * This method is used to add filtering and validation to the form elements of note case
     * @param void
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getInputFilterNoteCase() {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'case_contact_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'CASE_CONTACT_ID_MESSAGE',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'note',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'MESSAGE_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    /**
     * This method is used to return the stored procedure array for create case
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getInputFilterCaseSearch($filterData) {
        $inputFilterData = array();
        if (is_array($filterData)) {
            foreach ($filterData as $key => $val) {
                $inputFilterData[$key] = trim(addslashes(strip_tags($val)));
            }
        } else {
            $inputFilterData = trim(addslashes(strip_tags($filterData)));
        }


        return $inputFilterData;
    }

    /**
     * This method is used to call add reply procedure parameter
     * @param array
     * @return array
     * @author Icreon Tech - DT
     */
    public function getCaseReplyArr($data) {
        $returnArr = array();
        $returnArr[] = (isset($data['case_contact_id']) && !empty($data['case_contact_id'])) ? $data['case_contact_id'] : '';
        $returnArr[] = (isset($data['reply']) && !empty($data['reply'])) ? $data['reply'] : '';
        $returnArr[] = (isset($data['is_admin_reply']) && !empty($data['is_admin_reply'])) ? $data['is_admin_reply'] : '0';
        $returnArr[] = (isset($data['added_by']) && !empty($data['added_by'])) ? $data['added_by'] : '';
        $returnArr[] = (isset($data['added_date']) && !empty($data['added_date'])) ? $data['added_date'] : '';
        $returnArr[] = (isset($data['modify_date']) && !empty($data['modify_date'])) ? $data['modify_date'] : '';
        return $returnArr;
    }

    /**
     * This method is used to call add note procedure parameter
     * @param array
     * @return array
     * @author Icreon Tech - DT
     */
    public function getCaseNoteArr($data) {
        $returnArr = array();
        $returnArr[] = (isset($data['case_contact_id']) && !empty($data['case_contact_id'])) ? $data['case_contact_id'] : '';
        $returnArr[] = (isset($data['note']) && !empty($data['note'])) ? $data['note'] : '';
        $returnArr[] = (isset($data['added_by']) && !empty($data['added_by'])) ? $data['added_by'] : '';
        $returnArr[] = (isset($data['added_date']) && !empty($data['added_date'])) ? $data['added_date'] : '';
        $returnArr[] = (isset($data['modify_date']) && !empty($data['modify_date'])) ? $data['modify_date'] : '';
        return $returnArr;
    }

    /**
     * Function used to get array of elements need to call remove group
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getArrayForRemoveCase($dataArr) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['case_contact_id']) && $dataArr['case_contact_id'] != '') ? $dataArr['case_contact_id'] : '';
        return $returnArr;
    }

    /**
     * Function used to get array of elements need to call add to archvie
     * @return Array
     * @param Array
     * @author Icreon Tech - DT
     */
    public function getArrayForAddToArchiveCase($dataArr) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['case_contact_id']) && $dataArr['case_contact_id'] != '') ? $dataArr['case_contact_id'] : '';
        $returnArr[] = (isset($dataArr['is_archived']) && $dataArr['is_archived'] != '') ? $dataArr['is_archived'] : '';
        return $returnArr;
    }

    /**
     * Function used to check variables cases
     * @author Icreon Tech - DT
     * @return void
     * @param Array
     */
    public function exchangeArray($data) {
        
    }

}