<?php

/**
 * This page is used for Case module database related work.
 * @package    Cases_CasesTable
 * @author     Icreon Tech - DT
 */

namespace Cases\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This model is used for Case module database related work.
 * @package    CasesTable
 * @author     Icreon Tech - DT
 */
class CasesTable {

    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
    }

    /**
     * Function for insert case data
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function saveCase($caseData) {
        $procquesmarkapp = $this->appendQuestionMars(count($caseData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_cam_insertCase(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $caseData[0]);
        $stmt->getResource()->bindParam(2, $caseData[1]);
        $stmt->getResource()->bindParam(3, $caseData[2]);
        $stmt->getResource()->bindParam(4, $caseData[3]);
        $stmt->getResource()->bindParam(5, $caseData[4]);
        $stmt->getResource()->bindParam(6, $caseData[5]);
        $stmt->getResource()->bindParam(7, $caseData[6]);
        $stmt->getResource()->bindParam(8, $caseData[7]);
        $stmt->getResource()->bindParam(9, $caseData[8]);
        $stmt->getResource()->bindParam(10, $caseData[9]);
        $stmt->getResource()->bindParam(11, $caseData[10]);
        $stmt->getResource()->bindParam(12, $caseData[11]);
        $stmt->getResource()->bindParam(13, $caseData[12]);
        $stmt->getResource()->bindParam(14, $caseData[13]);
        $stmt->getResource()->bindParam(15, $caseData[14]);
        $stmt->getResource()->bindParam(16, $caseData[15]);
		$stmt->getResource()->bindParam(17, $caseData[16]);

        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for edit case data
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function editCase($caseData) {
        $procquesmarkapp = $this->appendQuestionMars(count($caseData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_cam_updateCase(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $caseData[0]);
        $stmt->getResource()->bindParam(2, $caseData[1]);
        $stmt->getResource()->bindParam(3, $caseData[2]);
        $stmt->getResource()->bindParam(4, $caseData[3]);
        $stmt->getResource()->bindParam(5, $caseData[4]);
        $stmt->getResource()->bindParam(6, $caseData[5]);
        $stmt->getResource()->bindParam(7, $caseData[6]);
        $stmt->getResource()->bindParam(8, $caseData[7]);
        $stmt->getResource()->bindParam(9, $caseData[8]);
        $stmt->getResource()->bindParam(10, $caseData[9]);
        $stmt->getResource()->bindParam(11, $caseData[10]);
        $stmt->getResource()->bindParam(12, $caseData[11]);
        $stmt->getResource()->bindParam(13, $caseData[12]);
        $stmt->getResource()->bindParam(14, $caseData[13]);
        $stmt->getResource()->bindParam(15, $caseData[14]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for update user view case
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function updateUserViewCase($caseData) {
        $procquesmarkapp = $this->appendQuestionMars(count($caseData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_cas_updateUserViewCase(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $caseData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for add to archive
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function addToArchive($caseData) {
        $procquesmarkapp = $this->appendQuestionMars(count($caseData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_cas_addToArchiveCase(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $caseData[0]);
        $stmt->getResource()->bindParam(2, $caseData[1]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for get case data
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function getAllCase($caseData) {
        $procquesmarkapp = $this->appendQuestionMars(count($caseData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_cam_getCases(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $caseData[0]);
        $stmt->getResource()->bindParam(2, $caseData[1]);
        $stmt->getResource()->bindParam(3, (isset($caseData[2]) and trim($caseData[2]) != "")?addslashes($caseData[2]):"");
        $stmt->getResource()->bindParam(4, $caseData[3]);
        $stmt->getResource()->bindParam(5, $caseData[4]);
        $stmt->getResource()->bindParam(6, $caseData[5]);
        $stmt->getResource()->bindParam(7, $caseData[6]);
        $stmt->getResource()->bindParam(8, $caseData[7]);
        $stmt->getResource()->bindParam(9, $caseData[8]);
        $stmt->getResource()->bindParam(10, $caseData[9]);
        $stmt->getResource()->bindParam(11, $caseData[10]);
        $stmt->getResource()->bindParam(12, $caseData[11]);
        $stmt->getResource()->bindParam(13, $caseData[12]);
        $stmt->getResource()->bindParam(14, $caseData[13]);
        $stmt->getResource()->bindParam(15, $caseData[14]);
        $stmt->getResource()->bindParam(16, $caseData[15]);
        $stmt->getResource()->bindParam(17, $caseData[16]);
        $stmt->getResource()->bindParam(18, $caseData[17]);
		$stmt->getResource()->bindParam(19, $caseData[18]);
		$stmt->getResource()->bindParam(20, $caseData[19]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        // asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * This function is used to get the saved search listing for case search
     * @param this will be an array.
     * @return this will return a title of the saved search
     * @author Icreon Tech -DT
     */
    public function getCaseSavedSearch($caseData = array()) {
        $procquesmarkapp = $this->appendQuestionMars(count($caseData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_cas_getCaseSavedSearch(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $caseData[0]);
        $stmt->getResource()->bindParam(2, $caseData[1]);
        $stmt->getResource()->bindParam(3, $caseData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
    }

    /**
     * This function will save the search title into the table for case search
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech -DT
     */
    public function saveCaseSearch($caseData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($caseData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_cas_saveCaseSearch(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $caseData[0]);
            $stmt->getResource()->bindParam(2, $caseData[1]);
            $stmt->getResource()->bindParam(3, $caseData[2]);
            $stmt->getResource()->bindParam(4, $caseData[3]);
            $stmt->getResource()->bindParam(5, $caseData[4]);
            $stmt->getResource()->bindParam(6, $caseData[5]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update the search title into the table for case search
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech -DT
     */
    public function updateCaseSearch($caseData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($caseData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_cas_updateCaseSearch(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $caseData[0]);
            $stmt->getResource()->bindParam(2, $caseData[1]);
            $stmt->getResource()->bindParam(3, $caseData[2]);
            $stmt->getResource()->bindParam(4, $caseData[3]);
            $stmt->getResource()->bindParam(5, $caseData[4]);
            $stmt->getResource()->bindParam(6, $caseData[5]);
            $stmt->getResource()->bindParam(7, $caseData[6]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will delete the saved case search titles
     * @param this will be an array having the params.
     * @return this will return a confirmation
     * @author Icreon Tech -DT
     */
    public function deleteSavedSearch($caseData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($caseData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_cas_deleteSavedSearch(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $caseData[0]);
            $stmt->getResource()->bindParam(2, $caseData[1]);
            $stmt->getResource()->bindParam(3, $caseData[2]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for remove case
     * @author Icreon Tech - DT
     * @return Void
     * @param Array
     */
    public function removeCaseById($caseData) {
        $procquesmarkapp = $this->appendQuestionMars(count($caseData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_cam_deleteCase(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $caseData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for get case data on the base of id
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function getCaseById($caseData) {
        $procquesmarkapp = $this->appendQuestionMars(count($caseData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_cam_getCaseDetails(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $caseData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet[0];
        else
            return false;
    }

    /**
     * Function for insert case reply
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function saveReply($caseData) {
        $procquesmarkapp = $this->appendQuestionMars(count($caseData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_cas_insertCaseReply(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $caseData[0]);
        $stmt->getResource()->bindParam(2, $caseData[1]);
        $stmt->getResource()->bindParam(3, $caseData[2]);
        $stmt->getResource()->bindParam(4, $caseData[3]);
        $stmt->getResource()->bindParam(5, $caseData[4]);
        $stmt->getResource()->bindParam(6, $caseData[5]);

        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for insert case note
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function saveNote($caseData) {
        $procquesmarkapp = $this->appendQuestionMars(count($caseData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_cas_insertCaseNote(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $caseData[0]);
        $stmt->getResource()->bindParam(2, $caseData[1]);
        $stmt->getResource()->bindParam(3, $caseData[2]);
        $stmt->getResource()->bindParam(4, $caseData[3]);
        $stmt->getResource()->bindParam(5, $caseData[4]);

        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for get case replies on base of case id
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function getCaseReplies($caseData) {
        $procquesmarkapp = $this->appendQuestionMars(count($caseData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_cam_getCaseReplies(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $caseData[0]);
        $stmt->getResource()->bindParam(2, $caseData[1]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        
        $statement->closeCursor();

        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get case replies on base of case id
     * @author Icreon Tech - DG
     * @return Int
     * @param Array
     */
    public function getUserCaseReplies($caseData) {
        $procquesmarkapp = $this->appendQuestionMars(count($caseData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_cam_getUserCaseReplies(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $caseData[0]);
        $stmt->getResource()->bindParam(2, $caseData[1]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get case notes on base of case id
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function getCaseNotes($caseData) {
        $procquesmarkapp = $this->appendQuestionMars(count($caseData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_cam_getCaseNotes(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $caseData[0]);
        $stmt->getResource()->bindParam(2, $caseData[1]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /* Function for get cases count
     * @author Icreon Tech- DT
     * param void
     * return array
     */

    public function getCaseCount($param) {
        $stmt = $this->dbAdapter->createStatement();

        $stmt->prepare("CALL usp_com_getCaseCount(?)");
		$user_id = (isset($param['user_id']) && $param['user_id']!='')?$param['user_id']:'';
        $stmt->getResource()->bindParam(1, $user_id);
       
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['totalRecord'];
        } else {
            return false;
        }
    }

    /**
     * Function for append number of questions marks for stored procedure
     * @author Icreon Tech - DT
     * @return String
     * @param Array
     */
    public function appendQuestionMars($count) {
        $returnstr = '';
        for ($i = 1; $i <= $count; $i++) {
            $returnstr.='?,';
        }
        return rtrim($returnstr, ',');
    }

    /**
     * Function for update user case data
     * @author Icreon Tech - DG
     * @param Array
     */
    public function updateUserCase($params) {
        $transactionId = isset($params['transaction_id']) ? $params['transaction_id'] : NULL;
        $caseStatus = isset($params['case_status']) ? $params['case_status'] : NULL;
        $caseId = isset($params['case_id']) ? $params['case_id'] : NULL;
        $strat_datetime = isset($params['start_datetime']) ? $params['start_datetime'] : NULL;
		$modified_by = isset($params['modified_by']) ? $params['modified_by'] : NULL;
		$modified_date = isset($params['modified_date']) ? $params['modified_date'] : DATE_TIME_FORMAT;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_cam_updateUserCase(?,?,?,?,?,?)");
        $stmt->getResource()->bindParam(1, $transactionId);
        $stmt->getResource()->bindParam(2, $caseStatus);
        $stmt->getResource()->bindParam(3, $caseId);
		$stmt->getResource()->bindParam(4, $strat_datetime);
		$stmt->getResource()->bindParam(5, $modified_by);
		$stmt->getResource()->bindParam(6, $modified_date);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }
    
    
    public function addCaseFile($filedata){
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_cas_addCaseAttachments(?,?,?,?,?)");
        $stmt->getResource()->bindParam(1, $filedata['case_contact_id']);
        $stmt->getResource()->bindParam(2, $filedata['file_name']);
        $stmt->getResource()->bindParam(3, $filedata['added_by']);
        $stmt->getResource()->bindParam(4, $filedata['added_date']);
        $stmt->getResource()->bindParam(5, $filedata['modified_date']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }
    
    public function getCaseFiles($param = array()){
        $case_contact_id = (isset($param['case_contact_id']) && $param['case_contact_id']!='' )?$param['case_contact_id']:'';
        $case_attachment_id = (isset($param['case_attachment_id']) && $param['case_attachment_id']!='' )?$param['case_attachment_id']:'';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_cas_getCaseFiles(?,?)");
        $stmt->getResource()->bindParam(1, $case_attachment_id);
        $stmt->getResource()->bindParam(2, $case_contact_id);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }
    
    public function deleteCaseFiles($fileid){
       
    try
        { 
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_cas_deleteCaseFile(?)");
            $stmt->getResource()->bindParam(1 , $fileid);
            $result = $stmt->execute();
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
        /*$statement = $result->getResource();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;*/
    }

	public function getCaseReplyNoteCount($caseContactId){
		$stmt = $this->dbAdapter->createStatement();
		$stmt->prepare("CALL usp_cas_getCaseReplyNoteCount(?)");
		$stmt->getResource()->bindParam(1, $caseContactId);       
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0];
        } else {
            return false;
        }
	}

}