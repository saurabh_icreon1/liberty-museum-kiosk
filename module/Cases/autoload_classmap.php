<?php
/**
 * This page is used for case module autoload classes and module.
 * @package    Case
 * @author     Icreon Tech - DT
 */
return array(
    'Cases\Module'                       => __DIR__ . '/Module.php',
    'Cases\Controller\CaseController'   => __DIR__ . '/src/Cases/Controller/CaseController.php',
    'Cases\Model\Cases'            => __DIR__ . '/src/Cases/Model/Cases.php',
    'Cases\Model\CasesTable'                   => __DIR__ . '/src/Cases/Model/CasesTable.php'
);
?>