<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'Product\Controller\Product' => 'Product\Controller\ProductController'
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'create-product-category' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-product-category',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'addProductCategory'
                    ),
                ),
            ),
            'edit-product-category' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-product-category[/:category_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'editProductCategory'
                    ),
                ),
            ),
            'edit-product-category-drag-drop' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-product-category-drag-drop',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'editProductCategoryDragDrop'
                    ),
                ),
            ),
            'delete-product-category' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-product-category',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'deleteProductCategory'
                    ),
                ),
            ),
            'get-product-categories' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-product-categories',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'getProductCategories'
                    ),
                ),
            ),
            'productupload' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/product-upload[/:counter][/:container]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'productUpload'
                    ),
                ),
            ),
            'uploadproductfiles' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-product-files',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'uploadProductFiles'
                    ),
                ),
            ),
            'createcrmproduct' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-crm-product',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'addCrmProduct'
                    ),
                ),
            ),
            'addproductattribute' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-product-attribute',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'addProductAttribute'
                    ),
                ),
            ),
            'crmproducts' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-products',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'getCrmProducts'
                    ),
                ),
            ),
            'addCrmSearchProduct' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-crm-search-product',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'addCrmSearchProduct'
                    ),
                ),
            ),
            'getSavedCrmProductSearchSelect' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-saved-crm-product-search-select',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'getSavedCrmProductSearchSelect'
                    ),
                ),
            ),
            'getCrmSearchProductInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-search-product-info',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'getCrmSearchProductInfo'
                    ),
                ),
            ),
            'deleteCrmSearchProduct' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-search-product',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'deleteCrmSearchProduct'
                    ),
                ),
            ),
            'getcrmproductinfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-product-info[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'getCrmProductInfo'
                    ),
                ),
            ),
            'viewcrmproduct' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-crm-product[/:id][/:mode]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'viewCrmProduct'
                    //'action' => 'getCrmProductInfo'
                    ),
                ),
            ),
            'editcrmproductinfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-product-info[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'getCrmProductInfo'
                    ),
                ),
            ),
            'editcrmproductdetails' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-product[/:id][/:mode]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'viewCrmProduct'
                    ),
                ),
            ),
            'getCrmSearchProduct' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-search-product',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'getCrmSearchProduct'
                    ),
                ),
            ),
            'changeProductStatus' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/change-product-status',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'changeProductStatus'
                    ),
                ),
            ),
            'getProductAttributes' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-product-attributes',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'getProductAttributes'
                    ),
                ),
            ),
            'getListProductAttributes' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/list-product-attributes',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'getListProductAttributes'
                    ),
                ),
            ),
            'deleteProductAttribute' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-product-attribute[/:attribute_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'deleteProductAttribute'
                    ),
                ),
            ),
            'editcrmproduct' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-product[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'editCrmProduct'
                    ),
                ),
            ),
            'editProductAttribute' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-product-attribute[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'editProductAttribute'
                    ),
                ),
            ),
            'getallproducts' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-products',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'getProduct'
                    ),
                ),
            ),
            'addProductAttributeOption' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-product-attribute-option[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'addProductAttributeOption'
                    ),
                ),
            ),
            'getProductAttributeOptions' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/product-attribute-options[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'getProductAttributeOptions'
                    ),
                ),
            ),
            'listProductAttributeOptions' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/list-product-attribute-options[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'listProductAttributeOptions'
                    ),
                ),
            ),
            'deleteProductAttributeOption' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-product-attribute-option[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'deleteProductAttributeOption'
                    ),
                ),
            ),
            'editProductAttributeOption' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-product-attribute-option[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'editProductAttributeOption'
                    ),
                ),
            ),
            'deleteCrmProduct' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-product',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'deleteCrmProduct'
                    ),
                ),
            ),
            'manageProductAttributes' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/manage-product-attributes[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'manageProductAttributes'
                    ),
                ),
            ),
            'manageProductAttributesOptions' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/manage-product-attributes-options[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'manageProductAttributesOptions'
                    ),
                ),
            ),
            'listManageProductAttributes' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/list-manage-product-attributes[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'listManageProductAttributes'
                    ),
                ),
            ),
            'attachProductAttributes' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/attach-product-attributes[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'attachProductAttributes'
                    ),
                ),
            ),
            'getSelectCategory' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-select-category',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'getSelectCategory'
                    ),
                ),
            ),
            'getCategories' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-categories',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'getCategories'
                    ),
                ),
            ),
            'get-category-product' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-category-product',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'getCategoryProduct',
                    ),
                ),
            ),
            'get-product-detail-inventory' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-product-detail-inventory',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'getProductDetailInventory',
                    ),
                ),
            ),
            'get-related-products' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-related-products',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'getRelatedProduct'
                    ),
                ),
            ),
            'get-shop-products' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/shop[/:categoryId][/:product_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'getShopProducts'
                    ),
                ),
            ),
            'get-shop-products-list' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-shop-product-lists[/:categoryId]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'getShopProductsList'
                    ),
                ),
            ),
            'getProductTypes' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-product-types',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'getProductTypes'
                    ),
                ),
            ),
            'productLargeImageSize' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/product-large-image[/:image]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'productLargeImage'
                    ),
                ),
            ),
            'manage-category-product' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/manage-category-product',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'manageCategoryProduct'
                    ),
                ),
            ),
            'category-product-list' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/category-product-list[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'categoryProductList'
                    ),
                ),
            ),        
            'edit-cat-product-drag-drop' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-cat-product-drag-drop',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'editCatProductDragDrop'
                    ),
                ),
            ),
			'kioskShop' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/kiosk-fee',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'getKioskProduct'
                    ),
                ),
            ),
            'kioskShopproductList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-kiosk-shop-product-lists[/:categoryId]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Product\Controller',
                        'controller' => 'Product',
                        'action' => 'getKioskShopProductsList'
                    ),
                ),
            ),

            
            
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'navigation' => array(
        'default' => array(
            array(
                'label' => 'Product',
                'route' => 'get-product-categories',
                'pages' => array(
                    array(
                        'label' => 'Category',
                        'route' => 'get-product-categories',
                        'pages' => array(
                            array(
                                'label' => 'Create',
                                'route' => 'create-product-category',
                                'action' => 'addProductCategory',
                            ),
                            array(
                                'label' => 'Edit',
                                'route' => 'edit-product-category',
                                'action' => 'editProductCategory',
                            )
                        ),
                    ),
                    array(
                        'label' => 'Category Product',
                        'route' => 'manage-category-product',
                    ),
                )
            ),
            array(
                'label' => 'Attribute',
                'route' => 'getProductAttributes',
                'pages' => array(
                    array(
                        'label' => 'Search',
                        'route' => 'getProductAttributes',
                        'pages' => array(
                            array(
                                'label' => 'Edit',
                                'route' => 'editProductAttribute',
                                'action' => 'editProductAttribute',
                            ),
                            array(
                                'label' => 'Options',
                                'route' => 'getProductAttributeOptions',
                                'pages' => array(
                                    array(
                                        'label' => 'Edit',
                                        'route' => 'editProductAttributeOption',
                                        'action' => 'editProductAttributeOption',
                                    ),
                                    array(
                                        'label' => 'Create',
                                        'route' => 'addProductAttributeOption',
                                        'action' => 'addProductAttributeOption',
                                    )
                                ),
                            )
                        ),
                    ),
                    array(
                        'label' => 'Create',
                        'route' => 'addproductattribute',
                        'pages' => array(),
                    )
                ),
            ),
            array(
                'label' => 'Product',
                'route' => 'crmproducts',
                'pages' => array(
                    array(
                        'label' => 'Search',
                        'route' => 'crmproducts',
                        'pages' => array(
                            array(
                                'label' => 'View',
                                'route' => 'getcrmproductinfo',
                                'action' => 'getCrmProductInfo',
                            ),
                            array(
                                'label' => 'Edit',
                                'route' => 'editcrmproduct',
                                'action' => 'editCrmProduct',
                            ),
                            array(
                                'label' => 'Change Log',
                                'route' => 'productchangelog',
                                'action' => 'change-log'
                            ),
                            array(
                                'label' => 'Edit Attributes',
                                'route' => 'manageProductAttributes',
                                'action' => 'manageProductAttributes'
                            ),
                            array(
                                'label' => 'Add Attributes',
                                'route' => 'attachProductAttributes',
                                'action' => 'attachProductAttributes'
                            ),
                            array(
                                'label' => 'Edit Options',
                                'route' => 'manageProductAttributesOptions',
                                'action' => 'manageProductAttributesOptions'
                            )
                        ),
                    ),
                    array(
                        'label' => 'Create',
                        'route' => 'createcrmproduct',
                        'action' => 'addCrmProduct',
                    )
                ),
            ),
        )
    ),
    'ProductMessages' => array(
        'config' => include __DIR__ . '/../languages/en/language.php',
        'navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory'
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'product' => __DIR__ . '/../view'
        ),
        'template_map' => array(
            'layout/custom' => __DIR__ . '/../../module/Common/layout/crm.phtml',
            'error/404' => __DIR__ . '/../../module/Common/error/404.phtml',
            'error/index' => __DIR__ . '/../../module/Common/error/index.phtml',
        ),
    ),
    'global_config' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
);
