<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Product;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\I18n\Translator\Translator;
use Zend\Validator\AbstractValidator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Product\Model\Product;
use Product\Model\ProductTable;
use Product\Model\Category;
use Product\Model\CategoryTable;
use Product\Model\Attribute;
use Product\Model\AttributeTable;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface, ViewHelperProviderInterface {

    public function onBootstrap(MvcEvent $e) {
        $translator = $e->getApplication()->getServiceManager()->get('translator');
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $translator->addTranslationFile(
                'phpArray', './module/Product/languages/en/language.php', 'default', 'en_US'
        );
        //AbstractValidator::setDefaultTranslator($translator);
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Get Autoloader Configuration
     * @return array
     */
    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    /**
     * Get Service Configuration
     *
     * @return array
     */
    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Category\Model\CategoryTable' => function($sm) {
                    $tableGateway = $sm->get('CategoryTableGateway');
                    $table = new CategoryTable($tableGateway);
                    return $table;
                },
                'CategoryTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Category($dbAdapter));

                    return new TableGateway('tbl_pro_products', $dbAdapter, null, $resultSetPrototype);
                },
                'Product\Model\ProductTable' => function($sm) {
                    $tableGateway = $sm->get('ProductTableGateway');
                    $table = new ProductTable($tableGateway);
                    return $table;
                },
                'ProductTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Product($dbAdapter));

                    return new TableGateway('tbl_pro_products', $dbAdapter, null, $resultSetPrototype);
                },
                'Product\Model\AttributeTable' => function($sm) {
                    $tableGateway = $sm->get('AttributeTableGateway');
                    $table = new AttributeTable($tableGateway);
                    return $table;
                },
                'AttributeTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Attribute($dbAdapter));
                    return new TableGateway('', $dbAdapter, null, $resultSetPrototype);
                },
                'dbAdapter' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    return $dbAdapter;
                },
            ),
        );
    }

    /**
     * Get View Helper Configuration
     *
     * @return array
     */
    public function getViewHelperConfig() {
        return array(
            'factories' => array(
            // the array key here is the name you will call the view helper by in your view scripts
//                'absoluteUrl' => function($sm) {
//                    $locator = $sm->getServiceLocator(); // $sm is the view helper manager, so we need to fetch the main service manager
//                    return new AbsoluteUrl($locator->get('Request'));
//                },
            ),
        );
    }

    /**
     * Get Controller Configuration
     *
     * @return array
     */
    public function getControllerConfig() {
        return array();
    }

    public function loadConfiguration(MvcEvent $e) {
        $application = $e->getApplication();
        $serviceManager = $application->getServiceManager();
        $sharedManager = $application->getEventManager()->getSharedManager();
        $router = $serviceManager->get('router');
        $request = $serviceManager->get('request');

//        $matchedRoute = $router->match($request);
//        if (null !== $matchedRoute) {
//            $sharedManager->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function($e) use ($serviceManager) {
//                        $serviceManager->get('ControllerPluginManager')->get('AclPlugin')
//                                ->doAuthorization($e);
//                    }, 2
//            );
//        }
    }

    public function loadCommonViewVars(MvcEvent $e) {
        $e->getViewModel()->setVariables(array(
            'auth' => $e->getApplication()->getServiceManager()->get('AuthService')
        ));
    }

}
