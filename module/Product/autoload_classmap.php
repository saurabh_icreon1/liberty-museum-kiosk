<?php 
return array(
    'Product\Module'                       => __DIR__ . '/Module.php',
    'Product\Controller\ProductController'   => __DIR__ . '/src/Product/Controller/ProductController.php',
    'Product\Model\Product'            => __DIR__ . '/src/Product/Model/Product.php',
    'Product\Model\ProductTable'                   => __DIR__ . '/src/Product/Model/ProductTable.php',
    'Product\Model\AttributeTable'                   => __DIR__ . '/src/Product/Model/AttributeTable.php',
    'Product\Model\AuthStorage'                   => __DIR__ . '/src/Product/Model/AuthStorage.php',
);
?>