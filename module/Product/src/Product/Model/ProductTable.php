<?php

/**
 * This is used for Product module.
 * @package    Product
 * @author     Icreon Tech -SR.
 */

namespace Product\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This class is used for Passenger module.
 * @package    Document
 * @author     Icreon Tech -SR.
 */
class ProductTable {

    protected $tableGateway;
    protected $connection;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
        $this->connection = $this->tableGateway->getAdapter()->getDriver()->getConnection();
    }

    /**
     * Function for to get product types
     * @author Icreon Tech - AP
     * @return Array
     * @param Array
     */
    public function getProductType() {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pro_getProductCategories()');

            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will save the search title into the table for product search
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech -SR
     */
    public function saveProductSearch($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_insertCrmSearchProduct(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $stmt->getResource()->bindParam(2, $param['search_name']);
            $stmt->getResource()->bindParam(3, $param['search_query']);
            $stmt->getResource()->bindParam(4, $param['isActive']);
            $stmt->getResource()->bindParam(5, $param['isDelete']);
            $stmt->getResource()->bindParam(6, $param['added_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the saved search listing for manifest
     * @param this will be an array.
     * @return this will return a title of the saved search
     * @author Icreon Tech -SR
     */
    public function getProductSavedSearch($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getCrmSearchProduct(?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $stmt->getResource()->bindParam(2, $param['isActive']);
            $stmt->getResource()->bindParam(3, $param['searchId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will delete the saved crm product titles
     * @param this will be an array having the params.
     * @return this will return a confirmation
     * @author Icreon Tech -SR
     */
    public function deleteCrmProductSearch($param = array()) {

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_deleteCrmSearchProduct(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $stmt->getResource()->bindParam(2, $param['isDelete']);
            $stmt->getResource()->bindParam(3, $param['modifiend_date']);
            $stmt->getResource()->bindParam(4, $param['search_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update the search title and the search parameters
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech -SR
     */
    public function updateCrmProductSearch($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_updateCrmSearchProduct(?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $stmt->getResource()->bindParam(2, $param['search_name']);
            $stmt->getResource()->bindParam(3, $param['search_query']);
            $stmt->getResource()->bindParam(4, $param['isActive']);
            $stmt->getResource()->bindParam(5, $param['isDelete']);
            $stmt->getResource()->bindParam(6, $param['modifiend_date']);
            $stmt->getResource()->bindParam(7, $param['search_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will get product info
     * @param array as  product id.
     * @return array as product result
     * @author Icreon Tech -AP
     */
    public function getProductInfo($param = array()) {

        try {
            $productId = (isset($param['id'])) ? $param['id'] : '';
            $productIds = (isset($param['productIds'])) ? $param['productIds'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pro_getCrmProductInfo(?,?)');
            $stmt->getResource()->bindParam(1, $productId);
            $stmt->getResource()->bindParam(2, $productIds);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            //asd($resultSet);
            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the product details
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech -SR
     */
    public function searchCrmProduct($param = array()) {
        try {

            $param['productName'] = (isset($param['productName']) and trim($param['productName']) != "") ? addslashes($param['productName']) : "";
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pro_getCrmProducts(?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['productType']);
            $stmt->getResource()->bindParam(2, $param['productName']);
            $stmt->getResource()->bindParam(3, $param['productSku']);
            $stmt->getResource()->bindParam(4, $param['productCategory']);
            $stmt->getResource()->bindParam(5, $param['sellPriceFrom']);
            $stmt->getResource()->bindParam(6, $param['sellPriceTo']);
            $stmt->getResource()->bindParam(7, $param['promotional']);
            $stmt->getResource()->bindParam(8, $param['featured']);
            $stmt->getResource()->bindParam(9, $param['status']);
            $stmt->getResource()->bindParam(10, $param['available']);
            $stmt->getResource()->bindParam(11, $param['startIndex']);
            $stmt->getResource()->bindParam(12, $param['recordLimit']);
            $stmt->getResource()->bindParam(13, $param['sortField']);
            $stmt->getResource()->bindParam(14, $param['sortOrder']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update the product status
     * @param this will be an array having the product to change the status
     * @return this will return a confirmation
     * @author Icreon Tech -SR
     */
    public function changeProductStatus($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pro_updateProductStatus(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['productId']);
            $stmt->getResource()->bindParam(2, $param['status']);
            $stmt->getResource()->bindParam(3, $param['modifiedDate']);
            $stmt->getResource()->bindParam(4, $param['modifiedBy']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to insert product 
     * @param $param as array 
     * @return boolean value for confirmation
     * @author     Icreon Tech - SR
     */
    public function insertProduct($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pro_insertCrmProduct(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['productType']);
            $stmt->getResource()->bindParam(2, $param['name']);
            $stmt->getResource()->bindParam(3, $param['sku']);
            $stmt->getResource()->bindParam(4, $param['description']);
            $stmt->getResource()->bindParam(5, $param['available']);
            $stmt->getResource()->bindParam(6, $param['productCategory']);
            $stmt->getResource()->bindParam(7, $param['listPrice']);
            $stmt->getResource()->bindParam(8, $param['cost']);
            $stmt->getResource()->bindParam(9, $param['sellPrice']);
            $stmt->getResource()->bindParam(10, $param['membershipDiscount']);
            $stmt->getResource()->bindParam(11, $param['featured']);
            $stmt->getResource()->bindParam(12, $param['inStock']);
            $stmt->getResource()->bindParam(13, $param['inStockUnits']);
            $stmt->getResource()->bindParam(14, $param['threshold']);
            $stmt->getResource()->bindParam(15, $param['isTexable']);
            $stmt->getResource()->bindParam(16, $param['isRefundable']);
            $stmt->getResource()->bindParam(17, $param['isPromotional']);
            $stmt->getResource()->bindParam(18, $param['weight']);
            $stmt->getResource()->bindParam(19, $param['length']);
            $stmt->getResource()->bindParam(20, $param['width']);
            $stmt->getResource()->bindParam(21, $param['height']);
            $stmt->getResource()->bindParam(22, $param['unit']);
            $stmt->getResource()->bindParam(23, $param['addedBy']);
            $stmt->getResource()->bindParam(24, $param['addedDate']);
            $stmt->getResource()->bindParam(25, $param['modifiedBy']);
            $stmt->getResource()->bindParam(26, $param['modifiedDate']);
            $stmt->getResource()->bindParam(27, $param['additional_product_1']);
            $stmt->getResource()->bindParam(28, $param['additional_product_2']);
            $stmt->getResource()->bindParam(29, $param['additional_product_3']);
            $stmt->getResource()->bindParam(30, $param['isPickup']);
            $stmt->getResource()->bindParam(31, $param['shipping_description']);
            $stmt->getResource()->bindParam(32, $param['is_shippable']);
            $stmt->getResource()->bindParam(33, $param['product_transaction_type']);
            $stmt->getResource()->bindParam(34, $param['gl_code_value']);
            $stmt->getResource()->bindParam(35, $param['department']);
            $stmt->getResource()->bindParam(36, $param['designation']);
            $stmt->getResource()->bindParam(37, $param['allocation']);
            $stmt->getResource()->bindParam(38, $param['is_restriction']);

            $stmt->getResource()->bindParam(39, $param['in_ellis_stock']);
            $stmt->getResource()->bindParam(40, $param['total_in_ellis_stock']);
            $stmt->getResource()->bindParam(41, $param['ellis_product_threshold']);
            $stmt->getResource()->bindParam(42, $param['shipping_cost']);
            $stmt->getResource()->bindParam(43, $param['prog_code_value']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to insert product image
     * @param $param as array 
     * @return boolean value for confirmation
     * @author     Icreon Tech - SR
     */
    public function insertProductImage($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pro_insertCrmProductImage(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['productId']);
            $stmt->getResource()->bindParam(2, $param['imageName']);
            $stmt->getResource()->bindParam(3, $param['isDefault']);
            $stmt->getResource()->bindParam(4, $param['addedDate']);
            $stmt->getResource()->bindParam(5, $param['modifiedDate']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            // $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to insert related product
     * @param $param as array 
     * @return boolean value for confirmation
     * @author     Icreon Tech - SR
     */
    public function insertRelatedProduct($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pro_insertCrmReletedProduct(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['productId']);
            $stmt->getResource()->bindParam(2, $param['relatedProductId']);
            $stmt->getResource()->bindParam(3, $param['addedDate']);
            $stmt->getResource()->bindParam(4, $param['modifiedDate']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            // $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to upadate product details
     * @param $param as array 
     * @return boolean value for confirmation
     * @author     Icreon Tech - AP
     */
    public function updateProduct($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pro_updateCrmProduct(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['productId']);
            $stmt->getResource()->bindParam(2, $param['productType']);
            $stmt->getResource()->bindParam(3, $param['name']);
            $stmt->getResource()->bindParam(4, $param['sku']);
            $stmt->getResource()->bindParam(5, $param['description']);
            $stmt->getResource()->bindParam(6, $param['available']);
            $stmt->getResource()->bindParam(7, $param['productCategory']);
            $stmt->getResource()->bindParam(8, $param['listPrice']);
            $stmt->getResource()->bindParam(9, $param['cost']);
            $stmt->getResource()->bindParam(10, $param['sellPrice']);
            $stmt->getResource()->bindParam(11, $param['membershipDiscount']);
            $stmt->getResource()->bindParam(12, $param['featured']);
            $stmt->getResource()->bindParam(13, $param['inStock']);
            $stmt->getResource()->bindParam(14, $param['inStockUnits']);
            $stmt->getResource()->bindParam(15, $param['threshold']);
            $stmt->getResource()->bindParam(16, $param['isTexable']);
            $stmt->getResource()->bindParam(17, $param['isRefundable']);
            $stmt->getResource()->bindParam(18, $param['isPromotional']);
            $stmt->getResource()->bindParam(19, $param['weight']);
            $stmt->getResource()->bindParam(20, $param['length']);
            $stmt->getResource()->bindParam(21, $param['width']);
            $stmt->getResource()->bindParam(22, $param['height']);
            $stmt->getResource()->bindParam(23, $param['unit']);
            $stmt->getResource()->bindParam(24, $param['modifiedBy']);
            $stmt->getResource()->bindParam(25, $param['modifiedDate']);
            $stmt->getResource()->bindParam(26, $param['imageId']);
            $stmt->getResource()->bindParam(27, $param['defaultImageId']);
            $stmt->getResource()->bindParam(28, $param['additional_product_1']);
            $stmt->getResource()->bindParam(29, $param['additional_product_2']);
            $stmt->getResource()->bindParam(30, $param['additional_product_3']);
            $stmt->getResource()->bindParam(31, $param['isStatus']);
            $stmt->getResource()->bindParam(32, $param['isPickup']);
            $stmt->getResource()->bindParam(33, $param['shipping_description']);
            $stmt->getResource()->bindParam(34, $param['is_shippable']);
            $stmt->getResource()->bindParam(35, $param['product_transaction_type']);
            $stmt->getResource()->bindParam(36, $param['gl_code_value']);
            $stmt->getResource()->bindParam(37, $param['department']);
            $stmt->getResource()->bindParam(38, $param['designation']);
            $stmt->getResource()->bindParam(39, $param['allocation']);
            $stmt->getResource()->bindParam(40, $param['is_restriction']);

            $stmt->getResource()->bindParam(41, $param['in_ellis_stock']);
            $stmt->getResource()->bindParam(42, $param['total_in_ellis_stock']);
            $stmt->getResource()->bindParam(43, $param['ellis_product_threshold']);
            $stmt->getResource()->bindParam(44, $param['shipping_cost']);
            $stmt->getResource()->bindParam(45, $param['prog_code_value']);
            $result = $stmt->execute();

            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to delete product images
     * @param $param as array 
     * @return boolean value 
     * @author   Icreon Tech - AP
     */
    public function deleteProductImages($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();

            $stmt->prepare('CALL usp_pro_deleteCrmProductImage(?,?)');
            $stmt->getResource()->bindParam(1, $param['product_image_id']);
            $stmt->getResource()->bindParam(2, $param['product_id']);
            $result = $stmt->execute();

            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get Related product details
     * @param $param as array 
     * @return boolean value 
     * @author   Icreon Tech - AP
     */
    public function getRelatedProduct($param = array()) {
        try {

            $inStockFlag = isset($param['in_stock_flag']) ? $param['in_stock_flag'] : "0";  // 1 - Yes , 0 - No

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pro_getCrmRelatedProducts(?,?)');
            $stmt->getResource()->bindParam(1, $param['id']);
            $stmt->getResource()->bindParam(2, $inStockFlag);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will delete the products
     * @param this will be an array having the params.
     * @return this will return a confirmation
     * @author Icreon Tech -SR
     */
    public function deleteCrmProduct($param = array()) {

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pro_deleteCrmProduct(?,?,?)');
            $stmt->getResource()->bindParam(1, $param['id']);
            $stmt->getResource()->bindParam(2, $param['modifiend_date']);
            $stmt->getResource()->bindParam(3, $param['modifiend_by']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to get all crm  product name and id
     * @author Icreon Tech - AP
     * @return Array
     * @param void
     */
    public function getCrmProducts($param = array()) {
        // asd($param);
        $stmt = $this->dbAdapter->createStatement();
        $productName = (isset($param['product_name']) && $param['product_name'] != '') ? $param['product_name'] : '';
        $productType = (isset($param['product_type']) && $param['product_type'] != '') ? $param['product_type'] : '';
        $productId = (isset($param['product_id']) && $param['product_id'] != '') ? $param['product_id'] : '';
        $categoryId = (isset($param['category_id']) && $param['category_id'] != '') ? $param['category_id'] : '';

        $stmt->prepare("CALL usp_pro_getReletedProducts(?,?,?,?)");
        $stmt->getResource()->bindParam(1, $productName);
        $stmt->getResource()->bindParam(2, $productType);
        $stmt->getResource()->bindParam(3, $productId);
        $stmt->getResource()->bindParam(4, $categoryId);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet,2);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * This function is used to attach the product attributes
     * @param $param as array 
     * @return boolean value for confirmation
     * @author     Icreon Tech - SR
     */
    public function attachProductAttribute($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pro_attachProductAttributes(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['product_id']);
            $stmt->getResource()->bindParam(2, $param['attribute_id']);
            $stmt->getResource()->bindParam(3, $param['added_by']);
            $stmt->getResource()->bindParam(4, $param['added_date']);
            $stmt->getResource()->bindParam(5, $param['modified_by']);
            $stmt->getResource()->bindParam(6, $param['modified_date']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to attach the product attributes options
     * @param $param as array 
     * @return boolean value for confirmation
     * @author     Icreon Tech - SR
     */
    public function attachProductAttributeOptions($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pro_attachProductAttributesOptions(?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['product_id']);
            $stmt->getResource()->bindParam(2, $param['attribute_id']);
            $stmt->getResource()->bindParam(3, $param['option_id']);
            $stmt->getResource()->bindParam(4, $param['option_cost']);
            $stmt->getResource()->bindParam(5, $param['option_price']);
            $stmt->getResource()->bindParam(6, $param['is_default']);
            $stmt->getResource()->bindParam(7, $param['added_by']);
            $stmt->getResource()->bindParam(8, $param['added_date']);
            $stmt->getResource()->bindParam(9, $param['modified_by']);
            $stmt->getResource()->bindParam(10, $param['modified_date']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /* This function is used to remove the product attribute options
     * @param $param as array 
     * @return boolean value for confirmation
     * @author     Icreon Tech - SR
     */

    public function removeAttachProductAttributeOptions($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pro_removeProductAttributesOptions(?)');
            $stmt->getResource()->bindParam(1, $param['product_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to get all crm  product name and id
     * @author Icreon Tech - AP
     * @return Array
     * @param void
     */
    public function getProductAttributeOptions($param = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_pro_getProductsAttributesOptionsAttached(?)");
        $stmt->getResource()->bindParam(1, $param['product_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function to get all product name and id
     * @author Icreon Tech - DG
     * @return Array
     * @param void
     */
    public function getProduct($param = array()) {

        $total_in_stock = (isset($param['total_in_stock']) and trim($param['total_in_stock']) != "") ? $param['total_in_stock'] : "";
        $rand_order_by = (isset($param['rand_order_by']) and trim($param['rand_order_by']) != "") ? $param['rand_order_by'] : "";
        $restricted_category_id = (isset($param['restricted_category_id']) and trim($param['restricted_category_id']) != "") ? $param['restricted_category_id'] : "";

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_pro_getProducts(?,?,?,?,?,?)");
        $stmt->getResource()->bindParam(1, $param['is_available']);
        $stmt->getResource()->bindParam(2, $param['featured']);
        $stmt->getResource()->bindParam(3, $param['offset']);
        $stmt->getResource()->bindParam(4, $total_in_stock);
        $stmt->getResource()->bindParam(5, $rand_order_by);
        $stmt->getResource()->bindParam(6, $restricted_category_id);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * This function is used to get Related product for autocomplete
     * @param $param as array
     * @return array
     * @author   Icreon Tech - DT
     */
    public function getCrmRelatedProducts($param = array()) {
        try {
            $productId = (isset($param['product_id']) && !empty($param['product_id'])) ? $param['product_id'] : '';
            $productName = (isset($param['related_product_name']) && !empty($param['related_product_name'])) ? $param['related_product_name'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_com_getCrmRelatedProducts(?,?)');
            $stmt->getResource()->bindParam(1, $productId);
            $stmt->getResource()->bindParam(2, $productName);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the product for shop
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech -KK
     */
    public function getShopProduct($param = array()) {
        try {

            $param['restricted_category_id'] = (isset($param['restricted_category_id']) and trim($param['restricted_category_id']) != "") ? $param['restricted_category_id'] : "";

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pro_getShopProducts(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['productType']);
            $stmt->getResource()->bindParam(2, $param['productName']);
            $stmt->getResource()->bindParam(3, $param['productSku']);
            $stmt->getResource()->bindParam(4, $param['productCategory']);
            $stmt->getResource()->bindParam(5, $param['sellPriceFrom']);
            $stmt->getResource()->bindParam(6, $param['sellPriceTo']);
            $stmt->getResource()->bindParam(7, $param['promotional']);
            $stmt->getResource()->bindParam(8, $param['featured']);
            $stmt->getResource()->bindParam(9, $param['status']);
            $stmt->getResource()->bindParam(10, $param['available']);
            $stmt->getResource()->bindParam(11, $param['startIndex']);
            $stmt->getResource()->bindParam(12, $param['recordLimit']);
            $stmt->getResource()->bindParam(13, $param['sortField']);
            $stmt->getResource()->bindParam(14, $param['sortOrder']);
            $stmt->getResource()->bindParam(15, $param['product_id']);
            $stmt->getResource()->bindParam(16, $param['restricted_category_id']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            //asd($resultSet);
            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get Cart LIst
     * @author Icreon Tech - DT
     * @return Arr
     * @param Array
     */
    public function getUserMembershipFront($dataArr = null) {
        $membership_id = (isset($dataArr['membership_id']) && $dataArr['membership_id'] != '') ? $dataArr['membership_id'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_usr_getUserMembershipsFront(?)");
        $stmt->getResource()->bindParam(1, $membership_id);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * This function is used to get Related product details
     * @param $param as array 
     * @return boolean value 
     * @author   Icreon Tech - AP
     */
    public function getProductRelatedProduct($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pro_getProductRelatedProducts(?)');
            $stmt->getResource()->bindParam(1, $param['id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to get all product type lookup
     * @author Icreon Tech - DT
     * @return Array
     * @param void
     */
    public function getProductTypes($param = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_pro_getProductsType(?)");
        $stmt->getResource()->bindParam(1, $param[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
    }

    /**
     * Function for get not shippable products
     * @author Icreon Tech - DT
     * @return Int
     * @param void
     */
    public function getProductNotShippable($params = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $productIds = (isset($params['product_ids'])) ? $params['product_ids'] : '';
        $stmt->prepare("CALL usp_pro_getProductNoTShippable(?)");
        $stmt->getResource()->bindParam(1, $productIds);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetch(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
    }

    public function changeProOrder($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pro_changeProOrder(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['id']);
            $stmt->getResource()->bindParam(2, $params['order']);
            $stmt->getResource()->bindParam(3, $params['modifiedBy']);
            $stmt->getResource()->bindParam(4, $params['modifiedDate']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

}