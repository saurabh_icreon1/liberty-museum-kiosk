<?php

/**
 * This file is used for category module all validation and filetering task
 * @package    Products
 * @author     Icreon Tech -DT.
 */

namespace Product\Model;

use Base\Model\BaseModel;
use Product\Module;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\Adapter\Adapter;

/**
 * This class is used for category module all validation and filetering task
 * @package    Products_Category
 * @author     Icreon Tech -DT.
 */
class Category extends BaseModel {

    protected $inputFilter;
    protected $searchParam;
    protected $inputFilterCrmSearch;
    protected $adapter;

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    /**
     * This method is used to return the stored procedure array for category listing
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getCategoriesArr($dataArr=array()) {

        $returnArr = array();
        $returnArr[] = (isset($dataArr['category_id']) && $dataArr['category_id'] != '') ? $dataArr['category_id'] : '';
        $returnArr[] = (isset($dataArr['parent_id']) && $dataArr['parent_id'] != '') ? $dataArr['parent_id'] : '';
        $returnArr[] = (isset($dataArr['category_name']) && $dataArr['category_name'] != '') ? $dataArr['category_name'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $returnArr[] = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : 'parent_id,sort_order';
        $returnArr[] = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : 'asc';
        return $returnArr;
    }

    /**
     * This method is used to add filtering and validation to the form elements of create category
     * @param void
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getInputFilterCategory() {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'category_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'CATEGORY_NAME_EMPTY',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[A-Za-z0-9 -]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'VALID_DETAILS'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'category_description',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'DESCRIPTION_EMPTY',
                                    ),
                                ),
                            ),
//                            array(
//                                'name' => 'regex', false,
//                                'options' => array(
//                                    'pattern' => '/^[A-Za-z0-9 ,".-½]+$/',
//                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'VALID_DETAILS'
//                                    ),
//                                ),
//                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'parent_id',
                        'required' => false,
                    )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

     /**
     * This method is used to call add category procedure parameter
     * @param array
     * @return array
     * @author Icreon Tech - DT
     */
    public function getCreateCategoryArr($data) {
        $returnArr = array();
        $returnArr[]=(isset($data['parent_id']) && !empty($data['parent_id']))?$data['parent_id']:'0';
        $returnArr[]=(isset($data['category_name']) && !empty($data['category_name']))?$data['category_name']:'';
        $returnArr[]=(isset($data['category_description']) && !empty($data['category_description']))?$data['category_description']:'';
        $returnArr[]=(isset($data['added_by']) && !empty($data['added_by']))?$data['added_by']:'';
        $returnArr[]=(isset($data['added_date']) && !empty($data['added_date']))?$data['added_date']:'';
        $returnArr[]=(isset($data['modified_by']) && !empty($data['modified_by']))?$data['modified_by']:'';
        $returnArr[]=(isset($data['modified_date']) && !empty($data['modified_date']))?$data['modified_date']:'';
        return $returnArr;
    }

    /**
     * This method is used to call edit category procedure parameter
     * @param array
     * @return array
     * @author Icreon Tech - DT
     */
    public function getEditCategoryArr($data) {
        $returnArr = array();
        $returnArr[]=(isset($data['category_id']) && !empty($data['category_id']))?$data['category_id']:'0';
        $returnArr[]=(isset($data['parent_id']) && !empty($data['parent_id']))?$data['parent_id']:'0';
        $returnArr[]=(isset($data['category_name']) && !empty($data['category_name']))?$data['category_name']:'';
        $returnArr[]=(isset($data['category_description']) && !empty($data['category_description']))?$data['category_description']:'';
        $returnArr[]=(isset($data['modified_by']) && !empty($data['modified_by']))?$data['modified_by']:'';
        $returnArr[]=(isset($data['modified_date']) && !empty($data['modified_date']))?$data['modified_date']:'';
        return $returnArr;
    }

     /**
     * This method is used to call get category info procedure parameter
     * @param array
     * @return array
     * @author Icreon Tech - DT
     */
    public function getCategoryIntoArr($data) {
        $returnArr = array();
        $returnArr[]=(isset($data['category_id']) && !empty($data['category_id']))?$data['category_id']:'';
        $returnArr[]=(isset($data['category_name']) && !empty($data['category_name']))?$data['category_name']:'';
        $returnArr[]=(isset($data['not_category_id']) && !empty($data['not_category_id']))?$data['not_category_id']:'';
        return $returnArr;
    }

    /**
     * Function used to exchange the values
     * @author Icreon Tech - SR
     * @return $returnArray
     * @param $data array
     */
    public function exchangeArray($data) {
        
    }

    /**
     * Function used to pass for input filter
     * @author Icreon Tech - SR
     * @return array
     * @param Array
     */
    public function getInputFilterCrmSearch($filterData) {
        $inputFilterData = array();
        if (is_array($filterData)) {
            foreach ($filterData as $key => $val) {
                $inputFilterData[$key] = trim(addslashes(strip_tags($val)));
            }
        } else {
            $inputFilterData = trim(addslashes(strip_tags($filterData)));
        }
        return $this->inputFilterCrmSearch = $inputFilterData;
    }

}