<?php

/**
 * This is used for product attribute module.
 * @package    Product
 * @author     Icreon Tech - AS
 */

namespace Product\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This class is used for product attribute module.
 * @package    Product
 * @author     Icreon Tech - AS
 */
class AttributeTable {

    protected $tableGateway;
    protected $connection;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
        $this->connection = $this->tableGateway->getAdapter()->getDriver()->getConnection();
    }

    /**
     * This function is used to insert product attribute
     * @param $param as array 
     * @return boolean value for confirmation
     * @author     Icreon Tech - AS
     */
    public function insertProductAttribute($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pro_insertProductAttribute(?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['name']);
            $stmt->getResource()->bindParam(2, $params['label']);
            $stmt->getResource()->bindParam(3, $params['helpText']);
            $stmt->getResource()->bindParam(4, $params['makeRequired']);
            $stmt->getResource()->bindParam(5, $params['displayType']);
            $stmt->getResource()->bindParam(6, $params['addedDate']);
            $stmt->getResource()->bindParam(7, $params['addedBy']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /* This function is used to list product attribute
     * @param $param as array 
     * @return boolean value for confirmation
     * @author     Icreon Tech - AS
     */

    public function searchProductAttributes($params = array()) {
        try { //asd($params);
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pro_getProductAttributes(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['startIndex']);
            $stmt->getResource()->bindParam(2, $params['recordLimit']);
            $stmt->getResource()->bindParam(3, $params['sortField']);
            $stmt->getResource()->bindParam(4, $params['sortOrder']);
            $stmt->getResource()->bindParam(5, $params['attributeId']);
            $stmt->getResource()->bindParam(6, $params['not_inattributeId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            // asd($resultSet);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /* This function is used to delete product attribute
     * @param $param as array 
     * @return boolean value for confirmation
     * @author     Icreon Tech - AS
     */

    public function deleteProductAttribute($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pro_deleteProductAttribute(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['attributeId']);
            $stmt->getResource()->bindParam(2, $params['isDelete']);
            $stmt->getResource()->bindParam(3, $params['modifiedBy']);
            $stmt->getResource()->bindParam(4, $params['modifiedDate']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /* This function is used to update product attribute
     * @param $param as array 
     * @return boolean value for confirmation
     * @author     Icreon Tech - AS
     */

    public function updateProductAttribute($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pro_updateProductAttribute(?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['name']);
            $stmt->getResource()->bindParam(2, $params['label']);
            $stmt->getResource()->bindParam(3, $params['helpText']);
            $stmt->getResource()->bindParam(4, $params['makeRequired']);
            $stmt->getResource()->bindParam(5, $params['displayType']);
            $stmt->getResource()->bindParam(6, $params['modifiedDate']);
            $stmt->getResource()->bindParam(7, $params['modifiedBy']);
            $stmt->getResource()->bindParam(8, $params['id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /* This function is used to search product attribute options
     * @param $param as array 
     * @return boolean value for confirmation
     * @author Icreon Tech - AS
     */

    public function searchAttributeOptions($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pro_getProductAttributeOptions(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['startIndex']);
            $stmt->getResource()->bindParam(2, $params['recordLimit']);
            $stmt->getResource()->bindParam(3, $params['sortField']);
            $stmt->getResource()->bindParam(4, $params['sortOrder']);
            $stmt->getResource()->bindParam(5, $params['attributeId']);
            $stmt->getResource()->bindParam(6, $params['option']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /* This function is used to delete product attribute
     * @param $param as array 
     * @return boolean value for confirmation
     * @author     Icreon Tech - AS
     */

    public function deleteProductAttributeOption($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pro_deleteProductAttributeOption(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['attributeId']);
            $stmt->getResource()->bindParam(2, $params['attributeOptionId']);
            $stmt->getResource()->bindParam(3, $params['isDelete']);
            $stmt->getResource()->bindParam(4, $params['modifiedBy']);
            $stmt->getResource()->bindParam(5, $params['modifiedDate']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to insert product attribute option
     * @param $param as array 
     * @return boolean value for confirmation
     * @author     Icreon Tech - AS
     */
    public function insertProductAttributeOption($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pro_insertProductAttributeOption(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['attributeId']);
            $stmt->getResource()->bindParam(2, $params['name']);
            $stmt->getResource()->bindParam(3, $params['cost']);
            $stmt->getResource()->bindParam(4, $params['price']);
            $stmt->getResource()->bindParam(5, $params['addedDate']);
            $stmt->getResource()->bindParam(6, $params['addedBy']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to insert product attribute option
     * @param $param as array 
     * @return boolean value for confirmation
     * @author     Icreon Tech - AS
     */
    public function updateProductAttributeOption($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pro_updateProductAttributeOption(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['attributeOptionId']);
            $stmt->getResource()->bindParam(2, $params['name']);
            $stmt->getResource()->bindParam(3, $params['cost']);
            $stmt->getResource()->bindParam(4, $params['price']);
            $stmt->getResource()->bindParam(5, $params['addedDate']);
            $stmt->getResource()->bindParam(6, $params['addedBy']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /* This function is used to list attached product attribute
     * @param $param as array 
     * @return boolean value for confirmation
     * @author     Icreon Tech - SR
     */

    public function searchAttachedProductAttributes($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pro_getAttachedProductAttributes(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['productId']);
            $stmt->getResource()->bindParam(2, $param['startIndex']);
            $stmt->getResource()->bindParam(3, $param['recordLimit']);
            $stmt->getResource()->bindParam(4, $param['sortField']);
            $stmt->getResource()->bindParam(5, $param['sortOrder']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /* This function is used to remove the product attribute
     * @param $param as array 
     * @return boolean value for confirmation
     * @author     Icreon Tech - SR
     */

    public function removeProductAttribute($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pro_removeProductAttributes(?,?)');
            $stmt->getResource()->bindParam(1, $param['product_attribute_id']);
            $stmt->getResource()->bindParam(2, $param['product_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /* This function is used to change the required of product attribute
     * @param $param as array 
     * @return boolean value for confirmation
     * @author     Icreon Tech - SR
     */

    public function changeAttachedProductAttribute($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pro_changeProductAttributes(?,?)');
            $stmt->getResource()->bindParam(1, $param['product_attribute_id']);
            $stmt->getResource()->bindParam(2, $param['product_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

}