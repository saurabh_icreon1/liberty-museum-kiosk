<?php

/**
 * This is used for Products Attribute module.
 * @package    Products
 * @author     Icreon Tech - AS.
 */

namespace Product\Model;

use Base\Model\BaseModel;
use Product\Module;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\Adapter\Adapter;

class Attribute extends BaseModel {

    protected $inputFilter;
    protected $searchParam;
    protected $inputFilterCrmSearch;
    protected $adapter;

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    /**
     * Function used to exchange the values
     * @return $returnArray
     * @param $data array
     * @author Icreon Tech - AS
     */
    public function exchangeArray($data) {
        $this->name = (isset($data['name']) && !empty($data['name'])) ? $data['name'] : '';
        $this->label = (isset($data['label']) && !empty($data['label'])) ? $data['label'] : '';
        $this->helpText = (isset($data['helpText']) && !empty($data['helpText'])) ? $data['helpText'] : '';
        $this->displayType = (isset($data['displayType']) && !empty($data['displayType'])) ? $data['displayType'] : '';
        $this->makeRequired = (isset($data['makeRequired']) && !empty($data['makeRequired'])) ? $data['makeRequired'] : '';
        $this->id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : '';
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    /**
     * Function used to pass for input filter
     * @return array
     * @param Array
     * @author Icreon Tech - AS
     */
    public function getInputFilterAddAttribute() {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                        'name' => 'name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'NAME_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'label',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'LABEL_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'displayType',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'DISPLAY_TYPE_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'helpText',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'makeRequired',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'id',
                        'required' => false,
                    )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
    
    /**
     * Function used to pass for input filter
     * @return array
     * @param Array
     * @author Icreon Tech - AS
     */
    public function getInputFilterAddAttributeOption() {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                        'name' => 'name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'NAME_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'cost',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            
            $inputFilter->add($factory->createInput(array(
                        'name' => 'price',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
    
    /**
     * Function used to exchange the values
     * @return $returnArray
     * @param $data array
     * @author Icreon Tech - AS
     */
    public function exchangeOptionArray($data) {
        $this->id = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : '';
        $this->name = (isset($data['name']) && !empty($data['name'])) ? $data['name'] : '';
        $this->cost = (isset($data['cost']) && !empty($data['cost'])) ? $data['cost'] : '';
        $this->price = (isset($data['price']) && !empty($data['price'])) ? $data['price'] : '';
     }
    

}