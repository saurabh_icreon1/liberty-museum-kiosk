<?php

/**
 * This is used for Products module.
 * @package    Products
 * @author     Icreon Tech -AP.
 */

namespace Product\Model;

use Base\Model\BaseModel;
use Product\Module;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\Adapter\Adapter;

class Product extends BaseModel {

    protected $inputFilter;
    protected $searchParam;
    protected $inputFilterCrmSearch;
    protected $adapter;
    public $first_name;

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    /**
     * Function used to exchange the values
     * @author Icreon Tech - SR
     * @return $returnArray
     * @param $data array
     */
    public function exchangeArray($data) {
        $this->productType = (isset($data['product_type']) && !empty($data['product_type'])) ? $data['product_type'] : '';
        $this->name = (isset($data['name']) && !empty($data['name'])) ? $data['name'] : '';
        $this->sku = (isset($data['sku']) && !empty($data['sku'])) ? $data['sku'] : '';
        $this->description = (isset($data['description']) && !empty($data['description'])) ? $data['description'] : '';
        $this->available = (isset($data['available']) && !empty($data['available'])) ? $data['available'] : '';
        $this->productCategory = (isset($data['product_category']) && !empty($data['product_category'])) ? $data['product_category'] : '';
        $this->listPrice = (isset($data['list_price']) && !empty($data['list_price'])) ? $data['list_price'] : '';
        $this->cost = (isset($data['cost']) && !empty($data['cost'])) ? $data['cost'] : '';
        $this->sellPrice = (isset($data['sell_price']) && !empty($data['sell_price'])) ? $data['sell_price'] : '';
        $this->featured = (isset($data['featured']) && !empty($data['featured'])) ? $data['featured'] : '0';
        $this->membershipDiscount = (isset($data['membership_discount']) && !empty($data['membership_discount'])) ? $data['membership_discount'] : '0';
        $this->inStock = (isset($data['in_stock']) && !empty($data['in_stock'])) ? $data['in_stock'] : '0';
        $this->isTexable = (isset($data['is_texable']) && !empty($data['is_texable'])) ? $data['is_texable'] : '0';
        $this->isRefundable = (isset($data['is_refundable']) && !empty($data['is_refundable'])) ? $data['is_refundable'] : '0';
        $this->isPromotional = (isset($data['is_promotional']) && !empty($data['is_promotional'])) ? $data['is_promotional'] : '0';
        $this->weight = (isset($data['weight']) && !empty($data['weight'])) ? $data['weight'] : '';
        $this->length = (isset($data['length']) && !empty($data['length'])) ? $data['length'] : '';
        $this->width = (isset($data['width']) && !empty($data['width'])) ? $data['width'] : '';
        $this->height = (isset($data['height']) && !empty($data['height'])) ? $data['height'] : '';
        $this->unit = (isset($data['unit']) && !empty($data['unit'])) ? $data['unit'] : '';
        $this->productName = (isset($data['product_name']) && !empty($data['product_name'])) ? $data['product_name'] : '';
        $this->selectProduct = (isset($data['select_product']) && !empty($data['select_product'])) ? $data['select_product'] : '';
        $this->inStockUnits = (isset($data['in_Stock_units']) && !empty($data['in_Stock_units'])) ? $data['in_Stock_units'] : '';
        $this->threshold = (isset($data['threshold']) && !empty($data['threshold'])) ? $data['threshold'] : '';
        $this->productId = (isset($data['threshold']) && !empty($data['product_id'])) ? $data['product_id'] : '';
        $this->additional_product_1 = (isset($data['additional_product_1']) && !empty($data['additional_product_1'])) ? $data['additional_product_1'] : '';
        $this->additional_product_2 = (isset($data['additional_product_2']) && !empty($data['additional_product_2'])) ? $data['additional_product_2'] : '';
        $this->additional_product_3 = (isset($data['additional_product_3']) && !empty($data['additional_product_3'])) ? $data['additional_product_3'] : '';
        $this->isPickup = (isset($data['is_pickup']) && !empty($data['is_pickup'])) ? $data['is_pickup'] : '0';
        $this->status = (isset($data['status'])) ? $data['status'] : '';
        $this->shipping_description = (isset($data['shipping_description']) && !empty($data['shipping_description'])) ? $data['shipping_description'] : '';
        $this->is_shippable = (isset($data['is_shippable']) && !empty($data['is_shippable'])) ? $data['is_shippable'] : '0';
        $this->product_transaction_type = (isset($data['product_transaction_type']) && !empty($data['product_transaction_type'])) ? $data['product_transaction_type'] : '2';
        $this->gl_code_value = (isset($data['gl_code_value']) && !empty($data['gl_code_value'])) ? $data['gl_code_value'] : '';
        $this->prog_code_value = (isset($data['prog_code_value']) && !empty($data['prog_code_value'])) ? $data['prog_code_value'] : '';
        $this->department = (isset($data['department']) && !empty($data['department'])) ? $data['department'] : '';
        $this->designation = (isset($data['designation']) && !empty($data['designation'])) ? $data['designation'] : '';
        $this->allocation = (isset($data['allocation']) && !empty($data['allocation'])) ? $data['allocation'] : '';
        $this->is_restriction = (isset($data['is_restriction']) && !empty($data['is_restriction'])) ? $data['is_restriction'] : '0';
        $this->in_ellis_stock = (isset($data['in_ellis_stock']) && !empty($data['in_ellis_stock'])) ? $data['in_ellis_stock'] : '0';        
        $this->total_in_ellis_stock = (isset($data['total_in_ellis_stock']) && !empty($data['total_in_ellis_stock'])) ? $data['total_in_ellis_stock'] : '';
        $this->ellis_product_threshold = (isset($data['ellis_product_threshold']) && !empty($data['ellis_product_threshold'])) ? $data['ellis_product_threshold'] : '';        
        $this->shipping_cost = (isset($data['shipping_cost']) && !empty($data['shipping_cost'])) ? $data['shipping_cost'] : '';        
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    /**
     * Function used to exchange manifest values (assignment of values)
     * @author Icreon Tech - SR
     * @return $returnArray
     * @param $dataArr array
     */
    public function assignProductArray($dataArr = array()) {
        $catArr = array();
        $catArr = is_array($dataArr['productCategory'])?implode(',', $dataArr['productCategory']):$dataArr['productCategory'];
        $returnArray = array();
        $returnArray['productType'] = isset($dataArr['productType']) ? $dataArr['productType'] : '';
        $returnArray['productName'] = isset($dataArr['productName']) ? trim($dataArr['productName']) : '';
        $returnArray['productSku'] = isset($dataArr['productSku']) ? trim($dataArr['productSku']) : '';
        $returnArray['productCategory'] = isset($catArr) ? $catArr : '';
        $returnArray['sellPriceFrom'] = isset($dataArr['sellPriceFrom']) ? trim($dataArr['sellPriceFrom']) : '';
        $returnArray['sellPriceTo'] = isset($dataArr['sellPriceTo']) ? trim($dataArr['sellPriceTo']) : '';
        $returnArray['promotional'] = isset($dataArr['promotional']) ? $dataArr['promotional'] : '';
        $returnArray['featured'] = isset($dataArr['featured']) ? $dataArr['featured'] : '';
        $returnArray['status'] = isset($dataArr['status']) ? $dataArr['status'] : '';
        $returnArray['available'] = isset($dataArr['available']) ? $dataArr['available'] : '';
        return $returnArray;
    }

    /**
     * Function used to pass for input filter
     * @author Icreon Tech - SR
     * @return array
     * @param Array
     */
    public function getInputFilterCrmSearch($filterData) {
        $inputFilterData = array();
        if (is_array($filterData)) {
            foreach ($filterData as $key => $val) {
                if (is_array($val)) {
                    foreach ($val as $key2 => $val2) {
                        $inputFilterData[$key][$key2] = trim(addslashes(strip_tags($val2)));
                    }
                } else {
                    $inputFilterData[$key] = trim(addslashes(strip_tags($val)));
                }
            }
        } else {
            $inputFilterData = trim(addslashes(strip_tags($filterData)));
        }
        return $this->inputFilterCrmSearch = $inputFilterData;
    }

    /**
     * Function used to pass for input filter
     * @author Icreon Tech - AP
     * @return array
     * @param Array
     */
    public function getInputFilterAddProduct() {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'product_type',
                        'required' => false,
//                        'validators' => array(
//                            array(
//                                'name' => 'NotEmpty',
//                                'options' => array(
//                                    'messages' => array(
//                                        'isEmpty' => 'PRODUCT_TYPE_EMPTY',
//                                    ),
//                                ),
//                            ),
//                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'NAME_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'sku',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'SKU_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'description',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'DESCRIPTION_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'available',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'AVAILABLE_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'product_category',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'list_price',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'cost',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'sell_price',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'featured',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'membership_discount',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'in_stock',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'in_ellis_stock',
                        'required' => false,
                    )));            
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_texable',
                        'required' => false
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_refundable',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_promotional',
                        'required' => false
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'weight',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'length',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'width',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'height',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'unit',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'product_name',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'select_product',
                        'required' => false
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'additional_product_1',
                        'required' => false
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'additional_product_2',
                        'required' => false
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'additional_product_3',
                        'required' => false
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'status',
                        'required' => false
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'default_image',
                        'required' => false
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_shippable',
                        'required' => false
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'product_transaction_type',
                        'required' => false
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'gl_code_value',
                        'required' => false
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'department',
                        'required' => false
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'designation',
                        'required' => false
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'allocation',
                        'required' => false
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'shipping_cost',
                        'required' => false
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_restriction',
                        'required' => false
                    )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    /**
     * Function used to get product type look up array
     * @author Icreon Tech - SR
     * @return $returnArray
     * @param $dataArr array
     */
    public function getProductTypeArr($dataArr = array()) {
        $returnArray = array();
        $returnArray[] = isset($dataArr['product_type']) ? $dataArr['product_type'] : '';
        return $returnArray;
    }

}