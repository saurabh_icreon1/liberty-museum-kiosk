<?php

/**
 * This file is used for all database related task of category module
 * @package    Products
 * @author     Icreon Tech -DT.
 */

namespace Product\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This class is used for all database related task of category module
 * @package    Products_CategoryTable
 * @author     Icreon Tech -DT.
 */
class CategoryTable {

    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
    }

    /**
     * Function for get categories
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function getCategories($categoryData) {
        
        $procquesmarkapp=$this->appendQuestionMars(count($categoryData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_pro_getProductCategories(".$procquesmarkapp.")");
        $stmt->getResource()->bindParam(1, $categoryData[0]);
        $stmt->getResource()->bindParam(2, $categoryData[1]);
        $stmt->getResource()->bindParam(3, $categoryData[2]);
        $stmt->getResource()->bindParam(4, $categoryData[3]);
        $stmt->getResource()->bindParam(5, $categoryData[4]);
        $stmt->getResource()->bindParam(6, $categoryData[5]);
        $stmt->getResource()->bindParam(7, $categoryData[6]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

     /**
     * Function for insert category data
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function saveCategory($categoryData) {
        $procquesmarkapp=$this->appendQuestionMars(count($categoryData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_pro_insertProductCategory(".$procquesmarkapp.")");
        $stmt->getResource()->bindParam(1, $categoryData[0]);
        $stmt->getResource()->bindParam(2, $categoryData[1]);
        $stmt->getResource()->bindParam(3, $categoryData[2]);
        $stmt->getResource()->bindParam(4, $categoryData[3]);
        $stmt->getResource()->bindParam(5, $categoryData[4]);
        $stmt->getResource()->bindParam(6, $categoryData[5]);
        $stmt->getResource()->bindParam(7, $categoryData[6]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

     /**
     * Function for get category info
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function getCategoryInfo($categoryData)
    {
        $procquesmarkapp=$this->appendQuestionMars(count($categoryData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_pro_getProductCategoryInfo(".$procquesmarkapp.")");
        $stmt->getResource()->bindParam(1, $categoryData[0]);
        $stmt->getResource()->bindParam(2, $categoryData[1]);
        $stmt->getResource()->bindParam(3, $categoryData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet[0];
        else
            return false;
    }

    /**
     * Function for edit case data
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function editCategory($categoryData) {
        $procquesmarkapp=$this->appendQuestionMars(count($categoryData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_pro_updateProductCategory(".$procquesmarkapp.")");
        $stmt->getResource()->bindParam(1, $categoryData[0]);
        $stmt->getResource()->bindParam(2, $categoryData[1]);
        $stmt->getResource()->bindParam(3, $categoryData[2]);
        $stmt->getResource()->bindParam(4, $categoryData[3]);
        $stmt->getResource()->bindParam(5, $categoryData[4]);
        $stmt->getResource()->bindParam(6, $categoryData[5]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for remove category and subcategories of category
     * @author Icreon Tech - DT
     * @return Void
     * @param Array
     */
    public function removeCategories($categoryData) {
        $procquesmarkapp=$this->appendQuestionMars(count($categoryData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_pro_deleteProductCategory(".$procquesmarkapp.")");
        $stmt->getResource()->bindParam(1, $categoryData[0]);
        $stmt->getResource()->bindParam(2, $categoryData[1]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for change order of category
     * @author Icreon Tech - DT
     * @return Void
     * @param Array
     */
    public function changeOrderCategory($categoryData) {
        $procquesmarkapp=$this->appendQuestionMars(count($categoryData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_pro_updateCategoryOrder(".$procquesmarkapp.")");
        $stmt->getResource()->bindParam(1, $categoryData[0]);
        $stmt->getResource()->bindParam(2, $categoryData[1]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement = $result->getResource();
        $statement->closeCursor();
    }
   

    /**
     * Function for append number of questions marks for stored procedure
     * @author Icreon Tech - DT
     * @return String
     * @param Int
     */
    public function appendQuestionMars($count) {
        $returnstr='';
        for($i=1;$i<=$count;$i++)
        {
            $returnstr.='?,';
        }
        return rtrim($returnstr,',');
    }

    /**
     * Function for get category by product type id
     * @author Icreon Tech - DT
     * @return Void
     * @param Array
     */
    public function getCategoryByProductTypeId($categoryData) {
        
        $categoryData['restricted_category_id'] = (isset($categoryData['restricted_category_id']) and trim($categoryData['restricted_category_id']) != "") ? $categoryData['restricted_category_id'] : "";
        
        $procquesmarkapp=$this->appendQuestionMars(count($categoryData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_pro_getCategoryOfProductType(".$procquesmarkapp.")");
        $stmt->getResource()->bindParam(1, $categoryData[0]);
        $stmt->getResource()->bindParam(2, $categoryData['restricted_category_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return array();
        }
    }


}