<?php

/**
 * This file is  used for product form.
 * @package    Product
 * @author     Icreon Tech -AP.
 */

namespace Product\Form;

use Zend\Form\Form;

/**
 * This class is used for product form.
 * @author     Icreon Tech -AP.
 */
class UploadFileForm extends Form {

    public function __construct($name = null) {
        parent::__construct('uplaodFile');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->add(array(
            'name' => 'file_name0',
            'attributes' => array(
                'type' => 'file',
                'class' => 'fileUploadAddMore',
                'id' => 'file_name0',
            ),
        ));
        $this->add(array(
            'name' => 'file_name_value[]',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'file_name_value0',
            ),
        ));
//        $this->add(array(
//            'name' => 'file_name_count[]',
//            'attributes' => array(
//                'type' => 'hidden',
//                'id' => 'file_name_count_0',
//                'value' => '0',
//            ),
//        ));
    }

}