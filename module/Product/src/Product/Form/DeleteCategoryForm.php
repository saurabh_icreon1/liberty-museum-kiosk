<?php

/**
 * This is used for delete category form.
 * @package    Category
 * @author     Icreon Tech -DT.
 */

namespace Product\Form;

use Zend\Form\Form;

/**
 * This class  used for delete category form.
 * @package    Category_DeleteCategoryForm
 * @author     Icreon Tech -DT.
 *
 * 
 */
class DeleteCategoryForm extends Form {

    public function __construct($name = null) {
        parent::__construct('delete_category_form');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'type' => 'hidden',
            'name' => 'category_id',
            'attributes' => array(
                'id' => 'category_id',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_move',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'class' => 'checkbox',
                'id' => 'is_move',
                'class' => 'e2'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'new_category_id',
            'attributes' => array(
                'id' => 'new_category_id',
                'class' => 'select-w-320 e1'
            )
        ));
        $this->add(array(
            'name' => 'yes_delete_category',
            'attributes' => array(
                'type' => 'button',
                'id' => 'yes_delete_category',
                'class' => 'blue-btn m-r-10',
                'value' => 'YES'
            )
        ));
        $this->add(array(
            'name' => 'no_delete_category',
            'attributes' => array(
                'type' => 'button',
                'id' => 'no_delete_category',
                'class' => 'green-btn',
                'value' => 'NO',
            )
        ));
    }

}