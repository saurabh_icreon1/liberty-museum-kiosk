<?php

/**
 * This form is used for Seach Product.
 * @package    Product
 * @author     Icreon Tech - SR
 */

namespace Product\Form;

use Zend\Form\Form;

/**
 * This form is used for Seach Product.
 * @author     Icreon Tech - SR
 */
class SearchProductForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('search-product');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'type' => 'select',
            'name' => 'productType',
            'attributes' => array(
                'id' => 'productType',
                'class' => 'e1 select-w-320',
            ),
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    'T715' => 'T715',
                    'M237' => 'M237',
                )
            )
        ));
        $this->add(array(
            'name' => 'productName',
            'attributes' => array(
                'type' => 'text',
                'id' => 'productName'
            )
        ));
        $this->add(array(
            'name' => 'productSku',
            'attributes' => array(
                'type' => 'text',
                'id' => 'productSku'
            )
        ));
        $this->add(array(
            'type' => 'select',
            'name' => 'productCategory',
            'attributes' => array(
                'id' => 'productCategory',
                'class' => 'e1 select-w-320',
                'multiple'=>'multiple'
            )
        ));
        $this->add(array(
            'name' => 'sellPriceFrom',
            'attributes' => array(
                'type' => 'text',
                'id' => 'sellPriceFrom',
                'class'=>'width-125',
                'style'=>'width:103px'
            )
        ));
        $this->add(array(
            'name' => 'sellPriceTo',
            'attributes' => array(
                'type' => 'text',
                'id' => 'sellPriceTo',
                'class'=>'width-125',
                'style'=>'width:103px'
            )
        ));
        $this->add(array(
            'type' => 'select',
            'name' => 'status',
            'attributes' => array(
                'id' => 'status',
                'class' => 'e1 select-w-320',
            ),
            
        ));
        $this->add(array(
            'type' => 'select',
            'name' => 'available',
            'attributes' => array(
                'id' => 'available',
                'class' => 'e1 select-w-320',
            )
        ));
        
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'promotional',
               'attributes' => array(
                'value' => '', /* set checked to '1' */
                'class' => 'e2',
                'id' => 'promotional',
            )
        ));
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'featured',
            'attributes' => array(
                'id' => 'featured',
                'value' => '', /* set checked to '1' */
                'class' => 'e2',
            )
        )); 
        
        $this->add(array(
            'name' => 'search',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Search',
                'id' => 'search',
                'class' => 'search-btn',
            ),
        ));
        $this->add(array(
            'name' => 'update',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Update',
                'id' => 'update',
                'class' => 'save-btn',
            ),
        ));
        
       


        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Cancel',
                'id' => 'cancel',
                'class' => 'cancel-btn m-l-10',
            ),
        ));
    }

}
