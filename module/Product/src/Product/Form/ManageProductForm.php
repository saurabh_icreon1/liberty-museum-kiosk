<?php

/**
* This is used for product attribute form.
* @package    Product
* @author     Icreon Tech - SR.
*/ 

namespace Product\Form;
use Zend\Form\Form;
class ManageProductForm extends Form {
    public function __construct($name = null) 
    {
        parent::__construct('attributeList');
        $this->setAttribute('method', 'post');
        
       
        $this->add(array(
            'name' => 'save',
            'attributes' => array(
                'type'  => 'Submit',
                'id' => 'save',
                'class' => 'save-btn',
                'value' => 'Save'
            ),
        ));
        $this->add(array(
            'name' => 'save_continue',
            'attributes' => array(
                'type'  => 'submit',
                'id' => 'save_continue',
                'class' => 'save-btn m-l-10',
                'value' => 'Save and Continue'

            ),
        )); 
    }
}