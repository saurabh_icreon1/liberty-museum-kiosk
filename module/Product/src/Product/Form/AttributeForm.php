<?php

/**
* This is used for product attribute form.
* @package    Category
* @author     Icreon Tech -AP.
*/ 

namespace Product\Form;
use Zend\Form\Form;
class AttributeForm extends Form {
    public function __construct($name = null) 
    {
        parent::__construct('createAttribute');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type'  => 'text',
                'id' => 'name'
            ),
        )); 
        $this->add(array(
            'name' => 'label',
            'attributes' => array(
                'type'  => 'text',
                'id' => 'label'
            ),
        )); 
        $this->add(array(
            'name' => 'helpText',
            'attributes' => array(
                'type'  => 'text',
                'id' => 'helpText'            ),
        )); 
       $this->add(array(
            'type' => 'Select',
            'name' => 'displayType',
            'attributes' => array(
                'id' => 'displayType',
                'class' => 'e1 select-w-320'
            )
        ));
       $this->add(array(
            'type' => 'Checkbox',
            'name' => 'makeRequired',
            'attributes' => array(
                'id' => 'makeRequired',
                'class' => 'e2'
            )
        ));
        $this->add(array(
            'name' => 'save',
            'attributes' => array(
                'type'  => 'Submit',
                'id' => 'save',
                'class' => 'save-btn',
                'value' => 'Save'
            ),
        ));
        $this->add(array(
            'name' => 'update',
            'attributes' => array(
                'type'  => 'submit',
                'id' => 'update',
                'class' => 'save-btn',
                'value' => 'Update'
            ),
        )); 
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
                'id' => 'id',
                'value' => '0'
            ),
        )); 
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type'  => 'button',
                'id' => 'cancel',
                'class' => 'cancel-btn m-l-10',
                'value' => 'Cancel',
                'onclick' => 'window.location.href="/crm-product-attributes"'
            ),
        )); 
    }
}