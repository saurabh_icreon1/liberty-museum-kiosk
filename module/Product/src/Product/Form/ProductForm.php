<?php

/**
 * This file is  used for product form.
 * @package    Product
 * @author     Icreon Tech -AP.
 */

namespace Product\Form;

use Zend\Form\Form;

/**
 * This class is used for product form.
 * @author     Icreon Tech -AP.
 */
class ProductForm extends Form {

    public function __construct($name = null) {
        parent::__construct('createProduct');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'product_type',
            'attributes' => array(
                //    'multiple' => 'multiple',
                'id' => 'product_type',
                'size' => '4',
                'class' => 'e1 select-w-320',
                'onChange' => 'javascript:showOptions(this.value);',
            )
        ));
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'name'
            ),
        ));
        $this->add(array(
            'name' => 'sku',
            'attributes' => array(
                'type' => 'text',
                'id' => 'sku'
            ),
        ));
        $this->add(array(
            'name' => 'description',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'description',
                'class' => 'width-65'
            ),
        ));
        $this->add(array(
            'name' => 'shipping_description',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'shipping_description',
                'class' => 'width-65'
            ),
        ));        
        
        $this->add(array(
            'type' => 'MultiCheckbox',
            'name' => 'available',
            'options' => array(
                'value_options' => array(
                    '1' => 'Web',
                    '2' => 'Ellis Island'
                ),
            ),
            'attributes' => array(
                'value' => '', //set checked to '1'
                'class' => 'e3 available available_class',
                'notInArray' => false,
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'product_category',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'product_category',
                'value' => '1', //set selected to 'blank'
                'class' => 'e1 select-w-320'
            )
        ));
        $this->add(array(
            'name' => 'list_price',
            'attributes' => array(
                'type' => 'text',
                'id' => 'list_price'
            ),
        ));
        $this->add(array(
            'name' => 'cost',
            'attributes' => array(
                'type' => 'text',
                'id' => 'cost'
            ),
        ));
        $this->add(array(
            'name' => 'sell_price',
            'attributes' => array(
                'type' => 'text',
                'id' => 'sell_price'
            ),
        ));
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'membership_discount',
            'attributes' => array(
                'class' => 'address-checkbox e3',
                'id' => 'membership_discount'
            ),
            'options' => array(
                'use_hidden_element' => false,
            ),
        ));
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'featured',
            'attributes' => array(
                'class' => 'e4 address-checkbox',
                'id' => 'featured'
            ),
            'options' => array(
                'use_hidden_element' => false,
            ),
        ));
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'in_stock',
            'attributes' => array(
                'class' => 'e3 in_stock',
                'id' => 'in_stock',
                'onClick' => "javascript:showInStock();",
            ),
        ));
        $this->add(array(
            'name' => 'in_Stock_units',
            'attributes' => array(
                'type' => 'text',
                'id' => 'in_Stock_units',
                'class'=>'width-80'
            ),
        ));
        $this->add(array(
            'name' => 'threshold',
            'attributes' => array(
                'type' => 'text',
                'id' => 'threshold',
                'class'=>'width-80'
            ),
        ));
        
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'in_ellis_stock',
            'attributes' => array(
                'class' => 'e3 in_ellis_stock',
                'id' => 'in_ellis_stock',
                'onClick' => "javascript:showInEllisStock();",
            ),
        ));
        $this->add(array(
            'name' => 'total_in_ellis_stock',
            'attributes' => array(
                'type' => 'text',
                'id' => 'total_in_ellis_stock',
                'class'=>'width-80'
            ),
        ));
        $this->add(array(
            'name' => 'ellis_product_threshold',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ellis_product_threshold',
                'class'=>'width-80'
            ),
        ));        
        
        

        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'is_texable',
            'attributes' => array(
                'class' => 'e3 is_texable',
                'id' => 'is_texable',
            ),
        ));
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'is_refundable',
            'attributes' => array(
                'class' => 'e3 is_refundable',
                'id' => 'is_refundable',
            ),
        ));
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'is_promotional',
            'attributes' => array(
                'class' => 'e3 is_promotional',
                'id' => 'is_promotional',
            ),
        ));
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'is_pickup',
            'attributes' => array(
                'class' => 'e3 is_pickup',
                'id' => 'is_pickup',
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'product_transaction_type',
            'checked_value' => '1',
            'unchecked_value' => '2',
            'attributes' => array(
                'id' => 'product_transaction_type',
                'class' => 'e3 product_transaction_type'
            )
        ));
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'shipping_cost',
            'attributes' => array(
                'class' => 'e3 shipping_cost',
                'id' => 'shipping_cost',
            ),
        ));
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'is_shippable',
            'attributes' => array(
                'class' => 'e3 is_shippable',
                'id' => 'is_shippable',
            ),
        ));        
        $this->add(array(
            'name' => 'gl_code_value',
            'attributes' => array(
                'type' => 'text',
                'id' => 'gl_code_value'
            ),
        ));
        $this->add(array(
            'name' => 'prog_code_value',
            'attributes' => array(
                'type' => 'text',
                'id' => 'prog_code_value'
            ),
        ));
        $this->add(array(
            'name' => 'department',
            'attributes' => array(
                'type' => 'text',
                'id' => 'department'
            ),
        ));

        $this->add(array(
            'name' => 'designation',
            'attributes' => array(
                'type' => 'text',
                'id' => 'designation'
            ),
        ));

        $this->add(array(
            'name' => 'allocation',
            'attributes' => array(
                'type' => 'text',
                'id' => 'allocation'
            ),
        ));

        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'is_restriction',
            'attributes' => array(
                'class' => 'e3 is_restriction',
                'id' => 'is_restriction',
            ),
        ));
        
        $this->add(array(
            'name' => 'weight',
            'attributes' => array(
                'type' => 'text',
                'id' => 'weight'
            ),
        ));
        $this->add(array(
            'name' => 'width',
            'attributes' => array(
                'type' => 'text',
                'id' => 'width'
            ),
        ));
        $this->add(array(
            'name' => 'length',
            'attributes' => array(
                'type' => 'text',
                'id' => 'length'
            ),
        ));
        $this->add(array(
            'name' => 'height',
            'attributes' => array(
                'type' => 'text',
                'id' => 'height'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'unit',
            'options' => array(
                'value_options' => array(
                    '0' => 'Inches',
                    '1' => 'Feet',
                    '2' => 'Centimeters',
                    '3' => 'Millimeters'
                ),
            ),
            'attributes' => array(
                'id' => 'unit',
                'value' => '', //set selected to 'blank'
                'class' => 'e1 select-w-320'
            )
        ));
        $this->add(array(
            'name' => 'product_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'product_name',
                'class'=>'search-icon'
            ),
        ));
        $this->add(array(
            'name' => 'product_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'product_id'
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'select_product',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'select_product',
                'multiple' => 'multiple',
                'class' => 'e1 select-w-320'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'additional_product_1',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'additional_product_1',
                'value' => '', //set selected to 'blank'
                'class' => 'e1 select-w-320'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'additional_product_2',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'additional_product_2',
                'value' => '', //set selected to 'blank'
                'class' => 'e1 select-w-320'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'additional_product_3',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'additional_product_3',
                'value' => '', //set selected to 'blank'
                'class' => 'e1 select-w-320'
            )
        ));


        $this->add(array(
            'name' => 'save',
            'attributes' => array(
                'type' => 'Submit',
                'id' => 'save',
                'class' => 'save-btn',
                'value' => 'Save'
            ),
        ));
        $this->add(array(
            'name' => 'save_continue',
            'attributes' => array(
                'type' => 'Submit',
                'id' => 'save_continue',
                'class' => 'save-btn m-l-10',
                'value' => 'Save & Continue'
            ),
        ));
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'id' => 'cancel',
                'class' => 'cancel-btn  m-l-10',
                'value' => 'Cancel',
                'onClick' => "javascript:window.location.href='/crm-products'",
            ),
        ));

        $this->add(array(
            'name' => 'image_id[]',
            'attributes' => array(
                'type' => 'hidden',
                'id' => ''
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'status',
            'options' => array(
                'value_options' => array(
                    '0' => 'Inactive',
                    '1' => 'Active'
                ),
            ),
            'attributes' => array(
                'id' => 'status',
                'value' => '', //set selected to 'blank'
                'class' => 'e1 select-w-320'
            )
        ));
    }

}