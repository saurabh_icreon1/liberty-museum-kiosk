<?php

/**
 * This is used for select category product form.
 * @package    Category
 * @author     Icreon Tech -DT.
 */

namespace Product\Form;

use Zend\Form\Form;

/**
 * This class  used for delete category form.
 * @package    Category_DeleteCategoryForm
 * @author     Icreon Tech -DT.
 *
 * 
 */
class SelectProductForm extends Form {

    public function __construct($name = null) {
        parent::__construct('select_product_form');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'category_id',
            'attributes' => array(
                'id' => 'category_id',
                'class' => 'select-w-320 e1'
            )
        ));
    }
}