<?php

/**
* This is used for product attribute form.
* @package    Category
* @author     Icreon Tech -AP.
*/ 

namespace Product\Form;
use Zend\Form\Form;
class AddProductAttributesForm extends Form {
    public function __construct($name = null) 
    {
        parent::__construct('attachAttribute');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type'  => 'hidden',
                'id' => 'id',
                'value' => '0'
            ),
        )); 
       $this->add(array(
            'type' => 'MultiCheckbox',
            'name' => 'productAttributes',
            'options' => array(
                'value_options' => array(
                    '1' => 'Web',
                    '2' => 'Ellis Island'
                ),
            ),
            'attributes' => array(
                'value' => '',
                'class' => 'e4 productAttributes',
                'notInArray' => false,
            )
        ));
        $this->add(array(
            'name' => 'save',
            'attributes' => array(
                'type'  => 'Submit',
                'id' => 'save',
                'class' => 'save-btn m-r-10',
                'value' => 'Add Attributes',
				
            ),
        ));
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type'  => 'button',
                'id' => 'cancel',
                'class' => 'cancel-btn',
                'value' => 'Cancel',
                'onclick' => '$("#tab-4").click();'
            ),
        )); 
    }
}