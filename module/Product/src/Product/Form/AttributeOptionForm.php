<?php

/**
 * This is used for product attribute form.
 * @package    Product
 * @author     Icreon Tech - SR.
 */

namespace Product\Form;

use Zend\Form\Form;

class AttributeOptionForm extends Form {

    public function __construct($name = null) {
        parent::__construct('createAttributeOption');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'name'
            ),
        ));
        $this->add(array(
            'name' => 'cost',
            'attributes' => array(
                'type' => 'text',
                'id' => 'label'
            ),
        ));
        $this->add(array(
            'name' => 'price',
            'attributes' => array(
                'type' => 'text',
                'id' => 'helpText'
            ),
        ));
        $this->add(array(
            'name' => 'save',
            'attributes' => array(
                'type' => 'Submit',
                'id' => 'save',
                'class' => 'save-btn',
                'value' => 'save'
            ),
        ));
        $this->add(array(
            'name' => 'update',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'update',
                'class' => 'save-btn',
                'value' => 'Update'
            ),
        ));
        $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'id',
                'value' => '0'
            ),
        ));
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'id' => 'cancel',
                'class' => 'cancel-btn m-l-10',
                'value' => 'Cancel',
                'onclick' => 'window.location.href="/product-attribute-option"'
            ),
        ));
    }

}