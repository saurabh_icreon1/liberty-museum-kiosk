<?php

/**
* This page is used for creating form for create category
* @package    Category
* @author     Icreon Tech -AP.
*/ 

namespace Product\Form;
use Zend\Form\Form;
/**
* This class is used for creating form for create category
* @package    Category_CreateCategoryForm
* @author     Icreon Tech -AP.
*/ 
class CreateCategoryForm extends Form {
    public function __construct($name = null) 
    {
        parent::__construct();
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'category_id',
            'attributes' => array(
                'type'  => 'hidden',
                'id' => 'category_id'
            ),
        )); 
        $this->add(array(
            'name' => 'category_name',
            'attributes' => array(
                'type'  => 'text',
                'id' => 'category_name'
            ),
        )); 
        $this->add(array(
            'name' => 'category_description',
            'attributes' => array(
                'type'  => 'textarea',
                'id' => 'category_description'
            ),
        )); 
       $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'parent_id',
            'attributes' => array(
                'id' => 'parent_id',
                'class' => 'e1 select-w-320'
            )
        ));
        $this->add(array(
            'name' => 'save',
            'attributes' => array(
                'type'  => 'Submit',
                'id' => 'save',
                'class' => 'save-btn',
                'value' => 'Save'
            ),
        )); 
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type'  => 'button',
                'id' => 'cancel',
                'class' => 'cancel-btn m-l-10',
                'value' => 'Cancel',
                'onclick' => 'window.location.href="/crm-product-categories"'
            ),
        )); 
    }
}