<?php

/**
 * This form is used for Save Search Product
 * @package    Product
 * @author     Icreon Tech - AP
 */

namespace Product\Form;

use Zend\Form\Form;

/**
 * This form is used for Save Search Product
 * @author     Icreon Tech - SR 
 */
class SaveSearchProductForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('save-search-product');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'search_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'search_id'
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'saved_search',
            'attributes' => array(
                'id' => 'saved_search',
                'class' => 'e1 select-w-320',
            )
        ));
        $this->add(array(
            'name' => 'search_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'search_name'
            )
        ));
        $this->add(array(
            'name' => 'savebutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'id' => 'savebutton',
                'class' => 'save-btn m-l-5',
            ),
        ));
    }

}
