<?php

/**
 * This Page is used for Product,Attributes And Categories
 * @package    Product
 * @author     Icreon Tech - AP
 */

namespace Product\Controller;

use Base\Controller\BaseController;
use Base\Model\UploadHandler;
use Zend\View\Model\ViewModel;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Zend\Mvc\Controller\Plugin\Params;
use Zend\Http\Request;
use Zend\Authentication\AuthenticationService;
use Product\Form\CreateCategoryForm;
use Product\Form\DeleteCategoryForm;
use Product\Form\SelectProductForm;
use Product\Form\AttributeForm;
use Product\Model\Category;
use Product\Form\ProductForm;
use Product\Form\UploadFileForm;
use Product\Model\Product;
use Product\Model\Attribute;
use Product\Form\SearchProductForm;
use Product\Form\ManageProductForm;
use Product\Form\SaveSearchProductForm;
use Product\Form\AttributeOptionForm;
use Product\Form\AddProductAttributesForm;
use Zend\Session\Container;

/**
 * This controller is used for Product,Attributes And Categories
 * @package    Product
 * @author     Icreon Tech - AP
 */
class ProductController extends BaseController {

    protected $_productTable = null;
    protected $_attributeTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_flashMessage = null;
    public $_categorySelectArr = array();
    protected $_auth = null;
    protected $_categoryTreeArr = array();
    protected $_deleteCategoryIdArr = array();
	protected $_reservationTable = null;

    public function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        parent::__construct();
        $auth = new AuthenticationService();
        $this->_auth = $auth;
    }

    /**
     * This function is used to get table and config file object
     * @param 
     * @return   array 
     */
    public function getProductTable() {
        if (!$this->_productTable) {
            $sm = $this->getServiceLocator();
            $this->_productTable = $sm->get('Product\Model\ProductTable');
            $this->_categoryTable = $sm->get('Category\Model\CategoryTable');
            $this->_translator = $sm->get('translator');
            $this->_config = $sm->get('Config');
            $this->_adapter = $sm->get('dbAdapter');
        }
        return $this->_productTable;
    }
	/**
     * This function is used to get table and config file object
     * @param 
     * @return   array 
     */
    public function getReservationTable() {
        if (!$this->_reservationTable) {
            $sm = $this->getServiceLocator();
            $this->_reservationTable = $sm->get('Kiosk\Model\KioskReservationTable');
            $this->_translator = $sm->get('translator');
            $this->_config = $sm->get('Config');
            $this->_adapter = $sm->get('dbAdapter');
        }
        return $this->_productTable;
    }

    /**
     * This function is used to get table and config file object
     * @param
     * @return   array
     */
    public function getAttributeTable() {
        if (!$this->_attributeTable) {
            $sm = $this->getServiceLocator();
            $this->_attributeTable = $sm->get('Product\Model\AttributeTable');
            $this->_translator = $sm->get('translator');
            $this->_config = $sm->get('Config');
            $this->_adapter = $sm->get('dbAdapter');
        }
        return $this->_attributeTable;
    }

    /**
     * This function is used to create category
     * @param       void
     * @return     json
     * @author     Icreon Tech - DT
     */
    public function addProductCategoryAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getProductTable();
        $category = new Category($this->_adapter);
        $categoryMsg = $this->_config['ProductMessages']['config']['create_category'];
        $categoryForm = new CreateCategoryForm();
        /* Get all Categories code */
        $catArr = array();
        $categoryArr = $category->getCategoriesArr($catArr);
        $allCategories = $this->_categoryTable->getCategories($categoryArr);
        if (!empty($allCategories)) {
            $this->getSelectArr($allCategories);
        }

        $parentCategoryArray = array();
        $parentCategoryArray[0] = '<ROOT>';
        foreach ($allCategories as $key => $val) {
            if ($val['parent_id'] == 0) {
                $parentCategoryArray[$val['category_id']] = $val['category_name'];
            }
        }
        $categoryForm->get('parent_id')->setAttribute('options', $parentCategoryArray);
        //asd($this->_categorySelectArr,2);
        // asd($parentCategoryArray);
        //$categoryForm->get('parent_id')->setAttribute('options', $this->_categorySelectArr);
        /* End Get all Categories code */
        $request = $this->getRequest();
        $response = $this->getResponse();

        if ($request->isPost()) {
            $categoryForm->setInputFilter($category->getInputFilterCategory());

            $categoryForm->setData($request->getPost());
            if (!$categoryForm->isValid()) {
                $errors = $categoryForm->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'continue') {
                        foreach ($row as $type_error => $rower) {
                            if ($type_error != 'notInArray') {
                                $msg [$key] = $categoryMsg[$rower];
                            }
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            $categoryFormArr = $categoryForm->getData();
            /* Check category already exist or not */
            $categoryInfoArr = $category->getCategoryIntoArr($categoryFormArr);
            $getCategoryDetail = $this->_categoryTable->getCategoryInfo($categoryInfoArr);
            if (!empty($getCategoryDetail)) {
                $msg = array();
                $msg ['category_name'] = $categoryMsg['CATEGORY_ALREADY_EXIST'];
                $messages = array('status' => "error", 'message' => $msg);
            }
            /* End check category already exist or not */

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $categoryFormArr['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                $categoryFormArr['added_date'] = DATE_TIME_FORMAT;
                $categoryFormArr['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                $categoryFormArr['modified_date'] = DATE_TIME_FORMAT;
                $catagoryArr = $category->getCreateCategoryArr($categoryFormArr);
                $categoryId = $this->_categoryTable->saveCategory($catagoryArr);
                $this->flashMessenger()->addMessage($categoryMsg['CATEGORY_CREATED_SUCCESS']);
                $messages = array('status' => "success", 'message' => $categoryMsg['CATEGORY_CREATED_SUCCESS']);
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        } else {
            $viewModel = new ViewModel();
            $viewModel->setVariables(array(
                'form' => $categoryForm,
                'jsLangTranslate' => $categoryMsg
            ));
            return $viewModel;
        }
    }

    /**
     * This function is used to get select category structure
     * @param       void
     * @return     json
     * @author     Icreon Tech - DT
     */
    public function getSelectArr($categories, $parentVal = 0, $level = 0) {
        foreach ($categories as $cat) {
            if ($cat['parent_id'] == $parentVal) {
                $this->_categorySelectArr[$cat['category_id']] = str_repeat('-', $level) . $cat['category_name'];
                $this->getSelectArr($categories, $cat['category_id'], $level + 1);
            }
        }
    }

    /**
     * This function is used to get select category structure for edit
     * @param       void
     * @return     json
     * @author     Icreon Tech - DT
     */
    public function getSelectArrForEdit($categories, $excludeParent, $parentVal = 0, $level = 0) {

        foreach ($categories as $cat) {
            if ($cat['parent_id'] == $parentVal && $excludeParent != $cat['parent_id'] && $cat['category_id'] != $excludeParent && $cat['category_id'] != 1) {
                $this->_categorySelectArr[$cat['category_id']] = str_repeat('-', $level) . $cat['category_name'];
                $this->getSelectArrForEdit($categories, $excludeParent, $cat['category_id'], $level + 1);
            }
        }
    }

    /**
     * This function is used to edit category
     * @param       void
     * @return     json
     * @author     Icreon Tech - DT
     */
    public function editProductCategoryAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getProductTable();
        $category = new Category($this->_adapter);
        $categoryMsg = $this->_config['ProductMessages']['config']['create_category'];
        $categoryForm = new CreateCategoryForm();
        $request = $this->getRequest();
        /* Get all Categories code */
        $catArr = array();
        $categoryArr = $category->getCategoriesArr($catArr);
        $allCategories = $this->_categoryTable->getCategories($categoryArr);
        $param = $this->params()->fromRoute();
        $categoryId = (!empty($param['category_id'])) ? $this->decrypt($param['category_id']) : $request->getPost('category_id');

        if (!empty($allCategories)) {
            $this->getSelectArrForEdit($allCategories, $categoryId);
        }
        $parentCategoryArray = array();
        $parentCategoryArray[0] = '<ROOT>';
        foreach ($allCategories as $key => $val) {
            if ($val['parent_id'] == 0) {
                $parentCategoryArray[$val['category_id']] = $val['category_name'];
            }
        }
        $categoryForm->get('parent_id')->setAttribute('options', $parentCategoryArray);
        //$categoryForm->get('parent_id')->setAttribute('options', $this->_categorySelectArr);
        /* End Get all Categories code */
        $response = $this->getResponse();

        if ($request->isPost()) {
            $categoryForm->setInputFilter($category->getInputFilterCategory());
            $categoryForm->setData($request->getPost());
            if (!$categoryForm->isValid()) {
                $errors = $categoryForm->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'continue') {
                        foreach ($row as $type_error => $rower) {
                            if ($type_error != 'notInArray') {
                                $msg [$key] = $categoryMsg[$rower];
                            }
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }

            /* Check category already exist or not */
            $categoryFormArr = $categoryForm->getData();
            $categoryDetailArr = array();
            $categoryDetailArr['not_category_id'] = $categoryFormArr['category_id'];
            $categoryDetailArr['category_name'] = $categoryFormArr['category_name'];
            $categoryInfoArr = $category->getCategoryIntoArr($categoryDetailArr);
            $getCategoryDetail = $this->_categoryTable->getCategoryInfo($categoryInfoArr);
            if (!empty($getCategoryDetail)) {
                $msg = array();
                $msg ['category_name'] = $categoryMsg['CATEGORY_ALREADY_EXIST'];
                $messages = array('status' => "error", 'message' => $msg);
            }

            /* End check category already exist or not */

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $categoryDetailArr = array();
                $categoryDetailArr['category_id'] = $categoryFormArr['category_id'];
                $categoryInfoArr = $category->getCategoryIntoArr($categoryDetailArr);
                $getCategoryDetail = $this->_categoryTable->getCategoryInfo($categoryInfoArr);
                $categoryFormArr['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                $categoryFormArr['modified_date'] = DATE_TIME_FORMAT;
                $categoryArr = $category->getEditCategoryArr($categoryFormArr);
                $categoryId = $this->_categoryTable->editCategory($categoryArr);
                $session = new Container('Category');
                $session->offsetSet('edit_category_id', $categoryId);
                $this->flashMessenger()->addMessage($categoryMsg['CATEGORY_UPDATE_SUCCESS']);
                $messages = array('status' => "success", 'message' => $categoryMsg['CATEGORY_UPDATE_SUCCESS']);
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        } else {
            $categoryForm->get('category_id')->setValue($categoryId);
            $categoryForm->get('save')->setValue('Update');
            $categoryDetailArr = array();
            $categoryDetailArr['category_id'] = $categoryId;
            $categoryInfoArr = $category->getCategoryIntoArr($categoryDetailArr);
            $getCategoryDetail = $this->_categoryTable->getCategoryInfo($categoryInfoArr);
            $this->setValueForElement($getCategoryDetail, $categoryForm);
            $viewModel = new ViewModel();
            $viewModel->setVariables(array(
                'form' => $categoryForm,
                'jsLangTranslate' => $categoryMsg
            ));
            return $viewModel;
        }
    }

    /**
     * This function is used to edit category on drag drop
     * @param       void
     * @return     json
     * @author     Icreon Tech - DT
     */
    public function editProductCategoryDragDropAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getProductTable();
        $category = new Category($this->_adapter);
        $categoryMsg = $this->_config['ProductMessages']['config']['category_list'];
        $request = $this->getRequest();
        $response = $this->getResponse();

        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $categoryId = $this->decrypt($request->getPost('categoryId'));
            $categoryDetailArr = array();
            $categoryDetailArr['category_id'] = $categoryId;
            $categoryInfoArr = $category->getCategoryIntoArr($categoryDetailArr);
            $getCategoryDetail = $this->_categoryTable->getCategoryInfo($categoryInfoArr);
            $categoryArr = array();
            $categoryArr['category_id'] = $getCategoryDetail['category_id'];
            $categoryArr['parent_id'] = $this->decrypt($request->getPost('parentId'));
            $categoryArr['category_name'] = $getCategoryDetail['category_name'];
            $categoryArr['category_description'] = $getCategoryDetail['category_description'];
            $categoryArr['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
            $categoryArr['modified_date'] = DATE_TIME_FORMAT;
            $categoryArr = $category->getEditCategoryArr($categoryArr);
            $categoryId = $this->_categoryTable->editCategory($categoryArr);
            $childCatArr = explode(',', $request->getPost('siblings'));
            $order = 1;
            foreach ($childCatArr as $childCat) {
                $orderArr = array();
                $orderArr[] = $this->decrypt($childCat);
                $orderArr[] = $order;
                $this->_categoryTable->changeOrderCategory($orderArr);
                $order++;
            }
            $messages = array('status' => "success", 'message' => $categoryMsg['CATEGORY_UPDATE_SUCCESS']);
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This function is used to delete category and move product from category to other category
     * @param       void
     * @return     json
     * @author     Icreon Tech - DT
     */
    public function deleteProductCategoryAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getProductTable();
        $category = new Category($this->_adapter);
        $categoryMsg = $this->_config['ProductMessages']['config']['category_list'];
        $request = $this->getRequest();
        $response = $this->getResponse();

        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $categoryId = $this->decrypt($request->getPost('category_id'));
            /* Get all Categories code for delete */
            $catArr = array();
            $categoryArr = $category->getCategoriesArr($catArr);
            $allCategories = $this->_categoryTable->getCategories($categoryArr);
            $catDataArr = $this->getProductCategories($allCategories);
            $this->getCategoriesIdsForDelete($allCategories, $categoryId);
            array_unshift($this->_deleteCategoryIdArr, $categoryId);
            $deleteCategoryIds = implode(',', $this->_deleteCategoryIdArr);
            /* End Get all Categories code for delete */
            $categoryArr = array();
            $categoryArr[] = $deleteCategoryIds;
            $isMove = $request->getPost('is_move');
            $categoryArr[] = ($isMove == 1) ? $request->getPost('new_category_id') : 1;
            $this->_categoryTable->removeCategories($categoryArr);   // need to uncomment for remove categories
            $this->flashMessenger()->addMessage($categoryMsg['CATEGORIES_DELETE_SUCCESS']);
            $message = (count($this->_deleteCategoryIdArr) > 1) ? $categoryMsg['CATEGORIES_DELETE_SUCCESS'] : $categoryMsg['CATEGORY_DELETE_SUCCESS'];
            $messages = array('status' => "success", 'message' => $message);
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This function is used to get categories ids in comma seperated for delete
     * @param   array,Int,array
     * @return  void
     * @author  Icreon Tech - DT
     */
    function getCategoriesIdsForDelete(array $elements, $parentId = 0, $catIds = array()) {
        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $this->_deleteCategoryIdArr[] = $element['category_id'];
                $children = $this->getCategoriesIdsForDelete($elements, $element['category_id']);
            }
        }
    }

    /**
     * This Action is used to set post array values to elements value of form
     * @param $post_arr Array,$form Array
     * @return void
     * @author Icreon Tech -DT
     */
    public function setValueForElement($postArr = array(), $form = array()) {
        if (!is_array($postArr)) {
            $postArr = $postArr->toArray();
        }
        $elements = $form->getElements();
        foreach ($elements as $elementName => $elementObject) {

            if (array_key_exists($elementName, $postArr)) {
                if (is_array($postArr[$elementName])) {
                    $value = implode(',', $postArr[$elementName]);
                } else {

                    $value = $postArr[$elementName];
                }
                $elementObject->setValue($value);
            }
        }
    }

    /**
     * This function is used to list categories
     * @param      void
     * @return     json
     * @author     Icreon Tech - DT
     */
    public function getProductCategoriesAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getProductTable();
        $category = new Category($this->_adapter);
        $categoryMsg = $this->_config['ProductMessages']['config']['category_list'];
        $deleteCategoryForm = new DeleteCategoryForm();

        /* Get all Categories code for select box */
        $catArr = array();
        $categoryArr = $category->getCategoriesArr($catArr);
        $allCategories = $this->_categoryTable->getCategories($categoryArr);
        if (!empty($allCategories)) {
            $this->getSelectArr($allCategories);
        }
        $deleteCategoryForm->get('new_category_id')->setAttribute('options', $this->_categorySelectArr);
        /* End Get all Categories code for select box */

        /* Get all Categories code for */
        $catArr = array();
        $categoryArr = $category->getCategoriesArr($catArr);
        $allCategories = $this->_categoryTable->getCategories($categoryArr);
        $categoryData = array();
        if (!empty($allCategories)) {
            foreach ($allCategories as $category) {
                $catData = array();
                $catData['label'] = $category['category_name'];
                $catData['id'] = $this->encrypt($category['category_id']);
                $catData['parent_id'] = $this->encrypt($category['parent_id']);
                $catData['is_deletable'] = $category['is_deletable'];
                $categoryData[] = $catData;
            }
        }

        $categoriesArr = $this->buildTreeArray($categoryData);
        $categoriesJsonStr = json_encode($categoriesArr);
        /* End Get all Categories code for buildTree */
        $request = $this->getRequest();
        $response = $this->getResponse();
        $viewModel = new ViewModel();
        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        $editCategoryId = '';
        $session = new Container('Category');
        if ($session->offsetExists('edit_category_id')) {
            $editCategoryId = $this->encrypt($session->offsetGet('edit_category_id'));
            $session->offsetUnset('edit_category_id');
        }
        $viewModel->setVariables(array(
            'jsLangTranslate' => $categoryMsg,
            'messages' => $messages,
            'categoriesJsonStr' => $categoriesJsonStr,
            'deleteCategoryForm' => $deleteCategoryForm,
            'editCategoryId' => $editCategoryId
        ));
        return $viewModel;
    }

    /**
     * This function is used to get product categories
     * @param   array
     * @return  array
     * @author  Icreon Tech - DT
     */
    public function getProductCategories($allCatArr = array()) {
        $categoryArr = array();
        $i = 0;
        if (!empty($allCatArr)) {
            foreach ($allCatArr as $category) {
                $categoryArr['category_id'][$i] = $category['category_id'];
                $categoryArr['parent_id'][$i] = $category['parent_id'];
                $categoryArr['category_name'][$i] = $category['category_name'];
                $i++;
            }
        }
        $categoryArr['count'] = $i;
        return $categoryArr;
    }

    /**
     * This function is used to get categories build tree array
     * @param   array
     * @return  array
     * @author  Icreon Tech - DT
     */
    function buildTreeArray(array $elements, $parentId = 'czoxOiIwIjs=') {
        $branch = array();

        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $children = $this->buildTreeArray($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                unset($element['parent_id']);
                $branch[] = $element;
            }
        }
        return $branch;
    }

    /**
     * This function is used to create attribute
     * @param array $form
     * @return     array 
     * @author     Icreon Tech - AS
     */
    public function addProductAttributeAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getAttributeTable();
        $attributeObj = new Attribute($this->_adapter);
        $attributeMsg = $this->_config['ProductMessages']['config']['createAttribute'];
        $attributeForm = new AttributeForm();
        $displayList = array();
        $displayList[0] = "Select";
        $displayList[1] = "Text Field";
        $displayList[2] = "Select Box";
        $displayList[3] = "Radio Buttons";
        $displayList[4] = "Checkbox";
        $attributeForm->get('displayType')->setAttribute('options', $displayList);
        $response = $this->getResponse();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $attributeForm->setInputFilter($attributeObj->getInputFilterAddAttribute());
            $attributeForm->setData($request->getPost());
            if (!$attributeForm->isValid()) {
                $errors = $attributeForm->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            $msg [$key] = $attributeMsg[$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $attributeObj->exchangeArray($attributeForm->getData());
                $postData = array();
                $postData['name'] = $attributeObj->name;
                $postData['label'] = $attributeObj->label;
                $postData['helpText'] = $attributeObj->helpText;
                $postData['makeRequired'] = $attributeObj->makeRequired;
                $postData['displayType'] = $attributeObj->displayType;
                $postData['addedDate'] = DATE_TIME_FORMAT;
                $postData['addedBy'] = $this->_auth->getIdentity()->crm_user_id;
                $returnData = $this->getAttributeTable()->insertProductAttribute($postData);
                if ($returnData) {
                    $this->flashMessenger()->addMessage($attributeMsg['ATTRIBUTE_ADDED']);
                    $messages = array('status' => "success", 'id' => $this->encrypt($returnData[0]['lastId']));
                } else {
                    $this->flashMessenger()->addMessage($attributeMsg['ERROR_ATTRIBUTE_MSG']);
                    $messages = array('status' => "error");
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'form' => $attributeForm,
            'jsLangTranslate' => $attributeMsg
        ));
        return $viewModel;
    }

    /**
     * This function is used to create product
     * @param      array $form
     * @return     array 
     * @author     Icreon Tech - AP
     */
    public function addCrmProductAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getProductTable();
        $productForm = new ProductForm();
        $uploadForm = new UploadFileForm();
        $productObj = new Product($this->_adapter);
        $category = new Category($this->_adapter);
        $productResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductsType();
        $productUnitList = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductsUnit();
        $postArr = array();
        $postArr['product_name'] = '';
        $postArr['product_type'] = '6';/**  Wall of Honor */
        $postArr['product_id'] = '';
        $wallOfHonorProducts = array();
        $wallOfHonorProducts = $this->getProductTable()->getCrmProducts($postArr);
        /** category section */
        $catArr = array();
        $categoryForm = new CreateCategoryForm();
        $category = new Category($this->_adapter);
        $categoryArr = $category->getCategoriesArr($catArr);
        $allCategories = $this->_categoryTable->getCategories($categoryArr);
        if (!empty($allCategories)) {
            $this->getSelectArr($allCategories);
        }

        $wallOfHonorProductsArray = array();
        $wallOfHonorProductsArray[''] = 'Select';
        if (!empty($wallOfHonorProducts)) {
            foreach ($wallOfHonorProducts as $val) {
                $wallOfHonorProductsArray[$val['product_id']] = $val['product_name'];
            }
        }
        $productForm->get('product_category')->setAttribute('options', $this->_categorySelectArr);
        $productForm->get('additional_product_1')->setAttribute('options', $wallOfHonorProductsArray);
        $productForm->get('additional_product_2')->setAttribute('options', $wallOfHonorProductsArray);
        $productForm->get('additional_product_3')->setAttribute('options', $wallOfHonorProductsArray);
        /** category section */
        $productArray = array("" => "Select");
        $unitArr = array("" => "Select");
        foreach ($productResult as $key => $val) {
            $productArray[$val['product_type_id']] = $val['product_type'];
        }
        foreach ($productUnitList as $k => $val) {
            $unitArr[$val['product_unit']] = $val['product_unit'];
        }
        $productMsg = $this->_config['ProductMessages']['config']['createproduct'];

        $request = $this->getRequest();
        $response = $this->getResponse();
        $catArr = array();
        $productForm->add($uploadForm);
        $productForm->get('product_type')->setAttribute('options', $productArray);
        $productForm->get('unit')->setAttribute('options', $unitArr);
        if ($request->isPost()) {
            $productForm->setInputFilter($productObj->getInputFilterAddProduct());
            $productForm->setData($request->getPost());

            if (!$productForm->isValid()) {
                $errors = $productForm->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit' && $key != 'is_pickup') {
                        foreach ($row as $rower) {
                            $msg [$key] = $productMsg[$rower];
                        }
                    }
                }
                if (!empty($msg))
                    $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $productObj->exchangeArray($productForm->getData());
                $postData = array();
                $postData['productType'] = $productObj->productType;
                $postData['name'] = $productObj->name;
                $postData['sku'] = $productObj->sku;
                $postData['description'] = $productObj->description;
                $postData['available'] = implode(',', $productObj->available);
                $postData['productCategory'] = $productObj->productCategory;
                $postData['listPrice'] = $productObj->listPrice;
                $postData['cost'] = $productObj->cost;
                $postData['sellPrice'] = $productObj->sellPrice;
                $postData['featured'] = $productObj->featured;
                $postData['membershipDiscount'] = $productObj->membershipDiscount;
                $postData['inStock'] = $productObj->inStock;
                $postData['inStockUnits'] = $productObj->inStockUnits;
                $postData['isTexable'] = $productObj->isTexable;
                $postData['isRefundable'] = $productObj->isRefundable;
                $postData['isPromotional'] = $productObj->isPromotional;
                $postData['isPickup'] = $productObj->isPickup;
                $postData['weight'] = $productObj->weight;
                $postData['length'] = $productObj->length;
                $postData['width'] = $productObj->width;
                $postData['height'] = $productObj->height;
                $postData['unit'] = $productObj->unit;
                $postData['threshold'] = $productObj->threshold;
                $postData['addedBy'] = $this->_auth->getIdentity()->crm_user_id;
                $postData['addedDate'] = DATE_TIME_FORMAT;
                $postData['modifiedBy'] = $this->_auth->getIdentity()->crm_user_id;
                $postData['modifiedDate'] = DATE_TIME_FORMAT;
                $postData['additional_product_1'] = $productObj->additional_product_1;
                $postData['additional_product_2'] = $productObj->additional_product_2;
                $postData['additional_product_3'] = $productObj->additional_product_3;
                $postData['shipping_description'] = $productObj->shipping_description;
                $postData['product_transaction_type'] = $productObj->product_transaction_type;
                $postData['gl_code_value'] = $productObj->gl_code_value;
                $postData['prog_code_value'] = $productObj->prog_code_value;
                $postData['department'] = $productObj->department;
                $postData['designation'] = $productObj->designation;
                $postData['allocation'] = $productObj->allocation;
                $postData['is_restriction'] = $productObj->is_restriction;
                $postData['in_ellis_stock'] = $productObj->in_ellis_stock;
                $postData['total_in_ellis_stock'] = $productObj->total_in_ellis_stock;
                $postData['ellis_product_threshold'] = $productObj->ellis_product_threshold;

                // if (empty($productObj->isPickup)) {
                $postData['is_shippable'] = $productObj->is_shippable;
                $postData['shipping_cost'] = $productObj->shipping_cost;
                //} else {
                //    $postData['is_shippable'] = '';
                //    $postData['shipping_cost'] = '';
                //}
                $retArr = $this->getProductTable()->insertProduct($postData);
                $productId = $retArr[0]['LAST_INSERT_ID'];
                $formData = $request->getPost();
                if ($productId) {
                    $postImageData = array();
                    if ($request->getPost('file_name_value')) {
                        foreach ($request->getPost('file_name_value') as $key => $val) {
                            if ($val != '') {
                                if ($key == 0)
                                    $postImageData['isDefault'] = '1';
                                else
                                    $postImageData['isDefault'] = '0';

                                $postImageData['productId'] = $productId;
                                $postImageData['imageName'] = $val;
                                $postImageData['addedDate'] = DATE_TIME_FORMAT;
                                $postImageData['modifiedDate'] = DATE_TIME_FORMAT;

                                $this->getProductTable()->insertProductImage($postImageData);

                                $this->moveFileFromTempToUser($val);
                            }
                        }
                    }

                    $postReletedData = array();
                    if ($request->getPost('related_product_id') != '') {
                        $relatedProductIdArray = $request->getPost('related_product_id');
                        foreach ($relatedProductIdArray as $key => $val) {
                            $postReletedData['productId'] = $productId;
                            $postReletedData['relatedProductId'] = $val;
                            $postReletedData['addedDate'] = DATE_TIME_FORMAT;
                            $postReletedData['modifiedDate'] = DATE_TIME_FORMAT;

                            $this->getProductTable()->insertRelatedProduct($postReletedData);
                        }
                    }
                    /** insert into the change log */
                    $changeLogArray = array();
                    $changeLogArray['table_name'] = 'tbl_pro_change_logs';
                    $changeLogArray['activity'] = '1';/** 1== for insert */
                    $changeLogArray['id'] = $productId;
                    $changeLogArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                    $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                    $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                    /** end insert into the change log */
                }
                if ($productId) {
                    $this->flashMessenger()->addMessage($productMsg['CREATE_PRODUCT_SUCCESS']);
                    $messages = array('status' => "success");
                } else {
                    $this->flashMessenger()->addMessage($productMsg['CREATE_PRODUCT_FAIL']);
                    $messages = array('status' => "error");
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'form' => $productForm,
            'jsLangTranslate' => $productMsg
        ));
        return $viewModel;
    }

    /**
     * This function is used to get product info
     * @param      array
     * @return     array 
     * @author     Icreon Tech - AP
     */
    public function getCrmProductInfoAction() {
        $this->checkUserAuthentication();
        //$this->layout('crm');
        $this->getProductTable();
        $productMsg = array_merge($this->_config['file_upload_path'], $this->_config['ProductMessages']['config']['createproduct']);

        $param = $this->params()->fromRoute();
        $searchParam['id'] = $this->decrypt($param['id']);
        $result = $this->getProductTable()->getProductInfo($searchParam);

        $proResult = $result['0'];
        $wallOfHonorAddProducts = array();
        $wallOfHonorAdditionalProductsArray = array();
        if (!empty($proResult['additional_product_1']) || !empty($proResult['additional_product_2']) || !empty($proResult['additional_product_3'])) {
            $postArr['product_name'] = '';
            $postArr['product_type'] = '6';
            $productIdArray[] = $proResult['additional_product_1'];
            $productIdArray[] = $proResult['additional_product_2'];
            $productIdArray[] = $proResult['additional_product_3'];
            $postArr['product_id'] = implode(',', $productIdArray);

            $wallOfHonorAddProducts = $this->getProductTable()->getCrmProducts($postArr);
            foreach ($wallOfHonorAddProducts as $val)
                $wallOfHonorAdditionalProductsArray[$val['product_id']] = stripslashes($val['product_name']);
        }

        $productResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductsType();
        foreach ($productResult as $key => $val) {
            if ($val['product_type_id'] == $proResult['product_type_id']) {
                $proResult['product_type'] = $val['product_type'];
            }
        }

        $proResult['is_available'] = explode(',', $proResult['is_available']);
        $proResult['product_image'] = explode(',', $proResult['product_image']);
        $relatedProd = array();
        $relatedProd['id'] = $searchParam['id'];
        $relatedProd['is_admin'] = 1; // for crm admin user
        $relatedProductArray = $this->getProductTable()->getRelatedProduct($relatedProd);

        foreach ($proResult['is_available'] as $key => $val) {
            if ($val == '1') {
                $proResult['is_available'][$key] = 'Web';
            } else {
                $proResult['is_available'][$key] = 'Ellis Island';
            }
        }

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'param' => $proResult,
            'jsLangTranslate' => $productMsg,
            'image_path' => $productMsg['assets_url'],
            'wallOfHonorAdditionalProductsArray' => $wallOfHonorAdditionalProductsArray,
            'relatedProductArray' => $relatedProductArray
        ));
        return $viewModel;
    }

    /**
     * This function is used to get product info
     * @param      array
     * @return     array
     * @author     Icreon Tech - AP
     */
    public function editCrmProductAction() {
        $this->checkUserAuthentication();
        $this->getProductTable();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $param = $this->params()->fromRoute();
        $productMsg = array_merge($this->_config['file_upload_path'], $this->_config['ProductMessages']['config']['createproduct']);
        $in_stock = '';
        $productForm = new ProductForm();
        $productForm->setAttribute('id', 'editProduct');
        $productForm->setAttribute('name', 'editProduct');
        $uploadForm = new UploadFileForm();
        $productObj = new Product($this->_adapter);
        $category = new Category($this->_adapter);
        $productForm->add($uploadForm);
        $postArr = array();
        $postArr['product_name'] = '';
        $postArr['product_type'] = '6';/**  Wall of Honor */
        $postArr['product_id'] = '';
        $wallOfHonorProducts = array();
        $wallOfHonorProducts = $this->getProductTable()->getCrmProducts($postArr);
        $wallOfHonorProductsArray = array();
        $wallOfHonorProductsArray[''] = 'Select';
        if (!empty($wallOfHonorProducts)) {
            foreach ($wallOfHonorProducts as $val) {
                $wallOfHonorProductsArray[$val['product_id']] = $val['product_name'];
            }
        }
        $catArr = array();
        $categoryForm = new CreateCategoryForm();
        $category = new Category($this->_adapter);
        $categoryArr = $category->getCategoriesArr($catArr);
        $allCategories = $this->_categoryTable->getCategories($categoryArr);
        if (!empty($allCategories)) {
            $this->getSelectArr($allCategories);
        }
        $productForm->get('product_category')->setAttribute('options', $this->_categorySelectArr);
        $productForm->get('additional_product_1')->setAttribute('options', $wallOfHonorProductsArray);
        $productForm->get('additional_product_2')->setAttribute('options', $wallOfHonorProductsArray);
        $productForm->get('additional_product_3')->setAttribute('options', $wallOfHonorProductsArray);
        $productList = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductsType();
        $productTypeResult = array();
        $messages = array();
        foreach ($productList as $key => $val) {
            $productTypeResult[$val['product_type_id']] = $val['product_type'];
        }
        $catArr = array();
        $categoryArr = $category->getCategoriesArr($catArr);
        $allCategories = $this->_categoryTable->getCategories($categoryArr);
        $categoriesList[''] = 'Select Category';
        if ($allCategories) {
            foreach ($allCategories as $key => $val) {
                $categoriesList[$val['category_id']] = $val['category_name'];
            }
        }
        $productUnitList = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductsUnit();
        $unitArr = array();
        foreach ($productUnitList as $k => $val) {
            $unitArr[$val['product_unit']] = $val['product_unit'];
        }
        $productForm->get('product_type')->setAttribute('options', $productTypeResult);
        //  $productForm->get('product_category')->setAttribute('options', $categoriesList);
        $productForm->get('unit')->setAttribute('options', $unitArr);
        if ($request->isPost()) {
            $productForm->setInputFilter($productObj->getInputFilterAddProduct());
            $productForm->setData($request->getPost());
            if (!$productForm->isValid()) {
                $errors = $productForm->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'save' && $key != 'is_pickup') {
                        foreach ($row as $rower) {
                            echo $msg [$key] = $productMsg[$rower];
                        }
                    }
                }
                if (!empty($msg))
                    $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {

                $productObj->exchangeArray($productForm->getData());
                $postData = array();
                $postData['productId'] = $productObj->productId;
                $postData['productType'] = $productObj->productType;
                $postData['name'] = $productObj->name;
                $postData['sku'] = $productObj->sku;
                $postData['description'] = $productObj->description;
                $postData['available'] = implode(',', $productObj->available);
                $postData['productCategory'] = $productObj->productCategory;
                $postData['listPrice'] = $productObj->listPrice;
                $postData['cost'] = $productObj->cost;
                $postData['sellPrice'] = $productObj->sellPrice;
                $postData['featured'] = $productObj->featured;
                $postData['membershipDiscount'] = $productObj->membershipDiscount;
                $postData['inStock'] = $productObj->inStock;
                if ($productObj->inStock == 1) {
                    $postData['inStockUnits'] = $productObj->inStockUnits;
                    $postData['threshold'] = $productObj->threshold;
                } else {
                    $postData['inStockUnits'] = '';
                    $postData['threshold'] = '';
                }

                $postData['isTexable'] = $productObj->isTexable;
                $postData['isRefundable'] = $productObj->isRefundable;
                $postData['isPromotional'] = $productObj->isPromotional;
                $postData['weight'] = $productObj->weight;
                $postData['length'] = $productObj->length;
                $postData['width'] = $productObj->width;
                $postData['height'] = $productObj->height;
                $postData['unit'] = $productObj->unit;
                $postData['modifiedBy'] = $this->_auth->getIdentity()->crm_user_id;
                $postData['modifiedDate'] = DATE_TIME_FORMAT;
                $postData['additional_product_1'] = $productObj->additional_product_1;
                $postData['additional_product_2'] = $productObj->additional_product_2;
                $postData['additional_product_3'] = $productObj->additional_product_3;
                $postData['isStatus'] = $productObj->status;
                $postData['isPickup'] = $productObj->isPickup;
                $postData['shipping_description'] = $productObj->shipping_description;
                $postData['product_transaction_type'] = $productObj->product_transaction_type;
                $postData['gl_code_value'] = $productObj->gl_code_value;
                $postData['prog_code_value'] = $productObj->prog_code_value;
                $postData['department'] = $productObj->department;
                $postData['designation'] = $productObj->designation;
                $postData['allocation'] = $productObj->allocation;
                $postData['is_restriction'] = $productObj->is_restriction;
                $postData['in_ellis_stock'] = $productObj->in_ellis_stock;
                // if (empty($productObj->isPickup)) {
                $postData['is_shippable'] = $productObj->is_shippable;
                $postData['shipping_cost'] = $productObj->shipping_cost;
                //} else {
                //    $postData['is_shippable'] = '';
                //    $postData['shipping_cost'] = '';
                // }

                if ($productObj->in_ellis_stock != 1) {
                    $postData['total_in_ellis_stock'] = '';
                    $postData['ellis_product_threshold'] = '';
                } else {
                    $postData['total_in_ellis_stock'] = $productObj->total_in_ellis_stock;
                    $postData['ellis_product_threshold'] = $productObj->ellis_product_threshold;
                }
                if ($postData['productType'] != 1) {
                    $postData['inStock'] = '';
                    $postData['inStockUnits'] = '';
                    $postData['threshold'] = '';
                    // $postData['weight'] = '';
                    $postData['length'] = '';
                    $postData['width'] = '';
                    $postData['height'] = '';
                    $postData['unit'] = '';
                    $postData['featured'] = '';
                }
                $postImageData = array();
                if ($request->getPost('image_id') != '')
                    $postImageData['product_image_id'] = implode(',', $request->getPost('image_id'));

                $postData['defaultImageId'] = $request->getPost('default_image');
                $retArr = $this->getProductTable()->updateProduct($postData);
                $postImageData['product_id'] = $postData['productId'];
                $this->getProductTable()->deleteProductImages($postImageData);
                $formData = $request->getPost();
                if ($retArr) {
                    $postReletedData = array();
                    if ($request->getPost('related_product_id') != '') {
                        $relatedProductIdArray = $request->getPost('related_product_id');
                        foreach ($relatedProductIdArray as $key => $val) {
                            $postReletedData['productId'] = $postData['productId'];
                            $postReletedData['relatedProductId'] = $val;
                            $postReletedData['addedDate'] = DATE_TIME_FORMAT;
                            $postReletedData['modifiedDate'] = DATE_TIME_FORMAT;
                            $this->getProductTable()->insertRelatedProduct($postReletedData);
                        }
                    }
                    $postImageData = array();
                    $imgName = array();
                    $imgName = $request->getPost('file_name_value');
                    //asd($imgName);
                    if ($imgName[0] != '') {
                        foreach ($imgName as $key => $val) {
                            if (empty($postData['defaultImageId'])) {
                                if (count($imgName) == 1) {
                                    $postImageData['isDefault'] = '1';
                                } else {
                                    if ($key == 0)
                                        $postImageData['isDefault'] = '1';
                                    else
                                        $postImageData['isDefault'] = '0';
                                }
                            }
                            //      asd($postImageData);
                            $postImageData['productId'] = $postData['productId'];
                            $postImageData['imageName'] = $val;
                            $postImageData['addedDate'] = DATE_TIME_FORMAT;
                            $postImageData['modifiedDate'] = DATE_TIME_FORMAT;
                            //$postImageData['isDefault'] = '0';
                            $this->getProductTable()->insertProductImage($postImageData);
                            $this->moveFileFromTempToUser($val);
                        }
                    }
                }
                if ($retArr) {
                    $this->flashMessenger()->addMessage($productMsg['UPDATE_PRODUCT_SUCCESS']);
                    $actPage = 'save';
                    if ($request->getPost('save_continue') != '') {
                        $actPage = 'save_continue';
                        $mode = $this->encrypt('edit');
                    }
                    if ($request->getPost('save') != '') {
                        $actPage = 'save';
                    }
                    $messages = array('status' => "success", "actPage" => $actPage);
                    
                    /** insert into the change log */
                    $changeLogArray = array();
                    $changeLogArray['table_name'] = 'tbl_pro_change_logs';
                    $changeLogArray['activity'] = '2';/** 2 == for update */
                    $changeLogArray['id'] = $postData['productId'];
                    $changeLogArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                    $changeLogArray['added_date'] = DATE_TIME_FORMAT;

                    $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                   
                    /** end insert into the change log */
                } else {
                    $this->flashMessenger()->addMessage($productMsg['CREATE_PRODUCT_FAIL']);
                    $messages = array('status' => "error");
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $searchParam['id'] = $this->decrypt($param['id']);
        $result = $this->getProductTable()->getProductInfo($searchParam);
        $relatedProductResult = $this->getProductTable()->getRelatedProduct($searchParam);
        $productResult = $result['0'];
        if ($productResult['product_image_ids']) {
            $imageIdResult = !empty($productResult['product_image_ids']) ? explode(',', $productResult['product_image_ids']) : array();
            $imageIdResult = array_unique($imageIdResult);
            $imageResult = explode(',', $productResult['product_image']);
            $imageResult = array_unique($imageResult);
            $imageDefaultResult = !empty($productResult['default_image']) ? explode(',', $productResult['default_image']) : array();
            $imageDefaultResult = array_unique($imageDefaultResult);
            foreach ($imageIdResult as $k => $val) {
                $arrImage['values'][] = array(
                    'id' => $val,
                    'imageName' => @$imageResult[$k],
                    'isDefault' => !empty($imageDefaultResult) ? @$imageDefaultResult[$k] : ''
                );
                if (!empty($imageDefaultResult) && @$imageDefaultResult[$k] == 1)
                    $arrImage['isDefault'] = $val;
            }
            $imageArray = array();
            foreach ($arrImage['values'] as $key => $value) {
                $imgId = trim($value['id']);
                $productForm->add(array(
                    'type' => 'Zend\Form\Element\Radio',
                    'name' => 'default_image',
                    'options' => array(
                        'value_options' => array(
                        //'' => 'is Default'
                        ),
                    ),
                    'attributes' => array(
                        'class' => 'e3',
                        'tag' => 'div'
                    )
                ));
                $productForm->add(array(
                    'name' => 'removeImage' . $imgId,
                    'attributes' => array(
                        'onClick' => '$("#image_div_' . $imgId . '").remove();',
                        'class' => 'cancel-btn right m-r-100',
                        'value' => 'Remove',
                        'type' => 'button'
                    )
                ));
            }
        }
        $availableResult = explode(',', $productResult['is_available']);
        foreach ($availableResult as $k => $val) {
            if ($val == '1') {
                $available[$k] = 'Web';
            } else {
                $available[$k] = 'Kiosk';
            }
        }

        $productForm->get('product_type')->setAttribute('value', $productResult['product_type_id']);
        $productForm->get('product_category')->setAttribute('value', $productResult['category_id']);
        $productForm->get('name')->setValue(stripslashes($productResult['product_name']));
        $productForm->get('description')->setValue(stripslashes($productResult['product_description']));
        $productForm->get('sku')->setValue($productResult['product_sku']);
        //$productForm->get('available')->setValue($productResult['is_available']);
        $productForm->get('available')->setAttribute('value', explode(',', $productResult['is_available']));
        $productForm->get('product_category')->setAttribute('value', $productResult['category_id']);
        $productForm->get('list_price')->setAttribute('value', $productResult['product_list_price']);
        $productForm->get('cost')->setAttribute('value', $productResult['product_cost']);
        $productForm->get('sell_price')->setAttribute('value', $productResult['product_sell_price']);
        $productForm->get('membership_discount')->setAttribute('value', $productResult['is_membership_discount']);
        $productForm->get('featured')->setAttribute('value', $productResult['is_featured']);
        $productForm->get('in_stock')->setAttribute('value', $productResult['in_stock']);
        $productForm->get('in_Stock_units')->setAttribute('value', $productResult['total_in_stock']);
        $productForm->get('threshold')->setAttribute('value', $productResult['product_threshold']);
        $productForm->get('is_texable')->setAttribute('value', $productResult['is_taxable']);
        $productForm->get('is_refundable')->setAttribute('value', $productResult['is_refundable']);
        $productForm->get('is_promotional')->setAttribute('value', $productResult['is_promotional']);
        $productForm->get('weight')->setAttribute('value', $productResult['product_weight']);
        $productForm->get('length')->setAttribute('value', $productResult['product_length']);
        $productForm->get('width')->setAttribute('value', $productResult['product_width']);
        $productForm->get('height')->setAttribute('value', $productResult['product_height']);
        $productForm->get('unit')->setAttribute('value', $productResult['product_unit']);
        $productForm->get('product_id')->setAttribute('value', $productResult['product_id']);
        $productForm->get('status')->setAttribute('value', $productResult['is_status']);
        $productForm->get('additional_product_1')->setAttribute('value', $productResult['additional_product_1']);
        $productForm->get('additional_product_2')->setAttribute('value', $productResult['additional_product_2']);
        $productForm->get('additional_product_3')->setAttribute('value', $productResult['additional_product_3']);
        $productForm->get('is_pickup')->setAttribute('value', $productResult['is_pickup']);
        $productForm->get('shipping_description')->setAttribute('value', stripslashes($productResult['shipping_description']));
        $productForm->get('is_shippable')->setAttribute('value', stripslashes($productResult['is_shippable']));
        $productForm->get('shipping_cost')->setAttribute('value', stripslashes($productResult['shipping_cost']));
        $productForm->get('product_transaction_type')->setAttribute('value', stripslashes($productResult['product_transaction_type']));
        $productForm->get('gl_code_value')->setAttribute('value', stripslashes($productResult['gl_code_value']));
        $productForm->get('prog_code_value')->setAttribute('value', stripslashes($productResult['prog_code_value']));
        $productForm->get('department')->setAttribute('value', stripslashes($productResult['department']));
        $productForm->get('designation')->setAttribute('value', stripslashes($productResult['designation']));
        $productForm->get('allocation')->setAttribute('value', stripslashes($productResult['allocation']));
        $productForm->get('is_restriction')->setAttribute('value', stripslashes($productResult['is_restriction']));

        $productForm->get('in_ellis_stock')->setAttribute('value', stripslashes($productResult['in_ellis_stock']));
        $productForm->get('total_in_ellis_stock')->setAttribute('value', stripslashes($productResult['total_in_ellis_stock']));
        $productForm->get('ellis_product_threshold')->setAttribute('value', stripslashes($productResult['ellis_product_threshold']));

        //  $productForm->get('encrypt_product_id')->setAttribute('value', $this->encrypt($productResult['product_id']));
        $in_stock = $productResult['in_stock'];
        $in_ellis_stock = $productResult['in_ellis_stock'];
        
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => $productMsg,
            'relatedProductResult' => $relatedProductResult,
            'form' => $productForm,
            'image_path' => $productMsg['assets_url'] . '/product/thumbnail/',
            'imageArray' => !empty($arrImage) ? $arrImage : '',
            'relatedProductId' => !empty($productResult['related_product_id']) ? explode(',', $productResult['related_product_id']) : '',
            'relatedProductName' => !empty($productResult['related_product_name']) ? explode(',', $productResult['related_product_name']) : '',
            'in_stock' => $in_stock,
            'in_ellis_stock' => $in_ellis_stock
        ));
        return $viewModel;
    }

    /**
     * This Action is used to file upload form elements .
     * @author Icreon Tech-AP
     * @return upload file form
     */
    public function productUploadAction() {
        $this->checkUserAuthentication();
        $uploadForm = new UploadFileForm();
        $this->getProductTable();
        $param = $this->params()->fromRoute();
        $data['counter'] = $param['counter'];
        $data['container'] = $param['container'];
        $fieldName = 'file_name' . $data['counter'];
        $fieldFileName = 'file_name_value' . $data['counter'];
        $productMsg = $this->_config['ProductMessages']['config']['createproduct'];
        //$uploadForm->get('file_name_count[]')->setValue($data['counter']);
        $uploadForm->get('file_name0')->setName($fieldName);
        $uploadForm->get('file_name0')->setAttribute('id', $fieldName);
        $uploadForm->get('file_name_value[]')->setAttribute('id', $fieldFileName);
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'uploadForm' => $uploadForm,
            'jsLangTranslate' => $productMsg,
            'param' => $data,
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used for search products
     * @return this will the manifest search form values
     * @author Icreon Tech -SR
     */
    public function getCrmProductsAction() {

        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getProductTable();
        $productMsg = array_merge($this->_config['ProductMessages']['config']['common'], $this->_config['ProductMessages']['config']['search_product']);
        $request = $this->getRequest();

        $searchProductForm = new SearchProductForm();
        $saveSearchProductForm = new SaveSearchProductForm();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $response = $this->getResponse();

        /** category section */
        $catArr = array();
        $categoryForm = new CreateCategoryForm();
        $category = new Category($this->_adapter);
        $categoryArr = $category->getCategoriesArr($catArr);
        $allCategories = $this->_categoryTable->getCategories($categoryArr);
        if (!empty($allCategories)) {
            $this->getSelectArr($allCategories);
        }
        $searchProductForm->get('productCategory')->setAttribute('options', $this->_categorySelectArr);
        /** category section */
        $statusArray = $this->statusArray();
        $searchProductForm->get('status')->setAttribute('options', $statusArray);
        $availableArray[''] = 'Any';
        $availableArray['1'] = 'Web';
        $availableArray['2'] = 'Ellis Island';
        $searchProductForm->get('available')->setAttribute('options', $availableArray);


        $productResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductsType();
        $productArray = array("" => "Select");
        foreach ($productResult as $key => $val) {
            $productArray[$val['product_type_id']] = $val['product_type'];
        }
        $searchProductForm->get('productType')->setAttribute('options', $productArray);

        $getSearchArray = array();
        $getSearchArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
        $getSearchArray['isActive'] = '1';
        $getSearchArray['searchId'] = '';
        $crmSearchArray = $this->_productTable->getProductSavedSearch($getSearchArray);
        $searchList = array();
        $searchList[''] = 'Select';
        foreach ($crmSearchArray as $key => $val) {
            $searchList[$val['product_search_id']] = stripslashes($val['title']);
        }
        $saveSearchProductForm->get('saved_search')->setAttribute('options', $searchList);
        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        $viewModel->setVariables(array(
            'searchProductForm' => $searchProductForm,
            'saveSearchProductForm' => $saveSearchProductForm,
            'mode' => '',
            'messages' => $messages,
            'jsLangTranslate' => $productMsg
        ));
        return $viewModel;
    }

    /**
     * This action is used to upload file for product
     * @return json
     * @param void
     * @author Icreon Tech - AP
     */
    public function uploadProductFilesAction() {
        $this->checkUserAuthentication();
        $fileInputNames = array_keys($_FILES);

        $fileInputName = $fileInputNames[0];
        $fileExt = $this->GetFileExt($_FILES[$fileInputName]['name']);
        $filename = $this->GenerateMediaFileName($fileExt[1]);    //Generate file name
        $_FILES[$fileInputName]['name'] = $filename;                 // assign name to file variable
        $this->getProductTable();
        $productMsg = $this->_config['ProductMessages']['config']['createproduct'];
        $uploadFilePath = $this->_config['file_upload_path'];   // get file upload configuration
        $errorMessages = array(
            'post_max_size' => $productMsg['L_FILE_SIZE_EXCEEDS'], // Set error messages
            'max_file_size' => $productMsg['L_FILE_BIG'],
            'min_file_size' => $productMsg['L_FILE_SMALL'],
            'accept_file_types' => $productMsg['L_FILE_TYPE'],
        );
        $fileUploadInformation = $this->_config['file_upload_information'];

        $options = array(
            'upload_dir' => $uploadFilePath['temp_upload_dir'],
            'param_name' => $fileInputName, //file input name                      // Set configuration
            'inline_file_types' => $fileUploadInformation['file_types_allowed'],
            'accept_file_types' => $fileUploadInformation['file_types_allowed'],
            'max_file_size' => $fileUploadInformation['max_allowed_file_size'],
            'image_versions' => array(
                'large' => array(
                    'max_width' => $this->_config['file_upload_path']['product_large_width'],
                    'max_height' => $this->_config['file_upload_path']['product_large_height']
                ),
                'thumbnail' => array(
                    'max_width' => $this->_config['file_upload_path']['product_thumb_width'],
                    'max_height' => $this->_config['file_upload_path']['product_thumb_height']
                ),
                'small' => array(
                    'max_width' => $this->_config['file_upload_path']['product_small_width'],
                    'max_height' => $this->_config['file_upload_path']['product_small_height']
            ))
        );
        $uploadHandler = new UploadHandler($options, true, $errorMessages);
        $fileResponse = $uploadHandler->jsonResponceData;
        if (isset($fileResponse[$fileInputName][0]->error)) {
            $messages = array('status' => "error", 'message' => $fileResponse[$fileInputName][0]->error);
        } else {
            $messages = array('status' => "success", 'message' => 'Image uploaded', 'filename' => $fileResponse[$fileInputName][0]->name);
        }
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This Action is used to save the crm product search
     * @return this will be a confirmation message
     * @author Icreon Tech - SR
     */
    public function addCrmSearchProductAction() {
        $this->checkUserAuthentication();
        $this->getProductTable();
        $productMsg = array_merge($this->_config['ProductMessages']['config']['common'], $this->_config['ProductMessages']['config']['search_product']);
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $product = new Product($this->_adapter);
        if ($request->isPost()) {
            parse_str($request->getPost('searchString'), $searchParam);
            $searchName = $request->getPost('search_name');
            $messages = array();
            $searchParam['search_title'] = $searchName;
            // asd($searchParam);
            $searchParam = $product->getInputFilterCrmSearch($searchParam);
            $searchName = $product->getInputFilterCrmSearch($searchName);

            if (trim($searchName) != '') {
                $saveSearchParam['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $saveSearchParam['isActive'] = 1;
                $saveSearchParam['isDelete'] = 0;
                $saveSearchParam['search_name'] = $searchName;
                $saveSearchParam['search_query'] = serialize($searchParam);
                //asd($saveSearchParam);

                if ($request->getPost('search_id') != '') {
                    $saveSearchParam['modifiend_date'] = DATE_TIME_FORMAT;
                    $saveSearchParam['search_id'] = $request->getPost('search_id');
                    if ($this->_productTable->updateCrmProductSearch($saveSearchParam) == true) {
                        $this->flashMessenger()->addMessage($productMsg['SEARCH_UPDATE_CONFIRM']);
                        $messages = array('status' => "success", 'message' => $productMsg['SEARCH_UPDATE_CONFIRM']);
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                } else {
                    $saveSearchParam['added_date'] = DATE_TIME_FORMAT;
                    if ($this->_productTable->saveProductSearch($saveSearchParam) == true) {
                        $messages = array('status' => "success", 'message' => $productMsg['SEARCH_SAVE_CONFIRM']);
                        $this->flashMessenger()->addMessage($productMsg['SEARCH_SAVE_CONFIRM']);
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                }
            }
        }
    }

    /**
     * This Action is used to generte the select option after saving the new search title
     * @return this will be a string having the all options
     * @author Icreon Tech - SR
     */
    public function getSavedCrmProductSearchSelectAction() {
        $this->checkUserAuthentication();
        $this->getProductTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam = array();
            $dataParam['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
            $dataParam['isActive'] = '1';
            $dataParam['shipSearchId'] = '';

            $crmSearchArray = $this->_productTable->getProductSavedSearch($dataParam);
            $options = '';
            $options .="<option value=''>Select</option>";
            foreach ($crmSearchArray as $key => $val) {
                $options .="<option value='" . $val['product_search_id'] . "'>" . $val['title'] . "</option>";
            }
            return $response->setContent($options);
        }
    }

    /**
     * This Action is used to get the saved crm ship search
     * @param this will be search_id
     * @return this will be search result array
     * @author Icreon Tech - SR
     */
    public function getCrmSearchProductInfoAction() {
        $this->checkUserAuthentication();
        $this->getProductTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            if ($request->getPost('search_id') != '') {
                $dataParam['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $dataParam['isActive'] = '1';
                $dataParam['searchId'] = $request->getPost('search_id');
                $searchResult = $this->_productTable->getProductSavedSearch($dataParam);
                $searchResult = json_encode(unserialize($searchResult[0]['search_query']));
                return $response->setContent($searchResult);
            }
        }
    }

    /**
     * This Action is used to delete the the saved crm product search
     * @return this will be a confirmation message
     * @author Icreon Tech - SR
     */
    public function deleteCrmSearchProductAction() {
        $this->checkUserAuthentication();
        $this->getProductTable();
        $productMsg = $this->_config['ProductMessages']['config']['common'];
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam['search_id'] = $request->getPost('search_id');
            $dataParam['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
            $dataParam['isDelete'] = 1;
            $dataParam['modifiend_date'] = DATE_TIME_FORMAT;
            $this->_productTable->deleteCrmProductSearch($dataParam);
            $this->flashMessenger()->addMessage($productMsg['DELETE_CONFIRM']);
            $messages = array('status' => "success");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This Action is used for the product listing
     * @return this will be result array all product result.
     * @author Icreon Tech - SR
     */
    public function getCrmSearchProductAction() {
        $this->checkUserAuthentication();
        $this->getProductTable();
        $productMsg = array_merge($this->_config['ProductMessages']['config']['common'], $this->_config['ProductMessages']['config']['search_product'], $this->_config['file_upload_path']);
        $request = $this->getRequest();
        $response = $this->getResponse();
        $product = new Product($this->_adapter);
        parse_str($request->getPost('searchString'), $searchParam);

        $dashlet = $request->getPost('crm_dashlet');
        $dashletSearchId = $request->getPost('searchId');
        $dashletId = $request->getPost('dashletId');
        $dashletSearchType = $request->getPost('searchType');
        $dashletSearchString = $request->getPost('dashletSearchString');
        if (isset($dashletSearchString) && $dashletSearchString != '') {
            $searchParam = unserialize($dashletSearchString);
        }
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        $ArrivalDateArray = array();
        $isExport = $request->getPost('export');
        $page_counter = 1;
        do {
            $searchProductParam = $product->assignProductArray($searchParam);
            $searchProductParam['recordLimit'] = $limit;
            $searchProductParam['startIndex'] = $start;

            $searchProductParam['sortField'] = $request->getPost('sidx');
            $searchProductParam['sortOrder'] = $request->getPost('sord');

            if ($isExport != "" && $isExport == "excel") {
                $chunksize = $this->_config['export']['chunk_size'];
                $export_limit = $this->_config['export']['export_limit'];

                $searchProductParam['recordLimit'] = $chunksize;
            }
            $searchResult = $this->_productTable->searchCrmProduct($searchProductParam);
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $dashletResult = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getTableStruct(array('user_id' => $this->_auth->getIdentity()->crm_user_id, 'search_id' => $dashletSearchId, 'dash_id' => $dashletId, 'search_type' => $dashletSearchType));
            $dashletColumnName = array();
            if (!empty($dashletResult)) {
                $dashletColumn = explode(",", $dashletResult[0]['table_column_names']);
                foreach ($dashletColumn as $val) {
                    if (strpos($val, ".")) {
                        $dashletColumnName[] = trim(strstr($val, ".", false), ".");
                    } else {
                        $dashletColumnName[] = trim($val);
                    }
                }
            }
            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $total = $countResult[0]->RecordCount;
            if ($isExport == "excel") {
                if ($total > $chunksize) {
                    $number_of_pages = ceil($total / $chunksize);
                    //echo $number_of_pages .'--'.$page_counter;
                    //echo "<br>";
                    $page_counter++;
                    $start = $chunksize * $page_counter - $chunksize;
                    $searchProductParam['startIndex'] = $start;
                    $searchProductParam['page'] = $page_counter;
                } else {
                    $number_of_pages = 1;
                    $page_counter++;
                }
            } else {
                $page_counter = $page;
                $number_of_pages = ceil($total / $limit);
                $jsonResult['records'] = $countResult[0]->RecordCount;
                $jsonResult['page'] = $page;
                $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
            }
            // $jsonResult['page'] = $page;
            //$jsonResult['records'] = $countResult[0]->RecordCount;
            //$jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
            if (count($searchResult) > 0) {
                $arrExport = array();
                foreach ($searchResult as $val) {
                    $dashletCell = array();
                    $arrCell['id'] = $val['product_id'];
                    $viewLabel = $productMsg['VIEW'];
                    $editLabel = $productMsg['EDIT'];
                    $deleteLabel = $productMsg['DELETE'];
                    $statusArray = $this->statusArray();
                    //$changeStatus = "<div id='product" . $val['product_id'] . "'>" . $statusArray[$val['is_status']] . "</div>";
                    if ($val['is_status'] == 1)
                        $changeStatus = 'Active';
                    else
                        $changeStatus = 'Inactive';
                    $view = "<a class='view-icon' href='/view-crm-product/" . $this->encrypt($val['product_id']) . "/" . $this->encrypt('view') . "'><div class='tooltip'>" . $viewLabel . "<span></span></div></a>";
                    $edit = "<a class='edit-icon' href='/edit-crm-product/" . $this->encrypt($val['product_id']) . "/" . $this->encrypt('edit') . "'><div class='tooltip'>" . $editLabel . "<span></span></div></a>";
                    $delete = "<a class='delete_product delete-icon' href='#delete_product' onClick=deleteProduct('" . $this->encrypt($val['product_id']) . "')><div class='tooltip'>" . $deleteLabel . "<span></span></div></a>";
                    //if (isset($val['category_id']) && trim($val['category_id']) == "0" && $val['product_type_id']!='1') {
                    if ($val['product_type_id'] != '1') {
                        $delete = "";
                    }
                    if ($val['image_name'] != '')
                        $imagePath = "<img src='" . $productMsg['assets_url'] . 'product/thumbnail/' . trim($val['image_name']) . "' width='100px'>";
                    else
                        $imagePath = '';

                    if (isset($val['product_type_id']) and $val['product_type_id'] == "1") {
                        $totalInStock = (isset($val['total_in_stock']) && !empty($val['total_in_stock'])) ? $val['total_in_stock'] : 'N/A';
                        $totalInStockEllis = (isset($val['total_in_ellis_stock']) && !empty($val['total_in_ellis_stock'])) ? $val['total_in_ellis_stock'] : 'N/A';
                        ;
                    } else {
                        $totalInStock = 'N/A';
                        $totalInStockEllis = 'N/A';
                    }

                    if (false !== $dashletColumnKey = array_search('image_name', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $imagePath;
                    if (false !== $dashletColumnKey = array_search('product_name', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = '<a class="txt-decoration-underline" href="/view-crm-product/' . $this->encrypt($val['product_id']) . '/' . $this->encrypt('view') . '">' . $val['product_name'] . '</a>';

                    if (false !== $dashletColumnKey = array_search('product_sell_price', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $val['product_sell_price'];
                    if (false !== $dashletColumnKey = array_search('modified_date', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $this->OutputDateFormat($val['modified_date'], 'dateformatampm');

                    if (false !== $dashletColumnKey = array_search('is_status', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $changeStatus;
                    if (false !== $dashletColumnKey = array_search('total_in_stock', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $totalInStock;
                    if (false !== $dashletColumnKey = array_search('total_in_ellis_stock', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $totalInStockEllis;
                    if (isset($dashlet) && $dashlet == 'dashlet') {
                        $arrCell['cell'] = $dashletCell;
                    } else if ($isExport == "excel") {
                        $arrExport[] = array('Name' => $val['product_name'], 'Sell Price' => $val['product_sell_price'], 'Updated On' => $this->OutputDateFormat($val['modified_date'], 'dateformatampm'), 'Status' => $changeStatus, 'In Stock (Web)' => $totalInStock, 'In Stock (Ellis)' => $totalInStockEllis);
                    } else {
                        $arrCell['cell'] = array($imagePath, $val['product_name'], $val['product_sell_price'], $val['product_sku'], $this->OutputDateFormat($val['modified_date'], 'dateformatampm'), $changeStatus, $totalInStock, $totalInStockEllis, $view . $edit . $delete);
                    }

                    $subArrCell[] = $arrCell;
                    $jsonResult['rows'] = $subArrCell;
                }
                if ($isExport == "excel") {
                    $filename = "product_export_" . date("Y-m-d") . ".csv";
                    $this->arrayToCsv($arrExport, $filename, $page_counter);
                }
            }
        } while ($page_counter <= $number_of_pages && $isExport == "excel");
        if ($isExport == "excel") {
            $this->downloadSendHeaders("product_export_" . date("Y-m-d") . ".csv");
            die;
        } else {
            $response->setContent(\Zend\Json\Json::encode($jsonResult));
            return $response;
        }
        //return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    /**
     * This function is used to list attribute
     * @param array $form
     * @return     array
     * @author     Icreon Tech - AS
     */
    public function getProductAttributesAction() {
        $this->checkUserAuthentication();
        $this->getAttributeTable();
        $this->layout('crm');
        $attributeMsg = $this->_config['ProductMessages']['config']['createAttribute'];
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        $viewModel->setVariables(array(
            'jsLangTranslate' => $attributeMsg,
            'messages' => $messages
        ));
        return $viewModel;
    }

    /**
     * This action is used for showing product attribute in the grid
     * @param $searchParam
     * @return $viewModel
     * @author Icreon Tech - AS
     */
    public function getListProductAttributesAction() {
        $this->checkUserAuthentication();
        $this->getAttributeTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        $searchParam['startIndex'] = $start;
        $searchParam['recordLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');
        $searchParam['attributeId'] = NULL;
        $searchParam['not_inattributeId'] = '';
        $searchResult = $this->getAttributeTable()->searchProductAttributes($searchParam);
        $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
        $jsonResult = array();
        $arrCell = array();
        $subArrCell = array();
        $jsonResult['page'] = $page;
        $jsonResult['records'] = $countResult[0]->RecordCount;
        $jsonResult['total'] = @ceil($countResult[0]->RecordCount / $limit);
        if (count($searchResult) > 0) {
            foreach ($searchResult as $val) {
                $arrCell['id'] = $val['attribute_id'];
                $required = ($val['attribute_mandatory'] == '1') ? 'YES' : 'NO';
                switch ($val['attribute_display_type']) {
                    case '1' : $option = "Text Field";
                        break;
                    case '2' : $option = "Select Box";
                        break;
                    case '3' : $option = "Radio Buttons";
                        break;
                    case '4' : $option = "Checkbox";
                        break;
                }
                $edit = "<a class='edit-icon' href='/edit-product-attribute/" . $this->encrypt($val['attribute_id']) . "'><div class='tooltip'>Edit<span></span></div></a>";
                $attributeOptions = "<a class='option_icon' href='/product-attribute-options/" . $this->encrypt($val['attribute_id']) . "'><div class='tooltip'>Option<span></span></div></a>";
                $delete = "<a class='delete-icon delete_attribute' href='#delete_attribute' onclick=deleteAttribute('" . $this->encrypt($val['attribute_id']) . "')><div class='tooltip'>Delete<span></span></div></a>";
                $arrCell['cell'] = array(stripslashes($val['attribute_name']), stripslashes($val['attribute_label']), $required, $val['num_options'], $option, $edit . $attributeOptions . $delete);
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
        }
        return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    /**
     * This action is used for deleting product attribute 
     * @param $searchParam
     * @return $viewModel
     * @author Icreon Tech - AS
     */
    public function deleteProductAttributeAction() {
        $this->checkUserAuthentication();
        $this->getAttributeTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $attributeMsg = $this->_config['ProductMessages']['config']['createAttribute'];
        if ($request->isPost()) {
            $dataParam['attributeId'] = $this->decrypt($request->getPost('attribute_id'));
            $dataParam['isDelete'] = 1;
            $dataParam['modifiedDate'] = DATE_TIME_FORMAT;
            $dataParam['modifiedBy'] = $this->_auth->getIdentity()->crm_user_id;
            $result = $this->getAttributeTable()->deleteProductAttribute($dataParam);
            if ($result == TRUE) {
                $this->flashMessenger()->addMessage($attributeMsg['DELETE_MSG']);
                $messages = array('status' => "success");
            } else {
                $this->flashMessenger()->addMessage($attributeMsg['ERROR_DELETE_MSG']);
                $messages = array('status' => "error");
            }
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This Action is used to change the product status
     * @return this will result the confirmation
     * @author Icreon Tech - SR
     */
    public function changeProductStatusAction() {
        $this->checkUserAuthentication();
        $this->getProductTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $postData = array();
        $postData['productId'] = $request->getPost('productId');
        if ($request->getPost('status') == 1)
            $postData['status'] = 0;
        else if ($request->getPost('status') == 0)
            $postData['status'] = 1;

        $postData['modifiedDate'] = DATE_TIME_FORMAT;
        $postData['modifiedBy'] = $this->_auth->getIdentity()->crm_user_id;
        $flag = $this->_productTable->changeProductStatus($postData);
        if ($flag == true)
            $messages = array('status' => "success", 'status_value' => $request->getPost('status'));
        else
            $messages = array('status' => "error", 'status_value' => $request->getPost('status'));
        return $response->setContent(\Zend\Json\Json::encode($messages));
    }

    /**
     * This action is used for editing product attribute 
     * @param $searchParam
     * @return $viewModel
     * @author Icreon Tech - AS
     */
    public function editProductAttributeAction() {
        $this->checkUserAuthentication();
        $this->getAttributeTable();
        $attributeObj = new Attribute($this->_adapter);
        $attributeForm = new AttributeForm();
        $this->layout('crm');
        $attributeMsg = $this->_config['ProductMessages']['config']['createAttribute'];
        $response = $this->getResponse();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $displayList = array();
        $displayList[0] = "Select";
        $displayList[1] = "Text Field";
        $displayList[2] = "Select Box";
        $displayList[3] = "Radio Buttons";
        $displayList[4] = "Checkbox";
        $attributeForm->get('displayType')->setAttribute('options', $displayList);
        if ($request->isPost()) {
            $attributeForm->setInputFilter($attributeObj->getInputFilterAddAttribute());
            $attributeForm->setData($request->getPost());
            if (!$attributeForm->isValid()) {
                $errors = $attributeForm->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            $msg [$key] = $attributeMsg[$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $attributeObj->exchangeArray($attributeForm->getData());
                $postData = array();
                $postData['id'] = $attributeObj->id;
                $postData['name'] = $attributeObj->name;
                $postData['label'] = $attributeObj->label;
                $postData['helpText'] = $attributeObj->helpText;
                $postData['makeRequired'] = $attributeObj->makeRequired;
                $postData['displayType'] = $attributeObj->displayType;
                $postData['modifiedDate'] = DATE_TIME_FORMAT;
                $postData['modifiedBy'] = $this->_auth->getIdentity()->crm_user_id;
                $returnData = $this->getAttributeTable()->updateProductAttribute($postData);
                if ($returnData) {
                    $this->flashMessenger()->addMessage($attributeMsg['ATTRIBUTE_UPDATED']);
                    $messages = array('status' => "success");
                } else {
                    $this->flashMessenger()->addMessage($attributeMsg['ERROR_ATTRIBUTE_UPDATE_MSG']);
                    $messages = array('status' => "error");
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $searchParam['startIndex'] = NULL;
        $searchParam['recordLimit'] = NULL;
        $searchParam['sortField'] = NULL;
        $searchParam['sortOrder'] = NULL;
        $searchParam['attributeId'] = $this->decrypt($params['id']);
        $searchParam['not_inattributeId'] = '';
        $searchResult = $this->getAttributeTable()->searchProductAttributes($searchParam);
        $attributeForm->get('name')->setAttribute('value', $searchResult[0]['attribute_name']);
        $attributeForm->get('label')->setAttribute('value', $searchResult[0]['attribute_label']);
        $attributeForm->get('helpText')->setAttribute('value', $searchResult[0]['attribute_help_text']);
        $attributeForm->get('displayType')->setAttribute('value', $searchResult[0]['attribute_display_type']);
        $attributeForm->get('makeRequired')->setAttribute('value', $searchResult[0]['attribute_mandatory']);
        $attributeForm->get('id')->setAttribute('value', $searchResult[0]['attribute_id']);
        $viewModel = new ViewModel();
        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        $viewModel->setVariables(array(
            'jsLangTranslate' => $attributeMsg,
            'messages' => $messages,
            'form' => $attributeForm,
        ));
        return $viewModel;
    }

    /**
     * This action is used to move file from temp to campaign folder
     * @return json
     * @param 1 for add , 2 for edit
     * @author Icreon Tech - AP
     */
    public function moveFileFromTempToUser($filename, $operation = 1, $oldfilename = '') {
        $this->getProductTable();
        $uploadFilePath = $this->_config['file_upload_path'];   /*  get file upload configuration */
        $tempDir = $uploadFilePath['temp_upload_dir'];
        $tempThumbnailDir = $uploadFilePath['temp_upload_thumbnail_dir'];
        $tempLargeDir = $uploadFilePath['temp_upload_large_dir'];
        $tempSmallDir = $uploadFilePath['temp_upload_small_dir'];
        $crmProductDir = $uploadFilePath['assets_upload_dir'] . "product/";
        $crmProductThumbnail = $uploadFilePath['assets_upload_dir'] . "product/thumbnail/";
        $crmProductLarge = $uploadFilePath['assets_upload_dir'] . "product/large/";
        $crmProductSmall = $uploadFilePath['assets_upload_dir'] . "product/small/";
        $tempFile = $tempDir . $filename;
        $tempThumbnailFile = $tempThumbnailDir . $filename;
        $tempLargeFile = $tempLargeDir . $filename;
        $tempSmallFile = $tempSmallDir . $filename;
        $crmProductFilename = $crmProductDir . $filename;
        $crmProductThumbnailFilename = $crmProductThumbnail . $filename;
        if (file_exists($tempFile)) {
            if (@copy($tempFile, $crmProductFilename)) {
                unlink($tempFile);
            }
        }
        if (file_exists($tempThumbnailFile)) {
            if (@copy($tempThumbnailFile, $crmProductThumbnailFilename)) {
                unlink($tempThumbnailFile);
            }
        }

        $crmProductSmallFilename = $crmProductSmall . $filename;
        if (file_exists($tempFile)) {
            if (@copy($tempFile, $crmProductFilename)) {
                unlink($tempFile);
            }
        }
        if (file_exists($tempSmallFile)) {
            if (@copy($tempSmallFile, $crmProductSmallFilename)) {
                unlink($tempSmallFile);
            }
        }
        $crmProductLargeFilename = $crmProductLarge . $filename;
        if (file_exists($tempFile)) {
            if (@copy($tempFile, $crmProductFilename)) {
                unlink($tempFile);
            }
        }
        if (file_exists($tempLargeFile)) {
            if (@copy($tempLargeFile, $crmProductLargeFilename)) {
                unlink($tempLargeFile);
            }
        }



        if ($operation == 2) {
            if ($oldfilename != $filename) {
                $crmProductOldFilename = $tempDir . $oldfilename;
                $crmProductOldThumbnailFilename = $tempThumbnailDir . $oldfilename;
                $crmProductOldLargeFilename = $tempLargeDir . $oldfilename;
                $crmProductOldSmallFilename = $tempSmallDir . $oldfilename;
                if (file_exists($crmProductOldFilename)) {
                    unlink($crmProductOldFilename);
                }
                if (file_exists($crmProductOldThumbnailFilename)) {
                    unlink($crmProductOldThumbnailFilename);
                }
                if (file_exists($crmProductOldLargeFilename)) {
                    unlink($crmProductOldLargeFilename);
                }
                if (file_exists($crmProductOldSmallFilename)) {
                    unlink($crmProductOldSmallFilename);
                }
            }
        }
    }

    /**
     * This action is used to show the large image size of product photo
     * @param image name
     * @author Icreon Tech - SR
     */
    public function productLargeImageAction() {
        $this->getProductTable();
        $param = $this->params()->fromRoute();
        $imageName = $this->Decrypt($param['image']);
        $uploadFilePath = $this->_config['file_upload_path']['assets_url'];   /*  get file upload configuration */
        $crmProductLarge = $uploadFilePath . "product/large/" . $imageName;
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'crmProductLarge' => $crmProductLarge
        ));
        return $viewModel;
    }

    /**
     * This action is used for get Product name in autocomplete
     * @param $product_name
     * @return json array
     * @author Icreon Tech - AP
     */
    public function getProductAction() {
        $this->checkUserAuthentication();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $postArr = $request->getPost()->toArray();
        $this->getProductTable();

        if (isset($postArr['product_type']) && ($postArr['product_type'] == 5 || $postArr['product_type'] == 6 || $postArr['product_type'] == 7 || $postArr['product_type'] == 8)) {
            $postArr['category_id'] = 2;
        } else {
            $postArr['category_id'] = '';
        }

        $postArr['product_type'] = '';
        $productMsg = $this->_config['ProductMessages']['config']['common'];
        $crmProducts = $this->getProductTable()->getCrmProducts($postArr);
        $productResult = array();
        $i = 0;
        if ($crmProducts !== false) {
            foreach ($crmProducts as $id => $product) {
                if (!empty($postArr['is_promotion_check'])) {
                    if ($product['is_promotional'] == 1) {
                        $productResult[$i] = array();
                        $productResult[$i]['product_id'] = $product['product_id'];
                        $productResult[$i]['product_name'] = $product['product_name'];
                        $i++;
                    }
                } else {
                    $productResult[$i] = array();
                    $productResult[$i]['product_id'] = $product['product_id'];
                    $productResult[$i]['product_name'] = $product['product_name'];
                    $i++;
                }
            }
        } else {
            $productResult[$i] = array();
            $productResult[$i]['product_id'] = '';
            $productResult[$i]['product_name'] = $productMsg['NO_RECORD_FOUND'];
        }
        $response->setContent(\Zend\Json\Json::encode($productResult));
        return $response;
    }

    /**
     * This action is used for adding product attribute options
     * @param $searchParam
     * @return $viewModel
     * @author Icreon Tech - AS
     */
    public function addProductAttributeOptionAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getAttributeTable();
        $attributeObj = new Attribute($this->_adapter);
        $attributeOptionMsg = $this->_config['ProductMessages']['config']['createAttributeOption'];
        $attributeOptionForm = new AttributeOptionForm();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        if ($request->isPost()) {
            $attributeOptionForm->setInputFilter($attributeObj->getInputFilterAddAttributeOption());
            $attributeOptionForm->setData($request->getPost());
            if (!$attributeOptionForm->isValid()) {
                $errors = $attributeOptionForm->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            $msg [$key] = $attributeOptionMsg[$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $attributeObj->exchangeOptionArray($attributeOptionForm->getData());
                $postData = array();
                $postData['attributeId'] = $this->decrypt($params['id']);
                $postData['name'] = $attributeObj->name;
                $postData['cost'] = $attributeObj->cost;
                $postData['price'] = $attributeObj->price;
                $postData['addedDate'] = DATE_TIME_FORMAT;
                $postData['addedBy'] = $this->_auth->getIdentity()->crm_user_id;
                $returnData = $this->getAttributeTable()->insertProductAttributeOption($postData);
                if ($returnData) {
                    $this->flashMessenger()->addMessage($attributeOptionMsg['ATTRIBUTE_OPTION_ADDED']);
                    $messages = array('status' => "success");
                } else {
                    $this->flashMessenger()->addMessage($attributeOptionMsg['ERROR_ATTRIBUTE_OPTION_MSG']);
                    $messages = array('status' => "error");
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $viewModel = new ViewModel();
        $cancelUrl = "window.location.href='/product-attribute-options/" . $params['id'] . "'";
        $attributeOptionForm->get('cancel')->setAttribute('onclick', $cancelUrl);
        $viewModel->setVariables(array(
            'form' => $attributeOptionForm,
            'jsLangTranslate' => $attributeOptionMsg,
            'attributeId' => $params['id']
        ));
        return $viewModel;
    }

    /**
     * This action is used for listing product attribute options
     * @param $searchParam
     * @return $viewModel
     * @author Icreon Tech - AS
     */
    public function getProductAttributeOptionsAction() {
        $this->checkUserAuthentication();
        $this->getAttributeTable();
        $this->layout('crm');
        $attributeMsg = $this->_config['ProductMessages']['config']['createAttributeOption'];
        $response = $this->getResponse();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $viewModel = new ViewModel();
        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }

        $prodAttrName = '';
        if (isset($params['id']) and trim($params['id']) != "") {
            $param = array();
            $param['startIndex'] = '';
            $param['recordLimit'] = '';
            $param['sortField'] = '';
            $param['sortOrder'] = '';
            $param['attributeId'] = $this->decrypt(trim($params['id']));
            $param['not_inattributeId'] = '';
            $prodAttrResult = $this->getAttributeTable()->searchProductAttributes($param);
            $prodAttrName = isset($prodAttrResult[0]['attribute_name']) ? $prodAttrResult[0]['attribute_name'] : '';
        }

        $viewModel->setVariables(array(
            'jsLangTranslate' => $attributeMsg,
            'messages' => $messages,
            'attributeId' => $params['id'],
            'prodAttrName' => $prodAttrName
        ));
        return $viewModel;
    }

    /**
     * This action is used for showing product attribute options in the grid
     * @param $searchParam
     * @return $viewModel
     * @author Icreon Tech - AS
     */
    public function listProductAttributeOptionsAction() {
        $this->checkUserAuthentication();
        $this->getAttributeTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        $searchParam['startIndex'] = $start;
        $searchParam['recordLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');
        $searchParam['attributeId'] = $this->decrypt($params['id']);
        $searchParam['option'] = 0;
        $searchResult = $this->getAttributeTable()->searchAttributeOptions($searchParam);
        $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
        $jsonResult = array();
        $arrCell = array();
        $subArrCell = array();
        $jsonResult['page'] = $page;
        $jsonResult['records'] = $countResult[0]->RecordCount;
        $jsonResult['total'] = @ceil($countResult[0]->RecordCount / $limit);
        if (count($searchResult) > 0) {
            foreach ($searchResult as $val) {
                $arrCell['id'] = $val['attribute_option_id'];
                $edit = "<a class='edit-icon' href='/edit-product-attribute-option/" . $this->encrypt($val['attribute_option_id']) . "'><div class='tooltip'>Edit<span></span></div></a>";
                $delete = "<a class='delete-icon delete_attribute_option' href='#delete_attribute_option' onclick=deleteAttributeOption('" . $this->encrypt($val['attribute_option_id']) . "')><div class='tooltip'>Delete<span></span></div></a>";
                $arrCell['cell'] = array(stripslashes($val['option_name']), "$ " . stripslashes($val['option_cost']), "$ " . $val['option_price'], $edit . $delete);
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
        }
        return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    /**
     * This action is used for deleting product attribute option
     * @param $searchParam
     * @return $viewModel
     * @author Icreon Tech - AS
     */
    public function deleteProductAttributeOptionAction() {
        $this->checkUserAuthentication();
        $this->getAttributeTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        $attributeMsg = $this->_config['ProductMessages']['config']['createAttributeOption'];
        if ($request->isPost()) {
            $dataParam['attributeId'] = $this->decrypt($params['id']);
            $dataParam['attributeOptionId'] = $this->decrypt($request->getPost('id'));
            $dataParam['isDelete'] = 1;
            $dataParam['modifiedDate'] = DATE_TIME_FORMAT;
            $dataParam['modifiedBy'] = $this->_auth->getIdentity()->crm_user_id;
            $result = $this->getAttributeTable()->deleteProductAttributeOption($dataParam);
            if ($result == TRUE) {
                $this->flashMessenger()->addMessage($attributeMsg['DELETE_MSG']);
                $messages = array('status' => "success");
            } else {
                $this->flashMessenger()->addMessage($attributeMsg['ERROR_DELETE_MSG']);
                $messages = array('status' => "error");
            }
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This action is used for editing product attribute option
     * @param $searchParam
     * @return $viewModel
     * @author Icreon Tech - AS
     */
    public function editProductAttributeOptionAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getAttributeTable();
        $attributeObj = new Attribute($this->_adapter);
        $attributeOptionMsg = $this->_config['ProductMessages']['config']['createAttributeOption'];
        $attributeOptionForm = new AttributeOptionForm();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        if ($request->isPost()) {
            $attributeOptionForm->setInputFilter($attributeObj->getInputFilterAddAttributeOption());
            $attributeOptionForm->setData($request->getPost());
            if (!$attributeOptionForm->isValid()) {
                $errors = $attributeOptionForm->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            $msg [$key] = $attributeOptionMsg[$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $attributeObj->exchangeOptionArray($attributeOptionForm->getData());
                $postData = array();
                $postData['attributeOptionId'] = $attributeObj->id;
                $postData['name'] = $attributeObj->name;
                $postData['cost'] = $attributeObj->cost;
                $postData['price'] = $attributeObj->price;
                $postData['addedDate'] = DATE_TIME_FORMAT;
                $postData['addedBy'] = $this->_auth->getIdentity()->crm_user_id;
                $returnData = $this->getAttributeTable()->updateProductAttributeOption($postData);
                if ($returnData) {
                    $this->flashMessenger()->addMessage($attributeOptionMsg['ATTRIBUTE_OPTION_UPDATED']);
                    $messages = array('status' => "success");
                } else {
                    $this->flashMessenger()->addMessage($attributeOptionMsg['ERROR_ATTRIBUTE_UPDATED']);
                    $messages = array('status' => "error");
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $viewModel = new ViewModel();
        $searchParam['startIndex'] = NULL;
        $searchParam['recordLimit'] = NULL;
        $searchParam['sortField'] = NULL;
        $searchParam['sortOrder'] = NULL;
        $searchParam['attributeId'] = $this->decrypt($params['id']);
        $searchParam['option'] = '1';
        $searchResult = $this->getAttributeTable()->searchAttributeOptions($searchParam);
        $attributeOptionForm->get('name')->setAttribute('value', $searchResult[0]['option_name']);
        $attributeOptionForm->get('cost')->setAttribute('value', $searchResult[0]['option_cost']);
        $attributeOptionForm->get('price')->setAttribute('value', $searchResult[0]['option_price']);
        $attributeOptionForm->get('id')->setAttribute('value', $searchResult[0]['attribute_option_id']);
        $cancelUrl = "window.location.href='/product-attribute-options/" . $this->encrypt($searchResult[0]['attribute_id']) . "'";
        $attributeOptionForm->get('cancel')->setAttribute('onclick', $cancelUrl);
        $viewModel->setVariables(array(
            'form' => $attributeOptionForm,
            'jsLangTranslate' => $attributeOptionMsg,
            'attributeId' => $this->encrypt($searchResult[0]['attribute_id'])
        ));
        return $viewModel;
    }

    /**
     * This action is used for delete the product
     * @param id of the product
     * @return confirmation
     * @author Icreon Tech - SR
     */
    public function deleteCrmProductAction() {
        $this->checkUserAuthentication();

        $this->getProductTable();
        $request = $this->getRequest();
        $productMsg = $this->_config['ProductMessages']['config']['common'];
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam['id'] = $this->decrypt($request->getPost('id'));
            $dataParam['isDelete'] = 1;
            $dataParam['modifiend_date'] = DATE_TIME_FORMAT;
            $dataParam['modifiend_by'] = $this->_auth->getIdentity()->crm_user_id;

            $this->getProductTable()->deleteCrmProduct($dataParam);
            $this->flashMessenger()->addMessage($productMsg['DELETE_CONFIRM']);
            $messages = array('status' => "success");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This Action is used to get the view crm product
     * @param id to get the product details
     * @return this will be the product details
     * @author Icreon Tech - SR
     */
    public function viewCrmProductAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getProductTable();
        $productMsg = array_merge($this->_config['ProductMessages']['config']['common'], $this->_config['ProductMessages']['config']['search_product'], $this->_config['ProductMessages']['config']['createproduct']);
        $params = $this->params()->fromRoute();
        $searchParam['product_id'] = $this->decrypt($params['id']);

        $productName = '';
        if (isset($searchParam['product_id']) and trim($searchParam['product_id']) != "") {
            $paramP = array();
            $paramP['id'] = trim($searchParam['product_id']);
            $prodInfo = $this->getProductTable()->getProductInfo($paramP);
            $productName = isset($prodInfo[0]['product_name']) ? $prodInfo[0]['product_name'] : '';
        }

        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'product_id' => $searchParam['product_id'],
            'mode' => $this->decrypt($params['mode']),
            'id' => $params['id'],
            'moduleName' => $this->encrypt('product'),
            'jsLangTranslate' => $productMsg,
            'productName' => $productName
        ));
        return $viewModel;
    }

    /**
     * This Action is used show the list of the attributes assigned for a product
     * @param id of the product
     * @author Icreon Tech - SR
     */
    public function manageProductAttributesAction() {
        $this->checkUserAuthentication();
        $this->getProductTable();
        $manageProductForm = new ManageProductForm();
        $params = $this->params()->fromRoute();
        $productMsg = array_merge($this->_config['ProductMessages']['config']['common'], $this->_config['ProductMessages']['config']['createproduct']);

        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }


        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => $productMsg,
            'id' => $params['id'],
            'messages' => $messages,
            'manageProductForm' => $manageProductForm
        ));
        return $viewModel;
    }

    /**
     * This Action is used show the list of the attributes assigned for a product
     * @param id of the product
     * @author Icreon Tech - SR
     */
    public function listManageProductAttributesAction() {
        $this->checkUserAuthentication();
        $this->getProductTable();
        $this->getAttributeTable();
        $productMsg = array_merge($this->_config['ProductMessages']['config']['common'], $this->_config['ProductMessages']['config']['search_product']);
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();

        if ($request->isPost()) {

            $flag = '';
            if (($request->getPost('save_continue')))
                $flag = 1;
            else if (($request->getPost('save')))
                $flag = 1;


            $product_attribute_id = '';
            $product_attribute_required_id = '';

            if ($request->getPost('product_attribute_id')) {
                $product_attribute_id = implode(',', $request->getPost('product_attribute_id'));
            }
            if ($request->getPost('product_attribute_required_id')) {
                $product_attribute_required_id = implode(',', $request->getPost('product_attribute_required_id'));
            }
            if ($product_attribute_id != '') {
                $postData = array();
                $postData['product_attribute_id'] = $product_attribute_id;
                $postData['product_id'] = $this->decrypt($params['id']);
                $flag = $this->getAttributeTable()->removeProductAttribute($postData);
            }
            if ($product_attribute_required_id != '') {

                $postData = array();
                $postData['product_attribute_id'] = $product_attribute_required_id;
                $postData['product_id'] = $this->decrypt($params['id']);
                $flag = $this->getAttributeTable()->changeAttachedProductAttribute($postData);
            }

            if ($flag) {
                $this->flashMessenger()->addMessage($productMsg['CHANGE_CONFIRM']);

                if (($request->getPost('save_continue')))
                    $act = 'save_continue';
                else
                    $act = 'save';
                $messages = array('status' => "success", 'act' => $act);
                return $response->setContent(\Zend\Json\Json::encode($messages));
            }
        }
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        $searchParam['startIndex'] = $start;
        $searchParam['recordLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');
        $searchParam['productId'] = $this->decrypt($params['id']);

        $searchResult = $this->getAttributeTable()->searchAttachedProductAttributes($searchParam);
        $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
        $jsonResult = array();
        $arrCell = array();
        $subArrCell = array();
        $jsonResult['page'] = $page;
        $jsonResult['records'] = $countResult[0]->RecordCount;
        $jsonResult['total'] = @ceil($countResult[0]->RecordCount / $limit);
        $editLabel = $productMsg['EDIT'];
        $deleteLabel = $productMsg['DELETE'];

        if (count($searchResult) > 0) {
            foreach ($searchResult as $val) {
                $arrCell['id'] = $val['attribute_id'];
                switch ($val['attribute_display_type']) {
                    case '1' : $option = $productMsg['TEXT_FIELD'];
                        break;
                    case '2' : $option = $productMsg['SELECT_BOX'];
                        break;
                    case '3' : $option = $productMsg['RADIO_BUTTONS'];
                        break;
                    case '4' : $option = $productMsg['CHECKBOX'];
                        break;
                }
                $edit = "<a class='edit-icon' href='/edit-product-attribute/" . $this->encrypt($val['attribute_id']) . "'><div class='tooltip'>" . $editLabel . "<span></span></div></a>";
                $attributeOptions = "<a class='option_icon' href='/product-attribute-options/" . $this->encrypt($val['attribute_id']) . "'><div class='tooltip'>Option<span></span></div></a>";
                $delete = "<a class='delete-icon delete_attribute' href='#delete_attribute' onclick=deleteAttribute('" . $this->encrypt($val['attribute_id']) . "')><div class='tooltip'>" . $deleteLabel . "<span></span></div></a>";
                $checked = '';
                if ($val['attribute_mandatory'] == 1)
                    $checked = 'checked';
                $removeCheck = "<input type='checkbox' name='product_attribute_id[]' value='" . $val['product_attribute_id'] . "' id='" . $val['product_attribute_id'] . "'>";
                $requiredCheck = "<input type='checkbox' name='product_attribute_required_id[]' " . $checked . " value='" . $val['product_attribute_id'] . "' id='required_" . $val['product_attribute_id'] . "'>";
                $arrCell['cell'] = array($removeCheck, stripslashes($val['attribute_name']), stripslashes($val['attribute_label']), $val['num_options'], $requiredCheck, $option, $edit . $attributeOptions . $delete);
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
        }
        return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    /**
     * This Action is used assign the product attributes to the product
     * @param id of the product
     * @author Icreon Tech - SR
     */
    public function attachProductAttributesAction() {
        $this->checkUserAuthentication();
        $this->getProductTable();
        $this->getAttributeTable();
        $productMsg = array_merge($this->_config['ProductMessages']['config']['common'], $this->_config['ProductMessages']['config']['search_product']);
        $params = $this->params()->fromRoute();
        $attributesForm = new AddProductAttributesForm();
        $searchParam = array();
        $searchParam['startIndex'] = '';
        $searchParam['recordLimit'] = '';
        $searchParam['sortField'] = '';
        $searchParam['sortOrder'] = '';
        $searchParam['productId'] = $this->decrypt($params['id']);
        $searchResult = $this->getAttributeTable()->searchAttachedProductAttributes($searchParam);
        $searchResultAttribute = array();
        foreach ($searchResult as $val) {
            $searchResultAttribute[] = $val['attribute_id'];
        }
        $attributeId = implode(',', $searchResultAttribute);

        $searchParam = array();
        $searchParam['startIndex'] = 0;
        $searchParam['recordLimit'] = '';
        $searchParam['sortField'] = 'attribute_name';
        $searchParam['sortOrder'] = 'ASC';
        $searchParam['not_inattributeId'] = $attributeId;
        $searchResult = $this->getAttributeTable()->searchProductAttributes($searchParam);
        $attributeArray = array();
        $recCount = count($searchResult);
        if (!empty($searchResult)) {
            foreach ($searchResult as $val) {
                $attributeArray[$val['attribute_id']] = stripslashes($val['attribute_name']);
            }
            $attributesForm->get('productAttributes')->setAttribute('options', $attributeArray);
        }

        $attributesForm->get('id')->setAttribute('value', $params['id']);
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $postedData = array();
            $postedData['product_id'] = $this->decrypt($request->getPost('id'));
            $postedData['added_by'] = $this->_auth->getIdentity()->crm_user_id;
            $postedData['added_date'] = DATE_TIME_FORMAT;
            $postedData['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
            $postedData['modified_date'] = DATE_TIME_FORMAT;
            foreach ($request->getPost('productAttributes') as $val) {
                $postedData['attribute_id'] = $val;
                $this->getProductTable()->attachProductAttribute($postedData);
            }
            $this->flashMessenger()->addMessage($productMsg['ATTACH_ATTRIBUTE_SUCCESS_MESSAGE']);
            $messages = array('status' => "success", 'message' => $productMsg['ATTACH_ATTRIBUTE_SUCCESS_MESSAGE']);
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'attributesForm' => $attributesForm,
            'jsLangTranslate' => $productMsg,
            'productId' => $this->decrypt($params['id']),
            'recCount' => $recCount
        ));
        return $viewModel;
    }

    /**
     * This Action is used to generte the select option after click on move checkbox
     * @param void
     * @return this will be a string having the all options
     * @author Icreon Tech - DT
     */
    public function getSelectCategoryAction() {
        $this->checkUserAuthentication();
        $this->getProductTable();
        $category = new Category($this->_adapter);
        $request = $this->getRequest();
        $response = $this->getResponse();
        /* Get all Categories code */
        $catArr = array();
        $categoryArr = $category->getCategoriesArr($catArr);
        $allCategories = $this->_categoryTable->getCategories($categoryArr);
        $notCategoryId = $this->decrypt($request->getPost('notCategoryId'));
        if (!empty($allCategories)) {
            $this->getSelectDeleteArr($allCategories, $notCategoryId);
        }
        /* End Get all Categories code */
        unset($this->_categorySelectArr[0]);
        $categorySelectArr = $this->_categorySelectArr;
        if ($request->isPost()) {
            $options = '';
            foreach ($categorySelectArr as $key => $val) {
                $options .="<option value='" . $key . "'>" . $val . "</option>";
            }
            echo $options;
            die;
            return $response->setContent($options);
        }
    }

    /**
     * This function is used to get select category structure for delete category
     * @param       void
     * @return     json
     * @author     Icreon Tech - DT
     */
    public function getSelectDeleteArr($categories, $notCategoryId, $parentVal = 0, $level = 0) {
        foreach ($categories as $cat) {
            if ($cat['parent_id'] == $parentVal && $cat['category_id'] != $notCategoryId) {
                $this->_categorySelectArr[$cat['category_id']] = str_repeat('-', $level) . $cat['category_name'];
                $this->getSelectDeleteArr($categories, $notCategoryId, $cat['category_id'], $level + 1);
            }
        }
    }

    /**
     * This Action is used show the list of the attributes options assigned for a product
     * @param id of the product
     * @author Icreon Tech - SR
     */
    public function manageProductAttributesOptionsAction() {
        $this->checkUserAuthentication();
        $this->getProductTable();
        $manageProductForm = new ManageProductForm();
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();
        $productMsg = array_merge($this->_config['ProductMessages']['config']['common'], $this->_config['ProductMessages']['config']['createproduct']);
        $this->getAttributeTable();
        $searchParam = array();
        $searchParam['startIndex'] = '';
        $searchParam['recordLimit'] = '';
        $searchParam['sortField'] = '';
        $searchParam['sortOrder'] = '';
        $searchParam['productId'] = $this->decrypt($params['id']);
        $attributeResult = $this->getAttributeTable()->searchAttachedProductAttributes($searchParam);
        //asd($attributeResult);
        $attributeIdArray = array();
        foreach ($attributeResult as $val) {
            $attributeIdArray[] = $val['attribute_id'];
        }
        $attributeId = implode(',', $attributeIdArray);
        $searchParam = array();
        $searchParam['startIndex'] = '0';
        $searchParam['recordLimit'] = '';
        $searchParam['sortField'] = 'option_name';
        $searchParam['sortOrder'] = 'ASC';
        $searchParam['attributeId'] = $attributeId;
        $searchParam['option'] = 0;

        /** Check if exists */
        $searchAttributeParam = array();
        $attributeOptionResultArray = array();
        $searchAttributeParam['product_id'] = $this->decrypt($params['id']);
        $attributeOptionResult = $this->getProductTable()->getProductAttributeOptions($searchAttributeParam);

        if (!empty($attributeOptionResult)) {
            foreach ($attributeOptionResult as $key => $val) {
                $attributeOptionResultArray[$val['attribute_option_id']] = $attributeOptionResult[$key];
            }
        }
        $optionResult = $this->getAttributeTable()->searchAttributeOptions($searchParam);



        $optionResultArray = array();
        foreach ($optionResult as $key => $val) {
            $optionResultArray[$val['attribute_id']][] = $val;
        }
        //  asd($optionResultArray);
        $response = $this->getResponse();

        if ($request->isPost()) {
            $flag = '';
            $removePostData['product_id'] = $this->decrypt($params['id']);
            $this->getProductTable()->removeAttachProductAttributeOptions($removePostData);
            $flag = 1;
            if ($request->getPost('attribute_option')) {
                $postParam = $request->getPost();
                $postParam['attribute_option'];
                foreach ($postParam['attribute_option'] as $val) {
                    $postData = array();
                    $postData['product_id'] = $this->decrypt($params['id']);

                    $optionArray = explode('^^', $val);
                    $postData['attribute_id'] = $optionArray[0];
                    $postData['option_id'] = $optionArray[1];
                    if ($postParam['is_default_' . $optionArray[0]] == $optionArray[1])
                        $postData['is_default'] = 1;
                    else
                        $postData['is_default'] = 0;
                    $postData['option_cost'] = $postParam['cost_' . $optionArray[1]];
                    $postData['option_price'] = $postParam['price_' . $optionArray[1]];
                    $postData['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                    $postData['added_date'] = DATE_TIME_FORMAT;
                    $postData['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                    $postData['modified_date'] = DATE_TIME_FORMAT;
                    $flag = $this->getProductTable()->attachProductAttributeOptions($postData);
                }
            }

            if ($flag) {
                $this->flashMessenger()->addMessage($productMsg['PRODUCT_OPTIONS_CONFIRMATION']);
                $messages = array('status' => "success");
                return $response->setContent(\Zend\Json\Json::encode($messages));
            }
        }



        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => $productMsg,
            'id' => $params['id'],
            'messages' => $messages,
            'manageProductForm' => $manageProductForm,
            'attributeResult' => $attributeResult,
            'optionResultArray' => $optionResultArray,
            'attributeOptionResult' => $attributeOptionResultArray
        ));
        return $viewModel;
    }

    /**
     * This action is used for get Category name in autocomplete
     * @param void
     * @return json array
     * @author Icreon Tech - DT
     */
    public function getCategoriesAction() {
        $this->checkUserAuthentication();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $postArr = $request->getPost()->toArray();
        $this->getProductTable();
        $category = new Category($this->_adapter);
        $catArr = array();
        $categoryArr = $category->getCategoriesArr($postArr);
        $allCategories = $this->_categoryTable->getCategories($categoryArr);
        $categoryResult = array();
        $i = 0;
        if (!empty($allCategories)) {
            foreach ($allCategories as $id => $category) {
                $categoryResult[$i] = array();
                $categoryResult[$i]['category_id'] = $category['category_id'];
                $categoryResult[$i]['category_name'] = $category['category_name'];
                $i++;
            }
        }
        $response->setContent(\Zend\Json\Json::encode($categoryResult));
        return $response;
    }

    /**
     * This action is used for get Category name in autocomplete
     * @param void
     * @return html string
     * @author Icreon Tech - DT
     */
    public function getCategoryProductAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $categoryId = $request->getPost('category_id');
            $productTypeId = $request->getPost('product_type_id');
            $response = $this->getResponse();
            $this->getProductTable();
            $productParam = array();
            $productParam['productType'] = $productTypeId;/** product */
            $productParam['categoryId'] = $categoryId;
            $productResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getProductAttributeDetails($productParam);
            $productResultArr = array();
            $i = 0;
            $optionsStr = "<option value=''>Select</option>";
            //asd($productResult);
            if (!empty($productResult)) {
                $alreadyExist = array();
                foreach ($productResult as $product) {
                    if ($product['total_in_stock'] != 0) {
                        if (!in_array($product['product_id'], $alreadyExist)) {
                            $productResultArr[$i] = array();
                            $productResultArr[$i]['product_id'] = $product['product_id'];
                            $productResultArr[$i]['category_name'] = $product['product_name'];
                            $optionsStr.="<option value='{$product['product_id']}'>{$product['product_name']}</option>";
                            $alreadyExist[] = $product['product_id'];
                        }
                        $i++;
                    }
                }
            }
            return $response->setContent($optionsStr);
        }
    }

    /**
     * This action is used for get product detail
     * @param void
     * @return json array
     * @author Icreon Tech - DT
     */
    public function getProductDetailInventoryAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $productId = $request->getPost('product_id');
            $response = $this->getResponse();
            $this->getProductTable();
            $productParam = array();
            $productParam['productId'] = $productId;
            $productResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getProductAttributeDetails($productParam);
            $discount = 0;
            if(count($productResult)>0){
                foreach($productResult as $key=>$val){
                    if (defined('MEMBERSHIP_DISCOUNT') && MEMBERSHIP_DISCOUNT!=0) {
                            $discount = MEMBERSHIP_DISCOUNT;
                        }
                        if($val['is_membership_discount'] == 1)
                            $productResult[$key]['product_sell_price'] = $val['product_sell_price']- $this->Roundamount(($val['product_sell_price']*$discount)/100);
                }
            }
            
            $response->setContent(\Zend\Json\Json::encode($productResult[0]));
            return $response;
        }
    }

    /**
     * This action is used for get related Product name in autocomplete
     * @param void
     * @return json array
     * @author Icreon Tech - DT
     */
    public function getRelatedProductAction() {
        $this->checkUserAuthentication();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $postArr = $request->getPost()->toArray();
        $this->getProductTable();
        $postArr['product_id'] = isset($postArr['product_id']) ? $postArr['product_id'] : '';
        $postArr['related_product_name'] = $postArr['related_product_name'];
        $crmProducts = $this->getProductTable()->getCrmRelatedProducts($postArr);
        $discount = 0;
            if (defined('MEMBERSHIP_DISCOUNT') && MEMBERSHIP_DISCOUNT!=0) {
                $discount = MEMBERSHIP_DISCOUNT;
            }         
        $productResult = array();
        $i = 0;
        if ($crmProducts !== false) {
            if (!empty($crmProducts)) {
                foreach ($crmProducts as $id => $product) {
                    $productResult[$i] = array();
                    $productResult[$i]['product_id'] = $product['product_id'];
                    $productResult[$i]['product_name'] = $product['product_name'];
                    if($product['is_membership_discount'] == 1) {
                        $productResult[$i]['product_sell_price'] = $product['product_sell_price'] - $this->Roundamount(($product['product_sell_price']*$discount)/100);
                    }
                    else {
                        $productResult[$i]['product_sell_price'] = $product['product_sell_price'];
                    }
                    
                    $i++;
                }
            } else {
                $productResult[$i] = array();
                $productResult[$i]['product_id'] = 'No Record Found';
                $productResult[$i]['product_name'] = 'No Record Found';
            }
        }
        $response->setContent(\Zend\Json\Json::encode($productResult));
        return $response;
    }

    /**
     * This action is used for display products for shop in frontend
     * @param void
     * @return 
     * @author Icreon Tech - KK
     */
    public function getShopProductsAction() {
        $this->getProductTable();
        $productMsg = array_merge($this->_config['ProductMessages']['config']['common'], $this->_config['ProductMessages']['config']['search_product'], $this->_config['file_upload_path']);
        $request = $this->getRequest();
        $response = $this->getResponse();
        $param = $this->params()->fromRoute();
        $productId = '';

        if (isset($param['product_id']) && !empty($param['product_id']))
            $productId = $param['product_id'];

        $category = new Category($this->_adapter);
        $catArr = array();
        $categoryArr = $category->getCategoriesArr($catArr);
        $allCategories = $this->_categoryTable->getCategories($categoryArr);
        $catArr = array();
        $catArr['sort_field'] = 'tbl_cat.sort_order';
        $catArr['sort_order'] = 'ASC';
        $categoryArr = $category->getCategoriesArr($catArr);
        $allCategories = $this->_categoryTable->getCategories($categoryArr);

        $categoryData = array();
        if (!empty($allCategories)) {
            foreach ($allCategories as $category) {
                if ($category['category_id'] != $this->_config['custom_frame_id']) {
                    $catData = array();
                    $catData['label'] = $category['category_name'];
                    $catData['id'] = $this->encrypt($category['category_id']);
                    $catData['parent_id'] = $this->encrypt($category['parent_id']);
                    $catData['is_deletable'] = $category['is_deletable'];
                    $categoryData[] = $catData;
                }
            }
        }

        $categoriesArr = $this->buildTreeArray($categoryData);
        $postData[0] = '';
        $postData[1] = '';
        $postData[3] = 'asc';
        $postData[2] = 'tbl_membership.minimun_donation_amount';
        $membershipData = $this->getServiceLocator()->get('User\Model\MembershipTable')->getAllMembership($postData);
        $i = 0;
        foreach ($membershipData as $key) {
            if ($key['membership_id'] != '1' && $key['minimun_donation_amount'] > 0) {
                if ($i == 0) {
                    $min_donation = $key['minimun_donation_amount'];
                } else {
                    if ($min_donation > $key['minimun_donation_amount']) {
                        $min_donation = $key['minimun_donation_amount'];
                    }
                }
                $i++;
            }
        }
        if ($this->_flashMessage->hasMessages()) {
                $messages = $this->_flashMessage->getMessages();
            }
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'jsLangTranslate' => $productMsg,
            'category' => $categoriesArr,
            'messages' => $productMsg,
            'minDonation' => $min_donation,
            'productId' => $productId,
            'messages'=>$messages
        ));
        return $viewModel;
    }

    /**
     * This action is used for display products for shop in frontend
     * @param void
     * @return 
     * @author Icreon Tech - KK
     */
    public function getShopProductsListAction() {
        $this->getProductTable();
        $productMsg = array_merge($this->_config['ProductMessages']['config']['common'], $this->_config['ProductMessages']['config']['search_product'], $this->_config['file_upload_path']);
        $request = $this->getRequest();
        $response = $this->getResponse();
        $product = new Product($this->_adapter);
        parse_str($request->getPost('searchString'), $searchParam);
        $productId = '';
        if ($request->getPost('product_id') != '')
            $productId = $this->decrypt($request->getPost('product_id'));

        $limit = $this->_config['grid_config']['numRecPerPage'];
        $page = $request->getPost('page');
        $categoryid = $request->getPost('categoryid');
        $moreCnt = $request->getPost('moreCnt');
        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        $ArrivalDateArray = array();
        $searchProductParam = $product->assignProductArray($searchParam);
        $searchProductParam['startIndex'] = $start;
        $searchProductParam['recordLimit'] = $limit;
        $sortField = $request->getPost('sidx');
        $sortOrder = $request->getPost('sord');

        if (isset($sortField) and trim($sortField) != "") {
            $searchProductParam['sortField'] = $sortField;
        } else {
            $searchProductParam['sortField'] = "productTab.disp_order";
        }

        if (isset($sortOrder) and trim($sortOrder) != "") {
            $searchProductParam['sortOrder'] = $sortOrder;
        } else {
            $searchProductParam['sortOrder'] = "asc";
        }


        $titleCategory1 = "";
        $descriptionCategory1 = "";
        $titleCategory2 = "";
        $descriptionCategory2 = "";

        if (!empty($categoryid)) {
            $searchProductParam['productCategory'] = $this->decrypt($categoryid);
            $parentcatid = $request->getPost('parentcatid');
            $category = new Category($this->_adapter);

            // 2
            if (isset($parentcatid) and trim($parentcatid) != "" and $this->decrypt($parentcatid) != "0" and $parentcatid != "0") {
                $catArr = array();
                $catArr['category_id'] = $this->decrypt($parentcatid);
                $categoryArr = $category->getCategoriesArr($catArr);
                $allCategories1 = $this->_categoryTable->getCategories($categoryArr);
                $titleCategory1 = (isset($allCategories1[0]['category_name']) and trim($allCategories1[0]['category_name']) != "") ? trim($allCategories1[0]['category_name']) : "";
                $descriptionCategory1 = (isset($allCategories1[0]['category_description']) and trim($allCategories1[0]['category_description']) != "") ? trim($allCategories1[0]['category_description']) : "";

                $catArr['category_id'] = $this->decrypt($categoryid);
                $categoryArr = $category->getCategoriesArr($catArr);
                $allCategories2 = $this->_categoryTable->getCategories($categoryArr);
                $titleCategory2 = (isset($allCategories2[0]['category_name']) and trim($allCategories2[0]['category_name']) != "") ? trim($allCategories2[0]['category_name']) : "";
                $descriptionCategory2 = (isset($allCategories2[0]['category_description']) and trim($allCategories2[0]['category_description']) != "") ? trim($allCategories2[0]['category_description']) : "";
            } else {
                $catArr = array();
                $catArr['category_id'] = $this->decrypt($categoryid);
                $categoryArr = $category->getCategoriesArr($catArr);
                $allCategories2 = $this->_categoryTable->getCategories($categoryArr);
                $titleCategory1 = (isset($allCategories2[0]['category_name']) and trim($allCategories2[0]['category_name']) != "") ? trim($allCategories2[0]['category_name']) : "";
                $descriptionCategory1 = (isset($allCategories2[0]['category_description']) and trim($allCategories2[0]['category_description']) != "") ? trim($allCategories2[0]['category_description']) : "";
            }
            // 2 
        }

        $discount = '0';
        /*
        if (!empty($this->_auth->getIdentity()->user_id)) {

            $user_id = $this->_auth->getIdentity()->user_id;
            $userArr = array('user_id' => $user_id);
            $contactDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserMemberShipDetail($userArr);
            // asd($contactDetail);
            if ($contactDetail['membership_id'] != '1') {
                $discount = $contactDetail['membership_discount'];
            }
        }*/
        if (defined('MEMBERSHIP_DISCOUNT') && MEMBERSHIP_DISCOUNT!=0) {
			$discount = MEMBERSHIP_DISCOUNT;
	}
        /*else {
                        if (!empty($this->_auth->getIdentity()->user_id)) {

                    $user_id = $this->_auth->getIdentity()->user_id;
                    $userArr = array('user_id' => $user_id);
                    $contactDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserMemberShipDetail($userArr);
                    if ($contactDetail['membership_id'] != '1') {
                        $discount = $contactDetail['membership_discount'];
                    }
                }
        }*/
        
        $searchProductParam['available'] = $this->_config['application_source_id'];
        $searchProductParam['product_id'] = $productId;
        $searchProductParam['restricted_category_id'] = $this->_config['custom_frame_id'];
        if(isset($searchProductParam['productCategory']) && $searchProductParam['productCategory'] == 0)
        {
            $searchProductParam['productCategory'] = "";
            $searchProductParam['featured'] = 1;
        }
        
       // echo "<pre>";print_r($searchProductParam);die;
        $searchResult = $this->_productTable->getShopProduct($searchProductParam);
        $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => $productMsg,
            'productlist' => $searchResult,
            'messages' => $productMsg,
            'categoryid' => $categoryid,
            'discount' => $discount,
            'upload_file_path' => $this->_config['file_upload_path'],
            'countResult' => $countResult[0]->RecordCount,
            'record' => $limit * $page,
            'page' => $page,
            'titleCategory1' => $titleCategory1,
            'descriptionCategory1' => $descriptionCategory1,
            'titleCategory2' => $titleCategory2,
            'descriptionCategory2' => $descriptionCategory2,
            'moreCnt' => $moreCnt
        ));
        return $viewModel;
    }

    /**
     * This action is used for get product type in autocomplete
     * @param void
     * @return json array
     * @author Icreon Tech - DT
     */
    public function getProductTypesAction() {
        $this->checkUserAuthentication();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $postArr = $request->getPost()->toArray();
        $this->getProductTable();
        $product = new Product($this->_adapter);
        $productTypeArr = $product->getProductTypeArr($postArr);
        $allProductTypes = $this->_productTable->getProductTypes($productTypeArr);
        $productTypeResult = array();
        $i = 0;
        if (!empty($allProductTypes)) {
            foreach ($allProductTypes as $id => $productType) {
                $productTypeResult[$i] = array();
                $productTypeResult[$i]['product_type_id'] = $productType['product_type_id'];
                $productTypeResult[$i]['product_type'] = $productType['product_type'];
                $i++;
            }
        }
        $response->setContent(\Zend\Json\Json::encode($productTypeResult));
        return $response;
    }

    /**
     * This action is used to rearrange all product of any category
     * @param void
     * @return json array
     * @author Icreon Tech - DT
     */
    public function manageCategoryProductAction() {

        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getProductTable();
        $category = new Category($this->_adapter);
        $categoryMsg = $this->_config['ProductMessages']['config']['category_list'];
        $request = $this->getRequest();
        $response = $this->getResponse();
        $param = $this->params()->fromRoute();
        $selectProductForm = new SelectProductForm();
        $catArr = array();
        $categoryForm = new CreateCategoryForm();
        $category = new Category($this->_adapter);
        $categoryArr = $category->getCategoriesArr($catArr);
        $allCategories = $this->_categoryTable->getCategories($categoryArr);
        if (!empty($allCategories)) {
            $this->getSelectArr($allCategories);
        }
        $selectProductForm->get('category_id')->setAttribute('options', $this->_categorySelectArr);
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'jsLangTranslate' => $categoryMsg,
            'selectProductForm' => $selectProductForm,
        ));
        return $viewModel;
    }

    public function categoryProductListAction() {
        $this->getProductTable();
        $this->layout('crm');
        $productMsg = array_merge($this->_config['ProductMessages']['config']['common'], $this->_config['ProductMessages']['config']['search_product'], $this->_config['file_upload_path']);
        $request = $this->getRequest();
        $response = $this->getResponse();
        $product = new Product($this->_adapter);
        $param = $this->params()->fromRoute();
        $searchProductParam['productCategory'] = $param['id'];
        $searchProductParamArr = $product->assignProductArray($searchProductParam);
        $searchProductParamArr['startIndex'] = '';
        $searchProductParamArr['recordLimit'] = '';
        $searchProductParamArr['sortField'] = 'productTab.disp_order';
        $searchProductParamArr['sortOrder'] = 'asc';
        $searchProductParamArr['product_id'] = '';
        $searchProductParamArr['restricted_category_id'] = $this->_config['custom_frame_id'];
		$searchProductParamArr['productType'] = 1;
        $searchResult = $this->_productTable->getShopProduct($searchProductParamArr);
        $categoryData = array();
        if (!empty($searchResult)) {
            foreach ($searchResult as $category) {
                $catData = array();
                $catData['label'] = $category['product_name'];
                $catData['id'] = $category['product_id'];
                $categoryData[] = $catData;
            }
        }
        $catProArr = $this->buildTreeArrayPro($categoryData);
        $response->setContent(\Zend\Json\Json::encode($catProArr));
        return $response;
    }

    /**
     * This function is used to get categories build tree array
     * @param   array
     * @return  array
     * @author  Icreon Tech - DT
     */
    function buildTreeArrayPro(array $elements, $parentId = 0) {
        $branch = array();
        if (!empty($elements)) {            
            foreach ($elements as $element) {
                $element['id'] = $element['id'];
                $element['name'] = $element['label'];
                $branch[] = $element;
            }
        }
        return $branch;
    }

    public function editCatProductDragDropAction() {
        $this->checkUserAuthentication();
        $this->getProductTable();
        $this->layout('crm');
        $product = new Product($this->_adapter);
        $productMsg = $this->_config['ProductMessages']['config']['common'];
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $sibling = $request->getPost('siblings');
            $proArr = explode(',', $request->getPost('siblings'));
            $trgtNode = $request->getPost('targetNode');
            $arr = array_count_values($proArr);
            foreach ($arr as $key => $val) {
                if ($val == 2) {
                    $movedNode = $key;
                }
            }
            $order = 1;
            for ($i = 0; $i < count($proArr); $i++) {
                if ($proArr[$i] == $movedNode && $trgtNode == $proArr[$i - 1]) {
                    $param = array();
                    $param['id'] = $proArr[$i];
                    $param['order'] = $order++;
                    $param['modifiedBy'] = $this->_auth->getIdentity()->crm_user_id;
                    $param['modifiedDate'] = DATE_TIME_FORMAT;
                    $this->getProductTable()->changeProOrder($param);
                } else if ($proArr[$i] != $movedNode) {
                    $param = array();
                    $param['id'] = $proArr[$i];
                    $param['order'] = $order++;
                    $param['modifiedBy'] = $this->_auth->getIdentity()->crm_user_id;
                    $param['modifiedDate'] = DATE_TIME_FORMAT;
                    $this->getProductTable()->changeProOrder($param);
                }
            }
            $messages = array('status' => "success", 'message' => $productMsg['ORDER_UPDATED']);
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }
	/**
     * This action is used for get kiosk Product
     * @param void
     * @return json array
     * @author Icreon Tech - DT
     */
    public function getKioskProductAction() {
 
//        $param['productType'] ='';
//        $param['productName']='';
//        $param['productSku']='';
//        $param['productCategory']='';
//        $param['sellPriceFrom']='';
//        $param['sellPriceTo']='';
//        $param['promotional']='';
//        $param['featured']='';
//        $param['status']='';
//        $param['available']='';
//        $param['startIndex']='';
//        $param['recordLimit']='';
//        $param['sortField']='';
//        $param['sortOrder']='';
//        $param['product_id']='71';
//        $param['restricted_category_id'] ='';
//        $searchResult = $this->_productTable->getShopProduct($param);

        $this->getProductTable();
        $productMsg = array_merge($this->_config['ProductMessages']['config']['common'], $this->_config['ProductMessages']['config']['search_product'], $this->_config['file_upload_path']);
        $request = $this->getRequest();
        $response = $this->getResponse();
        $param = $this->params()->fromRoute();
        $productId = '';

        if (isset($param['product_id']) && !empty($param['product_id']))
            $productId = $param['product_id'];

        $category = new Category($this->_adapter);
        $catArr = array();
        $categoryArr = $category->getCategoriesArr($catArr);
        $allCategories = $this->_categoryTable->getCategories($categoryArr);
        $catArr = array();
        $catArr['sort_field'] = 'tbl_cat.sort_order';
        $catArr['sort_order'] = 'ASC';
        $categoryArr = $category->getCategoriesArr($catArr);
        $allCategories = $this->_categoryTable->getCategories($categoryArr);

        $categoryData = array();
        if (!empty($allCategories)) {
            foreach ($allCategories as $category) {
                if ($category['category_id'] != $this->_config['custom_frame_id']) {
                    $catData = array();
                    $catData['label'] = $category['category_name'];
                    $catData['id'] = $this->encrypt($category['category_id']);
                    $catData['parent_id'] = $this->encrypt($category['parent_id']);
                    $catData['is_deletable'] = $category['is_deletable'];
                    $categoryData[] = $catData;
                }
            }
        }

        $categoriesArr = $this->buildTreeArray($categoryData);
        $postData[0] = '';
        $postData[1] = '';
        $postData[3] = 'asc';
        $postData[2] = 'tbl_membership.minimun_donation_amount';
        $membershipData = $this->getServiceLocator()->get('User\Model\MembershipTable')->getAllMembership($postData);
        $i = 0;
        foreach ($membershipData as $key) {
            if ($key['membership_id'] != '1' && $key['minimun_donation_amount'] > 0) {
                if ($i == 0) {
                    $min_donation = $key['minimun_donation_amount'];
                } else {
                    if ($min_donation > $key['minimun_donation_amount']) {
                        $min_donation = $key['minimun_donation_amount'];
                    }
                }
                $i++;
            }
        }
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'jsLangTranslate' => $productMsg,
            'category' => $categoriesArr,
            'messages' => $productMsg,
            'minDonation' => $min_donation,
            'productId' => $productId
        ));
        return $viewModel;
    }
    /**
     * This action is used for display products for kiosk shop in frontend
     * @param void
     * @return 
     * @author Icreon Tech - KK
     */
    public function getKioskShopProductsListAction() {
        $this->getProductTable();
        $productMsg = array_merge($this->_config['ProductMessages']['config']['common'], $this->_config['ProductMessages']['config']['search_product'], $this->_config['file_upload_path']);
        $request = $this->getRequest();
        $response = $this->getResponse();
         /*
          $product = new Product($this->_adapter);
        parse_str($request->getPost('searchString'), $searchParam);
        $productId = '';
        if ($request->getPost('product_id') != '')
            $productId = $this->decrypt($request->getPost('product_id'));

        $limit = $this->_config['grid_config']['numRecPerPage'];
        $page = $request->getPost('page');
        $categoryid = $request->getPost('categoryid');
        $moreCnt = $request->getPost('moreCnt');
        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        $ArrivalDateArray = array();
        $searchProductParam = $product->assignProductArray($searchParam);
        $searchProductParam['startIndex'] = $start;
        $searchProductParam['recordLimit'] = $limit;
        $sortField = $request->getPost('sidx');
        $sortOrder = $request->getPost('sord');

        if (isset($sortField) and trim($sortField) != "") {
            $searchProductParam['sortField'] = $sortField;
        } else {
            $searchProductParam['sortField'] = "productTab.disp_order";
        }

        if (isset($sortOrder) and trim($sortOrder) != "") {
            $searchProductParam['sortOrder'] = $sortOrder;
        } else {
            $searchProductParam['sortOrder'] = "asc";
        }

       
        $titleCategory1 = "";
        $descriptionCategory1 = "";
        $titleCategory2 = "";
        $descriptionCategory2 = "";

        if (!empty($categoryid)) {
            $searchProductParam['productCategory'] = $this->decrypt($categoryid);
			$searchProductParam['productCategory'] = ""; 
            $parentcatid = $request->getPost('parentcatid');
            $category = new Category($this->_adapter);

            // 2
            if (isset($parentcatid) and trim($parentcatid) != "" and $this->decrypt($parentcatid) != "0" and $parentcatid != "0") {
                $catArr = array();
                $catArr['category_id'] = $this->decrypt($parentcatid);
                $categoryArr = $category->getCategoriesArr($catArr);
                $allCategories1 = $this->_categoryTable->getCategories($categoryArr);
                $titleCategory1 = (isset($allCategories1[0]['category_name']) and trim($allCategories1[0]['category_name']) != "") ? trim($allCategories1[0]['category_name']) : "";
                $descriptionCategory1 = (isset($allCategories1[0]['category_description']) and trim($allCategories1[0]['category_description']) != "") ? trim($allCategories1[0]['category_description']) : "";

                $catArr['category_id'] = $this->decrypt($categoryid);
                $categoryArr = $category->getCategoriesArr($catArr);
                $allCategories2 = $this->_categoryTable->getCategories($categoryArr);
                $titleCategory2 = (isset($allCategories2[0]['category_name']) and trim($allCategories2[0]['category_name']) != "") ? trim($allCategories2[0]['category_name']) : "";
                $descriptionCategory2 = (isset($allCategories2[0]['category_description']) and trim($allCategories2[0]['category_description']) != "") ? trim($allCategories2[0]['category_description']) : "";
            } else {
                $catArr = array();
                $catArr['category_id'] = $this->decrypt($categoryid);
                $categoryArr = $category->getCategoriesArr($catArr);
                $allCategories2 = $this->_categoryTable->getCategories($categoryArr);
                $titleCategory1 = (isset($allCategories2[0]['category_name']) and trim($allCategories2[0]['category_name']) != "") ? trim($allCategories2[0]['category_name']) : "";
                $descriptionCategory1 = (isset($allCategories2[0]['category_description']) and trim($allCategories2[0]['category_description']) != "") ? trim($allCategories2[0]['category_description']) : "";
            }
            // 2 
        }

        $discount = '';
        if (!empty($this->_auth->getIdentity()->user_id)) {

            $user_id = $this->_auth->getIdentity()->user_id;
            $userArr = array('user_id' => $user_id);
            $contactDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserMemberShipDetail($userArr);
            // asd($contactDetail);
            if ($contactDetail['membership_id'] != '1') {
                $discount = $contactDetail['membership_discount'];
            }
        }
        */

        $searchProductParam['available'] = "";
        $searchProductParam['product_id'] = 9;
        $searchProductParam['restricted_category_id'] = $this->_config['custom_frame_id'];
        $searchProductParam['productType'] = 3;
        $searchResult = $this->_productTable->getShopProduct($searchProductParam);
        $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
        
        $this->getReservationTable(); 
        
        $cartParam['id'] = "";
        $cartParam['user_session_id'] = session_id();
        $cartParam['user_id'] = $userDetails->user_id;
        $cartDetails = $this->_reservationTable->getKioskCart($cartParam);
        $checkoutButton = true;
        if(!empty($cartDetails[0])){
            $checkoutButton = false;
        }   
        $nextAvailableTime = date('h:i A',strtotime("+30 minutes",strtotime(DATE_TIME_FORMAT)));
        $spot = 6; 
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => $productMsg,
            'productlist' => $searchResult,
            'messages' => $productMsg,
            'categoryid' => $categoryid,
            'discount' => $discount,
            'upload_file_path' => $this->_config['file_upload_path'],
            'countResult' => $countResult[0]->RecordCount,
            'record' => $limit * $page,
            'page' => $page,
            'titleCategory1' => $titleCategory1,
            'descriptionCategory1' => $descriptionCategory1,
            'titleCategory2' => $titleCategory2,
            'descriptionCategory2' => $descriptionCategory2,
            'moreCnt' => $moreCnt,
            'nextAvailableTime' => $nextAvailableTime,
            'checkoutButton' => $checkoutButton,

        ));
        return $viewModel;
    }

}
