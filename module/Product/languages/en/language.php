<?php

return array(
    "common" => array(
        "PRODUCT_HEADING" => "Product",
        "SEARCH_NAME_MSG" => "Please enter search name",
        "SAVE_SUCCESS_MSG" => "Search is saved successfully.",
        "DELETE_ALERT" => "Are you sure you want to delete this record?",
        "DELETE_PRODUCT_ALERT" => "Are you sure you want to delete this product?",
        "DELETE_CONFIRM" => "Record is deleted successfully.",
        "ERROR_SAVING_SEARCH" => "Error in saving the search.",
        "NO_RECORD_FOUND" => "No Record Found",
        "UPDATE_CONFIRM_MSG" => "Record details updated successfully!",
        "VIEW" => "View",
        "EDIT" => "Edit",
        "DELETE" => "Delete",
        "SAVED_SEARCH_DELETE_MSG" => "Saved search deleted successfully",
        "SELECT" => "Select",
        "DETAILS" => "Details",
        "EDIT" => "Edit",
        "CHANGE_LOG" => "Change Log",
        "ATTACH_ATTRIBUTE_SUCCESS_MESSAGE" => "Attributes added successfully !",
        "CHANGE_CONFIRM" => "Changes have been saved !",
        "EDIT_ATTRIBUTES" => "Edit Attributes",
        "EDIT_OPTIONS" => "Edit Options",
        "PRODUCT_OPTIONS_CONFIRMATION" => "Product options have been saved !",
        "TEXT_FIELD" => "Text Field",
        "SELECT_BOX" => "Select Box",
        "RADIO_BUTTONS" => "Radio Buttons",
        "CHECKBOX" => "Checkbox",
        "REMOVE" => "Remove",
        "NAME" => "Name",
        "LABEL" => "Label",
        "NO_OF_OPTIONS" => "No. Of Options",
        "REQUIRED" => "Required",
        "DISPLAY_TYPE" => "Display Type",
        "IMAGE" => "Image",
        "UPDATED_ON" => "Updated On",
        "STATUS" => "Status",
        "ACTION" => "Action(s)",
        "IMAGE" => "Image",
        "ORDER_UPDATED" => "Product Order Updated Successfully"
    )
    ,
    "create_category" => array(
        "L_PRODUCT" => "Product",
        "L_CATEGORY_INFORMATION" => "Category Information",
        "L_CAREGORY_NAME" => "Category Name",
        "L_DESCRIPTION" => " Description",
        "L_RELATION" => "Relations",
        "L_PARENT_CATEGORY" => "Parent Category",
        "L_DASHBOARD" => "Dashboard",
        "L_MANAGE_CATEGORY" => "Manage Category",
        "L_DONATION" => "Donations",
        "L_SYMBOL" => ">>",
        "CATEGORY_NAME_EMPTY" => "Please enter category name",
        "CATEGORY_ALREADY_EXIST" => "Category name already exist",
        "DESCRIPTION_EMPTY" => "Please enter description",
        "VALID_DETAILS" => "Please enter valid details",
        "CATEGORY_CREATED_SUCCESS" => "Category is created successfully !",
        "CATEGORY_UPDATE_SUCCESS" => "Category is updated successfully !"
    ),
    "createAttribute" => array(
        "L_CREATE_ATTRIBUTE" => "Category Attribute",
        "L_MANAGE_ATTRIBUTE" => "Manage Attribute",
        "L_ATTRIBUTE_INFORMATION" => "Attributes Information",
        "L_LIST" => "List",
        "L_NAME" => "Name",
        "L_LABEL" => " Label",
        "L_HELP_TEXT" => "Help Text",
        "L_DISPLAY_TYPE" => "Display Type ",
        "L_MARK_AS_REQUIRED" => "Make this attribute required, forcing the contact to choose an option.",
        "L_DASHBOARD" => "Dashboard",
        "L_MANAGE_CATEGORY" => "Manage Category",
        "L_DONATION" => "Donations",
        "L_SYMBOL" => ">>",
        "L_PRODUCT" => "Product",
        "NAME_EMPTY" => "Please enter name",
        "LABEL_EMPTY" => "Please enter label",
        "DISPLAY_TEXT_EMPTY" => "Please select display text",
        "VALID_DETAILS" => "Please enter valid text",
        "NAME_MSG" => "Please enter a valid name",
        "LABEL_MSG" => "Please enter a valid label",
        "DISPLAY_TYPE_MSG" => "Please select a display type",
        "ATTRIBUTE_ADDED" => "Attribute added successfully",
        "ERROR_ATTRIBUTE_MSG" => "Error adding attribute",
        "HEADER_TEXT" => "Manage the Attributes of products with adding new attributes , adjust existing attributes , defining its options, display type, etc. If you do not find the attribute you seek, you can <a href='/create-product-attribute'> Create New Attribute</a>. ",
        "NO_RECORD_FOUND" => "No Record Found",
        "DELETE_CONF_MSG" => "Are you sure you want to delete this attribute ?",
        "DELETE_MSG" => "Attribute deleted successfully ",
        "ERROR_DELETE_MSG" => "Error in deleting attribute",
        "ATTRIBUTE_UPDATED" => "Attribute updated successfully",
        "ERROR_ATTRIBUTE_UPDATE_MSG" => "Error updating attribute",
    ),
    "createproduct" => array(
        "L_PRODUCT" => "Product",
        "L_PRODUCT_INFO" => "Product Information",
        "L_PRODUCT_TYPE" => "Product Type",
        "L_PRODUCT_CATEGORY" => "Select Category",
        "L_NAME" => "Name",
        "L_SKU" => "SKU",
        "L_DESCRIPTION" => "Description",
        "L_CATEGORY_INFO" => "Category Information",
        "L_CATEGORY" => "Category",
        "L_DETAILED_INFO" => "Detailed Information",
        "L_LIST_PRICE" => "List Price ($)",
        "L_COST" => "Cost ($)",
        "L_SELL_PRICE" => "Sell Price ($)",
        "L_PRODUCT_NAME" => "Product Name",
        "L_RELATED_PRODUCT" => "Related Product(s)",
        "L_LENGTH" => "Length",
        "L_WIDTH" => "Width",
        "L_UNIT" => "Unit",
        "L_HEIGHT" => "Height",
        "L_WEIGHT" => "Weight",
        "L_SELECT_PRODUCT" => "Select Product",
        "L_ADD_NEW_FILE" => "Product Image",
        "L_AVAILABLE" => "Available",
        "L_STATUS" => "Status",
        "L_MEMBERSHIP_DISCOUNT" => "Membership Discount",
        "L_FEATURED" => "Featured",
        "L_IN_STOCK" => "In Stock",
        "L_THRESHOLD" => "Threshold",
        "L_IS_TAXABLE" => "Taxable",
        "L_CAP_IS_TAXABLE" => "Taxable",
        "L_REFUNDABLE" => "Refundable",
        "L_PROMOTIONAL" => "Promotional",
        "L_ADDITIONAL_PRODUCT" => "Additional Product(s)",
        "L_DUPLICATE_CERTIFICATE" => "Duplicate Certificate",
        "L_BIO_CERTIFICATE" => "Bio Certificate",
        "L_PERSONALIZED_PANEL" => "Personalized Panel",
        "PRODUCT_TYPE_EMPTY" => "Please Select product type",
        "NAME_EMPTY" => "Please enter name",
        "SKU_EMPTY" => "Please enter sku",
        "VALID_DETAILS" => "Please enter a valid detail",
        "DESCRIPTION_EMPTY" => "Please enter description",
        "SHIPPING_DESCRIPTION_EMPTY" => "Please enter shipping description",
        "AVAILABLE_EMPTY" => "Please select available",
        "SELLEIN_PRICE_EMPTY" => "Please enter sell price",
        "L_FILE_SIZE_EXCEEDS" => "The uploaded file exceeds the post_max_size directive in php.ini",
        "L_FILE_BIG" => "File is too big",
        "L_FILE_SMALL" => "File is too small",
        "L_FILE_TYPE" => "File type not allowed",
        "CREATE_PRODUCT_FAIL" => "",
        "CREATE_PRODUCT_SUCCESS" => "Product has been created successfully.",
        "UPDATE_PRODUCT_SUCCESS" => "Product has been updated successfully.",
        "L_UPLOADED_IMAGE" => "Uploaded Image",
        "IMG_DEFAULT_EMPTY" => "Please select default image.",
        "OPTIONS" => "Options",
        "DEFAULT" => "Default",
        "COST" => "Cost",
        "PRICE" => "Price",
        "ATTRIBUTE" => "Attribute",
        "ADD_ATTRIBUTE" => "Add Attribute",
        "ADD_ATTRIBUTE_OPTION" => "Please add attributes to this product to attach the options.",
        "ATTRIBUTE_LABLE_MSG" => "Add attributes to this product using the add attributes form. You may then adjust the settings for these attributes on this page and go on to configure their options in the Options tab.",
        "LIST_PRICE_VALID" => "Please enter valid list price",
        "COST_PRICE_VALID" => "Please enter valid cost price",
        "WEIGHT_VALID" => "Please enter valid weight",
        "WIDTH_VALID" => "Please enter valid width",
        "LENGTH_VALID" => "Please enter valid length",
        "HEIGHT_VALID" => "Please enter valid height",
        "IN_STOCK_VALID" => "Please enter valid in stock value",
        "THRESHOLD_VALID" => "Please enter valid threshold value",
        "SELL_PRICE_VALID" => "Please enter valid sell price",
        "SKU_VALID" => "Please enter valid SKU",
        "DESCRIPTION_VALID" => "Please enter valid description",
        "PRODUCT_NAME_VALID" => "Please enter valid product name",
        "IS_PICKUP" => "Pickup",
        "L_STOCK" => "Stock",
        "L_SHIPPING_DESCRIPTION" => "Shipping Description",
        "L_GL_CODE_INFO" => "GL Code Information",
        "L_GL_CODE" => "GL Code",
        "L_PROG_CODE" => "Program Code",
        "L_DEPARTMENT" => "Department",
        "L_DESIGNATION" => "Designation",
        "L_ALLOCATION" => "Allocation",
        "L_RESTRICTED" => "Restricted",
        "IS_SHIPPABLE" => "Shippable",
        "IS_SHIPPABLE_FREE" => "Shipping Cost",
        "IS_DONATION" => "Donation",
    ),
    "category_list" => array(
        "L_PRODUCT" => "Product",
        "L_PRODUCT_CATEGORY" => "Select Category",
        "L_NAME" => "Name",
        "L_OPERATIONS" => "Operations",
        "L_ADD_CATEGORY" => "Add Category",
        "CATEGORY_EMPTY" => "Please select category.",
        "DELETE_CATEGORY_ALERT" => "Are you sure you want to delete this category ?",
        "CATEGORIES_DELETE_SUCCESS" => "Categories is successfully deleted !",
        "CATEGORY_DELETE_SUCCESS" => "Category is successfully deleted !",
        "CATEGORY_UPDATE_SUCCESS" => "Category is updated successfully !"
    ),
    "search_product" => array(
        "SEARCH_SAVE_CONFIRM" => "Product search details saved successfully!",
        "SEARCH_UPDATE_CONFIRM" => "Product search details updated successfully!",
        "L_HEADING" => "Product",
        "L_HEADING_CONTENT" => "Search Products by filling the following filters, or just do a blank search for the full Products list. If you do not find the product you seek, you can ",
        "L_HEADING_CONTENT2" => "Create New Product.",
        "L_HEADING_CONTENT3" => " Also, you can save the search criteria for future use by naming it and clicking 'Save', as well as retrieve saved searches by selecting from the list.",
        "L_SEARCH_PRODUCT" => "Search Product",
        "L_Product_Type" => "Product Type",
        "L_PRODUCT_CATEGORY" => "Select Category",
        "L_Name" => "Name",
        "L_SKU" => "SKU",
        "L_CATEGORY" => "Category",
        "L_SELL_PRICE" => "Sell Price ($)",
        "L_PROMOTIONAL" => "Promotional",
        "L_FEATURED" => "Featured",
        "L_STATUS" => "Status",
        "L_AVAILABLE" => "Available",
        "L_SEARCH" => "Search",
        "L_SAVED_SEARCHES" => "Saved Searches",
        "L_SAVE_SEARCH_AS" => "Save Search As",
        "L_SEARCH_RESULTS" => "Search Results",
        "L_CREATE_PRODUCT_MSG" => "Below are the records that match the search criteria you entered. If you do not find the contact you seek, try",
        "L_CREATE_NEW_PRODUCT" => "Create New Product",
        "L_TO" => "-to-",
        "SELL_PRICE" => "Sell Price",
        "IN_STOCK" => "In Stock",
        "L_FROM" => "From",
        "IN_STOCK_WEB" => "In Stock (Web)",
        "IN_STOCK_ELLIS" => "In Stock (Ellis Island)"
    ),
    "createAttributeOption" => array(
        "ADD_OPTION" => "Add Option",
        "NAME" => "Name",
        "HEADER_TEXT" => "This name will appear to contacts on product add to cart forms.",
        "DEFAUL_ADJUSTMENTS" => "Default Adjustments",
        "DEFAULT_TEXT" => "Enter a positive or negative value for each adjustment applied when this option is selected. Any of these may be overridden at the product level. ",
        "COST" => "Cost ($)",
        "PRICE" => "Price ($)",
        "NAME_MSG" => "Please enter a valid name",
        "L_CREATE_ATTRIBUTE" => "Category Attribute",
        "L_MANAGE_ATTRIBUTE" => "Manage Attribute",
        "L_ATTRIBUTE_INFORMATION" => "Attributes Information",
        "L_LIST" => "List",
        "L_NAME" => "Name",
        "L_LABEL" => " Label",
        "L_HELP_TEXT" => "Help Text",
        "L_DISPLAY_TYPE" => "Display Type ",
        "L_MARK_AS_REQUIRED" => "Make this attribute required, forcing the contact to choose an option.",
        "L_DASHBOARD" => "Dashboard",
        "L_MANAGE_CATEGORY" => "Manage Category",
        "L_DONATION" => "Donations",
        "L_SYMBOL" => ">>",
        "L_PRODUCT" => "Product",
        "NAME_EMPTY" => "Please enter name",
        "LABEL_EMPTY" => "Please enter label",
        "DISPLAY_TEXT_EMPTY" => "Please select display text",
        "VALID_DETAILS" => "Please enter valid details.",
        "NAME_MSG" => "Please enter a valid name",
        "LABEL_MSG" => "Please enter a valid label",
        "DISPLAY_TYPE_MSG" => "Please enter a valid display type",
        "ATTRIBUTE_OPTION_ADDED" => "Attribute option added successfully",
        "ATTRIBUTE_OPTION_UPDATED" => "Attribute option updated successfully",
        "ERROR_ATTRIBUTE_UPDATED" => "Error updating attribiute option",
        "ERROR_ATTRIBUTE_OPTION_MSG" => "Error adding attribute option",
        "HEADER_TEXT" => "Manage the Options of attribute with adding new options, adjust existing options etc. If you do not find the options you seek, you can <a href='/create-product-attribute-option/",
        "HEADER_TEXT1" => "'>Create Attribute Options</a>",
        "NO_RECORD_FOUND" => "No Record Found",
        "DELETE_CONF_MSG" => "Are you sure you want to delete this attribute option ?",
        "DELETE_MSG" => "Attribute option deleted successfully ",
        "ERROR_DELETE_MSG" => "Error in deleting attribute option",
        "ATTRIBUTE_UPDATED" => "Attribute option updated successfully",
        "ERROR_ATTRIBUTE_UPDATE_MSG" => "Error updating attribute option",
        "COST_MSG" => "Please enter a valid cost price",
        "PRICE_MSG" => "Please enter a valid price",
    ),
);
?>