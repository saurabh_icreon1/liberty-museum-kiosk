<?php

/**
 * This page is used for group module configuration details.
 * @package    Group
 * @author     Icreon Tech - DT
 */
return array(
    'controllers' => array(
        'invokables' => array(
            'Group\Controller\Group' => 'Group\Controller\GroupController'
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'creatGroup' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-group[/][:group_id]',
                    'constraints' => array(
                        'id' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Group\Controller\Group',
                        'action' => 'createGroup',
                    ),
                ),
            ),
            'editGroup' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-group',
                    'defaults' => array(
                        'controller' => 'Group\Controller\Group',
                        'action' => 'editGroup',
                    ),
                ),
            ),
            'deleteGroup' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-group',
                    'defaults' => array(
                        'controller' => 'Group\Controller\Group',
                        'action' => 'deleteGroup',
                    ),
                ),
            ),
            'searchGroups' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/groups',
                    'defaults' => array(
                        'controller' => 'Group\Controller\Group',
                        'action' => 'getGroup',
                    ),
                ),
            ),
            'searchContactsGroup' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/search-contacts-group',
                    'defaults' => array(
                        'controller' => 'Group\Controller\Group',
                        'action' => 'searchContactsGroup',
                    ),
                ),
            ),
            'getContactSearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-contact-search',
                    'defaults' => array(
                        'controller' => 'Group\Controller\Group',
                        'action' => 'getContactSearch',
                    ),
                ),
            ),
            'addUserToGroup' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-user-to-group',
                    'defaults' => array(
                        'controller' => 'Group\Controller\Group',
                        'action' => 'addUserToGroup',
                    ),
                ),
            ),
            'groupDetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/group-detail/:group_id[/][:mode]',
                    'defaults' => array(
                        'controller' => 'Group\Controller\Group',
                        'action' => 'groupDetail',
                    ),
                ),
            ),
            'addGroupToArchive' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-group-to-archive',
                    'defaults' => array(
                        'controller' => 'Group\Controller\Group',
                        'action' => 'addGroupToArchive',
                    ),
                ),
            ),
            'getGroupContact' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/group-contact/:group_id',
                    'defaults' => array(
                        'controller' => 'Group\Controller\Group',
                        'action' => 'getGroupContact',
                    ),
                ),
            ),
           'getCampaignAutoSuggest' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-campaign-auto-suggest',
                    'defaults' => array(
                            'controller' => 'Group\Controller\Group',
                            'action' => 'getCampaignAutoSuggest',
                    ),
                ),
            ),
            'deleteUserToGroup' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/remove-user-from-group',
                    'defaults' => array(
                        'controller' => 'Group\Controller\Group',
                        'action' => 'deleteUserToGroup',
                    ),
                ),
            ),
            'viewGroupDetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-group-detail/:group_id',
                    'defaults' => array(
                        'controller' => 'Group\Controller\Group',
                        'action' => 'viewGroupDetail',
                    ),
                ),
            ),
            'editGroupDetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-group-detail/:group_id',
                    'defaults' => array(
                        'controller' => 'Group\Controller\Group',
                        'action' => 'editGroupDetail',
                    ),
                ),
            ),
            'getGroupContactsDetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-group-contacts-detail/:group_id',
                    'defaults' => array(
                        'controller' => 'Group\Controller\Group',
                        'action' => 'getGroupContactsDetail',
                    ),
                ),
            ),
            'getGroups' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-groups',
                    'defaults' => array(
                        'controller' => 'Group\Controller\Group',
                        'action' => 'getGroups',
                    ),
                ),
            ),

        )
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'navigation' => array(
        'default' => array(
            array(
                'label' => 'Group',
                'route' => 'searchGroups',
                'pages' => array(
                    array(
                        'label' => 'Search',
                        'route' => 'searchGroups',
                        'pages' => array(
                            array(
                                'label' => 'Create',
                                'route' => 'creatGroup',
                                'action' => 'createGroup',
                            ),
                            array(
                                'label' => 'View',
                                'route' => 'viewGroupDetail',
                                'action' => 'viewGroupDetail',
                            ),
                            array(
                                'label' => 'Edit',
                                'route' => 'editGroupDetail',
                                'action' => 'editGroupDetail',
                            ),
                            array(
                                'label' => 'Contact(s)',
                                'route' => 'getGroupContactsDetail',
                                'action' => 'getGroupContactsDetail',
                            ),
                            array(
                                'label' => 'Change Log',
                                'route' => 'groupchangelog',
                                'action' => 'change-log'
                            ),
                            array(
                                'label' => 'Add to Group',
                                'route' => 'searchContactsGroup',
                                'action' => 'searchContactsGroup'
                            ),
                        ),
                    ),
                )
            )
        ),
    ),
    'group_messages' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
            'navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'group' => __DIR__ . '/../view'
        ),
        'template_map' => array(
            'layout/custom' => __DIR__ . '/../../module/Common/layout/crm.phtml',
            'error/404' => __DIR__ . '/../../module/Common/error/404.phtml',
            'error/index' => __DIR__ . '/../../module/Common/error/index.phtml',
        ),
    ),
    'global_config' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
);
