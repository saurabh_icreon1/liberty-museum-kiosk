<?php

/**
 * This file is used for error messages and labels message on view pages for group module
 * @package    Group
 * @author     Icreon Tech - DT
 */
//ticket 73 private group: add L_PRIVATE
return array(
    'search_group' => array(
        "L_NAME" => "Name",
        "L_GROUP_TYPE" => "Group Type",
        "L_INCLUDE_ARCHIVED" => "Include Archived",
        "L_SEARCH" => "Search",
        "L_SAVED_SEARCHES" => "Saved Searches",
        "L_SAVE_SEARCH_AS" => "Save Search As ",
        "L_SAVE" => "Save",
        "NO_RECORD_FOUND" => "No Record Found"
    ),
    'create_group' => array(
        "L_DASHBOARD" => "Dashboard",
        "L_GROUP_STATUS" => "Status",
        "L_GROUP_MEMBERSHIP_LEVEL" => "Membership Level",
        "L_GROUP_TRANSACTION_STATUS" => "Transaction Status",
        "L_GROUP_ITEM_STATUS" => "Item Status",
        "L_CREATE" => "Create",
        "L_LIST" => "List",
        "L_GROUP_TYPE" => "Group Type",
        "L_SYMBOL" => ">>",
        "L_GROUP_INFORMATION" => "Group Information",
        "L_NAME" => "Name",
        "L_DESCRIPTION" => "Description",
        "L_CAMPAIGN" => "Campaign",
        "L_TRANSACTION_AMOUNT_RANGE" => "Transaction Amount Range",
        "L_FROM" => "From",
        "L_TO" => "To",
        "L_DOLLER" => "$",
        "L_TRANSACTION_AMOUNT_DATE" => "Transaction Amount Date",
        "L_TRANSACTION_DATE_RANGE" => "Date Range",
        "L_TRANSACTION_CHK" => "Transaction",
        "L_SIGNUP_CHK" => "Registered",
        "L_WEB_CHK" => "Web",
        "L_ELLIS_CHK" => "Ellis",
        "L_ELLIS_VISIT" => "Ellis Visit",
        "L_STATE" => "State",
        "L_CITY" => "City",
        "L_COUNTRY" => "Country",
        "L_PRODUCT_TYPE" => "Product Type",
        "L_ASSOCATION" => "Association",
        "L_RELATIONSHIP" => "Relationship",
        "L_BULK_MAILING" => "Bulk Mailing",
        "L_NO_EMAIL_ADDRESS" => "Don&apos;t have Email Address",
        "L_CONTACT_TYPE" => "Contact Type",
        "L_PRIVACY" => "Privacy",
        "L_EXCLUSIVE_GROUPS" => "Group(s)",
        "L_EXCLUSIVE_TAGS" => "Tag(s)",
        "L_ADD_CONTACT_TO_GROUP_BASED_ONSURVEY_ANS" => "Add Contacts to Group based on Survey answers. Select Contatcts who:",
        "L_HAVE_FOLLOWING_KNOWLEDGE_ABOUT_SOLEIF" => "Have the following selected knowledge about The Statue of Liberty - Ellis  Island Foundation:",
        "L_EI_PART_OF_MY_FAMILY_STORY_AND_FOUNDATION_KEEPS_STORY_ALIVE" => "Ellis Island is part of my own Family’s story and the Foundation keeps that story alive",
        "L_ARE_YOU_DESCENDED_FROM_IMMIGRATION" => "Are you descended from immigrants who arrived through Ellis Island?",
        "L_ARE_YOU_AN_IMMIGRANT" => "Are you yourself an Immigrant?",
        "L_DID_YOU_FIND_YOUR_ANCESTORS_RECORD_ON_EI" => "Did you find your ancestors records on Ellis Island?",
        "L_WHAT_YOU_KNOW_ABOUT_SOLELF" => "What do you know about The Statue of Liberty-Ellis Island Foundation?",
        "L_VISITED_EI" => "Visited on Ellis Island :",
        "L_WANT_TO_LEARN_MORE" => "Want to learn more about:",
        "L_RECOMMEND_BY" => "Are recommended to our website by :",
        "L_INTERESTED_ID" => "Are interested in :",
        "L_SUPPORT" => "Support:",
        "L_SURVEY" => "Survey",
        "L_GROUPS" => "Groups",
        "L_ADD_TO_GROUP" => "Add to Group",
        "L_ADMINISTRATORS" => "Administrators",
        "L_NAME_AND_EMAIL" => "Name or Email",
        "L_IN_GROUP" => "In Group",
        "L_WITH_TAG" => "With Tag",
        "L_SEARCH_GROUP" => "Search Group",
        "L_NAME" => "Name",
        "L_INCLUDE_ARCHIEVE" => "Include Archived",
        "L_ADD_TO_GROUP_ADMINISTRATION" => "Add to Group : Administrators",
        "L_SURVEY_QUESTIONS" => "Survey Questions",
        "L_FIND_CONTACT_TO_ADD_THIS_GROUP" => "Find Contacts to Add to this group",
        "L_SEARCH_RESULTS" => "Search Results",
        "L_VIEW" => "Views",
        "L_EDIT" => "Edit",
        "L_CONTACT_IN_GROUP" => "Contacts in Group : ",
        "L_ADD_TO_ARCHIEVE" => "Add to Archive",
        "L_DETAILS" => "Details",
        "L_CONTACTS" => "Contacts",
        "L_CHANGE_LOG" => "Change Log",
        "L_ARE_YOU_SURE_GROUP_DELETE" => "Are you sure you want to delete this Group?",
        "L_CONFIRM_ARCHIVE" => "Are you sure you want to add this Group to Archive?",
        "L_CONFIRM_REMOVE_CONTACTS" => "Are you sure you want to remove multiple Contact(s)?",
        "L_CONFIRM_REMOVE_CONTACT" => "Are you sure you want to remove this Contact?",
        "L_REMOVE_SELECTED_CONTACTS" => "REMOVE SELECTED CONTACTS",
        "L_SELECT_CONTACTS" => "Please select at least one contact.",
        
        "GROUP_DATE_RANGE_EMPTY" => "Please check Transaction / Registered / Web.",
        "GROUP_CHOOSE_DATE_RANGE_EMPTY" => "Please select date range from drop down.",
        "GROUP_ENTER_DATE_EMPTY" => "Please enter at least one date.",
        
        "GROUP_NAME_EMPTY" => "Please enter name.",
        "GROUP_DESCRIPTION_EMPTY" => "Please enter description.",
        "GROUP_DESCRIPTION_VALID" => "Please enter valid details.",
        "GROUP_NAME_VALID" => "Please enter a valid name",
        "GROUP_NAME_ALREADY_EXIST" => "Group name already exist.",
        "GROUP_CREATED_SUCCESSFULLY" => "Group created successfully.",
        "GROUP_UPDATED_SUCCESSFULLY" => "Group Updated Successfully!",
        "CONTACTS_TO_GROUP" => "Contacts to Group:",
        "ADDED_SUCCESSFULLY" => "added successfully!",
        "UPDATED_SUCCESSFULLY" => "updated successfully!",
        "CONTACT_REMOVED" => "Contact removed successfully !",
        "CONTACTS_REMOVED" => "Contact(s) removed successfully !",
        "GROUP_DELETED_SUCCESSFULLY" => "Group deleted successfully !",
        "NO_RECORD_FOUND" => "No Record Found",
        "L_PHONE" => "Phone",
        "L_NAME_OR_EMAIL" => "Name/Company or Email",
        "L_PRODUCTS" => "Products",
        "L_EXCLUDE" => "Exclude",
        "L_PRIVATE" => "Is Private",
    ),
);
?>