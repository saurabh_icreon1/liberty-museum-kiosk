<?php
/**
 * This page is used for group module autoload class at the time of intilization of module.
 * @package    Group
 * @author     Icreon Tech - DT
 */
return array(
    'Group\Module'                       => __DIR__ . '/Module.php',
    'Group\Controller\GroupController'   => __DIR__ . '/src/Group/Controller/GroupController.php',
    'Group\Model\Group'            => __DIR__ . '/src/Group/Model/Group.php',
    'Group\Model\GroupTable'                   => __DIR__ . '/src/Group/Model/GroupTable.php',
    'Group\Model\AuthStorage'                   => __DIR__ . '/src/Group/Model/AuthStorage.php',
);
?>