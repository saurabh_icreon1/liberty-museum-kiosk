<?php

/**
 * This file is used for groups module
 * @category   Zend
 * @package    Group_GroupController
 * @author     Icreon Tech - DT
 */

namespace Group\Controller;

use Base\Controller\BaseController;
use Zend\View\Model\ViewModel;
use Group\Form\CreategroupForm;
use Group\Form\EditgroupForm;
use Group\Form\SearchgroupForm;
use Group\Form\SearchContactGroupForm;
use Group\Model\Group;
use Common\Model\Common;
use Zend\Http\Request;
use Zend\Authentication\AuthenticationService;

/**
 * This controller is used groups module
 * @category   Zend
 * @package    Group_GroupController
 * @author     Icreon Tech - DT
 */
class GroupController extends BaseController {

    protected $_groupTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_flashMessage = null;

    function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        parent::__construct();
        $auth = new AuthenticationService();
        $this->auth = $auth;
    }

    /**
     * This function is used to get table and config file object
     * @return     array
     * @param array $form
     * @author Icreon Tech - DT
     */
    public function getGroupTable() {
        $this->checkUserAuthentication();
        if (!$this->_groupTable) {
            $sm = $this->getServiceLocator();
            $this->_groupTable = $sm->get('Group\Model\GroupTable');
            $this->_translator = $sm->get('translator');
            $this->_config = $sm->get('Config');
            $this->_adapter = $sm->get('dbAdapter');
            $this->_auth = new AuthenticationService();
        }
        return $this->_groupTable;
    }

    /**
     * This action is used for create new group.
     * @return     array
     * @author Icreon Tech - DT
     */
    public function createGroupAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $form = new CreategroupForm();
        // $form = new SurveyForm();
        $this->getGroupTable();
        $createMessages = $this->_config['group_messages']['config']['create_group'];

        $request = $this->getRequest();

        $group = new Group($this->_adapter);
        $viewModel = new ViewModel();

        $response = $this->getResponse();

        $params = $this->params()->fromRoute();
        $searchQuery = "''";
        $surveyQuery = '';
        $groupCampaignData = array();
        if (isset($params['group_id']) && $params['group_id'] != '') {
            $groupId = $this->decrypt($params['group_id']);
            $groupArr = array();
            $groupArr['group_id'] = $groupId;
            $groupData = $this->getGroupTable()->getGroupById($groupArr);
            
            if(isset($groupData['search_query']) and trim($groupData['search_query']) != "") {
                parse_str($groupData['search_query'], $search_query_array);
                if(isset($search_query_array['campaign']) and trim($search_query_array['campaign']) != "") {
                  $groupCampaignData = $this->getGroupTable()->getCampaignGroupSearch(array('campaigns_ids' => trim($search_query_array['campaign'])));
                }
             }
  
            $groupDataForm = array();
            $groupDataForm['group_id'] = $groupData['group_id'];
            $groupDataForm['group_name'] = $groupData['name'];
            $groupDataForm['group_description'] = $groupData['description'];
            $groupDataForm['group_type'] = $groupData['group_type'];
            $groupDataForm['group_type_display'] = $groupData['group_type'];
            $this->setValueForElement($groupDataForm, $form);
            if ($groupData['search_query'] != '') {
                parse_str($groupData['search_query'], $searchQuery);
                $searchQuery = json_encode($searchQuery);
            }
            if ($groupData['survey_query'] != '') {
                $surveyQuery = $groupData['survey_query'];
            }
        }

        $surveyQuestions = $this->getServiceLocator()->get('Common\Model\SurveyTable')->getSurveyQuestions();
        $this->addSurveyFormElementsToForm($form, $surveyQuestions);

        /* 
        $common = new Common($this->_adapter);
        $compaignSearchDataArray = $common->getCompaignDataArr(array());
        $compaignDataArray = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCampaign($compaignSearchDataArray);
        $campaigns = array();
        foreach ($compaignDataArray as $key => $val) {
            $campaigns[$val['campaign_id']] = $val['campaign_title'];
        }
        $form->get('campaign')->setAttribute('options', $campaigns);
        */
        
        $getProducts = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProducts();
        $products = array();
        foreach ($getProducts as $key => $val) {
            $products[$val['product_type_id']] = $val['product_type'];
        }
        $form->get('product_type')->setAttribute('options', $products);


        $getUserGroups = $this->getServiceLocator()->get('Common\Model\CommonTable')->getGroups();
        $userGroup = array();
        foreach ($getUserGroups as $key => $val) {
            $userGroup[$val['group_id']] = $val['name'];
        }
        $form->get('exclude_group')->setAttribute('options', $userGroup);

        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
        $countryList = array();
        foreach ($country as $key => $val) {
            $countryList[$val['country_id']] = $val['name'];
        }
        $form->get('country')->setAttribute('options', $countryList);

        $states = $this->getServiceLocator()->get('Common\Model\CommonTable')->getStates();
        $stateList = array();
        foreach ($states as $key => $val) {
            $stateList[$val['state']] = $val['state'] . " - " . $val['state_code'];
        }
        $form->get('state')->setAttribute('options', $stateList);

        $getRelations = $this->getServiceLocator()->get('Common\Model\CommonTable')->getRelations();
        $relations = array();
        foreach ($getRelations as $key => $val) {
            $relations[$val['relationship_id']] = $val['relationship'];
        }
        $form->get('relationship')->setAttribute('options', $relations);


        $getTransactionRange = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTransactionRange();
        $transactionRange = array("" => "Select One");
        foreach ($getTransactionRange as $key => $val) {
            $transactionRange[$val['range_id']] = $val['range'];
        }
        $form->get('transaction_amount_date')->setAttribute('options', $transactionRange);
        //$form->get('transaction_amount_date')->setValue("5");


        $getUserType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getType();
        $userType = array();
        $userType[''] = 'Select Type';
        foreach ($getUserType as $key => $val) {
            $userType[$val['user_type_id']] = $val['user_type'];
        }
        $form->get('contact_type')->setAttribute('options', $userType);


        $getTag = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTags();
        $userTag = array();
        foreach ($getTag as $key => $val) {
            $userTag[$val['tag_id']] = $val['tag_name'];
        }
        $form->get('exclude_tags')->setAttribute('options', $userTag);

        $crmProducts = $this->getServiceLocator()->get('Product\Model\ProductTable')->getCrmProducts(array());
        $productResult = array();
        if ($crmProducts !== false) {
            foreach ($crmProducts as $id => $product) {
                $productResult[$product['product_id']] = $product['product_name'];
            }
        }
        $form->get('product_ids')->setAttribute('options', $productResult);

        
        // new - additions - start
        $getMembership = $this->getServiceLocator()->get('Common\Model\CommonTable')->getMembership();
        $membership_level = array();
       // $membership_level[''] = 'Select';
        foreach ($getMembership as $key => $value) {
                $membership_level[$value['membership_id']] = $value['membership_title'];
        }
        $form->get('membership_title')->setAttribute('options', $membership_level);
        
        $getTransactionStatus = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTransactionStatus();
        $transactionStatus = array();
        $transactionStatus[''] = 'Any';
        if ($getTransactionStatus !== false) {
           foreach ($getTransactionStatus as $key => $val) {
              $transactionStatus[$val['transaction_status_id']] = $val['transaction_status'];
           }
        }
        $form->get('transaction_status_id')->setAttribute('options', $transactionStatus);
        
        $getProductStatus = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductStatus();
        $productStatus = array();
        $productStatus[''] = 'Any';
        if ($getTransactionStatus !== false) {
            foreach ($getProductStatus as $key => $val) {
              $productStatus[$val['product_status_id']] = $val['product_status'];
            }
        }
        $form->get('item_status')->setAttribute('options', $productStatus);
        // new - additions - end
        
        $viewModel->setVariables(array(
            'form' => $form,
            'survey_questions' => $surveyQuestions,
            'jsLangTranslate' => $createMessages,
            'searchQuery' => $searchQuery,
            'groupCampaignData' => $groupCampaignData,
            'surveyQuery' => $surveyQuery,
                )
        );
        return $viewModel;
    }

    
      
    /**
     * This action is used getCampaignAutoSuggest
     * @return     void
     * @param  Object form,array survey questions
     * @author Icreon Tech - NS
     */
    public function getCampaignAutoSuggestAction() {
       $this->checkUserAuthentication();
       $this->getGroupTable();
           
        $common = new Common($this->_adapter);
        $response = $this->getResponse();
        $request = $this->getRequest();
        $post_arr = $request->getPost()->toArray();
        $all_campaign = array();
        $i = 0; 
        $campaign_title = (isset($post_arr['campaign_title']) and trim($post_arr['campaign_title']) != "") ? trim($post_arr['campaign_title']) : ""; //(isset($post_arr['campaign_title']) and trim($post_arr['campaign_title']) != "") ? trim($post_arr['campaign_title']) : "";
        if($campaign_title != "") {
            $compaignDataArray = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCampaignAutoSuggest(array('campaign_title'=>$campaign_title));
            if(isset($compaignDataArray) and count($compaignDataArray) > 0) {
              foreach ($compaignDataArray as $val) {
                 if(isset($val['campaign_title']) and trim($val['campaign_title']) != "" and isset($val['campaign_id']) and trim($val['campaign_id']) != "") {
                      $all_campaign[$i] = array();
                      $all_campaign[$i]['id'] = $val['campaign_id'];
                      $all_campaign[$i]['text'] = $val['campaign_title'];
                      $i++;
                 } 
               }
            }
        }
        
        $response->setContent(\Zend\Json\Json::encode($all_campaign));
        return $response;
    }
    
    
    
    /**
     * This action is used for add survey form element to form
     * @return     void
     * @param  Object form,array survey questions
     * @author Icreon Tech - DT
     */
    public function addSurveyFormElementsToForm($form, $surveyQuestions, $inputType = 'checkbox') {
        foreach ($surveyQuestions as $questions) {
            $sectionId = $questions['survey_section_id'];
            $elementName = 'survey_question_' . $questions['survey_question_id'];
            $value = $questions['survey_question_id'];
            if ($inputType != 'hidden') {
                if ($questions['section_type'] == 1) {
                    $form->add(array(
                        'type' => 'Zend\Form\Element\Checkbox',
                        'options' => array(
                            'checked_value' => $value,
                            'unchecked_value' => '0',
                        ),
                        'name' => $elementName,
                        'attributes' => array(
                            'id' => $elementName,
                            'class' => 'e2 checkbox save-survey-param'
                        )
                    ));
                } else {
                    $form->add(array(
                        'type' => 'Zend\Form\Element\Checkbox',
                        'options' => array(
                            'checked_value' => $value,
                            'unchecked_value' => '0',
                        ),
                        'name' => $elementName,
                        'attributes' => array(
                            'id' => $elementName,
                            'class' => 'e2 dsfcheckbox save-survey-param'
                        )
                    ));
                }
            } else {
                $form->add(array(
                    'type' => 'hidden',
                    'name' => $elementName,
                    'attributes' => array(
                        'id' => $elementName
                    )
                ));
            }
        }
        //echo "<pre>";print_r($form);die;
    }

    /**
     * This action is used for searching of group
     * @return     array
     * @author Icreon Tech - DT
     */
    public function getGroupAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $this->getGroupTable();
        $searchGroupMessage = $this->_config['group_messages']['config']['create_group'];
        if (($request->isXmlHttpRequest() && $request->isPost()) || $request->getPost('export') == 'excel') {

            $group = new Group($this->_adapter);

            $response = $this->getResponse();

            $postArray = $request->getPost()->toArray();
            parse_str($request->getPost('searchString'), $searchParam);
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit; // do not put $limit*($page - 1)
            $isExport = $request->getPost('export');
            if ($isExport != "" && $isExport == "excel") {
                $chunksize = $this->_config['export']['chunk_size'];
                $export_limit = $this->_config['export']['export_limit'];

                $searchParam['record_limit'] = $chunksize;
            }
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            $page_counter = 1;
            do {
                $seachResult = $this->_groupTable->getAllGroup($searchParam);
                $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
                $total = $countResult[0]->RecordCount;
                $jsonResult = array();
                $arrCell = array();
                $subArrCell = array();

                if ($isExport == "excel") {
                    if ($total > $chunksize) {
                        $number_of_pages = ceil($total / $chunksize);
                        $page_counter++;
                        $start = $chunksize * $page_counter - $chunksize;
                        $searchParam['start_index'] = $start;
                        $searchParam['page'] = $page_counter;
                    } else {
                        $number_of_pages = 1;
                        $page_counter++;
                    }
                } else {
                    $page_counter = $page;
                    $number_of_pages = ceil($total / $limit);
                    $jsonResult['records'] = $countResult[0]->RecordCount;
                    $jsonResult['page'] = $request->getPost('page');
                    $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
                }
                if (!empty($seachResult)) {
                    $arrExport = array();
                    foreach ($seachResult as $val) {
                        $arrCell['id'] = $val['group_id'];
                        $encryptId = $this->encrypt($val['group_id']);
                        $viewLink = '<a href="/group-detail/' . $encryptId . '/' . $this->encrypt('view') . '" class="view-icon"><div class="tooltip">View<span></span></div></a>';
                        $editLink = '<a href="/group-detail/' . $encryptId . '/' . $this->encrypt('edit') . '" class="edit-icon"><div class="tooltip">Edit<span></span></div></a>';
                        $deleteLink = '<a onclick="deleteGroup(\'' . $encryptId . '\')" href="#delete_group_content" class="delete_group delete-icon"><div class="tooltip">Delete<span></span></div></a>';
                        $createGroupLink='';
                        if($val['group_type']==1)
                        {
                            $createGroupLink = '<a href="/create-group/' . $encryptId . '" class="add-icon"><div class="tooltip">Create Static Group<span></span></div></a>';
                        }
                        $actions = $viewLink . $editLink . $deleteLink . $createGroupLink;
                        $groupName = '<span class="left">' . $this->chunkData($val['name'], 120) . "</span>";
                        if ($val['is_archived'] == 1) {
                            $groupName.= '<span class="archive"></span>';
                        }
                        $groupType = 'Static';
                        if ($val['group_type'] == 2) {
                            $groupType = 'Dynamic';
                        }
                        if ($isExport == "excel") {
                            $arrExport[] = array('Name' => $this->chunkData($val['name'], 120), 'Type' => $groupType, 'Description' => $this->chunkData($val['description'], 100), 'Created Date' => $this->OutputDateFormat($val['added_date'], 'dateformatampm'), 'Updated Date' => $this->OutputDateFormat($val['modified_date'], 'dateformatampm'));
                        } else {

                            $arrCell['cell'] = array($groupName, $groupType, $this->chunkData($val['description'], 100), $this->OutputDateFormat($val['added_date'], 'dateformatampm'), $this->OutputDateFormat($val['modified_date'], 'dateformatampm'), $actions);
                            $subArrCell[] = $arrCell;
                            $jsonResult['rows'] = $subArrCell;
                        }
                    }
                    if ($isExport == "excel") {
                        $filename = "group_export_" . date("Y-m-d") . ".csv";
                        $this->arrayToCsv($arrExport, $filename, $page_counter);
                    }
                } else {
                    $jsonResult['rows'] = array();
                }
            } while ($page_counter <= $number_of_pages && $isExport == "excel");
            if ($isExport == "excel") {
                $this->downloadSendHeaders("group_export_" . date("Y-m-d") . ".csv");
                die;
            } else {
                $response->setContent(\Zend\Json\Json::encode($jsonResult));
                return $response;
            }
        } else {
            $this->layout('crm');
            $form = new SearchgroupForm();
            $this->getGroupTable();
            $messages = array();

            if ($this->_flashMessage->hasMessages()) {
                $messages = $this->_flashMessage->getMessages();
            }
            $group = new Group($this->_adapter);
            $viewModel = new ViewModel();

            $response = $this->getResponse();
            $viewModel->setVariables(array(
                'form' => $form,
                'messages' => $messages,
                'jsLangTranslate' => $searchGroupMessage)
            );
            return $viewModel;
        }
    }

    /**
     * This action is used to update the group
     * @return     array
     * @author Icreon Tech - DT
     */
    public function editGroupAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        if ($request->isPost() && $request->isXmlHttpRequest()) {

            if ($request->getPost('group_id') != '') {
                $form = new EditgroupForm();

                $postArray = $request->getPost()->toArray();
				$this->getGroupTable();

                $createMessages = $this->_config['group_messages']['config']['create_group'];

                $group = new Group($this->_adapter);

                $response = $this->getResponse();

                if($request->isPost('edit_group')){
                        $form->setInputFilter($group->getInputFilterEditSaveGroup());
                }else{
                        $form->setInputFilter($group->getInputFilterEditGroup());
                } 
                //ticket 73 private group
                $FormData = $request->getPost();
                if(!isset($FormData['is_private']))
                {
                    $FormData['is_private'] = 0;
                }
                $form->setData($FormData);
                if (!$form->isValid()) {
                    $errors = $form->getMessages();
                    $msg = array();
                    foreach ($errors as $key => $row) {
                        if (!empty($row) && $key != 'continue') {
                            foreach ($row as $rower) {
                                $msg [$key] = $createMessages[$rower];
                            }
                        }
                    }
                    $messages = array('status' => "error", 'message' => $msg);
                }
                if (!empty($messages)) {
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                } else {

                    $group->exchangeArrayCreateGroup($postArray);

                    $groupId = $this->getGroupTable()->saveGroup($group);
                    /** insert into the change log */
                    $changeLogArray = array();
                    $changeLogArray['table_name'] = 'tbl_grp_change_logs';
                    $changeLogArray['activity'] = '2';/** 1 == for insert */
                    $changeLogArray['id'] = $groupId;
                    $changeLogArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                    $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                    $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                    /** end insert into the change log */
                    $this->flashMessenger()->addMessage($createMessages['GROUP_UPDATED_SUCCESSFULLY']);

                    $messages = array('status' => "success", 'message' => $createMessages['GROUP_UPDATED_SUCCESSFULLY']);

                    $response->setContent(\Zend\Json\Json::encode($messages));

                    return $response;
                }
            } else {
                return $this->redirect()->toRoute('searchGroups');
            }
        } else {
            return $this->redirect()->toRoute('searchGroups');
        }
    }

    /**
     * This action is used to search contact on base of some filtering and listing them
     * @return     array
     * @author Icreon Tech - DT
     */
    public function searchContactsGroupAction() {
		ini_set('max_execution_time',0);
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        if (!$request->isPost()) {
            return $this->redirect()->toRoute('creatGroup');
        } else {
            $this->layout('crm');
            $form = new SearchContactGroupForm();
            $this->getGroupTable();
            $createMessages = $this->_config['group_messages']['config']['create_group'];
            $group = new Group($this->_adapter);
            $viewModel = new ViewModel();

            $response = $this->getResponse();
            $searchFilters = $request->getPost();
            $searchFilters['campaign'] = (isset($searchFilters['campaign']) and trim($searchFilters['campaign']) != "") ? explode(",",trim($searchFilters['campaign'])) : "";
            //print_r($searchFilters); die();

            if (isset($searchFilters['transaction_amount_date']) && $searchFilters['transaction_amount_date'] != 1 && $searchFilters['transaction_amount_date'] != '') {
                $dateRange = $this->getDateRange($searchFilters['transaction_amount_date']);
                $searchFilters['transaction_amount_date_from'] = $dateRange['from'];
                $searchFilters['transaction_amount_date_to'] = $dateRange['to'];
            }
            $searchType = 'survey';
            if ($request->getPost('searchcontacts')) {
                $searchType = 'searchcontacts';
            }
            $form->add(array(
                'name' => 'searchtype',
                'attributes' => array(
                    'type' => 'hidden',
                    'value' => $searchType
                )
            ));
            /* Survey question form */
            $surveyQuestions = $this->getServiceLocator()->get('Common\Model\SurveyTable')->getSurveyQuestions();
            $surveySearchIds = '';
            if ($searchType == 'survey') {
                $surveySearchIds = ''; //survey ids which need to search
                foreach ($surveyQuestions as $questions) {
                    $elementId = 'survey_question_' . $questions['survey_question_id'];
                    $elementValue = $request->getPost($elementId);
                    if ($elementValue != 0) {
                        $surveySearchIds.=$questions['survey_question_id'] . ",";
                    }
                }
                $surveySearchIds = rtrim($surveySearchIds, ',');
            }

            $form->add(array(
                'name' => 'surveyids',
                'attributes' => array(
                    'type' => 'hidden',
                    'id' => 'surveyids',
                    'value' => $surveySearchIds
                )
            ));

            $getUserGroups = $this->getServiceLocator()->get('Common\Model\CommonTable')->getGroups();
            $userGroup = array();
            foreach ($getUserGroups as $key => $val) {
                $userGroup[$val['group_id']] = $val['name'];
            }
            $form->get('in_group_name')->setAttribute('options', $userGroup);

            $getTag = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTags();
            $userTag = array();
            foreach ($getTag as $key => $val) {
                $userTag[$val['tag_id']] = $val['tag_name'];
            }
            $form->get('tags')->setAttribute('options', $userTag);
            //$this->addSurveyFormElementsToForm($form,$surveyQuestions,'hidden');

            /* Survey question form end */
            $searchFilters = $this->setValueForElement($searchFilters, $form);
            
            //ticket 73 private group
            if(isset($searchFilters['is_private']) && $searchFilters['is_private'] == 1)
            {
                $form->get('is_private')->setAttribute('value', 1);
            }
            else
            {
               $form->get('is_private')->setAttribute('value', 0);
            }

            /* Get userids */
            $searchParam = array();
            $searchParam['group_id'] = $request->getPost('group_id');
            //$groupContactSearchArray = $group->getArrayForSearchCotactsGroup($searchParam);


            if (isset($searchFilters['transaction_amount_date_from']) and trim($searchFilters['transaction_amount_date_from']) != "") {
                $TransactionAmountDateFrom = trim($searchFilters['transaction_amount_date_from']);
                $searchFilters['transaction_amount_date_from'] = $this->DateFormat($TransactionAmountDateFrom, 'db_date_format_from');
                
            }


            if (isset($searchFilters['transaction_amount_date_to']) and trim($searchFilters['transaction_amount_date_to']) != "") {
                $TransactionAmountDateTo = trim($searchFilters['transaction_amount_date_to']);
                $searchFilters['transaction_amount_date_to'] = $this->DateFormat($TransactionAmountDateTo, 'db_date_format_to');
            }
            
            $groupContactSearchArray = $group->getArrayForSearchCotactsGroup($searchFilters);
            // $seachResult = $this->_groupTable->getContactSearchCreateGroup($groupContactSearchArray);
            $seachResult = $this->_groupTable->getContactSearch($groupContactSearchArray);
            
            
            $selectedUserIds = '';
            if (!empty($seachResult)) {
                foreach ($seachResult as $userResult) {
                    if ($selectedUserIds == '') {
                        $selectedUserIds = $userResult->user_id;
                    } else {
                        $selectedUserIds.=',' . $userResult->user_id;
                    }
                }
            }

            $userIdsExcluded = '';
            $excludedContact = $this->getGroupTable()->getExcludedContactInGroup(array('groupId' => $request->getPost('group_id')));
            if (!empty($excludedContact)) {
                $userIds = array();
                foreach ($excludedContact as $val) {
                    $userIds[] = $val['user_id'];
                }
                $userIdsExcluded = implode(",", $userIds);
            }
            /* End Get userids */
            $viewModel->setVariables(array(
                'form' => $form,
                'survey_questions' => $surveyQuestions,
                'jsLangTranslate' => $createMessages,
                'userIdsNotInGroup' => $selectedUserIds,
                'userIdsExcluded' => $userIdsExcluded
                    )
            );
            return $viewModel;
        }
    }

    /**
     * This Action is used for the contact search for create group
     * @param this will pass an array $searchParam
     * @return this will be json format
     * @author Icreon Tech -DT
     */
    public function getContactSearchAction() {
		ini_set('max_execution_time',0);
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->getGroupTable();
            $response = $this->getResponse();
            $group = new Group($this->_adapter);
            parse_str($request->getPost('searchString'), $searchParam);
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit; // do not put $limit*($page - 1)
            $searchParam['startIndex'] = $start;
            $searchParam['recordLimit'] = $limit;
            $searchParam['sortField'] = $request->getPost('sidx');
            $searchParam['sortOrder'] = $request->getPost('sord');

            if (isset($searchParam['transaction_amount_date_from']) and trim($searchParam['transaction_amount_date_from']) != "") {
                $TransactionAmountDateFrom = trim($searchParam['transaction_amount_date_from']);
                $searchParam['transaction_amount_date_from'] = $this->DateFormat($TransactionAmountDateFrom, 'db_date_format_from');
                
            }

            if (isset($searchParam['transaction_amount_date_to']) and trim($searchParam['transaction_amount_date_to']) != "") {
                $TransactionAmountDateTo = trim($searchParam['transaction_amount_date_to']);
                $searchParam['transaction_amount_date_to'] = $this->DateFormat($TransactionAmountDateTo, 'db_date_format_to');
            }
            
            $groupContactSearchArray = $group->getArrayForSearchCotactsGroup($searchParam);
            
            $seachResult = $this->_groupTable->getContactSearch($groupContactSearchArray);
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $jsonResult['page'] = $request->getPost('page');
            $jsonResult['records'] = $countResult[0]->RecordCount;
            $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);

            foreach ($seachResult as $val) {
                $arrCell['id'] = $val->user_id;
                $userName = (trim($val->full_name) == '') ? $val->company_name : $val->full_name;
                $arrCell['cell'] = array($userName, $val->email_id, $val->membership, $val->country_name, $val->source_name, $val->user_type);
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
            return $response->setContent(\Zend\Json\Json::encode($jsonResult));
        }
    }

    /**
     * This Action is used to save user to group 
     * @param this will pass an array of users are group data
     * @return this will be json format
     * @author Icreon Tech -DT
     */
    public function addUserToGroupAction() {
		ini_set('max_execution_time',0);
        $this->checkUserAuthentication();
        $request = $this->getRequest();

        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $this->getGroupTable();
            $createMessages = $this->_config['group_messages']['config']['create_group'];

            $group = new Group($this->_adapter);

            $response = $this->getResponse();

            $postArray = $request->getPost()->toArray();
            $group->exchangeArrayCreateGroup($postArray);
            $groupId = $this->getGroupTable()->saveGroup($group);
            
            if ($group->group_type == 2) {
                $this->getGroupTable()->deleteExcludeContactToGroup($group);
            }
            $group->group_id = $groupId;
            if (isset($postArray['user_ids']) && !empty($postArray['user_ids'])) {
                $user_id_arr = explode(',', $postArray['user_ids']);
                foreach ($user_id_arr as $user_id) {
                    $group->user_id = $user_id;
                    if ($group->group_type == 2) {
                        $groupId = $this->getGroupTable()->excludeContactToGroup($group);
                    } else {
                        $groupId = $this->getGroupTable()->saveContactToGroup($group);
                    }
                }
            }
            /** insert into the change log */
            $changeLogArray = array();
            $changeLogArray['table_name'] = 'tbl_grp_change_logs';
            $changeLogArray['activity'] = (isset($group->group_id)) ? '2' : '1';/** 1 == for insert */
            $changeLogArray['id'] = $groupId;
            $changeLogArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
            $changeLogArray['added_date'] = DATE_TIME_FORMAT;
            $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
            /** end insert into the change log */
            if (isset($group->group_id) && ($group->group_id != '')) {
                $this->flashMessenger()->addMessage($createMessages['CONTACTS_TO_GROUP'] . " " . $group->group_name . " " . $createMessages['UPDATED_SUCCESSFULLY']);
                $messages = array('status' => "success", 'message' => $createMessages['CONTACTS_TO_GROUP'] . " " . $group->group_name . " " . $createMessages['UPDATED_SUCCESSFULLY']);
            } else {
                $this->flashMessenger()->addMessage($createMessages['CONTACTS_TO_GROUP'] . " " . $group->group_name . " " . $createMessages['ADDED_SUCCESSFULLY']);
                $messages = array('status' => "success", 'message' => $createMessages['CONTACTS_TO_GROUP'] . " " . $group->group_name . " " . $createMessages['ADDED_SUCCESSFULLY']);
            }

            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        } else {
            return $this->redirect()->toRoute('creatGroup');
        }
    }

    /**
     * This action is used to show all group detail
     * @return     array
     * @author Icreon Tech - DT
     */
    public function groupDetailAction() {
        $this->checkUserAuthentication();
        $params = $this->params()->fromRoute();
        if (isset($params['group_id']) && $params['group_id'] != '') {
            $groupId = $this->decrypt($params['group_id']);
            $groupArr = array();
            $groupArr['group_id'] = $groupId;
            $groupData = $this->getGroupTable()->getGroupById($groupArr);
            if (!empty($groupData)) { //print_r($groupData);
                $request = $this->getRequest();
                $this->layout('crm');
                $this->getGroupTable();
                $createMessages = $this->_config['group_messages']['config']['create_group'];
                $group = new Group($this->_adapter);
                $viewModel = new ViewModel();

                $mode = (isset($params['mode'])) ? $this->decrypt($params['mode']) : 'view';
                $viewModel->setVariables(array(
                    'group_data' => $groupData,
                    'module_name' => $this->encrypt('group'),
                    'encrypt_group_id' => $params['group_id'],
                    'mode' => $mode,
                    'jsLangTranslate' => $createMessages
                        )
                );
                return $viewModel;
            } else {
                return $this->redirect()->toRoute('searchGroups');
            }
        } else {
            return $this->redirect()->toRoute('searchGroups');
        }
    }

    /**
     * This Action is used to add group to archive
     * @param string encrypted group id
     * @return this will be json format
     * @author Icreon Tech -DT
     */
    public function addGroupToArchiveAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();

        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $postArray = $request->getPost()->toArray();
            if (isset($postArray['group_id']) && $postArray['group_id'] != '') {
                $groupId = $this->decrypt($postArray['group_id']);
                $postArray['group_id'] = $groupId;
                $groupArr = array();
                $groupArr['group_id'] = $groupId;
                $this->getGroupTable();

                $createMessages = $this->_config['group_messages']['config']['create_group'];

                $group = new Group($this->_adapter);

                $response = $this->getResponse();

                $group->exchangeArrayCreateGroup($postArray);

                $groupId = $this->getGroupTable()->saveGroup($group);

                $this->flashMessenger()->addMessage($createMessages['GROUP_UPDATED_SUCCESSFULLY']);
                $messages = array('status' => "success", 'message' => $createMessages['GROUP_UPDATED_SUCCESSFULLY']);

                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                return $this->redirect()->toRoute('creatGroup');
            }
        } else {
            return $this->redirect()->toRoute('creatGroup');
        }
    }

    /**
     * This Action is used to set post array elements value from contact search to add user to group page
     * @param this will pass an array $searchParam
     * @return this will be json format
     * @author Icreon Tech -DT
     */
    public function setValueForElement($postArr = array(), $form = array()) {
        $returnPostArr = array();
        if (!is_array($postArr)) {
            $postArr = $postArr->toArray();
        }
        $elements = $form->getElements();
        foreach ($elements as $elementName => $elementObject) {
            if (array_key_exists($elementName, $postArr)) {
                if (is_array($postArr[$elementName])) {
                    $value = implode(',', $postArr[$elementName]);
                } else {
                    $value = $postArr[$elementName];
                }
                $returnPostArr[$elementName] = $value;
                $elementObject->setValue($value);
            }
        }
        return $returnPostArr;
    }

    /**
     * This action is used to get all contacts of group in grid
     * @param  string encrypted group id
     * @return     json format string
     * @author Icreon Tech - DT
     */
    public function getGroupContactAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $params = $this->params()->fromRoute();
            if (isset($params['group_id']) && $params['group_id'] != '') {
                $groupId = $this->decrypt($params['group_id']);
                $this->getGroupTable();
                $createMessages = $this->_config['group_messages']['config']['create_group'];
                $group = new Group($this->_adapter);

                $response = $this->getResponse();
                /* Contact in group grid data */
                parse_str($request->getPost('searchString'), $searchParam);
                $limit = $request->getPost('rows');
                $page = $request->getPost('page');
                $start = $limit * $page - $limit; // do not put $limit*($page - 1)
                $searchParam['start_index'] = $start;
                $searchParam['record_limit'] = $limit;
                $searchParam['sort_field'] = $request->getPost('sidx');
                $searchParam['sort_order'] = $request->getPost('sord');
                $searchParam['group_id'] = $groupId;
                $groupArrForContacts = $group->getArrayForCotacts($searchParam);
                $groupContactsData = $this->getGroupTable()->getContactsByGroupId($groupArrForContacts);
                $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
                $jsonResult = array();
                $arrCell = array();
                $subArrCell = array();
                $limit = 10;
                $jsonResult['page'] = $request->getPost('page');
                $jsonResult['records'] = $countResult[0]->RecordCount;
                $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
                foreach ($groupContactsData as $val) {
                    $val = (object) $val;
                    $arrCell['id'] = $val->user_id;
                    $userName = (trim($val->full_name) == '') ? $val->company_name : $val->full_name;
                    $removeLink = '<a href="#remove_selected_contact_block" onclick="removeContactToGroup(' . $groupId . ',' . $val->user_id . ')" class="delete-icon remove_selected_contacts"><div class="tooltip">Remove<span></span></div></a>';
                    $arrCell['cell'] = array($userName, $val->email_id, $val->membership, $val->country_name, $val->source_name, $val->user_type, $removeLink);
                    $subArrCell[] = $arrCell;
                    $jsonResult['rows'] = $subArrCell;
                }
                $response->setContent(\Zend\Json\Json::encode($jsonResult));
                return $response;
            } else {
                return $this->redirect()->toRoute('searchGroups');
            }
        } else {
            return $this->redirect()->toRoute('searchGroups');
        }
    }

    /**
     * This action is used to delete user from group
     * @param  int group_id, user_ids comma separated
     * @return     json format string
     * @author Icreon Tech - DT
     */
    public function deleteUserToGroupAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $groupId = $request->getPost('group_id');
            $userIds = $request->getPost('user_ids');
            if ($groupId != '' && $userIds != '' && !empty($userIds)) {
                $this->getGroupTable();
                $createMessages = $this->_config['group_messages']['config']['create_group'];
                $group = new Group($this->_adapter);

                $response = $this->getResponse();
                /* Contact in group grid data */
				
                $groupArrForContacts = $group->getArrayForRemoveCotacts($request->getPost());
				//asd($groupArrForContacts);
                $this->getGroupTable()->removeContactsByGroupId($groupArrForContacts);
                $contactRemoveMsg = (strpos(implode(",",$userIds), ',')) ? $createMessages['CONTACTS_REMOVED'] : $createMessages['CONTACT_REMOVED'];
                $this->flashMessenger()->addMessage($contactRemoveMsg);
                $messages = array('status' => "success", 'message' => $contactRemoveMsg);

                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                return $this->redirect()->toRoute('searchGroups');
            }
        } else {
            return $this->redirect()->toRoute('searchGroups');
        }
    }

    /**
     * This action is used to delete group
     * @param  string group_id encrypted
     * @return     json format string
     * @author Icreon Tech - DT
     */
    public function deleteGroupAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $groupId = $request->getPost('group_id');
            if ($groupId != '') {
                $groupId = $this->decrypt($groupId);
                //ticket 73 private group
                $groupData = $this->getGroupTable()->getGroupById(array('group_id'=>$groupId));
                if (!empty($groupData)) {
                    $postArr = $request->getPost();
                    $postArr['group_id'] = $groupId;
                    $this->getGroupTable();
                    $createMessages = $this->_config['group_messages']['config']['create_group'];
                    $group = new Group($this->_adapter);

                    $response = $this->getResponse();
                    /* Contact in group grid data */

                    $groupArr = $group->getArrayForRemoveGroup($request->getPost());
                    $this->getGroupTable()->removeGroupById($groupArr);
                    /** insert into the change log */
                    $changeLogArray = array();
                    $changeLogArray['table_name'] = 'tbl_grp_change_logs';
                    $changeLogArray['activity'] = '3';/** 1 == for insert */
                    $changeLogArray['id'] = $groupId;
                    $changeLogArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                    $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                    $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                    /** end insert into the change log */
                    $this->flashMessenger()->addMessage($createMessages['GROUP_DELETED_SUCCESSFULLY']);
                    $messages = array('status' => "success", 'message' => $createMessages['GROUP_DELETED_SUCCESSFULLY']);

                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                } //ticket 73 private group
                else 
                {
                    $response = $this->getResponse();
                    $messages = array('status' => "error", 'message' => 'Group Not found');
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                }
            } else {
                return $this->redirect()->toRoute('searchGroups');
            }
        } else {
            return $this->redirect()->toRoute('searchGroups');
        }
    }

    /**
     * This action is used to show view group detail on ajax call
     * @return     array
     * @author Icreon Tech - DT
     */
    public function viewGroupDetailAction() {
        $this->checkUserAuthentication();
        $this->getGroupTable();
        $createGroupMessages = $this->_config['group_messages']['config']['create_group'];
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();
        if (isset($params['group_id']) && $params['group_id'] != '') {
            $groupId = $this->decrypt($params['group_id']);
            $groupArr = array();
            $groupArr['group_id'] = $groupId;
            $groupData = $this->getGroupTable()->getGroupById($groupArr);
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => $createGroupMessages,
                'encrypt_group_id' => $params['group_id'],
                'group_data' => $groupData,
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to get edit group detail page
     * @return string
     * @param void
     * @author Icreon Tech - DT
     */
    public function editGroupDetailAction() {
        $this->checkUserAuthentication();
        $params = $this->params()->fromRoute();
        $groupCampaignData = array();
        if (isset($params['group_id']) && $params['group_id'] != '') {
            $groupId = $this->decrypt($params['group_id']);
            $groupArr = array();
            $groupArr['group_id'] = $groupId;
            $groupData = $this->getGroupTable()->getGroupById($groupArr);
            if (!empty($groupData)) {
                if(isset($groupData['search_query']) and trim($groupData['search_query']) != "") {
                  parse_str($groupData['search_query'], $search_query_array);
                  if(isset($search_query_array['campaign']) and trim($search_query_array['campaign']) != "") {
                    $groupCampaignData = $this->getGroupTable()->getCampaignGroupSearch(array('campaigns_ids' => trim($search_query_array['campaign'])));
                  }
                }
                
                $request = $this->getRequest();
                $this->layout('crm');
                $form = new EditgroupForm();
                $groupDataForm = array();
                $groupDataForm['group_id'] = $groupData['group_id'];
                $groupDataForm['group_name'] = $groupData['name'];
                $groupDataForm['group_description'] = $groupData['description'];
                $groupDataForm['group_type'] = $groupData['group_type'];
                $groupDataForm['group_type_display'] = $groupData['group_type'];
                $this->setValueForElement($groupDataForm, $form);
                $this->getGroupTable();
                //ticket 73 private group
                if(isset($groupData['is_private']) && $groupData['is_private'] == 1)
                {
                    $form->get('is_private')->setAttribute('value', 1);
                    $form->get('is_private')->setChecked(true);
                }
                else
                {
                   $form->get('is_private')->setAttribute('value', 0);
                   $form->get('is_private')->setChecked(false); 
                }
                
                $createMessages = $this->_config['group_messages']['config']['create_group'];
                $group = new Group($this->_adapter);
                $viewModel = new ViewModel();

                $response = $this->getResponse();
                /* Survey question form */
                $surveyQuestions = $this->getServiceLocator()->get('Common\Model\SurveyTable')->getSurveyQuestions();
                $this->addSurveyFormElementsToForm($form, $surveyQuestions);
                /* Survey question form end */


                $common = new Common($this->_adapter);
                $compaignSearchDataArray = $common->getCompaignDataArr(array());
                $compaignDataArray = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCampaign($compaignSearchDataArray);
                $campaigns = array();
                foreach ($compaignDataArray as $key => $val) {
                    $campaigns[$val['campaign_id']] = $val['campaign_title'];
                }
                $form->get('campaign')->setAttribute('options', $campaigns);


                $getUserGroups = $this->getServiceLocator()->get('Common\Model\CommonTable')->getGroups();
                $userGroup = array();
                foreach ($getUserGroups as $key => $val) {
                    $userGroup[$val['group_id']] = $val['name'];
                }
                $form->get('exclude_group')->setAttribute('options', $userGroup);


                $getProducts = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProducts();
                $products = array();
                foreach ($getProducts as $key => $val) {
                    $products[$val['product_type_id']] = $val['product_type'];
                }
                $form->get('product_type')->setAttribute('options', $products);

                $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
                $countryList = array();
                foreach ($country as $key => $val) {
                    $countryList[$val['country_id']] = $val['name'];
                }
                $form->get('country')->setAttribute('options', $countryList);

                $states = $this->getServiceLocator()->get('Common\Model\CommonTable')->getStates();
                $stateList = array();
                foreach ($states as $key => $val) {
                    $stateList[$val['state']] = $val['state'];
                }
                $form->get('state')->setAttribute('options', $stateList);

                $getRelations = $this->getServiceLocator()->get('Common\Model\CommonTable')->getRelations();
                $relations = array();
                foreach ($getRelations as $key => $val) {
                    $relations[$val['relationship_id']] = $val['relationship'];
                }
                $form->get('relationship')->setAttribute('options', $relations);


                $getTransactionRange = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTransactionRange();
                $transactionRange = array("" => "Select One");
                foreach ($getTransactionRange as $key => $val) {
                    $transactionRange[$val['range_id']] = $val['range'];
                }
                $form->get('transaction_amount_date')->setAttribute('options', $transactionRange);


                $getUserType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getType();
                $userType = array();
                $userType[''] = 'Select Type';
                foreach ($getUserType as $key => $val) {
                    $userType[$val['user_type_id']] = $val['user_type'];
                }
                $form->get('contact_type')->setAttribute('options', $userType);


                $getTag = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTags();
                $userTag = array();
                foreach ($getTag as $key => $val) {
                    $userTag[$val['tag_id']] = $val['tag_name'];
                }
                $form->get('exclude_tags')->setAttribute('options', $userTag);
                $viewModel->setTerminal(true);
                if ($groupData['search_query'] != '') {
                    parse_str($groupData['search_query'], $searchQuery);
                    $searchQuery = json_encode($searchQuery);
                } else {
                    $searchQuery = "''";
                }

                if ($groupData['survey_query'] != '') {
                    $surveyQuery = $groupData['survey_query'];
                } else {
                    $surveyQuery = '';
                }

                $crmProducts = $this->getServiceLocator()->get('Product\Model\ProductTable')->getCrmProducts(array());
                $productResult = array();
                if ($crmProducts !== false) {
                    foreach ($crmProducts as $id => $product) {
                        $productResult[$product['product_id']] = $product['product_name'];
                    }
                }
                $form->get('product_ids')->setAttribute('options', $productResult);
                
                
                // new - additions - start
               $getMembership = $this->getServiceLocator()->get('Common\Model\CommonTable')->getMembership();
               $membership_level = array();
              // $membership_level[''] = 'Select';
               foreach ($getMembership as $key => $value) {
                       $membership_level[$value['membership_id']] = $value['membership_title'];
               }
               $form->get('membership_title')->setAttribute('options', $membership_level);

               $getTransactionStatus = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTransactionStatus();
               $transactionStatus = array();
               $transactionStatus[''] = 'Any';
               if ($getTransactionStatus !== false) {
                  foreach ($getTransactionStatus as $key => $val) {
                     $transactionStatus[$val['transaction_status_id']] = $val['transaction_status'];
                  }
               }
               $form->get('transaction_status_id')->setAttribute('options', $transactionStatus);

               $getProductStatus = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductStatus();
               $productStatus = array();
               $productStatus[''] = 'Any';
               if ($getTransactionStatus !== false) {
                   foreach ($getProductStatus as $key => $val) {
                     $productStatus[$val['product_status_id']] = $val['product_status'];
                   }
               }
               $form->get('item_status')->setAttribute('options', $productStatus);
               // new - additions - end
        
               
                $viewModel->setVariables(array(
                    'form' => $form,
                    'survey_questions' => $surveyQuestions,
                    'encrypt_group_id' => $params['group_id'],
                    'searchQuery' => $searchQuery,
                    'groupCampaignData' => $groupCampaignData,
                    'surveyQuery' => $surveyQuery,
                    'jsLangTranslate' => $createMessages
                ));
                return $viewModel;
            } else {
                return $this->redirect()->toRoute('searchGroups');
            }
        } else {
            return $this->redirect()->toRoute('searchGroups');
        }
    }

    /**
     * This action is used to show view group contacts detail on ajax call
     * @return     array
     * @author Icreon Tech - DT
     */
    public function getGroupContactsDetailAction() {
        $this->checkUserAuthentication();
        $this->getGroupTable();
        $createGroupMessages = $this->_config['group_messages']['config']['create_group'];
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();
        $response = $this->getRequest();
        $groupId = $params['group_id'];
        $groupId = $this->decrypt($groupId);
        $groupArr = array();
        $groupArr['group_id'] = $groupId;
        $group = new Group($this->_adapter);
        $groupData = $this->getGroupTable()->getGroupById($groupArr);
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            if ($groupData['group_type'] == 2) {
                if ($groupData['search_query'] != '' || $groupData['survey_query'] != '') {
                    parse_str($groupData['search_query'], $searchParam); 
                    $searchParam['transaction_amount_date_from'] = (isset($searchParam['transaction_amount_date_from']) and trim($searchParam['transaction_amount_date_from']) != "") ? date("Y-m-d",strtotime($searchParam['transaction_amount_date_from'])) : "";
                    $searchParam['transaction_amount_date_to'] = (isset($searchParam['transaction_amount_date_to']) and trim($searchParam['transaction_amount_date_to']) != "") ? date("Y-m-d",strtotime($searchParam['transaction_amount_date_to'])) : ""; 
                    $searchParam['userId_exclude'] = '';
                    $excludedContact = $this->getGroupTable()->getExcludedContactInGroup(array('groupId' => $groupId));
                    if (!empty($excludedContact)) {
                        $userIds = array();
                        foreach ($excludedContact as $val) {
                            $userIds[] = $val['user_id'];
                        }
                        $searchParam['userId_exclude'] = implode(",", $userIds);
                    }

                    $limit = $request->getPost('rows');
                    $page = $request->getPost('page');
                    $start = $limit * $page - $limit; // do not put $limit*($page - 1)
                    $searchParam['startIndex'] = $start;
                    $searchParam['recordLimit'] = $limit;
                    $searchParam['sortField'] = $request->getPost('sidx');
                    $searchParam['sortOrder'] = $request->getPost('sord');                 
                    $groupContactSearchArray = $group->getArrayForSearchCotactsGroup($searchParam);
                    $groupContactsData = $this->getGroupTable()->getContactSearch($groupContactSearchArray);
                }
            } else {
                $limit = $request->getPost('rows');
                $page = $request->getPost('page');
                $start = $limit * $page - $limit; // do not put $limit*($page - 1)
                $searchParam = array();
                $searchParam['group_id'] = $groupId;
                $searchParam['start_index'] = $start;
                $searchParam['record_limit'] = $limit;
                $searchParam['sort_field'] = $request->getPost('sidx');
                $searchParam['sort_order'] = $request->getPost('sord');
                $groupArrForContacts = $group->getArrayForCotacts($searchParam);
                $groupContactsData = $this->getGroupTable()->getContactsByGroupId($groupArrForContacts);
            }

            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $limit = 10;
            $jsonResult['page'] = $request->getPost('page');
            $jsonResult['records'] = $countResult[0]->RecordCount;
            $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
            foreach ($groupContactsData as $val) {
                $val = (object) $val;
                $arrCell['id'] = $val->user_id;
                $userName = (trim($val->full_name) == '') ? $val->company_name : $val->full_name;
                $arrCell['cell'] = array($userName, $val->email_id, $val->membership, $val->country_name, $val->source_name, $val->user_type);
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
            //$contactGridStr = \Zend\Json\Json::encode($jsonResult);
            //asd($jsonResult);
            echo \Zend\Json\Json::encode($jsonResult);
            die;
            //return $response->setContent(\Zend\Json\Json::encode($jsonResult));
        } else {
            //echo $contactGridStr;die;
            /* End contact in group grid data */
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);

            $viewModel->setVariables(array(
                'jsLangTranslate' => $createGroupMessages,
                //'contacts_grid_str' => $contactGridStr,
                // 'encrypt_group_id' => $params['group_id'],
                'group_data' => $groupData
            ));
            return $viewModel;
        }
    }

    /**
     * This function is used to get group lookup
     * @return     array
     * @param array $form
     * @author Icreon Tech - DT
     */
    public function getGroupsAction() {
        $this->checkUserAuthentication();
        $this->getGroupTable();
        $group = new Group($this->_adapter);
        $response = $this->getResponse();
        $request = $this->getRequest();
        $postData = $request->getPost()->toArray();
        $groupDataArray = $this->_groupTable->getAllGroup($postData);
        $groups = array();
        $i = 0;
        if ($groupDataArray !== false) {
            foreach ($groupDataArray as $compaign) {
                $groups[$i] = array();
                $groups[$i]['group_id'] = $compaign['group_id'];
                $groups[$i]['group_name'] = $compaign['name'];
                $i++;
            }
        }
        $response->setContent(\Zend\Json\Json::encode($groups));
        return $response;
    }

}
