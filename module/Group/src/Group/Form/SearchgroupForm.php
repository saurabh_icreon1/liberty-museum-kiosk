<?php

/**
 * This page is used for create Group.
 * @package    Group
 * @author     Icreon Tech - AP
 */

namespace Group\Form;

use Zend\Form\Form;
use Group\Form\SearchGroupForm;

/**
 * This form is used for create Group.
 * @package    Group_SearchgroupForm
 * @author     Icreon Tech - AP
 */
class SearchGroupForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('search_group');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'group_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'group_name'
            )
        ));
        
        $this->add(array(
            'type' => 'Select',
            'name' => 'group_type',
            'options' => array(
                'value_options' => array(
                    '' => 'Any',
                    '1' => 'Static',
                    '2' => 'Dynamic'
                ),
            ),
            'attributes' => array(
                'id' => 'group_type',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));

        $this->add(array(
            'name' => 'is_archived',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'type' => 'Zend\Form\Element\Checkbox',
            'attributes' => array(
                'id' => 'is_archived',
                'class' =>  'checkbox e2'
            )
        ));
        $this->add(array(
            'name' => 'searchgroupbutton',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'searchgroupbutton',
                'class' => 'search-btn right',
                'value' => 'SEARCH'
            )
        ));
    }

}

