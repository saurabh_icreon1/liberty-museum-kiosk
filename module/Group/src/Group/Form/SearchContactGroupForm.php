<?php

/**
 * This page is used for search contacts in group page.
 * @package    Group_SearchContactGroupForm
 * @author     Icreon Tech - AP
 */

namespace Group\Form;

use Zend\Form\Form;
use Group\Form\SearchContactGroupForm;

/**
 * This form is used for search contacts in group page.
 * @package    SearchContactGroupForm
 * @author     Icreon Tech - AP
 */
class SearchContactGroupForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('search-group');
        $this->setAttribute('method', 'post');
        /* Create group elements */
        $this->add(array(
            'name' => 'group_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'group_id'
            )
        ));

        $this->add(array(
            'name' => 'group_name',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'group_name'
            )
        ));

        $this->add(array(
            'name' => 'group_description',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'group_description',
            )
        ));
        $this->add(array(
            'name' => 'group_type',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'group_type',
            )
        ));
        /* End of create group elements */

        /* search contacts group elements */

        $this->add(array(
            'type' => 'hidden',
            'name' => 'campaign',
            'attributes' => array(
                'id' => 'campaign',
                'class' => 'save-search-param'
            )
        ));

        $this->add(array(
            'type' => 'hidden',
            'name' => 'transaction_amount_from',
            'attributes' => array(
                'id' => 'transaction_amount_from',
                'class' => 'save-search-param'
                
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'transaction_amount_to',
            'attributes' => array(
                'id' => 'transaction_amount_to',
                'class' => 'save-search-param'
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'transaction_amount_date',
            'attributes' => array(
                'id' => 'transaction_amount_date',
                'class' => 'save-search-param'
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'ellis_visit',
            'attributes' => array(
                'id' => 'ellis_visit',
                'class' => 'save-search-param'
            )
        ));
        $this->add(array(
            'name' => 'ellis_visit_date_from',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 'ellis_visit_date_from',
                'class' => 'save-search-param'
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'ellis_visit_date_to',
            'attributes' => array(
                'id' => 'ellis_visit_date_to',
                'class' => 'save-search-param'
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'transaction_amount_date_from',
            'attributes' => array(
                'id' => 'transaction_amount_date_from',
                'class' => 'save-search-param'
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'transaction_amount_date_to',
            'attributes' => array(
                'id' => 'transaction_amount_date_to',
                'class' => 'save-search-param'
            )
        ));
        $this->add(array(
            'name' => 'state',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 'state',                
                'class' => 'save-search-param'
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'city',
            'attributes' => array(
                'id' => 'city',                
                'class' => 'save-search-param'
            )
        ));
       $this->add(array(
            'type' => 'hidden',
            'name' => 'phone',
            'attributes' => array(
                'id' => 'phone',
                'class' => 'save-search-param'
            )
        ));
       $this->add(array(
	'type' => 'hidden',
	'name' => 'transaction_status',
	'attributes' => array(
		'id' => 'transaction_status',
                'class' => 'save-search-param'
	)
        ));

        $this->add(array(
                'type' => 'hidden',
                'name' => 'signup_status',
                'attributes' => array(
                        'id' => 'signup_status',
                    'class' => 'save-search-param'
                )
        ));

        $this->add(array(
                'type' => 'hidden',
                'name' => 'web_status',
                'attributes' => array(
                        'id' => 'web_status',
                    'class' => 'save-search-param'
                )
        ));
        
        $this->add(array(
                'type' => 'hidden',
                'name' => 'ellis_status',
                'attributes' => array(
                        'id' => 'ellis_status',
                        'value' => '0',
                        'class' => 'save-search-param'
                )
        ));
        
        $this->add(array(
            'type' => 'hidden',
            'name' => 'country',
            'attributes' => array(
                'id' => 'country',                
                'class' => 'save-search-param'
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'is_exclude_country',
            'attributes' => array(
                'id' => 'is_exclude_country',                
                'class' => 'save-search-param'
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'product_type',
            'attributes' => array(
                'id' => 'product_type',                
                'class' => 'save-search-param'
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'relationship',
            'attributes' => array(
                'id' => 'relationship',
                'class' => 'save-search-param'
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'assocation',
            'attributes' => array(
                'id' => 'assocation',
                'class' => 'save-search-param'
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'no_email_address',
            'attributes' => array(
                'id' => 'no_email_address',
                'value' => '0',
                'class' => 'save-search-param'
            ),
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'contact_type',
            'attributes' => array(
                'id' => 'contact_type',
                'class' => 'save-search-param'
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'exclude_group',
            'attributes' => array(
                'id' => 'exclude_group',
                'class' => 'save-search-param'
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'is_exclude_group',
            'attributes' => array(
                'id' => 'is_exclude_group',
                'class' => 'save-search-param'
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'exclude_tags',
            'attributes' => array(
                'id' => 'exclude_tags',
                'class' => 'save-search-param'
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'is_exclude_tags',
            'attributes' => array(
                'id' => 'is_exclude_tags',
                'class' => 'save-search-param'
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'privacy',
            'attributes' => array(
                'id' => 'privacy',
                'class' => 'save-search-param'
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'name_email_look',
            'attributes' => array(
                'id' => 'name_email_look',
                'class' => 'save-search-param'
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'name_email_look_id',
            'attributes' => array(
                'id' => 'name_email_look_id',
                'class' => 'save-search-param'
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'product_ids',
            'attributes' => array(
                'id' => 'product_ids',
                'class' => 'save-search-param'
            )
        ));
        /* end of search contacts group elements */

        $this->add(array(
            'name' => 'name_email',
            'attributes' => array(
                'type' => 'text',
                'id' => 'name_email',
                'class' => 'save-search-param'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'in_group_name',
            'attributes' => array(
                'id' => 'in_group_name',
                'multiple' => 'multiple',
                'class' => 'e1 select-w-320 save-search-param'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'tags',
            'attributes' => array(
                'id' => 'tags',
                'multiple' => 'multiple',
                'class' => 'e1 select-w-320 save-search-param',

            )
        ));
        $this->add(array(
            'name' => 'search_more_filter',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'search_more_filter',
                'class' => 'save-btn',
                'value' => 'SEARCH'
            )
        ));
        $this->add(array(
            'name' => 'addusertogroup',
            'attributes' => array(
                'type' => 'button',
                'id' => 'addusertogroup',
                'class' => 'save-btn',
                'value' => 'ADD CONTACTS TO GROUP'
            )
        ));
        
        // new - start
        $this->add(array(
            'type' => 'hidden',
            'name' => 'status',
            'attributes' => array(
                'id' => 'status',
                'class' => 'save-search-param'
             )
        ));

        $this->add(array(
            'type' => 'hidden',
            'name' => 'membership_title',
            'attributes' => array(
                'id' => 'membership_title',
                'class' => 'save-search-param'
            )
        ));

        $this->add(array(
            'type' => 'hidden',
            'name' => 'transaction_status_id',
            'attributes' => array(
                'id' => 'transaction_status_id',
                'class' => 'save-search-param'
             )
        ));

        $this->add(array(
            'type' => 'hidden',
            'name' => 'item_status',
            'attributes' => array(
                'id' => 'item_status',
                'class' => 'save-search-param'
             )
        )); 
        //ticket 73 private group
        $this->add(array(
            'type' => 'hidden',
            'name' => 'is_private',
            'attributes' => array(
                'id' => 'is_private',
                'value' => '0'
            ),
        ));
        // new - end
    }

}

