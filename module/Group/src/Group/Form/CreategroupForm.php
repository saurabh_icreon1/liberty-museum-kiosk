<?php

/**
 * This page is used for create Group form.
 * @package    Group_CreategroupForm
 * @author     Icreon Tech - AP
 */

namespace Group\Form;

use Zend\Form\Form;
use Group\Form\CreategroupForm;

/**
 * This form is used for create Group.
 * @package    CreategroupForm
 * @author     Icreon Tech - AP
 */
class CreategroupForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('group');
        $this->setAttribute('method', 'post');
        /* Create group form */
        $this->add(array(
            'name' => 'group_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'group_name'
            )
        ));

        $this->add(array(
            'name' => 'group_description',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'group_description',
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'group_type',
            'options' => array(
                'value_options' => array(
                    '1' => 'Static',
                    '2' => 'Dynamic',
                ),
            ),
            'attributes' => array(
                'class' => 'e3',
                'value' => '1', //set checked to '1'
                'tag' => 'div'
            )
        ));

        $this->add(array(
            'name' => 'continue',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'continue',
                'class' => 'save-btn',
                'value' => 'CONTINUE'
            )
        ));
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'id' => 'cancel',
                'class' => 'cancel-btn m-l-20',
                'value' => 'CANCEL',
                'onclick' => 'window.location.href="/groups"'
            )
        ));
        /* Create group form end */

        /* Search contacts form */
        

         /* Search contacts form */
       /* 
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'campaign',
            'attributes' => array(
                'id' => 'campaign',
                'class' => 'e1 select-w-320',
                'multiple' => 'multiple'
            )
        ));
        
         $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'campaign_look',
            'attributes' => array(
                'id' => 'campaign',
                'class' => 'e1 select-w-320',
                'multiple' => 'multiple'
            )
        ));
        */
        
         
       /* $this->add(array(
            'name' => 'campaign_look',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'campaign_look'
               // 'class' => 'search-icon'
            )
        ));    
*/
        
        
        $this->add(array(
            'name' => 'campaign',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'campaign',
            )
        ));
        
 /*         $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'campaign',
            'attributes' => array(
                'id' => 'campaign',
                'class' => 'e1 select-w-320',
                'multiple' => 'multiple'
            )
        ));
   */     
//        $this->add(array(
//            'type' => 'Zend\Form\Element\Select',
//            'name' => 'campaign',
//            'attributes' => array(
//                'id' => 'campaign',
//                'class' => 'e1 select-w-320',
//                'multiple' => 'multiple'
//            )
//        ));
            

        $this->add(array(
            'name' => 'transaction_amount_from',
            'attributes' => array(
                'type' => 'text',
                'class' => 'width-108',
                'id' => 'transaction_amount_from',
            )
        ));
        $this->add(array(
            'name' => 'transaction_amount_to',
            'attributes' => array(
                'type' => 'text',
                'class' => 'width-108',
                'id' => 'transaction_amount_to',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'transaction_amount_date',
            'attributes' => array(
                'id' => 'transaction_amount_date',
                'onChange' => 'showDate(this.id,"transactiondate_div")',
                'class' => 'e1 select-w-320'
            )
        ));
         $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'ellis_visit',
            'checked_value' => '1',
            'unchecked_value' => '',
            'attributes' => array(
                'id' => 'ellis_visit',
                'class' => 'checkbox e2'
            ),
        ));
        $this->add(array(
            'name' => 'ellis_visit_date_from',
            'attributes' => array(
                'type' => 'text',
		'class' => 'calendar-input cal-icon width-120',
                'id' => 'ellis_visit_date_from'
            )
        ));
        $this->add(array(
            'name' => 'ellis_visit_date_to',
            'attributes' => array(
                'type' => 'text',
		'class' => 'calendar-input cal-icon width-120',
                'id' => 'ellis_visit_date_to'
            )
        ));
        $this->add(array(
            'name' => 'transaction_amount_date_from',
            'attributes' => array(
                'type' => 'text',
                'class' => 'cal-icon width-120',
                'id' => 'transaction_amount_date_from'
            )
        ));
        $this->add(array(
            'name' => 'transaction_amount_date_to',
            'attributes' => array(
                'type' => 'text',
		'class' => 'cal-icon width-120',
                'id' => 'transaction_amount_date_to'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'state',
            'attributes' => array(
                'multiple' => 'multiple',
                'id' => 'state',
                'size' => '4',
                'class' => 'e1 select-w-320'

            )
        ));
        $this->add(array(
            'name' => 'city',
            'attributes' => array(
                'type' => 'text',
                'id' => 'city',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'country',
            'attributes' => array(
                'multiple' => 'multiple',
                'id' => 'country',
                'size' => '4',
                'class' => 'e1 select-w-320'
                
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_exclude_country',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'is_exclude_country',
                'class' => 'checkbox e2'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'product_type',
            'options' => array(
                'value_options' => array(
                    '0' => 'Inventory',
                    '1' => 'Donation',
                    '2' => 'Kiosk Fee'
                ),
            ),
            'attributes' => array(
                'multiple' => 'multiple',
                'id' => 'product_type',
                'style' => 'height:100 px;',
                'class' => 'e1 select-w-320'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'relationship',
            'options' => array(
                
            ),
            'attributes' => array(
                'multiple' => 'multiple',
                'id' => 'relationship',
                'class' => 'e1 select-w-320'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'assocation',
            'options' => array(
                'value_options' => array(
                    ''  => 'Any',
                    '1' => 'Account',
                    '0' => 'Non-Account'
                ),
            ),
            'attributes' => array(
                'id' => 'assocation',
                'class' => 'e1 select-w-320'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'no_email_address',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'no_email_address',
                'class' => 'checkbox e2'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'contact_type',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'contact_type',
                'class' => 'e1 select-w-320'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'exclude_group',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'exclude_group',
                'multiple' => 'multiple',
                'class' => 'e1 select-w-320'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_exclude_group',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'is_exclude_group',
                'class' => 'checkbox e2'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'exclude_tags',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'exclude_tags',
                'multiple' => 'multiple',
                'class' => 'e1 select-w-320'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_exclude_tags',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'is_exclude_tags',
                'class' => 'checkbox e2'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'privacy',
            'options' => array(
                'value_options' => array(
                    '1' => 'Do not Phone',
                    '2' => 'Do not Email',
                    '3' => 'Do not mail',
                    '4' => 'Do not SMS',
                    '5' => 'Do not Trade',
                    '6' => 'Once a Year'
                ),
            ),
            'attributes' => array(
                'id' => 'privacy',
                'multiple' => 'multiple',
                'class' => 'e1 select-w-320'
            )
        ));
        
      $this->add(array(
            'name' => 'phone',
            'attributes' => array(
                'type' => 'text',
                'id' => 'phone'
            )
        ));
        
       $this->add(array(
	'type' => 'Zend\Form\Element\Checkbox',
	'name' => 'transaction_status',
	'checked_value' => '1',
	'unchecked_value' => '0',
	'attributes' => array(
		'id' => 'transaction_status',
		'class' => 'checkbox e2'
	),
        ));

        $this->add(array(
                'type' => 'Zend\Form\Element\Checkbox',
                'name' => 'signup_status',
                'checked_value' => '1',
                'unchecked_value' => '0',
                'attributes' => array(
                        'id' => 'signup_status',
                        'class' => 'checkbox e2'
                ),
        ));

        $this->add(array(
                'type' => 'Zend\Form\Element\Checkbox',
                'name' => 'web_status',
                'checked_value' => '1',
                'unchecked_value' => '0',
                'attributes' => array(
                        'id' => 'web_status',
                        'class' => 'checkbox e2'
                ),
        ));
        
         $this->add(array(
                'type' => 'hidden',
                'name' => 'ellis_status',
                'attributes' => array(
                        'id' => 'ellis_status',
                        'value' => '0'
                )
        ));

        $this->add(array(
            'name' => 'searchcontacts',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'searchcontacts',
                'class' => 'save-btn',
                'value' => 'SEARCH'
            )
        ));
        $this->add(array(
            'name' => 'continuetosurvey',
            'attributes' => array(
                'type' => 'button',
                'id' => 'continuetosurvey',
                'class' => 'cancel-btn m-l-20',
                'value' => 'CONTINUE TO SURVEY QUESTIONS'
            )
        ));
        
        $this->add(array(
            'name' => 'name_email_look',
            'attributes' => array(
                'type' => 'text',
                'id' => 'name_email_look',
                'class' => 'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'name_email_look_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'name_email_look_id',
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'product_ids',
            'attributes' => array(
                'multiple' => 'multiple',
                'id' => 'product_ids',
                'size' => '4',
                'class' => 'e1 select-w-320'

            )
        ));
        /*Search contacts form end */

        /*Survey form start here */
          $this->add(array(
            'name' => 'surveyformsearch',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'continuetosurvey',
                'class' => 'save-btn',
                'value' => 'SEARCH'
            )
        ));
        /*Survey form end here */

        // new - start
          $this->add(array(
                'type' => 'Select',
                'name' => 'status',
                'options' => array(
                        'value_options' => array(
                                '' => 'Any',
                                '1' => 'Active',
                                '0' => 'Inactive'
                        ),
                ),
                'attributes' => array(
                        'id' => 'status',
                        'class' => 'e1 select-w-320',
                        'value' => ''
                )
            ));

             $this->add(array(
                    'type' => 'Select',
                    'name' => 'membership_title',
                    'options' => array(
                            'value_options' => array(
                            ),
                    ),
                    'attributes' => array(
                            'multiple' => 'multiple',
                            'value' => '',
                            'class' => 'e1 select-w-320',
                            'id' => 'membership_title'
                    ),
            ));


            $this->add(array(
                    'type' => 'Zend\Form\Element\Select',
                    'name' => 'item_status',
                    'options' => array(
                    ),
                    'attributes' => array(
                            'id' => 'item_status',
                            'multiple' => 'multiple',
                            'class' => 'e1 select-w-320'
                    )
            ));

            $this->add(array(
                    'type' => 'Zend\Form\Element\Select',
                    'name' => 'transaction_status_id',
                    'options' => array(
                            'value_options' => array(
                            ),
                    ),
                    'attributes' => array(
                            'id' => 'transaction_status_id',
                            'class' => 'e1'
                    )
            ));
            //ticket 73 private group
            $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_private',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'is_private',
                'class' => 'checkbox e2'
            ),
        ));
        // new - end   
          
    }

}

