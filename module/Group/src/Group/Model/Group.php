<?php

/**
 * This file is used for group module validation and filtering.
 * @package    Group_Group
 * @author     Icreon Tech - DT
 */

namespace Group\Model;

use Group\Module;
// Add these import statements
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\Adapter\Adapter;

/**
 * This class is used for group module validation and filtering.
 * @package    Group_Group
 * @author     Icreon Tech - DT
 */
class Group implements InputFilterAwareInterface {

    protected $inputFilter;
    protected $adapter;
    public $group_id;
    public $group_name;
    public $group_type;
    public $group_description;
    public $is_archived;
    public $save_search_param;
    public $save_survey_param;
    public $user_id;
    //ticket 73 private group: add L_PRIVATE
    public $is_private;

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    /**
     * This method is used to convert form post array of create group to Object of class type Group
     * @param array
     * @return Void
     * @author Icreon Tech - DT
     */
    public function exchangeArrayCreateGroup($data) {
        $this->group_id = (isset($data['group_id']) && !empty($data['group_id'])) ? $data['group_id'] : '';
        $this->group_name = (isset($data['group_name']) && !empty($data['group_name'])) ? $data['group_name'] : '';
        $this->group_type = (isset($data['group_type']) && !empty($data['group_type'])) ? $data['group_type'] : '';
        $this->save_search_param = (isset($data['save_search_param']) && !empty($data['save_search_param'])) ? $data['save_search_param'] : '';
        $this->save_survey_param = (isset($data['save_survey_param']) && !empty($data['save_survey_param'])) ? $data['save_survey_param'] : '';
        $this->group_description = (isset($data['group_description']) && !empty($data['group_description'])) ? $data['group_description'] : '';
        $this->is_archived = (isset($data['is_archived']) && !empty($data['is_archived'])) ? $data['is_archived'] : '';
        //ticket 73 private group: add L_PRIVATE
        $this->is_private = (isset($data['is_private']) && !empty($data['is_private'])) ? $data['is_private'] : '0';
    }

    /**
     * This method is used to add filtering and validation to the form elements of create group
     * @param void
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getInputFilterCreateGroup() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'group_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'GROUP_NAME_EMPTY',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[A-Za-z0-9\n\r ,.-]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'GROUP_NAME_VALID'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'Db\NoRecordExists',
                                'options' => array(
                                    'table' => 'tbl_grp_groups',
                                    'field' => 'name',
                                    'adapter' => $this->adapter,
                                    'messages' => array(
                                        'recordFound' => 'GROUP_NAME_ALREADY_EXIST',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'group_description',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'GROUP_DESCRIPTION_EMPTY',
                                    ),
                                ),
                            ),
                            /*array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[A-Za-z0-9 ~@!#$%&*?{}+/,.-_""]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'GROUP_DESCRIPTION_VALID'
                                    ),
                                ),
                            ),*/
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'transaction_amount_date',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'ellis_visit',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'country',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'product_type',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'relationship',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'exclude_group',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'exclude_group',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'exclude_tags',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'privacy',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * This method is used to add filtering and validation to the form elements of edit group
     * @param void
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getInputFilterEditGroup() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'group_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'GROUP_NAME_EMPTY',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[A-Za-z0-9\n\r ,.-]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'GROUP_NAME_VALID'
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'group_description',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'GROUP_DESCRIPTION_EMPTY',
                                    ),
                                ),
                            ),
                            /*array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[A-Za-z0-9\n\r /,.-_]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'GROUP_DESCRIPTION_VALID'
                                    ),
                                ),
                            ),*/
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'transaction_amount_date',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'campaign',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'state',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'product_ids',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            
            $inputFilter->add($factory->createInput(array(
                        'name' => 'ellis_visit',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'country',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'product_type',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'relationship',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'exclude_group',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'exclude_group',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'exclude_tags',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'privacy',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'contact_type',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'group_type_display',
                        'required' => false
                    )));

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * Function used to get array of elements need to call get contacts in group
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function getArrayForCotacts($groupArr) {
        $returnGroupArr = array();
        if (!empty($groupArr)) {
            $returnGroupArr[] = (isset($groupArr['group_id']) && $groupArr['group_id'] != '') ? $groupArr['group_id'] : '';
            $returnGroupArr[] = (isset($groupArr['user_id']) && $groupArr['user_id'] != '') ? $groupArr['user_id'] : '';
            $returnGroupArr[] = (isset($groupArr['sort_field']) && $groupArr['sort_field'] != '') ? $groupArr['sort_field'] : '';
            $returnGroupArr[] = (isset($groupArr['sort_order']) && $groupArr['sort_order'] != '') ? $groupArr['sort_order'] : '';
            $returnGroupArr[] = (isset($groupArr['start_index']) and trim($groupArr['start_index']) != '') ? trim($groupArr['start_index']) : '';
            $returnGroupArr[] = (isset($groupArr['record_limit']) && $groupArr['record_limit'] != '') ? $groupArr['record_limit'] : '';
        }
        return $returnGroupArr;
    }

    /**
     * Function used to get array of elements need to call remove contacts in group
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function getArrayForRemoveCotacts($groupArr) {
        $returnGroupArr = array();
        if (!empty($groupArr)) {
            $returnGroupArr[] = (isset($groupArr['group_id']) && $groupArr['group_id'] != '') ? $groupArr['group_id'] : '';
            $returnGroupArr[] = (isset($groupArr['user_ids']) && $groupArr['user_ids'] != '') ? implode(',',$groupArr['user_ids']) : '';
        }
        return $returnGroupArr;
    }

    /**
     * Function used to get array of elements need to call get contact search in group
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function getArrayForSearchCotactsGroup($params) {
        $returnGroupArr = array();
        if (!empty($params)) {

            if (isset($params['group_id']) && $params['group_id'] != '') {
                $returnGroupArr[] = $params['group_id'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['campaign']) && $params['campaign'] != '') {
                $returnGroupArr[] = $params['campaign'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['transaction_amount_from']) && $params['transaction_amount_from'] != '') {
                $returnGroupArr[] = $params['transaction_amount_from'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['transaction_amount_to']) && $params['transaction_amount_to'] != '') {
                $returnGroupArr[] = $params['transaction_amount_to'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['ellis_visit']) && $params['ellis_visit'] != '' && $params['ellis_visit'] != '0') {
                $returnGroupArr[] = $params['ellis_visit'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['transaction_amount_date']) && $params['transaction_amount_date'] != '') {
                $returnGroupArr[] = $params['transaction_amount_date'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['transaction_amount_date_from']) && $params['transaction_amount_date_from'] != '') {
                $returnGroupArr[] = $params['transaction_amount_date_from'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['transaction_amount_date_to']) && $params['transaction_amount_date_to'] != '') {
                $returnGroupArr[] = $params['transaction_amount_date_to'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['state']) && $params['state'] != '') {
                $returnGroupArr[] = $params['state'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['city']) && $params['city'] != '') {
                $returnGroupArr[] = $params['city'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['country']) && $params['country'] != '') {
                $returnGroupArr[] = $params['country'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['is_exclude_country']) && $params['is_exclude_country'] != '') {
                $returnGroupArr[] = $params['is_exclude_country'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['product_type']) && $params['product_type'] != '') {
                $returnGroupArr[] = $params['product_type'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['relationship']) && $params['relationship'] != '') {
                $returnGroupArr[] = $params['relationship'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['assocation']) && $params['assocation'] != '') {
                $returnGroupArr[] = $params['assocation'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['no_email_address']) && $params['no_email_address'] != '') {
                $returnGroupArr[] = $params['no_email_address'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['contact_type']) && $params['contact_type'] != '') {
                $returnGroupArr[] = $params['contact_type'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['exclude_group']) && $params['exclude_group'] != '') {
                $returnGroupArr[] = $params['exclude_group'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['is_exclude_group']) && $params['is_exclude_group'] != '') {
                $returnGroupArr[] = $params['is_exclude_group'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['exclude_tags']) && $params['exclude_tags'] != '') {
                $returnGroupArr[] = $params['exclude_tags'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['is_exclude_tags']) && $params['is_exclude_tags'] != '') {
                $returnGroupArr[] = $params['is_exclude_tags'];
            } else {
                $returnGroupArr[] = '';
            }

            if (isset($params['privacy']) && $params['privacy'] != '') {
                $returnGroupArr[] = $params['privacy'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['name_email_look']) && $params['name_email_look'] != '') {
                $returnGroupArr[] = $params['name_email_look'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['product_ids']) && $params['product_ids'] != '') {
                $returnGroupArr[] = $params['product_ids'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['name_email']) && $params['name_email'] != '') {
                $returnGroupArr[] = $params['name_email'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['in_group_name']) && $params['in_group_name'] != '') {
                $returnGroupArr[] = $params['in_group_name'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['tags']) && $params['tags'] != '') {
                $returnGroupArr[] = $params['tags'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['surveyids']) && $params['surveyids'] != '') {
                $returnGroupArr[] = $params['surveyids'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['startIndex']) && $params['startIndex'] != '') {
                $returnGroupArr[] = $params['startIndex'];
            } else {
                $returnGroupArr[] = 0;
            }
            if (isset($params['recordLimit']) && $params['recordLimit'] != '') {
                $returnGroupArr[] = $params['recordLimit'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['sortField']) && $params['sortField'] != '') {
                $returnGroupArr[] = $params['sortField'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['sortOrder']) && $params['sortOrder'] != '') {
                $returnGroupArr[] = $params['sortOrder'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['searchtype']) && $params['searchtype'] != '') {
                $returnGroupArr[] = $params['searchtype'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['userId_exclude']) && $params['userId_exclude'] != '') {
                $returnGroupArr[] = $params['userId_exclude'];
            } else {
                $returnGroupArr[] = '';
            }
            if (isset($params['phone']) && $params['phone'] != '') {
                $returnGroupArr[] = $params['phone'];
            } else {
                $returnGroupArr[] = '';
            }
            
           if (isset($params['transaction_status']) && $params['transaction_status'] != '') {
                $returnGroupArr[] = $params['transaction_status'];
            } else {
                $returnGroupArr[] = '';
            }
            
           if (isset($params['signup_status']) && $params['signup_status'] != '') {
                $returnGroupArr[] = $params['signup_status'];
            } else {
                $returnGroupArr[] = '';
            }
            
            if (isset($params['web_status']) && $params['web_status'] != '') {
                $returnGroupArr[] = $params['web_status'];
            } else {
                $returnGroupArr[] = '';
            }
            
            if (isset($params['ellis_status']) && $params['ellis_status'] != '') {
                $returnGroupArr[] = $params['ellis_status'];
            } else {
                $returnGroupArr[] = '';
            }

            if (isset($params['membership_title']) && $params['membership_title'] != '') { $returnGroupArr[] = $params['membership_title']; } else { $returnGroupArr[] = ''; }            
            if (isset($params['status']) && $params['status'] != '') { $returnGroupArr[] = $params['status']; } else { $returnGroupArr[] = ''; }
            if (isset($params['item_status']) && is_array($params['item_status']) && count($params['item_status']) > 0) { $returnGroupArr[] = implode(",", $params['item_status']); } else { $returnGroupArr[] = ''; }
            if (isset($params['transaction_status_id']) && $params['transaction_status_id'] != '') { $returnGroupArr[] = $params['transaction_status_id']; } else { $returnGroupArr[] = ''; }
            
        }
        return $returnGroupArr;
    }

    /**
     * Function used to get array of elements need to call remove group
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function getArrayForRemoveGroup($groupArr) {
        $returnGroupArr = array();
        if (!empty($groupArr)) {
            $returnGroupArr[] = (isset($groupArr['group_id']) && $groupArr['group_id'] != '') ? $groupArr['group_id'] : '';
        }
        return $returnGroupArr;
    }

    /**
     * Function used to check validation for user signup form
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();


            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add content to these methods:
    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    /**
     * Function used to check variables group
     * @author Icreon Tech - DT
     * @return boolean
     * @param Array
     */
    public function exchangeArray($data) {
        
    }


	public function getInputFilterEditSaveGroup() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'group_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'GROUP_NAME_EMPTY',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[A-Za-z0-9\n\r ,.-]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'GROUP_NAME_VALID'
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'group_description',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'GROUP_DESCRIPTION_EMPTY',
                                    ),
                                ),
                            ),
                            /*array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[A-Za-z0-9\n\r /,.-_]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'GROUP_DESCRIPTION_VALID'
                                    ),
                                ),
                            ),*/
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'transaction_amount_date',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'campaign',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'state',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'product_ids',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            
            $inputFilter->add($factory->createInput(array(
                        'name' => 'ellis_visit',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'country',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'product_type',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'relationship',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'exclude_group',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'exclude_group',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'exclude_tags',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'privacy',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'contact_type',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'group_type_display',
                        'required' => false
                    )));

			$inputFilter->add($factory->createInput(array(
                        'name' => 'assocation',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
			$inputFilter->add($factory->createInput(array(
                        'name' => 'status',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
			$inputFilter->add($factory->createInput(array(
                        'name' => 'membership_title',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
			$inputFilter->add($factory->createInput(array(
                        'name' => 'item_status',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
			$inputFilter->add($factory->createInput(array(
                        'name' => 'transaction_status_id',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

}