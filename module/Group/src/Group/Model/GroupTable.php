<?php

/**
 * This file is used for group module database related .
 * @package    Group_GroupTable
 * @author     Icreon Tech - DT
 */

namespace Group\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This model is used for group module database related .
 * @package    GroupTable
 * @author     Icreon Tech - DT
 */
class GroupTable {

    protected $tableGateway;
    protected $dbAdapter;
    protected $connection;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
        $this->connection = $this->tableGateway->getAdapter()->getDriver()->getConnection();
    }

    /**
     * Function for insert group data
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function saveGroup($group) {
        $addedDate = DATE_TIME_FORMAT;
        $modifyDate = DATE_TIME_FORMAT;
        if (isset($group->group_id) && ($group->group_id != '')) {
            $groupId = $group->group_id;
        } else {
            $groupId = '';
        }
        $groupName = $group->group_name;
        $groupDescription = stripslashes($group->group_description);
        $groupType = $group->group_type;
        $isArchived = $group->is_archived;
       /* if ($groupType == 1) {
            $saveSearchParam = NULL;
            $saveSurveyParam = NULL;
        } else {*/
            $saveSearchParam = $group->save_search_param;
            $saveSurveyParam = $group->save_survey_param;
        //}
        //ticket 73 private group
        if(isset($group->is_private) && $group->is_private == '1')  
        {
            $is_private = "1";
            
        }
        else
        {
            $is_private = "0";
        }
        if (isset($groupId) && !empty($groupId)) {
            $result = $this->connection->execute("CALL usp_grp_updateGroup('" . $groupId . "','" . $groupName . "','" . $groupDescription . "','" . $isArchived . "','" . $modifyDate . "','" . $groupType . "','" . $saveSearchParam . "','" . $saveSurveyParam . "','" . $is_private . "')");
        } else {
            $result = $this->connection->execute("CALL usp_grp_insertGroup('" . $groupName . "','" . $groupDescription . "','" . $addedDate . "','" . $modifyDate . "','" . $groupType . "','" . $saveSearchParam . "','" . $saveSurveyParam . "','" . $is_private . "')");
        }

        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for get group data on the base of id
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function getGroupById($groupArr) {
        if (isset($groupArr)) {

            if (isset($groupArr['group_id']) && $groupArr['group_id'] != '') {
                $groupId = $groupArr['group_id'];
            } else {
                $groupId = '';
            }
             //ticket 73 private group
            if(ALLOW_PRIVATE_GROUP == 1)
            {
                $allow_private = 1;
            }
            else
            {
                $allow_private = 0;
            }
            
            $result = $this->connection->execute("CALL usp_grp_getGroups('" . $groupId . "','','','','','','','','".$allow_private."')");

            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet[0];
            else
                return false;
        }
    }

    /**
     * Function for get group data
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function getAllGroup($groupArr) {
        if (isset($groupArr)) {

            if (isset($groupArr['group_id']) && $groupArr['group_id'] != '') {
                $groupId = $groupArr['group_id'];
            } else {
                $groupId = '';
            }

            if (isset($groupArr['group_name']) && $groupArr['group_name'] != '') {
                $groupName = $groupArr['group_name'];
            } else {
                $groupName = '';
            }

            if (isset($groupArr['is_archived']) && $groupArr['is_archived'] != '') {
                $isArchived = $groupArr['is_archived'];
            } else {
                $isArchived = '0';
            }
            
            if (isset($groupArr['group_type']) && $groupArr['group_type'] != '') {
                $groupType = $groupArr['group_type'];
            } else {
                $groupType = null;
            }
            if (isset($groupArr['sort_field']) && $groupArr['sort_field'] != '') {
                $sort_field = $groupArr['sort_field'];
            } else {
                $sort_field = '';
            }
            if (isset($groupArr['sort_order']) && $groupArr['sort_order'] != '') {
                $sort_order = $groupArr['sort_order'];
            } else {
                $sort_order = '';
            }
            if (isset($groupArr['start_index']) && $groupArr['start_index'] != '') {
                $start_index = $groupArr['start_index'];
            } else {
                $start_index = '';
            }
            if (isset($groupArr['record_limit']) && $groupArr['record_limit'] != '') {
                $record_limit = $groupArr['record_limit'];
            } else {
                $record_limit = '';
            }
            //ticket 73 private group
            if(ALLOW_PRIVATE_GROUP == 1)
            {
                $allow_private = 1;
            }
            else
            {
                $allow_private = 0;
            }

            $result = $this->connection->execute("CALL usp_grp_getGroups('" . $groupId . "','" . $groupName . "','" . $isArchived . "','" . $sort_field . "','" . $sort_order . "','" . $start_index . "','" . $record_limit . "','".$groupType."','".$allow_private."')");

            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        }
    }

    /**
     * This function will fetch the all contact search record on the basis in search group
     * @param this will be an array having the search Params.
     * @return this will return a contacts array
     * @author Icreon Tech -DT
     */
    public function getContactSearchCreateGroup($params = array()) {
        $procstr = $this->getParamsForProcedure($params);
        $result = $this->connection->execute("CALL usp_grp_contacts_search_create_group(" . $procstr . ")");        
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_OBJ);
        return $resultSet;
    }
    
    /**
     * This function will fetch the all contact search record on the basis in search group
     * @param this will be an array having the search Params.
     * @return this will return a contacts array
     * @author Icreon Tech -DT
     */
    public function getContactSearch($params = array()) {
        $procstr = $this->getParamsForProcedure($params);        
        $result = $this->connection->execute("CALL usp_grp_contacts_search(" . $procstr . ")");   
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_OBJ);
        return $resultSet;
    }

    /**
     * Function for insert contact into group
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function saveContactToGroup($group) {
        $addedDate = DATE_TIME_FORMAT;
        $modifyDate = DATE_TIME_FORMAT;
        $groupId = $group->group_id;
        $userId = $group->user_id;
        $result = $this->connection->execute("CALL usp_grp_insertUserToGroup('" . $userId . "','" . $groupId . "','" . $addedDate . "','" . $modifyDate . "')");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }
    
    /**
     * Function for exclude contact into group
     * @author Icreon Tech - DG
     * @return Int
     * @param Array
     */
    public function excludeContactToGroup($group) {
        $addedDate = DATE_TIME_FORMAT;
        $modifyDate = DATE_TIME_FORMAT;
        $groupId = $group->group_id;
        $userId = $group->user_id;
        $result = $this->connection->execute("CALL usp_grp_excludeUserToGroup('" . $userId . "','" . $groupId . "','" . $addedDate . "','" . $modifyDate . "')");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }
    
    /**
     * Function for delete exclude contact into group
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function deleteExcludeContactToGroup($group) {
        $groupId = $group->group_id;
        $result = $this->connection->execute("CALL usp_grp_deleteExcludeUserToGroup('" . $groupId . "')");
        return $result;
    }

    

    /**
     * Function for get contact from group
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function getContactsByGroupId($group_array) {
        $procstr = $this->getParamsForProcedure($group_array);
        $result = $this->connection->execute("CALL usp_usr_getGroupContacts(" . $procstr . ")");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        //asd($resultSet);
        return $resultSet;
    }

    /**
     * Function for get Campaign Group Search
     * @author Icreon Tech - NS
     * @return Int
     * @param Array
     */
    public function getCampaignGroupSearch($campaignsArr) {
        try {
            $campaignsArr['campaigns_ids'] = (isset($campaignsArr['campaigns_ids']) and trim($campaignsArr['campaigns_ids']) != "") ? trim($campaignsArr['campaigns_ids']) : "";
            $result = $this->connection->execute("CALL usp_com_getCampaignsGroupSearch('" . $campaignsArr['campaigns_ids'] . "')");
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return array();
        }
        catch(Exception $e) {
            return array();
        }
     }
    
    /**
     * Function for remove contacts by group id
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function removeContactsByGroupId($group_array) {
        $procstr = $this->getParamsForProcedure($group_array);
        $result = $this->connection->execute("CALL usp_grp_deleteUserFromGroup(" . $procstr . ")");
        return $result;
    }

    /**
     * Function for remove group
     * @author Icreon Tech - DT
     * @return Void
     * @param Array
     */
    public function removeGroupById($group_array) {
        $procquesmarkapp = $this->appendQuestionMars($group_array);
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_grp_deleteGroup(" . $procquesmarkapp . ")");
        $this->bindFieldArray($stmt, $group_array);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for get parameters for call stored procedure
     * @author Icreon Tech - DT
     * @return String
     * @param Array
     */
    public function getParamsForProcedure($fields_value_arr) {
        $returnstr = '';
        foreach ($fields_value_arr as $field_value) {
            $returnstr.="'" . $field_value . "',";
        }
        return rtrim($returnstr, ',');
    }
    
    /**
     * Function for get excluded contact from group
     * @author Icreon Tech - DT
     * @return String
     * @param Array
     */
    public function getExcludedContactInGroup($dataArr) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_grp_getExcludedContact(?)');
        $stmt->getResource()->bindParam(1, $dataArr['groupId']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for bind field for call to stored procedure
     * @author Icreon Tech - DT
     * @return Void
     * @param object stm,array fields_value_arr
     */
    public function bindFieldArray($stmt, $fields_value_arr) {
        $i = 1;
        foreach ($fields_value_arr as $field_value) {
            $stmt->getResource()->bindParam($i, $field_value);
            $i++;
        }
    }

    /**
     * Function for append number of questions marks for stored procedure
     * @author Icreon Tech - DT
     * @return String
     * @param Array
     */
    public function appendQuestionMars($fields_value_arr) {
        $returnstr = '';
        foreach ($fields_value_arr as $field_value) {
            $returnstr.='?,';
        }
        return rtrim($returnstr, ',');
    }

}