<?php

/**
 * This file define all controller and routes
 * @package    Transaction
 * @author     Icreon Tech - DG
 */
return array(
    'controllers' => array(
        'invokables' => array(
            'Transaction\Controller\Transaction' => 'Transaction\Controller\TransactionController',
            'Transaction\Controller\Woh' => 'Transaction\Controller\WohController',
            'Transaction\Controller\Rma' => 'Transaction\Controller\RmaController',
            'Transaction\Controller\Bulkdonation' => 'Transaction\Controller\BulkdonationController',
            'Transaction\Controller\Sage' => 'Transaction\Controller\SageController',
            'Transaction\Controller\Batch' => 'Transaction\Controller\BatchController',
            'Transaction\Controller\Donation' => 'Transaction\Controller\DonationController',
            'Transaction\Controller\Postransaction' => 'Transaction\Controller\PostransactionController',
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'createTransaction' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-transaction',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'create-transaction',
                    ),
                ),
            ),
            'getUserTransactions' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-transactions[/:id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'get-user-transactions',
                    ),
                ),
            ),
            'getUserSearchTransactions' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-search-transactions',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'user-search-transactions',
                    ),
                ),
            ),
            'transaction-receipt' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-transaction-receipt[/:user_id][/:transaction_id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'viewTaxReceipt',
                    ),
                ),
            ),
            'create-crm-transaction' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-crm-transaction',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'addCrmTransaction',
                    ),
                ),
            ),
            'getusertransactionlist' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-transaction-list[/:user_id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'getUserTransactionList',
                    ),
                ),
            ),
            'get-crm-transactions' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-transactions[/:item_status]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'getCrmTransactions',
                    ),
                ),
            ),
            'delete-crm-transaction' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-transaction',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'deleteCrmTransaction',
                    ),
                ),
            ),
            'add-crm-search-transaction' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-crm-search-transaction',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'addCrmSearchTransaction',
                    ),
                ),
            ),
            'get-transaction-search-saved-param' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-transaction-search-saved-param',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'getTransactionSearchSavedParam',
                    ),
                ),
            ),
            'delete-crm-search-transaction' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-search-transaction',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'deleteCrmSearchTransaction',
                    ),
                ),
            ),
            'get-crm-cart-products' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-cart-products[/:user_id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'getCrmCartProducts',
                    ),
                ),
            ),
            'get-billing-shipping-address-info' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-billing-shipping-address-info',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'getBillingShippingAddressInfo',
                    ),
                ),
            ),
            'get-billing-shipping-address-info-front' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-billing-shipping-address-info-front',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'getBillingShippingAddressInfoFront',
                    ),
                ),
            ),
            'add-product-to-cart-detail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-product-to-cart-detail[/:user_id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'addProductToCartDetail',
                    ),
                ),
            ),
            'get-donation-add-to-cart-detail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-donation-add-to-cart-detail',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'getDonationAddToCartDetail',
                    ),
                ),
            ),
            'get-flag-of-faces-add-to-cart-detail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-flag-of-faces-add-to-cart-detail',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'getFlagOfFacesAddToCartDetail',
                    ),
                ),
            ),
            'get-kiosk-fee-add-to-cart-detail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-kiosk-fee-add-to-cart-detail',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'getKioskFeeAddToCartDetail',
                    ),
                ),
            ),
            'get-inventory-add-to-cart-detail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-inventory-add-to-cart-detail',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'getInventoryAddToCartDetail',
                    ),
                ),
            ),
            'get-membership-add-to-cart-detail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-membership-add-to-cart-detail',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'getMembershipAddToCartDetail',
                    ),
                ),
            ),
            'get-passenger-document-add-to-cart-detail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-passenger-document-add-to-cart-detail',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'getPassengerDocumentAddToCartDetail',
                    ),
                ),
            ),
            'get-wall-of-honor-add-to-cart-detail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-wall-of-honor-add-to-cart-detail',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'getWallOfHonorAddToCartDetail',
                    ),
                ),
            ),
            'add-more-notify' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-more-notify',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'addMoreNotify',
                    ),
                ),
            ),
            'generate-woh-form-by-naming-convention' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/generate-woh-form-by-naming-convention',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'generateWohFormByNamingConvention',
                    ),
                ),
            ),
            'add-additional-contact' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-additional-contact',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'addAdditionalContact',
                    ),
                ),
            ),
            'add-to-cart-donation' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-to-cart-donation',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'addToCartDonation',
                    ),
                ),
            ),
            'add-to-cart-fof' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-to-cart-fof',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'addToCartFof',
                    ),
                ),
            ),
            'add-to-cart-kiosk-fee' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-to-cart-kiosk-fee',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'addToCartKioskFee',
                    ),
                ),
            ),
            'add-to-cart-inventory' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-to-cart-inventory',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'addToCartInventory',
                    ),
                ),
            ),
            'add-to-cart-membership' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-to-cart-membership',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'addToCartMembership',
                    ),
                ),
            ),
            'add-to-cart-wall-of-honor' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-to-cart-wall-of-honor',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => ' addToCartWallOfHonor',
                    ),
                ),
            ),
            'get-add-to-cart-crm-search-passenger' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-add-to-cart-crm-search-passenger',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'getAddToCartCrmSearchPassenger',
                    ),
                ),
            ),
            'get-passenger-record-info' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-passenger-record-info',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'getPassengerRecordInfo',
                    ),
                ),
            ),
            'get-wall-of-honor-bio-certificate-search' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-wall-of-honor-bio-certificate-search',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'getWallOfHonorBioCertificateSearch'
                    ),
                ),
            ),
            'get-wall-of-honor-other-products' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-wall-of-honor-other-products',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'getWallOfHonorOtherProducts'
                    ),
                ),
            ),
            'getTransactionDetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-transaction-detail[/:id][/:mode]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'getTransactionDetail',
                    ),
                ),
            ),
            'viewTransaction' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-transaction[/:id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'viewTransaction',
                    ),
                ),
            ),
            'create-bio-certificate' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-bio-certificate/:bioProId',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'addBioCertificate'
                    ),
                ),
            ),
            'addNote' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-note',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'addNote'
                    ),
                ),
            ),
            'add-to-cart-passenger-document' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-to-cart-passenger-document',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'addToCartPassengerDocument',
                    ),
                ),
            ),
            'get-donation-add-to-cart-front' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/donate[/:amount]',
                    'defaults' => array(
                         //'controller' => 'Transaction\Controller\Transaction',
                          'controller' => 'Transaction\Controller\Donation',
                         'action' => 'getDonationFront',
                    ),
                ),
            ),/*
            'get-donation-add-to-cart-front-test' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/donate-test[/:amount]',
                    'defaults' => array(
                         //'controller' => 'Transaction\Controller\Transaction',
                          'controller' => 'Transaction\Controller\Donation',
                         'action' => 'getDonationFront',
                    ),
                ),
            ), */           
            'add-more-notify-front' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-more-notify-front',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'addMoreNotifyFront',
                    ),
                ),
            ),
            'add-to-cart-donation-front' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-to-cart-donation-front',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'addToCartDonationFront',
                    ),
                ),
            ),
            'donate-encrypt-text' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/donate-encrypt-text',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'donateEncryptText',
                    ),
                ),
            ),
            'add-to-cart-donation-front-cms' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-to-cart-donation-front-cms[/:amount]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'addToCartDonationFrontCms',
                    ),
                ),
            ),
            'showShipImage' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/show-ship-image[/:id][/:flag][/:arrive_date][/:ship_name]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'showShipImage',
                    ),
                ),
            ),
            'cart-list-front' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/cart',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'cartListFront',
                    ),
                ),
            ),
            'add-additional-image-backend' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-additional-image-backend',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'addAdditionalImageBackend',
                    ),
                ),
            ),
            'viewFofImage' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-fof-image[/:id][/:transactionid]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'viewFofImage',
                    ),
                ),
            ),
            'viewFofImageCart' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-fof-image-cart[/:id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'viewFofImageCart',
                    ),
                ),
            ),
            'viewPassengerCertificate' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-passenger-certificate[/:id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'viewPassengerCertificate',
                    ),
                ),
            ),
            'viewWohDetails' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-woh-details[/:id][/:transactionid][/:status]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'viewWohDetails',
                    ),
                ),
            ),
            'viewWohLetter' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-woh-letter[/:id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'viewWohLetter',
                    ),
                ),
            ),
            'viewWohCertificate' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-woh-certificate[/:id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'viewWohCertificate',
                    ),
                ),
            ),
            'viewManifest' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-manifest[/:id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'viewManifest',
                    ),
                ),
            ),
            'downloadManifest' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/download-manifest[/:id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'downloadManifest',
                    ),
                ),
            ),
            'viewPanelCertificate' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-panel-certificate[/:id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'viewPanelCertificate',
                    ),
                ),
            ),
            'cart-list-front' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/cart',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'cartListFront',
                    ),
                ),
            ),
            'add-to-cart-inventory-front' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-to-cart-inventory-front',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'addToCartInventoryFront',
                    ),
                ),
            ),
            'remove-cart-front' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/remove-cart-front',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'removeCartFront',
                    ),
                ),
            ),
            'cart-list-front-product' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/cart-front-product',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'cartListFrontProduct',
                    ),
                ),
            ),
            'update-cart-front' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/update-cart-front',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'updateCartFront',
                    ),
                ),
            ),
            'update-cart-crm' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/update-cart-crm',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'updateCartCrm',
                    ),
                ),
            ),
            'checkout-cart-front' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/checkout',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'checkoutCartFront',
                    ),
                ),
            ),
            'wohEntries' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/woh-entries',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Woh',
                        'action' => 'wohEntries',
                    ),
                ),
            ),
            'getWohEntries' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-woh-entries',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Woh',
                        'action' => 'getWohEntries',
                    ),
                ),
            ),
            'wohBulkPanelAssignment' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/woh-bulk-panel-assignment',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Woh',
                        'action' => 'wohBulkPanelAssignment',
                    ),
                ),
            ),
            'wohBulkPanelAssignmentUpload' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/woh-bulk-panel-assignment-upload',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Woh',
                        'action' => 'wohBulkPanelAssignmentUpload',
                    ),
                ),
            ),
            'wohBulkPanelAssignmentDownload' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/woh-bulk-panel-assignment-download[/:year]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Woh',
                        'action' => 'wohBulkPanelAssignmentDownload',
                    ),
                ),
            ),
            'updateProductStatus' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/update-product-status[/:id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'updateProductStatus',
                    ),
                ),
            ),
            'delete-product-to-cart' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-product-to-cart',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'deleteProductToCart',
                    ),
                ),
            ),
            'get-crm-cart-products-reload' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-cart-products-reload',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'getCrmCartProductsReload',
                    ),
                ),
            ),
            'updateProductQty' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/update-product-qty[/:id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'updateProductQty',
                    ),
                ),
            ),
            'viewShipment' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-shipment[/:id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'viewShipment',
                    ),
                ),
            ),
            'orderreview-cart-front' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/orderreview-cart-front',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'orderreviewCartFront',
                    ),
                ),
            ),
            'add-check' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-check',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'addCheck',
                    ),
                ),
            ),
            'apply-coupon' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/apply-coupon',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'applyCoupon',
                    ),
                ),
            ),
            'save-transaction' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/save-transaction',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'saveTransaction',
                    ),
                ),
            ),
            'packingSlip' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/packing-slip[/:id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'packingSlip',
                    ),
                ),
            ),
            'get-total-cart' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-total-cart',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'getTotalCart',
                    ),
                ),
            ),
            'usertransactiondetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-transaction-detail[/:transaction_id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'getUserTransactionDetail',
                    ),
                ),
            ),
            'usertransactiondetailcrm' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-transaction-detail-crm[/:transaction_id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'getUserTransactionDetailCrm',
                    ),
                ),
            ),
            'userdonationdeceipt' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-donation-receipt[/:user_id][/:transaction_id][/:amount]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'userDonationReceipt',
                    ),
                ),
            ),
            'updateAllStatus' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/update-all-status[/:id][/:status]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'updateAllStatus',
                    ),
                ),
            ),
            'transactionInvoice' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/transaction-invoice[/:id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'transactionInvoice',
                    ),
                ),
            ),
            'placeOrder' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/place-order',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'placeOrder',
                    ),
                ),
            ),
            'addToCartPassengerDocumentFront' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-cart-passenger-document-front',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'addToCartPassengerDocumentFront',
                    ),
                ),
            ),
            'checkRelatedProductsExists' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/check-related-products-exists',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'checkRelatedProductsExists',
                    ),
                ),
            ),
            'addToCartMembershipFront' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-cart-membership-front',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'addToCartMembershipFront',
                    ),
                ),
            ),
            'barcode' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/barcode[/:content]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'barcode',
                    ),
                ),
            ),
            'createRma' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-rma[/:transaction_id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Rma',
                        'action' => 'createRma',
                    ),
                ),
            ),
            'getTransactionId' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-transaction-id',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Rma',
                        'action' => 'getTransactionId',
                    ),
                ),
            ),
            'getTransactionProducts' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-transaction-products',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Rma',
                        'action' => 'getTransactionProducts',
                    ),
                ),
            ),
            'crmRma' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-rma',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Rma',
                        'action' => 'getCrmRma',
                    ),
                ),
            ),
            'addWishList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-to-wishlist',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'addWishList',
                    ),
                ),
            ),
            'wishList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/wishlist',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'wishList',
                    ),
                ),
            ),
            'wishListFrontProduct' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/wishlist-product',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'wishListFrontProduct',
                    ),
                ),
            ),
            'remove-wishlist-front' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/remove-wishlist-front',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'removeWishlistFront',
                    ),
                ),
            ),
            'get-transaction-addtocart' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-transaction-addtocart',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => ' getTransactionAddtocart',
                    ),
                ),
            ),
            'listCrmRma' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/list-crm-rma',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Rma',
                        'action' => ' listCrmRma',
                    ),
                ),
            ),
            'viewCrmRma' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-crm-rma[/:id][/:mode]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Rma',
                        'action' => 'viewCrmRma',
                    ),
                ),
            ),
            'editRmaDetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-rma-detail[/:id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Rma',
                        'action' => 'editRmaDetail',
                    ),
                ),
            ),
            'viewRmaDetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-rma-detail[/:id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Rma',
                        'action' => 'viewRmaDetail',
                    ),
                ),
            ),
            'get-credit-card-detail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-credit-card-detail[/:user_id][/:pledge_transaction_id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'getCreditCardDetail',
                    ),
                ),
            ),
            'rmaSlip' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/rma-slip[/:id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Rma',
                        'action' => 'rmaSlip',
                    ),
                ),
            ),
            'addRmaNote' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-rma-note',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Rma',
                        'action' => 'addRmaNote'
                    ),
                ),
            ),
            'donationbulkentries' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/donation-bulk-entries',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Bulkdonation',
                        'action' => 'donationBulkEntries',
                    ),
                ),
            ),
            'createbatch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-batch',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Bulkdonation',
                        'action' => 'createBatch',
                    ),
                ),
            ),
            'getbatchdetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-batch-detail[/:batch_id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Bulkdonation',
                        'action' => 'getBatchDetail',
                    ),
                ),
            ),
            'getbatchdonationdetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-batch-donation-detail[/:batch_id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Bulkdonation',
                        'action' => 'getBatchDonationDetail',
                    ),
                ),
            ),
            'addmoredonations' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-more-donations',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Bulkdonation',
                        'action' => 'addMoreDonations',
                    ),
                ),
            ),
            'make-authorize-payment' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/make-authorize-payment',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'makeAuthorizePayment',
                    ),
                ),
            ),
            'get-export-to-sage' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-sages[/:sage_batch_no]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Sage',
                        'action' => 'getExportToSage',
                    ),
                ),
            ),
            'create-sage-batch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-sage-batch',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Sage',
                        'action' => 'createSageBatch',
                    ),
                ),
            ),
            'saveRmaSearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/save-rma-search',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Rma',
                        'action' => 'saveRmaSearch',
                    ),
                ),
            ),
            'getrmasearchsavedselect' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-rma-search-saved-select',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Rma',
                        'action' => 'getRmaSearchSavedSelect',
                    ),
                ),
            ),
            'deleteRmaSearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-rma-search',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Rma',
                        'action' => 'deleteRmaSearch',
                    ),
                ),
            ),
            'save-download-batch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/save-download-batch[/:sage_batch_no]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Sage',
                        'action' => 'saveDownloadBatch',
                    ),
                ),
            ),
            'generateZipFiles' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/generate-zip-files',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Sage',
                        'action' => 'generateZipFiles',
                    ),
                ),
            ),
            'getExportToSageDownload' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/download-sage-file[/:sage_batch_no]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Sage',
                        'action' => 'getExportToSageDownload',
                    ),
                ),
            ),
            'shippingMethodFront' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/shipping-method-front',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'shippingMethodFront',
                    ),
                ),
            ),
            'getShippingMethod' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-shipping-method',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'getShippingMethod',
                    ),
                ),
            ),
            'viewBioDetails' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-bio-details[/:id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'viewBioDetails',
                    ),
                ),
            ),
            'downloadZip' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/download-zip[/:shipmentId]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'downloadZip',
                    ),
                ),
            ),
            'letterSavePdf' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/letter-save-pdf[/:id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'letterSavePdf',
                    ),
                ),
            ),
            'certificateSavePdf' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/certificate-save-pdf[/:id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'certificateSavePdf',
                    ),
                ),
            ),
            'passengercertificateSavePdf' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/passenger-certificate-save-pdf[/:id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'passengercertificateSavePdf',
                    ),
                ),
            ),
            'getCampaignProgram' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-campaign-program',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'getCampaignProgram',
                    ),
                ),
            ),
            'deleteCreditCard' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-credit-card',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'deleteCreditCard',
                    ),
                ),
            ),
            'modifyNote' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/modify-note[/:tableName][/:tableField][/:noteId][/:val]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'modifyNote'
                    ),
                ),
            ),
            'viewDonDetails' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-don-details[/:id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'viewDonDetails',
                    ),
                ),
            ),
            'viewBioCertificate' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-bio-certificate[/:id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'viewBioCertificate',
                    ),
                ),
            ),
            'cartDetails' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/cart-details[/:id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'cartDetails',
                    ),
                ),
            ),
            'editwohentries' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-woh-entries[/:woh_id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Woh',
                        'action' => 'editWohEntries',
                    ),
                ),
            ),
            'updatewohentry' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/update-woh-entry',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Woh',
                        'action' => 'updateWohEntry',
                    ),
                ),
            ),
            'saveUserBillingAddress' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/save-user-billing-address[/:addressid]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'saveUserBillingAddress',
                    ),
                ),
            ),
            'saveUserShippingAddress' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/save-user-shipping-address[/:addressid]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'saveUserShippingAddress',
                    ),
                ),
            ),
            'userShippingAddress' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-shipping-address[/:user_id][/:address_location][/:default_id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'userShippingAddress',
                    ),
                ),
            ),
            'userBillingAddress' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-billing-address[/:user_id][/:address_location]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'userBillingAddress',
                    ),
                ),
            ),
            'deleteBillingShippingAddress' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-billing-shipping-address[/:address_id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'deleteBillingShippingAddress',
                    ),
                ),
            ),
            'saveCreditCartFront' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/save-credit-cart-front',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'saveCreditCartFront',
                    ),
                ),
            ),
            'userCreditCardListing' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-credit-card-listing[/:user_id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'userCreditCardListing',
                    ),
                ),
            ),
            'updateTrackingCode' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/update-tracking-code[/:id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'updateTrackingCode',
                    ),
                ),
            ),
            'updateTransactionBatch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/update-transaction-batch',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Batch',
                        'action' => 'updateTransactionBatch',
                    ),
                ),
            ),
            'selectBatchId' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/select-batch-id[/:batch_text_id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Batch',
                        'action' => 'selectBatchId',
                    ),
                ),
            ),
            'getBatchList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-batch-list[/:id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Batch',
                        'action' => 'getBatchList',
                    ),
                ),
            ),
            'createBatchId' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-batch-id',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Batch',
                        'action' => 'createBatchId',
                    ),
                ),
            ),
            'updateBatchId' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/update-batch-id',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Batch',
                        'action' => 'updateBatchId',
                    ),
                ),
            ),
            'editBatchId' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-batch-id',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Batch',
                        'action' => 'editBatchId',
                    ),
                ),
            ),
            'getavailablebatch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-available-batch',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Bulkdonation',
                        'action' => 'getAvailableBatch',
                    ),
                ),
            ),
            'gettransactionlist' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-transaction-list',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'getTransactionList',
                    ),
                ),
            ),
            'assignbatchid' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/assign-batch-id',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'assignBatchId',
                    ),
                ),
            ),
            'showProductImage' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/show-product-image[/:id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'showProductImage',
                    ),
                ),
            ),
            'donateCheckout' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/checkout-donate[/:id]',
                    'defaults' => array(
                       //'controller' => 'Transaction\Controller\Transaction',
                       'controller' => 'Transaction\Controller\Donation',
                       'action' => 'donateCheckout',
                    ),
                ),
            ),
            'donateConfirmation' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/donate-confirmation[/:id]',
                    'defaults' => array(
                       //'controller' => 'Transaction\Controller\Transaction',
                       'controller' => 'Transaction\Controller\Donation',
                       'action' => 'donateConfirmation',
                    ),
                ),
            ),
			'checkUserTransaction' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/check-transaction[/:transaction_id]',
                    'defaults' => array(
                       //'controller' => 'Transaction\Controller\Transaction',
                       'controller' => 'Transaction\Controller\Transaction',
                       'action' => 'checkUserTransaction',
                    ),
                ),
            ),
			'creditUserTransaction' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/credit-user-transaction[/:transaction_id][/:user_id]',
                    'defaults' => array(
                       //'controller' => 'Transaction\Controller\Transaction',
                       'controller' => 'Transaction\Controller\Transaction',
                       'action' => 'creditUserTransaction',
                    ),
                ),
            ),
			 'markWrongManifest' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/mark-wrong-manifest[/:id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'markWrongManifest',
                    ),
                ),
            ),
             'printOrderFiles' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/print-order-files[/:transaction_id][/:payment_method]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Transaction',
                        'action' => 'printOrderFiles',
                    ),
                ),
            ),
            'posCheckoutCartFront' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/poscheckout[/:status]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Postransaction',
                        'action' => 'posCheckoutCartFront',
                    ),
                ),
            ),
            'posSaveUserShippingAddress' => array(
                    'type' => 'segment',
                    'options' => array(
                        'route' => '/pos-save-user-shipping-address[/:addressid]',
                        'defaults' => array(
                            'controller' => 'Transaction\Controller\Postransaction',
                            'action' => 'posSaveUserShippingAddress',
                        ),
                    ),
                ),
            'pos-get-total-cart' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/pos-get-total-cart',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Postransaction',
                        'action' => 'posGetTotalCart',
                    ),
                ),
            ), 
            'posOrderreviewCartFront' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/pos-orderreview-cart-front',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Postransaction',
                        'action' => 'posOrderreviewCartFront',
                    ),
                ),
            ),                        
            'payPosTransaction' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/pay-pos-transaction',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Postransaction',
                        'action' => 'payPosTransaction',
                    ),
                ),
            ),
            'PosShippingMethodFront' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/pos-shipping-method-front',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Postransaction',
                        'action' => 'PosShippingMethodFront',
                    ),
                ),
            ),
            'getPosShippingMethod' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-pos-shipping-method',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Postransaction',
                        'action' => 'getPosShippingMethod',
                    ),
                ),
            ),
            'posTransactionZipcode' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/pos-transaction-zipcode',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Postransaction',
                        'action' => 'posTransactionZipcode',
                    ),
                ),
            ),
            'posplaceOrder' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/pos-place-order',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Postransaction',
                        'action' => 'posplaceOrder',
                    ),
                ),
            ),
            'posTransactionThanks' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/pos-transaction-thanks[/:is_email][/:transaction_id]',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Postransaction',
                        'action' => 'posTransactionThanks',
                    ),
                ),
            ),
            
            'deleteWohCart' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-woh-cart',
                    'defaults' => array(
                        'controller' => 'Transaction\Controller\Postransaction',
                        'action' => 'deleteWohCart',
                    ),
                ),
            ),                                                         
        )
    ),
    'navigation' => array(
        'default' => array(
            array(
                'label' => 'Transaction',
                'route' => 'get-crm-transactions',
                'pages' => array(
                    array(
                        'label' => 'Search',
                        'route' => 'get-crm-transactions',
                        'action' => 'addCrmTransaction',
                        'pages' => array(
                            array(
                                'label' => 'Create Transaction',
                                'route' => 'create-crm-transaction',
                                'action' => 'addCrmTransaction',
                                'pages' => array(
                                    array(
                                        'label' => 'New Transaction',
                                        'route' => 'get-crm-cart-products',
                                        'action' => 'getCrmCartProducts'
                                    )
                                )
                            ),
                            array(
                                'label' => 'View Transaction',
                                'route' => 'viewTransaction',
                                'action' => 'viewTransaction',
                            ),
                            array(
                                'label' => 'View Shipment',
                                'route' => 'viewShipment',
                                'action' => 'viewShipment',
                            ),
                            array(
                                'label' => 'Create/Update Batch',
                                'route' => 'selectBatchId',
                                'action' => 'selectBatchId',
                            ),
                            array(
                                'label' => 'Change Log',
                                'route' => 'transactionchangelog'
                            ),
                        )
                    )
            )),
            array(
                'label' => 'Wall Of Honor Entries',
                'route' => 'wohEntries',
                'pages' => array(
                    array(
                        'label' => 'Bulk panel assignment',
                        'route' => 'wohBulkPanelAssignment'
                    )
                ),
            ),
            array(
                'label' => 'Export to Sage',
                'route' => 'get-export-to-sage',
                'pages' => array(
                    array(
                        'label' => 'Create Batch',
                        'route' => 'create-sage-batch'
                    )
                ),
            ),
            array(
                'label' => 'RMA',
                'route' => 'crmRma',
                'pages' => array(
                    array(
                        'label' => 'Search',
                        'route' => 'crmRma',
                        'action' => 'getCrmRma',
                        'pages' => array(
                             array(
                                'label' => 'Create',
                                'route' => 'createRma',
                                'pages' => array(),
                             ),
                             array(
                                    'label' => 'View Rma',
                                    'route' => 'viewRmaDetail',
                                    'action' => 'viewRmaDetail',
                                ),
                                array(
                                    'label' => 'Edit Rma',
                                    'route' => 'editRmaDetail',
                                    'action' => 'editRmaDetail',
                                ),
                                array(
                                    'label' => 'Change Log',
                                    'route' => 'rmachangelog',
                                    'action' => 'change-log',
                                ),
                            
                        ),
                    ),
                   
                    
                ),
            ),
            array(
                'label' => 'Bulk Donations List',
                'route' => 'donationbulkentries',
                'pages' => array(
                    array(
                        'label' => 'Search',
                        'route' => 'donationbulkentries',
                        'pages' => array(
                            array(
                                'label' => 'View',
                                'route' => 'getbatchdetail',
                                'action' => 'getBatchDetail',
                            ),
                            array(
                                'label' => 'Add Donations',
                                'route' => 'createbatch',
                                'action' => 'createBatch',
                            ),
                        ),
                    ),
                ),
            ),
        )
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'transaction_messages' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
    'transaction_config' => array(
        'origin_country' => "United States of America",
        /* 'SHIP_API_URL' => 'http://198.61.196.58/SendSuite%20Live%20Shipping/Services/XmlService.aspx',
          'SHIP_API_USERNAME' => 'Administrator',
          'SHIP_API_REQUEST_USERNAME' => 'admin',
          'SHIP_API_PASSWORD' => 'G7FuswLVxysy',
          'SHIP_PACKAGE_TYPE' => 27,
          'return_ship_address_info' => array(
          'address_one' => '6000 route Trans-Canada',
          'city' => 'Pointe-Claire',
          'state' => 'QC',
          'zip_code' => 'H9R 1C5',
          'country' => 'CA'
          ),
          'domestic_config_info' => array(
          'shippings_types' => array(
          array('carrier_id' => 12, 'service_type' => 77, 'description' => '', 'web_selection' => '5-7 Days'),
          array('carrier_id' => 12, 'service_type' => 75, 'description' => '', 'web_selection' => 'Overnight'),
          array('carrier_id' => 12, 'service_type' => 80, 'description' => '', 'web_selection' => '3 – 5 days')
          ),
          'lbs_check' => 5,
          'surchange_under_5' => 20,
          'surchange_above_5' => 20,
          'shipping_handling_under_5' => 5,
          'shipping_handling_above_5' => 10,
          ),
          'usps_iop_config_info' => array(
          'shippings_types' => array(
          array('carrier_id' => 13, 'service_type' => 463, 'description' => 'Overnight', 'web_selection' => ''),
          array('carrier_id' => 13, 'service_type' => 86, 'description' => '3-5 days', 'web_selection' => ''),
          array('carrier_id' => 13, 'service_type' => 85, 'description' => '5-7 days', 'web_selection' => '')
          ),
          'lbs_check' => 5,
          'surchange_under_5' => 20,
          'surchange_above_5' => 20,
          'shipping_handling_under_5' => 5,
          'shipping_handling_above_5' => 10,
          ),
          'international_config_info' => array(
          'shippings_types' => array(
          array('carrier_id' => 13, 'service_type' => 92, 'description' => 'Express Mail International', 'web_selection' => ''),
          array('carrier_id' => 13, 'service_type' => 94, 'description' => 'Priority Mail International', 'web_selection' => ''),
          array('carrier_id' => 13, 'service_type' => 289, 'description' => 'First Class Mail International', 'web_selection' => '')
          ),
          'shipping_types_priority' => array(
          array('carrier_id' => 13, 'service_type' => 94, 'description' => '3-5 days', 'web_selection' => '')
          ),
          'shipping_types_first_class' => array(
          array('carrier_id' => 13, 'service_type' => 289, 'description' => '5-7 days', 'web_selection' => '')
          ),
          'lbs_check' => 4,
          'surchange_under_5' => 20,
          'surchange_above_5' => 20,
          'shipping_handling_under_5' => 5,
          'shipping_handling_above_5' => 10,
          ), */

        'apo_po_state' => array(
            'AA' => 'Americas - AA',
            'AE' => 'Europe - AE',
            'AP' => 'Pacific - AP'
        )
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'transaction' => __DIR__ . '/../view'
        ),
        'template_map' => array(
            'layout/custom' => __DIR__ . '/../../module/Common/layout/crm.phtml',
            'error/404' => __DIR__ . '/../../module/Common/error/404.phtml',
            'error/index' => __DIR__ . '/../../module/Common/error/index.phtml',
        ),
    ),
    'global_config' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
);
