<?php

/**
 * This file is used for initialize all obj
 * @package    Transaction
 * @author     Icreon Tech - DG
 */

namespace Transaction;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\I18n\Translator\Translator;
use Zend\Validator\AbstractValidator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Transaction\Model\Transaction;
use Transaction\Model\TransactionTable;
use Transaction\Model\Batch;
use Transaction\Model\BatchTable;
use Transaction\Model\Donation;
use Transaction\Model\DonationTable;
use Transaction\Model\Postransaction;
use Transaction\Model\PostransactionTable;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;

/**
 * This file is used for initialize all obj
 * @package    Transaction
 * @author     Icreon Tech - DG
 */
class Module implements AutoloaderProviderInterface, ConfigProviderInterface, ViewHelperProviderInterface {

    public function onBootstrap(MvcEvent $e) {
        //error_reporting(E_ALL);
        $translator = $e->getApplication()->getServiceManager()->get('translator');
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $translator->addTranslationFile(
                'phpArray', './module/Transaction/languages/en/language.php', 'default', 'en_US'
        );
        $viewModel = $e->getApplication()->getMvcEvent()->getViewModel();
        $sm = $e->getApplication()->getServiceManager();
        $this->_config = $sm->get('Config');
		$viewModel->imageFilePath = $imageFilePath = $this->_config['img_file_path']['path'];
        //AbstractValidator::setDefaultTranslator($translator);
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Get Autoloader Configuration
     * @return array
     */
    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    /**
     * Get Service Configuration
     *
     * @return array
     */
    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Transaction\Model\TransactionTable' => function($sm) {
                    $tableGateway = $sm->get('TransactionTableGateway');
                    $table = new TransactionTable($tableGateway);
                    return $table;
                },
                'TransactionTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Transaction($dbAdapter));
                    return new TableGateway('tbl_cas_Pledge', $dbAdapter, null, $resultSetPrototype);
                },
                'Transaction\Model\BatchTable' => function($sm) {
                    $tableGateway = $sm->get('BatchTableGateway');
                    $table = new BatchTable($tableGateway);
                    return $table;
                },
                'BatchTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Batch($dbAdapter));
                    return new TableGateway('tbl_tra_payment_receives', $dbAdapter, null, $resultSetPrototype);
                },  
		'Transaction\Model\DonationTable' => function($sm) {
                    $tableGateway = $sm->get('DonationTableGateway');
                    $table = new DonationTable($tableGateway);
                    return $table;
                },
                'DonationTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Donation($dbAdapter));
                    return new TableGateway('tbl_tra_payment_receives', $dbAdapter, null, $resultSetPrototype);
                },
                'Transaction\Model\PostransactionTable' => function($sm) {
                    $tableGateway = $sm->get('PostransactionTableGateway');
                    $table = new PostransactionTable($tableGateway);
                    return $table;
                },
                'PostransactionTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Postransaction($dbAdapter));
                    return new TableGateway('tbl_user_transactions', $dbAdapter, null, $resultSetPrototype);
                },                        
                'dbAdapter' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    return $dbAdapter;
                },
            ),
        );
    }

    /**
     * Get View Helper Configuration
     *
     * @return array
     */
    public function getViewHelperConfig() {
        return array(
            'factories' => array(
            // the array key here is the name you will call the view helper by in your view scripts
//                'absoluteUrl' => function($sm) {
//                    $locator = $sm->getServiceLocator(); // $sm is the view helper manager, so we need to fetch the main service manager
//                    return new AbsoluteUrl($locator->get('Request'));
//                },
            ),
        );
    }

    /**
     * Get Controller Configuration
     *
     * @return array
     */
    public function getControllerConfig() {
        return array();
    }

    public function loadConfiguration(MvcEvent $e) {
        $application = $e->getApplication();
        $serviceManager = $application->getServiceManager();
        $sharedManager = $application->getEventManager()->getSharedManager();
        $router = $serviceManager->get('router');
        $request = $serviceManager->get('request');
    }

    public function loadCommonViewVars(MvcEvent $e) {
        $e->getViewModel()->setVariables(array(
            'auth' => $e->getApplication()->getServiceManager()->get('AuthService')
        ));
    }

}
