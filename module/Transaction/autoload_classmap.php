<?php

/**
 * This file define all controller and Models
 * @package    User
 * @author     Icreon Tech - DG
 */
return array(
    'Transaction\Module' => __DIR__ . '/Module.php',
    'Transaction\Controller\TransactionController' => __DIR__ . '/src/Transaction/Controller/TransactionController.php',
    'Transaction\Model\Transaction' => __DIR__ . '/src/Transaction/Model/Transaction.php',
    'Transaction\Model\TransactionTable' => __DIR__ . '/src/Transaction/Model/TransactionTable.php',
    'Transaction\Model\Batch' => __DIR__ . '/src/Transaction/Model/Batch.php',
    'Transaction\Model\BatchTable' => __DIR__ . '/src/Transaction/Model/BatchTable.php',
    'Transaction\Model\Donation' => __DIR__ . '/src/Transaction/Model/Donation.php',
    'Transaction\Model\DonationTable' => __DIR__ . '/src/Transaction/Model/DonationTable.php',
    'Transaction\Model\Postransaction' => __DIR__ . '/src/Transaction/Model/Postransaction.php',
    'Transaction\Model\PostransactionTable' => __DIR__ . '/src/Transaction/Model/PostransactionTable.php',	
);
?>