<?php

/**
 * This page is used for updating transaction status form.
 * @package    Transaction
 * @author     Icreon Tech - AS
 */

namespace Transaction\Form;

use Zend\Form\Form;

/**
 * This class is used for updating transaction status form.
 * @package    Transaction
 * @author     Icreon Tech - AS
 */
class TransactionStatusForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('update_transaction_status');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'auto');
        $this->add(array(
            'type' => 'Select',
            'name' => 'transaction_status',
            'attributes' => array(
                'id' => 'transaction_status',
                'class' => 'e1 select-w-320',
            )
        ));
    }

}
