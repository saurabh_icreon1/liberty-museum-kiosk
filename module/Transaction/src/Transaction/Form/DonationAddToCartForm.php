<?php

/**
 * This page is used to donaction add to cart
 * @package    Transaction_DonationAddToCartForm
 * @author     Icreon Tech - DT
 */

namespace Transaction\Form;

use Zend\Form\Form;

/**
 * This form is used to donation add to cart
 * @package    DonationAddToCartForm
 * @author     Icreon Tech - DT
 */
class DonationAddToCartForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('donation_add_to_cart');
        $this->setAttribute('method', 'post');
        
        $this->add(array(
            'name' => 'other_amount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'other_amount',
                'class' => 'amount',
                'placeholder' => 'Amount'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'amount',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'amount',
                'class' => 'e1'
                
            )
        ));
        /*$this->add(array(
            'name' => 'amount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'amount'
            )
        ));*/
        $this->add(array(
            'name' => 'campaign',
            'attributes' => array(
                'type' => 'text',
                'id' => 'campaign',
                'class' => 'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'campaign_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'campaign_id'
            )
        ));
        
       $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'matching_gift',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'matching_gift',
                'class'=>'checkbox e2'
            )
        ));
       
       
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'matching_gift_front',
            'options' => array(
                'value_options' => array(
                  '0'=>array(
                            'label'=>"No",
                            'label_attributes' => array('class'=>'css-label','for'=>'no'),
                            'value' => '0',
                            'attributes' => array(
                                'id' => 'no',
                            ),
                        ),
                  '1'=>array(
                            'label'=>"Yes",
                            'label_attributes' => array('class'=>'css-label','for'=>'yes'),
                            'value' => '1',
                            'attributes' => array(
                                'id' => 'yes',
                            ),
                        )                  
                ),
            ),
            'attributes' => array(
                'id' => 'matching_gift_front',
                'value' => '', //set checked to '1'
                'class'=>'css-checkbox',
               
            )
        ));
       
       $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'program_id',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'program_id',
                'class' => 'e1'
                
            )
        ));
        
         $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'campaign_id_front',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'campaign_id_front',
                'class' => 'e1'
               
            )
        ));
       
       
        $this->add(array(
            'name' => 'company',
            'attributes' => array(
                'type' => 'text',
                'id' => 'company',
                'class' => 'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'company_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'company_id'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'in_honoree',
            'options' => array(
                'value_options' => array(
                  '1'=>'In Honor of',
                  '2'=>'In Memory of',
                  '3'=>'None'
                ),
            ),
            'attributes' => array(
                'id' => 'in_honoree',
                'value' => '3', //set checked to '1'
                'class'=>'css-checkbox',
                'onClick'=>'showHonoreeName(this)'
            )
        ));
        $this->add(array(
            'name' => 'honor_memory_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'honor_memory_name',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_notify',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'is_notify',
                'class'=>'checkbox e2'
            )
        ));
        $this->add(array(
            'name' => 'donationaddtocart',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Add To Cart',
                'id' => 'donationaddtocart',
                'class' => 'button cart-btn save-btn'
            ),
        ));
    }

}