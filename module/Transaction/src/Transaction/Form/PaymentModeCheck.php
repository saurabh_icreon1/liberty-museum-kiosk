<?php

/**
 * This page is used to Transaction add more payment mode check
 * @package    Transaction_PaymentModeCheck
 * @author     Icreon Tech - DT
 */

namespace Transaction\Form;

use Zend\Form\Form;

/**
 * This form is used to Transaction add more payment mode check
 * @package    PaymentModeCheck
 * @author     Icreon Tech - DT
 */
class PaymentModeCheck extends Form {

    public function __construct($name = null) {
        parent::__construct('payment_mode_check');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'payment_mode_chk_amount[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'payment_mode_chk_amount_1',
                'onblur' => 'calculateTransaction()'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'payment_mode_chk_type[]',
            'options' => array(
                'value_options' => array(
                )
            ),
            'attributes' => array(
                'id' => 'payment_mode_chk_type_1',
                'class' => 'e1'
            )
        ));
        $this->add(array(
            'name' => 'payment_mode_chk_num[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'payment_mode_chk_num_1'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'payment_mode_chk_id_type[]',
            'options' => array(
                'value_options' => array(
                )
            ),
            'attributes' => array(
                'id' => 'payment_mode_chk_id_type_1',
                'class' => 'e1'
            )
        ));
        $this->add(array(
            'name' => 'payment_mode_chk_id[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'payment_mode_chk_id_1'
            )
        ));
        $this->add(array(
            'name' => 'check_batch_id[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'check_batch_id_1',
                'class' => 'width-200 search-icon'
            )
        ));

        $this->add(array(
            'name' => 'payment_check_batch_id[]',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'payment_check_batch_id_1'
            )
        ));
    }

}