<?php

/**
 * This form is used for Create Transaction.
 * @package    Transaction
 * @author     Icreon Tech - DT
 */

namespace Transaction\Form;

use Zend\Form\Form;

/**
 * This form is used for Create Transaction.
 * @package    Transaction
 * @author     Icreon Tech - DT
 */
class CreateTrasactionForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('create-transaction');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'user_id',
            'attributes' => array(
                'type' => 'hidden'
            )
        ));

        /* Coupon code elements */
        $this->add(array(
            'name' => 'coupon_code',
            'attributes' => array(
                'type' => 'text',
                'id' => 'coupon_code',
                'class' => 'search-icon',
                'onblur' => 'if(this.value==""){$("#promotion_id").val("");refreshCartData()}'
            )
        ));
        $this->add(array(
            'name' => 'promotion_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'promotion_id',
            )
        ));
        $this->add(array(
            'name' => 'is_promotion_applied',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'is_promotion_applied',
            )
        ));
        $this->add(array(
            'name' => 'coupon_apply',
            'attributes' => array(
                'type' => 'button',
                'id' => 'coupon_apply',
                'class' => 'save-btn',
                'value' => 'Apply Coupon',
                'onclick' => 'applyCoupon()'
            )
        ));
        $this->add(array(
            'name' => 'campaign_code',
            'attributes' => array(
                'type' => 'text',
                'id' => 'campaign_code',
                'class' => 'search-icon',
                'placeholder' => 'Search Campaign',
                'onblur' => 'if(this.value==""){$("#campaign_id").val("");refreshCartData()}'
            )
        ));
        $this->add(array(
            'name' => 'campaign_code_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'campaign_code_id',
            )
        ));
        $this->add(array(
            'name' => 'campaign_apply',
            'attributes' => array(
                'type' => 'button',
                'id' => 'campaign_apply',
                'class' => 'save-btn',
                'value' => 'Apply Campaign',
                'onclick' => 'applyCampaign()'
            )
        ));
        /* End coupon code elements */

        /* Billing address elements */

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'billing_existing_contact',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'billing_existing_contact',
                'class' => 'e1',
                'onchange' => "getAddressInfo(this.value,'billing')"
            )
        ));

        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'billing_title',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'billing_title',
                'class' => 'e1'
            )
        ));
        $this->add(array(
            'name' => 'billing_first_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_first_name'
            )
        ));
        $this->add(array(
            'name' => 'custom_shipping',
            'attributes' => array(
                'type' => 'text',
                'id' => 'custom_shipping',
                'onBlur'=>'addShippingValue()',
                'style'=>'display:none;',
                'class'=>'width-125 m-l-20'
            )
        ));
        $this->add(array(
            'name' => 'billing_last_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_last_name',
            )
        ));
        $this->add(array(
            'name' => 'billing_company',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_company',
            )
        ));

        $this->add(array(
            'name' => 'billing_address',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_address',
            )
        ));
        $this->add(array(
            'name' => 'billing_city',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_city',
            )
        ));
        $this->add(array(
            'name' => 'billing_state',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'billing_state',
            )
        ));

        $this->add(array(
            'name' => 'billing_state_text',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_state_text',
                'onblur' => 'changeState(this.value,"billing")'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'billing_state_select',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'billing_state_select',
                'class' => 'e1',
                'onchange' => 'changeState(this.value,"billing")'
            )
        ));

        $this->add(array(
            'name' => 'billing_zip_code',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_zip_code',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'billing_country',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'billing_country',
                'class' => "e1",
                'onChange' => 'changeShippingMethod(this.value,"billing")'
            )
        ));
       $this->add(array(
            'name' => 'billing_country_code',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_country_code',
                'class' => 'width-125 m-r-20'
            )
        ));
       $this->add(array(
            'name' => 'billing_area_code',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_area_code',
                'class' => 'width-80 m-r-20'
            )
        ));
        $this->add(array(
            'name' => 'billing_country_code',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_country_code',
                'class' => 'width-125 m-r-20'
            )
        ));
        $this->add(array(
            'name' => 'billing_area_code',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_area_code',
                'class' => 'width-80 m-r-20'
            )
        ));
        $this->add(array(
            'name' => 'billing_phone',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_phone',
                'class' => 'width-155'
            )
        ));
        $this->add(array(
            'name' => 'billing_phone_info_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'billing_phone_info_id'
            )
        ));

        /* End billing address elements */

        /* Shipping address elements */
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'pick_up',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'pick_up',
                'class' => 'checkbox e2'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'billing_shipping_same',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'billing_shipping_same',
                'class' => 'checkbox e2'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'shipping_existing_contact',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'shipping_existing_contact',
                'class' => 'e1',
                'onchange' => "getAddressInfo(this.value,'shipping')"
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'shipping_title',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'shipping_title',
                'class' => 'e1'
            )
        ));
        $this->add(array(
            'name' => 'shipping_first_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'shipping_first_name' //,
                //'readonly'=>'readonly'
            )
        ));

        $this->add(array(
            'name' => 'shipping_last_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'shipping_last_name' //,
               //'readonly'=>'readonly'                
            )
        ));
        $this->add(array(
            'name' => 'shipping_company',
            'attributes' => array(
                'type' => 'text',
                'id' => 'shipping_company',
            )
        ));

        $this->add(array(
            'name' => 'shipping_address',
            'attributes' => array(
                'type' => 'text',
                'id' => 'shipping_address',
            )
        ));
        $this->add(array(
            'name' => 'shipping_city',
            'attributes' => array(
                'type' => 'text',
                'id' => 'shipping_city',
            )
        ));
        $this->add(array(
            'name' => 'shipping_state_text',
            'attributes' => array(
                'type' => 'text',
                'id' => 'shipping_state_text',
                'onblur' => 'changeState(this.value,"shipping")'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'shipping_state_select',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'shipping_state_select',
                'class' => 'e1',
                'onchange' => 'changeState(this.value,"shipping")'
            )
        ));
        $this->add(array(
            'name' => 'shipping_state',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'shipping_state',
            )
        ));

        $this->add(array(
            'name' => 'shipping_zip_code',
            'attributes' => array(
                'type' => 'text',
                'id' => 'shipping_zip_code'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'shipping_country',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'shipping_country',
                'class' => "e1",
                'onChange' => 'changeShippingMethod(this.value,"shipping")'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'apo_po_shipping_country',
            'options' => array(
                'value_options' => array(
                    '228' => 'UNITED STATES'
                )
            ),
            'attributes' => array(
                'id' => 'apo_po_shipping_country',
                'class' => "e1"
            )
        ));
        $this->add(array(
            'name' => 'shipping_country_code',
            'attributes' => array(
                'type' => 'text',
                'id' => 'shipping_country_code',
                'class' => 'width-125 m-r-20'
            )
        ));

        $this->add(array(
            'name' => 'shipping_area_code',
            'attributes' => array(
                'type' => 'text',
                'id' => 'shipping_area_code',
                'class' => 'width-80 m-r-20'
            )
        ));
        $this->add(array(
            'name' => 'shipping_phone',
            'attributes' => array(
                'type' => 'text',
                'id' => 'shipping_phone',
                'class' => 'width-155'
            )
        ));
        $this->add(array(
            'name' => 'shipping_phone_info_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'shipping_phone_info_id'
            )
        ));
        $this->add(array(
            'name' => 'shipping_location_type',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'shipping_location_type',
            )
        ));

        $this->add(array(
            'name' => 'billing_location_type',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'billing_location_type',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'save_shipping_address',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'save_shipping_address',
                'class' => 'checkbox e2',
                'checked'=>'checked'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'save_billing_address',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'save_billing_address',
                'class' => 'checkbox e2',
                //'checked'=>'checked'
            )
        ));
        /* End shipping address elements */

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_email_confirmation',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'is_email_confirmation',
                'class' => 'checkbox e2'
            )
        ));



        /* Payment mode elementws */
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'payment_mode_cash',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'payment_mode_cash',
                'class' => 'checkbox e2',
                'onchange' => 'calculateTransaction()'
            )
        ));

        $this->add(array(
            'name' => 'payment_mode_cash_amount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'payment_mode_cash_amount',
                'onblur' => 'calculateTransaction()'
            )
        ));
        $this->add(array(
            'name' => 'cash_batch_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'cash_batch_id',
                'class' => 'width-200 search-icon'
            )
        ));

        $this->add(array(
            'name' => 'payment_cash_batch_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'payment_cash_batch_id'
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'payment_mode_chk',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'payment_mode_chk',
                'class' => 'checkbox e2',
                'onchange' => 'calculateTransaction()'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'payment_mode_credit',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'payment_mode_credit',
                'class' => 'checkbox e2',
                'onchange' => 'calculateTransaction()'
            )
        ));

        $this->add(array(
            'type' => 'hidden',
            'name' => 'transaction_ids',
            'attributes' => array(
                'id' => 'transaction_ids'
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'transaction_amounts',
            'attributes' => array(
                'id' => 'transaction_amounts'
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'customer_profile_ids',
            'attributes' => array(
                'id' => 'customer_profile_ids'
            )
        ));

        /* End payment mode elements */
        $this->add(array(
            'name' => 'notes',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'notes',
                'class' => 'width-90',
            )
        ));

        /* $this->add(array(
          'type' => 'Zend\Form\Element\Select',
          'name' => 'shipping_method',
          'options' => array(
          ),
          'attributes' => array(
          'id' => 'shipping_method',
          'class' => 'e1',
          'onChange' => 'refreshCartData()'
          )
          )); */

        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'shipping_method',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'shipping_method',
                'class' => 'e3 shipping_method',
                'onClick' => 'refreshCartData()'
            )
        ));
        /* $this->add(array(
          'type' => 'Zend\Form\Element\Checkbox',
          'name' => 'is_apo_po',
          'checked_value' => '1',
          'unchecked_value' => '0',
          'attributes' => array(
          'id' => 'is_apo_po',
          'class' => 'checkbox e2',
          'onChange' => 'changeShippingMethod()'
          )
          )); */

        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'is_apo_po',
            'options' => array(
                "value_options" => array(
                    '1' => 'Residential/Corporate',
                    '2' => 'PO',
                    '3' => 'APO'
                    )
            ),
            'attributes' => array(
                'id' => 'is_apo_po',
                'class' => 'e3 is_apo_po',
                'onchange' => 'changeApoPo(this.value)',
                'value' => '1'
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'apo_po_state',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'apo_po_state',
                'class' => 'e1',
            )
        ));



        $this->add(array(
            'name' => 'totalProducts',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'totalProducts'
            )
        ));
        $this->add(array(
            'name' => 'shipCompleteStatus',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'shipCompleteStatus'
            )
        ));

        $this->add(array(
            'name' => 'user_id',
            'attributes' => array(
                'type' => 'hidden'
            )
        ));
		
	 $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'payment_mode_invoice',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'payment_mode_invoice',
                'class' => 'checkbox e2',
                //'onchange' => 'calculateTransaction()'
            )
        ));

		$this->add(array(
            'name' => 'invoice_number',
            'attributes' => array(
                'type' => 'text',
                'id' => 'invoice_number',
            )
        ));
		$this->add(array(
            'name' => 'amount_invoice_number',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'amount_invoice_number',
            )
        ));
		$this->add(array(
            'name' => 'invoice_batch_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'invoice_batch_id',
                'class' => 'width-200 search-icon'
            )
        ));

        $this->add(array(
            'name' => 'payment_invoice_batch_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'payment_invoice_batch_id'
            )
        ));
        $this->add(array(
            'name' => 'transactionsubmit',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'transactionsubmit',
                'class' => 'save-btn',
                'value' => 'Submit Transaction'
            )
        ));

        $this->add(array(
            'name' => 'transactioncancel',
            'attributes' => array(
                'type' => 'button',
                'id' => 'transactioncancel',
                'class' => 'cancel-btn m-l-20',
                'value' => 'CANCEL',
                'onclick' => 'window.location.href="/crm-transactions";'
            )
        ));
    }

}
