<?php

/**
 * This form is used to search contacts on create transaction page
 * @package    Transaction
 * @author     Icreon Tech - DT
 */

namespace Transaction\Form;

use Zend\Form\Form;

class EditWohEntryForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('editwohentries');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'panel_number',
            'attributes' => array(
                'type' => 'text',
                'id' => 'panel_number'
            )
        ));
        
        $this->add(array(
            'name' => 'woh_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'woh_id'
            )
        ));
        
        $this->add(array(
            'name' => 'update_entry',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Update',
                'id' => 'update_entry',
                'class' => 'save-btn'
            ),
        ));
    }

}