<?php

/**
 * This form is used to update transaction batch ID.
 * @package    Setting
 * @author     Icreon Tech - SR
 */

namespace Transaction\Form;

use Zend\Form\Form;

/**
 * This form is used to update transaction batch Id
 * @package    Setting
 * @author     Icreon Tech - SR
 */
class UpdateTransactionBatchForm extends Form {

    public function __construct($name = null) {

        parent::__construct('search-template');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'batch_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'batch_id',
                'class' => 'width-200 search-icon'
            )
        ));

        $this->add(array(
            'name' => 'payment_batch_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'payment_batch_id'
            )
        ));

        $this->add(array(
            'name' => 'search',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Update Batch',
                'id' => 'search',
                'class' => 'save-btn',
            ),
        ));
    }

}
