<?php

/**
 * This page is used for updating transaction status form.
 * @package    Transaction
 * @author     Icreon Tech - AS
 */

namespace Transaction\Form;

use Zend\Form\Form;

/**
 * This class is used for creating rma
 * @package    Transaction
 * @author     Icreon Tech - AS
 */
class CreateRmaForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('create_rma');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'type' => 'text',
            'name' => 'transaction_id',
            'attributes' => array(
                'id' => 'transaction_id',
                'class' => 'search-icon',
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'rma_product_id[]',
            'attributes' => array(
                'id' => 'product_amount',
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'product_amount[]',
            'attributes' => array(
                'id' => 'product_amount',
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'product_tax[]',
            'attributes' => array(
                'id' => 'product_tax',
            )
        )); 
        $this->add(array(
            'type' => 'hidden',
            'name' => 'product_shipping[]',
            'attributes' => array(
                'id' => 'product_shipping',
            )
        )); 
        $this->add(array(
            'type' => 'hidden',
            'name' => 'qty_shipped[]',
            'attributes' => array(
                'id' => 'qty_shipped',
            )
        ));         
        $this->add(array(
            'type' => 'hidden',
            'name' => 'product_handling[]',
            'attributes' => array(
                'id' => 'product_handling',
            )
        )); 
        $this->add(array(
            'type' => 'hidden',
            'name' => 'product_discount[]',
            'attributes' => array(
                'id' => 'product_discount',
            )
        ));         
        $this->add(array(
            'type' => 'hidden',
            'name' => 'product_total_qty[]',
            'attributes' => array(
                'id' => 'product_total_qty',
            )
        )); 
        
        $this->add(array(
            'type' => 'hidden',
            'name' => 'shipment_id[]',
            'attributes' => array(
                'id' => 'shipment_id',
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'product_id[]',
            'attributes' => array(
                'id' => 'product_id',
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'product_type[]',
            'attributes' => array(
                'id' => 'product_type',
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'transaction_product_id[]',
            'attributes' => array(
                'id' => 'transaction_product_id',
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'transaction_product_type[]',
            'attributes' => array(
                'id' => 'transaction_product_type',
            )
        ));
        $this->add(array(
            'type' => 'text',
            'name' => 'returned_qty[]',
            'attributes' => array(
                'id' => 'returned_qty',
            )
        ));
        $this->add(array(
            'type' => 'text',
            'name' => 'returned_reason[]',
            'attributes' => array(
                'id' => 'returned_reason',
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'item_condition[]',
            'attributes' => array(
                'id' => 'item_status',
                'class' => 'e1 select-w-320',
            )
        ));
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'is_ship_return[]',
            'attributes' => array(
                'id' => 'is_ship_return',
                'value' => '1'
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'rma_action',
            'attributes' => array(
                'id' => 'rma_action',
                'class' => 'e1',
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'rma_status',
            'attributes' => array(
                'id' => 'rma_status',
                'class' => 'e1',
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'rma_id',
            'attributes' => array(
                'id' => 'rma_id',
            )
        ));
        $this->add(array(
            'type' => 'text',
            'name' => 'bank_name',
            'attributes' => array(
                'id' => 'bank_name',
            )
        ));
        $this->add(array(
            'type' => 'text',
            'name' => 'check_no',
            'attributes' => array(
                'id' => 'check_no',
            )
        ));
        $this->add(array(
            'type' => 'text',
            'name' => 'amount',
            'attributes' => array(
                'id' => 'amount',
            )
        ));
        $this->add(array(
            'type' => 'text',
            'name' => 'refund_shipping',
            'attributes' => array(
                'id' => 'refund_shipping',
            )
        ));
        $this->add(array(
            'type' => 'text',
            'name' => 'refund_tax',
            'attributes' => array(
                'id' => 'refund_tax',
            )
        ));                
        $this->add(array(
            'type' => 'text',
            'name' => 'transaction',
            'attributes' => array(
                'id' => 'transaction',
            )
        ));
        
        $this->add(array(
            'name' => 'savebutton',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'savebutton',
                'class' => 'save-btn',
                'value' => 'Save',
                'style' => 'display:none'
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'returned_orignal_qty[]',
            'attributes' => array(
                'id' => 'returned_orignal_qty',
            )
        ));
         $this->add(array(
            'name' => 'note',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'note',
                'class' => 'width-90'
            )
        )); 
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'notify_note',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'class' => 'checkbox',
                'id' => 'notify_note',
                'class' => 'e2',
                'value' => 'Private'
            )
        ));               
        $this->add(array(
            'name' => 'cancelbutton',
            'attributes' => array(
                'type' => 'button',
                'id' => 'cancelbutton',
                'class' => 'cancel-btn m-l-20',
                'value' => 'CANCEL',
                'style' => 'display:none',
                'onclick' => 'window.location.href="/crm-rma";'
            )
        ));
    }

}
