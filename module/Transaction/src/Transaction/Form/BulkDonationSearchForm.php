<?php

/**
 * This form is used to search contacts on create transaction page
 * @package    Transaction
 * @author     Icreon Tech - DT
 */

namespace Transaction\Form;

use Zend\Form\Form;

class BulkDonationSearchForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('bulk_donation');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'batch_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'batch_id',
				'class' => 'search-icon',
            ),
        ));
		$this->add(array(
            'name' => 'payment_batch_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'payment_batch_id'
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'batch_date_range',
            'attributes' => array(
                'id' => 'batch_date_range',
                'class' => 'e1 select-w-320',
                'onChange' => 'showDate(this.id,"date_range_div")'
            )
        ));
        $this->add(array(
            'name' => 'batch_date_from',
            'attributes' => array(
                'type' => 'text',
                'class' => 'width-125 cal-icon',
                'id' => 'batch_date_from',
            )
        ));
        $this->add(array(
            'name' => 'batch_date_to',
            'attributes' => array(
                'type' => 'text',
                'class' => 'width-125 cal-icon',
                'id' => 'batch_date_to',
            )
        ));
        $this->add(array(
            'name' => 'amount_from',
            'attributes' => array(
                'type' => 'text',
                'class' => 'width-125',
                'id' => 'amount_from',
            )
        ));
        $this->add(array(
            'name' => 'amount_to',
            'attributes' => array(
                'type' => 'text',
                'class' => 'width-125',
                'id' => 'amount_to',
            )
        ));
        $this->add(array(
            'name' => 'created_by_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'created_by_name',
                'class' => 'search-icon',
            )
        ));
        
        $this->add(array(
            'name' => 'added_by',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'added_by'
            )
        ));

        $this->add(array(
            'name' => 'searchbutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Search',
                'id' => 'searchbutton',
                'class' => 'save-btn'
            ),
        ));
    }

}