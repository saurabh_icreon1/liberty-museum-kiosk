<?php

/**
 * This page is used to add to cart product type form
 * @package    Transaction_AddToCartForm
 * @author     Icreon Tech - DT
 */

namespace Transaction\Form;

use Zend\Form\Form;

/**
 * This form is used to add to cart product type form
 * @package    AddToCartForm
 * @author     Icreon Tech - DT
 */
class AddToCartForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('add_to_cart');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'user_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'user_id'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'product_mapping_id',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'product_mapping_id',
                'class' => 'e1',
                'onchange' => "getAddToCartData(this.value)"
            )
        ));
    }

}