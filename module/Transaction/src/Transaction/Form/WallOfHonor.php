<?php

/**
 * This page is used to add to cart wall of honor
 * @package    Transaction_WallOfHonor
 * @author     Icreon Tech - DT
 */

namespace Transaction\Form;

use Zend\Form\Form;

/**
 * This form is used to add to cart wall of honor
 * @package    KioskFeeForm
 * @author     Icreon Tech - DT
 */
class WallOfHonor extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('wall_of_honor_form');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'woh_main_product',
            'options' => array(
                'value_options' => array(
                )
            ),
            'attributes' => array(
                'class' => 'e3 honoree',
            )
        ));
        $this->add(array(
            'name' => 'product_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'product_id'
            )
        ));
        $this->add(array(
            'name' => 'product_attribute_option_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'product_attribute_option_id'
            )
        ));
        $this->add(array(
            'name' => 'addToCartButton',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'addToCartButton'
            )
        ));
        $this->add(array(
            'name' => 'first_name_one',
            'attributes' => array(
                'type' => 'text',
                'id' => 'first_name_one',
                'maxlength' => '45'
            )
        ));
        $this->add(array(
            'name' => 'first_name_two',
            'attributes' => array(
                'type' => 'text',
                'id' => 'first_name_two',
                'maxlength' => '45'
            )
        ));
        $this->add(array(
            'name' => 'other_init_one',
            'attributes' => array(
                'type' => 'text',
                'id' => 'other_init_one',
                'maxlength' => '2'
            )
        ));
        $this->add(array(
            'name' => 'other_init_two',
            'attributes' => array(
                'type' => 'text',
                'id' => 'other_init_two',
                'maxlength' => '2'
            )
        ));
        $this->add(array(
            'name' => 'other_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'other_name',
                'maxlength' => '45'
            )
        ));
        $this->add(array(
            'name' => 'last_name_one',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_name_one',
                'maxlength' => '45'
            )
        ));
        $this->add(array(
            'name' => 'last_name_two',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_name_two',
                'maxlength' => '45'
            )
        ));

        $this->add(array(
            'name' => 'first_line',
            'attributes' => array(
                'type' => 'text',
                'id' => 'first_line',
                'maxlength' => '45'
            )
        ));
        $this->add(array(
            'name' => 'second_line',
            'attributes' => array(
                'type' => 'text',
                'id' => 'second_line',
                'maxlength' => '45'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_usa',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'is_usa',
                'class' => 'checkbox e2'
            )
        ));
        $this->add(array(
            'name' => 'other_country',
            'attributes' => array(
                'type' => 'text',
                'id' => 'other_country',
                'maxlength' => '30'
            )
        ));

        $this->add(array(
            'name' => 'another_email',
            'attributes' => array(
                'type' => 'button',
                'id' => 'another_email'
            )
        ));
        $this->add(array(
            'name' => 'donated_by',
            'attributes' => array(
                'type' => 'text',
                'id' => 'donated_by',
                'maxlength' => '30'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'donated_for',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    '1' => 'In Memory of',
                    '2' => 'In Honor of'
                )
            ),
            'attributes' => array(
                'id' => 'donated_for',
                'class' => 'e1'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_duplicate_certificate',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'is_duplicate_certificate',
                'class' => 'checkbox e2'
            )
        ));

        
        $this->add(array(
            'name' => 'duplicate_certificate_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'duplicate_certificate_id'
            )
        ));
        
        $this->add(array(
            'name' => 'duplicate_certificate_qty',
            'attributes' => array(
                'type' => 'text',
                'id' => 'duplicate_certificate_qty',
                'readonly' => 'readonly'
            )
        ));
        $this->add(array(
            'name' => 'duplicate_certificate_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'duplicate_certificate_name'
            )
        ));
        $this->add(array(
            'name' => 'duplicate_certificate_transaction_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'duplicate_certificate_transaction_id'
            )
        ));
        $this->add(array(
            'name' => 'duplicate_certificate_panel_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'duplicate_certificate_panel_id',
            )
        ));
        $this->add(array(
            'name' => 'duplicate_certificate_search',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Search',
                'id' => 'duplicate_certificate_search',
                'class' => 'save-btn',
                'onclick' => 'getBioPersonalizeSearch(this.id)'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_related_product',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'is_related_product',
                'class' => 'checkbox e2'
            )
        ));

        $this->add(array(
            'name' => 'related_product_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'related_product_name',
                'class' => 'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'related_product_name_ids',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'related_product_name_ids'
            )
        ));


        /* Other products elements */
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_bio_certificate',
            'unchecked_value' => '0',
            'checked_value' => '1',
            'attributes' => array(
                'id' => 'is_bio_certificate',
                'class' => 'checkbox e2'
            )
        ));
        $this->add(array(
            'name' => 'bio_certificate_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'bio_certificate_id'
            )
        ));
        $this->add(array(
            'name' => 'bio_certificate_transaction_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'bio_certificate_transaction_id'
            )
        ));
        $this->add(array(
            'name' => 'bio_certificate_panel_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'bio_certificate_panel_id',
            )
        ));
        $this->add(array(
            'name' => 'bio_certificate_search',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Search',
                'id' => 'bio_certificate_search',
                'class' => 'save-btn',
                'onclick' => 'getBioPersonalizeSearch(this.id)'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_personalize_panel',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'is_personalize_panel',
                'class' => 'checkbox e2'
            )
        ));
        $this->add(array(
            'name' => 'personalize_panel_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'personalize_panel_id'
            )
        ));
        $this->add(array(
            'name' => 'personalize_panel_transaction_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'personalize_panel_transaction_id'
            )
        ));
         $this->add(array(
            'name' => 'personalize_panel_honoree_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'personalize_panel_honoree_name'
            )
        ));
        $this->add(array(
            'name' => 'personalize_panel_panel_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'personalize_panel_panel_id',
            )
        ));
        $this->add(array(
            'name' => 'personalize_panel_search',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Search',
                'id' => 'personalize_panel_search',
                'class' => 'save-btn',
                'onclick' => 'getBioPersonalizeSearch(this.id)'
            )
        ));
        $this->add(array(
            'name' => 'otherproductaddtocart',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Add To Cart',
                'id' => 'otherproductaddtocart',
                'class' => 'save-btn'
            )
        ));
        /* End other products elements */
        $this->add(array(
            'name' => 'bio_wohid',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'bio_wohid'
            )
        ));
        $this->add(array(
            'name' => 'bio_name',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'bio_name'
            )
        ));
        $this->add(array(
            'name' => 'immigrated_from',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'immigrated_from'
            )
        ));
        $this->add(array(
            'name' => 'method_of_travel',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'method_of_travel'
            )
        ));
        $this->add(array(
            'name' => 'port_of_entry',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'port_of_entry'
            )
        ));
        $this->add(array(
            'name' => 'date_of_entry_to_us',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'date_of_entry_to_us',
                'class' => 'width-128 cal-icon '
            )
        ));
        $this->add(array(
            'name' => 'additional_info',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'additional_info'
            )
        ));
         $this->add(array(
            'name' => 'bio_certificate_qty',
            'attributes' => array(
                'type' => 'text',
                'id' => 'bio_certificate_qty',
                'class' => 'width-128'                
            )
        ));
         $this->add(array(
            'name' => 'personalized_qty',
            'attributes' => array(
                'type' => 'text',
                'id' => 'personalized_qty',
                'class' => 'width-128'
            )
        ));
         $this->add(array(
            'name' => 'personalize_wohid',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'personalize_wohid'
            )
        ));
        /*Bio certificate form */
        
        /*End bio certificate form */
        $this->add(array(
            'name' => 'wohaddtocart',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Add To Cart',
                'id' => 'wohaddtocart',
                'class' => 'save-btn'
            ),
        ));
    }

}