<?php

/**
 * This form is used to search contacts on create transaction page
 * @package    Transaction
 * @author     Icreon Tech - DT
 */

namespace Transaction\Form;

use Zend\Form\Form;

class SearchUserForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('search_user');
        $this->setAttribute('method', 'post');

//        $this->add(array(
//            'name' => 'membership_afihc',
//            'attributes' => array(
//                'type' => 'text',
//                'id' => 'membership_afihc',
//                'class' => 'search-icon'
//            )
//        ));
        $this->add(array(
            'name' => 'contact_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'contact_id'
            )
        ));

          // new fields - start
        $this->add(array(
	'name' => 'first_name',
	'attributes' => array(
            'type' => 'text',
            'id' => 'first_name',
          )
        ));

        $this->add(array(
            'name' => 'last_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_name',
            )
        ));

        $this->add(array(
            'name' => 'company_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'company_name',
            )
        ));	

        $this->add(array(
            'name' => 'email_address',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email_address',
             )
        ));	
        // new fields - end
        
        $this->add(array(
            'name' => 'name_email',
            'attributes' => array(
                'type' => 'text',
                'id' => 'name_email',
                'class' => 'search-icon'
            )
        ));

        $this->add(array(
            'name' => 'phone',
            'attributes' => array(
                'type' => 'text',
                'id' => 'phone',
                'class' => ''
            )
        ));
        $this->add(array(
            'name' => 'zip_code',
            'attributes' => array(
                'type' => 'text',
                'id' => 'zip_code',
                'class' => 'search-icon'
            )
        ));
 
         
      $this->add(array(
            'type' => 'Select',
            'name' => 'membership_level',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'value' => '',
                'class' => 'e1 select-w-320',
                'id' => 'membership_level'
            ),
        ));
          
        $this->add(array(
            'name' => 'state',
            'attributes' => array(
                'type' => 'text',
                'id' => 'state',
                'class' => ''
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'state_select',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'state_select',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_usa',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'class' => 'checkbox',
                'id' => 'is_usa',
                'class' => 'e2',
                'value' => '1'
            )
        ));

        
        $this->add(array(
            'name' => 'country',
            'attributes' => array(
                'type' => 'text',
                'id' => 'country',
                'class' => 'search-icon'
            )
        ));

        $this->add(array(
            'name' => 'searchbutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Search',
                'id' => 'searchbutton',
                'class' => 'save-btn'
            ),
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'last_transaction',
            'attributes' => array(
                'id' => 'last_transaction',
                'class' => 'e1 select-w-320',
                'onChange' => 'showDate(this.id,"date_range_div")'
            )
        ));
        $this->add(array(
            'name' => 'from_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'from_date',
                'class' => 'calendar-input cal-icon',
                'readonly' => 'readonly',
                'style' => 'width:125px;'
                
            )
        ));
        $this->add(array(
            'name' => 'to_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'to_date',
                'class' => 'calendar-input cal-icon',
                'readonly' => 'readonly',
                'style' => 'width:125px;'
            )
        ));
    }

}