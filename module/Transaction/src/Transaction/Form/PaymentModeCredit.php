<?php

/**
 * This page is used to Transaction add more payment mode credit
 * @package    Transaction_PaymentModeCredit
 * @author     Icreon Tech - DT
 */

namespace Transaction\Form;

use Zend\Form\Form;

/**
 * This form is used to Transaction add more payment mode credit
 * @package    PaymentModeCredit
 * @author     Icreon Tech - DT
 */
class PaymentModeCredit extends Form {

    public function __construct($name = null) {
        parent::__construct('payment_mode_credit_form');
        $this->setAttribute('method', 'post');

         $this->add(array(
            'name' => 'pledge_transaction_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'pledge_transaction_id',
            )
        ));
         $this->add(array(
            'name' => 'user_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'user_id',
            )
        ));
        /* Existing card detail */
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'new_old_card',
            'options' => array(
                'value_options' => array(
                    '1' => 'Use Credit Card on File',
                    '2' => 'Add new Credit Card information'
                ),
            ),
            'attributes' => array(
                'id' => 'new_old_card',
                'class' => 'e3',
                'onchange' => 'showCreditDetail(this.value)'
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'existing_profile_id',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'existing_profile_id',
                'class' => 'e1 select-w-320',
            )
        ));
        
        $this->add(array(
            'name' => 'exist_amount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'exist_amount'
            )
        ));

        $this->add(array(
            'name' => 'exist_payment',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'exist_payment',
                'class' => 'save-btn m-r-10',
                'value' => 'Payment'
            )
        ));
        $this->add(array(
            'name' => 'exist_cancel',
            'attributes' => array(
                'type' => 'button',
                'id' => 'exist_cancel',
                'class' => 'save-btn',
                'value' => 'Cancel'
            )
        ));

        /* End existing card detail */
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_exist_billing',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'is_exist_billing',
                'class' => 'checkbox e2'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_save_profile',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'is_save_profile',
                'class' => 'checkbox e2'
            )
        ));

        $this->add(array(
            'name' => 'card_holder_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'card_holder_name',
            )
        ));
        $this->add(array(
            'name' => 'card_number',
            'attributes' => array(
                'type' => 'text',
                'id' => 'card_number',
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'expiry_month',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'expiry_month',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'expiry_year',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'expiry_year',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));

        $this->add(array(
            'name' => 'cvv_number',
            'attributes' => array(
                'type' => 'password',
                'id' => 'cvv_number',
            )
        ));
        $this->add(array(
            'name' => 'amount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'amount'
            )
        ));
        $this->add(array(
            'name' => 'billing_address_1',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_address_1'
            )
        ));
        $this->add(array(
            'name' => 'billing_address_2',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_address_2'
            )
        ));
        $this->add(array(
            'name' => 'billing_city_auth',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_city_auth'
            )
        ));
        $this->add(array(
            'name' => 'billing_state_auth',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_state_auth'
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'billing_state_auth_select',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'billing_state_auth_select',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));
        
        $this->add(array(
            'name' => 'billing_zip_code_auth',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_zip_code_auth'
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'billing_country_auth',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'billing_country_auth',
                'class' => 'e1 select-w-320',
                'onchange' => 'onChangeCountry(this.value)'
            )
        ));

        
        $this->add(array(
            'name' => 'payment',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'payment',
                'class' => 'save-btn',
                'value' => 'Payment'
            )
        ));
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'id' => 'cancel',
                'class' => 'save-btn',
                'value' => 'Cancel'
            )
        ));
    }

}