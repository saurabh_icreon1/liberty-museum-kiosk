<?php

/**
 * This form is used to select batch ID.
 * @package    Setting
 * @author     Icreon Tech - SR
 */

namespace Transaction\Form;

use Zend\Form\Form;

/**
 * This form is used to select batch Id
 * @package    Setting
 * @author     Icreon Tech - SR
 */
class SelectBatchIdForm extends Form {

    public function __construct($name = null) {

        parent::__construct('search-template');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'statement_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'statement_date',
                'class' =>'width-124 cal-icon',
                'autocomplete' => 'off'
            )
        ));
        
        $this->add(array(
            'name' => 'search',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Find Batch',
                'id' => 'search',
                'class' => 'save-btn',
            ),
        ));

    }

}
