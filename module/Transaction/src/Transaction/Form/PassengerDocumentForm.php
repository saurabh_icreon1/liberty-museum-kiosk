<?php

/**
 * This form is used to get passenger record info
 * @package    Document
 * @author     Icreon Tech - DT
 */

namespace Transaction\Form;

use Zend\Form\Form;

/**
 * This class is used to make the form passenger document
 * @package    Document
 * @author     Icreon Tech - DT
 */
class PassengerDocumentForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('passenger_document');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'passenger_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'passenger_id'
            )
        ));
        $this->add(array(
            'name' => 'product_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'product_id'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_passenger_record',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'is_passenger_record',
                'class' => 'checkbox e2'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'passenger_record_options',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'passenger_record_options',
                'class' => 'e3 honoree',
                'disabled' => 'disabled',
            )
        ));
        $this->add(array(
            'name' => 'passenger_record_qty',
            'attributes' => array(
                'type' => 'text',
                'id' => 'passenger_record_qty',
                'readonly' => 'readonly'
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_ship_image',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'is_ship_image',
                'class' => 'checkbox e2'
            )
        ));

        $this->add(array(
            'name' => 'ship_image_qty',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ship_image_qty',
                'readonly' => 'readonly'
            )
        ));

        $this->add(array(
            'name' => 'ship_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'ship_id'
            )
        ));

        /* manifest element */
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_manifest',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'is_manifest',
                'class' => 'checkbox e2'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_first_image',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'is_first_image',
                'class' => 'checkbox e2',
                'onchange' => 'calculateManifestPrice()'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_second_image',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'is_second_image',
                'class' => 'checkbox e2',
                'onchange' => 'calculateManifestPrice()'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'centerImage',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'centerImage',
                'class' => 'checkbox e2',
                'onclick' => "getImage('save')"
            )
        ));

        /* end manifest element */

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_related_product',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'is_related_product',
                'class' => 'checkbox e2'
            )
        ));

        $this->add(array(
            'name' => 'related_product_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'related_product_name',
                'class' => 'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'related_product_name_ids',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'related_product_name_ids'
            )
        ));

        $this->add(array(
            'name' => 'passengeraddtocart',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Add To Cart',
                'id' => 'passengeraddtocart',
                'class' => 'save-btn'
            ),
        ));
    }

}