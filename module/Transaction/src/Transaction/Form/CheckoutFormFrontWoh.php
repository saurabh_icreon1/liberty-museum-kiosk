<?php

/**
 * This form is used for Create Checkout form.
 * @package    Transaction
 * @author     Icreon Tech - KK
 */

namespace Transaction\Form;

use Zend\Form\Form;

/**
 * This form is used for Create Checkout form.
 * @package    Transaction
 * @author     Icreon Tech - DT
 */
class CheckoutFormFrontWoh extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('checkout_form');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'free_shipping',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'free_shipping',
                'value' => 'false'
            )
        ));
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'check-shipping',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'check-shipping',
                'value' => '1'
            )
        ));


        $this->add(array(
            'name' => 'ship_method_error',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'ship_method_error',
                'value' => '0'
            )
        ));

        $this->add(array(
            'name' => 'user_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'user_id'
            )
        ));

        /* Coupon code elements */
        $this->add(array(
            'name' => 'coupon_code',
            'attributes' => array(
                'type' => 'text',
                'id' => 'coupon_code',
            )
        ));
        $this->add(array(
            'name' => 'coupon_apply',
            'attributes' => array(
                'type' => 'button',
                'id' => 'coupon_apply',
            )
        ));
        /* End coupon code elements */

        /* Billing address elements */

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'billing_existing_contact',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'billing_existing_contact',
                'class' => 'e1 select-wid-401',
                'onchange' => "getAddressInfoFront(this.value,'billing')"
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'billing_title',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'billing_title',
                'class' => 'e1'
            )
        ));
        $this->add(array(
            'name' => 'billing_first_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_first_name',
                'class' => 'width-222'
            )
        ));
        $this->add(array(
            'name' => 'billing_middle_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_middle_name',
                'class' => 'width-222'
            )
        )); 
        $this->add(array(
            'name' => 'billing_last_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_last_name',
                'class' => 'width-222'
            )
        ));
        $this->add(array(
            'name' => 'billing_company',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_company',
            )
        ));

        $this->add(array(
            'name' => 'billing_address_1',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_address_1',
                'class' => 'width-222'
            )
        ));

        $this->add(array(
            'name' => 'billing_address_2',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_address_2',
                'class' => 'width-222'
            )
        ));

        $this->add(array(
            'name' => 'billing_city',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_city',
                'class' => 'width-222'
            )
        ));
        $this->add(array(
            'name' => 'billing_state',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_state',
                'class' => 'width-222'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'billing_state_id',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'id' => 'billing_state_id',
                'onchange' => "assignStateValue(this.value,this.id,'billing')",
                'class' => 'e1',
                'value' => '' /* set selected to 'blank' */
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'shipping_state_id',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'id' => 'shipping_state_id',
                'onchange' => "assignStateValue(this.value,this.id,'shipping')",
                'class' => 'e1',
                'value' => '' /* set selected to 'blank' */
            )
        ));



        $this->add(array(
            'name' => 'billing_zip_code',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_zip_code',
                'class' => 'width-222'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'billing_country',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'billing_country',
                'onchange' => "selectUsaState(this.value,this.id,'billing')",
                'class' => "e1"
            )
        ));
        $this->add(array(
            'name' => 'billing_phone',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_phone',
            )
        ));

        $this->add(array(
            'name' => 'billing_country_code',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_country_code',
                'class' => 'width-125 m-r-20'
            )
        ));
        $this->add(array(
            'name' => 'billing_area_code',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_area_code',
                'class' => 'width-80 m-r-20'
            )
        ));
        $this->add(array(
            'name' => 'billing_phone',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_phone',
                'class' => 'width-155'
            )
        ));
        $this->add(array(
            'name' => 'billing_phone_info_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'billing_phone_info_id'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'bill_to_address',
            'options' => array(
                'value_options' => array(
                    '1' => 'Address on profile '
                ),
            ),
            'attributes' => array(
                'id' => 'bill_to_address',
                'value' => '', //set checked to '1'
                'class' => 'e3 billtoaddress',
                'onClick' => 'showbilloption(this)'
            )
        ));

        /* End billing address elements */

        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'ship_to_address',
            'options' => array(
                'value_options' => array(
                    '1' => 'Ship to address on profile ',
//                    '2' => 'Ship to new address'
                ),
            ),
            'attributes' => array(
                'id' => 'ship_to_address',
                'value' => '', //set checked to '1'
                'class' => 'e3 matchinggift',
                'onClick' => 'showshipoption(this)'
            )
        ));



        /* Shipping address elements */

        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'shipping_method',
            'options' => array(
                'value_options' => array(
                    '1' => '3 – 5 days  ',
                    '2' => '5 – 7 days ',
                    '3' => '5 – 7 days International ',
                    '4' => 'Overnight'
                ),
            ),
            'attributes' => array(
                'id' => 'shipping_method',
                'value' => '', //set checked to '1'
                'class' => 'e3 shipmethod'
            )
        ));

        /* $this->add(array(
          'type' => 'Zend\Form\Element\Checkbox',
          'name' => 'is_apo_po',
          'checked_value' => '1',
          'unchecked_value' => '0',
          'attributes' => array(
          'id' => 'is_apo_po',
          'class' => 'checkbox e2',

          )
          )); */

        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'is_apo_po',
            'options' => array(
                "value_options" => array(
                    '1' => 'Residential/Corporate',
                    '2' => 'PO',
                    '3' => 'APO'
                )
            ),
            'attributes' => array(
                'id' => 'is_apo_po',
                'class' => 'e3 is_apo_po',
                'onchange' => 'changeApoPo(this.value)',
                'value' => '1'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'is_apo_po_record',
            'options' => array(
                "value_options" => array(
                    '1' => 'Residential/Corporate',
                    '2' => 'PO',
                    '3' => 'APO'
                )
            ),
            'attributes' => array(
                'id' => 'is_apo_po_record',
                'class' => 'e3 is_apo_po_record',
                'onchange' => 'changeApoPo(this.value)',
                'value' => '1'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'apo_po_state',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'apo_po_state',
                'class' => 'e1',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'apo_po_shipping_country',
            'options' => array(
                'value_options' => array(
                    '228' => 'UNITED STATES'
                )
            ),
            'attributes' => array(
                'id' => 'apo_po_shipping_country',
                'class' => "e1"
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'pick_up',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'pick_up',
                'class' => 'checkbox e2'
            ),
            'options' => array(
                'use_hidden_element' => false
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'billing_shipping_same',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'billing_shipping_same',
                'class' => 'checkbox e2'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'shipping_existing_contact',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'shipping_existing_contact',
                'class' => 'e1 select-wid-401',
                'onchange' => "getAddressInfoFront(this.value,'shipping')"
            )
        ));

        
          $this->add(array(
          'type' => 'Zend\Form\Element\Select',
          'name' => 'shipping_title',
          'options' => array(
          ),
          'attributes' => array(
          'id' => 'shipping_title',
          'class' => 'e1'
          )
          ));
       /* 
        $this->add(array(
            'name' => 'shipping_title',
            'attributes' => array(
                'type' => 'text',
                'id' => 'shipping_title',
                'class' => 'width-222'
            )
        )); */
        $this->add(array(
            'name' => 'shipping_first_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'shipping_first_name',
                'class' => 'texttransform width-222'
            )
        ));
        $this->add(array(
            'name' => 'shipping_middle_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'shipping_middle_name',
                'class' => 'width-222'
            )
        ));        

        $this->add(array(
            'name' => 'shipping_last_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'shipping_last_name',
               'class' => 'texttransform width-222'
            )
        ));

        $this->add(array(
            'name' => 'shipping_attention',
            'attributes' => array(
                'type' => 'text',
                'id' => 'shipping_attention'
            )
        ));

        $this->add(array(
            'name' => 'billing_attention',
            'attributes' => array(
                'type' => 'text',
                'id' => 'billing_attention'
            )
        ));





        $this->add(array(
            'name' => 'shipping_company',
            'attributes' => array(
                'type' => 'text',
                'id' => 'shipping_company',
            )
        ));

        $this->add(array(
            'name' => 'shipping_address_1',
            'attributes' => array(
                'type' => 'text',
                'id' => 'shipping_address_1',
                'class' => 'width-350'
            )
        ));
        $this->add(array(
            'name' => 'shipping_address_2',
            'attributes' => array(
                'type' => 'text',
                'id' => 'shipping_address_2',
                'class' => 'width-350'
            )
        ));
        $this->add(array(
            'name' => 'shipping_city',
            'attributes' => array(
                'type' => 'text',
                'id' => 'shipping_city',
                'class' => 'width-222'
            )
        ));
        $this->add(array(
            'name' => 'shipping_state',
            'attributes' => array(
                'type' => 'text',
                'id' => 'shipping_state',
                'class' => 'width-222'
            )
        ));

        $this->add(array(
            'name' => 'shipping_zip_code',
            'attributes' => array(
                'type' => 'text',
                'id' => 'shipping_zip_code',
                'class' => 'width-222'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'shipping_country',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'shipping_country',
                'onchange' => "selectUsaState(this.value,this.id,'shipping')",
                'class' => "e1"
            )
        ));
        $this->add(array(
            'name' => 'shipping_phone',
            'attributes' => array(
                'type' => 'text',
                'id' => 'shipping_phone',
            )
        ));

        $this->add(array(
            'name' => 'shipping_location_type',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'shipping_location_type',
            )
        ));

        $this->add(array(
            'name' => 'billing_location_type',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'billing_location_type',
            )
        ));

        $this->add(array(
            'name' => 'shipping_country_code',
            'attributes' => array(
                'type' => 'text',
                'id' => 'shipping_country_code',
                'class' => 'width-125 m-r-20'
            )
        ));

        $this->add(array(
            'name' => 'shipping_area_code',
            'attributes' => array(
                'type' => 'text',
                'id' => 'shipping_area_code',
                'class' => 'width-80 m-r-20'
            )
        ));
        $this->add(array(
            'name' => 'shipping_phone',
            'attributes' => array(
                'type' => 'text',
                'id' => 'shipping_phone',
                'class' => 'width-155'
            )
        ));
        $this->add(array(
            'name' => 'shipping_phone_info_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'shipping_phone_info_id'
            )
        ));



        $this->add(array(
            'name' => 'shipping_location_type',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'shipping_location_type',
            )
        ));

        $this->add(array(
            'name' => 'billing_location_type',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'billing_location_type',
            )
        ));

        /* End shipping address elements */

        /*
         *  credit card selection
         */
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'credit_card_exist',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'credit_card_exist',
                'class' => "e1"
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'credit_card_selection',
            'options' => array(
                'value_options' => array(
                    '1' => 'Use credit card on file ',
                    '2' => 'Add new credit card information'
                ),
            ),
            'attributes' => array(
                'id' => 'credit_card_selection',
                'value' => '', //set checked to '1'
                'class' => 'e3 creditcardselect',
                'onClick' => 'showcreditcardoption(this)'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'save_credit_card',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'save_credit_card',
                'class' => 'checkbox'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'save_shipping_address',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'save_shipping_address',
                'class' => 'checkbox'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'save_billing_address',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'save_billing_address',
                'class' => 'checkbox'
            )
        ));

        $this->add(array(
            'name' => 'credit_card_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'credit_card_name',
                'class' => 'width-222'
            )
        ));
        $this->add(array(
            'name' => 'credit_card_number',
            'attributes' => array(
                'type' => 'text',
                'id' => 'credit_card_number',
                'class' => 'width-222'
            )
        ));
        $this->add(array(
            'name' => 'credit_card_code',
            'attributes' => array(
                'type' => 'password',
                'id' => 'credit_card_code',
                'class' => 'width-222'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'credit_card_exp_month',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'credit_card_exp_month',
                'class' => "e1"
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'credit_card_exp_year',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'credit_card_exp_year',
                'class' => "e1"
            )
        ));


        /*
         *  credit card options ends
         */



        /* Payment mode elementws */
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'payment_mode_cash',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'payment_mode_cash',
                'class' => 'checkbox'
            )
        ));

        $this->add(array(
            'name' => 'payment_mode_cash_amount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'payment_mode_cash_amount',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'payment_mode_chk',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'payment_mode_chk',
                'class' => 'checkbox'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'payment_mode_credit',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'payment_mode_credit',
                'class' => 'checkbox'
            )
        ));

        /* End payment mode elements */
        $this->add(array(
            'name' => 'notes',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'notes',
                'class' => 'width-90',
            )
        ));
        $this->add(array(
            'name' => 'transactionsubmit',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'transactionsubmit',
                'class' => 'button semibold green',
                'value' => 'Submit Transaction'
            )
        ));

        $this->add(array(
            'name' => 'step1',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'step1',
                'class' => 'button semibold',
                'value' => 'Continue'
            )
        ));
        $this->add(array(
            'name' => 'step2',
            'attributes' => array(
                'type' => 'button',
                'id' => 'step2',
                'class' => 'button semibold green',
                'value' => 'Continue to Next Step'
            )
        ));
        $this->add(array(
            'name' => 'transactioncancel',
            'attributes' => array(
                'type' => 'button',
                'id' => 'transactioncancel',
                'class' => 'cancel-btn m-l-20',
                'value' => 'CANCEL',
                'onclick' => 'window.location.href="/crm-transactions";'
            )
        ));
        
        $this->add(array(
            'name' => 'email_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email_id',
                'class' => 'width-222'
            )
        ));        
    }

}
