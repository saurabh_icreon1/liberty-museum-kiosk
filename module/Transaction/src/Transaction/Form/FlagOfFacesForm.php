<?php

/**
 * This page is used to flag of faces add to cart detail
 * @package    Transaction_FlagOfFacesForm
 * @author     Icreon Tech - DT
 */

namespace Transaction\Form;

use Zend\Form\Form;

/**
 * This form is used to donation add to cart
 * @package    FlagOfFacesForm
 * @author     Icreon Tech - DT
 */
class FlagOfFacesForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('flag_of_faces_form');
        $this->setAttributes(array('method' => 'post', 'enctype' => 'multipart/form-data'));

       $this->add(array(
            'name' => 'crop_fof_image_status',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'crop_fof_image_status',
            ),
        ));
           
        $this->add(array(
            'name' => 'upload_photo',
            'attributes' => array(
                'type' => 'file',
                'value' => 'upload',
                'id' => 'upload_photo',
            ),
        ));
        $this->add(array(
            'name' => 'image_name',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'image_name',
            ),
        ));
        $this->add(array(
            'name' => 'fof_image',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'fof_image',
            ),
        ));
        
        $this->add(array(
            'name' => 'temp_image',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'temp_image'
            )
        ));
          
        $this->add(array(
            'name' => 'donated_by',
            'attributes' => array(
                'type' => 'text',
                'id' => 'donated_by',
                'maxlength' => '100'
            )
        ));
        $this->add(array(
            'name' => 'people_photo',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'people_photo'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'is_people_photo_visible',
            'options' => array(
                'value_options' => array(
                    '1' => 'Visible',
                    '0' => 'Hidden',
                ),
            ),
            'attributes' => array(
                'id' => 'is_people_photo_visible',
                'value' => '', //set checked to '1'
                'class' => 'e3'
            )
        ));
        $this->add(array(
            'name' => 'photo_caption',
            'attributes' => array(
                'type' => 'text',
                'id' => 'photo_caption',
                'maxlength' => '40'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'is_photo_caption_visible',
            'options' => array(
                'value_options' => array(
                    '1' => 'Visible',
                    '0' => 'Hidden',
                ),
            ),
            'attributes' => array(
                'id' => 'is_photo_caption_visible',
                'value' => '', //set checked to '1'
                'class' => 'e3'
            )
        ));

        $this->add(array(
            'name' => 'fofaddtocart',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Add To Cart',
                'id' => 'fofaddtocart',
                'class' => 'save-btn'
            ),
        ));
        $this->add(array(
            'name' => 'fofcancel',
            'attributes' => array(
                'type' => 'button',
                'id' => 'fofcancel',
                'class' => 'cancel-btn m-l-20',
                'value' => 'CANCEL'
            )
        ));
    }

}