<?php

/**
 * This page is used for search Transaction form.
 * @package    Transaction_SearchTransactionForm
 * @author     Icreon Tech - DT
 */

namespace Transaction\Form;

use Zend\Form\Form;

/**
 * This form is used for search export to sage.
 * @package    SearchSageForm
 * @author     Icreon Tech - KK
 */
class SearchSageForm extends Form {

    public function __construct($name = null) {
        parent::__construct('create_batch');
        $this->setAttribute('method', 'post');

         $this->add(array(
            'name' => 'transaction_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'transaction_date',
                'class' => 'width-155 cal-icon'
            )
        ));
        $this->add(array(
            'name' => 'transaction_time',
            'attributes' => array(
                'type' => 'text',
                'id' => 'transaction_time',
                'class' => 'width-108 m-l-20 time-icon'
            )
        ));
      
      
      
      
        
        $this->add(array(
            'name' => 'searchtransactionbutton',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'searchtransactionbutton',
                'class' => 'save-btn',
                'value' => 'Update'
            )
        ));
          $this->add(array(
            'name' => 'saveanddownload',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'saveanddownload',
                'class' => 'save-btn',
                'value' => 'Save and download'
            )
        ));
            $this->add(array(
            'name' => 'searchtransactionbutton',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'searchtransactionbutton',
                'class' => 'save-btn m-l-10',
                'value' => 'Update'
            )
        ));
            
             $this->add(array(
            'name' => 'cancelbutton',
            'attributes' => array(
                'type' => 'button',
                'id' => 'cancelbutton',
                'class' => 'cancel-btn m-l-20',
                'value' => 'CANCEL',
                'onclick' => 'window.location.href="/crm-sages";'
            )
        ));
       
    }

}

