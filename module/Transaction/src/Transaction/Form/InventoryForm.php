<?php

/**
 * This page is used to flag of faces add to cart detail
 * @package    Transaction_InventoryForm
 * @author     Icreon Tech - DT
 */

namespace Transaction\Form;

use Zend\Form\Form;

/**
 * This form is used to flag of faces add to cart detail
 * @package    InventoryForm
 * @author     Icreon Tech - DT
 */
class InventoryForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('inventory_form');
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'category_id',
            'attributes' => array(
                'id' => 'category_id',
                'class' => 'e1 select-w-320',
                'onchange' => "getProductsByCategoryId(this.value)"
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'product_id',
            'attributes' => array(
                'id' => 'product_id',
                'class' => 'e1 select-w-320',
                'onchange' => "getProductsDetailById(this.value)"
            )
        ));
        $this->add(array(
            'name' => 'num_quantity',
            'attributes' => array(
                'type'  => 'text',
                'id' => 'num_quantity'
            ),
        ));
        $this->add(array(
            'name' => 'inventoryaddtocart',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Add To Cart',
                'id' => 'inventoryaddtocart',
                'class' => 'save-btn'
            ),
        ));
        
    }

}