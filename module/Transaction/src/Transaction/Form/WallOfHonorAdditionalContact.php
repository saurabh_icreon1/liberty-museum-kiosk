<?php

/**
 * This page is used to add to cart wall of honor additional contact
 * @package    Transaction_WallOfHonorAdditionalContact
 * @author     Icreon Tech - DT
 */

namespace Transaction\Form;

use Zend\Form\Form;

/**
 * This form is used to add to cart wall of honor additional contact
 * @package    WallOfHonorAdditionalContact
 * @author     Icreon Tech - DT
 */
class WallOfHonorAdditionalContact extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('wall_of_honor_additional_contact_form');

        $this->add(array(
            'name' => 'first_name[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'first_name_1',
                'maxlength' => '20'
            )
        ));
        $this->add(array(
            'name' => 'last_name[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_name_1',
                'maxlength' => '20'
            )
        ));
        $this->add(array(
            'name' => 'email_id[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email_id_1'
            )
        ));
    }

}