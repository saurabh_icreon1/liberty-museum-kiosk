<?php

/**
 * This form is used to select batch ID.
 * @package    Setting
 * @author     Icreon Tech - SR
 */

namespace Transaction\Form;

use Zend\Form\Form;

/**
 * This form is used to select batch Id
 * @package    Setting
 * @author     Icreon Tech - SR
 */
class EditBatchIdForm extends Form {

    public function __construct($name = null) {

        parent::__construct('edit-batch-id');
        $this->setAttribute('method', 'post');
        
        $this->add(array(
            'name' => 'batch_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'batch_id_edit',
                'class' =>'width-124',
                'autocomplete' => 'off'
            )
        ));
		$this->add(array(
			'name' => 'old_batch_id',
			'attributes' => array(
				'type' => 'hidden',
				'id' => 'old_batch_id',
				'class' =>'width-124'
			)
        ));
        $this->add(array(
            'name' => 'payment_batch_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'payment_batch_id_edit',
                'class' =>'width-124'
            )
        ));
        $this->add(array(
            'name' => 'settlement_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'settlement_date_edit',
                'class' =>'width-124 cal-icon',
                'autocomplete' => 'off'
            )
        ));
        
        $this->add(array(
            'name' => 'update_batch',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Update Batch',
                'id' => 'update_batch',
                'class' => 'save-btn',
            ),
        ));

    }

}
