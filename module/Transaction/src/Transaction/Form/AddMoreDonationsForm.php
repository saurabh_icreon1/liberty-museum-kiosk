<?php

/**
 * This form is used to search contacts on create transaction page
 * @package    Transaction
 * @author     Icreon Tech - DT
 */

namespace Transaction\Form;

use Zend\Form\Form;

class AddMoreDonationsForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('add_more_donations');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'contact_name[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'contact_name',
                'class' => 'search-icon search-icon-bg'
            )
        ));
        
        $this->add(array(
            'name' => 'contact_name_id[]',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'contact_name_id'
            )
        ));
        
        $this->add(array(
            'name' => 'campaign[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'campaign',
                'class' => 'search-icon search-icon-bg'
            )
        ));
        $this->add(array(
            'name' => 'campaign_id[]',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'campaign_id'
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'payment_mode_id[]',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'payment_mode_id',
                'class' => 'e1'
            )
        ));
        
        $this->add(array(
            'name' => 'check_number[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'check_number'
            )
        ));
        
        $this->add(array(
            'name' => 'amount[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'amount_1',
                'onKeyup' => 'updateTotalAmount(this,"keyup");',
                'onBlur' => 'updateTotalAmount(this,"blur");'
            )
        ));		

		$this->add(array(
            'name' => 'company_name[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'company_name'
            )
        ));
    }
}