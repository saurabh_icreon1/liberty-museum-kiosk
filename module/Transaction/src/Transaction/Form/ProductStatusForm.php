<?php

/**
 * This page is used for updating product status form.
 * @package    Transaction
 * @author     Icreon Tech - AS
 */

namespace Transaction\Form;

use Zend\Form\Form;

/**
 * This class is used for updating product status form.
 * @package    Transaction
 * @author     Icreon Tech - AS
 */
class ProductStatusForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('product_status');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'type' => 'Select',
            'name' => 'product_status[]',
            'attributes' => array(
                'id' => 'product_status',
                'class' => 'e1 select-w-100',
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'product_pre_status[]',
            'attributes' => array(
                'id' => 'product_pre_status'                
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'product_id[]',
            'attributes' => array(
                'id' => 'product_id'                
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'product_type_id[]',
            'attributes' => array(
                'id' => 'product_type_id'                
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'product_type[]',
            'attributes' => array(
                'id' => 'product_type'                
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'product_shp_qty[]',
            'attributes' => array(
                'id' => 'product_shp_qty'                
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'product_total_qty[]',
            'attributes' => array(
                'id' => 'product_total_qty'                
            )
        ));
        $this->add(array(
            'type' => 'text',
            'name' => 'product_qty[]',
            'attributes' => array(
                'id' => 'product_qty',
                'class' => 'width-80'
            )
        ));
        $this->add(array(
            'type' => 'checkbox',
            'name' => 'email_copy',
            'attributes' => array(
                'id' => 'email_copy'                
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'submit',
                'class' => 'save-btn m-t-15 right',
                'value' => 'submit'
            )
        ));
    }

}
