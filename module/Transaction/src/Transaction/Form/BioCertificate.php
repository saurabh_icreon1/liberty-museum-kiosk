<?php

/**
 * This page is used to save bio certificate
 * @package    Transaction_BioCertificate
 * @author     Icreon Tech - DT
 */

namespace Transaction\Form;

use Zend\Form\Form;

/**
 * This form is used to add to cart wall of honor
 * @package    BioCertificate
 * @author     Icreon Tech - DT
 */
class BioCertificate extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('bio_certificate_form');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'bio_wohid',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'bio_wohid'
            )
        ));
        $this->add(array(
            'name' => 'bio_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'bio_name'
            )
        ));
        $this->add(array(
            'name' => 'immigrated_from',
            'attributes' => array(
                'type' => 'text',
                'id' => 'immigrated_from'
            )
        ));
        $this->add(array(
            'name' => 'method_of_travel',
            'attributes' => array(
                'type' => 'text',
                'id' => 'method_of_travel'
            )
        ));
        $this->add(array(
            'name' => 'port_of_entry',
            'attributes' => array(
                'type' => 'text',
                'id' => 'port_of_entry'
            )
        ));
        $this->add(array(
            'name' => 'date_of_entry_to_us',
            'attributes' => array(
                'type' => 'text',
                'id' => 'date_of_entry_to_us',
                'class' => 'width-128 cal-icon '
            )
        ));
        $this->add(array(
            'name' => 'additional_info',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'additional_info'
            )
        ));
        $this->add(array(
            'name' => 'biosubmit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'id' => 'biosubmit',
                'class' => 'save-btn',
                'onClick' => 'buttonShow()'
            )
        ));
        $this->add(array(
            'name' => 'biocancel',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Close',
                'id' => 'biocancel',
                'class' => 'save-btn  m-l-30'
            )
        ));
    }

}