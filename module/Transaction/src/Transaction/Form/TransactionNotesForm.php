<?php

/**
 * This page is used for Create Transaction note form.
 * @package    Transaction
 * @author     Icreon Tech - AS
 */
namespace Transaction\Form;

use Zend\Form\Form;

/**
 * This class is used for Create Transaction note form.
 * @package    Transaction
 * @author     Icreon Tech - AS
 */
class TransactionNotesForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('transaction_note');
        $this->setAttribute('method', 'post');
	 $this->add(array(
            'name' => 'transaction_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'transaction_id'
            )
        ));
        $this->add(array(
            'name' => 'note',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'note',
                'class' => 'width-90'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'notify_note',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'class' => 'checkbox',
                'id' => 'notify_note',
                'class' => 'e2',
                'value' => 'Private'
            )
        ));
     $this->add(array(
        'name' => 'postnote',
        'attributes' => array(
            'type' => 'submit',
            'id' => 'postnote',
            'class' => 'save-btn right',
            'value' => 'Post'
        )
    ));
}
}
