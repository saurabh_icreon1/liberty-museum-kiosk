<?php

/**
 * This form is used to search contacts on create transaction page
 * @package    Transaction
 * @author     Icreon Tech - DT
 */

namespace Transaction\Form;

use Zend\Form\Form;

class SearchWohForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('search_woh');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'type' => 'file',
            'name' => 'upload_panel_entries',
            'attributes' => array(
                'id' => 'upload_panel_entries'
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'select_year',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
                'disable_inarray_validator' => true
            ),
            'attributes' => array(
                'id' => 'select_year',
                'class' => 'e1 select2-offscreen',
                'value' => ''
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'approval_required',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    '0' => 'Yes',
                    '1' => 'No'
                ),
                'disable_inarray_validator' => true
            ),
            'attributes' => array(
                'id' => 'approval_required',
                'class' => 'e1 select2-offscreen',
                'value' => ''
            )
        ));
        
        $this->add(array(
            'name' => 'download_file',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Download file for the selected year',
                'id' => 'download_file',
                'class' => 'save-btn'
            ),
        ));

        $this->add(array(
            'name' => 'upload_file',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Upload',
                'id' => 'upload_file',
                'class' => 'save-btn'
            ),
        ));


        $this->add(array(
            'name' => 'name_or_email',
            'attributes' => array(
                'type' => 'text',
                'id' => 'name_or_email'
            )
        ));

        $this->add(array(
            'name' => 'transaction_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'transaction_id'
            )
        ));

        $this->add(array(
            'name' => 'panel_number',
            'attributes' => array(
                'type' => 'text',
                'id' => 'panel_number'
            )
        ));

        $this->add(array(
            'name' => 'transaction_date_from',
            'attributes' => array(
                'type' => 'text',
                'id' => 'transaction_date_from',
                'class' => 'width-120 cal-icon',
                'style' => 'float:none;'
            )
        ));

        $this->add(array(
            'name' => 'transaction_date_to',
            'attributes' => array(
                'type' => 'text',
                'id' => 'transaction_date_to',
                'class' => 'width-120 cal-icon',
                'style' => 'float:none;'
            )
        ));


        $this->add(array(
            'type' => 'Radio',
            'name' => 'panel_assigned',
            'attributes' => array(
                'class' => 'e3 gender'
            ),
            'options' => array(
                'value_options' => array(
                    '0' => 'No',
                    '1' => 'Yes'
                ),
            )
        ));


        $this->add(array(
            'name' => 'honoree_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'honoree_name'
            )
        ));



        $this->add(array(
            'name' => 'searchbutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Search',
                'id' => 'searchbutton',
                'class' => 'save-btn'
            ),
        ));
		 $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'item_status',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'item_status',
                'multiple' => 'multiple',
                'class' => 'e1 select-w-320'
            )
        ));
    }

}