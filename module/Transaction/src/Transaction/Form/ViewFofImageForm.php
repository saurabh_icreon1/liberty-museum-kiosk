<?php

/**
 * This page is used for approve fof image
 * @package    Transaction
 * @author     Icreon Tech - AS
 */

namespace Transaction\Form;

use Zend\Form\Form;

/**
 * This class is used for approve fof image
 * @package    Transaction
 * @author     Icreon Tech - AS
 */
class ViewFofImageForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('fof_image');
        $this->setAttribute('method', 'post');

       $this->add(array(
            'name' => 'crop_fof_image_status',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'crop_fof_image_status',
            ),
        ));
           
        $this->add(array(
            'name' => 'approve',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'approve',
                'class' => 'save-btn right',
                'value' => 'approve'
            )
        ));
        $this->add(array(
            'name' => 'donated_by',
            'attributes' => array(
                'type' => 'text',
                'id' => 'donated_by'
            )
        ));
        $this->add(array(
            'name' => 'people_photo',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'people_photo'
            )
        ));
        $this->add(array(
            'name' => 'is_people_photo_visible',
            'type' => 'Zend\Form\Element\Radio',
            'options' => array(
                'value_options' => array(
                    '0' => 'Hidden',
                    '1' => 'Visible',
                ),
            ),
            'attributes' => array(
                'id' => 'is_people_photo_visible',
                'class' => 'e3',
                'tag' => 'div'
            )
        ));
        $this->add(array(
            'name' => 'photo_caption',
            'attributes' => array(
                'type' => 'text',
                'id' => 'photo_caption'
            )
        ));
        $this->add(array(
            'name' => 'is_photo_caption_visible',
            'type' => 'Zend\Form\Element\Radio',
            'options' => array(
                'value_options' => array(
                    '0' => 'Hidden',
                    '1' => 'Visible',
                ),
            ),
            'attributes' => array(
                'id' => 'is_photo_caption_visible',
                'class' => 'e3',
                'tag' => 'div'
            )
        ));
        
        
        // new - added - start
        $this->add(array(
                'type' => 'file',
                'name' => 'upload_photo',
                'attributes' => array(
                   'id' => 'upload_photo'
                )
        ));
        
        $this->add(array(
            'type' => 'hidden',
            'name' => 'fof_id',
            'attributes' => array(
               'id' => 'fof_id',
            )
        ));
        
        $this->add(array(
            'type' => 'hidden',
            'name' => 'photo_modified',
            'attributes' => array(
               'id' => 'photo_modified',
                'value' => '0'
            )
        ));
        
         $this->add(array(
            'type' => 'hidden',
            'name' => 'photo_modified_name',
            'attributes' => array(
               'id' => 'photo_modified_name'
            )
        ));
        
       $this->add(array(
            'name' => 'save_photo',
            'attributes' => array(
                'type' => 'button',
                'id' => 'save_photo',
                'class' => 'save-btn right',
                'value' => 'save'
            )
          ));
       
       $this->add(array(
            'name' => 'countTagNum',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'countTagNum',
                'value' => '0'
            )
          ));
       
         // new - added - end           
    }

}
