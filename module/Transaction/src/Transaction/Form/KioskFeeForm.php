<?php

/**
 * This page is used to add to cart kiosk fee form
 * @package    Transaction_KioskFeeForm
 * @author     Icreon Tech - DT
 */

namespace Transaction\Form;

use Zend\Form\Form;

/**
 * This form is used to add to cart kiosk fee form
 * @package    KioskFeeForm
 * @author     Icreon Tech - DT
 */
class KioskFeeForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('kiosk_fee_form');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'kioskfeeaddtocart',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Add To Cart',
                'id' => 'kioskfeeaddtocart',
                'class' => 'save-btn'
            ),
        ));
    }

}