<?php

/**
 * This page is used to add more notify form
 * @package    Transaction_DonationNotifyForm
 * @author     Icreon Tech - DT
 */

namespace Transaction\Form;

use Zend\Form\Form;

/**
 * This form is used to add more notify form
 * @package    DonationNotifyForm
 * @author     Icreon Tech - DT
 */
class DonationNotifyForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('donation_notify');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'name[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'name_1'
                )
        ));
         $this->add(array(
            'name' => 'firstname[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'firstname_1',
                'placeholder' => 'First Name',
                'class' => 'button'
                
            )
        ));
          $this->add(array(
            'name' => 'lastname[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'lastname_1',
                'placeholder' => 'Last Name',
                'class' => 'button'
            )
        ));
        $this->add(array(
            'name' => 'email[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email_1',
                'placeholder' => 'Email',
                'class' => 'button large'
            )
        ));
    }
}