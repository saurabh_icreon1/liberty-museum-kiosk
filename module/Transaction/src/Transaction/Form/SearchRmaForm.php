<?php

/**
 * This page is used for updating transaction status form.
 * @package    Transaction
 * @author     Icreon Tech - AS
 */

namespace Transaction\Form;

use Zend\Form\Form;

/**
 * This class is used for search rma
 * @package    Transaction
 * @author     Icreon Tech - AS
 */
class SearchRmaForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('search_rma');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'type' => 'text',
            'name' => 'transaction_id',
            'attributes' => array(
                'id' => 'transaction_id',
            )
        ));
        $this->add(array(
            'type' => 'text',
            'name' => 'name',
            'attributes' => array(
                'id' => 'name',
            )
        ));
        $this->add(array(
            'type' => 'text',
            'name' => 'rma',
            'attributes' => array(
                'id' => 'rma',
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'status',
            'attributes' => array(
                'id' => 'status',
                'class' => 'e1 select-w-320',
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'action',
            'attributes' => array(
                'id' => 'action',
                'class' => 'e1 select-w-320',
            )
        ));        
        $this->add(array(
            'name' => 'searchbutton',
            'attributes' => array(
                'type' => 'button',
                'id' => 'searchbutton',
                'class' => 'search-btn',
                'value' => 'search'
            )
        ));
        $this->add(array(
            'name' => 'cancelbutton',
            'attributes' => array(
                'type' => 'button',
                'id' => 'cancelbutton',
                'class' => 'cancel-btn m-l-20',
                'value' => 'CANCEL',
                'onclick' => 'window.location.href="/crm-rma";'
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'saved_search',
            'options' => array(),
            'attributes' => array(
                'id' => 'saved_search',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));
    }

}

