<?php

/**
 * This form is used to search user transaction record
 * @package    Transaction
 * @author     Icreon Tech - DG
 */

namespace Transaction\Form;

use Zend\Form\Form;

class SearchUserTransactionForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('user_search_transaction');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'user_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'user_id'
            )
        ));

        $this->add(array(
            'type' => 'MultiCheckbox',
            'name' => 'transaction_type',
            'options' => array(
                'value_options' => array(
                    '1' => 'Donation',
                    '2' => 'Purchases',
                ),
            ),
            'attributes' => array(
                'class' => 'e4',
                'onclick' => 'searchTransaction();'
            ),
        ));

        $this->add(array(
            'name' => 'searchbutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Search',
                'id' => 'searchbutton',
                'class' => 'save-btn'
            ),
        ));
    }

}