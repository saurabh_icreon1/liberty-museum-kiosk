<?php

/**
 * This form is used to search contacts on create transaction page
 * @package    Transaction
 * @author     Icreon Tech - DT
 */

namespace Transaction\Form;

use Zend\Form\Form;

class BulkDonationCreateForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('bulk_donation_create');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'batch_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'batch_id',
                'class' => 'search-icon width-295 search-icon-bg'
            )
        ));

		
		$this->add(array(
            'name' => 'payment_batch_id[]',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'payment_batch_id'
            )
        ));

		$this->add(array(
            'name' => 'file_name',
            'attributes' => array(
                'type' => 'file',
                'id' => 'file_name',
            ),
        ));
        
        $this->add(array(
            'name' => 'contact_name[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'contact_name_1',
                'class' => 'search-icon search-icon-bg'
            )
        ));
        $this->add(array(
            'name' => 'contact_name_id[]',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'contact_name_id_1'
            )
        ));
        
        $this->add(array(
            'name' => 'campaign[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'campaign',
                'class' => 'search-icon search-icon-bg'
            )
        ));
        $this->add(array(
            'name' => 'campaign_id[]',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'campaign_id'
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'payment_mode_id[]',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'payment_mode_id_1',
                'class' => 'e1'
            )
        ));
        
        $this->add(array(
            'name' => 'check_number[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'check_number_1'
            )
        ));
        
        $this->add(array(
            'name' => 'amount[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'amount_1',
                'onKeyup' => 'updateTotalAmount(this,"keyup");',
                'onBlur' => 'updateTotalAmount(this,"blur");'
            )
        ));

		$this->add(array(
            'name' => 'company_name[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'company_name_1'
            )
        ));

        $this->add(array(
            'name' => 'uploadbutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit',
                'id' => 'uploadbutton',
                'class' => 'save-btn'
            ),
        ));
		
		$this->add(array(
            'name' => 'submitandlock',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit and Lock Batch',
                'id' => 'submitandlock',
                'class' => 'cancel-btn m-l-20'
            ),
        ));
        
        $this->add(array(
            'name' => 'uploadfile',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Upload',
                'id' => 'uploadfile',
                'class' => 'save-btn'
            ),
        ));
    }
}