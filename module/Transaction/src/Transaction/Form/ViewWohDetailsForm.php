<?php

/**
 * This page is used for approve woh details
 * @package    Transaction
 * @author     Icreon Tech - AS
 */

namespace Transaction\Form;

use Zend\Form\Form;

/**
 * This class is used for approve woh details
 * @package    Transaction
 * @author     Icreon Tech - AS
 */
class ViewWohDetailsForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('woh_details');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'first_name_one',
            'attributes' => array(
                'type' => 'text',
                'id' => 'first_name_one',
                'class' => 'width-90',
                'maxlength' => '45'
            )
        ));
        $this->add(array(
            'name' => 'other_init_one',
            'attributes' => array(
                'type' => 'text',
                'id' => 'other_init_one',
                'class' => 'width-90',
                'maxlength' => '2'
            )
        ));
        $this->add(array(
            'name' => 'first_name_two',
            'attributes' => array(
                'type' => 'text',
                'id' => 'first_name_two',
                'class' => 'width-90',
                'maxlength' => '45'
            )
        ));
        $this->add(array(
            'name' => 'other_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'other_name',
                'class' => 'width-90',
                'maxlength' => '45'
            )
        ));
        $this->add(array(
            'name' => 'other_init_two',
            'attributes' => array(
                'type' => 'text',
                'id' => 'other_init_two',
                'class' => 'width-90',
                'maxlength' => '2'
            )
        ));
        $this->add(array(
            'name' => 'last_name_one',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_name_one',
                'class' => 'width-90',
                'maxlength' => '45'
            )
        ));
        $this->add(array(
            'name' => 'last_name_two',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_name_two',
                'class' => 'width-90',
                'maxlength' => '45'
            )
        ));
        $this->add(array(
            'name' => 'first_line',
            'attributes' => array(
                'type' => 'text',
                'id' => 'first_line',
                'class' => 'width-90',
                'maxlength' => '45'
            )
        ));
        $this->add(array(
            'name' => 'second_line',
            'attributes' => array(
                'type' => 'text',
                'id' => 'second_line',
                'class' => 'width-90',
                'maxlength' => '45'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'country_originUS',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'class' => 'checkbox',
                'id' => 'country_originUS',
                'value' => 'United States Of America'
            )
        ));
        $this->add(array(
            'name' => 'country_origin',
            'attributes' => array(
                'type' => 'text',
                'id' => 'country_origin',
                'class' => 'width-90',
                'maxlength' => '30'
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'donation_for',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    '1' => 'In Memory of',
                    '2' => 'In Honor of'
                )
            ),
            'attributes' => array(
                'id' => 'donation_for',
                'class' => 'e1 select2',
                'value' => ''
            )
        ));
        $this->add(array(
            'name' => 'donated_by',
            'attributes' => array(
                'type' => 'text',
                'id' => 'donated_by',
                'class' => 'width-90',
                'maxlength' => '30'
            )
        ));
        $this->add(array(
            'name' => 'product_attribute',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'product_attribute'
            )
        ));
        $this->add(array(
            'name' => 'approve',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'approve',
                'class' => 'save-btn',
                'value' => 'approve'
            )
        ));
    }

}
