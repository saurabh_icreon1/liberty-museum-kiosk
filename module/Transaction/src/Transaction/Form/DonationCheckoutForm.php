<?php

/**
 * This page is used to donaction add to cart
 * @package    Transaction_DonationAddToCartForm
 * @author     Icreon Tech - DT
 */

namespace Transaction\Form;

use Zend\Form\Form;

/**
 * This form is used to donation add to cart
 * @package    DonationAddToCartForm
 * @author     Icreon Tech - DT
 */
class DonationCheckoutForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('donation_checkout');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'referal_url_path',
            'attributes' => array(
                'type' => 'hidden',
            )
        ));
           
        $this->add(array(
            'name' => 'first_name',
            'attributes' => array(
                'type' => 'text',
            )
        ));
        $this->add(array(
            'name' => 'middle_name',
            'attributes' => array(
                'type' => 'text',
            )
        ));
        $this->add(array(
            'name' => 'last_name',
            'attributes' => array(
                'type' => 'text',
            )
        ));
        $this->add(array(
            'name' => 'email_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email_id'
            )
        ));
        $this->add(array(
            'name' => 'confirm_email_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'confirm_email_id'
            )
        ));
        /*$this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type' => 'Password',
                'id' => 'password',
            )
        ));
        $this->add(array(
            'name' => 'password_conf',
            'attributes' => array(
                'type' => 'Password',
                'id' => 'password_conf'
            )
        ));*/

        $this->add(array(
            'name' => 'address_1',
            'attributes' => array(
                'type' => 'text',
                'id' => 'address_1'
            )
        ));
        $this->add(array(
            'name' => 'address_2',
            'attributes' => array(
                'type' => 'text',
                'id' => 'address_2'
            )
        ));
        
        $this->add(array(
            'name' => 'zipcode',
            'attributes' => array(
                'type' => 'text',
            )
        ));
        
         $this->add(array(
            'name' => 'city',
            'attributes' => array(
                'type' => 'text',
                'id' => 'city'
            )
        ));
         
         $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'state',
            'options' => array(
                'value_options' => array(
                    '' => 'Select State',
                    '2' => 'AL'
                ),
            ),
            'attributes' => array(
                'id' => 'state',
                'value' => '' //set selected to 'blank'
            )
        ));
         
        $this->add(array(
            'name' => 'state_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'state_id'
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'country',
            'options' => array(
                'value_options' => array(
                    '' => 'Select Country',
                    '2' => 'India',
                    '3' => 'USA'
                ),
            ),
            'attributes' => array(
                'id' => 'country',
                'value' => '' //set selected to 'blank'
            )
        ));

        $this->add(array(
            'name' => 'credit_card_number',
            'attributes' => array(
                'type' => 'text',
            )
        ));
        $this->add(array(
            'name' => 'credit_card_code',
            'attributes' => array(
                'type' => 'password',
                'id'=>'credit_card_code'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'credit_card_exp_month',
            'options' => array(
                'value_options' => array(
                    '' => 'Select Month',
                ),
            ),
            'attributes' => array(
                'id' => 'credit_card_exp_month',
                'value' => '', //set selected to 'blank'
                'style'=>'margin-left:0px'
            )
        ));
         $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'credit_card_exp_year',
            'options' => array(
                'value_options' => array(
                    '' => 'Select Year',
                ),
            ),
            'attributes' => array(
                'id' => 'credit_card_exp_year',
                'value' => '', //set selected to 'blank'
                'style'=>'margin-left:0px'
            )
        ));
        
       
  
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'subscribe_newsletter',
            'attributes' => array(
                'value' => '1' //set checked to '1'
            )
        ));


        $this->add(array('type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 600
                )
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Pay Now',
                'class' => 'button green',
                'id' => 'submitbutton',
            ),
        ));
        
        
    }

}