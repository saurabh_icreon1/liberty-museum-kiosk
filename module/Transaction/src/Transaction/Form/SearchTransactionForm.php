<?php

/**
 * This page is used for search Transaction form.
 * @package    Transaction_SearchTransactionForm
 * @author     Icreon Tech - DT
 */

namespace Transaction\Form;

use Zend\Form\Form;
use Activity\Form\SearchActivityForm;

/**
 * This form is used for search Activity.
 * @package    SearchActivityForm
 * @author     Icreon Tech - DT
 */
class SearchTransactionForm extends Form {

    public function __construct($name = null) {
        parent::__construct('search_transaction');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'first_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'first_name'
            )
        ));
        $this->add(array(
            'name' => 'last_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_name'
            )
        ));
        $this->add(array(
            'name' => 'company_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'company_name'
            )
        ));
        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email'
            )
        ));
        
        
        $this->add(array(
            'name' => 'transaction_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'transaction_id'
            )
        ));
        $this->add(array(
            'name' => 'campaign_title',
            'attributes' => array(
                'type' => 'text',
                'id' => 'campaign_title',
                'class' => 'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'campaign_title_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'campaign_title_id'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'transaction_source_id',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'transaction_source_id',
                'class' => 'e1'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'received_date_range',
            'attributes' => array(
                'id' => 'received_date_range',
                'class' => 'e1 select-w-320',
                'onChange' => 'showDate(this.id,"date_range_div")'
            )
        ));
        $this->add(array(
            'name' => 'received_date_from',
            'attributes' => array(
                'type' => 'text',
                'class' => 'calendar-input width-120 cal-icon',
                'id' => 'added_date_from',
            )
        ));
        $this->add(array(
            'name' => 'received_date_to',
            'attributes' => array(
                'type' => 'text',
                'class' => 'calendar-input width-120 cal-icon',
                'id' => 'added_date_to',
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'item_status',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'item_status',
                'multiple' => 'multiple',
                'class' => 'e1 select-w-320'
            )
        ));

        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'transaction_status_id',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'transaction_status_id',
                'class' => 'e1'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'product_type_id',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'product_type_id',
                'multiple' => 'multiple',
                'class' => 'e1'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'payment_mode_id',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'payment_mode_id',
                'class' => 'e1'
            )
        ));
        $this->add(array(
            'name' => 'transaction_amount_from',
            'attributes' => array(
                'type' => 'text',
                'id' => 'transaction_amount_from',
                'class' => 'width-125'
            )
        ));
        $this->add(array(
            'name' => 'transaction_amount_to',
            'attributes' => array(
                'type' => 'text',
                'id' => 'transaction_amount_to',
                'class' => 'width-125'
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'pick_up',
            'checked_value' => '1',
            'unchecked_value' => '',
            'attributes' => array(
                'id' => 'pick_up',
                'class' => 'checkbox e2'
            ),
        ));
        $this->add(array(
            'name' => 'omx_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'omx_id'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'shipping_id',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'shipping_id',
                'class' => 'e1'
            )
        ));
        $this->add(array(
            'name' => 'searchtransactionbutton',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'searchtransactionbutton',
                'class' => 'search-btn',
                'value' => 'SEARCH'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'product_ids',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'product_ids',
                'class' => 'e1',
                'multiple' => 'multiple',
            )
        ));
        $this->add(array(
            'name' => 'batch_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'batch_id'
            )
        ));
        $this->add(array(
            'name' => 'type',
            'type' => 'Select',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'id' => 'type',
                'class' => 'e1',
                'value' => ''
            )
        ));
        $this->add(array(
            'name' => 'product_category',
            'type' => 'Select',
            'options' => array(
                'value_options' => array(
                    '' => 'Select Category'
                ),
            ),
            'attributes' => array(
                'id' => 'product_category',
                'class' => 'e1',
                'value' => ''
            )
        ));
	  $this->add(array(
            'name' => 'transaction_invoice_number',
            'attributes' => array(
                'type' => 'text',
                'id' => 'transaction_invoice_number',
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'saved_search',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'value' => '',
                'class' => 'e1 select-w-320',
                'id' => 'saved_search',
                'onChange' => 'getSavedTransactionSearchResult();'
            ),
        ));
    }

}

