<?php

/**
 * This page is used to add to cart membership detail form
 * @package    Transaction_MembershipDetailForm
 * @author     Icreon Tech - DT
 */

namespace Transaction\Form;

use Zend\Form\Form;

/**
 * This form is used to add to cart kiosk fee form
 * @package    KioskFeeForm
 * @author     Icreon Tech - DT
 */
class MembershipDetailForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('membership_form');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'membership_id',
            'options' => array(
                'value_options' => array(
                )
            ),
            'attributes' => array(
                'class' => 'e3 honoree',
            )
        ));
        $this->add(array(
            'name' => 'minimum_donation_amount[]',
            'attributes' => array(
                'type' => 'text',
            )
        ));
        $this->add(array(
            'name' => 'membershipaddtocart',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Add To Cart',
                'id' => 'membershipaddtocart',
                'class' => 'save-btn'
            ),
        ));
    }

}