<?php

/**
 * This form is used to select batch ID.
 * @package    Setting
 * @author     Icreon Tech - SR
 */

namespace Transaction\Form;

use Zend\Form\Form;

/**
 * This form is used to select batch Id
 * @package    Setting
 * @author     Icreon Tech - SR
 */
class CreateBatchIdForm extends Form {

    public function __construct($name = null) {

        parent::__construct('create-batch-id');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'batch_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'batch_id',
                'class' =>'width-124',
                'autocomplete' => 'off'
            )
        ));
        $this->add(array(
            'name' => 'settlement_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'settlement_date',
                'class' =>'width-124 cal-icon',
                'autocomplete' => 'off'
            )
        ));
        
        $this->add(array(
            'name' => 'save_batch',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save Batch',
                'id' => 'save_batch',
                'class' => 'save-btn',
            ),
        ));

    }

}
