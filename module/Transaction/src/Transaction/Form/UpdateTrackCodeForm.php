<?php

/**
 * This page is used for updating transaction status form.
 * @package    Transaction
 * @author     Icreon Tech - AS
 */

namespace Transaction\Form;

use Zend\Form\Form;

/**
 * This class is used for updating transaction status form.
 * @package    Transaction
 * @author     Icreon Tech - AS
 */
class UpdateTrackCodeForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('update_track_code');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'auto');
        $this->add(array(
            'type' => 'text',
            'name' => 'tracking_code',
            'attributes' => array(
                'id' => 'tracking_code',
                'class' => 'width_200'
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'product_id',
            'attributes' => array(
                'id' => 'product_id'
            )
        ));
        $this->add(array(
            'name' => 'update',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'update',
                'class' => 'green-btn',
                'value' => 'UPDATE'
            )
        ));
    }

}