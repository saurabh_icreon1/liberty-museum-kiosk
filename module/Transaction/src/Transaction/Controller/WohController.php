<?php

/**
 * This controller is used to  manage Wall of honour entries
 * @package    Transaction
 * @author     Icreon Tech - NS
 */

namespace Transaction\Controller;

//use PHPExcel; 
use Base\Controller\BaseController;
use Zend\View\Model\ViewModel;
use Zend\Http\Request;
use Zend\Authentication\AuthenticationService;
use Transaction\Form\SearchWohForm;
use Transaction\Form\EditWohEntryForm;
use Base\Model\SpreadsheetExcelReader;
use PHPExcel\Classes\PHPExcel;
use PHPExcel\Classes\PHPExcel\PHPExcel_IOFactory;

/**
 * This controller is used to manage Wall of honour entries
 * @category   Zend
 * @package    Transaction
 * @author     Icreon Tech - NS
 */
class WohController extends BaseController {

    protected $_documentTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_flashMessage = null;
    protected $_wohTable = null;

    /**
     * This function is used to constructor
     * @return     array
     * @param array $form
     * @author Icreon Tech - NS
     */
    function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        parent::__construct();
        $auth = new AuthenticationService();
        $this->auth = $auth;
    }

    /**
     * This function is used to get table object
     * @return     array
     * @param array $form
     * @author Icreon Tech - NS
     */
    public function getTable() {
        if (!$this->_wohTable) {
            $sm = $this->getServiceLocator();
            $this->_wohTable = $sm->get('User\Model\WohTable');
            $this->_adapter = $sm->get('dbAdapter');
        }
        return $this->_wohTable;
    }

    /**
     * This function is used to get config object
     * @return     array
     * @param array $form
     * @author Icreon Tech - NS
     */
    public function getConfig() {
        $sm = $this->getServiceLocator();
        $this->_translator = $sm->get('translator');
        $this->_config = $sm->get('Config');
    }

    /**
     * This action is used to woh Entries
     * @return json
     * @param void
     * @author Icreon Tech - NS
     */
    public function wohEntriesAction() {
        $this->layout('crm');
        $this->checkUserAuthentication();
        $this->getConfig();
        $createWohMessage = $this->_config['transaction_messages']['config']['wall_of_honour'];
        $searchWohForm = new SearchWohForm();
		/* item status */
        $getProductStatus = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductStatus();
        $productStatus = array();
        $productStatus[''] = 'Any';
        if ($getTransactionStatus !== false) {
            foreach ($getProductStatus as $key => $val) {
                $productStatus[$val['product_status_id']] = $val['product_status'];
            }
        }
        $searchWohForm->get('item_status')->setAttribute('options', $productStatus);
        /* end item status */
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'jsLangTranslate' => $createWohMessage,
            'searchWohForm' => $searchWohForm
        ));
        return $viewModel;
    }

    /*     * updateWohRecord
     * This action is used to get Woh Entries
     * @return json
     * @param void
     * @author Icreon Tech - NS
     */

    public function getWohEntriesAction() {

        $this->getTable();
        $response = $this->getResponse();
        $request = $this->getRequest();
        
        $this->getConfig();
        
        if ($request->isXmlHttpRequest() && $request->isPost() || $request->getPost('export') == 'excel') {
            $response = $this->getResponse();
            $postArray = $request->getPost()->toArray();
            parse_str($request->getPost('searchString'), $searchParam);
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit; // do not put $limit*($page - 1)
            
            $isExport = $request->getPost('export');
            
            if ($isExport != "" && $isExport == "excel") {
                ini_set('max_execution_time',0);
                $chunksize = $this->_config['export']['chunk_size'];
                $export_limit = $this->_config['export']['export_limit'];

               $limit = $searchParam['record_limit'] = $chunksize;
            }
            
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'modified_date') ? 'usr_wall_of_honor.woh_id' : $searchParam['sort_field'];

            if (isset($searchParam['transaction_date_from']) and trim($searchParam['transaction_date_from']) != "") {
                /*$searchParam['transaction_date_from'] = trim($searchParam['transaction_date_from']);
                $searchParam['transaction_date_from'] = date("Y-m-d", strtotime($searchParam['transaction_date_from']));*/
                
               $searchParam['transaction_date_from'] = $this->DateFormat($searchParam['transaction_date_from'], 'db_date_format_from');
            }

            if (isset($searchParam['transaction_date_to']) and trim($searchParam['transaction_date_to']) != "") {
                /*$searchParam['transaction_date_to'] = trim($searchParam['transaction_date_to']);
                $searchParam['transaction_date_to'] = date("Y-m-d", strtotime($searchParam['transaction_date_to']));*/
               
                $searchParam['transaction_date_to'] = $this->DateFormat($searchParam['transaction_date_to'], 'db_date_format_to');
            }
			if (!empty($searchParam['item_status'])) {

                if (is_array($searchParam['item_status'])) {
                    $searchParam['item_status'] = trim(implode(',', $searchParam['item_status']), ",");
                }
            }
			/* item status */
        $getProductStatus = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductStatus();
        $productStatus = array();
        $productStatus[''] = 'Any';
            foreach ($getProductStatus as $key => $val) {
                $productStatus[$val['product_status_id']] = $val['product_status'];
            }

        /* end item status */
            $page_counter = 1;
            $fileCounter = 1;
            $filename = "woh_entries_export_" . EXPORT_DATE_TIME_FORMAT;
            do{
                $seachResult = $this->_wohTable->getAllWohRecords($searchParam);
                $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
                $total = @$countResult[0]->RecordCount;
                $jsonResult = array();
                $arrCell = array();
                $subArrCell = array();
                
                if ($isExport == "excel") {
					if ($total > $chunksize) {
						@$number_of_pages = ceil($total / $chunksize);
						$page_counter++;
						$start = $chunksize * $page_counter - $chunksize;
						  
						$searchParam['start_index'] = $start;
						$searchParam['page'] = $page_counter;
					} else {
						$number_of_pages = 1;
						$page_counter++;
					}
				} else {
					$page_counter = $page;
					$number_of_pages = ceil($total / $limit);
					$jsonResult['page'] = $page;
					$jsonResult['records'] = $countResult[0]->RecordCount;
					$jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
				}
                
                if (!empty($seachResult)) {
                    $arrExport = array();
                    foreach ($seachResult as $val) {
                        $arrCell['id'] = $val['woh_id'];
                        $name = '';
                        if (isset($val['usr_title']) and trim($val['usr_title']) != '') {
                            $name .= trim($val['usr_title']) . '&nbsp;';
                        }
                        if (isset($val['usr_first_name']) and trim($val['usr_first_name']) != '') {
                            $name .= trim($val['usr_first_name']) . '&nbsp;';
                        }
                        if (isset($val['usr_middle_name']) and trim($val['usr_middle_name']) != '') {
                            $name .= trim($val['usr_middle_name']) . '&nbsp;';
                        }
                        if (isset($val['usr_last_name']) and trim($val['usr_last_name']) != '') {
                            $name .= trim($val['usr_last_name']) . '&nbsp;';
                        }
                        $transactionId = '<a class="txt-decoration-underline" href="/get-transaction-detail/' . $this->encrypt($val['transaction_id']) . '/' . $this->encrypt('view') . '">' . $val['transaction_id'] . '</a>';
                        if ($val['status'] == 1)
                            $action = '<a href="javscript:void(0);" onClick="editWohEntries(\'' . $this->encrypt($val['woh_id']) . '\');return false;" class="edit-icon"><div class="tooltip">Edit<span></span></div></a>';
                        else
                            $action = '';

                        $honoree_name = isset($val['final_name'])?trim($val['final_name']):'';

						$honoree_name_csv =  str_replace('&nbsp;',' ',str_replace('<br>','&nbsp;',$honoree_name));
						
						$status = ($val['status'] == '0') ? 'Yes' : 'No';
                        $wohStatus = (!empty($val['woh_status'])) ? $productStatus[$val['woh_status']] : '';
						
                        if ($isExport == "excel") {
                            $arrExport[] = array("Contact Name" => str_replace('&nbsp;',' ',$name), "Email" => $val['usr_email_id'], "Transaction #" => $val['transaction_id'], "Transaction Date" => $this->OutputDateFormat($val['transaction_date'], 'dateformatampm'), "Name Format" => $val['product_name'], "Honoree Name" =>  utf8_decode($honoree_name_csv), "Panel #" => $val['panel_no'], "Approval Required ?" => $status, "Amount($)" => $val['product_total'],"Item Status"=>$wohStatus);
                        }else{
                            $arrCell['cell'] = array($name, $val['usr_email_id'], $transactionId, $this->OutputDateFormat($val['transaction_date'], 'dateformatampm'), $val['product_name'], $honoree_name, $val['panel_no'], $status, $val['product_total'], $wohStatus,$action);
                        }
                        
                        $subArrCell[] = $arrCell;
                        $jsonResult['rows'] = $subArrCell;
                    }
                    
                    if ($isExport == "excel") { 
                        $fileLimit = $this->_config['csv_file']['max_records'];
                        $fileCounter = $this->exportCSV($arrExport,$filename,$fileLimit,$fileCounter);
                        //$this->arrayToCsv($arrExport, $filename, $page_counter);
                        $arrExport = array();
                    }
                } else {
                    $jsonResult['rows'] = array();
                } 
                 
            }while($page_counter <= $number_of_pages && $isExport == "excel");            
            
            if ($isExport == "excel") {
                $this->downloadSendHeaders($filename . ".zip");
                //removing temp files
                $tempfilePath = $this->_config['file_upload_path']['temp_data_upload_dir'] . 'export/';
                //sudo chmod -R ug+rw export/   folder should have this permission
                $tempFiles = glob($tempfilePath .$filename. '*' , GLOB_BRACE);
                foreach ($tempFiles as $file)
                {
                    if (is_file($file))
                    {
                        unlink($file);
                    }
                }
                die;
            } else {
                return $response->setContent(\Zend\Json\Json::encode($jsonResult));
            }
        }
    }

    /**
     * This action is used to woh Bulk Panel Assignment
     * @return json
     * @param void
     * @author Icreon Tech - NS
     */
    public function wohBulkPanelAssignmentAction() {
        $this->layout('crm');
        $this->checkUserAuthentication();
        $this->getConfig();
        $createWallOfHonorMessage = $this->_config['transaction_messages']['config']['wall_of_honour'];

        $select_year = array('' => 'Select');
        for ($i = 1999; $i <= date("Y") + 2; $i++) {
            $select_year[$this->encrypt($i)] = $i;
        }

        $searchWohForm = new SearchWohForm();
        $searchWohForm->get('select_year')->setAttribute('options', $select_year);

        $viewModel = new ViewModel();

        $viewModel->setVariables(array(
            'jsLangTranslate' => $createWallOfHonorMessage,
            'searchWohForm' => $searchWohForm
        ));
        return $viewModel;
    }

    /**
     * This action is used to woh Bulk Panel Assignment Download
     * @return json
     * @param void
     * @author Icreon Tech - NS
     */
    public function wohBulkPanelAssignmentDownloadAction() {

        $this->checkUserAuthentication();
        $this->getTable();
        $this->getConfig();
        $objPHPExcel = new \PHPExcel();
        $createWallOfHonorMessage = $this->_config['transaction_messages']['config']['wall_of_honour'];
        $filename = "wall-of-honor-" . time() . ".xls";

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("SOLEIF CRM")
                ->setLastModifiedBy("SOLEIF CRM")
                ->setTitle("Office 2007 XLSX WOH Document")
                ->setSubject("Office 2007 XLSX WOH Document")
                ->setDescription("Wall of Honor document for Office 2007 XLSX, generated by SOLEIF CRM.")
                ->setKeywords("office 2007 openxml php")
                ->setCategory("Wall of Honor");

// Add some data
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', $createWallOfHonorMessage['WOH_HEADING_LIST_SNO'])
                ->setCellValue('B1', $createWallOfHonorMessage['WOH_HEADING_LIST_ID'])
                ->setCellValue('C1', $createWallOfHonorMessage['WOH_HEADING_LIST_CONTACT_NAME'])
                ->setCellValue('D1', $createWallOfHonorMessage['WOH_HEADING_LIST_EMAIL'])
                ->setCellValue('E1', $createWallOfHonorMessage['WOH_HEADING_LIST_TRANSACTION'])
                ->setCellValue('F1', $createWallOfHonorMessage['WOH_HEADING_LIST_TRANSACTION_DATE'])
                ->setCellValue('G1', $createWallOfHonorMessage['WOH_HEADING_LIST_NAME_FORMAT'])
                ->setCellValue('H1', $createWallOfHonorMessage['WOH_HEADING_LIST_HONOREE_NAME'])
                ->setCellValue('I1', $createWallOfHonorMessage['WOH_HEADING_LIST_PANEL'])
                ->setCellValue('J1', $createWallOfHonorMessage['WOH_HEADING_LIST_AMOUNT']);

        try {

            $param = $this->params()->fromRoute();

            if (isset($param['year']) and trim($param['year']) != "") {

                $year = $this->decrypt($param['year']);
                $year = trim($year);

                $searchParam = array();
                $searchParam['sort_field'] = 'usr_wall_of_honor.woh_id';
                $searchParam['sort_order'] = 'desc';
                $searchParam['transaction_year'] = $year;
                $searchParam['panel_assigned'] = "0";
                $searchParam['approval_required'] = 1;
				$searchParam['woh_status_to_leave'] = 1;
                $seachResult = $this->_wohTable->getAllWohRecords($searchParam);

                $sn = 1;
                $exl = 2;
                if (isset($seachResult) and is_array($seachResult) and count($seachResult) > 0) {
                    foreach ($seachResult as $val) {

                        $name = '';
                        if (isset($val['usr_title']) and trim($val['usr_title']) != '') {
                            $name .= trim($val['usr_title']) . ' ';
                        }
                        if (isset($val['usr_first_name']) and trim($val['usr_first_name']) != '') {
                            $name .= trim($val['usr_first_name']) . ' ';
                        }
                        if (isset($val['usr_middle_name']) and trim($val['usr_middle_name']) != '') {
                            $name .= trim($val['usr_middle_name']) . ' ';
                        }
                        if (isset($val['usr_last_name']) and trim($val['usr_last_name']) != '') {
                            $name .= trim($val['usr_last_name']) . ' ';
                        }

                        $honoree_name = isset($val['final_name'])?$val['final_name']:'';
                        $objPHPExcel->getActiveSheet()->setCellValue('A' . $exl, $sn)
                                ->setCellValue('B' . $exl, $val['woh_id'])
                                ->setCellValue('C' . $exl, $name)
                                ->setCellValue('D' . $exl, $val['usr_email_id'])
                                ->setCellValue('E' . $exl, $val['transaction_id'])
                                ->setCellValue('F' . $exl, $this->OutputDateFormat($val['transaction_date'], 'dateformatampm'))
                                ->setCellValue('G' . $exl, $val['product_name'])
                                ->setCellValue('H' . $exl, str_replace(array("<br>", "&nbsp;"), " ", $honoree_name))
                                ->setCellValue('I' . $exl, $val['panel_no'])
                                ->setCellValue('J' . $exl, $val['product_total']);

                        $sn++;
                        $exl++;
                    }
                }
            }
        } catch (Exception $e) {
            
        }


        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Wall of Honor');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);


        // Redirect output to a client�s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = new \PHPExcel_IOFactory;
        $objWriter = $objWriter->createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }

    /**
     * This action is used to woh Bulk Panel Assignment Upload
     * @return json
     * @param void
     * @author Icreon Tech - NS
     */
    public function wohBulkPanelAssignmentUploadAction() {
        $this->checkUserAuthentication();
        $this->getTable();
        $this->getConfig();

        $createWallOfHonorMessage = $this->_config['transaction_messages']['config']['wall_of_honour'];
        $response = $this->getResponse();
        $request = $this->getRequest();

        try {
            $fileExt = $this->GetFileExt($_FILES['upload_panel_entries']['name']);

            if ($fileExt[1] != 'xls') {
                $messages = array('status' => "error", 'message' => $createWallOfHonorMessage['WOH_HEADING_BULK_ASSIGNMENT_UPLOAD_WRONG_FORMAT']);
            } else {
                $import_handler = new SpreadsheetExcelReader($_FILES['upload_panel_entries']['tmp_name']);

                $imported_data = $import_handler->dumpToArray();

                $flag = 0;
                if (!empty($imported_data)) {
                    foreach ($imported_data as $key => $val) {
                        if ($key > 0) {

                            if (isset($val[1]) and trim($val[1]) != "" and isset($val[8]) and trim($val[8]) != "") {
                                $postUserData = array();
                                $postUserData['woh_id'] = trim(strip_tags($val[1]));
                                $postUserData['panel_number'] = trim(strip_tags($val[8]));
                                $postUserData['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                                $postUserData['modified_date'] = DATE_TIME_FORMAT;

                                if (isset($postUserData['woh_id']) and is_numeric($postUserData['woh_id'])) {
                                    $result = $this->_wohTable->updateWohRecord($postUserData);

                                    $title = isset($result[0]['title']) ? $result[0]['title'] : '';
                                    $first_name = isset($result[0]['first_name']) ? $result[0]['first_name'] : '';
                                    $middle_name = isset($result[0]['middle_name']) ? $result[0]['middle_name'] : '';
                                    $last_name = isset($result[0]['last_name']) ? $result[0]['last_name'] : '';
                                    $email_id = isset($result[0]['email_id']) ? $result[0]['email_id'] : '';
                                    $panel_no = isset($result[0]['panel_no']) ? $result[0]['panel_no'] : '';

                                    if (isset($email_id) and trim($email_id) != "" and filter_var(trim($email_id), FILTER_VALIDATE_EMAIL)) {
                                        $userDataArr = array();
                                        $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                                        $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                                        $userDataArr['title'] = $title;
                                        $userDataArr['first_name'] = $first_name;
                                        $userDataArr['last_name'] = $last_name;
                                        $userDataArr['email_id'] = $email_id;
                                        $userDataArr['woh_panel_num'] = $panel_no;
                                        $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, 33, $this->_config['email_options']);
                                    }
                                    $flag++;
                                }
                            }
                        }
                    }
                }
                if ($flag > 0) {
                    $messages = array('status' => "success", 'message' => $createWallOfHonorMessage['WOH_HEADING_BULK_ASSIGNMENT_UPLOAD_EXCEL_IMPORTED']);
                } else if ($flag == 0) {
                    $messages = array('status' => "error", 'message' => $createWallOfHonorMessage['WOH_HEADING_BULK_ASSIGNMENT_UPLOAD_EXCEL_IMPORTED_NOT']);
                }
            }
        } catch (Exception $e) {
            $messages = array('status' => "error", 'message' => $createWallOfHonorMessage['WOH_HEADING_BULK_ASSIGNMENT_UPLOAD_EXCEL_IMPORTED_NOT']);
        }
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    public function editWohEntriesAction() {
        $this->getConfig();
        $this->checkUserAuthentication();
        $this->layout('popup');
        $request = $this->getRequest();
        $this->getTable();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        $edit_woh_entry = new EditWohEntryForm();
        $edit_woh_entry = new EditWohEntryForm();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $data = $request->getPost()->toArray();
            $wohData = array();
            $wohData['panel_number'] = $data['panel_number'];
            $wohData['woh_id'] = $this->decrypt($data['woh_id']);
            $wohData['modified_by'] = $this->auth->getIdentity()->crm_user_id;
            $wohData['modified_date'] = DATE_TIME_FORMAT;
            $update = $this->_wohTable->updateWohRecord($wohData);
            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        } else {
            $wohId = $this->decrypt($params['woh_id']);
            $searchParam = array();
            $searchParam['woh_id'] = $wohId;
            $seachResult = $this->_wohTable->getAllWohRecords($searchParam);
            $panel_no = $seachResult[0]['panel_no'];
            $edit_woh_entry->get('panel_number')->setValue($panel_no);
            $edit_woh_entry->get('woh_id')->setValue($params['woh_id']);
            $viewModel = new ViewModel();
            $viewModel->setVariables(array(
                'edit_woh_entry' => $edit_woh_entry,
                'jsLangTranslate' => $this->_config['transaction_messages']['config']['wall_of_honour']
            ));
            return $viewModel;
        }
    }

    public function updateWohEntryAction() {
        $this->getConfig();
        $response = $this->getResponse();
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $this->getTable();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $data = $request->getPost()->toArray();
            $data['modified_by'] = $this->auth->getIdentity()->crm_user_id;
            $data['modified_date'] = DATE_TIME_FORMAT;
            if ($this->_wohTable->updateWohRecord($data) == true) {
                $messages = array('status' => "success");
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
    }

}

