<?php

/**
 * This controller is used to manage Transactions
 * @package    Transaction
 * @author     Icreon Tech - DG
 */

namespace Transaction\Controller;

use Base\Controller\BaseController;
use Zend\View\Model\ViewModel;
use Transaction\Model\Transaction;
use User\Model\Contact;
use Common\Model\Common;
use Pledge\Model\Pledge;
use Zend\Http\Request;
use Zend\Authentication\AuthenticationService;
use Transaction\Form\BulkDonationSearchForm;
use Transaction\Form\BulkDonationCreateForm;
use Transaction\Form\AddMoreDonationsForm;
use Base\Model\SpreadsheetExcelReader;

/**
 * This controller is used to manage Transactions
 * @category   Zend
 * @package    Transaction
 * @author     Icreon Tech - DG
 */
class BulkdonationController extends BaseController {

    protected $_transactionTable = null;
    protected $_documentTable = null;
    protected $_productTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_flashMessage = null;

    function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        parent::__construct();
        $auth = new AuthenticationService();
        $this->auth = $auth;
    }

    /**
     * This function is used to get config object
     * @return     array
     * @param array $form
     * @author Icreon Tech - SK
     */
    public function getConfig() {
        $sm = $this->getServiceLocator();
        $this->_translator = $sm->get('translator');
        $this->_config = $sm->get('Config');
    }

    /**
     * This function is used to get table object
     * @return     array
     * @param array $form
     * @author Icreon Tech - SK
     */
    public function getTransactionTable() {
        if (!$this->_transactionTable) {
            $sm = $this->getServiceLocator();
            $this->_transactionTable = $sm->get('Transaction\Model\TransactionTable');
            $this->_documentTable = $sm->get('Passenger\Model\DocumentTable');
            $this->_productTable = $sm->get('Product\Model\ProductTable');
            $this->_adapter = $sm->get('dbAdapter');
            $this->_config = $sm->get('Config');
        }
        return $this->_transactionTable;
    }

    /**
     * This function is used to list batch lists of bulk donations
     * @author Icreon Tech - SK
     */
    public function donationBulkEntriesAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $this->getConfig();

        $request = $this->getRequest();
        $form = new BulkDonationSearchForm();
        /* Get date range */
        $historyDateRange = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTransactionRange();
        $dateRange = array("" => "Select");
        foreach ($historyDateRange as $key => $val) {
            $dateRange[$val['range_id']] = $val['range'];
        }
        $form->get('batch_date_range')->setAttribute('options', $dateRange);

        $searchParam = array();

        if ($request->isXmlHttpRequest() && $request->isPost() || $request->getPost('export') == 'excel') {
            $response = $this->getResponse();
            parse_str($request->getPost('searchString'), $searchParam);

            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = ($limit * $page) - $limit;
            $isExport = $request->getPost('export');

            if ($isExport != "" && $isExport == "excel") {
                $chunksize = $this->_config['export']['chunk_size'];
                $export_limit = $this->_config['export']['export_limit'];

                $searchParam['recordLimit'] = $chunksize;
            }

            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'modified_date') ? 'batch_date' : $searchParam['sort_field'];
            if (isset($searchParam['batch_date_range']) && $searchParam['batch_date_range'] != 1 && $searchParam['batch_date_range'] != '') {
                $dateRange = $this->getDateRange($searchParam['batch_date_range']);
                $searchParam['batch_date_from'] = $this->DateFormat($dateRange['from'], 'db_date_format');
                $searchParam['batch_date_to'] = $this->DateFormat($dateRange['to'], 'db_date_format');
            } else if (isset($searchParam['batch_date_range']) && $searchParam['batch_date_range'] == '') {
                $searchParam['batch_date_from'] = '';
                $searchParam['batch_date_to'] = '';
            } else {
                if (isset($searchParam['batch_date_from']) && $searchParam['batch_date_from'] != '') {
                    $searchParam['batch_date_from'] = $this->DateFormat($searchParam['batch_date_from'], 'db_date_format');
                }
                if (isset($searchParam['batch_date_to']) && $searchParam['batch_date_to'] != '') {
                    $searchParam['batch_date_to'] = $this->DateFormat($searchParam['batch_date_to'], 'db_date_format');
                }
            }
            $transaction = new Transaction($this->_adapter);
            $number_of_pages = 1;
            $page_counter = 1;
            do {
                $bulkDonationParams = $transaction->getBulkDonationEntriesArray($searchParam);
                $seachResult = $this->_transactionTable->getBulkEntries($bulkDonationParams);
                if (!empty($seachResult)) {
                    $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
                    $total = @$countResult[0]->RecordCount;
                    $jsonResult = array();
                    $arrCell = array();
                    $subArrCell = array();

                    if ($isExport == "excel") {
                        if ($total > $chunksize) {
                            @$number_of_pages = ceil($total / $chunksize);
                            $page_counter++;
                            $start = $chunksize * $page_counter - $chunksize;
                            $searchParam['start_index'] = $start;
                            $searchParam['page'] = $page_counter;
                        } else {
                            $number_of_pages = 1;
                            $page_counter++;
                        }
                    } else {
                        $page_counter = $page;
                        $number_of_pages = ceil($total / $limit);
                        $jsonResult['records'] = $countResult[0]->RecordCount;
                        $jsonResult['page'] = $request->getPost('page');
                        $jsonResult['total'] = $number_of_pages;
                    }

                    foreach ($seachResult as $val) {
						if($val['amount']!='0'){
							$arrCell['id'] = $val['batch_group_id'];

							if ($isExport == "excel") {
								$arrExport[] = array("Batch #" => $val['batch_no'], "Amount" => '$ ' . $this->Currencyformat($val['amount']));
							} else {
								$encryptId = $this->encrypt($val['payment_batch_id']);
								$viewLink = '<a href="/get-batch-detail/' . $encryptId . '" class="view-icon"><div class="tooltip">View<span></span></div></a>';
								$actions = '<div class="action-col">' . $viewLink . '</div>';

								$arrCell['cell'] = array($val['payment_batch_id'], $val['batch_no'], '$ ' . $this->Currencyformat($val['amount']), $actions);
							}
							$subArrCell[] = $arrCell;
							$jsonResult['rows'] = $subArrCell;
						}                        
                    }
					if ($isExport == "excel") {
                        $filename = "bulk_donation_export_" . date("Y-m-d") . ".csv";
                        $this->arrayToCsv($arrExport, $filename, $page_counter);
                    }
                } else {
                    $jsonResult['rows'] = array();
                }
            } while ($page_counter <= $number_of_pages && $isExport == "excel");

            if ($isExport == "excel") {
                $this->downloadSendHeaders("bulk_donation_export_" . date("Y-m-d") . ".csv");
                die;
            } else {
                return $response->setContent(\Zend\Json\Json::encode($jsonResult));
            }
        } else {
            $this->layout('crm');
            $viewModel = new ViewModel();

            $messages = array();
            $viewModel->setVariables(array(
                'form' => $form
                    )
            );
            return $viewModel;
        }
    }

    /**
     * This function is used to insert the record in payment receive table.
     * @param array
     * @author Icreon Tech - SR
     */
    public function insertIntoPaymentReceive($dataArray = array()) {
        $this->getTransactionTable();
        $receivedPaymentIdArray = array();
        $totalCreditCardAmount = 0;
        $transaction = new Transaction($this->_adapter);
        $paymentTransactionData = array();
        $paymentTransactionData['amount'] = $dataArray['amount'];
        $paymentTransactionData['transaction_id'] = $dataArray['transaction_id'];
        $paymentTransactionData['authorize_transaction_id'] = $dataArray['authorize_transaction_id'];
        $paymentTransactionData['transaction_payment_id'] = $dataArray['transaction_payment_id'];
        $paymentTransactionData['added_by'] = isset($this->auth->getIdentity()->crm_user_id) ? $this->auth->getIdentity()->crm_user_id : '';
        $paymentTransactionData['added_date'] = DATE_TIME_FORMAT;
        $paymentTransactionData['modify_by'] = isset($this->auth->getIdentity()->crm_user_id) ? $this->auth->getIdentity()->crm_user_id : '';
        $paymentTransactionData['modify_date'] = DATE_TIME_FORMAT;
        $paymentTransactionDataArr['0'] = $dataArray['transaction_id'];
        $creditTransactionPayments = $this->_transactionTable->getCreditTransactionPayment($paymentTransactionDataArr);
        // $paymentTransactionData['transaction_payment_id'] = $creditTransactionPayments[0]['transaction_payment_id'];
        $paymentTransactionData = $transaction->getTransactionPaymentReceiveArr($paymentTransactionData);
        $paymentTransactionData['10'] = $dataArray['product_type_id'];
        $paymentTransactionData['11'] = @$dataArray['table_auto_id'];
        $paymentTransactionData['12'] = @$dataArray['table_type'];
        $paymentTransactionData['13'] = @$dataArray['isPaymentReceived'];
        $paymentTransactionData['14'] = @$dataArray['batch_id'];
        $paymentTransactionData['9'] = ''; // credit_card_type_id
        return $this->_transactionTable->saveTransactionPaymentReceive($paymentTransactionData);
    }

    /**
     * This function is used to add batch for bulk donations
     * @author Icreon Tech - SK
     */
    public function createBatchAction() {
		
		$this->checkUserAuthentication();
        $this->layout('crm');
        $this->getConfig();
        $this->getTransactionTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
		$message = $this->_config['transaction_messages']['config']['create_donation_batch'];
        $form = new BulkDonationCreateForm();
        /* payment mode */
        $getPaymentMode = $this->getServiceLocator()->get('Common\Model\CommonTable')->getPaymentMode();

        $paymentMode = array();
        $paymentMode[''] = 'Select One';
        if ($getPaymentMode !== false) {
            foreach ($getPaymentMode as $key => $val) {
                if ($val['payment_mode_id'] != "1") {
                    $paymentMode[$val['payment_mode_id']] = $val['payment_mode'];
                }
            }
        }
		if(isset($paymentMode[5])){
		 unset($paymentMode[5]);
		}
        $form->get('payment_mode_id[]')->setAttribute('options', $paymentMode);
        $form->get('payment_mode_id[]')->setValue(3);

        if ($request->isPost()) { 
            $totalSubtotal = 0;
            $totalDiscount = 0;
            $totalAmountProduct = 0;
            $taxableAmount = 0;
            $shippingAmount = 0;
            $totalTaxableAmount = 0;
            $userId = 0;
            $i = 0;

            $donationBatchAmount = 0;
            $crm_user_id = !(empty($this->auth->getIdentity()->crm_user_id)) ? $this->auth->getIdentity()->crm_user_id : '';

            if ($request->getPost('uploadbutton') || $request->getPost('submitandlock')) {

				$generalCampaign = $this->_transactionTable->getGeneralCampaign();

                $transaction = new Transaction($this->_adapter);
                $data = $request->getPost()->toArray();
                $cartProductArr = array();
                $cartProductArr['source_type'] = !empty($postArr['source_type']) ? $postArr['source_type'] : 'backend';
                if (!empty($data['amount']) && $data['amount'][0] != '') {
                    foreach ($data['amount'] as $key1 => $value1) {
                        if ($value1 != '' && $data['contact_name_id'][$key1] != '')
                            $donationBatchAmount+= $value1;
                    }

                    $batchData['payment_batch_id'] = $data['payment_batch_id'][0];
                    $batchData['amount'] = $donationBatchAmount;
                    $batchData['batch_date'] = DATE_TIME_FORMAT;
                    $batchData['added_by'] = $crm_user_id;
                    $batchData['added_date'] = DATE_TIME_FORMAT;

                    $batchGroupId = $this->_transactionTable->saveDonationBatches($batchData);

                    foreach ($data['amount'] as $key => $value) {
                        if ($data['contact_name_id'][$key] != '') {
                            $productLabel = '';
                            $userId = $data['contact_name_id'][$key];
                            $productQty = 1;
                            $productPrice = $value;
                            $productSubtotal = $value;
                            $membershipDiscount = 0;
                            $productDiscount = 0;
                            $productTaxableAmount = 0;
                            $productTotal = $value;
                            $totalSubtotal = $value;
                            $totalDiscount = 0;
                            $totalAmountProduct = $value;
                            $taxableAmount = 0;

							if($data['campaign'][$key]==''){
								$campaign_code = $generalCampaign[0]['campaign_code'];
								$campaign_id = $generalCampaign[0]['campaign_id'];
							}else{
								$campaign_code = $data['campaign'][$key];
								$campaign_id = $data['campaign_id'][$key];
							}

                            /** Transaction * */
                            $finalTransaction['user_id'] = $data['contact_name_id'][$key];
                            $finalTransaction['transaction_source_id'] = $this->_config['transaction_source']['transaction_source_crm_id'];
                            $finalTransaction['num_items'] = 1;
                            $finalTransaction['sub_total_amount'] = $totalSubtotal;
                            $finalTransaction['shipping_amount'] = $shippingAmount;
                            $finalTransaction['total_tax'] = $totalTaxableAmount;
                            $finalTransaction['transaction_amount'] = $totalAmountProduct;
                            $finalTransaction['transaction_status_id'] = 10;
                            $finalTransaction['transaction_type'] = 1;
                            $finalTransaction['transaction_date'] = DATE_TIME_FORMAT;
                            $finalTransaction['added_by'] = $crm_user_id;
                            $finalTransaction['added_date'] = DATE_TIME_FORMAT;
                            $finalTransaction['modify_by'] = $crm_user_id;
                            $finalTransaction['modify_date'] = DATE_TIME_FORMAT;
							$finalTransaction['don_amount'] = $totalAmountProduct;
							$finalTransaction['campaign_code'] = $campaign_code;
							$finalTransaction['campaign_id'] = $campaign_id;
                                                        
                        if(isset($this->auth->getIdentity()->crm_user_id) && !empty($this->auth->getIdentity()->crm_user_id))
                            $transactionChannelId = 2; // Direct mail
                        else
                            $transactionChannelId = 5; // Web                                                          
                                                        
							$finalTransaction['channel_id'] = $transactionChannelId;
							$finalTransaction['total_product_revenue'] = $totalAmountProduct;
							

                            $finalTransactionArr = $transaction->getFinalTransactionArr($finalTransaction);
                            $transactionId = $this->_transactionTable->saveFinalTransaction($finalTransactionArr);
                            /* end add transaction record */
				
				            $this->_transactionTable->updateRevenueGoal($finalTransactionArr);
				            $this->_transactionTable->updateChannelRevenue($finalTransactionArr);							
							
							/** campaign revenue update **/
							//if (!empty($data['campaign_id'][$key])) {
								$campArrayBack['campaign_id'] = $campaign_id;
								$campArrayBack['revenue_recieved'] = $totalAmountProduct;
								// $this->getServiceLocator()->get('Campaign\Model\CampaignTable')->updateRevenueGoal($campArrayBack);
							//}
							/****/

                            $userArr = array('user_id' => $data['contact_name_id'][$key]);
                            $contactDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getContact($userArr);

                            $user_address_info_primary = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations(array('user_id' => $data['contact_name_id'][$key], 'address_location' => '1'));

                            if (empty($user_address_info_primary)) {
                                $user_address_info_primary = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations(array('user_id' => $data['contact_name_id'][$key], 'address_location' => '2'));
                            }

                            if (!empty($user_address_info_primary)) {

                                $user_phone_info_primary = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserPhoneInformations(array('user_id' => $data['contact_name_id'][$key], 'is_primary' => '1'));

                                if (empty($user_phone_info_primary)) {
                                    $user_phone_info_primary = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserPhoneInformations(array('user_id' => $data['contact_name_id'][$key]));
                                }

                                $billingShipping = array();

                                $billingShipping['transaction_id'] = $transactionId;
                                $billingShipping['billing_title'] = $contactDetail[0]['title'];
                                $billingShipping['billing_first_name'] = $contactDetail[0]['first_name'];
                                $billingShipping['billing_last_name'] = $contactDetail[0]['last_name'];
                                $billingShipping['billing_company'] = $contactDetail[0]['company_name'];


                                $address = $user_address_info_primary[0]['street_address_one'];
                                if ($user_address_info_primary[0]['street_address_two'] != '')
                                    $address.=", " . $user_address_info_primary[0]['street_address_two'];
                                if ($user_address_info_primary[0]['street_address_three'] != '')
                                    $address.=", " . $user_address_info_primary[0]['street_address_three'];

                                $billingShipping['billing_address'] = $address;

                                $billingShipping['billing_city'] = $user_address_info_primary[0]['city'];
                                $billingShipping['billing_state'] = $user_address_info_primary[0]['state'];
                                $billingShipping['billing_zip_code'] = $user_address_info_primary[0]['zip_code'];
                                $billingShipping['billing_country'] = $user_address_info_primary[0]['country_id'];

                                if (!empty($user_phone_info_primary)) {
                                    $billingShipping['billing_phone'] = $user_phone_info_primary[0]['country_code'] . '-' . $user_phone_info_primary[0]['area_code'] . '-' . $user_phone_info_primary[0]['phone_number'];
                                }


                                $billingShipping['added_by'] = $crm_user_id;
                                $billingShipping['modify_by'] = $crm_user_id;


                                $transactionBillingShippingArr = $transaction->getTransactionBillingShipping($billingShipping);

                                $transactionBillingShippingId = $this->_transactionTable->saveTransactionBillingShippingDetail($transactionBillingShippingArr);
                            }




                            /* update membership */
                            if ($totalAmountProduct > 0) {
                                $userPurchasedProductArr = array();
                                $userPurchasedProductArr['user_id'] = $userId;
                                $date_range = $this->getDateRange(4);
                                $userPurchasedProductArr['from_date'] = $this->DateFormat($date_range['from'], 'db_date_format_from');
                                $userPurchasedProductArr['to_date'] = $this->DateFormat($date_range['to'], 'db_date_format_to');
                                $getTotalUserPurchaseProductAmountArr = $transaction->getTotalUserPurchaseProductAmountArr($userPurchasedProductArr);
                                $totalAmountPurchased = $this->_transactionTable->getTotalUserPurchased($getTotalUserPurchaseProductAmountArr);
								$userMembershipArr = array();
                                $userMembershipArr['transaction_amount'] = $totalAmountPurchased;
                                $userMembershipArr['user_id'] = $userId;
                                $userMembershipArr['transaction_id'] = $transactionId;
                                $userMembershipArr['transaction_date'] = DATE_TIME_FORMAT;
                                $this->updateUserMembership($userMembershipArr);
                            }
                            /* End of Update membership */

                            /* Add payment information  of transaction */
                            $amount_total = $totalAmountProduct;
                            $paymentTransactionData = array();
                            $paymentTransactionData['amount'] = $value;
                            $paymentTransactionData['transaction_id'] = $transactionId;
                            $paymentTransactionData['payment_mode_id'] = $data['payment_mode_id'][$key];
                            if ($data['payment_mode_id'][$key] == 3) {
                                $paymentTransactionData['cheque_number'] = $data['check_number'][$key];
                            }
                            $paymentTransactionData['added_by'] = $crm_user_id;
                            $paymentTransactionData['added_date'] = DATE_TIME_FORMAT;
                            $paymentTransactionData['modify_by'] = $crm_user_id;
                            $paymentTransactionData['modify_date'] = DATE_TIME_FORMAT;

                            $transactionCheckPaymentArr = $transaction->getTransactionPaymentArr($paymentTransactionData);
                            $transactionPaymentId = $this->_transactionTable->saveTransactionPaymentDetail($transactionCheckPaymentArr);
                            /* End of Payment Information */

                            $companydata = array();
                            $companydata['company_name'] = $data['company_name'][$key];

                            $companies = $this->getServiceLocator()->get('Common\Model\CommonTable')->getMasterCompany($companydata);
                            if (!empty($companies)) {
                                $company = $companies[0];
                                $donationProductData['company_id'] = $company['company_id'];
                            } else {
                                $common = new Common($this->_adapter);
                                $company_array = $common->getCompanyArr($companydata);
                                $company_id = $this->getServiceLocator()->get('Common\Model\CommonTable')->createMasterCompany($company_array);
                                $donationProductData['company_id'] = $company_id;
                            }

                            /* Add Donation Product */
                            $donationProductData['campaign_id'] = $campaign_id;
                            $donationProductData['batch_group_id'] = $batchGroupId;
                            $donationProductData['transaction_id'] = $transactionId;
                            $donationProductData['product_mapping_id'] = 1;
                            $donationProductData['product_type_id'] = 2;
							$donationProductData['product_id'] = 8;
							//$donationProductData['product_name'] = 'Donation';
                            $donationProductData['product_price'] = $value;
                            $donationProductData['product_sub_total'] = $value;
                            $donationProductData['product_discount'] = 0;
                            $donationProductData['product_tax'] = 0;
                            $donationProductData['product_total'] = $value;
                            $donationProductData['product_status_id'] = 10;
                            $donationProductData['purchase_date'] = DATE_TIME_FORMAT;
                            $donationProductData['is_deleted'] = 0;
                            $donationProductData['added_by'] = $crm_user_id;
                            $donationProductData['added_date'] = DATE_TIME_FORMAT;
                            $donationProductData['modify_by'] = $crm_user_id;
                            $donationProductData['modified_date'] = DATE_TIME_FORMAT;
                            $donationProductData['company_name'] = $data['company_name'][$key];
							$donationProductData['user_id'] = $userId;
							$donationProductData['num_quantity'] = 1;
							
                            $productSearchParam['id'] = 8;/** product Donation */
					        $productResultArr = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProductInfo($productSearchParam);   
					        $donationProductData['product_sku'] = $productResultArr[0]['product_sku'];
        					$donationProductData['product_name'] = $productResultArr[0]['product_name'];
							
							$donationProductArray = $transaction->getDonationProductArray($donationProductData);
                            $donationProductId = $this->_transactionTable->saveTransactionDonationProdcts($donationProductArray);


                            // Insert into payment received table    
                            $paymentTransactionData = array();
                            $paymentTransactionData['amount'] = $data['amount'][$key];
                            $paymentTransactionData['transaction_id'] = $transactionId;
                            $paymentTransactionData['authorize_transaction_id'] = '';
                            $paymentTransactionData['transaction_payment_id'] = $transactionPaymentId;
                            $paymentTransactionData['added_by'] = isset($this->auth->getIdentity()->crm_user_id) ? $this->auth->getIdentity()->crm_user_id : '';
                            $paymentTransactionData['added_date'] = DATE_TIME_FORMAT;
                            $paymentTransactionData['modify_by'] = isset($this->auth->getIdentity()->crm_user_id) ? $this->auth->getIdentity()->crm_user_id : '';
                            $paymentTransactionData['modify_date'] = DATE_TIME_FORMAT;
                            $paymentTransactionData['product_type_id'] = '2'; // for donation
                            $paymentTransactionData['table_auto_id'] = $donationProductId;
                            $paymentTransactionData['table_type'] = 2;
                            $paymentTransactionData['batch_id'] = $batchData['payment_batch_id'];
                            $paymentTransactionData['credit_card_type_id'] = '';
                            $paymentTransactionData['isPaymentReceived'] = '1';
                            $paymentTransactionData['payment_source'] = '';
                            $this->insertIntoPaymentReceive($paymentTransactionData);
                            // End Insert into payment received table
                        }
                    }
                    if ($request->getPost('submitandlock')) {
                        $lockBatchData = array();
                        $lockBatchData['payment_batch_id'] = $data['payment_batch_id'][0];
                        $lockBatchData['is_locked'] = '1';
                        $lockBatchData['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                        $lockBatchData['modified_date'] = DATE_TIME_FORMAT;
                        $this->getServiceLocator()->get('Transaction\Model\BatchTable')->updateBatchId($lockBatchData);
                    }
                    $messages = array('status' => "success", 'message' => 'Bulk donation entries done successfully!”');
                    if ($this->_flashMessage->hasMessages()) {
                        $messages = $this->_flashMessage->getMessages();
                    }
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                }
            } else {
				$generalCampaign = $this->_transactionTable->getGeneralCampaign();

                $postData = $request->getPost()->toArray();
                $fileExt = $this->GetFileExt($_FILES['file_name']['name']);
                if ($fileExt[1] != 'xls') {
                    $messages = array('status' => "error", 'message' => 'InValid File');
                } else {
                    $import_handler = new SpreadsheetExcelReader($_FILES['file_name']['tmp_name']);
                    $imported_data = $import_handler->dumpToArray();
                    foreach ($imported_data as $key1 => $value1) {
                        if ($key1 != 0) {
                            $transaction_amount = $value1[7];
                            $donationBatchAmount+= $transaction_amount;
                        }
                    }

                    $batchData['payment_batch_id'] = $postData['payment_batch_id'][1];
                    $batchData['amount'] = $donationBatchAmount;
                    $batchData['batch_date'] = DATE_TIME_FORMAT;
                    $batchData['added_by'] = $crm_user_id;
                    $batchData['added_date'] = DATE_TIME_FORMAT;

                    $batchGroupId = $this->_transactionTable->saveDonationBatches($batchData);

                    foreach ($imported_data as $key => $value) {
                        if ($key != 0) {
                            $user_email = $value[0];
                            $first_name = $value[1];
                            $last_name = $value[2];
                            $campaign_code = $value[3];
                            $payment_mode = $value[4];
                            foreach ($getPaymentMode as $index => $val) {
                                if ($val['payment_mode'] == $payment_mode)
                                    $payment_mode_id = $val['payment_mode_id'];
                            }
                            $check_no = $value[5];
                            $check_date = ($value[6] != '') ? $this->DateFormat($value[6], 'db_date_format') : '';
                            $transaction_amount = $value[7];

							if($campaign_code!=''){
								$campaigndata = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCampaign(array('', $campaign_code));

								$campaign_id = $campaigndata[0]['campaign_id'];
							}else{

								$campaign_code = $generalCampaign[0]['campaign_code'];
								$campaign_id = $generalCampaign[0]['campaign_id'];
							}

							

                            if ($user_email != '') {
                                if (is_numeric($user_email)) {
                                    $get_contact_arr = $this->getServiceLocator()->get('User\Model\ContactTable')->getContact(array('contact_id' => $user_email));
                                } else {
                                    $get_contact_arr = $this->getServiceLocator()->get('User\Model\ContactTable')->getContact(array('user_email' => $user_email));
                                }
                                if (!empty($get_contact_arr) && $get_contact_arr[0] != '') {
                                    $userId = $get_contact_arr[0]['user_id'];
                                } else {
                                    if ($first_name != '' && $last_name != '') {
                                        $contact = new Contact($this->_adapter);
                                        $data['contact_type'] = 1;
                                        $data['first_name'] = $first_name;
                                        $data['last_name'] = $last_name;
                                        $data['email_id'] = $user_email;
                                        $added_date = $data['add_date'] = DATE_TIME_FORMAT;
                                        $modified_date = $data['modified_date'] = DATE_TIME_FORMAT;

                                        $filterParam = $contact->filterParam($data);
                                        $filterParam['username'] = null;

                                        $userId = $this->getServiceLocator()->get('User\Model\ContactTable')->insertContact($filterParam);

                                        $this->getServiceLocator()->get('User\Model\ContactTable')->updateContactId(array('user_id' => $userId));

                                        /* user membership */
                                        $membershipDetailArr = array();
                                        $membershipDetailArr[] = 1;
                                        $getMemberShip = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);

                                        $userMembershipData = array();
                                        $userMembershipData['user_id'] = $userId;
                                        $userMembershipData['membership_id'] = $getMemberShip['membership_id'];
										$userMembershipInfo = $this->getServiceLocator()->get('UserMembership')->getUserMembership($getMemberShip);
							$userMembershipData['membership_date_from'] = $userMembershipInfo['membership_date_from'];
							$userMembershipData['membership_date_to'] = $userMembershipInfo['membership_date_to'];
                                        /*$startDate = date("Y-m-d");
                                        if ($getMemberShip['validity_type'] == 1) {
                                            $userMembershipData['membership_date_from'] = $startDate;
                                            $startDay = $this->_config['financial_year']['srart_day'];
                                            $startMonth = $this->_config['financial_year']['srart_month'];
                                            $startYear = date("Y");
                                            $startDate = $startYear . "-" . $startMonth . "-" . $startDay;
                                            $addYear = $getMemberShip['validity_time'] - date("Y");
                                            $futureDate = date('Y-m-d', strtotime('+' . $addYear . ' year', strtotime($startDate)));
                                            $userMembershipData['membership_date_to'] = $futureDate;
                                        } else if ($getMemberShip['validity_type'] == 2) {
                                            $userMembershipData['membership_date_from'] = $startDate;
                                            $futureDate = date('Y-m-d', strtotime('+' . $getMemberShip['validity_time'] . ' year', strtotime($startDate)));
                                            $userMembershipData['membership_date_to'] = $futureDate;
                                        }*/
                                        $this->getServiceLocator()->get('User\Model\UserTable')->saveUserMembership($userMembershipData);
                                        /* end of membership */

                                        /* email */
                                        $userDataArr = $this->getServiceLocator()->get('User\Model\UserTable')->getUser(array('user_id' => $userId));
                                        $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                                        $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                                        $userDataArr['genrated_password'] = $filterParam['genrated_password'];
                                        $userDataArr['verification_code'] = $filterParam['verify_code'];
                                        $userDataArr['first_name'] = $first_name;
                                        $userDataArr['email_id'] = $user_email;
                                        $signup_email_id_template_int = 4;
                                        $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $signup_email_id_template_int, $this->_config['email_options']);
                                        /* end of email */
                                    }
                                }
                            }

                            $productQty = 1;
                            $productPrice = $transaction_amount;
                            $productSubtotal = $transaction_amount;
                            $membershipDiscount = 0;
                            $productDiscount = 0;
                            $productTaxableAmount = 0;
                            $productTotal = $transaction_amount;
                            $totalSubtotal = $transaction_amount;
                            $totalDiscount = 0;
                            $totalAmountProduct = $transaction_amount;
                            $taxableAmount = 0;

                            /** Transaction * */
                            $finalTransaction['user_id'] = $userId;
                            $finalTransaction['transaction_source_id'] = $this->_config['transaction_source']['transaction_source_crm_id'];
                            $finalTransaction['num_items'] = 1;
                            $finalTransaction['sub_total_amount'] = $totalSubtotal;
                            $finalTransaction['shipping_amount'] = $shippingAmount;
                            $finalTransaction['total_tax'] = $totalTaxableAmount;
                            $finalTransaction['transaction_amount'] = $totalAmountProduct;
                            $finalTransaction['transaction_status_id'] = 10;
                            $finalTransaction['transaction_type'] = 1;
                            $finalTransaction['transaction_date'] = DATE_TIME_FORMAT;
                            $finalTransaction['added_by'] = $crm_user_id;
                            $finalTransaction['added_date'] = DATE_TIME_FORMAT;
                            $finalTransaction['modify_by'] = $crm_user_id;
                            $finalTransaction['modify_date'] = DATE_TIME_FORMAT;			
                            
                        if(isset($this->auth->getIdentity()->crm_user_id) && !empty($this->auth->getIdentity()->crm_user_id))
                            $transactionChannelId = 2; // Direct mail
                        else
                            $transactionChannelId = 5; // Web                            
                            
							$finalTransaction['don_amount'] = $totalAmountProduct;
							$finalTransaction['campaign_code'] = $campaign_code;
							$finalTransaction['campaign_id'] = $campaign_id;
							$finalTransaction['channel_id'] = $transactionChannelId;
							$finalTransaction['total_product_revenue'] = $totalAmountProduct;


                            $transaction = new Transaction($this->_adapter);

                            $finalTransactionArr = $transaction->getFinalTransactionArr($finalTransaction);
                            $transactionId = $this->_transactionTable->saveFinalTransaction($finalTransactionArr);
                            /* end add transaction record */

                            $userArr = array('user_id' => $userId);
                            $contactDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getContact($userArr);

                            $user_address_info_primary = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations(array('user_id' => $userId, 'address_location' => '1'));

                            if (empty($user_address_info_primary)) {
                                $user_address_info_primary = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations(array('user_id' => $userId, 'address_location' => '2'));
                            }

                            if (!empty($user_address_info_primary)) {

                                $user_phone_info_primary = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserPhoneInformations(array('user_id' => $userId, 'is_primary' => '1'));

                                if (empty($user_phone_info_primary)) {
                                    $user_phone_info_primary = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserPhoneInformations(array('user_id' => $userId));
                                }

                                $billingShipping = array();

                                $billingShipping['transaction_id'] = $transactionId;
                                $billingShipping['billing_title'] = $contactDetail[0]['title'];
                                $billingShipping['billing_first_name'] = $contactDetail[0]['first_name'];
                                $billingShipping['billing_last_name'] = $contactDetail[0]['last_name'];
                                $billingShipping['billing_company'] = $contactDetail[0]['company_name'];


                                $address = $user_address_info_primary[0]['street_address_one'];
                                if ($user_address_info_primary[0]['street_address_two'] != '')
                                    $address.=", " . $user_address_info_primary[0]['street_address_two'];
                                if ($user_address_info_primary[0]['street_address_three'] != '')
                                    $address.=", " . $user_address_info_primary[0]['street_address_three'];

                                $billingShipping['billing_address'] = $address;

                                $billingShipping['billing_city'] = $user_address_info_primary[0]['city'];
                                $billingShipping['billing_state'] = $user_address_info_primary[0]['state'];
                                $billingShipping['billing_zip_code'] = $user_address_info_primary[0]['zip_code'];
                                $billingShipping['billing_country'] = $user_address_info_primary[0]['country_id'];

                                if (!empty($user_phone_info_primary)) {
                                    $billingShipping['billing_phone'] = $user_phone_info_primary[0]['country_code'] . '-' . $user_phone_info_primary[0]['area_code'] . '-' . $user_phone_info_primary[0]['phone_number'];
                                }

                                $billingShipping['added_by'] = $crm_user_id;
                                $billingShipping['modify_by'] = $crm_user_id;


                                $transactionBillingShippingArr = $transaction->getTransactionBillingShipping($billingShipping);

                                $transactionBillingShippingId = $this->_transactionTable->saveTransactionBillingShippingDetail($transactionBillingShippingArr);
                            }



                            /* update membership */
                            if ($totalAmountProduct > 0) {
                                $userPurchasedProductArr = array();
                                $userPurchasedProductArr['user_id'] = $userId;
                                $date_range = $this->getDateRange(4);
                                $userPurchasedProductArr['from_date'] = $this->DateFormat($date_range['from'], 'db_date_format_from');
                                $userPurchasedProductArr['to_date'] = $this->DateFormat($date_range['to'], 'db_date_format_to');
                                $getTotalUserPurchaseProductAmountArr = $transaction->getTotalUserPurchaseProductAmountArr($userPurchasedProductArr);
                                $totalAmountPurchased = $this->_transactionTable->getTotalUserPurchased($getTotalUserPurchaseProductAmountArr);
                                $userMembershipArr = array();
                                $userMembershipArr['transaction_amount'] = $totalAmountPurchased;
                                $userMembershipArr['user_id'] = $userId;
                                $userMembershipArr['transaction_id'] = $transactionId;
                                $userMembershipArr['transaction_date'] = DATE_TIME_FORMAT;
                                $this->updateUserMembership($userMembershipArr);
                            }
                            /* End of Update membership */

                            /* Add payment information  of transaction */
                            $amount_total = $totalAmountProduct;
                            $paymentTransactionData = array();
                            $paymentTransactionData['amount'] = $transaction_amount;
                            $paymentTransactionData['transaction_id'] = $transactionId;
                            $paymentTransactionData['payment_mode_id'] = $payment_mode_id;
                            if ($payment_mode_id == 3) {
                                $paymentTransactionData['cheque_number'] = $check_no;
                                $paymentTransactionData['cheque_date'] = $check_date;
                            }
                            $paymentTransactionData['added_by'] = $crm_user_id;
                            $paymentTransactionData['added_date'] = DATE_TIME_FORMAT;
                            $paymentTransactionData['modify_by'] = $crm_user_id;
                            $paymentTransactionData['modify_date'] = DATE_TIME_FORMAT;
                            $transactionCheckPaymentArr = $transaction->getTransactionPaymentArr($paymentTransactionData);
                            $transactionPaymentId = $this->_transactionTable->saveTransactionPaymentDetail($transactionCheckPaymentArr);
                            /* End of Payment Information */

                            $companydata = array();
                            $companydata['company_name'] = $value[8];

                            $companies = $this->getServiceLocator()->get('Common\Model\CommonTable')->getMasterCompany($companydata);
                            if (!empty($companies)) {
                                $company = $companies[0];
                                $donationProductData['company_id'] = $company['company_id'];
                            } else {
                                $common = new Common($this->_adapter);
                                $company_array = $common->getCompanyArr($companydata);
                                $company_id = $this->getServiceLocator()->get('Common\Model\CommonTable')->createMasterCompany($company_array);
                                $donationProductData['company_id'] = $company_id;
                            }

                            /* Add Donation Product */                            
                            $donationProductData['campaign_id'] = $campaign_id;
                            $donationProductData['batch_group_id'] = $batchGroupId;
                            $donationProductData['transaction_id'] = $transactionId;
                            $donationProductData['product_mapping_id'] = 1;
                            $donationProductData['product_type_id'] = 2;
							$donationProductData['product_id'] = 8;
							//$donationProductData['product_name'] = 'Donation';
                            $donationProductData['product_price'] = $transaction_amount;
                            $donationProductData['product_sub_total'] = $transaction_amount;
                            $donationProductData['product_discount'] = 0;
                            $donationProductData['product_tax'] = 0;
                            $donationProductData['product_total'] = $transaction_amount;
                            $donationProductData['product_status_id'] = 10;
                            $donationProductData['purchase_date'] = DATE_TIME_FORMAT;
                            $donationProductData['is_deleted'] = 0;
                            $donationProductData['added_by'] = $crm_user_id;
                            $donationProductData['added_date'] = DATE_TIME_FORMAT;
                            $donationProductData['modify_by'] = $crm_user_id;
                            $donationProductData['modified_date'] = DATE_TIME_FORMAT;
                            $donationProductData['company_name'] = $value[8];
							$donationProductData['user_id'] = $userId;
							$donationProductData['num_quantity'] = 1;
							
			                $productSearchParam['id'] = 8;/** product */
			                $productResultArr = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProductInfo($productSearchParam);   
			                $donationProductData['product_sku'] = $productResultArr[0]['product_sku'];
			                $donationProductData['product_name'] = $productResultArr[0]['product_name'];							
							
                            $donationProductArray = $transaction->getDonationProductArray($donationProductData);
                            $donationProductId = $this->_transactionTable->saveTransactionDonationProdcts($donationProductArray);


				            $this->_transactionTable->updateRevenueGoal($finalTransactionArr);
				            $this->_transactionTable->updateChannelRevenue($finalTransactionArr);								

							/** campaign revenue update **/
							//$campArrayBack['campaign_id'] = $campaign_id;
							//$campArrayBack['revenue_recieved'] = $transaction_amount;
							//$this->getServiceLocator()->get('Campaign\Model\CampaignTable')->updateRevenueGoal($campArrayBack);
							/***         **/

                            // Insert into payment received table    
                            $paymentTransactionData = array();
                            $paymentTransactionData['amount'] = $transaction_amount;
                            $paymentTransactionData['transaction_id'] = $transactionId;
                            $paymentTransactionData['authorize_transaction_id'] = '';
                            $paymentTransactionData['transaction_payment_id'] = $transactionPaymentId;
                            $paymentTransactionData['added_by'] = isset($this->auth->getIdentity()->crm_user_id) ? $this->auth->getIdentity()->crm_user_id : '';
                            $paymentTransactionData['added_date'] = DATE_TIME_FORMAT;
                            $paymentTransactionData['modify_by'] = isset($this->auth->getIdentity()->crm_user_id) ? $this->auth->getIdentity()->crm_user_id : '';
                            $paymentTransactionData['modify_date'] = DATE_TIME_FORMAT;
                            $paymentTransactionData['product_type_id'] = '2'; // for donation
                            $paymentTransactionData['table_auto_id'] = $donationProductId;
                            $paymentTransactionData['table_type'] = 2;
                            $paymentTransactionData['batch_id'] = $postData['payment_batch_id'][1];
                            $paymentTransactionData['credit_card_type_id'] = '';
                            $paymentTransactionData['isPaymentReceived'] = '1';
                            $paymentTransactionData['payment_source'] = '';
                            $this->insertIntoPaymentReceive($paymentTransactionData);
                            // End Insert into payment received table
                        }
                    }
                    $messages = array('status' => "success", 'message' => 'Bulk donation entries uploaded successfully!”');
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                }
            }
        }
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'form' => $form,
            'jsLangTranslate' => $message
        ));
        return $viewModel;
    }

    /**
     * This function is used to get batch and their donations details
     * @author Icreon Tech - SK
     */
    public function getBatchDetailAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getConfig();
        $this->getTransactionTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        $searchParam = array();

        $payment_batch_id = $this->decrypt($params['batch_id']);
        $searchParam['payment_batch_id'] = $payment_batch_id;
        $batchData = array();
        $donationData = array();
        if ($request->isPost()) {

            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit;
            $isExport = $request->getPost('export');

            if ($isExport != "" && $isExport == "excel") {
                $chunksize = $this->_config['export']['chunk_size'];
                $export_limit = $this->_config['export']['export_limit'];

                $searchParam['recordLimit'] = $chunksize;
            }

            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');

            $page_counter = 1;
            do { 
				
                $batchDetails = $this->_transactionTable->getDonationBatchDetail($searchParam);
				if (!empty($batchDetails)) {
                    $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
                    $total = @$countResult[0]->RecordCount;
                    $jsonResult = array();
                    $arrCell = array();
                    $subArrCell = array();

                    if ($isExport == "excel") {
                        if ($total > $chunksize) {
                            @$number_of_pages = ceil($total / $chunksize);
                            $page_counter++;
                            $start = $chunksize * $page_counter - $chunksize;
                            $searchParam['start_index'] = $start;
                            $searchParam['page'] = $page_counter;
                        } else {
                            $number_of_pages = 1;
                            $page_counter++;
                        }
                    } else {
                        $page_counter = $page;
                        $number_of_pages = ceil($total / $limit);
                        $jsonResult['records'] = $countResult[0]->RecordCount;
                        $jsonResult['page'] = $request->getPost('page');
                        $jsonResult['total'] = $number_of_pages;
                    }
					$totalAmount = 0;
                    foreach ($batchDetails as $key => $value) { 
						if($value['rma_action_id']!='5'){
							$arrCell['id'] = $value['batch_id'];
							if ($isExport == "excel") {
								$totalAmount+= $value['transaction_amount'];
								$arrExport[] = array("Batch Id" => $value['batch_id'], "Transaction Id"=> $value['transaction_id'],  "Contact Id" => "'".$value['contact_id'], "Contact" => $value['Contact'], "Campaign Code" => $value['campaign_code'], "Payment Mode" => $value['payment_mode'], "Check #" => "'".$value['cheque_number'], "Amount($)" =>  $this->Currencyformat($value['transaction_amount']), "Company Name" => $value['company_name'], "Added By" => $value['added_by'], "Batch Date" => str_replace('-', '/', $this->OutputDateFormat($value['batch_date'], 'dateformatampm')));
							} else {
								$arrCell['cell'] = array($value['batch_id'],$value['transaction_id'], $value['contact_id'], $value['Contact'], $value['campaign_code'], $value['payment_mode'], $value['cheque_number'], '$ ' . $this->Currencyformat($value['transaction_amount']), $value['company_name'], $value['added_by'], str_replace('-', '/', $this->OutputDateFormat($value['batch_date'], 'dateformatampm')));
							}

							$subArrCell[] = $arrCell;
							$jsonResult['rows'] = $subArrCell;
						}
                    }
                    if ($isExport == "excel") {

						$arrExport[] = array("Batch Id" => "", "Contact Id" => "", "Contact" => "", "Campaign Code" => "", "Payment Mode" => "", "Check #" => "", "Amount" => "", "Company Name" => "");

						$arrExport[] = array("Batch Id" => "", "Contact Id" => "", "Contact" => "", "Campaign Code" => "", "Payment Mode" => "", "Check #" => "Total Amount", "Amount" => '$ ' . $this->Currencyformat($totalAmount), "Company Name" => "");

                        //$filename = "contact_transaction_export_" . date("Y-m-d") . ".csv";
                        $filename = "bulk_donation_details_export_" . date("Y-m-d") . ".csv";
                        $this->arrayToCsv($arrExport, $filename, $page_counter);
                    }
                } else {
                    $jsonResult['rows'] = array();
                }
            } while ($page_counter <= $number_of_pages && $isExport == "excel");

            if ($isExport == "excel") {
                $this->downloadSendHeaders("bulk_donation_details_export_" . date("Y-m-d") . ".csv");
                die;
            } else {
                return $response->setContent(\Zend\Json\Json::encode($jsonResult));
            }
        } else { 

			$transaction = new Transaction($this->_adapter);
			$bulkDonationParams = $transaction->getBulkDonationEntriesArray(array('payment_batch_id' => $payment_batch_id));
            $batchDetails = $this->_transactionTable->getBulkEntries($bulkDonationParams);
            
            if (!empty($batchDetails)) {
                foreach ($batchDetails as $key => $value) {
                    $batchData['encBatchId'] = $params['batch_id'];
                    $batchData['batch_id'] = $value['batch_no'];
                    $batchData['amount'] = '$ ' . $this->Currencyformat($value['amount']);
                }
            }

            $viewModel = new ViewModel();

            $messages = array();
            $viewModel->setVariables(array(
                'batchData' => $batchData
                    )
            );
            return $viewModel;
        }
    }

    /**
     * This function is used to add more donations for manual bulk entires
     * @author Icreon Tech - SK
     */
    public function addMoreDonationsAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $this->checkUserAuthenticationAjx();
            $totalDonations = $request->getPost('totalDonations');
            $add_more_donations = new AddMoreDonationsForm();
            /* payment mode */
            $getPaymentMode = $this->getServiceLocator()->get('Common\Model\CommonTable')->getPaymentMode();
            $paymentMode = array();
            $paymentMode[''] = 'Select One';
            if ($getPaymentMode !== false) {
                foreach ($getPaymentMode as $key => $val) {
					if ($val['payment_mode_id'] != "1")
						$paymentMode[$val['payment_mode_id']] = $val['payment_mode'];
                }
            }
            $add_more_donations->get('payment_mode_id[]')->setAttribute('options', $paymentMode);
            $add_more_donations->get('payment_mode_id[]')->setValue(3);
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'add_more_donations' => $add_more_donations,
                'totalDonations' => $totalDonations
            ));
            return $viewModel;
        }
    }

    public function updateUserMembership($userMembershipArr) {
        $transaction = new Transaction($this->_adapter);
        $userMembershipUpdateArr = array();
        $userMembershipArr['transaction_amount'] = $userMembershipArr['transaction_amount'];
        $userMembershipUpdateArr['minimun_donation_amount'] = isset($userMembershipArr['transaction_amount']) ? $userMembershipArr['transaction_amount'] : '';
        $getMatchingMembershipArr = $transaction->getMatchingMembershipArr($userMembershipUpdateArr);
        $matchingMembership = $this->_transactionTable->getMatchingMembership($getMatchingMembershipArr);
        $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $userMembershipArr['user_id']));
        //if (!empty($matchingMembership) && $matchingMembership['membership_id'] != $userDetail['membership_id']) {
       $gracePeriodStatus = false;
        $membershipExpiredStatus = false;
        $todayTime= time();
        $memberExpired = date("Y-m-d",strtotime($userDetail['membership_expire']));
        $leftTime = strtotime($memberExpired) - $todayTime;
       if($leftTime > 0){
			if(floor($leftTime / (60 * 60 * 24)) < $this->_config['membership_plus'])
				$gracePeriodStatus = true;//allow update 
		}

        if($gracePeriodStatus){
        $userMembershipUpdateArr = array();
        $userMembershipUpdateArr['minimun_donation_amount'] = isset($userMembershipArr['donation_amount']) ? $userMembershipArr['donation_amount'] : '';
        $getMatchingMembershipArr = $transaction->getMatchingMembershipArr($userMembershipUpdateArr);
        $matchingMembership = $this->_transactionTable->getMatchingMembership($getMatchingMembershipArr);
        
        }
        if ( $leftTime < 1 || ( $gracePeriodStatus == true || (!empty($matchingMembership) && $matchingMembership['membership_id'] != $userDetail['membership_id'] && ($userMembershipUpdateArr['minimun_donation_amount'] >= $userDetail['donation_amount'])) ) ) {
            $startDate = date("Y-m-d",strtotime(DATE_TIME_FORMAT));
            $membershipDateFrom = '';
            $membershipDateTo = '';

            if ($matchingMembership['validity_type'] == 1) {
                $membershipDateFrom = $startDate;
                $startDay = $this->_config['financial_year']['srart_day'];
                $startMonth = $this->_config['financial_year']['srart_month'];
                $startYear = date("Y",strtotime(DATE_TIME_FORMAT));
                $startDate = $startYear . "-" . $startMonth . "-" . $startDay;
//$year = 1;

                $addYear = ($matchingMembership['validity_time'] - $startYear) + 1;
                $futureDate = date('Y-m-d', strtotime('+' . $addYear . ' year -1 day', strtotime($startDate)));
                $your_date = $futureDate;
                $now = time();
                $datediff = strtotime($your_date) - $now;
                if ((isset($this->_config['membership_plus']) && !empty($this->_config['membership_plus'])) && (floor($datediff / (60 * 60 * 24)) < $this->_config['membership_plus'])) {
                    $addYear = ($matchingMembership['validity_time'] - $startYear) + 2;
                    $futureDate = date('Y-m-d', strtotime('+' . $addYear . ' year', strtotime($startDate)));
                    $futureDate = date('Y-m-d', strtotime('-1 day', strtotime($futureDate)));
                }                
                $membershipDateTo = $futureDate;
            } else if ($matchingMembership['validity_type'] == 2) {
                $membershipDateFrom = $startDate;
                $futureDate = date('Y-m-d', strtotime('+' . $matchingMembership['validity_time'] . ' year -1 day', strtotime($startDate)));
                $membershipDateTo = $futureDate;
            }
            $userMembershipUpdateArr = array();
            $userMembershipUpdateArr['user_id'] = $userMembershipArr['user_id'];
            $userMembershipUpdateArr['membership_id'] = $matchingMembership['membership_id'];
            $userMembershipUpdateArr['transaction_id'] = isset($userMembershipArr['transaction_id']) ? $userMembershipArr['transaction_id'] : '';
            $userMembershipUpdateArr['transaction_date'] = isset($userMembershipArr['transaction_date']) ? $userMembershipArr['transaction_date'] : '';
            $userMembershipUpdateArr['transaction_amount'] = isset($userMembershipArr['transaction_amount']) ? $userMembershipArr['transaction_amount'] : '';
            $userMembershipUpdateArr['membership_date_from'] = $membershipDateFrom;
            $userMembershipUpdateArr['membership_date_to'] = $membershipDateTo;
			if(empty($membershipDateFrom) || empty($membershipDateTo)){
				return false;
			}
            $getUpdateUserMembershipArr = $transaction->getUpdateUserMembershipArr($userMembershipUpdateArr);
            $userMembershipId = $this->_transactionTable->updateUserMembership($getUpdateUserMembershipArr);
        }
    }

    public function getAvailableBatchAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTransactionTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $postData = $request->getPost()->toArray();
            $postData['check_available'] = '1';
			if($request->getPost('is_locked')){
				$postData['is_locked'] = $request->getPost('is_locked');
			}
            $batchList = array();
            $searchResult = $this->getServiceLocator()->get('Transaction\Model\BatchTable')->getBatchListing($postData);
            if (!empty($searchResult)) {
                $i = 0;
                foreach ($searchResult as $key => $value) {
                    $batchList[$i] = array();
                    $batchList[$i]['payment_batch_id'] = $value['payment_batch_id'];
                    $batchList[$i]['batch_id'] = $value['batch_id'];
                    $i++;
                }
            }
            return $response->setContent(\Zend\Json\Json::encode($batchList));
        }
    }

}

