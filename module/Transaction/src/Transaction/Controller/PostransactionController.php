<?php

/**
 * This controller is used to manage Transactions
 * @package    Transaction
 * @author     Icreon Tech - DG
 */

namespace Transaction\Controller;

use Base\Controller\BaseController;
use Pledge\Controller\PledgeController;
use Zend\View\Model\ViewModel;
use Transaction\Model\Transaction;
use Transaction\Model\Postransaction;
use Common\Model\Common;
use Zend\Http\Request;
use Zend\Authentication\AuthenticationService;
use Transaction\Form\SearchUserTransactionForm;
use Transaction\Form\SearchUserForm;
use Transaction\Form\SearchTransactionForm;
use Transaction\Form\SaveSearchForm;
use Transaction\Form\CreateTrasactionForm;
use Transaction\Form\CheckoutFormFront;
use Transaction\Form\CheckoutFormFrontWoh;
use Transaction\Form\PaymentModeCheck;
use Transaction\Form\PaymentModeCredit;
use Transaction\Form\AddToCartForm;
use Transaction\Form\DonationAddToCartForm;
use Transaction\Form\DonationNotifyForm;
use Transaction\Form\KioskFeeForm;
use Transaction\Form\FlagOfFacesForm;
use Transaction\Form\InventoryForm;
use Transaction\Form\ViewTransactionForm;
use Transaction\Form\TransactionNotesForm;
use Transaction\Form\MembershipDetailForm;
use Transaction\Form\WallOfHonor;
use Transaction\Form\ViewFofImageForm;
use Transaction\Form\ViewWohDetailsForm;
use Transaction\Form\WallOfHonorAdditionalContact;
use Product\Controller\ProductController;
use Transaction\Form\CrmSearchPassengerForm;
use Passenger\Controller\PassengerController;
use Transaction\Form\BioCertificate;
use Transaction\Form\PassengerDocumentForm;
use Transaction\Form\TransactionStatusForm;
use Transaction\Form\ProductStatusForm;
use Transaction\Form\UpdateTrackCodeForm;
use Transaction\Form\UpdateTransactionBatchForm;
use Zend\Barcode\Barcode;
use Authorize\lib\AuthnetXML;
use Pledge\Model\Pledge;
use SimpleXMLElement;
use Base\Model\html2fpdf;
use Zend\View\Renderer\PhpRenderer;
use Zend\Session\Container;
use Base\Model\ZipStream;
use Product\Model\Category;
use Zend\Db\Adapter\Driver\ConnectionInterface;
use Zend\Db\Adapter\Adapter;
use Common\Controller\DoController;
use Group\Model\Group;
use Cases\Model\Cases;
use User\Model\Woh;
use Kiosk\Model\PosManagement;
/**
 * This controller is used to manage Transactions
 * @category   Zend
 * @package    Transaction
 * @author     Icreon Tech - DG
 */
class PostransactionController extends BaseController {

    protected $_transactionTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_flashMessage = null;
    public $auth = null;

    function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        parent::__construct();
        $auth = new AuthenticationService();
        $this->auth = $auth;
    }

    /**
     * This function is used to get table object
     * @return     array
     * @param array $form
     * @author Icreon Tech - DG
     */
    public function getTransactionTable() {
        if (!$this->_transactionTable) {
            $sm = $this->getServiceLocator();
            $this->_transactionTable = $sm->get('Transaction\Model\PostransactionTable');
            $this->_posTable = $sm->get('Kiosk\Model\PosTable');
            $this->_adapter = $sm->get('dbAdapter');
            $this->_config = $sm->get('Config');
        }
        return $this->_transactionTable;
    }

    /**
     * This function is used to get config object
     * @return     array
     * @param array $form
     * @author Icreon Tech - DG
     */
    public function getConfig() { //error_reporting(E_ALL);   
        $sm = $this->getServiceLocator();
        $this->_translator = $sm->get('translator');
        $this->_config = $sm->get('Config');
    }

    /**
     * This action is used to create Transaction
     * @return json
     * @param void
     * @author Icreon Tech - DG
     */
    public function createTransactionAction() {
        $this->layout('crm');
    }

    public function posCheckoutCartFrontAction() { 
        $moduleSession = new Container('moduleSession');
        if ($moduleSession->moduleGroup != 3)
            $this->checkFrontUserAuthentication();
		
		$this->layout('wohlayout');
        $this->getTransactionTable();
        $msgArr = $this->_config['transaction_messages']['config']['checkout_front'];
        $transaction = new Transaction($this->_adapter);
        $inventoryForm = new InventoryForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $createTransactionForm = new CheckoutFormFrontWoh();
        $itemsTotal = '';
        $discount = '';
        $params = $this->params()->fromRoute();
        $posTransactionSession = new Container('posTransactionSession'); /* pos session */
        $transactionPhoneData = $posTransactionSession->posTransactionData['phone'];		
        $transactionAddressData = $posTransactionSession->posTransactionData['address'];		
        $transactionUserData = $posTransactionSession->posTransactionData['user_details'];		
		
        $this->campaignSession = new Container('campaign');
		
        $finalTransaction['campaign_code'] = (isset($this->campaignSession->campaign_code) && !empty($this->campaignSession->campaign_code) ? $this->campaignSession->campaign_code : $finalTransaction['campaign_code']);
        $finalTransaction['campaign_id'] = (isset($this->campaignSession->campaign_id) && !empty($this->campaignSession->campaign_id) ? $this->campaignSession->campaign_id : $finalTransaction['campaign_id']);
        $finalTransaction['channel_id'] = (isset($this->campaignSession->channel_id) && !empty($this->campaignSession->channel_id) ? $this->campaignSession->channel_id : $finalTransaction['channel_id']);
        $campResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getChannelPromo(array('campaign_id' => $finalTransaction['campaign_id'], 'channel_id' => $finalTransaction['channel_id']));
        if (isset($campResult[0]['promotion_id']) && !empty($campResult[0]['promotion_id'])) {
            $couponCode['coupon_code'] = $campResult[0]['coupon_code'];
            $couponCode['promotion_id'] = $campResult[0]['promotion_id'];
        } else {
            /** set default promotion code for ellis
             * * @ added by Saurabh 
             * */
            $couponCode['promotion_id'] = 0;
        }
        $user_id = session_id();


        if (isset($user_id) && $user_id != '') {
            $userId = $user_id;
            $paramArray['userid'] = $user_id;
            $userArr = array('user_id' => $userId);
            $shippingMetArr = array();
            $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
            $getShippingMethods = $this->_transactionTable->getShippingMethod($shippingMethodArr);
            $shippingMethods = array();
            if (!empty($getShippingMethods)) {
                foreach ($getShippingMethods as $shippingMet) {
                    $key = $shippingMet['carrier_id'] . ',' . $shippingMet['service_type'] . ',' . $shippingMet['pb_shipping_type_id'];
                    $webSelection = (!empty($shippingMet['web_selection'])) ? '  ' . $shippingMet['web_selection'] . ' ' : '';
                    $shippingMethods[$key] = $webSelection;
                }
            }
            $createTransactionForm->get('shipping_method')->setAttribute('options', $shippingMethods);
            $state = $this->getServiceLocator()->get('Common\Model\CommonTable')->getStates();
            $stateList[' '] = 'Select State';
            foreach ($state as $key => $val) {
                $stateList[$val['state_code']] = $val['state'] . " - " . $val['state_code'];
            }
            $createTransactionForm->get('shipping_state_id')->setAttribute('options', $stateList);
            $getTitle = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTitle();
            $user_title = array();
            $userTitle[' '] = 'None';
            foreach ($getTitle as $key => $val) {
                $userTitle[$val['title']] = $val['title'];
            }
            $createTransactionForm->get('shipping_title')->setAttribute('options', $userTitle);
            $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
            $countryList = array();
            foreach ($country as $key => $val) {
                $countryList[$val['country_id']] = $val['name'];
            }
            $createTransactionForm->get('shipping_country')->setAttribute('options', $countryList);
            $monthArray = $this->creditMonthArray();
            $yearArray = range(date('Y'), date('Y') + 10);
            $newYearArray = array_combine($yearArray, $yearArray);
            $createTransactionForm->get('credit_card_exp_month')->setAttribute('options', $monthArray);
            $createTransactionForm->get('credit_card_exp_year')->setAttribute('options', $newYearArray);

            $arrayPost['userid'] = $user_id;

            $finalCart = $this->cartTotalWithTax($arrayPost);
            $cartlist = $finalCart['cartData'];
            $tp = $finalCart['totalproduct'];
            $apoPoStates = $this->_config['transaction_config']['apo_po_state'];
            $apoPoStatesArray = array();
            foreach ($apoPoStates as $key => $val) {
                $apoPoStatesArray[$key] = $val;
            }
            $createTransactionForm->get('apo_po_state')->setAttribute('options', $apoPoStatesArray);

            if ($contactDetail[0]['membership_id'] != '1') {
                $discount = $contactDetail[0]['discount'];
            }
            $first_name = isset($transactionUserData['first_name'])?$transactionUserData['first_name']:$transactionUserData['shipping_first_name'];
	     $last_name = isset($transactionUserData['last_name'])?$transactionUserData['last_name']:$transactionUserData['shipping_last_name'];
	     $createTransactionForm->get('shipping_first_name')->setAttribute('value', $first_name);
            $createTransactionForm->get('shipping_last_name')->setAttribute('value', $last_name);
            $createTransactionForm->get('shipping_address_1')->setAttribute('value', $transactionAddressData['addressinfo_street_address_1']);
            $createTransactionForm->get('shipping_address_2')->setAttribute('value', $transactionAddressData['addressinfo_street_address_2']);
            $createTransactionForm->get('shipping_city')->setAttribute('value', $transactionAddressData['addressinfo_city']);
            $createTransactionForm->get('shipping_country_code')->setAttribute('value', $transactionPhoneData['country_code']);
            $createTransactionForm->get('shipping_area_code')->setAttribute('value', $transactionPhoneData['area_code']);
            $createTransactionForm->get('shipping_phone')->setAttribute('value', $transactionPhoneData['phone_no']);
			if($transactionAddressData['addressinfo_country'] == '228'){
				$createTransactionForm->get('shipping_state_id')->setAttribute('value', $transactionAddressData['addressinfo_state']);
			}else{ 
				$createTransactionForm->get('shipping_state')->setAttribute('value', $transactionAddressData['addressinfo_state']);
			}
            $createTransactionForm->get('shipping_country')->setAttribute('value', $transactionAddressData['addressinfo_country']);
            $createTransactionForm->get('shipping_zip_code')->setAttribute('value', $transactionAddressData['addressinfo_zip']);
            $createTransactionForm->get('shipping_title')->setAttribute('value', $transactionUserData['shipping_title']);
            $createTransactionForm->get('email_id')->setAttribute('value', $transactionUserData['email_id']);
            
            
        }
        $memberShip = array();
        $memberdiscount = '';
        $isOnlyDonation = '';
        $defaultToken = '';
        if (empty($discount) && $finalCart['finaltotaldiscounted'] != 0) {
            $memberShip = $this->getTransactionTable()->getMinimumMembership();
            $memberdiscount = ($finalCart['finaltotaldiscounted'] * $memberShip[0]['discount']) / 100;
            $isOnlyDonation = $finalCart['isOnlyDonation'];
        }
        if (empty($tp)) {
            $location = SITE_URL . '/cart';
            echo '<script>window.location.href="' . $location . '";</script>';
            // header('Location: ' . SITE_URL . '/cart');
            die;
        } else {

            $viewModel = new ViewModel();
            $viewModel->setVariables(array(
                'jsLangTranslate' => array_merge($msgArr, $this->_config['transaction_messages']['config']['common_message']),
                'userId' => $userId,
                'memberdiscount' => $memberdiscount,
                'contactDetail' => $contactDetail,
                'contactAfihcDetail' => $contactAfihcDetail,
                'itemsTotal' => $finalCart,
                'createTransactionForm' => $createTransactionForm,
                'isOnlyDonation' => $isOnlyDonation,
                'membershipDiscountAmount' => $finalCart['membershipDiscountAmount'],
                'validPromotionsForUser' => $finalCart['validPromotionsForUser'],
                'cartlist' => $cartlist,
                'upload_file_path' => $this->_config['file_upload_path'],
                'shippingLocation' => $shippingLocation,
                'billingLocation' => $billingLocation,
                'defaultShippingAddress' => $defaultShippingAddress,
                'defaultBillingAddress' => $defaultBillingAddress,
                'tokenArray' => $tokenArray,
                'defaultToken' => $defaultToken,
                'couponCode' => $couponCode['coupon_code'],
                'pick_up' => $request->getPost('pick_up'),
                'moduleGroup' => $moduleSession->moduleGroup,
                'payment_status'=>$this->Decrypt($params['status'])
            ));
            return $viewModel;
        }
    }

 
 
    /**
     * This function is used to checkJsonFormatReturnString
     * @return     array
     * $get - 1 : names ("," Seperated) , 2 : Array (if json), 3 : Return Json
     * @author Icreon Tech - NS
     */
    private function checkJsonFormatReturnString($string, $get) {
        try {
            $strTxt = $string;
            $flag = json_decode($string) != null;

// if string is Json else return string
            if ($flag) {
                $strTxt = json_decode($string);
                if ($get == "1") {
                    $arrNames = array();
                    foreach ($strTxt as $valST) {
                        if (isset($valST->name) and trim($valST->name) != "") {
                            array_push($arrNames, trim($valST->name));
                        }
                    }
                    if (count($arrNames) > 0) {
                        $strTxt = implode(", ", $arrNames);
                    } else {
                        $strTxt = "";
                    }
                } else if ($get == "3") {
                    $strTxt = $string;
                }
            } else {
                $strTxt = isset($string) ? trim($string) : '';
            }

            return $strTxt;
        } catch (Exception $e) {
            return $string;
        }
    }
    /**
     * This action is used to add inventory product to cart
     * @return int
     * @param array
     * @author Icreon Tech - DT
     */
    public function addInventoryProductToCart($params = array()) {
        $searchParam['id'] = $params['productId'];
        $productResult = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProductInfo($searchParam);
        $transaction = new Transaction($this->_adapter);
        $addToCartProductId = 0;
        if (!empty($productResult)) {
            $inventoryFormArr = array();
            $inventoryFormArr['user_id'] = $params['user_id'];
            $inventoryFormArr['user_session_id'] = session_id();
            $inventoryFormArr['product_mapping_id'] = isset($params['product_mapping_id']) ? $params['product_mapping_id'] : $productResult[0]['product_mapping_id'];
            $inventoryFormArr['product_id'] = isset($params['product_id']) ? $params['product_id'] : $productResult[0]['product_id'];
            $inventoryFormArr['product_type_id'] = isset($params['product_type_id']) ? $params['product_type_id'] : $productResult[0]['product_type_id'];
            $inventoryFormArr['product_attribute_option_id'] = isset($params['product_attribute_option_id']) ? $params['product_attribute_option_id'] : NULL;
            $inventoryFormArr['item_id'] = isset($params['item_id']) ? $params['item_id'] : NULL;
            $inventoryFormArr['item_type'] = isset($params['item_type']) ? $params['item_type'] : NULL;
            $inventoryFormArr['num_quantity'] = isset($params['num_quantity']) ? $params['num_quantity'] : 1;
            $inventoryFormArr['item_price'] = isset($params['item_price']) ? $params['item_price'] : NULL;
            $inventoryFormArr['product_sku'] = $productResult[0]['product_sku'];
            $inventoryFormArr['product_price'] = isset($params['product_price']) ? $params['product_price'] : $productResult[0]['product_sell_price'];
            $inventoryFormArr['added_by'] = isset($this->auth->getIdentity()->crm_user_id) ? $this->auth->getIdentity()->crm_user_id : '0';
            $inventoryFormArr['added_date'] = DATE_TIME_FORMAT;
            $inventoryFormArr['modify_by'] = isset($this->auth->getIdentity()->crm_user_id) ? $this->auth->getIdentity()->crm_user_id : '0';
            $inventoryFormArr['modify_date'] = DATE_TIME_FORMAT;
            $inventoryFormArr['cart_date'] = DATE_TIME_FORMAT;
            $inventoryFormArr['item_name'] = isset($params['item_name']) ? $params['item_name'] : NULL;
            $inventoryFormArr['item_info'] = isset($params['item_info']) ? $params['item_info'] : NULL;
            $inventoryFormArr['manifest_info'] = isset($params['manifest_info']) ? $params['manifest_info'] : NULL;
            $inventoryFormArr['manifest_type'] = isset($params['manifest_type']) ? $params['manifest_type'] : NULL;
            $addToCartProductArr = $transaction->getAddToCartProductArr($inventoryFormArr);

            /* add campaign,promotion and user group id in cart start */
            $promotionId = "";
            $couponCode = "";
            $user_group_id = "";

            $this->campaignSession = new Container('campaign');
            $campaign_code = (isset($this->campaignSession->campaign_code) && !empty($this->campaignSession->campaign_code) ? $this->campaignSession->campaign_code : "");

            $campaign_id = isset($this->campaignSession->campaign_id) && !empty($this->campaignSession->campaign_id) ? $this->campaignSession->campaign_id : 0;
            $channel_id = (isset($this->campaignSession->channel_id) && !empty($this->campaignSession->channel_id) ? $this->campaignSession->channel_id : 0);

            if ($campaign_id != 0 && $channel_id != 0) {
                $campResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getChannelPromo(array('campaign_id' => $campaign_id, 'channel_id' => $channel_id));

                if (isset($campResult[0]['promotion_id']) && !empty($campResult[0]['promotion_id'])) {
                    $couponCode = $campResult[0]['coupon_code'];
                    $promotionId = $campResult[0]['promotion_id'];
                } else {
                    $couponCode = "";
                    $promotionId = 0;
                }

                $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '';
                if (isset($user_id) && $user_id != '') {
                    $searchParam['user_id'] = $user_id;
                    $searchParam['campaign_id'] = $campaign_id;
                    $cmTable = $this->getServiceLocator()->get('Campaign\Model\CampaignTable');
                    $campaign_arr = $cmTable->getUserCampaigns($searchParam);
                    if ($campaign_arr && isset($campaign_arr[0]['group_id'])) {
                        $user_group_id = $campaign_arr[0]['group_id'];
                    }
                    
                }
            }

            $addToCartProductArr[] = $campaign_id;
            $addToCartProductArr[] = $campaign_code;
            $addToCartProductArr[] = $promotionId;
            $addToCartProductArr[] = $couponCode;
            $addToCartProductArr[] = $user_group_id;
			$addToCartProductArr[] = '3';


            
            /* add campaign,promotion and user group id in cart ends */

            $addToCartProductId = $this->_transactionTable->addToCartProduct($addToCartProductArr);
        }
        return $addToCartProductId;
    }

    /**
     * This action is used to add additional passenger manifest
     * @return int
     * @param array
     * @author Icreon Tech - DT
     */
    public function addToCartPassengerManifest($params = array()) {
        $transaction = new Transaction($this->_adapter);
        $addToCartManifestId = 0;
        $manifestFormArr = array();
        $manifestFormArr = $params;
        $manifestFormArr['added_by'] = isset($this->auth->getIdentity()->crm_user_id) ? $this->auth->getIdentity()->crm_user_id : '0';
        $manifestFormArr['added_date'] = DATE_TIME_FORMAT;
        $manifestFormArr['modified_by'] = isset($this->auth->getIdentity()->crm_user_id) ? $this->auth->getIdentity()->crm_user_id : '0';
        $manifestFormArr['modified_date'] = DATE_TIME_FORMAT;
        $manifestFormArr['session_id'] = session_id();
        $addToCartManifestArr = $transaction->getAddToCartManifestArr($manifestFormArr);

        /* add campaign,promotion and user group id in cart start */
        $promotionId = "";
        $couponCode = "";
        $user_group_id = "";

        $this->campaignSession = new Container('campaign');
        $campaign_code = (isset($this->campaignSession->campaign_code) && !empty($this->campaignSession->campaign_code) ? $this->campaignSession->campaign_code : "");

        $campaign_id = isset($this->campaignSession->campaign_id) && !empty($this->campaignSession->campaign_id) ? $this->campaignSession->campaign_id : 0;
        $channel_id = (isset($this->campaignSession->channel_id) && !empty($this->campaignSession->channel_id) ? $this->campaignSession->channel_id : 0);

        if ($campaign_id != 0 && $channel_id != 0) {
            $campResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getChannelPromo(array('campaign_id' => $campaign_id, 'channel_id' => $channel_id));

            if (isset($campResult[0]['promotion_id']) && !empty($campResult[0]['promotion_id'])) {
                $couponCode = $campResult[0]['coupon_code'];
                $promotionId = $campResult[0]['promotion_id'];
            } else {
                $couponCode = "";
                $promotionId = 0;
            }

            $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '';
            if (isset($user_id) && $user_id != '') {
                $searchParam['user_id'] = $user_id;
                $searchParam['campaign_id'] = $campaign_id;
                $cmTable = $this->getServiceLocator()->get('Campaign\Model\CampaignTable');
                $campaign_arr = $cmTable->getUserCampaigns($searchParam);
                if ($campaign_arr && isset($campaign_arr[0]['group_id'])) {
                    $user_group_id = $campaign_arr[0]['group_id'];
                }
                
            }
        }

        $addToCartManifestArr[] = $campaign_id;
        $addToCartManifestArr[] = $campaign_code;
        $addToCartManifestArr[] = $promotionId;
        $addToCartManifestArr[] = $couponCode;
        $addToCartManifestArr[] = $user_group_id;
		$addToCartManifestArr[] = '3';

        
        /* add campaign,promotion and user group id in cart ends */

        $addToCartManifestId = $this->_transactionTable->addToCartManifest($addToCartManifestArr);
        return $addToCartManifestId;
    }


    /**
     * This function is used to encrypt text
     * @return     encrypted text
     * @author Icreon Tech - NS
     */
    public function donateEncryptTextAction() {
        try {
            $this->getConfig();
            $request = $this->getRequest();
            $decryptText = $request->getPost('decryptText');
            if (isset($decryptText) and trim($decryptText) != "") {
                $encryptText = $this->encrypt('/add-to-cart-donation-front-cms/' . trim($decryptText));
            } else {
                $encryptText = '';
            }
            echo $encryptText;
        } catch (Exception $e) {
            echo '';
        }
        exit();
    }

    /**
     * This action is used to apply coupon discount in cart array
     * @param int,array
     * @return     array
     * @author Icreon Tech - DT
     */
    public function addCouponDiscountInCart($promotionId, $cartProductArr = array(), $flag = 'indidiscount', $user_id = 0, $sourceType = 'frontend', $shipping_country) {
        if (!empty($cartProductArr)) {
            $totalProductWeight = 0;
            $totalProductWeightDiscount = 0;
            $getCoponStatus = $this->isCouponValid($promotionId, $user_id, $sourceType, $shipping_country);
            if ($getCoponStatus['status'] == 'success') {
                $promotionData = $getCoponStatus['promotionData'];
                $isFreeShipping = $promotionData['is_free_shipping'];
                $freeShippingAmount = $promotionData['free_shipping_amount'];
                $usersArr = array();
                $productsArr = array();
                if ($promotionData['promotion_user_ids'] != '') {
                    $usersArr = explode(',', $promotionData['promotion_user_ids']);
                }

                if ($promotionData['promotion_product_ids'] != '') {
                    $productsArr = explode(',', $promotionData['promotion_product_ids']);
                }

                /* public campaign check start */
                $isGlobalCampaign = 0;
                $this->campaignSession = new Container('campaign');
                if (isset($this->campaignSession->campaign_code)) {
                    $campParam = array('', $this->campaignSession->campaign_code);
                    $campaign = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCampaign($campParam);

                    if (isset($campaign[0]['is_global']) && $campaign[0]['is_global'] == 1) {
                        $isGlobalCampaign = 1;
                    }
                }
                /* public campaign check ends */

                $i = 0;

                if ($flag == 'indidiscount') {
                    foreach ($cartProductArr as $cartProduct) {
                        if ($cartProduct['product_id'] != '' && $cartProduct['product_id'] != 0) {


                            if (($promotionData['applicable_id'] == 1 || in_array($cartProduct['user_id'], $usersArr) || $isGlobalCampaign == 1) && ($promotionData['product_type'] == 1 || in_array($cartProduct['product_id'], $productsArr)) && $cartProduct['is_promotional'] == '1') {
                                $cartProductArr[$i]['product_discount'] = round(($promotionData['discount'] / 100) * ($cartProductArr[$i]['product_subtotal'] - $cartProduct['membership_discount']), 2);
                                $productAmount = ($cartProduct['product_subtotal'] - $cartProduct['membership_discount']);
                                $cartProductArr[$i]['is_free_shipping'] = ($isFreeShipping == '1' && ($productAmount >= $freeShippingAmount || $freeShippingAmount = '0' || $freeShippingAmount = '')) ? '1' : '0';
                                /* if ($isFreeShipping) {
                                  $cartProducts[$i]['shipping_price'] = 0;
                                  $cartProducts[$i]['surcharge_price'] = 0;
                                  $cartProducts[$i]['shipping_handling_price'] = 0;
                                  } */
                            } else {
                                $cartProductArr[$i]['is_free_shipping'] = '0';
                            }
                        }
                        $i++;
                    }
                } elseif ($flag == 'totaldiscount') {
                    $totalDiscount = 0;

                    foreach ($cartProductArr as $cartProduct) {
                        if ($cartProduct['product_id'] != '' && $cartProduct['product_id'] != 0) {
                            if (($promotionData['applicable_id'] == 1 || in_array($cartProduct['user_id'], $usersArr) || $isGlobalCampaign == 1) && ($promotionData['product_type'] == 1 || in_array($cartProduct['product_id'], $productsArr)) && $cartProduct['is_promotional'] == '1') {
                                $productDiscount = round(($promotionData['discount'] / 100) * ($cartProductArr[$i]['product_subtotal'] - $cartProduct['membership_discount']), 2);
                                $totalDiscount = $totalDiscount + $productDiscount;
                                if (($promotionData['applicable_to'] == 2 && $shipping_country != 228) || ($promotionData['applicable_to'] == 1 && $shipping_country == 228) || $promotionData['applicable_to'] == 3) {
                                    if ($promotionData['is_free_shipping'])
                                        $cartProduct['is_free_shipping'] = '1';
                                }
                                $totalProductWeightDiscount = $totalProductWeightDiscount + $cartProduct['product_weight'] * $cartProduct['product_qty'];
                                if ($cartProduct['is_shippable'] == '1' && $cartProduct['product_weight'] > 0 && $cartProduct['shipping_cost'] == '1' && $cartProduct['is_free_shipping'] == 0) {
                                    $totalProductWeight = $totalProductWeight + $cartProduct['product_weight'] * $cartProduct['product_qty'];
                                }
                            } else {
                                if ($cartProduct['is_shippable'] == '1' && $cartProduct['product_weight'] > 0 && $cartProduct['shipping_cost'] == '1' && $cartProduct['is_free_shipping'] == 0)
                                    $totalProductWeight = $totalProductWeight + $cartProduct['product_weight'] * $cartProduct['product_qty'];
                            }
                        }

                        $i++;
                    }

                    $retArr['totalDiscount'] = $totalDiscount;
                    $retArr['totalProductWeight'] = $totalProductWeight;
                    $retArr['totalProductWeightDiscount'] = $totalProductWeightDiscount;
                    return $retArr;
                }
            }
        }
        return $cartProductArr;
    }

    /**
     * This action is used to apply coupon discount in cart array
     * @param int,array
     * @return array
     * @author Icreon Tech - DT
     */
    public function getCouponDiscountForProductInCart($paramArr = array(), $shipping_country) {
        $promotionId = isset($paramArr['promotion_id']) ? $paramArr['promotion_id'] : '';
        $userId = isset($paramArr['user_id']) ? $paramArr['user_id'] : '';
        $cartParam['user_id'] = $userId;
        $cartParam['source_type'] = !empty($paramArr['source_type']) ? $paramArr['source_type'] : 'frontend';
        $cartProductArr = $this->getTransactionTable()->getCartProducts($cartParam);

        $totalDiscount['discount'] = 0;
        if (!empty($cartProductArr) && !empty($paramArr['promotionData'])) {
            $promotionData = $paramArr['promotionData'];
            $usersArr = array();
            $productsArr = array();
            if ($promotionData['promotion_user_ids'] != '') {
                $usersArr = explode(',', $promotionData['user_ids']);
            }
            if ($promotionData['promotion_product_ids'] != '') {
                $productsArr = explode(',', $promotionData['product_ids']);
            }

            /* public campaign check start */
            $isGlobalCampaign = 0;
            $this->campaignSession = new Container('campaign');
            if (isset($this->campaignSession->campaign_code)) {
                $campParam = array('', $this->campaignSession->campaign_code);
                $campaign = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCampaign($campParam);

                if (isset($campaign[0]['is_global']) && $campaign[0]['is_global'] == 1) {
                    $isGlobalCampaign = 1;
                }
                
            }
            /* public campaign check ends */

            $i = 0;
            $totalDiscount['apply'] = 0;

            foreach ($cartProductArr as $key => $cartProduct) {
                if ($cartProduct['product_id'] != '' && $cartProduct['product_id'] != 0) {
                    if (($promotionData['applicable_id'] == 1 || in_array($cartProduct['user_id'], $usersArr) || $isGlobalCampaign == 1) && ($promotionData['product_type'] == 1 || in_array($cartProduct['product_id'], $productsArr)) &&
                            $cartProduct['is_promotional'] == '1'
                    ) {
                        $productDiscount = round(($promotionData['discount'] / 100) * ($cartProductArr[$i]['product_subtotal'] - $cartProductArr[$i]['membership_discount']), 2);
                        $totalDiscount['discount'] = $totalDiscount['discount'] + $productDiscount;
                        $totalDiscount['apply']++;

                        if (($paramArr['promotionData']['applicable_to'] == 2 && $shipping_country != 228) || ($paramArr['promotionData']['applicable_to'] == 1 && $shipping_country == 228) || $paramArr['promotionData']['applicable_to'] == 3) {
                            if ($paramArr['promotionData']['is_free_shipping'])
                                $cartProductArr[$key]['is_free_shipping'] = '1';
                        }
                    }
                }
                $i++;
            }
        }
        $totalDiscount['cartArray'] = $cartProductArr;
        return $totalDiscount;
    }

   
     /**
     * This function is used to for order review page
     * @param void
     * @return     array
     * @author Icreon Tech - DT
     */
   
    public function posOrderreviewCartFrontAction() {
        $moduleSession = new Container('moduleSession');
        if ($moduleSession->moduleGroup != 3)
            $this->checkFrontUserAuthentication();
        $this->getTransactionTable();
        $msgArr = $this->_config['transaction_messages']['config']['checkout_front'];
        $transaction = new Transaction($this->_adapter);
        $inventoryForm = new InventoryForm();
        $request = $this->getRequest();
        $response = $this->getResponse();

        $paramArray = $request->getPost()->toArray();
        $transaction = new Postransaction($this->_adapter);
        $formDataFilter = $transaction->filterParam($formData);
        $posTransactionSession = new Container('posTransactionSession');
        $posTransactionSession->posTransactionData['user_details'] = array_merge($paramArray, $formDataFilter);
		
		
		
        
        if ($paramArray['pick_up'] == 1) {
            $shipcode = '';
        } else {
            $shipcode = !empty($paramArray['shipping_zip_code']) ? $paramArray['shipping_zip_code'] : '';
        }
        $user_id = session_id();
        $cartParam['user_id'] = $user_id;
        $cartParam['source_type'] = 'frontend';
        $cartData = $this->getTransactionTable()->getCartProducts($cartParam);
        $paramArray['userid'] = $user_id;
        $paramArray['shipcode'] = $shipcode;
        $paramArray['couponcode'] = !empty($paramArray['couponcode']) ? $paramArray['couponcode'] : '';
        if ($request->getPost('ship_m') != '' && $request->getPost('status') != 'error') {
        }
        $paramArray['postData'] = $paramArray;
        if ($paramArray['pick_up'] == 1) {
            $paramArray['shipping_method'] = '';
            $paramsArray['shipping_state'] = '';
        }
        $itemsTotal = $this->cartTotalWithTax($paramArray);

        $discount = '';

        $memberShip = array();
        $memberdiscount = '';
        if (empty($discount) && $itemsTotal['finaltotaldiscounted'] != 0) {
            $memberShip = $this->getTransactionTable()->getMinimumMembership();
            $memberdiscount = ($itemsTotal['finaltotaldiscounted'] * $memberShip[0]['discount']) / 100;
        }
        $isBookingKiosk = 0;
        $requestingIp = $this->getServiceLocator()->get('KioskModules')->getRemoteIP();
        if ($requestingIp) {
            if (isset($this->_config['kiosk-system-settings'][$requestingIp]))
                $isBookingKiosk = $this->_config['kiosk-system-settings'][$requestingIp]['isHotSystem'];
        }
        $viewModel = new ViewModel();

        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => array_merge($msgArr, $this->_config['transaction_messages']['config']['common_message']),
            'postdata' => $paramArray,
            'cartlist' => $cartData,
            'memberdiscount' => $memberdiscount,
            'memberShip' => $memberShip,
            'itemsTotal' => $itemsTotal,
            'upload_file_path' => $this->_config['file_upload_path'],
            'membershipDiscountAmount' => $itemsTotal['membershipDiscountAmount'],
            'isBookingKiosk' => $isBookingKiosk,
            'pick_up' => $request->getPost('pick_up'),
            'cash_payment' => $request->getPost('cash_payment')
        ));
        return $viewModel;
    }

    /**
     * This function is used for cart total with tax calculation 
     * @param void
     * @return     array
     * @author Icreon Tech - DT
     */
     
    function cartTotalWithTax($paramArray) {
        $user_id = $paramArray['userid'];
        $shippingcode = !empty($paramArray['shipcode']) ? $paramArray['shipcode'] : '';
        $couponCode = isset($paramArray['postData']['coupon_code']) ? $paramArray['postData']['coupon_code'] : $paramArray['couponcode'];
		if(strpos($couponCode,'(')){
				$couponCodeArray = explode('(',$couponCode);
				$couponCode = trim($couponCodeArray[0]);
		}
        
        $source_all = !empty($paramArray['source_all']) ? $paramArray['source_all'] : '';
        $cartParam['user_id'] = $user_id;
        $cartParam['source_type'] = !empty($paramArray['source_type']) ? $paramArray['source_type'] : 'frontend';
        if (defined('MEMBERSHIP_DISCOUNT'))
            $cartParam['membership_percent_discount'] = MEMBERSHIP_DISCOUNT;
        else
            $cartParam['membership_percent_discount'] = '';

        $cartParam['user_session_id'] = $user_id;
        $cartData = $this->getTransactionTable()->getCartProducts($cartParam);
		$promotionTable = $this->getServiceLocator()->get('Promotions\Model\PromotionTable');		
		$getPromotionDetail = $promotionTable->getPromotionDetailByCode($couponCode, 'code');
         if (!empty($getPromotionDetail['excluded_product_type'])) { // Exclude product
            $excludeProductTypeArray = explode(',', $getPromotionDetail['excluded_product_type']);
        }
        if (!empty($getPromotionDetail['excluded_products'])) {
            $excludeProductArray = explode(',', $getPromotionDetail['excluded_products']);
        }
        $totalCartAmount = 0;
        $totalShippableProductWeight = 0;
        $totalPromotionalCartAmount = 0;
        $totalPromotionalShippableProductWeight = 0;
        foreach ($cartData as $key => $val) {
            $totalCartAmount = $totalCartAmount + $val['product_subtotal'] - $val['membership_discount'];
            if ($val['is_shippable'] == 1 && $val['shipping_cost'] == 1 && $val['is_free_shipping'] == 0 && $val['product_weight'] > 0) {
                $totalShippableProductWeight+= ($val['product_weight'] * $val['product_qty']);
                if ($val['is_promotional'] == 1) {
                    $totalPromotionalShippableProductWeight+= ($val['product_weight'] * $val['product_qty']);
                    //$totalPromotionalCartAmount+= $val['product_subtotal'] - $val['membership_discount'];
                }
            }
			if ($val['is_promotional'] == 1 && (!in_array($val['product_type_id'], $excludeProductTypeArray)) && (!in_array($val['product_id'], $excludeProductArray))) {
				//$totalPromotionalShippableProductWeight+= ($val['product_weight'] * $val['product_qty']);
				$totalPromotionalCartAmount+= $val['product_subtotal'] - $val['membership_discount'];
			}
        }

        $subTotal = 0;
        $authorizeAmountTotal = 0;

        /** TAX * */
        $taxDetail = array();

        if (($paramArray['shipping_country'] == $paramArray['apo_po_shipping_country'] || $paramArray['shipping_country'] == 228) || ($paramArray['postData']['shipping_country'] == $paramArray['postData']['apo_po_shipping_country'] || $paramArray['postData']['shipping_country'] == 228)) {

            if (!empty($shippingcode)) {
                $postal_code = substr($shippingcode, 0, 5);
                $postal_code = explode('-', $postal_code);
                $postArray['postal_code'] = $postal_code[0];

                if (isset($paramArray['shipping_state']))
                    $postArray['shipping_state'] = $paramArray['shipping_state'];
                else if (isset($paramArray['postData']['shipping_state']))
                    $postArray['shipping_state'] = $paramArray['postData']['shipping_state'];

                $taxDetail = $this->getTransactionTable()->getTaxDetail($postArray);
            }
        }

        if (isset($taxDetail[0]['CombinedRate']) && !empty($taxDetail[0]['CombinedRate'])) {
            $combined_sales_tax = $taxDetail[0]['CombinedRate'];
        }
        $paramArray['combined_sales_tax'] = $combined_sales_tax;
        /* end of tax* */

        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $application_source_id = $this->_config['application_source_id'];
        /** code for promotion * */
        $promotionTable = $this->getServiceLocator()->get('Promotions\Model\PromotionTable');
        $couponCode = isset($paramArray['postData']['coupon_code']) ? $paramArray['postData']['coupon_code'] : $paramArray['couponcode'];
        if (!empty($couponCode)) {
            $getPromotionDetail = $promotionTable->getPromotionDetailByCode($couponCode, 'code');

            $getPromotionDetail['promotionbundledbroducts'] = array();
            if ((isset($getPromotionDetail['bundle_product_type']) && $getPromotionDetail['bundle_product_type'] != '') || (isset($getPromotionDetail['bundle_products']) && $getPromotionDetail['bundle_products'] != '')) {
                $arrBundleProduct = $promotionTable->getPromotionBundledProducts($getPromotionDetail['bundle_product_type'], $getPromotionDetail['bundle_products']);
                foreach ($arrBundleProduct as $bundleProduct) {
                    $getPromotionDetail['promotionbundledbroducts'][] = $bundleProduct['product_id'];
                }
            }

            $getPromotionDetail['promotionMinimumAmountProducts'] = array();
            if ((isset($getPromotionDetail['minimum_amount_product_type']) && $getPromotionDetail['minimum_amount_product_type'] != '') || (isset($getPromotionDetail['minimum_amount_products']) && $getPromotionDetail['minimum_amount_products'] != '')) {
                $arrMinimumAmountProduct = $promotionTable->getPromotionMinimumAmountProducts($getPromotionDetail['minimum_amount_product_type'], $getPromotionDetail['minimum_amount_products']);
                foreach ($arrMinimumAmountProduct as $minimumAmountProduct) {
                    $getPromotionDetail['promotionMinimumAmountProducts'][] = $minimumAmountProduct['product_id'];
                }
            }

            $getPromotionDetail['promotionAnyProducts'] = array();
            if ((isset($getPromotionDetail['promotion_any_product_type']) && $getPromotionDetail['promotion_any_product_type'] != '') || (isset($getPromotionDetail['promotion_any_products']) && $getPromotionDetail['promotion_any_products'] != '')) {
                $arrAnyProduct = $promotionTable->getPromotionMinimumAmountProducts($getPromotionDetail['promotion_any_product_type'], $getPromotionDetail['promotion_any_products']);
                foreach ($arrAnyProduct as $anyProduct) {
                    $getPromotionDetail['promotionAnyProducts'][] = $anyProduct['product_id'];
                }
            }

            if (!empty($getPromotionDetail)) {
                $promotionId = $getPromotionDetail['promotion_id'];
                // By Dev 2 NEWPROMOCODE.

                $returnArray = $this->isValidCouponCheck($selectedShipping_method, $user_id, $paramArray['postData']['shipping_country'], $getPromotionDetail, $cartData);

                $totalProductCartAmount = isset($returnArray['totalProductCartAmount']) ? $returnArray['totalProductCartAmount'] : 0;
                if ($returnArray['status'] == 'error') {
                    $getPromotionDetail['is_valid_coupon'] = 0;
                } else {
                    $getPromotionDetail['is_valid_coupon'] = 1;
                    // By Dev 2 NEWPROMOCODE

                    $inValidShippingMethod = 0;

                    if (($getPromotionDetail['applicable_to'] == 1 || $getPromotionDetail['applicable_to'] == 3) && (isset($paramArray['postData']['shipping_method']) && $paramArray['postData']['shipping_method'] != '') && $paramArray['postData']['shipping_country'] == '228') {

                        $shipping_method_array = explode(",", $paramArray['postData']['shipping_method']);
                        $selectedShipping_method = trim($shipping_method_array[2]);

                        $selectedPromoShippingMethod = $getPromotionDetail['pb_shipping_type_id'];
                        if ($selectedPromoShippingMethod != '' && !is_null($selectedPromoShippingMethod)) {
                            $shipMethodPromoArray = explode(",", $selectedPromoShippingMethod);
                        }
                        $shipingM = array();
                        if ($selectedShipping_method != '' && !in_array($selectedShipping_method, $shipMethodPromoArray)) {
                            $returnArray['status'] = "error";
                            $transaction = new Transaction($this->_adapter);
                            foreach ($shipMethodPromoArray as $shippingPromoId) {
                                $shippingMetArr = array();
                                $shippingMetArr['pb_shipping_type_id'] = $shippingPromoId;
                                $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                                $getShippingMethods = $this->_transactionTable->getShippingMethod($shippingMethodArr);

                                $shipingM[] = $getShippingMethods[0]['web_selection'];
                            }

                            $ship_method_name = '"' . implode('" OR "', $shipingM) . '"';

                            $returnArray['message'] = "Please select " . $ship_method_name . " shipping method for free shipping";
                            $inValidShippingMethod = 1;
                            //$getPromotionDetail['is_valid_coupon'] = 0;
                        }
                    }
                    if ($inValidShippingMethod == 0) {

                        $cartData = $this->applyCouponDiscountInCart($getPromotionDetail, $cartData, $paramArray['postData']['shipping_country'], $totalPromotionalCartAmount, $totalProductCartAmount, $totalPromotionalShippableProductWeight);
                    }
                }
            } else {
                $returnArray['status'] = "error";
                $returnArray['message'] = $msgArr['COUPON_CODE_NOT_EXIST'];
                $getPromotionDetail['is_valid_coupon'] = 0;
            }
        }
        /** end of promotion code * */
        $totalProductWeightWeight = 0;
        $isInventory = 0;
        foreach ($cartData as $key => $value) {
            if (($this->_config['application_source_id'] == 1) && ($value['is_shippable'] == '0' || empty($value['is_shippable']) || $value['shipping_cost'] == '0')) {
                $isInventory++;
            }

            if ($value['is_free_shipping'] != 1 && $value['product_weight'] > 0 && $value['is_shippable'] == 1 && $value['shipping_cost'] == 1) {
                $totalProductWeightWeight+= ($value['product_weight'] * $value['product_qty']);
            }
        }
        if (empty($cartData)) {
            $totalproduct = 0;
        } else {
            $totalproduct = count($cartData);
        }

        $cartDataArray = $this->addShippingCostToProducts($cartData, $paramArray, $totalProductWeightWeight);
        $cartData = $cartDataArray['cartdata'];
        $cartDataTotal = $cartDataArray['cartdataTotal'];

        $totalWeight = $totalProductWeightforPB;
        $returnArray['isAnyNotPickupField'] = 0;
        $returnArray['isInventory'] = $isInventory;
        // $returnArray['isInventory'] = 0;

        $returnArray['totalproduct'] = $totalproduct;
        $returnArray['totalweight'] = $totalProductWeightWeight;
        $returnArray['itemtotal'] = $this->Currencyformat($cartDataTotal['subTotalAmount']);
        $returnArray['itemtotalwoformat'] = round($cartDataTotal['subTotalAmount'] + $cartDataTotal['shipping_charge'], 2);
        //$returnArray['coupondiscount'] = $this->Roundamount($coupondis);
        $returnArray['coupondiscount'] = $this->Currencyformat($cartDataTotal['total_discount']);
        $returnArray['shipping'] = $this->Currencyformat($cartDataTotal['shipping_charge']);
        $returnArray['totalaftershipping'] = $this->Currencyformat($cartDataTotal['totalaftershipping']);
        $returnArray['estimatedtax'] = $this->Currencyformat($cartDataTotal['totalTaxAmount']);
        $returnArray['finaltotal'] = $this->Currencyformat($cartDataTotal['order_total']);
        $returnArray['finaltotaldiscounted'] = $this->Currencyformat($cartDataTotal['total_discount']);
        //$returnArray['finaltotaldonationamount'] = ($donationTotalAmount != 0) ? $this->Roundamount($donationTotalAmount + $shipping + $estimatedtax) : 0;
        $returnArray['finaltotaldonationamount'] = 0;
        //$returnArray['isOnlyDonation'] = $isOnlyDonation;
        $returnArray['isOnlyDonation'] = 0;
        $returnArray['membershipDiscountAmount'] = $cartDataTotal['total_membership_discount'];
        $returnArray['validPromotionsForUser'] = $validPromotionsForUser;
        $returnArray['application_source_id'] = $application_source_id;
        $returnArray['cartData'] = $cartData;

        return $returnArray;
    }

    /**
     * This function is used to get shipping cost of product
     * @param void
     * @return void
     * @author Icreon Tech - DT
     */
    public function addShippingCostToProducts($cartProducts, $postArr = array(), $totalProductWeightAmountForShipping) { // dev 2 NEWPROMOCODE 
        //echo $totalProductWeightAmountForShipping;die;
        if (!empty($cartProducts) && !empty($postArr)) {
            $postData = $postArr['postData'];
            $countryName = '';
            if (!empty($postData['shipping_country'])) {
                $countryArr = array('country_id' => $postData['shipping_country']);
                $common = new Common($this->_adapter);
                $countryArr = $common->getCountryArr($countryArr);
                $countryData = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCountryUser($countryArr);
                $countryName = $countryData[0]['country_code'];
            }
            $residential = 'true';
            if (isset($postArr['is_apo_po']) && $postArr['is_apo_po'] != '0' && $postArr['is_apo_po'] != '1') {
                $residential = 'false';
            }
            $postArr['residential'] = $residential;
            $postArr['postData']['shipping_country'] = $countryName;


            if (isset($postArr['shipping_method']) && trim($postArr['shipping_method']) != 'undefined' && trim($postArr['shipping_method']) != '') {
                $pbSettingInfo = $this->getServiceLocator()->get('Common\Model\CommonTable')->getPBSetting();
                $shipApiUrl = $pbSettingInfo['pb_api_url'];
                $shipApiUsername = $pbSettingInfo['pb_api_username'];
                $shipApiRequestUsername = $pbSettingInfo['request_username'];
                $shipApiPassword = $pbSettingInfo['pb_api_password'];
                $packageType = $pbSettingInfo['package_type'];
                $carrShipArr = explode(',', $postArr['shipping_method']);
                $carrierId = $carrShipArr[0];
                if($carrierId == '13') 
					$packageType = '33';
                $serviceType = $carrShipArr[1];
                $shipDate = DATE_TIME_FORMAT;
                $holidayDate = $shipDate;
                $validDate = 0;
                $transaction = new Transaction($this->_adapter);
                while (!$validDate) {
                    $holidArr = array('holiday_date' => $holidayDate);
                    $holidayArr = $transaction->getHolidayArr($holidArr);
                    $holidayData = $this->_transactionTable->getHoliday($holidayArr);
                    if (empty($holidayData)) {
                        $validDate = 1;
                        $shipDate = date('Y-m-d h:i:s', strtotime($holidayDate));
                    }
                    $holidayDate = date('Y-m-d', strtotime('+1 day', strtotime($holidayDate)));
                }
                $currentDay = date('N', strtotime($shipDate));
                if ($currentDay > 5) {
                    $nextShipDate = 7 - ($currentDay - 1);
                    $shipDate = date('m-d-Y h:i:s A', strtotime('+' . $nextShipDate . ' day', strtotime($shipDate)));
                }
                $xmlRateRequest = '<?xml version = "1.0" encoding = "utf-8"?><PierbridgeRateRequest>
    <TransactionIdentifier />
    <Orders>
        <Order>
            <OrderID />
            <PickLists>
                <PickList>
                    <PickListID />
                </PickList>
            </PickLists>
        </Order>
    </Orders>
    <InsuranceProvider />
    <Live />
    <CloseShipment />
    <Carrier>' . $carrierId . '</Carrier>
    <ServiceType>' . $serviceType . '</ServiceType>
    <RateType />
    <ShipDate>' . $shipDate . '</ShipDate>
    <RequiredDate />
    <SaturdayDelivery />
    <Location />
    <AccountID />
    <Truckload />
    <ShipmentDescription />
    <FreightAllKinds />
    <Interline />
    <PreferredCarrier />
    <PreferredCarrierCode />
    <AccessorialCodes />
    <LoadingDockDelivery />
    <ConstructionSitePickup />
    <ConstructionSiteDelivery />
    <TradeShowPickup />
    <TradeShowDelivery />
    <InsidePickup />
    <InsideDelivery />
    <LiftGateDelivery />
    <LiftGatePickup />
    <AppointmentDelivery />
    <AppointmentPickup />
    <UnloadFreightAtDelivery />
    <LoadFreightAtPickup />
    <WhiteGlove />
    <TwoManDelivery />
    <DefaultWeightUOM />
    <DefaultDimensionsUOM />
    <ConsolidatedShipmentID />
    <DefaultCurrency />
    <Sender>
        <SentBy />
        <CompanyName />
        <Street />
        <Locale />
        <Other />
        <City />
        <Region />
        <PostalCode />
        <Country />
        <Phone />
        <Email />
        <JobTitle />
        <DepartmentName />
        <LocationDescription />
        <Residential>true</Residential>
        <OverrideType />
    </Sender>
    <Receiver>
        <CompanyName>' . $postData['shipping_company'] . '</CompanyName>
        <Street>' . $postData['shipping_address'] . '</Street>
        <Locale />
        <Other />
        <City>' . $postData['shipping_city'] . '</City>
        <State>' . $postData['shipping_state'] . '</State>
        <PostalCode>' . $postData['shipping_zip_code'] . '</PostalCode>
        <Country>' . $countryName . '</Country>
        <Residential />
        <ReceiverIdentifier />
    </Receiver>
    <ReturnTo>
        <Name />
        <CompanyName />
        <Street />
        <Locale />
        <Other />
        <City />
        <Region />
        <PostalCode />
        <Country />
        <Phone />
    </ReturnTo>
    <Billing>
        <PayerType />
        <AccountNumber />
        <CompanyName />
        <Street />
        <Locale />
        <Other />
        <City />
        <Region />
        <PostalCode />
        <Country />
    </Billing>
    <International>
        <AESTransactionNumber />
        <TermsOfSale />
        <SED>
            <Method />
            <ExemptNumber />
        </SED>
    </International>
    <Packages>';
                $xmlRateRequest.='<Package>
            <Copies />
            <PackItemID />
            <ShipperReference/>
            <ReceiverName>' . $postData['shipping_first_name'] . $postData['shipping_last_name'] . '</ReceiverName>
            <ReceiverPhone />
            <ReceiverEmail />
            <PackageType>' . $packageType . '</PackageType>
            <Weight>' . $totalProductWeightAmountForShipping . '</Weight>
            <WeightUOM />
            <Length/>
            <Width/>
            <Height/>
            <DimensionsUOM />
            <ContentDescription>' . $cartProducts[0]['product_name'] . '</ContentDescription>
            <Insurance>
                <Type />
                <Value />
                <ValueCurrency />
            </Insurance>
            <COD>
                <Type />
                <Value />
                <ValueCurrency />
            </COD>
            <Hold />
            <Holder>
                <Name />
                <CompanyName />
                <Street />
                <Locale />
                <Other />
                <City />
                <Region />
                <PostalCode />
                <Country />
                <Phone />
            </Holder>
            <AdditionalHandling />
            <Oversize />
            <LargePackage />
            <DeliveryConfirmation />
            <FreightClass />
            <NMFC />
            <ItemsOnPallet />
            <NonStandardContainer />
            <HomeDeliveryType />
            <EmailNotification />
            <NonFlatMachinable />
            <NonMachinable />
            <NonRectangular />
            <Flat />
            <Stackable />
            <Registered />
            <Certified />
            <DryIceWeight />
            <DryIceWeightUOM />
            <ShipperRelease />
            <FreezeProtect />
            <PalletExchange />
            <SortAndSegregate />
            <International>
                <DocumentsOnly />
                <Contents>
                    <Content>
                        <Code />
                        <Quantity >' . $cartProducts[0]['product_qty'] . '</Quantity>
                        <OrderedQuantity />
                        <BackOrderedQuantity />
                        <ContentLineValue />
                        <Value />
                        <ValueCurrency />
                        <Weight />
                        <WeightUOM />
                        <Description />
                        <OriginCountry />
                        <PurchaseOrderNumber />
                        <SalesOrderNumber />
                        <ItemCode />
                        <ItemDescription />
                        <UnitsOfMeasure />
                        <CustomerCode />
                        <PartNumber />
                        <BinNumber />
                        <LotNumber />
                        <SerialNumber />
                        <Hazardous />
                        <HazardousExemptionID />
                        <HazardousType />
                        <HazardousUnits />
                        <HazardousUnitsUOM />
                        <HazardousDescription />
                        <HazardousProperShippingName />
                        <HazardousIdentifier />
                        <HazardousClass />
                        <HazardousSubClass />
                        <HazardousPackingGroup />
                        <HazardousTechnicalName />
                        <HazardousTotalQuantity />
                        <HazardousTotalQuantityUOM />
                        <HazardousLabelCodes />
                        <HazardousLabelCodesMask />
                        <HazardousAccessible />
                        <HazardousPassengerAircraft />
                        <HazardousCargoAircraftOnly />
                        <HazardousRequiredInformation />
                        <HazardousConsumerCommodity />
                        <HazardousPackingInstructions />
                        <HazardousIsSpecialProvisionA1A2A51A109 />
                    </Content>
                </Contents>
            </International>
        </Package>';
//    }
//}

                $xmlRateRequest.='</Packages>
    <UserName>' . $shipApiRequestUsername . '</UserName>
</PierbridgeRateRequest>';

                $isAnyProduct = ($totalProductWeightAmountForShipping > 0) ? 1 : 0;

                $totalShipping = 0;

                if ($isAnyProduct == 1) {
                    $curl = curl_init();
                    curl_setopt($curl, CURLOPT_URL, $shipApiUrl);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($curl, CURLOPT_TIMEOUT, 120);
                    curl_setopt($curl, CURLOPT_USERPWD, "$shipApiUsername:$shipApiPassword");
                    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                    curl_setopt($curl, CURLOPT_POST, 1);
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $this->removeSpecialChar($xmlRateRequest));
                    $response = $this->getResponse();
                    $createTransactionMessage = $this->_config['transaction_messages']['config']['create_crm_transaction'];

                    if (($result = curl_exec($curl)) === false) {
                        $msg = array();
                        $msg ['shipping_method'] = curl_errno($curl);
                        $messages = array('status' => "shipping_error", 'message' => $msg);
                        echo \Zend\Json\Json::encode($messages);

                        if (isset($postArr['source_type']) && $postArr['source_type'] == 'frontend') {
                            die;
                        }
                    }
                    curl_close($curl);
                    $xml = new SimpleXMLElement($result);
                    $shipItemPrices = $xml->Packages->Package;

                    $j = 0;
                    $applicationConfigIds = $this->_config['application_config_ids'];
                    $uSId = $applicationConfigIds['united_state_country_id'];
                    if ($shipItemPrices->Status->Code == 0 && $carrierId != 'custom') { // commented on 2 july 2014
//if ($shipItemPrices->Status->Code == '9') { // we need to remove this line and uncomment the above line
                        $errorDescription = $shipItemPrices->Status->Description;
                        $errorType = 3;

                        if (strpos($errorDescription, 'country code for Consignee') > 0 || strpos($errorDescription, 'country is not valid') > 0 || strpos($errorDescription, 'country not served') > 0) {
                            $errorType = 1;
                            $errorMessage = $createTransactionMessage['L_INVALID_COUNTRY'];
                        } else if (strpos($errorDescription, 'CONSIGNEE') > 0) {
                            $errorType = 2;
                            $errorMessage = $createTransactionMessage['L_INVALID_POSTAL'];
                        } else {
                            $errorType = 3;
                            $errorMessage = $createTransactionMessage['INVALID_ADDRESS'];
                        }

                        $msg = array();
                        if ($postData['shipping_zip_code'] == '') {
                            $msg ['shipping_zip_code'] = $createTransactionMessage['SHIPPING_ZIP_CODE_EMPTY'];
                        } else if ($countryName == '') {
                            $msg ['shipping_country'] = $createTransactionMessage['SHIPPING_COUNTRY_EMPTY'];
                        } else {
                            if ($errorType == 1) {
                                $msg ['shipping_country'] = $errorMessage;
                            } else if ($errorType == 2) {
                                $msg ['shipping_zip_code'] = $errorMessage;
                            } else {
                                $msg ['shipping_method'] = $errorMessage;
                            }
                        }
                        $messages = array('status' => "shipping_error", 'message' => $msg);
                        echo \Zend\Json\Json::encode($messages);
                        if (isset($postArr['source_type']) && $postArr['source_type'] == 'frontend') {
                            die;
                        }
                    } else {
                        $lbsCheck = 0;
                        $surchangeUnder = 0;
                        $surchangeAbove = 0;
                        $shippingHandlingUnder = 0;
                        $shippingHandlingAbove = 0;
                        if (isset($postData['is_apo_po']) && $postData['is_apo_po'] != '0' && $postData['is_apo_po'] != '1') {
                            $shippingMetArr = array();
                            $shippingMetArr['pb_shipping_id'] = $applicationConfigIds['apo_po_pb_id'];
                            $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                            $getShippingMethods = $this->_transactionTable->getShippingMethod($shippingMethodArr);
                            $lbsCheck = $getShippingMethods[0]['lbs_check'];
                            $surchangeUnder = $getShippingMethods[0]['surchange_under_5'];
                            $surchangeAbove = $getShippingMethods[0]['surchange_above_5'];
                            $shippingHandlingUnder = $getShippingMethods[0]['shipping_handling_under_5'];
                            $shippingHandlingAbove = $getShippingMethods[0]['shipping_handling_above_5'];
                        } else if (isset($postData['shipping_country']) && $postData['shipping_country'] == $uSId && $postData['is_apo_po'] == '1') {
                            $shippingMetArr = array();
                            $shippingMetArr['pb_shipping_id'] = $applicationConfigIds['domestic_pb_id'];
                            $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                            $getShippingMethods = $this->_transactionTable->getShippingMethod($shippingMethodArr);
                            $lbsCheck = $getShippingMethods[0]['lbs_check'];
                            $surchangeUnder = $getShippingMethods[0]['surchange_under_5'];
                            $surchangeAbove = $getShippingMethods[0]['surchange_above_5'];
                            $shippingHandlingUnder = $getShippingMethods[0]['shipping_handling_under_5'];
                            $shippingHandlingAbove = $getShippingMethods[0]['shipping_handling_above_5'];
                        } else {

                            $shippingMetArr = array();
                            $shippingMetArr['pb_shipping_id'] = $applicationConfigIds['internation_pb_id'];
                            $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                            $getShippingMethodsInter = $this->_transactionTable->getShippingMethod($shippingMethodArr);
                            $lbsCheck = $getShippingMethodsInter[0]['lbs_check'];
                            if ($totalProductWeightAmountForShipping <= $lbsCheck) {
                                foreach ($getShippingMethodsInter as $shipMethod) {
                                    if ($shipMethod['pb_shipping_type_id'] == $applicationConfigIds['internation_less_than_lbs_id']) {
                                        $getShippingMethods[] = $shipMethod;
                                        $lbsCheck = $shipMethod['lbs_check'];
										$surchangeUnder = $shipMethod['surchange_under_5'];
										$surchangeAbove = $shipMethod['surchange_above_5'];
										$shippingHandlingUnder = $shipMethod['shipping_handling_under_5'];
										$shippingHandlingAbove = $shipMethod['shipping_handling_above_5'];
                                    }
                                }
                            } else {
                                foreach ($getShippingMethodsInter as $shipMethod) {
                                    if ($shipMethod['pb_shipping_type_id'] == $applicationConfigIds['internation_greater_than_lbs_id']) {
                                        $getShippingMethods[] = $shipMethod;
                                        $lbsCheck = $shipMethod['lbs_check'];
										$surchangeUnder = $shipMethod['surchange_under_5'];
										$surchangeAbove = $shipMethod['surchange_above_5'];
										$shippingHandlingUnder = $shipMethod['shipping_handling_under_5'];
										$shippingHandlingAbove = $shipMethod['shipping_handling_above_5'];
                                    }
                                }
                            }
                            //$lbsCheck = $getShippingMethods[0]['lbs_check'];
                            //$surchangeUnder = $getShippingMethods[0]['surchange_under_5'];
                            //$surchangeAbove = $getShippingMethods[0]['surchange_above_5'];
                            //$shippingHandlingUnder = $getShippingMethods[0]['shipping_handling_under_5'];
                            //$shippingHandlingAbove = $getShippingMethods[0]['shipping_handling_above_5'];
                        }
                    }
                }
            }
//foreach ($shipItemPrices as $itemPrice) {
            $category = new Category($this->_adapter);
            $catArr = array();
            $categoryArr = $category->getCategoriesArr($catArr);
            $allCategories = $this->getServiceLocator()->get('Category\Model\CategoryTable')->getCategories($categoryArr);
            $categoryData = array();
            if (!empty($allCategories)) {
                foreach ($allCategories as $category) {
                    $categoryData[$category['category_id']] = $category['parent_id'];
                }
            }
            $dataArr = array();
            $dataArr['categoryData'] = $categoryData;

            $totalShipping = 0;
            $j = 0;
            $z = 1;

            $productCnt = count($cartProducts);

            $totalProductDiscount = 0;
            $totalMembershipDiscount = 0;
            $totalShippingDiscount = 0;
            $totalShipping = 0;
            $totalTaxAmount = 0;
            $subTotal = 0;
            $subtotalAmount = 0;
            $totalAfterShipping = 0;
            $totalShippingPercentage = 0;
            //

            $orderTotal = 0; // final amount

            $totalShippingCostFromPB = (isset($shipItemPrices->Shipping->TotalCharge)) ? ((string) $shipItemPrices->Shipping->TotalCharge) : 0;

            foreach ($cartProducts as $index => $value) {

                $totalProductDiscount+=$value['product_discount'];
                $totalMembershipDiscount+=$value['membership_discount'];
                $totalShippingDiscount+=$value['shipping_discount_amount'];
                $productShippingPrice = 0;

                if ($value['is_shippable'] == '1' && $value['product_weight'] > 0 && $value['is_free_shipping'] != '1' && $value['shipping_cost'] == '1') {

                    $percentShipping = ($value['product_weight'] * $value['product_qty'] * 100) / $totalProductWeightAmountForShipping;

                    $totalShippingPercentage = $totalShippingPercentage + $percentShipping;

                    //....... to be set above ...../
                    if ($z == count($cartProducts)) {
                        if ($totalShippingPercentage < 100)
                            $percentShipping = $percentShipping + ( 100 - $totalShippingPercentage);
                    }
                    // .................... to set above .........................//
                    $productShippingPrice = ($percentShipping * $totalShippingCostFromPB) / 100;
                    //
                    /* if ($carrierId == 'custom') {
                      $productShippingPrice = ($postArr['postData']['custom_shipping'] * $percentShipping) / 100;
                      $productShippingPrice = $productShippingPrice - $value['shipping_discount_amount'];
                      } */
                    // .......................//

                    $dataArr['category_id'] = $value['category_id'];
                    $isCustomFrame = $this->isCustomFrame($dataArr);

                    if ($isCustomFrame) {

                        $cartProducts[$index]['shipping_price'] = $productShippingPrice;
                        $surchangeTax = ($cartProducts[$index]['product_weight'] <= $getShippingMethods[0]['custom_frames_lbs_check']) ? $getShippingMethods[0]['surchange_under_15'] : $getShippingMethods[0]['surchange_above_15'];
                        $shippingHandlineTax = ($cartProducts[$index]['product_weight'] <= $getShippingMethods[0]['custom_frames_lbs_check']) ? $getShippingMethods[0]['shipping_handling_under_15'] : $getShippingMethods[0]['shipping_handling_above_15'];
                        if ($productShippingPrice == 0)
                            $shippingHandlineTax = 0;
                        $cartProducts[$index]['surcharge_price'] = ((($productShippingPrice) * $surchangeTax) / 100);
                        $cartProducts[$index]['shipping_handling_price'] = $shippingHandlineTax;
                    } else {
                        $cartProducts[$index]['shipping_price'] = $productShippingPrice;
                        $surchangeTax = ($cartProducts[$index]['product_weight'] <= $lbsCheck) ? $surchangeUnder : $surchangeAbove;
                        $shippingHandlineTax = ($cartProducts[$index]['product_weight'] <= $lbsCheck) ? $shippingHandlingUnder : $shippingHandlingAbove;
                        if ($productShippingPrice == 0)
                            $shippingHandlineTax = 0;
                        $cartProducts[$index]['surcharge_price'] = ($productShippingPrice * $surchangeTax / 100);
                        $cartProducts[$index]['shipping_handling_price'] = $shippingHandlineTax;
                    }
                }else {
                    $cartProducts[$index]['shipping_price'] = '0.00';
                    $cartProducts[$index]['surcharge_price'] = '0.00';
                    $cartProducts[$index]['shipping_handling_price'] = '0.00';
                }

                if ($carrierId == 'custom') {

                    $productShippingPrice = ($postArr['postData']['custom_shipping'] * $percentShipping) / 100;
                    $productShippingPrice = $productShippingPrice - $value['shipping_discount_amount'];
                    $totalShipping+=$this->Roundamount($productShippingPrice);
                    $cartProducts[$index]['shipping_price'] = $this->Roundamount($productShippingPrice);
                    $cartProducts[$index]['surcharge_price'] = '0.00';
                    $cartProducts[$index]['shipping_handling_price'] = '0.00';
                } else {
                    if ($productShippingPrice != 0) {
                        //round off shipping before adding into product total
                        $productShippingPrice = $this->Roundamount($productShippingPrice) + $this->Roundamount($cartProducts[$index]['surcharge_price']) + $this->Roundamount($cartProducts[$index]['shipping_handling_price']);

                        $productShippingPrice = $this->Roundamount($productShippingPrice - $value['shipping_discount_amount']);
                        if ($productShippingPrice < 0)
                            $productShippingPrice = '0.00';
                        $totalShipping+=$productShippingPrice;
                    }
                }
                $subtotalAmount = ($value['product_amount'] * $value['product_qty']) - $value['membership_discount'];
                //.... tax ...//

                $product_amount_for_tax = $subtotalAmount - $value['product_discount'] + $productShippingPrice;
                $productTotalForRevenue = $subtotalAmount - $value['product_discount'];
                if ($product_amount_for_tax < 0)
                    $product_amount_for_tax = 0;
                $totalAfterShipping+= $this->Roundamount($product_amount_for_tax);


                if (isset($cartProducts[$index]['is_taxable']) && $cartProducts[$index]['is_taxable'] == 1 && (isset($postData['shipping_country']) && $postData['shipping_country'] == '228') && (((isset($postData['shipping_state']) && $postData['shipping_state'] == 'NY') || (isset($postData['shipping_state_id']) && $postData['shipping_state_id'] == 'NY') || (isset($postData['apo_po_state']) && $postData['apo_po_state'] == 'NY')))) {

                    $product_tax = $this->Roundamount($product_amount_for_tax) * $postArr['combined_sales_tax'];
                    $cartProducts[$index]['product_taxable_amount'] = $this->Roundamount($product_tax);
                } else {
                    $cartProducts[$index]['product_taxable_amount'] = '0.00';
                }
                $totalTaxAmount+=$cartProducts[$index]['product_taxable_amount'];

                //.... end of tax //


                $cartProducts[$index]['product_total'] = $this->Roundamount($product_amount_for_tax) + $cartProducts[$index]['product_taxable_amount'];
                $cartProducts[$index]['total_product_revenue'] = $this->Roundamount($productTotalForRevenue);

                $orderTotal+= $cartProducts[$index]['product_total'];

                $subTotal+= ($value['product_amount'] * $value['product_qty']) - $value['membership_discount'];

                $cartProducts[$index]['product_discount'] = $this->Roundamount($cartProducts[$index]['product_discount']);
                $cartProducts[$index]['shipping_price'] = $this->Roundamount($cartProducts[$index]['shipping_price']);
                $cartProducts[$index]['surcharge_price'] = $this->Roundamount($cartProducts[$index]['surcharge_price']);
                $cartProducts[$index]['shipping_handling_price'] = $this->Roundamount($cartProducts[$index]['shipping_handling_price']);
                $cartProducts[$index]['product_taxable_amount'] = $this->Roundamount($cartProducts[$index]['product_taxable_amount']);
                $cartProducts[$index]['product_total'] = $this->Roundamount($cartProducts[$index]['product_total']);
                $j++;
                $z++;
            }
            //}
            //}
            $cartProductsTotalArray['totalTaxAmount'] = $this->Roundamount($totalTaxAmount);
            $cartProductsTotalArray['subTotalAmount'] = $this->Roundamount($subTotal);
            $cartProductsTotalArray['totalaftershipping'] = $this->Roundamount($totalAfterShipping);
            $cartProductsTotalArray['shipping_charge'] = $this->Roundamount($totalShipping);
            $cartProductsTotalArray['total_discount'] = $this->Roundamount($totalProductDiscount);
            $cartProductsTotalArray['total_membership_discount'] = $this->Roundamount($totalMembershipDiscount);
            $cartProductsTotalArray['order_total'] = $this->Roundamount($orderTotal);
            $cartProductsTotalArray['shipping_discount_in_amount'] = $this->Roundamount($totalShippingDiscount);


            $retArray = array();
            $retArray['cartdata'] = $cartProducts;
            $retArray['cartdataTotal'] = $cartProductsTotalArray;

            return $retArray;
        }
    }

    /**
     * This action is used to check whether coupon is valid or not
     * @param void
     * @return void
     * @author Icreon Tech - DT
     */
    public function applyCouponAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $postArr = $request->getPost()->toArray();
            $applicationConfigIds = $this->_config['application_config_ids'];
            $uSId = $applicationConfigIds['united_state_country_id'];
            $this->getTransactionTable();
            $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
            $commonMsg = $this->_config['transaction_messages']['config']['common_message'];
            // By Dev 2 NEWPROMOCODE
            if ($request->isPost()) {
                if ($request->getPost('user_id') && $request->getPost('user_id') != '') {
                    if ($request->getPost('save_shipping_address') != '' && $request->getPost('status') != 'error') {
                        $paramsArray = $request->getPost()->toArray();
                        if (isset($paramsArray['is_apo_po']) && $paramsArray['is_apo_po'] == '3') {
                            $paramsArray['shipping_state'] = $paramsArray['apo_po_state'];
                            $paramsArray['shipping_country'] = $paramsArray['apo_po_shipping_country'];
                        } else if (isset($paramsArray['shipping_country']) && $paramsArray['shipping_country'] == $uSId) {
                            if ($paramsArray['is_apo_po'] == '2') {
                                $paramsArray['shipping_country'] = $paramsArray['apo_po_shipping_country'];
                            }
                        }
                        $paramsArray['postData'] = $paramsArray;
                    }
                    $paramsArray['userid'] = $request->getPost('user_id');
                    $paramsArray['shipcode'] = $request->getPost('shipping_zip_code');
                    $paramsArray['couponcode'] = trim($request->getPost('coupon_code'));
                    $paramsArray['source_type'] = 'backend';

                    $searchResult = $this->cartTotalWithTax($paramsArray);

                    if ($searchResult['status'] == 'error') {
                        $msg = array();
                        $msg ['coupon_code'] = $searchResult['message'];
                    } else {
                        $msg = $searchResult['message'];
                    }
                    $messages = array('status' => $searchResult['status'], 'message' => $msg);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                }
            }
        } else {
            return $this->redirect()->toRoute('crm-transaction');
        }
    }

    // By Dev 2 used to validate the coupon code NEWPROMOCODE
    public function isValidCouponCheck($selectedShipping_method, $userId, $shipping_country = '0', $getPromotionDetail, $cartData) {
        $returnData = array();
        $applicationConfigIds = $this->_config['application_config_ids'];
        $uSId = $applicationConfigIds['united_state_country_id'];

        $serviceManager = $this->getServiceLocator();
        $promotionTable = $serviceManager->get('Promotions\Model\PromotionTable');

        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        if (count($getPromotionDetail) > 0) {
            $promotionUsedByUserRecord = array();

            /** Expiry date check * */
            if (($getPromotionDetail['expiry_date'] != '' && $getPromotionDetail['expiry_date'] != 0 && substr(DATE_TIME_FORMAT, 0, 10) > substr($getPromotionDetail['expiry_date'], 0, 10)) || ($getPromotionDetail['start_date'] != '' && $getPromotionDetail['start_date'] != 0 && substr(DATE_TIME_FORMAT, 0, 10) < substr($getPromotionDetail['start_date'], 0, 10))) {
                $returnData['status'] = "error";
                $returnData['message'] = $msgArr['COUPON_CODE_EXPIRED'];
                return $returnData;
            }
            /** end of expiry chaek * */
            /** Usage Limit Check * */
            if ($getPromotionDetail['usage_limit_type'] == 2 || $getPromotionDetail['usage_limit_type'] == 3) { // One Time Per Contact Limit Condition
                $promoUsedSearchParam = array();
                $promoUsedSearchParam['userId'] = $userId;
                $promoUsedSearchParam['promotionId'] = $getPromotionDetail['promotion_id'];
                $promotionUsedDetail = $promotionTable->checkUserUsedCoupon($promoUsedSearchParam); // used to check that coupon is used by user or not
            }

            if (($getPromotionDetail['usage_limit_type'] == 1) || ($getPromotionDetail['usage_limit_type'] == 2 && $promotionUsedDetail[0]['userUsedCount'] == 0) || (($getPromotionDetail['usage_limit_type'] == 3 && $getPromotionDetail['usage_limit_per_contact'] != 1) && ($getPromotionDetail['usage_limit'] > 0 && $getPromotionDetail['usage_limit'] > $promotionUsedDetail[0]['promotionUsedCount']) ) || (($getPromotionDetail['usage_limit_type'] == 3 && $getPromotionDetail['usage_limit_per_contact'] == 1) && ($getPromotionDetail['usage_limit'] > 0 && $promotionUsedDetail[0]['userUsedCount'] == 0) && ($getPromotionDetail['usage_limit'] > $promotionUsedDetail[0]['promotionUsedCount']))) {

                if ($getPromotionDetail['is_shipping_discount'] == 1 && $getPromotionDetail['shipping_discount_type'] == 1) { // shipping_discount
                    if (!empty($getPromotionDetail['applicable_to'])) { // domestic/international
                        $invalidFl = 0;
                        if ($getPromotionDetail['applicable_to'] == 1 && $shipping_country != '228') { // Domestic
                            $invalidFl = 1;
                        } else if ($getPromotionDetail['applicable_to'] == 2 && $shipping_country == '228') { // International
                            $invalidFl = 1;
                        } else if ($getPromotionDetail['applicable_to'] == 3) {
                            $invalidFl = 0;
                        }



                        if ($invalidFl == 1) { 
                            $returnData['status'] = "error";
                            $returnData['message'] = $msgArr['COUPON_NOT_VALID'];
                            return $returnData;
                        }
                    }
                }

                if ($getPromotionDetail['applicable_id'] == 2) { // Applied for check in contact check
                    $promotionUserSearchParam = array();
                    $promotionUserSearchParam['promotion_id'] = $getPromotionDetail['promotion_id'];
                    $promotionUserSearchParam['user_id'] = $userId;
                    $promotionUserSearchParam['applicable_id'] = 2; // 2=for user, 3=Group        

                    $promotionUsersRecords = $promotionTable->getUsersInPromotion($promotionUserSearchParam);
                } else if ($getPromotionDetail['applicable_id'] == 3) { // Applied for check in group check
                    $promotionUserSearchParam = array();
                    $promotionUserSearchParam['promotion_id'] = $getPromotionDetail['promotion_id'];
                    $promotionUserSearchParam['user_id'] = $userId;
                    $promotionUserSearchParam['applicable_id'] = 3; // 2=for user, 3=Group 					

                    $promotionUsersRecords = $promotionTable->getUsersInPromotion($promotionUserSearchParam);

                    if (empty($promotionUsersRecords) || ($promotionUsersRecords[0]['totalApplied'] == 0)) {
                        $appliedSearchParam = array();
                        $availableUsers = array();
                        $appliedSearchParam[] = $getPromotionDetail['promotion_id'];
                        $appliedSearchParam[] = 'applied';
                        $appliedSearchParam[] = 'Group';
                        $appliedData = $promotionTable->getProductPromotionAppliedByPromotionId($appliedSearchParam);

                        foreach ($appliedData as $appliedRecord) {
                            if ($appliedRecord['group_type'] == 2) {

                                parse_str($appliedRecord['search_query'], $searchParam);

                                $searchParam['transaction_amount_date_from'] = (isset($searchParam['transaction_amount_date_from']) and trim($searchParam['transaction_amount_date_from']) != "") ? $this->DateFormat($searchParam['transaction_amount_date_from'], 'db_date_format_from') : "";

                                $searchParam['transaction_amount_date_to'] = (isset($searchParam['transaction_amount_date_to']) and trim($searchParam['transaction_amount_date_to']) != "") ? $this->DateFormat($searchParam['transaction_amount_date_to'], 'db_date_format_to') : "";

                                $searchParam['userId_exclude'] = '';
                                $excludedContact = $this->getServiceLocator()->get('Group\Model\GroupTable')->getExcludedContactInGroup(array('groupId' => $appliedRecord['applied_id']));
                                if (!empty($excludedContact)) {
                                    $userIds = array();
                                    foreach ($excludedContact as $val) {
                                        $userIds[] = $val['user_id'];
                                    }
                                    $searchParam['userId_exclude'] = implode(",", $userIds);
                                }

                                $searchParam['startIndex'] = '';
                                $searchParam['recordLimit'] = '';
                                $searchParam['sortField'] = '';
                                $searchParam['sortOrder'] = '';



                                $group = new Group($this->_adapter);

                                $groupContactSearchArray = $group->getArrayForSearchCotactsGroup($searchParam);

                                $groupContactsData = $this->getServiceLocator()->get('Group\Model\GroupTable')->getContactSearch($groupContactSearchArray);

                                foreach ($groupContactsData as $k => $grpUsers) {
                                    $users = (array) $grpUsers;
                                    if ($users['user_id'] == $userId) {
                                        $promotionUsersRecords[0]['totalApplied'] = 1;
                                        break;
                                    }else
                                        continue;
                                }
                            }
                        }
                    }
                }


                if (($getPromotionDetail['applicable_id'] == 2 || $getPromotionDetail['applicable_id'] == 3) && $promotionUsersRecords[0]['totalApplied'] == 0) { 
                    $returnData['status'] = "error";
                    $returnData['message'] = $msgArr['COUPON_NOT_VALID'];
                    return $returnData;
                }
                $promoQualifyCondition = $this->promoQualifyCondition($getPromotionDetail, $cartData);
                $isValidPromotion = $promoQualifyCondition['isValidPromotion'];
                unset($promoQualifyCondition['isValidPromotion']);

                if ($isValidPromotion == 1) { // checking for qualify filter parameters NEWPROMOCODE
                    $returnData['status'] = "success";
                    $returnData['message'] = $msgArr['COUPON_CODE_APPLIED'];
                    $returnData['promotionData'] = $getPromotionDetail;
                    $returnData['totalProductCartAmount'] = $promoQualifyCondition['totalProductCartAmount'];
                    return $returnData;
                } else { 
                    $returnData['status'] = "error";
                    $returnData['message'] = $msgArr['COUPON_NOT_VALID'];
                    return $returnData;
                }
            } else { 
                $returnData['status'] = "error";
                $returnData['message'] = $msgArr['COUPON_NOT_VALID'];
                return $returnData;
            }
            // End of Usage Limit check
        } else {
            $returnData['status'] = "error";
            $returnData['message'] = 'This promo code does not exists.';
            return $returnData;
        }
    }

    // dev 2 used checking for qualify filter parameters NEWPROMOCODE
    function promoQualifyCondition($getPromotionDetail, $cartDataRecord) {
        $bundleProductTypeArray = array();
        $bundleProductArray = array();
        $anyProductTypeArray = array();
        $anyProductArray = array();
        $excludeProductTypeArray = array();
        $excludeProductArray = array();
        $promoQualifiedArray = array();

        if (!empty($getPromotionDetail['bundle_product_type'])) { // Buldle products
            $bundleProductTypeArray = explode(',', $getPromotionDetail['bundle_product_type']);
        }
        if (!empty($getPromotionDetail['bundle_products'])) {
            $bundleProductArray = explode(',', $getPromotionDetail['bundle_products']);
        }
        if (!empty($getPromotionDetail['promotion_any_product_type'])) { // Any product
            $anyProductTypeArray = explode(',', $getPromotionDetail['promotion_any_product_type']);
        }
        if (!empty($getPromotionDetail['promotion_any_products'])) {
            $anyProductArray = explode(',', $getPromotionDetail['promotion_any_products']);
        }

        if (!empty($getPromotionDetail['excluded_product_type'])) { // Exclude product
            $excludeProductTypeArray = explode(',', $getPromotionDetail['excluded_product_type']);
        }
        if (!empty($getPromotionDetail['excluded_products'])) {
            $excludeProductArray = explode(',', $getPromotionDetail['excluded_products']);
        }

        if (!empty($getPromotionDetail['minimum_amount_product_type'])) { // Minimum amount products
            $minimumAmountProductTypeArray = explode(',', $getPromotionDetail['minimum_amount_product_type']);
        }
        if (!empty($getPromotionDetail['minimum_amount_products'])) {
            $minimumAmountProductArray = explode(',', $getPromotionDetail['minimum_amount_products']);
        }

        if (!empty($getPromotionDetail['free_products']) && $getPromotionDetail['is_free_product'] == 1) {
            $freeProductsArray = explode(',', $getPromotionDetail['free_products']);
        }

        $arrCartProductId = array();
        foreach ($cartDataRecord as $cartData) {
            $arrCartProductId[] = $cartData['product_id'];
        }
        $arrCartProductId = array_unique($arrCartProductId);

        $totalCartAmount = 0; // total cart amount with membership discount
        $totalProductCartAmount = 0;
        $isValid = 0;
        $totalMinimumProductCartAmount = 0;

        foreach ($cartDataRecord as $cartData) {
            if ($cartData['is_promotional'] == 1) {
                if (!in_array($cartData['product_type_id'], $excludeProductTypeArray) && !in_array($cartData['product_id'], $excludeProductArray)) { // exclude condition
                    if ($getPromotionDetail['promotion_qualified_by'] == 2) { // All bundle products conditions
                        sort($arrCartProductId);
                        sort($getPromotionDetail['promotionbundledbroducts']);
                        $arrComanProduct = array_intersect($arrCartProductId, $getPromotionDetail['promotionbundledbroducts']);
                        sort($arrComanProduct);
                        if ($arrComanProduct == $getPromotionDetail['promotionbundledbroducts']) {
                            $isValid = 1;
                            if (in_array($cartData['product_id'], $getPromotionDetail['promotionbundledbroducts'])) {
                                $totalProductCartAmount+= $cartData['product_subtotal'] - $cartData['membership_discount'];
                            }
                            if (in_array($cartData['product_id'], $freeProductsArray)) {
                                $totalProductCartAmount-= ($cartData['product_amount'] - $cartData['membership_indi_discount']);
                            }
                        }
                    } else if ($getPromotionDetail['promotion_qualified_by'] == 3) { // Any Product
                        if (in_array($cartData['product_type_id'], $anyProductTypeArray) || in_array($cartData['product_id'], $anyProductArray)) {
                            if (in_array($cartData['product_id'], $getPromotionDetail['promotionAnyProducts'])) {
                                $totalProductCartAmount+= $cartData['product_subtotal'] - $cartData['membership_discount'];
                            }
                            $isValid = 1;
                            if (in_array($cartData['product_id'], $freeProductsArray)) {
                                $totalProductCartAmount-= ($cartData['product_amount'] - $cartData['membership_indi_discount']);
                            }
                        }
                    } else if ($getPromotionDetail['promotion_qualified_by'] == 4) { // Global coupon
                        $isValid = 1;
                        $totalProductCartAmount+= $cartData['product_subtotal'] - $cartData['membership_discount'];
                        if (in_array($cartData['product_id'], $freeProductsArray)) {
                            $totalProductCartAmount-= ($cartData['product_amount'] - $cartData['membership_indi_discount']);
                        }
                    } else if ($getPromotionDetail['promotion_qualified_by'] == 1) { // Min amount
                        if (!empty($getPromotionDetail['promotionMinimumAmountProducts'])) {
                            if ((in_array($cartData['product_type_id'], $minimumAmountProductTypeArray) || in_array($cartData['product_id'], $minimumAmountProductArray)) && in_array($cartData['product_id'], $getPromotionDetail['promotionMinimumAmountProducts'])) {
                                $totalProductCartAmount+= ($cartData['product_amount'] - $cartData['membership_indi_discount']);
                                $isValid = 1;
                            }
                            if (in_array($cartData['product_id'], $freeProductsArray)) {
                                $totalProductCartAmount-= ($cartData['product_amount'] - $cartData['membership_indi_discount']);
                            }
                        } else {
                            $isValid = 1;
                        }
                    }
                }
                /* $totalProductCartAmount+= $cartData['product_subtotal'] - $cartData['membership_discount'];
                  if ($isValid == 1) {
                  if (in_array($cartData['product_id'], $freeProductsArray)) {
                  $totalProductCartAmount-= ($cartData['product_amount'] - $cartData['membership_indi_discount']);
                  }
                  } */
            }
        }



        if ($getPromotionDetail['promotion_qualified_by'] == 1) { // Min amount conditions
            if ($isValid == 1 && $totalProductCartAmount >= $getPromotionDetail['promotion_qualified_min_amount']) {
                $isValid = 1;
            } else {
                $isValid = 0;
            }
        }

        $promoQualifiedArray['totalProductCartAmount'] = $totalProductCartAmount;
        $promoQualifiedArray['isValidPromotion'] = $isValid;
        return $promoQualifiedArray;
    }

    // By Dev 2 used toapply the coupon code NEWPROMOCODE
    public function applyCouponDiscountInCart($getPromotionDetail, $cartData, $shipping_country, $totalCartAmount, $totalProductCartAmount, $totalShippableProductWeight) {
        $returnData = array();
        $applicationConfigIds = $this->_config['application_config_ids'];
        $uSId = $applicationConfigIds['united_state_country_id'];
        $serviceManager = $this->getServiceLocator();
        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $bundleProductTypeArray = array();
        $bundleProductArray = array();
        $anyProductTypeArray = array();
        $anyProductArray = array();
        $excludeProductTypeArray = array();
        $excludeProductArray = array();

        if (!empty($getPromotionDetail['bundle_product_type'])) { // Buldle products
            $bundleProductTypeArray = explode(',', $getPromotionDetail['bundle_product_type']);
        }
        if (!empty($getPromotionDetail['bundle_products'])) {
            $bundleProductArray = explode(',', $getPromotionDetail['bundle_products']);
        }

        if (!empty($getPromotionDetail['promotion_any_product_type'])) { // Any product
            $anyProductTypeArray = explode(',', $getPromotionDetail['promotion_any_product_type']);
        }
        if (!empty($getPromotionDetail['promotion_any_products'])) {
            $anyProductArray = explode(',', $getPromotionDetail['promotion_any_products']);
        }

        if (!empty($getPromotionDetail['excluded_product_type'])) { // Exclude product
            $excludeProductTypeArray = explode(',', $getPromotionDetail['excluded_product_type']);
        }
        if (!empty($getPromotionDetail['excluded_products'])) {
            $excludeProductArray = explode(',', $getPromotionDetail['excluded_products']);
        }

        if (!empty($getPromotionDetail['minimum_amount_product_type'])) { // Minimum amount products
            $minimumAmountProductTypeArray = explode(',', $getPromotionDetail['minimum_amount_product_type']);
        }
        if (!empty($getPromotionDetail['minimum_amount_products'])) {
            $minimumAmountProductArray = explode(',', $getPromotionDetail['minimum_amount_products']);
        }

        $arrCartProductId = array();
        $totalBundleProductAmount = 0;
        $totalMinimumAmountProductsAmount = 0;
        foreach ($cartData as $cData) {
            $arrCartProductId[] = $cData['product_id'];
            if (in_array($cData['product_id'], $getPromotionDetail['promotionbundledbroducts']))
                $totalBundleProductAmount+=$cData['product_subtotal'] - $cData['membership_discount'];

            if ($getPromotionDetail['promotion_qualified_by'] == 1) {
                if (!empty($getPromotionDetail['promotionMinimumAmountProducts']) && in_array($cData['product_id'], $getPromotionDetail['promotionMinimumAmountProducts'])) {
                    if ((in_array($cData['product_type_id'], $minimumAmountProductTypeArray)) || (in_array($cData['product_id'], $minimumAmountProductArray)))
                        $totalMinimumAmountProductsAmount+=$cData['product_subtotal'] - $cData['membership_discount'];
                }
            }elseif ($getPromotionDetail['promotion_qualified_by'] == 3) {
                if (!empty($getPromotionDetail['promotionAnyProducts'])) {
                    if ((in_array($cData['product_type_id'], $anyProductTypeArray)) || (in_array($cData['product_id'], $anyProductArray)))
                        $totalAnyProductsAmount+=$cData['product_subtotal'] - $cData['membership_discount'];
                }
            }
        }

        $arrCartProductId = array_unique($arrCartProductId);

        $z = 0;
        $totalProductTax = 0;
        $totalMembershipDiscount = 0;

        foreach ($cartData as $index => $item) {
            if ($item['is_promotional'] == 1 && (!in_array($item['product_type_id'], $excludeProductTypeArray)) && (!in_array($item['product_id'], $excludeProductArray))) {
                if ($getPromotionDetail['promotion_qualified_by'] == 4) {
                    // global
                    // Shipping Discount
                    if ($getPromotionDetail['is_shipping_discount'] == 1) {

                        if ($getPromotionDetail['shipping_discount_type'] == 1) {
                            $totalShippableProductWeight-= ($item['product_weight'] * $item['product_qty']);
                        }
                    }
                } elseif ($getPromotionDetail['promotion_qualified_by'] == 1) {
                    // min product cart amount

                    if (!empty($getPromotionDetail['promotionMinimumAmountProducts'])) {

                        if (in_array($item['product_type_id'], $minimumAmountProductTypeArray) || in_array($item['product_id'], $minimumAmountProductArray)) {

                            // Shipping Discount
                            if ($getPromotionDetail['is_shipping_discount'] == 1) {

                                if ($getPromotionDetail['shipping_discount_type'] == 1) {
                                    $totalShippableProductWeight-= ($item['product_weight'] * $item['product_qty']);
                                }
                            }
                            // end of shippin discount                        
                        }
                    } else {
                        if ($totalProductCartAmount >= $getPromotionDetail['promotion_qualified_min_amount']) {
                            // Shipping Discount
                            if ($getPromotionDetail['is_shipping_discount'] == 1) {

                                if ($getPromotionDetail['shipping_discount_type'] == 1) {
                                    $totalShippableProductWeight-= ($item['product_weight'] * $item['product_qty']);
                                }
                            }
                            // end of shippin discount							
                        }
                    }
                } elseif ($getPromotionDetail['promotion_qualified_by'] == 2) {
                    // all bundled products
                    sort($arrCartProductId);
                    sort($getPromotionDetail['promotionbundledbroducts']);
                    $arrComanProduct = array_intersect($arrCartProductId, $getPromotionDetail['promotionbundledbroducts']);
                    sort($arrComanProduct);
                    if ($arrComanProduct == $getPromotionDetail['promotionbundledbroducts']) {
                        // Shipping Discount
                        if ($getPromotionDetail['is_shipping_discount'] == 1 && in_array($item['product_id'], $getPromotionDetail['promotionbundledbroducts'])) {

                            if ($getPromotionDetail['shipping_discount_type'] == 1) {
                                $totalShippableProductWeight-= ($item['product_weight'] * $item['product_qty']);
                            }
                        }
                        // end of shippin discount                        
                    }
                } elseif ($getPromotionDetail['promotion_qualified_by'] == 3) {
                    //any product in list
                    if ((in_array($item['product_type_id'], $anyProductTypeArray) || in_array($item['product_id'], $anyProductArray)) && in_array($item['product_id'], $getPromotionDetail['promotionAnyProducts'])) {
                        // Shipping Discount
                        if ($getPromotionDetail['is_shipping_discount'] == 1) {

                            if ($getPromotionDetail['shipping_discount_type'] == 1) {
                                $totalShippableProductWeight-= ($item['product_weight'] * $item['product_qty']);
                            }
                        }
                        // end of shippin discount                        
                    }
                }
				// @$itemToDiscount: array of free promotional products added to cart
				if ($getPromotionDetail['is_free_product'] == 1) {
                        $freeProductsArray = explode(',', $getPromotionDetail['free_products']);
						if (in_array($item['product_id'], $freeProductsArray)) {							
								$itemToDiscount[$item['product_id']]['itemQty'][] = $item['product_qty'];
								$itemToDiscount[$item['product_id']]['itemTotalAmount'][] = $item['product_subtotal'] - $item['membership_discount'];
						}
				}
            }
        }
		$freeDiscountedItems = array();
        foreach ($cartData as $key => $val) {
            if ($val['is_promotional'] == 1 && (!in_array($val['product_type_id'], $excludeProductTypeArray)) && (!in_array($val['product_id'], $excludeProductArray))) {
                if ($getPromotionDetail['promotion_qualified_by'] == 4) {
                    // global
                    // Shipping Discount
                    if ($getPromotionDetail['is_shipping_discount'] == 1) {

                        if ($getPromotionDetail['shipping_discount_type'] == 1) {
                            $cartData[$key]['is_free_shipping'] = 1;
                            $cartData[$key]['shipping_discount_amount'] = 0;
                        } elseif ($getPromotionDetail['shipping_discount_type'] == 2) {
                            if ($val['is_free_shipping'] == 0 && $val['is_shippable'] == 1 && $val['shipping_cost'] == 1) {
                                $shipping_discount_amount = $this->calculateProductShippingDiscount(($val['product_weight'] * $val['product_qty']), $totalShippableProductWeight, $getPromotionDetail['free_shipping_amount']);
                                $cartData[$key]['is_free_shipping'] = 0;
                                $cartData[$key]['shipping_discount_amount'] = $shipping_discount_amount;
                            }
                        }
                    }
                    // end of shippin discount
                    // cart discount
                    if ($getPromotionDetail['is_cart_discount'] == 1) {
                        if ($getPromotionDetail['cart_discount_type'] == 1) { // percent
                            $cartDiscountAmountOff = ($totalCartAmount * $getPromotionDetail['cart_discount_percent']) / 100;
                        } elseif ($getPromotionDetail['cart_discount_type'] == 2) { // amount off
                            $cartDiscountAmountOff = $getPromotionDetail['cart_discount_amount'];
                        }

                        $productDiscount = $this->calculateProductCartDiscount($val['product_subtotal'] - $val['membership_discount'], $totalProductCartAmount, $cartDiscountAmountOff);

                        if($productDiscount>0){
							if($productDiscount<($val['product_subtotal'] - $val['membership_discount'])){
								$cartData[$key]['product_discount'] = $productDiscount;								
							}else
								$cartData[$key]['product_discount'] = $val['product_subtotal'] - $val['membership_discount'];
						}else
							$cartData[$key]['product_discount'] = '0.00';
                    }
                    // end of cart discount
                    // free products
                    if ($getPromotionDetail['is_free_product'] == 1) {
                        $freeProductsArray = explode(',', $getPromotionDetail['free_products']);
                        if (in_array($val['product_id'], $freeProductsArray) && !in_array($val['product_id'],$freeDiscountedItems)) {
                            $totalItemQty = array_sum($itemToDiscount[$val['product_id']]['itemQty']);
							$totalItemAmount = array_sum($itemToDiscount[$val['product_id']]['itemTotalAmount']);
                            $productDiscount = $this->calculateFreeProductDiscount($val, $itemToDiscount);
                            if($productDiscount>0){
								if($productDiscount<($val['product_subtotal'] - $val['membership_discount'])){
									$cartData[$key]['product_discount'] = $productDiscount;								
								}else
									$cartData[$key]['product_discount'] = $val['product_subtotal'] - $val['membership_discount'];
								$freeDiscountedItems[] = $val['product_id'];
							}else
								$cartData[$key]['product_discount'] = '0.00';
							$cartData[$key]['is_taxable'] = 0;
                        }
                    }
                    // end of free products discount
                } elseif ($getPromotionDetail['promotion_qualified_by'] == 1) {
                    // min product cart amount

                    if (!empty($getPromotionDetail['promotionMinimumAmountProducts'])) {

                        sort($arrCartProductId);
                        sort($getPromotionDetail['promotionMinimumAmountProducts']);
                        $arrComanProduct = array_intersect($arrCartProductId, $getPromotionDetail['promotionMinimumAmountProducts']);
                        sort($arrComanProduct);

                        if ($totalProductCartAmount >= $getPromotionDetail['promotion_qualified_min_amount']) {
                            // Shipping Discount
                            if ($getPromotionDetail['is_shipping_discount'] == 1 && ((in_array($val['product_type_id'], $minimumAmountProductTypeArray)) || (in_array($val['product_id'], $minimumAmountProductArray)))) {

                                if ($getPromotionDetail['shipping_discount_type'] == 1) {
                                    $cartData[$key]['is_free_shipping'] = 1;
                                    $cartData[$key]['shipping_discount_amount'] = 0;
                                } elseif ($getPromotionDetail['shipping_discount_type'] == 2) {
                                    if ($val['is_free_shipping'] == 0 && $val['is_shippable'] == 1 && $val['shipping_cost'] == 1) {
                                        $shipping_discount_amount = $this->calculateProductShippingDiscount(($val['product_weight'] * $val['product_qty']), $totalShippableProductWeight, $getPromotionDetail['free_shipping_amount']);
                                        $cartData[$key]['is_free_shipping'] = 0;
                                        $cartData[$key]['shipping_discount_amount'] = $shipping_discount_amount;
                                    }
                                }
                            }
                            // end of shippin discount
                            // cart discount

                            if ($getPromotionDetail['is_cart_discount'] == 1 && ((in_array($val['product_type_id'], $minimumAmountProductTypeArray)) || (in_array($val['product_id'], $minimumAmountProductArray)))) {
                                if ($getPromotionDetail['cart_discount_type'] == 1) { // percent
                                    $cartDiscountAmountOff = ($totalMinimumAmountProductsAmount * $getPromotionDetail['cart_discount_percent']) / 100;
                                } elseif ($getPromotionDetail['cart_discount_type'] == 2) { // amount off
                                    $cartDiscountAmountOff = $getPromotionDetail['cart_discount_amount'];
                                }

                                $productDiscount = $this->calculateProductCartDiscount($val['product_subtotal'] - $val['membership_discount'], $totalProductCartAmount, $cartDiscountAmountOff);

                                if($productDiscount>0){
									if($productDiscount<($val['product_subtotal'] - $val['membership_discount'])){
										$cartData[$key]['product_discount'] = $productDiscount;								
									}else
										$cartData[$key]['product_discount'] = $val['product_subtotal'] - $val['membership_discount'];
								}else
									$cartData[$key]['product_discount'] = '0.00';
                            }
                            // end of cart discount
                            // free products
                            if ($getPromotionDetail['is_free_product'] == 1 && ((in_array($val['product_type_id'], $minimumAmountProductTypeArray)) || (in_array($val['product_id'], $minimumAmountProductArray)))) {
                                $freeProductsArray = explode(',', $getPromotionDetail['free_products']);
                                if (in_array($val['product_id'], $freeProductsArray) && !in_array($val['product_id'],$freeDiscountedItems)) {
                                    $totalItemQty = array_sum($itemToDiscount[$val['product_id']]['itemQty']);
									$totalItemAmount = array_sum($itemToDiscount[$val['product_id']]['itemTotalAmount']);
									$productDiscount = $this->calculateFreeProductDiscount($val, $itemToDiscount);
									if($productDiscount>0){
										if($productDiscount<($val['product_subtotal'] - $val['membership_discount'])){
											$cartData[$key]['product_discount'] = $productDiscount;								
										}else
											$cartData[$key]['product_discount'] = $val['product_subtotal'] - $val['membership_discount'];
										$freeDiscountedItems[] = $val['product_id'];
									}else
										$cartData[$key]['product_discount'] = '0.00';
									$cartData[$key]['is_taxable'] = 0;
                                }
                            }
                            // end of free products discount
                        }
                    } else {
                        if ($totalProductCartAmount >= $getPromotionDetail['promotion_qualified_min_amount']) {
                            // Shipping Discount
                            if ($getPromotionDetail['is_shipping_discount'] == 1) {

                                if ($getPromotionDetail['shipping_discount_type'] == 1) {
                                    $cartData[$key]['is_free_shipping'] = 1;
                                    $cartData[$key]['shipping_discount_amount'] = 0;
                                } elseif ($getPromotionDetail['shipping_discount_type'] == 2) {
                                    if ($val['is_free_shipping'] == 0 && $val['is_shippable'] == 1 && $val['shipping_cost'] == 1) {
                                        $shipping_discount_amount = $this->calculateProductShippingDiscount(($val['product_weight'] * $val['product_qty']), $totalShippableProductWeight, $getPromotionDetail['free_shipping_amount']);
                                        $cartData[$key]['is_free_shipping'] = 0;
                                        $cartData[$key]['shipping_discount_amount'] = $shipping_discount_amount;
                                    }
                                }
                            }
                            // end of shippin discount
                            // cart discount
                            if ($getPromotionDetail['is_cart_discount'] == 1) {
                                if ($getPromotionDetail['cart_discount_type'] == 1) { // percent
                                    $cartDiscountAmountOff = ($totalCartAmount * $getPromotionDetail['cart_discount_percent']) / 100;
                                } elseif ($getPromotionDetail['cart_discount_type'] == 2) { // amount off
                                    $cartDiscountAmountOff = $getPromotionDetail['cart_discount_amount'];
                                }


                                $productDiscount = $this->calculateProductCartDiscount($val['product_subtotal'] - $val['membership_discount'], $totalProductCartAmount, $cartDiscountAmountOff);

                                if($productDiscount>0){
									if($productDiscount<($val['product_subtotal'] - $val['membership_discount'])){
										$cartData[$key]['product_discount'] = $productDiscount;								
									}else
										$cartData[$key]['product_discount'] = $val['product_subtotal'] - $val['membership_discount'];
								}else
									$cartData[$key]['product_discount'] = '0.00';
                            }
                            // end of cart discount
                            // free products
                            if ($getPromotionDetail['is_free_product'] == 1) {
                                $freeProductsArray = explode(',', $getPromotionDetail['free_products']);
                                if (in_array($val['product_id'], $freeProductsArray) && !in_array($val['product_id'],$freeDiscountedItems)) {
                                    $totalItemQty = array_sum($itemToDiscount[$val['product_id']]['itemQty']);
									$totalItemAmount = array_sum($itemToDiscount[$val['product_id']]['itemTotalAmount']);
									$productDiscount = $this->calculateFreeProductDiscount($val, $itemToDiscount);
									if($productDiscount>0){
										if($productDiscount<($val['product_subtotal'] - $val['membership_discount'])){
											$cartData[$key]['product_discount'] = $productDiscount;								
										}else
											$cartData[$key]['product_discount'] = $val['product_subtotal'] - $val['membership_discount'];
										$freeDiscountedItems[] = $val['product_id'];
									}else
										$cartData[$key]['product_discount'] = '0.00';
									$cartData[$key]['is_taxable'] = 0;
                                }
                            }
                            // end of free products discount
                        }
                    }
                } elseif ($getPromotionDetail['promotion_qualified_by'] == 2) {
                    // all bundled products
                    sort($arrCartProductId);
                    sort($getPromotionDetail['promotionbundledbroducts']);
                    $arrComanProduct = array_intersect($arrCartProductId, $getPromotionDetail['promotionbundledbroducts']);
                    sort($arrComanProduct);
                    if ($arrComanProduct == $getPromotionDetail['promotionbundledbroducts']) {
                        // Shipping Discount
                        if ($getPromotionDetail['is_shipping_discount'] == 1 && in_array($val['product_id'], $getPromotionDetail['promotionbundledbroducts'])) {

                            if ($getPromotionDetail['shipping_discount_type'] == 1) {
                                $cartData[$key]['is_free_shipping'] = 1;
                                $cartData[$key]['shipping_discount_amount'] = 0;
                            } elseif ($getPromotionDetail['shipping_discount_type'] == 2) {
                                if ($val['is_free_shipping'] == 0 && $val['is_shippable'] == 1 && $val['shipping_cost'] == 1) {
                                    $shipping_discount_amount = $this->calculateProductShippingDiscount(($val['product_weight'] * $val['product_qty']), $totalShippableProductWeight, $getPromotionDetail['free_shipping_amount']);
                                    $cartData[$key]['is_free_shipping'] = 0;
                                    $cartData[$key]['shipping_discount_amount'] = $shipping_discount_amount;
                                }
                            }
                        }
                        // end of shippin discount
                        // cart discount

                        if ($getPromotionDetail['is_cart_discount'] == 1 && in_array($val['product_id'], $getPromotionDetail['promotionbundledbroducts'])) {
                            if ($getPromotionDetail['cart_discount_type'] == 1) { // percent
                                $cartDiscountAmountOff = ($totalProductCartAmount * $getPromotionDetail['cart_discount_percent']) / 100;
                            } elseif ($getPromotionDetail['cart_discount_type'] == 2) { // amount off
                                $cartDiscountAmountOff = $getPromotionDetail['cart_discount_amount'];
                            }

                            $productDiscount = $this->calculateProductCartDiscount($val['product_subtotal'] - $val['membership_discount'], $totalProductCartAmount, $cartDiscountAmountOff);

                            if($productDiscount>0){
								if($productDiscount<($val['product_subtotal'] - $val['membership_discount'])){
									$cartData[$key]['product_discount'] = $productDiscount;								
								}else
									$cartData[$key]['product_discount'] = $val['product_subtotal'] - $val['membership_discount'];
							}else
								$cartData[$key]['product_discount'] = '0.00';
                        }
                        // end of cart discount
                        // free products
                        if ($getPromotionDetail['is_free_product'] == 1 && in_array($val['product_id'], $getPromotionDetail['promotionbundledbroducts'])) {
                            $freeProductsArray = explode(',', $getPromotionDetail['free_products']);
                            if (in_array($val['product_id'], $freeProductsArray) && !in_array($val['product_id'],$freeDiscountedItems)) {
                                $totalItemQty = array_sum($itemToDiscount[$val['product_id']]['itemQty']);
								$totalItemAmount = array_sum($itemToDiscount[$val['product_id']]['itemTotalAmount']);
								$productDiscount = $this->calculateFreeProductDiscount($val, $itemToDiscount);
                                if($productDiscount>0){
									if($productDiscount<($val['product_subtotal'] - $val['membership_discount'])){
										$cartData[$key]['product_discount'] = $productDiscount;								
									}else
										$cartData[$key]['product_discount'] = $val['product_subtotal'] - $val['membership_discount'];
									$freeDiscountedItems[] = $val['product_id'];
								}else
									$cartData[$key]['product_discount'] = '0.00';
								$cartData[$key]['is_taxable'] = 0;
                            }
                        }
                        // end of free products discount
                    }
                } elseif ($getPromotionDetail['promotion_qualified_by'] == 3) {
                    //any product in list
                    if ((in_array($val['product_type_id'], $anyProductTypeArray) || in_array($val['product_id'], $anyProductArray)) && in_array($val['product_id'], $getPromotionDetail['promotionAnyProducts'])) {
                        // Shipping Discount
                        if ($getPromotionDetail['is_shipping_discount'] == 1) {

                            if ($getPromotionDetail['shipping_discount_type'] == 1) {
                                $cartData[$key]['is_free_shipping'] = 1;
                                $cartData[$key]['shipping_discount_amount'] = 0;
                            } elseif ($getPromotionDetail['shipping_discount_type'] == 2) {
                                if ($val['is_free_shipping'] == 0 && $val['is_shippable'] == 1 && $val['shipping_cost'] == 1) {
                                    $shipping_discount_amount = $this->calculateProductShippingDiscount(($val['product_weight'] * $val['product_qty']), $totalShippableProductWeight, $getPromotionDetail['free_shipping_amount']);
                                    $cartData[$key]['is_free_shipping'] = 0;
                                    $cartData[$key]['shipping_discount_amount'] = $shipping_discount_amount;
                                }
                            }
                        }
                        // end of shippin discount
                        // cart discount
                        if ($getPromotionDetail['is_cart_discount'] == 1) {
                            if ($getPromotionDetail['cart_discount_type'] == 1) { // percent
                                $cartDiscountAmountOff = ($totalProductCartAmount * $getPromotionDetail['cart_discount_percent']) / 100;
                            } elseif ($getPromotionDetail['cart_discount_type'] == 2) { // amount off
                                $cartDiscountAmountOff = $getPromotionDetail['cart_discount_amount'];
                            }

                            $productDiscount = $this->calculateProductCartDiscount($val['product_subtotal'] - $val['membership_discount'], $totalProductCartAmount, $cartDiscountAmountOff);

                            if($productDiscount>0){
								if($productDiscount<($val['product_subtotal'] - $val['membership_discount'])){
									$cartData[$key]['product_discount'] = $productDiscount;								
								}else
									$cartData[$key]['product_discount'] = $val['product_subtotal'] - $val['membership_discount'];
							}else
								$cartData[$key]['product_discount'] = '0.00';
                        }
                        // end of cart discount
                        // free products
                        if ($getPromotionDetail['is_free_product'] == 1) {
                            $freeProductsArray = explode(',', $getPromotionDetail['free_products']);
                            if (in_array($val['product_id'], $freeProductsArray) && !in_array($val['product_id'],$freeDiscountedItems)) {
                                $totalItemQty = array_sum($itemToDiscount[$val['product_id']]['itemQty']);
								$totalItemAmount = array_sum($itemToDiscount[$val['product_id']]['itemTotalAmount']);
								$productDiscount = $this->calculateFreeProductDiscount($val, $itemToDiscount);
                                if($productDiscount>0){
									if($productDiscount<($val['product_subtotal'] - $val['membership_discount'])){
										$cartData[$key]['product_discount'] = $productDiscount;								
									}else
										$cartData[$key]['product_discount'] = $val['product_subtotal'] - $val['membership_discount'];
									$freeDiscountedItems[] = $val['product_id'];
								}else
									$cartData[$key]['product_discount'] = '0.00';
								$cartData[$key]['is_taxable'] = 0;
                            }
                        }
                        // end of free products discount
                    }
                }
            } else {
                $cartData[$key]['shipping_discount_amount'] = '0.00';
            }
        }
        return $cartData;
    }

    /**
     * This action is used to check whether coupon is valid or not
     * @param void
     * @return void
     * @author Icreon Tech - DT
     */
    public function isCouponValid($promotionId, $userId, $sourceType = 'backend', $shipping_country = '0') {
        $returnData = array();
        $applicationConfigIds = $this->_config['application_config_ids'];
        $uSId = $applicationConfigIds['united_state_country_id'];
        $promotionDetailArr = array();
        $promotionDetailArr[] = $promotionId;
        $serviceManager = $this->getServiceLocator();
        $promotionTable = $serviceManager->get('Promotions\Model\PromotionTable');
        $getPromotionDetail = $promotionTable->getPromotionById($promotionDetailArr);

        $cartParam['user_id'] = $userId;
        $cartParam['source_type'] = !empty($sourceType) ? $sourceType : 'frontend';
        $cartProductArr = $this->getTransactionTable()->getCartProducts($cartParam);


        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];


        if (($getPromotionDetail['expiry_date'] != '' && $getPromotionDetail['expiry_date'] != 0 && substr(DATE_TIME_FORMAT, 0, 10) > substr($getPromotionDetail['expiry_date'], 0, 10)) || ($getPromotionDetail['start_date'] != '' && $getPromotionDetail['start_date'] != 0 && substr(DATE_TIME_FORMAT, 0, 10) < substr($getPromotionDetail['start_date'], 0, 10))) {
            $returnData['status'] = "error";
            $returnData['message'] = $msgArr['COUPON_CODE_EXPIRED'];
            $returnData['cartArray'] = $cartProductArr;
        } else if ($getPromotionDetail['usage_limit'] < 1) {
            $returnData['status'] = "error";
            $returnData['message'] = $msgArr['COUPON_CODE_REACH_LIMIT'];
            $returnData['cartArray'] = $cartProductArr;
        } else {
            $paramsArr = array();
            $paramsArr['user_id'] = $userId;
            $paramsArr['source_type'] = $sourceType;
            $paramsArr['promotionData'] = $getPromotionDetail;
            $totalDiscount = $this->getCouponDiscountForProductInCart($paramsArr, $shipping_country);
            $returnData['cartArray'] = $totalDiscount['cartArray'];

            if ($totalDiscount['apply'] == 0) {
                $returnData['status'] = "error";
                $returnData['message'] = $msgArr['COUPON_NOT_VALID_FOR_ANY_PRODUCT'];
            } else if ($totalDiscount['discount'] == 0 && $getPromotionDetail['is_free_shipping'] == 0) {
                $returnData['status'] = "error";
                $returnData['message'] = $msgArr['COUPON_NOT_VALID_FOR_ANY_PRODUCT'];
            } else {
                if (isset($getPromotionDetail['applicable_to'])) {
                    switch ($getPromotionDetail['applicable_to']) {
                        case '0' :
                        case '3' : $userUsedCouponArr = array();
                            $userUsedCouponArr['promotionId'] = $promotionId;
                            $userUsedCouponArr['userId'] = $userId;
                            $isUserUsedCoupon = $promotionTable->checkUserUsedCoupon($userUsedCouponArr);
                            $returnData['status'] = "success";
                            $returnData['message'] = $msgArr['COUPON_CODE_APPLIED'];
                            /* if (empty($isUserUsedCoupon)) {
                              $returnData['status'] = "success";
                              $returnData['message'] = $msgArr['COUPON_CODE_APPLIED'];
                              } else {
                              $returnData['status'] = "error";
                              $returnData['message'] = $msgArr['ALREADY_USED_COUPON'];
                              } */
                            break;
                        case '1' : if ($shipping_country != $uSId) {
                                $returnData['status'] = "error";
                                $returnData['message'] = $msgArr['COUPON_NOT_VALID'];
                            } else {
                                $userUsedCouponArr = array();
                                $userUsedCouponArr['promotionId'] = $promotionId;
                                $userUsedCouponArr['userId'] = $userId;
                                $isUserUsedCoupon = $promotionTable->checkUserUsedCoupon($userUsedCouponArr);
                                $returnData['status'] = "success";
                                $returnData['message'] = $msgArr['COUPON_CODE_APPLIED'];
                                /*
                                  if (empty($isUserUsedCoupon)) {
                                  $returnData['status'] = "success";
                                  $returnData['message'] = $msgArr['COUPON_CODE_APPLIED'];
                                  } else {
                                  $returnData['status'] = "error";
                                  $returnData['message'] = $msgArr['ALREADY_USED_COUPON'];
                                  } */
                            }
                            break;
                        case '2' :
                            if ($shipping_country == $uSId) {
                                $returnData['status'] = "error";
                                $returnData['message'] = $msgArr['COUPON_NOT_VALID'];
                            } else {
                                $userUsedCouponArr = array();
                                $userUsedCouponArr['promotionId'] = $promotionId;
                                $userUsedCouponArr['userId'] = $userId;
                                $isUserUsedCoupon = $promotionTable->checkUserUsedCoupon($userUsedCouponArr);

                                $returnData['status'] = "success";
                                $returnData['message'] = $msgArr['COUPON_CODE_APPLIED'];

                                /* if (empty($isUserUsedCoupon)) {
                                  $returnData['status'] = "success";
                                  $returnData['message'] = $msgArr['COUPON_CODE_APPLIED'];
                                  } else {
                                  $returnData['status'] = "error";
                                  $returnData['message'] = $msgArr['ALREADY_USED_COUPON'];
                                  } */
                            }
                            break;
                    }
                }
            }
        }
        $returnData['promotionData'] = $getPromotionDetail;


        return $returnData;
    }


    public function appendQuestionMars($count) {
        $returnstr = '';
        for ($i = 1; $i <= $count; $i++) {
            $returnstr.='?,';
        }
        return rtrim($returnstr, ',');
    }

    /**
     * This action is used to save transaction from cart
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function saveFinalTransaction($postArr) { // by dev 3

	    $pos = new PosManagement($this->_adapter);
        $response = $this->getResponse();
        $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter')->getDriver()->getConnection()->beginTransaction();
        $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->offAutoCommit();
        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $postArr['shipping_method'] = (!empty($postArr['shipping_method'])) ? $postArr['shipping_method'] : '';
        $postArr['is_free_shipping'] = '0';
        $responseArr = array();
        $cartProductArr = array();
        $cartProductArr['source_type'] = 'frontend';
        $isFreeShipping = false;
        $postArr['is_free_shipping'] = '0';
        $freeShippingAmount = '';
        $transaction = new Transaction($this->_adapter);
        $this->campaignSession = new Container('campaign');
        $posTransactionSession = new Container('posTransactionSession'); /* pos session */
        
        $cartProductData = $posTransactionSession->posTransactionData['transaction_details'];
        $postArr['promotion_id'] = $cartProductData['promotionData']['promotion_id'];
        if (isset($cartProductData['cartData'])) {
            $cartProducts = $cartProductData['cartData'];
        }
        $responseArr['status'] = 'error';
        if (!empty($cartProducts)) {
            $totalSubtotalAmount = 0;
            $totalDiscountAmount = 0;
            $totalTaxableAmount = 0;
            $totalCartAmount = 0;
            $totalShippingAmount = 0;
            $totalShippingHandlingAmount = 0;
            $pledgeDonationAmount = 0;
            $shipCompleteStatus = 0;
            $userId = 0;
            $i = 0;
            $donationTotalAmount = 0;
            $unshipProductTotalAmount = 0;
            $arrUnshipProductTotalAmount = array();
            $arrAllProductTotalAmount = array();
            $transactionTypeDonation = '';
            $transactionTypePurchase = '';
            $productQtyErrorArr = array();
            $totalProductsInCart = count($cartProducts);
            $isSaveShippingAddress = 0;
            $totalCashAmount = 0;
            $totalCheckAmount = 0;
            $totalCreditAmount = 0;
            $shippableProductsAmount = 0;
            $transactionStatus = 1;
            $countProStatus = 0;
            $totalProductRevenue = 0;
            $donAmt = 0;
            $paymentArray = array();
			$totalShippingPrice = 0;

		##################################################################################################################################
            $sendThankyouMail = 0;
            foreach ($cartProducts as $cartKey => $cartProduct) {
                $productLabel = '';
                $totalSubtotalAmount+= $cartProduct['product_subtotal'] - $cartProduct['membership_discount'];
                $totalDiscountAmount+= $cartProduct['product_discount'];
                $totalTaxableAmount+= $cartProduct['product_taxable_amount'];
                $totalCartAmount+=$cartProduct['product_total'];
                $totalProductRevenue+=$cartProduct['total_product_revenue'];
                if (isset($cartProduct['shipping_discount_amount']))
                    $totalShippingAmount+=($cartProduct['shipping_price'] - $cartProduct['shipping_discount_amount']);
                elseif(isset($cartProduct['shipping_discount_in_amount']))
                    $totalShippingAmount+=($cartProduct['shipping_price'] - $cartProduct['shipping_discount_in_amount']);
				else
                    $totalShippingAmount+=$cartProduct['shipping_price'];

                $totalShippingHandlingAmount+=$cartProduct['surcharge_price'] + $cartProduct['shipping_handling_price'];

				$totalShippingPrice+= $totalShippingAmount+$totalShippingHandlingAmount;
                $productTotal = $cartProduct['product_total'];

                if ($cartProduct['product_type_id'] == '2') {
                    $pledgeDonationAmount+=$cartProduct['product_total'];
                    $sendThankyouMail = 1;
                }
                if ($cartProduct['product_transaction_type'] == '1') {
                    $transactionTypeDonation = '1';
                } else {
                    $transactionTypePurchase = '2';
                }

                if ($cartProduct['product_type_id'] != '4') {
                    if ($cartProduct['is_shippable'] == '0' || empty($cartProduct['is_shippable'])) {
                        $shipCompleteStatus++;
                        $unshipProductTotalAmount = $unshipProductTotalAmount + $productTotal;
                        //$arrUnshipProductTotalAmount['unshipped_product_amount'][] = $productTotal;
                        $arrUnshipProductTotalAmount['product_type_id'][] = $cartProduct['product_type_id'];
                        $paymentArray[$cartKey]['cart_product_amount'] = round($productTotal, 2);
                        $paymentArray[$cartKey]['cart_product_type_id'] = $cartProduct['product_type_id'];
                        //$paymentArray[$cartKey]['unshipped_product_amount'] = round($productTotal, 2);
                        $paymentArray[$cartKey]['product_type_id'] = $cartProduct['product_type_id'];
                        $paymentArray[$cartKey]['payment_recieve'] = '1';
                    } else {
                        $shippableProductsAmount+=$shippableProductsAmount + ($cartProduct['product_subtotal'] - $cartProduct['membership_discount']);
                        $isSaveShippingAddress = 1;
                        $paymentArray[$cartKey]['cart_product_amount'] = round($productTotal, 2);
                        $paymentArray[$cartKey]['cart_product_type_id'] = $cartProduct['product_type_id'];
                        //$paymentArray[$cartKey]['unshipped_product_amount'] = round($productTotal, 2);
                        $paymentArray[$cartKey]['product_type_id'] = $cartProduct['product_type_id'];
                        $paymentArray[$cartKey]['payment_recieve'] = '2';
                    }
                } else {
                    $paymentArray[$cartKey]['cart_product_amount'] = round($productTotal, 2);
                    $paymentArray[$cartKey]['cart_product_type_id'] = $cartProduct['product_type_id'];
                    //$paymentArray[$cartKey]['unshipped_product_amount'] = round($productTotal, 2);
                    $paymentArray[$cartKey]['product_type_id'] = $cartProduct['product_type_id'];
                    $paymentArray[$cartKey]['payment_recieve'] = '2';
                }
                if ($cartProduct['product_mapping_id'] == '1' || $cartProduct['product_mapping_id'] == '3' || $cartProduct['product_mapping_id'] == '5') {
                    $transactionStatus = 9;
                    $countProStatus++;
                }
                if ($cartProduct['product_transaction_type'] == 1) {
                    $donAmt = $donAmt + ($cartProduct['product_subtotal'] - $cartProduct['membership_discount']);
                }

                if ($cartProduct['product_type_id'] == 1 && $cartProduct['product_qty'] > $cartProduct['total_in_stock']) {
                    $productQtyErrorArr['message'] = sprintf($msgArr['QUANTITY_EXCEED'], $cartProduct['total_in_stock'], $cartProduct['product_name']);
                }
                $i++;
            }

            $userId = $postArr['user_id'];
            if (!empty($productQtyErrorArr)) {
                $productQtyErrorArr['quantity_error'] = 1;
                $productQtyErrorArr['status'] = 'quantity_error';
                return $productQtyErrorArr; //vvvvvvvvvvvvvvvv
            }

            $transactionType = trim($transactionTypeDonation . ',' . $transactionTypePurchase, ',');

            if ($countProStatus == count($cartProducts)) {
                $transactionStatus = '10';
            }
            /* if ($transactionType == '1') {
              $transactionStatus = 1;
              } else if ($transactionType == '2') {
              $transactionStatus = 1;
              } else {
              $transactionStatus = 9;
              } */


            /* Add transaction record */
            
            $finalTransaction = $postArr;
            $dateTime = DATE_TIME_FORMAT;

            $crm_user_id = '';
            //$transactionSourceId = $this->_config['transaction_source']['transaction_source_id'];
            $finalTransaction['transaction_source_id'] = $transactionSourceId = '7'; // Ellis WOH
            $finalTransaction['num_items'] = $totalProductsInCart;
            $finalTransaction['sub_total_amount'] = $totalSubtotalAmount;
            $finalTransaction['total_discount'] = $totalDiscountAmount;
            $finalTransaction['shipping_amount'] = ($totalShippingPrice<0)?0.00:$totalShippingAmount;
            $finalTransaction['handling_amount'] = ($totalShippingPrice<0)?0.00:$totalShippingHandlingAmount;
            $finalTransaction['total_tax'] = $totalTaxableAmount;
            //$finalTransaction['transaction_amount'] = $totalCartAmount;
			$finalTransaction['transaction_amount'] = ($totalSubtotalAmount-$totalDiscountAmount)+($finalTransaction['shipping_amount']+$finalTransaction['handling_amount']+$totalTaxableAmount);
            $finalTransaction['transaction_date'] = $dateTime;
            $finalTransaction['transaction_type'] = $transactionType;
            $finalTransaction['transaction_status_id'] = $transactionStatus;
            $finalTransaction['added_by'] = $crm_user_id;
            $finalTransaction['added_date'] = $dateTime;
            $finalTransaction['modify_by'] = $crm_user_id;
            $finalTransaction['modify_date'] = $dateTime;
            $finalTransaction['campaign_code'] = '';
            $finalTransaction['campaign_id'] = '';
            $finalTransaction['channel_id'] = '';
            $finalTransaction['don_amount'] = $donAmt;
            $finalTransaction['total_product_revenue'] = $totalProductRevenue;
            if (isset($postArr['payment_mode_invoice']) && $postArr['payment_mode_invoice'] == 1) {
                $finalTransaction['transaction_status_id'] = 10;
            }

            /* General Campaign Code */
            $transactionChannelId = 5; // Web

            if (!isset($postArr['campaign_code_id']) || empty($postArr['campaign_code_id'])) {
                $generalCampaign = $this->_transactionTable->getGeneralCampaign();

                if (isset($generalCampaign) && !empty($generalCampaign)) {
                    $finalTransaction['campaign_code'] = $generalCampaign[0]['campaign_code'];
                    $finalTransaction['campaign_id'] = $generalCampaign[0]['campaign_id'];
                    $finalTransaction['channel_id'] = $transactionChannelId;
                }


                $finalTransaction['campaign_code'] = (isset($this->campaignSession->campaign_code) && !empty($this->campaignSession->campaign_code) ? $this->campaignSession->campaign_code : $finalTransaction['campaign_code']);
                $finalTransaction['campaign_id'] = (isset($this->campaignSession->campaign_id) && !empty($this->campaignSession->campaign_id) ? $this->campaignSession->campaign_id : $finalTransaction['campaign_id']);
                $finalTransaction['channel_id'] = (isset($this->campaignSession->channel_id) && !empty($this->campaignSession->channel_id) ? $this->campaignSession->channel_id : $finalTransaction['channel_id']);
            } else {
                $finalTransaction['campaign_code'] = (isset($postArr['campaign_code']) && !empty($postArr['campaign_code']) ? $postArr['campaign_code'] : $finalTransaction['campaign_code']);
                $finalTransaction['campaign_id'] = (isset($postArr['campaign_code_id']) && !empty($postArr['campaign_code_id']) ? $postArr['campaign_code_id'] : $finalTransaction['campaign_id']);
                $searchParam['campaign_id'] = $finalTransaction['campaign_id'];
                $searchResultCamp = $this->getServiceLocator()->get('Campaign\Model\CampaignTable')->searchCampaignDetails($searchParam);
                if (isset($searchResultCamp[0]['channel_id']) && !empty($searchResultCamp[0]['channel_id'])) {
                    $arr = explode(",", $searchResultCamp[0]['channel_id']);
                    $finalTransaction['channel_id'] = $transactionChannelId;
                    //if (is_array($arr) && count($arr) > 0 && in_array(5 , $arr))
                    if (is_array($arr) && count($arr) > 0 && in_array($transactionChannelId, $arr)) {
                        //update user campaign channel
                        $finalTransaction['user_id'] = (isset($finalTransaction['user_id'])
                                && !empty($finalTransaction['user_id']) ? $finalTransaction['user_id'] : '');
                        $updatedResponse = $this->getServiceLocator()->get('Campaign\Model\CampaignTable')->userUpdateCampaign(array(
                            'user_id' => $finalTransaction['user_id'], 'campaign_id' => $finalTransaction['campaign_id'],
                            'channel_id' => $transactionChannelId));
                    }
                } else {
                    $finalTransaction['channel_id'] = $transactionChannelId;
                }
            }
            $group_id = $this->_transactionTable->getGroupId($finalTransaction);
            $finalTransaction['group_id'] = isset($group_id[0]['group_id']) && !empty($group_id[0]['group_id']) ? $group_id[0]['group_id'] : "";
            $pbShippingTypeId = '';
            if (!empty($finalTransaction['shipping_method'])) {
                $shippingMetArr = explode(',', $finalTransaction['shipping_method']);
                $pbShippingTypeId = (!empty($shippingMetArr[2])) ? $shippingMetArr[2] : '';
            }
            $finalTransaction['shipping_type_id'] = $pbShippingTypeId;
            $transaction = new Transaction($this->_adapter);
            if ($totalProductsInCart == $shipCompleteStatus || $postArr['invoice_number'] != '') {
                $finalTransaction['transaction_status_id'] = 10;
            } elseif (!empty($shipCompleteStatus) && $shipCompleteStatus < $totalProductsInCart) {
                $finalTransaction['transaction_status_id'] = 11;
            }

            if (isset($postArr['pick_up']) && $postArr['pick_up'] == 1) {
                $finalTransaction['transaction_status_id'] = 4; // Release status
            }
            $finalTransactionArr = $transaction->getFinalTransactionArr($finalTransaction);
            $transactionId = $this->_transactionTable->saveFinalTransaction($finalTransactionArr);

            $this->_transactionTable->updateRevenueGoal($finalTransactionArr);
            $this->_transactionTable->updateChannelRevenue($finalTransactionArr);
			$this->getServiceLocator()->get('Transaction\Model\TransactionTable')->updateTransctionCustomerIP(array($transactionId,$_SERVER['REMOTE_ADDR']));
            /* end add transaction record */

            $finalTransaction['transaction_id'] = $transactionId;

            $isPromotionApplied = '0';
            /* Code start for save coupon used by user */
            if (!empty($postArr['promotion_id'])) {
                $isPromotionApplied = '1';
                $promotionDetailArr = array();
                $promotionDetailArr[] = $postArr['promotion_id'];
                $promotionTable = $this->getServiceLocator()->get('Promotions\Model\PromotionTable');
                $getPromotionDetail = $promotionTable->getPromotionByPromotionId($promotionDetailArr);

                if ($getPromotionDetail['usage_limit_type'] != '1') {
                    $couponUsedArr = $transaction->getCouponUsedArr($finalTransaction);
                    $couponLogId = $this->_transactionTable->saveCouponUsedArr($couponUsedArr);
                }
            }
            /* End  Code for save coupon used by user */
            /* Add payment information  of transaction */

            $amount_total = $totalCartAmount;
            $amountIdArray = array();

            $postArr['source_type'] = $cartProductArr['source_type'];
            $postArr['address_type_status'] = "0";
            if (!empty($postArr['save_shipping_address']) && $postArr['save_shipping_address'] == '1' && isset($postArr['shipping_phone']) && trim($postArr['shipping_phone']) != "" && is_numeric(trim($postArr['shipping_phone']))) {
                $postArr['address_type_status'] = "1";
                $searchArrayN1 = array();
                $searchArrayN1['user_id'] = $postArr['user_id'];
                $phoneDetail1 = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserPhoneInformations($searchArrayN1);
                $postArr['shipping_phone_info_id'] = (!empty($phoneDetail1[0]['phone_contact_id'])) ? $phoneDetail1[0]['phone_contact_id'] : "";
            } elseif (!empty($postArr['save_billing_address']) && $postArr['save_billing_address'] == '1' && isset($postArr['billing_phone']) && trim($postArr['billing_phone']) != "" && is_numeric(trim($postArr['billing_phone']))) {
                $postArr['address_type_status'] = "2";
                $searchArrayN1 = array();
                $searchArrayN1['user_id'] = $postArr['user_id'];
                $phoneDetail1 = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserPhoneInformations($searchArrayN1);
                $postArr['billing_phone_info_id'] = (!empty($phoneDetail1[0]['phone_contact_id'])) ? $phoneDetail1[0]['phone_contact_id'] : "";
            }

            if (!empty($postArr['save_billing_address']) && $postArr['save_billing_address'] == '1') {
                $postArr['address_type'] = 'billing';
                $this->insertUpdateBillingShipping($postArr);
            }
            if (!empty($postArr['save_shipping_address']) && $postArr['save_shipping_address'] == '1') {
                $postArr['address_type'] = 'shipping';
                $this->insertUpdateBillingShipping($postArr);
            }
            /* End add payment information  of transaction */
            /* Add transaction note info */
            if (!empty($finalTransaction['notes']) && trim($finalTransaction['notes']) != '') {
                $transactionNoteData = array();
                $transactionNoteData['transaction_id'] = $transactionId;
                $transactionNoteData['note'] = $finalTransaction['notes'];
                $transactionNoteData['added_by'] = $crm_user_id;
                $transactionNoteData['added_date'] = $dateTime;
                $transactionNoteData['modify_date'] = $dateTime;
                $transactionNoteArr = $transaction->getTransactionNoteArr($transactionNoteData);
                $transactionNoteId = $this->_transactionTable->saveTransactionNote($transactionNoteArr);
            }
            /* Add transaction note info */
            /* Add transaction billing shipping address info */
            $finalTransaction['transaction_id'] = $transactionId;
            $pickUp = !empty($finalTransaction['pick_up']) ? $finalTransaction['pick_up'] : '';
            $phoneNumFinal = "";
            if (isset($finalTransaction['shipping_country_code']) and trim($finalTransaction['shipping_country_code']) != "" and is_numeric(trim($finalTransaction['shipping_country_code']))) {
                $phoneNumFinal .= trim($finalTransaction['shipping_country_code']);
            }
            if (isset($finalTransaction['shipping_area_code']) and trim($finalTransaction['shipping_area_code']) != "" and is_numeric(trim($finalTransaction['shipping_area_code']))) {
                $phoneNumFinal .= "-" . trim($finalTransaction['shipping_area_code']);
            }
            if (isset($finalTransaction['shipping_phone']) and trim($finalTransaction['shipping_phone']) != "" and is_numeric(trim($finalTransaction['shipping_phone']))) {
                $phoneNumFinal .= "-" . trim($finalTransaction['shipping_phone']);
            }

            $phoneNumFinal2 = "";
            if (isset($finalTransaction['billing_country_code']) and trim($finalTransaction['billing_country_code']) != "" and is_numeric(trim($finalTransaction['billing_country_code']))) {
                $phoneNumFinal2 .= trim($finalTransaction['billing_country_code']);
            }
            if (isset($finalTransaction['billing_area_code']) and trim($finalTransaction['billing_area_code']) != "" and is_numeric(trim($finalTransaction['billing_area_code']))) {
                $phoneNumFinal2 .= "-" . trim($finalTransaction['billing_area_code']);
            }
            if (isset($finalTransaction['billing_phone']) and trim($finalTransaction['billing_phone']) != "" and is_numeric(trim($finalTransaction['billing_phone']))) {
                $phoneNumFinal2 .= "-" . trim($finalTransaction['billing_phone']);
            }

            $finalTransaction['shipping_phone'] = $phoneNumFinal;
            $finalTransaction['billing_phone'] = $phoneNumFinal2;
            if ((!empty($isSaveShippingAddress) && $isSaveShippingAddress == 0) ||
                    ($postArr['totalProducts'] > 0 && $postArr['totalProducts'] == $postArr['shipCompleteStatus'])) {
                $finalTransaction['shipping_title'] = '';
                $finalTransaction['shipping_first_name'] = '';
                $finalTransaction['shipping_last_name'] = '';
                $finalTransaction['shipping_company'] = '';
                $finalTransaction['shipping_address'] = '';
                $finalTransaction['shipping_city'] = '';
                $finalTransaction['shipping_state'] = '';
                $finalTransaction['shipping_zip_code'] = '';
                $finalTransaction['shipping_country'] = '';
                $finalTransaction['shipping_phone'] = '';
            }

            /*if (isset($postArr['pick_up']) && $postArr['pick_up'] == 1) {
                $finalTransaction['shipping_title'] = '';
                $finalTransaction['shipping_first_name'] = '';
                $finalTransaction['shipping_last_name'] = '';
                $finalTransaction['shipping_company'] = '';
                $finalTransaction['shipping_address'] = '';
                $finalTransaction['shipping_city'] = '';
                $finalTransaction['shipping_state'] = '';
                $finalTransaction['shipping_zip_code'] = '';
                $finalTransaction['shipping_country'] = '';
                $finalTransaction['shipping_phone'] = '';
            }*/

            $transactionBillingShippingArr = $transaction->getTransactionBillingShipping($finalTransaction);
            $transactionBillingShippingId = $this->_transactionTable->saveTransactionBillingShippingDetail($transactionBillingShippingArr);
            /* End add transaction billing shipping address info */
            /* Add cart product to transaction table */

            $isShipping = 0;
            if (!empty($cartProducts)) {
                $arrDuplicateCertificate = array();
                foreach ($cartProducts as $cartKey => $cartProduct) {
                    if ($cartProduct['is_shippable'] == '1') {
                        $isShipping = 1;
                    }
                    $insertCartDataArr = $cartProduct;
                    $insertCartDataArr['transaction_id'] = $transactionId;
                    $insertCartDataArr['user_id'] = $postArr['user_id'];
                    $insertCartDataArr['added_by'] = '';
                    $insertCartDataArr['added_date'] = $dateTime;
                    $insertCartDataArr['modify_by'] = '';
                    $insertCartDataArr['modify_date'] = $dateTime;
                    $insertCartDataArr['type_of_product'] = $cartProduct['type_of_product'];

					if (isset($cartProduct['shipping_discount_amount']))
						$insertCartDataArr['shipping_price']=$this->RoundAmount($cartProduct['shipping_price'] - $cartProduct['shipping_discount_amount']);
					elseif(isset($cartProduct['shipping_discount_in_amount']))
						$insertCartDataArr['shipping_price']=$this->RoundAmount($cartProduct['shipping_price'] - $cartProduct['shipping_discount_in_amount']);

					
					$totalShipping = $insertCartDataArr['shipping_price']+$cartProduct['surcharge_price']+$cartProduct['shipping_handling_price'];

					if($totalShipping<0){
						$insertCartDataArr['shipping_price'] = 0.00;
						$insertCartDataArr['surcharge_price'] = 0.00;
						$insertCartDataArr['shipping_handling_price'] = 0.00;
					}

                    if (!empty($cartProduct['item_name']))
                        $insertCartDataArr['item_name'] = $cartProduct['item_name'];

                    if (!empty($cartProduct['manifest_info']))
                        $insertCartDataArr['manifest_info'] = $cartProduct['manifest_info'];

                    if (!empty($cartProduct['manifest_type']))
                        $insertCartDataArr['manifest_type'] = $cartProduct['manifest_type'];

                    if ($cartProduct['product_type_id'] == 4) {
                        $search = array();
                        $search['fof_id'] = $cartProduct['cart_id'];
                        $searchResult = $this->getServiceLocator()->get('User\Model\UserTable')->getFofCart($search);
                        $fofImage = $searchResult[0]['fof_image'];
                        $this->moveFileFromTempToFOF($fofImage);
                    }
                    if ($cartProduct['product_type_id'] == '2') {
                        if (!empty($cartProduct['campaign_id'])) {
                            $donation_amount = $cartProduct['product_amount'];
                            $campaign_id_backend = $cartProduct['campaign_id'];
                            $campArrayBack['campaign_id'] = $campaign_id_backend;
                            $campArrayBack['revenue_recieved'] = $donation_amount;
                            $this->getServiceLocator()->get('Campaign\Model\CampaignTable')->updateRevenueGoal($campArrayBack);
                        }
                        /*$searchDonNotify = $this->_transactionTable->getDonNotify($cartProduct);
                        foreach ($searchDonNotify as $key => $val) {
                            if ($val['email_id'] != 'Email') {
                                $donationNotifyArr['donarName'] = $val['name'];
                                $name = explode(" ", $val['notify_name']);
                                $donationNotifyArr['first_name'] = $name[0];
                                $donationNotifyArr['last_name'] = $name[1];
                                $donationNotifyArr['name'] = $val['notify_name'];
                                $donationNotifyArr['email_id'] = $val['email_id'];
                                $donationNotifyArr['amount'] = $cartProduct['product_amount'];
                                $donationNotifyArr['program_name'] = $val['program'];
                                $email_id_template_int = 15;
                                $donationNotifyArr['admin_email'] = $this->_config['soleif_email']['email'];
                                $donationNotifyArr['admin_name'] = $this->_config['soleif_email']['name'];
                                $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($donationNotifyArr, $email_id_template_int, $this->_config['email_options']);
                            }
                        }*/
                    }
                    if (isset($postArr['pick_up']) && $postArr['pick_up'] == 1)
                        $insertCartDataArr['pick_up'] = 1;
                    else
                        $insertCartDataArr['pick_up'] = 0;
                    if (isset($cartProduct['type_of_product']) and isset($cartProduct['item_info']) and trim($cartProduct['type_of_product']) != "" and trim($cartProduct['item_info']) != "" and trim($cartProduct['type_of_product']) == "personalizeduplicate" and trim($cartProduct['item_info']) == "1") {
                        $insertCartDataArr['item_info'] = "2";
                        $insertCartArr = $transaction->getInsertCartToTransactionArr($insertCartDataArr);
                        array_push($arrDuplicateCertificate, array('arr1' => $insertCartArr, 'cartkey' => $cartKey));
                    } else {
                        $insertCartArr = $transaction->getInsertCartToTransactionArr($insertCartDataArr);

                        $productTransactionId = $this->_transactionTable->saveCartToTransaction($insertCartArr);
                        $paymentArray[$cartKey]['table_auto_id'] = $productTransactionId;
                        $paymentArray[$cartKey]['transaction_id'] = $transactionId;
                        $arrUnshipProductTotalAmount['table_auto_id'][] = $productTransactionId;
                        if ($cartProduct['product_type_id'] == 2) { //Donation
                            if ($cartProduct['product_mapping_id'] == 5)
                                $paymentArray[$cartKey]['table_type'] = 4;
                            else
                                $paymentArray[$cartKey]['table_type'] = 2;
                        }
                        else if ($cartProduct['product_type_id'] == 4) //FOF
                            $paymentArray[$cartKey]['table_type'] = 3;
                        else if ($cartProduct['product_type_id'] == 6) { //WOH
                            if ($cartProduct['type_of_product'] == 'biocertificate')
                                $paymentArray[$cartKey]['table_type'] = 1;
                            elseif ($cartProduct['type_of_product'] == 'woh')
                                $paymentArray[$cartKey]['table_type'] = 5;
                            else
                                $paymentArray[$cartKey]['table_type'] = 4;
                        }
                        else {
                            $paymentArray[$cartKey]['table_type'] = 4; // for transaction
                        }
                    }
                }

                if (isset($arrDuplicateCertificate) and count($arrDuplicateCertificate) > 0) { //????????????????????????????????????????????
                    foreach ($arrDuplicateCertificate as $valDC) {
                        $productTransactionId = $this->_transactionTable->saveCartToTransaction($valDC['arr1']);
                        $paymentArray[$valDC['cartkey']]['table_auto_id'] = $productTransactionId; // for transaction
                        $paymentArray[$valDC['cartkey']]['table_type'] = 4; // for transaction
                        $paymentArray[$valDC['cartkey']]['transaction_id'] = $transactionId; // for transaction
                    }
                }
            }

            if ($cartProductArr['source_type'] == 'frontend') {
                $paymentFrontArray = array();
                //$paymentFrontArray['profile_id'] = $postArr['credit_card_exist']; 
                $paymentFrontArray['transaction_amounts'] = $totalCartAmount;
                $paymentFrontArray['transaction_id'] = $transactionId;
                $paymentSuccessStatus = '';

                $paymentTransactionArray['amount'] = $amount_total;
                $paymentTransactionArray['description'] = 'Woh Transaction';
                $paymentTransactionArray['invoice_num'] = $transactionId;
                $paymentTransactionArray['first_name'] = $postArr['shipping_first_name'];
                $paymentTransactionArray['last_name'] = $postArr['shipping_last_name'];
                $paymentTransactionArray['address'] = trim($postArr['shipping_address_1'].' '.$postArr['shipping_address_2']);
                $paymentTransactionArray['city'] = $postArr['shipping_city'];
                $paymentTransactionArray['state'] = $postArr['shipping_state'];
                $paymentTransactionArray['zip'] = $postArr['shipping_zip_code'];
                $paymentTransactionArray['country'] = $postArr['shipping_country_text'];
                $paymentTransactionArray['phone'] = $postArr['shipping_country_code'].$postArr['shipping_area_code'].$postArr['shipping_phone'];
                $paymentTransactionArray['email'] = trim($postArr['email_id']);
                $paymentTransactionArray['cust_id'] = $postArr['user_id'];
                $paymentTransactionArray['card_details'] = $posTransactionSession->posTransactionData['card_details'];
                if ($amount_total > 0) {
                    $paymentResponse = $this->makeAuthorizeWohPayment($paymentTransactionArray); 
                }
                if ($paymentResponse->approved == 1){
                    $authorizeTransactionId = $paymentResponse->transaction_id;    
                    
                    $creditCardResult = $this->_posTable->getCreditCards();
                    foreach ($creditCardResult as $key => $val) {
                        $creditCardResultArray[$val['credit_card_type_id']] = strtolower($val['credit_card']);
                    }
                    
                    $dataArr['user_id'] = $postArr['user_id'];
                    $dataArr['profile_id'] = '';
                    $creditCardType = array_search(strtolower($paymentResponse->card_type), $creditCardResultArray);
                    $dataArr['credit_card_type_id'] = $creditCardType;
                    $dataArr['credit_card_no'] = $paymentResponse->card_num;
                    $dataArr['added_by'] = DATE_TIME_FORMAT;
                    $dataArr['billing_address_id'] = $transactionBillingShippingId;
                    $dataArr['modified_by'] = DATE_TIME_FORMAT;
                    $tokenResult = $this->_posTable->insertTokenInfo($dataArr);                    
                    
                    /* insert into payment table */
                    $paymentTransactionData = array();

                    $paymentTransactionData['amount'] = $posTransactionSession->posTransactionData['transaction_details']['finaltotal'];
                    $paymentTransactionData['transaction_id'] = $transactionId;
                    $paymentTransactionData['payment_mode_id'] = 1;
                    $paymentTransactionData['authorize_transaction_id'] = $authorizeTransactionId;
                    $paymentTransactionData['profile_id'] = '';
                    $paymentTransactionData['payment_profile_id'] = $tokenResult[0]['LAST_INSERT_ID'];
                    $paymentTransactionData['added_by'] = '';
                    $paymentTransactionData['added_date'] = DATE_TIME_FORMAT;
                    $paymentTransactionData['modify_by'] = '';
                    $paymentTransactionData['modify_date'] = DATE_TIME_FORMAT;
                    $paymentTransactionData = $pos->getTransactionPaymentArr($paymentTransactionData);
                    $transactionPaymentId = $this->_posTable->saveTransactionPaymentDetail($paymentTransactionData);                    
                    
                    /* insert into payment received table */
                    foreach ($paymentArray as $key => $val) {
	                    $paymentReceivedTransactionData = array();
	                    $paymentReceivedTransactionData['table_auto_id'] = $val['table_auto_id'];
	                    $paymentReceivedTransactionData['table_type'] = $val['table_type']; // transaction product ??????????????????????????????????
	                    $paymentReceivedTransactionData['authorize_transaction_id'] = $authorizeTransactionId;
	                    $paymentReceivedTransactionData['profile_id'] = ''; //$customerProfileId;
	                    $paymentReceivedTransactionData['amount'] = $val['cart_product_amount'];
	                    $paymentReceivedTransactionData['product_type_id'] = $val['cart_product_type_id'];
	                    $paymentReceivedTransactionData['transaction_payment_id'] = $transactionPaymentId;
	                    $paymentReceivedTransactionData['transaction_id'] = $val['transaction_id'];
	                    $paymentReceivedTransactionData['payment_source'] = 'credit';
	                    $paymentReceivedTransactionData['isPaymentReceived'] = '1';
	                    $paymentReceivedTransactionData['add_date'] = $postParam['add_date'];
	                    $this->insertIntoPaymentReceive($paymentReceivedTransactionData);                    
                    }
                }
                else{ /* transaction fail condition*/
                    
                    $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter')->getDriver()->getConnection()->rollback();
	                $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->onAutoCommit();
		            $paymentError = $this->Encrypt('payment_error');
		            
		            if (isset($this->_config['https_on']) && $this->_config['https_on'] == '1') {
                        $siteUrl = SITE_URL_HTTPS;
	                } else {
	                    $siteUrl = SITE_URL;
	                }
                
		            $location = $siteUrl . '/poscheckout/'.$paymentError;
		            header('Location: ' . $location);
		            //echo '<script>window.location.href="' . $location . '";</script>';
		            die;   
                }
            }

           /* if (isset($paymentSuccessStatus['status']) && $paymentSuccessStatus['status'] == 'payment_error') {
                $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter')->getDriver()->getConnection()->rollback();
                $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->onAutoCommit();
                return $paymentSuccessStatus;
                exit;
            }*/

            $associationArr = array();
            $associationArr['user_id'] = $userId;
            $associationArr['association'] = '1';
            $this->getServiceLocator()->get('User\Model\UserTable')->updateUserAssociation($associationArr);
            $userPurchasedProductArr = array();
            $userPurchasedProductArr['user_id'] = $userId;

            $date_range = $this->getDateRange(4);
            $userPurchasedProductArr['from_date'] = $this->DateFormat($date_range['from'], 'db_date_format_from');
            $userPurchasedProductArr['to_date'] = $this->DateFormat($date_range['to'], 'db_date_format_to');
            $getTotalUserPurchaseProductAmountArr = $transaction->getTotalUserPurchaseProductAmountArr($userPurchasedProductArr);
            $totalAmountPurchased = $this->_transactionTable->getTotalUserPurchased($getTotalUserPurchaseProductAmountArr);
            $userMembershipArr = array();
            $userMembershipArr['transaction_amount'] = $totalAmountPurchased;
            $userMembershipArr['donation_amount'] = $donAmt;
            $userMembershipArr['user_id'] = $userId;
            $userMembershipArr['transaction_id'] = $transactionId;
            $userMembershipArr['transaction_date'] = $dateTime;

            $this->updateUserMembership($userMembershipArr);


            if (isset($pledgeDonationAmount) && !empty($pledgeDonationAmount)) {
                $userPledgeArr = array();
                $userPledgeArr['userId'] = $userId;
                $userPledgeArr['transaction_id'] = $transactionId;
                $userPledgeArr['donation_amount'] = $pledgeDonationAmount;
                $this->updateUserPledgeTransaction($userPledgeArr);
            }
            $creditAmountArr = array();
            $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter')->getDriver()->getConnection()->commit();
            $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->onAutoCommit();
            /* End send stock notification */
            $responseArr['status'] = 'success';
            $responseArr['transaction_id'] = $transactionId;

            /* end add cart product to transaction table */
            if ($cartProductArr['source_type'] == 'backend') {
                $postArr['billing_address_1'] = $postArr['billing_address'];
                $postArr['billing_address_2'] = '';
                $postArr['shipping_address_1'] = (!empty($postArr['shipping_address'])) ? $postArr['shipping_address'] : 'N/A';
                $postArr['shipping_address_2'] = '';
            }
            $isEmailConfirmation = !empty($postArr['is_email_confirmation']) ? $postArr['is_email_confirmation'] : '';


            if (($cartProductArr['source_type'] == 'backend' && $isEmailConfirmation != 0 && !empty($isEmailConfirmation)) || ($cartProductArr['source_type'] == 'frontend')) {
                /*  send email  */
                $email_id_template_int = 10;
                $viewLink = '/profile';
                $param['user_id'] = $userId;
                $userDataArr = $this->getServiceLocator()->get('User\Model\UserTable')->getUser(array('user_id' => $userId));

                $postArr['shipping_first_name'] = !empty($postArr['shipping_first_name']) ? $postArr['shipping_first_name'] : '';
                $postArr['shipping_last_name'] = !empty($postArr['shipping_last_name']) ? $postArr['shipping_last_name'] : '';
                $postArr['shipping_address_1'] = !empty($postArr['shipping_address_1']) ? $postArr['shipping_address_1'] : '';
                $postArr['shipping_address_2'] = !empty($postArr['shipping_address_2']) ? $postArr['shipping_address_2'] : '';
                $postArr['shipping_city'] = !empty($postArr['shipping_city']) ? $postArr['shipping_city'] : '';
                $postArr['shipping_state'] = !empty($postArr['shipping_state']) ? $postArr['shipping_state'] : '';
                $postArr['shipping_zip_code'] = !empty($postArr['shipping_zip_code']) ? $postArr['shipping_zip_code'] : '';
                $postArr['shipping_country_text'] = !empty($postArr['shipping_country_text']) ? $postArr['shipping_country_text'] : '';


                $shippingAddressDetail = '';

//if (!((empty($postArr['shipping_first_name']) || $postArr['shipping_first_name'] != 'N/A') && (empty($postArr['shipping_last_name']) || $postArr['shipping_last_name'] != 'N/A') && (empty($postArr['shipping_address_1']) || $postArr['shipping_address_1'] != 'N/A') && (empty($postArr['shipping_address_2']) || $postArr['shipping_address_2'] != 'N/A') && (empty($postArr['shipping_city']) || $postArr['shipping_city'] != 'N/A') && (empty($postArr['shipping_state']) || $postArr['shipping_state'] != 'N/A') && (empty($postArr['shipping_zip_code']) || $postArr['shipping_zip_code'] != 'N/A'))) {
                if ($isShipping == 1) {
                    $address = $postArr['shipping_address_1'];
                    $address.= (!empty($postArr['shipping_address_2'])) ? '<br>' . $postArr['shipping_address_2'] : "";
                    $shippingStreetAddress = ((!empty($postArr['shipping_title'])) ? $postArr['shipping_title'] . ' ' : '') . $postArr['shipping_first_name'] . ' ' . $postArr['shipping_last_name'] . '<br>' . $address;
                    $shippingAddressDetail = '<td style="50%">  <p style="margin: 0px; padding-left: 0pt; padding-right: 0pt; font: 20px/36px arial; color: #000;  padding-top: 20px; ">Shipping Address :<br />
        <strong style="color: #7E8183; font-weight:normal; font: 16px/30px arial;">' . $shippingStreetAddress . '</strong><br />
        <strong style="color: #7E8183; font-weight:normal; font: 16px/30px arial;">' . $postArr['shipping_city'] . ',&nbsp;' . $postArr['shipping_state'] . '</strong><br />
        <strong style="color: #7E8183; font-weight:normal; font: 16px/30px arial;">' . $postArr['shipping_country_text'] . '&nbsp;-&nbsp;' . $postArr['shipping_zip_code'] . '</strong><br />
        <strong style="color: #7E8183; font-weight:normal; font: 16px/30px arial;"></strong></p></td>';
                }

                $userDataArr = array_merge($userDataArr, $postArr);
                $userDataArr['transactionId'] = $transactionId;
                $userDataArr['transaction_date'] = $this->OutputDateFormat($dateTime, 'dateformatampm');
                $userDataArr['transaction_amount_total'] = $amount_total;

                if ($userDataArr['user_type'] != 1) {
                    $userDataArr['first_name'] = $userDataArr['company_name'];
                    $userDataArr['last_name'] = '';
                } else {
                    $userDataArr['billing_first_name'] = ((!empty($postArr['billing_title'])) ? $postArr['billing_title'] . ' ' : '') . $userDataArr['billing_first_name'];
                }
                $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                $userDataArr['shippingAddressDetail'] = $shippingAddressDetail;
                /** Order Details Template */
                $searchParam['transactionId'] = $transactionId;
                $searchResult = $this->_transactionTable->searchTransactionDetails($searchParam);
                $searchProduct = $this->_transactionTable->searchTransactionProducts($searchParam);
                foreach ($searchProduct as $key => $val) {
                    switch ($val['tra_pro']) {
                        case 'fof':
                            $fofResult = $this->_transactionTable->searchFofDetails($val);
                            array_push($searchProduct[$key], $fofResult[0]);
                            break;
                        case 'pro':
                            $passengerResult = $this->_transactionTable->searchDocPass($val);
                            array_push($searchProduct[$key], $passengerResult[0]);
                            break;
                    }
                    $proParam['transactionId'] = $searchParam['transactionId'];
                    $proParam['productId'] = $val['id'];
                    $proResult = $this->_transactionTable->searchProductsShipped($proParam);
                    if (isset($proResult[0]) && !empty($proResult[0])) {
                        $searchProduct[$key]['shippedQty'] = $proResult[0]['shippped_qty'];
                    } else {
                        $searchProduct[$key]['shippedQty'] = 0;
                    }
                }

//n-woh- start 
                $arrFinalWallOfHonorId = array();
                $arrFinalWallOfHonorIdContacts = array();
//n-woh- end

                $orderDetailsArr = '<div><table width="100%" border="0" style="border-bottom:1px dotted #ccc;" cellspacing="5" cellpadding="10" class="table-bh tra-view">
                    <tr>
                    <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:left; padding-left:0;">Sr. No</th>
                    <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:left">Product</th>
                    <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:left">SKU</th>
                    <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:left">Qty.</th>
                    <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:center">Price</th>
                    <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:center">SubTotal</th></tr>';
                /* '<th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:center">Tax</th>
                  <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:center">Shipping & Handling</th>
                  <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:center">Discount</th>
                  <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:right">Total</th>
                  </tr>'; */
                $i = 1;

                foreach ($searchProduct as $key => $val) {
                    $orderDetailsArr.= '<tr><td style="width: 48px; font: 16px/20px arial; color: #7e8183; text-align:left; vertical-align:top;">' . $i++ . '</td>
                        <td style="font: 16px/20px arial; color: #7e8183; text-align:left">';
                    switch ($val['tra_pro']) {
                        case 'don' :
                            $orderDetailsArr.=$val['product_name'] . ' <label>' . $val['option_name'] . '</label>';
                            break;
                        case 'bio' :
                            $orderDetailsArr.=$val['product_name'] . '<br> <label>' . $val['item_name'] . '</label>';
                            break;
                        case 'fof' :
                            $orderDetailsArr.=$val['product_name'] . '<br> <label>' . $val[0]['donated_by'] . '</label>';
                            break;
                        case 'pro' :
                            switch ($val['product_type']) {

                                case '5' :
                                    $orderDetailsArr.=$val['product_name'] . '<label> :</label> ' . $val['option_name'] . '<br><label>Passenger Name : ' . $val[0]['NAME'] . '</label><br/><label>Passenger Id : </label> ' . $val[0]['item_id'];
                                    break;
                                case '6' :
                                    $orderDetailsArr.=$val['product_name'] . '<br><label> ' . $val[0]['final_name'] . '</label>';
                                    break;
                                case '7' :
                                    $itemName = '';
                                    if (strlen($val[0]['item_id']) < 12)
                                        $itemName = "Ship Name : " . $val['item_name'];
                                    else
                                        $itemName = "Passenger Name : " . $val['item_name'];

                                    if (!empty($val['item_info']) && $val['item_info'] != '0000-00-00')
                                        $arrivalDate = $this->OutputDateFormat($val['item_info'], 'calender');
                                    else
                                        $arrivalDate = '';

                                    $productName = $val['product_name'];
                                    if (isset($val['manifest_info']) && !empty($val['manifest_info']))
                                        $productName.= ' : ' . $val['manifest_info'];

                                    $orderDetailsArr.=$productName . '<br>  ' . $itemName . '<br/><label>Arrival Date : </label>' . $arrivalDate;
                                    break;
                                case '8' :
                                    if (!empty($val['item_info']) && $val['item_info'] != '0000-00-00')
                                        $arrivalDate = $this->OutputDateFormat($val['item_info'], 'calender');
                                    else
                                        $arrivalDate = '';
                                    $orderDetailsArr.=$val['product_name'] . '<label> : </label>' . $val['option_name'] . '<br/><label>Ship Name : </label>' . $val['item_name'] . '<br/><label>Arrival Date : </label>' . $arrivalDate;
                                    break;
                                default :
                                    $orderDetailsArr.=$val['product_name'] . ' <label>' . $val['option_name'] . '</label>';
                                    break;
                            }
                            break;
                        case 'woh' :

//n-woh- start 
                            if (isset($val['id']) and trim($val['id']) != "" and !in_array(trim($val['id']), $arrFinalWallOfHonorId)) {
                                array_push($arrFinalWallOfHonorId, trim($val['id']));
                            }
//n-woh- end
                            $orderDetailsArr.=$val['product_name'] . '<br> <label>' . utf8_decode($val['option_name']) . '</label>';
                            break;
                    }
                    $orderDetailsArr .='</td><td style="font: 16px/20px arial; color: #7e8183; text-align:left">';
                    if (isset($val['product_sku']) && !empty($val['product_sku'])) {
                        $orderDetailsArr .=$val['product_sku'];
                    } else {
                        $orderDetailsArr .='N/A';
                    }
                    $orderDetailsArr.='</td><td style="font: 16px/20px arial; color: #7e8183; text-align:left">' . $val['product_quantity'] . '</td>
                        <td style="font: 16px/20px arial; color: #7e8183; text-align:center">$' . $this->Currencyformat($val['product_price']) . '</td>
                        <td style="font: 16px/20px arial; color: #7e8183; text-align:right">$' . $this->Currencyformat($val['product_sub_total']) . '</td></tr>';
                }
                $orderDetailsArr .= '<tr>
                    <td align="right" colspan="5"><span style="text-align:right; font: 16px/28px arial; color: #7e8183;">Sub Total :</span></td>
                    <td align="right">
                    <strong style="text-align:right; font: 16px/28px arial; color: #7e8183; font-weight:bold;">$' . $this->Currencyformat($searchResult[0]['sub_total_amount']) . '
                    </strong></td>
                    </tr>
                    <tr>
                    <td align="right" colspan="5"><span style="text-align:right; font: 16px/28px arial; color: #7e8183;">Shipping & Handling :</span></td>
                    <td align="right">
                    <strong style="text-align:right; font: 16px/28px arial; color: #7e8183; font-weight:bold;">$' . $this->Currencyformat($searchResult[0]['shipping_amount'] + $searchResult[0]['handling_amount']) . '
                    </strong></td>
                    </tr>
                    <tr>
                    <td align="right" colspan="5"><span style="text-align:right; font: 16px/28px arial; color: #7e8183;">Tax :</span></td>
                    <td align="right">
                    <strong style="text-align:right; font: 16px/28px arial; color: #7e8183; font-weight:bold;">$' . $this->Currencyformat($searchResult[0]['total_tax']) . '
                    </strong></td>
                    </tr>
                    <tr>
                    <td align="right" colspan="5"><span style="text-align:right; font: 16px/28px arial; color: #7e8183;">Discount :</span></td>
                    <td align="right">
                    <strong style="text-align:right; font: 16px/28px arial; color: #7e8183; font-weight:bold;">($' . $this->Currencyformat($searchResult[0]['total_discount']) . ')
                    </strong></td>
                    </tr>
                    <tr>
                    <td align="right" colspan="5"><span style="text-align:right; font: 16px/28px arial; color: #7e8183;">Purchase Total :</span></td>
                    <td align="right">
                    <strong style="text-align:right; font: 16px/28px arial; color: #7e8183; font-weight:bold;">$' . $this->Currencyformat($searchResult[0]['transaction_amount'] - ($searchResult[0]['cancel_amount'] + $searchResult[0]['refund_amount'])) . '
                    </strong></td>
                    </tr></table>';
                $userDataArr['order_summary'] = $orderDetailsArr;
                $userDataArr['invoice_number'] = $searchResult[0]['invoice_number'];

                if (!empty($userDataArr['email_id'])) // order email
                    $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $email_id_template_int, $this->_config['email_options']);



                // Thank you mail dev 4
                if ($sendThankyouMail == 1) {

                    $userDataThankArr = array();
                    $userDataThankArr['email_id'] = $userDataArr['email_id'];
                    if ($userDataArr['user_type'] != 1) {
                        $userDataThankArr['first_name'] = $userDataArr['company_name'];
                    } else {
                        $userDataThankArr['first_name'] = $userDataArr['first_name'];
                    }

                    $userDataThankArr['amount'] = $this->Currencyformat($pledgeDonationAmount);
                    $userDataThankArr['admin_email'] = $this->_config['soleif_email']['email'];
                    $userDataThankArr['admin_name'] = $this->_config['soleif_email']['name'];
                    $email_id_template_int = '8'; // Thank you mail 

                    if (!empty($userDataArr['email_id']))
                        $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataThankArr, $email_id_template_int, $this->_config['email_options']);
                }
                // End Thank you mail                
//n-woh- start 

                if (is_array($arrFinalWallOfHonorId) and count($arrFinalWallOfHonorId) > 0) {
                    foreach ($arrFinalWallOfHonorId as $valWoh) {
                        $arrContacts = $this->_transactionTable->getUserWohContact(array('woh_id' => $valWoh));
                        if (is_array($arrContacts) and count($arrContacts) > 0) {
                            foreach ($arrContacts as $valWoh2) {
                                if (!in_array(array("firstNameWoh" => $valWoh2['firstNameWoh'], "lastNameWoh" => $valWoh2['lastNameWoh'], "emailIdWoh" => $valWoh2['emailIdWoh']), $arrFinalWallOfHonorIdContacts)) {
                                    //array_push($arrFinalWallOfHonorIdContacts, array("firstNameWoh" => $valWoh2['firstNameWoh'], "lastNameWoh" => $valWoh2['lastNameWoh'], "emailIdWoh" => $valWoh2['emailIdWoh']));
                                }
                            }
                        }
                    }
                }


                if (is_array($arrFinalWallOfHonorIdContacts) and count($arrFinalWallOfHonorIdContacts) > 0) {
                    foreach ($arrFinalWallOfHonorIdContacts as $valWoh3) {
                        if (isset($valWoh3['emailIdWoh']) and trim($valWoh3['emailIdWoh']) != "") {
                            $userDataArr['first_name'] = $valWoh3['firstNameWoh'];
                            $userDataArr['last_name'] = $valWoh3['lastNameWoh'];
                            $userDataArr['email_id'] = $valWoh3['emailIdWoh'];
                            if(!empty($userDataArr['email_id']))
                                $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $email_id_template_int, $this->_config['email_options']);
                        }
                    }
                }
//n-woh- end
            }
            /* send email end */


            /* Send stock notification */

            if (!empty($cartProducts)) {
                $stockNotificationData = array();
                $stockNotificationData = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getStockNotification();
                if (!empty($stockNotificationData)) {
                    $notificationReceiptsArr = explode(',', $stockNotificationData[0]['notification_recipients']);
                    $isEmailSend = $stockNotificationData[0]['is_email_send'];
                    foreach ($cartProducts as $index=>$inventoryPro) {
						if($inventoryPro['in_ellis_stock']=='1'){
							if($postArr['pick_up']==1){
								$remainingQty = $inventoryPro['total_in_ellis_stock'] - $inventoryPro['product_qty'];
								$product_threshold = $inventoryPro['ellis_product_threshold'];
								$site_url = 'http://elliscrm.ellisisland.org';
							}else{
								$remainingQty = $inventoryPro['total_in_stock'] - $inventoryPro['product_qty'];
								$product_threshold = $inventoryPro['product_threshold'];
								$site_url = 'http://soleifcrm.ellisisland.org';
							}
							
							if ($isEmailSend == 1 && !empty($notificationReceiptsArr) && $remainingQty <= $product_threshold) {
								$productViewLink = $site_url . '/view-crm-product/' . $this->encrypt($inventoryPro['product_id']) . '/' . $this->encrypt('view');
								$productViewLink = '<a href="' . $productViewLink . '">' . $productViewLink . '</a>';
								$messageSubject = str_replace('[store: name]', $inventoryPro['product_name'], $stockNotificationData[0]['message_subject']);
								if($postArr['pick_up']==1)
									$messageSubject = str_replace('[uc_stock:source]', 'Ellis', $messageSubject);
								else
									$messageSubject = str_replace('[uc_stock:source]', 'Web', $messageSubject);
								$messageText = str_replace('[node:title]', $inventoryPro['product_name'], $stockNotificationData[0]['message_text']);
								$messageText = str_replace('[uc_stock:model]', $inventoryPro['product_sku'], $messageText);
								$messageText = str_replace('[uc_stock:level]', $remainingQty, $messageText);
								$messageText = str_replace('#[uc_purchase:link]', $productViewLink, $messageText);
								if($postArr['pick_up']==1)
									$messageText = str_replace('[uc_stock:source]', 'Ellis', $messageText);
								else
									$messageText = str_replace('[uc_stock:source]', 'Web', $messageText);
								$emailDataArr = array();
								$emailDataArr['subject'] = $messageSubject;
								$emailDataArr['message'] = $messageText;
								$emailDataArr['admin_email'] = $this->_config['soleif_email']['email'];
								$emailDataArr['admin_name'] = $this->_config['soleif_email']['name'];
								foreach ($notificationReceiptsArr as $receipt) {
									$emailDataArr['emailId'] = $receipt;
									if(!empty($emailDataArr['emailId']))
										$this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmailTemplate($emailDataArr, $this->_config['email_options']);
								}
							}
						}                        
                    }
                }
            }
        }
        return $responseArr;
    }

    /**
     * This action is used to update user membership
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function updateUserMembership($userMembershipArr) {
        $transaction = new Transaction($this->_adapter);
        $userMembershipUpdateArr = array();
        $userMembershipArr['transaction_amount'] = (isset($userMembershipArr['transaction_amount']) && $userMembershipArr['transaction_amount'] == 0 &&
                !empty($userMembershipArr['donation_amount'])) ? $userMembershipArr['donation_amount'] : $userMembershipArr['transaction_amount'];
        $userMembershipUpdateArr['minimun_donation_amount'] = isset($userMembershipArr['transaction_amount']) ? $userMembershipArr['transaction_amount'] : '';
        $getMatchingMembershipArr = $transaction->getMatchingMembershipArr($userMembershipUpdateArr);
        $matchingMembership = $this->_transactionTable->getMatchingMembership($getMatchingMembershipArr);
        $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $userMembershipArr['user_id']));
        //if (!empty($matchingMembership) && $matchingMembership['membership_id'] != $userDetail['membership_id']) {
        $gracePeriodStatus = false;
        $membershipExpiredStatus = false;
        $todayTime = time();
        $memberExpired = date("Y-m-d", strtotime($userDetail['membership_expire']));
        $leftTime = strtotime($memberExpired) - $todayTime;
		if ($userDetail['is_ellis_default'] != 1) {
			if ($leftTime > 0) {
				if (floor($leftTime / (60 * 60 * 24)) < $this->_config['membership_plus'])
					$gracePeriodStatus = true; //allow update 
			}
		}

        if ($gracePeriodStatus) {
            $userMembershipUpdateArr = array();
            $userMembershipUpdateArr['minimun_donation_amount'] = isset($userMembershipArr['transaction_amount']) ? $userMembershipArr['transaction_amount'] : '';
            $getMatchingMembershipArr = $transaction->getMatchingMembershipArr($userMembershipUpdateArr);
            $matchingMembership = $this->_transactionTable->getMatchingMembership($getMatchingMembershipArr);
        }
		
        if ($leftTime < 1 || ( /*$gracePeriodStatus == true ||*/ (!empty($matchingMembership) && $matchingMembership['membership_id'] != $userDetail['membership_id'] && ($userMembershipUpdateArr['minimun_donation_amount'] >= $userDetail['donation_amount']) && !preg_match('/annual/',strtolower($userDetail['membership_title'])) && !preg_match('/patron/',strtolower($userDetail['membership_title'])) ) )) {
            $startDate = date("Y-m-d", strtotime(DATE_TIME_FORMAT));
            $membershipDateFrom = '';
            $membershipDateTo = '';

            if ($matchingMembership['validity_type'] == 1) { 
                $membershipDateFrom = $startDate;
                $startDay = $this->_config['financial_year']['srart_day'];
                $startMonth = $this->_config['financial_year']['srart_month'];
                $startYear = date("Y", strtotime(DATE_TIME_FORMAT));
                $startDate = $startYear . "-" . $startMonth . "-" . $startDay;
//$year = 1;

                $addYear = ($matchingMembership['validity_time'] - $startYear) + 1;
                $futureDate = date('Y-m-d', strtotime('+' . $addYear . ' year -1 day', strtotime($startDate)));
                $your_date = $futureDate;
                $now = time();
                $datediff = strtotime($your_date) - $now;
                if ((isset($this->_config['membership_plus']) && !empty($this->_config['membership_plus'])) && (floor($datediff / (60 * 60 * 24)) < $this->_config['membership_plus'])) {
                    $addYear = ($matchingMembership['validity_time'] - $startYear) + 2;
                    $futureDate = date('Y-m-d', strtotime('+' . $addYear . ' year', strtotime($startDate)));
                    $futureDate = date('Y-m-d', strtotime('-1 day', strtotime($futureDate)));
                }
                $membershipDateTo = $futureDate;
            } else if ($matchingMembership['validity_type'] == 2) {  
                $membershipDateFrom = $startDate;
				if($matchingMembership['is_ellis_default'] == 1){
					$expirationMonths = $this->_config['ellis_default']['membership_expiration_days']/30;
					$futureDate = date('Y-m-d', strtotime('+' . $expirationMonths . ' months -1 day', strtotime($startDate)));
				}else if(!is_null($matchingMembership['validity_time']) &&	$matchingMembership['validity_time']!=''){
					$futureDate = date('Y-m-d', strtotime('+' . $matchingMembership['validity_time'] . ' year -1 day', strtotime($startDate)));
				}
            }
			$membershipDateTo = $futureDate;
            $userMembershipUpdateArr = array();
            $userMembershipUpdateArr['user_id'] = $userMembershipArr['user_id'];
            $userMembershipUpdateArr['membership_id'] = $matchingMembership['membership_id'];
            $userMembershipUpdateArr['transaction_date'] = isset($userMembershipArr['transaction_date']) ? $userMembershipArr['transaction_date'] : '';
            $userMembershipUpdateArr['transaction_amount'] = isset($userMembershipArr['transaction_amount']) ? $userMembershipArr['transaction_amount'] : '';
            $userMembershipUpdateArr['membership_date_from'] = $membershipDateFrom;
            $userMembershipUpdateArr['membership_date_to'] = $membershipDateTo;
            if (empty($membershipDateFrom) || empty($membershipDateTo)) {
                return false;
            }
            $getUpdateUserMembershipArr = $transaction->getUpdateUserMembershipArr($userMembershipUpdateArr);
            $userMembershipId = $this->_transactionTable->updateUserMembership($getUpdateUserMembershipArr);
        }
    }

    /**
     * This action is used to update user membership
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function updateUserPledgeTransaction($userPledgeArr = array()) {
        /* Update active pledge for user */
        $pledge = new Pledge($this->_adapter);
        $this->getTransactionTable();
        $postedData = array();
        $postedData['userId'] = $userPledgeArr['userId'];
        $checkUserActivePledgeArr = $pledge->getCheckUserActivePledgeArr($postedData);
        $activePledges = $this->getServiceLocator()->get('Pledge\Model\PledgeTable')->checkUserActivePledge($checkUserActivePledgeArr);
        if (!empty($activePledges)) {
            $searchParam = array();
            $pledgeId = $activePledges[0]['pledge_id'];
            $searchParam['pledge_id'] = $pledgeId;
            $pledgeDetailArr = array();
            $searchPledgeTransactionsArr = $pledge->getPledgeTransactionsSearchArr($searchParam);

            $pledgeData = $this->getServiceLocator()->get('Pledge\Model\PledgeTable')->getPledgeTransactions($searchPledgeTransactionsArr);
            if (!empty($pledgeData)) {
                $pledgeDetailArr = array();
                $pledgeDetailArr[] = $pledgeId;
                $pledgeDataResult = $this->getServiceLocator()->get('Pledge\Model\PledgeTable')->getPledgeById($pledgeDetailArr);
                $pledgeDonationAmount = $userPledgeArr['donation_amount'];
                foreach ($pledgeData as $pleData) {
                    if ($pledgeDonationAmount == 0) {
                        break;
                    }
                    if ($pleData['status'] == 0 && $pledgeDataResult['pledge_status_id'] != 3) {
                        if ($pledgeDonationAmount >= $pleData['amount_pledge']) {
                            $pledgeDonationAmount = $pledgeDonationAmount - $pleData['amount_pledge'];
                            /* Update pledge transaction status */
                            $updatePledgeStatusArray = array();
                            $updatePledgeStatusArray['pledge_transaction_id'] = $pleData['pledge_transaction_id'];
                            $updatePledgeStatusArray['status'] = '1';
                            $updatePledgeStatusArray['transaction_id'] = $userPledgeArr['transaction_id'];
                            $updatePledgeStatusArray['modify_by'] = $this->auth->getIdentity()->crm_user_id;
                            $updatePledgeStatusArray['modify_date'] = DATE_TIME_FORMAT;
                            $updatePledgeTransactionStatusArr = $pledge->updatePledgeTransactionStatusArr($updatePledgeStatusArray);
                            $pledgeTransactionId = $this->getServiceLocator()->get('Pledge\Model\PledgeTable')->updatePledgeTransactionStatus($updatePledgeTransactionStatusArr);
                            /* End update pledge transaction status */
                        } else {
                            $updatedAmountToBePaid = $pleData['amount_pledge'] - $pledgeDonationAmount;
                            $updatePledgeAmountArray = array();
                            $updatePledgeAmountArray['pledge_transaction_id'] = $pleData['pledge_transaction_id'];
                            $updatePledgeAmountArray['amount_pledge'] = $pledgeDonationAmount;
                            $updatePledgeAmountArray['remaining_amount_pledge'] = $updatedAmountToBePaid;
                            $updatePledgeAmountArray['status'] = '1';
                            $updatePledgeAmountArray['transaction_id'] = $userPledgeArr['transaction_id'];
                            $updatePledgeAmountArray['modify_by'] = $this->auth->getIdentity()->crm_user_id;
                            $updatePledgeAmountArray['modify_date'] = DATE_TIME_FORMAT;
                            $updatePledgeTransactionAmountArr = $pledge->updatePledgeTransactionAmountArr($updatePledgeAmountArray);

                            $pledgeTransactionId = $this->getServiceLocator()->get('Pledge\Model\PledgeTable')->updatePledgeTransactionAmount($updatePledgeTransactionAmountArr);
//Update pledge amount to complete
                            break;
                        }
                    }
                }
            }
        }

        /* end  Update activie pledge for user */
    }

    /**
     * This Action is used to get the post total cart
     * @author Icreon Tech-DG
     * @return Array
     * @param Array
     */
    public function posGetTotalCartAction() {
        $this->checkFrontUserAuthenticationAjx();
        $this->getTransactionTable();
        $transaction = new Transaction($this->_adapter);
        $request = $this->getRequest();
        $response = $this->getResponse();
        $applicationConfigIds = $this->_config['application_config_ids'];
        $uSId = $applicationConfigIds['united_state_country_id'];
        if ($request->isPost()) {
            // if (isset($this->auth->getIdentity()->user_id) and $this->auth->getIdentity()->user_id != "") {
            if ($request->getPost('ship_m') != '' && $request->getPost('status') != 'error') {
                $paramsArray = $request->getPost()->toArray();


                if (isset($paramsArray['is_apo_po']) && $paramsArray['is_apo_po'] == '3') {
                    $paramsArray['shipping_state'] = $paramsArray['apo_po_state'];
                    $paramsArray['shipping_country'] = $paramsArray['apo_po_shipping_country'];
                } else if (isset($paramsArray['shipping_country']) && $paramsArray['shipping_country'] == $uSId) {
                    $paramsArray['shipping_state'] = $paramsArray['shipping_state_id'];
                    if ($paramsArray['is_apo_po'] == '2') {
                        $paramsArray['shipping_country'] = $paramsArray['apo_po_shipping_country'];
                    }
                }
                if ($paramsArray['pick_up'] == 1) {
                    $paramsArray['shipping_method'] = '';
                    $paramsArray['shipping_state'] = '';
                }

                $paramsArray['postData'] = $paramsArray;
            }
            //$paramsArray['userid'] = $this->auth->getIdentity()->user_id;
            $paramsArray['userid'] = session_id();
            $paramsArray['shipcode'] = $request->getPost('shipcode');
            $paramsArray['couponcode'] = trim($request->getPost('couponcode'));

            $searchResult = $this->cartTotalWithTax($paramsArray);
            $posTransactionSession = new Container('posTransactionSession');
            $posTransactionSession->posTransactionData['transaction_details'] = $searchResult;

            $response->setContent(\Zend\Json\Json::encode($searchResult));
            return $response;
        }
        //}
    }

     /**
     * This Action is used for placing the final order
     * @author Icreon Tech-SR
     * @return Array
     * @param Array
     */
    function posPlaceOrderAction() {
        $this->checkFrontUserAuthentication();
        $this->getTransactionTable();
        $this->layout('wohlayout');
        $msgArr = array_merge($this->_config['transaction_messages']['config']['checkout_front'], $this->_config['transaction_messages']['config']['create_crm_transaction']);
        $transaction = new Transaction($this->_adapter);
        $inventoryForm = new InventoryForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $posTransactionSession = new Container('posTransactionSession'); /* Pos session */
        
        $applicationConfigIds = $this->_config['application_config_ids'];
        $uSId = $applicationConfigIds['united_state_country_id'];
        if ($request->isPost()) { 
        
            $addressInformationId = 0;
            $creditCardNo = '';
            $posTransactionSession->posTransactionData['user_details']['billing_zip_code'] = $request->getPost('zipcode');
            $posTransactionSession->posTransactionData['user_details']['billing_title'] = $posTransactionSession->posTransactionData['user_details']['shipping_title'];
            $posTransactionSession->posTransactionData['user_details']['billing_first_name'] = $posTransactionSession->posTransactionData['user_details']['shipping_first_name'];
            $posTransactionSession->posTransactionData['user_details']['billing_last_name'] = $posTransactionSession->posTransactionData['user_details']['shipping_last_name'];
            
            
            
            $postData = $posTransactionSession->posTransactionData['user_details'];
            $arrPostData = $postData;
            $shipcode = isset($postData['shipping_zip_code']) ? $postData['shipping_zip_code'] : '';
            $user_id = session_id();
            $cartParam['user_id'] = $user_id;
            $cartParam['source_type'] = 'frontend';
            $paramArray['postData'] = $arrPostData;
            $paramArray['shipping_method'] = $arrPostData['shipping_method'];
            $paramArray['userid'] = $user_id;
            $paramArray['shipcode'] = $shipcode;
            $paramArray['couponcode'] = !empty($postData['couponcode']) ? $postData['couponcode'] : '';

            //if ($arrPostData['pick_up'] == 1) {
            //    $paramArray['shipping_state'] = '';
            //}
            //$itemsTotal = $this->cartTotalWithTax($paramArray);
            $itemsTotal = $posTransactionSession->posTransactionData['transaction_details'];
        }

        $postData['promotion_id'] = $promotionId;
        $postData['source_type'] = 'frontend';
        $postData['source_all'] = 'all';
        $postData['pick_up'] = isset($postData['pick_up']) ? $postData['pick_up'] : '0';

        /*if (isset($postData['cash_payment']) && $postData['cash_payment'] == '1') {
            $postData['payment_mode_cash'] = 1;
            $postData['payment_mode_cash_amount'] = $itemsTotal['finaltotal'];
        } else {
            $postData['payment_mode_credit'] = 1;
            $postData['payment_mode_credit_card_amount'] = $itemsTotal['finaltotal'];
        }*/
        $postData['payment_mode_credit'] = 1;
        $postData['payment_mode_credit_card_amount'] = $itemsTotal['finaltotal'];
        $paymentInfo = array();
        $postData['transaction_ids'] = 'WOH' . time();
        $postData['transaction_amounts'] = $itemsTotal['finaltotal'];
        $postData['customer_profile_ids'] = ''; //$responseMessage['customerProfileId'];
        $postData['coupon_code'] = $paramArray['couponcode'];
        
        $postData['is_apo_po'] = $postData['is_apo_po_record'];
        if (isset($postData['is_apo_po']) && $postData['is_apo_po'] == '3') {
            $postData['shipping_state'] = $postData['apo_po_state'];
            $postData['shipping_country'] = $postData['apo_po_shipping_country'];
        } else if (isset($postData['shipping_country']) && $postData['shipping_country'] == $uSId) {
            $postData['shipping_state'] = $postData['shipping_state_id'];
            if ($postData['is_apo_po'] == '2') {
                $postData['shipping_country'] = $postData['apo_po_shipping_country'];
            }
        }
        

        $transactionId = '';
		/* Create new user account*/
            $isEmailExist = false;
            $pos = new PosManagement($this->_adapter);
	        $postParam = $postData;
            if (!empty($postParam['email_id']))
                $isEmailExist = $this->_posTable->isEmailExits(array('email_id' => trim($postParam['email_id'])));		

			if ($isEmailExist) {
			    $userParam = array();
                $userParam['email_id'] = trim($postParam['email_id']);
                $userResult = $this->_posTable->getUser($userParam); 
                if (!empty($userResult) && count($userResult) > 0) {
                    $user_id = $userResult['user_id'];
                    $addressSearchArray['user_id'] = $user_id;
                    $addressSearchArray['address_location'] = '2';
                    $addressDetail = $this->_posTable->getUserAddressInformations($addressSearchArray);
                    $billingAdd = $this->getPrimaryUserAddress($addressDetail);
                    $userGroupSearchParam = array();
                    $userGroupSearchParam['user_id'] = $user_id;
                    $promoResult = $this->_posTable->getUserInGroup($userGroupSearchParam);
                    if (count($promoResult) > 0)
                        $groupId = $promoResult[0]['group_id'];

                    if (!empty($billingAdd['primary_billing'])) {
                        $addKey = $billingAdd['primary_billing'][0];
                    } else if (!empty($billingAdd['billing'])) {
                        $addKey = $billingAdd['billing'][0];
                    } else {
                        $addKey = "";
                    }
                    $userBillingAddress = array();
                    $userBillingAddress = $addressDetail[$addKey];
                    $userDetailsData = $userBillingAddress;
                }                               
            } else {
                    $postUserParam = array();

					if (empty($postParam['email_id'])) {
	                    $postUserParam['username'] = substr(trim(preg_replace('/\s+/', '', $postParam['shipping_first_name'])),0,5) . "-" . $this->GetRandomAlphaNumeric(5);
	                    $postUserParam['no_email'] = 0;
	                } else {
	                    $postUserParam['username'] = '';
	                    $postUserParam['no_email'] = 1;
	                }                    
                    $postUserParam['contact_type'] = 1;
                    $postUserParam['first_name'] = $postParam['shipping_first_name'];
                    $postUserParam['last_name'] = $postParam['shipping_last_name'];
                    $postUserParam['email'] = $postParam['email_id'];
                    $postUserParam['password'] = $postParam['password'];
                    $postUserParam['salt'] = $postParam['salt'];
                    $postUserParam['title'] = $postParam['shipping_title'];
                    $postUserParam['login_enable'] = 1;
                    $postUserParam['add_date'] = DATE_TIME_FORMAT;
                    $postUserParam['modified_date'] = DATE_TIME_FORMAT;
                    $postUserParam['verify_code'] = $postParam['verify_code'];
                    $postUserParam['source_id'] = 2; // ellis

                        $user_id = $this->_posTable->insertWohContact($postUserParam);
		                if ($user_id) {
		                    $this->_posTable->updateContactId(array('user_id' => $user_id));

							$membershipDetailArr = array();
							$membershipDetailArr[] = 1;
							$getMemberShip = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
							$userMembershipData = array();
							$userMembershipData['user_id'] = $user_id;
							$userMembershipData['membership_id'] = $getMemberShip['membership_id'];

							$userMembershipInfo = $this->getServiceLocator()->get('UserMembership')->getUserMembership($getMemberShip);
							$userMembershipData['membership_date_from'] = $userMembershipInfo['membership_date_from'];
							$userMembershipData['membership_date_to'] = $userMembershipInfo['membership_date_to'];
							$this->_posTable->saveUserMembership($userMembershipData);
		
		                    $addressForm = array();
		                    $addressForm['first_name'] = $postParam['shipping_first_name'];
		                    $addressForm['last_name'] = $postParam['shipping_last_name'];
		                    $addressForm['addressinfo_location_type'] = '5'; // Home
		                    $addressForm['addressinfo_street_address_1'] = $postParam['shipping_address_1'];
		                    $addressForm['addressinfo_city'] = $postParam['shipping_city'];
		                    $addressForm['addressinfo_zip'] = $postParam['shipping_zip_code'];
		                    $addressForm['addressinfo_street_address_2'] = $postParam['shipping_address_2'];
		                    $addressForm['addressinfo_state'] = $postParam['shipping_state'];
		                    $addressForm['addressinfo_country'] = $postParam['shipping_country'];
		                    $addressForm['address_type'] = '1'; // Residential
		                    $addressForm['add_date'] = DATE_TIME_FORMAT;
		                    $addressForm['modified_date'] = DATE_TIME_FORMAT;
		                    $addressForm['user_id'] = $user_id;
		                    $addressForm['addressinfo_location'] = '1,2'; // primary and billing
		                    $this->_posTable->insertAddressInfo($addressForm); // insert address information   
		                    $this->_posTable->insertDefaultContactCommunicationPreferences(array('user_id' => $user_id, 'add_date' => $postParam['add_date']));
		
		                    if (!empty($postParam['email_id'])) {
		                        $userDataArr = $this->getServiceLocator()->get('Kiosk\Model\PosTable')->getUser(array('user_id' => $user_id));
		                        $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
		                        $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
		                        $userDataArr['genrated_password'] = $postParam['genrated_password'];
		                        $signup_email_id_template_int = 45;
		                        $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $signup_email_id_template_int, $this->_config['email_options']);
		                    }
		                }
                /** insert into the change log */
                $changeLogArray = array();
                $changeLogArray['table_name'] = 'tbl_usr_change_logs';
                $changeLogArray['activity'] = '1';/** 1 == for insert */
                $changeLogArray['id'] = $user_id;
                $changeLogArray['crm_user_id'] = $user_id;
                $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                $this->_posTable->insertChangeLog($changeLogArray);
            }
		
		/* End create new user account*/
		$postData['user_id'] = $user_id;
		
		// update the user id in cart table. //tbl_tra_cart_bio_certificates //tbl_tra_cart_products
		$cartUpdateParam = array();
		$cartUpdateParam['user_id'] = $user_id;
		$cartUpdateParam['session_id'] = session_id();
		$this->_transactionTable->updateCartTableUserId($cartUpdateParam);
		
		
        $res = $this->saveFinalTransaction($postData);
        $transactionId = $res['transaction_id'];
        $paymentMethod = "credit_card";

        if (isset($res['status']) && $res['status'] == 'payment_error') {

            return $response->setContent(\Zend\Json\Json::encode($res));
            exit;
        }
        if (isset($res['quantity_error']) && $res['quantity_error'] == '1') {

            return $response->setContent(\Zend\Json\Json::encode($res));
            exit;
        }
        if (isset($res['status']) && $res['status'] == 'success') {
            
            if(!empty($posTransactionSession->posTransactionData['user_details']['email_id']))
                $isEmailGiven = $this->Encrypt('yes');
            else    
                $isEmailGiven = $this->Encrypt('no');
            $location = SITE_URL . '/pos-transaction-thanks/'.$isEmailGiven.'/'.$this->Encrypt($res['transaction_id']);
            
            $posTransactionSession->posTransactionData = array();
            header('Location: ' . $location);
            //echo '<script>window.location.href="' . $location . '";</script>';
            die;   

        }
        
        $contactDetail = array();
        $userArr = array('user_id' => $user_id);
        $userEmailId = '';
        $contactDetail = $this->getServiceLocator()->get('User\Model\UserTable')->getUser($userArr);

        if (!empty($contactDetail))
            $userEmailId = $contactDetail['email_id'];
		
		//$posTransactionSession->posTransactionData = array();
        //$passengerSavedSearch = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getUserPassengerSearch(array('user_id' => $user_id));
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => array_merge($msgArr, $this->_config['transaction_messages']['config']['common_message']),
            'postdata' => $postData,
            'userEmailId' => $userEmailId,
            'finalTotal' => $itemsTotal['finaltotal'],
            //'passengerSavedSearch' => $passengerSavedSearch,
            'transactionId' => $transactionId,
            'paymentMethod' => $paymentMethod
        ));
        return $viewModel;
        //}
    }

    public function barcodeAction() {
        $params = $this->params()->fromRoute();
        $searchParam['content'] = $params['content'];
        $barcodeOptions = array('text' => $searchParam['content']);
        $rendererOptions = array();
// $filename = 'barcode_'.date('Y-m-d').'_'.$params['content'];
// $img_file_name = $this->_config['file_upload_path']['assets_upload_dir'] . 'temp/' . $fileName;
        Barcode::factory('code39', 'image', $barcodeOptions, $rendererOptions)->render();
    }

    public function barcodeImage($params) {
        $searchParam['content'] = $params;
        $barcodeOptions = array('text' => $searchParam['content']);
        $rendererOptions = array();
        $fileName = 'barcode_' . date('Y-m-d') . '_' . $searchParam['content'] . '.png';
        $img_file_name = $this->_config['file_upload_path']['assets_upload_dir'] . 'temp/' . $fileName;

        return Barcode::factory('code39', 'image', $barcodeOptions, $rendererOptions)->makeRenderer();
    }

   
    public function addtocartmanifestfront($postArr) {
        $manifestids = explode(",", $postArr['manifestids']);
        $shipIdArray = array();
        if (isset($postArr['ship_id']) && !empty($postArr['ship_id'])) {
            $shipIdArray = explode(",", $postArr['ship_id']);
        }
        $sm = $this->getServiceLocator();
        $passengerTable = $sm->get('Passenger\Model\PassengerTable');

        foreach ($manifestids as $key => $image_name) {
            $isManifest = $postArr['is_manifest'];
            if ($isManifest != 0 && !empty($isManifest)) {
                $quantity = (isset($postArr['quantity']) && !empty($postArr['quantity'])) ? $postArr['quantity'] : '1';
                $flag = $postArr['flag'];
                if (isset($postArr['old_manifest']) && !empty($postArr['old_manifest'])) {
                    $searchParam['id'] = $image_name;
                    $manifestResult = array();
                    $manifestResult = $passengerTable->getManifestDetails($searchParam);

                    $paramsArr = array();
                    $paramsArr['product_type_id'] = 7;
                    $paramsArr['productId'] = $manifestResult[0]['product_id'];
                    $paramsArr['item_id'] = $manifestResult[0]['item_id'];
                    $paramsArr['num_quantity'] = $quantity;
                    $paramsArr['item_type'] = '2';
                    $paramsArr['user_id'] = $postArr['user_id'];
                    $paramsArr['item_name'] = $manifestResult[0]['item_name'];
                    $paramsArr['item_info'] = $manifestResult[0]['item_info'];
                    $paramsArr['manifest_info'] = $manifestResult[0]['manifest_info'];
                    $paramsArr['manifest_type'] = 1;
                    $paramsArr['product_attribute_option_id'] = 3;

                    $addToCartProductId = $this->addInventoryProductToCart($paramsArr);



                    /* $countAdditional = count(explode(",", $manifestResult[0]['additional_images']));
                      $additionalProduct = array();
                      $additionalProduct['cart_product_id'] = $addToCartProductId;
                      $additionalProduct['first_image'] = $manifestResult[0]['first_image'];
                      $additionalProduct['second_image'] = $manifestResult[0]['second_image'];
                      $additionalProduct['first_attribute_option_id'] = $manifestResult[0]['first_attribute_option_id'];
                      $additionalProduct['second_attribute_option_id'] = $manifestResult[0]['second_attribute_option_id'];
                      $additionalProduct['num_additional_images'] = (!empty($countAdditional)) ? count($countAdditional) : 0;
                      $additionalProduct['additional_images'] = $manifestResult[0]['additional_images'];
                      $additionalProduct['user_id'] = $postArr['user_id']; */
// $this->addToCartPassengerManifest($additionalProduct);
                } else {

                    $productMenifestParam = array();
                    $productMenifestParam['productType'] = '7';/** manifest */
                    $productMenifestResult = $passengerTable->getProductAttributeDetails($productMenifestParam);
                    $manifestParam = array();
                    $manifestParam['manifest'] = $manifestids[$key];
                    $manifestResult = $passengerTable->getShipManifestDetails($manifestParam);


                    $paramsArr = array();
                    if (isset($postArr['passenger_id']) && !empty($postArr['passenger_id']))
                        $paramsArr['item_id'] = $postArr['passenger_id'];
                    else if (count($shipIdArray) > 0)
                        $paramsArr['item_id'] = $shipIdArray[$key];


                    $paramsArr['user_id'] = $postArr['user_id'];
                    $paramsArr['product_type_id'] = 7;
                    $paramsArr['productId'] = $productMenifestResult[0]['product_id'];
                    $paramsArr['item_type'] = '2';
                    $paramsArr['user_id'] = $postArr['user_id'];
                    $paramsArr['item_name'] = $manifestResult[0]['SHIP_NAME'];
                    $paramsArr['item_info'] = substr($manifestResult[0]['ARRIVAL_DATE'], 0, 10);
                    $paramsArr['manifest_info'] = $manifestids[$key];
                    $paramsArr['manifest_type'] = 1;
                    $paramsArr['product_attribute_option_id'] = 3;

                    $addToCartProductId = $this->addInventoryProductToCart($paramsArr);

                    /* Add to cart additional product */
                    /*
                      $additionalProduct = array();
                      $additionalProduct['cart_product_id'] = $addToCartProductId;
                      $additionalProduct['first_image'] = $firstImageName;
                      $additionalProduct['second_image'] = $secondImageName;
                      $additionalProduct['first_attribute_option_id'] = $firstImageOptionId;
                      $additionalProduct['second_attribute_option_id'] = $secondImageOptionId;
                      $additionalProduct['num_additional_images'] = (!empty($additionalImage)) ? count($additionalImage) : 0;
                      $additionalProduct['additional_images'] = $additionalImageName;
                      $additionalProduct['user_id'] = $postArr['user_id'];

                      $this->addToCartPassengerManifest($additionalProduct); */

                    /* Add to cart additional product */
                }
            }
        }
    }

   
    /**
     * This function is used to save pledge Transaction
     * @param Void
     * @return String
     * @author Icreon Tech - DT
     */
    public function savePledgeTransaction($postArr) {

        $pledge = new Pledge($this->_adapter);
        $pledgeTransData = $this->getServiceLocator()->get('Pledge\Model\PledgeTable')->getPledgeTransactionsById('', '', $postArr['pledge_transaction_id']);

        $dateTime = DATE_TIME_FORMAT;

        $crmUserId = !(empty($this->auth->getIdentity()->crm_user_id)) ? $this->auth->getIdentity()->crm_user_id : '';
        /* Add transaction in main table */
        $finalTransaction = array();
        $finalTransaction['user_id'] = $pledgeTransData[0]['user_id'];
        $finalTransaction['transaction_source_id'] = $this->_config['transaction_source']['transaction_source_id'];
        $finalTransaction['num_items'] = 1;
        $finalTransaction['sub_total_amount'] = $pledgeTransData[0]['amount_pledge'];
        $finalTransaction['shipping_amount'] = 0;
        $finalTransaction['total_tax'] = 0;
        $finalTransaction['transaction_amount'] = $pledgeTransData[0]['amount_pledge'];
        $finalTransaction['transaction_date'] = $dateTime;
        $finalTransaction['transaction_type'] = '1';
        $finalTransaction['added_by'] = $crmUserId;
        $finalTransaction['added_date'] = $dateTime;
        $finalTransaction['modify_by'] = $crmUserId;
        $finalTransaction['modify_date'] = $dateTime;
        if (!empty($postArr['pledge_transaction_id'])) {
            $finalTransaction['don_amount'] = $finalTransaction['transaction_amount'];
        }
        $transaction = new Transaction($this->_adapter);
        $finalTransactionArr = $transaction->getFinalTransactionArr($finalTransaction);

        $transactionId = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->saveFinalTransaction($finalTransactionArr);

        /* End add transaction in main table */
        /* Add Donation Product */
        $donationProductData = array();
        $donationProductData['transaction_id'] = $transactionId;
        $donationProductData['pledge_transaction_id'] = $pledgeTransData[0]['pledge_transaction_id'];
        $donationProductData['is_pledge_transaction'] = '1';
        $donationProductData['product_mapping_id'] = 1;
        $donationProductData['product_type_id'] = 2;
        $donationProductData['product_price'] = $pledgeTransData[0]['amount_pledge'];
        $donationProductData['product_sub_total'] = $pledgeTransData[0]['amount_pledge'];
        $donationProductData['product_discount'] = 0;
        $donationProductData['product_tax'] = 0;
        $donationProductData['product_total'] = $pledgeTransData[0]['amount_pledge'];
        $donationProductData['product_status_id'] = 10;
        $donationProductData['purchase_date'] = $dateTime;
        $donationProductData['is_deleted'] = 0;
        $donationProductData['added_by'] = $crmUserId;
        $donationProductData['added_date'] = $dateTime;
        $donationProductData['modify_by'] = $crmUserId;
        $donationProductData['modified_date'] = $dateTime;
        $donationProductData['user_id'] = $pledgeTransData[0]['user_id'];
        $donationProductData['num_quantity'] = 1;

        $productSearchParam['id'] = 8;/** product Donetion */
        $productResultArr = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProductInfo($productSearchParam);
        $donationProductData['product_sku'] = $productResultArr[0]['product_sku'];
        $donationProductData['product_name'] = $productResultArr[0]['product_name'];

        $donationProductArray = $transaction->getDonationProductArray($donationProductData);
        $donationProductId = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->saveTransactionDonationProdcts($donationProductArray);
        /* End add Donation Product */


        /* Update pledge transaction status */
        $updatePledgeStatusArray = array();
        $updatePledgeStatusArray['pledge_transaction_id'] = $pledgeTransData[0]['pledge_transaction_id'];
        $updatePledgeStatusArray['status'] = '1';
        $updatePledgeStatusArray['transaction_id'] = $transactionId;
        $updatePledgeStatusArray['modify_by'] = $crmUserId;
        $updatePledgeStatusArray['modify_date'] = $dateTime;
        $updatePledgeTransactionStatusArr = $pledge->updatePledgeTransactionStatusArr($updatePledgeStatusArray);
        $pledgeTransactionId = $this->getServiceLocator()->get('Pledge\Model\PledgeTable')->updatePledgeTransactionStatus($updatePledgeTransactionStatusArr);
        /* End update pledge transaction status */

        $userPurchasedProductArr = array();
        $userPurchasedProductArr['user_id'] = $pledgeTransData[0]['user_id'];

        $date_range = $this->getDateRange(4);
        $userPurchasedProductArr['from_date'] = $this->DateFormat($date_range['from'], 'db_date_format_from');
        $userPurchasedProductArr['to_date'] = $this->DateFormat($date_range['to'], 'db_date_format_to');
        $getTotalUserPurchaseProductAmountArr = $transaction->getTotalUserPurchaseProductAmountArr($userPurchasedProductArr);
        $totalAmountPurchased = $this->_transactionTable->getTotalUserPurchased($getTotalUserPurchaseProductAmountArr);

        $userMembershipArr = array();
        $userMembershipArr['transaction_amount'] = $totalAmountPurchased;
        $userMembershipArr['donation_amount'] = $pledgeTransData[0]['amount_pledge'];
        $userMembershipArr['user_id'] = $pledgeTransData[0]['user_id'];
        $userMembershipArr['transaction_id'] = $transactionId;
        $userMembershipArr['transaction_date'] = $dateTime;

        $this->updateUserMembership($userMembershipArr);
        return $transactionId;
    }


    /**
     * This action is used to add more notify
     * @param void
     * @return void
     * @author Icreon Tech - DT
     */
    public function posShippingMethodFrontAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getTransactionTable();
        $createTransactionMessage = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $transaction = new Transaction($this->_adapter);
        $applicationConfigIds = $this->_config['application_config_ids'];
        $uSId = $applicationConfigIds['united_state_country_id'];
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $createTransactionForm = new CheckoutFormFrontWoh();
            $postArr = $request->getPost()->toArray();

            /* if (isset($postArr['is_apo_po']) && $postArr['is_apo_po'] == '1') {
              $getShippingMethods = $this->_config['transaction_config']['usps_iop_config_info']['shippings_types'];
              } else if (isset($postArr['shipping_country']) && $postArr['shipping_country'] == 228) {
              $getShippingMethods = $this->_config['transaction_config']['domestic_config_info']['shippings_types'];
              } else {
              $getShippingMethods = $this->_config['transaction_config']['international_config_info']['shippings_types'];
              }

              $shippingMethods = array();
              if (!empty($getShippingMethods)) {
              foreach ($getShippingMethods as $shippingMet) {
              $key = $shippingMet['carrier_id'] . ', ' . $shippingMet['service_type'];
              $webSelection = (!empty($shippingMet['web_selection'])) ? ' ( ' . $shippingMet['web_selection'] . ' )' : '';
              $shippingMethods[$key] = $shippingMet['description'] . $webSelection;
              }
              }

              $createTransactionForm->get('shipping_method')->setAttribute('options', $shippingMethods);
             * 
             */
            $shippingMethodInter = '';

            $postArr['is_apo_po'] = $postArr['is_apo_po_record'];
            if (isset($postArr['is_apo_po']) && $postArr['is_apo_po'] != '0' && $postArr['is_apo_po'] != '1') {
//$getShippingMethods = $this->_config['transaction_config']['usps_iop_config_info']['shippings_types'];
                $shippingMetArr = array();
                $shippingMetArr['pb_shipping_id'] = $applicationConfigIds['apo_po_pb_id'];
                $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                $getShippingMethods = $this->_transactionTable->getShippingMethod($shippingMethodArr);
            } else if (isset($postArr['shipping_country']) && $postArr['shipping_country'] == $uSId && $postArr['is_apo_po'] == '1') {
//$getShippingMethods = $this->_config['transaction_config']['domestic_config_info']['shippings_types'];
                $shippingMetArr = array();
                $shippingMetArr['pb_shipping_id'] = $applicationConfigIds['domestic_pb_id'];
                $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                $getShippingMethods = $this->_transactionTable->getShippingMethod($shippingMethodArr);
            } else {
                $cartProductArr = array();
                $cartProductArr['user_id'] = $postArr['user_id'];
                $cartProductArr['userid'] = $postArr['user_id'];
                $cartProductArr['source_type'] = $postArr['source_type'];
                $cartProductsWeight = $this->cartTotalWithTax($cartProductArr);
                /* $totalWeight = 0;
                  if (!empty($cartProductsWeight)) {
                  foreach ($cartProductsWeight as $cartProductWei) {
                  if ($cartProductWei['product_type_id'] == 1) {
                  $totalWeight+=$cartProductWei['product_weight'];
                  }
                  }
                  } */
                $totalWeight = (isset($cartProductsWeight['totalweight']) && $cartProductsWeight['totalweight'] > 0) ? $cartProductsWeight['totalweight'] : '0.00';
                $shippingMetArr = array();
                $shippingMetArr['pb_shipping_id'] = $applicationConfigIds['internation_pb_id'];
                $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                $getShippingMethodsInter = $this->_transactionTable->getShippingMethod($shippingMethodArr);
//$lbsCheck = $this->_config['transaction_config']['international_config_info']['lbs_check'];
                $lbsCheck = $getShippingMethodsInter[0]['lbs_check'];
                if ($totalWeight <= $lbsCheck) {
//$getShippingMethods = $this->_config['transaction_config']['international_config_info']['shipping_types_first_class'];
                    foreach ($getShippingMethodsInter as $shipMethod) {
                        if ($shipMethod['pb_shipping_type_id'] == $applicationConfigIds['internation_less_than_lbs_id']) {
                            $getShippingMethods[] = $shipMethod;
                            $postArr['shipping_method'] = $shipMethod['carrier_id'] . ', ' . $shipMethod['service_type'] . ', ' . $shipMethod['pb_shipping_type_id'];
                        }
                    }
                } else {
//$getShippingMethods = $this->_config['transaction_config']['international_config_info']['shipping_types_priority'];
                    foreach ($getShippingMethodsInter as $shipMethod) {
                        if ($shipMethod['pb_shipping_type_id'] == $applicationConfigIds['internation_greater_than_lbs_id']) {
                            $getShippingMethods[] = $shipMethod;
                            $postArr['shipping_method'] = $shipMethod['carrier_id'] . ', ' . $shipMethod['service_type'] . ', ' . $shipMethod['pb_shipping_type_id'];
                        }
                    }
                }
                //$postArr['shipping_method'] = $getShippingMethods[0]['carrier_id'] . ', ' . $getShippingMethods[0]['service_type'] . ', ' . $getShippingMethods[0]['pb_shipping_type_id'];
                $shippingMethodInter = $postArr['shipping_method'];
            }

            $shippingMethods = array();
            if (!empty($getShippingMethods)) {
                foreach ($getShippingMethods as $shippingMet) {
                    $key = $shippingMet['carrier_id'] . ', ' . $shippingMet['service_type'] . ', ' . $shippingMet['pb_shipping_type_id'];
                    $webSelection = (!empty($shippingMet['web_selection'])) ? ' ' . $shippingMet['web_selection'] . ' ' : '';
                    $shippingMethods[$key] = $webSelection;
                }
            }
            $createTransactionForm->get('shipping_method')->setAttribute('options', $shippingMethods);
            $createTransactionForm->get('shipping_method')->setValue($shippingMethodInter);

            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => $createTransactionMessage,
                'createTransactionForm' => $createTransactionForm,
				'shippingMethods' => $shippingMethods
            ));
            return $viewModel;
        } else {
//  return $this->redirect()->toRoute('get-crm-transactions');
        }
    }

    /**
     * This action is used to get shipping method
     * @param void
     * @return void
     * @author Icreon Tech - DT
     */
    public function getPosShippingMethodAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $applicationConfigIds = $this->_config['application_config_ids'];
        $uSId = $applicationConfigIds['united_state_country_id'];
        if ($request->isXmlHttpRequest()) {
            $this->getTransactionTable();
            $createTransactionMessage = $this->_config['transaction_messages']['config']['create_crm_transaction'];
            $createTransactionForm = new CreateTrasactionForm();
            $transaction = new Transaction($this->_adapter);
            $postArr = $request->getPost()->toArray();
            /* Shipping methods */

            if (isset($postArr['is_apo_po']) && $postArr['is_apo_po'] == '1') {
//$getShippingMethods = $this->_config['transaction_config']['usps_iop_config_info']['shippings_types'];
                $shippingMetArr = array();
                $shippingMetArr['pb_shipping_id'] = $applicationConfigIds['apo_po_pb_id'];
                $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                $getShippingMethods = $this->_transactionTable->getShippingMethod($shippingMethodArr);
            } else if (isset($postArr['shipping_country']) && $postArr['shipping_country'] == $uSId) {
//$getShippingMethods = $this->_config['transaction_config']['domestic_config_info']['shippings_types'];
                $shippingMetArr = array();
                $shippingMetArr['pb_shipping_id'] = $applicationConfigIds['domestic_pb_id'];
                $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                $getShippingMethods = $this->_transactionTable->getShippingMethod($shippingMethodArr);
            } else {
                $cartProductArr = array();
                $cartProductArr['user_id'] = $postArr['user_id'];
                $cartProductArr['userid'] = $postArr['user_id'];
                $cartProductArr['source_type'] = 'backend';
                $cartProductsWeight = $this->cartTotalWithTax($cartProductArr);
                $totalWeight = 0;
                /* if (!empty($cartProductsWeight)) {
                  foreach ($cartProductsWeight as $cartProductWei) {
                  if ($cartProductWei['product_type_id'] == 1) {
                  $totalWeight+=$cartProductWei['product_weight'];
                  }
                  }
                  } */
                $totalWeight = (isset($cartProductsWeight['totalweight']) && $cartProductsWeight['totalweight'] > 0) ? $cartProductsWeight['totalweight'] : '0.00';
                $shippingMetArr = array();
                $shippingMetArr['pb_shipping_id'] = $applicationConfigIds['internation_pb_id'];
                $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                $getShippingMethodsInter = $this->_transactionTable->getShippingMethod($shippingMethodArr);
//$lbsCheck = $this->_config['transaction_config']['international_config_info']['lbs_check'];
                $lbsCheck = $getShippingMethodsInter[0]['lbs_check'];
                if ($totalWeight <= $lbsCheck) {
//$getShippingMethods = $this->_config['transaction_config']['international_config_info']['shipping_types_first_class'];
                    foreach ($getShippingMethodsInter as $shipMethod) {
                        if ($shipMethod['pb_shipping_type_id'] == $applicationConfigIds['internation_less_than_lbs_id']) {
                            $getShippingMethods[] = $shipMethod;
                        }
                    }
                } else {
//$getShippingMethods = $this->_config['transaction_config']['international_config_info']['shipping_types_priority'];
                    foreach ($getShippingMethodsInter as $shipMethod) {
                        if ($shipMethod['pb_shipping_type_id'] == $applicationConfigIds['internation_greater_than_lbs_id']) {
                            $getShippingMethods[] = $shipMethod;
                        }
                    }
                }
                $postArr['shipping_method'] = $getShippingMethods[0]['carrier_id'] . ', ' . $getShippingMethods[0]['service_type'] . ', ' . $getShippingMethods[0]['pb_shipping_type_id'];
                $shippingMethodInter = $postArr['shipping_method'];
            }

            $shippingMethods = array();
            if (!empty($getShippingMethods)) {
                foreach ($getShippingMethods as $shippingMet) {
                    $key = $shippingMet['carrier_id'] . ', ' . $shippingMet['service_type'] . ', ' . $shippingMet['pb_shipping_type_id'];
                    $webSelection = (!empty($shippingMet['web_selection'])) ? ' ' . $shippingMet['web_selection'] . ' ' : '';
                    $shippingMethods[$key] = $webSelection;
                }
            }
            $createTransactionForm->get('shipping_method')->setAttribute('options', $shippingMethods);
            $createTransactionForm->get('shipping_method')->setValue($postArr['shipping_method']);
            /* End Shipping methods */
            /* End Product Copon discount */
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => array_merge($createTransactionMessage, $this->_config['transaction_messages']['config']['common_message']),
                'createTransactionForm' => $createTransactionForm,
                'isPickUp' => (!empty($postArr['pick_up'])) ? $postArr['pick_up'] : '0'
            ));
            return $viewModel;
        }
    }

   
    public function letterSavePdf($params = array(), $mode) {
        error_reporting(0);
        $this->getTransactionTable();
        $messages = $this->_config['transaction_messages']['config']['view_crm_transaction'];
        $this->layout('popup');
        $search['woh_id'] = $params['product_id'];
        $searchResult = $this->getServiceLocator()->get('User\Model\UserTable')->getWoh($search);
        $searchResult = $searchResult[0];
        $pdfPath = $this->_config['file_upload_path']['assets_upload_dir'] . "transaction/shipment_" . $this->encrypt($params['shipmentId']);
        $str = '<div style="width:85%; padding-top:235px; padding-left:80px; padding-right:20px; font-size:15px; font-family:Arial; line-height:19px;">
    <div>
        <div>
            <div>
                <div><div>' . $this->DateFormat($searchResult['purchase_date'], 'displaymonth') . '</div><div style=" margin-top:6px;">';
        if (trim($searchResult['title']) != '')
            $str.=$searchResult['title'] . '&nbsp';

        $str.=$searchResult['full_name'] . '</div>';

        if (!empty($searchResult['address']) or !empty($searchResult['city']) or !empty($searchResult['state']) or !empty($searchResult['zip_code']) or !empty($searchResult['user_country'])) {

            if (!empty($searchResult['address'])) {
                $str.= '<div style="width:50%;">' . $searchResult['address'] . '</div>';
            }

            if (!empty($searchResult['city']) or !empty($searchResult['state']) or !empty($searchResult['zip_code'])) {

                $str.= '<div style="width:50%;">';

                if (!empty($searchResult['city'])) {
                    $str.= $searchResult['city'];
                }

                if (!empty($searchResult['state'])) {
                    if (!empty($searchResult['city'])) {
                        $str.= ',' . $searchResult['state'] . ' ';
                    } else {
                        $str.= '' . $searchResult['state'] . ' ';
                    }
                }

                if (!empty($searchResult['zip_code'])) {
                    $str.= $searchResult['zip_code'];
                }

                $str.= '</div>';
            }

            if (!empty($searchResult['user_country'])) {
                $str.= '<div style="width:50%;">' . $searchResult['user_country'] . '</div>';
            }

            $str.= '</div>';
        } else {
            $str.= '</div>';
        }
        $str.= '<div style="padding-top:12px;">' . $messages['T_LETTER_CONTENT'] . '</div>
        <div style="padding-top:-5px;">' . $messages['T_LETTER_CONTENT1'] . '</div>
        <div>
            <div style="margin-top:165px; margin-left:300px;" align="right">' . $messages['YOUR_CONTRIBUTION'] . "<span style='margin-left:100px;'>" . "$ " . $searchResult['product_total'] . '</span></div>
            <div style="padding-top:15px;">';
        if (trim($searchResult['title']) != '')
            $str.=$searchResult['title'] . '&nbsp';

        $str.=$searchResult['full_name'] . '</div>';
        if (!empty($searchResult['address']) or !empty($searchResult['city']) or !empty($searchResult['state']) or !empty($searchResult['zip_code']) or !empty($searchResult['user_country'])) {

            if (!empty($searchResult['address'])) {
                $str.= '<div style="width:50%;">' . $searchResult['address'] . '</div>';
            }

            if (!empty($searchResult['city']) or !empty($searchResult['state']) or !empty($searchResult['zip_code'])) {

                $str.= '<div style="width:50%;">';

                if (!empty($searchResult['city'])) {
                    $str.= $searchResult['city'];
                }

                if (!empty($searchResult['state'])) {
                    if (!empty($searchResult['city'])) {
                        $str.= ',' . $searchResult['state'] . ' ';
                    } else {
                        $str.= '' . $searchResult['state'] . ' ';
                    }
                }

                if (!empty($searchResult['zip_code'])) {
                    $str.= $searchResult['zip_code'];
                }
                $str.= '</div>';
            }

            if (!empty($searchResult['user_country'])) {
                $str.= '<div style="width:50%;">' . $searchResult['user_country'] . '</div>';
            }

            $str.= '</div>';
        } else {
            $str.= '</div>';
        }
        $str.= '<div style="padding-top:20px;">' . $messages['T_LETTER_THANK'] . '</div>
<div style="padding-top:10px; padding-bottom:10px;">
    <label><strong>' . $searchResult['final_name'] . '</strong></label>
    <span style="margin-left:100px;"><strong>' . $searchResult['origin_country'] . '</strong></span>
</div>
<div>' . $messages['T_INFO'] . '</div></div>
    <div style="margin-top:20px; font-size:12px;" align="right"> 0 :' . $searchResult['transaction_id'] . '&nbsp;&nbsp;&nbsp;&nbsp; C :' . $searchResult['contact_id'] . '&nbsp;&nbsp;&nbsp;&nbsp;</div>
</div>
</div>
</div>';
        $format = array(16 * 25.4, 8.5 * 25.4);
        try {
            $html2pdf = new\ HTML2PDF('P', $format, 'en');
            $html2pdf->setDefaultFont('Arial');
            $html2pdf->writeHTML($str, false);
            $html2pdf->Output($pdfPath . "/" . date('Y-m-d') . "_" . $params['product_id'] . "_woh_letter.pdf", $mode);
        } catch (HTML2PDF_exception $e) {
            echo $e;
            exit;
        }
    }

    public function certificateSavePdf($params = array(), $mode) {
        error_reporting(0);
        $this->getTransactionTable();
        $messages = $this->_config['transaction_messages']['config']['view_crm_transaction'];
        $pdfPath = $this->_config['file_upload_path']['assets_upload_dir'] . "transaction/shipment_" . $this->encrypt($params['shipmentId']);
        $search['woh_id'] = $params['product_id'];
        $searchResult = $this->getServiceLocator()->get('User\Model\UserTable')->getWoh($search);
        $searchResult = $searchResult[0];
        $viewModel = new ViewModel();
        $str = '
<div style="padding-top:415px;">
    <div align="center" style="font-family:Times New Roman, Times, serif;  font-size: 20px;  color: #000; font-style:italic; padding-bottom: 10px; margin-left:-30px;">' . $messages['T_CERTIFICATE_1'] . '</div>';
        if ($searchResult['origin_country'] != 'United States of America') {
            if ($searchResult['product_name'] == 'Special Name entry') {
                $str = $str . '<div style="text-align:center; font-size:31px; line-height:36px; font-family:times new roman,Times, serif; font-weight:bold; color;#000;">' . $searchResult['final_name'] . '</div>
    <div align="center" style="font-family:Vijaya, Times New Roman, Times, serif;  font-size: 19px; margin-left:18px;  color: #000; font-style:italic; padding-bottom:5px;">' . $messages['T_CERTIFICATE_2'] . '</div>
    <div align="center" style="font-size: 31px;  font-family:times new roman,Times, serif; font-weight:bold; text-align:center;  color;#000; padding-bottom:5px;">' . $searchResult['origin_country'] . '</div>
    <div align="center" style="font-family:Times New Roman, Times, serif; font-size: 15px; color: #000; padding-left:25px;">' . $messages['T_CERTIFICATE_3'] . '</div>';
            } else {
                $str = $str . '
    <div style="text-align:center; font-size:31px; font-family:times new roman,Times, serif; font-weight:bold; color;#000; padding-bottom: 10px; padding-top:4px;">' . $searchResult['final_name'] . '</div>
    <div align="center" style="font-family:Vijaya, Times New Roman, Times, serif;  font-size: 20px;  color: #000; font-style:italic; padding-bottom:10px; padding-top:10px; padding-left:15px; text-align:center;">' . $messages['T_CERTIFICATE_2'] . '</div>
    <div align="center" style="font-size: 31px;  font-family:times new roman,Times, serif; font-weight:bold; text-align:center;  color;#000; padding-bottom:10px;">' . $searchResult['origin_country'] . '</div>
    <div align="center" style="font-family:Times New Roman, Times, serif; font-size: 15px; color: #000;">' . $messages['T_CERTIFICATE_3'] . '</div>';
            }
        } else {
            if ($searchResult['product_name'] == 'Special Name entry') {
                $str = $str . '
    <div style="text-align:center; font-size:30px; font-family:times new roman,Times, serif; font-weight:bold; color;#000; padding-top: 40px;">' . $searchResult['final_name'] . '</div>
    <div align="center" style="font-family:Times New Roman, Times, serif; font-size: 15px; color: #333; padding-top: 50px;">' . $messages['T_CERTIFICATE_4'] . '</div>';
            } else {
                $str = $str . '
    <div style="text-align:center; font-size:30px; font-family:times new roman,Times, serif; font-weight:bold; color;#000; padding-top: 42px; margin-left:-25px;">' . $searchResult['final_name'] . '</div>
    <div align="center" style="font-family:Times New Roman, Times, serif; font-size: 15px; color: #333; padding-top: 50px; margin-left:-20px;">' . $messages['T_CERTIFICATE_4'] . '</div>';
            }
        }
        $str = $str . '</div>';
        try {
            $html2pdf = new\ HTML2PDF('L', 'A4', 'en');
            $html2pdf->setDefaultFont('Times');
            $html2pdf->writeHTML($str, false);
            $html2pdf->Output($pdfPath . "/" . date('Y-m-d') . "_" . $params['product_id'] . "_woh_ceritificate.pdf", $mode);
        } catch (HTML2PDF_exception $e) {
            echo $e;
            exit;
        }
    }

    public function passengerCertificateSavePdf($params = array()) {
        $pdfPath = $this->_config['file_upload_path']['assets_upload_dir'] . "transaction/shipment_" . $this->encrypt($params['shipmentId']);
        error_reporting(0);
        $this->getTransactionTable();
        $messages = $this->_config['transaction_messages']['config']['view_crm_transaction'];
        $search['product_id'] = $params['product_id'];
        $searchResult = $this->getTransactionTable()->getShipImage($search);
        $searchParam['passenger_id'] = $searchResult[0]['item_id'];
        $searchResult = $this->getServiceLocator()->get('Passenger\Model\DocumentTable')->searchCrmPassenger($searchParam);
        $maritalStatusArray = $this->maritalStatusArray();
        $searchResult[0]['PRIN_MARITAL_STAT'] = (isset($searchResult[0]['PRIN_MARITAL_STAT']) ? $maritalStatusArray[$searchResult[0]['PRIN_MARITAL_STAT']] : "Unknown");
        $genderArray = $this->genderArray();
        $searchResult[0]['PRIN_GENDER_CODE'] = (isset($searchResult[0]['PRIN_GENDER_CODE']) ? $genderArray[$searchResult[0]['PRIN_GENDER_CODE']] : "Unknown");
        $searchResult = $searchResult[0];
//$pdf = new HTML2FPDF();
        $viewModel = new ViewModel();

        $dateArray = date_parse_from_format("Y-m-d", $searchResult['DATE_ARRIVE']);
        if ($searchResult['data_source'] == 2 && ($dateArray['month'] == 1 or $dateArray['month'] == '01') && ($dateArray['day'] == 1 or $dateArray['day'] == '01')) {
            $dateArr = $dateArray['year'];
        } else {
            if ($searchResult['DATE_ARRIVE'] != '' && $searchResult['DATE_ARRIVE'] != '0000-00-00 00:00:00') {
                $dateArr = $this->OutputDateFormat($searchResult['DATE_ARRIVE'], 'displaymonthformat2');
            }
        }
//  $pdf->AddPage();
        $str = '<page>
    <table style="width:100%; padding-top:266px" align="center" cellspacing="5">
        <tr>
            <td style="font-size:14px;"> ' . $messages['FIRST_NAME'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult['PRIN_FIRST_NAME'] . ' </td>
        </tr>
        <tr>
            <td style="font-size:14px;"> ' . $messages['LAST_NAME'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult['PRIN_LAST_NAME'] . ' </td>
        </tr>
        <tr>
            <td style="font-size:14px;"> ' . $messages['NATIONALITY'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult['PRIN_NATIONALITY'] . ' </td>
        </tr>
        <tr>
            <td style="font-size:14px;"> ' . $messages['LAST_RESIDENCE'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult['PRIN_PLACE_RESI'] . ' </td>
        </tr>
        <tr>
            <td style="font-size:14px;"> ' . $messages['DATE_ARRIVAL'] . ' : </td>
            <td style="font-size:14px;"> ' . $dateArr . ' </td>
        </tr>
        <tr>
            <td style="font-size:14px;"> ' . $messages['AGE_ARRIVAL'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult['PRIN_AGE_ARRIVAL'] . ' </td>
        </tr>
        <tr>
            <td style="font-size:14px;"> ' . $messages['GENDER'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult['PRIN_GENDER_CODE'] . ' </td>
        </tr>
        <tr>
            <td style="font-size:14px;"> ' . $messages['MARITAL_STATUS'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult['PRIN_MARITAL_STAT'] . ' </td>
        </tr>
        <tr>
            <td style="font-size:14px;"> ' . $messages['SHIP_TRAVEL'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult['SHIP_NAME'] . ' </td>
        </tr>
        <tr>
            <td style="font-size:14px;"> ' . $messages['PORT_DEPARTURE'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult['DEP_PORT_NAME'] . ' </td>
        </tr>
        <tr>
            <td style="font-size:14px;"> ' . $messages['MANIFEST_LINE_NUMBER'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult['REF_PAGE_LINE_NBR'] . ' </td>
        </tr>
    </table>
    </page>';
        try {
            $html2pdf = new\ HTML2PDF('L', 'A4', 'en');
            $html2pdf->setDefaultFont('Times');
            $html2pdf->writeHTML($str, false);
            $html2pdf->Output($pdfPath . "/" . date('Y-m-d') . "_" . $params['product_id'] . "_passenger_ceritificate.pdf", F);
        } catch (HTML2PDF_exception $e) {
            echo $e;
            exit;
        }
    }

    /**
     * This function is used to insert / update  the billing and shipping address
     * @return json
     * @param void
     * @author Icreon Tech - KK
     */
    public function insertUpdateBillingShipping($formData) {
        if ($formData['address_type'] == 'billing') {
            $form_address['title'] = isset($formData['billing_title']) ? $formData['billing_title'] : null;
            $form_address['first_name'] = isset($formData['billing_first_name']) ? $formData['billing_first_name'] : null;
            $form_address['last_name'] = isset($formData['billing_last_name']) ? $formData['billing_last_name'] : null;

            $form_address['address_id'] = isset($formData['billing_existing_contact']) ? $formData['billing_existing_contact'] : null;
            $form_address['addressinfo_country'] = isset($formData['billing_country']) ? $formData['billing_country'] : null;
            $form_address['addressinfo_zip'] = isset($formData['billing_zip_code']) ? $formData['billing_zip_code'] : null;
            $form_address['addressinfo_state'] = isset($formData['billing_state']) ? $formData['billing_state'] : null;
            $form_address['addressinfo_city'] = isset($formData['billing_city']) ? $formData['billing_city'] : null;
            $form_address['addressinfo_street_address_2'] = isset($formData['billing_address_2']) ? $formData['billing_address_2'] : null;
            if ($formData['source_type'] == 'backend') {

                $form_address['addressinfo_street_address_1'] = isset($formData['billing_address']) ? $formData['billing_address'] : null;
            } else {
                $form_address['addressinfo_street_address_1'] = isset($formData['billing_address_1']) ? $formData['billing_address_1'] : null;
            }

            $form_address['addressinfo_location_type'] = isset($formData['billing_location_type']) ? $formData['billing_location_type'] : null;

            if (!empty($form_address['address_id'])) {
                $form_address['addressinfo_location'] = $form_address['addressinfo_location_type'];
            } else {
                $form_address['addressinfo_location'] = 2;
            }
            if (isset($formData['billing_phone_info_id']) and trim($formData['billing_phone_info_id']) != "" and is_numeric(trim($formData['billing_phone_info_id']))) {
                $paramsPhoneUpdate = array();
                $paramsPhoneUpdate['phone_id'] = trim($formData['billing_phone_info_id']);
                $paramsPhoneUpdate['phone_type'] = "";
                $paramsPhoneUpdate['country_code'] = $formData['billing_country_code'];
                $paramsPhoneUpdate['area_code'] = $formData['billing_area_code'];
                $paramsPhoneUpdate['phone_no'] = $formData['billing_phone'];
                $paramsPhoneUpdate['continfo_primary'] = "";
                $paramsPhoneUpdate['extension'] = "";
                $paramsPhoneUpdate['modified_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('User\Model\UserTable')->updatePhoneInformations($paramsPhoneUpdate);
            } else {
                $paramsPhoneInsert = array();
                $paramsPhoneInsert['user_id'] = $formData['user_id'];
                $paramsPhoneInsert['phone_type'] = "";
                $paramsPhoneInsert['country_code'] = $formData['billing_country_code'];
                $paramsPhoneInsert['area_code'] = $formData['billing_area_code'];
                $paramsPhoneInsert['phone_no'] = $formData['billing_phone'];

                if ($formData['address_type_status'] == "2") {
                    $paramsPhoneInsert['continfo_primary'] = "1";
                } else {
                    $paramsPhoneInsert['continfo_primary'] = "0";
                }

                $paramsPhoneInsert['extension'] = "";
                $paramsPhoneInsert['add_date'] = DATE_TIME_FORMAT;
                $paramsPhoneInsert['modified_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('User\Model\ContactTable')->insertPhoneInformations($paramsPhoneInsert);
            }
        } elseif ($formData['address_type'] == 'shipping') {
            $form_address['title'] = isset($formData['shipping_title']) ? $formData['shipping_title'] : null;
            $form_address['first_name'] = isset($formData['shipping_first_name']) ? $formData['shipping_first_name'] : null;
            $form_address['last_name'] = isset($formData['shipping_last_name']) ? $formData['shipping_last_name'] : null;

            $form_address['address_id'] = isset($formData['shipping_existing_contact']) ? $formData['shipping_existing_contact'] : null;
            $form_address['addressinfo_country'] = isset($formData['shipping_country']) ? $formData['shipping_country'] : null;
            $form_address['addressinfo_zip'] = isset($formData['shipping_zip_code']) ? $formData['shipping_zip_code'] : null;
            $form_address['addressinfo_state'] = isset($formData['shipping_state']) ? $formData['shipping_state'] : null;
            $form_address['addressinfo_city'] = isset($formData['shipping_city']) ? $formData['shipping_city'] : null;
            $form_address['addressinfo_street_address_2'] = isset($formData['shipping_address_2']) ? $formData['shipping_address_2'] : null;
            if ($formData['source_type'] == 'backend') {

                $form_address['addressinfo_street_address_1'] = isset($formData['shipping_address']) ? $formData['shipping_address'] : null;
            } else {
                $form_address['addressinfo_street_address_1'] = isset($formData['shipping_address_1']) ? $formData['shipping_address_1'] : null;
            }
            $form_address['addressinfo_location_type'] = isset($formData['shipping_location_type']) ? $formData['shipping_location_type'] : null;
            if (!empty($form_address['address_id'])) {
                $form_address['addressinfo_location'] = $form_address['addressinfo_location_type'];
            } else {
                $form_address['addressinfo_location'] = 3;
            }

            if (isset($formData['shipping_phone_info_id']) and trim($formData['shipping_phone_info_id']) != "" and is_numeric(trim($formData['shipping_phone_info_id']))) {
                $paramsPhoneUpdate = array();
                $paramsPhoneUpdate['phone_id'] = trim($formData['shipping_phone_info_id']);
                $paramsPhoneUpdate['phone_type'] = "";
                $paramsPhoneUpdate['country_code'] = $formData['shipping_country_code'];
                $paramsPhoneUpdate['area_code'] = $formData['shipping_area_code'];
                $paramsPhoneUpdate['phone_no'] = $formData['shipping_phone'];
                $paramsPhoneUpdate['continfo_primary'] = "";
                $paramsPhoneUpdate['extension'] = "";
                $paramsPhoneUpdate['modified_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('User\Model\UserTable')->updatePhoneInformations($paramsPhoneUpdate);
            } else {
                $paramsPhoneInsert = array();
                $paramsPhoneInsert['user_id'] = $formData['user_id'];
                $paramsPhoneInsert['phone_type'] = "";
                $paramsPhoneInsert['country_code'] = $formData['shipping_country_code'];
                $paramsPhoneInsert['area_code'] = $formData['shipping_area_code'];
                $paramsPhoneInsert['phone_no'] = $formData['shipping_phone'];
                if ($formData['address_type_status'] == "1") {
                    $paramsPhoneInsert['continfo_primary'] = "1";
                } else {
                    $paramsPhoneInsert['continfo_primary'] = "0";
                }
                $paramsPhoneInsert['extension'] = "";
                $paramsPhoneInsert['add_date'] = DATE_TIME_FORMAT;
                $paramsPhoneInsert['modified_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('User\Model\ContactTable')->insertPhoneInformations($paramsPhoneInsert);
            }
        }
        /*
          $form_address['address_id'] = isset($formData['address_id']) ? $formData['address_id'] : null;
          $form_address['addressinfo_country'] = isset($formData['addressinfo_country']) ? $formData['addressinfo_country'] : null;
          $form_address['addressinfo_zip'] = isset($formData['addressinfo_zip']) ? $formData['addressinfo_zip'] : null;
          $form_address['addressinfo_state'] = isset($formData['addressinfo_state']) ? $formData['addressinfo_state'] : null;
          $form_address['addressinfo_city'] = isset($formData['addressinfo_city']) ? $formData['addressinfo_city'] : null;
          $form_address['addressinfo_street_address_2'] = isset($formData['addressinfo_street_address_2']) ? $formData['addressinfo_street_address_2'] : null;
          $form_address['addressinfo_street_address_1'] = isset($formData['addressinfo_street_address_1']) ? $formData['addressinfo_street_address_1'] : null;
          $form_address['addressinfo_location_type'] = isset($formData['addressinfo_location_type']) ? $formData['addressinfo_location_type'] : null;
         * 
         */
        if (!empty($form_address['address_id'])) {
//if ($formData['address_type'] == 'shipping') {
            $this->getServiceLocator()->get('User\Model\UserTable')->updateAddressInformations($form_address);
//}
        } else {
            $form_address['user_id'] = $formData['user_id'];
            $form_address['add_date'] = DATE_TIME_FORMAT;
            $this->getServiceLocator()->get('User\Model\ContactTable')->insertAddressInfo($form_address);
        }
    }

    /**
     * This function is used to find the categories for shipping charges
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function isCustomFrame($dataArr = array()) {
        /* Get all Categories code for */
        $categoryId = $dataArr['category_id'];
        $isCustomFrame = false;
        while (1) {
            if (isset($categoryData[$categoryId])) {
                if ($categoryData[$categoryId] == 0) {
                    break;
                } else {
                    $categoryId = $categoryData[$categoryId];
                }
            } else {
                break;
            }
        }
        if ($categoryId == 2) {
            $isCustomFrame = true;
        }
        return $isCustomFrame;
    }

    function subval_sort($a, $subkey) {
        foreach ($a as $k => $v) {
            $b[$k] = strtolower($v[$subkey]);
        }
        asort($b);
        foreach ($b as $key => $val) {
            $c[] = $a[$key];
        }
        return $c;
    }



    public function bioCertificatePdf($params = array(), $mode) {
        error_reporting(0);
        $this->getTransactionTable();
        $messages = $this->_config['transaction_messages']['config']['view_crm_transaction'];
        $pdfPath = $this->_config['file_upload_path']['assets_upload_dir'] . "transaction/shipment_" . $this->encrypt($params['shipmentId']);

        $search['woh_id'] = $params['product_id'];
        $searchResult = $this->getServiceLocator()->get('User\Model\UserTable')->getBioWoh(array('bio_certificate_product_id' => $search['woh_id']));
        $searchResult = $searchResult[0];
        $viewModel = new ViewModel();
        $str = '<page><div style="padding-top: 80px;">
        <div align="center" style="font-size: 40px; margin-top: 20px; font-family: times new Roman;">' .
                str_replace("<br>", ' ', $searchResult['final_name']) . '</div>
        <div align="center" style="margin-top: 34px; padding-left:70px; font-size: 20px;">' .
                $searchResult['panel_no'] . '</div>
        <div align="center" style="margin-top:81px; text-align: center; font-size: 22px;">' . $messages['NAME'] . ' : <b>' . $searchResult['name'] . '</b></div>
        <div align="center" style="margin-top:10px; text-align: center; font-size: 22px;">' . $messages['EMIGRANTED_FROM'] . ' : <b>' . $searchResult['immigrated_from'] . '</b></div>
        <div align="center" style="margin-top:10px; text-align: center; font-size: 22px;">' . $messages['ARRIVED_DATE'] . ' : <b>' . $this->OutputDateFormat($searchResult['date_of_entry_to_us'], 'displaymonthformat2') . '</b></div>
        <div align="center" style="margin-top:10px; text-align: center; font-size: 22px;">' . $messages['PORT_ENTRY'] . ' : <b>' . $searchResult['port_of_entry'] . '</b></div>
        <div align="center" style="margin-top:10px; text-align: center; font-size: 22px;">' . $messages['METHOD_OF_TRAVEL'] . ' : <b>' . $searchResult['method_of_travel'] . '</b></div>
        <div align="center" style="margin-top:20px; text-align: center; font-size: 22px;">' . $messages['FAMILY_HISTORY'] . ' :</div>
        <div align="center" style="margin: 0px 110px 0 130px; text-align: center; font-size: 20px;">' . $searchResult['additional_info'] . '</div>';
        $str = $str . '</div></page>';
        try {
            $html2pdf = new\ HTML2PDF('L', 'A4', 'en');
            $html2pdf->setDefaultFont('Times');
            $html2pdf->writeHTML($str, false);
            $html2pdf->Output($pdfPath . "/" . date('Y-m-d') . "_" . $search['product_id'] . "_woh_ceritificate.pdf");
        } catch (HTML2PDF_exception $e) {
            echo $e;
            exit;
        }
    }

    /**
     * This function is used to save the user shipping address
     * @return json
     * @param void
     * @author Icreon Tech - SR
     */
    public function posSaveUserShippingAddressAction() {
        //error_reporting(E_ALL);
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $response = $this->getResponse();
        $this->getTransactionTable();
        if ($request->isPost()) {
            $formData = $postData = $request->getPost()->toArray();
            $posTransactionSession = new Container('posTransactionSession'); /* Pos session */
            //$posTransactionSession->posTransactionData = array();

            if (empty($formData['email_id'])) {
                $formData['username'] = trim($formData['shipping_first_name']) . "" . trim($formData['shipping_last_name']) . $this->GetRandomAlphaNumeric(5);
            } else {
                $formData['username'] = '';
            }
            $curDate = DATE_TIME_FORMAT;
            $formData['modified_date'] = $curDate;
            $formData['curDate'] = $curDate;
            $formData['add_date'] = $curDate;
            $formData['contact_type'] = 1;
            $formData['login_enable'] = 1;
            $formData['source_id'] = 2;

            $transaction = new Postransaction($this->_adapter);
            $formDataFilter = $transaction->filterParam($formData);
            //$posTransactionSession->posTransactionData['user_details'] = array_merge($formData, $formDataFilter);
            

            $form_address = array();
            $form_address['addressinfo_street_address_1'] = isset($formData['shipping_address_1']) ? strip_tags($formData['shipping_address_1']) : null;
            $form_address['addressinfo_street_address_2'] = isset($formData['shipping_address_2']) ? strip_tags($formData['shipping_address_2']) : null;
            $form_address['addressinfo_country'] = isset($formData['shipping_country']) ? $formData['shipping_country'] : null;
            if (isset($formData['is_apo_po_new']) && $formData['is_apo_po_new'] == 1) {
                if (isset($formData['shipping_state_id']) && $formData['shipping_state_id'] != '')
                    $form_address['addressinfo_state'] = $formData['shipping_state_id'];
                if (isset($formData['shipping_state']) && $formData['shipping_state'] != '')
                    $form_address['addressinfo_state'] = $formData['shipping_state'];
            }
            if (isset($formData['is_apo_po_new']) && $formData['is_apo_po_new'] == 2) {
                if (isset($formData['shipping_state_id']) && $formData['shipping_state_id'] != '')
                    $form_address['addressinfo_state'] = $formData['shipping_state_id'];
                if (isset($formData['shipping_state']) && $formData['shipping_state'] != '')
                    $form_address['addressinfo_state'] = $formData['shipping_state'];
            }
            if (isset($formData['is_apo_po_new']) && $formData['is_apo_po_new'] == 3)
                $form_address['addressinfo_state'] = strip_tags($formData['apo_po_state']);

            $form_address['addressinfo_city'] = isset($formData['shipping_city']) ? strip_tags($formData['shipping_city']) : null;
            $form_address['addressinfo_zip'] = isset($formData['shipping_zip_code']) ? $formData['shipping_zip_code'] : null;
            $form_address['addressinfo_location'] = isset($formData['addressinfo_location']) ? $formData['addressinfo_location'] : null;
            $form_address['address_type'] = isset($formData['is_apo_po_new']) ? $formData['is_apo_po_new'] : null;
            //$form_address['addressinfo_location'] = isset($formData['is_apo_po_new']) ? $formData['is_apo_po_new'] : null;
            //$userId = $this->auth->getIdentity()->user_id;
            $userId = '';

            $form_address['title'] = isset($formData['shipping_title']) ? addslashes($formData['shipping_title']) : null;
            $form_address['first_name'] = isset($formData['shipping_first_name']) ? addslashes($formData['shipping_first_name']) : null;
            //$form_address['middle_name'] = isset($formData['shipping_middle_name']) ? addslashes($formData['shipping_middle_name']) : null;
            $form_address['last_name'] = isset($formData['shipping_last_name']) ? addslashes($formData['shipping_last_name']) : null;
            $form_address['user_id'] = $userId;
            $form_address['add_date'] = DATE_TIME_FORMAT;
            //$this->getServiceLocator()->get('User\Model\ContactTable')->insertAddressInfo($form_address);

            $form_address['email_id'] = $formData['email_id'];
            $paramsPhoneInsert = array();
            $paramsPhoneInsert['user_id'] = $userId;
            $paramsPhoneInsert['phone_type'] = "";
            $paramsPhoneInsert['country_code'] = strip_tags($formData['shipping_country_code']);
            $paramsPhoneInsert['area_code'] = strip_tags($formData['shipping_area_code']);
            $paramsPhoneInsert['phone_no'] = strip_tags($formData['shipping_phone']);
            $paramsPhoneInsert['continfo_primary'] = "1";
            $paramsPhoneInsert['extension'] = "";
            $paramsPhoneInsert['add_date'] = DATE_TIME_FORMAT;
            $paramsPhoneInsert['modified_date'] = DATE_TIME_FORMAT;

			$posTransactionSession->posTransactionData['user_details'] = $form_address;
            $posTransactionSession->posTransactionData['address'] = $form_address;
            $posTransactionSession->posTransactionData['phone'] = $paramsPhoneInsert;
            //$this->getServiceLocator()->get('User\Model\ContactTable')->insertPhoneInformationsCheckout($paramsPhoneInsert);
        }
        $messages = array('status' => "success", 'message' => 'Record is saved successfully');
        return $response->setContent(\Zend\Json\Json::encode($messages));
    }

   
    /**
     * This function is used to get select category structure
     * @param       void
     * @return     json
     * @author     Icreon Tech - DT
     */
    public function getSelectArr($categories, $parentVal = 0, $level = 0) {

        $this->_categorySelectArr[''] = "Select Category";
        foreach ($categories as $cat) {
            if ($cat['parent_id'] == $parentVal) {
                $this->_categorySelectArr[$cat['category_id']] = str_repeat('-', $level) . $cat['category_name'];
                $this->getSelectArr($categories, $cat['category_id'], $level + 1);
            }
        }
    }

    function calculateProductShippingDiscount($indShippableProductWeight, $totalShippableProductWeight, $shippingDiscountAmountOff) {
        $percentWeight = ($indShippableProductWeight * 100) / $totalShippableProductWeight;
        $shippingDiscountAmount = ($percentWeight * $shippingDiscountAmountOff) / 100;
        return $shippingDiscountAmount;
    }

    function calculateProductCartDiscount($indProductAmount, $totalCartAmount, $cartDiscountAmountOff) {
        $percentAmount = ($indProductAmount * 100) / $totalCartAmount;
        $productDiscount = round(($percentAmount * $cartDiscountAmountOff) / 100,2);
        return $productDiscount;
    }

    /*function calculateFreeProductDiscount($productQty, $productAmount) {
        $productDiscount = $productAmount / $productQty;
        return $productDiscount;
    }*/
  
  function calculateFreeProductDiscount($product, $itemToDiscount) {        
        $productId = $product['product_id'];
        $productDiscountValue = $product['product_subtotal'] - $product['membership_discount'];
        $discountValue = 0;
        foreach($itemToDiscount as $key => $discountItem)
        {
            if($key == $productId)
            {
                
                for($i = 0;$i< count($discountItem['itemQty']);$i++)
                {
                    $currentDiscountValue = $discountItem['itemTotalAmount'][$i]/$discountItem['itemQty'][$i];
                    if($currentDiscountValue < $discountValue || $discountValue == 0)
                    {
                        $discountValue = $currentDiscountValue;
                    }
                }
            }
        }
        $productDiscountValue = round(($product['product_subtotal'] - $product['membership_discount'])/$product['product_qty'],2);
        
        $currentDiscountValue = round($discountValue,2);
        if($productDiscountValue == $currentDiscountValue)
        {
            return $currentDiscountValue;
        }
        return 0;
    }

    /**
     * This action is used for swip cart
     * @return json
     * @param void
     * @author Icreon Tech - SR
     */
    public function payPosTransactionAction() {
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $this->layout('popup');
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
        ));
        return $viewModel;
    }

    /**
     * This action is used for zip code
     * @return json
     * @param void
     * @author Icreon Tech - SR
     */
    public function posTransactionZipcodeAction() {
        $this->getConfig();
        $this->getTransactionTable();
        $request = $this->getRequest();
        $this->layout('wohlayout');
        $posTransactionSession = new Container('posTransactionSession');
        //$paramArray = $request->getPost()->toArray();
        if(empty($posTransactionSession->posTransactionData)){
		    header('Location: ' . SITE_URL.'/wall-of-honor');
		    die;        
        }
        
        $paramArray = $posTransactionSession->posTransactionData['user_details'];
        $shipcode = !empty($paramArray['shipping_zip_code']) ? $paramArray['shipping_zip_code'] : '';
        
        $user_id = session_id();
        $cartParam['user_session_id'] = $user_id;
        $cartParam['source_type'] = 'frontend';
        $cartData = $posTransactionSession->posTransactionData['transaction_details']['cartData'];
        $msgArr = $this->_config['transaction_messages']['config']['checkout_front'];
        $paramArray['userid'] = $user_id;
        $paramArray['shipcode'] = $shipcode;
        $paramArray['couponcode'] = !empty($paramArray['couponcode']) ? $paramArray['couponcode'] : '';
        if ($request->getPost('ship_m') != '' && $request->getPost('status') != 'error') {
        }
        $paramArray['postData'] = $paramArray;
        if ($paramArray['pick_up'] == 1) {
            $paramArray['shipping_method'] = '';
            $paramsArray['shipping_state'] = '';
        }
        $itemsTotal = $posTransactionSession->posTransactionData['transaction_details'];
        $discount = '';
        $memberShip = array();
        $memberdiscount = '';
        if (empty($discount) && $itemsTotal['finaltotaldiscounted'] != 0) {
            $memberShip = $this->getTransactionTable()->getMinimumMembership();
            $memberdiscount = ($itemsTotal['finaltotaldiscounted'] * $memberShip[0]['discount']) / 100;
        }
        $isBookingKiosk = 0;
        $requestingIp = $this->getServiceLocator()->get('KioskModules')->getRemoteIP();
        if ($requestingIp) {
            if (isset($this->_config['kiosk-system-settings'][$requestingIp]))
                $isBookingKiosk = $this->_config['kiosk-system-settings'][$requestingIp]['isHotSystem'];
        }
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'jsLangTranslate' => array_merge($msgArr, $this->_config['transaction_messages']['config']['common_message']),
            'postdata' => $paramArray,
            'cartlist' => $cartData,
            'memberdiscount' => $memberdiscount,
            'memberShip' => $memberShip,
            'itemsTotal' => $itemsTotal,
            'upload_file_path' => $this->_config['file_upload_path'],
            'membershipDiscountAmount' => $itemsTotal['membershipDiscountAmount'],
            'isBookingKiosk' => $isBookingKiosk,
            'pick_up' => $request->getPost('pick_up'),
            'cash_payment' => $request->getPost('cash_payment'),
            'transaction_details'=>$posTransactionSession->posTransactionData['transaction_details']
        ));
        return $viewModel;
    }

    
    /**
     * This function is used to get the Primary address
     * @return     array 
     * @param array
     * @author  Icreon Tech - SR
     */
    protected function getPrimaryUserAddress($data) {
        $billingAdd = array();
        foreach ($data as $key => $val) {
            $locationVal = @explode(',', $val['location']);
            // 1,2; 1,2,3;
            if (isset($locationVal[0]) && isset($locationVal[1])) {
                if (($locationVal[0] == 1 && $locationVal[1] == 2) || ($locationVal[0] == 2 && $locationVal[1] == 1)) {
                    $billingAdd['primary_billing'][] = $key;
                }
            } else if (isset($locationVal[0]) && $locationVal[0] == 2) {
                //2; 2,3
                $billingAdd['billing'][] = $key;
            }
        }
        return $billingAdd;
    }    
    
   /**
     * This Service is used to get user membership period
     * @author Icreon Tech-DG
     * @return membership data
     */
    public function getUserMembership($getMemberShip = array()) {
        $sm = $this->getServiceLocator();
        $config = $sm->get('Config');
        $userMembershipData = array();
        $startDate = date("Y-m-d", strtotime(DATE_TIME_FORMAT));
        $membershipDateFrom = '';
        $membershipDateTo = '';

        if ($getMemberShip['validity_type'] == 1) {
            $membershipDateFrom = $startDate;
            $startDay = $config['financial_year']['srart_day'];
            $startMonth = $config['financial_year']['srart_month'];
            $startYear = date("Y", strtotime(DATE_TIME_FORMAT));
            $startDate = $startYear . "-" . $startMonth . "-" . $startDay;
            $addYear = ($getMemberShip['validity_time'] - $startYear) + 1;
            $futureDate = date('Y-m-d', strtotime('+' . $addYear . ' year -1 day', strtotime($startDate)));
            $your_date = $futureDate;
            $now = time();
            $datediff = strtotime($your_date) - $now;
            if ((isset($config['membership_plus']) && !empty($config['membership_plus'])) && (floor($datediff / (60 * 60 * 24)) < $config['membership_plus'])) {
                $addYear = ($getMemberShip['validity_time'] - $startYear) + 2;
                $futureDate = date('Y-m-d', strtotime('+' . $addYear . ' year', strtotime($startDate)));
                $futureDate = date('Y-m-d', strtotime('-1 day', strtotime($futureDate)));
            }
            $membershipDateTo = $futureDate;
        } else if ($getMemberShip['validity_type'] == 2) {
            $membershipDateFrom = $startDate;
            $futureDate = date('Y-m-d', strtotime('+' . $getMemberShip['validity_time'] . ' year -1 day', strtotime($startDate)));
            $membershipDateTo = $futureDate;
        }
        $userMembershipData['membership_date_from'] = $membershipDateFrom;
        $userMembershipData['membership_date_to'] = $membershipDateTo;
        return $userMembershipData;
    }    
    
    /**
     * This function is used to make authorize net payment with swap card
     * @return json
     * @param void
     * @author Icreon Tech - SR
     */
    public function makeAuthorizeWohPayment($paymentArray = array()) {
        $sale = new \AuthorizeNetCP($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);

        if ($this->_config['authorize_net_payment_gateway']['ENV'] == 'AuthnetXML::USE_PRODUCTION_SERVER')
            $sale->setSandbox(false);

        $sale->setFields(
                array(
                    'amount' => $paymentArray['amount'],
                    'device_type' => '4',
                    'invoice_num' => 'WOH' . sprintf('%011d', $paymentArray['invoice_num']),
                    'address' => $paymentArray['address'],
                    'city' => $paymentArray['city'],
                    'state' => $paymentArray['state'],
                    'zip' => $paymentArray['zip'],
                    'country' => $paymentArray['country'],
                    'phone' => $paymentArray['phone'],
                    'email' => $paymentArray['email'],
                    'cust_id' => $paymentArray['cust_id'],
                    'description' => $paymentArray['description'],
                )
        );
        $cardDetailsArray = explode(';', $paymentArray['card_details']);
	 //$cardDetailsArray = explode(';', $paymentArray['card_details']='B4222222222222^/John^151210100000019301000000877000000?');
        $sale->setTrack1Data($cardDetailsArray[0]);
        if (!empty($cardDetailsArray[1]))
            $sale->setTrack2Data($cardDetailsArray[1]);
        $response = $sale->authorizeAndCapture();
        return $response;
    }    
    
    /**
     * This function is used insert into the payment received
     * @return     array 
     * @param array
     * @author  Icreon Tech - SR
     */
    public function insertIntoPaymentReceive($dataArray = array()) {

        $receivedPaymentIdArray = array();
        $totalCreditCardAmount = 0;
        $pos = new PosManagement($this->_adapter);
        $paymentTransactionData = array();
        $paymentTransactionData['amount'] = round($dataArray['amount'], 2);
        $paymentTransactionData['transaction_id'] = $dataArray['transaction_id'];
        $paymentTransactionData['authorize_transaction_id'] = $dataArray['authorize_transaction_id'];

        $paymentTransactionData['transaction_payment_id'] = $dataArray['transaction_payment_id'];
        $paymentTransactionData['added_by'] = '';
        $paymentTransactionData['added_date'] = $dataArray['add_date'];
        $paymentTransactionData['modify_by'] = '';
        $paymentTransactionData['modify_date'] = $dataArray['add_date'];
        $paymentTransactionDataArr['0'] = $dataArray['transaction_id'];

        $paymentTransactionData = $pos->getTransactionPaymentReceiveArr($paymentTransactionData);
        $paymentTransactionData['10'] = $dataArray['product_type_id']; // $dataArray['UnshipProductType'];
        $paymentTransactionData['11'] = @$dataArray['table_auto_id'];
        $paymentTransactionData['12'] = @$dataArray['table_type'];
        $paymentTransactionData['13'] = @$dataArray['isPaymentReceived'];
        $paymentTransactionData['14'] = @$dataArray['batch_id'];
        $paymentTransactionData['9'] = '';
        if ($dataArray['payment_source'] == 'credit') {
            $profileParam = array();
            $profileParam['transaction_id'] = $dataArray['transaction_id'];
            $profileParam['authorize_transaction_id'] = $dataArray['authorize_transaction_id'];
            $transactionProfileDetails = $this->_posTable->getTransactionProfileCreditCartId($profileParam);
            foreach ($transactionProfileDetails as $profile) {
                if (!empty($profile['profile_id']))
                    $paymentTransactionData['9'] = $profile['credit_card_type_id'];
            }
            return $lastInsertId = $this->_posTable->saveTransactionPaymentReceive($paymentTransactionData);
        }
    }    
    
    /**
     * This function is used for thankyou confirmation
     * @return     array 
     * @param array
     * @author  Icreon Tech - SR
     */
    public function posTransactionThanksAction(){
        $this->layout('wohlayout');
        $params = $this->params()->fromRoute();
        $is_email = $this->Decrypt($params['is_email']);
        $transaction_id = $this->Decrypt($params['transaction_id']);
		$viewModel = new ViewModel();
            $viewModel->setVariables(array(
                'is_email' => $is_email,
                'transaction_id'=>$transaction_id
            ));
            return $viewModel;    
    
    }

	public function deleteWohCartAction(){
		$this->getConfig();
        $this->getTransactionTable();
        $request = $this->getRequest();
		$params = $this->params()->fromRoute();
		$response = $this->getResponse();
		$cartParam['user_id'] = '';
        $cartParam['source_type'] = 'frontend';
        $cartParam['user_session_id'] = session_id();
        $cartParam['membership_percent_discount'] = MEMBERSHIP_DISCOUNT;
		$cartlist = array();
		$cartlist = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getCartProducts($cartParam);
		$removeParam = array(); 
		$i=0;
		if(!empty($cartlist)){
			foreach($cartlist as $key=>$value){
				$removeParam['cart_id'] = $value['cart_id'];
				$removeParam['product_type'] = $value['product_type_id'];
				$this->getServiceLocator()->get('Transaction\Model\TransactionTable')->removeFromCart($removeParam);
				$i++;
			}
		}
		if(count($cartlist) == $i || empty($cartlist)){
			$messages = array('status' => "success");
			$response->setContent(\Zend\Json\Json::encode($messages));
			return $response;
		}
	}
    
}