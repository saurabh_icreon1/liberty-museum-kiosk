<?php

/**
 * This controller is used to manage batch ID
 * @package    Transaction
 * @author     Icreon Tech - DG
 */

namespace Transaction\Controller;

use Base\Controller\BaseController;
use Pledge\Controller\PledgeController;
use Zend\View\Model\ViewModel;
use Transaction\Model\Batch;
use Common\Model\Common;
use Zend\Http\Request;
use Zend\Authentication\AuthenticationService;
use Transaction\Form\UpdateTransactionBatchForm;
use Transaction\Form\SelectBatchIdForm;
use Transaction\Form\CreateBatchIdForm;
use Transaction\Form\EditBatchIdForm;
use SimpleXMLElement;
use Zend\View\Renderer\PhpRenderer;
use Zend\Session\Container;

/**
 * This controller is used to manage batch
 * @category   Zend
 * @package    Transaction
 * @author     Icreon Tech - SR
 */
class BatchController extends BaseController {

    protected $_batchTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_flashMessage = null;
    public $auth = null;

    function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        parent::__construct();
        $auth = new AuthenticationService();
        $this->auth = $auth;
    }

    /**
     * This function is used to get table object
     * @return     array
     * @param array $form
     * @author Icreon Tech - SR
     */
    public function getBatchTable() {
        if (!$this->_batchTable) {
            $sm = $this->getServiceLocator();
            $this->_batchTable = $sm->get('Transaction\Model\BatchTable');
            $this->_adapter = $sm->get('dbAdapter');
            $this->_config = $sm->get('Config');
        }
        return $this->_batchTable;
    }

    /**
     * This function is used to get config object
     * @return     array
     * @param array $form
     * @author Icreon Tech - SR
     */
    public function getConfig() {
        $sm = $this->getServiceLocator();
        $this->_translator = $sm->get('translator');
        $this->_config = $sm->get('Config');
    }

    /**
     * This function is used to update the transaction batch 
     * @param array $form
     * @author Icreon Tech - SR
     */
    public function updateTransactionBatchAction() {
        $this->checkUserAuthentication();
        $this->getBatchTable();
        $this->getConfig();
        $form = new UpdateTransactionBatchForm();
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->layout('crm');
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'updateTransactionBatchFrm' => $form,
            'jsLangTranslate' => array_merge($this->_config['transaction_messages']['config']['update_transaction_batch'], $this->_config['transaction_messages']['config']['common_message']),
        ));
        return $viewModel;
    }

    /**
     * This function is used to open the batch is and transaction id list
     * @return array
     * @param array
     * @author Icreon Tech - SR
     */
    public function selectBatchIdAction() {
        $this->getBatchTable();
        $this->getConfig();
        $params = $this->params()->fromRoute();
        
        // ns - start
        $batchIncrementedLastId = $this->_batchTable->getBatchIncrementedLastId();
        $batchIncrementedLastIdF = date("Ym").$batchIncrementedLastId; 
        // ns - end
        
        $id = '';
        if ($params['batch_text_id']) {
            $id = $params['batch_text_id'];
        }
        $form = new SelectBatchIdForm();
        $formCreateBatch = new CreateBatchIdForm();
        $formCreateBatch->get("batch_id")->setValue($batchIncrementedLastIdF);
        $formCreateBatch->get("batch_id")->setAttribute("readonly", "readonly");
        $formEditBatch = new EditBatchIdForm();
        $formEditBatch->get("batch_id")->setAttribute("readonly", "readonly");
        $request = $this->getRequest();
        $response = $this->getResponse();
        if(isset($id) and trim($id) != "") { $this->layout('popup'); } else { $this->layout('crm'); }
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'createBatchFrm' => $formCreateBatch,
            'editBatchFrm' => $formEditBatch,
            'selectBatchFrm' => $form,
            'jsLangTranslate' => array_merge($this->_config['transaction_messages']['config']['update_transaction_batch'], $this->_config['transaction_messages']['config']['common_message']),
            'id' => $id
        ));
        return $viewModel;
    }

    /**
     * This function is used to get the list of batch and transactions
     * @return array
     * @param array
     * @author Icreon Tech - SR
     */
    public function getBatchListAction() {
        $this->checkUserAuthentication();
        $this->getBatchTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $param = $this->params()->fromRoute();

        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        parse_str($request->getPost('searchString'), $searchParam);
        $searchParam['startIndex'] = $start;
        $searchParam['recordLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');
        $searchParam['statement_date'] = $this->DateFormat($searchParam['statement_date'], 'db_date_format');
		$searchResult = $this->_batchTable->getBatchListing($searchParam);
        $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
        $jsonResult = array();
        $arrCell = array();
        $subArrCell = array();
        $jsonResult['page'] = $page;
        $jsonResult['records'] = $countResult[0]->RecordCount;
        $jsonResult['total'] = @ceil($countResult[0]->RecordCount / $limit);
        if (count($searchResult) > 0) {
            $i = ($page - 1) * $limit + 1;
            foreach ($searchResult as $val) {
                $arrCell['id'] = $val['payment_batch_id'];
                if ($val['is_locked'] == 0) {
                    $lockStyle = '';
                    $unLockStyle = 'display:none;';
                } elseif ($val['is_locked'] == 1) {
                    $lockStyle = 'display:none;';
                    $unLockStyle = '';
                }
                if ($param['id'] != NONE) {
                    $id = $this->decrypt($param['id']);
					
					$action = '';
                    
					$action.= '<a id="select-batch-link-' . $val['payment_batch_id'] . '" href="javascript:void(0);" onClick="selectBatch(\'' . $val['batch_id'] . '\',\'' . $val['payment_batch_id'] . '\',\'' . $id . '\');"  Style="' . $lockStyle . '" class="batch-icon">Select<div class="tooltip">Select Batch<span></span></div></a>&nbsp;';
					
					/*$action.='<a id="edit-batch-link-' . $val['payment_batch_id'] . '" href="javascript:void(0);" onClick="editBatch(\'' . $val['payment_batch_id'] . '\');"  Style="' . $lockStyle . '" class="edit-icon"><div class="tooltip">Edit Batch<span></span></div></a>&nbsp;';*/
					
					$action.='<a id="lock-batch-link-' . $val['payment_batch_id'] . '" href="javascript:void(0);" onClick="lockBatch(\'' . $val['payment_batch_id'] . '\');" Style="' . $lockStyle . '" class="lock-icon">Lock<div class="tooltip">Lock Batch<span></span></div></a>&nbsp;<a id="unlock-batch-link-' . $val['payment_batch_id'] . '" href="javascript:void(0);" onClick="unLockBatch(\'' . $val['payment_batch_id'] . '\');" Style="' . $unLockStyle . '" class="unlock-icon">Unlock<div class="tooltip">Unlock Batch<span></span></div></a>';
                } else {

					$action = '';

                    /*$action.= '<a id="edit-batch-link-' . $val['payment_batch_id'] . '" href="javascript:void(0);" onClick="editBatch(\'' . $val['payment_batch_id'] . '\');"  Style="' . $lockStyle . '" class="edit-icon"><div class="tooltip">Edit Batch<span></span></div></a>&nbsp;';*/
					
					$action.='<a id="lock-batch-link-' . $val['payment_batch_id'] . '" href="javascript:void(0);" onClick="lockBatch(\'' . $val['payment_batch_id'] . '\');" Style="' . $lockStyle . '" class="lock-icon">Lock<div class="tooltip">Lock Batch<span></span></div></a>&nbsp;';
					
					$action.='<a id="unlock-batch-link-' . $val['payment_batch_id'] . '" href="javascript:void(0);" onClick="unLockBatch(\'' . $val['payment_batch_id'] . '\');" Style="' . $unLockStyle . '" class="unlock-icon">Unlock<div class="tooltip">Unlock Batch<span></span></div></a>';
                }
				$totalamount = (is_null($val['totalamount']) || $val['totalamount']=='' || $val['totalamount']=='0')?'N/A':$this->Currencyformat($val['totalamount']);
                $arrCell['cell'] = array($val['batch_id'], $val['settlement_date'], $totalamount, $val['added_by'], str_replace('-', '/', $this->OutputDateFormat($val['added_date'], 'dateformatampm')), str_replace('-', '/', $this->OutputDateFormat($val['modified_date'], 'dateformatampm')), $action);
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
                $i++;
            }
        }
        return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    public function createBatchIdAction() {
        $this->checkUserAuthentication();
        $this->getBatchTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $param = $this->params()->fromRoute();
        $data = $request->getPost()->toArray();
        if (!empty($data)) {
            try {
                $searchResult = $this->_batchTable->getBatchListing(array('batch_id' => $data['batch_id']));
                if (!empty($searchResult)) {
                    $messages = array('status' => "error", 'message' => "Batch ID alreday exists");
                } else {
                    $settlement_date = strtotime($data['settlement_date']);
                    $data['settlement_date'] = $this->DateFormat($data['settlement_date'], 'db_date_format');
                    $data['modified_by'] = $data['added_by'] = $this->auth->getIdentity()->crm_user_id;
                    $data['modified_date'] = $data['added_date'] = DATE_TIME_FORMAT;
                    $saveData = $this->_batchTable->createBatchId($data);
                    if (!empty($saveData) && $saveData[0]['payment_batch_id']) {
                        $batchIncrementedLastId = $this->_batchTable->getBatchIncrementedLastId();
                        $batchIncrementedLastIdF = date("Ym").$batchIncrementedLastId; 
                        $messages = array('status' => "success", 'message' => "Batch creatd successfully", 'incremented_batch_id'=>$batchIncrementedLastIdF);
                    }
                }
            } catch (Exception $e) {
                $batchIncrementedLastId = $this->_batchTable->getBatchIncrementedLastId();
                $batchIncrementedLastIdF = date("Ym").$batchIncrementedLastId; 
                $messages = array('status' => "error", 'message' => "Unbale to create batch", 'incremented_batch_id'=>$batchIncrementedLastIdF);
            }
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    public function updateBatchIdAction() {
        $this->checkUserAuthentication();
        $this->getBatchTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $param = $this->params()->fromRoute();
        $data = $request->getPost()->toArray();

        if (!empty($data)) {
            try {
                if (isset($data['batch_id']) && $data['batch_id'] != $data['old_batch_id']) {
                    $searchResult = $this->_batchTable->getBatchListing(array('batch_id' => $data['batch_id']));

                    if (!empty($searchResult)) {
                        $batchIncrementedLastId = $this->_batchTable->getBatchIncrementedLastId();
                        $batchIncrementedLastIdF = date("Ym").$batchIncrementedLastId; 
                        $messages = array('status' => "error", 'message' => "Batch ID already exists", 'incremented_batch_id'=>$batchIncrementedLastIdF);
                    } else {
                        if (isset($data['payment_batch_ids'])) {
                            foreach ($data['payment_batch_ids'] as $key => $value) {
                                $data['payment_batch_id'] = str_replace('jqg_list_', '', $value);
                                $data['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                                $data['modified_date'] = DATE_TIME_FORMAT;
                                $this->_batchTable->updateBatchId($data);
                            }
                        } else {
                            $data['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                            $data['modified_date'] = DATE_TIME_FORMAT;
                            if (isset($data['settlement_date']))
                                $data['settlement_date'] = $this->DateFormat($data['settlement_date'], 'db_datetime_format');
                            $this->_batchTable->updateBatchId($data);
                        }

                        $batchIncrementedLastId = $this->_batchTable->getBatchIncrementedLastId();
                        $batchIncrementedLastIdF = date("Ym").$batchIncrementedLastId; 
                        $messages = array('status' => "success", 'message' => "Batch updated successfully", 'incremented_batch_id'=>$batchIncrementedLastIdF);
                    }
                }else {
                    if (isset($data['payment_batch_ids'])) {
                        foreach ($data['payment_batch_ids'] as $key => $value) {
                            $data['payment_batch_id'] = str_replace('jqg_list_', '', $value);
                            $data['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                            $data['modified_date'] = DATE_TIME_FORMAT;
                            $this->_batchTable->updateBatchId($data);
                        }
                    } else {
                        $data['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                        $data['modified_date'] = DATE_TIME_FORMAT;
                        if (isset($data['settlement_date']))
                            $data['settlement_date'] = $this->DateFormat($data['settlement_date'], 'db_datetime_format');
                        $this->_batchTable->updateBatchId($data);
                    }

                    $batchIncrementedLastId = $this->_batchTable->getBatchIncrementedLastId();
                    $batchIncrementedLastIdF = date("Ym").$batchIncrementedLastId; 
                    $messages = array('status' => "success", 'message' => "Batch updated successfully", 'incremented_batch_id'=>$batchIncrementedLastIdF);
                }
            } catch (Exception $e) {
                $batchIncrementedLastId = $this->_batchTable->getBatchIncrementedLastId();
                $batchIncrementedLastIdF = date("Ym").$batchIncrementedLastId;  
                $messages = array('status' => "error", 'message' => "Unbale to update batch", 'incremented_batch_id'=>$batchIncrementedLastIdF);
            }

            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    public function editBatchIdAction() {
        $this->checkUserAuthentication();
        $this->getBatchTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $param = $this->params()->fromRoute();
        $data = $request->getPost()->toArray();
        $searchResult = $this->_batchTable->getBatchListing($data);
        $batchData = $searchResult[0];
        $batchData['settlement_date'] = $this->DateFormat($searchResult[0]['settlement_date'], 'calender');
        $response->setContent(\Zend\Json\Json::encode($batchData));
        return $response;
    }

    public function checkBatchExistsAction() {
        $this->checkUserAuthentication();
        $this->getBatchTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $param = $this->params()->fromRoute();
        $data = $request->getPost()->toArray();
        $searchResult = $this->_batchTable->getBatchListing($data);
        //asd($searchResult);
        if (!empty($searchResult)) {
            $messages = array('status' => "error");
        } else {
            $messages = array('status' => "success");
        }
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

}