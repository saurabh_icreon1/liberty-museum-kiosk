<?php

/**
 * This controller is used to manage Sage
 * @package    Transaction
 * @author     Icreon Tech - KK
 */

namespace Transaction\Controller;

use Base\Model\ZipStream;
use Base\Controller\BaseController;
use Zend\View\Model\ViewModel;
use Transaction\Model\Transaction;
use Zend\Http\Request;
use Zend\Authentication\AuthenticationService;
use Transaction\Form\SearchSageForm;

/**
 * This controller is used to manage Rma
 * @category   Zend
 * @package    Transaction
 * @author     Icreon Tech - AS
 */
class SageController extends BaseController {

    protected $_transactionTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_flashMessage = null;

    function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        parent::__construct();
        $auth = new AuthenticationService();
        $this->auth = $auth;
    }

    /**
     * This function is used to get table object
     * @return     array
     * @param array $form
     * @author Icreon Tech - AS
     */
    public function getTransactionTable() {
        if (!$this->_transactionTable) {
            $sm = $this->getServiceLocator();
            $this->_transactionTable = $sm->get('Transaction\Model\TransactionTable');
            $this->_adapter = $sm->get('dbAdapter');
            $this->_config = $sm->get('Config');
        }
        return $this->_transactionTable;
    }

    /**
     * This function is used to get config object
     * @return     array
     * @param array $form
     * @author Icreon Tech - AS
     */
    public function getConfig() {
        $sm = $this->getServiceLocator();
        $this->_translator = $sm->get('translator');
        $this->_config = $sm->get('Config');
    }

    /**
     * This action is used to get the transactions that have sent to sage
     * @return json
     * @param void
     * @author Icreon Tech - KK
     */
    public function getExportToSageAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $params = $this->params()->fromRoute();
        $searchTransactionMessage = array_merge($this->_config['transaction_messages']['config']['SAGE'], $this->_config['transaction_messages']['config']['create_crm_transaction']);
        $transaction = new Transaction($this->_adapter);
        $request = $this->getRequest();
        /* end  Saved search data */
        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $response = $this->getResponse();
            $postArray = $request->getPost()->toArray();
            parse_str($request->getPost('searchString'), $searchParam);
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit; // do not put $limit*($page - 1)
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            if($searchParam['sort_order'] == '')
                $searchParam['sort_order'] = 'desc';
            $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'modified_date') ? 'sage.exported_date' : $searchParam['sort_field'];
            if($searchParam['sort_field'] == '' || is_null($searchParam['sort_field']))
                $searchParam['sort_field'] = 'sage.exported_date';
            
            $searchTransactionArr = $transaction->getSageSearchArr($searchParam);

            $seachResult = $this->_transactionTable->getAllSageBatches($searchTransactionArr);
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $jsonResult['page'] = $request->getPost('page');
            $jsonResult['records'] = $countResult[0]->RecordCount;
            $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
            if (!empty($seachResult)) {
                foreach ($seachResult as $val) {
                    $arrCell['id'] = $val['sage_batch_id'];
                    $encryptId = $this->encrypt($val['sage_batch_id']);
                    $sage_batch_id = "/save-download-batch/" . $this->encrypt($val['sage_batch_id']);
                    $downloadLink = '<a href="' . $sage_batch_id . '" class="download_sage_batch" id="' . $val['sage_batch_id'] . '">Download</a>';
                    $actions = '<div class="action-col">' . $downloadLink . '</div>';
                    $transactionDate = ($val['exported_date'] == 0) ? 'N/A' : $this->OutputDateFormat($val['exported_date'], 'dateformatampm');
                    $sageCheckbox = "<div><input type='checkbox' class='checkbox e2 batch_checkbox' name='batch_nos[]' id='batch_no_" . $val['sage_batch_id'] . "' value='" . $val['sage_batch_id'] . "' /><label class='check' style='margin-right:20px' for='batch_no_" . $val['sage_batch_id'] . "'>&nbsp;</label>" . $val['sage_batch_no'] . " </div>";
                    $arrCell['cell'] = array($sageCheckbox, $transactionDate, $actions);
                    $subArrCell[] = $arrCell;
                    $jsonResult['rows'] = $subArrCell;
                }
            } else {
                $jsonResult['rows'] = array();
            }
            return $response->setContent(\Zend\Json\Json::encode($jsonResult));
        } else {
            $this->layout('crm');
            $messages = array();
            if ($this->_flashMessage->hasMessages()) {
                $messages = $this->_flashMessage->getMessages();
            }

            $viewModel = new ViewModel();
            $param = $this->params()->fromRoute();
            $batch_no = !empty($param['sage_batch_no']) ? $param['sage_batch_no'] : '';
            $response = $this->getResponse();
            $viewModel->setVariables(array(
                'messages' => $messages,
                'jsLangTranslate' => array_merge($searchTransactionMessage, $this->_config['transaction_messages']['config']['common_message']),
                'batch_no' => $batch_no
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to create the batches for transactions that have sent to sage
     * @return json
     * @param void
     * @author Icreon Tech - KK
     */
    public function createSageBatchAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $params = $this->params()->fromRoute();
        $searchTransactionMessage = array_merge($this->_config['transaction_messages']['config']['SAGE'], $this->_config['transaction_messages']['config']['create_crm_transaction']);
        $transaction = new Transaction($this->_adapter);


        $request = $this->getRequest();

        $all_gl_codes = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getGLCodes();
        foreach ($all_gl_codes as $key => $value) {
            $glCodes[$value['gl_code']] = $value['gl_code_value'];
        }
        /* end  Saved search data */
        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $response = $this->getResponse();
            $postArray = $request->getPost()->toArray();
            parse_str($request->getPost('searchString'), $searchParam);
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit; // do not put $limit*($page - 1)
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            $searchTransactionArr = $transaction->getSageBatchSearchArr($searchParam);

            $seachResult = $this->_transactionTable->getAllSageBatchesForTransaction($searchTransactionArr);
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $jsonResult['page'] = $request->getPost('page');
            $jsonResult['records'] = $countResult[0]->RecordCount;
            $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);

            if (!empty($seachResult)) {
                foreach ($seachResult as $val) {
                    if($val['transaction_id']!=''){
                        $arrCell['id'] = $val['cart_id'];
                        $encryptId = $this->encrypt($val['cart_id']);

                        if (isset($val['product_type']) && $val['product_type'] != '' && in_array($val['product_type'], array('Manifest', 'Ship Image')))
                            $glCodeValue = $glCodes['PASS_DOC'];
                        else
                            $glCodeValue = $val['gl_code_value'];

                        if (isset($val['product_type_id']) && $val['product_type_id']==2)
                            $transaction_amount = $val['transaction_sub_total_amount']/2;
                        else
                            $transaction_amount = $val['transaction_sub_total_amount'];                        

                        $arrCell['cell'] = array($val['transaction_id'], $glCodeValue, $val['department'], $val['program'], $val['designation'], $val['restricted'], $val['allocation'], number_format((float)$transaction_amount, 2, '.', ''));
                        $subArrCell[] = $arrCell;
                        unset($arrCell);
                        if ($val['transaction_product_discount'] != '0.00') {
                            $arrCell['id'] = $val['cart_id'];
                            $arrCell['cell'] = array($val['transaction_id'], $glCodes['DISCOUNT'], $val['department'], $val['program'], $val['designation'], $val['restricted'], $val['allocation'], '(' . $val['transaction_product_discount'] . ')');
                            $subArrCell[] = $arrCell;
                        }
                        unset($arrCell);
                        if ($val['transaction_product_tax'] != '0.00') {
                            $arrCell['id'] = $val['cart_id'];
                            $arrCell['cell'] = array($val['transaction_id'], $glCodes['TAX'], $val['department'], $val['program'], $val['designation'], $val['restricted'], $val['allocation'], $val['transaction_product_tax']);
                            $subArrCell[] = $arrCell;
                        }
                        unset($arrCell);
                        if ($val['transaction_product_shipping'] != '0.00' || $val['transaction_product_handling'] != '0.00') {
                            $sh_h = $val['transaction_product_shipping'] + $val['transaction_product_handling'];
                            if ($sh_h != '0' && $sh_h != '0.00') {
                                $arrCell['id'] = $val['cart_id'];
                                $arrCell['cell'] = array($val['transaction_id'], $glCodes['SHI_AND_HAND'], $val['department'], $val['program'], $val['designation'], $val['restricted'], $val['allocation'], $sh_h);
                                $subArrCell[] = $arrCell;
                            }
                        }
                        unset($arrCell);
                    }
                }
                $jsonResult['rows'] = $subArrCell;
            } else {
                $jsonResult['rows'] = array();
            }
            return $response->setContent(\Zend\Json\Json::encode($jsonResult));
        } else {
            $this->layout('crm');
            $messages = array();
            if ($this->_flashMessage->hasMessages()) {
                $messages = $this->_flashMessage->getMessages();
            }


            $SearchSageForm = new SearchSageForm();
            $viewModel = new ViewModel();
            $response = $this->getResponse();
            $viewModel->setVariables(array(
                'messages' => $messages,
                'search_sage_form' => $SearchSageForm,
                'jsLangTranslate' => array_merge($searchTransactionMessage, $this->_config['transaction_messages']['config']['common_message']),
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to save the batch for sage
     * @return json
     * @param void
     * @author Icreon Tech - KK
     */
    public function saveDownloadBatchAction() {
       $this->checkUserAuthentication();
        $this->getTransactionTable();
        $params = $this->params()->fromRoute();
        $searchTransactionMessage = array_merge($this->_config['transaction_messages']['config']['SAGE'], $this->_config['transaction_messages']['config']['create_crm_transaction']);
        $transaction = new Transaction($this->_adapter);
        $request = $this->getRequest();
        $param = $this->params()->fromRoute();
        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $response = $this->getResponse();
            $postArray = $request->getPost()->toArray();
            parse_str($request->getPost('searchString'), $searchParam);
            if (isset($postArray['saveanddownnload']) && $postArray['saveanddownnload'] == 'success') {
                $batchArray = $this->_transactionTable->getMaxTransactionBatchNo();
                $maximumBatch = $batchArray[0]['max_batch'];
                $insertBatchArray['batch_no'] = $maximumBatch;
                $postArray['update'] = 'update';
                $insertBatchArray['added_by'] = $this->auth->getIdentity()->crm_user_id;
                $batch_no = $this->_transactionTable->saveSageBatchNo($insertBatchArray);
                $searchTransactionArr = $transaction->getSageBatchSearchArr($postArray);
                $seachResult = $this->_transactionTable->getAllSageBatchesForTransaction($searchTransactionArr);
                foreach ($seachResult as $sageData) {
                    $batchArray['batch_no'] = $batch_no;
                    $batchArray['type_of_product'] = $sageData['type_of_product'];
                    $batchArray['cart_id'] = $sageData['cart_id'];
                    $batchArray['transaction_id'] = $sageData['transaction_id'];
                    $batchArray['product_type_id'] = $sageData['product_type_id'];
                    $batchArray['product_mapping_id'] = $sageData['product_mapping_id'];
                    $this->_transactionTable->updateSageBatchNo($batchArray);
                }

                $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
                $messages = array('status' => "success", 'batch_no' => $this->encrypt($batch_no));
                return $response->setContent(\Zend\Json\Json::encode($messages));
            } elseif (!empty($postArray['download']) || !empty($param['sage_batch_no'])) {

                if (empty($postArray['sage_batch_no'])) {
                    $postArray['sage_batch_no'] = $this->decrypt($param['sage_batch_no']);
                }
                //$postArray['update'] = 'update';
                $searchTransactionArr = $transaction->getSageBatchSearchArr($postArray);
                $seachResult = $this->_transactionTable->getAllSageBatchesForTransaction($searchTransactionArr);
                $this->downloadsagexls($seachResult);die;
                exit;
            }
        } elseif (!empty($param['sage_batch_no'])) {
            if (empty($postArray['sage_batch_no'])) {
                $postArray['sage_batch_no'] = $this->decrypt($param['sage_batch_no']);
            }
            //$postArray['update'] = 'update';
            $searchTransactionArr = $transaction->getSageBatchSearchArr($postArray);
            $seachResult = $this->_transactionTable->getAllSageBatchesForTransaction($searchTransactionArr);
            $this->downloadsagexls($seachResult);
            die;
        }
    }

    /**
     * This action is used to download the xls 
     * @return void
     * @param Array
     * @author Icreon Tech - KK
     */
    public function downloadsagexls($seachResult) {
        $all_gl_codes = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getGLCodes();
        foreach ($all_gl_codes as $key1 => $value1) {
            $glCodes[$value1['gl_code']] = $value1['gl_code_value'];
        }

        if (!empty($seachResult) && $seachResult[0] != '') {
            $seachResult2 = array();
            foreach ($seachResult as $key => $value) {
                if (isset($value['product_type']) && $value['product_type'] != '' && in_array($value['product_type'], array('Manifest', 'Ship Image')))
                    $glCodeValue = $glCodes['PASS_DOC'];
                else
                    $glCodeValue = $value['gl_code_value'];
                
                if (isset($value['product_type_id']) && $value['product_type_id']==2)
                    $transaction_amount = $value['transaction_sub_total_amount']/2;
                else
                    $transaction_amount = $value['transaction_sub_total_amount'];
                
                $array1 = array('transaction_id' => $value['transaction_id'], 'gl_code_value' => $glCodeValue, 'department' => $value['department'], 'program' => $value['program'], 'designation' => $value['designation'], 'restricted' => $value['restricted'], 'allocation' => '0', 'transaction_amount' => number_format((float)$transaction_amount, 2, '.', ''));
                $seachResult2[] = $array1;
                unset($array1);

                if ($value['transaction_product_discount'] != '0.00') {
                    $array1 = array('transaction_id' => $value['transaction_id'], 'gl_code_value' => $glCodes['DISCOUNT'], 'department' => $value['department'], 'program' => $value['program'], 'designation' => $value['designation'], 'restricted' => $value['restricted'], 'allocation' => '0', 'transaction_amount' => '(' . number_format((float)$value['transaction_product_discount'], 2, '.', '') . ')');
                    $seachResult2[] = $array1;
                }
                unset($array1);
                if ($value['transaction_product_tax'] != '0.00') {
                    $array1 = array('transaction_id' => $value['transaction_id'], 'gl_code_value' => $glCodes['TAX'], 'department' => $value['department'], 'program' => $value['program'], 'designation' => $value['designation'], 'restricted' => $value['restricted'], 'allocation' => '0', 'transaction_amount' => number_format((float)$value['transaction_product_tax'], 2, '.', ''));
                    $seachResult2[] = $array1;
                }
                unset($array1);
                if ($value['transaction_product_shipping'] != '0.00' || $value['transaction_product_handling'] != '0.00') {
                    $sh_h = $value['transaction_product_shipping'] + $value['transaction_product_handling'];
                    if ($sh_h != '0' && $sh_h != '0.00') {
                        $array1 = array('transaction_id' => $value['transaction_id'], 'gl_code_value' => $glCodes['SHI_AND_HAND'], 'department' => $value['department'], 'program' => $value['program'], 'designation' => $value['designation'], 'restricted' => $value['restricted'], 'allocation' => '0', 'transaction_amount' => number_format((float)$sh_h, 2, '.', ''));
                        $seachResult2[] = $array1;
                    }
                }
                unset($array1);
            }
        }
        $searchTransactionMessage = array_merge($this->_config['transaction_messages']['config']['SAGE'], $this->_config['transaction_messages']['config']['create_crm_transaction']);
        $filename = "sage-xls-" . time() . ".xls";

        header("Content-Disposition: attachment; filename=" . $filename);
        header("Content-Type: text/x-comma-separated-values");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Pragma: no-cache");



        echo '<table border="0" cellspacing="0" cellpadding="0">
              <tr bgcolor="#cccccc">
                <th align="center" valign="middle">' . $searchTransactionMessage['TRANSACTION_NO_SAGE'] . '</th>
                <th align="center" valign="middle">' . $searchTransactionMessage['TRANSACTION_GL_ACCOUNT'] . '</th>
                <th align="center" valign="middle">' . $searchTransactionMessage['DEPT'] . '</th>
                <th align="center" valign="middle">' . $searchTransactionMessage['PROGRAM'] . '</th>
                <th align="center" valign="middle">' . $searchTransactionMessage['DESIGNATION'] . '</th>
                <th align="center" valign="middle">' . $searchTransactionMessage['RESTRICTION'] . '</th>
                <th align="center" valign="middle">' . $searchTransactionMessage['ALLOCATION'] . '</th>
                <th align="center" valign="middle">' . $searchTransactionMessage['AMOUNT'] . '</th>
               
            </tr>
        ';

        try {


            if (isset($seachResult2) and is_array($seachResult2) and count($seachResult2) > 0) {
                foreach ($seachResult2 as $val) {

                    echo '<tr>';
                    echo '<td align="center">' . $val['transaction_id'] . '</td>';
                    echo '<td align="center">' . $val['gl_code_value'] . '</td>';
                    echo '<td align="center">' . $val['department'] . '</td>';
                    echo '<td align="center">' . $val['program'] . '</td>';
                    echo '<td align="center">' . $val['designation'] . '</td>';
                    echo '<td align="center">' . $val['restricted'] . '</td>';
                    echo '<td align="center">' . $val['allocation'] . '</td>';
                    echo '<td align="center">' . number_format((float)$val['transaction_amount'], 2, '.', '') . '</td>';
                    echo '</tr>';
                }
            }
            echo '</table>';
        } catch (Exception $e) {
            echo '</table>';
        }
    }

    /**
     * This action is used to generate zip files for Batch
     * @return json
     * @param void
     * @author Icreon Tech - KK
     */
    function generateZipFilesAction() {
        $fileTime = date("D, d M Y H:i:s T");


        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $request = $this->getRequest();
        if ($request->isPost() && $request->getPost('batch_nos') != '') {
            $response = $this->getResponse();
            $transaction = new Transaction($this->_adapter);
            $postArray = $request->getPost()->toArray();
            $zip = new ZipStream("sage_batch.zip");
            $searchTransactionMessage = array_merge($this->_config['transaction_messages']['config']['SAGE'], $this->_config['transaction_messages']['config']['create_crm_transaction']);
            $batchNos = $postArray['batch_nos'];
            if (!empty($batchNos)) {
                foreach ($batchNos as $batchNo) {
                    $arrayPost['sage_batch_no'] = $batchNo;
                    //$arrayPost['update'] = 'update';
                    $fileName = "sage_batch-" . $batchNo . ".xls";
                    $header = '<table border="0" cellspacing="0" cellpadding="0">
            <tr bgcolor="#cccccc">
                <th align="center" valign="middle">' . $searchTransactionMessage['TRANSACTION_NO_SAGE'] . '</th>
                <th align="center" valign="middle">' . $searchTransactionMessage['TRANSACTION_GL_ACCOUNT'] . '</th>
                <th align="center" valign="middle">' . $searchTransactionMessage['DEPT'] . '</th>
                <th align="center" valign="middle">' . $searchTransactionMessage['PROGRAM'] . '</th>
                <th align="center" valign="middle">' . $searchTransactionMessage['DESIGNATION'] . '</th>
                <th align="center" valign="middle">' . $searchTransactionMessage['RESTRICTION'] . '</th>
                <th align="center" valign="middle">' . $searchTransactionMessage['ALLOCATION'] . '</th>
                <th align="center" valign="middle">' . $searchTransactionMessage['AMOUNT'] . '</th>
               
            </tr>
        ';
                    $str = '';
                    $searchTransactionArr = $transaction->getSageBatchSearchArr($arrayPost);
                    $seachResult = $this->_transactionTable->getAllSageBatchesForTransaction($searchTransactionArr);
                    $all_gl_codes = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getGLCodes();
                    foreach ($all_gl_codes as $key1 => $value1) {
                        $glCodes[$value1['gl_code']] = $value1['gl_code_value'];
                    }

                    if (!empty($seachResult) && $seachResult[0] != '') {
                        $seachResult2 = array();
                        foreach ($seachResult as $key => $value) {
                            
                            if (isset($value['product_type']) && $value['product_type'] != '' && in_array($value['product_type'], array('Manifest', 'Ship Image')))
                                $glCodeValue = $glCodes['PASS_DOC'];
                            else
                                $glCodeValue = $value['gl_code_value'];
                            
                            if (isset($value['product_type_id']) && $value['product_type_id']==2)
                                $transaction_amount = $value['transaction_sub_total_amount']/2;
                            else
                                $transaction_amount = $value['transaction_sub_total_amount'];
                            
                            $array1 = array('transaction_id' => $value['transaction_id'], 'gl_code_value' => $glCodeValue, 'department' => $value['department'], 'program' => $value['program'], 'designation' => $value['designation'], 'restricted' => $value['restricted'], 'allocation' => '0', 'transaction_amount' => $transaction_amount);
                            $seachResult2[] = $array1;
                            unset($array1);

                            if ($value['transaction_product_discount'] != '0.00') {
                                $array1 = array('transaction_id' => $value['transaction_id'], 'gl_code_value' => $glCodes['DISCOUNT'], 'department' => $value['department'], 'program' => $value['program'], 'designation' => $value['designation'], 'restricted' => $value['restricted'], 'allocation' => '0', 'transaction_amount' => '(' . $value['transaction_product_discount'] . ')');
                                $seachResult2[] = $array1;
                            }
                            unset($array1);
                            if ($value['transaction_product_tax'] != '0.00') {
                                $array1 = array('transaction_id' => $value['transaction_id'], 'gl_code_value' => $glCodes['TAX'], 'department' => $value['department'], 'program' => $value['program'], 'designation' => $value['designation'], 'restricted' => $value['restricted'], 'allocation' => '0', 'transaction_amount' => $value['transaction_product_tax']);
                                $seachResult2[] = $array1;
                            }
                            unset($array1);
                            if ($value['transaction_product_shipping'] != '0.00' || $value['transaction_product_handling'] != '0.00') {
                                $sh_h = $value['transaction_product_shipping'] + $value['transaction_product_handling'];
                                if ($sh_h != '0' && $sh_h != '0.00') {
                                    $array1 = array('transaction_id' => $value['transaction_id'], 'gl_code_value' => $glCodes['SHI_AND_HAND'], 'department' => $value['department'], 'program' => $value['program'], 'designation' => $value['designation'], 'restricted' => $value['restricted'], 'allocation' => '0', 'transaction_amount' => $sh_h);
                                    $seachResult2[] = $array1;
                                }
                            }
                            unset($array1);
                        }
                    }
                    if (isset($seachResult2) and is_array($seachResult2) and count($seachResult2) > 0) {
                        $sn = 1;
                        foreach ($seachResult2 as $val) {

                            $str .= '<tr>';
                            $str .= '<td align="center">' . $val['transaction_id'] . '</td>';
                            $str .= '<td align="center">' . $val['gl_code_value'] . '</td>';
                            $str .= '<td align="center">' . $val['department'] . '</td>';
                            $str .= '<td align="center">' . $val['program'] . '</td>';
                            $str .= '<td align="center">' . $val['designation'] . '</td>';
                            $str .= '<td align="center">' . $val['restricted'] . '</td>';
                            $str .= '<td align="center">' . $val['allocation'] . '</td>';
                            $str .= '<td align="center">' . number_format((float)$val['transaction_amount'], 2, '.', '') . '</td>';
                            $str .= '</tr>';
                            $sn++;
                        }
                    }
                    $str .= '</table>';
                    $content = $header . $str;
                    $zip->addFile($content, $fileName);
                }
                $zip->finalize();
            }
        }
    }

    /**
     * This action is used to get the transactions that have sent to sage
     * @return json
     * @param void
     * @author Icreon Tech - KK
     */
    public function getExportToSageDownloadAction() {

        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $params = $this->params()->fromRoute();
        $searchTransactionMessage = array_merge($this->_config['transaction_messages']['config']['SAGE'], $this->_config['transaction_messages']['config']['create_crm_transaction']);
        $transaction = new Transaction($this->_adapter);
        $request = $this->getRequest();
        $param = $this->params()->fromRoute();
        if (!empty($param['sage_batch_no'])) {

            $postArray['sage_batch_no'] = $this->decrypt($param['sage_batch_no']);

            $searchTransactionArr = $transaction->getSageBatchSearchArr($postArray);
            $seachResult = $this->_transactionTable->getAllSageBatchesForTransaction($searchTransactionArr);
            $this->downloadsagexls($seachResult);
            header("refresh: 2; /crm-sages");
            exit();
        }
    }

}

