<?php

/**
 * This controller is used to manage Rma
 * @package    Transaction
 * @author     Icreon Tech - AS
 */

namespace Transaction\Controller;

use Base\Controller\BaseController;
use Zend\View\Model\ViewModel;
use Transaction\Model\Transaction;
use Zend\Http\Request;
use Zend\Authentication\AuthenticationService;
use Transaction\Form\CreateRmaForm;
use Transaction\Form\SearchRmaForm;
use Transaction\Form\SaveSearchRmaForm;
use Transaction\Form\TransactionNotesForm;
use SimpleXMLElement;

/**
 * This controller is used to manage Rma
 * @category   Zend
 * @package    Transaction
 * @author     Icreon Tech - AS
 */
class RmaController extends BaseController {

    protected $_transactionTable = null;
    protected $_documentTable = null;
    protected $_productTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_flashMessage = null;

    function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        parent::__construct();
        $auth = new AuthenticationService();
        $this->auth = $auth;
    }

    /**
     * This function is used to get table object
     * @return     array
     * @param array $form
     * @author Icreon Tech - AS
     */
    public function getTransactionTable() {
        if (!$this->_transactionTable) {
            $sm = $this->getServiceLocator();
            $this->_transactionTable = $sm->get('Transaction\Model\TransactionTable');
            $this->_adapter = $sm->get('dbAdapter');
            $this->_config = $sm->get('Config');
        }
        return $this->_transactionTable;
    }

    /**
     * This function is used to get config object
     * @return     array
     * @param array $form
     * @author Icreon Tech - AS
     */
    public function getConfig() {
        $sm = $this->getServiceLocator();
        $this->_translator = $sm->get('translator');
        $this->_config = $sm->get('Config');
    }

    /**
     * This function is used to create crm rma
     * @return     array
     * @param array $form
     * @author Icreon Tech - AS
     */
    public function createRmaAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $this->getConfig();
        $createRmaForm = new CreateRmaForm();
        $message = $this->_config['transaction_messages']['config']['rma'];
        $response = $this->getResponse();
        $request = $this->getRequest();
        $this->layout('crm');


        if ($request->isPost()) {
            $insert['transactionId'] = $request->getPost('transaction_id');
            $insert['shipmentId'] = implode(',', $request->getPost('shipment_id'));
            $insert['rmaDate'] = DATE_TIME_FORMAT;
            $insert['rmaAction'] = '1';
            $insert['addedBy'] = $this->auth->getIdentity()->crm_user_id;
            $insert['addedDate'] = DATE_TIME_FORMAT;
            $insertProduct['addedBy'] = $this->auth->getIdentity()->crm_user_id;
            $insertProduct['addedDate'] = DATE_TIME_FORMAT;
            $returned_qty = $request->getPost('returned_qty');
            $product_reason = $request->getPost('returned_reason');
            $product_id = $request->getPost('product_id');
            $product_type = $request->getPost('product_type');
            $product_condition = $request->getPost('item_condition');
            $is_ship_return = array();
            $is_ship_return = $request->getPost('is_ship_return');
            $refund_amount = $request->getPost('product_amount');


            //$total_refund_amount = $request->getPost('total_refund_amount'); // added on 29 Dec 
            $product_tax = $request->getPost('product_tax'); // added on 29 Dec 
            $product_shipping = $request->getPost('product_shipping'); // added on 29 Dec 
            $product_handling = $request->getPost('product_handling'); // added on 29 Dec 
            $product_total_qty = $request->getPost('product_total_qty'); // added on 29 Dec 
            $product_discount = $request->getPost('product_discount');   // added on 29 Dec 

            $tra_product_id = $request->getPost('transaction_product_id');
            $tra_product_type = $request->getPost('transaction_product_type');
            //  die; 
            $insert['total_refund'] = 0;
            for ($i = 0; $i < count($returned_qty); $i++) {
                if (isset($returned_qty[$i]) && !empty($returned_qty[$i])) {
                    $productDiscount = ($product_discount[$i] / $product_total_qty[$i]) * $returned_qty[$i];
                    if ($is_ship_return[$product_id[$i]] == 1) { // if shipping return is checked then shipping, tax and handling will be calculated 
                        if ($product_total_qty[$i] == $returned_qty[$i]) { // if return quantity == total quantity
                            $insert['total_refund'] = $insert['total_refund'] + $refund_amount[$i] * $returned_qty[$i] - $product_discount[$i];
                            $insert['total_shipping'] = $insert['total_shipping'] + $product_shipping[$i] + $product_handling[$i];
                            $insert['total_tax'] = $insert['total_tax'] + $product_tax[$i];
                        } else {
                            $insert['total_refund'] = $insert['total_refund'] + $refund_amount[$i] * $returned_qty[$i] - $productDiscount;
                            $insert['total_shipping'] = $insert['total_shipping'] + $this->Roundamount((($product_shipping[$i] + $product_handling[$i]) / $product_total_qty[$i]) * $returned_qty[$i]);
                            $insert['total_tax'] = $insert['total_tax'] + $this->Roundamount((($product_tax[$i]) / $product_total_qty[$i]) * $returned_qty[$i]);
                        }
                    } else { // if shipping return is un checked
                        $insert['total_refund'] = $insert['total_refund'] + $refund_amount[$i] * $returned_qty[$i] - $productDiscount;
                        $insert['total_shipping'] = $insert['total_shipping'] + 0;
                        $insert['total_tax'] = $insert['total_tax'] + 0;
                    }
                }
            }
            $insert['total_refund'] = $this->Roundamount($insert['total_refund']);
            $insert['total_shipping'] = $this->Roundamount($insert['total_shipping']);
            $insert['total_tax'] = $this->Roundamount($insert['total_tax']);

            $transaction_rma_id = $this->getTransactionTable()->insertRma($insert);
            $changeLogArray = array();
            $changeLogArray['table_name'] = 'tbl_tra_rma_change_logs';
            $changeLogArray['activity'] = '1';/** 1 == for insert */
            $changeLogArray['id'] = $transaction_rma_id[0]['LAST_INSERT_ID'];
            $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
            $changeLogArray['added_date'] = DATE_TIME_FORMAT;
            $insertProduct['transaction_rma_id'] = $transaction_rma_id[0]['LAST_INSERT_ID'];

            for ($i = 0; $i < count($returned_qty); $i++) {
                if (isset($returned_qty[$i]) && !empty($returned_qty[$i])) {
                    $insertProduct['retQty'] = $returned_qty[$i];
                    $insertProduct['retRsn'] = $product_reason[$i];
                    $insertProduct['retProid'] = $product_id[$i];
                    $insertProduct['retProtype'] = $product_type[$i];
                    $insertProduct['itemCondition'] = $product_condition[$i];
                    if ($is_ship_return[$product_id[$i]] != '')
                        $insertProduct['isShipReturn'] = $is_ship_return[$product_id[$i]];
                    else
                        $insertProduct['isShipReturn'] = '0';

                    // if($is_ship_return[$product_id[$i]] == 1) 
                    //    $insertProduct['refundAmount'] = $refund_amount[$i];
                    // else
                    $insertProduct['refundAmount'] = $refund_amount[$i];

                    $insertProduct['traProductId'] = $tra_product_id[$i];
                    switch ($tra_product_type[$i]) {
                        case 'bio':
                            $insertProduct['traProductType'] = '1';
                            break;
                        case 'woh': $insertProduct['traProductType'] = '2';
                            break;
                        case 'fof':
                            $insertProduct['traProductType'] = '3';
                            break;
                        case 'don':
                            $insertProduct['traProductType'] = '4';
                            break;
                        case 'pro':
                            $insertProduct['traProductType'] = '5';
                            break;
                    }
                    $this->getTransactionTable()->insertRmaProduct($insertProduct);
                }
            }
            $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
            /* Script used to add the Note for RMA */

            $rmaNote = $request->getPost('note');
            if (!empty($rmaNote)) {
                $addNote['transaction_rma_id'] = $transaction_rma_id[0]['LAST_INSERT_ID'];
                $addNote['notify_note'] = $request->getPost('notify_note');
                $addNote['note'] = $request->getPost('note');
                $addNote['added_by'] = $this->auth->getIdentity()->crm_user_id;
                $addNote['added_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->addRmaNotes($addNote);
            }
            /* End Script used to add the Note for RMA */


            $this->flashMessenger()->addMessage($message['RMA_CREATED']);
            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
        $params = $this->params()->fromRoute();
        if (!empty($params['transaction_id'])) {
            $transactionId = $this->decrypt($params['transaction_id']);
            $createRmaForm->get('transaction_id')->setValue($transactionId);
            $createRmaForm->get('savebutton')->setAttributes(array('style' => 'display:block'));
            $createRmaForm->get('cancelbutton')->setAttributes(array('style' => 'display:block'));
        }
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'jsLangTranslate' => $message,
            'create_rma_form' => $createRmaForm
                )
        );
        return $viewModel;
    }

    /**
     * This function is used to get config object
     * @return     array
     * @param array $form
     * @author Icreon Tech - AS
     */
    public function getTransactionIdAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getTransactionTable();
        $message = $this->_config['transaction_messages']['config']['rma'];
        if ($request->isPost()) {
            $getSearchArray['transaction_id'] = $request->getPost('transaction_id');
            $transactionArray = $this->getTransactionTable()->getTransaction($getSearchArray);
            $transactionList = array();
            $i = 0;
            if (!empty($transactionArray)) {
                foreach ($transactionArray as $key => $val) {
                    $transactionList[$i] = array();
                    $transactionList[$i]['transaction_id'] = $val['transaction_id'];
                    $i++;
                }
            } else {
                $transactionList[$i] = array();
                $transactionList[$i]['transaction_id'] = $message['NO_RECORD_FOUND'];
            }
            return $response->setContent(\Zend\Json\Json::encode($transactionList));
        }
    }

    /**
     * This function is used to get transaction product
     * @return     array
     * @param array $form
     * @author Icreon Tech - AS
     */
    public function getTransactionProductsAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $createRmaForm = new CreateRmaForm();
        $this->getConfig();
        $message = $this->_config['transaction_messages']['config']['rma'];
        $this->getTransactionTable();
        if ($request->isPost()) {
            $getSearchArray['transactionId'] = $request->getPost('transaction_id');
            $searchProduct = $this->getTransactionTable()->searchTransactionProducts($getSearchArray);
        }

        $searchParam['transactionId'] = $request->getPost('transaction_id');
        $getItemStatus = $this->getTransactionTable()->getItemStatus();
        $itemStatus = array();
        $itemStatus[''] = 'Any';
        if ($getItemStatus !== false) {
            foreach ($getItemStatus as $key => $val) {
                $itemStatus[$val['rma_item_status_id']] = $val['rma_item_status'];
            }
        }
        $createRmaForm->get('item_condition[]')->setAttribute('options', $itemStatus);
        $count = count($searchProduct);
        $countforRma = 0;
        $shippedProduct = array();
        foreach ($searchProduct as $key => $val) {
            switch ($val['tra_pro']) {
                case 'fof':
                    $fofResult = $this->getTransactionTable()->searchFofDetails($val);
                    array_push($searchProduct[$key], $fofResult[0]);
                    break;
                case 'pro':
                    $passengerResult = $this->getTransactionTable()->searchDocPass($val);
                    array_push($searchProduct[$key], $passengerResult[0]);
                    break;
            }
            $proParam['transactionId'] = $searchParam['transactionId'];
            $proParam['productId'] = $val['id'];
            $proResult = $this->getTransactionTable()->searchProductsShipped($proParam);
            if (isset($proResult[0]) && !empty($proResult[0])) {
                $searchProduct[$key]['shippedQty'] = $proResult[0]['shippped_qty'];
            } else {
                $searchProduct[$key]['shippedQty'] = 0;
            }
        }

        // asd($searchProduct);
        foreach ($searchProduct as $key) {
            if (($key['product_status_id'] == '10' || $key['product_status_id'] == '11' || $key['product_status_id'] == '8' || $key['product_status_id'] == '9' || $key['product_status_id'] == '15')) {
                //  if ($key['item_type'] != '5' && $key['item_type'] != '6' && $key['tra_pro'] != 'don') {
                $rmaParam = array();
                $rmaParam['transaction_product_id'] = $key['id'];
                $rmaParam['product_type'] = $key['product_type'];
                $rmaResult = $this->getTransactionTable()->checkRmaAlreadyCreated($rmaParam);
                if (count($rmaResult) == 0 || $key['product_status_id'] == '15') {
                    $shippedProduct[] = $key;
                    $count++;
                    $countforRma++;
                }
                //}
            }
        }
        //die;
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => $message,
            'createRmaForm' => $createRmaForm,
            'searchProduct' => $shippedProduct,
            'count' => $count,
            'countforRma' => $countforRma
                )
        );
        return $viewModel;
    }

    /**
     * This function is used to get rma search page
     * @return     array
     * @param array $form
     * @author Icreon Tech - AS
     */
    public function getCrmRmaAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $this->getConfig();
        $searchRmaForm = new SearchRmaForm();
        $saveSearchRmaForm = new SaveSearchRmaForm();
        $message = $this->_config['transaction_messages']['config']['rma'];
        $response = $this->getResponse();
        $request = $this->getRequest();
        $getItemStatus = $this->getTransactionTable()->getRmaStatus();
        $rmaStatus[''] = 'Any';
        if ($getItemStatus !== false) {
            foreach ($getItemStatus as $key => $val) {
                $rmaStatus[$val['rma_status_id']] = $val['rma_status'];
            }
        }
        $searchRmaForm->get('status')->setAttribute('options', $rmaStatus);


        $getRmaAction = $this->getTransactionTable()->getRmaAction();
        $rmaAction = array();
        $rmaAction[''] = 'Any';

        if ($getRmaAction !== false) {
            foreach ($getRmaAction as $key => $val) {
                $rmaAction[$val['rma_action_id']] = $val['rma_action'];
            }
        }
        $searchRmaForm->get('action')->setAttribute('options', $rmaAction);

        $getSearchArray = array();
        $getSearchArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
        $getSearchArray['isActive'] = '1';
        $getSearchArray['searchId'] = '';
        $crmSearchArray = $this->getTransactionTable()->getRmaSavedSearch($getSearchArray);
        $crmSearchList = array();
        $crmSearchList[''] = 'Select';
        foreach ($crmSearchArray as $key => $val) {
            $crmSearchList[$val['rmat_search_id']] = stripslashes($val['title']);
        }
        $searchRmaForm->get('saved_search')->setAttribute('options', $crmSearchList);
        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        $this->layout('crm');
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'jsLangTranslate' => $message,
            'search_rma_form' => $searchRmaForm,
            'saveSearchRmaForm' => $saveSearchRmaForm,
            'messages' => $messages,
                )
        );
        return $viewModel;
    }

    /**
     * This function is used to get rma listing
     * @return     array
     * @param array $form
     * @author Icreon Tech - AS
     */
    public function listCrmRmaAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $searchParam['rma'] = "";
        $searchParam['name'] = "";
        $searchParam['transaction_id'] = "";
        $searchParam['status'] = "";
        parse_str($request->getPost('searchString'), $searchParam);
        $dashlet = $request->getPost('crm_dashlet');
        $dashletSearchId = $request->getPost('searchId');
        $dashletId = $request->getPost('dashletId');
        $dashletSearchType = $request->getPost('searchType');
        $dashletSearchString = $request->getPost('dashletSearchString');
        if (isset($dashletSearchString) && $dashletSearchString != '') {
            $searchParam = unserialize($dashletSearchString);
        }

        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        $searchParam['startIndex'] = $start;
        $searchParam['recordLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');
        $isExport = $request->getPost('export');
        if ($isExport != "" && $isExport == "excel") {
            $chunksize = $this->_config['export']['chunk_size'];
            $export_limit = $this->_config['export']['export_limit'];
            $searchParam['recordLimit'] = $chunksize;
        }
        $params = $this->params()->fromRoute();
        $page_counter = 1;
        do {
            $dashletResult = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getTableStruct(array('user_id' => $this->auth->getIdentity()->crm_user_id, 'search_id' => $dashletSearchId, 'dash_id' => $dashletId, 'search_type' => $dashletSearchType));
            $newExplodedColumnName = array();
            if (!empty($dashletResult)) {
                $explodedColumnName = explode(",", $dashletResult[0]['table_column_names']);
                foreach ($explodedColumnName as $val) {
                    if (strpos($val, ".")) {
                        $newExplodedColumnName[] = trim(strstr($val, ".", false), ".");
                    } else {
                        $newExplodedColumnName[] = trim($val);
                    }
                }
            }
            $searchResult = $this->getTransactionTable()->getCrmRma($searchParam);
            // asd($searchResult);

            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $total = $countResult[0]->RecordCount;
            if ($isExport == "excel") {

                /* if ($total > $export_limit) {
                  $totalExportedFiles = ceil($total / $export_limit);
                  $exportzip = 1;
                  }
                 * */

                if ($total > $chunksize) {
                    $number_of_pages = ceil($total / $chunksize);
                    $page_counter++;
                    $start = $chunksize * $page_counter - $chunksize;
                    $searchParam['startIndex'] = $start;
                    $searchParam['page'] = $page_counter;
                } else {
                    $number_of_pages = 1;
                    $page_counter++;
                }
            } else {
                $page_counter = $page;
                $number_of_pages = ceil($total / $limit);
                $jsonResult['records'] = $countResult[0]->RecordCount;
                $jsonResult['page'] = $page;
                $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
            }
            //$jsonResult['page'] = $page;
            //$jsonResult['records'] = $countResult[0]->RecordCount;
            // $jsonResult['total'] = @ceil($countResult[0]->RecordCount / $limit);

            if (count($searchResult) > 0) {
                $arrExport = array();
                foreach ($searchResult as $val) {
                    $dashletCell = array();
                    if (isset($val['transaction_rma_id']) && !empty($val['transaction_rma_id'])) {

                        if (false !== $dashletTransRmaId = array_search('transaction_rma_id', $newExplodedColumnName))
                            $dashletCell[$dashletTransRmaId] = '<a href="/view-crm-rma/' . $this->encrypt($val['transaction_rma_id']) . '/' . $this->encrypt('view') . '" class="txt-decoration-underline">' . $val['transaction_rma_id'] . "</a>";

                        if (false !== $dashletRmaDate = array_search('rma_date', $newExplodedColumnName))
                            $dashletCell[$dashletRmaDate] = $this->OutputDateFormat($val['rma_date'], 'dateformatampm');

                        if (false !== $dashletTransactionId = array_search('transaction_id', $newExplodedColumnName))
                            $dashletCell[$dashletTransactionId] = $val['transaction_id'];

                        if (false !== $dashletTransactionDate = array_search('transaction_date', $newExplodedColumnName))
                            $dashletCell[$dashletTransactionDate] = $this->OutputDateFormat($val['transaction_date'], 'dateformatampm');

                        if (false !== $dashletFirstName = array_search('first_name', $newExplodedColumnName))
                            $dashletCell[$dashletFirstName] = $val['first_name'];

                        if (false !== $dashletRmaStatus = array_search('rma_status', $newExplodedColumnName))
                            $dashletCell[$dashletRmaStatus] = $val['rma_status'];
                        if (false !== $dashletRmaStatus = array_search('revision_id', $newExplodedColumnName))
                            $dashletCell[$dashletRmaStatus] = $this->OutputDateFormat($val['transaction_date'], 'dateformatampm');

                        $arrCell['id'] = $val['transaction_rma_id'];

                        if ($val['rma_action_id'] != 5) {
                            $edit = "<a class='edit-icon' href='/view-crm-rma/" . $this->encrypt($val['transaction_rma_id']) . "/" . $this->encrypt('edit') . "'><div class='tooltip'>Edit<span></span></div></a>";
                        } else {
                            $edit = '';
                        }

                        $view = "<a class='view-icon' href='/view-crm-rma/" . $this->encrypt($val['transaction_rma_id']) . "/" . $this->encrypt('view') . "'><div class='tooltip'>View<span></span></div></a>";
                        $invoice = "<a class='link rma-slip' href='javascript:void(0)' onclick=window.open('/rma-slip/" . $val['transaction_rma_id'] . "','" . $val['transaction_rma_id'] . "','height=700,width=950,left=300,top=300,resizable=yes,scrollbars=yes')><img class='m-l-10' src='" . $this->_config['img_file_path']['path'] . "/img/comp-icon.png'><div class='tooltip'>Rma Slip<span></span></div></a>";
                        if (isset($dashlet) && $dashlet == 'dashlet') {
                            //$arrCell['cell'] = array($val['transaction_rma_id'], $this->DateFormat($val['rma_date']), $val['transaction_id'], $val['first_name']);
                            $arrCell['cell'] = $dashletCell;
                        } else if ($isExport == "excel") {
                            $arrExport[] = array('RMA #' => $val['transaction_rma_id'], 'Date Requested' => $this->OutputDateFormat($val['rma_date'], 'dateformatampm'), 'Transaction' => $val['transaction_id'], 'Purchase Date' => $this->OutputDateFormat($val['transaction_date'], 'dateformatampm'), 'Contact Name' => $val['first_name'], 'Last Action' => $val['rma_action'], 'Status' => $val['rma_status']);
                        } else {
                            $arrCell['cell'] = array($val['transaction_rma_id'], $this->OutputDateFormat($val['rma_date'], 'dateformatampm'), $val['transaction_id'], $this->OutputDateFormat($val['transaction_date'], 'dateformatampm'), $val['first_name'], $val['rma_action'], $val['rma_status'], $view . $edit . $invoice);
                        }

                        $subArrCell[] = $arrCell;
                        $jsonResult['rows'] = $subArrCell;
                    }
                }
                if ($isExport == "excel") {
                    $filename = "rma_export_" . date("Y-m-d") . ".csv";
                    $this->arrayToCsv($arrExport, $filename, $page_counter);
                }
            }
        } while ($page_counter <= $number_of_pages && $isExport == "excel");
        //return $response->setContent(\Zend\Json\Json::encode($jsonResult));
        if ($isExport == "excel") {
            //exit;
            $this->downloadSendHeaders("rma_export_" . date("Y-m-d") . ".csv");
            die;
        } else {
            $response->setContent(\Zend\Json\Json::encode($jsonResult));
            return $response;
        }
    }

    /**
     * This function is used to get rma listing
     * @return     array
     * @param array $form
     * @author Icreon Tech - AS
     */
    public function viewCrmRmaAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getTransactionTable();
        $request = $this->getRequest();
        $this->getConfig();
        $message = $this->_config['transaction_messages']['config']['rma'];
        $params = $this->params()->fromRoute();

        $searchParam['rma'] = $this->decrypt($params['id']);
        $searchParam['rma_id'] = $this->decrypt($params['id']);
        $searchResult = $this->getTransactionTable()->getCrmRma($searchParam);
        // asd($searchResult);
        $mode = (isset($params['mode']) && $params['mode'] != '') ? $this->decrypt($params['mode']) : 'view';
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'rma_id' => $params['id'],
            'rmaDecryptId' => $searchParam['rma_id'],
            'moduleName' => $this->encrypt('rma'),
            'mode' => $mode,
            'module_name' => $this->encrypt('rma'),
            'jsLangTranslate' => $message,
            'params' => $params,
            'searchResult' => $searchResult[0]
        ));
        return $viewModel;
    }

    /**
     * This function is used to edit rma 
     * @return     array
     * @param array $form
     * @author Icreon Tech - AS
     */
    public function editRmaDetailAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $this->getConfig();
        $createRmaForm = new CreateRmaForm();
        $message = $this->_config['transaction_messages']['config']['rma'];
        $response = $this->getResponse();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();

        $searchParam['rma'] = $this->decrypt($params['id']);
        if ($request->isPost() && $request->getPost('edit')) {
            parse_str($request->getPost('data'), $searchParam);
            if ($searchParam['rma_action'] == 6) {
                
                $insert['rma_id'] = $searchParam['rma_id'];
                $insert['rma_action'] = $searchParam['rma_action'];
                $insert['modifiedBy'] = $this->auth->getIdentity()->crm_user_id;
                $insert['modifiedDate'] = DATE_TIME_FORMAT;
                $this->getTransactionTable()->updateRmaStatus($insert);

            } else {
                $returned_qty = $searchParam['returned_qty'];
                $refund_amount = $searchParam['product_amount'];
                $product_tax = $searchParam['product_tax'];
                $product_shipping = $searchParam['product_shipping'];
                $product_handling = $searchParam['product_handling'];
                $product_discount = $searchParam['product_discount'];
                $product_total_qty = $searchParam['product_total_qty'];
                $is_ship_return = $searchParam['is_ship_return'];
                $product_id = $searchParam['product_id'];
                $insert['total_refund'] = 0;
                for ($i = 0; $i < count($returned_qty); $i++) {
                    if (isset($returned_qty[$i]) && !empty($returned_qty[$i])) {

                        $productDiscount = ($product_discount[$i] / $product_total_qty[$i]) * $returned_qty[$i];
                        if ($is_ship_return[$product_id[$i]] == 1) { // if shipping return is checked then shipping, tax and handling will be calculated 
                            if ($product_total_qty[$i] == $returned_qty[$i]) { // if return quantity == total quantity
                                $insert['total_refund'] = $insert['total_refund'] + $refund_amount[$i] * $returned_qty[$i] - $product_discount[$i];
                                $insert['total_shipping'] = $insert['total_shipping'] + ($product_shipping[$i] + $product_handling[$i]);
                                $insert['total_tax'] = $insert['total_tax'] + $product_tax[$i];
                            } else {
                                $insert['total_refund'] = $insert['total_refund'] + $refund_amount[$i] * $returned_qty[$i] - $productDiscount;
                                $insert['total_shipping'] = $insert['total_shipping'] + $this->Roundamount((($product_shipping[$i] + $product_handling[$i]) / $product_total_qty[$i]) * $returned_qty[$i]);
                                $insert['total_tax'] = $insert['total_tax'] + $this->Roundamount(($product_tax[$i] / $product_total_qty[$i]) * $returned_qty[$i]);
                            }
                        } else { // if shipping return is un checked
                            $insert['total_refund'] = $insert['total_refund'] + $refund_amount[$i] * $returned_qty[$i] - $productDiscount;
                            $insert['total_shipping'] = $insert['total_shipping'] + 0;
                            $insert['total_tax'] = $insert['total_tax'] + 0;
                        }
                    }
                }
                $totalRefundAmount = $insert['total_refund'];
                $insert['rma_id'] = $searchParam['rma_id'];
                $insert['rma_action'] = $searchParam['rma_action'];
                $insert['rma_status'] = $searchParam['rma_status'];
                $insert['bank_name'] = $searchParam['bank_name'];
                $insert['check_no'] = $searchParam['check_no'];
                $insert['transaction'] = $searchParam['transaction'];
                $insert['transaction_id'] = $searchParam['transaction_id'][0];

                if (!empty($searchParam['amount']) && $searchParam['amount'] != 0 && $searchParam['rma_action'] == 5) {
                    $insert['amount'] = $searchParam['amount'];
                    $insert['total_refund'] = $searchParam['amount'];
                    $insert['amount_shipping'] = $searchParam['refund_shipping'];
                    $insert['amount_tax'] = $searchParam['refund_tax'];
                } else {
                    $insert['amount'] = $insert['total_refund'];
                    $insert['amount_shipping'] = $insert['total_shipping'];
                    $insert['amount_tax'] = $insert['total_tax'];
                }

                // Refund payment process start here

                if ($searchParam['rma_action'] == 5 && $searchParam['rma_status'] == 3) {
                    $refundByCreditCardAmount = $searchParam['amount'] + $searchParam['refund_shipping'] + $searchParam['refund_tax'];
                    $paymentProfileParam = array();
                    $paymentProfileParam['transaction_id'] = $searchParam['transaction_id'][0];
                    $profileDetails = $this->getTransactionTable()->getPaymentProfileDetails($paymentProfileParam);
                    
                    if (count($profileDetails) > 0 && $profileDetails[0]['credit_card_no']!='') {
                        $userCeditCardNo = $profileDetails[0]['credit_card_no'];
                        $userCeditCardExpiryDate = "XXXX";
                        $authorizeTransactionId = $profileDetails[0]['authorize_transaction_id'];
                    }
                    else {
                        $transactionSearchParam = array();
                        $transactionSearchParam['transactionId'] = $searchParam['transaction_id'][0];
                        $transProfileDetails = $this->getTransactionTable()->getTransactionProfileId($transactionSearchParam);
                        $createCustomerProfile = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
                        $createCustomerProfile->getCustomerProfileRequest(array('customerProfileId' => $transProfileDetails[0]['profile_id']));
                        $userCeditCardNo = (string) $createCustomerProfile->profile->paymentProfiles->payment->creditCard->cardNumber;
                        //$userCeditCardExpiryDate = (string) $createCustomerProfile->profile->paymentProfiles->payment->creditCard->expirationDate;
                        $userCeditCardExpiryDate = "XXXX";
                        $authorizeTransactionId = $transProfileDetails[0]['authorize_transaction_id'];
                       
                    }

                        $payment = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
                        $payment->createTransactionRequest(array(
                            'refId' => rand(1000000, 100000000),
                            'transactionRequest' => array(
                                'transactionType' => 'refundTransaction',
                                'amount' => $refundByCreditCardAmount,
                                'payment' => array(
                                    'creditCard' => array(
                                        'cardNumber' => $userCeditCardNo,
                                        'expirationDate' => $userCeditCardExpiryDate,
                                    )
                                ),
                                'refTransId' => $authorizeTransactionId,
                                'order' => array(
                                    'invoiceNumber' => 'IN' . sprintf('%011d', $searchParam['transaction_id'][0]),
                                    'description' => 'Refund transaction'),
                            ),
                        ));

                        if ($payment->isSuccessful()) {
                            // continue
                        } else {
                            $paymentErrorArray = explode('-', $payment->messages->message->text);
                            if (isset($paymentErrorArray[1]) && !empty($paymentErrorArray[1]))
                                $paymentErrMessage = $paymentErrorArray[1];
                            else
                                $paymentErrMessage = $paymentErrorArray[0];

                            $this->flashMessenger()->addMessage($paymentErrMessage);
                            $messages = array('status' => 'error', 'msg' => $paymentErrMessage);
                            $response->setContent(\Zend\Json\Json::encode($messages));
                            return $response;
                        }
                }

                // End Refund payment process end here
                //  die;
                $insert['modifiedBy'] = $this->auth->getIdentity()->crm_user_id;
                $insert['modifiedDate'] = DATE_TIME_FORMAT;
                $insertProduct['modifiedBy'] = $this->auth->getIdentity()->crm_user_id;
                $insertProduct['modifiedDate'] = DATE_TIME_FORMAT;
                $this->getTransactionTable()->updateRma($insert);

                //$changeLogArray = array();
                $returned_qty = array();
                /*$changeLogArray['table_name'] = 'tbl_tra_rma_change_logs';
                $changeLogArray['activity'] = '2';
                $changeLogArray['id'] = $searchParam['rma_id'];
                $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $changeLogArray['added_date'] = DATE_TIME_FORMAT;*/
                $returned_qty = $searchParam['returned_qty'];
                $product_reason = $searchParam['returned_reason'];
                $product_rma_id = $searchParam['rma_product_id'];
                $product_condition = $searchParam['item_condition'];
                $transaction_product_id = $searchParam['transaction_product_id'];
                $transaction_product_type = $searchParam['transaction_product_type'];
                $product_total_qty = $searchParam['product_total_qty'];
                for ($i = 0; $i < count($returned_qty); $i++) {

                    $updateProductStatus = array();
                    $insertProduct['retQty'] = $returned_qty[$i];
                    $insertProduct['retRsn'] = $product_reason[$i];
                    $insertProduct['retProid'] = $product_rma_id[$i];
                    $insertProduct['itemCondition'] = $product_condition[$i];
                    $insertProduct['itemCondition'] = $product_condition[$i];
                    if ($is_ship_return[$product_id[$i]] != '')
                        $insertProduct['isShipReturn'] = $is_ship_return[$product_id[$i]];
                    else
                        $insertProduct['isShipReturn'] = '0';

                    $this->getTransactionTable()->updateRmaProduct($insertProduct);
                    if (isset($returned_qty[$i]) && !empty($returned_qty[$i])) {
                        if ($searchParam['rma_action'] == 5 || $searchParam['rma_action'] == 3) { // Refund and closed
                            $updateProductStatus['transactionProductType'] = $transaction_product_type[$i];
                            $updateProductStatus['transactionProductId'] = $transaction_product_id[$i];
                            if ($returned_qty[$i] == $product_total_qty[$i]) {
                                $updateProductStatus['transactionProductStatus'] = '13'; // Return Status
                            } else {
                                $updateProductStatus['transactionProductStatus'] = '15'; // Partial Return Status
                            }

                            if (isset($returned_qty[$i]) && !empty($returned_qty[$i])) {

                                $productDiscount = ($product_discount[$i] / $product_total_qty[$i]) * $returned_qty[$i];
                                if ($is_ship_return[$product_id[$i]] == 1) { // if shipping return is checked then shipping, tax and handling will be calculated 
                                    if ($product_total_qty[$i] == $returned_qty[$i]) { // if return quantity == total quantity
                                        $updateProductStatus['refundAmount'] = $refund_amount[$i] * $returned_qty[$i] - $product_discount[$i];
                                        $updateProductStatus['refundShipping'] = $product_shipping[$i] + $product_handling[$i];
                                        $updateProductStatus['refundTax'] = $product_tax[$i];
                                    } else {
                                        $updateProductStatus['refundAmount'] = $refund_amount[$i] * $returned_qty[$i] - $productDiscount;
                                        $updateProductStatus['refundShipping'] = (($product_shipping[$i] + $product_handling[$i]) / $product_total_qty[$i]) * $returned_qty[$i];
                                        $updateProductStatus['refundTax'] = (($product_tax[$i]) / $product_total_qty[$i]) * $returned_qty[$i];
                                    }
                                } else {
                                    $updateProductStatus['refundAmount'] = $refund_amount[$i] * $returned_qty[$i] - $productDiscount;
                                    $updateProductStatus['refundShipping'] = 0;
                                    $updateProductStatus['refundTax'] = 0;
                                }
                            }

                            $this->getTransactionTable()->updateTransactionProductStatus($updateProductStatus);
                        } else {
                            /*
                              if($searchParam['rma_action'] != 6) { // 6 = Returned Check

                              $updateProductStatus['transactionProductType'] = $transaction_product_type[$i];
                              $updateProductStatus['transactionProductId'] = $transaction_product_id[$i];
                              $updateProductStatus['transactionProductStatus'] = '8'; // process Status
                              $this->getTransactionTable()->updateTransactionProductStatus($updateProductStatus);
                              }
                             */
                        }
                    }
                }
                if ($searchParam['rma_action'] != 5 && $searchParam['rma_action'] != 3) {
                    if (isset($searchParam['transaction_status_id']) && ($searchParam['transaction_status_id'] != 9 && $searchParam['transaction_status_id'] != 10 && $searchParam['transaction_status_id'] != 11 && $searchParam['transaction_status_id'] != 12)) {
                        $update = array();
                        $update['transactionStatus'] = '8';
                        $update['transactionId'] = $searchParam['transaction_id'][0];
                        $update['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                        $update['modified_date'] = DATE_TIME_FORMAT;
                        $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->updateTransactionStatus($update);
                    }
                }
                /* update channel/campaign revenue amount */
                $revenueParam = array();
                $revenueParam['transaction_id'] = $searchParam['transaction_id'][0];
                $userTransactionArray = $this->getTransactionTable()->getUserTransactionDetails($revenueParam);
                $userCodeFlag = false;
                if (count($userTransactionArray) > 0) {
                    $campaignCode = $userTransactionArray[0]['campaign_code'];
                    $campaignId = $userTransactionArray[0]['campaign_id'];
                    $channelId = $userTransactionArray[0]['channel_id'];
                    $userCodeFlag = true;
                }
                if ($userCodeFlag == true) {
                    if ($searchParam['rma_action'] == 3 && ($searchParam['old_rma_action_id'] == 1 || $searchParam['old_rma_action_id'] == 2 || $searchParam['old_rma_action_id'] == 4)) {
                        $rmaAmountForRevenue = array();
                        //$rmaAmountForRevenue['amount'] = $insert['amount'] + $insert['amount_shipping'] + $insert['amount_tax'];
                        $rmaAmountForRevenue['amount'] = $insert['amount'];
                        $rmaAmountForRevenue['campaign_id'] = $campaignId;
                        $rmaAmountForRevenue['channel_id'] = $channelId;
                        $rmaAmountForRevenue['amount_action'] = 'minus';
                        $this->getTransactionTable()->updateTotalRevenueAmount($rmaAmountForRevenue);
                    }
                    /* if (($searchParam['rma_action'] == 1 || $searchParam['rma_action'] == 2 || $searchParam['rma_action'] == 4) && $searchParam['old_rma_action_id'] == 3) {
                        $rmaAmountForRevenue = array();
                        //$rmaAmountForRevenue['amount'] = $insert['amount'] + $insert['amount_shipping'] + $insert['amount_tax'];
                        $rmaAmountForRevenue['amount'] = $insert['amount'];
                        $rmaAmountForRevenue['campaign_id'] = $campaignId;
                        $rmaAmountForRevenue['channel_id'] = $channelId;
                        $rmaAmountForRevenue['amount_action'] = 'plus';
                        $this->getTransactionTable()->updateTotalRevenueAmount($rmaAmountForRevenue);
                    }
                    if ($searchParam['rma_action'] == 5 && $searchParam['old_rma_action_id'] != 3 && $searchParam['old_rma_action_id'] != 6) {
                        $rmaAmountForRevenue = array();
                        //$rmaAmountForRevenue['amount'] = $insert['amount'] + $insert['amount_shipping'] + $insert['amount_tax'];
                        $rmaAmountForRevenue['amount'] = $insert['amount'];
                        $rmaAmountForRevenue['campaign_id'] = $campaignId;
                        $rmaAmountForRevenue['channel_id'] = $channelId;
                        $rmaAmountForRevenue['amount_action'] = 'minus';
                        $this->getTransactionTable()->updateTotalRevenueAmount($rmaAmountForRevenue);
                    } */
                }

                /* End update channel/campaign revenue// update channel/campaign revenue amount     */


                //if($searchParam['rma_action'] == 3) // changed after discussion
                if ($searchParam['rma_action'] == 5 || $searchParam['rma_action'] == 3) {
                    /* update transaction status to return if all products have been returned start */
                    $getSearchArray['transactionId'] = $searchParam['transaction_id'][0];
                    $productsInTrans = $this->getTransactionTable()->searchTransactionProducts($getSearchArray);
                    $totalProductInTrans = 0;
                    $completelyRefundProductsCount = 0;
                    if (is_array($productsInTrans) && count($productsInTrans) > 0) {
                        $totalProductInTrans = count($productsInTrans);
                        foreach ($productsInTrans as $product) {
                            if ($product['product_status_id'] == 13 || $product['product_status_id'] == 15) {
                                $completelyRefundProductsCount++;
                            }
                        }
                    }
                    if ($totalProductInTrans == $completelyRefundProductsCount && $completelyRefundProductsCount > 0) {
                        $update = array();
                        $update['transactionStatus'] = 13;
                        $update['transactionId'] = $searchParam['transaction_id'][0];
                        $update['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                        $update['modified_date'] = DATE_TIME_FORMAT;
                        $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->updateTransactionStatus($update);
                    }
                }
            }
                $changeLogArray = array();
                $returned_qty = array();
                $changeLogArray['table_name'] = 'tbl_tra_rma_change_logs';
                $changeLogArray['activity'] = '2';/** 2 == for update */
                $changeLogArray['id'] = $searchParam['rma_id'];
                $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                /* Script used to add the Note for RMA */
                if (!empty($searchParam['note'])) {
                    $addNote['transaction_rma_id'] = $this->decrypt($params['id']);
                    $addNote['notify_note'] = $searchParam['notify_note'];
                    $addNote['note'] = $searchParam['note'];
                    $addNote['added_by'] = $this->auth->getIdentity()->crm_user_id;
                    $addNote['added_date'] = DATE_TIME_FORMAT;
                    $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->addRmaNotes($addNote);
                }
                /* End Script used to add the Note for RMA */



                $this->flashMessenger()->addMessage($message['RMA_UPDATED']);
                $messages = array('status' => "success");
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
           
        }
        // asd($searchParam);
        $searchResult = $this->getTransactionTable()->getCrmRma($searchParam);
        $searchProduct = $this->getTransactionTable()->getCrmRmaProduct($searchResult[0]);
        $searchParam['rma_id'] = $searchParam['rma'];
        $searchNotes = $this->getTransactionTable()->getCrmRmaNotes($searchParam);

        $traParam = array();
        $paymentMode = 0;
        $traParam['transactionId'] = $searchResult[0]['transaction_id'];
        $transactionPaymentArray = $this->getTransactionTable()->getTransactionPayments($traParam);
        if (count($transactionPaymentArray) > 0)
            $paymentMode = $transactionPaymentArray[0]['payment_mode_id'];

        foreach ($searchProduct as $key => $val) {
            $passArrayParam = array();
            $passArrayParam['product_type_id'] = $val['product_type'];
            $passArrayParam['item_id'] = $val['item_id'];
            $passArrayParam['id'] = $val['transaction_product_id'];

            switch ($val['tra_pro']) {
                case 'fof':
                    $fofResult = $this->getTransactionTable()->searchFofDetails($passArrayParam);
                    array_push($searchProduct[$key], $fofResult[0]);
                    break;
                case 'pro':
                    $passengerResult = $this->getTransactionTable()->searchDocPass($passArrayParam);
                    array_push($searchProduct[$key], $passengerResult[0]);
                    break;
            }

            $proParam['transactionId'] = $val['transactionId'];
            $proParam['productId'] = $val['transaction_product_id'];

            $proResult = $this->getTransactionTable()->searchProductsShipped($proParam);
            if (isset($proResult[0]) && !empty($proResult[0])) {
                $searchProduct[$key]['qty_shipped'] = $proResult[0]['shippped_qty'];
            } else {
                $searchProduct[$key]['qty_shipped'] = 0;
            }
        }

        $getItemStatus = $this->getTransactionTable()->getItemStatus();
        $itemStatus = array();
        $itemStatus[''] = 'Select';
        if ($getItemStatus !== false) {
            foreach ($getItemStatus as $key => $val) {
                $itemStatus[$val['rma_item_status_id']] = $val['rma_item_status'];
            }
        }
        $createRmaForm->get('item_condition[]')->setAttribute('options', $itemStatus);
        $getRmaAction = $this->getTransactionTable()->getRmaAction();
        $rmaAction = array();
        $rmaAction[''] = 'Select';

        if ($getRmaAction !== false) {
            foreach ($getRmaAction as $key => $val) {
                $rmaAction[$val['rma_action_id']] = $val['rma_action'];
            }
        }
        if ($searchResult[0]['rma_action_id'] == 3 || $searchResult[0]['rma_action_id'] == 6) { // if approved
            unset($rmaAction[1]);
            unset($rmaAction[2]);
            unset($rmaAction[3]);
            unset($rmaAction[4]);
        }
        if ($searchResult[0]['rma_action_id'] == 1 || $searchResult[0]['rma_action_id'] == 2 || $searchResult[0]['rma_action_id'] == 4) { // if not approved
            unset($rmaAction[5]);
            unset($rmaAction[6]);
        }

        $createRmaForm->get('rma_action')->setAttribute('options', $rmaAction);
        $createRmaForm->get('savebutton')->setAttributes(array('style' => 'display:block'));
        $createRmaForm->get('cancelbutton')->setAttributes(array('style' => 'display:block'));

        $getRmaStatus = $this->getTransactionTable()->getRmaStatus();

        $rmaStatus = array();
        $rmaStatus[''] = 'Select';
        if ($getRmaStatus !== false) {
            foreach ($getRmaStatus as $key => $val) {
                $rmaStatus[$val['rma_status_id']] = $val['rma_status'];
            }
        }
        if ($paymentMode == 2 || $paymentMode == 3 || $paymentMode == 4)
            unset($rmaStatus[3]);

        $createRmaForm->get('rma_status')->setAttribute('options', $rmaStatus);
        $createRmaForm->get('rma_action')->setValue($searchResult[0]['rma_action_id']);
        $createRmaForm->get('rma_status')->setValue($searchResult[0]['rma_status_id']);
        $createRmaForm->get('bank_name')->setValue($searchResult[0]['refund_bank_name']);
        $createRmaForm->get('amount')->setValue($searchResult[0]['rma_refund_amount']);
        $createRmaForm->get('refund_shipping')->setValue($searchResult[0]['rma_refund_shipping']);
        $createRmaForm->get('refund_tax')->setValue($searchResult[0]['rma_refund_tax']);
        $createRmaForm->get('transaction')->setValue($searchResult[0]['refund_transaction_id']);
        $createRmaForm->get('check_no')->setValue($searchResult[0]['refund_cheque_no']);
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'rma_id' => $params['id'],
            'moduleName' => $this->encrypt('rma'),
            'searchResult' => $searchResult[0],
            'searchProduct' => $searchProduct,
            'jsLangTranslate' => $message,
            'params' => $params,
            'createRmaForm' => $createRmaForm,
            'notes' => $searchNotes,
            'userId' => $this->auth->getIdentity()->crm_user_id,
            'tableName' => 'tbl_tra_rma_notes',
            'tableField' => 'rma_note_id',
            'refund_shipping' => $searchResult[0]['rma_refund_shipping'],
            'refund_tax' => $searchResult[0]['rma_refund_tax']
        ));
        return $viewModel;
    }

    /**
     * This function is used to view rma details
     * @return     array
     * @param array $form
     * @author Icreon Tech - AS
     */
    public function viewRmaDetailAction() {
        $this->checkUserAuthentication();
        $rmaNotesForm = new TransactionNotesForm();
        $this->getTransactionTable();
        $request = $this->getRequest();
        $this->getConfig();
        $message = $this->_config['transaction_messages']['config']['rma'];
        $params = $this->params()->fromRoute();
        $searchParam['rma'] = $this->decrypt($params['id']);
        $searchParam['rma_id'] = $this->decrypt($params['id']);
        $rmaNotesForm->get('transaction_id')->setAttribute('value', $searchParam['rma']);
        $searchResult = $this->getTransactionTable()->getCrmRma($searchParam);
        $searchProduct = $this->getTransactionTable()->getCrmRmaProduct($searchResult[0]);

        foreach ($searchProduct as $key => $val) {
            $passArrayParam = array();
            $passArrayParam['product_type_id'] = $val['product_type'];
            $passArrayParam['item_id'] = $val['item_id'];
            $passArrayParam['id'] = $val['transaction_product_id'];

            switch ($val['tra_pro']) {
                case 'fof':
                    $fofResult = $this->getTransactionTable()->searchFofDetails($passArrayParam);
                    array_push($searchProduct[$key], $fofResult[0]);
                    break;
                case 'pro':
                    $passengerResult = $this->getTransactionTable()->searchDocPass($passArrayParam);
                    array_push($searchProduct[$key], $passengerResult[0]);
                    break;
            }
            $proParam['transactionId'] = $val['transactionId'];
            $proParam['productId'] = $val['transaction_product_id'];

            $proResult = $this->getTransactionTable()->searchProductsShipped($proParam);
            if (isset($proResult[0]) && !empty($proResult[0])) {
                $searchProduct[$key]['qty_shipped'] = $proResult[0]['shippped_qty'];
            } else {
                $searchProduct[$key]['qty_shipped'] = 0;
            }
        }

        $searchNotes = $this->getTransactionTable()->getCrmRmaNotes($searchParam);
        $mode = (isset($params['mode']) && $params['mode'] != '') ? $this->decrypt($params['mode']) : 'view';
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'rma_id' => $params['id'],
            'moduleName' => $this->encrypt('rma'),
            'searchResult' => $searchResult[0],
            'searchProduct' => $searchProduct,
            'mode' => $mode,
            'jsLangTranslate' => $message,
            'params' => $params,
            'rmaNotesForm' => $rmaNotesForm,
            'notes' => $searchNotes,
            'userId' => $this->auth->getIdentity()->crm_user_id,
            'tableName' => 'tbl_tra_rma_notes',
            'tableField' => 'rma_note_id'
        ));
        return $viewModel;
    }

    /**
     * This function is used to view rma details
     * @return     array
     * @param array $form
     * @author Icreon Tech - AS
     */
    public function rmaSlipAction() {
        $this->checkUserAuthentication();
        $this->layout('popup');
        $this->getTransactionTable();
        $request = $this->getRequest();
        $this->getConfig();
        $message = $this->_config['transaction_messages']['config']['rma'];
        $params = $this->params()->fromRoute();
        $searchParam['rma'] = $params['id'];
        $searchResult = $this->getTransactionTable()->getCrmRma($searchParam);
        $searchProduct = $this->getTransactionTable()->getCrmRmaProduct($searchResult[0]);

        foreach ($searchProduct as $key => $val) {
            $passArrayParam = array();
            $passArrayParam['product_type_id'] = $val['product_type'];
            $passArrayParam['item_id'] = $val['item_id'];
            $passArrayParam['id'] = $val['transaction_product_id'];

            switch ($val['tra_pro']) {
                case 'fof':
                    $fofResult = $this->getTransactionTable()->searchFofDetails($passArrayParam);
                    array_push($searchProduct[$key], $fofResult[0]);
                    break;
                case 'pro':
                    $passengerResult = $this->getTransactionTable()->searchDocPass($passArrayParam);
                    array_push($searchProduct[$key], $passengerResult[0]);
                    break;
            }

            $proParam['transactionId'] = $val['transactionId'];
            $proParam['productId'] = $val['transaction_product_id'];

            $proResult = $this->getTransactionTable()->searchProductsShipped($proParam);
            if (isset($proResult[0]) && !empty($proResult[0])) {
                $searchProduct[$key]['qty_shipped'] = $proResult[0]['shippped_qty'];
            } else {
                $searchProduct[$key]['qty_shipped'] = 0;
            }
        }


        $mode = (isset($params['mode']) && $params['mode'] != '') ? $this->decrypt($params['mode']) : 'view';
        $viewModel = new ViewModel();

        $viewModel->setVariables(array(
            'rma_id' => $params['id'],
            'moduleName' => $this->encrypt('rma'),
            'searchResult' => $searchResult[0],
            'searchProduct' => $searchProduct,
            'mode' => $mode,
            'module_name' => $this->encrypt('rma'),
            'jsLangTranslate' => $message,
            'params' => $params
        ));
        return $viewModel;
    }

    /**
     * This action is used to add note to transaction
     * @return json
     * @param void
     * @author Icreon Tech - AS
     */
    public function addRmaNoteAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $viewModel = new ViewModel();
        $params = $this->params()->fromRoute();
        $addNote['transaction_rma_id'] = $request->getPost('transaction_id');
        $addNote['notify_note'] = $request->getPost('notify_note');
        $addNote['note'] = $request->getPost('note');
        $addNote['added_by'] = $this->auth->getIdentity()->crm_user_id;
        $addNote['added_date'] = DATE_TIME_FORMAT;
        $searchResult = $this->getTransactionTable()->addRmaNotes($addNote);

        $replydivClass = $request->getPost('replydivClass');
        $replydivClass = isset($replydivClass) ? $replydivClass : 'alterbg alterbg-none';

        $addedNote = "<div class='row " . $replydivClass . "'>";
        if (isset($searchResult[0]['note_private']) and trim($searchResult[0]['note_private']) != "") {
            $addedNote .= "<strong class='right bold m-b-10 aright'> " . $searchResult[0]['note_private'] . "</strong>";
        }
        $addedNote .= "<p class='text'>" . $searchResult[0]['note'] . "</p>
            <p class='text right no-padd'><strong>Date Created: </strong>" . $this->OutputDateFormat($searchResult[0]['added_date'], 'dateformatampm') . " by " . $searchResult[0]['name'] . "</p>
        </div>";
        $messages = array('status' => "success", 'content' => $addedNote);
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This Action is used to save a particular campaign search
     * @return message in json format
     * @param $searchParam
     * @author Icreon Tech-AS
     */
    public function saveRmaSearchAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getConfig();
        $message = $this->_config['transaction_messages']['config']['rma'];
        if ($request->isPost()) {
            $search_name = $request->getPost('search_name');
            $messages = array();
            parse_str($request->getPost('searchString'), $searchParam);
            $searchParam['search_title'] = $search_name;
            if (trim($search_name) != '') {
                $saveSearchParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $saveSearchParam['isActive'] = 1;
                $saveSearchParam['isDelete'] = 0;
                $saveSearchParam['search_name'] = $search_name;
                $saveSearchParam['search_query'] = serialize($searchParam);
                if ($request->getPost('search_id') != '') {
                    $saveSearchParam['modifiend_date'] = DATE_TIME_FORMAT;
                    $saveSearchParam['search_id'] = $request->getPost('search_id');
                    if ($this->getTransactionTable()->updateRmaSearch($saveSearchParam) == true) {
                        $this->flashMessenger()->addMessage($message['SEARCH_UPDATED']);
                        $messages = array('status' => "success");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                } else {
                    $saveSearchParam['added_date'] = DATE_TIME_FORMAT;
                    if ($this->getTransactionTable()->saveRmaSearch($saveSearchParam) == true) {
                        $this->flashMessenger()->addMessage($message['SEARCH_SAVED']);
                        $messages = array('status' => "success");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                }
            }
        }
    }

    /**
     * This Action is used to get the saved crm campaign search
     * @param this will pass an array $dataParam
     * @return this will be json format 
     * @author Icreon Tech - AS
     */
    public function getRmaSearchSavedSelectAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            if ($request->getPost('search_id') != '') {
                $dataParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $dataParam['isActive'] = '1';
                $dataParam['searchId'] = $request->getPost('search_id');
                $searchResult = $this->getTransactionTable()->getRmaSavedSearch($dataParam);
                $searchResult = json_encode(unserialize($searchResult[0]['search_query']));
                return $response->setContent($searchResult);
            }
        }
    }

    /**
     * This Action is used to delete the saved crm campaign search
     * @param this will pass an array $dataParam
     * @return this will be json format 
     * @author Icreon Tech - AS
     */
    public function deleteRmaSearchAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam['search_id'] = $request->getPost('search_id');
            $dataParam['isDelete'] = 1;
            $dataParam['modifiend_date'] = DATE_TIME_FORMAT;
            $this->getTransactionTable()->deleteRmaSearch($dataParam);
            $this->flashMessenger()->addMessage($this->_config['transaction_messages']['config']['rma']['SEARCH_DELETED']);
            $messages = array('status' => "success");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

}