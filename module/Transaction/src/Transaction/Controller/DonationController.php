<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Transaction\Controller;

use Base\Controller\BaseController;
use Pledge\Controller\PledgeController;
use Transaction\Controller\TransactionController;
use Zend\View\Model\ViewModel;
use Transaction\Model\Transaction;
use Transaction\Model\Donation;
use Lead\Model\Lead;
use Common\Model\Common;
use Zend\Http\Request;
use Zend\Authentication\AuthenticationService;
use Transaction\Form\SearchUserTransactionForm;
use Transaction\Form\SearchUserForm;
use Transaction\Form\SearchTransactionForm;
use Transaction\Form\SaveSearchForm;
use Transaction\Form\CreateTrasactionForm;
use Transaction\Form\CheckoutFormFront;
use Transaction\Form\PaymentModeCheck;
use Transaction\Form\PaymentModeCredit;
use Transaction\Form\AddToCartForm;
use Transaction\Form\DonationForm;
use Transaction\Form\DonationNotifyForm;
use Transaction\Form\ViewTransactionForm;
use Transaction\Form\TransactionNotesForm;
use Transaction\Form\MembershipDetailForm;
use Transaction\Form\WallOfHonor;
use Transaction\Form\ViewFofImageForm;
use Transaction\Form\ViewWohDetailsForm;
use Transaction\Form\WallOfHonorAdditionalContact;
use Product\Controller\ProductController;
use Passenger\Controller\PassengerController;
use Transaction\Form\TransactionStatusForm;
use Transaction\Form\ProductStatusForm;
use Transaction\Form\UpdateTrackCodeForm;
use Transaction\Form\UpdateTransactionBatchForm;
use Zend\Barcode\Barcode;
use Authorize\lib\AuthnetXML;
use Pledge\Model\Pledge;
use SimpleXMLElement;
use Base\Model\html2fpdf;
use Zend\View\Renderer\PhpRenderer;
use Zend\Session\Container;
use Zend\Session\Container as SessionContainer;
use Base\Model\ZipStream;
use Product\Model\Category;
use Zend\Db\Adapter\Driver\ConnectionInterface;
use Zend\Db\Adapter\Adapter;
use Common\Controller\DoController;
use Transaction\Form\DonationCheckoutForm;

/**
 * This controller is used to manage Transactions
 * @category   Zend
 * @package    Transaction
 * @author     Icreon Tech - DG
 */
class DonationController extends BaseController {

    protected $_donationTable = null;
    protected $_transactionTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_flashMessage = null;
    public $auth = null;

    function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        parent::__construct();
        $auth = new AuthenticationService();
        $this->auth = $auth;
    }

    /**
     * This function is used to get table object
     * @return     array
     * @param array $form
     * @author Icreon Tech - DG
     */
    public function getTransactionTable() {
        if (!$this->_transactionTable) {
            $sm = $this->getServiceLocator();
            $this->_transactionTable = $sm->get('Transaction\Model\TransactionTable');
            $this->_donationTable = $sm->get('Transaction\Model\DonationTable');
            $this->_adapter = $sm->get('dbAdapter');
            $this->_config = $sm->get('Config');
        }
        return $this->_transactionTable;
    }

    /**
     * This function is used to get config object
     * @return     array
     * @param array $form
     * @author Icreon Tech - DG
     */
    public function getConfig() {
        $sm = $this->getServiceLocator();
        $this->_translator = $sm->get('translator');
        $this->_config = $sm->get('Config');
    }

    /**
     * This Action is used to get donation add to cart detail
     * @param Void
     * @return String
     * @author Icreon Tech - DT dev 2
     */
    public function getDonationFrontAction() {
        //$this->checkFrontUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $amount = 0;
        if ($request->isPost()) {
            $donateamount = $request->getPost('amount');
            $amount = trim($donateamount, '$');
        }
        $this->getTransactionTable();
        $this->donationSession = new Container('donation');
        $this->donationSession->getManager()->getStorage()->clear('donation');
        $data = array();
        $data[0] = '';
        $data[1] = '';
        $data[3] = 'asc';
        $data[2] = 'tbl_membership.minimun_donation_amount';
        $membershipData = $this->getServiceLocator()->get('User\Model\MembershipTable')->getAllMembership($data);

        //$membership = array(' ' => 'Select');
        foreach ($membershipData as $key => $value) {
            if ($value['membership_id'] != '1' && $value['minimun_donation_amount'] > 0) {
            $membership[$value['minimun_donation_amount']] = '$'.round($value['minimun_donation_amount'])  . '-' . $value['membership_title'];
            }
          }
          $membership['other'] = '$';
         $donationForm = new DonationForm();

        $donationNotifyForm = new DonationNotifyForm();
        $createTransactionMessage = $this->_config['transaction_messages']['config']['donate_transaction_front'];
        $transaction = new Transaction($this->_adapter);
        $donationForm->add($donationNotifyForm);
        $donationForm->get('amount')->setAttributes(array('options' => $membership));
		if(!empty($amount)){
			$donationForm->get('other_amount')->setAttributes(array('value' => $amount));
			}
			
        $viewModel = new ViewModel();
        $programArray = $this->getTransactionTable()->getProgramWithCampaign(array());

        $campaignSession = new Container('campaign');
        $campCode = $campaignSession->ccd;

        $program_id = '';
        $campaign_id = '';
        if (!empty($campCode)) {
            $campaignDetail = $this->getServiceLocator()->get('Campaign\Model\CampaignTable')->getCampaignId(array('campaign_code' => $campCode));
            if (!empty($campaignDetail)) {
                $program_id = $campaignDetail['program_id'];
                $donationForm->get('program_id')->setAttributes(array('value' => $program_id));
//$campaign_list[$campaignDetail['campaign_id']] = $campaignDetail['campaign_title'];
                $campaign_id = $campaignDetail['campaign_id'];

                $getSearchArray['program_id'] = $program_id;
                $campaignListArray = $this->getTransactionTable()->getCampaignByProgram($getSearchArray);
                if (!empty($campaignListArray)) {
                    foreach ($campaignListArray as $key => $val) {
                        $campaign_list[$val['campaign_id']] = $val['campaign_title'];
                    }
                }
            }
        } else {
            $donationForm->get('program_id')->setAttributes(array('value' => 0001));
        }
        /* $donationForm->get('campaign_id_front')->setAttributes(array('options' => $campaign_list));
          $donationForm->get('campaign_id_front')->setAttributes(array('value' => $campaign_id)); */
        //$donationForm->get('amount')->setAttributes(array('value' => $amount));
        $postData[0] = '';
        $postData[1] = '';
        $postData[3] = 'asc';
        $postData[2] = 'tbl_membership.minimun_donation_amount';
        $membershipData = $this->getServiceLocator()->get('User\Model\MembershipTable')->getAllMembership($postData);
        $i = 0;
        foreach ($membershipData as $key) {
            if ($key['membership_id'] != '1' && $key['minimun_donation_amount'] > 0) {
                if ($i == 0) {
                    $min_donation = $key['minimun_donation_amount'];
                } else {
                    if ($min_donation > $key['minimun_donation_amount']) {
                        $min_donation = $key['minimun_donation_amount'];
                    }
                }
                $i++;
            }
        }
        $viewModel->setVariables(array(
            'jsLangTranslate' => array_merge($createTransactionMessage, $this->_config['transaction_messages']['config']['common_message'], $this->_config['transaction_messages']['config']['create_crm_transaction'], $this->_config['transaction_messages']['config']['donation_page']),
            'donationAddToCartForm' => $donationForm,
            'minDonation' => $min_donation,
			'donateAmount'=>$amount 
        ));
        return $viewModel;
    }

    /**
     * This Action is used to get donation add to cart detail
     * @param Void
     * @return String
     * @author Icreon Tech - DT dev 2
     */
    public function donateCheckoutAction() {

        //$this->checkUserAuthenticationAjx();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getTransactionTable();
        $viewModel = new ViewModel();
        $donateFormParam = array();
        $data = array();
        $data[0] = '';
        $data[1] = '';
        $data[3] = 'asc';
        $data[2] = 'tbl_membership.minimun_donation_amount';
        $membershipData = $this->getServiceLocator()->get('User\Model\MembershipTable')->getAllMembership($data);
        $membership = array('' => 'Select');
        foreach ($membershipData as $key => $value) {
            if ($value['membership_id'] != '1' && $value['minimun_donation_amount'] > 0) {
                $membership[$value['minimun_donation_amount']] = $value['membership_title'] . ' - $' . $value['minimun_donation_amount'];
            }
        }
        $membership['other'] = 'Other donation amount';
        $donationCheckoutForm = new DonationCheckoutForm();
        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
        $country_list = array();
        $country_list[''] = 'Select Country';
        foreach ($country as $key => $val) {
            $country_list[$val['country_id']] = $val['name'];
        }
        $donationCheckoutForm->get('country')->setAttribute('options', $country_list);

        $state = $this->getServiceLocator()->get('Common\Model\CommonTable')->getStates();
        $stateList = array();
        $stateList[''] = 'Select State';
        foreach ($state as $key => $val) {
            $stateList[$val['state_code']] = $val['state'] . " - " . $val['state_code'];
        }

        \Facebook\FacebookSession::setDefaultApplication($this->_config['facebook_app']['app_id'],$this->_config['facebook_app']['secret']);
        $helper = new \Facebook\FacebookRedirectLoginHelper(SITE_URL . "/login/profile/facebook",$this->_config['facebook_app']['app_id'],$this->_config['facebook_app']['secret']);        
        $permissions = array(
          'email',
          'user_location',
          'user_birthday'
        );
        $fbLoginUrl = $helper->getLoginUrl($permissions);
        //error_reporting(E_ALL);
        if($this->_config['https_on'] == 1)
            $refererUrl = SITE_URL_HTTPS . $_SERVER['REDIRECT_URL'];
        else
            $refererUrl = SITE_URL . $_SERVER['REDIRECT_URL'];
        $this->referPathSession = new SessionContainer('referPathSession');
        $this->referPathSession->path = $refererUrl;
        $this->referPathSession->path;
                
                
        $donationCheckoutForm->get('state')->setAttribute('options', $stateList);
        $monthArray = $this->creditMonthArray();
        $yearArray = range(date('Y'), date('Y') + 10);
        $newYearArray = array_combine($yearArray, $yearArray);
        $donationCheckoutForm->get('credit_card_exp_month')->setAttribute('options', $monthArray);
        $donationCheckoutForm->get('credit_card_exp_year')->setAttribute('options', $newYearArray);

        $user_id = '';
        if (isset($this->auth->getIdentity()->user_id) && !empty($this->auth->getIdentity()->user_id)) {
            $user_id = $this->auth->getIdentity()->user_id;
        }

        // error_reporting(E_ALL);
        $this->donationSession = new Container('donation');
        if ($request->isPost() && !$request->isXmlHttpRequest()) {
            $donateFormParam = $request->getPost();
            $this->donationSession->donateParam = serialize($donateFormParam); // session container to keep the posted form values
            if (isset($donateFormParam['amount']) && $donateFormParam['amount'] == 'other')
                $amount = $donateFormParam['other_amount'];
            else
                $amount = $donateFormParam['amount'];
            $inHonoree = $donateFormParam['in_honoree'];
            $honorMemoryName = $donateFormParam['honor_memory_name'];
            $companyName = $donateFormParam['company'];
            $companyId = $donateFormParam['company_id'];

            //asd(unserialize($this->donationSession->donatParam));
        }

        if ($request->isGet()) {
            if (!empty($this->donationSession->donateParam)) {
                $donateFormParam = unserialize($this->donationSession->donateParam)->toArray();
                if (isset($donateFormParam['amount']) && $donateFormParam['amount'] == 'other')
                    $amount = $donateFormParam['other_amount'];
                else
                    $amount = $donateFormParam['amount'];
            }
            $inHonoree = $donateFormParam['in_honoree'];
            $honorMemoryName = $donateFormParam['honor_memory_name'];
            $companyName = $donateFormParam['company'];
            $companyId = $donateFormParam['company_id'];

            if (empty($amount)) {
                return $this->redirect()->toRoute('get-donation-add-to-cart-front');
            }
        }

        $addressInformationId = 0;
        $location = '';
        $user_id = '';
        if (isset($this->auth->getIdentity()->user_id) && !empty($this->auth->getIdentity()->user_id)) {
            $user_id = $this->auth->getIdentity()->user_id;
            $userAddressDetails = $this->getBillingShippingAddressInfoFront($this->auth->getIdentity()->user_id);

            if(count($userAddressDetails) == 0)
            {
                $donationCheckoutForm->get('first_name')->setAttribute('value', $this->auth->getIdentity()->first_name);
                $donationCheckoutForm->get('last_name')->setAttribute('value', $this->auth->getIdentity()->last_name);                
            }
            else {
                $donationCheckoutForm->get('first_name')->setAttribute('value', $userAddressDetails['first_name']);
                $donationCheckoutForm->get('last_name')->setAttribute('value', $userAddressDetails['last_name']);
            }
            
            
            $donationCheckoutForm->get('email_id')->setAttribute('value', $userAddressDetails['email_id']);
            $donationCheckoutForm->get('confirm_email_id')->setAttribute('value', $userAddressDetails['email_id']);
            $donationCheckoutForm->get('country')->setAttribute('value', $userAddressDetails['country_id']);
            $donationCheckoutForm->get('address_1')->setAttribute('value', $userAddressDetails['street_address_one']);
            $donationCheckoutForm->get('address_2')->setAttribute('value', $userAddressDetails['street_address_two']);
            $donationCheckoutForm->get('zipcode')->setAttribute('value', $userAddressDetails['zip_code']);
            $donationCheckoutForm->get('city')->setAttribute('value', $userAddressDetails['city']);
            $donationCheckoutForm->get('state')->setAttribute('value', $userAddressDetails['state']);
            if ($userAddressDetails['country_id'] != '228') {
                $donationCheckoutForm->get('state_id')->setAttribute('value', $userAddressDetails['state']);
            }

            $addressInformationId = $userAddressDetails['address_information_id'];
            $location = $userAddressDetails['location'];
        }

        $curDate = DATE_TIME_FORMAT;
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            //error_reporting(E_ALL);
            $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter')->getDriver()->getConnection()->beginTransaction();
            $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->offAutoCommit();
            $postParam = $request->getPost()->toArray();
            if ($postParam['country'] != 228)
                $postParam['state'] = $postParam['state_id'];
            $amount = $postParam['amount'];
            $inHonoree = $postParam['inHonoree'];
            $honorMemoryName = $postParam['honorMemoryName'];
            // asd($postParam);
            $billingAddressParam = array();

            $groupId = 0;
            if (isset($this->auth->getIdentity()->user_id) && !empty($this->auth->getIdentity()->user_id)) { // update the billing address
                $this->donationSession->isAlreadyMember = "Yes";
                $user_id = $this->auth->getIdentity()->user_id; // if already logged in case

                $userGroupSearchParam = array();
                $userGroupSearchParam['user_id'] = $this->auth->getIdentity()->user_id;
                $promoResult = $this->getServiceLocator()->get('Promotions\Model\PromotionTable')->getUserInGroup($userGroupSearchParam);
                if (count($promoResult) > 0)
                    $groupId = $promoResult[0]['group_id'];

                $billingAddressParam = array();
                $billingAddressParam['first_name'] = $postParam['first_name'];
                $billingAddressParam['last_name'] = $postParam['last_name'];
                $billingAddressParam['address_1'] = $postParam['address_1'];
                $billingAddressParam['address_2'] = $postParam['address_2'];
                $billingAddressParam['zipcode'] = $postParam['zipcode'];
                $billingAddressParam['city'] = $postParam['city'];
                $billingAddressParam['country'] = $postParam['country'];
                $billingAddressParam['state'] = $postParam['state'];
                $billingAddressParam['location'] = $postParam['billing_addressinfo_location'];
                $billingAddressParam['user_id'] = $this->auth->getIdentity()->user_id;
                $billingAddressParam['address_information_id'] = $postParam['billing_address_information_id'];
                $billingAddressParam['added_date'] = DATE_TIME_FORMAT;
                $billingAddressParam['modified_date'] = DATE_TIME_FORMAT;
                $billingAddressInformationId = $postParam['billing_address_information_id'];
                if (!empty($postParam['billing_address_information_id'])) {
                    $resultFlag = $this->_donationTable->updateBillingAddress($billingAddressParam);
                } else {
                    $addressForm = array();
                    $addressForm['first_name'] = $postParam['first_name'];
                    $addressForm['last_name'] = $postParam['last_name'];
                    $addressForm['addressinfo_location_type'] = '4'; // Work
                    $addressForm['addressinfo_street_address_1'] = $postParam['address_1'];
                    $addressForm['addressinfo_city'] = $postParam['city'];
                    $addressForm['addressinfo_zip'] = $postParam['zipcode'];
                    $addressForm['addressinfo_street_address_2'] = $postParam['address_2'];
                    $addressForm['addressinfo_state'] = $postParam['state'];
                    $addressForm['addressinfo_country'] = $postParam['country'];
                    $addressForm['addressinfo_location'] = '1,2'; // primary and billing
                    $addressForm['address_type'] = '1'; // Residential
                    $addressForm['add_date'] = $curDate;
                    $addressForm['modified_date'] = $curDate;
                    $addressForm['user_id'] = $user_id;
                    $this->getServiceLocator()->get('User\Model\ContactTable')->insertAddressInfo($addressForm); // insert address information

                    $addParam = array();
                    $addParam['user_id'] = $user_id;
                    $addressResult = $this->getServiceLocator()->get('Transaction\Model\DonationTable')->getUserBillingAddress($addParam);
                    if (count($addressResult) > 0) {
                        $billingAddressInformationId = $addressResult[0]['address_information_id'];
                    }
                }
                
                            if ($postParam['subscribe_newsletter'] == 1) {
                                $prefeParam['user_id'] = $user_id;
                                $prefeParam['privacy'] = '1,3,4,5,6';
                                $this->_donationTable->updateDonateUserCommunicationPreferences($prefeParam);
                            } else {
                                $prefeParam['user_id'] = $user_id;
                                $prefeParam['privacy'] = '1,2,3,4,5';
                                $this->_donationTable->updateDonateUserCommunicationPreferences($prefeParam);
                            } 
                
                
            } else { // insert new user with billing address
                if (isset($postParam['email_id']) && $postParam['email_id'] != '') {
                    // check if user already exists
                    $isEmailExist = $this->getServiceLocator()->get('User\Model\UserTable')->isEmailExits(array('email_id' => $postParam['email_id']));
                    $addressForm = array();
                    $addressForm['first_name'] = $postParam['first_name'];
                    $addressForm['last_name'] = $postParam['last_name'];
                    $addressForm['addressinfo_location_type'] = '4'; // Work
                    $addressForm['addressinfo_street_address_1'] = $postParam['address_1'];
                    $addressForm['addressinfo_city'] = $postParam['city'];
                    $addressForm['addressinfo_zip'] = $postParam['zipcode'];
                    $addressForm['addressinfo_street_address_2'] = $postParam['address_2'];
                    $addressForm['addressinfo_state'] = $postParam['state'];
                    $addressForm['addressinfo_country'] = $postParam['country'];
                    $addressForm['address_type'] = '1'; // Residential
                    $addressForm['add_date'] = $curDate;
                    $addressForm['modified_date'] = $curDate;
                    


                    if ($isEmailExist) { // if user already exists
                        $userParam = array();
                        $userParam['email_id'] = trim($postParam['email_id']);
                        $userResult = $this->getServiceLocator()->get('User\Model\UserTable')->getUser($userParam);

                        if (!empty($userResult) && count($userResult) > 0) {
                            $user_id = $userResult['user_id'];
                        }
                        
                        // check for address information (primary/billing/shpping) if exists
                            $searchArray = array();

                            $searchArray['user_id'] = $user_id;
                            $searchArray['address_location'] = '1';                        
                       $addressResult = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations($searchArray);
                       if(!empty($addressResult) && is_array($addressResult)) {
                            if(count($addressResult)> 0) 
                                    $addressForm['addressinfo_location'] = '2'; // billing 
                                else
                                    $addressForm['addressinfo_location'] = '1,2'; // primary and billing 
                       }
                        if (empty($postParam['billing_address_information_id'])) { // insert new billing address and make it primary
                            $addressForm['user_id'] = $user_id;
                            $this->getServiceLocator()->get('User\Model\ContactTable')->insertAddressInfo($addressForm); // insert address information
                        }
                        
                            if ($postParam['subscribe_newsletter'] == 1) {
                                $prefeParam['user_id'] = $user_id;
                                $prefeParam['privacy'] = '1,3,4,5,6';
                                $this->_donationTable->updateDonateUserCommunicationPreferences($prefeParam);
                            } else {
                                $prefeParam['user_id'] = $user_id;
                                $prefeParam['privacy'] = '1,2,3,4,5';
                                $this->_donationTable->updateDonateUserCommunicationPreferences($prefeParam);
                            }                        
                        
                        
                        
                        $this->donationSession->isAlreadyMember = "Yes";
                    } else { // if user not exists
                        $userParam['first_name'] = (isset($postParam['first_name'])) ? $postParam['first_name'] : null;
                        $userParam['last_name'] = (isset($postParam['last_name'])) ? $postParam['last_name'] : null;
                        $userParam['email_id'] = (isset($postParam['email_id'])) ? $postParam['email_id'] : null;
                        
                        
                        $userParam['is_active'] = 1;
                        $userParam['is_activated'] = 1;
                        $userParam['is_login_enabled'] = 1;
                        $userParam['reason_id'] = '';
                        $userParam['email_verified'] = 1;
                        
                        
                        $user_id = $this->_donationTable->saveDonationUser($userParam);
                        if ($user_id) {
                            $this->getServiceLocator()->get('User\Model\ContactTable')->updateContactId(array('user_id' => $user_id));

                            $addressForm['user_id'] = $user_id;
                            $addressForm['addressinfo_location'] = '1,2'; // primary and billing, shipping ???????????????????
                            $this->getServiceLocator()->get('User\Model\ContactTable')->insertAddressInfo($addressForm); // insert address information

                            if ($postParam['subscribe_newsletter'] == 1) {
                                $prefeParam['user_id'] = $user_id;
                                $prefeParam['privacy'] = '1,3,4,5,6';
                                $this->_donationTable->insertDefaultDonateUserCommunicationPreferences($prefeParam);
                            } else {
                                $prefeParam['user_id'] = $user_id;
                                $prefeParam['privacy'] = '1,2,3,4,5';
                                $this->_donationTable->insertDefaultDonateUserCommunicationPreferences($prefeParam);
                            }
                            $membershipDetailArr = array();
                            $membershipDetailArr[] = 1;
                            $getMemberShip = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
                            $userMembershipData = array();
                            $userMembershipData['user_id'] = $user_id;
                            $userMembershipData['membership_id'] = $getMemberShip['membership_id'];
							$userMembershipInfo = $this->getServiceLocator()->get('UserMembership')->getUserMembership($getMemberShip);
							$userMembershipData['membership_date_from'] = $userMembershipInfo['membership_date_from'];
							$userMembershipData['membership_date_to'] = $userMembershipInfo['membership_date_to'];
                            /*$startDate = date("Y-m-d");
                            if ($getMemberShip['validity_type'] == 1) {
                                $userMembershipData['membership_date_from'] = $startDate;
                                $startDay = $this->_config['financial_year']['srart_day'];
                                $startMonth = $this->_config['financial_year']['srart_month'];
                                $startYear = date("Y");
                                $startDate = $startYear . "-" . $startMonth . "-" . $startDay;
                                $addYear = $getMemberShip['validity_time'] - date("Y");
                                $futureDate = date('Y-m-d', strtotime('+' . $addYear . ' year', strtotime($startDate)));
                                $userMembershipData['membership_date_to'] = $futureDate;
                            } else if ($getMemberShip['validity_type'] == 2) {
                                $userMembershipData['membership_date_from'] = $startDate;
                                $futureDate = date('Y-m-d', strtotime('+' . $getMemberShip['validity_time'] . ' year', strtotime($startDate)));
                                $userMembershipData['membership_date_to'] = $futureDate;
                            }*/
                            $this->getServiceLocator()->get('User\Model\UserTable')->saveUserMembership($userMembershipData);
                        }
                    }
                }
                // get the billing address 
                $addParam = array();
                $addParam['user_id'] = $user_id;
                $addressResult = $this->getServiceLocator()->get('Transaction\Model\DonationTable')->getUserBillingAddress($addParam);
                if (count($addressResult) > 0) {
                    $billingAddressInformationId = $addressResult[0]['address_information_id'];
                }
            }

            // code for payment process
            // First need to save the credit cart information
            $creditCardData = array();
            $creditCardResult = $this->getServiceLocator()->get('User\Model\UserTable')->getCreditCards();
            foreach ($creditCardResult as $key => $val) {
                $creditCardResultArray[$val['credit_card_type_id']] = strtolower($val['credit_card']);
            }
            $creditCardType = array_search(strtolower($postParam['credit_card_type']), $creditCardResultArray);
            $creditCardData['credit_card_type_id'] = $creditCardType;
            $creditCardData['user_id'] = $user_id;
            $creditCardData['credit_card_number'] = $postParam['credit_card_number'];
            $creditCardData['credit_card_exp_year'] = $postParam['credit_card_exp_year'];
            $creditCardData['credit_card_exp_month'] = $postParam['credit_card_exp_month'];
            $creditCardData['credit_card_code'] = $postParam['credit_card_code'];
            $creditCardData['billing_address_id'] = $billingAddressInformationId;
            $creditCardData['credit_card_name'] = trim($postParam['first_name'] . ' ' . $postParam['last_name']);
            $profileSuccessStatus = $this->saveCreditCardInformation($creditCardData);
            
            $this->campaignSession = new Container('campaign');
            
            if ($profileSuccessStatus == true) { //echo "Profile Created";
                $transaction = new Transaction($this->_adapter);
                $userTransactionParam = array();
                $userTransactionParam['user_id'] = $user_id;
                $userTransactionParam['transaction_source_id'] = 1; // web
                $userTransactionParam['num_items'] = 1;
                $userTransactionParam['sub_total_amount'] = $this->Currencyformat($postParam['amount']);
                $userTransactionParam['transaction_amount'] = $this->Currencyformat($postParam['amount']);
                $userTransactionParam['don_amount'] = $this->Currencyformat($postParam['amount']);
                $userTransactionParam['transaction_type'] = 1; // donation
                $userTransactionParam['transaction_date'] = $curDate;
                $userTransactionParam['transaction_status_id'] = 10;
                $userTransactionParam['added_by'] = 0;
                $userTransactionParam['added_date'] = $curDate;
                $userTransactionParam['modify_by'] = 0;
                $userTransactionParam['modify_date'] = $curDate;
                $userTransactionParam['group_id'] = $groupId;
                $userTransactionParam['total_product_revenue'] = $this->Currencyformat($postParam['amount']);
                
                $generalCampaign = $this->_transactionTable->getGeneralCampaign();
                if (isset($generalCampaign) && !empty($generalCampaign)) {
                    $finalTransaction['campaign_code'] = $generalCampaign[0]['campaign_code'];
                    $finalTransaction['campaign_id'] = $generalCampaign[0]['campaign_id'];
                    $finalTransaction['channel_id'] = 5;
                }                
                $userTransactionParam['campaign_code'] = (isset($this->campaignSession->campaign_code) && !empty($this->campaignSession->campaign_code) ? $this->campaignSession->campaign_code : $finalTransaction['campaign_code']);
                $userTransactionParam['campaign_id'] = (isset($this->campaignSession->campaign_id) && !empty($this->campaignSession->campaign_id) ? $this->campaignSession->campaign_id : $finalTransaction['campaign_id']);
                $userTransactionParam['channel_id'] = (isset($this->campaignSession->channel_id) && !empty($this->campaignSession->channel_id) ? $this->campaignSession->channel_id : $finalTransaction['channel_id']);                

				
                $userTransactionParam = $transaction->getFinalTransactionArr($userTransactionParam);
                $transactionId = $this->getTransactionTable()->saveFinalTransaction($userTransactionParam);
            
                $this->getTransactionTable()->updateRevenueGoal($userTransactionParam);
	            $this->getTransactionTable()->updateChannelRevenue($userTransactionParam);
				$this->getTransactionTable()->updateTransctionCustomerIP(array($transactionId,$_SERVER['REMOTE_ADDR']));
                /* Add Donation Product */
                // check for company exists or Not
                $userCompanyId = 0;
                $userCompanyName = '';
                if (!empty($postParam['companyName'])) {
                    $companyParam = array();
                    $companyParam['company_name'] = $postParam['companyName'];
                    $companyResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getMasterCompany($companyParam);
                    if (isset($companyResult) && !empty($companyResult)) {
                        $userCompanyId = $companyResult[0]['company_id'];
                        $userCompanyName = $companyResult[0]['company_name'];
                    } else { // insert new company
                        $common = new Common($this->_adapter);
                        $companyParam['added_date'] = $curDate;
                        $companyParam['modified_date'] = $curDate;
                        $company_array = $common->getCompanyArr($companyParam);
                        $userCompanyId = $this->getServiceLocator()->get('Common\Model\CommonTable')->createMasterCompany($company_array);
                        $userCompanyName = $companyParam['company_name'];
                    }
                }
                // End for company exists or not    
                
                $donationProductData = array();
                $donationProductData['transaction_id'] = $transactionId;
                $donationProductData['honoree_type'] = $postParam['inHonoree'];
                $donationProductData['honoree_name'] = $postParam['honorMemoryName'];
                $donationProductData['pledge_transaction_id'] = '';
                $donationProductData['is_pledge_transaction'] = '0';
                $donationProductData['product_mapping_id'] = 1;
                $donationProductData['product_type_id'] = 2;
                $donationProductData['product_price'] = $this->Currencyformat($postParam['amount']);
                $donationProductData['product_sub_total'] = $this->Currencyformat($postParam['amount']);
                $donationProductData['product_discount'] = 0;
                $donationProductData['product_tax'] = 0;
                $donationProductData['product_total'] = $this->Currencyformat($postParam['amount']);
                $donationProductData['product_status_id'] = 10;
                $donationProductData['purchase_date'] = $curDate;
                $donationProductData['is_deleted'] = 0;
                $donationProductData['added_by'] = 0;
                $donationProductData['added_date'] = $curDate;
                $donationProductData['modify_by'] = 0;
                $donationProductData['modified_date'] = $curDate;
                $donationProductData['user_id'] = $user_id;
                //$donationProductData['product_name'] = 'Donation';
                $donationProductData['product_id'] = '8';
                $donationProductData['company_name'] = $userCompanyName;
                $donationProductData['company_id'] = $userCompanyId;
                
                $productSearchParam['id'] = 8;/** product */
                $productResultArr = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProductInfo($productSearchParam);   
                $donationProductData['product_sku'] = $productResultArr[0]['product_sku'];
                $donationProductData['product_name'] = $productResultArr[0]['product_name'];
                $donationProductData['num_quantity'] = 1;
                //asd($donationProductData);
                
                $donationProductArray = $transaction->getDonationProductArray($donationProductData);
                $donationProductId = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->saveTransactionDonationProdcts($donationProductArray);
                /* End add Donation Product */

                $billingShippingTransaction['transaction_id'] = $transactionId;
                $billingShippingTransaction['billing_first_name'] = $postParam['first_name'];
                $billingShippingTransaction['billing_last_name'] = $postParam['last_name'];
                $billingShippingTransaction['billing_company'] = '';
                $billingShippingTransaction['billing_address'] = $postParam['address_1'] . ' ' . $postParam['address_2'];
                $billingShippingTransaction['billing_city'] = $postParam['city'];
                $billingShippingTransaction['billing_state'] = $postParam['state'];
                $billingShippingTransaction['billing_zip_code'] = $postParam['zipcode'];
                $billingShippingTransaction['billing_country'] = $postParam['country'];
                $billingShippingTransaction['billing_phone'] = '';
                /*$billingShippingTransaction['shipping_first_name'] = $postParam['first_name'];
                $billingShippingTransaction['shipping_last_name'] = $postParam['last_name'];
                $billingShippingTransaction['shipping_company'] = '';
                $billingShippingTransaction['shipping_address'] = $postParam['address_1'] . ' ' . $postParam['address_2'];
                $billingShippingTransaction['shipping_city'] = $postParam['city'];
                $billingShippingTransaction['shipping_state'] = $postParam['state'];
                $billingShippingTransaction['shipping_zip_code'] = $postParam['zipcode'];
                $billingShippingTransaction['shipping_country'] = $postParam['country'];
                $billingShippingTransaction['shipping_phone'] = '';*/
                $billingShippingTransaction['shipping_first_name'] = '';
                $billingShippingTransaction['shipping_last_name'] = '';
                $billingShippingTransaction['shipping_company'] = '';
                $billingShippingTransaction['shipping_address'] = '';
                $billingShippingTransaction['shipping_city'] ='';
                $billingShippingTransaction['shipping_state'] = '';
                $billingShippingTransaction['shipping_zip_code'] = '';
                $billingShippingTransaction['shipping_country'] = '';
                $billingShippingTransaction['shipping_phone'] = '';
                $billingShippingTransaction['added_by'] = 0;
                $billingShippingTransaction['added_date'] = $curDate;
                $billingShippingTransaction['modify_by'] = 0;
                $billingShippingTransaction['modify_date'] = $curDate;

                $transactionBillingShippingArr = $transaction->getTransactionBillingShipping($billingShippingTransaction);
                $transactionBillingShippingId = $this->_transactionTable->saveTransactionBillingShippingDetail($transactionBillingShippingArr);

                $userProfileParam = array();
                $userProfileParam['user_id'] = $user_id;
                $profileArray = $this->getServiceLocator()->get('Transaction\Model\DonationTable')->getUserProfileId($userProfileParam);

                if (count($profileArray) > 0) { //echo "Payment Done";
                    $paymentFrontArray = array();
                    $paymentFrontArray['profile_id'] = $profileArray[0]['profile_id'];
                    $paymentFrontArray['transaction_amounts'] = $this->Currencyformat($postParam['amount']);
                    $paymentFrontArray['transaction_id'] = $transactionId;

                    $paymentArray[0]['table_auto_id'] = $donationProductId;
                    $paymentArray[0]['table_type'] = 2;
                    $paymentArray[0]['cart_product_amount'] = $this->Currencyformat($postParam['amount']);
                    $paymentArray[0]['cart_product_type_id'] = 2;
                    $paymentArray[0]['transaction_id'] = $transactionId;
                    $paymentSuccessStatus = $this->capturePaymentFront($paymentArray, $paymentFrontArray); // by dev 3            

                    $userPurchasedProductArr = array();
                    $userPurchasedProductArr['user_id'] = $user_id;
                    $date_range = $this->getDateRange(4);
                    $userPurchasedProductArr['from_date'] = $this->DateFormat($date_range['from'], 'db_date_format_from');
                    $userPurchasedProductArr['to_date'] = $this->DateFormat($date_range['to'], 'db_date_format_to');
                    $getTotalUserPurchaseProductAmountArr = $transaction->getTotalUserPurchaseProductAmountArr($userPurchasedProductArr);
                    $totalAmountPurchased = $this->_transactionTable->getTotalUserPurchased($getTotalUserPurchaseProductAmountArr);                    
                    
                    $userMembershipArr = array();
                    $userMembershipArr['transaction_amount'] = $totalAmountPurchased;
                    $userMembershipArr['donation_amount'] = $postParam['amount'];
                    $userMembershipArr['user_id'] = $user_id;
                    $userMembershipArr['transaction_id'] = $transactionId;
                    $userMembershipArr['transaction_date'] = $curDate;

                    $this->updateUserMembership($userMembershipArr);

                    if (isset($postParam['amount']) && !empty($postParam['amount'])) {
                        $userPledgeArr = array();
                        $userPledgeArr['userId'] = $user_id;
                        $userPledgeArr['transaction_id'] = $transactionId;
                        $userPledgeArr['donation_amount'] = $postParam['amount'];
                        $this->updateUserPledgeTransaction($userPledgeArr);
                    }                    
                    
                    
                    if (isset($paymentSuccessStatus['status']) && $paymentSuccessStatus['status'] == 'payment_error') {
                        $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter')->getDriver()->getConnection()->rollback();
                        $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->onAutoCommit();
                        $response->setContent(\Zend\Json\Json::encode($paymentSuccessStatus));
                        return $response;
                        exit;
                    }

                    if ($paymentSuccessStatus == true) {
                        $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter')->getDriver()->getConnection()->commit();
                        $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->onAutoCommit();

                        // $this->donationSession = new Container('donation');
                        if (isset($this->auth->getIdentity()->user_id) && !empty($this->auth->getIdentity()->user_id))
                            $this->donationSession->email_id = $this->auth->getIdentity()->email_id;
                        else
                            $this->donationSession->email_id = $postParam['email_id'];

                            $this->donationSession->first_name = $postParam['first_name'];
                            $this->donationSession->last_name = $postParam['last_name'];
                            $this->donationSession->amount = $postParam['amount'];
                            $this->donationSession->donation_product_id = $donationProductId;                        
                            $this->donationSession->user_id = $user_id;                        
                        // ***************** Start mail section block ******************
                        
                        $common = new Common($this->_adapter);
                        $countrySearchParam = array();
                        $countrySearchParam['country_id'] = $postParam['country'];
                        $countryArr = $common->getCountryArr($countrySearchParam);
                        $countryResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCountryUser($countryArr);                        

                        $searchParam['transactionId'] = $transactionId;
                        $searchProduct = $this->_transactionTable->searchTransactionProducts($searchParam);
                        $searchResult = $this->_transactionTable->searchTransactionDetails($searchParam);

                        $orderDetailsArr = '<div><table width="100%" border="0" style="border-bottom:1px dotted #ccc;" cellspacing="5" cellpadding="10" class="table-bh tra-view">
                                    <tr>
                                    <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:left; padding-left:0;">Sr. No</th>
                                    <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:left">Product</th>
                                    <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:left">SKU</th>
                                    <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:left">Qty.</th>
                                    <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:center">Price</th>
                                    <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:center">SubTotal</th></tr>';
                        $i = 1;
                        foreach ($searchProduct as $key => $val) {
                            $orderDetailsArr.= '<tr><td style="width: 48px; font: 16px/20px arial; color: #7e8183; text-align:left; vertical-align:top;">' . $i++ . '</td>
                                        <td style="font: 16px/20px arial; color: #7e8183; text-align:left">';
                            switch ($val['tra_pro']) {
                                case 'don' :
                                    $orderDetailsArr.=$val['product_name'] . ' <label>' . $val['option_name'] . '</label>';
                                    break;
                            }
                            $orderDetailsArr .='</td><td style="font: 16px/20px arial; color: #7e8183; text-align:left">';
                            if (isset($val['product_sku']) && !empty($val['product_sku'])) {
                                $orderDetailsArr .=$val['product_sku'];
                            } else {
                                $orderDetailsArr .='N/A';
                            }


                            $orderDetailsArr.='</td><td style="font: 16px/20px arial; color: #7e8183; text-align:left">' . $val['product_quantity'] . '</td>
                                        <td style="font: 16px/20px arial; color: #7e8183; text-align:center">$' . $this->Currencyformat($val['product_price']) . '</td>
                                        <td style="font: 16px/20px arial; color: #7e8183; text-align:right">$' . $this->Currencyformat($val['product_sub_total']) . '</td></tr>';

                        }

                        /* $orderDetailsArr.='</td><td style="font: 16px/20px arial; color: #7e8183; text-align:left">' . $val['product_quantity'] . '</td>
                          <td style="font: 16px/20px arial; color: #7e8183; text-align:center">$' . $this->Currencyformat($val['product_price']) . '</td>
                          <td style="font: 16px/20px arial; color: #7e8183; text-align:right">$' . $this->Currencyformat($val['product_sub_total']) . '</td></tr>'; */
                        $orderDetailsArr .= '<tr>
                                    <td align="right" colspan="5"><span style="text-align:right; font: 16px/28px arial; color: #7e8183;">Sub Total :</span></td>
                                    <td align="right">
                                    <strong style="text-align:right; font: 16px/28px arial; color: #7e8183; font-weight:bold;">$' . $this->Currencyformat($searchResult[0]['sub_total_amount']) . '
                                    </strong></td>
                                    </tr>
                                    <tr>
                                    <td align="right" colspan="5"><span style="text-align:right; font: 16px/28px arial; color: #7e8183;">Shipping & Handling :</span></td>
                                    <td align="right">
                                    <strong style="text-align:right; font: 16px/28px arial; color: #7e8183; font-weight:bold;">$' . $this->Currencyformat($searchResult[0]['shipping_amount'] + $searchResult[0]['handling_amount']) . '
                                    </strong></td>
                                    </tr>
                                    <tr>
                                    <td align="right" colspan="5"><span style="text-align:right; font: 16px/28px arial; color: #7e8183;">Tax :</span></td>
                                    <td align="right">
                                    <strong style="text-align:right; font: 16px/28px arial; color: #7e8183; font-weight:bold;">$' . $this->Currencyformat($searchResult[0]['total_tax']) . '
                                    </strong></td>
                                    </tr>
                                    <tr>
                                    <td align="right" colspan="5"><span style="text-align:right; font: 16px/28px arial; color: #7e8183;">Discount :</span></td>
                                    <td align="right">
                                    <strong style="text-align:right; font: 16px/28px arial; color: #7e8183; font-weight:bold;">($' . $this->Currencyformat($searchResult[0]['total_discount']) . ')
                                    </strong></td>
                                    </tr>
                                    <tr>
                                    <td align="right" colspan="5"><span style="text-align:right; font: 16px/28px arial; color: #7e8183;">Purchase Total :</span></td>
                                    <td align="right">
                                    <strong style="text-align:right; font: 16px/28px arial; color: #7e8183; font-weight:bold;">$' . $this->Currencyformat($searchResult[0]['transaction_amount'] - ($searchResult[0]['cancel_amount'] + $searchResult[0]['refund_amount'])) . '
                                    </strong></td>
                                    </tr></table>';
                        $userDataArr['order_summary'] = $orderDetailsArr;

                        /*$address = $postParam['address_1'];
                        $address.= (!empty($postParam['address_2'])) ? '<br>' . $postParam['address_2'] : "";
                        $shippingStreetAddress = $postParam['first_name'] . ' ' . $postParam['last_name'] . '<br>' . $address;
                        $shippingAddressDetail = '<td style="50%">  <p style="margin: 0px; padding-left: 0pt; padding-right: 0pt; font: 20px/36px arial; color: #000;  padding-top: 20px; ">Shipping Address :<br />
                        <strong style="color: #7E8183; font-weight:normal; font: 16px/30px arial;">' . $shippingStreetAddress . '</strong><br />
                        <strong style="color: #7E8183; font-weight:normal; font: 16px/30px arial;">' . $postParam['city'] . ',&nbsp;' . $postParam['state'] . '</strong><br />
                        <strong style="color: #7E8183; font-weight:normal; font: 16px/30px arial;">' . $countryResult[0]['name'] . '&nbsp;-&nbsp;' . $postParam['zipcode'] . '</strong><br />
                        <strong style="color: #7E8183; font-weight:normal; font: 16px/30px arial;"></strong></p></td>';  
                         * */
                                             
                        
                        if (isset($this->auth->getIdentity()->user_id) && !empty($this->auth->getIdentity()->user_id))
                            $userDataArr['email_id'] = $this->auth->getIdentity()->email_id;
                        else
                            $userDataArr['email_id'] = $postParam['email_id'];
                        $userDataArr['transactionId'] = $transactionId;
                        $userDataArr['transaction_date'] = $this->OutputDateFormat($curDate, 'dateformatampm');
                        $userDataArr['transaction_amount_total'] = $amount_total;
                        $userDataArr['first_name'] = $postParam['first_name'];
                        $userDataArr['last_name'] = $postParam['last_name'];
                        $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                        $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                        $userDataArr['shippingAddressDetail'] = '';
                        //$userDataArr['shippingAddressDetail'] = $shippingAddressDetail;
                        $userDataArr['billing_address_1'] = $postParam['address_1'];
                        $userDataArr['billing_address_2'] = $postParam['address_2'];
                        $userDataArr['billing_first_name'] = $postParam['first_name'];
                        $userDataArr['billing_last_name'] = $postParam['last_name'];
                        $userDataArr['billing_city'] = $postParam['city'];
                        $userDataArr['billing_state'] = $postParam['state'];
                        $userDataArr['billing_zip_code'] = $postParam['zipcode'];
                        $userDataArr['billing_country_text'] = $countryResult[0]['name'];
                        $userDataArr['transaction_amount_total'] = $this->Currencyformat($searchResult[0]['transaction_amount'] - ($searchResult[0]['cancel_amount'] + $searchResult[0]['refund_amount']));

                        $email_id_template_int = '10'; // dev 4
                        $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $email_id_template_int, $this->_config['email_options']);
                        
                        // Thank you mail dev 4
                            $userDataThankArr = array();
                            if (isset($this->auth->getIdentity()->user_id) && !empty($this->auth->getIdentity()->user_id))
                                $userDataThankArr['email_id'] = $this->auth->getIdentity()->email_id;
                            else
                                $userDataThankArr['email_id'] = $postParam['email_id'];        
                            $userDataThankArr['first_name'] = $postParam['first_name'];
                            $userDataThankArr['amount'] = $this->Currencyformat($postParam['amount']);
                            $userDataThankArr['admin_email'] = $this->_config['soleif_email']['email'];
                            $userDataThankArr['admin_name'] = $this->_config['soleif_email']['name'];        
                            $email_id_template_int = '8'; // Thank you mail 
                            $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataThankArr, $email_id_template_int, $this->_config['email_options']); 
                        // End Thank you mail
                        // 
                        // ***************** End mail section block ****************** 
                        $messages = array('status' => "success");
                        $response->setContent(\Zend\Json\Json::encode($messages));
                        // asd($messages);

                        return $response;
                        exit;
                    }
                }
            } else {
                $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter')->getDriver()->getConnection()->rollback();
                $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->onAutoCommit();
                $response->setContent(\Zend\Json\Json::encode($profileSuccessStatus));
                return $response;
                exit;
            }
            // End code for payment process
            // print_r($request->getPost());
        }

        $viewModel->setVariables(array(
            'jsLangTranslate' => array_merge($this->_config['transaction_messages']['config']['common_message'], $this->_config['transaction_messages']['config']['donation_page']),
            'donationCheckoutForm' => $donationCheckoutForm,
            'amount' => $amount,
            'inHonoree' => $inHonoree,
            'honorMemoryName' => $honorMemoryName,
            'companyName' => $companyName,
            'companyId' => $companyId,
            'addressInformationId' => $addressInformationId,
            'location' => $location,
            'userId' => $user_id,
            'fbLoginUrl'=>$fbLoginUrl,
        ));
        return $viewModel;
    }

    /**
     * This Action is used to get donation add to cart detail
     * @param Void
     * @return String
     * @author Icreon Tech - DT
     */
    public function donateConfirmationAction() {
        //$this->checkUserAuthenticationAjx();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getTransactionTable();
        $data = array();
        $data[0] = '';
        $data[1] = '';
        $data[3] = 'asc';
        $data[2] = 'tbl_membership.minimun_donation_amount';
        $this->donationSession = new Container('donation');
        
        $firstName = $this->donationSession->first_name;
        $lastName = $this->donationSession->last_name;
        $donationProductId = $this->donationSession->donation_product_id;
        //Donor Details
        $donorDetails['name'] = $firstName." ".$lastName;
        $donorDetails['email'] = $this->donationSession->email_id;
        $donorDetails['amount'] = $this->donationSession->amount;
        
        if (empty($this->donationSession->user_id)) {
                return $this->redirect()->toRoute('get-donation-add-to-cart-front');
        }        
        
        
        $sec_question = $this->getServiceLocator()->get('Common\Model\QuestionTable')->getSecurityQuestion();
        $sec_question_list = array();
        //$sec_question_list[''] = 'Select Security Question';
        foreach ($sec_question as $key => $val) {
            $sec_question_list[$val['security_question_id']] = $val['security_question'];
        }
       
        
        if ($request->isPost()) {
            $postParam = $request->getPost();
          
            if(isset($postParam['password']) && !empty($postParam['password'])){
               $userAccountData = array();
               $userAccountData['user_id'] = $this->donationSession->user_id;
               $userAccountData['password'] = $postParam['password'];
               $userAccountData['security_question_id'] = $postParam['security_question_id'];
               $userAccountData['security_answer'] = $postParam['security_answer'];
               $this->_donationTable->updateDonatePassword($userAccountData);
               $termsVersionId = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCurrentTermsVersion();
               $this->getServiceLocator()->get('Common\Model\CommonTable')->updateUserTermsVersion(array('terms_version_id'=>$termsVersionId,'terms_accept_date'=>DATE_TIME_FORMAT,'user_id'=>$this->donationSession->user_id));               
               
              
            }
            $firstName = $postParam['first_name']; 
            $lastName = $postParam['last_name']; 
            $email = $postParam['email']; 
            $regex = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$"; 
            $notifyParam = array();
            for($i = 0; $i < count($postParam['first_name']);$i++){
                $notifyParam['donation_product_id'] = $donationProductId;
                $notifyParam['name'] = $firstName[$i]." ".$firstName[$i];
                $notifyParam['email_id'] = $email[$i];
                //$notifyParam['is_deleted'] = 0;
                $notifyParam['added_by'] = isset($this->auth->getIdentity()->user_id) && !empty($this->auth->getIdentity()->user_id) ?$this->auth->getIdentity()->user_id:0;
                $notifyParam['added_date'] = DATE_TIME_FORMAT;
                $notifyParam['modified_by'] = '';
                $notifyParam['modified_date'] = '';
                $notifyParam['first_name'] = $firstName[$i];
                $notifyParam['last_name'] = $lastName[$i];
                $lastInsertId = $this->_donationTable->addDonationNotify($notifyParam);
                if($lastInsertId){
                    if(!empty($email[$i])) {
                        if(eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email[$i])) {  
                             $this->sendEmailToUsers($notifyParam,$donorDetails);
                        }
                    }
                }
                }
               $this->donationSession->getManager()->getStorage()->clear('donation'); 
               $this->flashMessenger()->addMessage("Thank you for your Donation Today! Your good deed has been shared with the email address provided.");
               return $this->redirect()->toRoute('get-shop-products');
               exit;
            }
          $isMemeber = $this->donationSession->isAlreadyMember;
          $emailId = $this->donationSession->email_id;
          
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'jsLangTranslate' => array_merge($this->_config['transaction_messages']['config']['common_message'], $this->_config['transaction_messages']['config']['donation_page']),
            'email_id' => $emailId,
            'first_name' =>$firstName,
            'last_name' =>$lastName,
            'donation_product_id'=>$donationProductId,
            'secQuestionList'=>$sec_question_list,
            'isMember'=> $isMemeber,
            'facebookAppId' => $this->_config['facebook_app']['app_id'],
        ));
        
        return $viewModel;
    }
    protected function sendEmailToUsers($donationNotifyArr,$donorDetails){
        
        $donationNotifyInfo['donarName'] = $donorDetails['name'];
        $donationNotifyInfo['first_name'] = $donationNotifyArr['first_name'];
        $donationNotifyInfo['last_name'] = $donationNotifyArr['last_name'];
        
        $donationNotifyInfo['name'] = $donationNotifyArr['name'];
        $donationNotifyInfo['email_id'] = $donationNotifyArr['email_id'];
        $donationNotifyInfo['amount'] = $donorDetails['amount'];
        $donationNotifyInfo['program_name'] =$donationNotifyArr['program'];
        $email_id_template_int = 15;
        $donationNotifyInfo['admin_email'] = $this->_config['soleif_email']['email'];
        $donationNotifyInfo['admin_name'] = $this->_config['soleif_email']['name'];
        // dev 4
           $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($donationNotifyInfo, $email_id_template_int, $this->_config['email_options']);

        return true;
    }

    /**
     * This function is used to to save the credit card from front side
     * @return void
     * @param void
     * @author Icreon Tech - SR
     */
    public function saveCreditCardInformation($postArr = array()) {

        $paymentInfo = array();
        $paymentInfo['card_number'] = $postArr['credit_card_number'];
        $paymentInfo['expiry_year'] = $postArr['credit_card_exp_year'];
        $paymentInfo['expiry_month'] = $postArr['credit_card_exp_month'];
        $paymentInfo['cvv_number'] = $postArr['credit_card_code'];

        $addressInfoId = $postArr['billing_address_id'];
        $userAddressInfo = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations(array('address_information_id' => $addressInfoId));

        if (empty($postArr['existing_profile_id'])) {
//$userId = $postArr['user_id'];
            $userId = $postArr['user_id'];
            $userArr = array('user_id' => $userId);
            $contactDetail = array();
            $contactDetail[0] = $this->getServiceLocator()->get('User\Model\UserTable')->getUser($userArr);
            $createCustomerProfile = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);

            $createCustomerProfile->createCustomerProfileRequest(array(
                'profile' => array(
                    'merchantCustomerId' => $contactDetail[0]['user_id'] . time(),
                    'email' => $contactDetail[0]['email_id'],
                    'paymentProfiles' => array(
                        'billTo' => array(
                            'firstName' => $postArr['credit_card_name'],
                            'lastName' => '',
                            'address' => isset($userAddressInfo[0]['street_address_one']) ? $userAddressInfo[0]['street_address_one'] : '',
                            'city' => isset($userAddressInfo[0]['city']) ? $userAddressInfo[0]['city'] : '',
                            'state' => isset($userAddressInfo[0]['state']) ? $userAddressInfo[0]['state'] : '',
                            'zip' => isset($userAddressInfo[0]['zip_code']) ? $userAddressInfo[0]['zip_code'] : '',
                            'country' => isset($userAddressInfo[0]['country_name']) ? $userAddressInfo[0]['country_name'] : ''
                        ),
                        'payment' => array(
                            'creditCard' => array(
                                'cardNumber' => $postArr['credit_card_number'],
                                'expirationDate' => $postArr['credit_card_exp_year'] . "-" . $postArr['credit_card_exp_month'],
                                'cardCode' => $postArr['credit_card_code'],
                            ),
                        ),
                    ),
                )
            ));

            if ($createCustomerProfile->isSuccessful()) {
                $customerProfileId = $createCustomerProfile->customerProfileId;
                $paymentProfileId = $createCustomerProfile->customerPaymentProfileIdList->numericString;
                $shippingProfileId = $createCustomerProfile->customerShippingAddressIdList->numericString;
                $customerProfileId = $customerProfileId;
                $dataArr['user_id'] = $contactDetail[0]['user_id'];
                $dataArr['profile_id'] = $customerProfileId;
                $dataArr['credit_card_type_id'] = !empty($postArr['credit_card_type_id']) ? $postArr['credit_card_type_id'] : '';
                $dataArr['credit_card_no'] = 'XXXX' . substr($postArr['credit_card_number'], -4);
                $dataArr['added_by'] = $postArr['user_id'];
                $dataArr['modified_by'] = $postArr['user_id'];


                $this->getServiceLocator()->get('User\Model\UserTable')->insertTokenInfo($dataArr);
                return true;
//}
            } else {
                $messages = array('status' => "error", 'message' => "You have entered wrong credit card details.");
                return $messages;
            }
        }
    }

    /**
     * This function is used to capture the payment from front side
     * @param      $paymentArray array(), $paymentFrontArray array()
     * @return     json
     * @author     Icreon Tech - SR by dev 3
     */
    function capturePaymentFront($paymentArray, $paymentFrontArray) {

        $paymentAmount = $paymentFrontArray['transaction_amounts'];
        $createCustomerProfile = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
        $customerProfileId = $paymentFrontArray['profile_id'];
        $createCustomerProfile->getCustomerProfileRequest(array('customerProfileId' => $customerProfileId));
        $paymentProfileId = $createCustomerProfile->profile->paymentProfiles->customerPaymentProfileId;
        $payment = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
        $payment->createCustomerProfileTransactionRequest(array(
            'transaction' => array(
                'profileTransAuthCapture' => array(
                    'amount' => $paymentAmount,
                    'customerProfileId' => $customerProfileId,
                    'customerPaymentProfileId' => $paymentProfileId,
                    'order' => array(
                        'invoiceNumber' => 'IN' . sprintf('%011d', $paymentFrontArray['transaction_id']),
                        'description' => 'description of transaction',
                        'purchaseOrderNumber' => 'No' . sprintf('%011d', $paymentFrontArray['transaction_id'])),
                )
            ),
            'extraOptions' => '<![CDATA[x_customer_ip=' . $_SERVER['SERVER_ADDR'] . ']]>'
        ));

        if ($payment->isSuccessful()) {
            $transaction = new Transaction($this->_adapter);
            $responseArr = explode(',', $payment->directResponse);
            $authorizeTransactionId = $responseArr[6];

            $paymentTransactionData = array();
            $paymentTransactionData['amount'] = $paymentFrontArray['transaction_amounts'];
            $paymentTransactionData['transaction_id'] = $paymentFrontArray['transaction_id'];
            $paymentTransactionData['payment_mode_id'] = 1;
            $paymentTransactionData['authorize_transaction_id'] = $authorizeTransactionId;

            $paymentTransactionData['profile_id'] = $customerProfileId;
            $paymentTransactionData['added_by'] = 0;
            $paymentTransactionData['added_date'] = DATE_TIME_FORMAT;
            $paymentTransactionData['modify_by'] = 0;
            $paymentTransactionData['modify_date'] = DATE_TIME_FORMAT;
            $paymentTransactionData = $transaction->getTransactionPaymentArr($paymentTransactionData);

            $profileDataParam = array();
            $profileDataParam['profile_id'] = $customerProfileId;
            $tokenDetailsResult = $this->getServiceLocator()->get('User\Model\UserTable')->getTokenDetails($profileDataParam);

            if (count($tokenDetailsResult) > 0)
                $paymentTransactionData['22'] = $tokenDetailsResult[0]['credit_card_type_id'];
            else
                $paymentTransactionData['22'] = '';
            $transactionPaymentId = $this->getTransactionTable()->saveTransactionPaymentDetail($paymentTransactionData);

            foreach ($paymentArray as $key => $val) {
                $paymentReceivedTransactionData = array();
                $paymentReceivedTransactionData['table_auto_id'] = $val['table_auto_id'];
                $paymentReceivedTransactionData['table_type'] = $val['table_type'];
                $paymentReceivedTransactionData['authorize_transaction_id'] = $authorizeTransactionId;
                $paymentReceivedTransactionData['profile_id'] = $customerProfileId;
                $paymentReceivedTransactionData['amount'] = $val['cart_product_amount'];
                $paymentReceivedTransactionData['product_type_id'] = $val['cart_product_type_id'];
                $paymentReceivedTransactionData['transaction_payment_id'] = $transactionPaymentId;
                $paymentReceivedTransactionData['transaction_id'] = $val['transaction_id'];
                $paymentReceivedTransactionData['payment_source'] = 'credit';
                $paymentReceivedTransactionData['isPaymentReceived'] = '1';
                $this->insertIntoPaymentReceive($paymentReceivedTransactionData);
            }
            // code used to remove the authorize transaction id from payment table.
            $removeTransParam = array();
            $removeTransParam['transaction_id'] = $paymentFrontArray['transaction_id'];
            $this->getTransactionTable()->removeAuthorizeTransactionId($removeTransParam);
            return true;
        } else {
            // Code to display the error message for declined transactions.
            $paymentErrorArray = explode('-', $payment->messages->message->text);
            if (isset($paymentErrorArray[1]) && !empty($paymentErrorArray[1]))
                $paymentErrMessage = $paymentErrorArray[1];
            else
                $paymentErrMessage = $paymentErrorArray[0];
            $messages = array('status' => "payment_error", 'message' => trim($paymentErrMessage));
            $removeTransactionArr[0] = $paymentArray['transactionId'];
            $this->getTransactionTable()->removeTransactionById($removeTransactionArr);
            return $messages;
        }
    }

    public function insertIntoPaymentReceive($dataArray = array()) {
        $this->getTransactionTable();

        $receivedPaymentIdArray = array();
        $totalCreditCardAmount = 0;

        $transaction = new Transaction($this->_adapter);
        $paymentTransactionData = array();
        $paymentTransactionData['amount'] = round($dataArray['amount'], 2);
        $paymentTransactionData['transaction_id'] = $dataArray['transaction_id'];
        $paymentTransactionData['authorize_transaction_id'] = $dataArray['authorize_transaction_id'];

        $paymentTransactionData['transaction_payment_id'] = $dataArray['transaction_payment_id'];
        $paymentTransactionData['added_by'] = isset($this->auth->getIdentity()->crm_user_id) ? $this->auth->getIdentity()->crm_user_id : '';
        $paymentTransactionData['added_date'] = DATE_TIME_FORMAT;
        $paymentTransactionData['modify_by'] = isset($this->auth->getIdentity()->crm_user_id) ? $this->auth->getIdentity()->crm_user_id : '';
        $paymentTransactionData['modify_date'] = DATE_TIME_FORMAT;
        $paymentTransactionDataArr['0'] = $dataArray['transaction_id'];
        //$creditTransactionPayments = $this->_transactionTable->getCreditTransactionPayment($paymentTransactionDataArr);
// $paymentTransactionData['transaction_payment_id'] = $creditTransactionPayments[0]['transaction_payment_id'];

        $paymentTransactionData = $transaction->getTransactionPaymentReceiveArr($paymentTransactionData);

        $paymentTransactionData['10'] = $dataArray['product_type_id']; // $dataArray['UnshipProductType'];
        $paymentTransactionData['11'] = @$dataArray['table_auto_id'];
        $paymentTransactionData['12'] = @$dataArray['table_type'];
        $paymentTransactionData['13'] = @$dataArray['isPaymentReceived'];
        $paymentTransactionData['14'] = @$dataArray['batch_id'];
        $paymentTransactionData['9'] = '';

        if ($dataArray['payment_source'] == 'credit') {
            $profileParam = array();
            $profileParam['transaction_id'] = $dataArray['transaction_id'];
            $profileParam['authorize_transaction_id'] = $dataArray['authorize_transaction_id'];
            $transactionProfileDetails = $this->_transactionTable->getTransactionProfileCreditCartId($profileParam);
            foreach ($transactionProfileDetails as $profile) {
                if (!empty($profile['profile_id']))
                    $paymentTransactionData['9'] = $profile['credit_card_type_id'];
            }


            //if (!empty($creditTransactionPayments)) {
            //$customerProfileId = $dataArray['profile_id'];
            //$paymentTransactionData['13'] = 0; // is_payment_received field
            return $lastInsertId = $this->_transactionTable->saveTransactionPaymentReceive($paymentTransactionData);
            //}
        } else {
            $paymentTransactionData['9'] = ''; // credit_card_type_id
            $paymentTransactionData['13'] = 1; // is_payment_received field
            return $this->_transactionTable->saveTransactionPaymentReceive($paymentTransactionData);
        }
    }

    /**
     * This Action is used to get billling shipping address info on base of id
     * @param this will pass an array $dataParam
     * @return this will be json format
     * @author Icreon Tech - DT
     */
    public function getBillingShippingAddressInfoFront($user_id) {
//$this->checkUserAuthenticationAjx();
        $this->getTransactionTable();
        if ($user_id != '') {

            $userDetail = array();
            $userArr = array('user_id' => $user_id);
            $userDetail = $this->getServiceLocator()->get('User\Model\UserTable')->getUser($userArr);
            $searchArray = array();

            //$searchArray['address_information_id'] = $address_information_id;
            $searchArray['user_id'] = $user_id;
            $searchArray['address_location'] = '2';

            //$contactDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getContact($searchArray);
            // $addressDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations($searchArray);
            $addressDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations($searchArray);
            //asd($addressDetail,0);
            $billingAdd = $this->getPrimaryUserAddress($addressDetail);
            //asd($billingAdd);
            if (!empty($billingAdd['primary_billing'])) {
                $addKey = $billingAdd['primary_billing'][0];
            } else if (!empty($billingAdd['billing'])) {
                $addKey = $billingAdd['billing'][0];
            } else {
                $addKey = "";
            }
            $userBillingAddress = array();

            $userBillingAddress = $addressDetail[$addKey];

            $userDetailsData = array_merge($userDetail, $userBillingAddress);
            return $userDetailsData;
            //return $response->setContent($searchResult);
        }
    }

    /**
     * This Action is used to get billling shipping address info on base of id
     * @param this will pass an array $dataParam
     * @return this will be json format
     * @author Icreon Tech - DT
     */
    protected function getPrimaryUserAddress($data) {
        $billingAdd = array();
        foreach ($data as $key => $val) {
            $locationVal = @explode(',', $val['location']);
            // 1,2; 1,2,3;
            if (isset($locationVal[0]) && isset($locationVal[1])) {
                //asd($locationVal);
                if (($locationVal[0] == 1 && $locationVal[1] == 2) || ($locationVal[0] == 2 && $locationVal[1] == 1)) {
                    $billingAdd['primary_billing'][] = $key;
                }
            } else if (isset($locationVal[0]) && $locationVal[0] == 2) {
                //2; 2,3
                $billingAdd['billing'][] = $key;
            }
        }
        return $billingAdd;
    }

    /**
     * This action is used to update user membership
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function updateUserMembership($userMembershipArr) {
        $transaction = new Transaction($this->_adapter);
        $userMembershipUpdateArr = array();
        $userMembershipArr['transaction_amount'] = (isset($userMembershipArr['transaction_amount']) && $userMembershipArr['transaction_amount'] == 0 &&
                !empty($userMembershipArr['donation_amount'])) ? $userMembershipArr['donation_amount'] : $userMembershipArr['transaction_amount'];
        $userMembershipUpdateArr['minimun_donation_amount'] = isset($userMembershipArr['transaction_amount']) ? $userMembershipArr['transaction_amount'] : '';
        $getMatchingMembershipArr = $transaction->getMatchingMembershipArr($userMembershipUpdateArr);
        $matchingMembership = $this->_transactionTable->getMatchingMembership($getMatchingMembershipArr);
        $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $userMembershipArr['user_id']));
        
        $gracePeriodStatus = false;
        $membershipExpiredStatus = false;
        $todayTime= time();
        $memberExpired = date("Y-m-d",strtotime($userDetail['membership_expire']));
        $leftTime = strtotime($memberExpired) - $todayTime;
		
		if($leftTime > 0){
			if(floor($leftTime / (60 * 60 * 24)) < $this->_config['membership_plus'])
				$gracePeriodStatus = true;//allow update 
		}
        
        //asd(floor($leftTime / (60 * 60 * 24)));die;
        if($gracePeriodStatus){
        $userMembershipUpdateArr = array();
        $userMembershipUpdateArr['minimun_donation_amount'] = isset($userMembershipArr['donation_amount']) ? $userMembershipArr['donation_amount'] : '';
        $getMatchingMembershipArr = $transaction->getMatchingMembershipArr($userMembershipUpdateArr);
        $matchingMembership = $this->_transactionTable->getMatchingMembership($getMatchingMembershipArr);
		}
        if ( $leftTime < 1 || ( $gracePeriodStatus == true || (!empty($matchingMembership) && $matchingMembership['membership_id'] != $userDetail['membership_id'] && ($userMembershipUpdateArr['minimun_donation_amount'] >= $userDetail['donation_amount'])) ) ) {
            $startDate = date("Y-m-d",strtotime(DATE_TIME_FORMAT));
            $membershipDateFrom = '';
            $membershipDateTo = '';
            if ($matchingMembership['validity_type'] == 1) {
                $membershipDateFrom = $startDate;
                $startDay = $this->_config['financial_year']['srart_day'];
                $startMonth = $this->_config['financial_year']['srart_month'];
                $startYear = date("Y",strtotime(DATE_TIME_FORMAT));
                $startDate = $startYear . "-" . $startMonth . "-" . $startDay;
//$year = 1;

                $addYear = ($matchingMembership['validity_time'] - $startYear) + 1;      
                $futureDate = date('Y-m-d', strtotime('+' . $addYear . ' year -1 day', strtotime($startDate)));
                $your_date = $futureDate;
                $now = time();
                $datediff = strtotime($your_date) - $now;
                if ((isset($this->_config['membership_plus']) && !empty($this->_config['membership_plus'])) && (floor($datediff / (60 * 60 * 24)) < $this->_config['membership_plus'])) {
                    $addYear = ($matchingMembership['validity_time'] - $startYear) + 2;
                    $futureDate = date('Y-m-d', strtotime('+' . $addYear . ' year', strtotime($startDate)));
                    $futureDate = date('Y-m-d', strtotime('-1 day', strtotime($futureDate)));
                }
                $membershipDateTo = $futureDate;
            } else if ($matchingMembership['validity_type'] == 2) {
                $membershipDateFrom = $startDate;
                $futureDate = date('Y-m-d', strtotime('+' . $matchingMembership['validity_time'] . ' year -1 day', strtotime($startDate)));
                $membershipDateTo = $futureDate;
            }
            $userMembershipUpdateArr = array();
            $userMembershipUpdateArr['user_id'] = $userMembershipArr['user_id'];
            $userMembershipUpdateArr['membership_id'] = $matchingMembership['membership_id'];
            $userMembershipUpdateArr['transaction_date'] = isset($userMembershipArr['transaction_date']) ? $userMembershipArr['transaction_date'] : '';
            $userMembershipUpdateArr['transaction_amount'] = isset($userMembershipArr['transaction_amount']) ? $userMembershipArr['transaction_amount'] : '';
            $userMembershipUpdateArr['membership_date_from'] = $membershipDateFrom;
            $userMembershipUpdateArr['membership_date_to'] = $membershipDateTo;
            // asd($userMembershipUpdateArr,2);
			if(empty($membershipDateFrom) || empty($membershipDateTo)){
				return false;
			}
            $getUpdateUserMembershipArr = $transaction->getUpdateUserMembershipArr($userMembershipUpdateArr);
            $userMembershipId = $this->_transactionTable->updateUserMembership($getUpdateUserMembershipArr);
            //asd($getUpdateUserMembershipArr);
        }
    }

    public function updateUserPledgeTransaction($userPledgeArr = array()) {
        /* Update active pledge for user */
        $pledge = new Pledge($this->_adapter);
        $this->getTransactionTable();
        $postedData = array();
        $postedData['userId'] = $userPledgeArr['userId'];
        $checkUserActivePledgeArr = $pledge->getCheckUserActivePledgeArr($postedData);
        $activePledges = $this->getServiceLocator()->get('Pledge\Model\PledgeTable')->checkUserActivePledge($checkUserActivePledgeArr);
        if (!empty($activePledges)) {
            $searchParam = array();
            $pledgeId = $activePledges[0]['pledge_id'];
            $searchParam['pledge_id'] = $pledgeId;
            $pledgeDetailArr = array();
            $searchPledgeTransactionsArr = $pledge->getPledgeTransactionsSearchArr($searchParam);

            $pledgeData = $this->getServiceLocator()->get('Pledge\Model\PledgeTable')->getPledgeTransactions($searchPledgeTransactionsArr);
            if (!empty($pledgeData)) {
                $pledgeDetailArr = array();
                $pledgeDetailArr[] = $pledgeId;
                $pledgeDataResult = $this->getServiceLocator()->get('Pledge\Model\PledgeTable')->getPledgeById($pledgeDetailArr);
                $pledgeDonationAmount = $userPledgeArr['donation_amount'];
                foreach ($pledgeData as $pleData) {
                    if ($pledgeDonationAmount == 0) {
                        break;
                    }
                    if ($pleData['status'] == 0 && $pledgeDataResult['pledge_status_id'] != 3) {
                        if ($pledgeDonationAmount >= $pleData['amount_pledge']) {
                            $pledgeDonationAmount = $pledgeDonationAmount - $pleData['amount_pledge'];
                            /* Update pledge transaction status */
                            $updatePledgeStatusArray = array();
                            $updatePledgeStatusArray['pledge_transaction_id'] = $pleData['pledge_transaction_id'];
                            $updatePledgeStatusArray['status'] = '1';
                            $updatePledgeStatusArray['transaction_id'] = $userPledgeArr['transaction_id'];
                            $updatePledgeStatusArray['modify_by'] = $userPledgeArr['userId'];
                            $updatePledgeStatusArray['modify_date'] = DATE_TIME_FORMAT;
                            $updatePledgeTransactionStatusArr = $pledge->updatePledgeTransactionStatusArr($updatePledgeStatusArray);
                            $pledgeTransactionId = $this->getServiceLocator()->get('Pledge\Model\PledgeTable')->updatePledgeTransactionStatus($updatePledgeTransactionStatusArr);
                            /* End update pledge transaction status */
                        } else {
                            $updatedAmountToBePaid = $pleData['amount_pledge'] - $pledgeDonationAmount;
                            $updatePledgeAmountArray = array();
                            $updatePledgeAmountArray['pledge_transaction_id'] = $pleData['pledge_transaction_id'];
                            $updatePledgeAmountArray['amount_pledge'] = $pledgeDonationAmount;
                            $updatePledgeAmountArray['remaining_amount_pledge'] = $updatedAmountToBePaid;
                            $updatePledgeAmountArray['status'] = '1';
                            $updatePledgeAmountArray['transaction_id'] = $userPledgeArr['transaction_id'];
                            $updatePledgeAmountArray['modify_by'] = $userPledgeArr['userId'];
                            $updatePledgeAmountArray['modify_date'] = DATE_TIME_FORMAT;
                            $updatePledgeTransactionAmountArr = $pledge->updatePledgeTransactionAmountArr($updatePledgeAmountArray);

                            $pledgeTransactionId = $this->getServiceLocator()->get('Pledge\Model\PledgeTable')->updatePledgeTransactionAmount($updatePledgeTransactionAmountArr);
//Update pledge amount to complete
                            break;
                        }
                    }
                }
            }
        }

        /* end  Update activie pledge for user */
    }    
    
}