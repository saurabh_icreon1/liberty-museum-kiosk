<?php

/**
 * This controller is used to manage Transactions
 * @package    Transaction
 * @author     Icreon Tech - DG
 */

namespace Transaction\Controller;

use Base\Controller\BaseController;
use Pledge\Controller\PledgeController;
use Zend\View\Model\ViewModel;
use Transaction\Model\Transaction;
use Common\Model\Common;
use Zend\Http\Request;
use Zend\Authentication\AuthenticationService;
use Transaction\Form\SearchUserTransactionForm;
use Transaction\Form\SearchUserForm;
use Transaction\Form\SearchTransactionForm;
use Transaction\Form\SaveSearchForm;
use Transaction\Form\CreateTrasactionForm;
use Transaction\Form\CheckoutFormFront;
use Transaction\Form\PaymentModeCheck;
use Transaction\Form\PaymentModeCredit;
use Transaction\Form\AddToCartForm;
use Transaction\Form\DonationAddToCartForm;
use Transaction\Form\DonationNotifyForm;
use Transaction\Form\KioskFeeForm;
use Transaction\Form\FlagOfFacesForm;
use Transaction\Form\InventoryForm;
use Transaction\Form\ViewTransactionForm;
use Transaction\Form\TransactionNotesForm;
use Transaction\Form\MembershipDetailForm;
use Transaction\Form\WallOfHonor;
use Transaction\Form\ViewFofImageForm;
use Transaction\Form\ViewWohDetailsForm;
use Transaction\Form\WallOfHonorAdditionalContact;
use Product\Controller\ProductController;
use Transaction\Form\CrmSearchPassengerForm;
use Passenger\Controller\PassengerController;
use Transaction\Form\BioCertificate;
use Transaction\Form\PassengerDocumentForm;
use Transaction\Form\TransactionStatusForm;
use Transaction\Form\ProductStatusForm;
use Transaction\Form\UpdateTrackCodeForm;
use Transaction\Form\UpdateTransactionBatchForm;
use Zend\Barcode\Barcode;
use Authorize\lib\AuthnetXML;
use Pledge\Model\Pledge;
use SimpleXMLElement;
use Base\Model\html2fpdf;
use Zend\View\Renderer\PhpRenderer;
use Zend\Session\Container;
use Base\Model\ZipStream;
use Product\Model\Category;
use Zend\Db\Adapter\Driver\ConnectionInterface;
use Zend\Db\Adapter\Adapter;
use Common\Controller\DoController;
use Group\Model\Group;
use Cases\Model\Cases;
use User\Model\Woh;

/**
 * This controller is used to manage Transactions
 * @category   Zend
 * @package    Transaction
 * @author     Icreon Tech - DG
 */
class TransactionController extends BaseController {

    protected $_transactionTable = null;
    protected $_documentTable = null;
    protected $_productTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_flashMessage = null;
    public $auth = null;

    function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        parent::__construct();
        $auth = new AuthenticationService();
        $this->auth = $auth;
    }

    /**
     * This function is used to get table object
     * @return     array
     * @param array $form
     * @author Icreon Tech - DG
     */
    public function getTransactionTable() {
        if (!$this->_transactionTable) {
            $sm = $this->getServiceLocator();
            $this->_transactionTable = $sm->get('Transaction\Model\TransactionTable');
            $this->_documentTable = $sm->get('Passenger\Model\DocumentTable');
            $this->_productTable = $sm->get('Product\Model\ProductTable');
            $this->_adapter = $sm->get('dbAdapter');
            $this->_config = $sm->get('Config');
        }
        return $this->_transactionTable;
    }

    /**
     * This function is used to get config object
     * @return     array
     * @param array $form
     * @author Icreon Tech - DG
     */
    public function getConfig() {
        $sm = $this->getServiceLocator();
        $this->_translator = $sm->get('translator');
        $this->_config = $sm->get('Config');
    }

    /**
     * This action is used to create Transaction
     * @return json
     * @param void
     * @author Icreon Tech - DG
     */
    public function createTransactionAction() {
        $this->layout('crm');
    }

    /**
     * This action is used to get user transaction
     * @return json
     * @param void
     * @author Icreon Tech - DG
     */
    public function getUserTransactionsAction() {
        $this->checkUserAuthentication();

        $this->getTransactionTable();
        $this->getConfig();
        $form = new SearchUserTransactionForm();

        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $response = $this->getResponse();
        $messages = array();

        $params = $this->params()->fromRoute();
        $form->get('user_id')->setValue($params['id']);
        $viewModel->setVariables(array(
            'form' => $form,
            'user_id' => $params['id'],
            'jsLangTranslate' => $this->_config['transaction_messages']['config']['user_search_transaction'])
        );
        return $viewModel;
    }

    /**
     * This Action is used to get user search transaction
     * @author Icreon Tech-DG
     * @return Array
     * @param Array
     */
    public function userSearchTransactionsAction() {
        try {
            $this->checkUserAuthentication();
            $request = $this->getRequest();
            $response = $this->getResponse();
            $messages = array();
            parse_str($request->getPost('searchString'), $searchParam);
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit;
            $isExport = $request->getPost('export');

            if ($isExport != "" && $isExport == "excel") {
                $chunksize = $this->_config['export']['chunk_size'];
                $export_limit = $this->_config['export']['export_limit'];

                $searchParam['recordLimit'] = $chunksize;
            }

            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['user_id'] = @$this->decrypt($searchParam['user_id']);
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            $searchParam['transaction_id'] = (isset($searchParam['transaction_id'])) ? $searchParam['transaction_id'] : '';
            $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'transaction_date') ? 'usr_transaction.transaction_date' : $searchParam['sort_field'];

            $page_counter = 1;
            do {
                $transaction_arr = $this->getTransactionTable()->getUserTransactions($searchParam);
                $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
                $total = @$countResult[0]->RecordCount;

                $jsonResult = array();
                $arrCell = array();
                $subArrCell = array();
                if ($isExport == "excel") {
                    if ($total > $chunksize) {
                        @$number_of_pages = ceil($total / $chunksize);
                        $page_counter++;
                        $start = $chunksize * $page_counter - $chunksize;
                        $searchParam['start_index'] = $start;
                        $searchParam['page'] = $page_counter;
                    } else {
                        $number_of_pages = 1;
                        $page_counter++;
                    }
                } else {
                    $page_counter = $page;
                    $number_of_pages = ceil($total / $limit);
                    $jsonResult['records'] = $countResult[0]->RecordCount;
                    $jsonResult['page'] = $request->getPost('page');
                    $jsonResult['total'] = $number_of_pages;
                }

                if (!empty($transaction_arr)) {
                    foreach ($transaction_arr as $val) {
                        $addActivityLink = '';
                        $createRmaLink = '';
                        $viewRmaLink = '';
                        if ($val['transaction_type'] == '1') {
                            $addActivityLink = '<span class="plus-sign"><a href="/create-activity/' . $this->encrypt($val['user_id']) . '/' . $this->encrypt('1') . '"><div class="tooltip">Add activity<span></span></div></a></span>';
                        } else {
                            $rmaParams['transaction_id'] = $val['transaction_id'];
                            $rmaResult = $this->getTransactionTable()->getCrmRma($rmaParams);
                            if (!empty($rmaResult)) {
                                $viewRmaLink = '<span><a href="/view-crm-rma/' . $this->encrypt($rmaResult[0]['transaction_rma_id']) . '/' . $this->encrypt('view') . '" class="search-grid"><div class="tooltip">View RMA<span></span></div></a></span>';
                            } else {
                                $getSearchArray['transactionId'] = $val['transaction_id'];
                                $crmRmaShipped = $this->getTransactionTable()->searchTransactionProducts($getSearchArray);
                                if (!empty($crmRmaShipped)) {
                                    foreach ($crmRmaShipped as $rmaShip) {
                                        if (($rmaShip['product_status'] == '10' || $rmaShip['product_status'] == '11')) {
                                            if ($rmaShip['item_type'] != '5' && $rmaShip['item_type'] != '6' && $rmaShip['tra_pro'] != 'don') {
                                                $createRmaLink = '<span><a href="/create-rma/' . $this->encrypt($val['transaction_id']) . '"  class="create-grid"><div class="tooltip">Create RMA<span></span></div></a></span>';
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if ($val['transaction_type'] == '1') {
                            $type = 'Donation';
                        } else if ($val['transaction_type'] == '2') {
                            $type = 'Purchases';
                        } else {
                            $type = 'Donation, Purchases';
                        }
                        $arrCell['id'] = $val['user_transaction_id'];

                        $encryptedView = $this->encrypt('view');
                        $encryptedTransId = $this->encrypt($val['transaction_id']);
                        $transactionDetail = $addActivityLink . $createRmaLink . $viewRmaLink . "<a onclick=getUserTransactionDetail('" . $encryptedTransId . "'); class='txt-decoration-underline' href='javascript:void(0);'>" . $val['transaction_id'] . "</a>";
                        $receipt = '';
                        if (isset($val['donation_amount']) && !empty($val['donation_amount']) && $val['donation_amount'] != '0.00') {
                            $receipt = '<a href="javascript:void(0);" class="receipt_icon" onclick="viewReceipt(\'' . $this->encrypt($val['transaction_id']) . '\',\'' . $this->encrypt($searchParam['user_id']) . '\');">Receipt</a>';
                        }


                        $campaign_title = "<a href='/campaign/" . $this->encrypt($val['campaign_id']) . "'>" . $val['campaign_title'] . "</a>";
                        if ($isExport == "excel") {
                            $arrExport[] = array('Transaction #' => $val['transaction_id'], "Date" => $this->OutputDateFormat($val['transaction_date'], 'dateformatampm'), "Product Name" => $val['product_name'], "Transaction Type" => $type, "Program" => $val['program'], "Amount($)" => $val['transaction_amount']);
                        } else {
                            $arrCell['cell'] = array($transactionDetail, $this->OutputDateFormat($val['transaction_date'], 'dateformatampm'), $val['product_name'], $type, $val['program'], "<span class='left' style='width:100px'>$ " . $this->Currencyformat($val['transaction_amount'] - ($val['cancel_amount'] + $val['refund_amount'])) . "</span>" . $receipt);
                        }
                        $subArrCell[] = $arrCell;
                        $jsonResult['rows'] = $subArrCell;
                    }
                    if ($isExport == "excel") {
                        $filename = "contact_transaction_export_" . date("Y-m-d") . ".csv";
                        $this->arrayToCsv($arrExport, $filename, $page_counter);
                    }
                }
            } while ($page_counter <= $number_of_pages && $isExport == "excel");
            if ($isExport == "excel") {
                $this->downloadSendHeaders("contact_transaction_export_" . date("Y-m-d") . ".csv");
                die;
            } else {
                $response->setContent(\Zend\Json\Json::encode($jsonResult));
                return $response;
            }
        } catch (Exception $e) {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * This Action is used to view user transaction receipt
     * @author Icreon Tech-DG
     * @return Array
     * @param Array
     */
    public function viewTaxReceiptAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $this->getTransactionTable();
        $this->getConfig();
        $viewModel = new ViewModel();
        $this->layout('popup');
        $params = $this->params()->fromRoute();
        $user_id = $this->decrypt($params['user_id']);
        $transaction_id = $this->decrypt($params['transaction_id']);
        $searchParam['user_id'] = $user_id;
        $searchParam['transaction_id'] = $transaction_id;
        $pdfPath = $this->_config['file_upload_path']['assets_upload_dir'] . "transaction/tax_receipt_" . $this->encrypt($transaction_id);

        $get_user_info = $this->getServiceLocator()->get('Common\Model\CommonTable')->getUserCommonInfo(array('user_id' => $user_id));
        $transaction_arr = $this->getTransactionTable()->getUserTransactions($searchParam);
        $transaction_info = $transaction_arr[0];

        $getTransactionsReceipt = $this->getTransactionTable()->getTransactionReceipt(array('receipt_template_id' => 1));
        $receipt = array();
        $receipt['receipt_body'] = str_replace('{--TransactionDate--}', $this->DateFormat($transaction_info['transaction_date'], 'displayfullmonth'), $getTransactionsReceipt['receipt_body']);
        $receipt['receipt_body'] = str_replace('{--Title--}', $get_user_info['title'], $receipt['receipt_body']);
        if ($get_user_info['user_type'] == 1) {
            $receipt['receipt_body'] = str_replace('{--FullName--}', $get_user_info['full_name'] . "<br>", $receipt['receipt_body']);
            $receipt['receipt_body'] = str_replace('{--TitleFullName--}', $get_user_info['full_name'], $receipt['receipt_body']);
        } else {
            $receipt['receipt_body'] = str_replace('{--FullName--}', $get_user_info['company_name'] . "<br>", $receipt['receipt_body']);
            $receipt['receipt_body'] = str_replace('{--TitleFullName--}', $get_user_info['company_name'], $receipt['receipt_body']);
        }
        $brCount = "";
        $brString = "";
        if (isset($get_user_info['street_address_one']) && !empty($get_user_info['street_address_one'])) {
            /* $get_user_info['street_address_one'] = $get_user_info['street_address_one'] . "<br>";
              $get_user_info['street_address_two'] = $get_user_info['street_address_two'] . "<br>"; */
            $get_user_info['street_address_one'] = $get_user_info['street_address_one'] . "<br>";
        } else {
            $get_user_info['street_address_one'] = '';
            $brCount++;
            //$get_user_info['street_address_two'] = '';
        }
        if (isset($get_user_info['street_address_two']) && !empty($get_user_info['street_address_two'])) {
            $get_user_info['street_address_two'] = $get_user_info['street_address_two'] . "<br>";
        } else {
            $get_user_info['street_address_two'] = '';
            $brCount++;
        }

        $get_user_info['city'] = (isset($get_user_info['city']) && !empty($get_user_info['city'])) ? $get_user_info['city'] . ",&nbsp;" : '';
        $get_user_info['state'] = (isset($get_user_info['state']) && !empty($get_user_info['state'])) ? $get_user_info['state'] . "<br>" : '';

        if (isset($get_user_info['state']) && empty($get_user_info['state'])) {
            $brCount++;
        }
        $get_user_info['country_name'] = (isset($get_user_info['country_code']) && !empty($get_user_info['country_code'])) ? $get_user_info['country_code'] . " - " : '';
        $get_user_info['zip_code'] = (isset($get_user_info['zip_code']) && !empty($get_user_info['zip_code'])) ? $get_user_info['zip_code'] . "<br>" : '';

        if (isset($get_user_info['zip_code']) && empty($get_user_info['zip_code'])) {
            $brCount++;
        }

        $receipt['receipt_body'] = str_replace('{--Address--}', $get_user_info['street_address_one'] . $get_user_info['street_address_two'], $receipt['receipt_body']);
        $receipt['receipt_body'] = str_replace('{--City--}', $get_user_info['city'], $receipt['receipt_body']);
        $receipt['receipt_body'] = str_replace('{--State--}', $get_user_info['state'], $receipt['receipt_body']);
        $receipt['receipt_body'] = str_replace('{--Country--}', $get_user_info['country_name'], $receipt['receipt_body']);

        if (!empty($brCount)) {
            for ($i = 0; $i < $brCount; $i++) {
                $brString .= "<br>";
            }
        }

        $receipt['receipt_body'] = str_replace('{--ZipCode--}', $get_user_info['zip_code'] . $brString, $receipt['receipt_body']);

        $receipt['receipt_body'] = str_replace('{--Amount--}', ($transaction_info['donation_amount'] - $transaction_info['donation_refund']), $receipt['receipt_body']);
        $receipt['receipt_body'] = str_replace('{--CampaignCode--}', $transaction_info['campaign_code'], $receipt['receipt_body']);
        $receipt['receipt_body'] = str_replace('{--Program--}', $transaction_info['program'], $receipt['receipt_body']);
        $receipt['receipt_body'] = str_replace('{--ContentId--}', $get_user_info['contact_id'], $receipt['receipt_body']);
        $receipt['receipt_body'] = str_replace('{--Img--}', $this->_config['img_file_path']['path'] . '/img/digital_singed.png', $receipt['receipt_body']);
        try {
            $html2pdf = new\ HTML2PDF('P', 'A4', 'en');
            $html2pdf->setDefaultFont('Times');
            $html2pdf->writeHTML($receipt['receipt_body'], false);
            $html2pdf->Output($pdfPath . "/" . date('Y-m-d') . "_" . $transaction_id . "_tax_invoice.pdf");
        } catch (HTML2PDF_exception $e) {
            echo $e;
            exit;
        }

//        $viewModel->setVariables(array(
//            'receipt' => $receipt,
//            'jsLangTranslate' => $this->_config['transaction_messages']['config']['user_tax_receipt'])
//        );
//        return $viewModel;
    }

    /**
     * This Action is used to create crm transaction
     * @author Icreon Tech-DT
     * @return Array
     * @param Array
     */
    public function addCrmTransactionAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $createTransactionMessage = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $transaction = new Transaction($this->_adapter);
        $searchUserForm = new SearchUserForm();
        $request = $this->getRequest();
        $state = $this->getServiceLocator()->get('Common\Model\CommonTable')->getStates();
        $stateList = array();
        $stateList[''] = 'Select State';
        foreach ($state as $key => $val) {
            $stateList[$val['state']] = $val['state'] . " - " . $val['state_code'];
        }
        $searchUserForm->get('state_select')->setAttribute('options', $stateList);

        $membershiplevel = $this->getServiceLocator()->get('Common\Model\CommonTable')->getMembership();
        $membershiplevelList = array();
        $membershiplevelList[''] = 'Select Membership Level';
        if (isset($membershiplevel) and count($membershiplevel) > 0) {
            foreach ($membershiplevel as $key => $val) {
                $membershiplevelList[$val['membership_id']] = $val['membership_title'];
            }
        }
        $searchUserForm->get('membership_level')->setAttribute('options', $membershiplevelList);
        //$searchUserForm->get('last_transaction')->setValue('3');
        $receivedDateRange = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTransactionRange();
        $dateRange = array("" => "Select One");
        foreach ($receivedDateRange as $key => $val) {
            $dateRange[$val['range_id']] = $val['range'];
        }
        $searchUserForm->get('last_transaction')->setAttribute('options', $dateRange);

        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $response = $this->getResponse();
            $postArray = $request->getPost()->toArray();
            parse_str($request->getPost('searchString'), $searchParam);
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit; // do not put $limit*($page - 1)
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'modified_date') ? 'users.modified_date' : $searchParam['sort_field'];
            $searchParam['state'] = ($searchParam['is_usa'] == '1') ? $searchParam['state_select'] : $searchParam['state'];
// registration date - start
            if (isset($searchParam['last_transaction']) && $searchParam['last_transaction'] != 1 && $searchParam['last_transaction'] != '') {
                $date_range = $this->getDateRange($searchParam['last_transaction']);
                $searchParam['date_from'] = $this->DateFormat($date_range['from'], 'db_date_format_from');
                $searchParam['date_to'] = $this->DateFormat($date_range['to'], 'db_date_format_to');
            } else if (isset($searchParam['last_transaction']) && $searchParam['last_transaction'] == '') {
                $searchParam['date_from'] = '';
                $searchParam['date_to'] = '';
            } else {
                if (isset($searchParam['from_date']) && $searchParam['from_date'] != '') {
                    $searchParam['date_from'] = $this->DateFormat($searchParam['from_date'], 'db_date_format_from');
                }
                if (isset($searchParam['to_date']) && $searchParam['to_date'] != '') {
                    $searchParam['date_to'] = $this->DateFormat($searchParam['to_date'], 'db_date_format_to');
                }
            }
// registration date - end
            $searchContactArr = $transaction->getContactSearchArr($searchParam);
            $seachResult = $this->_transactionTable->getAllContactSearch($searchContactArr);
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $jsonResult['page'] = $request->getPost('page');
            $jsonResult['records'] = $countResult[0]->RecordCount;
            $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
            if (!empty($seachResult)) {
                if (isset($this->_config['https_on']) && $this->_config['https_on'] == '1') {
                    $siteUrl = SITE_URL_HTTPS;
                } else {
                    $siteUrl = SITE_URL;
                }
                foreach ($seachResult as $val) {
                    $arrCell['id'] = $val['user_id'];
                    $membershipAfhicStr = 'M : ';
                    if (isset($val['user_membership_title']) and trim($val['user_membership_title']) != "") {
                        $membershipAfhicStr .= '' . trim($val['user_membership_title']) . '';
                    }
                    $membershipAfhicStr .= "<br>A : ";
                    if (!empty($val['afihc_numbers'])) {
                        $afhicArr = explode(',', $val['afihc_numbers']);
                        $membershipAfhicStr.=$afhicArr[0] . "&nbsp;&nbsp;&nbsp;&nbsp;<a class='view_afihc_codes txt-decoration-underline' href='#view_afihc_content' onclick='showAfihcNumbers(\"{$val['afihc_numbers']}\")'>View</a>";
                    } else {
                        $membershipAfhicStr.='N/A';
                    }
                    $fullName = trim($val['full_name']);
                    $fullName = (!empty($fullName)) ? $val['full_name'] : $val['company_name'];
                    $fullName = '<a href="' . $siteUrl . '/get-crm-cart-products/' . $this->encrypt($val['user_id']) . '" class="txt-decoration-underline">' . $fullName . '</a>';
                    $phoneNum = !empty($val['phone_number']) ? $val['phone_number'] : 'N/A';
                    $arrCell['cell'] = array($membershipAfhicStr, $val['contact_id'], $fullName, $val['email_id'], $phoneNum, $val['zip_code'], $val['state'], $val['country_name']);
                    $subArrCell[] = $arrCell;
                    $jsonResult['rows'] = $subArrCell;
                }
            } else {
                $jsonResult['rows'] = array();
            }
            return $response->setContent(\Zend\Json\Json::encode($jsonResult));
        } else {
            $this->layout('crm');
            $messages = array();
            if ($this->_flashMessage->hasMessages()) {
                $messages = $this->_flashMessage->getMessages();
            }
            $viewModel = new ViewModel();
            $response = $this->getResponse();
            $viewModel->setVariables(array(
                'form' => $searchUserForm,
                'messages' => $messages,
                'jsLangTranslate' => array_merge($createTransactionMessage, $this->_config['transaction_messages']['config']['common_message']),
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to get user transaction liSt frontend
     * @return json
     * @param void
     * @author Icreon Tech - DG
     */
    public function getUserTransactionListAction() {
        $this->checkFrontUserAuthentication();
        $this->getTransactionTable();
        $this->getConfig();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $this->layout('layout');
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $params = $this->params()->fromRoute();
        $usrTransactionArr = $this->getTransactionTable()->getUserTransactions(array('user_id' => $this->decrypt($params['user_id'])));

        $viewModel->setVariables(array(
            'usrTransactionArr' => $usrTransactionArr,
            'jsLangTranslate' => $this->_config['transaction_messages']['config']['user_transaction_list'])
        );
        return $viewModel;
    }

    /**
     * This action is used to search transaction
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function getCrmTransactionsAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $params = $this->params()->fromRoute();
        $searchTransactionMessage = $this->_config['transaction_messages']['config']['search_crm_transaction'];
        $transaction = new Transaction($this->_adapter);
        $searchTransactionForm = new SearchTransactionForm();
        $saveSearchForm = new SaveSearchForm();
        $request = $this->getRequest();
        /* Transaction source type */
        $getTransactionSourceType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTransactionSource();
        $transactionSourceType = array();
        $transactionSourceType[''] = 'Any';
        if ($getTransactionSourceType !== false) {
            foreach ($getTransactionSourceType as $key => $val) {
                $transactionSourceType[$val['transaction_source_id']] = $val['transaction_source'];
            }
        }
        $searchTransactionForm->get('transaction_source_id')->setAttribute('options', $transactionSourceType);
        /* End  Transaction source type  */

        /* Get date range */
        $receivedDateRange = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTransactionRange();
        $dateRange = array("" => "Select One");
        foreach ($receivedDateRange as $key => $val) {
            $dateRange[$val['range_id']] = $val['range'];
        }
        $searchTransactionForm->get('received_date_range')->setAttribute('options', $dateRange);
        //$searchTransactionForm->get('received_date_range')->setAttribute('value', '3');
        /* end Get date range */

        /** Contact type */
        $getUserType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getType();
        $userType = array();
        $userType[''] = 'Select Type';
        foreach ($getUserType as $key => $val) {
            $userType[$val['user_type_id']] = $val['user_type'];
        }
        $searchTransactionForm->get('type')->setAttribute('options', $userType);

        /** Product category */
        $postArr = array();
        $postArr['product_name'] = '';
        $postArr['product_type'] = '6';/**  Wall of Honor */
        $postArr['product_id'] = '';

        /** category section */
        $catArr = array();
        $category = new Category($this->_adapter);
        $categoryArr = $category->getCategoriesArr($catArr);
        $sm = $this->getServiceLocator();
        $categoryTable = $sm->get('Category\Model\CategoryTable');

        $allCategories = $categoryTable->getCategories($categoryArr);
        if (!empty($allCategories)) {
            $this->getSelectArr($allCategories);
        }

        $searchTransactionForm->get('product_category')->setAttribute('options', $this->_categorySelectArr);
        /** Product category ends */
        /* Transaction status type */
        $getTransactionStatus = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTransactionStatus();
        $transactionStatus = array();
        $transactionStatus[''] = 'Any';
        if ($getTransactionStatus !== false) {
            foreach ($getTransactionStatus as $key => $val) {
                $transactionStatus[$val['transaction_status_id']] = $val['transaction_status'];
            }
        }
        $searchTransactionForm->get('transaction_status_id')->setAttribute('options', $transactionStatus);
        if (isset($params['item_status']) && $this->decrypt($params['item_status']) == 'pending') {
            $searchTransactionForm->get('transaction_status_id')->setValue('1');
        }
        if (isset($params['item_status']) && $this->decrypt($params['item_status']) == 'review') {
            $searchTransactionForm->get('transaction_status_id')->setValue('2');
        }
        /* End  Transaction status type  */
        /* product type */
        $getProductType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductsType();
        $productType = array();
        if ($getProductType !== false) {
            foreach ($getProductType as $key => $val) {
                $productType[$val['product_type_id']] = $val['product_type'];
            }
        }
        $searchTransactionForm->get('product_type_id')->setAttribute('options', $productType);
        if (isset($params['item_status']) && $this->decrypt($params['item_status']) == 'donation') {
            $searchTransactionForm->get('product_type_id')->setValue('2');
        }
        /* End  product type  */
        /* payment mode */
        $getPaymentMode = $this->getServiceLocator()->get('Common\Model\CommonTable')->getPaymentMode();
        $paymentMode = array();
        $paymentMode[''] = 'Select One';
        if ($getPaymentMode !== false) {
            foreach ($getPaymentMode as $key => $val) {
                $paymentMode[$val['payment_mode_id']] = $val['payment_mode'];
            }
        }
        $searchTransactionForm->get('payment_mode_id')->setAttribute('options', $paymentMode);
        /* End  payment mode */

        /* item status */
        $getShippingMethod = $this->_transactionTable->getAllShippingMethod();
        $shippingArray = array();
        $shippingArray[''] = 'Any';
        if ($getShippingMethod !== false) {
            foreach ($getShippingMethod as $key => $val) {
                $shippingArray[$val['pb_shipping_type_id']] = $val['web_selection'] . "( " . $val['description'] . " )";
            }
        }
        $searchTransactionForm->get('shipping_id')->setAttribute('options', $shippingArray);
        /* end item status */

        /* item status */
        $getProductStatus = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductStatus();
        $productStatus = array();
        $productStatus[''] = 'Any';
        if ($getTransactionStatus !== false) {
            foreach ($getProductStatus as $key => $val) {
                $productStatus[$val['product_status_id']] = $val['product_status'];
            }
        }
        $searchTransactionForm->get('item_status')->setAttribute('options', $productStatus);
        /* end item status */

        $crmProducts = $this->getServiceLocator()->get('Product\Model\ProductTable')->getCrmProducts(array());
        $productResult = array();
        if ($crmProducts !== false) {
            foreach ($crmProducts as $id => $product) {
                $productResult[$product['product_id']] = $product['product_name'];
            }
        }
        $searchTransactionForm->get('product_ids')->setAttribute('options', $productResult);


        $getSearchArray = array();
        $getSearchArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
        $getSearchArray['transaction_search_id'] = '';
        $getSearchArray['is_active'] = '1';
        $transactionSavedSearchArray = $transaction->getTransactionSavedSearchArr($getSearchArray);
        $transactionSearchArray = $this->_transactionTable->getTransactionSavedSearch($transactionSavedSearchArray);
        $transactionSearchList = array();
        $transactionSearchList[''] = 'Select';
        foreach ($transactionSearchArray as $key => $val) {
            $transactionSearchList[$val['transaction_search_id']] = stripslashes($val['title']);
        }
        $searchTransactionForm->get('saved_search')->setAttribute('options', $transactionSearchList);

        /* end  Saved search data */
        if (($request->isXmlHttpRequest() && $request->isPost()) || $request->getPost('export') == 'excel') {
            $response = $this->getResponse();
            $postArray = $request->getPost()->toArray();
            parse_str($request->getPost('searchString'), $searchParam);
            $dashlet = $request->getPost('crm_dashlet');
            $dashletSearchId = $request->getPost('searchId');
            $dashletId = $request->getPost('dashletId');
            $dashletSearchType = $request->getPost('searchType');
            $dashletSearchString = $request->getPost('dashletSearchString');
            if (isset($dashletSearchString) && $dashletSearchString != '') {
                $searchParam = unserialize($dashletSearchString);
            }
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit; // do not put $limit*($page - 1)
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            $isExport = $request->getPost('export');
            if ($isExport != "" && $isExport == "excel") {
                ini_set('max_execution_time', 0);
                $chunksize = $this->_config['export']['chunk_size'];
                $export_limit = $this->_config['export']['export_limit'];

                $searchParam['record_limit'] = $chunksize;
            }


            if (isset($searchParam['received_date_range']) && $searchParam['received_date_range'] != 1 && $searchParam['received_date_range'] != '') {
                $dateRange = $this->getDateRange($searchParam['received_date_range']);
                $searchParam['received_date_from'] = $this->DateFormat($dateRange['from'], 'db_date_format_from');
                $searchParam['received_date_to'] = $this->DateFormat($dateRange['to'], 'db_date_format_to');
            } else if (isset($searchParam['received_date_range']) && $searchParam['received_date_range'] == '') {
                $searchParam['received_date_from'] = '';
                $searchParam['received_date_to'] = '';
            } else {
                if (isset($searchParam['received_date_from']) && $searchParam['received_date_from'] != '') {
                    $searchParam['received_date_from'] = $this->DateFormat($searchParam['received_date_from'], 'db_date_format_from');
                }
                if (isset($searchParam['received_date_to']) && $searchParam['received_date_to'] != '') {
                    $searchParam['received_date_to'] = $this->DateFormat($searchParam['received_date_to'], 'db_date_format_to');
                }
            }
            $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'modified_date') ? 'usr_transaction.modified_date' : $searchParam['sort_field'];
            if (!empty($searchParam['item_status'])) {

                if (is_array($searchParam['item_status'])) {
                    $searchParam['item_status'] = trim(implode(',', $searchParam['item_status']), ",");
                }
            }

            if (!empty($searchParam['product_type_id'])) {

                if (is_array($searchParam['product_type_id'])) {
                    $searchParam['product_type_id'] = trim(implode(',', $searchParam['product_type_id']), ",");
                }
            }

            if (!empty($searchParam['product_ids'])) {

                if (is_array($searchParam['product_ids'])) {
                    $searchParam['product_ids'] = trim(implode(',', $searchParam['product_ids']), ",");
                }
            }
            $page_counter = 1;
            //ticket:976 Export on CSV
            $fileCounter = 1;
            $filename = "transaction_export_" . EXPORT_DATE_TIME_FORMAT;
            do {
                $searchTransactionArr = $transaction->getTransactionSearchArr($searchParam);
                $seachResult = $this->_transactionTable->getAllTransactions($searchTransactionArr);
                $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
                $dashletResult = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getTableStruct(array('user_id' => $this->auth->getIdentity()->crm_user_id, 'search_id' => $dashletSearchId, 'dash_id' => $dashletId, 'search_type' => $dashletSearchType));
                $dashletColumnName = array();
                if (!empty($dashletResult)) {
                    $dashletColumn = explode(",", $dashletResult[0]['table_column_names']);
                    foreach ($dashletColumn as $val) {
                        if (strpos($val, ".")) {
                            $dashletColumnName[] = trim(strstr($val, ".", false), ".");
                        } else {
                            $dashletColumnName[] = trim($val);
                        }
                    }
                }
                $jsonResult = array();
                $arrCell = array();
                $subArrCell = array();
                $total = $countResult[0]->RecordCount;
                if ($isExport == "excel") {

                    /* if ($total > $export_limit) {
                      $totalExportedFiles = ceil($total / $export_limit);
                      $exportzip = 1;
                      }
                     * */

                    if ($total > $chunksize) {
                        $number_of_pages = ceil($total / $chunksize);


                        $page_counter++;
                        $start = $chunksize * $page_counter - $chunksize;
                        $searchParam['start_index'] = $start;
                        $searchProductParam['page'] = $page_counter;
                    } else {
                        $number_of_pages = 1;
                        $page_counter++;
                    }
                } else {
                    $page_counter = $page;
                    $number_of_pages = ceil($total / $limit);
                    $jsonResult['records'] = $countResult[0]->RecordCount;
                    $jsonResult['page'] = $page;
                    $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
                }

//  $jsonResult['page'] = $request->getPost('page');
//$jsonResult['records'] = $countResult[0]->RecordCount;
// $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
                if (!empty($seachResult)) {
                    $arrExport = array();
                    foreach ($seachResult as $val) {
                        $dashletCell = array();
                        $arrCell['id'] = $val['transaction_id'];
                        $encryptId = $this->encrypt($val['transaction_id']);
                        $viewLink = '<a href="/get-transaction-detail/' . $encryptId . '/' . $this->encrypt('view') . '" class="view-icon" alt="view transaction"><div class="tooltip">View<span></span></div></a>';
                        $deleteLink = '<a onclick="deleteTransaction(\'' . $encryptId . '\')" href="#delete_transaction_content" class="delete_transaction delete-icon" alt="delete transaction"><div class="tooltip">Delete<span></span></div></a>';
                        $actions = '<div class="action-col">' . $viewLink . $deleteLink . '</div>';
                        $transactionDate = ($val['transaction_date'] == 0) ? 'N/A' : $this->OutputDateFormat($val['transaction_date'], 'dateformatampm');
                        $fullName = trim($val['full_name']);
                        $fullName = (!empty($fullName)) ? $val['full_name'] : $val['company_name'];
                        $addActivityLink = '';
                        $createRmaLink = '';
                        $viewRmaLink = '';
                        if ($val['transaction_type'] == '1') {
                            $addActivityLink = '<span class="plus-sign"><a href="/create-activity/' . $this->encrypt($val['user_id']) . '/' . $this->encrypt('1') . '"><div class="tooltip">Add activity<span></span></div></a></span>';
                        } else {
                            $rmaParams['transaction_id'] = $val['transaction_id'];
                            $rmaResult = $this->getTransactionTable()->getCrmRma($rmaParams);

                            if (!empty($rmaResult)) {
                                $viewRmaLink = '<span><a href="/view-crm-rma/' . $this->encrypt($rmaResult[0]['transaction_rma_id']) . '/' . $this->encrypt('view') . '" class="search-grid"><div class="tooltip">View RMA<span></span></div></a></span>';
                            } else {
                                $getSearchArray['transactionId'] = $val['transaction_id'];
                                $crmRmaShipped = $this->getTransactionTable()->searchTransactionProducts($getSearchArray);
                                if (!empty($crmRmaShipped)) {
                                    foreach ($crmRmaShipped as $rmaShip) {
                                        if (($rmaShip['product_status'] == '10' || $rmaShip['product_status'] == '11')) {
                                            if ($rmaShip['item_type'] != '5' && $rmaShip['item_type'] != '6' && $rmaShip['tra_pro'] != 'don') {
                                                $createRmaLink = '<span><a href="/create-rma/' . $this->encrypt($val['transaction_id']) . '"  class="create-grid"><div class="tooltip">Create RMA<span></span></div></a></span>';
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if (isset($val['omx_order_number']) && !empty($val['omx_order_number'])) {
                            $transactionId = $addActivityLink . $createRmaLink . $viewRmaLink . $val['transaction_id'] . '<br>OMX: ' . $val['omx_order_number'];
                        } else {
                            $transactionId = $addActivityLink . $createRmaLink . $viewRmaLink . $val['transaction_id'];
                        }
                        if (isset($dashlet) && $dashlet == 'dashlet') {

                            if (false !== $dashletColumnKey = array_search('transaction_id', $dashletColumnName)) {
                                $view = $this->encrypt('view');
                                $dashletCell[$dashletColumnKey] = '<a href="/get-transaction-detail/' . $this->encrypt($val['transaction_id']) . '/' . $view . '" class="txt-decoration-underline">' . $val['transaction_id'] . '</a>';
                            }
                            if (false !== $dashletColumnKey = array_search('full_name', $dashletColumnName))
                                $dashletCell[$dashletColumnKey] = "<a href='/view-contact/" . $this->encrypt($val['user_id']) . "/" . $view . "' class='txt-decoration-underline view-icon'>$fullName</a>";
                            if (false !== $dashletColumnKey = array_search('transaction_date', $dashletColumnName))
                                $dashletCell[$dashletColumnKey] = $transactionDate;
                            if (false !== $dashletColumnKey = array_search('product_type', $dashletColumnName))
                                $dashletCell[$dashletColumnKey] = $val['product_type'];
                            if (false !== $dashletColumnKey = array_search('transaction_amount', $dashletColumnName))
                                $dashletCell[$dashletColumnKey] = '$ ' . $val['transaction_amount'];
                            if (false !== $dashletColumnKey = array_search('transaction_source', $dashletColumnName))
                                $dashletCell[$dashletColumnKey] = $val['transaction_source'];
                            if (false !== $dashletColumnKey = array_search('payment_mode', $dashletColumnName))
                                $dashletCell[$dashletColumnKey] = $val['payment_mode'];
                            if (false !== $dashletColumnKey = array_search('campaign_title', $dashletColumnName))
                                $dashletCell[$dashletColumnKey] = $val['campaign_title'];
                            if (false !== $dashletColumnKey = array_search('transaction_status', $dashletColumnName))
                                $dashletCell[$dashletColumnKey] = $val['transaction_status'];
                            if (isset($dashlet) && $dashlet == 'dashlet') {
                                $arrCell['cell'] = $dashletCell;
                            }
                            $view = $this->encrypt('view');
// $dashletTransactionId = '<a href="/get-transaction-detail/' . $this->encrypt($val['transaction_id']) . '/' . $view . '" class="txt-decoration-underline">' . $val['transaction_id'] . '</a>';
// $fullName = "<a href='/view-contact/" . $this->encrypt($val['user_id']) . "/" . $view . "' class='txt-decoration-underline view-icon'>$fullName</a>";
// $arrCell['cell'] = array($dashletTransactionId, $fullName, $val['product_type'], '$ ' . $val['transaction_amount'], $transactionDate);
                        } else if ($isExport == "excel") {
                            $invoiceNumber = (!empty($val['invoice_number'])) ? $val['invoice_number'] : 'N/A';
                            $val['batch_id'] = str_replace('<br>', '', $val['batch_id']);
                            $batch_id = strpos($val['batch_id'], ",") ? str_replace(",", ", ", $val['batch_id']) : $val['batch_id'];
                            $arrExport[] = array('Transaction #' => $val['transaction_id'], 'Transaction Source' => $val['transaction_source'], 'Name' => $fullName, 'Payment mode' => $val['payment_mode'], 'Campaign' => $val['campaign_title'], 'Amount' => '$ ' . $val['transaction_amount'], 'Recieved' => $transactionDate, 'Batch #' => $val['batch_id'], 'Status' => $val['transaction_status']);
                        } else {
                            $invoiceNumber = (!empty($val['invoice_number'])) ? $val['invoice_number'] : 'N/A';
                            $arrCell['cell'] = array($transactionId, $val['transaction_source'], $fullName, $val['payment_mode'], $val['campaign_title'], '$ ' . $val['transaction_amount'], $transactionDate, $val['batch_id'], $val['transaction_status'], $actions);
                        }

                        $subArrCell[] = $arrCell;
                        $jsonResult['rows'] = $subArrCell;
                    }
                    if ($isExport == "excel") {
                        //$filename = "transaction_export_" . date("Y-m-d") . ".csv";
                        //$this->arrayToCsv($arrExport, $filename, $page_counter);
                        //ticket:976 Export on CSV
                        $fileLimit = $this->_config['csv_file']['max_records'];
                        $fileCounter = $this->exportCSV($arrExport, $filename, $fileLimit, $fileCounter);
                    }
                } else {
                    $jsonResult['rows'] = array();
                }
            } while ($page_counter <= $number_of_pages && $isExport == "excel");
            if ($isExport == "excel") {
                $this->downloadSendHeaders($filename . ".zip");
                //ticket:976 Export on CSV
                //removing temp files
                $tempfilePath = $this->_config['file_upload_path']['temp_data_upload_dir'] . 'export/';
                //sudo chmod -R ug+rw export/   folder should have this permission
                $tempFiles = glob($tempfilePath . $filename . '*', GLOB_BRACE);
                foreach ($tempFiles as $file) {
                    if (is_file($file)) {
                        unlink($file);
                    }
                }
                die;
            } else {
                $response->setContent(\Zend\Json\Json::encode($jsonResult));
                return $response;
            }
// return $response->setContent(\Zend\Json\Json::encode($jsonResult));
        } else {
            $this->layout('crm');
            $messages = array();
            if ($this->_flashMessage->hasMessages()) {
                $messages = $this->_flashMessage->getMessages();
            }
            $viewModel = new ViewModel();
            $response = $this->getResponse();
            $viewModel->setVariables(array(
                'form' => $searchTransactionForm,
                'search_form' => $saveSearchForm,
                'messages' => $messages,
                'jsLangTranslate' => array_merge($searchTransactionMessage, $this->_config['transaction_messages']['config']['common_message']),
            ));
            return $viewModel;
        }
    }

    /**
     * This Action is used to save the transaction search
     * @param this will pass an array $saveSearchParam
     * @return this will be json format with comfirmation message
     * @author Icreon Tech -DT
     */
    public function addCrmSearchTransactionAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $transaction = new Transaction($this->_adapter);
        $searchTransactionMessage = $this->_config['transaction_messages']['config']['search_crm_transaction'];
        if ($request->isPost()) {
            parse_str($request->getPost('searchString'), $searchParam);
            $searchName = $request->getPost('search_name');
            $messages = array();
            parse_str($request->getPost('searchString'), $searchParam);
            $searchParam['search_title'] = $searchName;
            if (isset($searchParam['item_status']))
                $searchParam['item_status'] = implode(',', $searchParam['item_status']);

            $searchParam = $transaction->getInputFilterTransactionSearch($searchParam);

            $searchName = $transaction->getInputFilterTransactionSearch($searchName);

            if (trim($searchName) != '') {
                $saveSearchParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $saveSearchParam['title'] = $searchName;
                $saveSearchParam['search_query'] = serialize($searchParam);
                $saveSearchParam['is_active'] = 1;
                $saveSearchParam['is_delete'] = '0';
                if ($request->getPost('search_id') != '') {
                    $saveSearchParam['modified_date'] = DATE_TIME_FORMAT;
                    $saveSearchParam['transaction_search_id'] = $request->getPost('search_id');
                    $transactionUpdateArr = $transaction->getAddTransactionSearchArr($saveSearchParam);
                    if ($this->_transactionTable->updateTransactionSearch($transactionUpdateArr) == true) {
                        $this->flashMessenger()->addMessage($searchTransactionMessage['SAVE_UPDATED_MSG']);
                        $messages = array('status' => "success");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                } else {
                    $saveSearchParam['added_date'] = DATE_TIME_FORMAT;
                    $transactionAddArray = $transaction->getAddTransactionSearchArr($saveSearchParam);
                    if ($this->_transactionTable->saveTransactionSearch($transactionAddArray) == true) {
                        $this->flashMessenger()->addMessage($searchTransactionMessage['SAVE_SUCCESS_MSG']);
                        $messages = array('status' => "success");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                }
            }
        }
    }

    /**
     * This Action is used to get the saved transaction search
     * @param this will pass an array $dataParam
     * @return this will be json format
     * @author Icreon Tech - DT
     */
    public function getTransactionSearchSavedParamAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $transaction = new Transaction($this->_adapter);
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            if ($request->getPost('search_id') != '') {

                $searchArray = array();
                $searchArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $searchArray['transaction_search_id'] = $request->getPost('search_id');
                $searchArray['is_active'] = '1';
                $transactionSavedSearchArray = $transaction->getTransactionSavedSearchArr($searchArray);
                $transactionSearchArray = $this->_transactionTable->getTransactionSavedSearch($transactionSavedSearchArray);
                $searchResult = json_encode(unserialize($transactionSearchArray[0]['search_query']));
                return $response->setContent($searchResult);
            }
        }
    }

    /**
     * This Action is used to delete the the saved transaction search
     * @param this will take an array $dataParam
     * @return this will be a confirmation message in json
     * @author Icreon Tech - DT
     */
    public function deleteCrmSearchTransactionAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $transaction = new Transaction($this->_adapter);
        $request = $this->getRequest();
        $response = $this->getResponse();
        $searchTransactionMessage = $this->_config['transaction_messages']['config']['search_crm_transaction'];
        if ($request->isPost()) {
            $deleteArray = array();
            $deleteArray['is_deleted'] = 1;
            $deleteArray['modified_date'] = DATE_TIME_FORMAT;
            $deleteArray['transaction_search_id'] = $request->getPost('search_id');
            $transactionDeleteSavedSearchArray = $transaction->getDeleteTransactionSavedSearchArr($deleteArray);
            $this->_transactionTable->deleteSavedSearch($transactionDeleteSavedSearchArray);
            $messages = array('status' => "success");
            $this->flashMessenger()->addMessage($searchTransactionMessage['SEARCH_DELETE_CONFIRM']);
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This action is used to delete transaction
     * @param  void
     * @return json format string
     * @author Icreon Tech - DT
     */
    public function deleteCrmTransactionAction() {
        $request = $this->getRequest();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $transactionId = $request->getPost('transaction_id');
            if ($transactionId != '') {
                $transactionId = $this->decrypt($transactionId);
                $postArr = $request->getPost();
                $postArr['transaction_id'] = $transactionId;
                $this->getTransactionTable();
                $searchTransactionMessage = $this->_config['transaction_messages']['config']['search_crm_transaction'];
                $transaction = new Transaction($this->_adapter);

                $response = $this->getResponse();

                $transactionArr = $transaction->getArrayForRemoveTransaction($postArr);

                $this->_transactionTable->removeTransactionById($transactionArr);
                $changeLogArray = array();
                $changeLogArray['table_name'] = 'tbl_tra_change_logs';
                $changeLogArray['activity'] = '3';
                $changeLogArray['id'] = $transactionId;
                $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                /** end insert into the change log */
                $this->flashMessenger()->addMessage($searchTransactionMessage['TRANSACTION_DELETED_SUCCESSFULLY']);
                $messages = array('status' => "success", 'message' => $searchTransactionMessage['TRANSACTION_DELETED_SUCCESSFULLY']);

                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                return $this->redirect()->toRoute('get-crm-transactions');
            }
        } else {
            return $this->redirect()->toRoute('get-crm-transactions');
        }
    }

    /**
     * This action is used to show get crm cart products detail
     * @param void
     * @return     array
     * @author Icreon Tech - DT
     */
    public function getCrmCartProductsAction() {
        $this->layout('crm');
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $createTransactionMessage = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $applicationConfigIds = $this->_config['application_config_ids'];
        $uSId = $applicationConfigIds['united_state_country_id'];
        $createTransactionForm = new CreateTrasactionForm();
        $paymentModeCheck = new PaymentModeCheck();
        $paymentModeCredit = new PaymentModeCredit();
        $batchForm = new UpdateTransactionBatchForm();
        $transaction = new Transaction($this->_adapter);
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();
        $billingFlagStatus = "1";
        if (isset($params['user_id']) && $params['user_id'] != '') {
            $userId = $this->decrypt($params['user_id']);
            /* Get check types */
            $checkTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCheckTypes();
            $checkTypeList = array();
            foreach ($checkTypes as $key => $val) {
                $checkTypeList[$val['cheque_type_id']] = $val['cheque_type_name'];
            }
            $paymentModeCheck->get('payment_mode_chk_type[]')->setAttribute('options', $checkTypeList);
            /* End get check types */

            /* Get check types */
            $checkTypeIds = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCheckIDTypes();
            $checkTypeIdsList = array();
            foreach ($checkTypeIds as $key => $val) {
                $checkTypeIdsList[$val['id_type_id']] = $val['id_type_name'];
            }
            $paymentModeCheck->get('payment_mode_chk_id_type[]')->setAttribute('options', $checkTypeIdsList);
            /* End get check types */
            $createTransactionForm->add($paymentModeCheck);
            $createTransactionForm->add($paymentModeCredit);
            $userArr = array('user_id' => $userId);
            $contactDetail = array();
            $contactDetail[0] = $this->getServiceLocator()->get('User\Model\UserTable')->getUser($userArr);

            /* Code used to get the latest membership of the user */
            $memShipArray = array();
            $memShipArray['user_id'] = $contactDetail[0]['user_id'];
            $memShipArray['start_index'] = 0;
            $memShipArray['record_limit'] = 1;
            $memShipArray['sort_field'] = 'usr_membership.user_membership_id';
            $memShipArray['sort_order'] = 'desc';
            $membershipResult = $this->getServiceLocator()->get('User\Model\MembershipTable')->getUserMemberships($memShipArray);
            /* End code used to get the latest membership of the user */

            $contactAfihcDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAfihc($userArr);
            $contacAddressDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations($userArr);
            $searchArray = array();
            $searchArray['user_id'] = $userId;
            $searchArray['is_primary'] = "1";
            $phoneDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserPhoneInformations($searchArray);

            $countryCode = (!empty($phoneDetail[0]['country_code'])) ? $phoneDetail[0]['country_code'] : "";
            $areaCode = (!empty($phoneDetail[0]['area_code'])) ? $phoneDetail[0]['area_code'] : "";
            $phoneNumber = (!empty($phoneDetail[0]['phone_number'])) ? $phoneDetail[0]['phone_number'] : "";


            $addressDetailArr = array('' => 'Select one');
            $billingLocation = array('' => 'Select one');
            $shippingLocation = array('' => 'Select one');
            $defaultBillingAddress = array();
            $defaultShippingAddress = array();
            if (!empty($contacAddressDetail)) {

                foreach ($contacAddressDetail as $addressDetail) {


                    if (!empty($addressDetail['first_name'])) {
                        $userName = (!empty($addressDetail['title'])) ? stripslashes($addressDetail['title']) . ' ' : '';
                        $userName.= (!empty($addressDetail['first_name'])) ? stripslashes($addressDetail['first_name']) . ' ' : '';
                        // $userName.= (!empty($addressDetail['middle_name'])) ? stripslashes($addressDetail['middle_name']) . ' ' : '';
                        $userName.= (!empty($addressDetail['last_name'])) ? stripslashes($addressDetail['last_name']) : ', ';
                    } else {
                        $userName = (!empty($contactDetail[0]['full_name'])) ? $contactDetail[0]['full_name'] . ", " : $contactDetail[0]['company_name'] . ", ";
                    }


                    $address = $addressDetail['street_address_one'] . " " . $addressDetail['street_address_two'] . " " . $addressDetail['street_address_three'];
                    $address = rtrim(trim($address), ",");
                    $address = (!empty($address)) ? $address . ", " : "";
                    $state = (!empty($addressDetail['state'])) ? $addressDetail['state'] . ", " : "";
                    $city = (!empty($addressDetail['city'])) ? $addressDetail['city'] . ", " : "";
                    $country_name = (!empty($addressDetail['country_name'])) ? $addressDetail['country_name'] . ", " : "";
                    $zip_code = (!empty($addressDetail['zip_code'])) ? $addressDetail['zip_code'] : "";
                    $addressLabel = $userName . ' ' . $address . $state . $city . $country_name . $zip_code;
                    $addressLabel = rtrim(trim($addressLabel), ',');
                    $addressDetailArr[$addressDetail['address_information_id']] = $addressLabel;

                    $locationType = explode(",", $addressDetail['location_name']);
                    if (in_array('2', $locationType)) {
                        $billingLocation[$addressDetail['address_information_id']] = $addressLabel;
                        $addressDetail['address'] = (!empty($address)) ? trim($address, ", ") : "";
                        $billingFlagStatus = "2";
                        if (in_array('1', $locationType)) {
                            $defaultBillingAddress = $addressDetail;
                            $createTransactionForm->get('billing_existing_contact')->setValue($addressDetail['address_information_id']);
                        }
                    }
                    if (in_array('3', $locationType)) {
                        $shippingLocation[$addressDetail['address_information_id']] = $addressLabel;
                        $addressDetail['address'] = (!empty($address)) ? trim($address, ", ") : "";
//$defaultShippingAddress = $addressDetail;
                        if (in_array('1', $locationType)) {
                            $defaultShippingAddress = $addressDetail;
                            $createTransactionForm->get('shipping_existing_contact')->setValue($addressDetail['address_information_id']);
                        }
                    }
                    if (in_array('1', $locationType)) {
                        $addressDetail['address'] = (!empty($address)) ? trim($address, ", ") : "";
//$shippingLocation[$addressDetail['address_information_id']] = $addressLabel;
                        $primaryShippingAddress = $addressDetail;
                    }
                }
            }

// add - n - start
            if ($billingFlagStatus == "1") {
                if (isset($contactDetail[0]['first_name']) and trim($contactDetail[0]['first_name']) != "") {
                    $createTransactionForm->get('billing_first_name')->setValue($contactDetail[0]['first_name']);
                }
                if (isset($contactDetail[0]['last_name']) and trim($contactDetail[0]['last_name']) != "") {
                    $createTransactionForm->get('billing_last_name')->setValue($contactDetail[0]['last_name']);
                }
            }
// add - n - end

            $createTransactionForm->get('billing_existing_contact')->setAttribute('options', $billingLocation);
            $createTransactionForm->get('shipping_existing_contact')->setAttribute('options', $shippingLocation);

            $getTitle = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTitle();
            $user_title = array();
            $userTitle[''] = 'None';
            foreach ($getTitle as $key => $val) {
                $userTitle[$val['title']] = $val['title'];
            }
            $createTransactionForm->get('billing_title')->setAttribute('options', $userTitle);
            $createTransactionForm->get('shipping_title')->setAttribute('options', $userTitle);
            $createTransactionForm->get('billing_title')->setValue($contactDetail[0]['title']);
            $createTransactionForm->get('shipping_title')->setValue($contactDetail[0]['title']);

            $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
            $countryList = array('' => 'Select Country');
            foreach ($country as $key => $val) {
                $countryList[$val['country_id']] = $val['name'];
            }
            $createTransactionForm->get('billing_country')->setAttribute('options', $countryList);
            $createTransactionForm->get('shipping_country')->setAttribute('options', $countryList);

            $state = $this->getServiceLocator()->get('Common\Model\CommonTable')->getStates();
            $stateList = array();
            $stateList[''] = 'Select State';
            foreach ($state as $key => $val) {
                $stateList[$val['state_code']] = $val['state'] . " - " . $val['state_code'];
            }
            $createTransactionForm->get('billing_state_select')->setAttribute('options', $stateList);
            $createTransactionForm->get('shipping_state_select')->setAttribute('options', $stateList);

            if (!empty($defaultBillingAddress)) {
                /*  $createTransactionForm->get('billing_first_name')->setValue($contactDetail[0]['first_name']);
                  $createTransactionForm->get('billing_last_name')->setValue($contactDetail[0]['last_name']);
                  $createTransactionForm->get('billing_company')->setValue($contactDetail[0]['company_name']);
                 */
                //  asd($defaultBillingAddress);

                $createTransactionForm->get('billing_first_name')->setValue($defaultBillingAddress['first_name']);
                $createTransactionForm->get('billing_last_name')->setValue($defaultBillingAddress['last_name']);
                $createTransactionForm->get('billing_company')->setValue($defaultBillingAddress['company_name']);
                $createTransactionForm->get('billing_location_type')->setValue($defaultBillingAddress['location']);

                $createTransactionForm->get('billing_address')->setValue($defaultBillingAddress['address']);
                $createTransactionForm->get('billing_city')->setValue($defaultBillingAddress['city']);
                $createTransactionForm->get('billing_state_text')->setValue($defaultBillingAddress['state']);
                $createTransactionForm->get('billing_state_select')->setValue($defaultBillingAddress['state']);
                $createTransactionForm->get('billing_zip_code')->setValue($defaultBillingAddress['zip_code']);
                $createTransactionForm->get('billing_country')->setValue($defaultBillingAddress['country_id']);
                $createTransactionForm->get('billing_country_code')->setValue($countryCode);
                $createTransactionForm->get('billing_area_code')->setValue($areaCode);
                $createTransactionForm->get('billing_phone')->setValue($phoneNumber);
            }
            $isSameBilling = 0;
//            if (empty($defaultShippingAddress)) {
//                if (!empty($defaultBillingAddress)) {
//                    $defaultShippingAddress = $defaultBillingAddress;
//                    $isSameBilling = 1;
//                }
//            } else if (count($shippingLocation) > 2) {
//                $defaultShippingAddress = array();
//            }
            if (!empty($defaultShippingAddress)) {
                $createTransactionForm->get('billing_shipping_same')->setValue($isSameBilling);
                /*
                  $createTransactionForm->get('shipping_first_name')->setValue($contactDetail[0]['first_name']);
                  $createTransactionForm->get('shipping_last_name')->setValue($contactDetail[0]['last_name']);
                  $createTransactionForm->get('shipping_company')->setValue($contactDetail[0]['company_name']);
                 */
                $createTransactionForm->get('shipping_first_name')->setValue($defaultShippingAddress['first_name']);
                $createTransactionForm->get('shipping_last_name')->setValue($defaultShippingAddress['last_name']);
                $createTransactionForm->get('shipping_company')->setValue($defaultShippingAddress['company_name']);

                $createTransactionForm->get('shipping_address')->setValue(isset($defaultShippingAddress['address']) ? $defaultShippingAddress['address'] : '');
                $createTransactionForm->get('shipping_city')->setValue($defaultShippingAddress['city']);
                $createTransactionForm->get('shipping_state_text')->setValue($defaultShippingAddress['state']);
                $createTransactionForm->get('shipping_state_select')->setValue($defaultShippingAddress['state']);
                $createTransactionForm->get('shipping_zip_code')->setValue($defaultShippingAddress['zip_code']);
                $createTransactionForm->get('shipping_country')->setValue($defaultShippingAddress['country_id']);
                $createTransactionForm->get('shipping_country_code')->setValue($countryCode);
                $createTransactionForm->get('shipping_area_code')->setValue($areaCode);
                $createTransactionForm->get('shipping_phone')->setValue($phoneNumber);
            }


            $viewModel = new ViewModel();
            $createTransactionForm->get('user_id')->setValue($userId);
            $cartProductArr = array();
            $cartProductArr['user_id'] = $userId;
            $cartProductArr['userid'] = $userId;
            $cartProductArr['source_type'] = 'backend';
            $cartProductArr['shipcode'] = '';
            $cartProductArr['shipping_method'] = '';
            $cartProductArr['postData'] = array();
            $cartProducts = $this->cartTotalWithTax($cartProductArr);


//die;

            $selectedShippingMethod = '';
            if (!empty($cartProducts) && is_array($cartProducts['cartData']) && !empty($cartProducts['cartData'])) {
                if (!empty($defaultShippingAddress['country_id']) && $defaultShippingAddress['country_id'] == $uSId) {
                    $shippingMetArr = array();
                    $shippingMetArr['pb_shipping_id'] = $applicationConfigIds['domestic_pb_id'];
                    $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                    $getShippingMethods = $this->_transactionTable->getShippingMethod($shippingMethodArr);
//$getShippingMethods = $this->_config['transaction_config']['domestic_config_info']['shippings_types'];
                } else {
                    $cartProductArr = array();
                    $cartProductArr['user_id'] = $userId;
                    $cartProductArr['userid'] = $userId;
                    $cartProductArr['source_type'] = 'backend';
                    if (isset($cartProducts['totalweight']) && $cartProducts['totalweight'] != '') {
                        $totalWeight = $cartProducts['totalweight'];
                    } else {
                        $totalWeight = 0;
                    }
                    $shippingMetArr = array();
                    $shippingMetArr['pb_shipping_id'] = $applicationConfigIds['internation_pb_id'];
                    $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                    $getShippingMethodsInter = $this->_transactionTable->getShippingMethod($shippingMethodArr);
                    $lbsCheck = $getShippingMethodsInter[0]['lbs_check'];
                    if ($totalWeight <= $lbsCheck) {
                        foreach ($getShippingMethodsInter as $shipMethod) {
                            if ($shipMethod['pb_shipping_type_id'] == $applicationConfigIds['internation_less_than_lbs_id']) {
                                $getShippingMethods[] = $shipMethod;

								$selectedShippingMethod = $shipMethod['carrier_id'] . ',' . $shipMethod['service_type'] . ',' . $shipMethod['pb_shipping_type_id'];
                            }
                        }
                    } else {
                        foreach ($getShippingMethodsInter as $shipMethod) {
                            if ($shipMethod['pb_shipping_type_id'] == $applicationConfigIds['internation_greater_than_lbs_id']) {
                                $getShippingMethods[] = $shipMethod;

								$selectedShippingMethod = $shipMethod['carrier_id'] . ',' . $shipMethod['service_type'] . ',' . $shipMethod['pb_shipping_type_id'];
                            }
                        }
                    }
                }

                $shippingMethods = array();
                if (!empty($getShippingMethods)) {
                    foreach ($getShippingMethods as $shippingMet) {
                        $key = $shippingMet['carrier_id'] . ',' . $shippingMet['service_type'] . ',' . $shippingMet['pb_shipping_type_id'];
                        $webSelection = (!empty($shippingMet['web_selection'])) ? '  ' . $shippingMet['web_selection'] . ' ' : '';
                        $shippingMethods[$key] = $webSelection;
                    }
                }
                $createTransactionForm->get('shipping_method')->setAttribute('options', $shippingMethods);
                $createTransactionForm->get('shipping_method')->setValue($selectedShippingMethod);
                /* $shippingMetArr = array();
                  $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                  $getShippingMethods = $this->_transactionTable->getShippingMethod($shippingMethodArr);

                  //$getShippingMethods = $this->_config['transaction_config']['domestic_config_info']['shippings_types'];
                  $shippingMethods = array();
                  if (!empty($getShippingMethods)) {
                  foreach ($getShippingMethods as $shippingMet) {
                  $key = $shippingMet['carrier_id'] . ',' . $shippingMet['service_type'] . ',' . $shippingMet['pb_shipping_type_id'];
                  $webSelection = (!empty($shippingMet['web_selection'])) ? '  ' . $shippingMet['web_selection'] . ' ' : '';
                  $shippingMethods[$key] = $webSelection;
                  }
                  }
                  $createTransactionForm->get('shipping_method')->setAttribute('options', $shippingMethods); */
            }

            $promotionTable = $this->getServiceLocator()->get('Promotions\Model\PromotionTable');

            //$getAllValidPromotionDetail = $promotionTable->getAllValidPromotions();
            $getAllValidPromotionDetail = array();


            $apoPoStates = $this->_config['transaction_config']['apo_po_state'];
            $createTransactionForm->get('apo_po_state')->setAttribute('options', $apoPoStates);
            $createTransactionForm->get('is_email_confirmation')->setValue(1);
            $viewModel->setVariables(array(
                'jsLangTranslate' => array_merge($createTransactionMessage, $this->_config['transaction_messages']['config']['common_message'], $this->_config['transaction_messages']['config']['update_transaction_batch']),
                'userId' => $userId,
                'encryptUserId' => $params['user_id'],
                'contactDetail' => $contactDetail,
                'contactAfihcDetail' => $contactAfihcDetail,
                'createTransactionForm' => $createTransactionForm,
                'batchForm' => $batchForm,
                'cartProducts' => $cartProducts['cartData'],
                'getAllValidPromotionDetail' => $getAllValidPromotionDetail,
                'applicationConfigIds' => $applicationConfigIds,
                'request_url' => $_SERVER['SERVER_NAME'],
                'defaultShippingAddressInformationId' => $defaultShippingAddress['address_information_id'],
                'membershipResult' => $membershipResult
            ));
            return $viewModel;
        }
    }

    /**
     * This Action is used to get billling shipping address info on base of id
     * @param this will pass an array $dataParam
     * @return this will be json format
     * @author Icreon Tech - DT
     */
    public function getBillingShippingAddressInfoAction() {
        $this->checkUserAuthenticationAjx();
        $this->getTransactionTable();
        $transaction = new Transaction($this->_adapter);
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            if ($request->getPost('address_information_id') != '') {
                $searchArray = array();
                $searchArray['address_information_id'] = $request->getPost('address_information_id');
                $post_user_id = $request->getPost('user_id');
//$searchArray['user_id'] = $request->getPost('user_id');
                if (isset($post_user_id) and trim($post_user_id) != "")
                    $searchArray['user_id'] = trim($post_user_id);
                else if (isset($this->auth->getIdentity()->user_id) && !empty($this->auth->getIdentity()->user_id))
                    $searchArray['user_id'] = $this->auth->getIdentity()->user_id;
                else if (isset($this->auth->getIdentity()->crm_user_id) && !empty($this->auth->getIdentity()->crm_user_id))
                    $searchArray['user_id'] = $this->auth->getIdentity()->crm_user_id;

                $contactDetail = array();
                $contactDetail[0] = $this->getServiceLocator()->get('User\Model\UserTable')->getUser($searchArray);

                $contactDetailArr = array();
                //$contactDetailArr['first_name'] = $contactDetail[0]['first_name'];
                //$contactDetailArr['last_name'] = $contactDetail[0]['last_name'];
                $contactDetailArr['company_name'] = $contactDetail[0]['company_name'];
                $addressDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations($searchArray);

                $contactDetailArr['title'] = $addressDetail[0]['title'];
                $contactDetailArr['first_name'] = $addressDetail[0]['first_name'];
                $contactDetailArr['last_name'] = $addressDetail[0]['last_name'];

                $searchArray['is_primary'] = "1";
                $phoneDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserPhoneInformations($searchArray);
                $fullAddress = $addressDetail[0]['street_address_one'] . " " . $addressDetail[0]['street_address_two'] . " " . $addressDetail[0]['street_address_three'];
                $fullAddress = trim($fullAddress);
                $addressDetail[0]['full_address'] = $fullAddress;
                $countryCode = (!empty($phoneDetail[0]['country_code'])) ? $phoneDetail[0]['country_code'] . "-" : "";
                $areaCode = (!empty($phoneDetail[0]['area_code'])) ? $phoneDetail[0]['area_code'] . "-" : "";
                $phoneNumber = (!empty($phoneDetail[0]['phone_number'])) ? $phoneDetail[0]['phone_number'] : "";
//$phoneNumberLabel = $countryCode . $areaCode . $phoneNumber;
                $phoneNumberLabel = $countryCode . $areaCode . $phoneNumber;
                $phoneNumberLabel = rtrim($phoneNumberLabel, "-");

                if (isset($phoneDetail[0]['is_primary']) and trim($phoneDetail[0]['is_primary']) == "1") {
                    $countryCode2 = (!empty($phoneDetail[0]['country_code'])) ? $phoneDetail[0]['country_code'] : "";
                    $areaCode2 = (!empty($phoneDetail[0]['area_code'])) ? $phoneDetail[0]['area_code'] : "";
                    $phoneNumber2 = (!empty($phoneDetail[0]['phone_number'])) ? $phoneDetail[0]['phone_number'] : "";
                    $phoneContactId2 = (!empty($phoneDetail[0]['phone_contact_id'])) ? $phoneDetail[0]['phone_contact_id'] : "";
                } else {
                    $countryCode2 = "";
                    $areaCode2 = "";
                    $phoneNumber2 = "";
                    $phoneContactId2 = "";
                }

                $phoneDetailArr = array('phone_number' => $phoneNumberLabel, 'country_code' => $countryCode2, 'area_code' => $areaCode2, 'phone_num' => $phoneNumber2, 'phone_contact_id' => $phoneContactId2);
                $searchResult = json_encode(array_merge($addressDetail[0], $contactDetailArr, $phoneDetailArr));
                return $response->setContent($searchResult);
            }
        }
    }

    /**
     * This Action is used to get add product to cart detail action
     * @param this will pass an array $dataParam
     * @return this will be json format
     * @author Icreon Tech - DT
     */
    public function addProductToCartDetailAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $this->layout('popup');
        $createTransactionMessage = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $transaction = new Transaction($this->_adapter);
        $request = $this->getRequest();
        $response = $this->getResponse();
        $addToCartForm = new AddToCartForm();

        /* Get product mapping types */
        $prodMappArr = array('');
        $getProductMappingTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductsMappingType($prodMappArr);
        $productMappingType = array();
        $productMappingType[''] = 'Select';
        if ($getProductMappingTypes !== false) {
            foreach ($getProductMappingTypes as $key => $val) {
                $productMappingType[$val['product_mapping_id']] = $val['product_type_name'];
            }
        }
        $addToCartForm->get('product_mapping_id')->setAttribute('options', $productMappingType);
        /* End get product mapping types */

        $params = $this->params()->fromRoute();
        if (isset($params['user_id']) && $params['user_id'] != '') {
            $userId = $this->decrypt($params['user_id']);
            $addToCartForm->get('user_id')->setValue($params['user_id']);
            $viewModel = new ViewModel();
            $viewModel->setVariables(array(
                'jsLangTranslate' => array_merge($createTransactionMessage, $this->_config['transaction_messages']['config']['common_message']),
                'userId' => $userId,
                'encryptUserId' => $params['user_id'],
                'addToCartForm' => $addToCartForm
            ));
            return $viewModel;
        }
    }

    /**
     * This Action is used to get donation add to cart detail
     * @param Void
     * @return String
     * @author Icreon Tech - DT
     */
    public function getDonationAddToCartDetailAction() {
        $this->checkUserAuthenticationAjx();
        $request = $this->getRequest();
        $response = $this->getResponse();

        $data = array();
        $data[0] = '';
        $data[1] = '';
        $data[3] = 'asc';
        $data[2] = 'tbl_membership.minimun_donation_amount';

        $membershipData = $this->getServiceLocator()->get('User\Model\MembershipTable')->getAllMembership($data);
        $membership = array('' => 'Select');
        foreach ($membershipData as $key => $value) {
            if ($value['membership_id'] != '1' && $value['minimun_donation_amount'] > 0) {
                $membership[$value['minimun_donation_amount']] = $value['membership_title'] . ' - $' . $value['minimun_donation_amount'];
            }
        }
        $membership['other'] = 'Other donation amount';
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $this->getTransactionTable();
            $donationAddToCartForm = new DonationAddToCartForm();
            $donationNotifyForm = new DonationNotifyForm();
            $createTransactionMessage = $this->_config['transaction_messages']['config']['create_crm_transaction'];
            $transaction = new Transaction($this->_adapter);
            $donationAddToCartForm->add($donationNotifyForm);
            $donationAddToCartForm->get('amount')->setAttributes(array('options' => $membership));
            $programArray = $this->getTransactionTable()->getProgramWithCampaign(array());
            $program_list = array();
            $campaign_list = array('' => 'Select');
            if (!empty($programArray)) {
                $i = 0;
                foreach ($programArray as $key => $val) {
                    $program_list[$val['program_id']] = $val['program'];
                }
            }

            $donationAddToCartForm->get('program_id')->setAttributes(array('options' => $program_list));
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => array_merge($createTransactionMessage, $this->_config['transaction_messages']['config']['common_message']),
                'donationAddToCartForm' => $donationAddToCartForm
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to add more notify
     * @param void
     * @return void
     * @author Icreon Tech - DT
     */
    public function addMoreNotifyAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $totalNotify = $request->getPost('totalNotify');
            $this->checkUserAuthenticationAjx();
            $this->getTransactionTable();
            $createTransactionMessage = $this->_config['transaction_messages']['config']['create_crm_transaction'];
            $donationNotifyForm = new DonationNotifyForm();
            $nameId = 'name_' . ($totalNotify + 1);
            $donationNotifyForm->get('name[]')->setAttributes(array('id' => $nameId));
            $emailId = 'email_' . ($totalNotify + 1);
            $donationNotifyForm->get('email[]')->setAttributes(array('id' => $emailId));
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => $createTransactionMessage,
                'donationNotifyForm' => $donationNotifyForm,
                'totalNotify' => $totalNotify
            ));
            return $viewModel;
        } else {
            return $this->redirect()->toRoute('get-crm-transactions');
        }
    }

    /**
     * This Action is used to get kiosk fee add to cart detail
     * @param Void
     * @return String
     * @author Icreon Tech - DT
     */
    public function getKioskFeeAddToCartDetailAction() {
        $this->checkUserAuthenticationAjx();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $productMappingId = $request->getPost('product_mapping_id');
            $prodMappArr = array($productMappingId);
            $getProductMappingTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductsMappingType($prodMappArr);
            $productParam = array();
            $productParam['productType'] = $getProductMappingTypes[0]['product_type_id'];/** product */
            $productResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getProductAttributeDetails($productParam);
            $this->getTransactionTable();
            $kioskFeeForm = new KioskFeeForm();
            $createTransactionMessage = $this->_config['transaction_messages']['config']['create_crm_transaction'];
            $transaction = new Transaction($this->_adapter);
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => array_merge($createTransactionMessage, $this->_config['transaction_messages']['config']['common_message']),
                'kioskFeeForm' => $kioskFeeForm,
                'productResult' => $productResult
            ));
            return $viewModel;
        }
    }

    /**
     * This Action is used to get flag of faces add to cart detail
     * @param Void
     * @return String
     * @author Icreon Tech - DT
     */
    public function getFlagOfFacesAddToCartDetailAction() {
        $this->checkUserAuthenticationAjx();
        $request = $this->getRequest();
        $discount = 0;
        if (defined('MEMBERSHIP_DISCOUNT') && MEMBERSHIP_DISCOUNT != 0) {
            $discount = MEMBERSHIP_DISCOUNT;
        }
        $response = $this->getResponse();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $productMappingId = $request->getPost('product_mapping_id');
            $prodMappArr = array($productMappingId);
            $getProductMappingTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductsMappingType($prodMappArr);
            $productParam = array();
            $productParam['productType'] = $getProductMappingTypes[0]['product_type_id'];/** product */
            $productResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getProductAttributeDetails($productParam);
            $this->getTransactionTable();
            $flagOfFacesForm = new FlagOfFacesForm();
            $createTransactionMessage = $this->_config['transaction_messages']['config']['create_crm_transaction'];
            $transaction = new Transaction($this->_adapter);
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => array_merge($createTransactionMessage, $this->_config['transaction_messages']['config']['common_message']),
                'flagOfFacesForm' => $flagOfFacesForm,
                'productResult' => $productResult,
                'discount' => $discount
            ));
            return $viewModel;
        }
    }

    /**
     * This Action is used to get inventory add to cart data
     * @param Void
     * @return String
     * @author Icreon Tech - DT
     */
    public function getInventoryAddToCartDetailAction() {
        $this->checkUserAuthenticationAjx();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $productMappingId = $request->getPost('product_mapping_id');
            $this->getTransactionTable();
            $createTransactionMessage = $this->_config['transaction_messages']['config']['create_crm_transaction'];
            $prodMappArr = array($productMappingId);
            $getProductMappingTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductsMappingType($prodMappArr);
            $sm = $this->getServiceLocator();
            $categoryData = array();
            $categoryData[] = $getProductMappingTypes[0]['product_type_id'];

            /**
             * comment to enable the Custom frame for Inventory Products
             * done by saurabh
             * date : 11 October,2014
             * */
            /* if (isset($getProductMappingTypes[0]['product_type_id']) and trim($getProductMappingTypes[0]['product_type_id']) == "1") {
              $categoryData['restricted_category_id'] = $this->_config['custom_frame_id'];
              } else {
              $categoryData['restricted_category_id'] = "";
              } */

            // end of commenting

            $categories = $sm->get('Category\Model\CategoryTable')->getCategoryByProductTypeId($categoryData);
//$productCatController=new ProductController();
//$productCatController->getSelectArr($categories);
            $categoryDetail = array('' => 'Select');
            if (!empty($categories)) {
                foreach ($categories as $category) {
                    $categoryDetail[$category['category_id']] = trim(trim($category['category_name'], '-'), ' ');
                }
            }
            $inventoryForm = new InventoryForm();
            $inventoryForm->get('category_id')->setAttribute('options', $categoryDetail);
//$inventoryForm->get('product_type_id')->setValue($getProductMappingTypes[0]['product_type_id']);

            $productParam = array();
            $productParam['productType'] = $getProductMappingTypes[0]['product_type_id'];/** product */
            $productResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getProductAttributeDetails($productParam);


            $transaction = new Transaction($this->_adapter);
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => array_merge($createTransactionMessage, $this->_config['transaction_messages']['config']['common_message']),
                'inventoryForm' => $inventoryForm
            ));
            return $viewModel;
        }
    }

    /**
     * This Action is used to get membership add to cart detail
     * @param Void
     * @return String
     * @author Icreon Tech - DT
     */
    public function getMembershipAddToCartDetailAction() {
        $this->checkUserAuthenticationAjx();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $this->getTransactionTable();
            $createTransactionMessage = $this->_config['transaction_messages']['config']['create_crm_transaction'];
            $transaction = new Transaction($this->_adapter);
            $membershipDetailForm = new MembershipDetailForm();
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $membershipsData = $this->getServiceLocator()->get('Common\Model\CommonTable')->getMembership();
            $viewModel->setVariables(array(
                'jsLangTranslate' => array_merge($createTransactionMessage, $this->_config['transaction_messages']['config']['common_message']),
                'membershipDetailForm' => $membershipDetailForm,
                'membershipsData' => $membershipsData
            ));
            return $viewModel;
        }
    }

    /**
     * This Action is used to get wall of honor add to cart detail
     * @param Void
     * @return String
     * @author Icreon Tech - DT
     */
    public function getWallOfHonorAddToCartDetailAction() {
        $this->checkUserAuthenticationAjx();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $this->getTransactionTable();
            $createTransactionMessage = $this->_config['transaction_messages']['config']['create_crm_transaction'];
            $transaction = new Transaction($this->_adapter);
            $wallOfHonor = new WallOfHonor();
            $wallOfHonorAdditionalContact = new WallOfHonorAdditionalContact();
            $wallOfHonor->add($wallOfHonorAdditionalContact);
            $sm = $this->getServiceLocator();
            $productMappingId = $request->getPost('product_mapping_id');
            $prodMappArr = array($productMappingId);
            $getProductMappingTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductsMappingType($prodMappArr);
            $productParam = array();
            $productParam['productType'] = $getProductMappingTypes[0]['product_type_id'];/** product */
            $productResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getProductAttributeDetails($productParam);
            /* $attibuteOptionsArr = array();
              $totalOptions = count($productResult);
              for ($i = $totalOptions - 1; $i >= 0; $i--) {
              $product = $productResult[$i];
              if (!isset($attibuteOptionsArr[$product['attribute_id']])) {
              $attributeArr = array();
              $attributeArr['attribute_id'] = $product['attribute_id'];
              $attributeArr['attribute_name'] = $product['attribute_name'];
              $attributeArr['attribute_label'] = $product['attribute_label'];
              $attributeArr['attribute_help_text'] = $product['attribute_help_text'];
              $attributeArr['attribute_mandatory'] = $product['attribute_mandatory'];
              $attributeArr['attribute_display_type'] = $product['attribute_display_type'];
              $attributeArr['num_options'] = $product['num_options'];
              $attributeArr['options'] = array();
              $attibuteOptionsArr[$product['attribute_id']] = $attributeArr;
              }
              $optionArr = array();
              $optionArr['attribute_option_id'] = $product['attribute_option_id'];
              $optionArr['option_name'] = $product['option_name'];
              $optionArr['option_cost'] = $product['option_cost'];
              $optionArr['option_price'] = $product['option_price'];
              $attibuteOptionsArr[$product['attribute_id']]['options'][] = $optionArr;
              }
             */
            $productOptionsArr = array();
            $totalOptions = count($productResult);
            $discount = 0;
            if (defined('MEMBERSHIP_DISCOUNT') && MEMBERSHIP_DISCOUNT != 0) {
                $discount = MEMBERSHIP_DISCOUNT;
            }
            for ($i = $totalOptions - 1; $i >= 0; $i--) {
                $product = $productResult[$i];
// if($product['attribute_option_id']!='' && !isset($productOptionsArr[$product['product_id']]))

                if (($product['additional_product_1'] != 0 || $product['additional_product_2'] != 0 || $product['additional_product_3'] != 0) && !isset($productOptionsArr[$product['product_id']])) {
                    $productOptionInfo = array();
                    $productOptionInfo['product_id'] = $product['product_id'];
                    $productOptionInfo['product_sku'] = $product['product_sku'];
                    $productOptionInfo['product_type_id'] = $product['product_type_id'];
                    $productOptionInfo['product_list_price'] = $product['product_list_price'];
                    $productOptionInfo['product_cost'] = $product['product_cost'];
                    $productOptionInfo['product_sell_price'] = $product['product_sell_price'];
                    $productOptionInfo['product_name'] = $product['product_name'];
                    $productOptionInfo['attribute_id'] = $product['attribute_id'];
                    $productOptionInfo['is_membership_discount'] = $product['is_membership_discount'];
                    $productOptionInfo['options'] = array();
                    $productOptionsArr[$product['product_id']] = $productOptionInfo;
                }
                if ($product['attribute_option_id'] != '') {
                    $optionArr = array();
                    $optionArr['attribute_option_id'] = $product['attribute_option_id'];
                    $optionArr['option_name'] = $product['option_name'];
                    $optionArr['additional_option_cost'] = $product['additional_option_cost'];
                    $optionArr['additional_option_price'] = $product['additional_option_price'];
                    $optionArr['is_membership_discount'] = $product['is_membership_discount'];
                    $optionArr['option_cost'] = $product['option_cost'];
                    $optionArr['option_price'] = $product['option_price'];
                    $productOptionsArr[$product['product_id']]['options'][] = $optionArr;
                }
            }

            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => array_merge($createTransactionMessage, $this->_config['transaction_messages']['config']['common_message']),
                'wallOfHonor' => $wallOfHonor,
                'attibuteOptionsArr' => $productOptionsArr,
                'discount' => $discount
            ));
            return $viewModel;
        }
    }

    /**
     * This Action is used to get wall of honor bio certificate search result
     * @param Void
     * @return String
     * @author Icreon Tech - DT
     */
    function getWallOfHonorBioCertificateSearchAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $createTransactionMessage = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $commonTransactionMessage = $this->_config['transaction_messages']['config']['common_message'];
        $transaction = new Transaction($this->_adapter);
        $request = $this->getRequest();
        $sord = $request->getPost('sord');
        if ($request->isXmlHttpRequest() && $request->isPost() && !empty($sord)) {
            $response = $this->getResponse();
            $postArray = $request->getPost()->toArray();
            parse_str($request->getPost('searchString'), $searchParam);
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit; // do not put $limit*($page - 1)
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'tbl_usr_woh.modified_date') ? 'tbl_usr_woh.modified_date' : $searchParam['sort_field'];
            $searchType = $request->getPost('search_type');
            if (trim($searchParam['panel_no']) != '' || trim($searchParam['transaction_id']) != '' || trim($searchParam['name'])) {
                $searchBioCertArr = $transaction->getWOHBioCertSearchArr($searchParam);
                $seachResult = $this->_transactionTable->getAllUserWallOfHonors($searchBioCertArr);
                $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            } else {
                $seachResult = array();
                $countResult = 0;
            }
            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $jsonResult['page'] = $request->getPost('page');
            $jsonResult['records'] = ($countResult == 0) ? $countResult : $countResult[0]->RecordCount;
            $jsonResult['total'] = ($countResult == 0) ? $countResult : ceil($countResult[0]->RecordCount / $limit);

            if (!empty($seachResult)) {
                foreach ($seachResult as $val) {
                    $arrCell['id'] = $val['woh_id'];
                    $radioButton = "<input type='radio' name='transaction[]' id='woh_" . $val['woh_id'] . "'> ";
                    if ($searchParam['search_type'] == 'bio_certificate') {
                        $productOptionName = "<a class='link m-l-25' href='javascript:void(0);' onclick='addBioCertificateToCart(\"" . $val['bio_certificate_product_id'] . "\")'>" .
                                $val['product_name'] . " : " . $val['option_name'] . "</a>";
                    } else {
                        $productOptionName = $val['product_name'] . " : " . $val['option_name'] . "<br>Honoree Name : " . $val['final_name'];
                    }
                    $arrCell['cell'] = array($radioButton, $val['transaction_id'], $val['panel_no'], $productOptionName);
                    $subArrCell[] = $arrCell;
                    $jsonResult['rows'] = $subArrCell;
                }
            } else {
                $jsonResult['rows'] = array();
            }
            return $response->setContent(\Zend\Json\Json::encode($jsonResult));
        } else {
            $userId = $this->decrypt($request->getPost('user_id'));
            $name = $request->getPost('name');
            $transactionId = $request->getPost('transaction_id');
            $panelNo = $request->getPost('panel_no');
            $searchType = $request->getPost('search_type');
            $wallOfHonor = new WallOfHonor();
            $viewModel = new ViewModel();
            $response = $this->getResponse();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'userId' => $userId,
                'name' => $name,
                'transactionId' => $transactionId,
                'panelNo' => $panelNo,
                'searchType' => $searchType,
                'wallOfHonor' => $wallOfHonor,
                'jsLangTranslate' => array_merge($createTransactionMessage, $commonTransactionMessage),
            ));
            return $viewModel;
        }
    }

    /**
     * This Action is used to show default search passenger records
     * @return this will return the crm search passengers form parameters
     * @author Icreon Tech -DT
     */
    public function getPassengerDocumentAddToCartDetailAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $request = $this->getRequest();
        $createTransactionMessage = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $search_passenger_form = new CrmSearchPassengerForm();
        $getSearchArray = array();
        $ethnicityArray = $this->_documentTable->getEthnicity($getSearchArray);
        $ethnicityList = array();
        $ethnicityList[''] = 'Select';
        foreach ($ethnicityArray as $key => $val) {
            $ethnicityList[$val['ethnicity']] = $val['ethnicity'];
        }
        $search_passenger_form->get('ethnicity')->setAttribute('options', $ethnicityList);


        $arrivalYear = array();
        $arrivalYear[''] = 'Select';
        $arrivalYear['1891 - 1895'] = '1891 - 1895';
        for ($k = 1896; $k < 1980;
        ) {
            $p = $k + 4;
            $arrivalYear[$k . ' - ' . $p] = $k . ' - ' . $p;
            $k = $k + 5;
        }
        $search_passenger_form->get('year_of_arrival')->setAttribute('options', $arrivalYear);

        $day = array();
        $day[''] = 'All';
        for ($i = 1; $i <= 31; $i++) {
            $day[$i] = $i;
        }
        $search_passenger_form->get('day_of_arrival')->setAttribute('options', $day);

        $getSearchArray = array();
        $portArray = $this->_documentTable->getArrivalPort($getSearchArray);
        $portList = array();
        $portList[''] = 'Select';
        foreach ($portArray as $key => $val) {
            $portList[$val['ship_arrival_port']] = $val['ship_arrival_port'];
        }
        $search_passenger_form->get('arrival_port')->setAttribute('options', $portList);


        $userId = $this->decrypt($request->getPost('user_id'));
        $passengerSavedSearch = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getUserPassengerSearch(array('user_id' => $userId));
        $isSavedPassenger = 1;
        if (empty($passengerSavedSearch)) {
            $isSavedPassenger = 0;
        }

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $response = $this->getResponse();
        $messages = array();

        $viewModel->setVariables(array(
            'search_passeneger_form' => $search_passenger_form,
            'isSavedPassenger' => $isSavedPassenger,
            'jsLangTranslate' => array_merge($createTransactionMessage, $this->_config['transaction_messages']['config']['common_message'])
        ));
        return $viewModel;
    }

    /**
     * This Action is used for the search of Document crm search passenger
     * @return this will be searched passengers data
     * @author Icreon Tech -DT
     */
    public function getAddToCartCrmSearchPassengerAction() {

        $passengerControllerObj = new PassengerController();

        $this->getTransactionTable();
        $response = $this->getResponse();
        $request = $this->getRequest();
        parse_str($request->getPost('searchString'), $searchParam);

        if (isset($searchParam['search_type']) && $searchParam['search_type'] != '') {
            $searchType = $searchParam['search_type'];
        } else {
            $searchType = 'search_type5';
        }
        $passIdsArr = array();
        $isSavePassengerRecord = 0;
        if (!empty($searchParam['saved_passenger_search'])) {
            $isSavePassengerRecord = 1;
            $userId = $this->decrypt($searchParam['user_id']);
            $passengerSavedSearch = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getUserPassengerSearch(array('user_id' => $userId));
            if (!empty($passengerSavedSearch)) {
                foreach ($passengerSavedSearch as $passenger) {
                    $passIdsArr[] = $passenger['passenger_id'];
                }
            }
        }

        if ($request->getPost('page') == '') {
            $page = 1;
        } else {
            $page = $request->getPost('page');
        }

        $this->_config['pageSource'] = 'transaction';
        $searchResult = $passengerControllerObj->getsphinxSearchPassenger($request, $searchType, $page, $this->_config, $passIdsArr, $isSavePassengerRecord);

        $limit = ($request->getPost('rows') != 0) ? $request->getPost('rows') : '1';
        $page = $request->getPost('page');
        $jsonResult['page'] = $page;
        $jsonResult['records'] = $searchResult['total'];
        $jsonResult['total'] = ceil($searchResult['total'] / $limit);
        $valueArray = array();

        if ($searchResult['total'] > 0) {
            foreach ($searchResult['matches'] as $key => $val) {
                $valueArray = $val['attrs'];
                $arrCell['id'] = $valueArray['field0'];
                $gender = '';
                if ($valueArray['field10'] == 'M')
                    $gender = 'Male';
                else if ($valueArray['field10'] == 'F')
                    $gender = 'Female';
                else if ($valueArray['field10'] == 'U')
                    $gender = 'Unknown';

                $field0 = '<a href="javascript:void(0);" class="link txt-decoration-underline" onclick="getPassengerRecordInfo(\'' . $this->encrypt($valueArray['field0']) . '\')">' . $valueArray['field0'] . '</a>';

                $passPlace = '';
                if (isset($valueArray['field28'])) {
                    if ($valueArray['field28'] == 2)
                        $passPlace = $valueArray['field26'];
                    else
                        $passPlace = $valueArray['field4'];
                }
                $arrCell['cell'] = array($field0, $valueArray['field3'], $valueArray['field2'], $gender, $valueArray['field14'], $valueArray['field36'], $valueArray['field7'], $this->Dateformat($valueArray['field18'], 'displayformat'), $passPlace);
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
        }

        return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    /**
     * This Action is used for get passenger record info on base of passenger id
     * @return string
     * @author Icreon Tech -DT
     */
    public function getPassengerRecordInfoAction() {
        $this->getTransactionTable();
        $createTransactionMessage = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $commonMessage = $this->_config['transaction_messages']['config']['common_message'];
        $request = $this->getRequest();
        $passengerId = $this->decrypt($request->getPost('passengerId'));
        $userId = $this->decrypt($request->getPost('user_id'));
        $searchParam['passenger_id'] = $passengerId;
        $searchResult = array();
        $sm = $this->getServiceLocator();
        $passengerTable = $sm->get('Passenger\Model\PassengerTable');
        $searchResult = $passengerTable->getPassengerRecord($searchParam);
        $discount = 0;
        if (defined('MEMBERSHIP_DISCOUNT') && MEMBERSHIP_DISCOUNT != 0) {
            $discount = MEMBERSHIP_DISCOUNT;
        }

        $passParam = array();
        $passParam['passengerId'] = $this->decrypt($request->getPost('passengerId'));
        $passengerEthnicity = $passengerTable->getPassengerEthnicity($passParam);

        $productParam = array();
        $productParam['productType'] = '5';/** product */
        $productResult = array();
        $productResult = $passengerTable->getProductAttributeDetails($productParam);

        $passengerRecordOptions = array();
        if (!empty($productResult)) {
            foreach ($productResult as $passengerRecord) {
                if (!empty($passengerRecord['product_attribute_option_id'])) {
                    $passengerRecordOptions[] = $passengerRecord;
                }
            }
        }
        $productResult = $passengerRecordOptions;
        if (!empty($searchResult[0]['SHIP_NAME'])) {
            $searchShipParam = array();
            $searchShipParam['ship_name'] = $searchResult[0]['SHIP_NAME'];
            $searchShipParam['asset_for'] = '1';
            $shipResult = $passengerTable->getShipDetails($searchShipParam);
            if (!empty($shipResult)) {
                if (count($shipResult[0]) > 0)
                    $shipResult = $shipResult[0];
            }
        }
        $shipPath = $this->_config['Image_path']['shipPath'];

        $productParam['productType'] = 8;/** Ship Image */
        $productParam['isOptions'] = 'yes';/** product */
        $productOptionResult = array();
        $productOptionResult = $passengerTable->getProductAttributeDetails($productParam);
        $shipId = (!empty($shipResult['SHIPID'])) ? $shipResult['SHIPID'] : '';
        $passengerDocumentForm = new PassengerDocumentForm();
        $passengerDocumentForm->get('passenger_id')->setValue($searchResult[0]['ID']);
        $passengerDocumentForm->get('product_id')->setValue($productResult[0]['product_id']);
        $passengerDocumentForm->get('ship_id')->setValue($shipId);
//$passengerDocumentForm->get('passenger_record_options')->setAttribute('options',$passengerRecordOptions);
        /* ship menifest record */
        $manifestPath = $this->_config['Image_path']['manifestPath'];
        $manifestParam['shipName'] = $searchResult[0]['SHIP_NAME'];
        $manifestParam['dateArrive'] = substr($searchResult[0]['DATE_ARRIVE'], 0, 10);

        if ($searchResult[0]['data_source'] == 1) {
            $shipManifestResult = $passengerTable->getShipManifest($manifestParam);
        } else if ($searchResult[0]['data_source'] == 2) {
            $manifestParam['dataSource'] = 2;

            $dateArray = date_parse_from_format("Y-m-d", $searchResult[0]['DATE_ARRIVE']);
            $manifestParam['dateArriveFrom'] = $dateArray['year'] . '-01-01';
            $manifestParam['dateArriveTo'] = $dateArray['year'] . '-12-31';

            $manifestFileNameArray = explode('_', $searchResult[0]['FILENAME']);
            $manifestParam['fileNamePattern'] = $manifestFileNameArray[0];

            $shipManifestResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getFamilyShipManifest($manifestParam);
        }


        $shipManifestArray = array();
        $shipManifestFilenameArray = array();
        if (count($shipManifestResult) > 0 && !isset($shipManifestResult[0]['Message'])) {
            if ($searchResult[0]['data_source'] == 1) {
                $startFrame = $shipManifestResult[0]['START_RANGE'];
                $endFrame = $shipManifestResult[0]['END_RANGE'];
                $rollNbr = $shipManifestResult[0]['ROLL_NBR'];
                for ($k = $startFrame; $k <= $endFrame; $k++) {
                    if ($shipManifestResult[0]['data_source'] == 2) {
                        $shipManifestFilenameArray[] = $shipManifestResult[0]['FILENAME'];
                        $fileName = $shipManifestResult[0]['FILENAME'];
                        $shipManifestArray[] = $shipManifestResult[0]['FILENAME'];
                    } else {
                        $fileName = $rollNbr . sprintf('%04d', $k) . '.TIF';
                        $folderName = substr($fileName, 0, 9);
                        $shipManifestArray[] = $fileName;
                        $shipManifestFilenameArray[] = strtoupper($fileName);
                    }
                }
            } else if ($searchResult[0]['data_source'] == 2) {
                if (count($shipManifestResult) > 0 && !isset($shipManifestResult[0]['Message'])) {
                    $shipManifestArray = array();

                    $startFrame = $shipManifestResult[0]['START_RANGE'];
                    $endFrame = $shipManifestResult[0]['END_RANGE'];
                    $shipManifestArray = array();
                    $rollNbr = $shipManifestResult[0]['ROLL_NBR'];
                    for ($k = $startFrame; $k <= $endFrame; $k++) {
                        $fileName = substr($rollNbr, 5) . '_' . sprintf('%05d', $k) . '.jpg';
                        $folderName = substr($fileName, 0, 9);
                        $shipManifestArray[] = $fileName;
                        $shipManifestFilenameArray[] = $fileName;
                    }

                    /* foreach ($shipManifestResult as $key => $val) {
                      $shipManifestArray[] = $val['FILENAME'];
                      $shipManifestFilenameArray[] = $val['FILENAME'];
                      } */
                }
            }
        }

        $defaultImageIndex = 0;
        $manifestResult = array();
        $manifestResult = $passengerTable->getPassengerManifest($searchParam);

        if (count($manifestResult) > 0) {

            if ($searchResult[0]['data_source'] == 2) {
                $manifest_file_name = $manifestResult[0]['FILENAME'];
                $fileName = $manifest_file_name;
                $shipManifestFilenameArray[] = $manifestResult[0]['FILENAME'];
                $shipManifestFilenameArray[] = $fileName;
            } else {
                $manifest_file_name = strtoupper($manifestResult[0]['FILENAME']);
                $fileName = $manifest_file_name;
                $folderName = substr($fileName, 0, 9);
                $shipManifestFilenameArray[] = $fileName;
            }

            $shipManifestFilenameArray = array_unique($shipManifestFilenameArray);
            $defaultImageIndex = array_search($manifest_file_name, $shipManifestFilenameArray);
        } else {
            if (!empty($manifestFile)) { {
                    if (strtolower(substr($manifestFile, -3, 3)) == 'jpg') {
                        $shipManifestResult[0]['FILENAME'] = $manifestFile;
                        $shipManifestResult[0]['FRAME'] = ltrim(substr($manifestFile, -9, 5), 0);
                        $shipManifestResult[0]['ROLL_NBR'] = substr($manifestFile, 1, 9);
                        $shipManifestResult[0]['data_source'] = 2;

                        $shipManifestFilenameArray[] = $manifestFile;
                        $shipManifestArray[] = $assets_url . 'manifest' . $manifestFile;
                        $shipManifestArray = array_unique($shipManifestArray);
                        $shipManifestFilenameArray = array_unique($shipManifestFilenameArray);
                    }
                }
            }

            $manifestResult = $shipManifestResult;
            $defaultImageIndex = 0;
        }

        if (count($shipManifestArray) == 1) {
            if ($manifestResult[0]['data_source'] == 2) {
                $sFileName = substr($manifestResult[0]['ROLL_NBR'], 5) . '_' . sprintf('%05d', $manifestResult[0]['FRAME'] + 1) . '.jpg';
                $shipManifestFilenameArray[] = $sFileName;
                $shipManifestArray[] = $sFileName;
            } else {
                $shipManifestArray[] = $manifestResult[0]['ROLL_NBR'] . sprintf('%04d', $manifestResult[0]['FRAME'] + 1) . ".TIF&S=.4";
                $shipManifestFilenameArray[] = $manifestResult[0]['ROLL_NBR'] . sprintf('%04d', $manifestResult[0]['FRAME'] + 1) . '.TIF';
            }
        }

        $newshipManifestFilenameArray = array();
        foreach ($shipManifestFilenameArray as $key => $val) {
            $newshipManifestFilenameArray[] = $val;
        }

        $productMenifestParam = array();
        $productMenifestParam['productType'] = '7';/** manifest */
        $productMenifestResult = $passengerTable->getProductAttributeDetails($productMenifestParam);
        /* end ship menifest record */


        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $encryptedShipId = (!empty($shipId)) ? $this->encrypt($shipId) : '';
        $viewModel->setVariables(array(
            'searchResult' => $searchResult[0],
            'jsLangTranslate' => array_merge($createTransactionMessage, $commonMessage),
            'productResult' => $productResult,
            'passengerEthnicity' => @$passengerEthnicity[0],
            'maritalStatusArray' => $this->maritalStatusArray(),
            'genderArray' => $this->genderArray(),
            'passengerId' => $request->getPost('passengerId'),
            'shipResult' => $shipResult,
            'shipPath' => $shipPath,
            'productOptionResult' => $productOptionResult,
            'shipId' => $encryptedShipId,
            'passengerDocumentForm' => $passengerDocumentForm,
            'manifestResult' => $manifestResult,
            'manifestPath' => $manifestPath,
            'productMenifestResult' => $productMenifestResult,
            'shipManifestArray' => $shipManifestArray,
            'shipManifestFilenameArray' => $newshipManifestFilenameArray,
            'defaultImageIndex' => $defaultImageIndex,
            'uploadFilePath' => $this->_config['file_upload_path']['assets_upload_dir'],
            'discount' => $discount
        ));
        return $viewModel;
    }

    /**
     * This action is used to generate wall of honor on basis of naming convention
     * @param void
     * @return void
     * @author Icreon Tech - DT
     */
    public function generateWohFormByNamingConventionAction() {
        $this->checkUserAuthenticationAjx();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $this->getTransactionTable();
            $createTransactionMessage = $this->_config['transaction_messages']['config']['create_crm_transaction'];
            $wallOfHonor = new WallOfHonor();
            $wallOfHonorAdditionalContact = new WallOfHonorAdditionalContact();
            $wallOfHonor->add($wallOfHonorAdditionalContact);
            $attributeId = $request->getPost('attribute_id');
            $optionId = $request->getPost('option_id');
            $optionName = $request->getPost('option_name');
            $attributeName = $request->getPost('attribute_type');
            $formatName = $request->getPost('format_name');
            $wallOfHonor->get('product_attribute_option_id')->setValue($optionId);
            $namingConventionArr = $this->getNamingConventionArr($optionName, $attributeName);
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => $createTransactionMessage,
                'wallOfHonor' => $wallOfHonor,
                'attributeId' => $attributeId,
                'optionId' => $optionId,
                'optionName' => $optionName,
                'formatName' => $formatName,
                'namingConventionArr' => $namingConventionArr
            ));
            return $viewModel;
        } else {
            return $this->redirect()->toRoute('get-crm-transactions');
        }
    }

    /**
     * This action is used to get wall of honor other products block
     * @param void
     * @return void
     * @author Icreon Tech - DT
     */
    public function getWallOfHonorOtherProductsAction() {
        $this->checkUserAuthenticationAjx();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $discount = 0;
        if (defined('MEMBERSHIP_DISCOUNT') && MEMBERSHIP_DISCOUNT != 0) {
            $discount = MEMBERSHIP_DISCOUNT;
        }
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $this->getTransactionTable();
            $createTransactionMessage = $this->_config['transaction_messages']['config']['create_crm_transaction'];
            $commonTransactionMessage = $this->_config['transaction_messages']['config']['common_message'];
            $wallOfHonor = new WallOfHonor();
            $productId = $request->getPost('productId');
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $searchParam['id'] = $productId;
            $productResult = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProductInfo($searchParam);

            $duplicateRelatedPersonalizeArr = array();
            if (!empty($productResult)) {
                $additionalProductStr = '';
                $additionalProductStr.= ( $productResult[0]['additional_product_1'] != 0) ? $productResult[0]['additional_product_1'] . "," : '';
                $additionalProductStr.= ( $productResult[0]['additional_product_2'] != 0) ? $productResult[0]['additional_product_2'] . "," : '';
                $additionalProductStr.= ( $productResult[0]['additional_product_3'] != 0) ? $productResult[0]['additional_product_3'] : '';
                $additionalProductStr = rtrim($additionalProductStr, ',');
                $searchParam = array();
                $searchParam['productIds'] = $additionalProductStr;
                $duplicateCertificateResult = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProductInfo($searchParam);
                if (!empty($duplicateCertificateResult)) {
                    foreach ($duplicateCertificateResult as $duplicate) {
                        $duplicateRelatedPersonalizeArr[$duplicate['product_id']] = $duplicate;
                    }
                }
            }
//Check whether already exist woh for user or not
            $transaction = new Transaction($this->_adapter);
            $searchParam = array();
            $searchParam['user_id'] = $this->decrypt($request->getPost('userId'));
            $searchBioCertArr = $transaction->getWOHBioCertSearchArr($searchParam);
            $userWallOfHonorRecords = array(); // $this->_transactionTable->getAllUserWallOfHonors($searchBioCertArr);
//Check whether already exist woh for user or not

            $wallOfHonor->get('product_id')->setValue($productId);
            $wallOfHonor->get('otherproductaddtocart')->setAttributes(array('style' => 'display:none'));
            $viewModel->setVariables(array(
                'jsLangTranslate' => array_merge($createTransactionMessage, $commonTransactionMessage),
                'wallOfHonor' => $wallOfHonor,
                'productId' => $productId,
                'productResult' => $productResult[0],
                'duplicateRelatedPersonalizeArr' => $duplicateRelatedPersonalizeArr,
                'userWallOfHonorRecords' => $userWallOfHonorRecords,
                'discount' => $discount
            ));
            return $viewModel;
        } else {
            return $this->redirect()->toRoute('get-crm-transactions');
        }
    }

    /**
     * This action is used to add more additional contact
     * @param void
     * @return void
     * @author Icreon Tech - DT
     */
    public function addAdditionalContactAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $totalSaveSearch = $request->getPost('totalSaveSearch');
            $this->checkUserAuthenticationAjx();
            $this->getTransactionTable();
            $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
            $wallOfHonorAdditionalContact = new WallOfHonorAdditionalContact();
            $firstNameId = 'first_name_' . ($totalSaveSearch + 1);
            $lastNameId = 'last_name_' . ($totalSaveSearch + 1);
            $emailId = 'email_id_' . ($totalSaveSearch + 1);
            $wallOfHonorAdditionalContact->get('first_name[]')->setAttributes(array('id' => $firstNameId));
            $wallOfHonorAdditionalContact->get('last_name[]')->setAttributes(array('id' => $lastNameId));
            $wallOfHonorAdditionalContact->get('email_id[]')->setAttributes(array('id' => $emailId));
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => $msgArr,
                'wallOfHonorAdditionalContact' => $wallOfHonorAdditionalContact,
                'totalSaveSearch' => $totalSaveSearch
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to add donation to cart
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function addToCartDonationAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $transaction = new Transaction($this->_adapter);

        $donationAddToCartForm = new DonationAddToCartForm();
        $donationNotifyForm = new DonationNotifyForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $donationAddToCartForm->setData($request->getPost());
            $validationGroups = array();
            $donationAmount = $request->getPost('amount');
            $validationGroups[] = ($donationAmount == 'other') ? 'other_amount' : 'amount';

            $donationAddToCartForm->setValidationGroup($validationGroups);
            $donationAddToCartForm->setInputFilter($transaction->getInputFilterAddToCartDonation());

            if (!$donationAddToCartForm->isValid()) {
                $errors = $donationAddToCartForm->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'continue') {
                        foreach ($row as $typeError => $rower) {
                            if ($typeError != 'notInArray') {
                                $msg [$key] = $msgArr[$rower];
                            }
                        }
                    }
                }
            }
            if (!empty($msg)) {
                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $postArr = $request->getPost()->toArray();
                $donationAddToCartFormArr = $postArr;
                $prodMappArr = array($donationAddToCartFormArr['product_mapping_id']);
                $getProductMappingTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductsMappingType($prodMappArr);

                $productParam = array();
                $productParam['productType'] = $getProductMappingTypes[0]['product_type_id'];/** product */
                $productResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getProductAttributeDetails($productParam);
                $donationAddToCartFormArr['product_type_id'] = $getProductMappingTypes[0]['product_type_id'];
                $donationAddToCartFormArr['product_id'] = $productResult[0]['product_id'];
                $donationAddToCartFormArr['user_id'] = $this->decrypt($postArr['user_id']);
                $donationAddToCartFormArr['user_session_id'] = session_id();
                $donationAddToCartFormArr['added_by'] = $this->auth->getIdentity()->crm_user_id;
                $donationAddToCartFormArr['added_date'] = DATE_TIME_FORMAT;
                $donationAddToCartFormArr['modify_by'] = $this->auth->getIdentity()->crm_user_id;
                $donationAddToCartFormArr['modify_date'] = DATE_TIME_FORMAT;
                $donationAddToCartFormArr['amount'] = ($donationAddToCartFormArr['amount'] == 'other') ? $donationAddToCartFormArr['other_amount'] : $donationAddToCartFormArr['amount'];
                /* add campaign,promotion and user group id in cart start */
                $promotionId = "";
                $couponCode = "";
                $user_group_id = "";

                $this->campaignSession = new Container('campaign');
                $campaign_code = (isset($this->campaignSession->campaign_code) && !empty($this->campaignSession->campaign_code) ? $this->campaignSession->campaign_code : "");

                $campaign_id = isset($this->campaignSession->campaign_id) && !empty($this->campaignSession->campaign_id) ? $this->campaignSession->campaign_id : 0;
                $channel_id = (isset($this->campaignSession->channel_id) && !empty($this->campaignSession->channel_id) ? $this->campaignSession->channel_id : 0);

                if ($campaign_id != 0 && $channel_id != 0) {
                    $campResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getChannelPromo(array('campaign_id' => $campaign_id, 'channel_id' => $channel_id));

                    if (isset($campResult[0]['promotion_id']) && !empty($campResult[0]['promotion_id'])) {
                        $couponCode = $campResult[0]['coupon_code'];
                        $promotionId = $campResult[0]['promotion_id'];
                    } else {
                        $couponCode = "";
                        $promotionId = 0;
                    }

                    $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '';
                    if (isset($user_id) && $user_id != '') {
                        $searchParam['user_id'] = $user_id;
                        $searchParam['campaign_id'] = $campaign_id;
                        $cmTable = $this->getServiceLocator()->get('Campaign\Model\CampaignTable');
                        $campaign_arr = $cmTable->getUserCampaigns($searchParam);
                        if ($campaign_arr && isset($campaign_arr[0]['group_id'])) {
                            $user_group_id = $campaign_arr[0]['group_id'];
                        }
                        //echo "<pre>";var_dump($campaign_arr);die;
                    }
                }

                //echo "<pre>";print_r($addToCartProductArr);die;
                /* add campaign,promotion and user group id in cart ends */
                $donationAddToCartFormArr['campaign_id_front'] = $campaign_id;
                $addToCartDonationArr = $transaction->getAddToCartDonationArr($donationAddToCartFormArr);

                $addToCartDonationArr[] = $campaign_code;
                $addToCartDonationArr[] = $promotionId;
                $addToCartDonationArr[] = $couponCode;
                $addToCartDonationArr[] = $user_group_id;
                $addToCartDonationArr[] = $productResult[0]['product_sku'];
				$addToCartDonationArr[] = '4';

                $donationAddToCartId = $this->_transactionTable->addToCartDonation($addToCartDonationArr);
                $donationAddToCartFormArr['cart_date'] = DATE_TIME_FORMAT;
                $addToCartProductArr = $transaction->getAddToCartProductArr($donationAddToCartFormArr);
//$productAddToCartId = $this->_transactionTable->addToCartProduct($addToCartProductArr);
                if (!empty($postArr['email'])) {
                    $donationNotifyArr = array();
                    $donationNotifyArr['donation_product_id'] = $donationAddToCartId;
                    $donationNotifyArr['added_by'] = $this->auth->getIdentity()->crm_user_id;
                    $donationNotifyArr['added_date'] = DATE_TIME_FORMAT;
                    $donationNotifyArr['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                    $donationNotifyArr['modified_date'] = DATE_TIME_FORMAT;
                    foreach ($postArr['email'] as $index => $notifyemail) {
                        if (!empty($notifyemail)) {
                            $notifyName = (trim($postArr['name'][$index]) != '') ? $postArr['name'][$index] : "Guest";
                            $donationNotifyArr['name'] = $notifyName;
                            $donationNotifyArr['email_id'] = $notifyemail;
                            $addToCartDonationNotifyArr = $transaction->getAddToCartDonationNotifyArr($donationNotifyArr);

                            $donationNotifyAddToCartId = $this->_transactionTable->addToCartDonationNotify($addToCartDonationNotifyArr);
                        }
                    }
                }
                $messages = array('status' => "success");
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
    }

    /**
     * This action is used to add fof to cart
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function addToCartFofAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $transaction = new Transaction($this->_adapter);
        $flagOfFacesForm = new FlagOfFacesForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $flagOfFacesForm->setData($request->getPost());
            $flagOfFacesForm->setInputFilter($transaction->getInputFilterAddToCartFOF());

            if (!$flagOfFacesForm->isValid()) {
                $errors = $flagOfFacesForm->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'continue') {
                        foreach ($row as $typeError => $rower) {
                            if ($typeError != 'notInArray') {
                                $msg [$key] = $msgArr[$rower];
                            }
                        }
                    }
                }
            }

            $uploadPhoto = $request->getPost('fof_image');
            if (empty($uploadPhoto)) {
                $msg = array();
                $msg ['upload_photo'] = $msgArr['FOF_IMAGE_EMPTY'];
            }

            if (!empty($msg)) {
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $postArr = $request->getPost()->toArray();
                $flagOfFacesFormArr = $postArr;
                $prodMappArr = array($flagOfFacesFormArr['product_mapping_id']);
                $getProductMappingTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductsMappingType($prodMappArr);
                $productParam = array();
                $productParam['productType'] = $getProductMappingTypes[0]['product_type_id'];/** product */
                $productResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getProductAttributeDetails($productParam);
                $flagOfFacesFormArr['product_type_id'] = $getProductMappingTypes[0]['product_type_id'];

                $flagOfFacesFormArr['product_id'] = $this->getServiceLocator()->get('User\Model\FofTable')->getUserFofProductId(array('product_type_id' => '4')); //$productResult[0]['product_id'];

                $proDetails = array();
                $proDetails['id'] = $flagOfFacesFormArr['product_id'];
                $productResultAttr = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProductInfo($proDetails);

                $flagOfFacesFormArr['product_sku'] = $productResultAttr[0]['product_sku'];
                $flagOfFacesFormArr['product_price'] = $productResult[0]['product_sell_price'];
                $flagOfFacesFormArr['user_id'] = $this->decrypt($postArr['user_id']);
                $flagOfFacesFormArr['user_session_id'] = session_id();
                $flagOfFacesFormArr['added_by'] = $this->auth->getIdentity()->crm_user_id;
                $flagOfFacesFormArr['added_date'] = DATE_TIME_FORMAT;
                $flagOfFacesFormArr['modify_by'] = $this->auth->getIdentity()->crm_user_id;
                $flagOfFacesFormArr['modify_date'] = DATE_TIME_FORMAT;
                $flagOfFacesFormArr['cart_date'] = DATE_TIME_FORMAT;
                $flagOfFacesFormArr['people_photo_json'] = $postArr['people_photo'];
                $flagOfFacesFormArr['people_photo'] = $this->checkJsonFormatReturnString($postArr['people_photo'], 1);
//print_r($flagOfFacesFormArr); die();
                $addToCartFOFArr = $transaction->getAddToCartFOFArr($flagOfFacesFormArr);
                $fofAddToCartId = $this->_transactionTable->addToCartFOF($addToCartFOFArr);
                $flagOfFacesFormArr['cart_date'] = DATE_TIME_FORMAT;
                $addToCartProductArr = $transaction->getAddToCartProductArr($flagOfFacesFormArr);
//$productAddToCartId = $this->_transactionTable->addToCartProduct($addToCartProductArr);
// Copying to temp folder - start
                try {
                    $cachefilename = $request->getPost('temp_image');
                    $jsFilePath = $this->_config['js_file_path']['path'];
                    $jsVersion = $this->_config['file_versions']['js_version'];

                    $temp_dir = dirname($_SERVER['DOCUMENT_ROOT']) . '/public/js/pixenate/cache/';
                    $temp_file = $temp_dir . $cachefilename;

                    $filename = $request->getPost('fof_image');
// $temp_dir_fof = $this->_config['file_upload_path']['temp_upload_dir'] . 'fof/';
                    $temp_fof_file = $temp_dir . $filename;

                    if (isset($cachefilename) and trim($cachefilename) != "") {  // and file_exists($temp_file)
                        $temp_file_d = $temp_file;
                    } else {
                        $temp_file_d = $temp_fof_file;
                    }

                    $temp_dir_fof_f = $this->_config['file_upload_path']['temp_upload_dir'] . 'fof/';
                    $temp_fof_file_f = $temp_dir_fof_f . $filename;

// Start
                    $fof_dir_thumb = $this->_config['file_upload_path']['temp_upload_dir'] . "fof/thumb/";
                    $fof_dir_medium = $this->_config['file_upload_path']['temp_upload_dir'] . "fof/medium/";
                    $fof_dir_large = $this->_config['file_upload_path']['temp_upload_dir'] . "fof/large/";
                    $fof_filename_large = $fof_dir_large . $filename;
                    $fof_filename_medium = $fof_dir_medium . $filename;
                    $fof_filename_thumb = $fof_dir_thumb . $filename;
// End

                    if (isset($temp_file_d) and trim($temp_file_d) != "") {
                        if (@copy($temp_file_d, $temp_fof_file_f)) {
//sleep(2);
                            $this->resizeImage($fof_filename_thumb, $temp_file_d, $this->_config['file_upload_path']['user_fof_medium_width'], $this->_config['file_upload_path']['user_fof_medium_height']);
                            $this->resizeImage($fof_filename_medium, $temp_file_d, $this->_config['file_upload_path']['user_fof_medium_width'], $this->_config['file_upload_path']['user_fof_medium_height']);
                            $this->resizeImage($fof_filename_large, $temp_file_d, $this->_config['file_upload_path']['user_fof_large_width'], $this->_config['file_upload_path']['user_fof_large_height']);
                        }
                    }
                } catch (Exception $e) {

                }
// Copying to temp folder - end

                $messages = array('status' => "success");
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
    }

    /**
     * This function is used to checkJsonFormatReturnString
     * @return     array
     * $get - 1 : names ("," Seperated) , 2 : Array (if json), 3 : Return Json
     * @author Icreon Tech - NS
     */
    private function checkJsonFormatReturnString($string, $get) {
        try {
            $strTxt = $string;
            $flag = json_decode($string) != null;

// if string is Json else return string
            if ($flag) {
                $strTxt = json_decode($string);
                if ($get == "1") {
                    $arrNames = array();
                    foreach ($strTxt as $valST) {
                        if (isset($valST->name) and trim($valST->name) != "") {
                            array_push($arrNames, trim($valST->name));
                        }
                    }
                    if (count($arrNames) > 0) {
                        $strTxt = implode(", ", $arrNames);
                    } else {
                        $strTxt = "";
                    }
                } else if ($get == "3") {
                    $strTxt = $string;
                }
            } else {
                $strTxt = isset($string) ? trim($string) : '';
            }

            return $strTxt;
        } catch (Exception $e) {
            return $string;
        }
    }

    /**
     * This action is used to add fof to cart
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function addToCartKioskFeeAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $transaction = new Transaction($this->_adapter);
        $kioskFeeForm = new KioskFeeForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $postArr = $request->getPost()->toArray();
            $kioskFormFormArr = $postArr;
            $prodMappArr = array($kioskFormFormArr['product_mapping_id']);
            $getProductMappingTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductsMappingType($prodMappArr);
            $productParam = array();
            $productParam['productType'] = $getProductMappingTypes[0]['product_type_id'];/** product */
            $productResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getProductAttributeDetails($productParam);
            $kioskFormFormArr['productId'] = $productResult[0]['product_id'];
            $kioskFormFormArr['user_id'] = $this->decrypt($postArr['user_id']);
            $kioskFormFormArr['item_price'] = $productResult[0]['product_sell_price'];
            $kioskFormFormArr['item_type'] = '6';
            $this->addInventoryProductToCart($kioskFormFormArr);
            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This action is used to add inventory to cart
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function addToCartInventoryAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $transaction = new Transaction($this->_adapter);
        $inventoryForm = new InventoryForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $inventoryForm->setData($request->getPost());
            $inventoryForm->setInputFilter($transaction->getInputFilterAddToCartInventory());
            if (!$inventoryForm->isValid()) {
                $errors = $inventoryForm->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'continue') {
                        foreach ($row as $typeError => $rower) {
                            if ($typeError != 'notInArray') {
                                $msg [$key] = $msgArr[$rower];
                            }
                        }
                    }
                }
            }
            if (!empty($msg)) {
                $messages = array('status' => "error", 'message' => $msg);
            } else {
                $productId = $request->getPost('product_id');
                $productQuantity = $request->getPost('num_quantity');
                $detail = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProductInfo(array('id' => $productId));
                $cartParam['user_id'] = $this->decrypt($request->getPost('user_id'));
                $cartParam['source_type'] = 'backend';
                $cartlist = $this->getTransactionTable()->getCartProducts($cartParam);
                $quantityArray[$productId] = $productQuantity;
                if (!empty($cartlist)) {
                    foreach ($cartlist as $cartProduct) {
                        if ($cartProduct['product_type_id'] == 1 && $cartProduct['product_id'] == $productId) {
                            $productId = $cartProduct['product_id'];
                            $productQuantityCart = $cartProduct['product_qty'];
                            if (!empty($quantityArray[$productId])) {
                                $quantityArray[$productId] = $quantityArray[$productId] + $productQuantityCart;
                            } else {
                                $quantityArray[$productId] = $productQuantityCart;
                            }
                        }
                    }
                }
                if (!empty($quantityArray)) {
                    foreach ($quantityArray as $productId => $p_quantity) {
                        $detail = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProductInfo(array('id' => $productId));
                        $total_in_stock = $detail[0]['total_in_stock'];
                        if ($p_quantity > $total_in_stock) {
                            $msg = array();
                            $msg ['num_quantity'] = sprintf($msgArr['QUANTITY_EXCEED'], $total_in_stock, $detail[0]['product_name']);
                            $messages = array('status' => "error", 'message' => $msg);
                        }
                    }
                }
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $postArr = $request->getPost()->toArray();
                $inventoryFormArr = $postArr;
                $prodMappArr = array($inventoryFormArr['product_mapping_id']);
                $getProductMappingTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductsMappingType($prodMappArr);
                $inventoryFormArr['productId'] = $postArr['product_id'];
                $inventoryFormArr['user_id'] = $this->decrypt($postArr['user_id']);
                $inventoryFormArr['item_type'] = '4';
                $params['productId'] = $postArr['product_id'];
                $params['user_id'] = $this->decrypt($postArr['user_id']);
                $params['num_quantity'] = $postArr['num_quantity'];
                $params['source_type'] = "backend";
                $productStatus = $this->getTransactionTable()->getInventoryProductAlreadyAdded($params);
                if ($productStatus[0]['product_exist'] == 0) {
                    $this->addInventoryProductToCart($inventoryFormArr);
                } else {
                    $params['num_quantity'] = $params['num_quantity'];
                    $productStatus = $this->getTransactionTable()->updateCartInventoryProduct($params);
                }
//$this->addInventoryProductToCart($inventoryFormArr);
                $messages = array('status' => "success");
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
    }

    /**
     * This action is used to add inventory to cart
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function addToCartMembershipAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $transaction = new Transaction($this->_adapter);
        $membershipDetailForm = new MembershipDetailForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $postArr = $request->getPost()->toArray();
            $membershipDetailFormArr = $postArr;
            if (!isset($membershipDetailFormArr['membership_id'])) {
                $msg ['membership_id_1'] = $msgArr['MEMBERSHIP_EMPTY'];
            }
            if (!empty($msg)) {
                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {

                $prodMappArr = array($membershipDetailFormArr['product_mapping_id']);
                $getProductMappingTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductsMappingType($prodMappArr);
                $membershipDetailFormArr['product_type_id'] = $getProductMappingTypes[0]['product_type_id'];
                $membershipDetailFormArr['item_id'] = $postArr['membership_id'];
                $membershipDetailFormArr['item_price'] = $postArr['minimum_donation_amount'][0];
                $membershipDetailFormArr['user_id'] = $this->decrypt($postArr['user_id']);
                $membershipDetailFormArr['user_session_id'] = session_id();
                $membershipDetailFormArr['added_by'] = $this->auth->getIdentity()->crm_user_id;
                $membershipDetailFormArr['added_date'] = DATE_TIME_FORMAT;
                $membershipDetailFormArr['modify_by'] = $this->auth->getIdentity()->crm_user_id;
                $membershipDetailFormArr['modify_date'] = DATE_TIME_FORMAT;
                $membershipDetailFormArr['cart_date'] = DATE_TIME_FORMAT;
                $membershipDetailFormArr['item_type'] = '5';
                $addToCartProductArr = $transaction->getAddToCartProductArr($membershipDetailFormArr);
                /* add campaign,promotion and user group id in cart start */
                $promotionId = "";
                $couponCode = "";
                $user_group_id = "";

                $this->campaignSession = new Container('campaign');
                $campaign_code = (isset($this->campaignSession->campaign_code) && !empty($this->campaignSession->campaign_code) ? $this->campaignSession->campaign_code : "");

                $campaign_id = isset($this->campaignSession->campaign_id) && !empty($this->campaignSession->campaign_id) ? $this->campaignSession->campaign_id : 0;
                $channel_id = (isset($this->campaignSession->channel_id) && !empty($this->campaignSession->channel_id) ? $this->campaignSession->channel_id : 0);

                if ($campaign_id != 0 && $channel_id != 0) {
                    $campResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getChannelPromo(array('campaign_id' => $campaign_id, 'channel_id' => $channel_id));

                    if (isset($campResult[0]['promotion_id']) && !empty($campResult[0]['promotion_id'])) {
                        $couponCode = $campResult[0]['coupon_code'];
                        $promotionId = $campResult[0]['promotion_id'];
                    } else {
                        $couponCode = "";
                        $promotionId = 0;
                    }

                    $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '';
                    if (isset($user_id) && $user_id != '') {
                        $searchParam['user_id'] = $user_id;
                        $searchParam['campaign_id'] = $campaign_id;
                        $cmTable = $this->getServiceLocator()->get('Campaign\Model\CampaignTable');
                        $campaign_arr = $cmTable->getUserCampaigns($searchParam);
                        if ($campaign_arr && isset($campaign_arr[0]['group_id'])) {
                            $user_group_id = $campaign_arr[0]['group_id'];
                        }
                        //echo "<pre>";var_dump($campaign_arr);die;
                    }
                }

                $addToCartProductArr[] = $campaign_id;
                $addToCartProductArr[] = $campaign_code;
                $addToCartProductArr[] = $promotionId;
                $addToCartProductArr[] = $couponCode;
                $addToCartProductArr[] = $user_group_id;
				$addToCartProductArr[] = '3';

                //echo "<pre>";print_r($addToCartProductArr);die;
                /* add campaign,promotion and user group id in cart ends */
                $productAddToCartId = $this->_transactionTable->addToCartProduct($addToCartProductArr);
                $messages = array('status' => "success");
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
    }

    /**
     * This action is used to show transaction details page
     * @return json
     * @param void
     * @author Icreon Tech - AS
     */
    public function getTransactionDetailAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getTransactionTable();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $msgArr = $this->_config['transaction_messages']['config']['view_crm_transaction'];
        $searchParam['transactionId'] = $this->decrypt($params['id']);
        $searchResult = $this->getTransactionTable()->searchTransactionDetails($searchParam);
        $mode = (isset($params['mode']) && $params['mode'] != '') ? $this->decrypt($params['mode']) : 'view';
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'transactionId' => $params['id'],
            'transactionDecryptId' => $searchParam['transactionId'],
            'moduleName' => $this->encrypt('transaction'),
            'mode' => $mode,
            'jsLangTranslate' => $msgArr,
            'params' => $params,
            'encUserId' => $this->encrypt($searchResult[0]['user_id'])
        ));
        return $viewModel;
    }

    /**
     * This action is used to view transaction details
     * @return json
     * @param void
     * @author Icreon Tech - AS
     */
    public function viewTransactionAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $transactionNoteForm = new TransactionNotesForm();
        $transactionStatusForm = new TransactionStatusForm();
        $productStatusForm = new ProductStatusForm();
        $request = $this->getRequest();
        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        $params = $this->params()->fromRoute();
        $msgArr = $this->_config['transaction_messages']['config']['view_crm_transaction'];
        $getTransactionStatus = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTransactionStatus();
        $getProductStatus = $this->getTransactionTable()->getProductStatus();
        $transactionStatus = array();
        $transactionStatus[''] = 'Select';
        if ($getTransactionStatus !== false) {
            foreach ($getTransactionStatus as $key => $val) {
                $transactionStatus[$val['transaction_status_id']] = $val['transaction_status'];
            }
        }
        $productStatus = array();
        $productStatus[''] = 'Select';
        if ($getProductStatus !== false) {
            foreach ($getProductStatus as $key => $val) {
                $productStatus[$val['product_status_id']] = $val['product_status'];
            }
        }
        $transactionStatusForm->get('transaction_status')->setAttribute('options', $transactionStatus);
        $productStatusForm->get('product_status[]')->setAttribute('options', $productStatus);
        $searchParam['transactionId'] = $this->decrypt($params['id']);
        $searchResult = $this->getTransactionTable()->searchTransactionDetails($searchParam);
        $searchProduct = $this->getTransactionTable()->searchTransactionProducts($searchParam);
        $transactionRmaStatus = $this->getTransactionTable()->getTransactionRmaStatus($searchParam);
        if (!empty($transactionRmaStatus)) {
            if (count($transactionRmaStatus) > 0)
                $transRmaStatus = $transactionRmaStatus[0]['rma_action_id'];
        }
        $transactionPaymentCash = 0;
        $transactionPaymentCheck = array();
        $transactionPaymentCredit = array();

        $transactionPaymentReceiveCash = 0;
        $transactionPaymentReceiveCheck = array();
        $transactionPaymentReceiveCredit = array();

        $totalAmountReceived = 0;

        $creditProfileInfo = array();

        $transactionPayments = $this->getTransactionTable()->getTransactionPayments($searchParam);
        $transactionPaymentsReceived = $this->getTransactionTable()->getTransactionPaymentsReceived($searchParam);

        if (!empty($transactionPayments)) {
            foreach ($transactionPayments as $paymentMade) {
                if ($paymentMade['payment_mode_id'] == 4) {
                    $transactionPaymentCash = $paymentMade['amount'];
                } else if ($paymentMade['payment_mode_id'] == 3) {
                    $checkArr = array();
                    $checkArr['cheque_number'] = $paymentMade['cheque_number'];
                    $checkArr['cheque_date'] = $paymentMade['cheque_date'];
                    $checkArr['amount'] = $paymentMade['amount'];
                    $transactionPaymentCheck[] = $checkArr;
                } else if ($paymentMade['payment_mode_id'] == 1) {
                    if (!isset($creditProfileInfo[$paymentMade['profile_id']])) {
                        $profileInfo = array();
                        if (empty($creditProfileInfo[$paymentMade['profile_id']]) && (isset($paymentMade['profile_id']) && !empty($paymentMade['profile_id']))) {
                            $createCustomerProfile = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
                            $createCustomerProfile->getCustomerProfileRequest(array('customerProfileId' => $paymentMade['profile_id']));
                            $cardNumber = (string) $createCustomerProfile->profile->paymentProfiles->payment->creditCard->cardNumber;
                            $cardExpiryDate = (string) $createCustomerProfile->profile->paymentProfiles->payment->creditCard->expirationDate;

                            // add - n -start
                            $cardFirstName = (string) $createCustomerProfile->profile->paymentProfiles->billTo->firstName;
                            $cardLastName = (string) $createCustomerProfile->profile->paymentProfiles->billTo->lastName;
                            $cardFullName = "";
                            if (isset($cardFirstName) and trim($cardFirstName) != "") {
                                $cardFullName .= trim($cardFirstName);
                            }
                            if (isset($cardLastName) and trim($cardLastName) != "") {
                                $cardFullName .= " " . trim($cardLastName);
                            }
                            $cardFullName = (isset($cardFullName) and trim($cardFullName) != "") ? trim($cardFullName) : "N/A";
                            $profileInfo['full_name'] = $cardFullName;
                            // add - n - end

                            $profileInfo['card_number'] = $cardNumber;
                            $profileInfo['expiry_date'] = $cardExpiryDate;
                            $creditProfileInfo[$paymentMade['profile_id']] = $profileInfo;
                        } else {
                            $cardNumber = 'N/A';
                            $cardExpiryDate = 'N/A';
                            $cardFullName = 'N/A';
                            $profileInfo['card_number'] = $cardNumber;
                            $profileInfo['expiry_date'] = $cardExpiryDate;
                            $profileInfo['full_name'] = $cardFullName;
                            $creditProfileInfo[$paymentMade['profile_id']] = $profileInfo;
                        }
                    } else {
                        $cardFullName = isset($creditProfileInfo[$paymentMade['profile_id']]['full_name']) ? $creditProfileInfo[$paymentMade['profile_id']]['full_name'] : "N/A";
                        $cardNumber = $creditProfileInfo[$paymentMade['profile_id']]['card_number'];
                        $cardExpiryDate = $creditProfileInfo[$paymentMade['profile_id']]['expiry_date'];
                    }
                    $creditArr = array();


                    $creditArr['profile_id'] = $paymentMade['profile_id'];
                    $creditArr['card_number'] = $cardNumber;
                    $creditArr['expiry_date'] = $cardExpiryDate;
                    $creditArr['full_name'] = $cardFullName;
                    $creditArr['amount'] = $paymentMade['amount'];
                    $creditArr['authorize_transaction_id'] = $paymentMade['authorize_transaction_id'];
                    $transactionPaymentCredit[] = $creditArr;
                }
            }
        }
        if (!empty($transactionPaymentsReceived)) {
            foreach ($transactionPaymentsReceived as $paymentMade) {
                if ($paymentMade['payment_mode_id'] == 4) {
                    $transactionPaymentReceiveCash = $paymentMade['amount'];
                    $totalAmountReceived = $totalAmountReceived + $transactionPaymentReceiveCash;
                } else if ($paymentMade['payment_mode_id'] == 3) {
                    $checkArr = array();
                    $checkArr['cheque_number'] = $paymentMade['cheque_number'];
                    $checkArr['cheque_date'] = $paymentMade['cheque_date'];
                    $checkArr['amount'] = $paymentMade['amount'];
                    $transactionPaymentReceiveCheck[] = $checkArr;
                    $totalAmountReceived = $totalAmountReceived + $paymentMade['amount'];
                } else if ($paymentMade['payment_mode_id'] == 1) {
                    $creditArr = array();
                    $creditArr['profile_id'] = $paymentMade['profile_id'];
                    $creditArr['card_number'] = $creditProfileInfo[$paymentMade['profile_id']]['card_number'];
                    $creditArr['expiry_date'] = $creditProfileInfo[$paymentMade['profile_id']]['expiry_date'];
                    $creditArr['amount'] = $paymentMade['amount'];
                    $creditArr['authorize_transaction_id'] = $paymentMade['authorize_transaction_id'];
                    $transactionPaymentReceiveCredit[] = $creditArr;
                    $totalAmountReceived = $totalAmountReceived + $paymentMade['amount'];
                }
            }
        }

        /* transaction cases start */
        $cases = new Cases($this->_adapter);
        $searchCaseParam = array();
        $searchCaseParam['user_transaction_id'] = $searchParam['transactionId'];
        $searchCaseParam['sort_field'] = "if(tbl_case.modified_date='0000-00-00 00:00:00',tbl_case.added_date,tbl_case.modified_date)";
        $searchCaseParam['sort_order'] = 'desc';
        $searchCaseArr = $cases->getCaseSearchArr($searchCaseParam);
        $casesResult = $this->getServiceLocator()->get('Cases\Model\CasesTable')->getAllCase($searchCaseArr);
        /* transaction cases ends */

        $transactionReceived = $this->getTransactionTable()->getTransactionReceivedPayment($searchParam);
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $transactionNoteForm->get('transaction_id')->setAttribute('value', $searchResult[0]['transaction_id']);
        $notes['note_id'] = explode(",", $searchResult[0]['note_id']);
        $notes['note_user'] = explode(",", $searchResult[0]['note_user']);
        $notes['note'] = explode(",", $searchResult[0]['note']);
        $notes['note_private'] = explode(",", $searchResult[0]['note_private']);
        $notes['name'] = explode(",", $searchResult[0]['name']);
        $notes['date'] = explode(",", $searchResult[0]['date']);
        $notes['role'] = explode(",", $searchResult[0]['crm_role']);
        foreach ($searchProduct as $key => $val) {
            switch ($val['tra_pro']) {
                case 'bio':
                    $bioResult = $this->getTransactionTable()->searchBioDetails($val);
                    if (isset($bioResult) && !empty($bioResult)) {
                        array_push($searchProduct[$key], $bioResult[0]);
                    }
                    break;
                case 'fof':
                    $fofResult = $this->getTransactionTable()->searchFofDetails($val);
                    if (isset($fofResult) && !empty($fofResult)) {
                        array_push($searchProduct[$key], $fofResult[0]);
                    }
                    break;
                case 'pro':
                    $passengerResult = $this->getTransactionTable()->searchDocPass($val);
                    if (isset($passengerResult) && !empty($passengerResult)) {
                        array_push($searchProduct[$key], $passengerResult[0]);
                    }
                    break;
            }
            $proParam['transactionId'] = $searchParam['transactionId'];
            $proParam['productId'] = $val['id'];
            $proResult = $this->getTransactionTable()->searchProductsShipped($proParam);
            if (isset($proResult[0]) && !empty($proResult[0])) {
                $searchProduct[$key]['shippedQty'] = $proResult[0]['shippped_qty'];
            } else {
                $searchProduct[$key]['shippedQty'] = 0;
            }
        }

        $transactionStatusForm->get('transaction_status')->setAttribute('value', $searchResult[0]['transaction_status_id']);
        $viewModel->setVariables(array(
            'searchResult' => $searchResult[0],
            'searchProduct' => $searchProduct,
            'transactionId' => $params['id'],
            'illustratedShip' => $this->_config['illustrated_ship_id'],
            'jsLangTranslate' => $msgArr,
            'params' => $params,
            'transactionNoteForm' => $transactionNoteForm,
            'transactionStatusForm' => $transactionStatusForm,
            'productStatusForm' => $productStatusForm,
            'cases' => $casesResult,
            'notes' => $notes,
            'messages' => $messages,
            'transactionPaymentCash' => $transactionPaymentCash,
            'transactionPaymentCheck' => $transactionPaymentCheck,
            'transactionPaymentCredit' => $transactionPaymentCredit,
            'transactionPaymentInvoice' => $transactionPaymentInvoice,
            'transactionPaymentReceiveCash' => $transactionPaymentReceiveCash,
            'transactionPaymentReceiveCheck' => $transactionPaymentReceiveCheck,
            'transactionPaymentReceiveCredit' => $transactionPaymentReceiveCredit,
            'totalAmountReceived' => $totalAmountReceived,
            'userId' => $this->auth->getIdentity()->crm_user_id,
            'tableName' => 'tbl_tra_notes',
            'tableField' => 'transaction_note_id',
            'transactionReceived' => $transactionReceived,
            'transRmaStatus' => $transRmaStatus
        ));
        return $viewModel;
    }

    /**
     * This action is used to create bio certificate
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function addBioCertificateAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $this->layout('popup');
        $bioCertificate = new BioCertificate();
        $request = $this->getRequest();
        $transaction = new Transaction($this->_adapter);
        $createTransactionMessage = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $commonTransactionMessage = $this->_config['transaction_messages']['config']['common_message'];
        $viewModel = new ViewModel();
        $params = $this->params()->fromRoute();
        $searchResult = $this->getServiceLocator()->get('User\Model\UserTable')->getBioWoh(array('bio_certificate_product_id' => $params['bioProId']));
        $viewModel->setVariables(array(
            'searchResult' => $searchResult[0],
            'bioCertificate' => $bioCertificate,
            'jsLangTranslate' => array_merge($createTransactionMessage, $commonTransactionMessage)
        ));
        return $viewModel;
    }

    /**
     * This action is used to add donation to cart
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function addToCartWallOfHonorAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $transaction = new Transaction($this->_adapter);
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $postArr = $request->getPost()->toArray();
            $wallOfHonorAddToCartFormArr = $postArr;
            $prodMappArr = array($wallOfHonorAddToCartFormArr['product_mapping_id']);
            $getProductMappingTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductsMappingType($prodMappArr);
            $wallOfHonorAddToCartFormArr['product_type_id'] = $getProductMappingTypes[0]['product_type_id'];
            $wallOfHonorAddToCartFormArr['user_id'] = $this->decrypt($postArr['user_id']);
            $wallOfHonorAddToCartFormArr['user_session_id'] = session_id();
            $wallOfHonorAddToCartFormArr['added_by'] = $this->auth->getIdentity()->crm_user_id;
            $wallOfHonorAddToCartFormArr['added_date'] = DATE_TIME_FORMAT;
            $wallOfHonorAddToCartFormArr['modify_by'] = $this->auth->getIdentity()->crm_user_id;
            $wallOfHonorAddToCartFormArr['modify_date'] = DATE_TIME_FORMAT;
            $wallOfHonorAddToCartFormArr['cart_date'] = DATE_TIME_FORMAT;
            $productId = $request->getPost('woh_main_product');
            $productMappingId = $request->getPost('product_mapping_id');
            /* Bio Certificate Code */
            $isBioCertificate = $request->getPost('is_bio_certificate');
            $bioCertificateQty = $request->getPost('bio_certificate_qty');
            $isPersonalize = $request->getPost('is_personalize_panel');
            $personalizeQty = $request->getPost('personalized_qty');
            $isDupCertificate = $request->getPost('is_duplicate_certificate');
            $dupCertificateQty = $request->getPost('duplicate_certificate_qty');
            if ($isBioCertificate != '' || $isPersonalize || $isDupCertificate) {
                /* Bio Certificate Code */
                if ($isBioCertificate != 0 && $isBioCertificate != '' && $bioCertificateQty != '' && $bioCertificateQty > 0) {
                    $paramsArr = array();
                    $paramsArr['productId'] = $wallOfHonorAddToCartFormArr['bio_certificate_id'];
                    $paramsArr['user_id'] = $this->decrypt($postArr['user_id']);
// $addToCartProductId = $this->addInventoryProductToCart($paramsArr);
                    $searchParam['id'] = $wallOfHonorAddToCartFormArr['bio_certificate_id'];
                    $productResult = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProductInfo($searchParam);
                    $paramsArr = array();
                    $paramsArr = $wallOfHonorAddToCartFormArr;
                    $paramsArr['user_id'] = $this->decrypt($postArr['user_id']);
                    $paramsArr['user_session_id'] = session_id();
                    $paramsArr['date_of_entry_to_us'] = $this->DateFormat($paramsArr['date_of_entry_to_us'], 'db_datetime_format');
                    $paramsArr['product_id'] = $productResult[0]['product_id'];
                    $paramsArr['product_type_id'] = $productResult[0]['product_type_id'];
                    $paramsArr['product_mapping_id'] = $productResult[0]['product_mapping_id'];
                    $paramsArr['product_sku'] = $productResult[0]['product_sku'];
                    $paramsArr['product_price'] = $productResult[0]['product_sell_price'];
                    $paramsArr['wohid'] = $postArr['bio_wohid'];
                    $paramsArr['added_by'] = $this->auth->getIdentity()->crm_user_id;
                    $paramsArr['added_date'] = DATE_TIME_FORMAT;
                    $paramsArr['modify_by'] = $this->auth->getIdentity()->crm_user_id;
                    $paramsArr['modify_date'] = DATE_TIME_FORMAT;
                    $paramsArr['item_type'] = '7';
                    $bioResult = $this->_transactionTable->getBioProductId($paramsArr);
                    $paramsArr['bio_certificate_product_id'] = $bioResult[0]['bio_certificate_product_id'];
                    $addToCartBioCertificateArr = $transaction->getAddToCartWallOfHonorBioCertificate($paramsArr);

                    /* add campaign,promotion and user group id in cart start */
                    $promotionId = "";
                    $couponCode = "";
                    $user_group_id = "";

                    $this->campaignSession = new Container('campaign');
                    $campaign_code = (isset($this->campaignSession->campaign_code) && !empty($this->campaignSession->campaign_code) ? $this->campaignSession->campaign_code : "");

                    $campaign_id = isset($this->campaignSession->campaign_id) && !empty($this->campaignSession->campaign_id) ? $this->campaignSession->campaign_id : 0;
                    $channel_id = (isset($this->campaignSession->channel_id) && !empty($this->campaignSession->channel_id) ? $this->campaignSession->channel_id : 0);

                    if ($campaign_id != 0 && $channel_id != 0) {
                        $campResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getChannelPromo(array('campaign_id' => $campaign_id, 'channel_id' => $channel_id));

                        if (isset($campResult[0]['promotion_id']) && !empty($campResult[0]['promotion_id'])) {
                            $couponCode = $campResult[0]['coupon_code'];
                            $promotionId = $campResult[0]['promotion_id'];
                        } else {
                            $couponCode = "";
                            $promotionId = 0;
                        }

                        $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '';
                        if (isset($user_id) && $user_id != '') {
                            $searchParam['user_id'] = $user_id;
                            $searchParam['campaign_id'] = $campaign_id;
                            $cmTable = $this->getServiceLocator()->get('Campaign\Model\CampaignTable');
                            $campaign_arr = $cmTable->getUserCampaigns($searchParam);
                            if ($campaign_arr && isset($campaign_arr[0]['group_id'])) {
                                $user_group_id = $campaign_arr[0]['group_id'];
                            }
                            //echo "<pre>";var_dump($campaign_arr);die;
                        }
                    }

                    $addToCartBioCertificateArr[] = $campaign_id;
                    $addToCartBioCertificateArr[] = $campaign_code;
                    $addToCartBioCertificateArr[] = $promotionId;
                    $addToCartBioCertificateArr[] = $couponCode;
                    $addToCartBioCertificateArr[] = $user_group_id;
					$addToCartBioCertificateArr[] = '6';

                    //echo "<pre>";print_r($addToCartProductArr);die;
                    /* add campaign,promotion and user group id in cart ends */
                    $addToCartBioCertificateId = $this->_transactionTable->addToCartBioCertificate($addToCartBioCertificateArr);
                }
                /* End Bio Certificate Code */

                /* Personalize Code */
                if ($isPersonalize != 0 && $isPersonalize != '' && $personalizeQty != '' && $personalizeQty > 0) {
                    $paramsArr = array();
                    $paramsArr['productId'] = $wallOfHonorAddToCartFormArr['personalize_panel_id'];
                    $paramsArr['user_id'] = $this->decrypt($postArr['user_id']);
                    $paramsArr['item_id'] = $wallOfHonorAddToCartFormArr['personalize_wohid'];
                    $paramsArr['item_type'] = '8';
                    $this->addInventoryProductToCart($paramsArr);
                }

                if ($isDupCertificate != 0 && $isDupCertificate != '' && $dupCertificateQty != '' && $dupCertificateQty > 0) {
                    $paramsArr = array();
                    $paramsArr['productId'] = $wallOfHonorAddToCartFormArr['duplicate_certificate_id'];
                    $paramsArr['user_id'] = $this->decrypt($postArr['user_id']);
                    $paramsArr['item_id'] = $wallOfHonorAddToCartFormArr['personalize_wohid'];
                    $paramsArr['item_type'] = '7';
                    $this->addInventoryProductToCart($paramsArr);
                }
                /* End personalize Code */
            }
            /* Wall of honor Code */
            $productParam = array();
            $productParam['productType'] = $wallOfHonorAddToCartFormArr['product_type_id'];/** product */
            $productParam['productId'] = $wallOfHonorAddToCartFormArr['woh_main_product'];
            $productResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getProductAttributeDetails($productParam);

// n - start
            $proDetails = array();
            $proDetails['id'] = $wallOfHonorAddToCartFormArr['woh_main_product'];
            $productResultAttr = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProductInfo($proDetails);
// n - end

            $wallOfHonorAddToCartFormArr['product_id'] = $wallOfHonorAddToCartFormArr['woh_main_product'];
            $wallOfHonorAddToCartFormArr['product_sku'] = $productResultAttr[0]['product_sku']; //$productResult[0]['product_sku'];
            $wallOfHonorAddToCartFormArr['product_price'] = $productResult[0]['product_sell_price'];
            $isUsa = $request->getPost('is_usa');
            $otherCountry = $request->getPost('other_country');
            $countryName = ($isUsa != 0 && $isUsa != '') ? $this->_config['transaction_config']['origin_country'] : $otherCountry;
            $wallOfHonorAddToCartFormArr['origin_country'] = $countryName;

            $wallOfHonorAddToCartFormArr['donation_for'] = $request->getPost('donated_for');

            /* Set option cost */
            foreach ($productResult as $productOption) {
                if ($productOption['attribute_option_id'] == $wallOfHonorAddToCartFormArr['product_attribute_option_id']) {
                    $wallOfHonorAddToCartFormArr['product_price']+=$productOption['option_price'];
                    break;
                }
            }

            $firstNameOne = $request->getPost('first_name_one');
            $otherName = $request->getPost('other_name');
            $otherInitOne = $request->getPost('other_init_one');
            $lastNameOne = $request->getPost('last_name_one');
            $firstNameTwo = $request->getPost('first_name_two');
            $otherInitTwo = $request->getPost('other_init_two');
            $lastNameTwo = $request->getPost('last_name_two');
            $firstLine = $request->getPost('first_line');
            $secondLine = $request->getPost('second_line');
            $productParam = array();
            $productParam['productType'] = $productResult[0]['product_type_id'];
            $productAttributResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getProductAttributeDetails($productParam);
            $optionId = $request->getPost('product_attribute_option_id');
            $productAttributeOptionName = '';
            foreach ($productAttributResult as $proAtt) {
                if ($proAtt['attribute_option_id'] == $optionId) {
                    $productAttributeOptionName = $proAtt['option_name'];
                }
            }


            $optionName = $request->getPost('option_name');
            $attributeName = $request->getPost('attribute_type');
            $formatName = $request->getPost('format_name');
//$wallOfHonor->get('product_attribute_option_id')->setValue($optionId);

            $wohDetail = array();
            $wohDetail['first_name_one'] = $firstNameOne;
            $wohDetail['other_init_one'] = $otherInitOne;
            $wohDetail['first_name_two'] = $firstNameTwo;
            $wohDetail['other_name'] = $otherName;
            $wohDetail['other_init_two'] = $otherInitTwo;
            $wohDetail['last_name_one'] = $lastNameOne;
            $wohDetail['last_name_two'] = $lastNameTwo;
            $wohDetail['first_line'] = $firstLine;
            $wohDetail['second_line'] = $secondLine;
            $finalName = $this->getNamingConventionArr($productAttributeOptionName, '', $wohDetail);
            $finalName = trim($finalName['honoree_name']);
            $wallOfHonorAddToCartFormArr['final_name'] = $finalName;
            if (!empty($firstNameOne) || !empty($otherName) || !empty($otherInitOne) || !empty($lastNameOne) || !empty($firstNameTwo) || !empty($otherInitTwo) || !empty($lastNameTwo) || !empty($firstLine) || !empty($secondLine)) {
                /* end Set option cost */

                $addToCartWallOfHonorArr = $transaction->getAddToCartWallOfHonorArr($wallOfHonorAddToCartFormArr);

                /* add campaign,promotion and user group id in cart start */
                $promotionId = "";
                $couponCode = "";
                $user_group_id = "";

                $this->campaignSession = new Container('campaign');
                $campaign_code = (isset($this->campaignSession->campaign_code) && !empty($this->campaignSession->campaign_code) ? $this->campaignSession->campaign_code : "");

                $campaign_id = isset($this->campaignSession->campaign_id) && !empty($this->campaignSession->campaign_id) ? $this->campaignSession->campaign_id : 0;
                $channel_id = (isset($this->campaignSession->channel_id) && !empty($this->campaignSession->channel_id) ? $this->campaignSession->channel_id : 0);

                if ($campaign_id != 0 && $channel_id != 0) {
                    $campResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getChannelPromo(array('campaign_id' => $campaign_id, 'channel_id' => $channel_id));

                    if (isset($campResult[0]['promotion_id']) && !empty($campResult[0]['promotion_id'])) {
                        $couponCode = $campResult[0]['coupon_code'];
                        $promotionId = $campResult[0]['promotion_id'];
                    } else {
                        $couponCode = "";
                        $promotionId = 0;
                    }

                    $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '';
                    if (isset($user_id) && $user_id != '') {
                        $searchParam['user_id'] = $user_id;
                        $searchParam['campaign_id'] = $campaign_id;
                        $cmTable = $this->getServiceLocator()->get('Campaign\Model\CampaignTable');
                        $campaign_arr = $cmTable->getUserCampaigns($searchParam);
                        if ($campaign_arr && isset($campaign_arr[0]['group_id'])) {
                            $user_group_id = $campaign_arr[0]['group_id'];
                        }
                        //echo "<pre>";var_dump($campaign_arr);die;
                    }
                }

                $addToCartWallOfHonorArr[] = $campaign_id;
                $addToCartWallOfHonorArr[] = $campaign_code;
                $addToCartWallOfHonorArr[] = $promotionId;
                $addToCartWallOfHonorArr[] = $couponCode;
                $addToCartWallOfHonorArr[] = $user_group_id;
                //Ticket-Id :130
                $wohObj = new Woh($this->_adapter);
                $inscribeKey = $wohObj->getInscribeKey($addToCartWallOfHonorArr[29]);
                $addToCartWallOfHonorArr[] = $inscribeKey;
                /* add campaign,promotion and user group id in cart ends */
                $wallOfHonorAddToCartId = $this->_transactionTable->addToCartWallOfHonorNaming($addToCartWallOfHonorArr);
                $addToCartProductArr = $transaction->getAddToCartProductArr($wallOfHonorAddToCartFormArr);

//$productAddToCartId = $this->_transactionTable->addToCartProduct($addToCartProductArr);

                /* Duplicate Certificate Code */
                  $isDuplicateCertificate = $request->getPost('is_duplicate_certificate');
                  $duplicateCertificateQty = $request->getPost('duplicate_certificate_qty');
                  if ($isDuplicateCertificate != 0 && $isDuplicateCertificate != '' && $duplicateCertificateQty != '' && $duplicateCertificateQty > 0) {
                  //Duplicate Certificate Add
                  $paramsArr = array();
                  $paramsArr['productId'] = $wallOfHonorAddToCartFormArr['duplicate_certificate_id'];
                  $paramsArr['item_id'] = $wallOfHonorAddToCartId;
                  $paramsArr['item_type'] = '7';
                  $paramsArr['item_info'] = '1';
                  $paramsArr['user_id'] = $this->decrypt($postArr['user_id']);
                  $paramsArr['num_quantity'] = $wallOfHonorAddToCartFormArr['duplicate_certificate_qty'];
                  $this->addInventoryProductToCart($paramsArr);
                  //End duplicate Certificate Add
                  }
                  /* End duplicate Certificate Code */

                /* Save data for additional contact */
                if (!empty($wallOfHonorAddToCartFormArr['first_name'])) {
                    $ii = 0;
                    foreach ($wallOfHonorAddToCartFormArr['first_name'] as $additionalContact) {
                        if ($wallOfHonorAddToCartFormArr['email_id'][$ii] != '') {
                            $additionalContactArr = array();
                            $additionalContactArr['woh_id'] = $wallOfHonorAddToCartId;
                            $additionalContactArr['first_name'] = $wallOfHonorAddToCartFormArr['first_name'][$ii];
                            $additionalContactArr['last_name'] = $wallOfHonorAddToCartFormArr['last_name'][$ii];
                            $additionalContactArr['email_id'] = $wallOfHonorAddToCartFormArr['email_id'][$ii];
                            $additionalContactArr['added_date'] = DATE_TIME_FORMAT;
                            $additionalContactArr['modify_date'] = DATE_TIME_FORMAT;
							$additionalContactArr['cart_source_id'] = '6';
                            $addToCartWallOfHonorAdditionalContactArr = $transaction->getAddToCartWallOfHonorAdditionalContactArr($additionalContactArr);
                            $wallOfHonorAdditionalContactAddToCartId = $this->_transactionTable->addToCartWallOfHonorAdditionalContact($addToCartWallOfHonorAdditionalContactArr);
                        }
                        $ii++;
                    }
                }
            }
            /* End Save data for additional contact */
            /* End wall of honor Code */



            /* Add related Product */
            $isRelatedProduct = $request->getPost('is_related_product');
            $relatedProductIds = $request->getPost('related_product_name_ids');
            if ($isRelatedProduct != 0 && !empty($isRelatedProduct) && !empty($relatedProductIds)) {
                $relatedProductIds = explode(',', $relatedProductIds);
                foreach ($relatedProductIds as $relatedProductId) {
                    $paramsArr = array();
                    $paramsArr['productId'] = $relatedProductId;
					$paramsArr['item_type'] = '4';
                    $paramsArr['user_id'] = $this->decrypt($postArr['user_id']);
                    $this->addInventoryProductToCart($paramsArr);
                }
            }
            /* End Add related Product */
            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This action is used to add note to transaction
     * @return json
     * @param void
     * @author Icreon Tech - AS
     */
    public function addNoteAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $viewModel = new ViewModel();
        $params = $this->params()->fromRoute();
        $addNote['transaction_id'] = $request->getPost('transaction_id');
        $addNote['notify_note'] = $request->getPost('notify_note');
        $addNote['note'] = $request->getPost('note');
        $addNote['added_by'] = $this->auth->getIdentity()->crm_user_id;
        $addNote['added_date'] = DATE_TIME_FORMAT;
        $searchResult = $this->getTransactionTable()->addTransactionNotes($addNote);
        $replydivClass = $request->getPost('replydivClass');
        $replydivClass = isset($replydivClass) ? $replydivClass : 'alterbg alterbg-none';

        $addedNote = "<div class='row " . $replydivClass . "'>";
        if (isset($searchResult[0]['note_private']) and trim($searchResult[0]['note_private']) != "") {
            $addedNote .= '<strong class="right bold m-b-10 aright"> <input id = "notes_' . $searchResult[0]['note_id'] . '" type="checkbox" checked="checked" />Private</strong>';
        } else {
            $addedNote .= '<strong class="right bold m-b-10 aright"> <input id = "notes_' . $searchResult[0]['note_id'] . '" type="checkbox" />Private</strong>';
        }
        $addedNote .= "<p class='text'>" . $searchResult[0]['note'] . "</p>
                        <p class='text right no-padd'><strong>Date Created: </strong>" . $this->OutputDateFormat($searchResult[0]['added_date'], 'dateformatampm') . " by " . $searchResult[0]['crm_name'] . "</p>
                    </div>";
        $messages = array('status' => "success", 'content' => $addedNote);
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This action is used to add inventory product to cart
     * @return int
     * @param array
     * @author Icreon Tech - DT
     */
    public function addInventoryProductToCart($params = array()) {
        $searchParam['id'] = $params['productId'];
        $productResult = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProductInfo($searchParam);
        $transaction = new Transaction($this->_adapter);
        $addToCartProductId = 0;
        if (!empty($productResult)) {
            $inventoryFormArr = array();
            $inventoryFormArr['user_id'] = $params['user_id'];
            $inventoryFormArr['user_session_id'] = session_id();
            $inventoryFormArr['product_mapping_id'] = isset($params['product_mapping_id']) ? $params['product_mapping_id'] : $productResult[0]['product_mapping_id'];
            $inventoryFormArr['product_id'] = isset($params['product_id']) ? $params['product_id'] : $productResult[0]['product_id'];
            $inventoryFormArr['product_type_id'] = isset($params['product_type_id']) ? $params['product_type_id'] : $productResult[0]['product_type_id'];
            $inventoryFormArr['product_attribute_option_id'] = isset($params['product_attribute_option_id']) ? $params['product_attribute_option_id'] : NULL;
            $inventoryFormArr['item_id'] = isset($params['item_id']) ? $params['item_id'] : NULL;
            $inventoryFormArr['item_type'] = isset($params['item_type']) ? $params['item_type'] : NULL;
            $inventoryFormArr['num_quantity'] = isset($params['num_quantity']) ? $params['num_quantity'] : 1;
            $inventoryFormArr['item_price'] = isset($params['item_price']) ? $params['item_price'] : NULL;
            $inventoryFormArr['product_sku'] = $productResult[0]['product_sku'];
            $inventoryFormArr['product_price'] = isset($params['product_price']) ? $params['product_price'] : $productResult[0]['product_sell_price'];
            $inventoryFormArr['added_by'] = isset($this->auth->getIdentity()->crm_user_id) ? $this->auth->getIdentity()->crm_user_id : '0';
            $inventoryFormArr['added_date'] = DATE_TIME_FORMAT;
            $inventoryFormArr['modify_by'] = isset($this->auth->getIdentity()->crm_user_id) ? $this->auth->getIdentity()->crm_user_id : '0';
            $inventoryFormArr['modify_date'] = DATE_TIME_FORMAT;
            $inventoryFormArr['cart_date'] = DATE_TIME_FORMAT;
            $inventoryFormArr['item_name'] = isset($params['item_name']) ? $params['item_name'] : NULL;
            $inventoryFormArr['item_info'] = isset($params['item_info']) ? $params['item_info'] : NULL;
            $inventoryFormArr['manifest_info'] = isset($params['manifest_info']) ? $params['manifest_info'] : NULL;
            $inventoryFormArr['manifest_type'] = isset($params['manifest_type']) ? $params['manifest_type'] : NULL;
            $addToCartProductArr = $transaction->getAddToCartProductArr($inventoryFormArr);

            /* add campaign,promotion and user group id in cart start */
            $promotionId = "";
            $couponCode = "";
            $user_group_id = "";

            $this->campaignSession = new Container('campaign');
            $campaign_code = (isset($this->campaignSession->campaign_code) && !empty($this->campaignSession->campaign_code) ? $this->campaignSession->campaign_code : "");

            $campaign_id = isset($this->campaignSession->campaign_id) && !empty($this->campaignSession->campaign_id) ? $this->campaignSession->campaign_id : 0;
            $channel_id = (isset($this->campaignSession->channel_id) && !empty($this->campaignSession->channel_id) ? $this->campaignSession->channel_id : 0);

            if ($campaign_id != 0 && $channel_id != 0) {
                $campResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getChannelPromo(array('campaign_id' => $campaign_id, 'channel_id' => $channel_id));

                if (isset($campResult[0]['promotion_id']) && !empty($campResult[0]['promotion_id'])) {
                    $couponCode = $campResult[0]['coupon_code'];
                    $promotionId = $campResult[0]['promotion_id'];
                } else {
                    $couponCode = "";
                    $promotionId = 0;
                }

                $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '';
                if (isset($user_id) && $user_id != '') {
                    $searchParam['user_id'] = $user_id;
                    $searchParam['campaign_id'] = $campaign_id;
                    $cmTable = $this->getServiceLocator()->get('Campaign\Model\CampaignTable');
                    $campaign_arr = $cmTable->getUserCampaigns($searchParam);
                    if ($campaign_arr && isset($campaign_arr[0]['group_id'])) {
                        $user_group_id = $campaign_arr[0]['group_id'];
                    }
                    //echo "<pre>";var_dump($campaign_arr);die;
                }
            }

            $addToCartProductArr[] = $campaign_id;
            $addToCartProductArr[] = $campaign_code;
            $addToCartProductArr[] = $promotionId;
            $addToCartProductArr[] = $couponCode;
            $addToCartProductArr[] = $user_group_id;
			$addToCartProductArr[] = '3';

            //echo "<pre>";print_r($addToCartProductArr);die;
            /* add campaign,promotion and user group id in cart ends */

            $addToCartProductId = $this->_transactionTable->addToCartProduct($addToCartProductArr);
        }
        return $addToCartProductId;
    }

    /**
     * This action is used to add additional passenger manifest
     * @return int
     * @param array
     * @author Icreon Tech - DT
     */
    public function addToCartPassengerManifest($params = array()) {
        $transaction = new Transaction($this->_adapter);
        $addToCartManifestId = 0;
        $manifestFormArr = array();
        $manifestFormArr = $params;
        $manifestFormArr['added_by'] = isset($this->auth->getIdentity()->crm_user_id) ? $this->auth->getIdentity()->crm_user_id : '0';
        $manifestFormArr['added_date'] = DATE_TIME_FORMAT;
        $manifestFormArr['modified_by'] = isset($this->auth->getIdentity()->crm_user_id) ? $this->auth->getIdentity()->crm_user_id : '0';
        $manifestFormArr['modified_date'] = DATE_TIME_FORMAT;
        $manifestFormArr['session_id'] = session_id();
        $addToCartManifestArr = $transaction->getAddToCartManifestArr($manifestFormArr);

        /* add campaign,promotion and user group id in cart start */
        $promotionId = "";
        $couponCode = "";
        $user_group_id = "";

        $this->campaignSession = new Container('campaign');
        $campaign_code = (isset($this->campaignSession->campaign_code) && !empty($this->campaignSession->campaign_code) ? $this->campaignSession->campaign_code : "");

        $campaign_id = isset($this->campaignSession->campaign_id) && !empty($this->campaignSession->campaign_id) ? $this->campaignSession->campaign_id : 0;
        $channel_id = (isset($this->campaignSession->channel_id) && !empty($this->campaignSession->channel_id) ? $this->campaignSession->channel_id : 0);

        if ($campaign_id != 0 && $channel_id != 0) {
            $campResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getChannelPromo(array('campaign_id' => $campaign_id, 'channel_id' => $channel_id));

            if (isset($campResult[0]['promotion_id']) && !empty($campResult[0]['promotion_id'])) {
                $couponCode = $campResult[0]['coupon_code'];
                $promotionId = $campResult[0]['promotion_id'];
            } else {
                $couponCode = "";
                $promotionId = 0;
            }

            $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '';
            if (isset($user_id) && $user_id != '') {
                $searchParam['user_id'] = $user_id;
                $searchParam['campaign_id'] = $campaign_id;
                $cmTable = $this->getServiceLocator()->get('Campaign\Model\CampaignTable');
                $campaign_arr = $cmTable->getUserCampaigns($searchParam);
                if ($campaign_arr && isset($campaign_arr[0]['group_id'])) {
                    $user_group_id = $campaign_arr[0]['group_id'];
                }
                //echo "<pre>";var_dump($campaign_arr);die;
            }
        }

        $addToCartManifestArr[] = $campaign_id;
        $addToCartManifestArr[] = $campaign_code;
        $addToCartManifestArr[] = $promotionId;
        $addToCartManifestArr[] = $couponCode;
        $addToCartManifestArr[] = $user_group_id;
		$addToCartManifestArr[] = '3';

        //echo "<pre>";print_r($addToCartProductArr);die;
        /* add campaign,promotion and user group id in cart ends */

        $addToCartManifestId = $this->_transactionTable->addToCartManifest($addToCartManifestArr);
        return $addToCartManifestId;
    }

    /**
     * This action is used to add fof to cart
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function addToCartPassengerDocumentAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $transaction = new Transaction($this->_adapter);
        $passengerDocumentForm = new PassengerDocumentForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $passengerDocumentForm->setData($request->getPost());
            $passengerDocumentForm->setInputFilter($transaction->getInputFilterAddToCartPassengerDocument());

            if (!$passengerDocumentForm->isValid()) {
                $errors = $passengerDocumentForm->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'continue') {
                        foreach ($row as $typeError => $rower) {
                            if ($typeError != 'notInArray') {
                                $msg [$key] = $msgArr[$rower];
                            }
                        }
                    }
                }
            }
            if (!empty($msg)) {
                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $sm = $this->getServiceLocator();
                $passengerTable = $sm->get('Passenger\Model\PassengerTable');
                $postArr = $request->getPost()->toArray();
                //asd($postArr);

                $passengerDocumentFormArr = $postArr;
//zzzzzzzzzzzzzzzz
                /* Add passenger Record */
                $isPassengerRecord = $request->getPost('is_passenger_record');
                $passengerRecordQty = $request->getPost('passenger_record_qty');
                $passengerRecordOption = $request->getPost('passenger_record_options');
                if ($isPassengerRecord != '' && $isPassengerRecord != 0 && $passengerRecordOption != '' && $passengerRecordOption != 0 && $passengerRecordQty != '' && $passengerRecordQty > 0) {
                    $sm = $this->getServiceLocator();
                    $passengerTable = $sm->get('Passenger\Model\PassengerTable');
                    $productParam = array();
                    $productParam['productType'] = '5';/** product */
                    $productResult = array();
                    $productResult = $passengerTable->getProductAttributeDetails($productParam);
                    $paramsArr = array();
                    $paramsArr['product_type_id'] = 5;
                    $paramsArr['productId'] = $productResult[0]['product_id'];
                    $paramsArr['product_attribute_option_id'] = $passengerRecordOption;
                    $paramsArr['item_id'] = $passengerDocumentFormArr['passenger_id'];
                    $paramsArr['item_type'] = '1';
                    $paramsArr['user_id'] = $this->decrypt($passengerDocumentFormArr['user_id']);
                    $paramsArr['item_name'] = $passengerDocumentFormArr['passenger_name'];
                    $paramsArr['num_quantity'] = $passengerRecordQty;
//$paramsArr['item_price'] = $productResult[0]['product_sell_price'] + $productResult[0]['option_price'];
                    $addToCartProductId = $this->addInventoryProductToCart($paramsArr);
                }

                /* End add passenger Record */

                /* Add passenger Ship Record */
                $isPassengerShip = $request->getPost('is_ship_image');
                $shipPassengerQty = $request->getPost('ship_image_qty');
                $attributeOptionIds = $request->getPost('attribute_option_id');
                if ($isPassengerShip != '' && $isPassengerShip != 0 && $shipPassengerQty != '' && $shipPassengerQty > 0 && count($attributeOptionIds) > 0) {

                    $productParam = array();
                    $productParam['productType'] = '8';/** product */
                    $productResult = array();
                    $productResult = $passengerTable->getProductAttributeDetails($productParam);
                    foreach ($productResult as $attribute) {
                        $isExistArr = array_keys($attributeOptionIds, $attribute['attribute_option_id']);
                        if (!empty($isExistArr)) {
//foreach ($attributeOptionIds as $attribute) {
                            $paramsArr = array();
                            $paramsArr['product_type_id'] = 8;
                            $paramsArr['productId'] = $attribute['product_id'];
                            $paramsArr['product_attribute_option_id'] = $attribute['attribute_option_id'];
                            $paramsArr['item_id'] = $passengerDocumentFormArr['ship_id'];
                            $paramsArr['item_name'] = $passengerDocumentFormArr['item_name'];
                            $paramsArr['item_info'] = $passengerDocumentFormArr['item_info'];
                            $paramsArr['item_type'] = '3';
                            $paramsArr['user_id'] = $this->decrypt($passengerDocumentFormArr['user_id']);
                            $paramsArr['num_quantity'] = $shipPassengerQty;
//$paramsArr['item_price'] = $attribute['option_price'] + $productResult[0]['additional_option_price'];
                            $addToCartProductId = $this->addInventoryProductToCart($paramsArr);
                        }
                    }
                }
                /* End passenger Ship Record */

                /* Add related Product */
                $isRelatedProduct = $request->getPost('is_related_product');
                $relatedProductIds = $request->getPost('related_product_name_ids');
                if ($isRelatedProduct != 0 && !empty($isRelatedProduct) && !empty($relatedProductIds)) {
                    $relatedProductIds = explode(',', $relatedProductIds);
                    foreach ($relatedProductIds as $relatedProductId) {
                        $paramsArr = array();
                        $paramsArr['productId'] = $relatedProductId;
                        $paramsArr['user_id'] = $this->decrypt($postArr['user_id']);
                        $paramsArr['item_type'] = '4';
                        $this->addInventoryProductToCart($paramsArr);
                    }
                }
                /* End Add related Product */

                /* Manifest code */
                $isManifest = $request->getPost('is_manifest');
                if ($isManifest != 0 && !empty($isManifest)) {



                    $purchasedManifestImages = array();
                    if ($request->getPost('is_first_image') != '')
                        $purchasedManifestImages[] = $request->getPost('is_first_image');
                    if ($request->getPost('is_second_image') != '')
                        $purchasedManifestImages[] = $request->getPost('is_second_image');
                    $additionalImage = $request->getPost('additional_images');
                    foreach ($additionalImage as $key => $val) {
                        $purchasedManifestImages[] = $val;
                    }
                    //  asd($purchasedManifestImages);
                    /* commented on 23 Dec 2014
                      $isFirstImage = $request->getPost('is_first_image');
                      $isSecondImage = $request->getPost('is_second_image');
                      $additionalImage = $request->getPost('additional_images');
                     */

                    $isFirstImage = $purchasedManifestImages[0];
                    unset($purchasedManifestImages[0]);
                    if (isset($purchasedManifestImages[1]) && $request->getPost('manifestType') == "TwoPage") {
                        $isSecondImage = $purchasedManifestImages[1];
                        unset($purchasedManifestImages[1]);
                    }

                    $additionalImage = $purchasedManifestImages;
                    // asd($additionalImage);
                    $passengerId = $request->getPost('passenger_id');
                    $searchParam['passenger_id'] = $passengerId;
                    /* $manifestResult = array();
                      $manifestResult = $passengerTable->getPassengerManifest($searchParam); */
                    $firstImageName = '';
                    $secondImageName = '';
                    $additionalImageName = '';
                    $firstImagePrice = 0;
                    $secondImagePrice = 0;
                    $additionalImagePrice = 0;
                    if (!empty($isFirstImage) && !empty($isSecondImage)) {
                        $firstImageName = $isFirstImage;
                        $secondImageName = $isSecondImage;
                    } else if (empty($isFirstImage)) {
                        $firstImageName = $isSecondImage;
                        $isSecondImage = '';
                    } else if (!empty($isFirstImage)) {
                        $firstImageName = $isFirstImage;
                    }

                    if (!empty($additionalImage)) {
                        $additionalImageName = implode(',', $additionalImage);
                    }
                    if (!empty($firstImageName) || !empty($secondImageName) || !empty($additionalImageName)) {
                        $productMenifestParam = array();
                        $productMenifestParam['productType'] = '7';/** manifest */
                        $productMenifestResult = $passengerTable->getProductAttributeDetails($productMenifestParam);
                        $firstImageOptionId = '';
                        $secondImageOptionId = '';
                        $priceArr = array();
                        if (!empty($productMenifestResult)) {
                            $i = 1;
                            foreach ($productMenifestResult as $productOptions) {

                                $price = $productOptions['product_sell_price'] + $productOptions['additional_option_price'];
                                $priceArr[$productOptions['attribute_option_id']] = $price;
                                $i++;
                            }
                        }
                        arsort($priceArr);
                        $i = 1;
                        if (!empty($priceArr)) {
                            foreach ($priceArr as $optionId => $price) {
                                if ($optionId == 3) {
                                    $firstImagePrice = $price;
                                    $firstImageOptionId = $optionId;
                                } else if ($optionId == 4) {
                                    $secondImagePrice = $price;
                                    $secondImageOptionId = $optionId;
                                }

                                $i++;
                            }
                        }

                        $firstImagePrice = (!empty($priceArr[3])) ? $priceArr[3] : 0;
                        $additionalImagePrice = (!empty($priceArr[3])) ? $priceArr[3] : 0;
                        $secondImagePrice = (!empty($priceArr[4])) ? $priceArr[4] : 0;

                        if (!empty($additionalImage)) {
                            $additionalImagePrice = count($additionalImage) * $firstImagePrice;
                        }

                        $firstImagePrice = (!empty($firstImageName)) ? $firstImagePrice : 0;
                        $secondImagePrice = (!empty($secondImageName)) ? $secondImagePrice : 0;
                        $additionalImagePrice = (!empty($additionalImageName)) ? $additionalImagePrice : 0;
                        $firstImageOptionId = (!empty($firstImageName)) ? $firstImageOptionId : '';
                        $secondImageOptionId = (!empty($secondImageName)) ? $secondImageOptionId : '';
                        $paramsArr = array();
                        $paramsArr['product_type_id'] = 7;
                        $paramsArr['productId'] = $productMenifestResult[0]['product_id'];
                        $paramsArr['item_id'] = $passengerDocumentFormArr['passenger_id'];
                        $paramsArr['item_name'] = $passengerDocumentFormArr['passenger_name'];
                        $paramsArr['item_info'] = $passengerDocumentFormArr['item_info'];

                        $paramsArr['item_type'] = '2';
                        $paramsArr['user_id'] = $this->decrypt($passengerDocumentFormArr['user_id']);
                        $paramsArr['item_price'] = $firstImagePrice + $secondImagePrice + $additionalImagePrice;

//$addToCartProductId = $this->addInventoryProductToCart($paramsArr);

                        /* Add to cart additional product */
                        $additionalProduct = array();
                        //$additionalImage = $request->getPost('additional_images');
                        //$additionalImage = $request->getPost('additional_images');
                        if (isset($isFirstImage) && !empty($isFirstImage)) {
                            $paramsArr['manifest_info'] = $isFirstImage;
                            $paramsArr['product_attribute_option_id'] = $firstImageOptionId;
                            $paramsArr['manifest_type'] = 1;
                            $paramsArr['item_price'] = $firstImagePrice;

                            $addToCartProductId = $this->addInventoryProductToCart($paramsArr);
                        }
                        if (isset($isSecondImage) && !empty($isSecondImage)) {
                            $paramsArr['manifest_info'] = $isSecondImage;
                            $paramsArr['product_attribute_option_id'] = $secondImageOptionId;
                            $paramsArr['manifest_type'] = 2;
                            $paramsArr['item_price'] = $secondImagePrice;
                            $addToCartProductId = $this->addInventoryProductToCart($paramsArr);
                        }
// manifest Admin
                        if (isset($additionalImage) && !empty($additionalImage)) {
                            foreach ($additionalImage as $key => $val) {
                                $paramsArr['manifest_info'] = $val;
                                $paramsArr['product_attribute_option_id'] = $firstImageOptionId;
                                $paramsArr['manifest_type'] = 3;
                                $paramsArr['item_price'] = $additionalImagePrice;
                                $addToCartProductId = $this->addInventoryProductToCart($paramsArr);
                            }
                        }


                        /* Add to cart additional product */
                    }
                }
                /* End Manifest code */

                $messages = array('status' => "success");
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
    }

    /**
     * This Action is used to get donation add to cart detail
     * @param Void
     * @return String
     * @author Icreon Tech - DT
     */
    public function getDonationAddToCartFrontAction() {
        //$this->checkFrontUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $amount = 0;
        if ($request->isPost()) {
            $donateamount = $request->getPost('donateamount');
            $amount = trim($donateamount, '$');
        }
        $this->getTransactionTable();

        $data = array();
        $data[0] = '';
        $data[1] = '';
        $data[3] = 'asc';
        $data[2] = 'tbl_membership.minimun_donation_amount';
        $membershipData = $this->getServiceLocator()->get('User\Model\MembershipTable')->getAllMembership($data);

        $membership = array(' ' => 'Select');
        foreach ($membershipData as $key => $value) {
            if ($value['membership_id'] != '1' && $value['minimun_donation_amount'] > 0) {
                $membership[$value['minimun_donation_amount']] = $value['membership_title'] . ' - $' . $value['minimun_donation_amount'];
            }
        }
        $membership['other'] = 'Other donation amount';
        $donationAddToCartForm = new DonationAddToCartForm();
        $donationNotifyForm = new DonationNotifyForm();
        $createTransactionMessage = $this->_config['transaction_messages']['config']['donate_transaction_front'];
        $transaction = new Transaction($this->_adapter);
        $donationAddToCartForm->add($donationNotifyForm);
        $donationAddToCartForm->get('amount')->setAttributes(array('options' => $membership));
        $viewModel = new ViewModel();
        $programArray = $this->getTransactionTable()->getProgramWithCampaign(array());

//        $program_list = array();
//        $campaign_list = array(' ' => 'Select');
//        if (!empty($programArray)) {
//            $i = 0;
//            foreach ($programArray as $key => $val) {
//                $program_list[$val['program_id']] = $val['program'];
//            }
//        }
//
//        $donationAddToCartForm->get('program_id')->setAttributes(array('options' => $program_list));
        $campaignSession = new Container('campaign');
        $campCode = $campaignSession->ccd;

        $program_id = '';
        $campaign_id = '';
        if (!empty($campCode)) {
            $campaignDetail = $this->getServiceLocator()->get('Campaign\Model\CampaignTable')->getCampaignId(array('campaign_code' => $campCode));




            if (!empty($campaignDetail)) {
                $program_id = $campaignDetail['program_id'];
                $donationAddToCartForm->get('program_id')->setAttributes(array('value' => $program_id));
//$campaign_list[$campaignDetail['campaign_id']] = $campaignDetail['campaign_title'];
                $campaign_id = $campaignDetail['campaign_id'];

                $getSearchArray['program_id'] = $program_id;
                $campaignListArray = $this->getTransactionTable()->getCampaignByProgram($getSearchArray);
                if (!empty($campaignListArray)) {
                    foreach ($campaignListArray as $key => $val) {
                        $campaign_list[$val['campaign_id']] = $val['campaign_title'];
                    }
                }
            }
        } else {
            $donationAddToCartForm->get('program_id')->setAttributes(array('value' => 0001));
        }
        /* $donationAddToCartForm->get('campaign_id_front')->setAttributes(array('options' => $campaign_list));
          $donationAddToCartForm->get('campaign_id_front')->setAttributes(array('value' => $campaign_id)); */
        $donationAddToCartForm->get('amount')->setAttributes(array('value' => $amount));
        $postData[0] = '';
        $postData[1] = '';
        $postData[3] = 'asc';
        $postData[2] = 'tbl_membership.minimun_donation_amount';
        $membershipData = $this->getServiceLocator()->get('User\Model\MembershipTable')->getAllMembership($postData);
        $i = 0;
        foreach ($membershipData as $key) {
            if ($key['membership_id'] != '1' && $key['minimun_donation_amount'] > 0) {
                if ($i == 0) {
                    $min_donation = $key['minimun_donation_amount'];
                } else {
                    if ($min_donation > $key['minimun_donation_amount']) {
                        $min_donation = $key['minimun_donation_amount'];
                    }
                }
                $i++;
            }
        }
        $viewModel->setVariables(array(
            'jsLangTranslate' => array_merge($createTransactionMessage, $this->_config['transaction_messages']['config']['common_message'], $this->_config['transaction_messages']['config']['create_crm_transaction']),
            'donationAddToCartForm' => $donationAddToCartForm,
            'minDonation' => $min_donation
        ));
        return $viewModel;
    }

    /**
     * This Action is used to get programs by autosuggest
     * @return Array of search result in json format
     * @param $getSearchArray
     * @author Icreon Tech-AS
     */
    public function getCampaignProgramAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getTransactionTable();
        $donationAddToCartForm = new DonationAddToCartForm();
        if ($request->isPost()) {
            $campaign_list = array('' => 'Select');
            $getSearchArray['program_id'] = $request->getPost('program_id');
            $campaignListArray = $this->getTransactionTable()->getCampaignByProgram($getSearchArray);

            if (!empty($campaignListArray)) {
                foreach ($campaignListArray as $key => $val) {
                    $campaign_list[$val['campaign_id']] = $val['campaign_title'];
                }
            }
            $donationAddToCartForm->get('campaign_id_front')->setAttributes(array('options' => $campaign_list));
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'donationAddToCartForm' => $donationAddToCartForm
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to add more notify
     * @param void
     * @return void
     * @author Icreon Tech - DT
     */
    public function addMoreNotifyFrontAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $totalNotify = $request->getPost('totalNotify');
//$this->checkUserAuthenticationAjx();
            $this->getTransactionTable();
            $createTransactionMessage = $this->_config['transaction_messages']['config']['donate_transaction_front'];
            $donationNotifyForm = new DonationNotifyForm();
            $nameId = 'name_' . ($totalNotify + 1);
            $donationNotifyForm->get('name[]')->setAttributes(array('id' => $nameId));
            $emailId = 'email_' . ($totalNotify + 1);
            $donationNotifyForm->get('email[]')->setAttributes(array('id' => $emailId));
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => $createTransactionMessage,
                'donationNotifyForm' => $donationNotifyForm,
                'totalNotify' => $totalNotify
            ));
            return $viewModel;
        } else {
            return $this->redirect()->toRoute('get-crm-transactions');
        }
    }

    /**
     * This action is used to add donation to cart from front end
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function addToCartDonationFrontCmsAction() {
        //$this->checkFrontUserAuthentication();
        $this->getTransactionTable();
        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $transaction = new Transaction($this->_adapter);
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();

        if (isset($params['amount']) and trim($params['amount']) != "") {
            $donationAddToCartFormArr = array();
            $prodMappArr = array(1);
            $getProductMappingTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductsMappingType($prodMappArr);
            $productParam = array();
            $productParam['productType'] = $getProductMappingTypes[0]['product_type_id'];/** product */
            $productResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getProductAttributeDetails($productParam);
            $donationAddToCartFormArr['product_type_id'] = $getProductMappingTypes[0]['product_type_id'];
            $donationAddToCartFormArr['product_id'] = $productResult[0]['product_id'];
            $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : 0;
            $donationAddToCartFormArr['user_id'] = $user_id;
            $donationAddToCartFormArr['user_session_id'] = session_id();
            $donationAddToCartFormArr['added_by'] = '';
            $donationAddToCartFormArr['added_date'] = DATE_TIME_FORMAT;
            $donationAddToCartFormArr['modify_by'] = '';
            $donationAddToCartFormArr['modify_date'] = DATE_TIME_FORMAT;
            $donationAddToCartFormArr['campaign_id'] = '';
            $donationAddToCartFormArr['campaign_id_front'] = '';
            $donationAddToCartFormArr['amount'] = trim($params['amount']);
            /* add campaign,promotion and user group id in cart start */
            $promotionId = "";
            $couponCode = "";
            $user_group_id = "";

            $this->campaignSession = new Container('campaign');
            $campaign_code = (isset($this->campaignSession->campaign_code) && !empty($this->campaignSession->campaign_code) ? $this->campaignSession->campaign_code : "");

            $campaign_id = isset($this->campaignSession->campaign_id) && !empty($this->campaignSession->campaign_id) ? $this->campaignSession->campaign_id : 0;
            $channel_id = (isset($this->campaignSession->channel_id) && !empty($this->campaignSession->channel_id) ? $this->campaignSession->channel_id : 0);

            if ($campaign_id != 0 && $channel_id != 0) {
                $campResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getChannelPromo(array('campaign_id' => $campaign_id, 'channel_id' => $channel_id));

                if (isset($campResult[0]['promotion_id']) && !empty($campResult[0]['promotion_id'])) {
                    $couponCode = $campResult[0]['coupon_code'];
                    $promotionId = $campResult[0]['promotion_id'];
                } else {
                    $couponCode = "";
                    $promotionId = 0;
                }

                $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '';
                if (isset($user_id) && $user_id != '') {
                    $searchParam['user_id'] = $user_id;
                    $searchParam['campaign_id'] = $campaign_id;
                    $cmTable = $this->getServiceLocator()->get('Campaign\Model\CampaignTable');
                    $campaign_arr = $cmTable->getUserCampaigns($searchParam);
                    if ($campaign_arr && isset($campaign_arr[0]['group_id'])) {
                        $user_group_id = $campaign_arr[0]['group_id'];
                    }
                    //echo "<pre>";var_dump($campaign_arr);die;
                }
            }

            /* add campaign,promotion and user group id in cart ends */
            $donationAddToCartFormArr['campaign_id_front'] = $campaign_id;
            $addToCartDonationArr = $transaction->getAddToCartDonationArr($donationAddToCartFormArr);

            $addToCartDonationArr[] = $campaign_code;
            $addToCartDonationArr[] = $promotionId;
            $addToCartDonationArr[] = $couponCode;
            $addToCartDonationArr[] = $user_group_id;

            $donationAddToCartId = $this->_transactionTable->addToCartDonation($addToCartDonationArr);

            $messages = array('status' => "success");
        } else {
            $messages = array('status' => "error");
        }

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => $msgArr,
            'messages' => $messages
        ));
        return $viewModel;
    }

    /**
     * This function is used to encrypt text
     * @return     encrypted text
     * @author Icreon Tech - NS
     */
    public function donateEncryptTextAction() {
        try {
            $this->getConfig();
            $request = $this->getRequest();
            $decryptText = $request->getPost('decryptText');
            if (isset($decryptText) and trim($decryptText) != "") {
                $encryptText = $this->encrypt('/add-to-cart-donation-front-cms/' . trim($decryptText));
            } else {
                $encryptText = '';
            }
            echo $encryptText;
        } catch (Exception $e) {
            echo '';
        }
        exit();
    }

    /**
     * This action is used to add donation to cart from front end
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function addToCartDonationFrontAction() {
        //$this->checkFrontUserAuthentication();
        $this->getTransactionTable();
        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $transaction = new Transaction($this->_adapter);

        $donationAddToCartForm = new DonationAddToCartForm();
        $donationNotifyForm = new DonationNotifyForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $donationAddToCartForm->setData($request->getPost());
            $donationAddToCartForm->setInputFilter($transaction->getInputFilterAddToCartDonationFront());
            if (!$donationAddToCartForm->isValid()) {
                $errors = $donationAddToCartForm->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'continue') {
                        foreach ($row as $typeError => $rower) {
                            if (($key != 'other_amount' && $typeError != 'notInArray') || ($key == 'other_amount' && $request->getPost('amount') == 'other' && !in_array($typeError, array('notInArray', 'isEmpty')))) {
                                $msg [$key] = $msgArr[$rower];
                            }
                        }
                    }
                }
            }
            if (!empty($msg)) {
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $postArr = $request->getPost()->toArray();
                $donationAddToCartFormArr = $postArr;
                $prodMappArr = array(1);
                $getProductMappingTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductsMappingType($prodMappArr);
                $productParam = array();
                $productParam['productType'] = $getProductMappingTypes[0]['product_type_id'];/** product */
                $productResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getProductAttributeDetails($productParam);
                $donationAddToCartFormArr['product_type_id'] = $getProductMappingTypes[0]['product_type_id'];
                $donationAddToCartFormArr['product_id'] = $productResult[0]['product_id'];
                $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : 0;
                $donationAddToCartFormArr['user_id'] = $user_id;
                $donationAddToCartFormArr['user_session_id'] = session_id();
                $donationAddToCartFormArr['added_by'] = '';
                $donationAddToCartFormArr['added_date'] = DATE_TIME_FORMAT;
                $donationAddToCartFormArr['modify_by'] = '';
                $donationAddToCartFormArr['modify_date'] = DATE_TIME_FORMAT;
                $donationAddToCartFormArr['campaign_id'] = !empty($postArr['campaign_id_front']) ? $postArr['campaign_id_front'] : '';
                $donationAddToCartFormArr['campaign_id_front'] = !empty($postArr['campaign_id_front']) ? $postArr['campaign_id_front'] : '';
                $donationAddToCartFormArr['amount'] = ($postArr['amount'] == 'other') ? $postArr['other_amount'] : $postArr['amount'];

                /* add campaign,promotion and user group id in cart start */
                $promotionId = "";
                $couponCode = "";
                $user_group_id = "";

                $this->campaignSession = new Container('campaign');
                $campaign_code = (isset($this->campaignSession->campaign_code) && !empty($this->campaignSession->campaign_code) ? $this->campaignSession->campaign_code : "");

                $campaign_id = isset($this->campaignSession->campaign_id) && !empty($this->campaignSession->campaign_id) ? $this->campaignSession->campaign_id : 0;
                $channel_id = (isset($this->campaignSession->channel_id) && !empty($this->campaignSession->channel_id) ? $this->campaignSession->channel_id : 0);

                if ($campaign_id != 0 && $channel_id != 0) {
                    $campResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getChannelPromo(array('campaign_id' => $campaign_id, 'channel_id' => $channel_id));

                    if (isset($campResult[0]['promotion_id']) && !empty($campResult[0]['promotion_id'])) {
                        $couponCode = $campResult[0]['coupon_code'];
                        $promotionId = $campResult[0]['promotion_id'];
                    } else {
                        $couponCode = "";
                        $promotionId = 0;
                    }

                    $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '';
                    if (isset($user_id) && $user_id != '') {
                        $searchParam['user_id'] = $user_id;
                        $searchParam['campaign_id'] = $campaign_id;
                        $cmTable = $this->getServiceLocator()->get('Campaign\Model\CampaignTable');
                        $campaign_arr = $cmTable->getUserCampaigns($searchParam);
                        if ($campaign_arr && isset($campaign_arr[0]['group_id'])) {
                            $user_group_id = $campaign_arr[0]['group_id'];
                        }
                        //echo "<pre>";var_dump($campaign_arr);die;
                    }
                }

                //echo "<pre>";print_r($addToCartProductArr);die;
                /* add campaign,promotion and user group id in cart ends */
                $donationAddToCartFormArr['campaign_id_front'] = $campaign_id;
                $addToCartDonationArr = $transaction->getAddToCartDonationArr($donationAddToCartFormArr);

                $addToCartDonationArr[] = $campaign_code;
                $addToCartDonationArr[] = $promotionId;
                $addToCartDonationArr[] = $couponCode;
                $addToCartDonationArr[] = $user_group_id;

                $donationAddToCartId = $this->_transactionTable->addToCartDonation($addToCartDonationArr);
                $donationAddToCartFormArr['cart_date'] = DATE_TIME_FORMAT;
// $addToCartProductArr = $transaction->getAddToCartProductArr($donationAddToCartFormArr);
// $productAddToCartId = $this->_transactionTable->addToCartProduct($addToCartProductArr);
                if (!empty($postArr['email'])) {
                    $donationNotifyArr = array();
                    $donationNotifyArr['donation_product_id'] = $donationAddToCartId;
                    $donationNotifyArr['added_by'] = $user_id;
                    $donationNotifyArr['added_date'] = DATE_TIME_FORMAT;
                    $donationNotifyArr['modified_by'] = $user_id;
                    $donationNotifyArr['modified_date'] = DATE_TIME_FORMAT;
                    $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $user_id));
// $programDetail = $this->getServiceLocator()->get('Setting\Model\ProgramTable')->getProgram(array('program_id' => $donationAddToCartFormArr['program_id']));
                    $userName = ($userDetail['user_type'] == '1') ? $userDetail['first_name'] . " " . $userDetail['last_name'] : $userDetail['company_name'];
                    foreach ($postArr['email'] as $index => $notifyemail) {
                        if (!empty($notifyemail) && $notifyemail != 'Email') {
                            $notifyName = (trim($postArr['firstname'][$index]) != '') ? $postArr['firstname'][$index] : "Guest";
                            $notifyLastName = (trim($postArr['lastname'][$index]) != '') ? $postArr['lastname'][$index] : "";
                            $donationNotifyArr['donarName'] = $userName;
                            $donationNotifyArr['first_name'] = $postArr['firstname'][$index];
                            $donationNotifyArr['last_name'] = $postArr['lastname'][$index];
                            $donationNotifyArr['name'] = $notifyName . ' ' . $notifyLastName;
                            $donationNotifyArr['email_id'] = $notifyemail;
                            $donationNotifyArr['amount'] = $donationAddToCartFormArr['amount'];
// $donationNotifyArr['program_name'] = $programDetail[0]['program'];
                            $addToCartDonationNotifyArr = $transaction->getAddToCartDonationNotifyArr($donationNotifyArr);
                            $donationNotifyAddToCartId = $this->_transactionTable->addToCartDonationNotify($addToCartDonationNotifyArr);
                            $email_id_template_int = 15;
                            $donationNotifyArr['admin_email'] = $this->_config['soleif_email']['email'];
                            $donationNotifyArr['admin_name'] = $this->_config['soleif_email']['name'];
                            if ($postArr['email'][0] != 'Email') {
//$this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($donationNotifyArr, $email_id_template_int, $this->_config['email_options']);
                            }
                        }
                    }
                }
                $messages = array('status' => "success");
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
    }

    /**
     * This action is used to show ship image
     * @return json
     * @param void
     * @author Icreon Tech - AS
     */
    public function showShipImageAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $this->getTransactionTable();
        $this->getConfig();
        $viewModel = new ViewModel();
        $imageName = '';
        $jsLangTranslate = $this->_config['transaction_messages']['config']['ship_search'];
        $params = $this->params()->fromRoute();
        $paramShipNameO = (isset($params['ship_name']) and trim($params['ship_name']) != "") ? $this->decrypt(trim($params['ship_name'])) : "";
        $flagValue = isset($params['flag']) ? $this->decrypt($params['flag']) : "0";
        $arriveDate = (isset($params['arrive_date']) and trim($params['arrive_date']) != "" and $this->decrypt(trim($params['arrive_date'])) != "0") ? $this->decrypt(trim($params['arrive_date'])) : "";
        if (isset($arriveDate) and trim($arriveDate) != "") {
            $arriveDateEx = explode('/', $arriveDate);
            $arriveDate = $arriveDateEx[2] . '-' . $arriveDateEx[0] . '-' . $arriveDateEx[1];
        } else {
            $arriveDate = "";
        }
        $search['product_id'] = $this->decrypt($params['id']);
        $shipDetails = $this->getTransactionTable()->getProductsDetails($search);
        $searchResult = $this->getTransactionTable()->getShipImage($search);
        $searchParam['ship_id'] = $searchResult[0]['item_id'];
        $searchParam['asset_for'] = '1';
        $searchShipImageResult = $this->getServiceLocator()->get('Passenger\Model\DocumentTable')->searchCrmShipImage($searchParam);
        $shipResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getShipDetails($searchParam);
        $searchShipLineParam = array();
        if (count($shipResult) > 0) {
            $shipResult = $shipResult[0];
            $shipID = $shipResult['SHIPID'];
            $searchShipLineParam['ship_id'] = $shipID;
            $shipLineResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getShipLineDetails($searchShipLineParam);
        }
        $imageName = $shipResult['ASSETVALUE'];
        if ($_SERVER['REMOTE_ADDR'] == $this->_config['file_upload_path']['local_internet_ip'] && $searchShipImageResult[0]['is_edited_image'] == 1)
            $shipImage = $this->_config['file_upload_path']['local_ship_image_path'] . '/' . $imageName;
        else
            $shipImage = $this->_config['Image_path']['shipPath'] . '/' . $imageName;

        /* if ($shipResult['NUMSCREWS'] == 1)
          $shipResult['NUMSCREWS'] = 'Single';
          else if ($shipResult['NUMSCREWS'] == 2)
          $shipResult['NUMSCREWS'] = 'Double';
          else if ($shipResult['NUMSCREWS'] == 3)
          $shipResult['NUMSCREWS'] = 'Triple';
         */
        $screwNameArray = $this->screwNameArray();
        $shipResult['NUMSCREWS'] = $screwNameArray[$shipResult['NUMSCREWS']];

        $lineLessTransTypeArray = $this->lineLessTransTypeArray();
        $howNameChangeArray = $this->howNameChangeArray();
        $transTypeArray = $this->transTypeArray();

//// Total Count - start ////////

        $searchShipDetailParamCount = array();
        $searchShipDetailParamCount['ship_name'] = $paramShipNameO;
        $searchShipDetailParamCount['DATE_ARRIVE'] = $arriveDate . ' 00:00:00';

        $passCountResult = $this->getServiceLocator()->get('Passenger\Model\DocumentTable')->getShipPassengerListCount($searchShipDetailParamCount);
        $passTotalCount = 0;
//  if (count($passCountResult) > 0) {
//     $passTotalCount = $passCountResult[0]['cnt'];
//  }
        $passTotalCount = $shipResult['PASSENGERSFIRST'] + $shipResult['PASSENGERSSECOND'] + $shipResult['PASSENGERSTHIRD'];

//// Total Count - end /////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $protocol = ($_SERVER['HTTPS'] == 'on')?'https://':'http://';
		if ($flagValue == "1") {
            $str = '<page>';
            if ($shipDetails[0]['attribute_option_id'] == '1') {
                $format = "Letter";
                $str = $str . '<table style="width: 100%; padding-top:150px" align="center" ><tr><td><img height="450" width="630" src="' . $shipImage . '" align="middle"/>';
            } else {
                $format = array(17 * 25.4, 11 * 25.4);
                $str = $str . '<table style="width: 100%; padding-top:15px" align="center" ><tr><td><img height="811" width="1080" src="' . $shipImage . '" align="middle"/>';
            }
            $str = $str . '</td></tr></table>
                    <page_footer><table style="width: 100%;" align="center"><tr><td><div style="text-align:center;">
                        <span style="font-size:37px; font-family:Times New Roman; font-weight:bold;">' . $paramShipNameO . '</SPAN>
                        <br><br>
                        <span><img src="' . $protocol. $_SERVER['HTTP_HOST'] . '/images/pdf_logo1.jpg" /></span></div></td></tr></table>
                        </page_footer></page>';
        } else if ($flagValue == "2") {
            $format = "A4";
            $str = '<page backtop="-20mm" backbottom="47mm"><page_header></page_header>
                        <page_footer><table style="width: 100%;" align="center"><tr><td><div style="text-align:center;">
                        <span style="font-size:37px; font-family:Times New Roman; font-weight:bold;">' . $paramShipNameO . '</SPAN>
                        <br><br>
                        <span><img src="' . $protocol. $_SERVER['HTTP_HOST'] . '/images/pdf_logo1.jpg" /></span></div></td></tr></table>
                        </page_footer>
                <table style="width: 100%; padding-top:120px;" align="center">
                <tr><td style="font-size:27px; font-family:Times New Roman;"><em>' . $paramShipNameO . '</em></td></tr><tr><td style="padding-top: -10px;"><hr></td></tr>
                <tr><td style="font-size:24px; font-family:Times New Roman; padding-top:25px"><em>' . $jsLangTranslate['L_ABOUT_SHIP'] . '</em></td></tr>
                <tr><td style="padding-top:32px; font-size:18px; font-family:Times New Roman;">' . $jsLangTranslate['L_BUILT_BY'] . ' ' . $shipResult['BUILDER'] . ', ' . $shipResult['BUILDCITY'] . ', ' . $shipResult['BUILDCOUNTRY'] . ', '
                    . $shipResult['BUILDYEAR'] . '. ' . number_format($shipResult['GROSSTONS']) . ' ' . $jsLangTranslate['L_GROSS_TONS'] . '; ' . $shipResult['FEETLONG'] . ' ' .
                    $jsLangTranslate['L_FEET_LONG'] . '; ' . $shipResult['BEAM'] . ' ' . $jsLangTranslate['L_FEET_WIDE'] . '. ' . $shipResult['engine_type'] . ' ' .
                    $jsLangTranslate['L_ENGINE'] . ', ' . $shipResult['NUMSCREWS'] . ' ' . $jsLangTranslate['L_SCREW'] . '. ' . $jsLangTranslate['L_SERVICE_SPEED'] . ' ' .
                    $shipResult['SERVICESPEEDKNOTS'] . ' ' . $jsLangTranslate['L_KNOTS'] . '.';


            $passengerCountStr = '';
            $passengerCountArr = array();
            $passengerFirstN = (!empty($shipResult['PASSENGERSFIRST'])) ? $shipResult['PASSENGERSFIRST'] . ' first class' : '';
            $passengerSecondN = (!empty($shipResult['PASSENGERSSECOND'])) ? $shipResult['PASSENGERSSECOND'] . ' second class' : '';
            $passengerThirdN = (!empty($shipResult['PASSENGERSTHIRD'])) ? $shipResult['PASSENGERSTHIRD'] . ' third class' : '';

            if ($passengerFirstN != "") {
                array_push($passengerCountArr, $passengerFirstN);
            }
            if ($passengerSecondN != "") {
                array_push($passengerCountArr, $passengerSecondN);
            }
            if ($passengerThirdN != "") {
                array_push($passengerCountArr, $passengerThirdN);
            }

            if (count($passengerCountArr) > 0) {
                $passengerCountStr = ' (' . implode(', ', $passengerCountArr) . ').';
            }


            if ($passTotalCount > 0) {
                $str .= ' ' . number_format($passTotalCount) . ' ' . $jsLangTranslate['L_PASSENGERS'];
                if (!empty($shipResult['PASSENGERSFIRST']) || !empty($shipResult['PASSENGERSSECOND']) || !empty($shipResult['PASSENGERSTHIRD'])) {
                    $str .= $passengerCountStr . ' ';
                }
                $str .=$shipResult['NOTE'];
            }
            $str .= '</td></tr>';
            $lastService = "-"; //
            $lastHowLeft = "Built for "; //
            $lastHowLeftType = -1; //

            if (count($shipLineResult) > 0) {
                if (isset($shipLineResult[0])) {
                    $str = $str . '<tr><td style="font-size:24px; font-family:Times New Roman;  padding-top:57px"><em>Ship History</em></td></tr>';
                    /*
                      if (isset($shipLineResult[0]['line_name']))
                      $str = $str . $jsLangTranslate['L_BUILT_FOR'];
                      $str = $str . ' ' . @$shipLineResult[0]['line_name'];
                      if ((!empty($shipLineResult[0]['FLAG'])))
                      $str = $str . ', ' . $shipLineResult[0]['FLAG'] . ' flag';
                      if ((!empty($shipLineResult[0]['STARTYEAR'])))
                      $str = $str . ', ' . 'in ' . $shipLineResult[0]['STARTYEAR'];
                      if (isset($shipLineResult[0]['SHIPNAME']))
                      $str = $str . ', ' . $jsLangTranslate['L_AND_NAMED'];
                      if (isset($shipLineResult[0]['SHIPNAME']))
                      $str = $str . ', ' . @$shipLineResult[0]['SHIPNAME'] . '. ';
                      if (isset($shipLineResult[0]['NOTE']) && !empty($shipLineResult[0]['NOTE']))
                      $str = $str . ' ' . $shipLineResult[0]['NOTE'] . '. ';
                     */
                    $i = 0;
                    foreach ($shipLineResult as $key => $val) {
                        $str = $str . '<tr><td style="padding-top:25px; font-size:18px; font-family:Times New Roman;">';
                        if ($lastHowLeftType != 14) {
                            if (0 == $val['LINEID']) {
                                if (-1 == $lastHowLeftType)
                                    $str.= "Built ";
                                else
                                    $str.= "" . $lineLessTransTypeArray[$lastHowLeftType];
                            } else {
                                $str.= "" . $lastHowLeft;
                                $str.= "" . $val['line_name'] . ",";
                            }
                            if ($val['FLAG'] != "") {
                                if ($html == "S") {
                                    $str.= "&nbsp;" . $val['FLAG'] . " flag,";
                                } else {
                                    $str.= " " . $val['FLAG'] . " flag,";
                                }
                            }
                            if ($val['STARTYEAR'] > 0) {
                                if ($html == "S") {
                                    $str.= "&nbsp;in " . $val['STARTYEAR'];
                                } else {
                                    $str.= " in " . $val['STARTYEAR'];
                                }
                            }

                            if ($val['HOWNAMECHANGE'] != 4) {
                                $str.= " and " . $howNameChangeArray[$val['HOWNAMECHANGE']] . " ";
                                if ($html == "S") {
                                    $str.= "<b>" . $val['SHIPNAME'] . "</b>";
                                } else {
                                    $str.= "" . $val['SHIPNAME'] . "";
                                }
                            }
                        } else {
                            $str.= "Renamed ";
                            if ($html == "S") {
                                $str.= "<b>" . $val['SHIPNAME'] . "</b>&nbsp;in " . $val['STARTYEAR'];
                            } else {
                                $str.= "" . $val['SHIPNAME'] . " in " . $val['STARTYEAR'];
                            }
                        }
                        $str.= ".  ";
                        if ($val['SERVICE'] != "") {
                            if ($lastService == $val['SERVICE'])
                                $str.= "Also ";
                            $str.= $val['SERVICE'] . " service.  ";
                        }
                        if ($val['NOTE'] != "") {
                            $str.= $val['NOTE'] . ".  ";
                        }
                        $lastHowLeft = $transTypeArray[$val['HOWLEFT']] . " ";
                        $lastHowLeftType = $val['HOWLEFT'];
                        $lastService = $val['SERVICE'];
                        $str.= "</td></tr>";
                    }
                }
            }
            $str = $str . '<tr><td style="font-size:18px; font-family:Times New Roman;"><br>' . $shipResult['SCRAPPEDINCOUNTRY'] . ' in ' . $shipResult['SCRAPPEDYEAR'] . '.</td></tr></table>';
            $str = $str . '</page>';
        }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        try {
            $html2pdf = new\ HTML2PDF('L', $format, 'en');
            $html2pdf->setDefaultFont('Times');
            $html2pdf->writeHTML($str, false);
            $html2pdf->Output('ship.pdf');
        } catch (HTML2PDF_exception $e) {
            echo $e;
            exit;
        }
    }

    /**
      /**
     * This action is used to show fof image
     * @return json
     * @param void
     * @author Icreon Tech - AS
     */
    public function viewFofImageAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $fofImageForm = new ViewFofImageForm();
        $messages = array();
        $this->getConfig();
        $messages = $this->_config['transaction_messages']['config']['view_crm_transaction'];
        $upload_file_path = $this->_config['file_upload_path'];
        $this->getTransactionTable();
        $this->getConfig();
        $viewModel = new ViewModel();
        $this->layout('popup');
        $params = $this->params()->fromRoute();
        $search['fof_id'] = $params['id'];
        $searchResult = $this->getServiceLocator()->get('User\Model\UserTable')->getFof($search);
        if ($request->isPost()) {
            $photoModified = $request->getPost('photo_modified');
            $photoModifiedName = $request->getPost('photo_modified_name');



            if (isset($photoModifiedName) and trim($photoModifiedName) != "" and isset($searchResult[0]['fof_image']) and trim($searchResult[0]['fof_image']) != "" and $this->decrypt($photoModifiedName) != $searchResult[0]['fof_image']) {
                $photoModifiedName = $this->decrypt($photoModifiedName);
// copy files  - start
// p - start
                $jsFilePath = $this->_config['js_file_path']['path'];
                $jsVersion = $this->_config['file_versions']['js_version'];
//$temp_dir = $jsFilePath . '/js/pixenate/cache/';
                $temp_dir = dirname($_SERVER['DOCUMENT_ROOT']) . '/public/js/pixenate/cache/';

                $temp_file = $temp_dir . $photoModifiedName;
                $fof_dir_root = $this->_config['file_upload_path']['assets_upload_dir'] . "fof/";
                $fof_dir_thumb = $this->_config['file_upload_path']['assets_upload_dir'] . "fof/thumb/";
                $fof_dir_medium = $this->_config['file_upload_path']['assets_upload_dir'] . "fof/medium/";
                $fof_dir_large = $this->_config['file_upload_path']['assets_upload_dir'] . "fof/large/";
                $fof_filename_large = $fof_dir_large . $searchResult[0]['fof_image'];
                $fof_filename_medium = $fof_dir_medium . $searchResult[0]['fof_image'];
                $fof_filename_thumb = $fof_dir_thumb . $searchResult[0]['fof_image'];
                $fof_filename_root = $fof_dir_root . $searchResult[0]['fof_image'];
                if (isset($temp_file) and trim($temp_file) != "" and file_exists($temp_file) and $photoModified != "2") {
                    if (@copy($temp_file, $fof_filename_root)) {
                        $this->resizeImage($fof_filename_thumb, $temp_file, $this->_config['file_upload_path']['user_fof_medium_width'], $this->_config['file_upload_path']['user_fof_medium_height']);
                        $this->resizeImage($fof_filename_medium, $temp_file, $this->_config['file_upload_path']['user_fof_medium_width'], $this->_config['file_upload_path']['user_fof_medium_height']);
                        $this->resizeImage($fof_filename_large, $temp_file, $this->_config['file_upload_path']['user_fof_large_width'], $this->_config['file_upload_path']['user_fof_large_height']);
                    }
                }
// p - end
                else {
// c - start
                    $temp_dir_fof = dirname($_SERVER['DOCUMENT_ROOT']) . '/public/js/pixenate/cache/'; //$this->_config['file_upload_path']['temp_upload_dir'] . 'fof/';
                    $temp_fof_file = $temp_dir_fof . $photoModifiedName;
                    $fof_dir_root = $this->_config['file_upload_path']['assets_upload_dir'] . "fof/";
                    $fof_dir_thumb = $this->_config['file_upload_path']['assets_upload_dir'] . "fof/thumb/";
                    $fof_dir_medium = $this->_config['file_upload_path']['assets_upload_dir'] . "fof/medium/";
                    $fof_dir_large = $this->_config['file_upload_path']['assets_upload_dir'] . "fof/large/";
                    $fof_filename_large = $fof_dir_large . $searchResult[0]['fof_image'];
                    $fof_filename_medium = $fof_dir_medium . $searchResult[0]['fof_image'];
                    $fof_filename_thumb = $fof_dir_thumb . $searchResult[0]['fof_image'];
                    $fof_filename_root = $fof_dir_root . $searchResult[0]['fof_image'];
                    if (isset($temp_fof_file) and trim($temp_fof_file) != "" and file_exists($temp_fof_file)) {
                        if (@copy($temp_fof_file, $fof_filename_root)) {
                            $this->resizeImage($fof_filename_thumb, $temp_fof_file, $this->_config['file_upload_path']['user_fof_medium_width'], $this->_config['file_upload_path']['user_fof_medium_height']);
                            $this->resizeImage($fof_filename_medium, $temp_fof_file, $this->_config['file_upload_path']['user_fof_medium_width'], $this->_config['file_upload_path']['user_fof_medium_height']);
                            $this->resizeImage($fof_filename_large, $temp_fof_file, $this->_config['file_upload_path']['user_fof_large_width'], $this->_config['file_upload_path']['user_fof_large_height']);
                        }
                    }
// c - end
                }
// copy files  - end
            }
            $params['table_auto_id'] = $search['fof_id'];
            $params['table_type'] = '3';
            $searchProduct = $this->getTransactionTable()->getPaymentReceived($params);
            foreach ($searchProduct as $key => $val) {
                if ($val['payment_mode_id'] == 1 && $val['is_payment_received'] == 0) {
                    $customerProfileId = $val['profile_id'];
                    $createCustomerProfile = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
                    $createCustomerProfile->getCustomerProfileRequest(array('customerProfileId' => $customerProfileId));
                    $paymentProfileId = $createCustomerProfile->profile->paymentProfiles->customerPaymentProfileId;
                    $responseArr = explode(',', $payment->directResponse);
                    $transactionId = $responseArr[6];
                    $payment = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
                    $payment->createCustomerProfileTransactionRequest(array(
                        'transaction' => array(
                            'profileTransPriorAuthCapture' => array(
                                'amount' => $val['amount'],
                                'customerProfileId' => $customerProfileId,
                                'customerPaymentProfileId' => $paymentProfileId,
                                'transId' => $val['authorize_transaction_id']
                            )
                        ),
                        'extraOptions' => '<![CDATA[x_customer_ip=' . $_SERVER['SERVER_ADDR'] . ']]>'
                    ));


                    if ($payment->isSuccessful()) {
                        $updatePaymentArray['is_payment_received'] = 1;
                        $updatePaymentArray['received_payment_id'] = $val['received_payment_id'];
                        $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->updatePaymentReceived($updatePaymentArray);
                    }
                } else {
                    $updatePaymentArray['is_payment_received'] = 1;
                    $updatePaymentArray['received_payment_id'] = $val['received_payment_id'];
                    $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->updatePaymentReceived($updatePaymentArray);
                }
            }
            $approve['id'] = $params['id'];
            $approve['status'] = '1';
            $approve['donated_by'] = $request->getPost('donated_by');
            $approve['people_photo'] = $this->checkJsonFormatReturnString($request->getPost('people_photo'), 1);
            $approve['people_photo_json'] = $request->getPost('people_photo');
            $approve['is_people_photo_visible'] = $request->getPost('is_people_photo_visible');
            $approve['photo_caption'] = $request->getPost('photo_caption');
            $approve['is_photo_caption_visible'] = $request->getPost('is_photo_caption_visible');
            $approve['product_status_id'] = '10';
            $approve['modified_by'] = $this->auth->getIdentity()->crm_user_id;
            $approve['modified_date'] = DATE_TIME_FORMAT;
            $approve['transactionId'] = $searchResult[0]['transaction_id'];
            $this->getTransactionTable()->updateFofDetails($approve);
            $searchProduct = $this->getTransactionTable()->searchTransactionProducts($approve);
            $count = count($searchProduct);
            $transactionStatus = 0;
            $traCount = 0;
            $cancelCount = 0;
            $holdCount = 0;
            $voidedShipCount = 0;
            $returnCount = 0;
            foreach ($searchProduct as $key => $val) {
                if ($val['product_status'] == '6') {
                    $cancelCount++;
                } else if ($val['product_status'] == '7') {
                    $holdCount++;
                } else if ($val['product_status'] == '12') {
                    $voidedShipCount++;
                } else if ($val['product_status'] == '13') {
                    $returnCount++;
                } else {
                    if ($transactionStatus < $val['product_status']) {
                        $traCount = 1;
                        $transactionStatus = $val['product_status'];
                    } else if ($transactionStatus == $val['product_status']) {
                        $traCount++;
                    }
                }
            }
            if ($cancelCount == $count) {
                $transactionStatus = '6';
            } else if ($holdCount == $count) {
                $transactionStatus = '7';
            } else if ($voidedShipCount == $count) {
                $transactionStatus = '12';
            } else if ($returnCount == $count) {
                $transactionStatus = '13';
            } else if (($count > ($traCount + $cancelCount + $voidedShipCount + $returnCount)) && ($transactionStatus == '2' || $transactionStatus == '4' || $transactionStatus == '8' || $transactionStatus == '10')) {
                $transactionStatus++;
            }
            $update['transactionStatus'] = $transactionStatus;
            $update['transactionId'] = $searchResult[0]['transaction_id'];
            $update['modified_by'] = $this->auth->getIdentity()->crm_user_id;
            $update['modified_date'] = DATE_TIME_FORMAT;
            $this->getTransactionTable()->updateTransactionStatus($update);

// n - insert into transaction log - start
            $changeLogArray = array();
            $changeLogArray['table_name'] = 'tbl_tra_change_logs';
            $changeLogArray['activity'] = '2';/** 2 == for update */
            $changeLogArray['id'] = $this->decrypt($params['transactionid']);
            $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
            $changeLogArray['added_date'] = DATE_TIME_FORMAT;
            $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
// n - insert into transaction log - end

            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }

        $countPeoplePhotoJson = "0";
        if (isset($searchResult[0]['people_photo_json']) and trim($searchResult[0]['people_photo_json']) != "") {
            $jsonPeoplePhoto = trim($searchResult[0]['people_photo_json']);
            $arrPeoplePhoto = json_decode($jsonPeoplePhoto);
            $countPeoplePhotoJson = count($arrPeoplePhoto);
        }
        $fofImageForm->get('countTagNum')->setAttribute('value', $countPeoplePhotoJson);
        $fofImageForm->get('photo_modified_name')->setAttribute('value', $this->encrypt($searchResult[0]['fof_image']));
        $fofImageForm->get('fof_id')->setAttribute('value', $this->encrypt($searchResult[0]['fof_id']));
        $fofImageForm->get('donated_by')->setAttribute('value', $searchResult[0]['donated_by']);
        $fofImageForm->get('people_photo')->setAttribute('value', $searchResult[0]['people_photo_json']);
        $fofImageForm->get('is_people_photo_visible')->setAttribute('value', $searchResult[0]['is_people_photo_visible']);
        $fofImageForm->get('photo_caption')->setAttribute('value', $searchResult[0]['photo_caption']);
        $fofImageForm->get('is_photo_caption_visible')->setAttribute('value', $searchResult[0]['is_photo_caption_visible']);
        $fofImage = $upload_file_path['assets_url'] . "fof/" . $searchResult[0]['fof_image'];
        $searchResult[0]['status'] = ($searchResult[0]['product_status_id'] == '6' || $searchResult[0]['product_status_id'] == '7' || $searchResult[0]['product_status_id'] > '11' ? '1' : $searchResult[0]['status'] );
        if ($searchResult[0]['status'] == 1) {
            $fofImageForm->get('donated_by')->setAttribute('readonly', 'readonly');
            $fofImageForm->get('people_photo')->setAttribute('readonly', 'readonly');
            $fofImageForm->get('is_people_photo_visible')->setAttribute('disabled', 'disabled');
            $fofImageForm->get('photo_caption')->setAttribute('readonly', 'readonly');
            $fofImageForm->get('is_photo_caption_visible')->setAttribute('disabled', 'disabled');
        }
        $fofImageForm->get('people_photo')->setAttribute('readonly', 'readonly');

        try {
            $temp_dir_fof_copyfrom = $this->_config['file_upload_path']['assets_upload_dir'] . 'fof/' . $searchResult[0]['fof_image'];
            $temp_dir_copyto = dirname($_SERVER['DOCUMENT_ROOT']) . '/public/js/pixenate/cache/' . $searchResult[0]['fof_image'];
            @copy($temp_dir_fof_copyfrom, $temp_dir_copyto);
            @chmod($temp_dir_copyto, 0777);
        } catch (Exception $e) {

        }

        $viewModel->setVariables(array(
            'peoplePhotoJson' => (isset($searchResult[0]['people_photo_json']) and trim($searchResult[0]['people_photo_json']) != "") ? trim($searchResult[0]['people_photo_json']) : "",
            'fofImage' => $fofImage,
            'searchResult' => $searchResult[0],
            'jsLangTranslate' => $messages,
            'fofImageForm' => $fofImageForm,
            'fof_id' => $search['fof_id'],
            'fof_transaction_id' => $params['transactionid'],
            'encrypted_filename' => $this->encrypt($searchResult[0]['fof_image']),
            'full_path' => $this->encrypt('/js/pixenate/cache/' . $searchResult[0]['fof_image']),
            'preview_path' => $this->encrypt('/js/pixenate/cache/' . $searchResult[0]['fof_image'])
        ));
        return $viewModel;
    }

    /**
      /**
     * This action is used to show fof image on cart
     * @return html
     * @param void
     * @author Icreon Tech - DT
     */
    public function viewFofImageCartAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $fofImageForm = new ViewFofImageForm();
        $messages = array();
        $this->getConfig();
        $messages = $this->_config['transaction_messages']['config']['view_crm_transaction'];
        $upload_file_path = $this->_config['file_upload_path'];
        $this->getTransactionTable();
        $this->getConfig();
        $viewModel = new ViewModel();
        $this->layout('popup');
        $params = $this->params()->fromRoute();
        $search['fof_id'] = $params['id'];
        $searchResult = $this->getServiceLocator()->get('User\Model\UserTable')->getFofCart($search);
        $fofImage = $upload_file_path['assets_url'] . "temp/fof/" . $searchResult[0]['fof_image'];
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'fofImage' => $fofImage,
        ));
        return $viewModel;
    }

    /**
     * This action is used to show manifest
     * @return json
     * @param void
     * @author Icreon Tech - AS
     */
    public function viewManifestAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $isEditedImagesArray = array();
        $this->getTransactionTable();
        $this->getConfig();
        $viewModel = new ViewModel();
        $this->layout('popup');
        $params = $this->params()->fromRoute();
        $params['id'] = $this->decrypt($params['id']);
        $searchResult = $this->getTransactionTable()->getManifestImage($params);
        $image = array();
        $image[] = $searchResult[0]['manifest_info'];
        $imageList = $image[0];
        $imageListParam['image'] = "'" . $imageList . "'";
        $isEditedImagesArray = $this->getTransactionTable()->getManifestImageEditFlag($imageListParam);
        $folderName = strtolower(substr($imageList, 0, 9));
        if ($_SERVER['REMOTE_ADDR'] == $this->_config['file_upload_path']['local_internet_ip'] && $isEditedImagesArray[0]['is_edited_image'] == 1)
            $manifestPath = $this->_config['file_upload_path']['assets_upload_dir'] . 'manifest_image/' . $folderName . '/';
        else {
            $manifestPath = $this->_config['file_upload_path']['assets_upload_dir'] . 'manifest/' . $folderName . '/';
        }

        $fileName = strtolower($imageList);
        $pdf_file_name = $this->_config['file_upload_path']['assets_upload_dir'] . 'temp/manifest/' . substr($fileName, 0, -3) . "pdf";
        $img_file_name = $manifestPath . $fileName;

        $result = $this->convertTifToPDF($img_file_name, $pdf_file_name);

        if ($result) {
            header('Content-type: application/pdf');
            readfile($pdf_file_name);
        }
    }

    /**
     * This action is used to show woh certificate
     * @return json
     * @param void
     * @author Icreon Tech - AS
     */
    public function viewWohCertificateAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $this->getTransactionTable();
        $this->getConfig();
        $messages = $this->_config['transaction_messages']['config']['view_crm_transaction'];
        $viewModel = new ViewModel();
        $this->layout('popup');
        $params = $this->params()->fromRoute();
// $search['woh_id'] = $params['id'];
        $search['product_id'] = $params['id'];
        $mode = 'I';
        $this->certificateSavePdf($search, $mode);
// $searchResult = $this->getServiceLocator()->get('User\Model\UserTable')->getWoh($search);
        $viewModel->setVariables(array(
            'searchResult' => $searchResult[0],
            'jsLangTranslate' => $messages
        ));
        return $viewModel;
    }

    /**
     * This action is used to show woh letter
     * @return json
     * @param void
     * @author Icreon Tech - AS
     */
    public function viewWohLetterAction() {
        $this->getConfig();
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = $this->_config['transaction_messages']['config']['view_crm_transaction'];
        $this->getTransactionTable();
        $viewModel = new ViewModel();
        $this->layout('popup');
        $params = $this->params()->fromRoute();
// $search['woh_id'] = $params['id'];
        $search['product_id'] = $params['id'];
        $mode = 'I';
        $this->letterSavePdf($search, $mode);
// $searchResult = $this->getServiceLocator()->get('User\Model\UserTable')->getWoh($search);
        $viewModel->setVariables(array(
            'searchResult' => $searchResult[0],
            'jsLangTranslate' => $messages
        ));
        return $viewModel;
    }

    /**
     * This action is used to view woh details
     * @return json
     * @param void
     * @author Icreon Tech - AS
     */
    public function viewWohDetailsAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $viewWohDetailsForm = new ViewWohDetailsForm();
        $messages = array();
        $this->getTransactionTable();
        $this->getConfig();
        $messages = $this->_config['transaction_messages']['config']['view_crm_transaction'];
        $viewModel = new ViewModel();
        $this->layout('popup');
        $params = $this->params()->fromRoute();
        $search['woh_id'] = $this->decrypt($params['id']);
        $arrContacts = $this->_transactionTable->getUserWohContact(array('woh_id' => $search['woh_id']));

        if ($request->isPost()) {
            $approve['first_name_one'] = "";
            $approve['other_init_one'] = "";
            $approve['other_name'] = "";
            $approve['last_name_one'] = "";
            $approve['other_init_two'] = "";
            $approve['first_name_two'] = "";
            $approve['last_name_two'] = "";
            $approve['first_line'] = "";
            $approve['second_line'] = "";
            $approve['donation_for'] = "";
            $approve['donated_by'] = "";
            $approve['product_attribute'] = "";
            $postData = $request->getPost();
            foreach ($postData as $key => $val) {
                $approve[$key] = $val;
            }
            $countryOriginUs = $request->getPost('country_originUS');
            $approve['id'] = $this->decrypt($params['id']);
            $search['woh_id'] = $this->decrypt($params['id']);
            $approve['country_origin'] = (isset($countryOriginUs) && !empty($countryOriginUs)) ? $this->_config['transaction_config']['origin_country'] : $request->getPost('country_origin');
            $approve['modified_by'] = $this->auth->getIdentity()->crm_user_id;
            $approve['modified_date'] = DATE_TIME_FORMAT;

            $wohHonoreeName = $this->getNamingConventionArr($approve['product_attribute'], $approve, $approve);
            $approve['final_name'] = $wohHonoreeName['honoree_name'];
            $this->getTransactionTable()->updateWohDetails($approve);
            $searchParam['transactionId'] = $this->decrypt($params['transactionid']);
            ;
            $searchProduct = $this->getTransactionTable()->searchTransactionProducts($searchParam);
            $count = count($searchProduct);
            $transactionStatus = 0;
            $traCount = 0;
            $cancelCount = 0;
            $holdCount = 0;
            $voidedShipCount = 0;
            $returnCount = 0;
            foreach ($searchProduct as $key => $val) {
                if ($val['product_status'] == '6') {
                    $cancelCount++;
                } else if ($val['product_status'] == '7') {
                    $holdCount++;
                } else if ($val['product_status'] == '12') {
                    $voidedShipCount++;
                } else if ($val['product_status'] == '13') {
                    $returnCount++;
                } else {
                    if ($transactionStatus < $val['product_status']) {
                        $traCount = 1;
                        $transactionStatus = $val['product_status'];
                    } else if ($transactionStatus == $val['product_status']) {
                        $traCount++;
                    }
                }
            }
            if ($cancelCount == $count) {
                $transactionStatus = '6';
            } else if ($holdCount == $count) {
                $transactionStatus = '7';
            } else if ($voidedShipCount == $count) {
                $transactionStatus = '12';
            } else if ($returnCount == $count) {
                $transactionStatus = '13';
            } else if (($count > ($traCount + $cancelCount + $voidedShipCount + $returnCount)) && ($transactionStatus == '2' || $transactionStatus == '4' || $transactionStatus == '8' || $transactionStatus == '10')) {
                $transactionStatus++;
            }
            $update['transactionStatus'] = $transactionStatus;
            $update['transactionId'] = $searchParam['transactionId'];
            $update['modified_by'] = '';
            $update['modified_date'] = DATE_TIME_FORMAT;
            $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->updateTransactionStatus($update);


// n - insert into transaction log - start
            $changeLogArray = array();
            $changeLogArray['table_name'] = 'tbl_tra_change_logs';
            $changeLogArray['activity'] = '2';/** 2 == for update */
            $changeLogArray['id'] = $this->decrypt($params['transactionid']);
            $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
            $changeLogArray['added_date'] = DATE_TIME_FORMAT;
            $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
// n - insert into transaction log - end

            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
        $searchResult = $this->getServiceLocator()->get('User\Model\UserTable')->getWoh($search);

        $honoree_name = $this->getNamingConventionArr($searchResult[0]['attribute_option_name'], '', $searchResult[0]);
        $viewWohDetailsForm->get('first_name_one')->setAttribute('value', $searchResult[0]['first_name_one']);
        $viewWohDetailsForm->get('last_name_one')->setAttribute('value', $searchResult[0]['last_name_one']);
        $viewWohDetailsForm->get('other_init_one')->setAttribute('value', $searchResult[0]['other_init_one']);
        $viewWohDetailsForm->get('other_name')->setAttribute('value', $searchResult[0]['other_name']);
        $viewWohDetailsForm->get('other_init_two')->setAttribute('value', $searchResult[0]['other_init_two']);
        $viewWohDetailsForm->get('first_name_two')->setAttribute('value', $searchResult[0]['first_name_two']);
        $viewWohDetailsForm->get('last_name_two')->setAttribute('value', $searchResult[0]['last_name_two']);
        $viewWohDetailsForm->get('first_line')->setAttribute('value', $searchResult[0]['first_line']);
        $viewWohDetailsForm->get('second_line')->setAttribute('value', $searchResult[0]['second_line']);
        if ($searchResult[0]['origin_country'] != $this->_config['transaction_config']['origin_country']) {
            $viewWohDetailsForm->get('country_origin')->setAttribute('value', $searchResult[0]['origin_country']);
        } else {
            $viewWohDetailsForm->get('country_originUS')->setAttribute('value', "1");
        }
        if (isset($searchResult[0]['donation_for']) && !empty($searchResult[0]['donation_for'])) {
            $viewWohDetailsForm->get('donation_for')->setAttribute('value', $searchResult[0]['donation_for']);
        }
        $viewWohDetailsForm->get('donated_by')->setAttribute('value', $searchResult[0]['donated_by']);
        $viewWohDetailsForm->get('product_attribute')->setAttribute('value', $searchResult[0]['attribute_option_name']);

        $searchResult[0]['status'] = ($searchResult[0]['product_status_id'] == '6' || $searchResult[0]['product_status_id'] == '7' || $searchResult[0]['product_status_id'] > '11' ? '1' : $searchResult[0]['status'] );
        if ($searchResult[0]['status'] == 1) {
            $viewWohDetailsForm->get('first_name_one')->setAttribute('readonly', 'readonly');
            $viewWohDetailsForm->get('last_name_one')->setAttribute('readonly', 'readonly');
            $viewWohDetailsForm->get('other_init_one')->setAttribute('readonly', 'readonly');
            $viewWohDetailsForm->get('other_name')->setAttribute('readonly', 'readonly');
            $viewWohDetailsForm->get('other_init_two')->setAttribute('readonly', 'readonly');
            $viewWohDetailsForm->get('first_name_two')->setAttribute('readonly', 'readonly');
            $viewWohDetailsForm->get('last_name_two')->setAttribute('readonly', 'readonly');
            $viewWohDetailsForm->get('first_line')->setAttribute('readonly', 'readonly');
            $viewWohDetailsForm->get('second_line')->setAttribute('readonly', 'readonly');
            $viewWohDetailsForm->get('country_origin')->setAttribute('readonly', 'readonly');
            $viewWohDetailsForm->get('country_originUS')->setAttribute('disabled', 'disabled');
            $viewWohDetailsForm->get('donation_for')->setAttribute('disabled', 'disabled');
            $viewWohDetailsForm->get('donated_by')->setAttribute('disabled', 'disabled');
        }
        $viewModel->setVariables(array(
            'searchResult' => $searchResult[0],
            'jsLangTranslate' => $messages,
            'viewWohDetailsForm' => $viewWohDetailsForm,
            'woh_id' => $params['id'],
            'woh_disp_id' => $search['woh_id'],
            'woh_transaction_id' => $params['transactionid'],
            'arrContacts' => $arrContacts
        ));
        return $viewModel;
    }

    /**
     * This action is used to show passenger certificate
     * @return json
     * @param void
     * @author Icreon Tech - AS
     */
    public function viewPassengerCertificateAction() {
        error_reporting(0);
        $params = $this->params()->fromRoute();
        $this->passengerCertificate($params, 'I');
    }

    /**
     * This action is used to get user cart list frontend
     * @return json
     * @param void
     * @author Icreon Tech - KK
     */
    public function cartListFrontAction() {
        $this->checkFrontUserAuthentication();

        $moduleSession = new Container('moduleSession');
        if($moduleSession->moduleGroup == 3)
            $this->layout('wohlayout');

        $this->getTransactionTable();
        $this->getConfig();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $getProduct = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProduct(array('is_available' => '1', 'featured' => '1', 'offset' => '1', 'total_in_stock' => '1', 'rand_order_by' => '1', 'restricted_category_id' => $this->_config['custom_frame_id']));
        $discount = '';

        /*
          if (!empty($this->auth->getIdentity()->user_id)) {
          $user_id = $this->auth->getIdentity()->user_id;
          $userArr = array('user_id' => $user_id);
          $contactDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserMemberShipDetail($userArr);
          if ($contactDetail['membership_id'] != '1') {
          $discount = $contactDetail['membership_discount'];
          }
          } */
        if (!empty($this->auth->getIdentity()->user_id)) {
            if (defined('MEMBERSHIP_DISCOUNT') && $getProduct[0]['is_membership_discount'] == 1) {
                $discount = MEMBERSHIP_DISCOUNT;
            }
        }
        $assetsUploadDirProduct = $this->_config['file_upload_path']['assets_upload_dir'] . 'product/thumbnail';
        $uploadedFilePathProduct = $this->_config['file_upload_path']['assets_url'] . 'product/thumbnail';
        $params = $this->params()->fromRoute();
        $viewModel->setVariables(array(
            'getProduct' => $getProduct,
            'discount' => $discount,
            'uploadedFilePathProduct' => $uploadedFilePathProduct,
            'assetsUploadDirProduct' => $assetsUploadDirProduct,
            'jsLangTranslate' => $this->_config['transaction_messages']['config']['donate_transaction_front'])
        );
        return $viewModel;
    }

    /**
     * This action is used to get user cart list frontend
     * @return json
     * @param void
     * @author Icreon Tech - KK
     */
    public function cartListFrontProductAction() {
//$this->checkFrontUserAuthenticationAjx();
        //$this->checkFrontUserAuthentication();
        $this->getTransactionTable();
        $this->getConfig();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();

        $params = $this->params()->fromRoute();
        $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '';
        $cartParam['user_id'] = $user_id;
        $cartParam['source_type'] = 'frontend';
        $cartParam['user_session_id'] = session_id();
        if (defined('MEMBERSHIP_DISCOUNT'))
            $cartParam['membership_percent_discount'] = MEMBERSHIP_DISCOUNT;
        else
            $cartParam['membership_percent_discount'] = '';
        //$cartParam['membership_percent_discount'] = MEMBERSHIP_DISCOUNT;

        $cartData = $this->getTransactionTable()->getCartProducts($cartParam);
        $discount = '';
        /* if (!empty($this->auth->getIdentity()->user_id)) {
          $userArr = array('user_id' => $user_id);
          $contactDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserMemberShipDetail($userArr);
          if ($contactDetail['membership_id'] != '1') {
          $discount = $contactDetail['membership_discount'];
          }
          } */
        if (!empty($this->auth->getIdentity()->user_id)) {
            if (defined('MEMBERSHIP_DISCOUNT')) {
                $discount = MEMBERSHIP_DISCOUNT;
            }
        }
        $memberShip = array();
        if (empty($discount)) {
            $memberShip = $this->getTransactionTable()->getMinimumMembershipDiscount();
        }

        $userId = "";
        if (!empty($this->auth->getIdentity()->user_id))
            $userId = $this->auth->getIdentity()->user_id;

		$moduleSession = new Container('moduleSession');

        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'cartlist' => $cartData,
            'membership' => $memberShip,
            'discount' => $discount,
            'jsLangTranslate' => $this->_config['transaction_messages']['config']['donate_transaction_front'],
            'upload_file_path' => $this->_config['file_upload_path'],
            'loggedInUser' => $userId,
            'moduleGroup'=>$moduleSession->moduleGroup
            )
        );
        return $viewModel;
    }

    /**
     * This action is used to save the additional image
     * @param ship id
     * @return this will return the confirmation.
     * @author Icreon Tech - SR
     */
    public function addAdditionalImageBackendAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $sm = $this->getServiceLocator();
        $passengerTable = $sm->get('Passenger\Model\PassengerTable');
        $request = $this->getRequest();
        $response = $this->getResponse();
        $userId = $this->decrypt($request->getPost('userId'));
        if ($request->isPost()) {
            if ($request->getPost('act') == 'save') {
                $postData = array();
                $postData['passenger_id'] = $this->decrypt($request->getPost('passengerId'));
                $postData['file_name'] = $request->getPost('fileName');
                $postData['user_id'] = $userId;
                $postData['file_path'] = $request->getPost('filePath');
                $passengerTable->savePassengerManifestAdditionalImage($postData);
            }
        }
        $searchParam = array();
        $searchParam['passenger_id'] = $this->decrypt($request->getPost('passengerId'));

        $searchParam['user_id'] = $userId;
        $shipManifestArray = $passengerTable->getPassengerManifestAdditionalImage($searchParam);

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        if ($this->getRequest()->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $viewModel->setVariables(array(
            'shipManifestArray' => $shipManifestArray
        ));
        return $viewModel;
    }

    /**
     * This action is used to add inventory to cart
     * @return json
     * @param void
     * @author Icreon Tech - KK
     */
    public function addToCartInventoryFrontAction() {
//  $this->checkFrontUserAuthenticationAjx();
        $this->getTransactionTable();
        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $transaction = new Transaction($this->_adapter);
        $inventoryForm = new InventoryForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $postArr = $request->getPost()->toArray();
            $inventoryFormArr = $postArr;
            $prodMappArr = array(4);
            $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '0';
            $getProductMappingTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductsMappingType($prodMappArr);
            $params['productId'] = $postArr['product_id'];
            $params['user_id'] = $user_id;
            $params['item_type'] = '4';
            $params['product_mapping_id'] = $prodMappArr[0];
            $params['num_quantity'] = $postArr['num_quantity'];
            $params['source_type'] = "frontend";
            $params['user_session_id'] = session_id();
            $productStatus = $this->getTransactionTable()->getInventoryProductAlreadyAdded($params);
            $numQuantityE = (isset($productStatus[0]['num_quantity']) and trim($productStatus[0]['num_quantity']) != "") ? $productStatus[0]['num_quantity'] : "0";
            $numQuantityN = (isset($postArr['num_quantity']) and trim($postArr['num_quantity']) != "") ? $postArr['num_quantity'] : "0";
            $numQuantity = $numQuantityE + $numQuantityN;


            $detail = $this->_productTable->getProductInfo(array('id' => $postArr['product_id']));
            $total_in_stock = $detail[0]['total_in_stock'];
            if (isset($numQuantity) and trim($numQuantity) != "" and $numQuantity > $total_in_stock) {
                if ($total_in_stock == "" or $total_in_stock == "0") {
                    $returnrray = array('status' => "error", 'message' => sprintf($msgArr['QUANTITY_EXCEED_FRONT_ZERO'], $total_in_stock), 'product_id' => $postArr['product_id']);
                } else {
                    $returnrray = array('status' => "error", 'message' => sprintf($msgArr['QUANTITY_EXCEED_FRONT_MORE'], $total_in_stock), 'product_id' => $postArr['product_id']);
                }
                $messages = $returnrray;
            } else {
                if ($productStatus[0]['product_exist'] == 0) {
                    $cartId = $this->addInventoryProductToCart($params);
                } else {

                    $params['num_quantity'] = $productStatus[0]['num_quantity'] + $params['num_quantity'];
                    $productStatus = $this->getTransactionTable()->updateCartInventoryProduct($params);
                }
                $messages = array('status' => "success");
            }

            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This action is used to add inventory to cart
     * @return json
     * @param void
     * @author Icreon Tech - KK
     */
    public function removeCartFrontAction() {
        $this->checkFrontUserAuthenticationAjx();
        $this->getTransactionTable();
        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $transaction = new Transaction($this->_adapter);
        $inventoryForm = new InventoryForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $postArr = $request->getPost()->toArray();
            $inventoryFormArr = $postArr;
            $transactionId = $inventoryFormArr['cart_id'];
            $productType = $inventoryFormArr['product_type'];
            $removeArray['cart_id'] = $transactionId;
            $removeArray['product_type'] = $productType;
            $this->getTransactionTable()->removeFromCart($removeArray);
            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This action is used to add inventory to cart
     * @return json
     * @param void
     * @author Icreon Tech - KK
     */
    public function updateCartFrontAction() {

        $this->getTransactionTable();
        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $transaction = new Transaction($this->_adapter);
        $inventoryForm = new InventoryForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $postArr = $request->getPost()->toArray();
            $inventoryFormArr = $postArr;
            $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '0';
            foreach ($inventoryFormArr['product'] as $key => $productDetail) {
                $paramArr = array();
                $paramArr['user_id'] = $user_id;
                $paramArr['cart_id'] = $productDetail['cart_id'];
                $paramArr['type_of_product'] = $productDetail['type_of_product'];
                $paramArr['num_quantity'] = $productDetail['num_qty'];
                $paramArr['product_id'] = $productDetail['product_id'];
                $paramArr['user_session_id'] = session_id();

                if ($productDetail['cart_id'] != "" and $productDetail['type_of_product'] != "" and $productDetail['num_qty'] != "") {

                    //  echo $productDetail['type_of_product'];
                    if ($productDetail['type_of_product'] == "inventory") {
                        $detail = $this->_productTable->getProductInfo(array('id' => $productDetail['product_id']));
                        $total_in_stock = $detail[0]['total_in_stock'];
                        if ($productDetail['num_qty'] > $total_in_stock) {
                            if ($total_in_stock == "" or $total_in_stock == "0") {
                                $returnrray[$key] = array('message' => sprintf($msgArr['QUANTITY_EXCEED_FRONT_ZERO'], $total_in_stock), 'product_name' => $detail[0]['product_name']);
                            } else {
                                $returnrray[$key] = array('message' => sprintf($msgArr['QUANTITY_EXCEED_FRONT_MORE'], $total_in_stock), 'product_name' => $detail[0]['product_name']);
                            }
                        } else {

                            if ($productDetail['num_qty'] == "0") {
                                $postArrDel = array();
                                $postArrDel['cartId'] = $productDetail['cart_id'];
                                $postArrDel['productTypeId'] = $productDetail['product_type_id'];
                                $postArrDel['productMappingId'] = $productDetail['product_mapping_id'];
                                $postArrDel['typeOfProduct'] = $productDetail['type_of_product'];
                                $transactionArr = $transaction->getArrayForRemoveCartProduct($postArrDel);
                                $this->_transactionTable->removeProductToCart($transactionArr);
                            } else {
                                $productStatus = $this->getTransactionTable()->updateCartInventoryProductFront($paramArr);
                            }
                        }
                    } else {
                        if ($productDetail['num_qty'] == "0") {
                            $postArrDel = array();
                            $postArrDel['cartId'] = $productDetail['cart_id'];
                            $postArrDel['productTypeId'] = $productDetail['product_type_id'];
                            $postArrDel['productMappingId'] = $productDetail['product_mapping_id'];
                            $postArrDel['typeOfProduct'] = $productDetail['type_of_product'];
                            $transactionArr = $transaction->getArrayForRemoveCartProduct($postArrDel);
                            $this->_transactionTable->removeProductToCart($transactionArr);
                        } else {
                            $productStatus = $this->getTransactionTable()->updateCartInventoryProductFront($paramArr);
                        }
                    }
                }

//                if ($productType == '1' || $productType == '5' || $productType == '8') {
//
//
//                    foreach ($productDetail as $productId => $productQuantity) {
//
//                        if ($productType == '1') {
//                            $detail = $this->_productTable->getProductInfo(array('id' => $productId));
//                            $total_in_stock = $detail[0]['total_in_stock'];
//                            if ($productQuantity > $total_in_stock) {
//                                $returnrray[$productId] = array('message' => sprintf($msgArr['QUANTITY_EXCEED_FRONT'], $total_in_stock), 'product_name' => $detail[0]['product_name']);
//                            } else {
//                                $params['productId'] = $productId;
//                                $params['user_id'] = $user_id;
//                                $params['num_quantity'] = $productQuantity;
//                                $productStatus = $this->getTransactionTable()->updateCartInventoryProduct($params);
//                            }
//                        } else {
//
//                            $params['productId'] = $productId;
//                            $params['user_id'] = $user_id;
//                            $params['num_quantity'] = $productQuantity;
//                            $productStatus = $this->getTransactionTable()->updateCartInventoryProduct($params);
//                        }
//                    }
//                }
            }
            if ($user_id == 0 && $postArr['submit_type'] == 'checkout_cart')
                $this->checkFrontUserAuthenticationAjx();

            if (!empty($returnrray)) {
                $messages = array('status' => "error", "info" => $returnrray);
            } else {
                $messages = array('status' => "success");
            }
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This action is used to add inventory to cart
     * @return json
     * @param void
     * @author Icreon Tech - KK
     */
    public function checkoutCartFrontAction() {
        $this->checkFrontUserAuthentication();

        $this->getTransactionTable();
        $msgArr = $this->_config['transaction_messages']['config']['checkout_front'];
        $transaction = new Transaction($this->_adapter);
        $inventoryForm = new InventoryForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $createTransactionForm = new CheckoutFormFront();
        $itemsTotal = '';
        $discount = '';
        $this->campaignSession = new Container('campaign');
        $finalTransaction['campaign_code'] = (isset($this->campaignSession->campaign_code) && !empty($this->campaignSession->campaign_code) ? $this->campaignSession->campaign_code : $finalTransaction['campaign_code']);
        $finalTransaction['campaign_id'] = (isset($this->campaignSession->campaign_id) && !empty($this->campaignSession->campaign_id) ? $this->campaignSession->campaign_id : $finalTransaction['campaign_id']);
        $finalTransaction['channel_id'] = (isset($this->campaignSession->channel_id) && !empty($this->campaignSession->channel_id) ? $this->campaignSession->channel_id : $finalTransaction['channel_id']);
        $campResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getChannelPromo(array('campaign_id' => $finalTransaction['campaign_id'], 'channel_id' => $finalTransaction['channel_id']));
        if (isset($campResult[0]['promotion_id']) && !empty($campResult[0]['promotion_id'])) {
            $couponCode['coupon_code'] = $campResult[0]['coupon_code'];
            $couponCode['promotion_id'] = $campResult[0]['promotion_id'];
        } else {
            /** set default promotion code for ellis
             * * @ added by Saurabh
             * */
            $couponCode['promotion_id'] = 0;
        }
        $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '';
        if (isset($user_id) && $user_id != '') {
            $userId = $user_id;
            $paramArray['userid'] = $user_id;
            //$returnData = $this->cartTotalWithTax($paramArray);
            //asd($returnData);
            /* if ($returnData['isInventory'] == $returnData['totalproduct']) {
              $class = "ignore";
              } else {
              $class = "notignore";
              } */
            $userArr = array('user_id' => $userId);
            $contactDetail = array();
            $contactDetail[0] = $this->getServiceLocator()->get('User\Model\UserTable')->getUser($userArr);

            $contactAfihcDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAfihc($userArr);
            $contacAddressDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations($userArr);

            $addressDetailArr = array('' => 'Select one');
//  $billingLocation = array('' => 'Select one');
//$shippingLocation = array('' => 'Select one');
            $defaultShippingAddress = '';
            $defaultBillingAddress = '';
            $billingLocation = array();
            $shippingLocation = array();

            if ($contactDetail[0]['user_type'] == 1) { // for individual user
                $createTransactionForm->get('shipping_title')->setAttribute('value', $contactDetail[0]['title']);
                $createTransactionForm->get('shipping_first_name')->setAttribute('value', stripslashes($contactDetail[0]['first_name']));
                $createTransactionForm->get('shipping_last_name')->setAttribute('value', stripslashes($contactDetail[0]['last_name']));

                $createTransactionForm->get('billing_title')->setAttribute('value', $contactDetail[0]['title']);
                $createTransactionForm->get('billing_first_name')->setAttribute('value', stripslashes($contactDetail[0]['first_name']));
                $createTransactionForm->get('billing_last_name')->setAttribute('value', stripslashes($contactDetail[0]['last_name']));
            } else { // for corprate case.
                $createTransactionForm->get('shipping_title')->setAttribute('value', '');
                $createTransactionForm->get('shipping_first_name')->setAttribute('value', stripslashes($contactDetail[0]['company_name']));
                $createTransactionForm->get('shipping_last_name')->setAttribute('value', stripslashes($contactDetail[0]['legal_name']));

                $createTransactionForm->get('billing_title')->setAttribute('value', '');
                $createTransactionForm->get('billing_first_name')->setAttribute('value', stripslashes($contactDetail[0]['company_name']));
                $createTransactionForm->get('billing_last_name')->setAttribute('value', stripslashes($contactDetail[0]['legal_name']));
            }



            $userPhoneInfoArray = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserPhoneInformations(array('user_id' => $user_id));

            if (count($userPhoneInfoArray) > 0) {
                $createTransactionForm->get('shipping_country_code')->setAttribute('value', $userPhoneInfoArray[0]['country_code']);
                $createTransactionForm->get('shipping_area_code')->setAttribute('value', $userPhoneInfoArray[0]['area_code']);
                $createTransactionForm->get('shipping_phone')->setAttribute('value', $userPhoneInfoArray[0]['phone_number']);
            }

            if (!empty($contacAddressDetail)) {

//$defaultShippingAddress = $contacAddressDetail[0]['address_information_id'];
                foreach ($contacAddressDetail as $addressDetail) {

                    if (!empty($addressDetail['first_name'])) {
                        $userName = (!empty($addressDetail['title'])) ? stripslashes($addressDetail['title']) . ' ' : '';
                        $userName.= (!empty($addressDetail['first_name'])) ? stripslashes($addressDetail['first_name']) . ' ' : '';
                        // $userName.= (!empty($addressDetail['middle_name'])) ? stripslashes($addressDetail['middle_name']) . ' ' : '';
                        $userName.= (!empty($addressDetail['last_name'])) ? stripslashes($addressDetail['last_name']) : '';
                    } else {
                        $userName = (!empty($contactDetail[0]['full_name'])) ? $contactDetail[0]['full_name'] . " " : $contactDetail[0]['company_name'] . " ";
                    }


                    $address = $addressDetail['street_address_one'] . " " . $addressDetail['street_address_two'] . " " . $addressDetail['street_address_three'];
                    $address = rtrim(trim($address), ",");
                    $address = (!empty($address)) ? $address . ", " : "";
                    $state = (!empty($addressDetail['state'])) ? $addressDetail['state'] . ", " : "";
                    $city = (!empty($addressDetail['city'])) ? $addressDetail['city'] . ", " : "";
                    $country_name = (!empty($addressDetail['country_name'])) ? $addressDetail['country_name'] . ", " : "";
                    $zip_code = (!empty($addressDetail['zip_code'])) ? $addressDetail['zip_code'] : "";
                    $addressLabel = '<strong>' . $userName . '</strong><br>' . $address . $state . $city . $country_name . $zip_code;
                    $addressLabel = rtrim(trim($addressLabel), ',');

                    $locationType = explode(",", $addressDetail['location_name']);
                    if (in_array('2', $locationType)) {

                        $billingLocation[$addressDetail['address_information_id']] = $addressLabel;
                        if (in_array('1', $locationType)) {
                            $defaultBillingAddress = $addressDetail['address_information_id'];
                        }
                    }
                    if (in_array('3', $locationType)) {

                        $shippingLocation[$addressDetail['address_information_id']] = $addressLabel;
                        if (in_array('1', $locationType)) {
                            $defaultShippingAddress = $addressDetail['address_information_id'];
                        }
                    }
                }
            }


            $createTransactionForm->get('billing_existing_contact')->setAttribute('options', $billingLocation);
//$createTransactionForm->get('shipping_existing_contact')->setAttribute('class', $class);
            $createTransactionForm->get('shipping_existing_contact')->setAttribute('options', $shippingLocation);
//$getShippingMethods = $this->_config['transaction_config']['domestic_config_info']['shippings_types'];
            $shippingMetArr = array();
            $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
            $getShippingMethods = $this->_transactionTable->getShippingMethod($shippingMethodArr);
            $shippingMethods = array();
            if (!empty($getShippingMethods)) {
                foreach ($getShippingMethods as $shippingMet) {
                    $key = $shippingMet['carrier_id'] . ',' . $shippingMet['service_type'] . ',' . $shippingMet['pb_shipping_type_id'];
                    $webSelection = (!empty($shippingMet['web_selection'])) ? '  ' . $shippingMet['web_selection'] . ' ' : '';
                    $shippingMethods[$key] = $webSelection;
                }
            }
            $createTransactionForm->get('shipping_method')->setAttribute('options', $shippingMethods);
            $state = $this->getServiceLocator()->get('Common\Model\CommonTable')->getStates();
            $stateList = array();
            $stateList[''] = 'Select State';
            foreach ($state as $key => $val) {
                $stateList[$val['state_code']] = $val['state'] . " - " . $val['state_code'];
            }
            $createTransactionForm->get('billing_state_id')->setAttribute('options', $stateList);

            $stateList[''] = 'Select State';
            foreach ($state as $key => $val) {
                $stateList[$val['state_code']] = $val['state'] . " - " . $val['state_code'];
            }

            $createTransactionForm->get('shipping_state_id')->setAttribute('options', $stateList);

            $tokenArray = array();
// $tokenArray[''] = 'Select';
            $userTokenInfo = $this->getServiceLocator()->get('User\Model\UserTable')->getUserTokenInformations(array('user_id' => $user_id));
            if (!empty($userTokenInfo)) {

                foreach ($userTokenInfo as $key => $val) {
                    //$createCustomerProfile = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
                    //$createCustomerProfile->getCustomerProfileRequest(array('customerProfileId' => $val['profile_id']));
                    // $cardNumbrer = $createCustomerProfile->profile->paymentProfiles->payment->creditCard->cardNumber;
                    //$cardExpiryDate = $createCustomerProfile->profile->paymentProfiles->payment->creditCard->expirationDate;
                    //$tokenArray[$val['profile_id']] = $cardNumbrer . '  ' . $cardExpiryDate;
                    $tokenArray[$val['profile_id']] = $val['credit_card_no'];
                }
                if ($defaultBillingAddress == '')
                    $defaultBillingAddress = $userTokenInfo[0]['address_information_id'];
                $createTransactionForm->get('credit_card_exist')->setAttribute('options', $tokenArray);
            }


            if ($contactDetail[0]['user_type'] == '1') {
                $name = "";
                if ($contactDetail[0]['first_name'] != '')
                    $name.= $contactDetail[0]['first_name'];
                if ($contactDetail[0]['middle_name'] != '')
                    $name.= " " . $contactDetail[0]['middle_name'];
                if ($contactDetail[0]['last_name'] != '')
                    $name.= " " . $contactDetail[0]['last_name'];
            }else {
                $name = $contactDetail[0]['company_name'];
            }

            $createTransactionForm->get('credit_card_name')->setValue($name);

            $getTitle = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTitle();
            $user_title = array();
            $userTitle[''] = 'None';
            foreach ($getTitle as $key => $val) {
                $userTitle[$val['title']] = $val['title'];
            }
            $createTransactionForm->get('billing_title')->setAttribute('options', $userTitle);
            $createTransactionForm->get('billing_title')->setValue($contactDetail[0]['title']);
            $createTransactionForm->get('shipping_title')->setAttribute('options', $userTitle);
            $createTransactionForm->get('shipping_title')->setValue($contactDetail[0]['title']);

            $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
            $countryList = array();
            foreach ($country as $key => $val) {
                $countryList[$val['country_id']] = $val['name'];
            }
            $createTransactionForm->get('billing_country')->setAttribute('options', $countryList);
            $createTransactionForm->get('shipping_country')->setAttribute('options', $countryList);

            $createTransactionForm->get('user_id')->setValue($userId);
            $monthArray = $this->creditMonthArray();

            $yearArray = range(date('Y'), date('Y') + 10);
            $newYearArray = array_combine($yearArray, $yearArray);
            $createTransactionForm->get('credit_card_exp_month')->setAttribute('options', $monthArray);
            $createTransactionForm->get('credit_card_exp_year')->setAttribute('options', $newYearArray);

            $arrayPost['userid'] = $user_id;

            //asd($arrayPost,2);

            $finalCart = $this->cartTotalWithTax($arrayPost);
            //asd($finalCart);

            $cartlist = $finalCart['cartData'];
            //asd($finalCart);

            $tp = $finalCart['totalproduct'];
            $apoPoStates = $this->_config['transaction_config']['apo_po_state'];
            $apoPoStatesArray = array();
//$apoPoStatesArray[''] = 'Select State';
            foreach ($apoPoStates as $key => $val) {
                $apoPoStatesArray[$key] = $val;
            }
            $createTransactionForm->get('apo_po_state')->setAttribute('options', $apoPoStatesArray);

            if ($contactDetail[0]['membership_id'] != '1') {
                $discount = $contactDetail[0]['discount'];
            }
        }
        $memberShip = array();
        $memberdiscount = '';
        $isOnlyDonation = '';
        $defaultToken = '';
        if (empty($discount) && $finalCart['finaltotaldiscounted'] != 0) {
            $memberShip = $this->getTransactionTable()->getMinimumMembership();
            $memberdiscount = ($finalCart['finaltotaldiscounted'] * $memberShip[0]['discount']) / 100;
            $isOnlyDonation = $finalCart['isOnlyDonation'];
        }
        /* $cartParam['user_id'] = $this->auth->getIdentity()->user_id;
          $cartParam['source_type'] = 'frontend';
          $cartlist = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getCartProducts($cartParam); */
        if (empty($tp)) {
            $location = SITE_URL . '/cart';
            echo '<script>window.location.href="' . $location . '";</script>';
            // header('Location: ' . SITE_URL . '/cart');
            die;
        } else {

            $viewModel = new ViewModel();
            $viewModel->setVariables(array(
                'jsLangTranslate' => array_merge($msgArr, $this->_config['transaction_messages']['config']['common_message']),
                'userId' => $userId,
                'memberdiscount' => $memberdiscount,
                'contactDetail' => $contactDetail,
                'contactAfihcDetail' => $contactAfihcDetail,
                'itemsTotal' => $finalCart,
                'createTransactionForm' => $createTransactionForm,
                'isOnlyDonation' => $isOnlyDonation,
                'membershipDiscountAmount' => $finalCart['membershipDiscountAmount'],
                'validPromotionsForUser' => $finalCart['validPromotionsForUser'],
                'cartlist' => $cartlist,
                'upload_file_path' => $this->_config['file_upload_path'],
                'shippingLocation' => $shippingLocation,
                'billingLocation' => $billingLocation,
                'defaultShippingAddress' => $defaultShippingAddress,
                'defaultBillingAddress' => $defaultBillingAddress,
                'tokenArray' => $tokenArray,
                'defaultToken' => $defaultToken,
                'couponCode' => $couponCode['coupon_code'],
                'pick_up' => $request->getPost('pick_up')

            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to update product status
     * @return json
     * @param void
     * @author Icreon Tech - AS
     */
    public function updateProductStatusAction() {
        $this->getConfig();
        $this->checkUserAuthentication();
        $productStatusForm = new ProductStatusForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $msgArr = $this->_config['transaction_messages']['config']['view_crm_transaction'];
        $this->getTransactionTable();
        $viewModel = new ViewModel();
        $params = $this->params()->fromRoute();
        $searchParam['transactionId'] = $this->decrypt($params['id']);
        $count = count($request->getPost('product_status'));
        $product_status = $request->getPost('product_status');
        $product_pre_status = $request->getPost('product_pre_status');
        $product_type = $request->getPost('product_type_id');
        $product_id = $request->getPost('product_id');
        $transactionStatus = 0;
        $count1 = 0;
        for ($i = 0; $i < $count; $i++) {
            $data['productTypeId'] = $product_type[$i];
            $data['productId'] = $product_id[$i];
            $data['productPreStatus'] = $product_pre_status[$i];

            if ($data['productPreStatus'] < '7' && $product_status[$i] < '7') {
                $data['productStatus'] = $product_status[$i];
            } else {
                $data['productStatus'] = $data['productPreStatus'];
            }
            $data['modified_by'] = $this->auth->getIdentity()->crm_user_id;
            $data['modified_date'] = DATE_TIME_FORMAT;
            switch ($data['productTypeId']) {
                case 'bio': $data['table'] = "tbl_tra_bio_certificates";
                    $data['field'] = "bio_certificate_id";
                    break;
                case 'fof': $data['table'] = "tbl_usr_flag_of_faces";
                    $data['field'] = "fof_id";
                    break;
                case 'woh': $data['table'] = "tbl_usr_wall_of_honor";
                    $data['field'] = "woh_id";
                    break;
                case 'pro': $data['table'] = "tbl_tra_transaction_products";
                    $data['field'] = "transaction_product_id";
                    break;
            }
            if ($transactionStatus < $data['productStatus']) {
                $count1 = 1;
                $transactionStatus = $data['productStatus'];
            } else if ($transactionStatus == $data['productStatus']) {
                $count1++;
            }
            if (isset($data['table']) && !empty($data['table'])) {
                if ($data['productPreStatus'] != '10')
                    ;
                $this->getTransactionTable()->updateProductStatus($data);
            }
        }
        if ($count > $count1 && ($transactionStatus == '2' || $transactionStatus == '4' || $transactionStatus == '8' || $transactionStatus == '10')) {
            $transactionStatus++;
        }
        $update['transactionStatus'] = $transactionStatus;
        $update['transactionId'] = $searchParam['transactionId'];
        $update['modified_by'] = $this->auth->getIdentity()->crm_user_id;
        $update['modified_date'] = DATE_TIME_FORMAT;
        $this->getTransactionTable()->updateTransactionStatus($update);
        $getProductStatus = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductStatus();
        $productStatus = array();
        $productStatus[''] = 'Any';
        foreach ($getProductStatus as $key => $val) {
            $productStatus[$val['product_status_id']] = $val['product_status'];
        }
        $productStatusForm->get('product_status[]')->setAttribute('options', $productStatus);
        $searchResult = $this->getTransactionTable()->searchTransactionDetails($searchParam);
        $searchProduct = $this->getTransactionTable()->searchTransactionProducts($searchParam);
        foreach ($searchProduct as $key => $val) {
            $proParam['transactionId'] = $searchParam['transactionId'];
            $proParam['productId'] = $val['id'];
            $proResult = $this->getTransactionTable()->searchProductsShipped($proParam);
            if (isset($proResult[0]) && !empty($proResult[0])) {
                $searchProduct[$key]['shippedQty'] = $proResult[0]['shippped_qty'];
            }
        }
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'searchResult' => $searchResult[0],
            'searchProduct' => $searchProduct,
            'transactionId' => $params['id'],
            'productStatusForm' => $productStatusForm,
            'jsLangTranslate' => $msgArr,
            'transactionStatus' => $update['transactionStatus']
        ));
        return $viewModel;
    }

    /**
     * This action is used to delete cart product
     * @param  void
     * @return json format string
     * @author Icreon Tech - DT
     */
    public function deleteProductToCartAction() {
        $request = $this->getRequest();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $cartId = $request->getPost('cartId');
            if ($cartId != '') {
                $postArr = $request->getPost();
                $this->getTransactionTable();
                $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
                $transaction = new Transaction($this->_adapter);

                $response = $this->getResponse();

                $transactionArr = $transaction->getArrayForRemoveCartProduct($postArr);
                $this->_transactionTable->removeProductToCart($transactionArr);
                $messages = array('status' => "success", 'message' => $msgArr['CART_PRODUCT_DELETED_SUCCESSFULLY']);

                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                return $this->redirect()->toRoute('get-crm-transactions');
            }
        } else {
            return $this->redirect()->toRoute('get-crm-transactions');
        }
    }

    /**
     * This action is used to show get crm cart cart products on refresh
     * @param void
     * @return     array
     * @author Icreon Tech - DT
     */
    public function getCrmCartProductsReloadAction() { //cccccccccccccccccc
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest()) {
            $this->getTransactionTable();
            $createTransactionMessage = $this->_config['transaction_messages']['config']['create_crm_transaction'];
            $applicationConfigIds = $this->_config['application_config_ids'];
            $uSId = $applicationConfigIds['united_state_country_id'];
            $createTransactionForm = new CreateTrasactionForm();
            $paymentModeCheck = new PaymentModeCheck();
            $paymentModeCredit = new PaymentModeCredit();
            $batchForm = new UpdateTransactionBatchForm();
            $transaction = new Transaction($this->_adapter);
            $postArr = $request->getPost()->toArray();
            $userId = @$postArr['user_id'];
            /* Get check types */
            $checkTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCheckTypes();
            $checkTypeList = array();
            foreach ($checkTypes as $key => $val) {
                $checkTypeList[$val['cheque_type_id']] = $val['cheque_type_name'];
            }
            $paymentModeCheck->get('payment_mode_chk_type[]')->setAttribute('options', $checkTypeList);

            /* End get check types */

            /* Get check types */
            $checkTypeIds = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCheckIDTypes();
            $checkTypeIdsList = array();
            foreach ($checkTypeIds as $key => $val) {
                $checkTypeIdsList[$val['id_type_id']] = $val['id_type_name'];
            }
            $paymentModeCheck->get('payment_mode_chk_id_type[]')->setAttribute('options', $checkTypeIdsList);


            /* End get check types */
            $createTransactionForm->add($paymentModeCheck);
            $createTransactionForm->add($paymentModeCredit);
            $userArr = array('user_id' => $userId);
            $shippingMethodInter = '';
            /* Shipping methods */

            $cartProductArr['custom_shipping'] = 0;

            if (isset($postArr['custom_shipping']) && !empty($cartProductArr['custom_shipping']))
                $cartProductArr['custom_shipping'] = $postArr['custom_shipping'];


            if (isset($postArr['is_apo_po']) && $postArr['is_apo_po'] != '0' && $postArr['is_apo_po'] != '1') {
                $shippingMetArr = array();
                $shippingMetArr['pb_shipping_id'] = $applicationConfigIds['apo_po_pb_id'];
                $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                $getShippingMethods = $this->_transactionTable->getShippingMethod($shippingMethodArr);
                $postArr['shipping_country'] = $postArr['apo_po_shipping_country'];
                if ($postArr['is_apo_po'] == '3') {
                    $postArr['shipping_state'] = $postArr['apo_po_state'];
                } else {
                    if (isset($postArr['shipping_country']) && $postArr['shipping_country'] == $uSId) {
                        $postArr['shipping_state'] = $postArr['shipping_state_select'];
                    }
                }
//$getShippingMethods = $this->_config['transaction_config']['usps_iop_config_info']['shippings_types'];
            } else if (isset($postArr['shipping_country']) && $postArr['shipping_country'] == $uSId && $postArr['is_apo_po'] == '1') {
                $shippingMetArr = array();
                $shippingMetArr['pb_shipping_id'] = $applicationConfigIds['domestic_pb_id'];
                $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                $getShippingMethods = $this->_transactionTable->getShippingMethod($shippingMethodArr);
                $postArr['shipping_state'] = $postArr['shipping_state_select'];
//$getShippingMethods = $this->_config['transaction_config']['domestic_config_info']['shippings_types'];
            } else {
                $cartProductArr = array();
                $cartProductArr['user_id'] = $postArr['user_id'];
                $cartProductArr['userid'] = $postArr['user_id'];

                $cartProductArr['source_type'] = 'backend';
                $cartProductsWeight = $this->cartTotalWithTax($cartProductArr);
                $totalWeight = 0;
                /*if (!empty($cartProductsWeight)) {
                    foreach ($cartProductsWeight as $cartProductWei) {
                        if ($cartProductWei['product_type_id'] == 1) {
                            $totalWeight+=$cartProductWei['product_weight'];
                        }
                    }
                }*/
                $totalWeight = (isset($cartProductsWeight['totalweight']) && $cartProductsWeight['totalweight']>0)?$cartProductsWeight['totalweight']:'0.00';
                $shippingMetArr = array();
                $shippingMetArr['pb_shipping_id'] = $applicationConfigIds['internation_pb_id'];
                $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                $getShippingMethodsInter = $this->_transactionTable->getShippingMethod($shippingMethodArr);
//$lbsCheck = $this->_config['transaction_config']['international_config_info']['lbs_check'];
                $lbsCheck = $getShippingMethodsInter[0]['lbs_check'];
                if ($totalWeight <= $lbsCheck) {
//$getShippingMethods = $this->_config['transaction_config']['international_config_info']['shipping_types_first_class'];
                    foreach ($getShippingMethodsInter as $shipMethod) {
                        if ($shipMethod['pb_shipping_type_id'] == $applicationConfigIds['internation_less_than_lbs_id']) {
                            $getShippingMethods[] = $shipMethod;

							$postArr['shipping_method'] = $shipMethod['carrier_id'] . ',' . $shipMethod['service_type'] . ',' . $shipMethod['pb_shipping_type_id'];
                        }
                    }
                } else {
//$getShippingMethods = $this->_config['transaction_config']['international_config_info']['shipping_types_priority'];
                    foreach ($getShippingMethodsInter as $shipMethod) {
                        if ($shipMethod['pb_shipping_type_id'] == $applicationConfigIds['internation_greater_than_lbs_id']) {
                            $getShippingMethods[] = $shipMethod;

							$postArr['shipping_method'] = $shipMethod['carrier_id'] . ',' . $shipMethod['service_type'] . ',' . $shipMethod['pb_shipping_type_id'];
                        }
                    }
                }

                $shippingMethodInter = $postArr['shipping_method'];

                if ($postArr['is_apo_po'] == 3)
                    $postArr['shipping_country'] = $postArr['apo_po_shipping_country'];
            }
            $shippingMethods = array();
            if (!empty($getShippingMethods)) {
                foreach ($getShippingMethods as $shippingMet) {
                    $key = $shippingMet['carrier_id'] . ',' . $shippingMet['service_type'] . ',' . $shippingMet['pb_shipping_type_id'];
                    $webSelection = (!empty($shippingMet['web_selection'])) ? '  ' . $shippingMet['web_selection'] . ' ' : '';
                    $shippingMethods[$key] = $webSelection;
                }
            }
            $shippingMethods['custom'] = 'Custom';

            $createTransactionForm->get('shipping_method')->setAttribute('options', $shippingMethods);
            $createTransactionForm->get('shipping_method')->setValue(@$postArr['shipping_method']);
            $createTransactionForm->get('custom_shipping')->setValue(@$postArr['custom_shipping']);

//$isEmailConfirmation=isset($postArr['is_email_confirmation'])?$postArr['is_email_confirmation']:'';
            $createTransactionForm->get('is_email_confirmation')->setValue(1);

            $postArr['shipping_method'] = (!empty($postArr['shipping_method']) && (empty($postArr['pick_up']) || $postArr['pick_up'] == 0) && array_key_exists($postArr['shipping_method'], $shippingMethods)) ? $postArr['shipping_method'] : '';
            $cartProductArr = array();
            $cartProductArr['user_id'] = $postArr['user_id'];
            $cartProductArr['userid'] = $postArr['user_id'];
            $cartProductArr['source_type'] = 'backend';
            $cartProductArr['shipcode'] = $postArr['shipping_zip_code'];
            $cartProductArr['shipping_method'] = $postArr['shipping_method'];
            $cartProductArr['postData'] = $postArr;


            if (isset($postArr['custom_shipping']) && !empty($cartProductArr['custom_shipping']))
                $cartProductArr['custom_shipping'] = $postArr['custom_shipping'];
            $cartProducts = $this->cartTotalWithTax($cartProductArr);

            //$getAllValidPromotionDetail = $promotionTable->getAllValidPromotions();
            $getAllValidPromotionDetail = array();
            /* End Product Copon discount */
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => array_merge($createTransactionMessage, $this->_config['transaction_messages']['config']['common_message'], $this->_config['transaction_messages']['config']['update_transaction_batch']),
                'createTransactionForm' => $createTransactionForm,
                'batchForm' => $batchForm,
                'isPickUp' => (!empty($postArr['pick_up'])) ? $postArr['pick_up'] : '0',
                'isFreeShipping' => $isFreeShipping,
                'freeShippingAmount' => '',
                'cartProducts' => $cartProducts['cartData'],
                'getAllValidPromotionDetail' => $getAllValidPromotionDetail
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to apply coupon discount in cart array
     * @param int,array
     * @return     array
     * @author Icreon Tech - DT
     */
    public function addCouponDiscountInCart($promotionId, $cartProductArr = array(), $flag = 'indidiscount', $user_id = 0, $sourceType = 'frontend', $shipping_country) {
        if (!empty($cartProductArr)) {
            $totalProductWeight = 0;
            $totalProductWeightDiscount = 0;
            $getCoponStatus = $this->isCouponValid($promotionId, $user_id, $sourceType, $shipping_country);
            if ($getCoponStatus['status'] == 'success') {
                $promotionData = $getCoponStatus['promotionData'];
                $isFreeShipping = $promotionData['is_free_shipping'];
                $freeShippingAmount = $promotionData['free_shipping_amount'];
                $usersArr = array();
                $productsArr = array();
                if ($promotionData['promotion_user_ids'] != '') {
                    $usersArr = explode(',', $promotionData['promotion_user_ids']);
                }

                if ($promotionData['promotion_product_ids'] != '') {
                    $productsArr = explode(',', $promotionData['promotion_product_ids']);
                }

                /* public campaign check start */
                $isGlobalCampaign = 0;
                $this->campaignSession = new Container('campaign');
                if (isset($this->campaignSession->campaign_code)) {
                    $campParam = array('', $this->campaignSession->campaign_code);
                    $campaign = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCampaign($campParam);

                    if (isset($campaign[0]['is_global']) && $campaign[0]['is_global'] == 1) {
                        $isGlobalCampaign = 1;
                    }
                }
                /* public campaign check ends */

                $i = 0;

                if ($flag == 'indidiscount') {
                    foreach ($cartProductArr as $cartProduct) {
                        if ($cartProduct['product_id'] != '' && $cartProduct['product_id'] != 0) {


                            if (($promotionData['applicable_id'] == 1 || in_array($cartProduct['user_id'], $usersArr) || $isGlobalCampaign == 1) && ($promotionData['product_type'] == 1 || in_array($cartProduct['product_id'], $productsArr)) && $cartProduct['is_promotional'] == '1') {
                                $cartProductArr[$i]['product_discount'] = round(($promotionData['discount'] / 100) * ($cartProductArr[$i]['product_subtotal'] - $cartProduct['membership_discount']), 2);
                                $productAmount = ($cartProduct['product_subtotal'] - $cartProduct['membership_discount']);
                                $cartProductArr[$i]['is_free_shipping'] = ($isFreeShipping == '1' && ($productAmount >= $freeShippingAmount || $freeShippingAmount = '0' || $freeShippingAmount = '')) ? '1' : '0';
                                /* if ($isFreeShipping) {
                                  $cartProducts[$i]['shipping_price'] = 0;
                                  $cartProducts[$i]['surcharge_price'] = 0;
                                  $cartProducts[$i]['shipping_handling_price'] = 0;
                                  } */
                            } else {
                                $cartProductArr[$i]['is_free_shipping'] = '0';
                            }
                        }
                        $i++;
                    }
                } elseif ($flag == 'totaldiscount') {
                    $totalDiscount = 0;

                    //   asd($cartProductArr);
                    foreach ($cartProductArr as $cartProduct) {
                        if ($cartProduct['product_id'] != '' && $cartProduct['product_id'] != 0) {
                            if (($promotionData['applicable_id'] == 1 || in_array($cartProduct['user_id'], $usersArr) || $isGlobalCampaign == 1) && ($promotionData['product_type'] == 1 || in_array($cartProduct['product_id'], $productsArr)) && $cartProduct['is_promotional'] == '1') {
                                $productDiscount = round(($promotionData['discount'] / 100) * ($cartProductArr[$i]['product_subtotal'] - $cartProduct['membership_discount']), 2);
                                $totalDiscount = $totalDiscount + $productDiscount;
                                if (($promotionData['applicable_to'] == 2 && $shipping_country != 228) || ($promotionData['applicable_to'] == 1 && $shipping_country == 228) || $promotionData['applicable_to'] == 3) {
                                    if ($promotionData['is_free_shipping'])
                                        $cartProduct['is_free_shipping'] = '1';
                                }
                                $totalProductWeightDiscount = $totalProductWeightDiscount + $cartProduct['product_weight'] * $cartProduct['product_qty'];
                                if ($cartProduct['is_shippable'] == '1' && $cartProduct['product_weight'] > 0 && $cartProduct['shipping_cost'] == '1' && $cartProduct['is_free_shipping'] == 0) {
                                    $totalProductWeight = $totalProductWeight + $cartProduct['product_weight'] * $cartProduct['product_qty'];
                                }
                            } else {
                                if ($cartProduct['is_shippable'] == '1' && $cartProduct['product_weight'] > 0 && $cartProduct['shipping_cost'] == '1' && $cartProduct['is_free_shipping'] == 0)
                                    $totalProductWeight = $totalProductWeight + $cartProduct['product_weight'] * $cartProduct['product_qty'];
                            }
                        }

                        $i++;
                    }

                    $retArr['totalDiscount'] = $totalDiscount;
                    $retArr['totalProductWeight'] = $totalProductWeight;
                    $retArr['totalProductWeightDiscount'] = $totalProductWeightDiscount;
                    //  asd($retArr); // trace
                    return $retArr;
                }
            }
        }
        return $cartProductArr;
    }

    /**
     * This action is used to apply coupon discount in cart array
     * @param int,array
     * @return array
     * @author Icreon Tech - DT
     */
    public function getCouponDiscountForProductInCart($paramArr = array(), $shipping_country) {
        $promotionId = isset($paramArr['promotion_id']) ? $paramArr['promotion_id'] : '';
        $userId = isset($paramArr['user_id']) ? $paramArr['user_id'] : '';
        $cartParam['user_id'] = $userId;
        $cartParam['source_type'] = !empty($paramArr['source_type']) ? $paramArr['source_type'] : 'frontend';
        $cartProductArr = $this->getTransactionTable()->getCartProducts($cartParam);

        $totalDiscount['discount'] = 0;
        if (!empty($cartProductArr) && !empty($paramArr['promotionData'])) {
            $promotionData = $paramArr['promotionData'];
            $usersArr = array();
            $productsArr = array();
            if ($promotionData['promotion_user_ids'] != '') {
                $usersArr = explode(',', $promotionData['user_ids']);
            }
            if ($promotionData['promotion_product_ids'] != '') {
                $productsArr = explode(',', $promotionData['product_ids']);
            }

            /* public campaign check start */
            $isGlobalCampaign = 0;
            $this->campaignSession = new Container('campaign');
            if (isset($this->campaignSession->campaign_code)) {
                $campParam = array('', $this->campaignSession->campaign_code);
                $campaign = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCampaign($campParam);

                if (isset($campaign[0]['is_global']) && $campaign[0]['is_global'] == 1) {
                    $isGlobalCampaign = 1;
                }
                //echo "<pre>";print_r($campaign);die;
            }
            /* public campaign check ends */

            $i = 0;
            $totalDiscount['apply'] = 0;

            foreach ($cartProductArr as $key => $cartProduct) {
                if ($cartProduct['product_id'] != '' && $cartProduct['product_id'] != 0) {
                    if (($promotionData['applicable_id'] == 1 || in_array($cartProduct['user_id'], $usersArr) || $isGlobalCampaign == 1) && ($promotionData['product_type'] == 1 || in_array($cartProduct['product_id'], $productsArr)) &&
                            $cartProduct['is_promotional'] == '1'
                    ) {
                        $productDiscount = round(($promotionData['discount'] / 100) * ($cartProductArr[$i]['product_subtotal'] - $cartProductArr[$i]['membership_discount']), 2);
                        $totalDiscount['discount'] = $totalDiscount['discount'] + $productDiscount;
                        $totalDiscount['apply']++;

                        if (($paramArr['promotionData']['applicable_to'] == 2 && $shipping_country != 228) || ($paramArr['promotionData']['applicable_to'] == 1 && $shipping_country == 228) || $paramArr['promotionData']['applicable_to'] == 3) {
                            if ($paramArr['promotionData']['is_free_shipping'])
                                $cartProductArr[$key]['is_free_shipping'] = '1';
                        }
                    }
                }
                $i++;
            }
        }
        $totalDiscount['cartArray'] = $cartProductArr;
        return $totalDiscount;
    }

    /**
     * This action is used to show get crm cart cart products on refresh
     * @param void
     * @return     array
     * @author Icreon Tech - DT
     */
    public function updateProductQtyAction() {
        $this->getConfig();
        $this->checkUserAuthentication();
        $productStatusForm = new ProductStatusForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $msgArr = $this->_config['transaction_messages']['config']['view_crm_transaction'];
        $this->getTransactionTable();
        $viewModel = new ViewModel();
        $params = $this->params()->fromRoute();
        $searchParam['transactionId'] = $this->decrypt($params['id']);
        $count = count($request->getPost('product_status'));
        $product_status = $request->getPost('product_status');
        $product_pre_status = $request->getPost('product_pre_status');
        $product_type_id = $request->getPost('product_type_id');
        $product_type = $request->getPost('product_type');
        $product_id = $request->getPost('product_id');
        $product_qty = $request->getPost('product_qty');
        $product_ttl_qty = $request->getPost('product_total_qty');
        $product_shp_qty = $request->getPost('product_shp_qty');
        $productInfo = array();
        $dataFlag = 0;
        $transaction = new Transaction($this->_adapter);
        /** Shipment code for release, partial release and partial process */
        for ($i = 0; $i < $count; $i++) {
            $data['productTypeId'] = $product_type_id[$i];
            $data['productType'] = $product_type[$i];
            $data['productId'] = $product_id[$i];
            $data['productStatus'] = $product_status[$i];
            $data['productPreStatus'] = $product_pre_status[$i];
            $data['productQty'] = $product_qty[$i];
            $data['productTtlQty'] = $product_qty[$i];
            $data['modified_by'] = $this->auth->getIdentity()->crm_user_id;
            $data['modified_date'] = DATE_TIME_FORMAT;
            $data['transactionId'] = $searchParam['transactionId'];
            if ($data['productStatus'] == $data['productPreStatus']) {
                if ($data['productStatus'] == '4' || $data['productStatus'] == '5' || $data['productStatus'] == '9') {
                    if ($dataFlag == 0) {
                        $data['shipmentId'] = $this->getTransactionTable()->insertShipment($data);
                        $dataFlag = 1;
                        $shipmentArr = array();
                        $shipmentArr['transaction_shipment_id'] = $data['shipmentId'];
                        $getShipmentIdArr = $transaction->getShippmentIdArr($shipmentArr);
                        $transactionShipmentDetail = $this->_transactionTable->getShippmentByTransShipment($getShipmentIdArr);
                        $productInfo['shipmentId'] = isset($transactionShipmentDetail[0]['shipment_id']) ? $transactionShipmentDetail[0]['shipment_id'] : '0';
                    }
                    $lastProductShipmentId = $this->getTransactionTable()->insertShipmentProduct($data);
                    $productInfo['transactionId'] = $searchParam['transactionId'];
                    $productInfo['productIds'][] = $product_id[$i];
                    $productInfo['productQtys'][] = $product_qty[$i];
                    $productInfo['productShipmentIds'][] = $lastProductShipmentId;
                    if ($product_shp_qty[$i] > $product_qty[$i]) {
                        if ($data['productStatus'] == '4') {
                            $data['productStatus'] = '9';
                        }
                        $product_status[$i] = $data['productStatus'];
                    } else {
                        $data['productStatus'] = '8';
                        $product_status[$i] = $data['productStatus'];
                    }
                }
            }
        }
        $customFrameId = $this->_config['custom_frame_id'];
        $shipResponse = $this->loadOrderXml($productInfo, $customFrameId);
        /** update product status */
        if (isset($data['shipmentId']) && !empty($data['shipmentId']) && $shipResponse != 1) {
            $this->getTransactionTable()->deleteShipment($data['shipmentId']);
            $this->flashMessenger()->addMessage($msgArr['L_SHIP_ERROR']);
        }
        for ($i = 0; $i < $count; $i++) {
            $data['productPreStatus'] = $product_pre_status[$i];
            if ($data['productPreStatus'] < '8' || $data['productPreStatus'] == '9') {
                $data['productTypeId'] = $product_type_id[$i];
                $data['productType'] = $product_type[$i];
                $data['productId'] = $product_id[$i];
                $data['productQty'] = $product_qty[$i];
                $data['productStatus'] = $product_status[$i];
                $data['productTtlQty'] = $product_qty[$i];
                $data['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                $data['modified_date'] = DATE_TIME_FORMAT;
                $data['transactionId'] = $searchParam['transactionId'];
                switch ($data['productTypeId']) {
                    case 'pro':$data['table'] = "tbl_tra_transaction_products";
                        $data['field'] = "transaction_product_id";
                        break;
                    case 'bio' :$data['table'] = "tbl_tra_bio_certificates";
                        $data['field'] = "bio_certificate_id";
                        break;
                    case 'woh' :
                        $data['table'] = "tbl_usr_wall_of_honor";
                        $data['field'] = "woh_id";
                        break;
                    case 'fof' :
                        $data['table'] = "tbl_usr_flag_of_faces";
                        $data['field'] = "fof_id";
                        break;
                }
                if (isset($data['table']) && !empty($data['table'])) {
                    if ($data['productPreStatus'] == '4' || $data['productPreStatus'] == '5' || $data['productPreStatus'] == '9') {
                        if ($shipResponse == '1' && $data['productPreStatus'] <= $data['productStatus']) {
                            $this->getTransactionTable()->updateProductStatus($data);
                        } else if ($data['productPreStatus'] > $data['productStatus']) {
                            $this->getTransactionTable()->updateProductStatus($data);
                        } else if ($data['productStatus'] == '6' || $data['productStatus'] == '7') {
                            $this->getTransactionTable()->updateProductStatus($data);
                        } else {
                            $data['productStatus'] = $data['productPreStatus'];
                            $this->getTransactionTable()->updateProductStatus($data);
                        }
                    } else {
                        if ($data['productStatus'] < '8') {
                            $this->getTransactionTable()->updateProductStatus($data);
                        }
                    }
                }
            }
        }
        $searchProduct = $this->getTransactionTable()->searchTransactionProducts($searchParam);
        $transactionStatus = 0;
        $traCount = 0;
        $cancelCount = 0;
        $holdCount = 0;
        $voidedShipCount = 0;
        $returnCount = 0;
        foreach ($searchProduct as $key => $val) {
            if ($val['product_status'] == '6') {
                $cancelCount++;
            } else if ($val['product_status'] == '7') {
                $holdCount++;
            } else if ($val['product_status'] == '12') {
                $voidedShipCount++;
            } else if ($val['product_status'] == '13') {
                $returnCount++;
            } else {
                if ($transactionStatus < $val['product_status']) {
                    $traCount = 1;
                    $transactionStatus = $val['product_status'];
                } else if ($transactionStatus == $val['product_status']) {
                    $traCount++;
                }
            }
        }
        if ($cancelCount == $count) {
            $transactionStatus = '6';
        } else if ($holdCount == $count) {
            $transactionStatus = '7';
        } else if ($voidedShipCount == $count) {
            $transactionStatus = '12';
        } else if ($returnCount == $count) {
            $transactionStatus = '13';
        } else if (($count > ($traCount + $cancelCount + $voidedShipCount + $returnCount)) && ($transactionStatus == '2' || $transactionStatus == '4' || $transactionStatus == '8' || $transactionStatus == '10')) {
            $transactionStatus++;
        }
// n - insert into transaction log - start
        $changeLogArray = array();
        $changeLogArray['table_name'] = 'tbl_tra_change_logs';
        $changeLogArray['activity'] = '2';/** 2 == for update */
        $changeLogArray['id'] = $searchParam['transactionId'];
        $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
        $changeLogArray['added_date'] = DATE_TIME_FORMAT;
        $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
// n - insert into transaction log - end


        $update['transactionStatus'] = $transactionStatus;
        $update['transactionId'] = $searchParam['transactionId'];
        $update['modified_by'] = $this->auth->getIdentity()->crm_user_id;
        $update['modified_date'] = DATE_TIME_FORMAT;
        $this->getTransactionTable()->updateTransactionStatus($update);
        $messages = array('status' => "success");
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This function is used to send the product for shipping
     * @param void
     * @return     array
     * @author Icreon Tech - DT
     */
    public function loadOrderXml($productInfo = array(), $customFrameId) {
        if (!empty($productInfo['transactionId'])) {
            $searchParam = array();
            $searchParam['transactionId'] = $productInfo['transactionId'];
            $transactionDetail = $this->getTransactionTable()->searchTransactionDetails($searchParam);
            $transactionDetail = $transactionDetail[0];
            $searchProduct = $this->getTransactionTable()->searchTransactionProducts($searchParam);
            $productDetails = array();
            $index = 0;
            $transaction = new Transaction($this->_adapter);
            $count = 0;
            foreach ($searchProduct as $product) {
                //if ($product['category_id'] != $customFrameId) {
                    if (in_array($product['id'], $productInfo['productIds'])) {
                        $key = array_search($product['id'], $productInfo['productIds']);
                        $product['product_shipment_id'] = $productInfo['productShipmentIds'][$key];
                        $productDetails[] = $product;
                    }
                /*} else {
                    $count++;
                }*/
            }
            if (!empty($productDetails) && $transactionDetail['is_pickup'] != 1) {
                $dateTime = DATE_TIME_FORMAT;
                $pbSettingInfo = $this->getServiceLocator()->get('Common\Model\CommonTable')->getPBSetting();
                $shipApiUrl = $pbSettingInfo['pb_api_url'];
                $shipApiUsername = $pbSettingInfo['pb_api_username'];
                $shipApiRequestUsername = $pbSettingInfo['request_username'];
                $shipApiPassword = $pbSettingInfo['pb_api_password'];
                $packageType = $pbSettingInfo['package_type'];
                $countryName = $transactionDetail['shipping_country_code'];
                $residential = ($transactionDetail['shipping_address_type'] == '1') ? 'true' : 'false';
                $costCenter = '3200';
                if ($transactionDetail['is_free_shipping'] == '1') {
                    $checkFreeShipArr = array();
                    $couponDetail = $this->getServiceLocator()->get('Promotions\Model\PromotionTable')->getPromotionDetailByCode($transactionDetail['coupan_code']);
                    $checkFreeShipArr['promotion_id'] = $couponDetail['promotion_id'];
                    $checkFreeShippingArr = $transaction->checkFreeShippingArr($checkFreeShipArr);
                    $campaignData = $this->_transactionTable->checkFreeShipping($checkFreeShippingArr);
                    $costCenter = $campaignData['program_code'];
                }
                $shippingPhoneNumber = substr($transactionDetail['shipping_phone_no'], strrpos($transactionDetail['shipping_phone_no'], '-') + 1);

                $xmlOrderLoadRequest = '<PierbridgeOrderLoadRequest>
            <Header>
                <OrderNumber>' . $productInfo['shipmentId'] . '</OrderNumber>
                <CustomerNumber>' . $transactionDetail['contact_id'] . '</CustomerNumber>
                <CustomIdentifier/>
                <ShipDate>' . $dateTime . '</ShipDate>
                <DeliverBy></DeliverBy>
                <WayBillNumber/>
                <Ship>
                    <To>' . ((!empty($transactionDetail['shipping_title'])) ? $transactionDetail['shipping_title'] . ' ' : '') . $transactionDetail['shipping_first_name'] . ' ' . $transactionDetail['shipping_last_name'] . '</To>
                    <Company>' . $transactionDetail['shipping_company'] . '</Company>
                    <AddressOne>' . $transactionDetail['shipping_address'] . '</AddressOne>
                    <AddressTwo/>
                    <AddressThree/>
                    <City>' . $transactionDetail['shipping_city'] . '</City>
                    <State>' . $transactionDetail['shipping_state'] . '</State>
                    <Zip>' . $transactionDetail['shipping_postal_code'] . '</Zip>
                    <Country>' . $countryName . '</Country>
                    <Phone>' . $shippingPhoneNumber . '</Phone>
                    <Email>' . $transactionDetail['email_id'] . '</Email>
                    <Residential>' . $residential . '</Residential>
                </Ship>
                <Bill>
                    <To/>
                    <Company/>
                    <AddressOne/>
                    <AddressTwo/>
                    <AddressThree/>
                    <City/>
                    <State/>
                    <Zip/>
                    <Country/>
                    <Phone/>
                    <AccountNumber/>
                </Bill>
                <Return>
                    <To/>
                    <Company/>
                    <AddressOne></AddressOne>
                    <AddressTwo/>
                    <AddressThree/>
                    <City></City>
                    <State></State>
                    <Zip></Zip>
                    <Country></Country>
                    <Phone/>
                    <Email/>
                </Return>
                <Order>
                    <ReferenceOne/>
                    <ReferenceTwo/>
                    <ReferenceThree/>
                    <ShipViaCode>' . $transactionDetail['ship_via_code'] . '</ShipViaCode>
                    <Value/>
                    <Comments/>
                </Order>
                <Record>
                    <KeyOne>' . $costCenter . '</KeyOne>
                    <KeyTwo/>
                    <KeyThree/>
                    <KeyFour/>
                    <KeyFive/>
                    <KeySix/>
                    <KeySeven/>
                    <KeyEight/>
                    <KeyNine/>
                    <KeyTen>' . $transactionDetail['description'] . '</KeyTen>
                    <KeyEleven/>
                    <KeyTwelve/>
                    <KeyThirteen/>
                    <KeyFourteen/>
                    <KeyFifteen/>
                    <KeySixteen/>
                    <KeySeventeen/>
                    <KeyEighteen/>
                    <KeyNineteen/>
                    <KeyTwenty/>
                </Record>
            </Header>
            <LineItems>';
                foreach ($productDetails as $productDetail) {
                    $xmlOrderLoadRequest.='<LineItem>
                    <PurchaseOrderNumber/>
                    <RequiresSerial/>
                    <Item>
                        <PartNumber>' . $productDetail['product_sku'] . '</PartNumber>
                        <LotNumber/>
                        <BinNumber/>
                        <Warehouse>1611</Warehouse>
                        <Description>' . $productDetail['product_name'] . '</Description>
                        <Quantity>' . $productDetail['qty_shipped'] . '</Quantity>
                        <UnitPrice>' . $productDetail['product_price'] . '</UnitPrice>
                        <Weight>' . $productDetail['product_weight'] . '</Weight>
                        <UnitsOfMeasure>EACH</UnitsOfMeasure>
                        <Hazardous>false</Hazardous>
                    </Item>
                    <Record>
                        <KeyOne>' . $productDetail['shipping_description'] . '</KeyOne>
                        <KeyTwo>' . $countryName . '</KeyTwo>
                        <KeyThree/>
                        <KeyFour/>
                        <KeyFive/>
                        <KeySix/>
                        <KeySeven/>
                        <KeyEight/>
                        <KeyNine/>
                        <KeyTen/>
                        <KeyEleven/>
                        <KeyTwelve/>
                        <KeyThirteen/>
                        <KeyFourteen/>
                        <KeyFifteen/>
                        <KeySixteen/>
                        <KeySeventeen/>
                        <KeyEighteen/>
                        <KeyNineteen/>
                        <KeyTwenty/>
                    </Record>
                </LineItem>';
                }
                $xmlOrderLoadRequest.='</LineItems><UserName>' . $shipApiRequestUsername . '</UserName>
        </PierbridgeOrderLoadRequest>';
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $shipApiUrl);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_TIMEOUT, 120);
                curl_setopt($curl, CURLOPT_USERPWD, "$shipApiUsername:$shipApiPassword");
                curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                curl_setopt($curl, CURLOPT_POST, 1);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $this->removeSpecialChar($xmlOrderLoadRequest));
                $response = $this->getResponse();
                $createTransactionMessage = $this->_config['transaction_messages']['config']['create_crm_transaction'];
                if (($result = curl_exec($curl)) === false) {
// moving to display page to display curl errors
                    $msg = array();
                    $msg ['product_qty'] = curl_errno($curl);
                    $messages = array('status' => "shipping_error", 'message' => $msg);
                    echo \Zend\Json\Json::encode($messages);
                    die;
                }
                curl_close($curl);
                $xml = new SimpleXMLElement($result);
                if ($xml->Status->Code == 1) {
                    /* $tranckingParams = array();
                      $tranckingParams['transaction_shipment_id'] = $productInfo['shipmentId'];
                      $tranckingParams['tracking_code'] = $xml->OrderHeaderID;
                      $tranckingParams['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                      $tranckingParams['modified_date'] = $dateTime;
                      $this->getTransactionTable()->updateShipmentTrackingCode($tranckingParams); */
                }
            } else if ($transactionDetail['is_pickup'] == 1) {
                return 1;
            }
            if (!empty($xml->Status->Code)) {
                return $xml->Status->Code;
            } else if ($count != 0) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    /**
     * This action is used to show shipment details
     * @param void
     * @return     array
     * @author Icreon Tech - As
     */
    public function viewShipmentAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $transactionNoteForm = new TransactionNotesForm();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $msgArr = $this->_config['transaction_messages']['config']['view_crm_transaction'];
        $searchParam['transactionId'] = $this->decrypt($params['id']);
        $searchResult = $this->getTransactionTable()->searchTransactionDetails($searchParam);
        $searchProduct = $this->getTransactionTable()->searchShippedProducts($searchParam);
        foreach ($searchProduct as $key => $val) {
            $product = explode(",", $val['product_id']);
            $product_type = explode(",", $val['product_type']);
            for ($i = 0; $i < count($product); $i++) {
                $searchParam['id'] = $product[$i];
                $searchParam['product_type_id'] = $product_type[$i];
                switch ($searchParam['product_type_id']) {
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        $paramResult = $this->getTransactionTable()->searchDocShip($searchParam);
                        $passengerResult[] = $paramResult[0];
                        break;
                    case '10':
                        $param['woh_id'] = $searchParam['id'];
                        $paramResult = $this->getServiceLocator()->get('User/model/UserTable')->getWoh($param);
                        $passengerResult[] = $paramResult[0];
                        break;
                    case '11':
                        $fofResult = $this->getTransactionTable()->searchFofDetails($searchParam);
                        $passengerResult[] = $fofResult[0];
                }
            }
        }
        if (!isset($passengerResult) && empty($passengerResult)) {
            $passengerResult[] = array();
        }
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $transactionNoteForm->get('transaction_id')->setAttribute('value', $searchResult[0]['transaction_id']);
        $notes['note_id'] = explode(",", $searchResult[0]['note_id']);
        $notes['note_user'] = explode(",", $searchResult[0]['note_user']);
        $notes['note'] = explode(",", $searchResult[0]['note']);
        $notes['note_private'] = explode(",", $searchResult[0]['note_private']);
        $notes['name'] = explode(",", $searchResult[0]['name']);
        $notes['date'] = explode(",", $searchResult[0]['date']);
        $notes['role'] = explode(",", $searchResult[0]['crm_role']);
        $viewModel->setVariables(array(
            'searchResult' => $searchResult[0],
            'searchProduct' => $searchProduct,
            'productDetail' => $passengerResult,
            'transactionId' => $params['id'],
            'jsLangTranslate' => $msgArr,
            'params' => $params,
            'transactionNoteForm' => $transactionNoteForm,
            'notes' => $notes,
            'userId' => $this->auth->getIdentity()->crm_user_id,
            'tableName' => 'tbl_tra_notes',
            'tableField' => 'transaction_note_id'
        ));
        return $viewModel;
    }

    /**
     * This Action is used to get billling shipping address info on base of id
     * @param this will pass an array $dataParam
     * @return this will be json format
     * @author Icreon Tech - DT
     */
    public function getBillingShippingAddressInfoFrontAction() {
//$this->checkUserAuthenticationAjx();
        $this->getTransactionTable();
        $transaction = new Transaction($this->_adapter);
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            if ($request->getPost('address_information_id') != '') {
                $searchArray = array();
                $searchArray['address_information_id'] = $request->getPost('address_information_id');
                $searchArray['user_id'] = $request->getPost('user_id');
                $contactDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getContact($searchArray);
                $addressDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations($searchArray);
                $searchArray['is_primary'] = 1; // added on 26 May 2014.
                $phoneDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserPhoneInformations($searchArray);
                $fullAddress = $addressDetail[0]['street_address_one'] . " " . $addressDetail[0]['street_address_two'] . " " . $addressDetail[0]['street_address_three'];
                $fullAddress = trim($fullAddress);
                $addressDetail[0]['full_address'] = $fullAddress;
                $countryCode = (isset($phoneDetail[0]['country_code']) && !empty($phoneDetail[0]['country_code'])) ? $phoneDetail[0]['country_code'] . "-" : "";
                $areaCode = (isset($phoneDetail[0]['area_code']) && !empty($phoneDetail[0]['area_code'])) ? $phoneDetail[0]['area_code'] . "-" : "";
                $phoneNumber = (isset($phoneDetail[0]['phone_number']) && !empty($phoneDetail[0]['phone_number'])) ? $phoneDetail[0]['phone_number'] : "";
                $phoneNumberLabel = $countryCode . $areaCode . $phoneNumber;
                $phoneNumberLabel = rtrim($phoneNumberLabel, "-");

                $addressDetail[0]['address_city'] = (isset($addressDetail[0]['city']) && !empty($addressDetail[0]['city'])) ? $addressDetail[0]['city'] : "";
                $addressDetail[0]['address_state'] = (isset($addressDetail[0]['state']) && !empty($addressDetail[0]['state'])) ? $addressDetail[0]['state'] : "";
                $addressDetail[0]['address_street_address_one'] = (isset($addressDetail[0]['street_address_one']) && !empty($addressDetail[0]['street_address_one'])) ? $addressDetail[0]['street_address_one'] : "";
                $addressDetail[0]['address_street_address_two'] = (isset($addressDetail[0]['street_address_two']) && !empty($addressDetail[0]['street_address_two'])) ? $addressDetail[0]['street_address_two'] : "";
                $addressDetail[0]['address_zip_code'] = (isset($addressDetail[0]['zip_code']) && !empty($addressDetail[0]['zip_code'])) ? $addressDetail[0]['zip_code'] : "";
                $addressDetail[0]['address_country_id'] = (isset($addressDetail[0]['country_id']) && !empty($addressDetail[0]['country_id'])) ? $addressDetail[0]['country_id'] : "";
                $addressDetail[0]['address_country_name'] = (isset($addressDetail[0]['country_name']) && !empty($addressDetail[0]['country_name'])) ? $addressDetail[0]['country_name'] : "";
                $addressDetail[0]['location'] = (isset($addressDetail[0]['location']) && !empty($addressDetail[0]['location'])) ? $addressDetail[0]['location'] : "";
                if (isset($phoneDetail[0]['is_primary']) and trim($phoneDetail[0]['is_primary']) == "1") {

                    $countryCode2 = (isset($phoneDetail[0]['country_code']) && !empty($phoneDetail[0]['country_code'])) ? $phoneDetail[0]['country_code'] : "";
                    $areaCode2 = (isset($phoneDetail[0]['area_code']) && !empty($phoneDetail[0]['area_code'])) ? $phoneDetail[0]['area_code'] : "";
                    $phoneNumber2 = (isset($phoneDetail[0]['phone_number']) && !empty($phoneDetail[0]['phone_number'])) ? $phoneDetail[0]['phone_number'] : "";
                    $phoneContactId2 = (isset($phoneDetail[0]['phone_contact_id']) && !empty($phoneDetail[0]['phone_contact_id'])) ? $phoneDetail[0]['phone_contact_id'] : "";
                } else {
                    $countryCode2 = "";
                    $areaCode2 = "";
                    $phoneNumber2 = "";
                    $phoneContactId2 = "";
                }

                unset($contactDetail[0]['title']);
                unset($contactDetail[0]['first_name']);
                unset($contactDetail[0]['last_name']);
                //  unset($contactDetail[0]['middle_name']);

                $phoneDetailArr = array('phone_number' => $phoneNumberLabel, 'country_code' => $countryCode2, 'area_code' => $areaCode2, 'phone_num' => $phoneNumber2, 'phone_contact_id' => $phoneContactId2);
                $searchResult = json_encode(array_merge($addressDetail[0], $contactDetail[0], $phoneDetailArr));
                return $response->setContent($searchResult);
            }
        }
    }

    function orderreviewCartFrontAction() {
        $this->checkFrontUserAuthentication();
        $this->getTransactionTable();
        $msgArr = $this->_config['transaction_messages']['config']['checkout_front'];
        $transaction = new Transaction($this->_adapter);
        $inventoryForm = new InventoryForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $paramArray = $request->getPost()->toArray();


            // If billing addres is not selected, shipping address will be the billing address
            if (empty($paramArray['billing_address_information_id'])) {
                $paramArray['billing_title'] = $paramArray['shipping_title'];
                $paramArray['billing_first_name'] = $paramArray['shipping_first_name'];
                $paramArray['billing_last_name'] = $paramArray['shipping_last_name'];
                $paramArray['billing_address_1'] = $paramArray['shipping_address_1'];
                $paramArray['billing_address_2'] = $paramArray['shipping_address_2'];
                $paramArray['billing_country'] = $paramArray['shipping_country'];
                $paramArray['billing_country_text'] = $paramArray['shipping_country_text'];
                $paramArray['billing_state'] = $paramArray['shipping_state'];
                $paramArray['billing_state_id'] = $paramArray['shipping_state_id'];
                $paramArray['billing_city'] = $paramArray['shipping_city'];
                $paramArray['billing_zip_code'] = $paramArray['shipping_zip_code'];
                $paramArray['billing_phone_info_id'] = $paramArray['shipping_phone_info_id'];
                $paramArray['billing_country_code'] = $paramArray['shipping_country_code'];
                $paramArray['billing_area_code'] = $paramArray['shipping_area_code'];
                $paramArray['billing_phone'] = $paramArray['shipping_phone'];
                $paramArray['billing_company'] = $paramArray['shipping_company'];
            }
            // End If billing addres is not selected, shipping address will be the billing address

			if($paramArray['pick_up'] == 1) {
                    $shipcode = '';
                }
                else {
                    $shipcode = !empty($paramArray['shipping_zip_code']) ? $paramArray['shipping_zip_code'] : '';

                }
            $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '';
            $cartParam['user_id'] = $user_id;
            $cartParam['source_type'] = 'frontend';
            $cartData = $this->getTransactionTable()->getCartProducts($cartParam);
            $paramArray['userid'] = $user_id;
            $paramArray['shipcode'] = $shipcode;
            $paramArray['couponcode'] = !empty($paramArray['couponcode']) ? $paramArray['couponcode'] : '';
            if ($request->getPost('ship_m') != '' && $request->getPost('status') != 'error') {
//$paramsArray = $request->getPost()->toArray();
//$paramsArray['postData'] = $paramArray;
            }
            $paramArray['postData'] = $paramArray;
//$paramArray[]
            if ($paramArray['pick_up'] == 1) {
                 $paramArray['shipping_method'] = '';
                 $paramsArray['shipping_state'] = '';
			}

            $itemsTotal = $this->cartTotalWithTax($paramArray);
        }

        $discount = '';
        if (!empty($this->auth->getIdentity()->user_id)) {


            $userArr = array('user_id' => $user_id);
            $contactDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserMemberShipDetail($userArr);
            if ($contactDetail['membership_id'] != '1') {
                $discount = $contactDetail['membership_discount'];
            }
        }
        $memberShip = array();
        $memberdiscount = '';
        if (empty($discount) && $itemsTotal['finaltotaldiscounted'] != 0) {
            $memberShip = $this->getTransactionTable()->getMinimumMembership();
            $memberdiscount = ($itemsTotal['finaltotaldiscounted'] * $memberShip[0]['discount']) / 100;
        }
        $isBookingKiosk = 0;
        $requestingIp = $this->getServiceLocator()->get('KioskModules')->getRemoteIP();
        if ($requestingIp) {
            if (isset($this->_config['kiosk-system-settings'][$requestingIp]))
                $isBookingKiosk = $this->_config['kiosk-system-settings'][$requestingIp]['isHotSystem'];
        }
        $viewModel = new ViewModel();

        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => array_merge($msgArr, $this->_config['transaction_messages']['config']['common_message']),
            'postdata' => $paramArray,
            'cartlist' => $cartData,
            'memberdiscount' => $memberdiscount,
            'memberShip' => $memberShip,
            'itemsTotal' => $itemsTotal,
            'upload_file_path' => $this->_config['file_upload_path'],
            'membershipDiscountAmount' => $itemsTotal['membershipDiscountAmount'],
            'isBookingKiosk' => $isBookingKiosk,
            'pick_up' => $request->getPost('pick_up'),
            'cash_payment' => $request->getPost('cash_payment')
        ));
        return $viewModel;
    }

    /**
     * This action is used to add more check block
     * @param void
     * @return void
     * @author Icreon Tech - DT
     */
    public function addCheckAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $totalSaveSearch = $request->getPost('totalSaveSearch');
            $this->getTransactionTable();
            $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
            $commonMsg = $this->_config['transaction_messages']['config']['common_message'];
            $paymentModeCheck = new PaymentModeCheck();
            $batchForm = new UpdateTransactionBatchForm();
            /* Get check types */
            $checkTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCheckTypes();
            $checkTypeList = array();
            foreach ($checkTypes as $key => $val) {
                $checkTypeList[$val['cheque_type_id']] = $val['cheque_type_name'];
            }
            $paymentModeCheck->get('payment_mode_chk_type[]')->setAttribute('options', $checkTypeList);

            /* End get check types */

            /* Get check types */
            $checkTypeIds = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCheckIDTypes();
            $checkTypeIdsList = array();
            foreach ($checkTypeIds as $key => $val) {
                $checkTypeIdsList[$val['id_type_id']] = $val['id_type_name'];
            }
            $paymentModeCheck->get('payment_mode_chk_id_type[]')->setAttribute('options', $checkTypeIdsList);

            /* End get check types */

            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => array_merge($msgArr, $commonMsg, $this->_config['transaction_messages']['config']['update_transaction_batch']),
                'paymentModeCheck' => $paymentModeCheck,
                'batchForm' => $batchForm,
                'totalSaveSearch' => $totalSaveSearch
            ));
            return $viewModel;
        } else {
            return $this->redirect()->toRoute('crm-transaction');
        }
    }

    function cartTotalWithTax($paramArray) {
        $user_id = $paramArray['userid'];
        $shippingcode = !empty($paramArray['shipcode']) ? $paramArray['shipcode'] : '';
        $couponCode = isset($paramArray['postData']['coupon_code']) ? $paramArray['postData']['coupon_code'] : $paramArray['couponcode'];
		if(strpos($couponCode,'(')){
				$couponCodeArray = explode('(',$couponCode);
				$couponCode = trim($couponCodeArray[0]);
		}
        $source_all = !empty($paramArray['source_all']) ? $paramArray['source_all'] : '';
        $cartParam['user_id'] = $user_id;
        $cartParam['source_type'] = !empty($paramArray['source_type']) ? $paramArray['source_type'] : 'frontend';
		$cartParam['user_session_id'] = session_id();
        if (defined('MEMBERSHIP_DISCOUNT'))
            $cartParam['membership_percent_discount'] = MEMBERSHIP_DISCOUNT;
        else
            $cartParam['membership_percent_discount'] = '';

        $cartData = $this->getTransactionTable()->getCartProducts($cartParam);
		$promotionTable = $this->getServiceLocator()->get('Promotions\Model\PromotionTable');		
		$getPromotionDetail = $promotionTable->getPromotionDetailByCode($couponCode, 'code');
         if (!empty($getPromotionDetail['excluded_product_type'])) { // Exclude product
            $excludeProductTypeArray = explode(',', $getPromotionDetail['excluded_product_type']);
        }
        if (!empty($getPromotionDetail['excluded_products'])) {
            $excludeProductArray = explode(',', $getPromotionDetail['excluded_products']);
        }


        $totalCartAmount = 0;
        $totalShippableProductWeight = 0;
        $totalPromotionalCartAmount = 0;
        $totalPromotionalShippableProductWeight = 0;
        foreach ($cartData as $key => $val) {
            $totalCartAmount = $totalCartAmount + $val['product_subtotal'] - $val['membership_discount'];
            if ($val['is_shippable'] == 1 && $val['shipping_cost'] == 1 && $val['is_free_shipping'] == 0 && $val['product_weight'] > 0) {
                $totalShippableProductWeight+= ($val['product_weight'] * $val['product_qty']);
                if ($val['is_promotional'] == 1) {
                    $totalPromotionalShippableProductWeight+= ($val['product_weight'] * $val['product_qty']);
                    //$totalPromotionalCartAmount+= $val['product_subtotal'] - $val['membership_discount'];
                }
            }
			if ($val['is_promotional'] == 1 && (!in_array($val['product_type_id'], $excludeProductTypeArray)) && (!in_array($val['product_id'], $excludeProductArray))) {
				//$totalPromotionalShippableProductWeight+= ($val['product_weight'] * $val['product_qty']);
				$totalPromotionalCartAmount+= $val['product_subtotal'] - $val['membership_discount'];
			}
        }

        $subTotal = 0;
        $authorizeAmountTotal = 0;
         $is_pickup = 0;
        /** TAX * */
        $taxDetail = array();

        if (((isset($paramArray['pick_up']) && $paramArray['pick_up'] == 1) ||(isset($paramArray['postData']['pick_up']) && $paramArray['postData']['pick_up'] == 1)) || ($paramArray['shipping_country'] == $paramArray['apo_po_shipping_country'] || $paramArray['shipping_country'] == 228) || ($paramArray['postData']['shipping_country'] == $paramArray['postData']['apo_po_shipping_country'] || $paramArray['postData']['shipping_country'] == 228)) {

            if(isset($paramArray['pick_up']) && $paramArray['pick_up'] == 1)
                $is_pickup = 1;
            elseif(isset($paramArray['postData']['pick_up']) && $paramArray['postData']['pick_up'] == 1)
                $is_pickup = 1;

			if ($is_pickup == 1) {
				$shippingcode = '10004';
			}

            if (!empty($shippingcode)) {
                $postal_code = substr($shippingcode, 0, 5);
                $postal_code = explode('-', $postal_code);
                $postArray['postal_code'] = $postal_code[0];

                if (isset($paramArray['shipping_state']))
                    $postArray['shipping_state'] = $paramArray['shipping_state'];
                else if (isset($paramArray['postData']['shipping_state']))
                    $postArray['shipping_state'] = $paramArray['postData']['shipping_state'];

                if ($is_pickup == 1) {
                    $postArray['shipping_state'] = 'NY';
                }
                $taxDetail = $this->getTransactionTable()->getTaxDetail($postArray);
            }
        }

        if (isset($taxDetail[0]['CombinedRate']) && !empty($taxDetail[0]['CombinedRate'])) {
            $combined_sales_tax = $taxDetail[0]['CombinedRate'];
        }
        $paramArray['combined_sales_tax'] = $combined_sales_tax;
        /* end of tax* */

        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $application_source_id = $this->_config['application_source_id'];
        /** code for promotion * */
        $promotionTable = $this->getServiceLocator()->get('Promotions\Model\PromotionTable');
        $couponCode = isset($paramArray['postData']['coupon_code']) ? $paramArray['postData']['coupon_code'] : $paramArray['couponcode'];
        if (!empty($couponCode)) {
            $getPromotionDetail = $promotionTable->getPromotionDetailByCode($couponCode, 'code');

            $getPromotionDetail['promotionbundledbroducts'] = array();
            if ((isset($getPromotionDetail['bundle_product_type']) && $getPromotionDetail['bundle_product_type'] != '') || (isset($getPromotionDetail['bundle_products']) && $getPromotionDetail['bundle_products'] != '')) {
                $arrBundleProduct = $promotionTable->getPromotionBundledProducts($getPromotionDetail['bundle_product_type'], $getPromotionDetail['bundle_products']);
                foreach ($arrBundleProduct as $bundleProduct) {
                    $getPromotionDetail['promotionbundledbroducts'][] = $bundleProduct['product_id'];
                }
            }

            $getPromotionDetail['promotionMinimumAmountProducts'] = array();
            if ((isset($getPromotionDetail['minimum_amount_product_type']) && $getPromotionDetail['minimum_amount_product_type'] != '') || (isset($getPromotionDetail['minimum_amount_products']) && $getPromotionDetail['minimum_amount_products'] != '')) {
                $arrMinimumAmountProduct = $promotionTable->getPromotionMinimumAmountProducts($getPromotionDetail['minimum_amount_product_type'], $getPromotionDetail['minimum_amount_products']);
                foreach ($arrMinimumAmountProduct as $minimumAmountProduct) {
                    $getPromotionDetail['promotionMinimumAmountProducts'][] = $minimumAmountProduct['product_id'];
                }
            }

			$getPromotionDetail['promotionAnyProducts'] = array();
            if ((isset($getPromotionDetail['promotion_any_product_type']) && $getPromotionDetail['promotion_any_product_type'] != '') || (isset($getPromotionDetail['promotion_any_products']) && $getPromotionDetail['promotion_any_products'] != '')) {
                $arrAnyProduct = $promotionTable->getPromotionMinimumAmountProducts($getPromotionDetail['promotion_any_product_type'], $getPromotionDetail['promotion_any_products']);
                foreach ($arrAnyProduct as $anyProduct) {
                    $getPromotionDetail['promotionAnyProducts'][] = $anyProduct['product_id'];
                }
            }

            if (!empty($getPromotionDetail)) {
                $promotionId = $getPromotionDetail['promotion_id'];
                // By Dev 2 NEWPROMOCODE.

                $returnArray = $this->isValidCouponCheck($selectedShipping_method, $user_id, $paramArray['postData']['shipping_country'], $getPromotionDetail, $cartData);

                $totalProductCartAmount = isset($returnArray['totalProductCartAmount']) ? $returnArray['totalProductCartAmount'] : 0;
                if ($returnArray['status'] == 'error') {
                    $getPromotionDetail['is_valid_coupon'] = 0;
                } else {
                    $getPromotionDetail['is_valid_coupon'] = 1;
                    // By Dev 2 NEWPROMOCODE

                    $inValidShippingMethod = 0;

                    if (($getPromotionDetail['applicable_to'] == 1 || $getPromotionDetail['applicable_to'] == 3) && (isset($paramArray['postData']['shipping_method']) && $paramArray['postData']['shipping_method'] != '') && $paramArray['postData']['shipping_country'] == '228') {

                        $shipping_method_array = explode(",", $paramArray['postData']['shipping_method']);
                        $selectedShipping_method = trim($shipping_method_array[2]);

                        $selectedPromoShippingMethod = $getPromotionDetail['pb_shipping_type_id'];
                        if ($selectedPromoShippingMethod != '' && !is_null($selectedPromoShippingMethod)) {
                            $shipMethodPromoArray = explode(",", $selectedPromoShippingMethod);
                        }
                        $shipingM = array();
                        if ($selectedShipping_method != '' && !in_array($selectedShipping_method, $shipMethodPromoArray)) {
                            $returnArray['status'] = "error";
                            $transaction = new Transaction($this->_adapter);
                            foreach ($shipMethodPromoArray as $shippingPromoId) {
                                $shippingMetArr = array();
                                $shippingMetArr['pb_shipping_type_id'] = $shippingPromoId;
                                $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                                $getShippingMethods = $this->_transactionTable->getShippingMethod($shippingMethodArr);

                                $shipingM[] = $getShippingMethods[0]['web_selection'];
                            }

                            $ship_method_name = '"' . implode('" OR "', $shipingM) . '"';

                            $returnArray['message'] = "Please select " . $ship_method_name . " shipping method for free shipping";
                            $inValidShippingMethod = 1;
                            //$getPromotionDetail['is_valid_coupon'] = 0;
                        }
                    }
                    if ($inValidShippingMethod == 0) {

                        $cartData = $this->applyCouponDiscountInCart($getPromotionDetail, $cartData, $paramArray['postData']['shipping_country'], $totalPromotionalCartAmount, $totalProductCartAmount, $totalPromotionalShippableProductWeight);
                    }
                }
            } else {
                $returnArray['status'] = "error";
                $returnArray['message'] = $msgArr['COUPON_CODE_NOT_EXIST'];
                $getPromotionDetail['is_valid_coupon'] = 0;
            }
        }
        /** end of promotion code * */
        $totalProductWeightWeight = 0;
        $isInventory = 0;
        //asd($cartData);

        foreach ($cartData as $key => $value) {
            if (($this->_config['application_source_id'] == 1) && ($value['is_shippable'] == '0' || empty($value['is_shippable']) || $value['shipping_cost'] == '0')) {
                $isInventory++;
            }

            if ($value['is_free_shipping'] != 1 && $value['product_weight'] > 0 && $value['is_shippable'] == 1 && $value['shipping_cost'] == 1) {
                $totalProductWeightWeight+= ($value['product_weight'] * $value['product_qty']);
            }
        }
        if (empty($cartData)) {
            $totalproduct = 0;
        } else {
            $totalproduct = count($cartData);
        }
        //asd($cartData);
        if($is_pickup=='1'){
             $cartDataArray = $this->addTaxToProducts($cartData, $paramArray);
         }else{
             $cartDataArray = $this->addShippingCostToProducts($cartData, $paramArray, $totalProductWeightWeight);
         }
        $cartData = $cartDataArray['cartdata'];
        $cartDataTotal = $cartDataArray['cartdataTotal'];
        //asd($cartDataArray);
        $totalWeight = $totalProductWeightforPB;
        $returnArray['isAnyNotPickupField'] = 0;
        $returnArray['isInventory'] = $isInventory;
        // $returnArray['isInventory'] = 0;

        $returnArray['totalproduct'] = $totalproduct;
        $returnArray['totalweight'] = $totalProductWeightWeight;
        $returnArray['itemtotal'] = $this->Currencyformat($cartDataTotal['subTotalAmount']);
        $returnArray['itemtotalwoformat'] = round($cartDataTotal['subTotalAmount'] + $cartDataTotal['shipping_charge'], 2);
        //$returnArray['coupondiscount'] = $this->Roundamount($coupondis);
        $returnArray['coupondiscount'] = $this->Currencyformat($cartDataTotal['total_discount']);
        $returnArray['shipping'] = $this->Currencyformat($cartDataTotal['shipping_charge']);
        $returnArray['totalaftershipping'] = $this->Currencyformat($cartDataTotal['totalaftershipping']);
        $returnArray['estimatedtax'] = $this->Currencyformat($cartDataTotal['totalTaxAmount']);
        $returnArray['finaltotal'] = $this->Currencyformat($cartDataTotal['order_total']);
        $returnArray['finaltotaldiscounted'] = $this->Currencyformat($cartDataTotal['total_discount']);
        //$returnArray['finaltotaldonationamount'] = ($donationTotalAmount != 0) ? $this->Roundamount($donationTotalAmount + $shipping + $estimatedtax) : 0;
        $returnArray['finaltotaldonationamount'] = 0;
        //$returnArray['isOnlyDonation'] = $isOnlyDonation;
        $returnArray['isOnlyDonation'] = 0;
        $returnArray['membershipDiscountAmount'] = $cartDataTotal['total_membership_discount'];
        $returnArray['validPromotionsForUser'] = $validPromotionsForUser;
        $returnArray['application_source_id'] = $application_source_id;
        $returnArray['cartData'] = $cartData;

        return $returnArray;
    }

    /**
     * This function is used to check valid address
     * @param void
     * @return void
     * @author Icreon Tech - DT
     */
    public function validShippingAddress($postArr = array()) {
        if (((isset($postArr['is_apo_po']) && trim($postArr['is_apo_po']) == '1') || (isset($postArr['postData']['is_apo_po']) && trim($postArr['postData']['is_apo_po']) == '1')) && !empty($postArr['shipping_method']) && trim($postArr['shipping_method']) != 'undefined') {
            $pbSettingInfo = $this->getServiceLocator()->get('Common\Model\CommonTable')->getPBSetting();
            $shipApiUrl = $pbSettingInfo['pb_api_url'];
            $shipApiUsername = $pbSettingInfo['pb_api_username'];
            $shipApiRequestUsername = $pbSettingInfo['request_username'];
            $shipApiPassword = $pbSettingInfo['pb_api_password'];
            $packageType = $pbSettingInfo['package_type'];
            $carrShipArr = explode(',', $postArr['shipping_method']);
            $carrierId = $carrShipArr[0];
            $serviceType = @$carrShipArr[1];
            $postData = $postArr['postData'];

            $validAddressRequest = '<?xml version = "1.0" encoding = "utf-8"?><PierbridgeAddressValidationRequest>
            <TransactionIdentifier />
            <UserName>' . $shipApiRequestUsername . '</UserName>
            <Carrier>' . $carrierId . '</Carrier>
            <Address>
                <Street>' . $postData['shipping_address'] . '</Street>
                <City>' . $postData['shipping_city'] . '</City>
                <Region>' . $postData['shipping_state'] . '</Region>
                <PostalCode>' . $postData['shipping_zip_code'] . '</PostalCode>
                <Country>' . $postData['shipping_country'] . '</Country>
                <Residential>' . $postArr['residential'] . '</Residential>
                <Locale><![CDATA[#210]]></Locale>
                <Other></Other>
            </Address>
        </PierbridgeAddressValidationRequest>';

            $curl = curl_init();

            curl_setopt($curl, CURLOPT_URL, $shipApiUrl);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_TIMEOUT, 120);
            curl_setopt($curl, CURLOPT_USERPWD, "$shipApiUsername:$shipApiPassword");
            curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $validAddressRequest);
            $createTransactionMessage = $this->_config['transaction_messages']['config']['create_crm_transaction'];
            $result = curl_exec($curl);
            if (($result = curl_exec($curl)) === false) {

                $msg = array();
                $msg ['shipping_method'] = curl_errno($curl);
                $messages = array('status' => "shipping_error", 'message' => $msg);
                echo \Zend\Json\Json::encode($messages); //commented on 30 MAy 2014.
                die;
            }
            curl_close($curl);
            $xml = new SimpleXMLElement($result);
            if ($xml->Status->Code == 0) {
                $errorDescription = $xml->Status->Description;
//$errorMessage = $createTransactionMessage['INVALID_ADDRESS'];
                $errorType = 3;

                if (strpos($errorDescription, 'country code for Consignee') > 0 || strpos($errorDescription, 'country is not valid') > 0 || strpos($errorDescription, 'country not served') > 0) {
                    $errorType = 1;
                    $errorMessage = $createTransactionMessage['L_INVALID_COUNTRY'];
                } else if (strpos($errorDescription, 'CONSIGNEE') > 0) {
                    $errorType = 2;
                    $errorMessage = $createTransactionMessage['L_INVALID_POSTAL'];
                } else {
                    $errorType = 3;
                    $errorMessage = $createTransactionMessage['INVALID_ADDRESS'];
                }

                $msg = array();
                if ($postData['shipping_zip_code'] == '') {
                    $msg ['shipping_zip_code'] = $createTransactionMessage['SHIPPING_ZIP_CODE_EMPTY'];
                } else if ($postData['shipping_country'] == '') {
                    $msg ['shipping_country'] = $createTransactionMessage['SHIPPING_COUNTRY_EMPTY'];
                } else {
//$msg ['shipping_method'] = $errorMessage;
                    if ($errorType == 1) {
                        $msg ['shipping_country'] = $errorMessage;
                    } else if ($errorType == 2) {
                        $msg ['shipping_zip_code'] = $errorMessage;
                    } else {
                        $msg ['shipping_method'] = $errorMessage;
                    }
                }
                $messages = array('status' => "shipping_error", 'message' => $msg);
                echo \Zend\Json\Json::encode($messages);
                die;
                return false;
            }
            return true;
        } else {
            return true;
        }
    }

    /**
     * This function is used to get shipping cost of product
     * @param void
     * @return void
     * @author Icreon Tech - DT
     */
    public function addShippingCostToProducts($cartProducts, $postArr = array(), $totalProductWeightAmountForShipping) { // dev 2 NEWPROMOCODE
        //echo $totalProductWeightAmountForShipping;die;
        if (!empty($cartProducts) && !empty($postArr)) {
            $postData = $postArr['postData'];
            $countryName = '';
            if (!empty($postData['shipping_country'])) {
                $countryArr = array('country_id' => $postData['shipping_country']);
                $common = new Common($this->_adapter);
                $countryArr = $common->getCountryArr($countryArr);
                $countryData = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCountryUser($countryArr);
                $countryName = $countryData[0]['country_code'];
            }
            $residential = 'true';
            if (isset($postArr['is_apo_po']) && $postArr['is_apo_po'] != '0' && $postArr['is_apo_po'] != '1') {
                $residential = 'false';
            }
            $postArr['residential'] = $residential;
            $postArr['postData']['shipping_country'] = $countryName;


            if (isset($postArr['shipping_method']) && trim($postArr['shipping_method']) != 'undefined' && trim($postArr['shipping_method']) != '') {
                $pbSettingInfo = $this->getServiceLocator()->get('Common\Model\CommonTable')->getPBSetting();
                $shipApiUrl = $pbSettingInfo['pb_api_url'];
                $shipApiUsername = $pbSettingInfo['pb_api_username'];
                $shipApiRequestUsername = $pbSettingInfo['request_username'];
                $shipApiPassword = $pbSettingInfo['pb_api_password'];
                $packageType = $pbSettingInfo['package_type'];
                $carrShipArr = explode(',', $postArr['shipping_method']);
                $carrierId = $carrShipArr[0];
                if($carrierId == '13') 
					$packageType = '33';
                $serviceType = $carrShipArr[1];
                $shipDate = DATE_TIME_FORMAT;
                $holidayDate = $shipDate;
                $validDate = 0;
                $transaction = new Transaction($this->_adapter);
                while (!$validDate) {
                    $holidArr = array('holiday_date' => $holidayDate);
                    $holidayArr = $transaction->getHolidayArr($holidArr);
                    $holidayData = $this->_transactionTable->getHoliday($holidayArr);
                    if (empty($holidayData)) {
                        $validDate = 1;
                        $shipDate = date('Y-m-d h:i:s', strtotime($holidayDate));
                    }
                    $holidayDate = date('Y-m-d', strtotime('+1 day', strtotime($holidayDate)));
                }
                $currentDay = date('N', strtotime($shipDate));
                if ($currentDay > 5) {
                    $nextShipDate = 7 - ($currentDay - 1);
                    $shipDate = date('m-d-Y h:i:s A', strtotime('+' . $nextShipDate . ' day', strtotime($shipDate)));
                }
                $xmlRateRequest = '<?xml version = "1.0" encoding = "utf-8"?><PierbridgeRateRequest>
    <TransactionIdentifier />
    <Orders>
        <Order>
            <OrderID />
            <PickLists>
                <PickList>
                    <PickListID />
                </PickList>
            </PickLists>
        </Order>
    </Orders>
    <InsuranceProvider />
    <Live />
    <CloseShipment />
    <Carrier>' . $carrierId . '</Carrier>
    <ServiceType>' . $serviceType . '</ServiceType>
    <RateType />
    <ShipDate>' . $shipDate . '</ShipDate>
    <RequiredDate />
    <SaturdayDelivery />
    <Location />
    <AccountID />
    <Truckload />
    <ShipmentDescription />
    <FreightAllKinds />
    <Interline />
    <PreferredCarrier />
    <PreferredCarrierCode />
    <AccessorialCodes />
    <LoadingDockDelivery />
    <ConstructionSitePickup />
    <ConstructionSiteDelivery />
    <TradeShowPickup />
    <TradeShowDelivery />
    <InsidePickup />
    <InsideDelivery />
    <LiftGateDelivery />
    <LiftGatePickup />
    <AppointmentDelivery />
    <AppointmentPickup />
    <UnloadFreightAtDelivery />
    <LoadFreightAtPickup />
    <WhiteGlove />
    <TwoManDelivery />
    <DefaultWeightUOM />
    <DefaultDimensionsUOM />
    <ConsolidatedShipmentID />
    <DefaultCurrency />
    <Sender>
        <SentBy />
        <CompanyName />
        <Street />
        <Locale />
        <Other />
        <City />
        <Region />
        <PostalCode />
        <Country />
        <Phone />
        <Email />
        <JobTitle />
        <DepartmentName />
        <LocationDescription />
        <Residential>true</Residential>
        <OverrideType />
    </Sender>
    <Receiver>
        <CompanyName>' . $postData['shipping_company'] . '</CompanyName>
        <Street>' . $postData['shipping_address'] . '</Street>
        <Locale />
        <Other />
        <City>' . $postData['shipping_city'] . '</City>
        <State>' . $postData['shipping_state'] . '</State>
        <PostalCode>' . $postData['shipping_zip_code'] . '</PostalCode>
        <Country>' . $countryName . '</Country>
        <Residential />
        <ReceiverIdentifier />
    </Receiver>
    <ReturnTo>
        <Name />
        <CompanyName />
        <Street />
        <Locale />
        <Other />
        <City />
        <Region />
        <PostalCode />
        <Country />
        <Phone />
    </ReturnTo>
    <Billing>
        <PayerType />
        <AccountNumber />
        <CompanyName />
        <Street />
        <Locale />
        <Other />
        <City />
        <Region />
        <PostalCode />
        <Country />
    </Billing>
    <International>
        <AESTransactionNumber />
        <TermsOfSale />
        <SED>
            <Method />
            <ExemptNumber />
        </SED>
    </International>
    <Packages>';
                $xmlRateRequest.='<Package>
            <Copies />
            <PackItemID />
            <ShipperReference/>
            <ReceiverName>' . $postData['shipping_first_name'] . $postData['shipping_last_name'] . '</ReceiverName>
            <ReceiverPhone />
            <ReceiverEmail />
            <PackageType>' . $packageType . '</PackageType>
            <Weight>' . $totalProductWeightAmountForShipping . '</Weight>
            <WeightUOM />
            <Length/>
            <Width/>
            <Height/>
            <DimensionsUOM />
            <ContentDescription>' . $cartProducts[0]['product_name'] . '</ContentDescription>
            <Insurance>
                <Type />
                <Value />
                <ValueCurrency />
            </Insurance>
            <COD>
                <Type />
                <Value />
                <ValueCurrency />
            </COD>
            <Hold />
            <Holder>
                <Name />
                <CompanyName />
                <Street />
                <Locale />
                <Other />
                <City />
                <Region />
                <PostalCode />
                <Country />
                <Phone />
            </Holder>
            <AdditionalHandling />
            <Oversize />
            <LargePackage />
            <DeliveryConfirmation />
            <FreightClass />
            <NMFC />
            <ItemsOnPallet />
            <NonStandardContainer />
            <HomeDeliveryType />
            <EmailNotification />
            <NonFlatMachinable />
            <NonMachinable />
            <NonRectangular />
            <Flat />
            <Stackable />
            <Registered />
            <Certified />
            <DryIceWeight />
            <DryIceWeightUOM />
            <ShipperRelease />
            <FreezeProtect />
            <PalletExchange />
            <SortAndSegregate />
            <International>
                <DocumentsOnly />
                <Contents>
                    <Content>
                        <Code />
                        <Quantity >' . $cartProducts[0]['product_qty'] . '</Quantity>
                        <OrderedQuantity />
                        <BackOrderedQuantity />
                        <ContentLineValue />
                        <Value />
                        <ValueCurrency />
                        <Weight />
                        <WeightUOM />
                        <Description />
                        <OriginCountry />
                        <PurchaseOrderNumber />
                        <SalesOrderNumber />
                        <ItemCode />
                        <ItemDescription />
                        <UnitsOfMeasure />
                        <CustomerCode />
                        <PartNumber />
                        <BinNumber />
                        <LotNumber />
                        <SerialNumber />
                        <Hazardous />
                        <HazardousExemptionID />
                        <HazardousType />
                        <HazardousUnits />
                        <HazardousUnitsUOM />
                        <HazardousDescription />
                        <HazardousProperShippingName />
                        <HazardousIdentifier />
                        <HazardousClass />
                        <HazardousSubClass />
                        <HazardousPackingGroup />
                        <HazardousTechnicalName />
                        <HazardousTotalQuantity />
                        <HazardousTotalQuantityUOM />
                        <HazardousLabelCodes />
                        <HazardousLabelCodesMask />
                        <HazardousAccessible />
                        <HazardousPassengerAircraft />
                        <HazardousCargoAircraftOnly />
                        <HazardousRequiredInformation />
                        <HazardousConsumerCommodity />
                        <HazardousPackingInstructions />
                        <HazardousIsSpecialProvisionA1A2A51A109 />
                    </Content>
                </Contents>
            </International>
        </Package>';
//    }
//}

                $xmlRateRequest.='</Packages>
    <UserName>' . $shipApiRequestUsername . '</UserName>
</PierbridgeRateRequest>';

                $isAnyProduct = ($totalProductWeightAmountForShipping > 0) ? 1 : 0;

                $totalShipping = 0;

                if ($isAnyProduct == 1) {
                    $curl = curl_init();
                    curl_setopt($curl, CURLOPT_URL, $shipApiUrl);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($curl, CURLOPT_TIMEOUT, 120);
                    curl_setopt($curl, CURLOPT_USERPWD, "$shipApiUsername:$shipApiPassword");
                    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
                    curl_setopt($curl, CURLOPT_POST, 1);
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $this->removeSpecialChar($xmlRateRequest));
                    $response = $this->getResponse();
                    $createTransactionMessage = $this->_config['transaction_messages']['config']['create_crm_transaction'];

                    if (($result = curl_exec($curl)) === false) {
                        $msg = array();
                        $msg ['shipping_method'] = curl_errno($curl);
                        $messages = array('status' => "shipping_error", 'message' => $msg);
                        echo \Zend\Json\Json::encode($messages);

                        if (isset($postArr['source_type']) && $postArr['source_type'] == 'frontend') {
                            die;
                        }
                    }
                    curl_close($curl);
                    $xml = new SimpleXMLElement($result);
                    //asd($xml);
                    $shipItemPrices = $xml->Packages->Package;

                    $j = 0;
                    $applicationConfigIds = $this->_config['application_config_ids'];
                    $uSId = $applicationConfigIds['united_state_country_id'];
                    if ($shipItemPrices->Status->Code == 0 && $carrierId != 'custom') { // commented on 2 july 2014
//if ($shipItemPrices->Status->Code == '9') { // we need to remove this line and uncomment the above line
                        $errorDescription = $shipItemPrices->Status->Description;
                        $errorType = 3;

                        if (strpos($errorDescription, 'country code for Consignee') > 0 || strpos($errorDescription, 'country is not valid') > 0 || strpos($errorDescription, 'country not served') > 0) {
                            $errorType = 1;
                            $errorMessage = $createTransactionMessage['L_INVALID_COUNTRY'];
                        } else if (strpos($errorDescription, 'CONSIGNEE') > 0) {
                            $errorType = 2;
                            $errorMessage = $createTransactionMessage['L_INVALID_POSTAL'];
                        } else {
                            $errorType = 3;
                            $errorMessage = $createTransactionMessage['INVALID_ADDRESS'];
                        }

                        $msg = array();
                        if ($postData['shipping_zip_code'] == '') {
                            $msg ['shipping_zip_code'] = $createTransactionMessage['SHIPPING_ZIP_CODE_EMPTY'];
                        } else if ($countryName == '') {
                            $msg ['shipping_country'] = $createTransactionMessage['SHIPPING_COUNTRY_EMPTY'];
                        } else {
                            if ($errorType == 1) {
                                $msg ['shipping_country'] = $errorMessage;
                            } else if ($errorType == 2) {
                                $msg ['shipping_zip_code'] = $errorMessage;
                            } else {
                                $msg ['shipping_method'] = $errorMessage;
                            }
                        }
                        $messages = array('status' => "shipping_error", 'message' => $msg);
                        echo \Zend\Json\Json::encode($messages);
                        if (isset($postArr['source_type']) && $postArr['source_type'] == 'frontend') {
                            die;
                        }
                    } else {
                        $lbsCheck = 0;
                        $surchangeUnder = 0;
                        $surchangeAbove = 0;
                        $shippingHandlingUnder = 0;
                        $shippingHandlingAbove = 0;
                        if (isset($postData['is_apo_po']) && $postData['is_apo_po'] != '0' && $postData['is_apo_po'] != '1') {
                            $shippingMetArr = array();
                            $shippingMetArr['pb_shipping_id'] = $applicationConfigIds['apo_po_pb_id'];
                            $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                            $getShippingMethods = $this->_transactionTable->getShippingMethod($shippingMethodArr);
                            $lbsCheck = $getShippingMethods[0]['lbs_check'];
                            $surchangeUnder = $getShippingMethods[0]['surchange_under_5'];
                            $surchangeAbove = $getShippingMethods[0]['surchange_above_5'];
                            $shippingHandlingUnder = $getShippingMethods[0]['shipping_handling_under_5'];
                            $shippingHandlingAbove = $getShippingMethods[0]['shipping_handling_above_5'];
                        } else if (isset($postData['shipping_country']) && $postData['shipping_country'] == $uSId && $postData['is_apo_po'] == '1') {
                            $shippingMetArr = array();
                            $shippingMetArr['pb_shipping_id'] = $applicationConfigIds['domestic_pb_id'];
                            $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                            $getShippingMethods = $this->_transactionTable->getShippingMethod($shippingMethodArr);
                            $lbsCheck = $getShippingMethods[0]['lbs_check'];
                            $surchangeUnder = $getShippingMethods[0]['surchange_under_5'];
                            $surchangeAbove = $getShippingMethods[0]['surchange_above_5'];
                            $shippingHandlingUnder = $getShippingMethods[0]['shipping_handling_under_5'];
                            $shippingHandlingAbove = $getShippingMethods[0]['shipping_handling_above_5'];
                        } else {

                            $shippingMetArr = array();
                            $shippingMetArr['pb_shipping_id'] = $applicationConfigIds['internation_pb_id'];
                            $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                            $getShippingMethodsInter = $this->_transactionTable->getShippingMethod($shippingMethodArr);
                            $lbsCheck = $getShippingMethodsInter[0]['lbs_check'];
                            if ($totalProductWeightAmountForShipping <= $lbsCheck) {
                                foreach ($getShippingMethodsInter as $shipMethod) {
                                    if ($shipMethod['pb_shipping_type_id'] == $applicationConfigIds['internation_less_than_lbs_id']) {
                                        $getShippingMethods[] = $shipMethod;

										$lbsCheck = $shipMethod['lbs_check'];
										$surchangeUnder = $shipMethod['surchange_under_5'];
										$surchangeAbove = $shipMethod['surchange_above_5'];
										$shippingHandlingUnder = $shipMethod['shipping_handling_under_5'];
										$shippingHandlingAbove = $shipMethod['shipping_handling_above_5'];
                                    }
                                }
                            } else {
                                foreach ($getShippingMethodsInter as $shipMethod) {
                                    if ($shipMethod['pb_shipping_type_id'] == $applicationConfigIds['internation_greater_than_lbs_id']) {
                                        $getShippingMethods[] = $shipMethod;

										$lbsCheck = $shipMethod['lbs_check'];
										$surchangeUnder = $shipMethod['surchange_under_5'];
										$surchangeAbove = $shipMethod['surchange_above_5'];
										$shippingHandlingUnder = $shipMethod['shipping_handling_under_5'];
										$shippingHandlingAbove = $shipMethod['shipping_handling_above_5'];
                                    }
                                }
                            }
                        }
                    }
                }
            }
//foreach ($shipItemPrices as $itemPrice) {
            $category = new Category($this->_adapter);
            $catArr = array();
            $categoryArr = $category->getCategoriesArr($catArr);
            $allCategories = $this->getServiceLocator()->get('Category\Model\CategoryTable')->getCategories($categoryArr);
            $categoryData = array();
            if (!empty($allCategories)) {
                foreach ($allCategories as $category) {
                    $categoryData[$category['category_id']] = $category['parent_id'];
                }
            }
            $dataArr = array();
            $dataArr['categoryData'] = $categoryData;

            $totalShipping = 0;
            $j = 0;
            $z = 1;

            $productCnt = count($cartProducts);

            $totalProductDiscount = 0;
            $totalMembershipDiscount = 0;
            $totalShippingDiscount = 0;
            $totalShipping = 0;
            $totalTaxAmount = 0;
            $subTotal = 0;
            $subtotalAmount = 0;
            $totalAfterShipping = 0;
            $totalShippingPercentage = 0;
            //

            $orderTotal = 0; // final amount

            $totalShippingCostFromPB = (isset($shipItemPrices->Shipping->TotalCharge)) ? ((string) $shipItemPrices->Shipping->TotalCharge) : 0;

            foreach ($cartProducts as $index => $value) {

                $totalProductDiscount+=$value['product_discount'];
                $totalMembershipDiscount+=$value['membership_discount'];
                $totalShippingDiscount+=$value['shipping_discount_amount'];
                $productShippingPrice = 0;

                if ($value['is_shippable'] == '1' && $value['product_weight'] > 0 && $value['is_free_shipping'] != '1' && $value['shipping_cost'] == '1') {

                    $percentShipping = ($value['product_weight'] * $value['product_qty'] * 100) / $totalProductWeightAmountForShipping;

                    $totalShippingPercentage = $totalShippingPercentage + $percentShipping;

                    //....... to be set above ...../
                    if ($z == count($cartProducts)) {
                        if ($totalShippingPercentage < 100)
                            $percentShipping = $percentShipping + ( 100 - $totalShippingPercentage);
                    }
                    // .................... to set above .........................//
                    $productShippingPrice = ($percentShipping * $totalShippingCostFromPB) / 100;
                    //
                    /* if ($carrierId == 'custom') {
                      $productShippingPrice = ($postArr['postData']['custom_shipping'] * $percentShipping) / 100;
                      $productShippingPrice = $productShippingPrice - $value['shipping_discount_amount'];
                      } */
                    // .......................//

                    $dataArr['category_id'] = $value['category_id'];
                    $isCustomFrame = $this->isCustomFrame($dataArr);

                    if ($isCustomFrame) {

                        $cartProducts[$index]['shipping_price'] = $productShippingPrice;
                        $surchangeTax = ($cartProducts[$index]['product_weight'] <= $getShippingMethods[0]['custom_frames_lbs_check']) ? $getShippingMethods[0]['surchange_under_15'] : $getShippingMethods[0]['surchange_above_15'];
                        $shippingHandlineTax = ($cartProducts[$index]['product_weight'] <= $getShippingMethods[0]['custom_frames_lbs_check']) ? $getShippingMethods[0]['shipping_handling_under_15'] : $getShippingMethods[0]['shipping_handling_above_15'];
                        if ($productShippingPrice == 0)
                            $shippingHandlineTax = 0;
                        $cartProducts[$index]['surcharge_price'] = ((($productShippingPrice) * $surchangeTax) / 100);
                        $cartProducts[$index]['shipping_handling_price'] = $shippingHandlineTax;
                    } else {
                        $cartProducts[$index]['shipping_price'] = $productShippingPrice;
                        $surchangeTax = ($cartProducts[$index]['product_weight'] <= $lbsCheck) ? $surchangeUnder : $surchangeAbove;
                        $shippingHandlineTax = ($cartProducts[$index]['product_weight'] <= $lbsCheck) ? $shippingHandlingUnder : $shippingHandlingAbove;
                        if ($productShippingPrice == 0)
                            $shippingHandlineTax = 0;
                        $cartProducts[$index]['surcharge_price'] = ($productShippingPrice * $surchangeTax / 100);
                        $cartProducts[$index]['shipping_handling_price'] = $shippingHandlineTax;
                    }
                }else {
                    $cartProducts[$index]['shipping_price'] = '0.00';
                    $cartProducts[$index]['surcharge_price'] = '0.00';
                    $cartProducts[$index]['shipping_handling_price'] = '0.00';
                }

                if ($carrierId == 'custom') {

                    $productShippingPrice = ($postArr['postData']['custom_shipping'] * $percentShipping) / 100;
                    $productShippingPrice = $productShippingPrice - $value['shipping_discount_amount'];
                    $totalShipping+=$this->Roundamount($productShippingPrice);
                    $cartProducts[$index]['shipping_price'] = $this->Roundamount($productShippingPrice);
                    $cartProducts[$index]['surcharge_price'] = '0.00';
                    $cartProducts[$index]['shipping_handling_price'] = '0.00';
                } else {
                    if ($productShippingPrice != 0) {
                        //round off shipping before adding into product total
                        $productShippingPrice = $this->Roundamount($productShippingPrice) + $this->Roundamount($cartProducts[$index]['surcharge_price']) + $this->Roundamount($cartProducts[$index]['shipping_handling_price']);

                        $productShippingPrice = $this->Roundamount($productShippingPrice - $value['shipping_discount_amount']);
                        if ($productShippingPrice < 0)
                            $productShippingPrice = '0.00';
                        $totalShipping+=$productShippingPrice;
                    }
                }
                $subtotalAmount = ($value['product_amount'] * $value['product_qty']) - $value['membership_discount'];
                //.... tax ...//

                $product_amount_for_tax = $subtotalAmount - $value['product_discount'] + $productShippingPrice;
                $productTotalForRevenue = $subtotalAmount - $value['product_discount'];
                if ($product_amount_for_tax < 0)
                    $product_amount_for_tax = 0;
                $totalAfterShipping+= $this->Roundamount($product_amount_for_tax);


                if (isset($cartProducts[$index]['is_taxable']) && $cartProducts[$index]['is_taxable'] == 1 && (isset($postData['shipping_country']) && $postData['shipping_country'] == '228') && (((isset($postData['shipping_state']) && $postData['shipping_state'] == 'NY') || (isset($postData['shipping_state_id']) && $postData['shipping_state_id'] == 'NY') || (isset($postData['apo_po_state']) && $postData['apo_po_state'] == 'NY')))) {

                    $product_tax = $this->Roundamount($product_amount_for_tax) * $postArr['combined_sales_tax'];
                    $cartProducts[$index]['product_taxable_amount'] = $this->Roundamount($product_tax);
                } else {
                    $cartProducts[$index]['product_taxable_amount'] = '0.00';
                }
                $totalTaxAmount+=$cartProducts[$index]['product_taxable_amount'];

                //.... end of tax //


                $cartProducts[$index]['product_total'] = $this->Roundamount($product_amount_for_tax) + $cartProducts[$index]['product_taxable_amount'];
                $cartProducts[$index]['total_product_revenue'] = $this->Roundamount($productTotalForRevenue);

                $orderTotal+= $cartProducts[$index]['product_total'];

                $subTotal+= ($value['product_amount'] * $value['product_qty']) - $value['membership_discount'];

                $cartProducts[$index]['product_discount'] = $this->Roundamount($cartProducts[$index]['product_discount']);
                $cartProducts[$index]['shipping_price'] = $this->Roundamount($cartProducts[$index]['shipping_price']);
                $cartProducts[$index]['surcharge_price'] = $this->Roundamount($cartProducts[$index]['surcharge_price']);
                $cartProducts[$index]['shipping_handling_price'] = $this->Roundamount($cartProducts[$index]['shipping_handling_price']);
                $cartProducts[$index]['product_taxable_amount'] = $this->Roundamount($cartProducts[$index]['product_taxable_amount']);
                $cartProducts[$index]['product_total'] = $this->Roundamount($cartProducts[$index]['product_total']);
                $j++;
                $z++;
            }
            //}
            //}
//asd($cartProducts);
            $cartProductsTotalArray['totalTaxAmount'] = $this->Roundamount($totalTaxAmount);
            $cartProductsTotalArray['subTotalAmount'] = $this->Roundamount($subTotal);
            $cartProductsTotalArray['totalaftershipping'] = $this->Roundamount($totalAfterShipping);
            $cartProductsTotalArray['shipping_charge'] = $this->Roundamount($totalShipping);
            $cartProductsTotalArray['total_discount'] = $this->Roundamount($totalProductDiscount);
            $cartProductsTotalArray['total_membership_discount'] = $this->Roundamount($totalMembershipDiscount);
            $cartProductsTotalArray['order_total'] = $this->Roundamount($orderTotal);
            $cartProductsTotalArray['shipping_discount_in_amount'] = $this->Roundamount($totalShippingDiscount);


            $retArray = array();
            $retArray['cartdata'] = $cartProducts;
            $retArray['cartdataTotal'] = $cartProductsTotalArray;
            //asd($retArray);

            return $retArray;
        }
    }

    /**
     * This action is used to check whether coupon is valid or not
     * @param void
     * @return void
     * @author Icreon Tech - DT
     */
    public function applyCouponAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $postArr = $request->getPost()->toArray();
            $applicationConfigIds = $this->_config['application_config_ids'];
            $uSId = $applicationConfigIds['united_state_country_id'];
            $this->getTransactionTable();
            $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
            $commonMsg = $this->_config['transaction_messages']['config']['common_message'];
            // By Dev 2 NEWPROMOCODE
            if ($request->isPost()) {
                if ($request->getPost('user_id') && $request->getPost('user_id') != '') {
                    if ($request->getPost('save_shipping_address') != '' && $request->getPost('status') != 'error') {
                        $paramsArray = $request->getPost()->toArray();
                        if (isset($paramsArray['is_apo_po']) && $paramsArray['is_apo_po'] == '3') {
                            $paramsArray['shipping_state'] = $paramsArray['apo_po_state'];
                            $paramsArray['shipping_country'] = $paramsArray['apo_po_shipping_country'];
                        } else if (isset($paramsArray['shipping_country']) && $paramsArray['shipping_country'] == $uSId) {
                            if ($paramsArray['is_apo_po'] == '2') {
                                $paramsArray['shipping_country'] = $paramsArray['apo_po_shipping_country'];
                            }
                        }
                        $paramsArray['postData'] = $paramsArray;
                    }
                    $paramsArray['userid'] = $request->getPost('user_id');
                    $paramsArray['shipcode'] = $request->getPost('shipping_zip_code');
                    $paramsArray['couponcode'] = trim($request->getPost('coupon_code'));
                    $paramsArray['source_type'] = 'backend';
                    //asd($paramsArray,2);
                    $searchResult = $this->cartTotalWithTax($paramsArray);
                    //asd($searchResult);
                    if ($searchResult['status'] == 'error') {
                        $msg = array();
                        $msg ['coupon_code'] = $searchResult['message'];
                    } else {
                        $msg = $searchResult['message'];
                    }
                    $messages = array('status' => $searchResult['status'], 'message' => $msg);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                }
            }
        } else {
            return $this->redirect()->toRoute('crm-transaction');
        }
    }

    // By Dev 2 used to validate the coupon code NEWPROMOCODE
    public function isValidCouponCheck($selectedShipping_method, $userId, $shipping_country = '0', $getPromotionDetail, $cartData) {
        $returnData = array();
        $applicationConfigIds = $this->_config['application_config_ids'];
        $uSId = $applicationConfigIds['united_state_country_id'];

        $serviceManager = $this->getServiceLocator();
        $promotionTable = $serviceManager->get('Promotions\Model\PromotionTable');

        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        if (count($getPromotionDetail) > 0) {
            $promotionUsedByUserRecord = array();

            /** Expiry date check * */
            if (($getPromotionDetail['expiry_date'] != '' && $getPromotionDetail['expiry_date'] != 0 && substr(DATE_TIME_FORMAT, 0, 10) > substr($getPromotionDetail['expiry_date'], 0, 10)) || ($getPromotionDetail['start_date'] != '' && $getPromotionDetail['start_date'] != 0 && substr(DATE_TIME_FORMAT, 0, 10) < substr($getPromotionDetail['start_date'], 0, 10))) {
                $returnData['status'] = "error";
                $returnData['message'] = $msgArr['COUPON_CODE_EXPIRED'];
                return $returnData;
            }
            /** end of expiry chaek * */
            /** Usage Limit Check * */
            if ($getPromotionDetail['usage_limit_type'] == 2 || $getPromotionDetail['usage_limit_type'] == 3) { // One Time Per Contact Limit Condition
                $promoUsedSearchParam = array();
                $promoUsedSearchParam['userId'] = $userId;
                $promoUsedSearchParam['promotionId'] = $getPromotionDetail['promotion_id'];
                $promotionUsedDetail = $promotionTable->checkUserUsedCoupon($promoUsedSearchParam); // used to check that coupon is used by user or not
            }

            if (($getPromotionDetail['usage_limit_type'] == 1) || ($getPromotionDetail['usage_limit_type'] == 2 && $promotionUsedDetail[0]['userUsedCount'] == 0) || (($getPromotionDetail['usage_limit_type'] == 3 && $getPromotionDetail['usage_limit_per_contact'] != 1) && ($getPromotionDetail['usage_limit'] > 0 && $getPromotionDetail['usage_limit'] > $promotionUsedDetail[0]['promotionUsedCount']) ) || (($getPromotionDetail['usage_limit_type'] == 3 && $getPromotionDetail['usage_limit_per_contact'] == 1) && ($getPromotionDetail['usage_limit'] > 0 && $promotionUsedDetail[0]['userUsedCount'] == 0) && ($getPromotionDetail['usage_limit'] > $promotionUsedDetail[0]['promotionUsedCount']))) {

                if ($getPromotionDetail['is_shipping_discount'] == 1 && $getPromotionDetail['shipping_discount_type'] == 1) { // shipping_discount
                    if (!empty($getPromotionDetail['applicable_to'])) { // domestic/international
                        $invalidFl = 0;
                        if ($getPromotionDetail['applicable_to'] == 1 && $shipping_country != '228') { // Domestic
                            $invalidFl = 1;
                        } else if ($getPromotionDetail['applicable_to'] == 2 && $shipping_country == '228') { // International
                            $invalidFl = 1;
                        } else if ($getPromotionDetail['applicable_to'] == 3) {
                            $invalidFl = 0;
                        }



                        if ($invalidFl == 1) {
                            $returnData['status'] = "error";
                            $returnData['message'] = $msgArr['COUPON_NOT_VALID'];
                            return $returnData;
                        }
                    }
                }

                if ($getPromotionDetail['applicable_id'] == 2) { // Applied for check in contact check
                    $promotionUserSearchParam = array();
                    $promotionUserSearchParam['promotion_id'] = $getPromotionDetail['promotion_id'];
                    $promotionUserSearchParam['user_id'] = $userId;
                    $promotionUserSearchParam['applicable_id'] = 2; // 2=for user, 3=Group

                    $promotionUsersRecords = $promotionTable->getUsersInPromotion($promotionUserSearchParam);
                } else if ($getPromotionDetail['applicable_id'] == 3) { // Applied for check in group check
                    $promotionUserSearchParam = array();
                    $promotionUserSearchParam['promotion_id'] = $getPromotionDetail['promotion_id'];
                    $promotionUserSearchParam['user_id'] = $userId;
                    $promotionUserSearchParam['applicable_id'] = 3; // 2=for user, 3=Group

                    $promotionUsersRecords = $promotionTable->getUsersInPromotion($promotionUserSearchParam);

                    if (empty($promotionUsersRecords) || ($promotionUsersRecords[0]['totalApplied'] == 0)) {
                        $appliedSearchParam = array();
                        $availableUsers = array();
                        $appliedSearchParam[] = $getPromotionDetail['promotion_id'];
                        $appliedSearchParam[] = 'applied';
                        $appliedSearchParam[] = 'Group';
                        $appliedData = $promotionTable->getProductPromotionAppliedByPromotionId($appliedSearchParam);

                        foreach ($appliedData as $appliedRecord) {
                            if ($appliedRecord['group_type'] == 2) {

                                parse_str($appliedRecord['search_query'], $searchParam);

                                $searchParam['transaction_amount_date_from'] = (isset($searchParam['transaction_amount_date_from']) and trim($searchParam['transaction_amount_date_from']) != "") ? $this->DateFormat($searchParam['transaction_amount_date_from'], 'db_date_format_from') : "";

                                $searchParam['transaction_amount_date_to'] = (isset($searchParam['transaction_amount_date_to']) and trim($searchParam['transaction_amount_date_to']) != "") ? $this->DateFormat($searchParam['transaction_amount_date_to'], 'db_date_format_to') : "";

                                $searchParam['userId_exclude'] = '';
                                $excludedContact = $this->getServiceLocator()->get('Group\Model\GroupTable')->getExcludedContactInGroup(array('groupId' => $appliedRecord['applied_id']));
                                if (!empty($excludedContact)) {
                                    $userIds = array();
                                    foreach ($excludedContact as $val) {
                                        $userIds[] = $val['user_id'];
                                    }
                                    $searchParam['userId_exclude'] = implode(",", $userIds);
                                }

                                $searchParam['startIndex'] = '';
                                $searchParam['recordLimit'] = '';
                                $searchParam['sortField'] = '';
                                $searchParam['sortOrder'] = '';



                                $group = new Group($this->_adapter);

                                $groupContactSearchArray = $group->getArrayForSearchCotactsGroup($searchParam);

                                $groupContactsData = $this->getServiceLocator()->get('Group\Model\GroupTable')->getContactSearch($groupContactSearchArray);

                                foreach ($groupContactsData as $k => $grpUsers) {
                                    $users = (array) $grpUsers;
                                    if ($users['user_id'] == $userId) {
                                        $promotionUsersRecords[0]['totalApplied'] = 1;
                                        break;
                                    }else
                                        continue;
                                }
                            }
                        }
                    }
                }


                if (($getPromotionDetail['applicable_id'] == 2 || $getPromotionDetail['applicable_id'] == 3) && $promotionUsersRecords[0]['totalApplied'] == 0) {
                    $returnData['status'] = "error";
                    $returnData['message'] = $msgArr['COUPON_NOT_VALID'];
                    return $returnData;
                }
                $promoQualifyCondition = $this->promoQualifyCondition($getPromotionDetail, $cartData);
                $isValidPromotion = $promoQualifyCondition['isValidPromotion'];
                unset($promoQualifyCondition['isValidPromotion']);

                if ($isValidPromotion == 1) { // checking for qualify filter parameters NEWPROMOCODE
                    $returnData['status'] = "success";
                    $returnData['message'] = $msgArr['COUPON_CODE_APPLIED'];
                    $returnData['promotionData'] = $getPromotionDetail;
                    $returnData['totalProductCartAmount'] = $promoQualifyCondition['totalProductCartAmount'];
                    return $returnData;
                } else {
                    $returnData['status'] = "error";
                    $returnData['message'] = $msgArr['COUPON_NOT_VALID'];
                    return $returnData;
                }
            } else {
                $returnData['status'] = "error";
                $returnData['message'] = $msgArr['COUPON_NOT_VALID'];
                return $returnData;
            }
            // End of Usage Limit check
        } else {
            $returnData['status'] = "error";
            $returnData['message'] = 'This promo code does not exists.';
            return $returnData;
        }
    }

    // dev 2 used checking for qualify filter parameters NEWPROMOCODE
    function promoQualifyCondition($getPromotionDetail, $cartDataRecord) {
        $bundleProductTypeArray = array();
        $bundleProductArray = array();
        $anyProductTypeArray = array();
        $anyProductArray = array();
        $excludeProductTypeArray = array();
        $excludeProductArray = array();
        $promoQualifiedArray = array();
        if (!empty($getPromotionDetail['bundle_product_type'])) { // Buldle products
            $bundleProductTypeArray = explode(',', $getPromotionDetail['bundle_product_type']);
        }
        if (!empty($getPromotionDetail['bundle_products'])) {
            $bundleProductArray = explode(',', $getPromotionDetail['bundle_products']);
        }
        if (!empty($getPromotionDetail['promotion_any_product_type'])) { // Any product
            $anyProductTypeArray = explode(',', $getPromotionDetail['promotion_any_product_type']);
        }
        if (!empty($getPromotionDetail['promotion_any_products'])) {
            $anyProductArray = explode(',', $getPromotionDetail['promotion_any_products']);
        }

        if (!empty($getPromotionDetail['excluded_product_type'])) { // Exclude product
            $excludeProductTypeArray = explode(',', $getPromotionDetail['excluded_product_type']);
        }
        if (!empty($getPromotionDetail['excluded_products'])) {
            $excludeProductArray = explode(',', $getPromotionDetail['excluded_products']);
        }

        if (!empty($getPromotionDetail['minimum_amount_product_type'])) { // Minimum amount products
            $minimumAmountProductTypeArray = explode(',', $getPromotionDetail['minimum_amount_product_type']);
        }
        if (!empty($getPromotionDetail['minimum_amount_products'])) {
            $minimumAmountProductArray = explode(',', $getPromotionDetail['minimum_amount_products']);
        }

        if (!empty($getPromotionDetail['free_products']) && $getPromotionDetail['is_free_product'] == 1) {
            $freeProductsArray = explode(',', $getPromotionDetail['free_products']);
        }

        $arrCartProductId = array();
        foreach ($cartDataRecord as $cartData) {
            $arrCartProductId[] = $cartData['product_id'];
        }
        $arrCartProductId = array_unique($arrCartProductId);

        $totalCartAmount = 0; // total cart amount with membership discount
        $totalProductCartAmount = 0;
        $isValid = 0;
        $totalMinimumProductCartAmount = 0;

        foreach ($cartDataRecord as $cartData) {
            if ($cartData['is_promotional'] == 1) {
                if (!in_array($cartData['product_type_id'], $excludeProductTypeArray) && !in_array($cartData['product_id'], $excludeProductArray)) { // exclude condition
                    if ($getPromotionDetail['promotion_qualified_by'] == 2) { // All bundle products conditions
                        sort($arrCartProductId);
                        sort($getPromotionDetail['promotionbundledbroducts']);
                        $arrComanProduct = array_intersect($arrCartProductId, $getPromotionDetail['promotionbundledbroducts']);
                        sort($arrComanProduct);
                        if ($arrComanProduct == $getPromotionDetail['promotionbundledbroducts']) {
                            $isValid = 1;
							if (in_array($cartData['product_id'], $getPromotionDetail['promotionbundledbroducts'])) {
                                $totalProductCartAmount+= $cartData['product_subtotal'] - $cartData['membership_discount'];
                            }
                            if (in_array($cartData['product_id'], $freeProductsArray)) {
                                $totalProductCartAmount-= ($cartData['product_amount'] - $cartData['membership_indi_discount']);
                            }
                        }
                    } else if ($getPromotionDetail['promotion_qualified_by'] == 3) { // Any Product
                        if (in_array($cartData['product_type_id'], $anyProductTypeArray) || in_array($cartData['product_id'], $anyProductArray)) {
							if (in_array($cartData['product_id'], $getPromotionDetail['promotionAnyProducts'])) {
                                $totalProductCartAmount+= $cartData['product_subtotal'] - $cartData['membership_discount'];
                            }
                            $isValid = 1;
                            if (in_array($cartData['product_id'], $freeProductsArray)) {
                                $totalProductCartAmount-= ($cartData['product_amount'] - $cartData['membership_indi_discount']);
                            }
                        }
                    } else if ($getPromotionDetail['promotion_qualified_by'] == 4) { // Global coupon
                        $isValid = 1;
                        $totalProductCartAmount+= $cartData['product_subtotal'] - $cartData['membership_discount'];
                        if (in_array($cartData['product_id'], $freeProductsArray)) {
                            $totalProductCartAmount-= ($cartData['product_amount'] - $cartData['membership_indi_discount']);
                        }
                    } else if ($getPromotionDetail['promotion_qualified_by'] == 1) { // Min amount
                        if (!empty($getPromotionDetail['promotionMinimumAmountProducts'])) {
                            if ((in_array($cartData['product_type_id'], $minimumAmountProductTypeArray) || in_array($cartData['product_id'], $minimumAmountProductArray)) && in_array($cartData['product_id'], $getPromotionDetail['promotionMinimumAmountProducts'])) {
                                $totalProductCartAmount+= ($cartData['product_amount'] - $cartData['membership_indi_discount']);
                                $isValid = 1;
                            }
                            if (in_array($cartData['product_id'], $freeProductsArray)) {
                                $totalProductCartAmount-= ($cartData['product_amount'] - $cartData['membership_indi_discount']);
                            }
                        } else {
                            $isValid = 1;
                        }
                    }
                }
                /*$totalProductCartAmount+= $cartData['product_subtotal'] - $cartData['membership_discount'];
                if ($isValid == 1) {
                    if (in_array($cartData['product_id'], $freeProductsArray)) {
                        $totalProductCartAmount-= ($cartData['product_amount'] - $cartData['membership_indi_discount']);
                    }
                }*/
            }
        }



        if ($getPromotionDetail['promotion_qualified_by'] == 1) { // Min amount conditions
            if ($isValid == 1 && $totalProductCartAmount >= $getPromotionDetail['promotion_qualified_min_amount']) {
                $isValid = 1;
            } else {
                $isValid = 0;
            }
        }

        $promoQualifiedArray['totalProductCartAmount'] = $totalProductCartAmount;
        $promoQualifiedArray['isValidPromotion'] = $isValid;
        return $promoQualifiedArray;
    }

    // By Dev 2 used toapply the coupon code NEWPROMOCODE
    public function applyCouponDiscountInCart($getPromotionDetail, $cartData, $shipping_country, $totalCartAmount, $totalProductCartAmount, $totalShippableProductWeight) {
        $returnData = array();
        $applicationConfigIds = $this->_config['application_config_ids'];
        $uSId = $applicationConfigIds['united_state_country_id'];
        $serviceManager = $this->getServiceLocator();
        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $bundleProductTypeArray = array();
        $bundleProductArray = array();
        $anyProductTypeArray = array();
        $anyProductArray = array();
        $excludeProductTypeArray = array();
        $excludeProductArray = array();

        if (!empty($getPromotionDetail['bundle_product_type'])) { // Buldle products
            $bundleProductTypeArray = explode(',', $getPromotionDetail['bundle_product_type']);
        }
        if (!empty($getPromotionDetail['bundle_products'])) {
            $bundleProductArray = explode(',', $getPromotionDetail['bundle_products']);
        }

        if (!empty($getPromotionDetail['promotion_any_product_type'])) { // Any product
            $anyProductTypeArray = explode(',', $getPromotionDetail['promotion_any_product_type']);
        }
        if (!empty($getPromotionDetail['promotion_any_products'])) {
            $anyProductArray = explode(',', $getPromotionDetail['promotion_any_products']);
        }

        if (!empty($getPromotionDetail['excluded_product_type'])) { // Exclude product
            $excludeProductTypeArray = explode(',', $getPromotionDetail['excluded_product_type']);
        }
        if (!empty($getPromotionDetail['excluded_products'])) {
            $excludeProductArray = explode(',', $getPromotionDetail['excluded_products']);
        }

        if (!empty($getPromotionDetail['minimum_amount_product_type'])) { // Minimum amount products
            $minimumAmountProductTypeArray = explode(',', $getPromotionDetail['minimum_amount_product_type']);
        }
        if (!empty($getPromotionDetail['minimum_amount_products'])) {
            $minimumAmountProductArray = explode(',', $getPromotionDetail['minimum_amount_products']);
        }

        $arrCartProductId = array();
        $totalBundleProductAmount = 0;
        $totalMinimumAmountProductsAmount = 0;
        foreach ($cartData as $cData) {
            $arrCartProductId[] = $cData['product_id'];
            if (in_array($cData['product_id'], $getPromotionDetail['promotionbundledbroducts']))
                $totalBundleProductAmount+=$cData['product_subtotal'] - $cData['membership_discount'];

            if ($getPromotionDetail['promotion_qualified_by'] == 1) {
                if (!empty($getPromotionDetail['promotionMinimumAmountProducts']) && in_array($cData['product_id'], $getPromotionDetail['promotionMinimumAmountProducts'])) {
                    if ((in_array($cData['product_type_id'], $minimumAmountProductTypeArray)) || (in_array($cData['product_id'], $minimumAmountProductArray)))
                        $totalMinimumAmountProductsAmount+=$cData['product_subtotal'] - $cData['membership_discount'];
                }
            }elseif ($getPromotionDetail['promotion_qualified_by'] == 3) {
                if (!empty($getPromotionDetail['promotionAnyProducts'])) {
                    if ((in_array($cData['product_type_id'], $anyProductTypeArray)) || (in_array($cData['product_id'], $anyProductArray)))
                        $totalAnyProductsAmount+=$cData['product_subtotal'] - $cData['membership_discount'];
                }
            }
        }

        $arrCartProductId = array_unique($arrCartProductId);

        $z = 0;
        $totalProductTax = 0;
        $totalMembershipDiscount = 0;

        foreach ($cartData as $index => $item) {
            if ($item['is_promotional'] == 1 && (!in_array($item['product_type_id'], $excludeProductTypeArray)) && (!in_array($item['product_id'], $excludeProductArray))) {
                if ($getPromotionDetail['promotion_qualified_by'] == 4) {
                    // global
                    // Shipping Discount
                    if ($getPromotionDetail['is_shipping_discount'] == 1) {

                        if ($getPromotionDetail['shipping_discount_type'] == 1) {
                            $totalShippableProductWeight-= ($item['product_weight'] * $item['product_qty']);
                        }
                    }
                } elseif ($getPromotionDetail['promotion_qualified_by'] == 1) {
                    // min product cart amount

                    if (!empty($getPromotionDetail['promotionMinimumAmountProducts'])) {

                        if (in_array($item['product_type_id'], $minimumAmountProductTypeArray) || in_array($item['product_id'], $minimumAmountProductArray)) {

                            // Shipping Discount
                            if ($getPromotionDetail['is_shipping_discount'] == 1) {

                                if ($getPromotionDetail['shipping_discount_type'] == 1) {
                                    $totalShippableProductWeight-= ($item['product_weight'] * $item['product_qty']);
                                }
                            }
                            // end of shippin discount
                        }
                    } else {
                        if ($totalProductCartAmount >= $getPromotionDetail['promotion_qualified_min_amount']) {
                            // Shipping Discount
                            if ($getPromotionDetail['is_shipping_discount'] == 1) {

                                if ($getPromotionDetail['shipping_discount_type'] == 1) {
                                    $totalShippableProductWeight-= ($item['product_weight'] * $item['product_qty']);
                                }
                            }
                            // end of shippin discount
                        }
                    }
                } elseif ($getPromotionDetail['promotion_qualified_by'] == 2) {
                    // all bundled products
                    sort($arrCartProductId);
                    sort($getPromotionDetail['promotionbundledbroducts']);
                    $arrComanProduct = array_intersect($arrCartProductId, $getPromotionDetail['promotionbundledbroducts']);
                    sort($arrComanProduct);
                    if ($arrComanProduct == $getPromotionDetail['promotionbundledbroducts']) {
                        // Shipping Discount
                        if ($getPromotionDetail['is_shipping_discount'] == 1 && in_array($item['product_id'], $getPromotionDetail['promotionbundledbroducts'])) {

                            if ($getPromotionDetail['shipping_discount_type'] == 1) {
                                $totalShippableProductWeight-= ($item['product_weight'] * $item['product_qty']);
                            }
                        }
                        // end of shippin discount
                    }
                } elseif ($getPromotionDetail['promotion_qualified_by'] == 3) {
                    //any product in list
                    if ((in_array($item['product_type_id'], $anyProductTypeArray) || in_array($item['product_id'], $anyProductArray)) && in_array($item['product_id'], $getPromotionDetail['promotionAnyProducts'])) {
                        // Shipping Discount
                        if ($getPromotionDetail['is_shipping_discount'] == 1) {

                            if ($getPromotionDetail['shipping_discount_type'] == 1) {
                                $totalShippableProductWeight-= ($item['product_weight'] * $item['product_qty']);
                            }
                        }
                        // end of shippin discount
                    }
                }
				// @$itemToDiscount: array of free promotional products added to cart
				if ($getPromotionDetail['is_free_product'] == 1) {
                        $freeProductsArray = explode(',', $getPromotionDetail['free_products']);
						if (in_array($item['product_id'], $freeProductsArray)) {							
								$itemToDiscount[$item['product_id']]['itemQty'][] = $item['product_qty'];
								$itemToDiscount[$item['product_id']]['itemTotalAmount'][] = $item['product_subtotal'] - $item['membership_discount'];
						}
				}
            }
        }
		$freeDiscountedItems = array();
        foreach ($cartData as $key => $val) {
            if ($val['is_promotional'] == 1 && (!in_array($val['product_type_id'], $excludeProductTypeArray)) && (!in_array($val['product_id'], $excludeProductArray))) {
                if ($getPromotionDetail['promotion_qualified_by'] == 4) {
                    // global
                    // Shipping Discount
                    if ($getPromotionDetail['is_shipping_discount'] == 1) {

                        if ($getPromotionDetail['shipping_discount_type'] == 1) {
                            $cartData[$key]['is_free_shipping'] = 1;
                            $cartData[$key]['shipping_discount_amount'] = 0;
                        } elseif ($getPromotionDetail['shipping_discount_type'] == 2) {
                            if ($val['is_free_shipping'] == 0 && $val['is_shippable'] == 1 && $val['shipping_cost'] == 1) {
                                $shipping_discount_amount = $this->calculateProductShippingDiscount(($val['product_weight'] * $val['product_qty']), $totalShippableProductWeight, $getPromotionDetail['free_shipping_amount']);
                                $cartData[$key]['is_free_shipping'] = 0;
                                $cartData[$key]['shipping_discount_amount'] = $shipping_discount_amount;
                            }
                        }
                    }
                    // end of shippin discount
                    // cart discount
                    if ($getPromotionDetail['is_cart_discount'] == 1) {
                        if ($getPromotionDetail['cart_discount_type'] == 1) { // percent
                            $cartDiscountAmountOff = ($totalCartAmount * $getPromotionDetail['cart_discount_percent']) / 100;
                        } elseif ($getPromotionDetail['cart_discount_type'] == 2) { // amount off
                            $cartDiscountAmountOff = $getPromotionDetail['cart_discount_amount'];
                        }

                        $productDiscount = $this->calculateProductCartDiscount($val['product_subtotal'] - $val['membership_discount'], $totalProductCartAmount, $cartDiscountAmountOff);

                        if($productDiscount>0){
							if($productDiscount<($val['product_subtotal'] - $val['membership_discount']))
								$cartData[$key]['product_discount'] = $productDiscount;
							else
								$cartData[$key]['product_discount'] = $val['product_subtotal'] - $val['membership_discount'];
						}else
								$cartData[$key]['product_discount'] = '0.00';
                    }
                    // end of cart discount
                    // free products
                    if ($getPromotionDetail['is_free_product'] == 1) {
                        $freeProductsArray = explode(',', $getPromotionDetail['free_products']);
                        if (in_array($val['product_id'], $freeProductsArray) && !in_array($val['product_id'],$freeDiscountedItems)) {
                            $totalItemQty = array_sum($itemToDiscount[$val['product_id']]['itemQty']);
							$totalItemAmount = array_sum($itemToDiscount[$val['product_id']]['itemTotalAmount']);
                            $productDiscount = $this->calculateFreeProductDiscount($val, $itemToDiscount);
                            if($productDiscount>0){
								if($productDiscount<($val['product_subtotal'] - $val['membership_discount'])){
									$cartData[$key]['product_discount'] = $productDiscount;								
								}else
									$cartData[$key]['product_discount'] = $val['product_subtotal'] - $val['membership_discount'];
								$freeDiscountedItems[] = $val['product_id'];
							}else
								$cartData[$key]['product_discount'] = '0.00';
							$cartData[$key]['is_taxable'] = 0;
                        }
                    }
                    // end of free products discount
                } elseif ($getPromotionDetail['promotion_qualified_by'] == 1) {
                    // min product cart amount

                    if (!empty($getPromotionDetail['promotionMinimumAmountProducts'])) {

                        sort($arrCartProductId);
                        sort($getPromotionDetail['promotionMinimumAmountProducts']);
                        $arrComanProduct = array_intersect($arrCartProductId, $getPromotionDetail['promotionMinimumAmountProducts']);
                        sort($arrComanProduct);

                        if ($totalProductCartAmount >= $getPromotionDetail['promotion_qualified_min_amount']) {
                            // Shipping Discount
                            if ($getPromotionDetail['is_shipping_discount'] == 1 && ((in_array($val['product_type_id'], $minimumAmountProductTypeArray)) || (in_array($val['product_id'], $minimumAmountProductArray)))) {

                                if ($getPromotionDetail['shipping_discount_type'] == 1) {
                                    $cartData[$key]['is_free_shipping'] = 1;
                                    $cartData[$key]['shipping_discount_amount'] = 0;
                                } elseif ($getPromotionDetail['shipping_discount_type'] == 2) {
                                    if ($val['is_free_shipping'] == 0 && $val['is_shippable'] == 1 && $val['shipping_cost'] == 1) {
                                        $shipping_discount_amount = $this->calculateProductShippingDiscount(($val['product_weight'] * $val['product_qty']), $totalShippableProductWeight, $getPromotionDetail['free_shipping_amount']);
                                        $cartData[$key]['is_free_shipping'] = 0;
                                        $cartData[$key]['shipping_discount_amount'] = $shipping_discount_amount;
                                    }
                                }
                            }
                            // end of shippin discount
                            // cart discount

                            if ($getPromotionDetail['is_cart_discount'] == 1 && ((in_array($val['product_type_id'], $minimumAmountProductTypeArray)) || (in_array($val['product_id'], $minimumAmountProductArray)))) {
                                if ($getPromotionDetail['cart_discount_type'] == 1) { // percent
                                    $cartDiscountAmountOff = ($totalMinimumAmountProductsAmount * $getPromotionDetail['cart_discount_percent']) / 100;
                                } elseif ($getPromotionDetail['cart_discount_type'] == 2) { // amount off
                                    $cartDiscountAmountOff = $getPromotionDetail['cart_discount_amount'];
                                }

                                $productDiscount = $this->calculateProductCartDiscount($val['product_subtotal'] - $val['membership_discount'], $totalProductCartAmount, $cartDiscountAmountOff);

                                if($productDiscount>0){
									if($productDiscount<($val['product_subtotal'] - $val['membership_discount']))
										$cartData[$key]['product_discount'] = $productDiscount;
									else
										$cartData[$key]['product_discount'] = $val['product_subtotal'] - $val['membership_discount'];
								}else
										$cartData[$key]['product_discount'] = '0.00';
                            }
                            // end of cart discount
                        }
						// free products
						if ($getPromotionDetail['is_free_product'] == 1 && ((in_array($val['product_type_id'], $minimumAmountProductTypeArray)) || (in_array($val['product_id'], $minimumAmountProductArray)))) {
							$freeProductsArray = explode(',', $getPromotionDetail['free_products']);
							if (in_array($val['product_id'], $freeProductsArray) && !in_array($val['product_id'],$freeDiscountedItems)) {
								$totalItemQty = array_sum($itemToDiscount[$val['product_id']]['itemQty']);
								$totalItemAmount = array_sum($itemToDiscount[$val['product_id']]['itemTotalAmount']);
								$productDiscount = $this->calculateFreeProductDiscount($val, $itemToDiscount);
                                if($productDiscount>0){
									if($productDiscount<($val['product_subtotal'] - $val['membership_discount'])){
										$cartData[$key]['product_discount'] = $productDiscount;								
									}else
										$cartData[$key]['product_discount'] = $val['product_subtotal'] - $val['membership_discount'];
									$freeDiscountedItems[] = $val['product_id'];
								}else
									$cartData[$key]['product_discount'] = '0.00';
								$cartData[$key]['is_taxable'] = 0;
							}
						}
						// end of free products discount
                    } else {
                        if ($totalProductCartAmount >= $getPromotionDetail['promotion_qualified_min_amount']) {
                            // Shipping Discount
                            if ($getPromotionDetail['is_shipping_discount'] == 1) {

                                if ($getPromotionDetail['shipping_discount_type'] == 1) {
                                    $cartData[$key]['is_free_shipping'] = 1;
                                    $cartData[$key]['shipping_discount_amount'] = 0;
                                } elseif ($getPromotionDetail['shipping_discount_type'] == 2) {
                                    if ($val['is_free_shipping'] == 0 && $val['is_shippable'] == 1 && $val['shipping_cost'] == 1) {
                                        $shipping_discount_amount = $this->calculateProductShippingDiscount(($val['product_weight'] * $val['product_qty']), $totalShippableProductWeight, $getPromotionDetail['free_shipping_amount']);
                                        $cartData[$key]['is_free_shipping'] = 0;
                                        $cartData[$key]['shipping_discount_amount'] = $shipping_discount_amount;
                                    }
                                }
                            }
                            // end of shippin discount
                            // cart discount
                            if ($getPromotionDetail['is_cart_discount'] == 1) {
                                if ($getPromotionDetail['cart_discount_type'] == 1) { // percent
                                    $cartDiscountAmountOff = ($totalCartAmount * $getPromotionDetail['cart_discount_percent']) / 100;
                                } elseif ($getPromotionDetail['cart_discount_type'] == 2) { // amount off
                                    $cartDiscountAmountOff = $getPromotionDetail['cart_discount_amount'];
                                }


                                $productDiscount = $this->calculateProductCartDiscount($val['product_subtotal'] - $val['membership_discount'], $totalProductCartAmount, $cartDiscountAmountOff);

                                if($productDiscount>0){
									if($productDiscount<($val['product_subtotal'] - $val['membership_discount']))
										$cartData[$key]['product_discount'] = $productDiscount;
									else
										$cartData[$key]['product_discount'] = $val['product_subtotal'] - $val['membership_discount'];
								}else
										$cartData[$key]['product_discount'] = '0.00';
                            }
                            // end of cart discount
                        }
						// free products
						if ($getPromotionDetail['is_free_product'] == 1) {
							$freeProductsArray = explode(',', $getPromotionDetail['free_products']);
							if (in_array($val['product_id'], $freeProductsArray) && !in_array($val['product_id'],$freeDiscountedItems)) {
								$totalItemQty = array_sum($itemToDiscount[$val['product_id']]['itemQty']);
								$totalItemAmount = array_sum($itemToDiscount[$val['product_id']]['itemTotalAmount']);
								$productDiscount = $this->calculateFreeProductDiscount($val, $itemToDiscount);
                                if($productDiscount>0){
									if($productDiscount<($val['product_subtotal'] - $val['membership_discount'])){
										$cartData[$key]['product_discount'] = $productDiscount;								
									}else
										$cartData[$key]['product_discount'] = $val['product_subtotal'] - $val['membership_discount'];
									$freeDiscountedItems[] = $val['product_id'];
								}else
									$cartData[$key]['product_discount'] = '0.00';
								$cartData[$key]['is_taxable'] = 0;
							}
						}
						// end of free products discount
                    }
                } elseif ($getPromotionDetail['promotion_qualified_by'] == 2) {
                    // all bundled products
                    sort($arrCartProductId);
                    sort($getPromotionDetail['promotionbundledbroducts']);
                    $arrComanProduct = array_intersect($arrCartProductId, $getPromotionDetail['promotionbundledbroducts']);
                    sort($arrComanProduct);
                    if ($arrComanProduct == $getPromotionDetail['promotionbundledbroducts']) {
                        // Shipping Discount
                        if ($getPromotionDetail['is_shipping_discount'] == 1 && in_array($val['product_id'], $getPromotionDetail['promotionbundledbroducts'])) {

                            if ($getPromotionDetail['shipping_discount_type'] == 1) {
                                $cartData[$key]['is_free_shipping'] = 1;
                                $cartData[$key]['shipping_discount_amount'] = 0;
                            } elseif ($getPromotionDetail['shipping_discount_type'] == 2) {
                                if ($val['is_free_shipping'] == 0 && $val['is_shippable'] == 1 && $val['shipping_cost'] == 1) {
                                    $shipping_discount_amount = $this->calculateProductShippingDiscount(($val['product_weight'] * $val['product_qty']), $totalShippableProductWeight, $getPromotionDetail['free_shipping_amount']);
                                    $cartData[$key]['is_free_shipping'] = 0;
                                    $cartData[$key]['shipping_discount_amount'] = $shipping_discount_amount;
                                }
                            }
                        }
                        // end of shippin discount
                        // cart discount

                        if ($getPromotionDetail['is_cart_discount'] == 1 && in_array($val['product_id'], $getPromotionDetail['promotionbundledbroducts'])) {
                            if ($getPromotionDetail['cart_discount_type'] == 1) { // percent
                                $cartDiscountAmountOff = ($totalProductCartAmount * $getPromotionDetail['cart_discount_percent']) / 100;
                            } elseif ($getPromotionDetail['cart_discount_type'] == 2) { // amount off
                                $cartDiscountAmountOff = $getPromotionDetail['cart_discount_amount'];
                            }

                            $productDiscount = $this->calculateProductCartDiscount($val['product_subtotal'] - $val['membership_discount'], $totalProductCartAmount, $cartDiscountAmountOff);

                            if($productDiscount>0){
								if($productDiscount<($val['product_subtotal'] - $val['membership_discount']))
									$cartData[$key]['product_discount'] = $productDiscount;
								else
									$cartData[$key]['product_discount'] = $val['product_subtotal'] - $val['membership_discount'];
							}else
									$cartData[$key]['product_discount'] = '0.00';
                        }
                        // end of cart discount
                    }					
					// free products
					if ($getPromotionDetail['is_free_product'] == 1 && in_array($val['product_id'], $getPromotionDetail['promotionbundledbroducts'])) {
						$freeProductsArray = explode(',', $getPromotionDetail['free_products']);
						if (in_array($val['product_id'], $freeProductsArray) && !in_array($val['product_id'],$freeDiscountedItems)) {
								$totalItemQty = array_sum($itemToDiscount[$val['product_id']]['itemQty']);
								$totalItemAmount = array_sum($itemToDiscount[$val['product_id']]['itemTotalAmount']);
								$productDiscount = $this->calculateFreeProductDiscount($val, $itemToDiscount);
                                if($productDiscount>0){
									if($productDiscount<($val['product_subtotal'] - $val['membership_discount'])){
										$cartData[$key]['product_discount'] = $productDiscount;								
									}else
										$cartData[$key]['product_discount'] = $val['product_subtotal'] - $val['membership_discount'];
									$freeDiscountedItems[] = $val['product_id'];
								}else
									$cartData[$key]['product_discount'] = '0.00';
								$cartData[$key]['is_taxable'] = 0;
						}
					}
					// end of free products discount
                } elseif ($getPromotionDetail['promotion_qualified_by'] == 3) {
                    //any product in list
                    if ((in_array($val['product_type_id'], $anyProductTypeArray) || in_array($val['product_id'], $anyProductArray)) && in_array($val['product_id'], $getPromotionDetail['promotionAnyProducts'])) {
                        // Shipping Discount
                        if ($getPromotionDetail['is_shipping_discount'] == 1) {

                            if ($getPromotionDetail['shipping_discount_type'] == 1) {
                                $cartData[$key]['is_free_shipping'] = 1;
                                $cartData[$key]['shipping_discount_amount'] = 0;
                            } elseif ($getPromotionDetail['shipping_discount_type'] == 2) {
                                if ($val['is_free_shipping'] == 0 && $val['is_shippable'] == 1 && $val['shipping_cost'] == 1) {
                                    $shipping_discount_amount = $this->calculateProductShippingDiscount(($val['product_weight'] * $val['product_qty']), $totalShippableProductWeight, $getPromotionDetail['free_shipping_amount']);
                                    $cartData[$key]['is_free_shipping'] = 0;
                                    $cartData[$key]['shipping_discount_amount'] = $shipping_discount_amount;
                                }
                            }
                        }
                        // end of shippin discount
                        // cart discount
                        if ($getPromotionDetail['is_cart_discount'] == 1) {
                            if ($getPromotionDetail['cart_discount_type'] == 1) { // percent
                                $cartDiscountAmountOff = ($totalProductCartAmount * $getPromotionDetail['cart_discount_percent']) / 100;
                            } elseif ($getPromotionDetail['cart_discount_type'] == 2) { // amount off
                                $cartDiscountAmountOff = $getPromotionDetail['cart_discount_amount'];
                            }

                            $productDiscount = $this->calculateProductCartDiscount($val['product_subtotal'] - $val['membership_discount'], $totalProductCartAmount, $cartDiscountAmountOff);

                            if($productDiscount>0){
								if($productDiscount<($val['product_subtotal'] - $val['membership_discount']))
									$cartData[$key]['product_discount'] = $productDiscount;
								else
									$cartData[$key]['product_discount'] = $val['product_subtotal'] - $val['membership_discount'];
							}else
									$cartData[$key]['product_discount'] = '0.00';
                        }
                        // end of cart discount
                    }
					// free products
					if ($getPromotionDetail['is_free_product'] == 1) {
						$freeProductsArray = explode(',', $getPromotionDetail['free_products']);
						if (in_array($val['product_id'], $freeProductsArray) && !in_array($val['product_id'],$freeDiscountedItems)) {
							$totalItemQty = array_sum($itemToDiscount[$val['product_id']]['itemQty']);
							$totalItemAmount = array_sum($itemToDiscount[$val['product_id']]['itemTotalAmount']);
							$productDiscount = $this->calculateFreeProductDiscount($val, $itemToDiscount);
                            if($productDiscount>0){
								if($productDiscount<($val['product_subtotal'] - $val['membership_discount'])){
									$cartData[$key]['product_discount'] = $productDiscount;								
								}else
									$cartData[$key]['product_discount'] = $val['product_subtotal'] - $val['membership_discount'];
								$freeDiscountedItems[] = $val['product_id'];
							}else
								$cartData[$key]['product_discount'] = '0.00';
							$cartData[$key]['is_taxable'] = 0;
						}
					}
					// end of free products discount
                }
            } else {
                $cartData[$key]['shipping_discount_amount'] = '0.00';
            }
        }
        return $cartData;
    }

    /**
     * This action is used to check whether coupon is valid or not
     * @param void
     * @return void
     * @author Icreon Tech - DT
     */
    public function isCouponValid($promotionId, $userId, $sourceType = 'backend', $shipping_country = '0') {
        $returnData = array();
        $applicationConfigIds = $this->_config['application_config_ids'];
        $uSId = $applicationConfigIds['united_state_country_id'];
        $promotionDetailArr = array();
        $promotionDetailArr[] = $promotionId;
        $serviceManager = $this->getServiceLocator();
        $promotionTable = $serviceManager->get('Promotions\Model\PromotionTable');
        $getPromotionDetail = $promotionTable->getPromotionById($promotionDetailArr);

        $cartParam['user_id'] = $userId;
        $cartParam['source_type'] = !empty($sourceType) ? $sourceType : 'frontend';
        $cartProductArr = $this->getTransactionTable()->getCartProducts($cartParam);


        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];


        if (($getPromotionDetail['expiry_date'] != '' && $getPromotionDetail['expiry_date'] != 0 && substr(DATE_TIME_FORMAT, 0, 10) > substr($getPromotionDetail['expiry_date'], 0, 10)) || ($getPromotionDetail['start_date'] != '' && $getPromotionDetail['start_date'] != 0 && substr(DATE_TIME_FORMAT, 0, 10) < substr($getPromotionDetail['start_date'], 0, 10))) {
            $returnData['status'] = "error";
            $returnData['message'] = $msgArr['COUPON_CODE_EXPIRED'];
            $returnData['cartArray'] = $cartProductArr;
        } else if ($getPromotionDetail['usage_limit'] < 1) {
            $returnData['status'] = "error";
            $returnData['message'] = $msgArr['COUPON_CODE_REACH_LIMIT'];
            $returnData['cartArray'] = $cartProductArr;
        } else {
            $paramsArr = array();
            $paramsArr['user_id'] = $userId;
            $paramsArr['source_type'] = $sourceType;
            $paramsArr['promotionData'] = $getPromotionDetail;
            $totalDiscount = $this->getCouponDiscountForProductInCart($paramsArr, $shipping_country);
            $returnData['cartArray'] = $totalDiscount['cartArray'];

            if ($totalDiscount['apply'] == 0) {
                $returnData['status'] = "error";
                $returnData['message'] = $msgArr['COUPON_NOT_VALID_FOR_ANY_PRODUCT'];
            } else if ($totalDiscount['discount'] == 0 && $getPromotionDetail['is_free_shipping'] == 0) {
                $returnData['status'] = "error";
                $returnData['message'] = $msgArr['COUPON_NOT_VALID_FOR_ANY_PRODUCT'];
            } else {
                if (isset($getPromotionDetail['applicable_to'])) {
                    switch ($getPromotionDetail['applicable_to']) {
                        case '0' :
                        case '3' : $userUsedCouponArr = array();
                            $userUsedCouponArr['promotionId'] = $promotionId;
                            $userUsedCouponArr['userId'] = $userId;
                            $isUserUsedCoupon = $promotionTable->checkUserUsedCoupon($userUsedCouponArr);
                            $returnData['status'] = "success";
                            $returnData['message'] = $msgArr['COUPON_CODE_APPLIED'];
                            /* if (empty($isUserUsedCoupon)) {
                              $returnData['status'] = "success";
                              $returnData['message'] = $msgArr['COUPON_CODE_APPLIED'];
                              } else {
                              $returnData['status'] = "error";
                              $returnData['message'] = $msgArr['ALREADY_USED_COUPON'];
                              } */
                            break;
                        case '1' : if ($shipping_country != $uSId) {
                                $returnData['status'] = "error";
                                $returnData['message'] = $msgArr['COUPON_NOT_VALID'];
                            } else {
                                $userUsedCouponArr = array();
                                $userUsedCouponArr['promotionId'] = $promotionId;
                                $userUsedCouponArr['userId'] = $userId;
                                $isUserUsedCoupon = $promotionTable->checkUserUsedCoupon($userUsedCouponArr);
                                $returnData['status'] = "success";
                                $returnData['message'] = $msgArr['COUPON_CODE_APPLIED'];
                                /*
                                  if (empty($isUserUsedCoupon)) {
                                  $returnData['status'] = "success";
                                  $returnData['message'] = $msgArr['COUPON_CODE_APPLIED'];
                                  } else {
                                  $returnData['status'] = "error";
                                  $returnData['message'] = $msgArr['ALREADY_USED_COUPON'];
                                  } */
                            }
                            break;
                        case '2' :
                            if ($shipping_country == $uSId) {
                                $returnData['status'] = "error";
                                $returnData['message'] = $msgArr['COUPON_NOT_VALID'];
                            } else {
                                $userUsedCouponArr = array();
                                $userUsedCouponArr['promotionId'] = $promotionId;
                                $userUsedCouponArr['userId'] = $userId;
                                $isUserUsedCoupon = $promotionTable->checkUserUsedCoupon($userUsedCouponArr);

                                $returnData['status'] = "success";
                                $returnData['message'] = $msgArr['COUPON_CODE_APPLIED'];

                                /* if (empty($isUserUsedCoupon)) {
                                  $returnData['status'] = "success";
                                  $returnData['message'] = $msgArr['COUPON_CODE_APPLIED'];
                                  } else {
                                  $returnData['status'] = "error";
                                  $returnData['message'] = $msgArr['ALREADY_USED_COUPON'];
                                  } */
                            }
                            break;
                    }
                }
            }
        }
        $returnData['promotionData'] = $getPromotionDetail;


        return $returnData;
    }

    /**
     * This action is used to save transaction from add to cart to main transaction
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function saveTransactionAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $applicationConfigIds = $this->_config['application_config_ids'];
        $uSId = $applicationConfigIds['united_state_country_id'];
        $transaction = new Transaction($this->_adapter);

        $createTransactionForm = new CreateTrasactionForm();
        $paymentModeCheck = new PaymentModeCheck();
        $paymentModeCredit = new PaymentModeCredit();

        $request = $this->getRequest();
        $response = $this->getResponse();
        $userId = $request->getPost('user_id');
        $userArr = array('user_id' => $userId);
        $contactDetail = array();
        $contactDetail[0] = $this->getServiceLocator()->get('User\Model\UserTable')->getUser($userArr);
        //$contactAfihcDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAfihc($userArr);
        $contacAddressDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations($userArr);
        $addressDetailArr = array('' => 'Select one');
        if (!empty($contacAddressDetail)) {
            foreach ($contacAddressDetail as $addressDetail) {
                $userName = (!empty($contactDetail[0]['full_name'])) ? $contactDetail[0]['full_name'] . ", " : $contactDetail[0]['company_name'] . ", ";
                $address = $addressDetail['street_address_one'] . " " . $addressDetail['street_address_two'] . " " . $addressDetail['street_address_three'];
                $address = trim($address);
                $address = (!empty($address)) ? $address . ", " : "";
                $state = (!empty($addressDetail['state'])) ? $addressDetail['state'] . ", " : "";
                $city = (!empty($addressDetail['city'])) ? $addressDetail['city'] . ", " : "";
                $country_name = (!empty($addressDetail['country_name'])) ? $addressDetail['country_name'] . ", " : "";
                $zip_code = (!empty($addressDetail['zip_code'])) ? $addressDetail['zip_code'] : "";
                $addressLabel = $userName . $address . $state . $city . $country_name . $zip_code;
                $addressLabel = rtrim(trim($addressLabel), ',');
                $addressDetailArr[$addressDetail['address_information_id']] = $addressLabel;
            }
        }
        $createTransactionForm->get('billing_existing_contact')->setAttribute('options', $addressDetailArr);
        $createTransactionForm->get('shipping_existing_contact')->setAttribute('options', $addressDetailArr);
        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
        $countryList = array();
        foreach ($country as $key => $val) {
            $countryList[$val['country_id']] = $val['name'];
        }
        $createTransactionForm->get('billing_country')->setAttribute('options', $countryList);
        $createTransactionForm->get('shipping_country')->setAttribute('options', $countryList);
        $apoPoStates = $this->_config['transaction_config']['apo_po_state'];
        $createTransactionForm->get('apo_po_state')->setAttribute('options', $apoPoStates);
        if ($request->isPost()) {
            $createTransactionForm->setData($request->getPost());
            $pickUp = $request->getPost('pick_up');
            $validationGroups = array('user_id', 'billing_first_name', 'billing_last_name', 'billing_address', 'billing_city', 'billing_state', 'billing_zip_code', 'billing_country', 'billing_phone');
            $totalProducts = $request->getPost('totalProducts');
            $shipCompleteStatus = $request->getPost('shipCompleteStatus');


            if ($totalProducts == $shipCompleteStatus) {

            } else {
                if (empty($pickUp) || $pickUp == 0) {
                    $validationGroups[] = 'shipping_first_name';
                    $validationGroups[] = 'shipping_last_name';
                    $validationGroups[] = 'shipping_address';
                    $validationGroups[] = 'shipping_city';
                    $validationGroups[] = 'shipping_state';
                    $validationGroups[] = 'shipping_zip_code';
                    $validationGroups[] = 'shipping_country';
                    $validationGroups[] = 'shipping_phone';
                }
            }
            $paymentModeCash = $request->getPost('payment_mode_cash');
            if ($paymentModeCash == 1) {
                $validationGroups[] = 'payment_mode_cash_amount';
            }
            $createTransactionForm->setValidationGroup($validationGroups);
            $createTransactionForm->setInputFilter($transaction->getInputFilterSaveTransaction());
            if (!$createTransactionForm->isValid()) {
                $errors = $createTransactionForm->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'continue') {
                        foreach ($row as $typeError => $rower) {
                            if ($typeError != 'notInArray') {
                                $msg [$key] = $msgArr[$rower];
                            }
                        }
                    }
                }
            }
            $paymentModeCheck = $request->getPost('payment_mode_chk');
            $postArr = $request->getPost()->toArray();

            if (!empty($paymentModeCheck) && $paymentModeCheck == 1) {

                $paymentModeChkAmount = $postArr['payment_mode_chk_amount'];
                foreach ($paymentModeChkAmount as $paymentModeChkAmt) {
                    if (trim($paymentModeChkAmt) != '') {

                    }
                }
            }



            if (!empty($msg)) {
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {

                if (isset($postArr['is_apo_po']) && $postArr['is_apo_po'] == '3') {
                    $postArr['shipping_state'] = $postArr['apo_po_state'];
                    $postArr['shipping_country'] = $postArr['apo_po_shipping_country'];
                } else if (isset($postArr['is_apo_po']) && $postArr['is_apo_po'] == '2') {

                    $postArr['shipping_country'] = $postArr['apo_po_shipping_country'];
                    $postArr['shipping_state'] = $postArr['shipping_state_select'];
                } else if (isset($postArr['is_apo_po']) && $postArr['is_apo_po'] == '1') {
                    if (isset($postArr['shipping_country']) && $postArr['shipping_country'] == $uSId) {
                        $postArr['shipping_state'] = $postArr['shipping_state_select'];
                    } else {
                        $postArr['shipping_state'] = $postArr['shipping_state_text'];
                    }
                }


                if ($postArr['is_apo_po'] == '1' && $postArr['shipping_country'] != $uSId && !empty($postArr['shipping_method'])) {
                    $cartProductArr = array();
                    $cartProductArr['user_id'] = $postArr['user_id'];
                    $cartProductArr['userid'] = $postArr['user_id'];
                    $cartProductArr['source_type'] = 'backend';
                    $cartProductsWeight = $this->cartTotalWithTax($cartProductArr);
                    $totalWeight = 0;
                    /*if (!empty($cartProductsWeight)) {
                        foreach ($cartProductsWeight as $cartProductWei) {
                            if ($cartProductWei['is_shippable'] == 1) {
                                $totalWeight+=$cartProductWei['product_weight'];
                            }
                        }
                    }*/
                    $totalWeight = (isset($cartProductsWeight['totalweight']) && $cartProductsWeight['totalweight']>0)?$cartProductsWeight['totalweight']:'0.00';
                    $shippingMetArr = array();
                    $shippingMetArr['pb_shipping_id'] = $applicationConfigIds['internation_pb_id'];
                    $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                    $getShippingMethodsInter = $this->_transactionTable->getShippingMethod($shippingMethodArr);
//$lbsCheck = $this->_config['transaction_config']['international_config_info']['lbs_check'];
                    $lbsCheck = $getShippingMethodsInter[0]['lbs_check'];
                    if ($totalWeight <= $lbsCheck) {
//$getShippingMethods = $this->_config['transaction_config']['international_config_info']['shipping_types_first_class'];
                        foreach ($getShippingMethodsInter as $shipMethod) {
                            if ($shipMethod['pb_shipping_type_id'] == $applicationConfigIds['internation_less_than_lbs_id']) {
                                $getShippingMethods[] = $shipMethod;

								$postArr['shipping_method'] = $shipMethod['carrier_id'] . ',' . $shipMethod['service_type'] . ',' . $shipMethod['pb_shipping_type_id'];
                            }
                        }
                    } else {
//$getShippingMethods = $this->_config['transaction_config']['international_config_info']['shipping_types_priority'];
                        foreach ($getShippingMethodsInter as $shipMethod) {
                            if ($shipMethod['pb_shipping_type_id'] == $applicationConfigIds['internation_greater_than_lbs_id']) {
                                $getShippingMethods[] = $shipMethod;

								$postArr['shipping_method'] = $shipMethod['carrier_id'] . ',' . $shipMethod['service_type'] . ',' . $shipMethod['pb_shipping_type_id'];
                            }
                        }
                    }
                }
                $postArr['check-shipping'] = 1;

                $saveTransactionFormArr = $postArr;

                /* $profileDataParam = array();
                  $profileDataParam['profile_id'] = $saveTransactionFormArr['customer_profile_ids'];
                  $tokenDetailsResult = $this->getServiceLocator()->get('User\Model\UserTable')->getTokenDetails($profileDataParam);
                  if(count($tokenDetailsResult)>0)
                  $paymentTransactionData['22'] = $tokenDetailsResult[0]['credit_card_type_id'];
                  else
                  $paymentTransactionData['22'] = '';
                 */

                $saveTransactionFormArr['profile_id'] = $saveTransactionFormArr['customer_profile_ids'];
                $responseArr = $this->saveFinalTransaction($saveTransactionFormArr);
                if (!empty($responseArr['quantity_error'])) {
                    unset($responseArr['quantity_error']);
                    $messages = array('status' => "quantity_error", 'message' => $responseArr);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                    exit;
                }
                $messages = array('status' => "success");
                if ($responseArr['status'] == 'success') {
                    $this->flashMessenger()->addMessage($msgArr['SUCCESSFULLY_CREATED_TRANSACTION']);
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
    }

    public function savePaymentReceived($paymentTransactionData = array(), $transaction) {
        $paymentTransactionData['added_by'] = isset($this->auth->getIdentity()->crm_user_id) ? $this->auth->getIdentity()->crm_user_id : '';
        $paymentTransactionData['added_date'] = DATE_TIME_FORMAT;
        $paymentTransactionData['modify_by'] = isset($this->auth->getIdentity()->crm_user_id) ? $this->auth->getIdentity()->crm_user_id : '';
        $paymentTransactionData['modify_date'] = DATE_TIME_FORMAT;

        $profileParam = array();
        $profileParam['transaction_id'] = $paymentTransactionData['transaction_id'];
        $transactionProfileDetails = $this->_transactionTable->getTransactionProfileCreditCartId($profileParam);
        $paymentTransactionData = $transaction->getTransactionPaymentReceiveArr($paymentTransactionData);
        $paymentTransactionData['9'] = $transactionProfileDetails[0]['credit_card_type_id'];
        $paymentTransactionData['10'] = '';
        $transactionPaymentId = $this->_transactionTable->saveTransactionPaymentReceive($paymentTransactionData);
    }

    public function appendQuestionMars($count) {
        $returnstr = '';
        for ($i = 1; $i <= $count; $i++) {
            $returnstr.='?,';
        }
        return rtrim($returnstr, ',');
    }

    /**
     * This action is used to save transaction from cart
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function saveFinalTransaction($postArr) { // by dev 3
        $response = $this->getResponse();
        $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter')->getDriver()->getConnection()->beginTransaction();
        $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->offAutoCommit();

        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $postArr['shipping_method'] = (!empty($postArr['shipping_method']) && (empty($postArr['pick_up']) || $postArr['pick_up'] == 0)) ? $postArr['shipping_method'] : '';
        $postArr['is_free_shipping'] = '0';
        $responseArr = array();
        $cartProductArr = array();
        $cartProductArr['user_id'] = $postArr['user_id'];
        $cartProductArr['userid'] = $postArr['user_id'];
        $cartProductArr['source_type'] = !empty($postArr['source_type']) ? $postArr['source_type'] : 'backend';
        $cartProductArr['shipcode'] = isset($postArr['shipping_zip_code']) ? $postArr['shipping_zip_code'] : '';
        $cartProductArr['source_all'] = !empty($postArr['source_all']) ? $postArr['source_all'] : '';
        $cartProductArr['shipping_method'] = $postArr['shipping_method'];
        $cartProductArr['couponcode'] = !empty($postArr['couponcode']) ? $postArr['couponcode'] : '';
        $cartProductArr['postData'] = $postArr;
        $cartProductArr['free_shipping'] = false;
        $isFreeShipping = false;
        $postArr['is_free_shipping'] = '0';
        $freeShippingAmount = '';


        $transaction = new Transaction($this->_adapter);

        /* if (!empty($postArr['promotion_id'])) {
          $checkFreeShipArr = array();
          $checkFreeShipArr['promotion_id'] = $postArr['promotion_id'];
          $promotionTable = $this->getServiceLocator()->get('Promotions\Model\PromotionTable');
          $getPromotionDetail = $promotionTable->getPromotionByPromotionId($checkFreeShipArr);
          $freeShipping = $getPromotionDetail['is_free_shipping'];
          $isFreeShipping = (!empty($freeShipping)) ? true : false;
          $freeShippingAmount = $getPromotionDetail['free_shipping_amount'];
          // $cartProductArr['free_shipping'] = $isFreeShipping;
          } */

        //asd($cartProductArr,2);

        $this->campaignSession = new Container('campaign');
        $cartProductData = $this->cartTotalWithTax($cartProductArr);
        //asd($cartProductData);

        $postArr['promotion_id'] = $cartProductData['promotionData']['promotion_id'];

        if (isset($cartProductData['cartData'])) {
            $cartProducts = $cartProductData['cartData'];
        }
        $responseArr['status'] = 'error';
        if (!empty($cartProducts)) {
            $totalSubtotalAmount = 0;
            $totalDiscountAmount = 0;
            $totalTaxableAmount = 0;
            $totalCartAmount = 0;
            $totalShippingAmount = 0;
            $totalShippingHandlingAmount = 0;

            $pledgeDonationAmount = 0;
            $shipCompleteStatus = 0;
            $userId = 0;
            $i = 0;
            $donationTotalAmount = 0;
            $unshipProductTotalAmount = 0;
            $arrUnshipProductTotalAmount = array();
            $arrAllProductTotalAmount = array();
            $transactionTypeDonation = '';
            $transactionTypePurchase = '';
            $productQtyErrorArr = array();
            $totalProductsInCart = count($cartProducts);

            $isSaveShippingAddress = 0;
            $totalCashAmount = 0;
            $totalCheckAmount = 0;
            $totalCreditAmount = 0;
            $shippableProductsAmount = 0;
            $transactionStatus = 1;
            $countProStatus = 0;
            $totalProductRevenue = 0;
            $donAmt = 0;
            $paymentArray = array();
			$totalShippingPrice = 0;


##################################################################################################################################
            $sendThankyouMail = 0;
            foreach ($cartProducts as $cartKey => $cartProduct) {
                $productLabel = '';
                $totalSubtotalAmount+= $cartProduct['product_subtotal'] - $cartProduct['membership_discount'];
                $totalDiscountAmount+= $cartProduct['product_discount'];
                $totalTaxableAmount+= $cartProduct['product_taxable_amount'];
                $totalCartAmount+=$cartProduct['product_total'];
                $totalProductRevenue+=$cartProduct['total_product_revenue'];


                if (isset($cartProduct['shipping_discount_amount']))
                    $totalShippingAmount+=($cartProduct['shipping_price'] - $cartProduct['shipping_discount_amount']);
                elseif(isset($cartProduct['shipping_discount_in_amount']))
                    $totalShippingAmount+=($cartProduct['shipping_price'] - $cartProduct['shipping_discount_in_amount']);
				else
                    $totalShippingAmount+=$cartProduct['shipping_price'];

                $totalShippingHandlingAmount+=$cartProduct['surcharge_price'] + $cartProduct['shipping_handling_price'];

				$totalShippingPrice+= $totalShippingAmount+$totalShippingHandlingAmount;
                $productTotal = $cartProduct['product_total'];

                if ($cartProduct['product_type_id'] == '2') {
                    $pledgeDonationAmount+=$cartProduct['product_total'];
                    $sendThankyouMail = 1;
                }
                if ($cartProduct['product_transaction_type'] == '1') {
                    $transactionTypeDonation = '1';
                } else {
                    $transactionTypePurchase = '2';
                }

                if ($cartProduct['product_type_id'] != '4') {
                    if ($cartProduct['is_shippable'] == '0' || empty($cartProduct['is_shippable'])) {
                        $shipCompleteStatus++;
                        $unshipProductTotalAmount = $unshipProductTotalAmount + $productTotal;
                        //$arrUnshipProductTotalAmount['unshipped_product_amount'][] = $productTotal;
                        $arrUnshipProductTotalAmount['product_type_id'][] = $cartProduct['product_type_id'];
                        $paymentArray[$cartKey]['cart_product_amount'] = round($productTotal, 2);
                        $paymentArray[$cartKey]['cart_product_type_id'] = $cartProduct['product_type_id'];
                        //$paymentArray[$cartKey]['unshipped_product_amount'] = round($productTotal, 2);
                        $paymentArray[$cartKey]['product_type_id'] = $cartProduct['product_type_id'];
                        $paymentArray[$cartKey]['payment_recieve'] = '1';
                    } else {
                        $shippableProductsAmount+=$shippableProductsAmount + ($cartProduct['product_subtotal'] - $cartProduct['membership_discount']);
                        $isSaveShippingAddress = 1;
                        $paymentArray[$cartKey]['cart_product_amount'] = round($productTotal, 2);
                        $paymentArray[$cartKey]['cart_product_type_id'] = $cartProduct['product_type_id'];
                        //$paymentArray[$cartKey]['unshipped_product_amount'] = round($productTotal, 2);
                        $paymentArray[$cartKey]['product_type_id'] = $cartProduct['product_type_id'];
                        $paymentArray[$cartKey]['payment_recieve'] = '2';
                    }
                } else {
                    $paymentArray[$cartKey]['cart_product_amount'] = round($productTotal, 2);
                    $paymentArray[$cartKey]['cart_product_type_id'] = $cartProduct['product_type_id'];
                    //$paymentArray[$cartKey]['unshipped_product_amount'] = round($productTotal, 2);
                    $paymentArray[$cartKey]['product_type_id'] = $cartProduct['product_type_id'];
                    $paymentArray[$cartKey]['payment_recieve'] = '2';
                }
                if ($cartProduct['product_mapping_id'] == '1' || $cartProduct['product_mapping_id'] == '3' || $cartProduct['product_mapping_id'] == '5') {
                    $transactionStatus = 9;
                    $countProStatus++;
                }
                if ($cartProduct['product_transaction_type'] == 1) {
                    $donAmt = $donAmt + ($cartProduct['product_subtotal'] - $cartProduct['membership_discount']);
                }

                if ($cartProduct['product_type_id'] == 1 && $cartProduct['product_qty'] > $cartProduct['total_in_stock']) {
                    $productQtyErrorArr['message'] = sprintf($msgArr['QUANTITY_EXCEED'], $cartProduct['total_in_stock'], $cartProduct['product_name']);
                }
                $i++;
            }
            $userId = $cartProduct['user_id'];
            if (!empty($productQtyErrorArr)) {
                $productQtyErrorArr['quantity_error'] = 1;
                $productQtyErrorArr['status'] = 'quantity_error';
                return $productQtyErrorArr; //vvvvvvvvvvvvvvvv
            }

            $transactionType = trim($transactionTypeDonation . ',' . $transactionTypePurchase, ',');

            if ($countProStatus == count($cartProducts)) {
                $transactionStatus = '10';
            }
            /* if ($transactionType == '1') {
              $transactionStatus = 1;
              } else if ($transactionType == '2') {
              $transactionStatus = 1;
              } else {
              $transactionStatus = 9;
              } */


            /* Add transaction record */
            $finalTransaction = $postArr;
            $dateTime = DATE_TIME_FORMAT;

            $crm_user_id = !(empty($this->auth->getIdentity()->crm_user_id)) ? $this->auth->getIdentity()->crm_user_id : '';
            $transactionSourceId = ($cartProductArr['source_type'] == 'backend') ? $this->_config['transaction_source']['transaction_source_crm_id'] : $this->_config['transaction_source']['transaction_source_id'];
            $finalTransaction['transaction_source_id'] = $transactionSourceId = '3'; // ellis
            $finalTransaction['num_items'] = $totalProductsInCart;
            $finalTransaction['sub_total_amount'] = $totalSubtotalAmount;
            $finalTransaction['total_discount'] = $totalDiscountAmount;
            $finalTransaction['shipping_amount'] = ($totalShippingPrice<0)?0.00:$totalShippingAmount;
            $finalTransaction['handling_amount'] = ($totalShippingPrice<0)?0.00:$totalShippingHandlingAmount;
            $finalTransaction['total_tax'] = $totalTaxableAmount;
            //$finalTransaction['transaction_amount'] = $totalCartAmount;
			$finalTransaction['transaction_amount'] = ($totalSubtotalAmount-$totalDiscountAmount)+($finalTransaction['shipping_amount']+$finalTransaction['handling_amount']+$totalTaxableAmount);
            $finalTransaction['transaction_date'] = $dateTime;
            $finalTransaction['transaction_type'] = $transactionType;
            $finalTransaction['transaction_status_id'] = $transactionStatus;
            $finalTransaction['added_by'] = $crm_user_id;
            $finalTransaction['added_date'] = $dateTime;
            $finalTransaction['modify_by'] = $crm_user_id;
            $finalTransaction['modify_date'] = $dateTime;
            $finalTransaction['campaign_code'] = '';
            $finalTransaction['campaign_id'] = '';
            $finalTransaction['channel_id'] = '';
            $finalTransaction['don_amount'] = $donAmt;
            $finalTransaction['total_product_revenue'] = $totalProductRevenue;
            if (isset($postArr['payment_mode_invoice']) && $postArr['payment_mode_invoice'] == 1) {
                $finalTransaction['transaction_status_id'] = 10;
            }



            /* General Campaign Code */
            if (isset($this->auth->getIdentity()->crm_user_id) && !empty($this->auth->getIdentity()->crm_user_id))
                $transactionChannelId = 2; // Direct mail
            else
                $transactionChannelId = 5; // Web

            if (!isset($postArr['campaign_code_id']) || empty($postArr['campaign_code_id'])) {
                $generalCampaign = $this->_transactionTable->getGeneralCampaign();

                if (isset($generalCampaign) && !empty($generalCampaign)) {
                    $finalTransaction['campaign_code'] = $generalCampaign[0]['campaign_code'];
                    $finalTransaction['campaign_id'] = $generalCampaign[0]['campaign_id'];
                    $finalTransaction['channel_id'] = $transactionChannelId;
                }


                $finalTransaction['campaign_code'] = (isset($this->campaignSession->campaign_code) && !empty($this->campaignSession->campaign_code) ? $this->campaignSession->campaign_code : $finalTransaction['campaign_code']);
                $finalTransaction['campaign_id'] = (isset($this->campaignSession->campaign_id) && !empty($this->campaignSession->campaign_id) ? $this->campaignSession->campaign_id : $finalTransaction['campaign_id']);
                $finalTransaction['channel_id'] = (isset($this->campaignSession->channel_id) && !empty($this->campaignSession->channel_id) ? $this->campaignSession->channel_id : $finalTransaction['channel_id']);
            } else {
                $finalTransaction['campaign_code'] = (isset($postArr['campaign_code']) && !empty($postArr['campaign_code']) ? $postArr['campaign_code'] : $finalTransaction['campaign_code']);
                $finalTransaction['campaign_id'] = (isset($postArr['campaign_code_id']) && !empty($postArr['campaign_code_id']) ? $postArr['campaign_code_id'] : $finalTransaction['campaign_id']);
                $searchParam['campaign_id'] = $finalTransaction['campaign_id'];
                $searchResultCamp = $this->getServiceLocator()->get('Campaign\Model\CampaignTable')->searchCampaignDetails($searchParam);
                if (isset($searchResultCamp[0]['channel_id']) && !empty($searchResultCamp[0]['channel_id'])) {
                    $arr = explode(",", $searchResultCamp[0]['channel_id']);
                    $finalTransaction['channel_id'] = $transactionChannelId;
                    //if (is_array($arr) && count($arr) > 0 && in_array(5 , $arr))
                    if (is_array($arr) && count($arr) > 0 && in_array($transactionChannelId, $arr)) {
                        //update user campaign channel
                        $finalTransaction['user_id'] = (isset($finalTransaction['user_id'])
                                && !empty($finalTransaction['user_id']) ? $finalTransaction['user_id'] : '');
                        $updatedResponse = $this->getServiceLocator()->get('Campaign\Model\CampaignTable')->userUpdateCampaign(array(
                            'user_id' => $finalTransaction['user_id'], 'campaign_id' => $finalTransaction['campaign_id'],
                            'channel_id' => $transactionChannelId));
                    }
                } else {
                    $finalTransaction['channel_id'] = $transactionChannelId;
                }
            }
            $group_id = $this->_transactionTable->getGroupId($finalTransaction);
            $finalTransaction['group_id'] = isset($group_id[0]['group_id']) && !empty($group_id[0]['group_id']) ? $group_id[0]['group_id'] : "";
            $pbShippingTypeId = '';
            if (!empty($finalTransaction['shipping_method'])) {
                $shippingMetArr = explode(',', $finalTransaction['shipping_method']);
                $pbShippingTypeId = (!empty($shippingMetArr[2])) ? $shippingMetArr[2] : '';
            }
            $finalTransaction['shipping_type_id'] = $pbShippingTypeId;
            $transaction = new Transaction($this->_adapter);
            if ($totalProductsInCart == $shipCompleteStatus || $postArr['invoice_number'] != '') {
                $finalTransaction['transaction_status_id'] = 10;
            } elseif (!empty($shipCompleteStatus) && $shipCompleteStatus < $totalProductsInCart) {
                $finalTransaction['transaction_status_id'] = 11;
            }

            if(isset($postArr['pick_up']) && $postArr['pick_up'] == 1) {
                $finalTransaction['transaction_status_id'] = 4; // Release status
            }
            $finalTransactionArr = $transaction->getFinalTransactionArr($finalTransaction);
            $transactionId = $this->_transactionTable->saveFinalTransaction($finalTransactionArr);


            $this->_transactionTable->updateRevenueGoal($finalTransactionArr);
            $this->_transactionTable->updateChannelRevenue($finalTransactionArr);
			$this->_transactionTable->updateTransctionCustomerIP(array($transactionId,$_SERVER['REMOTE_ADDR']));
            /* end add transaction record */

            $finalTransaction['transaction_id'] = $transactionId;

            $isPromotionApplied = '0';
            /* Code start for save coupon used by user */
            if (!empty($postArr['promotion_id'])) {
                $isPromotionApplied = '1';
                $promotionDetailArr = array();
                $promotionDetailArr[] = $postArr['promotion_id'];
                $promotionTable = $this->getServiceLocator()->get('Promotions\Model\PromotionTable');
                $getPromotionDetail = $promotionTable->getPromotionByPromotionId($promotionDetailArr);

                if ($getPromotionDetail['usage_limit_type'] != '1') {
                    $couponUsedArr = $transaction->getCouponUsedArr($finalTransaction);
                    $couponLogId = $this->_transactionTable->saveCouponUsedArr($couponUsedArr);
                }
            }
            /* End  Code for save coupon used by user */
            /* Add payment information  of transaction */

            $amount_total = $totalCartAmount;
            $amountIdArray = array();
            /*
              if (isset($postArr['payment_mode_cash']) && $postArr['payment_mode_cash'] == 1) {
              $paymentTransactionData = array();
              $paymentTransactionData['amount'] = $finalTransaction['payment_mode_cash_amount'];
              $paymentTransactionData['transaction_id'] = $transactionId;
              $paymentTransactionData['payment_mode_id'] = 4;
              $paymentTransactionData['added_by'] = $crm_user_id;
              $paymentTransactionData['added_date'] = $dateTime;
              $paymentTransactionData['modify_by'] = $crm_user_id;
              $paymentTransactionData['modify_date'] = $dateTime;
              $paymentTransactionData = $transaction->getTransactionPaymentArr($paymentTransactionData);
              $paymentTransactionData['22'] = '';
              $transactionPaymentId = $this->_transactionTable->saveTransactionPaymentDetail($paymentTransactionData);
              $totalCashAmount = $finalTransaction['payment_mode_cash_amount'];
              $paymentTransactionData = array();
              $paymentTransactionData['amount'] = $totalCashAmount;
              $paymentTransactionData['transaction_id'] = $transactionId;
              $paymentTransactionData['authorize_transaction_id'] = '';
              $paymentTransactionData['transaction_payment_id'] = $transactionPaymentId;
              $amountIdArray[] = array('id' => $transactionPaymentId, 'amount' => $totalCashAmount, 'type' => 'cash', 'batch_id' => $postArr['payment_cash_batch_id']);

              }
             */
            /*
              if (isset($postArr['payment_mode_chk']) && $postArr['payment_mode_chk'] == 1) {
              if (!empty($finalTransaction['payment_mode_chk_amount'])) {
              $i = 0;
              foreach ($finalTransaction['payment_mode_chk_amount'] as $paymentCheck) {
              if ($finalTransaction['payment_mode_chk_amount'][$i] > 0) {
              $paymentTransactionData = array();
              $paymentTransactionData['amount'] = $finalTransaction['payment_mode_chk_amount'][$i];
              $paymentTransactionData['cheque_type_id'] = $finalTransaction['payment_mode_chk_type'][$i];
              $paymentTransactionData['cheque_number'] = $finalTransaction['payment_mode_chk_num'][$i];
              $paymentTransactionData['id_type_id'] = $finalTransaction['payment_mode_chk_id_type'][$i];
              $paymentTransactionData['id_number'] = $finalTransaction['payment_mode_chk_id'][$i];
              $paymentTransactionData['transaction_id'] = $transactionId;
              $paymentTransactionData['payment_mode_id'] = 3;
              $paymentTransactionData['added_by'] = $crm_user_id;
              $paymentTransactionData['added_date'] = $dateTime;
              $paymentTransactionData['modify_by'] = $crm_user_id;
              $paymentTransactionData['modify_date'] = $dateTime;
              $transactionCheckPaymentArr = $transaction->getTransactionPaymentArr($paymentTransactionData);
              $paymentTransactionData['22'] = '';
              $transactionPaymentId = $this->_transactionTable->saveTransactionPaymentDetail($transactionCheckPaymentArr);
              $totalCheckAmount = $totalCheckAmount + $finalTransaction['payment_mode_chk_amount'][$i];
              $paymentTransactionData = array();
              $paymentTransactionData['amount'] = $totalCheckAmount;
              $paymentTransactionData['transaction_id'] = $transactionId;
              $paymentTransactionData['authorize_transaction_id'] = '';
              $paymentTransactionData['transaction_payment_id'] = $transactionPaymentId;

              $amountIdArray[] = array('id' => $transactionPaymentId, 'amount' => $finalTransaction['payment_mode_chk_amount'][$i], 'type' => 'check', 'batch_id' => $postArr['payment_check_batch_id'][$i]);

              }
              $i++;
              }
              }
              }
             */
            // $transactionIdsArr = explode(',', $postArr['transaction_ids']);
            // $transactionAmountsArr = explode(',', $postArr['transaction_amounts']);
            //  $customerProfileIdsArr = explode(',', $postArr['customer_profile_ids']);
            /*
              if (!empty($transactionIdsArr)) {
              $i = 0;
              foreach ($transactionIdsArr as $authorizeTransactionId) {
              if (!empty($authorizeTransactionId)) {
              $paymentTransactionData = array();
              $paymentTransactionData['amount'] = $transactionAmountsArr[$i];
              $paymentTransactionData['transaction_id'] = $transactionId;
              $paymentTransactionData['payment_mode_id'] = 1;
              $paymentTransactionData['authorize_transaction_id'] = $authorizeTransactionId;
              $paymentTransactionData['profile_id'] = $customerProfileIdsArr[$i];
              $paymentTransactionData['added_by'] = $crm_user_id;
              $paymentTransactionData['added_date'] = $dateTime;
              $paymentTransactionData['modify_by'] = $crm_user_id;
              $paymentTransactionData['modify_date'] = $dateTime;
              $paymentTransactionData = $transaction->getTransactionPaymentArr($paymentTransactionData);

              $profileDataParam = array();
              $profileDataParam['profile_id'] = $customerProfileIdsArr[$i];
              $tokenDetailsResult = $this->getServiceLocator()->get('User\Model\UserTable')->getTokenDetails($profileDataParam);

              if (count($tokenDetailsResult) > 0)
              $paymentTransactionData['22'] = $tokenDetailsResult[0]['credit_card_type_id'];
              else
              $paymentTransactionData['22'] = '';

              $transactionPaymentId = $this->_transactionTable->saveTransactionPaymentDetail($paymentTransactionData);
              $totalCreditAmount = $totalCreditAmount + $transactionAmountsArr[$i];

              $amountIdArray[] = array('id' => $transactionPaymentId, 'amount' => $transactionAmountsArr[$i], 'type' => 'credit', 'authorize_transaction_id' => $transactionIdsArr[$i], 'profile_id' => $customerProfileIdsArr[$i]);
              $i++;
              }
              }
              } */

            $postArr['source_type'] = $cartProductArr['source_type'];
            $postArr['address_type_status'] = "0";
            if (!empty($postArr['save_shipping_address']) && $postArr['save_shipping_address'] == '1' && isset($postArr['shipping_phone']) && trim($postArr['shipping_phone']) != "" && is_numeric(trim($postArr['shipping_phone']))) {
                $postArr['address_type_status'] = "1";
                $searchArrayN1 = array();
                $searchArrayN1['user_id'] = $postArr['user_id'];
                $phoneDetail1 = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserPhoneInformations($searchArrayN1);
                $postArr['shipping_phone_info_id'] = (!empty($phoneDetail1[0]['phone_contact_id'])) ? $phoneDetail1[0]['phone_contact_id'] : "";
            } elseif (!empty($postArr['save_billing_address']) && $postArr['save_billing_address'] == '1' && isset($postArr['billing_phone']) && trim($postArr['billing_phone']) != "" && is_numeric(trim($postArr['billing_phone']))) {
                $postArr['address_type_status'] = "2";
                $searchArrayN1 = array();
                $searchArrayN1['user_id'] = $postArr['user_id'];
                $phoneDetail1 = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserPhoneInformations($searchArrayN1);
                $postArr['billing_phone_info_id'] = (!empty($phoneDetail1[0]['phone_contact_id'])) ? $phoneDetail1[0]['phone_contact_id'] : "";
            }

            if (!empty($postArr['save_billing_address']) && $postArr['save_billing_address'] == '1') {
                $postArr['address_type'] = 'billing';
                $this->insertUpdateBillingShipping($postArr);
            }
            if (!empty($postArr['save_shipping_address']) && $postArr['save_shipping_address'] == '1') {
                $postArr['address_type'] = 'shipping';
                $this->insertUpdateBillingShipping($postArr);
            }
            /* End add payment information  of transaction */
            /* Add transaction note info */
            if (!empty($finalTransaction['notes']) && trim($finalTransaction['notes']) != '') {
                $transactionNoteData = array();
                $transactionNoteData['transaction_id'] = $transactionId;
                $transactionNoteData['note'] = $finalTransaction['notes'];
                $transactionNoteData['added_by'] = $crm_user_id;
                $transactionNoteData['added_date'] = $dateTime;
                $transactionNoteData['modify_date'] = $dateTime;
                $transactionNoteArr = $transaction->getTransactionNoteArr($transactionNoteData);
                $transactionNoteId = $this->_transactionTable->saveTransactionNote($transactionNoteArr);
            }
            /* Add transaction note info */
            /* Add transaction billing shipping address info */
            $finalTransaction['transaction_id'] = $transactionId;
            $pickUp = !empty($finalTransaction['pick_up']) ? $finalTransaction['pick_up'] : '';
            $phoneNumFinal = "";
            if (isset($finalTransaction['shipping_country_code']) and trim($finalTransaction['shipping_country_code']) != "" and is_numeric(trim($finalTransaction['shipping_country_code']))) {
                $phoneNumFinal .= trim($finalTransaction['shipping_country_code']);
            }
            if (isset($finalTransaction['shipping_area_code']) and trim($finalTransaction['shipping_area_code']) != "" and is_numeric(trim($finalTransaction['shipping_area_code']))) {
                $phoneNumFinal .= "-" . trim($finalTransaction['shipping_area_code']);
            }
            if (isset($finalTransaction['shipping_phone']) and trim($finalTransaction['shipping_phone']) != "" and is_numeric(trim($finalTransaction['shipping_phone']))) {
                $phoneNumFinal .= "-" . trim($finalTransaction['shipping_phone']);
            }

            $phoneNumFinal2 = "";
            if (isset($finalTransaction['billing_country_code']) and trim($finalTransaction['billing_country_code']) != "" and is_numeric(trim($finalTransaction['billing_country_code']))) {
                $phoneNumFinal2 .= trim($finalTransaction['billing_country_code']);
            }
            if (isset($finalTransaction['billing_area_code']) and trim($finalTransaction['billing_area_code']) != "" and is_numeric(trim($finalTransaction['billing_area_code']))) {
                $phoneNumFinal2 .= "-" . trim($finalTransaction['billing_area_code']);
            }
            if (isset($finalTransaction['billing_phone']) and trim($finalTransaction['billing_phone']) != "" and is_numeric(trim($finalTransaction['billing_phone']))) {
                $phoneNumFinal2 .= "-" . trim($finalTransaction['billing_phone']);
            }

            $finalTransaction['shipping_phone'] = $phoneNumFinal;
            $finalTransaction['billing_phone'] = $phoneNumFinal2;
            if ((!empty($isSaveShippingAddress) && $isSaveShippingAddress == 0) ||
                    ($postArr['totalProducts'] > 0 && $postArr['totalProducts'] == $postArr['shipCompleteStatus'])) {
                $finalTransaction['shipping_title'] = '';
                $finalTransaction['shipping_first_name'] = '';
                $finalTransaction['shipping_last_name'] = '';
                $finalTransaction['shipping_company'] = '';
                $finalTransaction['shipping_address'] = '';
                $finalTransaction['shipping_city'] = '';
                $finalTransaction['shipping_state'] = '';
                $finalTransaction['shipping_zip_code'] = '';
                $finalTransaction['shipping_country'] = '';
                $finalTransaction['shipping_phone'] = '';
            }

            if (isset($postArr['pick_up']) && $postArr['pick_up'] == 1) {
                $finalTransaction['shipping_title'] = '';
                $finalTransaction['shipping_first_name'] = '';
                $finalTransaction['shipping_last_name'] = '';
                $finalTransaction['shipping_company'] = '';
                $finalTransaction['shipping_address'] = '';
                $finalTransaction['shipping_city'] = '';
                $finalTransaction['shipping_state'] = '';
                $finalTransaction['shipping_zip_code'] = '';
                $finalTransaction['shipping_country'] = '';
                $finalTransaction['shipping_phone'] = '';
            }
            /* if(isset($postArr['cash_payment']) && $postArr['cash_payment']=='on') {
              $finalTransaction['billing_title'] = '';
              $finalTransaction['billing_first_name'] = '';
              $finalTransaction['billing_last_name'] = '';
              $finalTransaction['billing_company'] = '';
              $finalTransaction['billing_address'] = '';
              $finalTransaction['billing_city'] = '';
              $finalTransaction['billing_state'] = '';
              $finalTransaction['billing_zip_code'] = '';
              $finalTransaction['billing_country'] = '';
              $finalTransaction['billing_phone'] = '';
              } */


            $transactionBillingShippingArr = $transaction->getTransactionBillingShipping($finalTransaction);

            $transactionBillingShippingId = $this->_transactionTable->saveTransactionBillingShippingDetail($transactionBillingShippingArr);
            /* End add transaction billing shipping address info */
            /* Add cart product to transaction table */

            $isShipping = 0;

            if (!empty($cartProducts)) {

                $arrDuplicateCertificate = array();
                foreach ($cartProducts as $cartKey => $cartProduct) {
                    if ($cartProduct['is_shippable'] == '1') {
                        $isShipping = 1;
                    }
                    $insertCartDataArr = $cartProduct;
                    $insertCartDataArr['transaction_id'] = $transactionId;
                    $insertCartDataArr['added_by'] = $crm_user_id;
                    $insertCartDataArr['added_date'] = $dateTime;
                    $insertCartDataArr['modify_by'] = $crm_user_id;
                    $insertCartDataArr['modify_date'] = $dateTime;
                    $insertCartDataArr['type_of_product'] = $cartProduct['type_of_product'];

					if (isset($cartProduct['shipping_discount_amount']))
						$insertCartDataArr['shipping_price']=$this->RoundAmount($cartProduct['shipping_price'] - $cartProduct['shipping_discount_amount']);
					elseif(isset($cartProduct['shipping_discount_in_amount']))
						$insertCartDataArr['shipping_price']=$this->RoundAmount($cartProduct['shipping_price'] - $cartProduct['shipping_discount_in_amount']);


					$totalShipping = $insertCartDataArr['shipping_price']+$cartProduct['surcharge_price']+$cartProduct['shipping_handling_price'];

					if($totalShipping<0){
						$insertCartDataArr['shipping_price'] = 0.00;
						$insertCartDataArr['surcharge_price'] = 0.00;
						$insertCartDataArr['shipping_handling_price'] = 0.00;
					}

                    if (!empty($cartProduct['item_name']))
                        $insertCartDataArr['item_name'] = $cartProduct['item_name'];

                    if (!empty($cartProduct['manifest_info']))
                        $insertCartDataArr['manifest_info'] = $cartProduct['manifest_info'];

                    if (!empty($cartProduct['manifest_type']))
                        $insertCartDataArr['manifest_type'] = $cartProduct['manifest_type'];


                    if ($cartProduct['product_type_id'] == 4) {
                        $search = array();
                        $search['fof_id'] = $cartProduct['cart_id'];
                        $searchResult = $this->getServiceLocator()->get('User\Model\UserTable')->getFofCart($search);
                        $fofImage = $searchResult[0]['fof_image'];
                        $this->moveFileFromTempToFOF($fofImage);
                    }
                    if ($cartProduct['product_type_id'] == '2') {
                        if (!empty($cartProduct['campaign_id'])) {
                            $donation_amount = $cartProduct['product_amount'];
                            $campaign_id_backend = $cartProduct['campaign_id'];
                            $campArrayBack['campaign_id'] = $campaign_id_backend;
                            $campArrayBack['revenue_recieved'] = $donation_amount;
                            $this->getServiceLocator()->get('Campaign\Model\CampaignTable')->updateRevenueGoal($campArrayBack);
                        }
                        $searchDonNotify = $this->_transactionTable->getDonNotify($cartProduct);
                        foreach ($searchDonNotify as $key => $val) {
                            if ($val['email_id'] != 'Email') {
                                $donationNotifyArr['donarName'] = $val['name'];
                                $name = explode(" ", $val['notify_name']);
                                $donationNotifyArr['first_name'] = $name[0];
                                $donationNotifyArr['last_name'] = $name[1];
                                $donationNotifyArr['name'] = $val['notify_name'];
                                $donationNotifyArr['email_id'] = $val['email_id'];
                                $donationNotifyArr['amount'] = $cartProduct['product_amount'];
                                $donationNotifyArr['program_name'] = $val['program'];
                                $email_id_template_int = 15;
                                $donationNotifyArr['admin_email'] = $this->_config['soleif_email']['email'];
                                $donationNotifyArr['admin_name'] = $this->_config['soleif_email']['name'];
                                $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($donationNotifyArr, $email_id_template_int, $this->_config['email_options']);
                            }
                        }
                    }
                    //asd($cartProduct);
                    /*
                      if(isset($postArr['cash_payment']) && $postArr['cash_payment']=='on') { // cash payment
                      $isPendingTransaction = 1;
                      $updateCartParam = array();
                      $updateCartParam['cart_id'] = $cartProduct['cart_id'];
                      $updateCartParam['is_pending_transaction'] = $isPendingTransaction;
                      $updateCartParam['pending_transaction_id'] = $transactionId;
                      $updateCartParam['product_type_id'] = $cartProduct['product_type_id'];
                      $updateCartParam['user_id'] = $cartProduct['user_id'];
                      $updateCartParam['pending_transaction_date'] = $dateTime;
                      $updateCartParam['item_info'] = $cartProduct['item_info'];
                      $updateCartParam['product_mapping_id'] = $cartProduct['product_mapping_id'];
                      $updateCartParam['type_of_product'] = $cartProduct['type_of_product'];

                      $this->_transactionTable->updateCartTableInformation($updateCartParam);
                      }
                      else {
                      $isPendingTransaction = 0;
                      }
                     */
					if(isset($postArr['pick_up']) && $postArr['pick_up'] == 1)
						$insertCartDataArr['pick_up'] = 1;
					else
						$insertCartDataArr['pick_up'] = 0;
                    if (isset($cartProduct['type_of_product']) and isset($cartProduct['item_info']) and trim($cartProduct['type_of_product']) != "" and trim($cartProduct['item_info']) != "" and trim($cartProduct['type_of_product']) == "personalizeduplicate" and trim($cartProduct['item_info']) == "1") {
                        $insertCartDataArr['item_info'] = "2";

                        $insertCartArr = $transaction->getInsertCartToTransactionArr($insertCartDataArr);
                        array_push($arrDuplicateCertificate, array('arr1' => $insertCartArr, 'cartkey' => $cartKey));
                    } else {
                        $insertCartArr = $transaction->getInsertCartToTransactionArr($insertCartDataArr);

                        $productTransactionId = $this->_transactionTable->saveCartToTransaction($insertCartArr);


                        $paymentArray[$cartKey]['table_auto_id'] = $productTransactionId;
                        $paymentArray[$cartKey]['transaction_id'] = $transactionId;
                        $arrUnshipProductTotalAmount['table_auto_id'][] = $productTransactionId;
                        if ($cartProduct['product_type_id'] == 2) { //Donation
                            if ($cartProduct['product_mapping_id'] == 5)
                                $paymentArray[$cartKey]['table_type'] = 4;
                            else
                                $paymentArray[$cartKey]['table_type'] = 2;
                        }
                        else if ($cartProduct['product_type_id'] == 4) //FOF
                            $paymentArray[$cartKey]['table_type'] = 3;
                        else if ($cartProduct['product_type_id'] == 6) { //WOH
                            if ($cartProduct['type_of_product'] == 'biocertificate')
                                $paymentArray[$cartKey]['table_type'] = 1;
                            elseif ($cartProduct['type_of_product'] == 'woh')
                                $paymentArray[$cartKey]['table_type'] = 5;
                            else
                                $paymentArray[$cartKey]['table_type'] = 4;
                        }
                        else {
                            $paymentArray[$cartKey]['table_type'] = 4; // for transaction
                        }
// }
//}
                    }
                }

                if (isset($arrDuplicateCertificate) and count($arrDuplicateCertificate) > 0) { //????????????????????????????????????????????
                    foreach ($arrDuplicateCertificate as $valDC) {
                        $productTransactionId = $this->_transactionTable->saveCartToTransaction($valDC['arr1']);
                        $paymentArray[$valDC['cartkey']]['table_auto_id'] = $productTransactionId; // for transaction
                        $paymentArray[$valDC['cartkey']]['table_type'] = 4; // for transaction
                        $paymentArray[$valDC['cartkey']]['transaction_id'] = $transactionId; // for transaction
                    }
                }
            }
//asd($paymentArray);
            if ($cartProductArr['source_type'] == 'frontend') {
                $paymentFrontArray = array();
                $paymentFrontArray['profile_id'] = $postArr['credit_card_exist'];
                $paymentFrontArray['transaction_amounts'] = $totalCartAmount;
                $paymentFrontArray['transaction_id'] = $transactionId;
                $paymentSuccessStatus = $this->capturePaymentFront($paymentArray, $paymentFrontArray, $isPromotionApplied); // by dev 3

            } else { // backend case
                $paymentFinalArray = array();
                $paymentFinalArray['profile_id'] = $postArr['profile_id'];
                $paymentFinalArray['transaction_amounts'] = $totalCartAmount;
                $paymentFinalArray['transaction_id'] = $transactionId;
                $paymentFinalArray['paymentArray'] = $this->subval_sort($paymentArray, 'payment_recieve');
                $paymentSuccessStatus = $this->capturePaymentCrm($postArr, $paymentFinalArray);
            }
            // Ticket 645
            if ((!empty($transactionId)) && $postArr['invoice_number'] != '') {
                $updateInvoiceParams = array();
                $updateInvoiceParams['transactionId'] = $transactionId;
                $updateInvoiceParams['status'] = 10;
                $this->_transactionTable->updateInvoiceAllProduct($updateInvoiceParams);

                $data = array();
                $data['modified_by'] = $crm_user_id;
                $data['modified_date'] = $dateTime;
                $data['transactionId'] = $transactionId;
                $data['shippingChannelId'] = '';
                $data['shipmentId'] = $shipmentId = $this->_transactionTable->insertShipment($data);

                $searchProductShipment = $this->_transactionTable->searchTransactionProducts(array('transactionId' => $transactionId));
                if ($searchProductShipment) {
                    foreach ($searchProductShipment as $key => $value) {
                        $shipmentPro = array();
                        $shipmentPro['productType'] = $value['item_type'];
                        $shipmentPro['productId'] = $value['id'];
                        $shipmentPro['productQty'] = $value['product_quantity'];
                        $shipmentPro['modified_by'] = $crm_user_id;
                        $shipmentPro['modified_date'] = $dateTime;
                        $shipmentPro['transactionId'] = $transactionId;
                        $shipmentPro['shipmentId'] = $shipmentId;
                        $shipmentPro['shippingChannelId'] = '2';
                        $shipmentPro['is_default'] = '0';

                        $this->getTransactionTable()->insertShipmentProduct($shipmentPro);
                    }
                }
            }


            //Ticket 1211
            if (isset($paymentSuccessStatus['status']) && $paymentSuccessStatus['status'] == 'payment_error') {
                $authorizeTransactionId = $paymentSuccessStatus['authorizeTransactionId'];
				$payment = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
                $payment->createTransactionRequest(array(
                'refId' => rand(1000000, 100000000),
                'transactionRequest' => array(
                'transactionType' => 'voidTransaction',
                'refTransId' => $authorizeTransactionId
                ),
                ));

                $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter')->getDriver()->getConnection()->rollback();
                $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->onAutoCommit();
                

                return $paymentSuccessStatus;
                exit;
            }
// if ($donationTotalAmount > 0) {
            $associationArr = array();
            $associationArr['user_id'] = $userId;
            $associationArr['association'] = '1';
            $this->getServiceLocator()->get('User\Model\UserTable')->updateUserAssociation($associationArr);
            $userPurchasedProductArr = array();
            $userPurchasedProductArr['user_id'] = $userId;

            $date_range = $this->getDateRange(4);
            $userPurchasedProductArr['from_date'] = $this->DateFormat($date_range['from'], 'db_date_format_from');
            $userPurchasedProductArr['to_date'] = $this->DateFormat($date_range['to'], 'db_date_format_to');
            $getTotalUserPurchaseProductAmountArr = $transaction->getTotalUserPurchaseProductAmountArr($userPurchasedProductArr);
            $totalAmountPurchased = $this->_transactionTable->getTotalUserPurchased($getTotalUserPurchaseProductAmountArr);
            $userMembershipArr = array();
            $userMembershipArr['transaction_amount'] = $totalAmountPurchased;
            $userMembershipArr['donation_amount'] = $donAmt;
            $userMembershipArr['user_id'] = $userId;
            $userMembershipArr['transaction_id'] = $transactionId;
            $userMembershipArr['transaction_date'] = $dateTime;

            $this->updateUserMembership($userMembershipArr);
// }

            if (isset($pledgeDonationAmount) && !empty($pledgeDonationAmount)) {
                $userPledgeArr = array();
                $userPledgeArr['userId'] = $userId;
                $userPledgeArr['transaction_id'] = $transactionId;
                $userPledgeArr['donation_amount'] = $pledgeDonationAmount;
                $this->updateUserPledgeTransaction($userPledgeArr);
            }
            $creditAmountArr = array();



            //$paymentSuccessStatus =  $response->setContent(\Zend\Json\Json::encode($paymentSuccessStatus));


            $this->getServiceLocator()->get('Zend\Db\Adapter\Adapter')->getDriver()->getConnection()->commit();
            $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->onAutoCommit();

            /* End send stock notification */
            $responseArr['status'] = 'success';
            $responseArr['transaction_id'] = $transactionId;

            /* end add cart product to transaction table */
            if ($cartProductArr['source_type'] == 'backend') {
                $postArr['billing_address_1'] = $postArr['billing_address'];
                $postArr['billing_address_2'] = '';
                $postArr['shipping_address_1'] = (!empty($postArr['shipping_address'])) ? $postArr['shipping_address'] : 'N/A';
                $postArr['shipping_address_2'] = '';
            }
            $isEmailConfirmation = !empty($postArr['is_email_confirmation']) ? $postArr['is_email_confirmation'] : '';


            if (($cartProductArr['source_type'] == 'backend' && $isEmailConfirmation != 0 && !empty($isEmailConfirmation)) || ($cartProductArr['source_type'] == 'frontend')) {
                /*  send email  */
                $email_id_template_int = 10;
                $viewLink = '/profile';
                $param['user_id'] = $userId;
                $userDataArr = $this->getServiceLocator()->get('User\Model\UserTable')->getUser(array('user_id' => $userId));

                $postArr['shipping_first_name'] = !empty($postArr['shipping_first_name']) ? $postArr['shipping_first_name'] : '';
                $postArr['shipping_last_name'] = !empty($postArr['shipping_last_name']) ? $postArr['shipping_last_name'] : '';
                $postArr['shipping_address_1'] = !empty($postArr['shipping_address_1']) ? $postArr['shipping_address_1'] : '';
                $postArr['shipping_address_2'] = !empty($postArr['shipping_address_2']) ? $postArr['shipping_address_2'] : '';
                $postArr['shipping_city'] = !empty($postArr['shipping_city']) ? $postArr['shipping_city'] : '';
                $postArr['shipping_state'] = !empty($postArr['shipping_state']) ? $postArr['shipping_state'] : '';
                $postArr['shipping_zip_code'] = !empty($postArr['shipping_zip_code']) ? $postArr['shipping_zip_code'] : '';
                $postArr['shipping_country_text'] = !empty($postArr['shipping_country_text']) ? $postArr['shipping_country_text'] : '';


                $shippingAddressDetail = '';

//if (!((empty($postArr['shipping_first_name']) || $postArr['shipping_first_name'] != 'N/A') && (empty($postArr['shipping_last_name']) || $postArr['shipping_last_name'] != 'N/A') && (empty($postArr['shipping_address_1']) || $postArr['shipping_address_1'] != 'N/A') && (empty($postArr['shipping_address_2']) || $postArr['shipping_address_2'] != 'N/A') && (empty($postArr['shipping_city']) || $postArr['shipping_city'] != 'N/A') && (empty($postArr['shipping_state']) || $postArr['shipping_state'] != 'N/A') && (empty($postArr['shipping_zip_code']) || $postArr['shipping_zip_code'] != 'N/A'))) {
                if ($isShipping == 1) {
                    $address = $postArr['shipping_address_1'];
                    $address.= (!empty($postArr['shipping_address_2'])) ? '<br>' . $postArr['shipping_address_2'] : "";
                    $shippingStreetAddress = ((!empty($postArr['shipping_title'])) ? $postArr['shipping_title'] . ' ' : '') . $postArr['shipping_first_name'] . ' ' . $postArr['shipping_last_name'] . '<br>' . $address;
                    $shippingAddressDetail = '<td style="50%">  <p style="margin: 0px; padding-left: 0pt; padding-right: 0pt; font: 20px/36px arial; color: #000;  padding-top: 20px; ">Shipping Address :<br />
        <strong style="color: #7E8183; font-weight:normal; font: 16px/30px arial;">' . $shippingStreetAddress . '</strong><br />
        <strong style="color: #7E8183; font-weight:normal; font: 16px/30px arial;">' . $postArr['shipping_city'] . ',&nbsp;' . $postArr['shipping_state'] . '</strong><br />
        <strong style="color: #7E8183; font-weight:normal; font: 16px/30px arial;">' . $postArr['shipping_country_text'] . '&nbsp;-&nbsp;' . $postArr['shipping_zip_code'] . '</strong><br />
        <strong style="color: #7E8183; font-weight:normal; font: 16px/30px arial;"></strong></p></td>';
                }

                $userDataArr = array_merge($userDataArr, $postArr);
                $userDataArr['transactionId'] = $transactionId;
                $userDataArr['transaction_date'] = $this->OutputDateFormat($dateTime, 'dateformatampm');
                $userDataArr['transaction_amount_total'] = $amount_total;

                if ($userDataArr['user_type'] != 1) {
                    $userDataArr['first_name'] = $userDataArr['company_name'];
                    $userDataArr['last_name'] = '';
                } else {
                    $userDataArr['billing_first_name'] = ((!empty($postArr['billing_title'])) ? $postArr['billing_title'] . ' ' : '') . $userDataArr['billing_first_name'];
                }
                $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                $userDataArr['shippingAddressDetail'] = $shippingAddressDetail;
                /** Order Details Template */
                $searchParam['transactionId'] = $transactionId;
                $searchResult = $this->_transactionTable->searchTransactionDetails($searchParam);
                $searchProduct = $this->_transactionTable->searchTransactionProducts($searchParam);
                foreach ($searchProduct as $key => $val) {
                    switch ($val['tra_pro']) {
                        case 'fof':
                            $fofResult = $this->_transactionTable->searchFofDetails($val);
                            array_push($searchProduct[$key], $fofResult[0]);
                            break;
                        case 'pro':
                            $passengerResult = $this->_transactionTable->searchDocPass($val);
                            array_push($searchProduct[$key], $passengerResult[0]);
                            break;
                    }
                    $proParam['transactionId'] = $searchParam['transactionId'];
                    $proParam['productId'] = $val['id'];
                    $proResult = $this->_transactionTable->searchProductsShipped($proParam);
                    if (isset($proResult[0]) && !empty($proResult[0])) {
                        $searchProduct[$key]['shippedQty'] = $proResult[0]['shippped_qty'];
                    } else {
                        $searchProduct[$key]['shippedQty'] = 0;
                    }
                }

//n-woh- start
                $arrFinalWallOfHonorId = array();
                $arrFinalWallOfHonorIdContacts = array();
//n-woh- end

                $orderDetailsArr = '<div><table width="100%" border="0" style="border-bottom:1px dotted #ccc;" cellspacing="5" cellpadding="10" class="table-bh tra-view">
                    <tr>
                    <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:left; padding-left:0;">Sr. No</th>
                    <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:left">Product</th>
                    <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:left">SKU</th>
                    <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:left">Qty.</th>
                    <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:center">Price</th>
                    <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:center">SubTotal</th></tr>';
                /* '<th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:center">Tax</th>
                  <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:center">Shipping & Handling</th>
                  <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:center">Discount</th>
                  <th style="font: 16px/20px arial; font-weight:bold; color: #000; text-align:right">Total</th>
                  </tr>'; */
                $i = 1;

                foreach ($searchProduct as $key => $val) {
                    $orderDetailsArr.= '<tr><td style="width: 48px; font: 16px/20px arial; color: #7e8183; text-align:left; vertical-align:top;">' . $i++ . '</td>
                        <td style="font: 16px/20px arial; color: #7e8183; text-align:left">';
                    switch ($val['tra_pro']) {
                        case 'don' :
                            $orderDetailsArr.=$val['product_name'] . ' <label>' . $val['option_name'] . '</label>';
                            break;
                        case 'bio' :
                            $orderDetailsArr.=$val['product_name'] . '<br> <label>' . $val['item_name'] . '</label>';
                            break;
                        case 'fof' :
                            $orderDetailsArr.=$val['product_name'] . '<br> <label>' . $val[0]['donated_by'] . '</label>';
                            break;
                        case 'pro' :
                            switch ($val['product_type']) {

                                case '5' :
                                    $orderDetailsArr.=$val['product_name'] . '<label> :</label> ' . $val['option_name'] . '<br><label>Passenger Name : ' . $val[0]['NAME'] . '</label><br/><label>Passenger Id : </label> ' . $val[0]['item_id'];
                                    break;
                                case '6' :
                                    $orderDetailsArr.=$val['product_name'] . '<br><label> ' . $val[0]['final_name'] . '</label>';
                                    break;
                                case '7' :
                                    $itemName = '';
                                    if (strlen($val[0]['item_id']) < 12)
                                        $itemName = "Ship Name : " . $val['item_name'];
                                    else
                                        $itemName = "Passenger Name : " . $val['item_name'];

                                    if (!empty($val['item_info']) && $val['item_info'] != '0000-00-00')
                                        $arrivalDate = $this->OutputDateFormat($val['item_info'], 'calender');
                                    else
                                        $arrivalDate = '';

                                    $productName = $val['product_name'];
                                    if (isset($val['manifest_info']) && !empty($val['manifest_info']))
                                        $productName.= ' : ' . $val['manifest_info'];

                                    $orderDetailsArr.=$productName . '<br>  ' . $itemName . '<br/><label>Arrival Date : </label>' . $arrivalDate;
                                    break;
                                case '8' :
                                    if (!empty($val['item_info']) && $val['item_info'] != '0000-00-00')
                                        $arrivalDate = $this->OutputDateFormat($val['item_info'], 'calender');
                                    else
                                        $arrivalDate = '';
                                    $orderDetailsArr.=$val['product_name'] . '<label> : </label>' . $val['option_name'] . '<br/><label>Ship Name : </label>' . $val['item_name'] . '<br/><label>Arrival Date : </label>' . $arrivalDate;
                                    break;
                                default :
                                    $orderDetailsArr.=$val['product_name'] . ' <label>' . $val['option_name'] . '</label>';
                                    break;
                            }
                            break;
                        case 'woh' :

//n-woh- start
                            if (isset($val['id']) and trim($val['id']) != "" and !in_array(trim($val['id']), $arrFinalWallOfHonorId)) {
                                array_push($arrFinalWallOfHonorId, trim($val['id']));
                            }
//n-woh- end
                            $orderDetailsArr.=$val['product_name'] . '<br> <label>' . utf8_decode($val['option_name']) . '</label>';
                            break;
                    }
                    $orderDetailsArr .='</td><td style="font: 16px/20px arial; color: #7e8183; text-align:left">';
                    if (isset($val['product_sku']) && !empty($val['product_sku'])) {
                        $orderDetailsArr .=$val['product_sku'];
                    } else {
                        $orderDetailsArr .='N/A';
                    }
                    $orderDetailsArr.='</td><td style="font: 16px/20px arial; color: #7e8183; text-align:left">' . $val['product_quantity'] . '</td>
                        <td style="font: 16px/20px arial; color: #7e8183; text-align:center">$' . $this->Currencyformat($val['product_price']) . '</td>
                        <td style="font: 16px/20px arial; color: #7e8183; text-align:right">$' . $this->Currencyformat($val['product_sub_total']) . '</td></tr>';
                }
                $orderDetailsArr .= '<tr>
                    <td align="right" colspan="5"><span style="text-align:right; font: 16px/28px arial; color: #7e8183;">Sub Total :</span></td>
                    <td align="right">
                    <strong style="text-align:right; font: 16px/28px arial; color: #7e8183; font-weight:bold;">$' . $this->Currencyformat($searchResult[0]['sub_total_amount']) . '
                    </strong></td>
                    </tr>
                    <tr>
                    <td align="right" colspan="5"><span style="text-align:right; font: 16px/28px arial; color: #7e8183;">Shipping & Handling :</span></td>
                    <td align="right">
                    <strong style="text-align:right; font: 16px/28px arial; color: #7e8183; font-weight:bold;">$' . $this->Currencyformat($searchResult[0]['shipping_amount'] + $searchResult[0]['handling_amount']) . '
                    </strong></td>
                    </tr>
                    <tr>
                    <td align="right" colspan="5"><span style="text-align:right; font: 16px/28px arial; color: #7e8183;">Tax :</span></td>
                    <td align="right">
                    <strong style="text-align:right; font: 16px/28px arial; color: #7e8183; font-weight:bold;">$' . $this->Currencyformat($searchResult[0]['total_tax']) . '
                    </strong></td>
                    </tr>
                    <tr>
                    <td align="right" colspan="5"><span style="text-align:right; font: 16px/28px arial; color: #7e8183;">Discount :</span></td>
                    <td align="right">
                    <strong style="text-align:right; font: 16px/28px arial; color: #7e8183; font-weight:bold;">($' . $this->Currencyformat($searchResult[0]['total_discount']) . ')
                    </strong></td>
                    </tr>
                    <tr>
                    <td align="right" colspan="5"><span style="text-align:right; font: 16px/28px arial; color: #7e8183;">Purchase Total :</span></td>
                    <td align="right">
                    <strong style="text-align:right; font: 16px/28px arial; color: #7e8183; font-weight:bold;">$' . $this->Currencyformat($searchResult[0]['transaction_amount'] - ($searchResult[0]['cancel_amount'] + $searchResult[0]['refund_amount'])) . '
                    </strong></td>
                    </tr></table>';
                $userDataArr['order_summary'] = $orderDetailsArr;
                $userDataArr['invoice_number'] = $searchResult[0]['invoice_number'];

                if(!empty($userDataArr['email_id'])) // order email
                    $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $email_id_template_int, $this->_config['email_options']);



                // Thank you mail dev 4
                if ($sendThankyouMail == 1) {

                    $userDataThankArr = array();
                    $userDataThankArr['email_id'] = $userDataArr['email_id'];
                    if ($userDataArr['user_type'] != 1) {
                        $userDataThankArr['first_name'] = $userDataArr['company_name'];
                    } else {
                        $userDataThankArr['first_name'] = $userDataArr['first_name'];
                    }

                    $userDataThankArr['amount'] = $this->Currencyformat($pledgeDonationAmount);
                    $userDataThankArr['admin_email'] = $this->_config['soleif_email']['email'];
                    $userDataThankArr['admin_name'] = $this->_config['soleif_email']['name'];
                    $email_id_template_int = '8'; // Thank you mail

                    if(!empty($userDataArr['email_id']))
                        $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataThankArr, $email_id_template_int, $this->_config['email_options']);
                }
                // End Thank you mail
//n-woh- start

                if (is_array($arrFinalWallOfHonorId) and count($arrFinalWallOfHonorId) > 0) {
                    foreach ($arrFinalWallOfHonorId as $valWoh) {
                        $arrContacts = $this->_transactionTable->getUserWohContact(array('woh_id' => $valWoh));
                        if (is_array($arrContacts) and count($arrContacts) > 0) {
                            foreach ($arrContacts as $valWoh2) {
                                if (!in_array(array("firstNameWoh" => $valWoh2['firstNameWoh'], "lastNameWoh" => $valWoh2['lastNameWoh'], "emailIdWoh" => $valWoh2['emailIdWoh']), $arrFinalWallOfHonorIdContacts)) {
                                    array_push($arrFinalWallOfHonorIdContacts, array("firstNameWoh" => $valWoh2['firstNameWoh'], "lastNameWoh" => $valWoh2['lastNameWoh'], "emailIdWoh" => $valWoh2['emailIdWoh']));
                                }
                            }
                        }
                    }
                }


                if (is_array($arrFinalWallOfHonorIdContacts) and count($arrFinalWallOfHonorIdContacts) > 0) {
                    foreach ($arrFinalWallOfHonorIdContacts as $valWoh3) {
                        if (isset($valWoh3['emailIdWoh']) and trim($valWoh3['emailIdWoh']) != "") {
                            $userDataArr['first_name'] = $valWoh3['firstNameWoh'];
                            $userDataArr['last_name'] = $valWoh3['lastNameWoh'];
                            $userDataArr['email_id'] = $valWoh3['emailIdWoh'];
                            $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $email_id_template_int, $this->_config['email_options']);
                        }
                    }
                }
//n-woh- end
            }
            /* send email end */


            /* Send stock notification */

            if (!empty($cartProducts)) {
                $stockNotificationData = array();
                $stockNotificationData = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getStockNotification();
				if (!empty($stockNotificationData)) {
                    $notificationReceiptsArr = explode(',', $stockNotificationData[0]['notification_recipients']);
                    $isEmailSend = $stockNotificationData[0]['is_email_send'];
					foreach ($cartProducts as $index=>$inventoryPro) {
						if($inventoryPro['in_ellis_stock']=='1'){
							if($postArr['pick_up']==1){
								$remainingQty = $inventoryPro['total_in_ellis_stock'] - $inventoryPro['product_qty'];
								$product_threshold = $inventoryPro['ellis_product_threshold'];
								$site_url = 'http://elliscrm.ellisisland.org';
							}else{
								$remainingQty = $inventoryPro['total_in_stock'] - $inventoryPro['product_qty'];
								$product_threshold = $inventoryPro['product_threshold'];
								$site_url = 'http://soleifcrm.ellisisland.org';
							}
							if ($isEmailSend == 1 && !empty($notificationReceiptsArr) && $remainingQty <= $product_threshold) {
								$productViewLink = $site_url . '/view-crm-product/' . $this->encrypt($inventoryPro['product_id']) . '/' . $this->encrypt('view');
								$productViewLink = '<a href="' . $productViewLink . '">' . $productViewLink . '</a>';
								$messageSubject = str_replace('[store: name]', $inventoryPro['product_name'], $stockNotificationData[0]['message_subject']);
								if($postArr['pick_up']==1)
									$messageSubject = str_replace('[uc_stock:source]', 'Ellis', $messageSubject);
								else
									$messageSubject = str_replace('[uc_stock:source]', 'Web', $messageSubject);
								$messageText = str_replace('[node:title]', $inventoryPro['product_name'], $stockNotificationData[0]['message_text']);
								$messageText = str_replace('[uc_stock:model]', $inventoryPro['product_sku'], $messageText);
								$messageText = str_replace('[uc_stock:level]', $remainingQty, $messageText);
								$messageText = str_replace('#[uc_purchase:link]', $productViewLink, $messageText);
								if($postArr['pick_up']==1)
									$messageText = str_replace('[uc_stock:source]', 'Ellis', $messageText);
								else
									$messageText = str_replace('[uc_stock:source]', 'Web', $messageText);
								$emailDataArr = array();
								$emailDataArr['subject'] = $messageSubject;
								$emailDataArr['message'] = $messageText;
								$emailDataArr['admin_email'] = $this->_config['soleif_email']['email'];
								$emailDataArr['admin_name'] = $this->_config['soleif_email']['name'];
								foreach ($notificationReceiptsArr as $receipt) {
									$emailDataArr['emailId'] = $receipt;
									if(!empty($emailDataArr['emailId']))
										$this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmailTemplate($emailDataArr, $this->_config['email_options']);
								}
							}
						}                        
                    }
                }
            }
        }
        return $responseArr;
    }

    /**
     * This action is used to update user membership
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function updateUserMembership($userMembershipArr) {
        $transaction = new Transaction($this->_adapter);
        $userMembershipUpdateArr = array();
        $userMembershipArr['transaction_amount'] = (isset($userMembershipArr['transaction_amount']) && $userMembershipArr['transaction_amount'] == 0 &&
                !empty($userMembershipArr['donation_amount'])) ? $userMembershipArr['donation_amount'] : $userMembershipArr['transaction_amount'];
        $userMembershipUpdateArr['minimun_donation_amount'] = isset($userMembershipArr['transaction_amount']) ? $userMembershipArr['transaction_amount'] : '';
        $getMatchingMembershipArr = $transaction->getMatchingMembershipArr($userMembershipUpdateArr);
        $matchingMembership = $this->_transactionTable->getMatchingMembership($getMatchingMembershipArr);
        $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $userMembershipArr['user_id']));
        //if (!empty($matchingMembership) && $matchingMembership['membership_id'] != $userDetail['membership_id']) {
        $gracePeriodStatus = false;
        $membershipExpiredStatus = false;
        $todayTime = time();
        $memberExpired = date("Y-m-d", strtotime($userDetail['membership_expire']));
        $leftTime = strtotime($memberExpired) - $todayTime;
        if ($leftTime > 0) {
            if (floor($leftTime / (60 * 60 * 24)) < $this->_config['membership_plus'])
                $gracePeriodStatus = true; //allow update
        }

        if ($gracePeriodStatus) {
            $userMembershipUpdateArr = array();
            $userMembershipUpdateArr['minimun_donation_amount'] = isset($userMembershipArr['donation_amount']) ? $userMembershipArr['donation_amount'] : '';
            $getMatchingMembershipArr = $transaction->getMatchingMembershipArr($userMembershipUpdateArr);
            $matchingMembership = $this->_transactionTable->getMatchingMembership($getMatchingMembershipArr);
        }
        if ($leftTime < 1 || ( $gracePeriodStatus == true || (!empty($matchingMembership) && $matchingMembership['membership_id'] != $userDetail['membership_id'] && ($userMembershipUpdateArr['minimun_donation_amount'] >= $userDetail['donation_amount'])) )) {
            $startDate = date("Y-m-d", strtotime(DATE_TIME_FORMAT));
            $membershipDateFrom = '';
            $membershipDateTo = '';

            if ($matchingMembership['validity_type'] == 1) {
                $membershipDateFrom = $startDate;
                $startDay = $this->_config['financial_year']['srart_day'];
                $startMonth = $this->_config['financial_year']['srart_month'];
                $startYear = date("Y", strtotime(DATE_TIME_FORMAT));
                $startDate = $startYear . "-" . $startMonth . "-" . $startDay;
//$year = 1;

                $addYear = ($matchingMembership['validity_time'] - $startYear) + 1;
                $futureDate = date('Y-m-d', strtotime('+' . $addYear . ' year -1 day', strtotime($startDate)));
                $your_date = $futureDate;
                $now = time();
                $datediff = strtotime($your_date) - $now;
                if ((isset($this->_config['membership_plus']) && !empty($this->_config['membership_plus'])) && (floor($datediff / (60 * 60 * 24)) < $this->_config['membership_plus'])) {
                    $addYear = ($matchingMembership['validity_time'] - $startYear) + 2;
                    $futureDate = date('Y-m-d', strtotime('+' . $addYear . ' year', strtotime($startDate)));
                    $futureDate = date('Y-m-d', strtotime('-1 day', strtotime($futureDate)));
                }
                $membershipDateTo = $futureDate;
            } else if ($matchingMembership['validity_type'] == 2) {
                $membershipDateFrom = $startDate;
                $futureDate = date('Y-m-d', strtotime('+' . $matchingMembership['validity_time'] . ' year -1 day', strtotime($startDate)));
                $membershipDateTo = $futureDate;
            }
            $userMembershipUpdateArr = array();
            $userMembershipUpdateArr['user_id'] = $userMembershipArr['user_id'];
            $userMembershipUpdateArr['membership_id'] = $matchingMembership['membership_id'];
            $userMembershipUpdateArr['transaction_date'] = isset($userMembershipArr['transaction_date']) ? $userMembershipArr['transaction_date'] : '';
            $userMembershipUpdateArr['transaction_amount'] = isset($userMembershipArr['transaction_amount']) ? $userMembershipArr['transaction_amount'] : '';
            $userMembershipUpdateArr['membership_date_from'] = $membershipDateFrom;
            $userMembershipUpdateArr['membership_date_to'] = $membershipDateTo;
            if (empty($membershipDateFrom) || empty($membershipDateTo)) {
                return false;
            }
            $getUpdateUserMembershipArr = $transaction->getUpdateUserMembershipArr($userMembershipUpdateArr);
            $userMembershipId = $this->_transactionTable->updateUserMembership($getUpdateUserMembershipArr);
        }
    }

    /**
     * This action is used to update user membership
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function updateUserPledgeTransaction($userPledgeArr = array()) {
        /* Update active pledge for user */
        $pledge = new Pledge($this->_adapter);
        $this->getTransactionTable();
        $postedData = array();
        $postedData['userId'] = $userPledgeArr['userId'];
        $checkUserActivePledgeArr = $pledge->getCheckUserActivePledgeArr($postedData);
        $activePledges = $this->getServiceLocator()->get('Pledge\Model\PledgeTable')->checkUserActivePledge($checkUserActivePledgeArr);
        if (!empty($activePledges)) {
            $searchParam = array();
            $pledgeId = $activePledges[0]['pledge_id'];
            $searchParam['pledge_id'] = $pledgeId;
            $pledgeDetailArr = array();
            $searchPledgeTransactionsArr = $pledge->getPledgeTransactionsSearchArr($searchParam);

            $pledgeData = $this->getServiceLocator()->get('Pledge\Model\PledgeTable')->getPledgeTransactions($searchPledgeTransactionsArr);
            if (!empty($pledgeData)) {
                $pledgeDetailArr = array();
                $pledgeDetailArr[] = $pledgeId;
                $pledgeDataResult = $this->getServiceLocator()->get('Pledge\Model\PledgeTable')->getPledgeById($pledgeDetailArr);
                $pledgeDonationAmount = $userPledgeArr['donation_amount'];
                foreach ($pledgeData as $pleData) {
                    if ($pledgeDonationAmount == 0) {
                        break;
                    }
                    if ($pleData['status'] == 0 && $pledgeDataResult['pledge_status_id'] != 3) {
                        if ($pledgeDonationAmount >= $pleData['amount_pledge']) {
                            $pledgeDonationAmount = $pledgeDonationAmount - $pleData['amount_pledge'];
                            /* Update pledge transaction status */
                            $updatePledgeStatusArray = array();
                            $updatePledgeStatusArray['pledge_transaction_id'] = $pleData['pledge_transaction_id'];
                            $updatePledgeStatusArray['status'] = '1';
                            $updatePledgeStatusArray['transaction_id'] = $userPledgeArr['transaction_id'];
                            $updatePledgeStatusArray['modify_by'] = $this->auth->getIdentity()->crm_user_id;
                            $updatePledgeStatusArray['modify_date'] = DATE_TIME_FORMAT;
                            $updatePledgeTransactionStatusArr = $pledge->updatePledgeTransactionStatusArr($updatePledgeStatusArray);
                            $pledgeTransactionId = $this->getServiceLocator()->get('Pledge\Model\PledgeTable')->updatePledgeTransactionStatus($updatePledgeTransactionStatusArr);
                            /* End update pledge transaction status */
                        } else {
                            $updatedAmountToBePaid = $pleData['amount_pledge'] - $pledgeDonationAmount;
                            $updatePledgeAmountArray = array();
                            $updatePledgeAmountArray['pledge_transaction_id'] = $pleData['pledge_transaction_id'];
                            $updatePledgeAmountArray['amount_pledge'] = $pledgeDonationAmount;
                            $updatePledgeAmountArray['remaining_amount_pledge'] = $updatedAmountToBePaid;
                            $updatePledgeAmountArray['status'] = '1';
                            $updatePledgeAmountArray['transaction_id'] = $userPledgeArr['transaction_id'];
                            $updatePledgeAmountArray['modify_by'] = $this->auth->getIdentity()->crm_user_id;
                            $updatePledgeAmountArray['modify_date'] = DATE_TIME_FORMAT;
                            $updatePledgeTransactionAmountArr = $pledge->updatePledgeTransactionAmountArr($updatePledgeAmountArray);

                            $pledgeTransactionId = $this->getServiceLocator()->get('Pledge\Model\PledgeTable')->updatePledgeTransactionAmount($updatePledgeTransactionAmountArr);
//Update pledge amount to complete
                            break;
                        }
                    }
                }
            }
        }

        /* end  Update activie pledge for user */
    }

    /**
     * This action is used to show packing slip
     * @param void
     * @return array
     * @author Icreon Tech - As
     */
    public function packingSlipAction() {
        $this->getConfig();
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = $this->_config['transaction_messages']['config']['view_crm_transaction'];
        $this->getTransactionTable();
        $viewModel = new ViewModel();
        $this->layout('popup');
        $params = $this->params()->fromRoute();
        $searchParam['shipmentId'] = $params['id'];

        $pdfPath = $this->_config['file_upload_path']['assets_upload_dir'] . "transaction/invoice_" . $this->encrypt($params['id']);
		$protocol = ($_SERVER['HTTPS'] == 'on')?'https://':'http://';
		
        $searchResult = $this->getTransactionTable()->getShipmentDetails($searchParam);
        $searchDetails = $this->getTransactionTable()->searchTransactionDetails($searchResult[0]);
        foreach ($searchResult as $key => $val) {
            $product = explode(",", $val['pro_id']);
            $product_type = explode(",", $val['pro_type']);
            for ($i = 0; $i < count($product); $i++) {
                $searchParam['id'] = $product[$i];
                $searchParam['product_type_id'] = $product_type[$i];
                switch ($searchParam['product_type_id']) {
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                        $paramResult = $this->getTransactionTable()->searchDocShip($searchParam);
                        $passengerResult[] = $paramResult[0];
                        break;
                    case '9':$paramResult = $this->getTransactionTable()->getBioDetails($searchParam);
                        $passengerResult[] = $paramResult[0];
                        break;
                    case '10':
                        $param['woh_id'] = $searchParam['id'];
                        $paramResult = $this->getServiceLocator()->get('User/model/UserTable')->getWoh($param);
                        $passengerResult[] = $paramResult[0];
                        break;
                    case '11':
                        $fofResult = $this->getTransactionTable()->searchFofDetails($searchParam);
                        $passengerResult[] = $fofResult[0];
                }
            }
        }
        if (!isset($passengerResult) && empty($passengerResult)) {
            $passengerResult[] = array();
        }
        $searchDetails = $searchDetails[0];
        $str = '<table cellpadding="0" cellspacing="0" border="0" id="print-transaction-inner" style="width:800px!important; margin-left:60px; font-family:Arial, Helvetica, sans-serif"; >

     <tr><td colspan="3" height="15"></td></tr>
 	 <tr width="100%">
        <td align="left" valign="top" width="200">
        <div>
        <img width="123" height="54" src="' . $protocol. $_SERVER['HTTP_HOST'] . '/images/pdf_logo.jpg"><br>
                   <strong>The Statue of Liberty -<br>
                            Ellis Island Foundation, Inc.</strong>';
// <barcode type="C39E" value="' . $searchDetails['transaction_id'] . '" style="width:30mm; height:7mm; color: #000000; font-size: 4mm"></barcode>

        $str = $str . '</div></td>
        <td width="100">

		</td>
        <td valign="top" width="200" >
                        17 Battery Place<br>
                        New York, NY 10007<br>
                        Email: ' . $this->_config['contact_us_mail'] . '<br>
                        Phone: 212-561-4588
           </td>
    </tr>
    <tr><td colspan="3" height="25"></td></tr>

    <tr>
        <td valign="top" width="200"><div>
                <div style="font-size:16px; color:#000; font-weight:bold; margin-bottom:5px;">Ship To:</div>
                <div>
                    <strong>' . $searchDetails['shipping_first_name'] . " " . $searchDetails['shipping_last_name'] . '</strong><br>
                    <label>' . $searchDetails['shipping_address'] . '<br />' .
                $searchDetails['shipping_city'] . ", " . $searchDetails['shipping_state'] . '<br />' .
                $searchDetails['shipping_country'] . '<br />' .
                $searchDetails['shipping_postal_code'];
        if (!empty($searchDetails['shipping_phone_no'])) {
            $str = $str . '<br />Ph. : ' . $searchDetails['shipping_phone_no'];
        }
        $str = $str . '</label>
    </div></div><br></td>
<td valign="top" width="200"><div>
<div style="font-size:16px; color:#000; font-weight:bold; margin-bottom:5px;">Bill To:</div>
<div>
<strong>' . $searchDetails['billing_first_name'] . " " . $searchDetails['billing_last_name'] . '</strong><br>
<label>' . $searchDetails['billing_address'] . '<br />' .
                $searchDetails['billing_city'] . ", " . $searchDetails['billing_state'] . '<br />' .
                $searchDetails['billing_country'] . '<br />' .
                $searchDetails['billing_postal_code'];
        if (!empty($searchDetails['billing_phone_no'])) {
            $str = $str . '<br />Ph. : ' . $searchDetails['billing_phone_no'];
        }
        $str = $str . '</label>
        </div>
        </div></td>
        <td valign="top" width="200"><div>
                <div style="font-size:16px; color:#000; font-weight:bold; margin-bottom:5px;">Transaction #&nbsp;: <span>' . $searchDetails['transaction_id'] . '</span></div>
                <div>
                    <div><strong>Shipment #:&nbsp; </strong><label>' . $searchResult[0]['shipment_id'] . '</label></div>
                        <div><barcode type="C39E" value="' . $searchResult[0]['shipment_id'] . '" style="width:40mm; height:7mm; color: #000000; font-size: 4mm"></barcode></div>';
        if (isset($searchDetails['email_id']) && !empty($searchDetails['email_id'])) {
            $str = $str . '<div><strong>E-mail:</strong><label>&nbsp;' . $searchDetails['email_id'] . '</label></div>';
// <div><barcode type="C39E" value="' . $searchDetails['email_id'] . '" style="width:50mm; height:7mm; color: #000000; font-size: 4mm"></barcode></div>';
        }
        $str = $str . '<div><strong>Date:&nbsp; </strong><label>' . $this->OutputDateFormat($searchResult[0]['shipment_date'], 'dateformatampm') . '</label></div>
                    <div><strong>Ship Type:&nbsp; </strong><label>' . $searchDetails['web_selection'] . '</label></div>';
        if ($searchDetails['invoice_number'] != '') {
            $str.= '<div><strong>Invoice Number:&nbsp; </strong><label>' . $searchDetails['invoice_number'] . '</label></div>';
        }
        $str.='</div>
            </div></td>
        </tr>
        <tr><td colspan="3" height="25"></td></tr>
        <tr><td colspan="3">
		<table width="600" border="1px" bordercolor="#999" cellspacing="0" cellpadding="10">
                    <tr>
                        <th style="padding:10px; background:#ccc; font-size:15px;">SKU</th>
                        <th style="padding:10px; background:#ccc; font-size:15px;">Status</th>
                        <th style="padding:10px; background:#ccc; font-size:15px;">Description</th>
                        <th style="padding:10px; background:#ccc; font-size:15px;">$</th>
                        <th style="padding:10px; background:#ccc; font-size:15px;">Qty</th>
                        <th style="padding:10px; background:#ccc; font-size:15px;">Total</th>
                    </tr>
                    <tr>';

        $sku = explode(',', $searchResult[0]['sku']);
        $productName = explode(',', $searchResult[0]['product_name']);
        $qty = explode(',', $searchResult[0]['qty']);
        $pro_type = explode(",", $searchResult[0]['pro_type']);
        $count = count($passengerResult);

        for ($i = 0; $i < $count; $i++) {
            if (isset($passengerResult[$i]) && !empty($passengerResult[$i])) {
                $str = $str . '<tr><td align="center" style="padding:10px;">';
                if (isset($passengerResult[$i]['product_sku']) && !empty($passengerResult[$i]['product_sku'])) {
                    $str = $str . $passengerResult[$i]['product_sku'];
                } else {
                    $str = $str . 'N/A';
                }
                $str = $str . '</td>
                <td align="center" style="padding:10px;">Ok</td>
                <td align="left" style="padding:10px;">';
                switch ($pro_type[$i]) {
                    case '1':
                        $zip = 1;
                        $str = $str . '<div><strong>Passenger Record :</strong> <label>' . $passengerResult[$i]['name'] . '</label>
                            <br><strong>Passenger Id :</strong> <label>' . $passengerResult[$i]['item_id'] . '</label></div>';
                        break;
                    case '2' :
                        $manifestName = '';
                        if (isset($passengerResult[$i]['manifest_info']) && !empty($passengerResult[$i]['manifest_info']))
                            $manifestName.= ' : ' . $passengerResult[$i]['manifest_info'];
                        $str = $str . '<div><strong>Ship Manifest</strong>' . $manifestName;
                        if (isset($passengerResult[$i]['name']) && !empty($passengerResult[$i]['name'])) {
                            $str = $str . '<br><strong>Passenger Name : </strong><label>' . $passengerResult[$i]['name'] . '</label>';
                        }
                        if (isset($passengerResult[$i]['REF_PAGE_LINE_NBR']) && !empty($passengerResult[$i]['REF_PAGE_LINE_NBR'])) {
                            $str = $str . '<br><strong>Passenger Id : </strong><label>' . $passengerResult[$i]['item_id'] . '</label>';
                        } else {
                            $str = $str . '<br><strong>Ship Name : </strong><label>' . $passengerResult[$i]['item_name'] . '</label>';
                            $str = $str . '<br><strong>Ship Id : </strong><label>' . $passengerResult[$i]['item_id'] . '</label>';
                        }
                        if (isset($passengerResult[$i]['SHIP_NAME']) && !empty($passengerResult[$i]['SHIP_NAME'])) {
                            $str = $str . '<br><strong>Ship Name : </strong><label>' . $passengerResult[$i]['SHIP_NAME'] . '</label>';
                        }
                        if (isset($passengerResult[$i]['SHIP_ID']) && !empty($passengerResult[$i]['SHIP_ID'])) {
                            $str = $str . '<br><strong>Ship Id : </strong><label>' . $passengerResult[$i]['SHIP_ID'] . '</label>';
                        }
                        if (isset($passengerResult[$i]['REF_PAGE_LINE_NBR']) && !empty($passengerResult[$i]['REF_PAGE_LINE_NBR'])) {
                            $str = $str . '<br><strong>Ref Line : </strong><label>' . $passengerResult[$i]['REF_PAGE_LINE_NBR'] . '</label>';
                        }
                        if (isset($passengerResult[$i]['DATE_ARRIVE']) && !empty($passengerResult[$i]['DATE_ARRIVE'])) {
                            $str = $str . '<br><strong>Arrival Date : </strong><label>' . $this->OutputDateFormat($passengerResult[$i]['DATE_ARRIVE'], 'calender') . '</label>';
                        } else {
                            $str = $str . '<br><strong>Arrival Date : </strong><label>' . $this->OutputDateFormat($passengerResult[$i]['item_info'], 'calender') . '</label>';
                        }

                        $str = $str . '</div>';
                        break;
                    case '3':
                        $str = $str . '<div><strong>' . $passengerResult[$i]['product_name'] . ': </strong><label>' . $passengerResult[$i]['option_name'] . '</label>
<br><strong>Ship Name : </strong><label>' . $passengerResult[$i]['NAMEWHENBUILT'] . '</label>
<br><strong>Ship Id : </strong><label>' . $passengerResult[$i]['SHIPID'] . '</label>
    <br><strong>Arrival Date : </strong><label>' . $this->OutputDateFormat($passengerResult[$i]['item_info'], 'calender') . '</label></div>';
                        break;
                    case '4' :
                        $str = $str . '<div><strong>' . $passengerResult[$i]['product_name'] . '</strong></div>';
                        break;
                    case '5':
                        $str = $str . '<div><strong>Membership</strong></div>';
                        break;
                    case '6':
                        $str = $str . '<div><strong>Kiosk Fee</strong></div>';
                        break;
                    case '7':
                        $str = $str . '<div><strong>' . $passengerResult[$i]['product_name'] . '</strong></div>';
                        break;
                    case '8':
                        $str = $str . '<div><strong>' . $passengerResult[$i]['product_name'] . '</strong></div>';
                        $str = $str . '<div>' . $passengerResult[$i]['final_name'] . '</div>';
                        $str = $str . '<div><strong>Panel # : </strong>' . $passengerResult[$i]['panel_no'] . '</div>';
                        break;
                    case '9':
                        $str = $str . '<div><strong>Bio Certificate</strong></div>';
                        break;
                    case '10':
                        $zip = 1;
                        $str = $str . '<div><strong>' . $passengerResult[$i]['product_name'] . '</strong></div>
<br><div>' . $passengerResult[$i]['final_name'] . '</div>';
                        break;
                    case '11' :
                        $str = $str . '<div><strong>Flag of Faces : </strong><label>' . $passengerResult[$i]['donated_by'] . '</label></div>';
                        break;
                }
                $str = $str . '</td>
<td align = "center" style = "padding:10px;"><span>$ ' . $this->Currencyformat($passengerResult[$i]['product_total'] / $qty[$i]) . '</span></td>
<td align = "center" style = "padding:10px;">' . $qty[$i] . '</td>
<td align = "center" style = "padding:10px;"><span >$ ' . $this->Currencyformat($passengerResult[$i]['product_total']) . '</span></td></tr>';
                $total = $total + $passengerResult[$i]['product_total'];
            }
        }
        $str = $str . '</tr></table></td></tr>
<tr><td colspan="3" height="25"></td></tr>
<tr><td colspan="3"><div><label>Purchase Total :</label><label>&nbsp;
$ ' . $this->Currencyformat($total) . '</label></div></td></tr>
<tr><td colspan="3" height="25"></td></tr>
<tr><td colspan="3"><p>
Please note, your purchase may come in separate packages and from different facilities
Thank you for your continued support. Visit us again at www.libertyellisfoundation.org
</p></td></tr>
<tr>
<td></td>
<td></td>
<td></td>
</tr>
</table>';

        try {
            $html2pdf = new\ HTML2PDF('L', 'A4', 'en');
            $html2pdf->setDefaultFont('Times');
            $html2pdf->writeHTML($str, false);
            $html2pdf->Output($pdfPath . "/" . date('Y-m-d') . "_" . $this->encrypt($params['id']) . "_invoice.pdf");
        } catch (HTML2PDF_exception $e) {
            echo $e;
            exit;
        }
    }

    function getTotalCartAction() {
        $this->checkFrontUserAuthenticationAjx();
        $this->getTransactionTable();
        $transaction = new Transaction($this->_adapter);
        $request = $this->getRequest();
        $response = $this->getResponse();
        $applicationConfigIds = $this->_config['application_config_ids'];
        $uSId = $applicationConfigIds['united_state_country_id'];
        if ($request->isPost()) {
            if (isset($this->auth->getIdentity()->user_id) and $this->auth->getIdentity()->user_id != "") {
                if ($request->getPost('ship_m') != '' && $request->getPost('status') != 'error') {
                    $paramsArray = $request->getPost()->toArray();


                    if (isset($paramsArray['is_apo_po']) && $paramsArray['is_apo_po'] == '3') {
                        $paramsArray['shipping_state'] = $paramsArray['apo_po_state'];
                        $paramsArray['shipping_country'] = $paramsArray['apo_po_shipping_country'];
                    } else if (isset($paramsArray['shipping_country']) && $paramsArray['shipping_country'] == $uSId) {
                        $paramsArray['shipping_state'] = $paramsArray['shipping_state_id'];
                        if ($paramsArray['is_apo_po'] == '2') {
                            $paramsArray['shipping_country'] = $paramsArray['apo_po_shipping_country'];
                        }
                    }
                        if ($paramsArray['pick_up'] == 1) {
                            $paramsArray['shipping_method'] = '';
                            $paramsArray['shipping_state'] = '';
                        }

                    $paramsArray['postData'] = $paramsArray;
                }
                $paramsArray['userid'] = $this->auth->getIdentity()->user_id;
                $paramsArray['shipcode'] = $request->getPost('shipcode');
                $paramsArray['couponcode'] = trim($request->getPost('couponcode'));


                // asd($paramsArray);


                $searchResult = $this->cartTotalWithTax($paramsArray);

                $response->setContent(\Zend\Json\Json::encode($searchResult));
                return $response;
            }
        }
    }

    /**
     * This Action is used to view user transaction detial
     * @author Icreon Tech-DG
     * @return Array
     * @param Array
     */
    public function getUserTransactionDetailAction() {
        $this->checkFrontUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $viewModel = new ViewModel();
        $auth = new AuthenticationService();
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $params = $this->params()->fromRoute();
        $transactionId = $this->decrypt($params['transaction_id']);
        $this->auth = $auth;
        $transactionDetail = $this->getTransactionTable()->getUserTransactions(array('transaction_id' => $transactionId));
        $transactionProduct = $this->getTransactionTable()->searchTransactionProducts(array('transactionId' => $transactionId));
        if (!empty($transactionProduct)) {
            foreach ($transactionProduct as $key => $val) {
                if ($val['product_status'] == 8) {
                    $trackDetail = $this->getTransactionTable()->getProductTrackDetail(array('transactionId' => $transactionId, 'productId' => $val['product_id']));
                    if (!empty($trackDetail)) {
                        $transactionProduct[$key]['tracking_code'] = $trackDetail[0]['tracking_code'];
                    } else {
                        $transactionProduct[$key]['tracking_code'] = null;
                    }
                } else {
                    $transactionProduct[$key]['tracking_code'] = null;
                }
            }
        }
        $viewModel->setVariables(array(
            'transactionProduct' => $transactionProduct,
            'transactionDetail' => $transactionDetail[0],
            'userId' => $this->auth->getIdentity()->user_id,
            'jsLangTranslate' => $this->_config['transaction_messages']['config']['user_transaction_list']
        ));
        return $viewModel;
    }

    /**
     * This Action is used to view user transaction detial
     * @author Icreon Tech-DG
     * @return Array
     * @param Array
     */
    public function getUserTransactionDetailCrmAction() {
//$this->checkFrontUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $viewModel = new ViewModel();
        $auth = new AuthenticationService();
//$this->layout('popup');
        $params = $this->params()->fromRoute();
        $transactionId = $this->decrypt($params['transaction_id']);
        $this->auth = $auth;
        $transactionDetail = $this->getTransactionTable()->getUserTransactions(array('transaction_id' => $transactionId));
        $transactionProduct = $this->getTransactionTable()->searchTransactionProducts(array('transactionId' => $transactionId));
        if (isset($transactionProduct) && !empty($transactionProduct)) {
            foreach ($transactionProduct as $key => $val) {
                if ($val['product_status'] == 8) {
                    $trackDetail = $this->getTransactionTable()->getProductTrackDetail(array('transactionId' => $transactionId, 'productId' => $val['product_id']));
                    if (!empty($trackDetail)) {
                        $transactionProduct[$key]['tracking_code'] = $trackDetail[0]['tracking_code'];
                    } else {
                        $transactionProduct[$key]['tracking_code'] = null;
                    }
                } else {
                    $transactionProduct[$key]['tracking_code'] = null;
                }
            }
        }
        foreach ($transactionProduct as $key => $val) {
            switch ($val['tra_pro']) {
                case 'bio':
                    $bioResult = $this->getTransactionTable()->searchBioDetails($val);
                    if (isset($bioResult) && !empty($bioResult)) {
                        array_push($transactionProduct[$key], $bioResult[0]);
                    }
                    break;
                case 'fof':
                    $fofResult = $this->getTransactionTable()->searchFofDetails($val);
                    if (isset($fofResult) && !empty($fofResult)) {
                        array_push($transactionProduct[$key], $fofResult[0]);
                    }
                    break;
                case 'pro':
                    $passengerResult = $this->getTransactionTable()->searchDocPass($val);
                    if (isset($passengerResult) && !empty($passengerResult)) {
                        array_push($transactionProduct[$key], $passengerResult[0]);
                    }
                    break;
            }
            $proParam['transactionId'] = $params['transaction_id'];
            $proParam['productId'] = $val['id'];
            $proResult = $this->getTransactionTable()->searchProductsShipped($proParam);
            if (isset($proResult[0]) && !empty($proResult[0])) {
                $searchProduct[$key]['shippedQty'] = $proResult[0]['shippped_qty'];
            } else {
                $searchProduct[$key]['shippedQty'] = 0;
            }
        }
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'transactionProduct' => $transactionProduct,
            'transactionDetail' => $transactionDetail[0],
            'jsLangTranslate' => $this->_config['transaction_messages']['config']['user_transaction_list']
        ));
        return $viewModel;
    }

    /**
     * This Action is used to view user donation receipt
     * @author Icreon Tech-DG
     * @return Array
     * @param Array
     */
    public function userDonationReceiptAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $this->getTransactionTable();
        $this->getConfig();
        $viewModel = new ViewModel();
        $this->layout('web-popup');
        $params = $this->params()->fromRoute();
        $user_id = $this->decrypt($params['user_id']);
        $transaction_id = $this->decrypt($params['transaction_id']);
        $amount = $this->decrypt($params['amount']);
        $searchParam['user_id'] = $user_id;
        $searchParam['transaction_id'] = $transaction_id;
        $get_user_info = $this->getServiceLocator()->get('Common\Model\CommonTable')->getUserCommonInfo(array('user_id' => $user_id));
        $transaction_arr = $this->getTransactionTable()->getUserTransactions($searchParam);
        $transaction_info = $transaction_arr[0];
        $getTransactionsReceipt = $this->getTransactionTable()->getTransactionReceipt(array('receipt_template_id' => 2));
        $receipt = array();

        $receipt['receipt_body'] = str_replace('{--TransactionDate--}', $this->DateFormat($transaction_info['transaction_date'], 'displayformat'), $getTransactionsReceipt['receipt_body']);
        $receipt['receipt_body'] = str_replace('{--Title--}', $get_user_info['title'], $receipt['receipt_body']);
        if ($get_user_info['user_type'] == 1) {
            $receipt['receipt_body'] = str_replace('{--FullName--}', $get_user_info['full_name'], $receipt['receipt_body']);
        } else {
            $receipt['receipt_body'] = str_replace('{--FullName--}', $get_user_info['company_name'], $receipt['receipt_body']);
        }

        $receipt['receipt_body'] = str_replace('{--Address--}', $get_user_info['street_address_one'], $receipt['receipt_body']);
        $receipt['receipt_body'] = str_replace('{--City--}', $get_user_info['city'], $receipt['receipt_body']);
        $receipt['receipt_body'] = str_replace('{--State--}', $get_user_info['state'], $receipt['receipt_body']);
        $receipt['receipt_body'] = str_replace('{--Country--}', $get_user_info['country_name'], $receipt['receipt_body']);
        $receipt['receipt_body'] = str_replace('{--ZipCode--}', $get_user_info['zip_code'], $receipt['receipt_body']);
        $receipt['receipt_body'] = str_replace('{--Amount--}', $amount, $receipt['receipt_body']);

        $viewModel->setVariables(array(
            'receipt' => $receipt,
            'jsLangTranslate' => $this->_config['transaction_messages']['config']['user_tax_receipt'])
        );
        return $viewModel;
    }

    /**
     * This Action is used to update all product status
     * @author Icreon Tech - AS
     * @return Array
     * @param Array
     */
    public function updateAllStatusAction() {
        $this->getConfig();
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = $this->_config['transaction_messages']['config']['view_crm_transaction'];
        $this->getTransactionTable();
        $params = $this->params()->fromRoute();
        $search['transactionId'] = $this->decrypt($params['id']);
        $search['status'] = $params['status'];
        $searchResult = $this->getTransactionTable()->updateAllProduct($search);
        $searchProduct = $this->getTransactionTable()->searchTransactionProducts($search);
        $transactionStatus = 0;
        $count = 0;
        $traCount = 0;
        $cancelCount = 0;
        $holdCount = 0;
        $voidedShipCount = 0;
        $returnCount = 0;
        foreach ($searchProduct as $key => $val) {
            if ($val['product_status'] == '6') {
                $cancelCount++;
            } else if ($val['product_status'] == '7') {
                $holdCount++;
            } else if ($val['product_status'] == '12') {
                $voidedShipCount++;
            } else if ($val['product_status'] == '13') {
                $returnCount++;
            } else {
                if ($transactionStatus < $val['product_status']) {
                    $traCount = 1;
                    $transactionStatus = $val['product_status'];
                } else if ($transactionStatus == $val['product_status']) {
                    $traCount++;
                }
            }
        }
        if ($cancelCount == $count) {
            $transactionStatus = '6';
        } else if ($holdCount == $count) {
            $transactionStatus = '7';
        } else if ($voidedShipCount == $count) {
            $transactionStatus = '12';
        } else if ($returnCount == $count) {
            $transactionStatus = '13';
        } else if (($count > ($traCount + $cancelCount + $voidedShipCount + $returnCount)) && ($transactionStatus == '2' || $transactionStatus == '4' || $transactionStatus == '8' || $transactionStatus == '10')) {
            $transactionStatus++;
        }
        $update['transactionStatus'] = $transactionStatus;
        $update['transactionId'] = $search['transactionId'];
        $update['modified_by'] = $this->auth->getIdentity()->crm_user_id;
        $update['modified_date'] = DATE_TIME_FORMAT;
        $this->getTransactionTable()->updateTransactionStatus($update);
        $messages = array('status' => "success");
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This Action is used to get transaction invoice
     * @author Icreon Tech - AS
     * @return Array
     * @param Array
     */
    public function transactionInvoiceAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $this->getTransactionTable();
        $this->getConfig();
        $msgArr = $this->_config['transaction_messages']['config']['view_crm_transaction'];
        $viewModel = new ViewModel();
        $this->layout('popup');
        $params = $this->params()->fromRoute();
        $searchParam['transactionId'] = $params['id'];
        $pdfPath = $this->_config['file_upload_path']['assets_upload_dir'] . "transaction/invoice_" . $this->encrypt($params['id']);

        $searchResult = $this->getTransactionTable()->searchTransactionDetails($searchParam);
        $searchProduct = $this->getTransactionTable()->searchTransactionProducts($searchParam);
        $pbCode = $this->getTransactionTable()->getTransactionTrack($searchParam);
        $trackCode = $this->getTransactionTable()->getCustomTrack($searchParam);
        foreach ($searchProduct as $key => $val) {
            switch ($val['tra_pro']) {
                case 'bio':
                    $bioResult = $this->getTransactionTable()->searchBioDetails($val);
                    array_push($searchProduct[$key], $bioResult[0]);
                    break;
                case 'fof':
                    $fofResult = $this->getTransactionTable()->searchFofDetails($val);
                    array_push($searchProduct[$key], $fofResult[0]);
                    break;
                case 'pro':
                    $passengerResult = $this->getTransactionTable()->searchDocPass($val);
                    array_push($searchProduct[$key], $passengerResult);
                    break;
            }
        }
        $searchResult = $searchResult[0];
        $transactionDetails = $searchResult;
        $transactionProduct = $searchProduct;
        if (isset($transactionDetails['full_name']) && !empty($transactionDetails['full_name'])) {
            $name = $transactionDetails['full_name'];
        } else {
            $name = $transactionDetails['company_name'];
        }
        $protocol = ($_SERVER['HTTPS'] == 'on')?'https://':'http://';
		$str = '<table cellpadding="0" cellspacing="0" border="0" id="print-transaction-inner" style="width:800px!important; margin-left:60px; font-family:Arial, Helvetica, sans-serif"; >
	<tr>
		<td colspan="3" height="15"></td>
	</tr>
	<tr>
		<td align="left" valign="top" width="200"> <img width="123" height="54" src="' . $protocol. $_SERVER['HTTP_HOST'] . '/images/pdf_logo.jpg"><br> <strong>The Statue of Liberty -<br>
      Ellis Island Foundation, Inc.</strong> <br></td>
		<td align="left" valign="top" width="100"></td>
		<td align="left" valign="top" width="200">17 Battery Place<br>
      New York, NY 10007<br>
      Email: ' . $this->_config['contact_us_mail'] . '<br>
      Phone: 212-561-4588</td>
	</tr>
		<tr>
		<td colspan="3" height="15"></td>
	</tr>
	<tr>
		<td valign="top" width="200"><strong>Billing Address :</strong><br>';
        if (isset($transactionDetails['billing_first_name']) && !empty($transactionDetails['billing_first_name'])) {
            $name = $transactionDetails['billing_first_name'] . " " . $transactionDetails['billing_last_name'];
        } else if (isset($transactionDetails['billing_company']) && !empty($transactionDetails['billing_company'])) {
            $name = $transactionDetails['billing_company'];
        }

        if (isset($name) && !empty($name)) {
            $str = $str . '<strong style="color:#595959; margin-top:5px;">';
            if (!empty($transactionDetails['billing_title'])) {
                $str = $str . $transactionDetails['billing_title'] . ' ';
            }
            $str = $str . $name . '</strong>';

            $str = $str . '<br>';
            $str = $str . '<label>' . $transactionDetails['billing_address'] . '<br />';

            if (isset($transactionDetails['billing_city']) && !empty($transactionDetails['billing_city'])) {
                $str = $str . $transactionDetails['billing_city'] . ", ";
            }
            $str = $str . $transactionDetails['billing_state'] . '
            <br /> ' . $transactionDetails['billing_country'] . '<br /> ' . $transactionDetails['billing_postal_code'];
            if (!empty($transactionDetails['billing_phone_no'])) {
                $str = $str . '<br />Ph. : ' . $transactionDetails['billing_phone_no'];
            }
        } else {
            $str = $str . '<div>N/A</div>';
        }
        $str = $str . '</label></td>
		<td valign="top" width="200"><strong>Shipping Address :</strong><br>';
        if (isset($transactionDetails['shipping_first_name']) && !empty($transactionDetails['shipping_first_name'])) {
            $name = $transactionDetails['shipping_first_name'] . " " . $transactionDetails['shipping_last_name'];
        } else if (isset($transactionDetails['shipping_company']) && !empty($transactionDetails['shipping_company'])) {
            $name = $transactionDetails['shipping_company'];
        }
        if ($transactionDetails['is_pickup'] == '1') {
            $str = $str . '<div>N/A</div>';
        } else if (isset($name) && !empty($name)) {

            $str = $str . '<strong  style="color:#595959; margin-top:5px;">';
            if (isset($transactionDetails['shipping_title']) && !empty($transactionDetails['shipping_title'])) {
                $str = $str . $transactionDetails['shipping_title'] . ' ';
            }
            $str = $str . $name . '</strong>
            <label><br>' . $transactionDetails['shipping_address'] . '<br />' .
                    $transactionDetails['shipping_city'] . ', ' . $transactionDetails['shipping_state'] . '<br />' .
                    $transactionDetails['shipping_country'] . '<br />' . $transactionDetails['shipping_postal_code'];
            if (!empty($transactionDetails['shipping_phone_no'])) {
                $str = $str . '<br />Ph. : ' . $transactionDetails['shipping_phone_no'];
            }
            $str = $str . '</label>';
        } else {
            $str = $str . '<div >N/A</div>';
        }
        $str = $str . '</td>
		<td valign="top" width="200"> Contact Name:&nbsp;' . $name . '<br>
      E-mail:&nbsp;' . $transactionDetails['email_id'] . '<br>
      Contact Id:&nbsp;' . $transactionDetails['contact_id'] . '<br>';
        if (isset($transactionDetails['web_selection']) && trim($transactionDetails['web_selection']) != "") {
            $str = $str . 'Ship Type:&nbsp;' . $transactionDetails['web_selection'] . '<br>';
        }
        if (isset($transactionDetails['email_id']) && !empty($transactionDetails['email_id'])) {
            // $str = $str . '<br><barcode type="C39E" value="' . $transactionDetails['email_id'] . '" style="width:30mm; height:7mm; color: #000000; font-size: 4mm"></barcode>';
        }
        $str = $str . '</td>
	</tr>
        <tr>
		<td colspan="3" height="15"></td>
	</tr>
	<tr>
		<td colspan="3"><strong>Payment</strong><br>
                <label>Total : &nbsp;</label>
                <label>$ ' . $this->Currencyformat($transactionDetails['transaction_amount']) . '&nbsp;&nbsp;</label>
                <label>Method : &nbsp;</label>
                <label>' . $transactionDetails['payment_mode'] . '</label>';
        if ($transactionDetails['invoice_number'] != '') {
            $str = $str . '<label>&nbsp;&nbsp;Invoice No. : &nbsp;</label>
                <label>' . $transactionDetails['invoice_number'] . '</label>';
        }

        $str = $str . '</td>

	</tr>
        <tr>
		<td colspan="3" height="15"></td>
	</tr>
	<tr>
            <td colspan="3"><strong>Transaction Summary</strong></td>
        </tr>
	<tr>
	<td colspan="3">

<table border="1" width="550" bordercolor="#999" cellspacing="0" cellpadding="0" style="margin-top:5px;">
                        <tr>
                            <th style="padding:10px; background:#ccc;">Transaction #</th>
                            <th style="padding:10px; background:#ccc;">Purchase Date</th>
                            <th style="padding:10px; background:#ccc;">Product SubTotal</th>
                            <th style="padding:10px; background:#ccc;">Discount</th>
                            <th style="padding:10px; background:#ccc;">Shipping & Handling</th>
                            <th style="padding:10px; background:#ccc;">Global Tax</th>
                            <th style="padding:10px; background:#ccc;">Total for this transaction</th>
                        </tr>
                        <tr>
                            <td style="padding:10px; color:#595959">' . $transactionDetails['transaction_id'];
        if (isset($transactionDetails['omx_order_number']) && !empty($transactionDetails['omx_order_number'])) {
            $str = $str . '(OMX Id: ' . $transactionDetails['omx_order_number'] . ')';
        }
        $str = $str . '</td>
                            <td style="padding:10px; color:#595959">' . $this->OutputDateFormat($transactionDetails['transaction_date'], 'dateformatampm') . '</td>
                            <td style="padding:10px; color:#595959">$&nbsp;' . $this->Currencyformat($transactionDetails['sub_total_amount']) . '</td>
                            <td style="padding:10px; color:#595959">($&nbsp;' . $this->Currencyformat($transactionDetails['total_discount']) . ')</td>
                            <td style="padding:10px; color:#595959">$&nbsp;' . $this->Currencyformat($transactionDetails['shipping_amount'] + $transactionDetails['handling_amount']) . '</td>
                            <td style="padding:10px; color:#595959">$&nbsp;' . $this->Currencyformat($transactionDetails['total_tax']) . '</td>
                            <td style="padding:10px; color:#595959">$&nbsp;' . $this->Currencyformat($transactionDetails['transaction_amount'] - ($transactionDetails['cancel_amount'] + $transactionDetails['refund_amount'])) . '</td>
                        </tr>
</table></td>
</tr>
<tr>
		<td colspan="3" height="15"></td>
	</tr>
                <tr>
            <td valign="top" colspan="2"><strong>Shipment #:</strong></td>

	</tr>';

        $shipId = array_merge($pbCode, $trackCode);

        foreach ($shipId as $key) {
            $arr[] = $key[shipment_id];
        }
        $arr = array_unique($arr);
        if (isset($arr) && !empty($arr)) {
            foreach ($arr as $key) {
                if (isset($key) && !empty($key) && trim($key) != "") {
                    $str = $str . '<tr><td><label>' . $key . ' </label></td>
                           <td colspan="2"> <barcode type="C39E" value="' . $key . '" style="width:30mm; height:7mm; color: #000000; font-size: 4mm"></barcode></td></tr>';
                }
            }
        }
        $str = $str . '
        <tr>
		<td colspan="3" height="15"></td>
	</tr>
		 <tr>
                <td colspan="3"><strong >Products on Transaction</strong></td>
            </tr>
				 <tr>
                <td colspan="3"><table border="1" width="550" bordercolor="#999" cellspacing="0" cellpadding="0" style="margin-top:5px;">
                        <tr>
                            <th style="padding:10px; background:#ccc;">Qty</th>
                            <th style="padding:10px; background:#ccc;">Product</th>
                            <th style="padding:10px; background:#ccc;">SKU</th>
                            <th style="padding:10px; background:#ccc;">Product Price</th>
                            <th style="padding:10px; background:#ccc;">Total</th>
                        </tr>';
        foreach ($transactionProduct as $key) {
            $str = $str . '<tr >
                            <td style="padding:10px; color:#595959">' . $key['product_quantity'] . '</td>
                            <td style="padding:10px; color:#595959">';
            switch ($key['tra_pro']) {
                case 'don' :
                    $str = $str . '<div> <strong>' . $key['product_name'] . '</strong> ' . $key['option_name'] . ' </div>';
                    break;
                case 'bio' :
                    $str = $str . '<div><strong>' . $key['product_name'] . '</strong>' . $key['option_name'];
                    if (isset($key[0]) && !empty($key[0])) {
                        $str = $str . $key[0]['final_name'];
                    }
                    $str = $str . ' </div>';
                    break;
                case 'fof' :
                    $str = $str . '<div><strong>' . $key['product_name'] . ' : ' . $key[0]['donated_by'] . '</strong> </div>';
                    break;
                case 'pro' :
                    switch ($key['product_type']) {
                        case '5' :
                            $str = $str . '<div> <strong>' . $key['product_name'] . " : </strong>" . $key['option_name'];
                            if (isset($key['item_name']) && !empty($key['item_name'])) {
                                $str = $str . "<br><strong>Passenger Name : </strong>" . $key['item_name'];
                            }
                            if (isset($key['item_id']) && !empty($key['item_id'])) {
                                $str = $str . "<br><strong>Passenger Id : </strong>" . $key['item_id'];
                            }
                            $str = $str . '</div>';
                            break;
                        case '7' :
                            $str = $str . '<div> <strong>';
                            $productName = $key['product_name'];
                            if (isset($key['manifest_info']) && !empty($key['manifest_info']))
                                $productName.= ' : </strong>' . $key['manifest_info'] . "<br>";
                            $str = $str . $productName;

                            if (!empty($key['item_info']) && $key['item_info'] != '0000-00-00')
                                $arrivalDate = $this->OutputDateFormat($key['item_info'], 'calender');
                            else
                                $arrivalDate = '';

                            if (isset($key['item_name']) && !empty($key['item_name'])) {
                                if (isset($key[0][0]['REF_PAGE_LINE_NBR']) && !empty($key[0][0]['REF_PAGE_LINE_NBR'])) {
                                    $str = $str . "<strong>Passenger Name :</strong> " . $key['item_name'] . "<br>";
                                    $str = $str . "<strong>Passenger Id :</strong> " . $key['item_id'] . "<br>";
                                } else {
                                    $str = $str . "<strong>Ship Name : </strong>" . $key['item_name'] . "<br>";
                                }
                            }
                            if (isset($key[0][0]['REF_PAGE_LINE_NBR']) && !empty($key[0][0]['REF_PAGE_LINE_NBR'])) {
                                $str = $str . "<strong>Ref Line : </strong> <label>" . $key[0][0]['REF_PAGE_LINE_NBR'] . "</label><br>";
                            }
                            if (isset($key[0][0]['SHIP_NAME']) && !empty($key[0][0]['SHIP_NAME'])) {
                                $str = $str . "<strong>Ship Name :</strong> <label>" . $key[0][0]['SHIP_NAME'] . "</label><br>";
                            }
                            if (!isset($key[0][0]['REF_PAGE_LINE_NBR']) || empty($key[0][0]['REF_PAGE_LINE_NBR'])) {
                                $str = $str . "<strong>Ship Id :</strong> <label>" . $key['item_id'] . "</label><br>";
                            }
                            if (isset($key[0][0]['SHIP_ID']) && !empty($key[0][0]['SHIP_ID'])) {
                                $str = $str . "<strong>Ship Id : </strong><label>" . $key[0][0]['SHIP_ID'] . "</label><br>";
                            }
                            $str = $str . "<strong>Arrival Date : </strong>" . $arrivalDate;
                            $str = $str . '</div>';
                            break;
                        case '8' :
                            $str = $str . '<div><strong>' . $key['product_name'];
                            if (isset($key['option_name']) && !empty($key['option_name'])) {
                                $str = $str . "&nbsp;:</strong>&nbsp;" . $key['option_name'] . "";
                            }
                            $str = $str . "<br>";
                            if (!empty($key['item_info']) && $key['item_info'] != '0000-00-00')
                                $arrivalDate = $this->OutputDateFormat($key['item_info'], 'calender');
                            else
                                $arrivalDate = '';

                            if (isset($key['item_name']) && !empty($key['item_name'])) {
                                $str = $str . "<strong>Ship Name :</strong> " . $key['item_name'] . "<br>";
                            }
                            if (isset($key['item_id']) && !empty($key['item_id'])) {
                                $str = $str . "<strong>Ship Id :</strong> " . $key['item_id'] . "<br>";
                            }
                            if ($arrivalDate != '') {
                                $str = $str . "<strong>Arrival Date : </strong>" . $arrivalDate . "<br>";
                            }
                            $str = $str . '</div>';
                            break;
                        default :
                            if ($key['item_type'] == '7' || $key['item_type'] == '8') {
                                $str = $str . '<div><strong>' . $key['product_name'] . '</strong><br>';
                                if (isset($key[0][0]['final_name']) && !empty($key[0][0]['final_name'])) {
                                    $str = $str . $key[0][0]['final_name'] . "<br>";
                                }
                                if (isset($key[0][0]['panel_no']) && !empty($key[0][0]['panel_no'])) {
                                    $str = $str . "<strong>Panel # : </strong>" . $key[0][0]['panel_no'];
                                }
                                $str = $str . '</div>';
                            } else {
                                $str = $str . '<div> <strong>' . $key['product_name'] . '</strong>' . $key['option_name'] . '</div>';
                            }
                            break;
                    }
                    break;
                case 'woh' :
                    $str = $str . '<div> <strong>' . $key['product_name'] . '</strong> ' . $key['option_name'] . ' </div>';
                    break;
            }
            $str = $str . '</td>
                <td style="padding:10px; color:#595959">' . $key['product_sku'] . '</td>
                <td style="padding:10px; color:#595959">$  ' . $key['product_price'] . '</td>
                    <td style="padding:10px; color:#595959">';
            if ($key['product_status'] == '6' || $key['product_status'] == '13') {
                $str = $str . "($ " . $this->Currencyformat($key['product_total']) . ")";
            } else {
                $str = $str . "$ " . $this->Currencyformat($key['product_total']);
            }
            $str = $str . '</td></tr>';
        }
        $str = $str . '</table></td>
            </tr>
            <tr>
		<td colspan="3" height="15"></td>
	</tr>
			<tr>
                <td colspan="3"><strong>Tracking Number</strong></td>
            </tr>
			<tr>
                <td colspan="3"><div>';

        if (isset($pbCode) && !empty($pbCode)) {
            foreach ($pbCode as $key) {
                if (isset($key['tracking_code']) and !empty($key['tracking_code']) and trim($key['tracking_code']) != "") {
                    $str = $str . '<div><label>' . $key['tracking_code'] . ' </label></div>';
                }
            }
        }
        if (isset($trackCode) && !empty($trackCode)) {
            foreach ($trackCode as $key) {
                if (isset($key['tracking_code']) && !empty($key['tracking_code']) && $key['tracking_code'] != '0') {
                    $str = $str . '<div>
                            <label>' . $key['tracking_code'] . ' </label></div>';
                }
            }
        }
        $str = $str . '</div></td>
            </tr>




</table>';

        try {
            $html2pdf = new\ HTML2PDF('L', 'A4', 'en');
            $html2pdf->setDefaultFont('Times');
            $html2pdf->writeHTML($str, false);
            $html2pdf->Output($pdfPath . "/" . date('Y-m-d') . "_" . $this->encrypt($params['id']) . "_invoice.pdf");
        } catch (HTML2PDF_exception $e) {
            echo $e;
            exit;
        }
    }

    function placeOrderAction() {
        $this->checkFrontUserAuthentication();
        $this->getTransactionTable();
        $msgArr = array_merge($this->_config['transaction_messages']['config']['checkout_front'], $this->_config['transaction_messages']['config']['create_crm_transaction']);
        $transaction = new Transaction($this->_adapter);
        $inventoryForm = new InventoryForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $applicationConfigIds = $this->_config['application_config_ids'];
        $uSId = $applicationConfigIds['united_state_country_id'];
        if ($request->isPost()) {

            $addressInformationId = 0;
            $creditCardNo = '';
            $postData = $request->getPost()->toArray();

            if (isset($postData['credit_card_exist']) && trim($postData['credit_card_exist']) != '') {
                $tokenDetailsResult = $this->getServiceLocator()->get('User\Model\UserTable')->getTokenDetails(array('profile_id' => $postData['credit_card_exist']));
                if (count($tokenDetailsResult) > 0 && intval($tokenDetailsResult[0]['address_information_id']) > 0) {
                    $addressInformationId = $tokenDetailsResult[0]['address_information_id'];
                    $creditCardNo = $tokenDetailsResult[0]['credit_card_no'];
                }
            }
            if (!empty($postData['billing_address_information_id'])) {
                if ($addressInformationId != $postData['billing_address_information_id']) {
                    $tokenDetailsResult = $this->getServiceLocator()->get('User\Model\UserTable')->updatePaymentProfileAddressInfo(array('profile_id' => $postData['credit_card_exist'], 'address_id' => $postData['billing_address_information_id']));
                }
            }
            // If billing addres is not selected, Shipping address will be the billing address
            if (empty($postData['billing_address_information_id'])) {
                $postData['billing_title'] = $postData['shipping_title'];
                $postData['billing_first_name'] = $postData['shipping_first_name'];
                $postData['billing_last_name'] = $postData['shipping_last_name'];
                $postData['billing_address_1'] = $postData['shipping_address_1'];
                $postData['billing_address_2'] = $postData['shipping_address_2'];
                $postData['billing_country'] = $postData['shipping_country'];
                $postData['billing_country_text'] = $postData['shipping_country_text'];
                $postData['billing_state'] = $postData['shipping_state'];
                $postData['billing_state_id'] = $postData['shipping_state_id'];
                $postData['billing_city'] = $postData['shipping_city'];
                $postData['billing_zip_code'] = $postData['shipping_zip_code'];
                $postData['billing_phone_info_id'] = $postData['shipping_phone_info_id'];
                $postData['billing_country_code'] = $postData['shipping_country_code'];
                $postData['billing_area_code'] = $postData['shipping_area_code'];
                $postData['billing_phone'] = $postData['shipping_phone'];
                $postData['billing_company'] = $postData['shipping_company'];
            }
            if ($creditCardNo != '') {
                $this->updateProfile($postData, $creditCardNo);
            }
            // End If billing addres is not selected, Shipping address will be the billing address
            //asd($postData);

            $arrPostData = $postData;
//asd($arrPostData);
            $shipcode = isset($postData['shipping_zip_code']) ? $postData['shipping_zip_code'] : '';
            $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '';
            $cartParam['user_id'] = $user_id;
            $cartParam['source_type'] = 'frontend';
            //$cartData = $this->getTransactionTable()->getCartProducts($cartParam);
            //$returnrray = array();

            /* foreach ($cartData as $cartProducts) {
              $productType = $cartProducts['product_type_id'];
              $productId = $cartProducts['product_id'];
              $productQuantity = $cartProducts['product_qty'];
              if ($productType == '1') {
              $detail = $this->_productTable->getProductInfo(array('id' => $productId));
              $total_in_stock = $detail[0]['total_in_stock'];
              if ($productQuantity > $total_in_stock) {
              $returnrray[$productId] = array('message' => sprintf($msgArr['QUANTITY_EXCEED'], $total_in_stock, $detail[0]['product_name']), 'product_name' => $detail[0]['product_name']);
              }
              }

              if ($productType == '4') {
              $imageNameF = isset($cartProducts['image_name']) ? $cartProducts['image_name'] : '';
              if (isset($imageNameF) and trim($imageNameF) != "") {
              $this->moveFileFromTempToFOFFrontEnd(trim($imageNameF));
              }
              }
              }

              if (!empty($returnrray)) {
              $messages = array('status' => "error", "info" => $returnrray);
              $response->setContent(\Zend\Json\Json::encode($messages));
              return $response;
              } */


            $paramArray['postData'] = $arrPostData;
            if (isset($arrPostData['shipping_method']) && !empty($arrPostData['shipping_method']) && (empty($arrPostData['pick_up']) || $arrPostData['pick_up'] == 0))
                $paramArray['shipping_method'] = $arrPostData['shipping_method'];
            else
                $paramArray['shipping_method'] = '';
            $paramArray['source_type'] = 'frontend';
            $paramArray['userid'] = $user_id;
            $paramArray['shipcode'] = $shipcode;
            $paramArray['couponcode'] = !empty($postData['couponcode']) ? $postData['couponcode'] : '';
            //asd($paramArray,2);
            //$paramArray['shipping_method'] = (!empty($postData['shipping_method']) && (empty($postData['pick_up']) || $postData['pick_up'] == 0)) ? $postData['shipping_method'] : '';


            if ($arrPostData['pick_up'] == 1) {
                 $paramArray['shipping_state'] = '';
			}
            $itemsTotal = $this->cartTotalWithTax($paramArray);

            /* $promotionTable = $this->getServiceLocator()->get('Promotions\Model\PromotionTable');
              $promotionId = '';
              if ($paramArray['couponcode'] != '') {
              $getPromotionDetail = $promotionTable->getPromotionDetailByCode($paramArray['couponcode']);
              if (!empty($getPromotionDetail)) {

              $promotionId = $getPromotionDetail['promotion_id'];
              }
              } */
        }

        $postData['promotion_id'] = $promotionId;
        $postData['source_type'] = 'frontend';
        $postData['source_all'] = 'all';
        //  asd($postData);

        $postData['pick_up'] = isset($postData['pick_up']) ? $postData['pick_up'] : '0';

        if (isset($postData['cash_payment']) && $postData['cash_payment'] == '1') {
            $postData['payment_mode_cash'] = 1;
            $postData['payment_mode_cash_amount'] = $itemsTotal['finaltotal'];
        } else {
            $postData['payment_mode_credit'] = 1;
            $postData['payment_mode_credit_card_amount'] = $itemsTotal['finaltotal'];
        }

//$this->makeAuthorizePayment()


        $paymentInfo = array();
        $paymentInfo['card_number'] = $postData['credit_card_number'];
        $paymentInfo['expiry_year'] = $postData['credit_card_exp_year'];
        $paymentInfo['expiry_month'] = $postData['credit_card_exp_month'];
        $paymentInfo['cvv_number'] = $postData['credit_card_code'];
        $paymentInfo['billing_first_name'] = $postData['billing_first_name'];
        $paymentInfo['billing_last_name'] = $postData['billing_last_name'];
        $paymentInfo['address'] = $postData['billing_address_1'] . " " . $postData['billing_address_2'];
        $paymentInfo['city'] = $postData['billing_city'];
        $paymentInfo['state'] = $postData['billing_state'];
        $paymentInfo['zip_code'] = $postData['billing_zip_code'];
        $paymentInfo['country_name'] = $postData['billing_country_text'];
        $paymentInfo['user_id'] = $user_id;
        $paymentInfo['amount'] = $itemsTotal['finaltotal'];
        $paymentInfo['isSaveProfile'] = @$postData['save_credit_card'];
        $paymentInfo['existing_profile_id'] = !empty($postData['credit_card_exist']) ? $postData['credit_card_exist'] : '';
        // $responseMessage = $this->makeAuthorizePayment($paymentInfo);
        //if (trim($responseMessage['status']) == 'credit_card_error') {
        //    $response->setContent(\Zend\Json\Json::encode($responseMessage));
        //    return $response;
        //} else {
        //$postData['transaction_ids'] = $responseMessage['transaction_id'];
        $postData['transaction_ids'] = 'SK' . time();
        $postData['transaction_amounts'] = $paymentInfo['amount'];
        //$postData['customer_profile_ids'] = $responseMessage['customerProfileId'];
        $postData['customer_profile_ids'] = $postData['credit_card_exist']; //$responseMessage['customerProfileId'];
        $postData['coupon_code'] = $paramArray['couponcode'];

        if (isset($postData['is_apo_po']) && $postData['is_apo_po'] == '3') {
            $postData['shipping_state'] = $postData['apo_po_state'];
            $postData['shipping_country'] = $postData['apo_po_shipping_country'];
        } else if (isset($postData['shipping_country']) && $postData['shipping_country'] == $uSId) {
            $postData['shipping_state'] = $postData['shipping_state_id'];
            if ($postData['is_apo_po'] == '2') {
                $postData['shipping_country'] = $postData['apo_po_shipping_country'];
            }
        }

		$transactionId = '';
        if (isset($postData['cash_payment']) && $postData['cash_payment'] == '1') {

            $res = $this->saveCashFinalTransaction($postData);
            $transactionId = $res['transaction_id'];
            $paymentMethod = "cash";

        } else {
            $res = $this->saveFinalTransaction($postData);
            $transactionId = $res['transaction_id'];
            $paymentMethod = "credit_card";
        }

        if (isset($res['status']) && $res['status'] == 'payment_error') {

            return $response->setContent(\Zend\Json\Json::encode($res));
            exit;
        }
        if (isset($res['quantity_error']) && $res['quantity_error'] == '1') {

            return $response->setContent(\Zend\Json\Json::encode($res));
            exit;
        }
            $contactDetail = array();
			$userArr = array('user_id' => $user_id);
			$userEmailId = '';
            $contactDetail = $this->getServiceLocator()->get('User\Model\UserTable')->getUser($userArr);

            if(!empty($contactDetail))
                $userEmailId = $contactDetail['email_id'];

        $passengerSavedSearch = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getUserPassengerSearch(array('user_id' => $user_id));

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => array_merge($msgArr, $this->_config['transaction_messages']['config']['common_message']),
            'postdata' => $postData,
            'userEmailId'=>$userEmailId,
            'finalTotal' =>$itemsTotal['finaltotal'],
            'passengerSavedSearch' => $passengerSavedSearch,
            'transactionId'=>$transactionId,
            'paymentMethod'=>$paymentMethod
        ));
        return $viewModel;
        //}
    }

    /**
     * This action is used to add fof to cart
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function addToCartPassengerDocumentFrontAction() {
//$this->checkUserAuthentication();
        $this->checkFrontUserAuthentication();
        $this->getTransactionTable();
        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $transaction = new Transaction($this->_adapter);

        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {

            $sm = $this->getServiceLocator();
            $passengerTable = $sm->get('Passenger\Model\PassengerTable');
            $postArr = $request->getPost()->toArray();

            $passengerDocumentFormArr = $postArr;

            /* Add passenger Record */
            $isPassengerRecord = $request->getPost('is_passenger_record');
            $passengerRecordQty = $request->getPost('passenger_record_qty');
            if ($isPassengerRecord != '' && $isPassengerRecord != 0 && $passengerRecordQty != '' && $passengerRecordQty > 0) {
                $sm = $this->getServiceLocator();
                $passengerTable = $sm->get('Passenger\Model\PassengerTable');
                $productParam = array();
                $productParam['productType'] = '5';/** product */
                $productResult = array();
                $productResult = $passengerTable->getProductAttributeDetails($productParam);
                $paramsArr = array();
                $paramsArr['product_type_id'] = 5;
                $productTypeId = $productParam['productType'];
                $paramsArr['productId'] = $productResult[0]['product_id'];
                $paramsArr['item_id'] = $passengerDocumentFormArr['passenger_id'];
                $paramsArr['product_attribute_option_id'] = $postArr['attribute_option_id'];
                $paramsArr['item_type'] = '1';
                $paramsArr['user_id'] = $passengerDocumentFormArr['user_id'];
                $paramsArr['num_quantity'] = $passengerRecordQty;
                $paramsArr['item_name'] = $passengerDocumentFormArr['p_passenger_name'];
                $paramsArr['item_info'] = $passengerDocumentFormArr['p_date_arrive'];
//$paramsArr['item_price'] = $productResult[0]['product_sell_price'] + $productResult[0]['option_price'];

                $addToCartProductId = $this->addInventoryProductToCart($paramsArr);
            }

            /* End add passenger Record */

            /* Add passenger Ship Record */
            $isPassengerShip = $request->getPost('is_ship_image');
            $shipPassengerQty = $request->getPost('ship_image_qty');
            $attributeOptionIds = $request->getPost('attribute_option_id');
            if ($isPassengerShip != '' && $isPassengerShip != 0 && $shipPassengerQty != '' && $shipPassengerQty > 0 && count($attributeOptionIds) > 0) {

                $productParam = array();
                $productParam['productType'] = '8';/** product */
                $productResult = array();
                $productResult = $passengerTable->getProductAttributeDetails($productParam);
                foreach ($productResult as $attribute) {
                    $isExistArr = array_keys($attributeOptionIds, $attribute['attribute_option_id']);
                    if (!empty($isExistArr)) {
//foreach ($attributeOptionIds as $attribute) {
                        $paramsArr = array();
                        $paramsArr['product_type_id'] = 8;
                        $productTypeId = $paramsArr['product_type_id'];
                        $paramsArr['productId'] = $attribute['product_id'];
                        $paramsArr['product_attribute_option_id'] = $attribute['attribute_option_id'];
                        $paramsArr['item_id'] = $passengerDocumentFormArr['passenger_id'];
                        $paramsArr['item_type'] = '3';
                        $paramsArr['user_id'] = $passengerDocumentFormArr['user_id'];
                        $paramsArr['item_name'] = $passengerDocumentFormArr['ship_name'];
                        $paramsArr['item_info'] = $passengerDocumentFormArr['ship_arrival_date'];
                        $paramsArr['num_quantity'] = $shipPassengerQty;
//$paramsArr['item_price'] = $attribute['option_price'] + $productResult[0]['additional_option_price'];
                        $addToCartProductId = $this->addInventoryProductToCart($paramsArr);
                    }
                }
            }
            /* End passenger Ship Record */

            /* Add related Product */
            $isRelatedProduct = $request->getPost('is_related_product');
            $relatedProductIds = $request->getPost('related_product_name_ids');
            if ($isRelatedProduct != 0 && !empty($isRelatedProduct) && !empty($relatedProductIds)) {
                $relatedProductIds = explode(', ', $relatedProductIds);
                foreach ($relatedProductIds as $relatedProductId) {
                    $paramsArr = array();
                    $paramsArr['productId'] = $relatedProductId;
                    $paramsArr['user_id'] = $postArr['user_id'];
                    $this->addInventoryProductToCart($paramsArr);
                }
            }
            /* End Add related Product */

            /* Manifest code */

            $isManifest = $request->getPost('is_manifest');
            if ($isManifest != 0 && !empty($isManifest)) {
                $flag = $request->getPost('flag');

                //$isFirstImage = $request->getPost('is_first_image');
                //$isSecondImage = $request->getPost('is_second_image');

                $maifestImageArr = $request->getPost('manifest_images');
                $isFirstImage = $maifestImageArr[0];
                if (isset($maifestImageArr[1]) && !empty($maifestImageArr[1]))
                    $isSecondImage = $maifestImageArr[1];

                $nArr[] = $isFirstImage;
                $nArr[] = $isSecondImage;

                $additionalImage = $request->getPost('additional_images');

                $additionalImage = array_diff($additionalImage, $nArr);

                $passengerId = $request->getPost('passenger_id');
                $firstImageName = '';
                $secondImageName = '';
                $additionalImageName = '';
                $firstImagePrice = 0;
                $secondImagePrice = 0;
                $additionalImagePrice = 0;
                if ($flag == 'p') {
                    $searchParam['passenger_id'] = $passengerId;
                    $manifestResult = array();
                    $manifestResult = $passengerTable->getPassengerManifest($searchParam);
                }
                if (!empty($isFirstImage) && !empty($isSecondImage)) {
                    $firstImageName = $isFirstImage;
                    $secondImageName = $isSecondImage;
                } else if (empty($isFirstImage)) {
                    $firstImageName = $isSecondImage;
                    $isFirstImage = $isSecondImage;
                    $isSecondImage = '';
                } else if (!empty($isFirstImage)) {
                    $firstImageName = $isFirstImage;
                }

                if (!empty($additionalImage)) {
                    $additionalImageName = implode(', ', $additionalImage);
                }
                if (!empty($firstImageName) || !empty($secondImageName) || !empty($additionalImageName)) {
                    $productMenifestParam = array();
                    $productMenifestParam['productType'] = '7';
                    $productMenifestResult = $passengerTable->getProductAttributeDetails($productMenifestParam);

                    $firstImageOptionId = '';
                    $secondImageOptionId = '';
                    $priceArr = array();
                    if (!empty($productMenifestResult)) {
                        $i = 1;
                        foreach ($productMenifestResult as $productOptions) {
                            $price = $productOptions['product_sell_price'] + $productOptions['additional_option_price'];
                            $priceArr[$productOptions['attribute_option_id']] = $price;
                            $i++;
                        }
                    }
                    arsort($priceArr);
                    $secondImagePrice = 0;
                    $i = 1;
                    if (!empty($priceArr)) {
                        foreach ($priceArr as $optionId => $price) {
                            if ($optionId == 3) {
                                $firstImagePrice = $price;
                                $firstImageOptionId = $optionId;
                            } else if ($optionId == 4) {
                                $secondImagePrice = $price;
                                $secondImageOptionId = $optionId;
                            }
                            $i++;
                        }
                    }

                    /* $firstImagePrice = (!empty($priceArr[0])) ? $priceArr[0] : 0;
                      $additionalImagePrice = (!empty($priceArr[0])) ? $priceArr[0] : 0;
                      $secondImagePrice = (!empty($priceArr[1])) ? $priceArr[1] : 0;
                     */


                    $firstImagePrice = (!empty($priceArr[3])) ? $priceArr[3] : 0;
                    $additionalImagePrice = (!empty($priceArr[3])) ? $priceArr[3] : 0;

                    if ($request->getPost('secondImagePrice') != $request->getPost('firstImagePrice')) {
                        $secondImagePrice = (!empty($priceArr[4])) ? $priceArr[4] : 0;
                        $manifestType = 2;
                    } else {
                        $secondImagePrice = $request->getPost('firstImagePrice');
                        $secondImageOptionId = $firstImageOptionId;
                        $manifestType = 1;
                    }

                    if (!empty($additionalImage)) {
//$additionalImagePrice = count($additionalImage) * $firstImagePrice;
                    }
                    $additionalImageName = (!empty($additionalImage)) ? $additionalImageName : '';
                    $firstImagePrice = (!empty($firstImageName)) ? $firstImagePrice : 0;

                    if ($flag == 'p') {
                        $secondImagePrice = (!empty($secondImageName)) ? $secondImagePrice : 0;
                    }


                    $additionalImagePrice = (!empty($additionalImageName)) ? $additionalImagePrice : 0;
                    $firstImageOptionId = (!empty($firstImageName)) ? $firstImageOptionId : '';
                    $secondImageOptionId = (!empty($secondImageName)) ? $secondImageOptionId : '';
                    $paramsArr = array();
                    $paramsArr['product_type_id'] = 7;
                    $productTypeId = $paramsArr['product_type_id'];
                    $paramsArr['productId'] = $productMenifestResult[0]['product_id'];
                    $paramsArr['item_id'] = $passengerDocumentFormArr['passenger_id'];
                    $paramsArr['item_type'] = '2';
                    $paramsArr['user_id'] = $postArr['user_id'];
                    $paramsArr['item_name'] = $request->getPost('item_name');
                    $paramsArr['item_info'] = $request->getPost('item_info');
// $paramsArr['item_price'] = $firstImagePrice + $secondImagePrice + $additionalImagePrice;
//$addToCartProductId = $this->addInventoryProductToCart($paramsArr);

                    /* Add to cart additional product */
                    $additionalProduct = array();
// $additionalProduct['cart_product_id'] = $addToCartProductId;
                    $additionalProduct['first_image'] = $firstImageName;
                    $additionalProduct['second_image'] = $secondImageName;
                    $additionalProduct['num_additional_images'] = count($additionalImage);
                    $additionalProduct['additional_images'] = $additionalImageName;
                    $additionalProduct['user_id'] = $postArr['user_id'];
// $this->addToCartPassengerManifest($additionalProduct);
                    // $additionalImage = $request->getPost('additional_images');
                    if (isset($isFirstImage) && !empty($isFirstImage)) {
                        $paramsArr['manifest_info'] = $isFirstImage;
                        $paramsArr['product_attribute_option_id'] = $firstImageOptionId;
                        $paramsArr['manifest_type'] = 1;
                        $paramsArr['item_price'] = $firstImagePrice;
                        $addToCartProductId = $this->addInventoryProductToCart($paramsArr);
                    }
                    if (isset($isSecondImage) && !empty($isSecondImage)) {
                        $paramsArr['manifest_info'] = $isSecondImage;
                        $paramsArr['product_attribute_option_id'] = $secondImageOptionId;
                        $paramsArr['manifest_type'] = $manifestType;
                        $paramsArr['item_price'] = $secondImagePrice;

                        $addToCartProductId = $this->addInventoryProductToCart($paramsArr);
                    }


// add to cart for manifest
                    if (isset($additionalImage) && !empty($additionalImage)) {
                        foreach ($additionalImage as $key => $val) {
                            $paramsArr['manifest_info'] = $val;
                            $paramsArr['product_attribute_option_id'] = $firstImageOptionId;
                            $paramsArr['manifest_type'] = 3;
                            $paramsArr['item_price'] = $additionalImagePrice;
                            //  echo "bbbb";

                            $addToCartProductId = $this->addInventoryProductToCart($paramsArr);
                        }
                    }


                    /* Add to cart additional product */
                }
            }
            /* End Manifest code */


            $productParam['product_type'] = $productTypeId;
            $productResult = $this->getServiceLocator()->get('Product\Model\ProductTable')->getCrmProducts($productParam);
            $searchParams = array();
            $searchParams['id'] = $productResult[0]['product_id'];
            $relatedProduct = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProductRelatedProduct($searchParams);
            $hasReletedProducts = 'no';
            if (count($relatedProduct) > 0) {
                $hasReletedProducts = 'yes';
            } else {
                $hasReletedProducts = 'no';
            }

            $messages = array('status' => "success", 'hasReletedProducts' => $hasReletedProducts);
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This action is used to check Related Products Exists
     * @return json
     * @param void
     * @author Icreon Tech - NS
     */
    public function checkRelatedProductsExistsAction() {
        $this->checkFrontUserAuthentication();
        $this->getTransactionTable();
        $request = $this->getRequest();
        $response = $this->getResponse();

        try {
            $hasRelatedProducts = 'no';
            if ($request->isPost()) {
                $postArr = $request->getPost()->toArray();
                $searchParams = array();
                $searchParams['id'] = $postArr['product_id_related'];
                $relatedProduct = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProductRelatedProduct($searchParams);
                $hasReletedProducts = 'no';
                if (count($relatedProduct) > 0) {
                    $hasRelatedProducts = 'yes';
                } else {
                    $hasRelatedProducts = 'no';
                }
            }

            $messages = array('status' => 'success', 'hasRelatedProducts' => $hasRelatedProducts);
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        } catch (Exception $e) {
            $messages = array('status' => 'error', 'hasRelatedProducts' => 'no');
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This action is used to add inventory to cart
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function addToCartMembershipFrontAction() {
//$this->checkUserAuthentication();
        $this->getTransactionTable();
        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $transaction = new Transaction($this->_adapter);
        $membershipDetailForm = new MembershipDetailForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $postArr = $request->getPost()->toArray();
            $membershipDetailFormArr = $postArr;


            $prodMappArr = array($membershipDetailFormArr['product_mapping_id']);
            $getProductMappingTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductsMappingType($prodMappArr);
            $membershipDetailFormArr['product_type_id'] = $getProductMappingTypes[0]['product_type_id'];
            $membershipDetailFormArr['item_id'] = $postArr['membership_id'];
            $membershipDetailFormArr['item_price'] = $postArr['minimum_donation_amount'];
            $membershipDetailFormArr['user_id'] = $postArr['user_id'];
            $membershipDetailFormArr['user_session_id'] = session_id();
            $membershipDetailFormArr['added_by'] = '';
            $membershipDetailFormArr['added_date'] = DATE_TIME_FORMAT;
            $membershipDetailFormArr['modify_by'] = '';
            $membershipDetailFormArr['modify_date'] = DATE_TIME_FORMAT;
            $membershipDetailFormArr['cart_date'] = DATE_TIME_FORMAT;
            $membershipDetailFormArr['item_type'] = '5';
            $addToCartProductArr = $transaction->getAddToCartProductArr($membershipDetailFormArr);
            /* add campaign,promotion and user group id in cart start */
            $promotionId = "";
            $couponCode = "";
            $user_group_id = "";

            $this->campaignSession = new Container('campaign');
            $campaign_code = (isset($this->campaignSession->campaign_code) && !empty($this->campaignSession->campaign_code) ? $this->campaignSession->campaign_code : "");

            $campaign_id = isset($this->campaignSession->campaign_id) && !empty($this->campaignSession->campaign_id) ? $this->campaignSession->campaign_id : 0;
            $channel_id = (isset($this->campaignSession->channel_id) && !empty($this->campaignSession->channel_id) ? $this->campaignSession->channel_id : 0);

            if ($campaign_id != 0 && $channel_id != 0) {
                $campResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getChannelPromo(array('campaign_id' => $campaign_id, 'channel_id' => $channel_id));

                if (isset($campResult[0]['promotion_id']) && !empty($campResult[0]['promotion_id'])) {
                    $couponCode = $campResult[0]['coupon_code'];
                    $promotionId = $campResult[0]['promotion_id'];
                } else {
                    $couponCode = "";
                    $promotionId = 0;
                }

                $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '';
                if (isset($user_id) && $user_id != '') {
                    $searchParam['user_id'] = $user_id;
                    $searchParam['campaign_id'] = $campaign_id;
                    $cmTable = $this->getServiceLocator()->get('Campaign\Model\CampaignTable');
                    $campaign_arr = $cmTable->getUserCampaigns($searchParam);
                    if ($campaign_arr && isset($campaign_arr[0]['group_id'])) {
                        $user_group_id = $campaign_arr[0]['group_id'];
                    }
                    //echo "<pre>";var_dump($campaign_arr);die;
                }
            }

            $addToCartProductArr[] = $campaign_id;
            $addToCartProductArr[] = $campaign_code;
            $addToCartProductArr[] = $promotionId;
            $addToCartProductArr[] = $couponCode;
            $addToCartProductArr[] = $user_group_id;

            //echo "<pre>";print_r($addToCartProductArr);die;
            /* add campaign,promotion and user group id in cart ends */
            $productAddToCartId = $this->_transactionTable->addToCartProduct($addToCartProductArr);
            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    public function barcodeAction() {
        $params = $this->params()->fromRoute();
        $searchParam['content'] = $params['content'];
        $barcodeOptions = array('text' => $searchParam['content']);
        $rendererOptions = array();
// $filename = 'barcode_'.date('Y-m-d').'_'.$params['content'];
// $img_file_name = $this->_config['file_upload_path']['assets_upload_dir'] . 'temp/' . $fileName;
        Barcode::factory('code39', 'image', $barcodeOptions, $rendererOptions)->render();
    }

    public function barcodeImage($params) {
        $searchParam['content'] = $params;
        $barcodeOptions = array('text' => $searchParam['content']);
        $rendererOptions = array();
        $fileName = 'barcode_' . date('Y-m-d') . '_' . $searchParam['content'] . '.png';
        $img_file_name = $this->_config['file_upload_path']['assets_upload_dir'] . 'temp/' . $fileName;

        return Barcode::factory('code39', 'image', $barcodeOptions, $rendererOptions)->makeRenderer();
    }

    /**
     * This action is used to add inventory to cart
     * @return json
     * @param void
     * @author Icreon Tech - KK
     */
    public function addWishListAction() {
        $this->checkFrontUserAuthenticationAjx();
        $this->getTransactionTable();
        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $transaction = new Transaction($this->_adapter);
        $inventoryForm = new InventoryForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $postArr = $request->getPost()->toArray();
            $inventoryFormArr = $postArr;
            $prodMappArr = array(4);
            $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '';
            $getProductMappingTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductsMappingType($prodMappArr);
            $params['productId'] = $postArr['product_id'];
            $params['user_id'] = $user_id;
            $params['product_mapping_id'] = $prodMappArr[0];
            $params['num_quantity'] = $postArr['num_quantity'];

            $productStatus = $this->getTransactionTable()->getWishListProductAlreadyAdded($params);

            if ($productStatus[0]['product_exist'] == 0) {
                $cartId = $this->addInventoryProductToWishlist($params);
            } else {
                $params['num_quantity'] = $productStatus[0]['num_quantity'] + $params['num_quantity'];
                $productStatus = $this->getTransactionTable()->updateCartInventoryProduct($params);
            }
            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This action is used to add inventory product to cart
     * @return int
     * @param array
     * @author Icreon Tech - DT
     */
    public function addInventoryProductToWishlist($params = array()) {
        $searchParam['id'] = $params['productId'];
        $productResult = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProductInfo($searchParam);
        $transaction = new Transaction($this->_adapter);
        $addToCartProductId = 0;
        if (!empty($productResult)) {
            $inventoryFormArr = array();
            $inventoryFormArr['user_id'] = $params['user_id'];
            $inventoryFormArr['user_session_id'] = session_id();
            $inventoryFormArr['product_mapping_id'] = isset($params['product_mapping_id']) ? $params['product_mapping_id'] : $productResult[0]['product_mapping_id'];
            $inventoryFormArr['product_id'] = isset($params['product_id']) ? $params['product_id'] : $productResult[0]['product_id'];
            $inventoryFormArr['product_type_id'] = isset($params['product_type_id']) ? $params['product_type_id'] : $productResult[0]['product_type_id'];
            $inventoryFormArr['product_attribute_option_id'] = isset($params['product_attribute_option_id']) ? $params['product_attribute_option_id'] : NULL;
            $inventoryFormArr['item_id'] = isset($params['item_id']) ? $params['item_id'] : NULL;
            $inventoryFormArr['item_type'] = isset($params['item_type']) ? $params['item_type'] : NULL;
            $inventoryFormArr['num_quantity'] = isset($params['num_quantity']) ? $params['num_quantity'] : 1;
            $inventoryFormArr['item_price'] = isset($params['item_price']) ? $params['item_price'] : NULL;
            $inventoryFormArr['product_sku'] = $productResult[0]['product_sku'];
            $inventoryFormArr['product_price'] = isset($params['product_price']) ? $params['product_price'] : $productResult[0]['product_sell_price'];
            $inventoryFormArr['added_by'] = isset($this->auth->getIdentity()->crm_user_id) ? $this->auth->getIdentity()->crm_user_id : '0';
            $inventoryFormArr['added_date'] = DATE_TIME_FORMAT;
            $inventoryFormArr['modify_by'] = isset($this->auth->getIdentity()->crm_user_id) ? $this->auth->getIdentity()->crm_user_id : '0';
            $inventoryFormArr['modify_date'] = DATE_TIME_FORMAT;
            $inventoryFormArr['cart_date'] = DATE_TIME_FORMAT;
            $addToCartProductArr = $transaction->getAddToCartProductArr($inventoryFormArr);
            unset($addToCartProductArr[15]); // key of item_name
            unset($addToCartProductArr[16]); // key of item_info
            unset($addToCartProductArr[17]); // key of item_info
            unset($addToCartProductArr[18]); // key of item_info
            /* add campaign,promotion and user group id in cart start */
            $promotionId = "";
            $couponCode = "";
            $user_group_id = "";

            $this->campaignSession = new Container('campaign');
            $campaign_code = (isset($this->campaignSession->campaign_code) && !empty($this->campaignSession->campaign_code) ? $this->campaignSession->campaign_code : "");

            $campaign_id = isset($this->campaignSession->campaign_id) && !empty($this->campaignSession->campaign_id) ? $this->campaignSession->campaign_id : 0;
            $channel_id = (isset($this->campaignSession->channel_id) && !empty($this->campaignSession->channel_id) ? $this->campaignSession->channel_id : 0);

            if ($campaign_id != 0 && $channel_id != 0) {
                $campResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getChannelPromo(array('campaign_id' => $campaign_id, 'channel_id' => $channel_id));

                if (isset($campResult[0]['promotion_id']) && !empty($campResult[0]['promotion_id'])) {
                    $couponCode = $campResult[0]['coupon_code'];
                    $promotionId = $campResult[0]['promotion_id'];
                } else {
                    $couponCode = "";
                    $promotionId = 0;
                }

                $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '';
                if (isset($user_id) && $user_id != '') {
                    $searchParam['user_id'] = $user_id;
                    $searchParam['campaign_id'] = $campaign_id;
                    $cmTable = $this->getServiceLocator()->get('Campaign\Model\CampaignTable');
                    $campaign_arr = $cmTable->getUserCampaigns($searchParam);
                    if ($campaign_arr && isset($campaign_arr[0]['group_id'])) {
                        $user_group_id = $campaign_arr[0]['group_id'];
                    }
                    //echo "<pre>";var_dump($campaign_arr);die;
                }
            }

            $addToCartProductArr[] = $campaign_id;
            $addToCartProductArr[] = $campaign_code;
            $addToCartProductArr[] = $promotionId;
            $addToCartProductArr[] = $couponCode;
            $addToCartProductArr[] = $user_group_id;

            //echo "<pre>";print_r($addToCartProductArr);die;
            /* add campaign,promotion and user group id in cart ends */

            $addToCartProductId = $this->_transactionTable->addToWishlistProduct($addToCartProductArr);
        }
        return $addToCartProductId;
    }

    /**
     * This action is used to get user cart list frontend
     * @return json
     * @param void
     * @author Icreon Tech - KK
     */
    public function wishListAction() {
        $this->checkFrontUserAuthentication();
        $this->getTransactionTable();
        $this->getConfig();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();

        $getProduct = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProduct(array('is_available' => '1', 'featured' => '1', 'offset' => '1', 'total_in_stock' => '1', 'rand_order_by' => '1', 'restricted_category_id' => $this->_config['custom_frame_id']));
        $discount = '';
        /*
          if (!empty($this->auth->getIdentity()->user_id)) {

          $user_id = $this->auth->getIdentity()->user_id;
          $userArr = array('user_id' => $user_id);
          $contactDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserMemberShipDetail($userArr);
          if ($contactDetail['membership_id'] != '1') {
          $discount = $contactDetail['membership_discount'];
          }
          } */
        if (!empty($this->auth->getIdentity()->user_id)) {
            if (defined('MEMBERSHIP_DISCOUNT') && $getProduct[0]['is_membership_discount'] == 1) {
                $discount = MEMBERSHIP_DISCOUNT;
            }
        }
        $assetsUploadDirProduct = $this->_config['file_upload_path']['assets_upload_dir'] . 'product/thumbnail';
        $uploadedFilePathProduct = $this->_config['file_upload_path']['assets_url'] . 'product/thumbnail';
        $params = $this->params()->fromRoute();
        $viewModel->setVariables(array(
            'getProduct' => $getProduct,
            'discount' => $discount,
            'uploadedFilePathProduct' => $uploadedFilePathProduct,
            'assetsUploadDirProduct' => $assetsUploadDirProduct,
            'jsLangTranslate' => $this->_config['transaction_messages']['config']['donate_transaction_front'])
        );
        return $viewModel;
    }

    /**
     * This action is used to get user cart list frontend
     * @return json
     * @param void
     * @author Icreon Tech - KK
     */
    public function wishListFrontProductAction() {
//$this->checkFrontUserAuthenticationAjx();
        $this->checkFrontUserAuthentication();
        $this->getTransactionTable();
        $this->getConfig();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $params = $this->params()->fromRoute();
        $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '';
        $cartParam['user_id'] = $user_id;
        $cartParam['source_type'] = 'frontend';
        if (defined('MEMBERSHIP_DISCOUNT'))
            $cartParam['membership_percent_discount'] = MEMBERSHIP_DISCOUNT;
        else
            $cartParam['membership_percent_discount'] = '';
        $cartData = $this->getTransactionTable()->getWishlistProducts($cartParam);


//        $cartParam['user_id'] = $user_id;
//        $cartParam['session_id'] = session_id();
//        $userCartList = $this->getTransactionTable()->getCartList($cartParam);
//
//       $discount = '';
//        if (!empty($this->auth->getIdentity()->user_id)) {
//
//
//            $userArr = array('user_id' => $user_id);
//            $contactDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getContact($userArr);
//            if ($contactDetail[0]['membership_id'] != '1') {
//                $discount = $contactDetail[0]['discount'];
//            }
//        }

        $discount = '';
        /* if (!empty($this->auth->getIdentity()->user_id)) {
          $userArr = array('user_id' => $user_id);
          $contactDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserMemberShipDetail($userArr);
          if ($contactDetail['membership_id'] != '1') {
          $discount = $contactDetail['membership_discount'];
          }
          } */
        if (!empty($this->auth->getIdentity()->user_id)) {
            if (defined('MEMBERSHIP_DISCOUNT')) {
                $discount = MEMBERSHIP_DISCOUNT;
            }
        }
        $memberShip = array();
        if (empty($discount)) {
            $memberShip = $this->getTransactionTable()->getMinimumMembership();
        }
        $viewModel->setVariables(array(
            'cartlist1' => $cartData,
            'membership' => $memberShip,
            'discount' => $discount,
            'jsLangTranslate' => $this->_config['transaction_messages']['config']['donate_transaction_front'],
            'upload_file_path' => $this->_config['file_upload_path'])
        );
        return $viewModel;
    }

    /**
     * This action is used to add inventory to cart
     * @return json
     * @param void
     * @author Icreon Tech - KK
     */
    public function removeWishlistFrontAction() {
        $this->checkFrontUserAuthenticationAjx();
        $this->getTransactionTable();
        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $transaction = new Transaction($this->_adapter);
        $inventoryForm = new InventoryForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $postArr = $request->getPost()->toArray();
            $inventoryFormArr = $postArr;
            $transactionId = $inventoryFormArr['cart_id'];
            $productType = $inventoryFormArr['product_type'];
            $removeArray['cart_id'] = $transactionId;
            $removeArray['product_type'] = $productType;
            $this->getTransactionTable()->removeFromWishlist($removeArray);
            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This action is used to add inventory to cart
     * @return json
     * @param void
     * @author Icreon Tech - KK
     */
    public function getTransactionAddtocartAction() {
        $this->checkFrontUserAuthenticationAjx();
        $this->getTransactionTable();
        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $transaction = new Transaction($this->_adapter);
        $inventoryForm = new InventoryForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $postArr = $request->getPost()->toArray();
            if (!empty($this->auth->getIdentity()->user_id)) {
                $user_id = $this->auth->getIdentity()->user_id;
            }
            if (!empty($postArr['is_manifest'])) {
                $postArr['user_id'] = $user_id;
                $this->addtocartmanifestfront($postArr);
            } elseif (!empty($postArr['shipids'])) {
                $postArr['user_id'] = $user_id;
                $this->addtocartshipimagefront($postArr);
            } elseif (!empty($postArr['woh'])) {
                $postArr['user_id'] = $user_id;
                $this->addtocartwallofhonorfront($postArr);
            } else {
                $inventoryFormArr = $postArr;

                $transactionIds = explode(",", $inventoryFormArr['transactionids']);
                $quantity = $inventoryFormArr['quantity'];
                $sm = $this->getServiceLocator();
                $passengerTable = $sm->get('Passenger\Model\PassengerTable');
                if (!empty($this->auth->getIdentity()->user_id)) {

                    $user_id = $this->auth->getIdentity()->user_id;
                }

                foreach ($transactionIds as $transactionId) {
                    $transactionDetail = $this->getTransactionTable()->getTransactionDetailFront(array('tran_id' => $transactionId));

                    $productParam = array();
                    $productParam['productType'] = '5';/** product */
                    $productResult = array();
                    $productResult = $passengerTable->getProductAttributeDetails($productParam);

                    $paramsArr = array();
                    $paramsArr['product_type_id'] = 5;
                    $paramsArr['productId'] = $transactionDetail[0]['product_id'];
                    $paramsArr['item_id'] = $transactionDetail[0]['item_id'];
                    $paramsArr['item_name'] = $transactionDetail[0]['item_name'];
                    $paramsArr['item_info'] = $transactionDetail[0]['item_info'];
                    $paramsArr['item_type'] = '1';
                    $paramsArr['user_id'] = $user_id;
                    $paramsArr['num_quantity'] = $quantity;
                    $paramsArr['product_attribute_option_id'] = $transactionDetail[0]['product_attribute_option_id'];
                    $paramsArr['item_price'] = $productResult[0]['product_sell_price'] + $productResult[0]['option_price'];
                    $addToCartProductId = $this->addInventoryProductToCart($paramsArr);
                }
            }
            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    public function addtocartmanifestfront($postArr) {
        $manifestids = explode(",", $postArr['manifestids']);
        $shipIdArray = array();
        if (isset($postArr['ship_id']) && !empty($postArr['ship_id'])) {
            $shipIdArray = explode(",", $postArr['ship_id']);
        }
        $sm = $this->getServiceLocator();
        $passengerTable = $sm->get('Passenger\Model\PassengerTable');

        foreach ($manifestids as $key => $image_name) {
            $isManifest = $postArr['is_manifest'];
            if ($isManifest != 0 && !empty($isManifest)) {
                $quantity = (isset($postArr['quantity']) && !empty($postArr['quantity'])) ? $postArr['quantity'] : '1';
                $flag = $postArr['flag'];
                if (isset($postArr['old_manifest']) && !empty($postArr['old_manifest'])) {
                    $searchParam['id'] = $image_name;
                    $manifestResult = array();
                    $manifestResult = $passengerTable->getManifestDetails($searchParam);

                    $paramsArr = array();
                    $paramsArr['product_type_id'] = 7;
                    $paramsArr['productId'] = $manifestResult[0]['product_id'];
                    $paramsArr['item_id'] = $manifestResult[0]['item_id'];
                    $paramsArr['num_quantity'] = $quantity;
                    $paramsArr['item_type'] = '2';
                    $paramsArr['user_id'] = $postArr['user_id'];
                    $paramsArr['item_name'] = $manifestResult[0]['item_name'];
                    $paramsArr['item_info'] = $manifestResult[0]['item_info'];
                    $paramsArr['manifest_info'] = $manifestResult[0]['manifest_info'];
                    $paramsArr['manifest_type'] = 1;
                    $paramsArr['product_attribute_option_id'] = 3;

                    $addToCartProductId = $this->addInventoryProductToCart($paramsArr);



                    /* $countAdditional = count(explode(",", $manifestResult[0]['additional_images']));
                      $additionalProduct = array();
                      $additionalProduct['cart_product_id'] = $addToCartProductId;
                      $additionalProduct['first_image'] = $manifestResult[0]['first_image'];
                      $additionalProduct['second_image'] = $manifestResult[0]['second_image'];
                      $additionalProduct['first_attribute_option_id'] = $manifestResult[0]['first_attribute_option_id'];
                      $additionalProduct['second_attribute_option_id'] = $manifestResult[0]['second_attribute_option_id'];
                      $additionalProduct['num_additional_images'] = (!empty($countAdditional)) ? count($countAdditional) : 0;
                      $additionalProduct['additional_images'] = $manifestResult[0]['additional_images'];
                      $additionalProduct['user_id'] = $postArr['user_id']; */
// $this->addToCartPassengerManifest($additionalProduct);
                } else {

                    $productMenifestParam = array();
                    $productMenifestParam['productType'] = '7';/** manifest */
                    $productMenifestResult = $passengerTable->getProductAttributeDetails($productMenifestParam);
                    $manifestParam = array();
                    $manifestParam['manifest'] = $manifestids[$key];
                    $manifestResult = $passengerTable->getShipManifestDetails($manifestParam);


                    $paramsArr = array();
                    if (isset($postArr['passenger_id']) && !empty($postArr['passenger_id']))
                        $paramsArr['item_id'] = $postArr['passenger_id'];
                    else if (count($shipIdArray) > 0)
                        $paramsArr['item_id'] = $shipIdArray[$key];


                    $paramsArr['user_id'] = $postArr['user_id'];
                    $paramsArr['product_type_id'] = 7;
                    $paramsArr['productId'] = $productMenifestResult[0]['product_id'];
                    $paramsArr['item_type'] = '2';
                    $paramsArr['user_id'] = $postArr['user_id'];
                    $paramsArr['item_name'] = $manifestResult[0]['SHIP_NAME'];
                    $paramsArr['item_info'] = substr($manifestResult[0]['ARRIVAL_DATE'], 0, 10);
                    $paramsArr['manifest_info'] = $manifestids[$key];
                    $paramsArr['manifest_type'] = 1;
                    $paramsArr['product_attribute_option_id'] = 3;

                    $addToCartProductId = $this->addInventoryProductToCart($paramsArr);

                    /* Add to cart additional product */
                    /*
                      $additionalProduct = array();
                      $additionalProduct['cart_product_id'] = $addToCartProductId;
                      $additionalProduct['first_image'] = $firstImageName;
                      $additionalProduct['second_image'] = $secondImageName;
                      $additionalProduct['first_attribute_option_id'] = $firstImageOptionId;
                      $additionalProduct['second_attribute_option_id'] = $secondImageOptionId;
                      $additionalProduct['num_additional_images'] = (!empty($additionalImage)) ? count($additionalImage) : 0;
                      $additionalProduct['additional_images'] = $additionalImageName;
                      $additionalProduct['user_id'] = $postArr['user_id'];

                      $this->addToCartPassengerManifest($additionalProduct); */

                    /* Add to cart additional product */
                }
            }
        }
    }

    public function addtocartshipimagefront($postArr) {
        $isPassengerShip = 1;
        $sm = $this->getServiceLocator();
        $passengerTable = $sm->get('Passenger\Model\PassengerTable');
        $shipArray = explode(",", $postArr['shipids']);
        foreach ($shipArray as $shipId) {
            if (!empty($shipId)) {
                foreach ($postArr['attribute_option_id'] as $optionid => $optionQuantity) {
                    if (!empty($optionQuantity)) {
                        $shipPassengerQty = $optionQuantity;
                        $attributeOptionIds = array($optionid);
                        if ($isPassengerShip != '' && $isPassengerShip != 0 && $shipPassengerQty != '' && $shipPassengerQty > 0) {

                            $productParam = array();
                            $productParam['productType'] = '8';/** product */
                            $productResult = array();
                            $productResult = $passengerTable->getProductAttributeDetails($productParam);
                            foreach ($productResult as $attribute) {

                                $isExistArr = array_keys($attributeOptionIds, $attribute['attribute_option_id']);
                                if (!empty($isExistArr)) {
                                    $paramsArr = array();
                                    $paramsArr['product_type_id'] = 8;
                                    $paramsArr['productId'] = $attribute['product_id'];
                                    $paramsArr['product_attribute_option_id'] = $optionid;
                                    $shipIdArray = @explode('~~', $shipId);
                                    $paramsArr['item_id'] = $shipIdArray[0];
                                    $paramsArr['item_name'] = $shipIdArray[1];
                                    $paramsArr['item_info'] = $shipIdArray[2];
                                    $paramsArr['item_type'] = '3';
                                    $paramsArr['user_id'] = $postArr['user_id'];
                                    $paramsArr['num_quantity'] = $shipPassengerQty;
                                    $addToCartProductId = $this->addInventoryProductToCart($paramsArr);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * This Action is used to get credit card detail page
     * @param Void
     * @return String
     * @author Icreon Tech - DT
     */
    public function getCreditCardDetailAction() {
        $this->checkUserAuthenticationAjx();
        $this->layout('popup');
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getTransactionTable();
        $paymentModeCredit = new PaymentModeCredit();
        $createTransactionMessage = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $transaction = new Transaction($this->_adapter);

        $state = $this->getServiceLocator()->get('Common\Model\CommonTable')->getStates();
        $stateList = array();
        $stateList[''] = 'Select State';
        foreach ($state as $key => $val) {
            $stateList[$val['state']] = $val['state'];
        }
        $paymentModeCredit->get('billing_state_auth_select')->setAttribute('options', $stateList);


        $monthArr = $this->creditMonthArray();
        $paymentModeCredit->get('expiry_month')->setAttribute('options', $monthArr);
        $yearArr = $this->yearArrayRange(15);
        $paymentModeCredit->get('expiry_year')->setAttribute('options', $yearArr);
        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
        $countryList = array();
        foreach ($country as $key => $val) {
            $countryList[$val['country_id']] = $val['name'];
        }
        $paymentModeCredit->get('billing_country_auth')->setAttribute('options', $countryList);
        $params = $this->params()->fromRoute();
        $userId = $this->decrypt($params['user_id']);
        $paymentModeCredit->get('user_id')->setValue($userId);
        $userTokenInfo = $this->getServiceLocator()->get('User\Model\UserTable')->getUserTokenInformations(array('user_id' => $userId));

        $profileIdArr = array('' => 'Select');
        if (!empty($userTokenInfo)) {
//asd($userTokenInfo);
            foreach ($userTokenInfo as $key => $val) {
                /* $createCustomerProfile = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
                  $createCustomerProfile->getCustomerProfileRequest(array('customerProfileId' => $val['profile_id']));

                  $cardNumbrer = $createCustomerProfile->profile->paymentProfiles->payment->creditCard->cardNumber;
                  $cardExpiryDate = $createCustomerProfile->profile->paymentProfiles->payment->creditCard->expirationDate;
                  $userTokenInfo[$key]['card_number'] = (string) $cardNumbrer;
                  $userTokenInfo[$key]['expiry_date'] = (string) $cardExpiryDate;
                  $profileIdArr[$val['profile_id']] = (string) $cardNumbrer; */

                $userTokenInfo[$key]['card_number'] = $val['credit_card_no'];
                //  $userTokenInfo[$key]['expiry_date'] = (string) $cardExpiryDate;
                $profileIdArr[$val['profile_id']] = $val['credit_card_no'];
            }
        }
        $paymentModeCredit->get('existing_profile_id')->setAttribute('options', $profileIdArr);

        $userArr = array('user_id' => $userId);
        $contactDetail = array();
        $contactDetail[0] = $this->getServiceLocator()->get('User\Model\UserTable')->getUser($userArr);
        if ($contactDetail[0]['user_type'] == '1') {
            $name = "";
            if ($contactDetail[0]['first_name'] != '')
                $name.= $contactDetail[0]['first_name'];
            if ($contactDetail[0]['middle_name'] != '')
                $name.= " " . $contactDetail[0]['middle_name'];
            if ($contactDetail[0]['last_name'] != '')
                $name.= " " . $contactDetail[0]['last_name'];
        }else {
            $name = $contactDetail[0]['company_name'];
        }


        $paymentModeCredit->get('card_holder_name')->setValue($name);

        /* Pledge Transaction */
        if (!empty($params['pledge_transaction_id'])) {
            $pledgeTransactionId = $this->decrypt($params['pledge_transaction_id']);
            $pledgeTransData = $this->getServiceLocator()->get('Pledge\Model\PledgeTable')->getPledgeTransactionsById('', '', $pledgeTransactionId);
            $paymentModeCredit->get('pledge_transaction_id')->setValue($pledgeTransactionId);
            $paymentModeCredit->get('amount')->setValue($pledgeTransData[0]['amount_pledge']);
            $paymentModeCredit->get('exist_amount')->setValue($pledgeTransData[0]['amount_pledge']);
            $paymentModeCredit->get('amount')->setAttribute('readOnly', true);
            $paymentModeCredit->get('exist_amount')->setAttribute('readOnly', true);
            $paymentModeCredit->get('exist_amount')->setAttribute('class', "readonly");
        }
        /* End Pledge Transaction */

        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'jsLangTranslate' => array_merge($createTransactionMessage, $this->_config['transaction_messages']['config']['common_message']),
            'paymentModeCredit' => $paymentModeCredit
        ));
        return $viewModel;
    }

    /**
     * This Action is used to get credit card detail page
     * @param Void
     * @return String
     * @author Icreon Tech - DT
     */
    public function addtocartwallofhonorfront($postArr) {
        $wallOfHonorAddToCartFormArr = $postArr;
        $prodMappArr = array($wallOfHonorAddToCartFormArr['product_mapping_id']);
        $transaction = new Transaction($this->_adapter);
        $transactionDetails = $this->getServiceLocator()->get('User\Model\WohTable')->getUserWohTraDetailById(array('woh_id' => $postArr['woh_id']));
        $getProductMappingTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductsMappingType($prodMappArr);
        $wallOfHonorAddToCartFormArr['product_type_id'] = $getProductMappingTypes[0]['product_type_id'];
        $wallOfHonorAddToCartFormArr['user_id'] = $postArr['user_id'];
        $wallOfHonorAddToCartFormArr['user_session_id'] = session_id();
        $wallOfHonorAddToCartFormArr['added_by'] = '';
        $wallOfHonorAddToCartFormArr['added_date'] = DATE_TIME_FORMAT;
        $wallOfHonorAddToCartFormArr['modify_by'] = "";
        $wallOfHonorAddToCartFormArr['modify_date'] = DATE_TIME_FORMAT;
        $wallOfHonorAddToCartFormArr['cart_date'] = DATE_TIME_FORMAT;

        $productId = $transactionDetails[0]['product_id'];
        $productMappingId = $transactionDetails[0]['product_mapping_id'];

        $isBioCertificate = '';
        $bioCertificateQty = '';
        $isPersonalize = '';
        $personalizeQty = '';
        /* Bio Certificate Code */
        $isBioCertificate = $postArr['Boi'];
        $bioCertificateQty = $isBioCertificate;
        $isPersonalize = $postArr['Personalized'];
        $personalizeQty = $isPersonalize;
        if ($isBioCertificate != '' || $isPersonalize) {
            /* Bio Certificate Code */
            if ($isBioCertificate != 0 && $isBioCertificate != '' && $bioCertificateQty != '' && $bioCertificateQty > 0) {
                $paramsArr = array();
                $paramsArr['productId'] = $postArr['pro_Boi'];
                $paramsArr['user_id'] = $postArr['user_id'];
//$addToCartProductId = $this->addInventoryProductToCart($paramsArr);
                $searchParam['id'] = $postArr['pro_Boi'];
                $productResult = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProductInfo($searchParam);
                $paramsArr = array();
                $paramsArr = $wallOfHonorAddToCartFormArr;
                $paramsArr['user_id'] = $postArr['user_id'];
                $paramsArr['user_session_id'] = session_id();
                $paramsArr['date_of_entry_to_us'] = $this->DateFormat($paramsArr['date_of_entry_to_us'], 'db_datetime_format');
                $paramsArr['product_id'] = $productResult[0]['product_id'];
                $paramsArr['product_type_id'] = $productResult[0]['product_type_id'];
                $paramsArr['product_mapping_id'] = $productResult[0]['product_mapping_id'];
                $paramsArr['product_sku'] = $productResult[0]['product_sku'];
                $paramsArr['product_price'] = $productResult[0]['product_sell_price'];
                $paramsArr['bio_certificate_qty'] = $bioCertificateQty;
                $paramsArr['wohid'] = $postArr['woh_id'];
                $paramsArr['added_by'] = "";
                $paramsArr['added_date'] = DATE_TIME_FORMAT;
                $paramsArr['modify_by'] = "";
                $paramsArr['modify_date'] = DATE_TIME_FORMAT;
                $paramsArr['item_type'] = '7';
                $paramsArr['bio_certificate_product_id'] = isset($postArr['bioCertificateProdId']) ? $postArr['bioCertificateProdId'] : '';
                $addToCartBioCertificateArr = $transaction->getAddToCartWallOfHonorBioCertificate($paramsArr);

                /* add campaign,promotion and user group id in cart start */
                $promotionId = "";
                $couponCode = "";
                $user_group_id = "";

                $this->campaignSession = new Container('campaign');
                $campaign_code = (isset($this->campaignSession->campaign_code) && !empty($this->campaignSession->campaign_code) ? $this->campaignSession->campaign_code : "");

                $campaign_id = isset($this->campaignSession->campaign_id) && !empty($this->campaignSession->campaign_id) ? $this->campaignSession->campaign_id : 0;
                $channel_id = (isset($this->campaignSession->channel_id) && !empty($this->campaignSession->channel_id) ? $this->campaignSession->channel_id : 0);

                if ($campaign_id != 0 && $channel_id != 0) {
                    $campResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getChannelPromo(array('campaign_id' => $campaign_id, 'channel_id' => $channel_id));

                    if (isset($campResult[0]['promotion_id']) && !empty($campResult[0]['promotion_id'])) {
                        $couponCode = $campResult[0]['coupon_code'];
                        $promotionId = $campResult[0]['promotion_id'];
                    } else {
                        $couponCode = "";
                        $promotionId = 0;
                    }

                    $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '';
                    if (isset($user_id) && $user_id != '') {
                        $searchParam['user_id'] = $user_id;
                        $searchParam['campaign_id'] = $campaign_id;
                        $cmTable = $this->getServiceLocator()->get('Campaign\Model\CampaignTable');
                        $campaign_arr = $cmTable->getUserCampaigns($searchParam);
                        if ($campaign_arr && isset($campaign_arr[0]['group_id'])) {
                            $user_group_id = $campaign_arr[0]['group_id'];
                        }
                        //echo "<pre>";var_dump($campaign_arr);die;
                    }
                }

                $addToCartBioCertificateArr[] = $campaign_id;
                $addToCartBioCertificateArr[] = $campaign_code;
                $addToCartBioCertificateArr[] = $promotionId;
                $addToCartBioCertificateArr[] = $couponCode;
                $addToCartBioCertificateArr[] = $user_group_id;
				$addToCartBioCertificateArr[] = '6';

                //echo "<pre>";print_r($addToCartProductArr);die;
                /* add campaign,promotion and user group id in cart ends */
                $addToCartBioCertificateId = $this->_transactionTable->addToCartBioCertificate($addToCartBioCertificateArr);
            }
        }
        /* End Bio Certificate Code */

        /* Personalize Code */
        if ($isPersonalize != 0 && $isPersonalize != '' && $personalizeQty != '' && $personalizeQty > 0) {
            $paramsArr = array();
            $paramsArr['productId'] = $postArr['pro_Personalized'];
            $paramsArr['user_id'] = $postArr['user_id'];
            $paramsArr['item_id'] = $postArr['woh_id'];
            $paramsArr['item_type'] = '8';
            $paramsArr['num_quantity'] = $personalizeQty;


            $this->addInventoryProductToCart($paramsArr);
        }
        /* End personalize Code */



        /* Wall of honor Code */
        $productParam = array();
        $productParam['productType'] = $wallOfHonorAddToCartFormArr['product_type_id'];/** product */
        $productParam['productId'] = $productId;
        $productResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getProductAttributeDetails($productParam);


        $wallOfHonorAddToCartFormArr['product_id'] = $productId;
        $wallOfHonorAddToCartFormArr['product_sku'] = $productResult[0]['product_sku'];
        $wallOfHonorAddToCartFormArr['product_price'] = $productResult[0]['product_sell_price'];


        $wallOfHonorAddToCartFormArr['origin_country'] = $transactionDetails[0]['origin_country'];
        /* Set option cost */


        foreach ($productResult as $productOption) {
            if ($productOption['attribute_option_id'] == $transactionDetails[0]['product_attribute_option_id']) {
                $wallOfHonorAddToCartFormArr['product_price']+=$productOption['option_price'];
                break;
            }
        }

        $firstNameOne = $transactionDetails[0]['first_name_one'];
        $otherName = $transactionDetails[0]['other_name'];
        $otherInitOne = $transactionDetails[0]['other_init_one'];
        $lastNameOne = $transactionDetails[0]['last_name_one'];
        $firstNameTwo = $transactionDetails[0]['first_name_two'];
        $otherInitTwo = $transactionDetails[0]['other_init_two'];
        $lastNameTwo = $transactionDetails[0]['last_name_two'];
        $firstLine = $transactionDetails[0]['first_line'];
        $secondLine = $transactionDetails[0]['second_line'];
        if (!empty($firstNameOne) || !empty($otherName) || !empty($otherInitOne) || !empty($lastNameOne) || !empty($firstNameTwo) || !empty($otherInitTwo) || !empty($lastNameTwo) || !empty($firstLine) || !empty($secondLine)) {
            /* end Set option cost */
            $wallOfHonorAddToCartFormArr = $transactionDetails[0];
            $addToCartWallOfHonorArr = $transaction->getAddToCartWallOfHonorArr($wallOfHonorAddToCartFormArr);
// $wallOfHonorAddToCartId = $this->_transactionTable->addToCartWallOfHonorNaming($addToCartWallOfHonorArr);
// $addToCartProductArr = $transaction->getAddToCartProductArr($wallOfHonorAddToCartFormArr);

            /* Duplicate Certificate Code */
            $isDuplicateCertificate = $postArr['Additional'];
            $duplicateCertificateQty = $isDuplicateCertificate;
            if ($isDuplicateCertificate != 0 && $isDuplicateCertificate != '' && $duplicateCertificateQty != '' && $duplicateCertificateQty > 0) {
//Duplicate Certificate Add
                $paramsArr = array();
                $paramsArr['productId'] = $postArr['pro_Additional'];
                $paramsArr['item_id'] = $postArr['woh_id'];
                $paramsArr['item_type'] = '7';
                $paramsArr['item_info'] = '2';
                $paramsArr['user_id'] = $postArr['user_id'];
                $paramsArr['num_quantity'] = $duplicateCertificateQty;
                $this->addInventoryProductToCart($paramsArr);
//End duplicate Certificate Add
            }
        }
        /* End duplicate Certificate Code */

        /* Save data for additional contact
          if (!empty($wallOfHonorAddToCartFormArr['first_name'])) {
          $ii = 0;
          foreach ($wallOfHonorAddToCartFormArr['first_name'] as $additionalContact) {
          if ($wallOfHonorAddToCartFormArr['email_id'][$ii] != '') {
          $additionalContactArr = array();
          $additionalContactArr['woh_id'] = $wallOfHonorAddToCartId;
          $additionalContactArr['first_name'] = $wallOfHonorAddToCartFormArr['first_name'][$ii];
          $additionalContactArr['last_name'] = $wallOfHonorAddToCartFormArr['last_name'][$ii];
          $additionalContactArr['email_id'] = $wallOfHonorAddToCartFormArr['email_id'][$ii];
          $additionalContactArr['added_date'] = DATE_TIME_FORMAT;
          $additionalContactArr['modify_date'] = DATE_TIME_FORMAT;
          $addToCartWallOfHonorAdditionalContactArr = $transaction->getAddToCartWallOfHonorAdditionalContactArr($additionalContactArr);
          $wallOfHonorAdditionalContactAddToCartId = $this->_transactionTable->addToCartWallOfHonorAdditionalContact($addToCartWallOfHonorAdditionalContactArr);
          }
          $ii++;
          }
          }
          }
          End Save data for additional contact */

        /* End wall of honor Code */



        /* Add related Product
          $isRelatedProduct = $request->getPost('is_related_product');
          $relatedProductIds = $request->getPost('related_product_name_ids');
          if ($isRelatedProduct != 0 && !empty($isRelatedProduct) && !empty($relatedProductIds)) {
          $relatedProductIds = explode(', ', $relatedProductIds);
          foreach ($relatedProductIds as $relatedProductId) {
          $paramsArr = array();
          $paramsArr['productId'] = $relatedProductId;
          $paramsArr['user_id'] = $this->decrypt($postArr['user_id']);
          $this->addInventoryProductToCart($paramsArr);
          }
          }
          End Add related Product */
    }

    /**
     * This action is used to make authorize net payment
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function makeAuthorizePaymentAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $msgArr = array_merge($this->_config['transaction_messages']['config']['create_crm_transaction'], $this->_config['transaction_messages']['config']['common_message']);
        $applicationConfigIds = $this->_config['application_config_ids'];
        $uSId = $applicationConfigIds['united_state_country_id'];
        $transaction = new Transaction($this->_adapter);
        $paymentModeCredit = new PaymentModeCredit();

        $request = $this->getRequest();
        $response = $this->getResponse();

        $creditCardResult = $this->getServiceLocator()->get('User\Model\UserTable')->getCreditCards();
        foreach ($creditCardResult as $key => $val) {
            $creditCardResultArray[$val['credit_card_type_id']] = strtolower($val['credit_card']);
        }
        $creditCardType = array_search(strtolower($request->getPost('credit_card_type')), $creditCardResultArray);

        $monthArr = $this->creditMonthArray();
        $paymentModeCredit->get('expiry_month')->setAttribute('options', $monthArr);
        $yearArr = $this->yearArrayRange(15);
        $paymentModeCredit->get('expiry_year')->setAttribute('options', $yearArr);
        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
        $countryList = array();
        foreach ($country as $key => $val) {
            $countryList[$val['country_id']] = $val['name'];
        }
        $paymentModeCredit->get('billing_country_auth')->setAttribute('options', $countryList);


        $state = $this->getServiceLocator()->get('Common\Model\CommonTable')->getStates();
        $stateList = array();
        $stateList[''] = 'Select State';
        foreach ($state as $key => $val) {
            $stateList[$val['state']] = $val['state'];
        }
        $paymentModeCredit->get('billing_state_auth_select')->setAttribute('options', $stateList);

        $userId = $request->getPost('user_id');
        $params = $this->params()->fromRoute();
        $userId = $userId;
        $userTokenInfo = $this->getServiceLocator()->get('User\Model\UserTable')->getUserTokenInformations(array('user_id' => $userId));
        $profileIdArr = array('' => 'Select');
        if (!empty($userTokenInfo)) {
            foreach ($userTokenInfo as $key => $val) {
                /* $createCustomerProfile = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
                  $createCustomerProfile->getCustomerProfileRequest(array('customerProfileId' => $val['profile_id']));
                  $cardNumbrer = $createCustomerProfile->profile->paymentProfiles->payment->creditCard->cardNumber;
                  $cardExpiryDate = $createCustomerProfile->profile->paymentProfiles->payment->creditCard->expirationDate;
                  $userTokenInfo[$key]['card_number'] = $val['credit_card_no'];
                  $profileIdArr[$val['profile_id']] = $val['credit_card_no']; */
                //$userTokenInfo[$key]['card_number'] = (string) $cardNumbrer;
                //$userTokenInfo[$key]['expiry_date'] = (string) $cardExpiryDate;
                //$profileIdArr[$val['profile_id']] = (string) $cardNumbrer;
                $userTokenInfo[$key]['card_number'] = $val['credit_card_no'];
                $profileIdArr[$val['profile_id']] = $val['credit_card_no'];
            }
        }
        $paymentModeCredit->get('existing_profile_id')->setAttribute('options', $profileIdArr);
        if ($request->isPost()) {
            $paymentModeCredit->setData($request->getPost());
            $isExistPayment = $request->getPost('exist_payment');
            $validationGroups = array();
            if (!empty($isExistPayment)) {
                $validationGroups = array('existing_profile_id', 'exist_amount');
            } else {
                $validationGroups = array('card_holder_name', 'card_number', 'expiry_month', 'expiry_year', 'amount');
                $isExistBilling = $request->getPost('is_exist_billing');
                if (empty($isExistBilling) || $isExistBilling == 0) {
                    $validationGroups[] = 'billing_address_1';
                    $validationGroups[] = 'billing_city_auth';
//$validationGroups[] = 'billing_state_auth';
                    $validationGroups[] = 'billing_zip_code_auth';
                    $validationGroups[] = 'billing_country_auth';
                }
            }
            $paymentModeCredit->setValidationGroup($validationGroups);
            $paymentModeCredit->setInputFilter($transaction->getInputFilterMakeAuthorizePayment());
            if (!$paymentModeCredit->isValid()) {
                $errors = $paymentModeCredit->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'continue') {
                        foreach ($row as $typeError => $rower) {
                            if ($typeError != 'notInArray') {
                                $msg [$key] = $msgArr[$rower];
                            }
                        }
                    }
                }
            }
            if (!empty($msg)) {
                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $postArr = $request->getPost()->toArray();

                $paymentInfo = array();
                $paymentInfo['card_holder_name'] = isset($postArr['card_holder_name']) ? $postArr['card_holder_name'] : "";
                $paymentInfo['card_number'] = $postArr['card_number'];
                $paymentInfo['expiry_year'] = $postArr['expiry_year'];
                $paymentInfo['expiry_month'] = $postArr['expiry_month'];
                $paymentInfo['cvv_number'] = $postArr['cvv_number'];
                $paymentInfo['billing_first_name'] = !empty($postArr['billing_first_name']) ? $postArr['billing_first_name'] : '';
                $paymentInfo['billing_last_name'] = !empty($postArr['billing_last_name']) ? $postArr['billing_last_name'] : '';
                $isExistBilling = $postArr['is_exist_billing'];
                $postArr['billing_state_auth'] = ($postArr['billing_country_auth'] == $uSId) ? $postArr['billing_state_auth_select'] : $postArr['billing_state_auth'];
                $address = '';
                $city = '';
                $state = '';
                $zipCode = '';
                $countryName = '';
                if (!empty($isExistBilling) && $isExistBilling == 1) {
                    if (!empty($postArr['pledge_transaction_id'])) {
                        $userId = $postArr['user_id'];
                        $user_address_info = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations(array('user_id' => $userId));
                        if (!empty($user_address_info)) {
                            $locType = array();
                            foreach ($user_address_info as $address1) {
                                $locationArr = explode(', ', $address1['location']);
                                if (in_array(1, $locationArr)) {
                                    $address = $address1['street_address_one'];
                                    $address .= (!empty($address1['street_address_two'])) ? ' ' . $address1['street_address_two'] : '';
                                    $city = $address1['city'];
                                    $state = $address1['state'];
                                    $zipCode = $address1['zip_code'];
                                    $countryName = $address1['country_name'];
                                }
                            }
                        } else {
                            $address = '';
                            $city = '';
                            $state = '';
                            $zipCode = '';
                            $countryName = '';
                        }
                    } else {
                        $address = $postArr['billing_address'];
                        $city = $postArr['billing_city'];
                        $state = $postArr['billing_state'];
                        $zipCode = $postArr['billing_zip_code'];
                        $countryName = $postArr['billing_country_text'];
                    }
                } else {
                    $address = $postArr['billing_address_1'] . " " . $postArr['billing_address_2'];
                    $city = $postArr['billing_city_auth'];
                    $state = $postArr['billing_state_auth'];
                    $zipCode = $postArr['billing_zip_code_auth'];
                    $countryName = $postArr['billing_country_auth_text'];
                }
                if (!empty($isExistPayment)) {
                    $amount = $postArr['exist_amount'];
                    $existingProfileId = !empty($postArr['existing_profile_id']) ? $postArr['existing_profile_id'] : '';
                } else {
                    $amount = $postArr['amount'];
                    $existingProfileId = '';
                }
                $paymentInfo['address'] = $address;
                $paymentInfo['city'] = $city;
                $paymentInfo['state'] = $state;
                $paymentInfo['zip_code'] = $zipCode;
                $paymentInfo['country_name'] = $countryName;
                $paymentInfo['user_id'] = $postArr['user_id'];
                $paymentInfo['amount'] = $amount;
                $paymentInfo['isSaveProfile'] = $postArr['is_save_profile'];
                $paymentInfo['pledge_transaction_id'] = $postArr['pledge_transaction_id'];
                $paymentInfo['existing_profile_id'] = $existingProfileId;

                $paymentInfo['credit_card_type_id'] = $creditCardType;
                $responseMessage = $this->makeAuthorizePayment($paymentInfo);
                $response->setContent(\Zend\Json\Json::encode($responseMessage));
                return $response;
            }
        }
    }

    /**
     * This function is used to make authorize net payment
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function makeAuthorizePayment($postArr = array()) {

        if (empty($postArr['existing_profile_id'])) {
            $userId = $postArr['user_id'];
            $userArr = array('user_id' => $userId);
            $contactDetail = array();
            $contactDetail[0] = $this->getServiceLocator()->get('User\Model\UserTable')->getUser($userArr);
            $createCustomerProfile = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
            $createCustomerProfile->createCustomerProfileRequest(array(
                'profile' => array(
                    'merchantCustomerId' => $contactDetail[0]['user_id'] . time(),
                    'email' => $contactDetail[0]['email_id'],
                    'paymentProfiles' => array(
                        'billTo' => array(
                            'firstName' => $postArr['card_holder_name'], //$postArr['billing_first_name'],
                            'lastName' => "", //$postArr['billing_last_name'],
                            'address' => $postArr['address'],
                            'city' => $postArr['city'],
                            'state' => $postArr['state'],
                            'zip' => $postArr['zip_code'],
                            'country' => $postArr['country_name']
                        ),
                        'payment' => array(
                            'creditCard' => array(
                                'cardNumber' => $postArr['card_number'],
                                'expirationDate' => $postArr['expiry_year'] . "-" . $postArr['expiry_month'],
                                'cardCode' => $postArr['cvv_number'],
                            ),
                        ),
                    ),
                )
            ));
            if ($createCustomerProfile->isSuccessful()) {
                $customerProfileId = $createCustomerProfile->customerProfileId;
                $paymentProfileId = $createCustomerProfile->customerPaymentProfileIdList->numericString;
                $shippingProfileId = $createCustomerProfile->customerShippingAddressIdList->numericString;

                $trans = array('transaction' => array('profileTransAuthOnly' => array(
                            'amount' => $postArr['amount'],
                            'customerProfileId' => $customerProfileId,
                            'customerPaymentProfileId' => $paymentProfileId,
//'customerShippingAddressId' => $shippingProfileId,
                            'order' => array(
                                'invoiceNumber' => 'SK' . time(),
                                'description' => 'description of transaction',
                                'purchaseOrderNumber' => 'No' . time()),
                            'taxExempt' => 'false',
                            'recurringBilling' => 'false'
                        )
                    ),
                    'extraOptions' => '<![CDATA[x_customer_ip = ' . $_SERVER['SERVER_ADDR'] . ']]>'
                );
                $payment = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
                $payment->createCustomerProfileTransactionRequest($trans);
                if ($payment->isSuccessful()) {
//$this->_auth->deletePaymentProfile($this->_memberId); // delete profile from database
//$delete = new AuthnetXML($options['authrize']['login'], $options['authrize']['password'], $options['authrize']['env']);
//$delete->deleteCustomerProfileRequest(array('customerProfileId' => $this->userFinancials->customerProfileId)); // delete profile from Autherize.net
///$date = date('Y-m-d H.i.s');
//$arrUser['user_id'] = $this->_memberId;
// $arrUser['gateway_type'] = 'A';
// $arrUser['profile_id'] = $customer_profile_id;
// $arrUser['tn_number'] = 0;
// $arrUser['payment_id'] = $payment_profile_id;
// $arrUser['billing_id'] = $shipping_profile_id;
//  $arrUser['modified_date'] = $date;
//  $arrUser['date_modified'] = $date;
//  $this->_auth->savePaymentProfile($arrUser);
//   $this->_success.= '<div class = "success">Your payment profile has updated successfully.</div>';

                    $responseArr = explode(',', $payment->directResponse);

                    $transactionId = $responseArr[6];

                    /* $trans = array('transaction' => array('profileTransPriorAuthCapture' => array(
                      'amount' => 1,
                      // 'customerProfileId' => $customerProfileId,
                      // 'customerPaymentProfileId' => $paymentProfileId,
                      'transId' => $transactionId
                      )
                      ),
                      'extraOptions' => '<![CDATA[x_customer_ip = ' . $_SERVER['SERVER_ADDR'] . ']]>'
                      );
                      $paymentCapture = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
                      $paymentCapture->createCustomerProfileTransactionRequest($trans); */

                    if (isset($postArr['isSaveProfile']) && $postArr['isSaveProfile'] == 1) {
                        $customerProfileId = $customerProfileId;
                        $dataArr['user_id'] = $contactDetail[0]['user_id'];
                        $dataArr['credit_card_type_id'] = !empty($postArr['credit_card_type_id']) ? $postArr['credit_card_type_id'] : '';
                        $dataArr['profile_id'] = $customerProfileId;
                        $dataArr['added_by'] = !empty($this->auth->getIdentity()->crm_user_id) ? $this->auth->getIdentity()->crm_user_id : '';
                        $dataArr['modified_by'] = !empty($this->auth->getIdentity()->crm_user_id) ? $this->auth->getIdentity()->crm_user_id : '';
                        $dataArr['credit_card_no'] = 'XXXX' . substr($postArr['card_number'], -4);

                        $this->getServiceLocator()->get('User\Model\UserTable')->insertTokenInfo($dataArr);
                    }

                    $typePayment = 'NewProfile';

                    $paymentMessage = $payment->messages->message->text;
                    /* For Pledge Transaction */
                    if (!empty($postArr['pledge_transaction_id'])) {
                        $pledgeTransactionMessage = $this->_config['Pledge_messages']['config']['Pledge_transaction_message'];
                        $pledgeController = new PledgeController();
                        $response = $this->getResponse();
                        $request = $this->getRequest();
                        $pledgeTransactionArr = array('pledge_transaction_id' => $postArr['pledge_transaction_id']);
                        $paymentTransactionId = $this->savePledgeTransaction($pledgeTransactionArr);
                        $dateTime = DATE_TIME_FORMAT;
                        $crm_user_id = !(empty($this->auth->getIdentity()->crm_user_id)) ? $this->auth->getIdentity()->crm_user_id : '';
                        $paymentTransactionData = array();
                        $paymentTransactionData['amount'] = $postArr['amount'];
                        $paymentTransactionData['transaction_id'] = $paymentTransactionId;
                        $paymentTransactionData['payment_mode_id'] = 1;
                        $paymentTransactionData['authorize_transaction_id'] = $transactionId;
                        $paymentTransactionData['profile_id'] = $customerProfileId;
                        $paymentTransactionData['added_by'] = $crm_user_id;
                        $paymentTransactionData['added_date'] = $dateTime;
                        $paymentTransactionData['modify_by'] = $crm_user_id;
                        $paymentTransactionData['modify_date'] = $dateTime;
                        $transaction = new Transaction($this->_adapter);
                        $paymentTransactionData = $transaction->getTransactionPaymentArr($paymentTransactionData);
                        $transactionPaymentId = $this->_transactionTable->saveTransactionPaymentDetail($paymentTransactionData);
                        $typePayment = 'PledgeTransaction';
                        $paymentMessage = $pledgeTransactionMessage['SUCCESSFULLY_UPDATED_PLEDGE'];
                    }
                    /* End for pledge Transaction */
                    $messages = array('status' => "success", 'message' => $paymentMessage, 'code' => $payment->messages->message->code, 'transaction_id' => $transactionId, 'typePayment' => $typePayment, 'customerProfileId' => (string) $customerProfileId, 'paymentProfileId' => (string) $paymentProfileId);
                } else {
                    $messages = array('status' => "credit_card_error", 'message' => $payment->messages->message->text, 'code' => $payment->messages->message->code);
                }
            } else {
                $messages = array('status' => "credit_card_error", 'message' => $createCustomerProfile->messages->message->text, 'code' => $createCustomerProfile->messages->message->code);
            }
        } else {
            $customerProfileId = $postArr['existing_profile_id'];
            $createCustomerProfile = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
            $createCustomerProfile->getCustomerProfileRequest(array('customerProfileId' => $customerProfileId));
            $paymentProfileId = $createCustomerProfile->profile->paymentProfiles->customerPaymentProfileId;
            $trans = array('transaction' => array('profileTransAuthOnly' => array(
                        'amount' => $postArr['amount'],
                        'customerProfileId' => $customerProfileId,
                        'customerPaymentProfileId' => $paymentProfileId,
//'customerShippingAddressId' => $shippingProfileId,
                        'order' => array(
                            'invoiceNumber' => 'SK' . time(),
                            'description' => 'description of transaction',
                            'purchaseOrderNumber' => 'No' . time()),
                        'taxExempt' => 'false',
                        'recurringBilling' => 'false'
                    )
                ),
                'extraOptions' => '<![CDATA[x_customer_ip = ' . $_SERVER['SERVER_ADDR'] . ']]>'
            );
            $payment = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
            $payment->createCustomerProfileTransactionRequest($trans);
            if ($payment->isSuccessful()) {
                $responseArr = explode(',', $payment->directResponse);
                $transactionId = $responseArr[6];
                $typePayment = 'ExistProfile';

                $paymentMessage = $payment->messages->message->text;
                /* For Pledge Transaction */
                if (!empty($postArr['pledge_transaction_id'])) {
                    $pledgeTransactionMessage = $this->_config['Pledge_messages']['config']['Pledge_transaction_message'];
                    $pledgeController = new PledgeController();
                    $response = $this->getResponse();
                    $request = $this->getRequest();
                    $pledgeTransactionArr = array('pledge_transaction_id' => $postArr['pledge_transaction_id']);
                    $paymentTransactionId = $this->savePledgeTransaction($pledgeTransactionArr);
                    $dateTime = DATE_TIME_FORMAT;
                    $crm_user_id = !(empty($this->auth->getIdentity()->crm_user_id)) ? $this->auth->getIdentity()->crm_user_id : '';
                    $paymentTransactionData = array();
                    $paymentTransactionData['amount'] = $postArr['amount'];
                    $paymentTransactionData['transaction_id'] = $paymentTransactionId;
                    $paymentTransactionData['payment_mode_id'] = 1;
                    $paymentTransactionData['authorize_transaction_id'] = $transactionId;
                    $paymentTransactionData['profile_id'] = $customerProfileId;
                    $paymentTransactionData['added_by'] = $crm_user_id;
                    $paymentTransactionData['added_date'] = $dateTime;
                    $paymentTransactionData['modify_by'] = $crm_user_id;
                    $paymentTransactionData['modify_date'] = $dateTime;
                    $transaction = new Transaction($this->_adapter);
                    $paymentTransactionData = $transaction->getTransactionPaymentArr($paymentTransactionData);
                    $transactionPaymentId = $this->_transactionTable->saveTransactionPaymentDetail($paymentTransactionData);
                    $typePayment = 'PledgeTransaction';
                    $paymentMessage = $pledgeTransactionMessage['SUCCESSFULLY_UPDATED_PLEDGE'];
                }
                /* End for pledge Transaction */
                $messages = array('status' => "success", 'message' => $paymentMessage, 'code' => $payment->messages->message->code, 'transaction_id' => $transactionId, 'typePayment' => $typePayment, 'customerProfileId' => (string) $customerProfileId, 'paymentProfileId' => (string) $paymentProfileId);
            } else {
                $messages = array('status' => "credit_card_error", 'message' => $payment->messages->message->text, 'code' => $payment->messages->message->code);
            }
        }
        return $messages;
    }

    /**
     * This function is used to save pledge Transaction
     * @param Void
     * @return String
     * @author Icreon Tech - DT
     */
    public function savePledgeTransaction($postArr) {

        $pledge = new Pledge($this->_adapter);
        $pledgeTransData = $this->getServiceLocator()->get('Pledge\Model\PledgeTable')->getPledgeTransactionsById('', '', $postArr['pledge_transaction_id']);

        $dateTime = DATE_TIME_FORMAT;

        $crmUserId = !(empty($this->auth->getIdentity()->crm_user_id)) ? $this->auth->getIdentity()->crm_user_id : '';
        /* Add transaction in main table */
        $finalTransaction = array();
        $finalTransaction['user_id'] = $pledgeTransData[0]['user_id'];
        $finalTransaction['transaction_source_id'] = $this->_config['transaction_source']['transaction_source_id'];
        $finalTransaction['num_items'] = 1;
        $finalTransaction['sub_total_amount'] = $pledgeTransData[0]['amount_pledge'];
        $finalTransaction['shipping_amount'] = 0;
        $finalTransaction['total_tax'] = 0;
        $finalTransaction['transaction_amount'] = $pledgeTransData[0]['amount_pledge'];
        $finalTransaction['transaction_date'] = $dateTime;
        $finalTransaction['transaction_type'] = '1';
        $finalTransaction['added_by'] = $crmUserId;
        $finalTransaction['added_date'] = $dateTime;
        $finalTransaction['modify_by'] = $crmUserId;
        $finalTransaction['modify_date'] = $dateTime;
        if (!empty($postArr['pledge_transaction_id'])) {
            $finalTransaction['don_amount'] = $finalTransaction['transaction_amount'];
        }
        $transaction = new Transaction($this->_adapter);
        $finalTransactionArr = $transaction->getFinalTransactionArr($finalTransaction);

        $transactionId = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->saveFinalTransaction($finalTransactionArr);

        /* End add transaction in main table */
        /* Add Donation Product */
        $donationProductData = array();
        $donationProductData['transaction_id'] = $transactionId;
        $donationProductData['pledge_transaction_id'] = $pledgeTransData[0]['pledge_transaction_id'];
        $donationProductData['is_pledge_transaction'] = '1';
        $donationProductData['product_mapping_id'] = 1;
        $donationProductData['product_type_id'] = 2;
        $donationProductData['product_price'] = $pledgeTransData[0]['amount_pledge'];
        $donationProductData['product_sub_total'] = $pledgeTransData[0]['amount_pledge'];
        $donationProductData['product_discount'] = 0;
        $donationProductData['product_tax'] = 0;
        $donationProductData['product_total'] = $pledgeTransData[0]['amount_pledge'];
        $donationProductData['product_status_id'] = 10;
        $donationProductData['purchase_date'] = $dateTime;
        $donationProductData['is_deleted'] = 0;
        $donationProductData['added_by'] = $crmUserId;
        $donationProductData['added_date'] = $dateTime;
        $donationProductData['modify_by'] = $crmUserId;
        $donationProductData['modified_date'] = $dateTime;
        $donationProductData['user_id'] = $pledgeTransData[0]['user_id'];
        $donationProductData['num_quantity'] = 1;

        $productSearchParam['id'] = 8;/** product Donetion */
        $productResultArr = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProductInfo($productSearchParam);
        $donationProductData['product_sku'] = $productResultArr[0]['product_sku'];
        $donationProductData['product_name'] = $productResultArr[0]['product_name'];

        $donationProductArray = $transaction->getDonationProductArray($donationProductData);
        $donationProductId = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->saveTransactionDonationProdcts($donationProductArray);
        /* End add Donation Product */


        /* Update pledge transaction status */
        $updatePledgeStatusArray = array();
        $updatePledgeStatusArray['pledge_transaction_id'] = $pledgeTransData[0]['pledge_transaction_id'];
        $updatePledgeStatusArray['status'] = '1';
        $updatePledgeStatusArray['transaction_id'] = $transactionId;
        $updatePledgeStatusArray['modify_by'] = $crmUserId;
        $updatePledgeStatusArray['modify_date'] = $dateTime;
        $updatePledgeTransactionStatusArr = $pledge->updatePledgeTransactionStatusArr($updatePledgeStatusArray);
        $pledgeTransactionId = $this->getServiceLocator()->get('Pledge\Model\PledgeTable')->updatePledgeTransactionStatus($updatePledgeTransactionStatusArr);
        /* End update pledge transaction status */

        $userPurchasedProductArr = array();
        $userPurchasedProductArr['user_id'] = $pledgeTransData[0]['user_id'];

        $date_range = $this->getDateRange(4);
        $userPurchasedProductArr['from_date'] = $this->DateFormat($date_range['from'], 'db_date_format_from');
        $userPurchasedProductArr['to_date'] = $this->DateFormat($date_range['to'], 'db_date_format_to');
        $getTotalUserPurchaseProductAmountArr = $transaction->getTotalUserPurchaseProductAmountArr($userPurchasedProductArr);
        $totalAmountPurchased = $this->_transactionTable->getTotalUserPurchased($getTotalUserPurchaseProductAmountArr);

        $userMembershipArr = array();
        $userMembershipArr['transaction_amount'] = $totalAmountPurchased;
        $userMembershipArr['donation_amount'] = $pledgeTransData[0]['amount_pledge'];
        $userMembershipArr['user_id'] = $pledgeTransData[0]['user_id'];
        $userMembershipArr['transaction_id'] = $transactionId;
        $userMembershipArr['transaction_date'] = $dateTime;

        $this->updateUserMembership($userMembershipArr);
        return $transactionId;
    }

    /**
     * This function is used to resize Image
     * @return     array
     * @author Icreon Tech - NS
     */
    public function resizeImage($new_image_name, $image, $resizeWidth, $resizeHeight) {

        list($imagewidth, $imageheight, $imageType) = getimagesize($image);
        $imageType = image_type_to_mime_type($imageType);


        $SMALL_IMAGE_MAX_WIDTH = $resizeWidth;
        $SMALL_IMAGE_MAX_HEIGHT = $resizeHeight;

        $source_aspect_ratio = $imagewidth / $imageheight;
        $thumbnail_aspect_ratio = $SMALL_IMAGE_MAX_WIDTH / $SMALL_IMAGE_MAX_HEIGHT;
        if ($imagewidth <= $SMALL_IMAGE_MAX_WIDTH && $imageheight <= $SMALL_IMAGE_MAX_HEIGHT) {
            $small_image_width = $imagewidth;
            $small_image_height = $imageheight;
        } elseif ($thumbnail_aspect_ratio > $source_aspect_ratio) {
            $small_image_width = (int) ($SMALL_IMAGE_MAX_HEIGHT * $source_aspect_ratio);
            $small_image_height = $SMALL_IMAGE_MAX_HEIGHT;
        } else {
            $small_image_width = $SMALL_IMAGE_MAX_WIDTH;
            $small_image_height = (int) ($SMALL_IMAGE_MAX_WIDTH / $source_aspect_ratio);
        }

        $newImageWidth = $small_image_width;
        $newImageHeight = $small_image_height;

        $newImage = imagecreatetruecolor($newImageWidth, $newImageHeight);

        switch ($imageType) {
            case "image/gif":
                $source = imagecreatefromgif($image);
                break;
            case "image/pjpeg":
            case "image/jpeg":
            case "image/jpg":
                $source = imagecreatefromjpeg($image);
                break;
            case "image/png":
            case "image/x-png":
                $source = imagecreatefrompng($image);
                break;
        }

        imagecopyresampled($newImage, $source, 0, 0, 0, 0, $newImageWidth, $newImageHeight, $imagewidth, $imageheight);
        switch ($imageType) {
            case "image/gif":
                imagegif($newImage, $new_image_name);
                break;
            case "image/pjpeg":
            case "image/jpeg":
            case "image/jpg":
                imagejpeg($newImage, $new_image_name, 90);
                break;
            case "image/png":
            case "image/x-png":
                imagepng($newImage, $new_image_name);
                break;
        }

        chmod($new_image_name, 0777);
        return true;
    }

    /**
     * This action is used to move file from temp to ads folder
     * @param filename
     * @return
     * @author Icreon Tech - NS
     */
    public function moveFileFromTempToFOF($filename) {
        if (isset($filename) and trim($filename) != "") {
            $temp_dir = $this->_config['file_upload_path']['temp_upload_dir'];
            $temp_dir_fof = $this->_config['file_upload_path']['temp_upload_dir'] . 'fof/';
            $temp_thumbnail_dir = $this->_config['file_upload_path']['temp_upload_thumbnail_dir'];
            $temp_upload_medium_dir = $this->_config['file_upload_path']['temp_upload_medium_dir'];
            $temp_upload_large_dir = $this->_config['file_upload_path']['temp_upload_large_dir'];

            $temp_file = $temp_dir . $filename;
            $temp_fof_file = $temp_dir_fof . $filename;
            $temp_thumbnail_file = $temp_thumbnail_dir . $filename;
            $temp_medium_file = $temp_upload_medium_dir . $filename;
            $temp_large_file = $temp_upload_large_dir . $filename;

            $fof_dir_root = $this->_config['file_upload_path']['assets_upload_dir'] . "fof/";
            $fof_dir_thumb = $this->_config['file_upload_path']['assets_upload_dir'] . "fof/thumb/";
            $fof_dir_medium = $this->_config['file_upload_path']['assets_upload_dir'] . "fof/medium/";
            $fof_dir_large = $this->_config['file_upload_path']['assets_upload_dir'] . "fof/large/";

            $fof_filename_large = $fof_dir_large . $filename;
            $fof_filename_medium = $fof_dir_medium . $filename;
            $fof_filename_thumb = $fof_dir_thumb . $filename;
            $fof_filename_root = $fof_dir_root . $filename;


            if (file_exists($temp_fof_file) && file_exists($temp_fof_file)) {
                if (copy($temp_fof_file, $fof_filename_root)) {
//                    $this->resizeImage($fof_filename_thumb, $temp_fof_file, 35, 35);
//                    $this->resizeImage($fof_filename_medium, $temp_fof_file, 50, 50);

                    $this->resizeImage($fof_filename_thumb, $temp_fof_file, $this->_config['file_upload_path']['user_fof_medium_width'], $this->_config['file_upload_path']['user_fof_medium_height']);
                    $this->resizeImage($fof_filename_medium, $temp_fof_file, $this->_config['file_upload_path']['user_fof_medium_width'], $this->_config['file_upload_path']['user_fof_medium_height']);
                    $this->resizeImage($fof_filename_large, $temp_fof_file, $this->_config['file_upload_path']['user_fof_large_width'], $this->_config['file_upload_path']['user_fof_large_height']);
//                    if (file_exists($temp_file)) {
//                        unlink($temp_file);
//                    }
//                    if (file_exists($temp_fof_file)) {
//                        unlink($temp_fof_file);
//                    }
//                    if (file_exists($temp_thumbnail_file)) {
//                        unlink($temp_thumbnail_file);
//                    }
//                    if (file_exists($temp_medium_file)) {
//                        unlink($temp_medium_file);
//                    }
//                    if (file_exists($temp_large_file)) {
//                        unlink($temp_large_file);
//                    }
                }
            }
        }
    }

    /**
     * This action is used to move file from temp to fof folder
     * @param filename
     * @return
     * @author Icreon Tech - NS
     */
    public function moveFileFromTempToFOFFrontEnd($filename) {
        try {
            if (isset($filename) and trim($filename) != "") {
                $temp_dir_fof = $this->_config['file_upload_path']['temp_upload_dir'] . 'fof/';
                $temp_file = $temp_dir_fof . $filename;

                $fof_dir_root = $this->_config['file_upload_path']['assets_upload_dir'] . "fof/";
                $fof_dir_thumb = $this->_config['file_upload_path']['assets_upload_dir'] . "fof/thumb/";
                $fof_dir_medium = $this->_config['file_upload_path']['assets_upload_dir'] . "fof/medium/";
                $fof_dir_large = $this->_config['file_upload_path']['assets_upload_dir'] . "fof/large/";
                $fof_filename_large = $fof_dir_large . $filename;
                $fof_filename_medium = $fof_dir_medium . $filename;
                $fof_filename_thumb = $fof_dir_thumb . $filename;
                $fof_filename_root = $fof_dir_root . $filename;


                if (isset($temp_file) and trim($temp_file) != "" and file_exists($temp_file)) {
                    if (copy($temp_file, $fof_filename_root)) {
                        $this->resizeImage($fof_filename_thumb, $fof_filename_root, $this->_config['file_upload_path']['user_fof_medium_width'], $this->_config['file_upload_path']['user_fof_medium_height']);
                        $this->resizeImage($fof_filename_medium, $fof_filename_root, $this->_config['file_upload_path']['user_fof_medium_width'], $this->_config['file_upload_path']['user_fof_medium_height']);
                        $this->resizeImage($fof_filename_large, $fof_filename_root, $this->_config['file_upload_path']['user_fof_large_width'], $this->_config['file_upload_path']['user_fof_large_height']);
                    }
                }
            }
        } catch (Exception $e) {

        }
    }

    /**
     * This action is used to add more notify
     * @param void
     * @return void
     * @author Icreon Tech - DT
     */
    public function shippingMethodFrontAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getTransactionTable();
        $createTransactionMessage = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $transaction = new Transaction($this->_adapter);
        $applicationConfigIds = $this->_config['application_config_ids'];
        $uSId = $applicationConfigIds['united_state_country_id'];
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $createTransactionForm = new CheckoutFormFront();
            $postArr = $request->getPost()->toArray();

            /* if (isset($postArr['is_apo_po']) && $postArr['is_apo_po'] == '1') {
              $getShippingMethods = $this->_config['transaction_config']['usps_iop_config_info']['shippings_types'];
              } else if (isset($postArr['shipping_country']) && $postArr['shipping_country'] == 228) {
              $getShippingMethods = $this->_config['transaction_config']['domestic_config_info']['shippings_types'];
              } else {
              $getShippingMethods = $this->_config['transaction_config']['international_config_info']['shippings_types'];
              }

              $shippingMethods = array();
              if (!empty($getShippingMethods)) {
              foreach ($getShippingMethods as $shippingMet) {
              $key = $shippingMet['carrier_id'] . ', ' . $shippingMet['service_type'];
              $webSelection = (!empty($shippingMet['web_selection'])) ? ' ( ' . $shippingMet['web_selection'] . ' )' : '';
              $shippingMethods[$key] = $shippingMet['description'] . $webSelection;
              }
              }

              $createTransactionForm->get('shipping_method')->setAttribute('options', $shippingMethods);
             *
             */
            $shippingMethodInter = '';$is_apo_po = 1;
            if (isset($postArr['is_apo_po']) && $postArr['is_apo_po'] != '0' && $postArr['is_apo_po'] != '1') {
//$getShippingMethods = $this->_config['transaction_config']['usps_iop_config_info']['shippings_types'];
                $shippingMetArr = array();
                $shippingMetArr['pb_shipping_id'] = $applicationConfigIds['apo_po_pb_id'];
                $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                $getShippingMethods = $this->_transactionTable->getShippingMethod($shippingMethodArr);
		$is_apo_po = 2;
            } else if (isset($postArr['shipping_country']) && $postArr['shipping_country'] == $uSId && $postArr['is_apo_po'] == '1') {
//$getShippingMethods = $this->_config['transaction_config']['domestic_config_info']['shippings_types'];
                $shippingMetArr = array();
                $shippingMetArr['pb_shipping_id'] = $applicationConfigIds['domestic_pb_id'];
                $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                $getShippingMethods = $this->_transactionTable->getShippingMethod($shippingMethodArr);
            } else {
                $cartProductArr = array();
                $cartProductArr['user_id'] = $postArr['user_id'];
                $cartProductArr['userid'] = $postArr['user_id'];
                $cartProductArr['source_type'] = $postArr['source_type'];
                $cartProductsWeight = $this->cartTotalWithTax($cartProductArr);
                /*$totalWeight = 0;
                if (!empty($cartProductsWeight)) {
                    foreach ($cartProductsWeight as $cartProductWei) {
                        if ($cartProductWei['product_type_id'] == 1) {
                            $totalWeight+=$cartProductWei['product_weight'];
                        }
                    }
                }*/
				$totalWeight = (isset($cartProductsWeight['totalweight']) && $cartProductsWeight['totalweight']>0)?$cartProductsWeight['totalweight']:'0.00';
                $shippingMetArr = array();
                $shippingMetArr['pb_shipping_id'] = $applicationConfigIds['internation_pb_id'];
                $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                $getShippingMethodsInter = $this->_transactionTable->getShippingMethod($shippingMethodArr);
//$lbsCheck = $this->_config['transaction_config']['international_config_info']['lbs_check'];
                $lbsCheck = $getShippingMethodsInter[0]['lbs_check'];
                if ($totalWeight <= $lbsCheck) {
//$getShippingMethods = $this->_config['transaction_config']['international_config_info']['shipping_types_first_class'];
                    foreach ($getShippingMethodsInter as $shipMethod) {
                        if ($shipMethod['pb_shipping_type_id'] == $applicationConfigIds['internation_less_than_lbs_id']) {
                            $getShippingMethods[] = $shipMethod;

							$postArr['shipping_method'] = $shipMethod['carrier_id'] . ', ' . $shipMethod['service_type'] . ', ' . $shipMethod['pb_shipping_type_id'];
                        }
                    }
                } else {
//$getShippingMethods = $this->_config['transaction_config']['international_config_info']['shipping_types_priority'];
                    foreach ($getShippingMethodsInter as $shipMethod) {
                        if ($shipMethod['pb_shipping_type_id'] == $applicationConfigIds['internation_greater_than_lbs_id']) {
                            $getShippingMethods[] = $shipMethod;

							$postArr['shipping_method'] = $shipMethod['carrier_id'] . ', ' . $shipMethod['service_type'] . ', ' . $shipMethod['pb_shipping_type_id'];
                        }
                    }
                }
                $shippingMethodInter = $postArr['shipping_method'];
            }

            $shippingMethods = array();
            if (!empty($getShippingMethods)) {
                foreach ($getShippingMethods as $shippingMet) {
                    $key = $shippingMet['carrier_id'] . ', ' . $shippingMet['service_type'] . ', ' . $shippingMet['pb_shipping_type_id'];
                    $webSelection = (!empty($shippingMet['web_selection'])) ? ' ' . $shippingMet['web_selection'] . ' ' : '';
                    $shippingMethods[$key] = $webSelection;
                }
            }
            $createTransactionForm->get('shipping_method')->setAttribute('options', $shippingMethods);
            $createTransactionForm->get('shipping_method')->setValue($shippingMethodInter);

            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => $createTransactionMessage,
                'createTransactionForm' => $createTransactionForm,
		'is_apo_po' => $is_apo_po
            ));
            return $viewModel;
        } else {
//  return $this->redirect()->toRoute('get-crm-transactions');
        }
    }

    /**
     * This action is used to get shipping method
     * @param void
     * @return void
     * @author Icreon Tech - DT
     */
    public function getShippingMethodAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $applicationConfigIds = $this->_config['application_config_ids'];
        $uSId = $applicationConfigIds['united_state_country_id'];
        if ($request->isXmlHttpRequest()) {
            $this->getTransactionTable();
            $createTransactionMessage = $this->_config['transaction_messages']['config']['create_crm_transaction'];
            $createTransactionForm = new CreateTrasactionForm();
            $transaction = new Transaction($this->_adapter);
            $postArr = $request->getPost()->toArray();
            /* Shipping methods */
            if (isset($postArr['is_apo_po']) && $postArr['is_apo_po'] == '1') {
//$getShippingMethods = $this->_config['transaction_config']['usps_iop_config_info']['shippings_types'];
                $shippingMetArr = array();
                $shippingMetArr['pb_shipping_id'] = $applicationConfigIds['apo_po_pb_id'];
                $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                $getShippingMethods = $this->_transactionTable->getShippingMethod($shippingMethodArr);
            } else if (isset($postArr['shipping_country']) && $postArr['shipping_country'] == $uSId) {
//$getShippingMethods = $this->_config['transaction_config']['domestic_config_info']['shippings_types'];
                $shippingMetArr = array();
                $shippingMetArr['pb_shipping_id'] = $applicationConfigIds['domestic_pb_id'];
                $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                $getShippingMethods = $this->_transactionTable->getShippingMethod($shippingMethodArr);
            } else {
                $cartProductArr = array();
                $cartProductArr['user_id'] = $postArr['user_id'];
                $cartProductArr['userid'] = $postArr['user_id'];
                $cartProductArr['source_type'] = 'backend';
                $cartProductsWeight = $this->cartTotalWithTax($cartProductArr);
                $totalWeight = 0;
                /*if (!empty($cartProductsWeight)) {
                    foreach ($cartProductsWeight as $cartProductWei) {
                        if ($cartProductWei['product_type_id'] == 1) {
                            $totalWeight+=$cartProductWei['product_weight'];
                        }
                    }
                }*/
                $totalWeight = (isset($cartProductsWeight['totalweight']) && $cartProductsWeight['totalweight']>0)?$cartProductsWeight['totalweight']:'0.00';
                $shippingMetArr = array();
                $shippingMetArr['pb_shipping_id'] = $applicationConfigIds['internation_pb_id'];
                $shippingMethodArr = $transaction->getShippingMethodArr($shippingMetArr);
                $getShippingMethodsInter = $this->_transactionTable->getShippingMethod($shippingMethodArr);
//$lbsCheck = $this->_config['transaction_config']['international_config_info']['lbs_check'];
                $lbsCheck = $getShippingMethodsInter[0]['lbs_check'];
                if ($totalWeight <= $lbsCheck) {
//$getShippingMethods = $this->_config['transaction_config']['international_config_info']['shipping_types_first_class'];
                    foreach ($getShippingMethodsInter as $shipMethod) {
                        if ($shipMethod['pb_shipping_type_id'] == $applicationConfigIds['internation_less_than_lbs_id']) {
                            $getShippingMethods[] = $shipMethod;
                        }
                    }
                } else {
//$getShippingMethods = $this->_config['transaction_config']['international_config_info']['shipping_types_priority'];
                    foreach ($getShippingMethodsInter as $shipMethod) {
                        if ($shipMethod['pb_shipping_type_id'] == $applicationConfigIds['internation_greater_than_lbs_id']) {
                            $getShippingMethods[] = $shipMethod;
                        }
                    }
                }
                $postArr['shipping_method'] = $getShippingMethods[0]['carrier_id'] . ', ' . $getShippingMethods[0]['service_type'] . ', ' . $getShippingMethods[0]['pb_shipping_type_id'];
                $shippingMethodInter = $postArr['shipping_method'];
            }

            $shippingMethods = array();
            if (!empty($getShippingMethods)) {
                foreach ($getShippingMethods as $shippingMet) {
                    $key = $shippingMet['carrier_id'] . ', ' . $shippingMet['service_type'] . ', ' . $shippingMet['pb_shipping_type_id'];
                    $webSelection = (!empty($shippingMet['web_selection'])) ? ' ' . $shippingMet['web_selection'] . ' ' : '';
                    $shippingMethods[$key] = $webSelection;
                }
            }
            $createTransactionForm->get('shipping_method')->setAttribute('options', $shippingMethods);
            $createTransactionForm->get('shipping_method')->setValue($postArr['shipping_method']);
            /* End Shipping methods */
            /* End Product Copon discount */
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => array_merge($createTransactionMessage, $this->_config['transaction_messages']['config']['common_message']),
                'createTransactionForm' => $createTransactionForm,
                'isPickUp' => (!empty($postArr['pick_up'])) ? $postArr['pick_up'] : '0'
            ));
            return $viewModel;
        }
    }

    /**
      /**
     * This action is used to approve bio certificate details
     * @return json
     * @param void
     * @author Icreon Tech - AS
     */
    public function viewBioDetailsAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $bioCertificateForm = new BioCertificate();
        $messages = array();
        $this->getTransactionTable();
        $this->getConfig();
        $messages = $this->_config['transaction_messages']['config']['view_crm_transaction'];
        $viewModel = new ViewModel();
        $this->layout('popup');
        $params = $this->params()->fromRoute();
        $searchResult = $this->getServiceLocator()->get('User\Model\UserTable')->getBioWoh(array('bio_certificate_product_id' => $params['id']));
        if ($request->isPost()) {
            $data = $request->getPost();
            $approve['bio_certificate_id'] = $params['id'];
            $approve['name'] = $data['bio_name'];
            $approve['immigrated_from'] = $data['immigrated_from'];
            $approve['ship_method'] = $data['method_of_travel'];
            $approve['port_of_entry'] = $data['port_of_entry'];
            $approve['additional_info'] = $data['additional_info'];
            $approve['woh_id'] = $data['bio_wohid'];
            $approve['finalize'] = '1';
            $approve['date_of_entry'] = $this->DateFormat($data['date_of_entry_to_us']);
            $approve['modified_by'] = $this->auth->getIdentity()->crm_user_id;
            $approve['modified_date'] = DATE_TIME_FORMAT;
            $approve['status'] = '4';
            $this->getServiceLocator()->get('User\Model\UserTable')->updateBioWoh($approve);
            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
        $bioCertificateForm->get('bio_wohid')->setAttribute('value', $searchResult[0]['woh_id']);
        $bioCertificateForm->get('bio_name')->setAttribute('value', $searchResult[0]['name']);
        $bioCertificateForm->get('immigrated_from')->setAttribute('value', $searchResult[0]['immigrated_from']);
        $bioCertificateForm->get('method_of_travel')->setAttribute('value', $searchResult[0]['method_of_travel']);
        $bioCertificateForm->get('port_of_entry')->setAttribute('value', $searchResult[0]['port_of_entry']);
        $date_arr = explode(' ', $searchResult[0]['date_of_entry_to_us']);
        $searchResult[0]['date_of_entry_to_us'] = str_replace('-', '/', $date_arr[0]);
        $bioCertificateForm->get('date_of_entry_to_us')->setAttribute('value', $searchResult[0]['date_of_entry_to_us']);
        $bioCertificateForm->get('additional_info')->setAttribute('value', $searchResult[0]['additional_info']);
        if ($searchResult[0]['is_finalize'] == '1') {
            $bioCertificateForm->get('bio_name')->setAttribute('readonly', 'readonly');
            $bioCertificateForm->get('immigrated_from')->setAttribute('readonly', 'readonly');
            $bioCertificateForm->get('method_of_travel')->setAttribute('readonly', 'readonly');
            $bioCertificateForm->get('port_of_entry')->setAttribute('readonly', 'readonly');
            $bioCertificateForm->get('date_of_entry_to_us')->setAttribute('readonly', 'readonly');
            $bioCertificateForm->get('additional_info')->setAttribute('readonly', 'readonly');
        }
        $viewModel->setVariables(array(
            'searchResult' => $searchResult[0],
            'jsLangTranslate' => $messages,
            'bioCertificateForm' => $bioCertificateForm,
            'bio_id' => $params['id']
        ));
        return $viewModel;
    }

    /**
      /**
     * This action is used to download zip
     * @return json
     * @param void
     * @author Icreon Tech - AS
     */
    public function downloadZipAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $this->getTransactionTable();
        $this->getConfig();
        $params = $this->params()->fromRoute();
        $searchProduct = $this->getTransactionTable()->getShipmentDetails($params);
        $productType = explode(",", $searchProduct[0]['pro_type']);
        $productId = explode(",", $searchProduct[0]['pro_id']);
        $zipPath = $this->_config['file_upload_path']['assets_upload_dir'] . "transaction/shipment_" . $this->encrypt($params['shipmentId']);
        mkdir($zipPath, 0777);
        foreach ($productType as $key => $val) {
            switch ($val) {
                case '1':
                    $params['product_id'] = $productId[$key];
                    $this->passengerCertificateSavePdf($params);
                    break;
                case '10':
                    $params['product_id'] = $productId[$key];
                    $this->letterSavePdf($params, "F");
                    $this->certificateSavePdf($params, "F");
                    break;
            }
        }
        $zip = new ZipStream("shipment_" . $params['shipmentId'] . ".zip");
        $handle = opendir($zipPath);
        if ($handle) {
            while (false !== ($file = readdir($handle))) {
                if (($file != '.') && ($file != '..')) {
                    $zip->addFile(file_get_contents($zipPath . "/" . $file), $file, filectime($zipPath . "/" . $file));
                }
            }
        }
        $zip->finalize();
        if (is_dir($zipPath)) {
            $files = array_diff(scandir($zipPath), array('.', '..'));
            foreach ($files as $file) {
                (is_dir("$zipPath/$file")) ? delTree("$zipPath/$file") : unlink("$zipPath/$file");
            }
            rmdir($zipPath);
        }
        exit;
    }

    public function letterSavePdf($params = array(), $mode) {
        error_reporting(0);
        $this->getTransactionTable();
        $messages = $this->_config['transaction_messages']['config']['view_crm_transaction'];
        $this->layout('popup');
        $search['woh_id'] = $params['product_id'];
        $searchResult = $this->getServiceLocator()->get('User\Model\UserTable')->getWoh($search);
        $searchResult = $searchResult[0];
        $pdfPath = $this->_config['file_upload_path']['assets_upload_dir'] . "transaction/shipment_" . $this->encrypt($params['shipmentId']);
        $str = '<div style="width:85%; padding-top:235px; padding-left:80px; padding-right:20px; font-size:15px; font-family:Arial; line-height:19px;">
    <div>
        <div>
            <div>
                <div><div>' . $this->DateFormat($searchResult['purchase_date'], 'displaymonth') . '</div><div style=" margin-top:6px;">';
        if (trim($searchResult['title']) != '')
            $str.=$searchResult['title'] . '&nbsp';

        $str.=$searchResult['full_name'] . '</div>';

        if (!empty($searchResult['address']) or !empty($searchResult['city']) or !empty($searchResult['state']) or !empty($searchResult['zip_code']) or !empty($searchResult['user_country'])) {

            if (!empty($searchResult['address'])) {
                $str.= '<div style="width:50%;">' . $searchResult['address'] . '</div>';
            }

            if (!empty($searchResult['city']) or !empty($searchResult['state']) or !empty($searchResult['zip_code'])) {

                $str.= '<div style="width:50%;">';

                if (!empty($searchResult['city'])) {
                    $str.= $searchResult['city'];
                }

                if (!empty($searchResult['state'])) {
                    if (!empty($searchResult['city'])) {
                        $str.= ',' . $searchResult['state'] . ' ';
                    } else {
                        $str.= '' . $searchResult['state'] . ' ';
                    }
                }

                if (!empty($searchResult['zip_code'])) {
                    $str.= $searchResult['zip_code'];
                }

                $str.= '</div>';
            }

            if (!empty($searchResult['user_country'])) {
                $str.= '<div style="width:50%;">' . $searchResult['user_country'] . '</div>';
            }

            $str.= '</div>';
        } else {
            $str.= '</div>';
        }
        $str.= '<div style="padding-top:12px;">' . $messages['T_LETTER_CONTENT'] . '</div>
        <div style="padding-top:-5px;">' . $messages['T_LETTER_CONTENT1'] . '</div>
        <div>
            <div style="margin-top:165px; margin-left:300px;" align="right">' . $messages['YOUR_CONTRIBUTION'] . "<span style='margin-left:100px;'>" . "$ " . $searchResult['product_total'] . '</span></div>
            <div style="padding-top:15px;">';
        if (trim($searchResult['title']) != '')
            $str.=$searchResult['title'] . '&nbsp';

        $str.=$searchResult['full_name'] . '</div>';
        if (!empty($searchResult['address']) or !empty($searchResult['city']) or !empty($searchResult['state']) or !empty($searchResult['zip_code']) or !empty($searchResult['user_country'])) {

            if (!empty($searchResult['address'])) {
                $str.= '<div style="width:50%;">' . $searchResult['address'] . '</div>';
            }

            if (!empty($searchResult['city']) or !empty($searchResult['state']) or !empty($searchResult['zip_code'])) {

                $str.= '<div style="width:50%;">';

                if (!empty($searchResult['city'])) {
                    $str.= $searchResult['city'];
                }

                if (!empty($searchResult['state'])) {
                    if (!empty($searchResult['city'])) {
                        $str.= ',' . $searchResult['state'] . ' ';
                    } else {
                        $str.= '' . $searchResult['state'] . ' ';
                    }
                }

                if (!empty($searchResult['zip_code'])) {
                    $str.= $searchResult['zip_code'];
                }
                $str.= '</div>';
            }

            if (!empty($searchResult['user_country'])) {
                $str.= '<div style="width:50%;">' . $searchResult['user_country'] . '</div>';
            }

            $str.= '</div>';
        } else {
            $str.= '</div>';
        }
        $str.= '<div style="padding-top:20px;">' . $messages['T_LETTER_THANK'] . '</div>
<div style="padding-top:10px; padding-bottom:10px;">
    <label><strong>' . $searchResult['final_name'] . '</strong></label>
    <span style="margin-left:100px;"><strong>' . $searchResult['origin_country'] . '</strong></span>
</div>
<div>' . $messages['T_INFO'] . '</div></div>
    <div style="margin-top:20px; font-size:12px;" align="right"> 0 :' . $searchResult['transaction_id'] . '&nbsp;&nbsp;&nbsp;&nbsp; C :' . $searchResult['contact_id'] . '&nbsp;&nbsp;&nbsp;&nbsp;</div>
</div>
</div>
</div>';
        $format = array(16 * 25.4, 8.5 * 25.4);
        try {
            $html2pdf = new\ HTML2PDF('P', $format, 'en');
            $html2pdf->setDefaultFont('Arial');
            $html2pdf->writeHTML($str, false);
            $html2pdf->Output($pdfPath . "/" . date('Y-m-d') . "_" . $params['product_id'] . "_woh_letter.pdf", $mode);
        } catch (HTML2PDF_exception $e) {
            echo $e;
            exit;
        }
    }

    public function certificateSavePdf($params = array(), $mode) {
        error_reporting(0);
        $this->getTransactionTable();
        $messages = $this->_config['transaction_messages']['config']['view_crm_transaction'];
        $pdfPath = $this->_config['file_upload_path']['assets_upload_dir'] . "transaction/shipment_" . $this->encrypt($params['shipmentId']);
        $search['woh_id'] = $params['product_id'];
        $searchResult = $this->getServiceLocator()->get('User\Model\UserTable')->getWoh($search);
        $searchResult = $searchResult[0];
        $viewModel = new ViewModel();
        $str = '
<div style="padding-top:415px;">
    <div align="center" style="font-family:Times New Roman, Times, serif;  font-size: 20px;  color: #000; font-style:italic; padding-bottom: 10px; margin-left:-30px;">' . $messages['T_CERTIFICATE_1'] . '</div>';
        if ($searchResult['origin_country'] != 'United States of America') {
            if ($searchResult['product_name'] == 'Special Name entry') {
                $str = $str . '<div style="text-align:center; font-size:31px; line-height:36px; font-family:times new roman,Times, serif; font-weight:bold; color;#000;">' . $searchResult['final_name'] . '</div>
    <div align="center" style="font-family:Vijaya, Times New Roman, Times, serif;  font-size: 19px; margin-left:18px;  color: #000; font-style:italic; padding-bottom:5px;">' . $messages['T_CERTIFICATE_2'] . '</div>
    <div align="center" style="font-size: 31px;  font-family:times new roman,Times, serif; font-weight:bold; text-align:center;  color;#000; padding-bottom:5px;">' . $searchResult['origin_country'] . '</div>
    <div align="center" style="font-family:Times New Roman, Times, serif; font-size: 15px; color: #000; padding-left:25px;">' . $messages['T_CERTIFICATE_3'] . '</div>';
            } else {
                $str = $str . '
    <div style="text-align:center; font-size:31px; font-family:times new roman,Times, serif; font-weight:bold; color;#000; padding-bottom: 10px; padding-top:4px;">' . $searchResult['final_name'] . '</div>
    <div align="center" style="font-family:Vijaya, Times New Roman, Times, serif;  font-size: 20px;  color: #000; font-style:italic; padding-bottom:10px; padding-top:10px; padding-left:15px; text-align:center;">' . $messages['T_CERTIFICATE_2'] . '</div>
    <div align="center" style="font-size: 31px;  font-family:times new roman,Times, serif; font-weight:bold; text-align:center;  color;#000; padding-bottom:10px;">' . $searchResult['origin_country'] . '</div>
    <div align="center" style="font-family:Times New Roman, Times, serif; font-size: 15px; color: #000;">' . $messages['T_CERTIFICATE_3'] . '</div>';
            }
        } else {
            if ($searchResult['product_name'] == 'Special Name entry') {
                $str = $str . '
    <div style="text-align:center; font-size:30px; font-family:times new roman,Times, serif; font-weight:bold; color;#000; padding-top: 40px;">' . $searchResult['final_name'] . '</div>
    <div align="center" style="font-family:Times New Roman, Times, serif; font-size: 15px; color: #333; padding-top: 50px;">' . $messages['T_CERTIFICATE_4'] . '</div>';
            } else {
                $str = $str . '
    <div style="text-align:center; font-size:30px; font-family:times new roman,Times, serif; font-weight:bold; color;#000; padding-top: 42px; margin-left:-25px;">' . $searchResult['final_name'] . '</div>
    <div align="center" style="font-family:Times New Roman, Times, serif; font-size: 15px; color: #333; padding-top: 50px; margin-left:-20px;">' . $messages['T_CERTIFICATE_4'] . '</div>';
            }
        }
        $str = $str . '</div>';
        try {
            $html2pdf = new\ HTML2PDF('L', 'A4', 'en');
            $html2pdf->setDefaultFont('Times');
            $html2pdf->writeHTML($str, false);
            $html2pdf->Output($pdfPath . "/" . date('Y-m-d') . "_" . $params['product_id'] . "_woh_ceritificate.pdf", $mode);
        } catch (HTML2PDF_exception $e) {
            echo $e;
            exit;
        }
    }

    public function passengerCertificateSavePdf($params = array()) {
        $pdfPath = $this->_config['file_upload_path']['assets_upload_dir'] . "transaction/shipment_" . $this->encrypt($params['shipmentId']);
        error_reporting(0);
        $this->getTransactionTable();
        $messages = $this->_config['transaction_messages']['config']['view_crm_transaction'];
        $search['product_id'] = $params['product_id'];
        $searchResult = $this->getTransactionTable()->getShipImage($search);
        $searchParam['passenger_id'] = $searchResult[0]['item_id'];
        $searchResult = $this->getServiceLocator()->get('Passenger\Model\DocumentTable')->searchCrmPassenger($searchParam);
        $maritalStatusArray = $this->maritalStatusArray();
        $searchResult[0]['PRIN_MARITAL_STAT'] = (isset($searchResult[0]['PRIN_MARITAL_STAT']) ? $maritalStatusArray[$searchResult[0]['PRIN_MARITAL_STAT']] : "Unknown");
        $genderArray = $this->genderArray();
        $searchResult[0]['PRIN_GENDER_CODE'] = (isset($searchResult[0]['PRIN_GENDER_CODE']) ? $genderArray[$searchResult[0]['PRIN_GENDER_CODE']] : "Unknown");
        $searchResult = $searchResult[0];
//$pdf = new HTML2FPDF();
        $viewModel = new ViewModel();

        $dateArray = date_parse_from_format("Y-m-d", $searchResult['DATE_ARRIVE']);
        if ($searchResult['data_source'] == 2 && ($dateArray['month'] == 1 or $dateArray['month'] == '01') && ($dateArray['day'] == 1 or $dateArray['day'] == '01')) {
            $dateArr = $dateArray['year'];
        } else {
            if ($searchResult['DATE_ARRIVE'] != '' && $searchResult['DATE_ARRIVE'] != '0000-00-00 00:00:00') {
                $dateArr = $this->OutputDateFormat($searchResult['DATE_ARRIVE'], 'displaymonthformat2');
            }
        }
//  $pdf->AddPage();
        $str = '<page>
    <table style="width:100%; padding-top:266px" align="center" cellspacing="5">
        <tr>
            <td style="font-size:14px;"> ' . $messages['FIRST_NAME'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult['PRIN_FIRST_NAME'] . ' </td>
        </tr>
        <tr>
            <td style="font-size:14px;"> ' . $messages['LAST_NAME'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult['PRIN_LAST_NAME'] . ' </td>
        </tr>
        <tr>
            <td style="font-size:14px;"> ' . $messages['NATIONALITY'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult['PRIN_NATIONALITY'] . ' </td>
        </tr>
        <tr>
            <td style="font-size:14px;"> ' . $messages['LAST_RESIDENCE'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult['PRIN_PLACE_RESI'] . ' </td>
        </tr>
        <tr>
            <td style="font-size:14px;"> ' . $messages['DATE_ARRIVAL'] . ' : </td>
            <td style="font-size:14px;"> ' . $dateArr . ' </td>
        </tr>
        <tr>
            <td style="font-size:14px;"> ' . $messages['AGE_ARRIVAL'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult['PRIN_AGE_ARRIVAL'] . ' </td>
        </tr>
        <tr>
            <td style="font-size:14px;"> ' . $messages['GENDER'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult['PRIN_GENDER_CODE'] . ' </td>
        </tr>
        <tr>
            <td style="font-size:14px;"> ' . $messages['MARITAL_STATUS'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult['PRIN_MARITAL_STAT'] . ' </td>
        </tr>
        <tr>
            <td style="font-size:14px;"> ' . $messages['SHIP_TRAVEL'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult['SHIP_NAME'] . ' </td>
        </tr>
        <tr>
            <td style="font-size:14px;"> ' . $messages['PORT_DEPARTURE'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult['DEP_PORT_NAME'] . ' </td>
        </tr>
        <tr>
            <td style="font-size:14px;"> ' . $messages['MANIFEST_LINE_NUMBER'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult['REF_PAGE_LINE_NBR'] . ' </td>
        </tr>
    </table>
    </page>';
        try {
            $html2pdf = new\ HTML2PDF('L', 'A4', 'en');
            $html2pdf->setDefaultFont('Times');
            $html2pdf->writeHTML($str, false);
            $html2pdf->Output($pdfPath . "/" . date('Y-m-d') . "_" . $params['product_id'] . "_passenger_ceritificate.pdf", F);
        } catch (HTML2PDF_exception $e) {
            echo $e;
            exit;
        }
    }

    /**
     * This function is used to insert / update  the billing and shipping address
     * @return json
     * @param void
     * @author Icreon Tech - KK
     */
    public function insertUpdateBillingShipping($formData) {
        if ($formData['address_type'] == 'billing') {
            $form_address['title'] = isset($formData['billing_title']) ? $formData['billing_title'] : null;
            $form_address['first_name'] = isset($formData['billing_first_name']) ? $formData['billing_first_name'] : null;
            $form_address['last_name'] = isset($formData['billing_last_name']) ? $formData['billing_last_name'] : null;

            $form_address['address_id'] = isset($formData['billing_existing_contact']) ? $formData['billing_existing_contact'] : null;
            $form_address['addressinfo_country'] = isset($formData['billing_country']) ? $formData['billing_country'] : null;
            $form_address['addressinfo_zip'] = isset($formData['billing_zip_code']) ? $formData['billing_zip_code'] : null;
            $form_address['addressinfo_state'] = isset($formData['billing_state']) ? $formData['billing_state'] : null;
            $form_address['addressinfo_city'] = isset($formData['billing_city']) ? $formData['billing_city'] : null;
            $form_address['addressinfo_street_address_2'] = isset($formData['billing_address_2']) ? $formData['billing_address_2'] : null;
            if ($formData['source_type'] == 'backend') {

                $form_address['addressinfo_street_address_1'] = isset($formData['billing_address']) ? $formData['billing_address'] : null;
            } else {
                $form_address['addressinfo_street_address_1'] = isset($formData['billing_address_1']) ? $formData['billing_address_1'] : null;
            }

            $form_address['addressinfo_location_type'] = isset($formData['billing_location_type']) ? $formData['billing_location_type'] : null;

            if (!empty($form_address['address_id'])) {
                $form_address['addressinfo_location'] = $form_address['addressinfo_location_type'];
            } else {
                $form_address['addressinfo_location'] = 2;
            }
            if (isset($formData['billing_phone_info_id']) and trim($formData['billing_phone_info_id']) != "" and is_numeric(trim($formData['billing_phone_info_id']))) {
                $paramsPhoneUpdate = array();
                $paramsPhoneUpdate['phone_id'] = trim($formData['billing_phone_info_id']);
                $paramsPhoneUpdate['phone_type'] = "";
                $paramsPhoneUpdate['country_code'] = $formData['billing_country_code'];
                $paramsPhoneUpdate['area_code'] = $formData['billing_area_code'];
                $paramsPhoneUpdate['phone_no'] = $formData['billing_phone'];
                $paramsPhoneUpdate['continfo_primary'] = "";
                $paramsPhoneUpdate['extension'] = "";
                $paramsPhoneUpdate['modified_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('User\Model\UserTable')->updatePhoneInformations($paramsPhoneUpdate);
            } else {
                $paramsPhoneInsert = array();
                $paramsPhoneInsert['user_id'] = $formData['user_id'];
                $paramsPhoneInsert['phone_type'] = "";
                $paramsPhoneInsert['country_code'] = $formData['billing_country_code'];
                $paramsPhoneInsert['area_code'] = $formData['billing_area_code'];
                $paramsPhoneInsert['phone_no'] = $formData['billing_phone'];

                if ($formData['address_type_status'] == "2") {
                    $paramsPhoneInsert['continfo_primary'] = "1";
                } else {
                    $paramsPhoneInsert['continfo_primary'] = "0";
                }

                $paramsPhoneInsert['extension'] = "";
                $paramsPhoneInsert['add_date'] = DATE_TIME_FORMAT;
                $paramsPhoneInsert['modified_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('User\Model\ContactTable')->insertPhoneInformations($paramsPhoneInsert);
            }
        } elseif ($formData['address_type'] == 'shipping') {
            $form_address['title'] = isset($formData['shipping_title']) ? $formData['shipping_title'] : null;
            $form_address['first_name'] = isset($formData['shipping_first_name']) ? $formData['shipping_first_name'] : null;
            $form_address['last_name'] = isset($formData['shipping_last_name']) ? $formData['shipping_last_name'] : null;

            $form_address['address_id'] = isset($formData['shipping_existing_contact']) ? $formData['shipping_existing_contact'] : null;
            $form_address['addressinfo_country'] = isset($formData['shipping_country']) ? $formData['shipping_country'] : null;
            $form_address['addressinfo_zip'] = isset($formData['shipping_zip_code']) ? $formData['shipping_zip_code'] : null;
            $form_address['addressinfo_state'] = isset($formData['shipping_state']) ? $formData['shipping_state'] : null;
            $form_address['addressinfo_city'] = isset($formData['shipping_city']) ? $formData['shipping_city'] : null;
            $form_address['addressinfo_street_address_2'] = isset($formData['shipping_address_2']) ? $formData['shipping_address_2'] : null;
            if ($formData['source_type'] == 'backend') {

                $form_address['addressinfo_street_address_1'] = isset($formData['shipping_address']) ? $formData['shipping_address'] : null;
            } else {
                $form_address['addressinfo_street_address_1'] = isset($formData['shipping_address_1']) ? $formData['shipping_address_1'] : null;
            }
            $form_address['addressinfo_location_type'] = isset($formData['shipping_location_type']) ? $formData['shipping_location_type'] : null;
            if (!empty($form_address['address_id'])) {
                $form_address['addressinfo_location'] = $form_address['addressinfo_location_type'];
            } else {
                $form_address['addressinfo_location'] = 3;
            }

            if (isset($formData['shipping_phone_info_id']) and trim($formData['shipping_phone_info_id']) != "" and is_numeric(trim($formData['shipping_phone_info_id']))) {
                $paramsPhoneUpdate = array();
                $paramsPhoneUpdate['phone_id'] = trim($formData['shipping_phone_info_id']);
                $paramsPhoneUpdate['phone_type'] = "";
                $paramsPhoneUpdate['country_code'] = $formData['shipping_country_code'];
                $paramsPhoneUpdate['area_code'] = $formData['shipping_area_code'];
                $paramsPhoneUpdate['phone_no'] = $formData['shipping_phone'];
                $paramsPhoneUpdate['continfo_primary'] = "";
                $paramsPhoneUpdate['extension'] = "";
                $paramsPhoneUpdate['modified_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('User\Model\UserTable')->updatePhoneInformations($paramsPhoneUpdate);
            } else {
                $paramsPhoneInsert = array();
                $paramsPhoneInsert['user_id'] = $formData['user_id'];
                $paramsPhoneInsert['phone_type'] = "";
                $paramsPhoneInsert['country_code'] = $formData['shipping_country_code'];
                $paramsPhoneInsert['area_code'] = $formData['shipping_area_code'];
                $paramsPhoneInsert['phone_no'] = $formData['shipping_phone'];
                if ($formData['address_type_status'] == "1") {
                    $paramsPhoneInsert['continfo_primary'] = "1";
                } else {
                    $paramsPhoneInsert['continfo_primary'] = "0";
                }
                $paramsPhoneInsert['extension'] = "";
                $paramsPhoneInsert['add_date'] = DATE_TIME_FORMAT;
                $paramsPhoneInsert['modified_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('User\Model\ContactTable')->insertPhoneInformations($paramsPhoneInsert);
            }
        }
        /*
          $form_address['address_id'] = isset($formData['address_id']) ? $formData['address_id'] : null;
          $form_address['addressinfo_country'] = isset($formData['addressinfo_country']) ? $formData['addressinfo_country'] : null;
          $form_address['addressinfo_zip'] = isset($formData['addressinfo_zip']) ? $formData['addressinfo_zip'] : null;
          $form_address['addressinfo_state'] = isset($formData['addressinfo_state']) ? $formData['addressinfo_state'] : null;
          $form_address['addressinfo_city'] = isset($formData['addressinfo_city']) ? $formData['addressinfo_city'] : null;
          $form_address['addressinfo_street_address_2'] = isset($formData['addressinfo_street_address_2']) ? $formData['addressinfo_street_address_2'] : null;
          $form_address['addressinfo_street_address_1'] = isset($formData['addressinfo_street_address_1']) ? $formData['addressinfo_street_address_1'] : null;
          $form_address['addressinfo_location_type'] = isset($formData['addressinfo_location_type']) ? $formData['addressinfo_location_type'] : null;
         *
         */
        if (!empty($form_address['address_id'])) {
//if ($formData['address_type'] == 'shipping') {
            $this->getServiceLocator()->get('User\Model\UserTable')->updateAddressInformations($form_address);
//}
        } else {
            $form_address['user_id'] = $formData['user_id'];
            $form_address['add_date'] = DATE_TIME_FORMAT;
            $this->getServiceLocator()->get('User\Model\ContactTable')->insertAddressInfo($form_address);
        }
    }

    /**
     * This function is used to find the categories for shipping charges
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function isCustomFrame($dataArr = array()) {
        /* Get all Categories code for */
        $categoryId = $dataArr['category_id'];
        $isCustomFrame = false;
        while (1) {
            if (isset($categoryData[$categoryId])) {
                if ($categoryData[$categoryId] == 0) {
                    break;
                } else {
                    $categoryId = $categoryData[$categoryId];
                }
            } else {
                break;
            }
        }
        if ($categoryId == 2) {
            $isCustomFrame = true;
        }
        return $isCustomFrame;
    }

    /**
     * This function is used to insert the record in payment receive table.
     * @param array
     * @author Icreon Tech - SR
     */
    public function insertIntoPaymentReceive($dataArray = array()) {
        $this->getTransactionTable();

        $receivedPaymentIdArray = array();
        $totalCreditCardAmount = 0;

        $transaction = new Transaction($this->_adapter);
        $paymentTransactionData = array();
        $paymentTransactionData['amount'] = round($dataArray['amount'], 2);
        $paymentTransactionData['transaction_id'] = $dataArray['transaction_id'];
        $paymentTransactionData['authorize_transaction_id'] = $dataArray['authorize_transaction_id'];

        $paymentTransactionData['transaction_payment_id'] = $dataArray['transaction_payment_id'];
        $paymentTransactionData['added_by'] = isset($this->auth->getIdentity()->crm_user_id) ? $this->auth->getIdentity()->crm_user_id : '';
        $paymentTransactionData['added_date'] = DATE_TIME_FORMAT;
        $paymentTransactionData['modify_by'] = isset($this->auth->getIdentity()->crm_user_id) ? $this->auth->getIdentity()->crm_user_id : '';
        $paymentTransactionData['modify_date'] = DATE_TIME_FORMAT;
        $paymentTransactionDataArr['0'] = $dataArray['transaction_id'];
        //$creditTransactionPayments = $this->_transactionTable->getCreditTransactionPayment($paymentTransactionDataArr);
// $paymentTransactionData['transaction_payment_id'] = $creditTransactionPayments[0]['transaction_payment_id'];

        $paymentTransactionData = $transaction->getTransactionPaymentReceiveArr($paymentTransactionData);

        $paymentTransactionData['10'] = $dataArray['product_type_id']; // $dataArray['UnshipProductType'];
        $paymentTransactionData['11'] = @$dataArray['table_auto_id'];
        $paymentTransactionData['12'] = @$dataArray['table_type'];
        $paymentTransactionData['13'] = @$dataArray['isPaymentReceived'];
        $paymentTransactionData['14'] = @$dataArray['batch_id'];
        $paymentTransactionData['9'] = '';

        if ($dataArray['payment_source'] == 'credit') {
            $profileParam = array();
            $profileParam['transaction_id'] = $dataArray['transaction_id'];
            $profileParam['authorize_transaction_id'] = $dataArray['authorize_transaction_id'];
            $transactionProfileDetails = $this->_transactionTable->getTransactionProfileCreditCartId($profileParam);
            foreach ($transactionProfileDetails as $profile) {
                if (!empty($profile['profile_id']))
                    $paymentTransactionData['9'] = $profile['credit_card_type_id'];
            }


            //if (!empty($creditTransactionPayments)) {
            //$customerProfileId = $dataArray['profile_id'];
            //$paymentTransactionData['13'] = 0; // is_payment_received field
            return $lastInsertId = $this->_transactionTable->saveTransactionPaymentReceive($paymentTransactionData);
            //}
        } else {
            $paymentTransactionData['9'] = ''; // credit_card_type_id
            $paymentTransactionData['13'] = 1; // is_payment_received field
            return $this->_transactionTable->saveTransactionPaymentReceive($paymentTransactionData);
        }
    }

    function subval_sort($a, $subkey) {
        foreach ($a as $k => $v) {
            $b[$k] = strtolower($v[$subkey]);
        }
        asort($b);
        foreach ($b as $key => $val) {
            $c[] = $a[$key];
        }
        return $c;
    }

    /**
     * This function is used to capture amount from transaction
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function capturePaymentCrm($postArr, $creditAmountArr = array()) {
        $profileId = $postArr['profile_id'];
        //asd($creditAmountArr);
        //asd($creditAmountArr);
        $k = 0;
        $receivedPaymentIdArray = array();
        $captureAmount = false;
        $totalCreditCardAmount = 0;
        $authorizeTransactionIdsArray = array();
        $paymentTransactionData = array();
        $transaction = new Transaction($this->_adapter);
        $transactionId = $creditAmountArr['transaction_id'];
        /*         * **************************************** */
        $finalTransaction = $postArr;
        if (isset($postArr['payment_mode_cash']) && $postArr['payment_mode_cash'] == 1) {
            $paymentTransactionData = array();
            $paymentTransactionData['amount'] = $finalTransaction['payment_mode_cash_amount'];
            $paymentTransactionData['transaction_id'] = $transactionId;
            $paymentTransactionData['payment_mode_id'] = 4;
            $paymentTransactionData['added_by'] = $crm_user_id;
            $paymentTransactionData['added_date'] = $dateTime;
            $paymentTransactionData['modify_by'] = $crm_user_id;
            $paymentTransactionData['modify_date'] = $dateTime;
            $paymentTransactionData = $transaction->getTransactionPaymentArr($paymentTransactionData);
            $paymentTransactionData['22'] = '';
            $transactionPaymentId = $this->_transactionTable->saveTransactionPaymentDetail($paymentTransactionData);
            $totalCashAmount = $finalTransaction['payment_mode_cash_amount'];
            $paymentTransactionData = array();
            $paymentTransactionData['amount'] = $totalCashAmount;
            $paymentTransactionData['transaction_id'] = $transactionId;
            $paymentTransactionData['authorize_transaction_id'] = '';
            $paymentTransactionData['transaction_payment_id'] = $transactionPaymentId;
            $amountIdArray[] = array('id' => $transactionPaymentId, 'amount' => $totalCashAmount, 'type' => 'cash', 'batch_id' => $postArr['payment_cash_batch_id']);
        }

        if (isset($postArr['payment_mode_chk']) && $postArr['payment_mode_chk'] == 1) {
            if (!empty($finalTransaction['payment_mode_chk_amount'])) {
                $i = 0;
                foreach ($finalTransaction['payment_mode_chk_amount'] as $paymentCheck) {
                    if ($finalTransaction['payment_mode_chk_amount'][$i] > 0) {
                        $paymentTransactionData = array();
                        $paymentTransactionData['amount'] = $finalTransaction['payment_mode_chk_amount'][$i];
                        $paymentTransactionData['cheque_type_id'] = $finalTransaction['payment_mode_chk_type'][$i];
                        $paymentTransactionData['cheque_number'] = $finalTransaction['payment_mode_chk_num'][$i];
                        $paymentTransactionData['id_type_id'] = $finalTransaction['payment_mode_chk_id_type'][$i];
                        $paymentTransactionData['id_number'] = $finalTransaction['payment_mode_chk_id'][$i];
                        $paymentTransactionData['transaction_id'] = $transactionId;
                        $paymentTransactionData['payment_mode_id'] = 3;
                        $paymentTransactionData['added_by'] = $crm_user_id;
                        $paymentTransactionData['added_date'] = $dateTime;
                        $paymentTransactionData['modify_by'] = $crm_user_id;
                        $paymentTransactionData['modify_date'] = $dateTime;
                        $transactionCheckPaymentArr = $transaction->getTransactionPaymentArr($paymentTransactionData);
                        $paymentTransactionData['22'] = '';
                        $transactionPaymentId = $this->_transactionTable->saveTransactionPaymentDetail($transactionCheckPaymentArr);
                        $totalCheckAmount = $totalCheckAmount + $finalTransaction['payment_mode_chk_amount'][$i];
                        $paymentTransactionData = array();
                        $paymentTransactionData['amount'] = $totalCheckAmount;
                        $paymentTransactionData['transaction_id'] = $transactionId;
                        $paymentTransactionData['authorize_transaction_id'] = '';
                        $paymentTransactionData['transaction_payment_id'] = $transactionPaymentId;
                        $amountIdArray[] = array('id' => $transactionPaymentId, 'amount' => $finalTransaction['payment_mode_chk_amount'][$i], 'type' => 'check', 'batch_id' => $postArr['payment_check_batch_id'][$i]);
                    }
                    $i++;
                }
            }
        }



        $transactionIdsArr = explode(',', $postArr['transaction_ids']);
        $transactionAmountsArr = explode(',', $postArr['transaction_amounts']);
        $customerProfileIdsArr = explode(',', $postArr['customer_profile_ids']);

        if (!empty($transactionIdsArr)) {
            $i = 0;
            foreach ($transactionIdsArr as $authorizeTransactionId) {
                if (!empty($authorizeTransactionId)) {
                    // Start code to capture the payment
                    //$authorizeTransactionIdsArray['authorize_transaction_id'][] = $scVal['authorize_transaction_id'];
                    // $authorizeTransactionIdsArray['profile_id'][] = $customerProfileIdsArr['profile_id'];
                    $ccPaymentAmount = $transactionAmountsArr[$i];
                    $createCustomerProfile = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
                    $customerProfileId = $customerProfileIdsArr[$i];
                    $createCustomerProfile->getCustomerProfileRequest(array('customerProfileId' => $customerProfileId));

                    $paymentProfileId = $createCustomerProfile->profile->paymentProfiles->customerPaymentProfileId;
                    //$authorizeTransactionIdsArray['payment_profile_id'][] = $paymentProfileId;

                    $payment = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
                    $payment->createCustomerProfileTransactionRequest(array(
                        'transaction' => array(
                            'profileTransAuthCapture' => array(
                                'amount' => $ccPaymentAmount,
                                'customerProfileId' => $customerProfileId,
                                'customerPaymentProfileId' => $paymentProfileId,
                                'order' => array(
                                    'invoiceNumber' => 'SK' . sprintf('%011d', $transactionId),
                                    'description' => 'description of transaction',
                                    'purchaseOrderNumber' => 'No' . sprintf('%011d', $transactionId)),
                            )
                        ),
                        'extraOptions' => '<![CDATA[x_customer_ip=' . $_SERVER['SERVER_ADDR'] . ']]>'
                    ));
                    $responseArr = explode(',', $payment->directResponse);
                    $authorizeTransactionId = $responseArr[6];

                    if ($payment->isSuccessful()) {  // to change the payment received status and authorize_transaction_id.
                        // $creditAmountArr['payment_source'][$i]['authorize_transaction_id'] = $authorizeTransactionId;
                        //  $creditAmountArr['payment_source'][$i]['is_payment_received'] = 1;
                        //$amountIdArray[] = array('authorize_transaction_id' => $transactionIdsArr[$i], 'profile_id' => $customerProfileIdsArr[$i]);
                        /* foreach ($receivedPaymentIdArray[$customerProfileId]['received_payment_id'] as $key => $val) {
                          $updatePaymentStatusParam['received_payment_id'] = $receivedPaymentIdArray[$customerProfileId]['received_payment_id'][$key];
                          $updatePaymentStatusParam['is_payment_received'] = 1;
                          $updatePaymentStatusParam['authorize_transaction_id'] = $transactionId;
                          $this->_transactionTable->updatePaymentReceived($updatePaymentStatusParam);
                          } */
                    } else {
                        // Code to display the error message for declined transactions.
                        $paymentErrorFlag = 1;
                        $paymentErrorArray = explode('-', $payment->messages->message->text);
                        if (isset($paymentErrorArray[1]) && !empty($paymentErrorArray[1]))
                            $paymentErrMessage = $paymentErrorArray[1];
                        else
                            $paymentErrMessage = $paymentErrorArray[0];
                         //Ticket 1211
                        $paymentErrCode=(string)$payment->messages->message->code;
                        $messages = array('status' => "payment_error", 'message' => trim($paymentErrMessage), 'authorizeTransactionId' => $authorizeTransactionId, 'payment' => $payment,'paymentErrCode'=>$paymentErrCode);
                        $removeTransactionArr[0] = $creditAmountArr['transactionId'];
                        $this->_transactionTable->removeTransactionById($removeTransactionArr);
                    } // End code to capture the payment

                    $paymentTransactionData = array();
                    $paymentTransactionData['amount'] = $transactionAmountsArr[$i];
                    $paymentTransactionData['transaction_id'] = $transactionId;
                    $paymentTransactionData['payment_mode_id'] = 1;
                    $paymentTransactionData['authorize_transaction_id'] = $authorizeTransactionId;
                    $paymentTransactionData['profile_id'] = $customerProfileIdsArr[$i];
                    $paymentTransactionData['added_by'] = $crm_user_id;
                    $paymentTransactionData['added_date'] = $dateTime;
                    $paymentTransactionData['modify_by'] = $crm_user_id;
                    $paymentTransactionData['modify_date'] = $dateTime;
                    $paymentTransactionData = $transaction->getTransactionPaymentArr($paymentTransactionData);

                    $profileDataParam = array();
                    $profileDataParam['profile_id'] = $customerProfileIdsArr[$i];
                    $tokenDetailsResult = $this->getServiceLocator()->get('User\Model\UserTable')->getTokenDetails($profileDataParam);

                    if (count($tokenDetailsResult) > 0)
                        $paymentTransactionData['22'] = $tokenDetailsResult[0]['credit_card_type_id'];
                    else
                        $paymentTransactionData['22'] = '';

                    $transactionPaymentId = $this->_transactionTable->saveTransactionPaymentDetail($paymentTransactionData);
                    $totalCreditAmount = $totalCreditAmount + $transactionAmountsArr[$i];

                    $amountIdArray[] = array('id' => $transactionPaymentId, 'amount' => $transactionAmountsArr[$i], 'type' => 'credit', 'authorize_transaction_id' => $authorizeTransactionId, 'profile_id' => $customerProfileId, 'payment_profile_id' => $paymentProfileId, 'void_authorize_transaction_id' => $transactionIdsArr[$i]);
                    //$amountIdArray[] = array('id' => $transactionPaymentId, 'amount' => $transactionAmountsArr[$i], 'type' => 'credit');
                    $i++;
                }
            }
        }

        /*         * **************************************** */

        $remCashAmount = $totalCashAmount;
        $remCheckAmount = $totalCheckAmount;
        $remCreditAmount = $totalCreditAmount;
        $remInvoiceAmount = $totalInvoiceAmount;
        $creditAmountArr['payment_source'] = $amountIdArray;
        $creditCardPaymentSourceArr['payment_source'] = $creditAmountArr['payment_source'];
        // asd($creditAmountArr,2);

        foreach ($creditAmountArr['paymentArray'] as $kk => $amtPro) {
            $paymentTransactionData = array();
            $k = 0;
            foreach ($creditAmountArr['payment_source'] as $amtKey => $amtVal) {

                $paymentTransactionData['table_auto_id'] = $amtPro['table_auto_id'];
                $paymentTransactionData['table_type'] = $amtPro['table_type'];
                $paymentTransactionData['authorize_transaction_id'] = $creditAmountArr['payment_source'][$amtKey]['authorize_transaction_id'];



                $paymentTransactionData['profile_id'] = $creditAmountArr['payment_source'][$amtKey]['profile_id'];
                $paymentTransactionData['batch_id'] = $creditAmountArr['payment_source'][$amtKey]['batch_id'];
                if ($amtVal['type'] == 'cash') {//echo '1';
                    if ($remCashAmount > 0 && $amtPro['cart_product_amount'] > 0 && $creditAmountArr['payment_source'][$amtKey]['amount'] > 0) {
                        if ($remCashAmount >= $amtPro['cart_product_amount'] && $creditAmountArr['payment_source'][$amtKey]['amount'] >= $amtPro['cart_product_amount']) {


                            $paymentTransactionData['amount'] = $amtPro['cart_product_amount'];
                            $paymentTransactionData['transaction_payment_id'] = $creditAmountArr['payment_source'][$amtKey]['id'];
                            $remCashAmount = $this->Roundamount($remCashAmount) - $this->Roundamount($amtPro['cart_product_amount']);
                            $creditAmountArr['payment_source'][$amtKey]['amount'] = $this->Roundamount($creditAmountArr['payment_source'][$amtKey]['amount']) - $this->Roundamount($amtPro['cart_product_amount']);
                            $amtPro['cart_product_amount'] = 0;
                            /*

                              $paymentTransactionData['amount'] = $amtPro['cart_product_amount'];
                              $creditAmountArr['payment_source'][$amtKey]['amount'] = $this->Roundamount($creditAmountArr['payment_source'][$amtKey]['amount']) - $this->Roundamount($amtPro['cart_product_amount']);
                              $creditAmountArr['paymentArray'][$kk]['cart_product_amount'] = $creditAmountArr['paymentArray'][$kk]['cart_product_amount'] - $amtPro['cart_product_amount'];
                              $amtPro['cart_product_amount'] = $creditAmountArr['paymentArray'][$kk]['cart_product_amount'] - $amtPro['cart_product_amount'];
                              $remCashAmount = $remCashAmount - $amtPro['cart_product_amount'];

                             */
                        } else if ($remCashAmount <= $amtPro['cart_product_amount'] && $creditAmountArr['payment_source'][$amtKey]['amount'] < $amtPro['cart_product_amount']) {

                            $paymentTransactionData['amount'] = $amtVal['amount'];
                            $amtPro['cart_product_amount'] = $this->Roundamount($amtPro['cart_product_amount']) - $this->Roundamount($creditAmountArr['payment_source'][$amtKey]['amount']);
                            $remCashAmount = $this->Roundamount($remCashAmount) - $this->Roundamount($creditAmountArr['payment_source'][$amtKey]['amount']);
                            $creditAmountArr['payment_source'][$amtKey]['amount'] = 0;
                            /*
                              $paymentTransactionData['amount'] = $creditAmountArr['payment_source'][$amtKey]['amount'];
                              $creditAmountArr['paymentArray'][$kk]['cart_product_amount'] = $amtPro['cart_product_amount'] - $creditAmountArr['payment_source'][$amtKey]['amount'];
                              $amtPro['cart_product_amount'] = $amtPro['cart_product_amount'] - $creditAmountArr['payment_source'][$amtKey]['amount'];
                              $remCashAmount = $remCashAmount - $creditAmountArr['payment_source'][$amtKey]['amount'];
                              $creditAmountArr['payment_source'][$amtKey]['amount'] = $this->Roundamount($creditAmountArr['payment_source'][$amtKey]['amount']) - $this->Roundamount($creditAmountArr['payment_source'][$amtKey]['amount']);
                             */
                        }

                        $paymentTransactionData['transaction_payment_id'] = $amtVal['id'];
                        $paymentTransactionData['transaction_id'] = $transactionId;
                        $paymentTransactionData['product_type_id'] = $amtPro['cart_product_type_id'];
                        $paymentTransactionData['payment_source'] = 'cash';
                        if ($paymentTransactionData['amount'] != 0) {

                            if ($amtPro['payment_recieve'] == 1)
                                $paymentTransactionData['isPaymentReceived'] = 1;
                            else
                                $paymentTransactionData['isPaymentReceived'] = 0;

                            $transactionPaymentId = $this->insertIntoPaymentReceive($paymentTransactionData);
                        }
                    }
                }
                if ($amtVal['type'] == 'check') {
                    if ($creditAmountArr['paymentArray'][$kk]['cart_product_amount'] <= 0) {
                        continue 2;
                    }
                    if ($creditAmountArr['payment_source'][$amtKey]['amount'] <= 0) {
                        continue 1;
                    }

                    if ($remCheckAmount > 0 && $amtPro['cart_product_amount'] > 0 && $creditAmountArr['payment_source'][$amtKey]['amount'] > 0) {
                        if ($creditAmountArr['payment_source'][$amtKey]['amount'] < $amtPro['cart_product_amount'] && $remCheckAmount > 0) {

                            /* $paymentTransactionData['amount'] = $creditAmountArr['payment_source'][$amtKey]['amount'];
                              $amtPro['cart_product_amount'] = $this->Roundamount($amtPro['cart_product_amount']) - $this->Roundamount($creditAmountArr['payment_source'][$amtKey]['amount']);
                              $remCheckAmount = $remCheckAmount - $creditAmountArr['payment_source'][$amtKey]['amount'];
                              $creditAmountArr['payment_source'][$amtKey]['amount'] = $this->Roundamount($creditAmountArr['payment_source'][$amtKey]['amount']) - $this->Roundamount($creditAmountArr['payment_source'][$amtKey]['amount']); */

                            $paymentTransactionData['amount'] = $creditAmountArr['payment_source'][$amtKey]['amount'];
                            $creditAmountArr['paymentArray'][$kk]['cart_product_amount'] = $amtPro['cart_product_amount'] - $creditAmountArr['payment_source'][$amtKey]['amount'];
                            $amtPro['cart_product_amount'] = $amtPro['cart_product_amount'] - $creditAmountArr['payment_source'][$amtKey]['amount'];
                            $remCheckAmount = $remCheckAmount - $creditAmountArr['payment_source'][$amtKey]['amount'];
                            $creditAmountArr['payment_source'][$amtKey]['amount'] = $this->Roundamount($creditAmountArr['payment_source'][$amtKey]['amount']) - $this->Roundamount($creditAmountArr['payment_source'][$amtKey]['amount']);
                        } else if ($creditAmountArr['payment_source'][$amtKey]['amount'] >= $amtPro['cart_product_amount'] && $remCheckAmount > 0) {
                            /* $paymentTransactionData['amount'] = $amtPro['cart_product_amount'];
                              $creditAmountArr['payment_source'][$amtKey]['amount'] = $this->Roundamount($creditAmountArr['payment_source'][$amtKey]['amount']) - $this->Roundamount($amtPro['cart_product_amount']);
                              $remCheckAmount = $remCheckAmount - $amtPro['cart_product_amount']; */

                            $paymentTransactionData['amount'] = $amtPro['cart_product_amount'];
                            $creditAmountArr['payment_source'][$amtKey]['amount'] = $this->Roundamount($creditAmountArr['payment_source'][$amtKey]['amount']) - $this->Roundamount($amtPro['cart_product_amount']);
                            $creditAmountArr['paymentArray'][$kk]['cart_product_amount'] = $creditAmountArr['paymentArray'][$kk]['cart_product_amount'] - $amtPro['cart_product_amount'];
                            $amtPro['cart_product_amount'] = $creditAmountArr['paymentArray'][$kk]['cart_product_amount'] - $amtPro['cart_product_amount'];
                            $remCheckAmount = $remCheckAmount - $amtPro['cart_product_amount'];
                        }

                        $paymentTransactionData['transaction_payment_id'] = $amtVal['id'];
                        $paymentTransactionData['transaction_id'] = $transactionId;
                        $paymentTransactionData['product_type_id'] = $amtPro['cart_product_type_id'];
                        $paymentTransactionData['payment_source'] = 'check';

                        if ($paymentTransactionData['amount'] != 0) {

                            if ($amtPro['payment_recieve'] == 1)
                                $paymentTransactionData['isPaymentReceived'] = 1;
                            else
                                $paymentTransactionData['isPaymentReceived'] = 0;

                            $transactionPaymentId = $this->insertIntoPaymentReceive($paymentTransactionData);
                        }
                    }
                }


                if ($amtVal['type'] == 'credit') {
                    if ($creditAmountArr['paymentArray'][$kk]['cart_product_amount'] <= 0) {
                        continue 2;
                    }
                    if ($creditAmountArr['payment_source'][$amtKey]['amount'] <= 0) {
                        continue 1;
                    }


                    if ($remCreditAmount > 0 && $amtPro['cart_product_amount'] > 0 && $amtVal['amount'] > 0 && $creditAmountArr['payment_source'][$amtKey]['amount'] > 0) {

                        if ($creditAmountArr['payment_source'][$amtKey]['amount'] < $amtPro['cart_product_amount'] && $remCreditAmount > 0) {
                            $paymentTransactionData['amount'] = $creditAmountArr['payment_source'][$amtKey]['amount'];
                            $creditAmountArr['paymentArray'][$kk]['cart_product_amount'] = $amtPro['cart_product_amount'] - $creditAmountArr['payment_source'][$amtKey]['amount'];
                            $amtPro['cart_product_amount'] = $amtPro['cart_product_amount'] - $creditAmountArr['payment_source'][$amtKey]['amount'];
                            $remCreditAmount = $remCreditAmount - $creditAmountArr['payment_source'][$amtKey]['amount'];
                            $creditAmountArr['payment_source'][$amtKey]['amount'] = $this->Roundamount($creditAmountArr['payment_source'][$amtKey]['amount']) - $this->Roundamount($creditAmountArr['payment_source'][$amtKey]['amount']);
                        } else if ($creditAmountArr['payment_source'][$amtKey]['amount'] >= $amtPro['cart_product_amount'] && $remCreditAmount > 0) {
                            $paymentTransactionData['amount'] = $amtPro['cart_product_amount'];
                            $creditAmountArr['payment_source'][$amtKey]['amount'] = $this->Roundamount($creditAmountArr['payment_source'][$amtKey]['amount']) - $this->Roundamount($amtPro['cart_product_amount']);
                            $creditAmountArr['paymentArray'][$kk]['cart_product_amount'] = $creditAmountArr['paymentArray'][$kk]['cart_product_amount'] - $amtPro['cart_product_amount'];
                            $amtPro['cart_product_amount'] = $creditAmountArr['paymentArray'][$kk]['cart_product_amount'] - $amtPro['cart_product_amount'];
                            $remCreditAmount = $remCreditAmount - $amtPro['cart_product_amount'];
                        }

                        $paymentTransactionData['transaction_payment_id'] = $amtVal['id'];
                        $paymentTransactionData['transaction_id'] = $transactionId;
                        $paymentTransactionData['product_type_id'] = $amtPro['cart_product_type_id'];
                        $paymentTransactionData['payment_source'] = 'credit';

                        if ($paymentTransactionData['amount'] != 0) {
                            $captureAmount = true;
                            $paymentTransactionData['isPaymentReceived'] = 1;

                            $transactionPaymentId = $this->insertIntoPaymentReceive($paymentTransactionData);
                            $receivedPaymentIdArray[$paymentTransactionData['profile_id']]['received_payment_id'][] = $transactionPaymentId;
                            $totalCreditCardAmount = $this->Currencyformat($paymentTransactionData['amount']);
                            $receivedPaymentIdArray[$paymentTransactionData['profile_id']]['total_credit_card_amount'][] = $totalCreditCardAmount;
                            $totalCreditCardFinalAmount = $totalCreditCardFinalAmount + $totalCreditCardAmount;
                            $k++;
                        }
                    }
                }
            }
        }

        //$paymentErrorFlag = 0;
        //$messages = array();
        if ($captureAmount == true) {
            $customerProfileId = $paymentTransactionData['profile_id'];
            $k = 0;
            $authorizeTransactionIdsArray = array();

            $z = 0;
            // asd($creditCardPaymentSourceArr);
            foreach ($creditCardPaymentSourceArr['payment_source'] as $scKey => $scVal) {
                $transParamArray = array();
                if (!empty($scVal['profile_id'])) {

                    $transParamArray['customerProfileId'] = $scVal['profile_id'];
                    $transParamArray['paymentProfileId'] = $scVal['payment_profile_id'];
                    $transParamArray['authTransactionId'] = $scVal['void_authorize_transaction_id'];
                    $this->_transactionTable->voidAuthorizeTransaction($transParamArray, $this->_config);
                }
                $z++;
            }
            $removeTransParam = array();
            $removeTransParam['transaction_id'] = $transactionId;
            $this->_transactionTable->removeAuthorizeTransactionId($removeTransParam);
        }

        if ($paymentErrorFlag == 1) {
            return $messages;
        }
        // die;
    }

    /**
     * This function is used to delete authorize payment card id
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function deleteCreditCardAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $transaction = new Transaction($this->_adapter);
        $request = $this->getRequest();
        $response = $this->getResponse();
        $postArr = $request->getPost()->toArray();
        $customerProfileId = $postArr['customerProfileId'];
        $paymentProfileId = $postArr['paymentProfileId'];
        if (isset($postArr['authTransactionId']))
            $authTransactionId = $postArr['authTransactionId'];

        $messages = array('status' => "success");
        $response->setContent(\Zend\Json\Json::encode($messages));

//$delete = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
//$delete->deleteCustomerPaymentProfileRequest(array('customerProfileId' => $customerProfileId, 'customerPaymentProfileId' => $paymentProfileId));
        $transParamArray = array();
        $transParamArray['customerProfileId'] = $customerProfileId;
        $transParamArray['paymentProfileId'] = $paymentProfileId;
        $transParamArray['authTransactionId'] = $authTransactionId;
        $this->_transactionTable->voidAuthorizeTransaction($transParamArray, $this->_config);
        return $response;
        if ($delete->isSuccessful()) {
            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
        } else {
            $messages = array('status' => "error");
            $response->setContent(\Zend\Json\Json::encode($messages));
        }
        return $response;
    }

    /**
     * This function is used to modify note
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function modifyNoteAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $transaction = new Transaction($this->_adapter);
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $postArr = $request->getPost()->toArray();
        $params['tableName'] = $this->decrypt($params['tableName']);
        $params['tableField'] = $this->decrypt($params['tableField']);
        $params['noteId'] = $params['noteId'];
        $params['val'] = $params['val'];
        $searchResult = $this->getTransactionTable()->updateNote($params);
        $messages = array('status' => "success");
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This function is used to view passenger certificate
     * @return json
     * @param void
     * @author Icreon Tech - AS
     */
    function passengerCertificate($params = array(), $mode) {
        $this->getTransactionTable();
        $messages = $this->_config['transaction_messages']['config']['view_crm_transaction'];
        $pdfPath = $this->_config['file_upload_path']['assets_upload_dir'] . "transaction/passenger_" . $this->encrypt($params['id']);

        $search['product_id'] = $params['id'];
        $viewModel = new ViewModel();
        $searchResult = $this->getTransactionTable()->getShipImage($search);
        $searchParam['passenger_id'] = (isset($searchResult[0]['item_id']) && !empty($searchResult[0]['item_id'])) ? $searchResult[0]['item_id'] : "0";
        $searchResult = $this->getServiceLocator()->get('Passenger\Model\DocumentTable')->searchCrmPassenger($searchParam);
        $maritalStatusArray = $this->maritalStatusArray();
        $searchResult[0]['PRIN_MARITAL_STAT'] = (isset($searchResult[0]['PRIN_MARITAL_STAT']) ? $maritalStatusArray[$searchResult[0]['PRIN_MARITAL_STAT']] : "Unknown");
        $genderArray = $this->genderArray();
        $searchResult[0]['PRIN_GENDER_CODE'] = (isset($searchResult[0]['PRIN_GENDER_CODE']) ? $genderArray[$searchResult[0]['PRIN_GENDER_CODE']] : "Unknown");
        $messages['LAST_RESIDENCE'] = (($searchResult[0]['data_source'] == '1') ? $messages['LAST_RESIDENCE'] : $messages['PLACE_OF_BIRTH']);
        $prin_place = (($searchResult[0]['data_source'] == '1') ? $searchResult[0]['PRIN_PLACE_RESI'] : $searchResult[0]['PLACE_OF_BIRTH']);
        $dateArray = date_parse_from_format("Y-m-d", $searchResult[0]['DATE_ARRIVE']);
        if ($searchResult[0]['data_source'] == 2 && ($dateArray['month'] == '1' or $dateArray['month'] == '01') && ($dateArray['day'] == 1 or $dateArray['day'] == '01')) {
            $dateArr = $dateArray['year'];
        } else {
            if ($searchResult[0]['DATE_ARRIVE'] != '' && $searchResult[0]['DATE_ARRIVE'] != '0000-00-00 00:00:00') {
                $dateArr = $this->OutputDateFormat($searchResult[0]['DATE_ARRIVE'], 'displaymonthformat2');
            }
        }

        $str = '<page>
    <table style="width:100%; padding-top:266px" align="center" cellspacing="5">';
        if (!empty($searchResult[0]['PRIN_FIRST_NAME'])) {
            $str.='<tr>
            <td style="font-size:14px;"> ' . $messages['FIRST_NAME'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult[0]['PRIN_FIRST_NAME'] . ' </td>
        </tr>';
        }
        $str.='<tr>
            <td style="font-size:14px;"> ' . $messages['LAST_NAME'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult[0]['PRIN_LAST_NAME'] . ' </td>
        </tr>';
        if (!empty($searchResult[0]['PRIN_NATIONALITY'])) {
            $str.='<tr>
            <td style="font-size:14px;"> ' . $messages['NATIONALITY'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult[0]['PRIN_NATIONALITY'] . ' </td>
        </tr>';
        }

        if (!empty($prin_place)) {
            $str.='<tr>
            <td style="font-size:14px;"> ' . $messages['LAST_RESIDENCE'] . ' : </td>
            <td style="font-size:14px;"> ' . $prin_place . ' </td>
        </tr>';
        }

        if (!empty($searchResult[0]['DATE_ARRIVE'])) {
            $str.='<tr>
            <td style="font-size:14px;"> ' . $messages['DATE_ARRIVAL'] . ' : </td>
            <td style="font-size:14px;"> ' . $dateArr . ' </td>
        </tr>';
        }
        if (!empty($searchResult[0]['PRIN_AGE_ARRIVAL'])) {
            $str.='<tr>
            <td style="font-size:14px;"> ' . $messages['AGE_ARRIVAL'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult[0]['PRIN_AGE_ARRIVAL'] . ' </td>
        </tr>';
        }

        $str.='<tr>
            <td style="font-size:14px;"> ' . $messages['GENDER'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult[0]['PRIN_GENDER_CODE'] . ' </td>
        </tr>';

        $str.='<tr>
            <td style="font-size:14px;"> ' . $messages['MARITAL_STATUS'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult[0]['PRIN_MARITAL_STAT'] . ' </td>
        </tr>';
        if (!empty($searchResult[0]['SHIP_NAME'])) {
            $str.='<tr>
                <td style="font-size:14px;"> ' . $messages['SHIP_TRAVEL'] . ' : </td>
                <td style="font-size:14px;"> ' . $searchResult[0]['SHIP_NAME'] . ' </td>
            </tr>';
        }
        if (!empty($searchResult[0]['DEP_PORT_NAME'])) {
            $str.='<tr>
                    <td style="font-size:14px;"> ' . $messages['PORT_DEPARTURE'] . ' : </td>
                    <td style="font-size:14px;"> ' . $searchResult[0]['DEP_PORT_NAME'] . ' </td>
            </tr>';
        }
        if (!empty($searchResult[0]['REF_PAGE_LINE_NBR'])) {
            $str.='<tr>
            <td style="font-size:14px;"> ' . $messages['MANIFEST_LINE_NUMBER'] . ' : </td>
            <td style="font-size:14px;"> ' . $searchResult[0]['REF_PAGE_LINE_NBR'] . ' </td>
        </tr>';
        }
        $str.='</table>
    </page>';
        try {
            $html2pdf = new\ HTML2PDF('L', 'A4', 'en');
            $html2pdf->setDefaultFont('Times');
            $html2pdf->writeHTML($str, false);
            $html2pdf->Output("passenger.pdf");
        } catch (HTML2PDF_exception $e) {
            echo $e;
            exit;
        }
    }

    /**
     * This function is used to view donation details
     * @return json
     * @param void
     * @author Icreon Tech - AS
     */
    public function viewDonDetailsAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $form = new DonationAddToCartForm();
        $this->getTransactionTable();
        $this->getConfig();
        $messages = $this->_config['transaction_messages']['config']['view_crm_transaction'];
        $viewModel = new ViewModel();
        $this->layout('popup');
        $params = $this->params()->fromRoute();
        $searchResult = $this->getTransactionTable()->getDonDetails($params);
        if ($request->isPost()) {
            $data = $request->getPost();
            $param['id'] = $params['id'];
            $param['in_honoree'] = $data['in_honoree'];
            $param['honor_memory_name'] = '';
            if ($param['in_honoree'] != '3') {
                $param['honor_memory_name'] = $data['honor_memory_name'];
            }
            $this->getTransactionTable()->updateDonDetails($param);
            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
        $form->get('in_honoree')->setAttribute('value', $searchResult[0]['honoree_type']);
        $form->get('honor_memory_name')->setAttribute('value', $searchResult[0]['honoree_name']);
        $form->get('donationaddtocart')->setAttribute('value', 'Submit');

        $viewModel->setVariables(array(
            'searchResult' => $searchResult[0],
            'jsLangTranslate' => $messages,
            'form' => $form,
            'id' => $params['id']
        ));
        return $viewModel;
    }

    /**
     * This function is used to delete authorize payment card id
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function updateCartCrmAction() {
        $this->getTransactionTable();
        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $transaction = new Transaction($this->_adapter);
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $postArr = $request->getPost()->toArray();

            if ($postArr['typeOfProduct'] == 'inventory') {
                $detail = $this->_productTable->getProductInfo(array('id' => $postArr['productId']));
                $total_in_stock = $detail[0]['total_in_stock'];
                if ($postArr['qty'] > $total_in_stock) {
                    $returnrray[$postArr['typeOfProduct'] . '_' . $postArr['cartId'] . '_' . $postArr['productId']] = array('message' => sprintf($msgArr['QUANTITY_EXCEED_FRONT'], $total_in_stock), 'product_name' => $detail[0]['product_name']);
                } else {
                    if ($postArr['qty'] == "0") {
                        $postArrDel = array();
                        $postArrDel['cartId'] = $postArr['cartId'];
                        $postArrDel['productTypeId'] = $postArr['productTypeId'];
                        $postArrDel['productMappingId'] = $postArr['productMappingId'];
                        $postArrDel['typeOfProduct'] = $postArr['typeOfProduct'];
                        $transactionArr = $transaction->getArrayForRemoveCartProduct($postArrDel);
                        $this->_transactionTable->removeProductToCart($transactionArr);
                    } else {
                        $productStatus = $this->getTransactionTable()->updateCartCrmProduct($postArr);
                    }
                }
            } else {
                if ($postArr['qty'] == "0") {
                    $postArrDel = array();
                    $postArrDel['cartId'] = $postArr['cartId'];
                    $postArrDel['productTypeId'] = $postArr['productTypeId'];
                    $postArrDel['productMappingId'] = $postArr['productMappingId'];
                    $postArrDel['typeOfProduct'] = $postArr['typeOfProduct'];
                    $transactionArr = $transaction->getArrayForRemoveCartProduct($postArrDel);
                    $this->_transactionTable->removeProductToCart($transactionArr);
                } else {
                    $productStatus = $this->getTransactionTable()->updateCartCrmProduct($postArr);
                }
            }

            if (!empty($returnrray)) {
                $messages = array('status' => "error", "info" => $returnrray);
            } else {
                $messages = array('status' => "success");
            }
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This action is used to show bio certificate
     * @return json
     * @param void
     * @author Icreon Tech - AS
     */
    public function viewBioCertificateAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $this->getTransactionTable();
        $this->getConfig();
        $messages = $this->_config['transaction_messages']['config']['view_crm_transaction'];
        $viewModel = new ViewModel();
        $this->layout('popup');
        $params = $this->params()->fromRoute();

        $search['woh_id'] = $params['id'];
        $search['product_id'] = $params['id'];
        $mode = 'I';
        $this->bioCertificatePdf($search, $mode);
// $searchResult = $this->getServiceLocator()->get('User\Model\UserTable')->getWoh($search);
        $viewModel->setVariables(array(
            'searchResult' => $searchResult[0],
            'jsLangTranslate' => $messages
        ));
        return $viewModel;
    }

    public function cartDetailsAction() {
        $viewModel = new ViewModel();
        $cartlist = array();
        if (isset($this->auth->getIdentity()->user_id) && $this->auth->getIdentity()->user_id != '') {
            $cartParam['user_id'] = $this->auth->getIdentity()->user_id;
            $cartParam['source_type'] = 'frontend';
            $cartlist = $this->getTransactionTable()->getCartProducts($cartParam);
        }
        return $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'cartlist' => $cartlist
        ));
    }

    public function bioCertificatePdf($params = array(), $mode) {
        error_reporting(0);
        $this->getTransactionTable();
        $messages = $this->_config['transaction_messages']['config']['view_crm_transaction'];
        $pdfPath = $this->_config['file_upload_path']['assets_upload_dir'] . "transaction/shipment_" . $this->encrypt($params['shipmentId']);

        $search['woh_id'] = $params['product_id'];
        $searchResult = $this->getServiceLocator()->get('User\Model\UserTable')->getBioWoh(array('bio_certificate_product_id' => $search['woh_id']));
        $searchResult = $searchResult[0];
        $viewModel = new ViewModel();
        $str = '<page><div style="padding-top: 80px;">
        <div align="center" style="font-size: 40px; margin-top: 20px; font-family: times new Roman;">' .
                str_replace("<br>", ' ', $searchResult['final_name']) . '</div>
        <div align="center" style="margin-top: 34px; padding-left:70px; font-size: 20px;">' .
                $searchResult['panel_no'] . '</div>
        <div align="center" style="margin-top:81px; text-align: center; font-size: 22px;">' . $messages['NAME'] . ' : <b>' . $searchResult['name'] . '</b></div>
        <div align="center" style="margin-top:10px; text-align: center; font-size: 22px;">' . $messages['EMIGRANTED_FROM'] . ' : <b>' . $searchResult['immigrated_from'] . '</b></div>
        <div align="center" style="margin-top:10px; text-align: center; font-size: 22px;">' . $messages['ARRIVED_DATE'] . ' : <b>' . $this->OutputDateFormat($searchResult['date_of_entry_to_us'], 'displaymonthformat2') . '</b></div>
        <div align="center" style="margin-top:10px; text-align: center; font-size: 22px;">' . $messages['PORT_ENTRY'] . ' : <b>' . $searchResult['port_of_entry'] . '</b></div>
        <div align="center" style="margin-top:10px; text-align: center; font-size: 22px;">' . $messages['METHOD_OF_TRAVEL'] . ' : <b>' . $searchResult['method_of_travel'] . '</b></div>
        <div align="center" style="margin-top:20px; text-align: center; font-size: 22px;">' . $messages['FAMILY_HISTORY'] . ' :</div>
        <div align="center" style="margin: 0px 110px 0 130px; text-align: center; font-size: 20px;">' . $searchResult['additional_info'] . '</div>';
        $str = $str . '</div></page>';
        try {
            $html2pdf = new\ HTML2PDF('L', 'A4', 'en');
            $html2pdf->setDefaultFont('Times');
            $html2pdf->writeHTML($str, false);
            $html2pdf->Output($pdfPath . "/" . date('Y-m-d') . "_" . $search['product_id'] . "_woh_ceritificate.pdf");
        } catch (HTML2PDF_exception $e) {
            echo $e;
            exit;
        }
    }

    /**
     * This function is used to save the user shipping address
     * @return json
     * @param void
     * @author Icreon Tech - SR
     */
    public function saveUserShippingAddressAction() {
        //error_reporting(E_ALL);
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $response = $this->getResponse();
        if ($request->isPost()) {
            $formData = $postData = $request->getPost()->toArray();
            $form_address = array();
            $form_address['addressinfo_street_address_1'] = isset($formData['shipping_address_1']) ? strip_tags($formData['shipping_address_1']) : null;
            $form_address['addressinfo_street_address_2'] = isset($formData['shipping_address_2']) ? strip_tags($formData['shipping_address_2']) : null;
            $form_address['addressinfo_country'] = isset($formData['shipping_country']) ? $formData['shipping_country'] : null;
            if (isset($formData['is_apo_po_new']) && $formData['is_apo_po_new'] == 1) {
                if (isset($formData['shipping_state_id']) && $formData['shipping_state_id'] != '')
                    $form_address['addressinfo_state'] = $formData['shipping_state_id'];
                if (isset($formData['shipping_state']) && $formData['shipping_state'] != '')
                    $form_address['addressinfo_state'] = $formData['shipping_state'];
            }
            if (isset($formData['is_apo_po_new']) && $formData['is_apo_po_new'] == 2) {
                if (isset($formData['shipping_state_id']) && $formData['shipping_state_id'] != '')
                    $form_address['addressinfo_state'] = $formData['shipping_state_id'];
                if (isset($formData['shipping_state']) && $formData['shipping_state'] != '')
                    $form_address['addressinfo_state'] = $formData['shipping_state'];
            }
            if (isset($formData['is_apo_po_new']) && $formData['is_apo_po_new'] == 3)
                $form_address['addressinfo_state'] = strip_tags($formData['apo_po_state']);

            $form_address['addressinfo_city'] = isset($formData['shipping_city']) ? strip_tags($formData['shipping_city']) : null;
            $form_address['addressinfo_zip'] = isset($formData['shipping_zip_code']) ? $formData['shipping_zip_code'] : null;
            $form_address['addressinfo_location'] = isset($formData['addressinfo_location']) ? $formData['addressinfo_location'] : null;
            $form_address['address_type'] = isset($formData['is_apo_po_new']) ? $formData['is_apo_po_new'] : null;
            //$form_address['addressinfo_location'] = isset($formData['is_apo_po_new']) ? $formData['is_apo_po_new'] : null;

            $userId = $this->auth->getIdentity()->user_id;

            if (isset($formData['shipping_address_information_id']) && !empty($formData['shipping_address_information_id'])) {

                $form_address['title'] = isset($formData['shipping_title']) ? $formData['shipping_title'] : null;
                $form_address['first_name'] = isset($formData['shipping_first_name']) ? $formData['shipping_first_name'] : null;
                // $form_address['middle_name'] = isset($formData['shipping_middle_name']) ? $formData['shipping_middle_name'] : null;
                $form_address['last_name'] = isset($formData['shipping_last_name']) ? $formData['shipping_last_name'] : null;
                $form_address['address_id'] = $formData['shipping_address_information_id'];
                $form_address['modified_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('User\Model\UserTable')->updateAddressInformations($form_address);
            } else {
                $form_address['title'] = isset($formData['shipping_title']) ? addslashes($formData['shipping_title']) : null;
                $form_address['first_name'] = isset($formData['shipping_first_name']) ? addslashes($formData['shipping_first_name']) : null;
                //$form_address['middle_name'] = isset($formData['shipping_middle_name']) ? addslashes($formData['shipping_middle_name']) : null;
                $form_address['last_name'] = isset($formData['shipping_last_name']) ? addslashes($formData['shipping_last_name']) : null;
                $form_address['user_id'] = $userId;
                $form_address['add_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('User\Model\ContactTable')->insertAddressInfo($form_address);
            }

            $paramsPhoneInsert = array();
            $paramsPhoneInsert['user_id'] = $userId;
            $paramsPhoneInsert['phone_type'] = "";
            $paramsPhoneInsert['country_code'] = strip_tags($formData['shipping_country_code']);
            $paramsPhoneInsert['area_code'] = strip_tags($formData['shipping_area_code']);
            $paramsPhoneInsert['phone_no'] = strip_tags($formData['shipping_phone']);
            $paramsPhoneInsert['continfo_primary'] = "1";
            $paramsPhoneInsert['extension'] = "";
            $paramsPhoneInsert['add_date'] = DATE_TIME_FORMAT;
            $paramsPhoneInsert['modified_date'] = DATE_TIME_FORMAT;

            $this->getServiceLocator()->get('User\Model\ContactTable')->insertPhoneInformationsCheckout($paramsPhoneInsert);
        }
        $messages = array('status' => "success", 'message' => 'Address is saved successfully');
        return $response->setContent(\Zend\Json\Json::encode($messages));
    }

    /**
     * This function is used to save the user billing address
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function saveUserBillingAddressAction() {
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $response = $this->getResponse();
        if ($request->isPost() && isset($this->auth->getIdentity()->user_id)) {

            $formData = $postData = $request->getPost()->toArray();
            $form_address = array();
            $form_address['addressinfo_street_address_1'] = isset($formData['billing_address_1']) ? strip_tags($formData['billing_address_1']) : null;
            $form_address['addressinfo_street_address_2'] = isset($formData['billing_address_2']) ? strip_tags($formData['billing_address_2']) : null;
            $form_address['addressinfo_country'] = isset($formData['billing_country']) ? $formData['billing_country'] : null;
            $form_address['addressinfo_state'] = isset($formData['billing_state']) ? $formData['billing_state'] : null;
            $form_address['addressinfo_city'] = isset($formData['billing_city']) ? strip_tags($formData['billing_city']) : null;
            $form_address['addressinfo_zip'] = isset($formData['billing_zip_code']) ? $formData['billing_zip_code'] : null;
            $form_address['addressinfo_location'] = isset($formData['billing_addressinfo_location']) ? $formData['billing_addressinfo_location'] : null;

            if (isset($formData['billing_address_information_id']) && !empty($formData['billing_address_information_id'])) {
                $form_address['address_id'] = $formData['billing_address_information_id'];
                $form_address['modified_date'] = DATE_TIME_FORMAT;

                $form_address['title'] = isset($formData['billing_title']) ? $formData['billing_title'] : null;
                $form_address['first_name'] = isset($formData['billing_first_name']) ? $formData['billing_first_name'] : null;
                // $form_address['middle_name'] = isset($formData['billing_middle_name']) ? $formData['billing_middle_name'] : null;
                $form_address['last_name'] = isset($formData['billing_last_name']) ? $formData['billing_last_name'] : null;

                $this->getServiceLocator()->get('User\Model\UserTable')->updateAddressInformations($form_address);
            } else {
                $form_address['user_id'] = $this->auth->getIdentity()->user_id;
                $form_address['add_date'] = DATE_TIME_FORMAT;

                $form_address['title'] = isset($formData['billing_title']) ? addslashes($formData['billing_title']) : null;
                $form_address['first_name'] = isset($formData['billing_first_name']) ? addslashes($formData['billing_first_name']) : null;
                // $form_address['middle_name'] = isset($formData['billing_middle_name']) ? addslashes($formData['billing_middle_name']) : null;
                $form_address['last_name'] = isset($formData['billing_last_name']) ? addslashes($formData['billing_last_name']) : null;

                $this->getServiceLocator()->get('User\Model\ContactTable')->insertAddressInfo($form_address);
            }

            $paramsPhoneInsert = array();
            $paramsPhoneInsert['user_id'] = $this->auth->getIdentity()->user_id;
            $paramsPhoneInsert['phone_type'] = "";
            $paramsPhoneInsert['country_code'] = strip_tags($formData['billing_country_code']);
            $paramsPhoneInsert['area_code'] = strip_tags($formData['billing_area_code']);
            $paramsPhoneInsert['phone_no'] = strip_tags($formData['billing_phone']);
            $paramsPhoneInsert['continfo_primary'] = "0";
            $paramsPhoneInsert['extension'] = "";
            $paramsPhoneInsert['add_date'] = DATE_TIME_FORMAT;
            $paramsPhoneInsert['modified_date'] = DATE_TIME_FORMAT;
            $this->getServiceLocator()->get('User\Model\ContactTable')->insertPhoneInformations($paramsPhoneInsert);
        }

        $messages = array('status' => "success", 'message' => 'Address is saved successfully');
        return $response->setContent(\Zend\Json\Json::encode($messages));
    }

    /**
     * This function is used to save the user shipping address
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function userShippingAddressAction() {
        $this->getTransactionTable();

        $params = $this->params()->fromRoute();
        $userId = $this->auth->getIdentity()->user_id;
        $defaultId = '';
        if (isset($params['default_id']))
            $defaultId = $params['default_id'];

        $request = $this->getRequest();
        $userArr = array('user_id' => $userId);

        $contacAddressDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations($userArr);

        $searchArray = array();
        $searchArray['user_id'] = $userId;
// $searchArray['is_primary'] = "1";
        $phoneDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserPhoneInformations($searchArray);

        $countryCode = (!empty($phoneDetail[0]['country_code'])) ? $phoneDetail[0]['country_code'] : "";
        $areaCode = (!empty($phoneDetail[0]['area_code'])) ? $phoneDetail[0]['area_code'] : "";
        $phoneNumber = (!empty($phoneDetail[0]['phone_number'])) ? $phoneDetail[0]['phone_number'] : "";
        $contactDetail = array();
        $contactDetail[0] = $this->getServiceLocator()->get('User\Model\UserTable')->getUser($userArr);
        $defaultBillingAddress = array();
        $defaultShippingAddress = array();
        $primaryShippingAddress = array();
        $primaryBillingAddress = '';
        $shippingLocation = array();
        $billingLocation = array();
        if (!empty($contacAddressDetail)) {
            foreach ($contacAddressDetail as $addressDetail) {
                //$userName = (!empty($contactDetail[0]['full_name'])) ? $contactDetail[0]['full_name'] . " " : $contactDetail[0]['company_name'] . " ";

                if (isset($addressDetail['first_name']) && !empty($addressDetail['first_name'])) {
                    $userName = (!empty($addressDetail['title'])) ? stripslashes($addressDetail['title']) . ' ' : '';
                    $userName.= (!empty($addressDetail['first_name'])) ? stripslashes($addressDetail['first_name']) . ' ' : '';
                    // $userName.= (!empty($addressDetail['middle_name'])) ? stripslashes($addressDetail['middle_name']) . ' ' : '';
                    $userName.= (!empty($addressDetail['last_name'])) ? stripslashes($addressDetail['last_name']) : '';
                } else {
                    $userName = (!empty($contactDetail[0]['full_name'])) ? $contactDetail[0]['full_name'] . " " : $contactDetail[0]['company_name'] . " ";
                }

                $address = $addressDetail['street_address_one'] . " " . $addressDetail['street_address_two'] . " " . $addressDetail['street_address_three'];
                $address = rtrim(trim($address), ",");
                $address = (!empty($address)) ? $address . ", " : "";
                $state = (!empty($addressDetail['state'])) ? $addressDetail['state'] . ", " : "";
                $city = (!empty($addressDetail['city'])) ? $addressDetail['city'] . ", " : "";
                $country_name = (!empty($addressDetail['country_name'])) ? $addressDetail['country_name'] . ", " : "";
                $zip_code = (!empty($addressDetail['zip_code'])) ? $addressDetail['zip_code'] : "";
                $addressLabel = '<strong>' . $userName . '</strong><br>' . $address . $state . $city . $country_name . $zip_code;
                $addressLabel = rtrim(trim($addressLabel), ', ');
                $addressDetailArr[$addressDetail['address_information_id']] = $addressLabel;

                $locationType = explode(",", $addressDetail['location_name']);
                if (in_array('2', $locationType)) {
                    $billingLocation[$addressDetail['address_information_id']] = $addressLabel;
                    $addressDetail['address'] = $address;

                    if (in_array('1', $locationType)) {
                        $primaryBillingAddress = $addressDetail;
                        $defaultBillingAddress = $addressDetail;
                    }
                }
                if (in_array('3', $locationType)) {
                    $shippingLocation[$addressDetail['address_information_id']] = $addressLabel;

                    if (in_array('1', $locationType)) {
                        $primaryShippingAddress = $addressDetail;
                        $defaultShippingAddress = $addressDetail;
                    }
                }
            }
        }
        $paramArray['userid'] = $userId;
        $cartDeta = $this->cartTotalWithTax($paramArray);

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'userId' => $userId,
            'shippingLocation' => $shippingLocation,
            'billingLocation' => @$billingLocation,
            'defaultShippingAddress' => $defaultShippingAddress,
            'primaryShippingAddress' => $primaryShippingAddress,
            'primaryBillingAddress' => $primaryBillingAddress,
            'addressLocation' => $params['address_location'],
            'defaultId' => $defaultId,
            'cartDeta' => $cartDeta
        ));
        return $viewModel;
    }

    /**
     * This function is used to delete the billing/shipping address
     * @return json
     * @param void
     * @author Icreon Tech - SR
     */
    function deleteBillingShippingAddressAction() {
        $this->getTransactionTable();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        $param['address_id'] = $this->decrypt($params['address_id']);
        $param['is_delete'] = 1;
        $this->getServiceLocator()->get('User\Model\UserTable')->deleteUserAddressInfo($param);
        $messages = array('status' => "success", 'message' => 'Address is deleted successfully.');
        return $response->setContent(\Zend\Json\Json::encode($messages));
    }

    /**
     * This function is used to to save the credit card from front side
     * @return json
     * @param void
     * @author Icreon Tech - SR
     */
    public function saveCreditCartFrontAction() {
        $this->getTransactionTable();
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $formData = $postData = $request->getPost()->toArray();
            $creditCardResult = $this->getServiceLocator()->get('User\Model\UserTable')->getCreditCards();
            foreach ($creditCardResult as $key => $val) {
                $creditCardResultArray[$val['credit_card_type_id']] = strtolower($val['credit_card']);
            }
            $creditCardType = array_search(strtolower($formData['credit_card_type']), $creditCardResultArray);
            $formData['credit_card_type_id'] = $creditCardType;
            $formData['user_id'] = $this->auth->getIdentity()->user_id;


            if ($this->saveCreditCardInformation($formData) == true) {
                $messages = array('status' => "success", 'message' => 'Credit card is saved successfully.');
                return $response->setContent(\Zend\Json\Json::encode($messages));
            } else {
                $messages = array('status' => "error", 'message' => 'Credit card is not valid.');
                return $response->setContent(\Zend\Json\Json::encode($messages));
            }
        }
    }

    /**
     * This function is used to to save the credit card from front side
     * @return void
     * @param void
     * @author Icreon Tech - SR
     */
    public function saveCreditCardInformation($postArr = array()) {

        $paymentInfo = array();
        $paymentInfo['card_number'] = $postArr['credit_card_number'];
        $paymentInfo['expiry_year'] = $postArr['credit_card_exp_year'];
        $paymentInfo['expiry_month'] = $postArr['credit_card_exp_month'];
        $paymentInfo['cvv_number'] = $postArr['credit_card_code'];

        $addressInfoId = $postArr['billing_address_id'];
        $userAddressInfo = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations(array('address_information_id' => $addressInfoId));

        if (empty($postArr['existing_profile_id'])) {
//$userId = $postArr['user_id'];
            $userId = $this->auth->getIdentity()->user_id;
            $userArr = array('user_id' => $userId);
            $contactDetail = array();
            $contactDetail[0] = $this->getServiceLocator()->get('User\Model\UserTable')->getUser($userArr);
            $createCustomerProfile = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);

            $createCustomerProfile->createCustomerProfileRequest(array(
                'profile' => array(
                    'merchantCustomerId' => $contactDetail[0]['user_id'] . time(),
                    'email' => $contactDetail[0]['email_id'],
                    'paymentProfiles' => array(
                        'billTo' => array(
                            'firstName' => $postArr['credit_card_name'],
                            'lastName' => '',
                            'address' => isset($userAddressInfo[0]['street_address_one']) ? $userAddressInfo[0]['street_address_one'] : '',
                            'city' => isset($userAddressInfo[0]['city']) ? $userAddressInfo[0]['city'] : '',
                            'state' => isset($userAddressInfo[0]['state']) ? $userAddressInfo[0]['state'] : '',
                            'zip' => isset($userAddressInfo[0]['zip_code']) ? $userAddressInfo[0]['zip_code'] : '',
                            'country' => isset($userAddressInfo[0]['country_name']) ? $userAddressInfo[0]['country_name'] : ''
                        ),
                        'payment' => array(
                            'creditCard' => array(
                                'cardNumber' => $postArr['credit_card_number'],
                                'expirationDate' => $postArr['credit_card_exp_year'] . "-" . $postArr['credit_card_exp_month'],
                                'cardCode' => $postArr['credit_card_code'],
                            ),
                        ),
                    ),
                )
            ));

            if ($createCustomerProfile->isSuccessful()) {
                $customerProfileId = $createCustomerProfile->customerProfileId;
                $paymentProfileId = $createCustomerProfile->customerPaymentProfileIdList->numericString;
                $shippingProfileId = $createCustomerProfile->customerShippingAddressIdList->numericString;


//if ($payment->isSuccessful()) {
//$responseArr = explode(', ', $payment->directResponse);
//$transactionId = $responseArr[6];
                $customerProfileId = $customerProfileId;
                $dataArr['user_id'] = $contactDetail[0]['user_id'];
                $dataArr['profile_id'] = $customerProfileId;
                $dataArr['credit_card_type_id'] = !empty($postArr['credit_card_type_id']) ? $postArr['credit_card_type_id'] : '';
                $dataArr['credit_card_no'] = 'XXXX' . substr($postArr['credit_card_number'], -4);
                $dataArr['added_by'] = !empty($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '';
                $dataArr['modified_by'] = !empty($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '';


                $this->getServiceLocator()->get('User\Model\UserTable')->insertTokenInfo($dataArr);
                return true;
//}
            }
        }
    }

    public function userCreditCardListingAction() {
        $this->getTransactionTable();
        $params = $this->params()->fromRoute();
        $user_id = $params['user_id'];
        $user_id = $this->auth->getIdentity()->user_id;
        $userTokenInfo = $this->getServiceLocator()->get('User\Model\UserTable')->getUserTokenInformations(array('user_id' => $user_id));
        if (!empty($userTokenInfo)) {
            foreach ($userTokenInfo as $key => $val) {
                // $createCustomerProfile = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
                // $createCustomerProfile->getCustomerProfileRequest(array('customerProfileId' => $val['profile_id']));
                // $cardNumbrer = $createCustomerProfile->profile->paymentProfiles->payment->creditCard->cardNumber;
                // $cardExpiryDate = $createCustomerProfile->profile->paymentProfiles->payment->creditCard->expirationDate;
                //$tokenArray[$val['profile_id']] = $cardNumbrer . ' ' . $cardExpiryDate;
                $tokenArray[$val['profile_id']] = $val['credit_card_no'];
            }
        }
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'userId' => $user_id,
            'tokenArray' => $tokenArray,
            'defaultToken' => ''
        ));
        return $viewModel;
    }

    public function updateTrackingCodeAction() {
        $this->getTransactionTable();
        $this->getConfig();
        $form = new UpdateTrackCodeForm();
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $insert['track_code'] = $request->getPost('tracking_code');
            $insert['shipment_product_id'] = $request->getPost('product_id');
            $insert['modified_by'] = $this->auth->getIdentity()->crm_user_id;
            $insert['modified_date'] = DATE_TIME_FORMAT;
            $result = $this->getTransactionTable()->insertTrackingCode($insert);
            $messages = array('status' => "success", 'message' => 'Tracking code saved successfully.');
            $searchResult = $this->getTransactionTable()->searchTransactionDetails($result[0]);
            $searchProduct = $this->getTransactionTable()->searchTransactionProducts($result[0]);
            $count = count($searchProduct);
            $traCount = 0;
            $transactionStatus = 0;
            $cancelCount = 0;
            $holdCount = 0;
            $voidedShipCount = 0;
            $returnCount = 0;
            foreach ($searchProduct as $key => $val) {
                if ($val['product_status'] == '6') {
                    $cancelCount++;
                } else if ($val['product_status'] == '7') {
                    $holdCount++;
                } else if ($val['product_status'] == '12') {
                    $voidedShipCount++;
                } else if ($val['product_status'] == '13') {
                    $returnCount++;
                } else {
                    if ($transactionStatus < $val['product_status']) {
                        $traCount = 1;
                        $transactionStatus = $val['product_status'];
                    } else if ($transactionStatus == $val['product_status']) {
                        $traCount++;
                    }
                }
            }
            if ($cancelCount == $count) {
                $transactionStatus = '6';
            } else if ($holdCount == $count) {
                $transactionStatus = '7';
            } else if ($voidedShipCount == $count) {
                $transactionStatus = '12';
            } else if ($returnCount == $count) {
                $transactionStatus = '13';
            } else if (($count > ($traCount + $cancelCount + $voidedShipCount + $returnCount)) && ($transactionStatus == '2' || $transactionStatus == '4' || $transactionStatus == '8' || $transactionStatus == '10')) {
                $transactionStatus++;
            }

            $update['transactionStatus'] = $transactionStatus;
            $update['transactionId'] = $result[0]['transactionId'];
            $update['modified_by'] = $this->auth->getIdentity()->crm_user_id;
            $update['modified_date'] = DATE_TIME_FORMAT;
            $this->getTransactionTable()->updateTransactionStatus($update);

            //code added for tracking code
            $changeLogArray = array();
            $changeLogArray['table_name'] = 'tbl_tra_change_logs';
            $changeLogArray['activity'] = '2'; /* 2 == for update */
            $changeLogArray['id'] = $result[0]['transactionId'];
            $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
            $changeLogArray['added_date'] = DATE_TIME_FORMAT;
            $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);

            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
        $id = $params['id'];
        $viewModel = new ViewModel();
        $this->layout('popup');
        $form->get('product_id')->setValue($params['id']);
        $viewModel->setVariables(array(
            'form' => $form
        ));
        return $viewModel;
    }

    public function getTransactionListAction() {
        $this->getTransactionTable();
        $this->getConfig();
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();
        $response = $this->getResponse();

        $searchParam = array();
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = ($limit * $page) - $limit;

        $searchParam['start_index'] = $start;
        $searchParam['record_limit'] = $limit;
        $searchParam['sort_field'] = $request->getPost('sidx');
        $searchParam['sort_order'] = $request->getPost('sord');

        $number_of_pages = 1;
        $page_counter = 1;
        $transactionList = $this->_transactionTable->getBatchFreeTransactions($searchParam);
        if (!empty($transactionList)) {
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $total = @$countResult[0]->RecordCount;
            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();

            $page_counter = $page;
            $number_of_pages = ceil($total / $limit);
            $jsonResult['records'] = $countResult[0]->RecordCount;
            $jsonResult['page'] = $request->getPost('page');
            $jsonResult['total'] = $number_of_pages;

            foreach ($transactionList as $val) {
                $arrCell['id'] = $val['received_payment_id'];
                $created_by = ($val['created_by'] != '') ? $val['created_by'] : 'N/A';
                $transaction_id = "<a onclick=getUserTransactionDetail('" . $this->encrypt($val['transaction_id']) . "'); class='txt-decoration-underline' href='javascript:void(0);'>" . $val['transaction_id'] . "</a>";
                $arrCell['cell'] = array($arrCell['id'], $val['contact_name'], $transaction_id, $val['product_name'], $this->Currencyformat($val['amount']), $val['payment_mode'], $created_by, $this->OutputDateFormat($val['transaction_date'], 'dateformatampm'));
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
        } else {
            $jsonResult['rows'] = array();
        }

        return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    public function assignBatchIdAction() {
        $this->checkUserAuthentication();
        $this->getTransactionTable();
        $this->getConfig();
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $received_payment_ids = $data['received_payment_ids'];
            $payment_batch_id = $data['payment_batch_id'];
            $received_payment_id = array();
            if (!empty($received_payment_ids)) {
                foreach ($received_payment_ids as $key => $value) {
                    $received_payment_id[] = "'" . str_replace('jqg_list_', '', $value) . "'";
                }
            }
            $received_payment_id = implode(',', $received_payment_id);
            $updateData = array();
            $updateData['received_payment_id'] = $received_payment_id;
            $updateData['payment_batch_id'] = $payment_batch_id;
            $updateData['modified_date'] = DATE_TIME_FORMAT;
            $updateData['modified_by'] = $this->auth->getIdentity()->crm_user_id;
            $this->_transactionTable->assignBatchId($updateData);
            $messages = array('status' => "success", 'message' => 'Batch updated successfully');
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    public function downloadManifestAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $isEditedImagesArray = array();
        $this->getTransactionTable();
        $this->getConfig();
        $params = $this->params()->fromRoute();
        $params['id'] = $this->decrypt($params['id']);
        $searchResult = $this->getTransactionTable()->getManifestImage($params);
        $image = array();
        $galleryType = array();
        $image[] = $searchResult[0]['manifest_info'];
        $editedImages = array();
        $imageList = $image[0];
        $imageListParam['image'] = "'" . $imageList . "'";
        $isEditedImagesArray = $this->getTransactionTable()->getManifestImageEditFlag($imageListParam);

        $folderName = strtolower(substr($imageList, 0, 9));
        if ($_SERVER['REMOTE_ADDR'] == $this->_config['file_upload_path']['local_internet_ip'] && $isEditedImagesArray[0]['is_edited_image'] == 1)
            $manifestPath = $this->_config['file_upload_path']['assets_upload_dir'] . 'manifest_image/' . $folderName . '/';
        else {
            $manifestPath = $this->_config['file_upload_path']['assets_upload_dir'] . 'manifest/' . $folderName . '/';
        }
        $fileName = strtolower($imageList);
        $img_file_name = $manifestPath . $fileName;





        if (file_exists($img_file_name)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($img_file_name));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($img_file_name));
            ob_clean();
            flush();
            readfile($img_file_name);
            exit;
        }
    }

    public function viewPanelCertificateAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $isEditedImagesArray = array();
        $this->getTransactionTable();
        $this->getConfig();
        $params = $this->params()->fromRoute();
        $param['woh_id'] = $this->decrypt($params['id']);
        $searchResult = $this->getServiceLocator()->get('User\Model\UserTable')->getWoh($param);
        $img_file_name = $this->_config['file_upload_path']['assets_upload_dir'] . 'wall_panel_repro/' . $searchResult[0]['panel_no'] . ".pdf";
        if (file_exists($img_file_name)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($img_file_name));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($img_file_name));
            ob_clean();
            flush();
            readfile($img_file_name);
            exit;
        } else {
            exit;
        }
    }

    public function showProductImageAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $isEditedImagesArray = array();
        $this->getTransactionTable();
        $this->getConfig();
        $params = $this->params()->fromRoute();
        $searchParam['id'] = $this->decrypt($params['id']);
        $result = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProductInfo($searchParam);
        $productMsg = array_merge($this->_config['file_upload_path']);
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'result' => $result[0],
            'imagePath' => $this->_config['file_upload_path']['assets_url'] . '/product/',
        ));
        return $viewModel;
    }

    /**
     * This function is used to update credit card information on authorized net
     */
    public function updateProfile($paramArray, $creditCardNo) {
        //error_reporting( E_ALL );

        $customerPaymentProfile = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
        $customerPaymentProfile->getCustomerProfileRequest(array('customerProfileId' => $paramArray['credit_card_exist']));
        $customerProfileId = (int) $customerPaymentProfile->profile->customerProfileId;
        $paymentProfileId = (int) $customerPaymentProfile->profile->paymentProfiles->customerPaymentProfileId;
        $api_request_data = array(
            'customerProfileId' => $customerProfileId,
            'paymentProfile' => array(
                'billTo' => array(
                    'firstName' => $paramArray['billing_first_name'],
                    'lastName' => $paramArray['billing_last_name'],
                    'company' => '',
                    'address' => $paramArray['billing_address_1'],
                    'city' => $paramArray['billing_city'],
                    'state' => $paramArray['billing_state'],
                    'zip' => $paramArray['billing_zip_code'],
                    'country' => $paramArray['billing_country'],
                ),
                'payment' => array(
                    'creditCard' => array(
                        'cardNumber' => trim($creditCardNo),
                        'expirationDate' => 'XXXX',
                    ),
                ),
                'customerPaymentProfileId' => $paymentProfileId,
            ),
        );
        //echo "<pre>";var_dump($customerPaymentProfile->profile->paymentProfiles);echo "<pre>"; var_dump($api_request_data['paymentProfile']['payment']);

        $updateCustomerPaymentProfile = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
        $updateCustomerPaymentProfile->updateCustomerPaymentProfileRequest($api_request_data);

        if ($updateCustomerPaymentProfile->isSuccessful()) {
            return true;
        } else {
             $messages = array('status' => "credit_card_error", 'message' => trim($updateCustomerPaymentProfile->messages->message->text.','. $updateCustomerPaymentProfile->messages->message->code));
           //  return  $response->setContent(\Zend\Json\Json::encode($messages));
          // die;
            //echo $updateCustomerPaymentProfile->messages->message->text, $updateCustomerPaymentProfile->messages->message->code;
            //return false;
        }
    }

    /**
     * This function is used to get select category structure
     * @param       void
     * @return     json
     * @author     Icreon Tech - DT
     */
    public function getSelectArr($categories, $parentVal = 0, $level = 0) {

        $this->_categorySelectArr[''] = "Select Category";
        foreach ($categories as $cat) {
            if ($cat['parent_id'] == $parentVal) {
                $this->_categorySelectArr[$cat['category_id']] = str_repeat('-', $level) . $cat['category_name'];
                $this->getSelectArr($categories, $cat['category_id'], $level + 1);
            }
        }
    }

    function calculateProductShippingDiscount($indShippableProductWeight, $totalShippableProductWeight, $shippingDiscountAmountOff) {
        $percentWeight = ($indShippableProductWeight * 100) / $totalShippableProductWeight;
        $shippingDiscountAmount = ($percentWeight * $shippingDiscountAmountOff) / 100;
        return $shippingDiscountAmount;
    }

    function calculateProductCartDiscount($indProductAmount, $totalCartAmount, $cartDiscountAmountOff) {
        $percentAmount = ($indProductAmount * 100) / $totalCartAmount;
        $productDiscount = round(($percentAmount * $cartDiscountAmountOff) / 100,2);
        return $productDiscount;
    }

    /*function calculateFreeProductDiscount($productQty, $productAmount) {
        $productDiscount = $productAmount / $productQty;
        return $productDiscount;
    }*/

	function calculateFreeProductDiscount($product, $itemToDiscount) {        
        $productId = $product['product_id'];
        $productDiscountValue = $product['product_subtotal'] - $product['membership_discount'];
        $discountValue = 0;
        foreach($itemToDiscount as $key => $discountItem)
        {
            if($key == $productId)
            {
                
                for($i = 0;$i< count($discountItem['itemQty']);$i++)
                {
                    $currentDiscountValue = $discountItem['itemTotalAmount'][$i]/$discountItem['itemQty'][$i];
                    if($currentDiscountValue < $discountValue || $discountValue == 0)
                    {
                        $discountValue = $currentDiscountValue;
                    }
                }
            }
        }
        $productDiscountValue = round(($product['product_subtotal'] - $product['membership_discount'])/$product['product_qty'],2);
        
        $currentDiscountValue = round($discountValue,2);
        if($productDiscountValue == $currentDiscountValue)
        {
            return $currentDiscountValue;
        }
        return 0;
    }

    /**
     * This function is used to capture the payment from front side
     * @param      $paymentArray array(), $paymentFrontArray array()
     * @return     json
     * @author     Icreon Tech - SR by dev 3
     */
    function capturePaymentFront($paymentArray, $paymentFrontArray, $isPromotionApplied) {

        $paymentAmount = $paymentFrontArray['transaction_amounts'];
        $createCustomerProfile = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
        $customerProfileId = $paymentFrontArray['profile_id'];
        $createCustomerProfile->getCustomerProfileRequest(array('customerProfileId' => $customerProfileId));
        $paymentProfileId = $createCustomerProfile->profile->paymentProfiles->customerPaymentProfileId;
        $payment = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
        $payment->createCustomerProfileTransactionRequest(array(
            'transaction' => array(
                'profileTransAuthCapture' => array(
                    'amount' => $paymentAmount,
                    'customerProfileId' => $customerProfileId,
                    'customerPaymentProfileId' => $paymentProfileId,
                    'order' => array(
                        'invoiceNumber' => 'SK' . sprintf('%011d', $paymentFrontArray['transaction_id']),
                        'description' => 'description of transaction',
                        'purchaseOrderNumber' => 'No' . sprintf('%011d', $paymentFrontArray['transaction_id'])),
                )
            ),
            'extraOptions' => '<![CDATA[x_customer_ip=' . $_SERVER['SERVER_ADDR'] . ']]>'
        ));
        $payment_code = (array) ($payment->messages->message->code);
        if ($payment->isSuccessful() || ($isPromotionApplied == 1 && $payment_code[0] == 'E00003' && $paymentFrontArray['transaction_amounts'] == '0')) {
            $transaction = new Transaction($this->_adapter);
            $responseArr = explode(',', $payment->directResponse);
            $authorizeTransactionId = $responseArr[6];

            $paymentTransactionData = array();
            $paymentTransactionData['amount'] = $paymentFrontArray['transaction_amounts'];
            $paymentTransactionData['transaction_id'] = $paymentFrontArray['transaction_id'];
            $paymentTransactionData['payment_mode_id'] = 1;
            $paymentTransactionData['authorize_transaction_id'] = $authorizeTransactionId;

            $paymentTransactionData['profile_id'] = $customerProfileId;
            $paymentTransactionData['added_by'] = 0;
            $paymentTransactionData['added_date'] = DATE_TIME_FORMAT;
            $paymentTransactionData['modify_by'] = 0;
            $paymentTransactionData['modify_date'] = DATE_TIME_FORMAT;
            $paymentTransactionData = $transaction->getTransactionPaymentArr($paymentTransactionData);

            $profileDataParam = array();
            $profileDataParam['profile_id'] = $customerProfileId;
            $tokenDetailsResult = $this->getServiceLocator()->get('User\Model\UserTable')->getTokenDetails($profileDataParam);

            if (count($tokenDetailsResult) > 0)
                $paymentTransactionData['22'] = $tokenDetailsResult[0]['credit_card_type_id'];
            else
                $paymentTransactionData['22'] = '';
            $transactionPaymentId = $this->_transactionTable->saveTransactionPaymentDetail($paymentTransactionData);

            foreach ($paymentArray as $key => $val) {
                $paymentReceivedTransactionData = array();
                $paymentReceivedTransactionData['table_auto_id'] = $val['table_auto_id'];
                $paymentReceivedTransactionData['table_type'] = $val['table_type'];
                $paymentReceivedTransactionData['authorize_transaction_id'] = $authorizeTransactionId;
                $paymentReceivedTransactionData['profile_id'] = $customerProfileId;
                $paymentReceivedTransactionData['amount'] = $val['cart_product_amount'];
                $paymentReceivedTransactionData['product_type_id'] = $val['cart_product_type_id'];
                $paymentReceivedTransactionData['transaction_payment_id'] = $transactionPaymentId;
                $paymentReceivedTransactionData['transaction_id'] = $val['transaction_id'];
                $paymentReceivedTransactionData['payment_source'] = 'credit';
                $paymentReceivedTransactionData['isPaymentReceived'] = '1';
                $this->insertIntoPaymentReceive($paymentReceivedTransactionData);
            }
            // code used to remove the authorize transaction id from payment table.
            $removeTransParam = array();
            $removeTransParam['transaction_id'] = $paymentFrontArray['transaction_id'];
            $this->_transactionTable->removeAuthorizeTransactionId($removeTransParam);
        } else {
	    //Ticket 1211
            $responseArr = explode(',', $payment->directResponse);
            $authorizeTransactionId = $responseArr[6];
            // Code to display the error message for declined transactions.
            $paymentErrorArray = explode('-', $payment->messages->message->text);
            if (isset($paymentErrorArray[1]) && !empty($paymentErrorArray[1]))
                $paymentErrMessage = $paymentErrorArray[1];
            else
                $paymentErrMessage = $paymentErrorArray[0];
	    //Ticket 1211
	    $paymentErrCode=(string)$payment->messages->message->code;
	    $messages = array('status' => "payment_error", 'message' => trim($paymentErrMessage), 'authorizeTransactionId' => $authorizeTransactionId, 'payment' => $payment,'paymentErrCode'=>$paymentErrCode);
            //$removeTransactionArr[0] = $paymentFrontArray['transaction_id'];
            //$this->_transactionTable->removeTransactionById($removeTransactionArr);
            return $messages;
        }
    }

    /**
     * This function is used to check if transaction already associated with other user
     * @return     json
     * @author     Icreon Tech
     */
    public function checkUserTransactionAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $this->getTransactionTable();
        $response = $this->getResponse();
        $msg = array();
        if ($request->isXmlHttpRequest()) {
            $transactionId = $request->getPost('transaction_id');
            $searchParams = array();
            if (!empty($transactionId)) {
                $transactionId = $this->decrypt($transactionId);
                $searchParams['transaction_id'] = $transactionId;
                $result = $this->_transactionTable->checkUserTransaction($searchParams);
                $msg['status'] = "success";
                if (!empty($result['cnt'])) {
                    $msg['status'] = "error";
                }
                return $response->setContent(\Zend\Json\Json::encode($msg));
            }
        } else {
            return false;
        }
    }

    /**
     * This function is used to save credit
     * @return     json
     * @author     Icreon Tech
     */
    public function creditUserTransactionAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $this->getTransactionTable();
        $response = $this->getResponse();
        $transaction = new Transaction($this->_adapter);
        $msg = array();
        if ($request->isXmlHttpRequest()) {
            $transactionId = $request->getPost('transaction_id');
            $userId = $request->getPost('user_id');
            $transactionNote = $request->getPost('note');
            $searchParams = array();
            if (!empty($transactionId)) {
                $transactionId = $this->decrypt($transactionId);
                $userId = $this->decrypt($userId);
                $searchParams['transaction_id'] = $transactionId;
                $transactionDetails = $this->_transactionTable->getUserTransactionDetails($searchParams);
                if (!empty($transactionDetails[0])) {
                    //insert to user soft credit table
                    $saveParams = $transaction->getUserSoftCreditArr($transactionDetails[0]);
                    $saveParams['user_id'] = $userId;
                    $saveParams['notes'] = $transactionNote;
                    //$saveParams['credit_date'] = date('Y-m-d H:i:s',strtotime(DATE_TIME_FORMAT));
                    $saveParams['credit_date'] = $transactionDetails[0]['transaction_date'];
                    $saveParams['added_date'] = date('Y-m-d H:i:s', strtotime(DATE_TIME_FORMAT));
                    ;
                    $saveParams['added_by'] = $this->auth->getIdentity()->crm_user_id;
                    $returnStatus = $this->_transactionTable->saveSoftCreditTransaction($saveParams);
                }
                $msg['status'] = "error";
                if (!empty($returnStatus)) {
                    $msg['status'] = "success";
                }
                return $response->setContent(\Zend\Json\Json::encode($msg));
            }
        } else {
            return false;
        }
    }

    /**
     * This action is used to set manifest
     * @return json
     * @param void
     * @author Icreon Tech - AS
     */
    public function markWrongManifestAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $isEditedImagesArray = array();
        $this->getTransactionTable();
        $this->getConfig();
        $searchResult = $this->getTransactionTable()->getManifestImage($params);
        $msg = array();
        $msg['status'] = "error";
        $msg['msg'] = "Please try again.";
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $updateParams = array();
            $updateParams['id'] = $this->decrypt($request->getPost('transaction_id'));
            $updateParams['status'] = $request->getPost('status');
            $returnStatus = $this->getTransactionTable()->markManifestImage($updateParams);

            if ($returnStatus) {
                $msg['status'] = "success";
                $msg['msg'] = "Manifest has been marked as wrong";
            }
        }
        return $response->setContent(\Zend\Json\Json::encode($msg));
    }

    /**
     * This action is used to save transaction from cart for cash payment
     * @return json
     * @param void
     * @author Icreon Tech - SR
     */
    public function saveCashFinalTransaction($postArr) {
        $response = $this->getResponse();
        $this->getConfig();
        $msgArr = $this->_config['transaction_messages']['config']['create_crm_transaction'];
        $postArr['shipping_method'] = (!empty($postArr['shipping_method']) && (empty($postArr['pick_up']) || $postArr['pick_up'] == 0)) ? $postArr['shipping_method'] : '';

        if ($postArr['pick_up'] == 1) {
                 $postArr['shipping_state'] = '';
			}

        $responseArr = array();
        $cartProductArr = array();
        $cartProductArr['user_id'] = $postArr['user_id'];
        $cartProductArr['userid'] = $postArr['user_id'];
        $cartProductArr['shipcode'] = isset($postArr['shipping_zip_code']) ? $postArr['shipping_zip_code'] : '';
        $cartProductArr['source_all'] = !empty($postArr['source_all']) ? $postArr['source_all'] : '';
        $cartProductArr['shipping_method'] = $postArr['shipping_method'];
        $cartProductArr['couponcode'] = !empty($postArr['couponcode']) ? $postArr['couponcode'] : '';
        $cartProductArr['postData'] = $postArr;
        $cartProductArr['free_shipping'] = false;
        $isFreeShipping = false;
        $postArr['is_free_shipping'] = '0';
        $freeShippingAmount = '';


        $transaction = new Transaction($this->_adapter);


        $this->campaignSession = new Container('campaign');
        $cartProductData = $this->cartTotalWithTax($cartProductArr);
        //asd($cartProductData);

        $postArr['promotion_id'] = $cartProductData['promotionData']['promotion_id'];

        if (isset($cartProductData['cartData'])) {
            $cartProducts = $cartProductData['cartData'];
        }
        $responseArr['status'] = 'error';
        if (!empty($cartProducts)) {
            $totalSubtotalAmount = 0;
            $totalDiscountAmount = 0;
            $totalTaxableAmount = 0;
            $totalCartAmount = 0;
            $totalShippingAmount = 0;
            $totalShippingHandlingAmount = 0;

            foreach ($cartProducts as $cartKey => $cartProduct) {
                $totalSubtotalAmount+= $cartProduct['product_subtotal'] - $cartProduct['membership_discount'];
                $totalDiscountAmount+= $cartProduct['product_discount'];
                $totalTaxableAmount+= $cartProduct['product_taxable_amount'];
                $totalCartAmount+=$cartProduct['product_total'];
                $totalProductRevenue+=$cartProduct['total_product_revenue'];

                if (isset($cartProduct['shipping_discount_amount']))
                    $totalShippingAmount+=($cartProduct['shipping_price'] - $cartProduct['shipping_discount_amount']);
                elseif(isset($cartProduct['shipping_discount_in_amount']))
                    $totalShippingAmount+=($cartProduct['shipping_price'] - $cartProduct['shipping_discount_in_amount']);
				else
                    $totalShippingAmount+=$cartProduct['shipping_price'];

                $totalShippingHandlingAmount+=$cartProduct['surcharge_price'] + $cartProduct['shipping_handling_price'];
                $productTotal = $cartProduct['product_total'];

                $i++;
            }

            /* Add transaction record */

            $dateTime = DATE_TIME_FORMAT;

            $finalTransaction = array();
            $finalTransaction['user_id'] = $postArr['user_id'];
            $finalTransaction['amount'] = $totalCartAmount;
            $finalTransaction['payment_mode_id'] = 4; // cash payment
            $finalTransaction['is_payment_status'] = 0;
            $finalTransaction['transaction_date'] = $dateTime;

            if($postArr['cash_payment'] == 0) {
	            $finalTransaction['billing_title'] = $postArr['billing_title'];
	            $finalTransaction['billing_first_name'] = $postArr['billing_first_name'];
	            $finalTransaction['billing_last_name'] = $postArr['billing_last_name'];
	            $finalTransaction['billing_company_name'] = '';
	            $finalTransaction['billing_address'] = $postArr['billing_address_1'].$postArr['billing_address_2'];
	            $finalTransaction['billing_city'] = $postArr['billing_city'];
	            $finalTransaction['billing_state'] = $postArr['billing_state'];
	            $finalTransaction['billing_postal_code'] = $postArr['billing_zip_code'];
	            $finalTransaction['billing_country'] = $postArr['billing_country'];
	            $finalTransaction['billing_phone_no'] = $postArr['billing_country_code'].$postArr['billing_area_code'].$postArr['billing_phone'];
            }
	        else {
				$finalTransaction['billing_title'] = '';
	            $finalTransaction['billing_first_name'] = '';
	            $finalTransaction['billing_last_name'] = '';
	            $finalTransaction['billing_company_name'] = '';
	            $finalTransaction['billing_address'] = '';
	            $finalTransaction['billing_city'] = '';
	            $finalTransaction['billing_state'] = '';
	            $finalTransaction['billing_postal_code'] = '';
	            $finalTransaction['billing_country'] = '';
	            $finalTransaction['billing_phone_no'] = '';
            }
            $finalTransaction['shipping_address_type'] = $postArr['is_apo_po'];

            if ($postArr['pick_up'] == 0) {
                    $finalTransaction['shipping_title'] = $postArr['shipping_title'];
                    $finalTransaction['shipping_first_name'] = $postArr['shipping_first_name'];
                    $finalTransaction['shipping_last_name'] = $postArr['shipping_last_name'];
                    $finalTransaction['shipping_company_name'] = '';
                    $finalTransaction['shipping_address'] = $postArr['shipping_address_1'].$postArr['shipping_address_2'];
                    $finalTransaction['shipping_city'] = $postArr['shipping_city'];
                    $finalTransaction['shipping_state'] = $postArr['shipping_state'];
                    $finalTransaction['shipping_postal_code'] = $postArr['shipping_zip_code'];
                    $finalTransaction['shipping_country'] = $postArr['shipping_country'];
                    $finalTransaction['shipping_phone_no'] = $postArr['shipping_country_code'].$postArr['shipping_area_code'].$postArr['shipping_phone'];
            }
            else {
                    $finalTransaction['shipping_title'] = '';
                    $finalTransaction['shipping_first_name'] = '';
                    $finalTransaction['shipping_last_name'] = '';
                    $finalTransaction['shipping_company_name'] = '';
                    $finalTransaction['shipping_address'] = '';
                    $finalTransaction['shipping_city'] = '';
                    $finalTransaction['shipping_state'] = '';
                    $finalTransaction['shipping_postal_code'] = '';
                    $finalTransaction['shipping_country'] = '';
                    $finalTransaction['shipping_phone_no'] = '';

            }
            $finalTransaction['added_date'] = $dateTime;
            $finalTransaction['modified_date'] = $dateTime;
            $finalTransaction['coupon_code'] = $postArr['couponcode'];
            $finalTransaction['pick_up'] = $postArr['pick_up'];
            $finalTransaction['shipping_method'] = $postArr['shipping_method'];
            $transactionId = $this->_transactionTable->saveFinalCashTransaction($finalTransaction);
        foreach ($cartProducts as $cartKey => $cartProduct) {
                    if (isset($postArr['cash_payment']) && $postArr['cash_payment'] == '1') { // cash payment
                        $isPendingTransaction = 1;
                        $updateCartParam = array();
                        $updateCartParam['cart_id'] = $cartProduct['cart_id'];
                        $updateCartParam['is_pending_transaction'] = $isPendingTransaction;
                        $updateCartParam['pending_transaction_id'] = $transactionId;
                        $updateCartParam['product_type_id'] = $cartProduct['product_type_id'];
                        $updateCartParam['user_id'] = $cartProduct['user_id'];
                        $updateCartParam['pending_transaction_date'] = $dateTime;
                        $updateCartParam['item_info'] = $cartProduct['item_info'];
                        $updateCartParam['product_mapping_id'] = $cartProduct['product_mapping_id'];
                        $updateCartParam['type_of_product'] = $cartProduct['type_of_product'];
                        $this->_transactionTable->updateCartTableInformation($updateCartParam);
                    }
                }
        }
         $responseArr['status'] = 'success';
         $responseArr['transaction_id'] = $transactionId;

            /* Send order Email */
               $param['user_id'] = $postArr['user_id'];
               $userDataArr = $this->getServiceLocator()->get('User\Model\UserTable')->getUser(array('user_id' => $postArr['user_id']));

               if(!empty($userDataArr['email_id'])) // order email
               {
                   $email_id_template_int = '44'; // cash order email
                    $userEmailDataArr['transaction_amount'] = $totalCartAmount;
                    $userEmailDataArr['first_name'] = $userDataArr['first_name'];
                    $userEmailDataArr['last_name'] = $userDataArr['last_name'];
                    $userEmailDataArr['email_id'] = $userDataArr['email_id'];
                    $userEmailDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                    $userEmailDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                   $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userEmailDataArr, $email_id_template_int, $this->_config['email_options']);
               }
        return $responseArr;
    }

    /**
     * This action is used to print the manifest/ship image
     * @return json
     * @param void
     * @author Icreon Tech - SR
     */
    public function printOrderFilesAction(){
		$this->getTransactionTable();
		//error_reporting(E_ALL);
		$params = $this->params()->fromRoute();

        $transactionId = $this->decrypt($params['transaction_id']);
        $paymentMethod = $this->decrypt($params['payment_method']);
        $searchParam['transaction_id'] = $transactionId;
		$jsLangTranslate = $this->_config['transaction_messages']['config']['ship_search'];

		$searchParam['transactionId'] = $transactionId;
		$manifestImage = array();
		$protocol = ($_SERVER['HTTPS'] == 'on')?'https://':'http://';
		$protocol1 = ($_SERVER['HTTPS'] == 'on')?'https:':'http:';

		if($paymentMethod == 'cash') { 
			$searchProduct = $this->_transactionTable->searchCashTransactionProducts($searchParam);

			foreach ($searchProduct as $key => $val) {
				switch ($val['tra_pro']) {
					case 'fof':
						$fofResult = $this->_transactionTable->searchFofDetails($val);
						array_push($searchProduct[$key], $fofResult[0]);
						break;
					case 'pro':
						$passengerResult = $this->_transactionTable->searchDocPass($val);
						if(empty($passengerResult)){
							$passengerResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getPassengerRecordByPID(array('passenger_id'=>$val['item_id']));
						}
						array_push($searchProduct[$key], $passengerResult[0]);
						break;
				}
			}
					
			$kioskParam = array();	
			$kioskParam['kiosk_id'] = $this->auth->getIdentity()->kiosk_id;
			$kioskDetails = $this->getServiceLocator()->get('Kiosk\Model\KioskManagementTable')->getkioskDetails($kioskParam);	
				
//n-woh- start 
			$arrFinalWallOfHonorId = array();
			$arrFinalWallOfHonorIdContacts = array();
//n-woh- end
			$pdf1 = $pdf1.'<page><table style="width:100%" cellspacing="5" cellpadding="2" style="padding-top:20px;background-color: #FAFAFA; font: 12pt \"Tahoma\"" align="center"><tr><td style="font: 12px/16px arial; font-weight:bold; color: #000; text-align:left" width="2%">&nbsp;Name: </td><td width="5%">'.ucfirst($this->auth->getIdentity()->first_name).' '.ucfirst($this->auth->getIdentity()->last_name).'</td>
			<td style="font: 12px/16px arial; font-weight:bold; color: #000; text-align:left" width="4%">Station ID: </td><td style="text-align:left" width="5%">'.$kioskDetails[0]['kiosk_name'].'</td>';
				
			if(!empty($passengerID))
				$pdf1.='<td style="font: 12px/16px arial; font-weight:bold; color: #000; text-align:left" width="5%">Passenger ID: </td><td style="text-align:left" width="5%">'.$passengerID.'</td>';
			if(!empty($paymentMethod)){
				$pdf1.='<td style="font: 12px/16px arial; font-weight:bold; color: #000; text-align:left" width="2%">Payment Method: </td><td style="text-align:left" width="5%">'.ucfirst($paymentMethod).'</td>';
			}						
			$pdf1.='<td style="font: 12px/16px arial; font-weight:bold; color: #000; text-align:left" width="2%">Order #: </td><td style="text-align:left" width="5%">'.$searchParam['transactionId'].'</td>';
			$pdf1.='</tr></table>';
            $pdf1.= '<div><table width="100%" border="0" align="center" style="border-bottom:1px dotted #ccc;" cellspacing="2" cellpadding="2" class="table-bh tra-view">
                    <tr>
                    <th style="font: 12px/16px arial; font-weight:bold; color: #000; text-align:left; padding-left:0; border: 1px solid #999; border-collapse: collapse; padding: 8px; vertical-align: middle; background: #ccc; font-family: "open_sanssemibold"; font-size: 15px; color: #000;">Sr. No</th>
                    <th style="font: 12px/16px arial; font-weight:bold; color: #000; text-align:left; border: 1px solid #999; border-collapse: collapse; padding: 8px; vertical-align: middle; background: #ccc; font-family: "open_sanssemibold"; font-size: 15px; color: #000;">Product</th>
                    <th style="font: 12px/16px arial; font-weight:bold; color: #000; text-align:left; border: 1px solid #999; border-collapse: collapse; padding: 8px; vertical-align: middle; background: #ccc; font-family: "open_sanssemibold"; font-size: 15px; color: #000;">SKU</th>
                    <th style="font: 12px/16px arial; font-weight:bold; color: #000; text-align:left; border: 1px solid #999; border-collapse: collapse; padding: 8px; vertical-align: middle; background: #ccc; font-family: "open_sanssemibold"; font-size: 15px; color: #000;">Qty.</th>
					</tr>';
             $i = 1;

			foreach ($searchProduct as $key => $val) {
			  $pdf1.= '<tr><td style="width: 48px; font: 12px/16px arial; color: #7e8183; text-align:left; vertical-align:top;">' . $i++ . '</td>
					<td style="font: 12px/16px arial; color: #7e8183; text-align:left">';
			  switch ($val['tra_pro']) {
				case 'don' :
					$pdf1.=$val['product_name'] . ' <label>' . $val['option_name'] . '</label>';
					break;
				case 'bio' :
					$pdf1.=$val['product_name'] . '<br> <label>' . $val['item_name'] . '</label>';
					break;
				case 'fof' :
					$pdf1.=$val['product_name'] . '<br> <label>' . $val['donated_by'] . '</label>';
					break;
				case 'pro' :
					switch ($val['product_type']) {

						case '5' :
							$pdf1.=$val['product_name'] . '<label> :</label> ' . $val['option_name'] . '<br><label>Passenger Name : ' . $val['item_name'] . '</label><br/><label>Passenger Id : </label> ' . $val['item_id'];
							break;
						case '6' :
							$pdf1.=$val['product_name'] . '<br><label> ' . $val['final_name'] . '</label>';
							break;
						case '7' :
							$itemName = '';
							if (strlen($val[0]['item_id']) < 12)
								$itemName = "Ship Name : " . $val['item_name'];
							else
								$itemName = "Passenger Name : " . $val['item_name'];

							if (!empty($val['item_info']) && $val['item_info'] != '0000-00-00')
								$arrivalDate = $this->OutputDateFormat($val['item_info'], 'calender');
							else
								$arrivalDate = '';

							$productName = $val['product_name'];
							if (isset($val['manifest_info']) && !empty($val['manifest_info']))
								$productName.= ' : ' . $val['manifest_info'];

							$pdf1.=$productName . '<br>  ' . $itemName . '<br/><label>Arrival Date : </label>' . $arrivalDate. '<br/><label>Ref Line Number : </label>' . $val[0]['REF_PAGE_LINE_NBR'];
							break;
						case '8' :
							if (!empty($val['item_info']) && $val['item_info'] != '0000-00-00')
								$arrivalDate = $this->OutputDateFormat($val['item_info'], 'calender');
							else
								$arrivalDate = '';
							$pdf1.=$val['product_name'] . '<label> : </label>' . $val['option_name'] . '<br/><label>Ship Name : </label>' . $val['item_name'] . '<br/><label>Arrival Date : </label>' . $arrivalDate;
							break;
						default :
							$pdf1.=$val['product_name'] . ' <label>' . $val['option_name'] . '</label>';
							break;
					}
					break;
					case 'woh' :
//n-woh- start 
						if (isset($val['id']) and trim($val['id']) != "" and !in_array(trim($val['id']), $arrFinalWallOfHonorId)) {
							array_push($arrFinalWallOfHonorId, trim($val['id']));
						}
//n-woh- end
						$pdf1.=$val['product_name'] . '<br> <label>' . utf8_decode($val['option_name']) . '</label>';
						break;
				}
				$pdf1 .='</td><td style="font: 12px/16px arial; color: #7e8183; text-align:left">';
				if (isset($val['product_sku']) && !empty($val['product_sku'])) {
					$pdf1 .=$val['product_sku'];
				} else {
					$pdf1 .='N/A';
				}
				$pdf1.='</td><td style="font: 12px/16px arial; color: #7e8183; text-align:left">' . $val['product_quantity'] . '</td></tr>';
			}
            $pdf1 .= '</table></div></page>';
		}else {		    
		// code to get the manifest image name.
			    
            /** Order Details Template */
			$searchParam['transactionId'] = $this->decrypt($params['transaction_id']);
			$searchResult = $this->_transactionTable->searchTransactionDetails($searchParam);
			$searchProduct = $this->_transactionTable->searchTransactionProducts($searchParam);
			foreach ($searchProduct as $key => $val) {
				switch ($val['tra_pro']) {
					case 'fof':
						$fofResult = $this->_transactionTable->searchFofDetails($val);
						array_push($searchProduct[$key], $fofResult[0]);
						break;
					case 'pro':
						$passengerResult = $this->_transactionTable->searchDocPass($val);
						array_push($searchProduct[$key], $passengerResult[0]);
						break;
				}
				$proParam['transactionId'] = $searchParam['transactionId'];
				$proParam['productId'] = $val['id'];
				$proResult = $this->_transactionTable->searchProductsShipped($proParam);
				if (isset($proResult[0]) && !empty($proResult[0])) {
					$searchProduct[$key]['shippedQty'] = $proResult[0]['shippped_qty'];
				} else {
					$searchProduct[$key]['shippedQty'] = 0;
				}
			}
					
			$kioskParam = array();	
			$kioskParam['kiosk_id'] = $this->auth->getIdentity()->kiosk_id;
			$kioskDetails = $this->getServiceLocator()->get('Kiosk\Model\KioskManagementTable')->getkioskDetails($kioskParam);	
				
//n-woh- start 
			$arrFinalWallOfHonorId = array();
			$arrFinalWallOfHonorIdContacts = array();
//n-woh- end
			$pdf1.='<page><table cellspacing="5" cellpadding="2" style="padding-top:20px;" align="center"><tr><td style="font: 12px/16px arial; font-weight:bold; color: #000; text-align:left" width="2%">&nbsp;Name: </td><td width="5%">'.ucfirst($this->auth->getIdentity()->first_name).' '.ucfirst($this->auth->getIdentity()->last_name).'</td>
			<td style="font: 12px/16px arial; font-weight:bold; color: #000; text-align:left" width="4%">Station ID: </td><td style="text-align:left" width="5%">'.$kioskDetails[0]['kiosk_name'].'</td>';
				
			if(!empty($passengerID))
				$pdf1.='<td style="font: 12px/16px arial; font-weight:bold; color: #000; text-align:left" width="5%">Passenger ID: </td><td style="text-align:left" width="5%">'.$passengerID.'</td>';
			if(!empty($paymentMethod)){
				$pdf1.='<td style="font: 12px/16px arial; font-weight:bold; color: #000; text-align:left" width="2%">Payment Method: </td><td style="text-align:left" width="5%">'.ucwords(str_replace('_',' ',$paymentMethod)).'</td>';
			}						
			$pdf1.='<td style="font: 12px/16px arial; font-weight:bold; color: #000; text-align:left" width="2%">Order #: </td><td style="text-align:left" width="5%">'.$searchParam['transactionId'].'</td>';
			$pdf1.='</tr></table>';
			$pdf1.= '<div><table width="100%" border="0" align="center" style="border-bottom:1px dotted #ccc;" cellspacing="2" cellpadding="2" class="table-bh tra-view">
				<tr>
				<th style="font: 12px/16px arial; font-weight:bold; color: #000; text-align:left; padding-left:0; border: 1px solid #999; border-collapse: collapse; padding: 8px; vertical-align: middle; background: #ccc; font-family: "open_sanssemibold"; font-size: 15px; color: #000;">Sr. No</th>
				<th style="font: 12px/16px arial; font-weight:bold; color: #000; text-align:left border: 1px solid #999; border-collapse: collapse; padding: 8px; vertical-align: middle; background: #ccc; font-family: "open_sanssemibold"; font-size: 15px; color: #000;">Product</th>
				<th style="font: 12px/16px arial; font-weight:bold; color: #000; text-align:left border: 1px solid #999; border-collapse: collapse; padding: 8px; vertical-align: middle; background: #ccc; font-family: "open_sanssemibold"; font-size: 15px; color: #000;">SKU</th>
				<th style="font: 12px/16px arial; font-weight:bold; color: #000; text-align:left border: 1px solid #999; border-collapse: collapse; padding: 8px; vertical-align: middle; background: #ccc; font-family: "open_sanssemibold"; font-size: 15px; color: #000;">Qty.</th>
				<th style="font: 12px/16px arial; font-weight:bold; color: #000; text-align:center border: 1px solid #999; border-collapse: collapse; padding: 8px; vertical-align: middle; background: #ccc; font-family: "open_sanssemibold"; font-size: 15px; color: #000;">Price</th>
				<th style="font: 12px/16px arial; font-weight:bold; color: #000; text-align:right border: 1px solid #999; border-collapse: collapse; padding: 8px; vertical-align: middle; background: #ccc; font-family: "open_sanssemibold"; font-size: 15px; color: #000;">SubTotal</th></tr>';
			$i = 1;

			foreach ($searchProduct as $key => $val) {
				$pdf1.= '<tr><td style="width: 48px; font: 12px/16px arial; color: #7e8183; text-align:left; vertical-align:top;">' . $i++ . '</td>
					<td style="font: 12px/16px arial; color: #7e8183; text-align:left">';
				switch ($val['tra_pro']) {
					case 'don' :
						$pdf1.=$val['product_name'] . ' <label>' . $val['option_name'] . '</label>';
						break;
					case 'bio' :
						$pdf1.=$val['product_name'] . '<br> <label>' . $val['item_name'] . '</label>';
						break;
					case 'fof' :
						$pdf1.=$val['product_name'] . '<br> <label>' . $val[0]['donated_by'] . '</label>';
						break;
					case 'pro' :
						switch ($val['product_type']) {

							case '5' :
								$pdf1.=$val['product_name'] . '<label> :</label> ' . $val['option_name'] . '<br><label>Passenger Name : ' . $val[0]['NAME'] . '</label><br/><label>Passenger Id : </label> ' . $val[0]['item_id'];
								break;
							case '6' :
								$pdf1.=$val['product_name'] . '<br><label> ' . $val[0]['final_name'] . '</label>';
								break;
							case '7' :
								$itemName = '';
								if (strlen($val[0]['item_id']) < 12)
									$itemName = "Ship Name : " . $val['item_name'];
								else
									$itemName = "Passenger Name : " . $val['item_name'];

								if (!empty($val['item_info']) && $val['item_info'] != '0000-00-00')
									$arrivalDate = $this->OutputDateFormat($val['item_info'], 'calender');
								else
									$arrivalDate = '';

								$productName = $val['product_name'];
								if (isset($val['manifest_info']) && !empty($val['manifest_info']))
									$productName.= ' : ' . $val['manifest_info'];

								$pdf1.=$productName . '<br>  ' . $itemName . '<br/><label>Arrival Date : </label>' . $arrivalDate. '<br/><label>Ref Line Number : </label>' . $val[0]['REF_PAGE_LINE_NBR'];
								break;
							case '8' :
								if (!empty($val['item_info']) && $val['item_info'] != '0000-00-00')
									$arrivalDate = $this->OutputDateFormat($val['item_info'], 'calender');
								else
									$arrivalDate = '';
								$pdf1.=$val['product_name'] . '<label> : </label>' . $val['option_name'] . '<br/><label>Ship Name : </label>' . $val['item_name'] . '<br/><label>Arrival Date : </label>' . $arrivalDate;
								break;
							default :
								$pdf1.=$val['product_name'] . ' <label>' . $val['option_name'] . '</label>';
								break;
						}
						break;
					case 'woh' :

//n-woh- start 
						if (isset($val['id']) and trim($val['id']) != "" and !in_array(trim($val['id']), $arrFinalWallOfHonorId)) {
							array_push($arrFinalWallOfHonorId, trim($val['id']));
						}
//n-woh- end
						$pdf1.=$val['product_name'] . '<br> <label>' . utf8_decode($val['option_name']) . '</label>';
						break;
				}
				$pdf1 .='</td><td style="font: 12px/16px arial; color: #7e8183; text-align:left">';
				if (isset($val['product_sku']) && !empty($val['product_sku'])) {
					$pdf1 .=$val['product_sku'];
				} else {
					$pdf1 .='N/A';
				}
				$pdf1.='</td><td style="font: 12px/16px arial; color: #7e8183; text-align:left">' . $val['product_quantity'] . '</td>
					<td style="font: 12px/16px arial; color: #7e8183; text-align:center">$' . $this->Currencyformat($val['product_price']) . '</td>
					<td style="font: 12px/16px arial; color: #7e8183; text-align:right">$' . $this->Currencyformat($val['product_sub_total']) . '</td></tr>';
			}
			$pdf1 .= '<tr>
				<td align="right" colspan="5"><span style="text-align:right; font: 12px/16px arial; color: #7e8183;">Sub Total :</span></td>
				<td align="right">
				<strong style="text-align:right; font: 12px/16px arial; color: #7e8183; font-weight:bold;">$' . $this->Currencyformat($searchResult[0]['sub_total_amount']) . '
				</strong></td>
				</tr>
				<tr>
				<td align="right" colspan="5"><span style="text-align:right; font: 12px/16px arial; color: #7e8183;">Shipping & Handling :</span></td>
				<td align="right">
				<strong style="text-align:right; font: 12px/16px arial; color: #7e8183; font-weight:bold;">$' . $this->Currencyformat($searchResult[0]['shipping_amount'] + $searchResult[0]['handling_amount']) . '
				</strong></td>
				</tr>
				<tr>
				<td align="right" colspan="5"><span style="text-align:right; font: 12px/16px arial; color: #7e8183;">Tax :</span></td>
				<td align="right">
				<strong style="text-align:right; font: 12px/16px arial; color: #7e8183; font-weight:bold;">$' . $this->Currencyformat($searchResult[0]['total_tax']) . '
				</strong></td>
				</tr>
				<tr>
				<td align="right" colspan="5"><span style="text-align:right; font: 12px/16px arial; color: #7e8183;">Discount :</span></td>
				<td align="right">
				<strong style="text-align:right; font: 12px/16px arial; color: #7e8183; font-weight:bold;">($' . $this->Currencyformat($searchResult[0]['total_discount']) . ')
				</strong></td>
				</tr>
				<tr>
				<td align="right" colspan="5"><span style="text-align:right; font: 12px/16px arial; color: #7e8183;">Purchase Total :</span></td>
				<td align="right">
				<strong style="text-align:right; font: 12px/16px arial; color: #7e8183; font-weight:bold;">$' . $this->Currencyformat($searchResult[0]['transaction_amount'] - ($searchResult[0]['cancel_amount'] + $searchResult[0]['refund_amount'])) . '
				</strong></td>
				</tr></table></div></page>';
		}


        if($paymentMethod == 'cash') {
			$searchParam['transactionId'] = $transactionId;
			$manifestImage = array();
			
			$searchProduct = $this->_transactionTable->searchCashTransactionProducts($searchParam);
			
			foreach ($searchProduct as $key1 => $val1) {
				if($val1['product_type_id'] == 7){
					for($x=$val1['product_quantity'];$x>0;$x--){
						$manifestImage[]= $val1['manifest_info'];
						$manifest_image = $val1['manifest_info'];
						$manifest_folder =  substr($val1['manifest_info'],0,9);
						$manifest_path = str_replace('tif','jpg',strtolower($this->_config['Image_path']['manifestPath'].$manifest_folder.'/'.$manifest_image));
						$pdf2 = $pdf2. '<page><table align="center" style="width: 100%;margin-right:0px;margin-left:0px;margin-top:22px;margin-bottom:0px;" cellpadding="0" cellspacing="0">
							<tr><td><img height="975" width="1450" src="'.$protocol1.$manifest_path.'" /></td></tr>
						</table></page>';
					}
				}

				if($val1['product_type_id'] == "8") {
					$shipImage = array();
					for($y=$val1['product_quantity'];$y>0;$y--){
						$searchParam = array();
						$searchParam['ship_id'] = $val1['item_id'];
						$searchParam['asset_for'] = '1';
						$searchShipImageResult = $this->getServiceLocator()->get('Passenger\Model\DocumentTable')->searchCrmShipImage($searchParam);
						$shipResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getShipDetails($searchParam);
						$searchShipLineParam = array();
						
						if (count($shipResult) > 0) {
							$shipResult = $shipResult[0];
							$shipID = $shipResult['SHIPID'];
							$searchShipLineParam['ship_id'] = $shipID;
							$shipLineResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getShipLineDetails($searchShipLineParam);
						}
						$imageName = $searchShipImageResult[0]['ASSETVALUE'];
						if ($_SERVER['REMOTE_ADDR'] == $this->_config['file_upload_path']['local_internet_ip'] && $searchShipImageResult[0]['is_edited_image'] == 1) {
							$shipImage[$key1]['image_name']= $this->_config['file_upload_path']['local_ship_image_path'] . $imageName;
							$shipImage[$key1]['image_size']= $val1['option_name'];
						}
						else {
							$shipImage[$key1]['image_name']= $this->_config['Image_path']['shipPath'] . $imageName;
							$shipImage[$key1]['image_size']= $val1['option_name'];
						}

						$screwNameArray = $this->screwNameArray();
						$shipResult['NUMSCREWS'] = $screwNameArray[$shipResult['NUMSCREWS']];

						$lineLessTransTypeArray = $this->lineLessTransTypeArray();
						$howNameChangeArray = $this->howNameChangeArray();
						$transTypeArray = $this->transTypeArray();

						$searchShipDetailParamCount = array();
						$searchShipDetailParamCount['ship_name'] = $paramShipNameO;
						$searchShipDetailParamCount['DATE_ARRIVE'] = $arriveDate . ' 00:00:00';

						$passCountResult = $this->getServiceLocator()->get('Passenger\Model\DocumentTable')->getShipPassengerListCount($searchShipDetailParamCount);
						$passTotalCount = 0;
						$passTotalCount = $shipResult['PASSENGERSFIRST'] + $shipResult['PASSENGERSSECOND'] + $shipResult['PASSENGERSTHIRD'];

						$imageName1 = ucfirst(substr(substr(strrchr($this->_config['Image_path']['shipPath'] . $imageName, "/"), 1),0,-4));
							
						$pdf2 = $pdf2 .'<page><table style="width: 100%;" align="center" ><tr><td><img height="790" width="1400" src="' .$protocol1. $this->_config['Image_path']['shipPath'] . $imageName . '" align="middle"/>';

						$pdf2 = $pdf2 . '</td></tr></table>
					<page_footer><table style="width: 100%;" align="center"><tr><td><div style="text-align:center;">
						<span style="font-size:37px; font-family:Times New Roman; font-weight:bold;">' . $val1['item_name'] . '</span>
						<br><br>
						<span><img src="' . $protocol. $_SERVER['HTTP_HOST'] . '/images/pdf_logo3.jpg" style="width:230px" /></span></div></td></tr></table>
						</page_footer></page>';


						$pdf1 = $pdf1 . '<page backtop="-20mm" backbottom="47mm"><page_header></page_header>
							<page_footer><table style="width: 100%;" align="center"><tr><td><div style="text-align:center;">
							<span style="font-size:24px; font-family:Times New Roman; font-weight:bold;">' . $val1['item_name'] . '</SPAN>
							<br><br>
							<span><img src="' . $protocol. $_SERVER['HTTP_HOST'] . '/images/pdf_logo3.jpg" style="width:150px"/></span></div></td></tr></table>
							</page_footer>
					<table style="width: 100%; padding-top:100px;" align="center">
					<tr><td style="font-size:27px; font-family:Times New Roman;"><em>' . $val1['item_name'] . '</em></td></tr><tr><td style="padding-top: -10px;"><hr></td></tr>
					<tr><td style="font-size:24px; font-family:Times New Roman; padding-top:25px"><em>' . $jsLangTranslate['L_ABOUT_SHIP'] . '</em></td></tr>
					<tr><td style="padding-top:22px; font-size:18px; font-family:Times New Roman;">' . $jsLangTranslate['L_BUILT_BY'] . ' ' . $shipResult['BUILDER'] . ', ' . $shipResult['BUILDCITY'] . ', ' . $shipResult['BUILDCOUNTRY'] . ', '
						. $shipResult['BUILDYEAR'] . '. ' . number_format($shipResult['GROSSTONS']) . ' ' . $jsLangTranslate['L_GROSS_TONS'] . '; ' . $shipResult['FEETLONG'] . ' ' .
						$jsLangTranslate['L_FEET_LONG'] . '; ' . $shipResult['BEAM'] . ' ' . $jsLangTranslate['L_FEET_WIDE'] . '. ' . $shipResult['engine_type'] . ' ' .
						$jsLangTranslate['L_ENGINE'] . ', ' . $shipResult['NUMSCREWS'] . ' ' . $jsLangTranslate['L_SCREW'] . '. ' . $jsLangTranslate['L_SERVICE_SPEED'] . ' ' .
						$shipResult['SERVICESPEEDKNOTS'] . ' ' . $jsLangTranslate['L_KNOTS'] . '.';


						$passengerCountStr = '';
						$passengerCountArr = array();
						$passengerFirstN = (!empty($shipResult['PASSENGERSFIRST'])) ? $shipResult['PASSENGERSFIRST'] . ' first class' : '';
						$passengerSecondN = (!empty($shipResult['PASSENGERSSECOND'])) ? $shipResult['PASSENGERSSECOND'] . ' second class' : '';
						$passengerThirdN = (!empty($shipResult['PASSENGERSTHIRD'])) ? $shipResult['PASSENGERSTHIRD'] . ' third class' : '';

						if ($passengerFirstN != "") {
							array_push($passengerCountArr, $passengerFirstN);
						}
						if ($passengerSecondN != "") {
							array_push($passengerCountArr, $passengerSecondN);
						}
						if ($passengerThirdN != "") {
							array_push($passengerCountArr, $passengerThirdN);
						}

						if (count($passengerCountArr) > 0) {
							$passengerCountStr = ' (' . implode(', ', $passengerCountArr) . ').';
						}


						if ($passTotalCount > 0) {
							$pdf1 .= ' ' . number_format($passTotalCount) . ' ' . $jsLangTranslate['L_PASSENGERS'];
							if (!empty($shipResult['PASSENGERSFIRST']) || !empty($shipResult['PASSENGERSSECOND']) || !empty($shipResult['PASSENGERSTHIRD'])) {
								$pdf1 .= $passengerCountStr . ' ';
							}
							$pdf1 .=$shipResult['NOTE'];
						}
						$pdf1 .= '</td></tr>';
						$lastService = "-"; //
						$lastHowLeft = "Built for "; //
						$lastHowLeftType = -1; //

						if (count($shipLineResult) > 0) {
							if (isset($shipLineResult[0])) {
								$pdf1 = $pdf1 . '<tr><td style="font-size:24px; font-family:Times New Roman;  padding-top:40px"><em>Ship History</em></td></tr>';
								$i = 0;
								foreach ($shipLineResult as $key2 => $val2) {
									$pdf1 = $pdf1 . '<tr><td style="padding-top:21px; font-size:18px; font-family:Times New Roman;">';
									if ($lastHowLeftType != 14) {
										if (0 == $val2['LINEID']) {
											if (-1 == $lastHowLeftType)
												$pdf1.= "Built ";
											else
												$pdf1.= "" . $lineLessTransTypeArray[$lastHowLeftType];
										} else {
											$pdf1.= "" . $lastHowLeft;
											$pdf1.= "" . $val2['line_name'] . ",";
										}
										if ($val2['FLAG'] != "") {
											if ($html == "S") {
												$pdf1.= "&nbsp;" . $val2['FLAG'] . " flag,";
											} else {
												$pdf1.= " " . $val2['FLAG'] . " flag,";
											}
										}
										if ($val2['STARTYEAR'] > 0) {
											if ($html == "S") {
												$pdf1.= "&nbsp;in " . $val2['STARTYEAR'];
											} else {
												$pdf1.= " in " . $val2['STARTYEAR'];
											}
										}

										if ($val2['HOWNAMECHANGE'] != 4) {
											$pdf1.= " and " . $howNameChangeArray[$val2['HOWNAMECHANGE']] . " ";
											if ($html == "S") {
												$pdf1.= "<b>" . $val2['SHIPNAME'] . "</b>";
											} else {
												$pdf1.= "" . $val2['SHIPNAME'] . "";
											}
										}
									} else {
										$pdf1.= "Renamed ";
										if ($html == "S") {
											$pdf1.= "<b>" . $val2['SHIPNAME'] . "</b>&nbsp;in " . $val2['STARTYEAR'];
										} else {
											$pdf1.= "" . $val2['SHIPNAME'] . " in " . $val2['STARTYEAR'];
										}
									}
									$pdf1.= ".  ";
									if ($val2['SERVICE'] != "") {
										if ($lastService == $val2['SERVICE'])
											$pdf1.= "Also ";
										$pdf1.= $val2['SERVICE'] . " service.  ";
									}
									if ($val2['NOTE'] != "") {
										$pdf1.= $val2['NOTE'] . ".  ";
									}
									$lastHowLeft = $transTypeArray[$val2['HOWLEFT']] . " ";
									$lastHowLeftType = $val2['HOWLEFT'];
									$lastService = $val2['SERVICE'];
									$pdf1.= "</td></tr>";
								}
							}
						}
						$pdf1 = $pdf1 . '<tr><td style="font-size:18px; font-family:Times New Roman;"><br>' . $shipResult['SCRAPPEDINCOUNTRY'] . ' in ' . $shipResult['SCRAPPEDYEAR'] . '.</td></tr></table>';
						$pdf1 = $pdf1 . '</page>';

					}
				}
			}
			
		}else{
			if(!empty($params['transaction_id'])) {
				$transParam = array();
				$manifestImage = array();
				$shipImage = array();
				$transParam['transaction_id'] = $this->decrypt($params['transaction_id']);
				$transactionResult = $this->_transactionTable->getOrderFileName($transParam);		
				$passengerID = '';
				foreach($transactionResult as $key3=>$value) {					
					if(!empty($value['item_id']) && strlen($value['item_id'])>=12 && $passengerID=='') {
						$passengerID = $value['item_id'];
					}
					if($value['product_type_id'] == "7") {
						for($z=$value['num_quantity'];$z>0;$z--){
							$manifestImage[]= $value['manifest_info'];
							$manifest_image = $value['manifest_info'];
							$manifest_folder =  substr($value['manifest_info'],0,9);
							$manifest_path = str_replace('tif','jpg',strtolower($this->_config['Image_path']['manifestPath'].$manifest_folder.'/'.$manifest_image));
							$pdf2 = $pdf2. '<page><table align="center" style="width: 100%;margin-right:0px;margin-left:0px;margin-top:22px;margin-bottom:0px;" cellpadding="0" cellspacing="0">
								<tr><td><img height="975" width="1450" src="'.$protocol1.$manifest_path.'" /></td></tr>
							</table></page>';
						}
					}

					if($value['product_type_id'] == "8") {
						for($t=$value['num_quantity'];$t>0;$t--){
							$shipSearch['product_id'] = $value['transaction_product_id'];
							$searchParam['ship_id'] = $value['item_id'];
							$searchParam['asset_for'] = '1';
							$searchShipImageResult = $this->getServiceLocator()->get('Passenger\Model\DocumentTable')->searchCrmShipImage($searchParam);
							$shipResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getShipDetails($searchParam);
							$searchShipLineParam = array();

							if (count($shipResult) > 0) {
								$shipResult = $shipResult[0];
								$shipID = $shipResult['SHIPID'];
								$searchShipLineParam['ship_id'] = $shipID;
								$shipLineResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getShipLineDetails($searchShipLineParam);
							}
							$imageName = $searchShipImageResult[0]['ASSETVALUE'];
							if ($_SERVER['REMOTE_ADDR'] == $this->_config['file_upload_path']['local_internet_ip'] && $searchShipImageResult[0]['is_edited_image'] == 1) {
								$shipImage[$key3]['image_name']= $this->_config['file_upload_path']['local_ship_image_path'] . $imageName;
								$shipImage[$key3]['image_size']= $value['product_attribute_option_id'];
							}else {
								$shipImage[$key3]['image_name']= $this->_config['Image_path']['shipPath'] . $imageName;
								$shipImage[$key3]['image_size']= $value['product_attribute_option_id'];
							}
							$screwNameArray = $this->screwNameArray();
							$shipResult['NUMSCREWS'] = $screwNameArray[$shipResult['NUMSCREWS']];

							$lineLessTransTypeArray = $this->lineLessTransTypeArray();					
							$howNameChangeArray = $this->howNameChangeArray();
							$transTypeArray = $this->transTypeArray();

							$searchShipDetailParamCount = array();
							$searchShipDetailParamCount['ship_name'] = $paramShipNameO;
							$searchShipDetailParamCount['DATE_ARRIVE'] = $arriveDate . ' 00:00:00';

							$passCountResult = $this->getServiceLocator()->get('Passenger\Model\DocumentTable')->getShipPassengerListCount($searchShipDetailParamCount);
							$passTotalCount = 0;
							$passTotalCount = $shipResult['PASSENGERSFIRST'] + $shipResult['PASSENGERSSECOND'] + $shipResult['PASSENGERSTHIRD'];

							$imageName1 = ucfirst(substr(substr(strrchr($this->_config['Image_path']['shipPath'] . $imageName, "/"), 1),0,-4));

							$pdf2 = $pdf2 .'<page><table style="width: 100%;" align="center" ><tr><td><img height="790" width="1400" src="' .$protocol1. $this->_config['Image_path']['shipPath'] . $imageName . '" align="middle"/>';
							$pdf2 = $pdf2 . '</td></tr></table>
					<page_footer><table style="width: 100%;" align="center"><tr><td><div style="text-align:center;">
						<span style="font-size:37px; font-family:Times New Roman; font-weight:bold;">' . $value['item_name'] . '</span>
						<br><br>
						<span><img src="' . $protocol. $_SERVER['HTTP_HOST'] . '/images/pdf_logo3.jpg" style="width:230px" /></span></div></td></tr></table>
						</page_footer></page>';


							$pdf1 = $pdf1 . '<page backtop="-20mm" backbottom="47mm"><page_header></page_header>
							<page_footer><table style="width: 100%;" align="center"><tr><td><div style="text-align:center;">
							<span style="font-size:24px; font-family:Times New Roman; font-weight:bold;">' . $value['item_name'] . '</SPAN>
							<br><br>
							<span><img src="' . $protocol. $_SERVER['HTTP_HOST'] . '/images/pdf_logo3.jpg" style="width:150px"/></span></div></td></tr></table>
							</page_footer>
					<table style="width: 100%; padding-top:100px;" align="center">
					<tr><td style="font-size:27px; font-family:Times New Roman;"><em>' . $value['item_name'] . '</em></td></tr><tr><td style="padding-top: -10px;"><hr></td></tr>
					<tr><td style="font-size:22px; font-family:Times New Roman; padding-top:25px"><em>' . $jsLangTranslate['L_ABOUT_SHIP'] . '</em></td></tr>
					<tr><td style="padding-top:22px; font-size:18px; font-family:Times New Roman;">' . $jsLangTranslate['L_BUILT_BY'] . ' ' . $shipResult['BUILDER'] . ', ' . $shipResult['BUILDCITY'] . ', ' . $shipResult['BUILDCOUNTRY'] . ', '
						. $shipResult['BUILDYEAR'] . '. ' . number_format($shipResult['GROSSTONS']) . ' ' . $jsLangTranslate['L_GROSS_TONS'] . '; ' . $shipResult['FEETLONG'] . ' ' .
						$jsLangTranslate['L_FEET_LONG'] . '; ' . $shipResult['BEAM'] . ' ' . $jsLangTranslate['L_FEET_WIDE'] . '. ' . $shipResult['engine_type'] . ' ' .
						$jsLangTranslate['L_ENGINE'] . ', ' . $shipResult['NUMSCREWS'] . ' ' . $jsLangTranslate['L_SCREW'] . '. ' . $jsLangTranslate['L_SERVICE_SPEED'] . ' ' .
						$shipResult['SERVICESPEEDKNOTS'] . ' ' . $jsLangTranslate['L_KNOTS'] . '.';


							$passengerCountStr = '';
							$passengerCountArr = array();
							$passengerFirstN = (!empty($shipResult['PASSENGERSFIRST'])) ? $shipResult['PASSENGERSFIRST'] . ' first class' : '';
							$passengerSecondN = (!empty($shipResult['PASSENGERSSECOND'])) ? $shipResult['PASSENGERSSECOND'] . ' second class' : '';
							$passengerThirdN = (!empty($shipResult['PASSENGERSTHIRD'])) ? $shipResult['PASSENGERSTHIRD'] . ' third class' : '';

							if ($passengerFirstN != "") {
								array_push($passengerCountArr, $passengerFirstN);
							}
							if ($passengerSecondN != "") {
								array_push($passengerCountArr, $passengerSecondN);
							}
							if ($passengerThirdN != "") {
								array_push($passengerCountArr, $passengerThirdN);
							}

							if (count($passengerCountArr) > 0) {
								$passengerCountStr = ' (' . implode(', ', $passengerCountArr) . ').';
							}


							if ($passTotalCount > 0) {
								$pdf1 .= ' ' . number_format($passTotalCount) . ' ' . $jsLangTranslate['L_PASSENGERS'];
								if (!empty($shipResult['PASSENGERSFIRST']) || !empty($shipResult['PASSENGERSSECOND']) || !empty($shipResult['PASSENGERSTHIRD'])) {
									$pdf1 .= $passengerCountStr . ' ';
								}
								$pdf1 .=$shipResult['NOTE'];
							}
							$pdf1 .= '</td></tr>';
							$lastService = "-"; //
							$lastHowLeft = "Built for "; //
							$lastHowLeftType = -1; //

							if (count($shipLineResult) > 0) {
								if (isset($shipLineResult[0])) {
									$pdf1 = $pdf1 . '<tr><td style="font-size:24px; font-family:Times New Roman;  padding-top:40px"><em>Ship History</em></td></tr>';
									$i = 0;
									foreach ($shipLineResult as $key2 => $value2) {
										$pdf1 = $pdf1 . '<tr><td style="padding-top:21px; font-size:18px; font-family:Times New Roman;">';
										if ($lastHowLeftType != 14) {
											if (0 == $value2['LINEID']) {
												if (-1 == $lastHowLeftType)
													$pdf1.= "Built ";
												else
													$pdf1.= "" . $lineLessTransTypeArray[$lastHowLeftType];
											} else {
												$pdf1.= "" . $lastHowLeft;
												$pdf1.= "" . $value2['line_name'] . ",";
											}
											if ($value2['FLAG'] != "") {
												if ($html == "S") {
													$pdf1.= "&nbsp;" . $value2['FLAG'] . " flag,";
												} else {
													$pdf1.= " " . $value2['FLAG'] . " flag,";
												}
											}
											if ($value2['STARTYEAR'] > 0) {
												if ($html == "S") {
													$pdf1.= "&nbsp;in " . $value2['STARTYEAR'];
												} else {
													$pdf1.= " in " . $value2['STARTYEAR'];
												}
											}

											if ($value2['HOWNAMECHANGE'] != 4) {
												$pdf1.= " and " . $howNameChangeArray[$value2['HOWNAMECHANGE']] . " ";
												if ($html == "S") {
													$pdf1.= "<b>" . $value2['SHIPNAME'] . "</b>";
												} else {
													$pdf1.= "" . $value2['SHIPNAME'] . "";
												}
											}
										} else {
											$pdf1.= "Renamed ";
											if ($html == "S") {
												$pdf1.= "<b>" . $value2['SHIPNAME'] . "</b>&nbsp;in " . $value2['STARTYEAR'];
											} else {
												$pdf1.= "" . $value2['SHIPNAME'] . " in " . $value2['STARTYEAR'];
											}
										}
										$pdf1.= ".  ";
										if ($value2['SERVICE'] != "") {
											if ($lastService == $value2['SERVICE'])
												$pdf1.= "Also ";
											$pdf1.= $value2['SERVICE'] . " service.  ";
										}
										if ($value2['NOTE'] != "") {
											$pdf1.= $value2['NOTE'] . ".  ";
										}
										$lastHowLeft = $transTypeArray[$value2['HOWLEFT']] . " ";
										$lastHowLeftType = $value2['HOWLEFT'];
										$lastService = $value2['SERVICE'];
										$pdf1.= "</td></tr>";
									}
								}
							}
							$pdf1 = $pdf1 . '<tr><td style="font-size:18px; font-family:Times New Roman;"><br>' . $shipResult['SCRAPPEDINCOUNTRY'] . ' in ' . $shipResult['SCRAPPEDYEAR'] . '.</td></tr></table>';
							$pdf1 = $pdf1 . '</page>';
						}
					}
				}
			}
		}
		
		try {
            $html2pdf = new\ HTML2PDF('L', array(8.5*25.4,11*25.4), 'en');
            $html2pdf->setDefaultFont('Times');
            $html2pdf->writeHTML($pdf1,false);
			$html2pdf->Output($this->_config['file_upload_path']['assets_upload_dir'].'print-order/printReciept_and_ShipHistory_'.time().'.pdf','F');
			if($pdf2!=''){
				$html2pdf = new\ HTML2PDF('L', array(11*25.4,17*25.4), 'en');
				$html2pdf->setDefaultFont('Times');
				$html2pdf->writeHTML($pdf2,false);
				$html2pdf->Output($this->_config['file_upload_path']['assets_upload_dir'].'print-order/shipImage_and_shipManifest_'.time().'.pdf','F');
			}
        } catch (HTML2PDF_exception $e) {
            echo $e;
            exit;
        }
    
    }


     public function addTaxToProducts($cartProducts, $postArr = array()) {
            if (!empty($cartProducts) && !empty($postArr)) {
                    $postData = $postArr['postData'];
                    $totalShipping = 0;
                    $j = 0;
                    $z = 1;

                    $productCnt = count($cartProducts);

                    $totalProductDiscount = 0;
                    $totalMembershipDiscount = 0;
                    $totalTaxAmount = 0;
                    $subTotal = 0;
                    $subtotalAmount = 0;
                    $totalAfterShipping = 0;
                    //

                    $orderTotal = 0; // final amount

                    foreach ($cartProducts as $index => $value) {

                            $totalProductDiscount+=$value['product_discount'];
                            $totalMembershipDiscount+=$value['membership_discount'];
                            $totalShippingDiscount+=$value['shipping_discount_amount'];

                            $subtotalAmount = ($value['product_amount'] * $value['product_qty']) - $value['membership_discount'];

                            //.... tax ...//

                            $product_amount_for_tax = $subtotalAmount - $value['product_discount'];
                            $productTotalForRevenue = $subtotalAmount - $value['product_discount'];
                            if ($product_amount_for_tax < 0)
                                    $product_amount_for_tax = 0;
                            $totalAfterShipping+= $this->Roundamount($product_amount_for_tax);


                            if (isset($cartProducts[$index]['is_taxable']) && $cartProducts[$index]['is_taxable'] == 1) {

                                    $product_tax = $product_amount_for_tax * $postArr['combined_sales_tax'];
                                   $cartProducts[$index]['product_taxable_amount'] = $this->Roundamount($product_tax);
                            } else {
                                    $cartProducts[$index]['product_taxable_amount'] = '0.00';
                            }
                            $totalTaxAmount+=$cartProducts[$index]['product_taxable_amount'];

                            //.... end of tax //
                            $cartProducts[$index]['product_total'] = $this->Roundamount($product_amount_for_tax) + $cartProducts[$index]['product_taxable_amount'];
                            $cartProducts[$index]['total_product_revenue'] = $this->Roundamount($productTotalForRevenue);

                            $orderTotal+= $cartProducts[$index]['product_total'];

                            $subTotal+= ($value['product_amount'] * $value['product_qty']) - $value['membership_discount'];

                            $cartProducts[$index]['product_discount'] = $this->Roundamount($cartProducts[$index]['product_discount']);
                            $cartProducts[$index]['shipping_price'] = '0.00';
                            $cartProducts[$index]['surcharge_price'] = '0.00';
                            $cartProducts[$index]['shipping_handling_price'] = '0.00';
                            //$cartProducts[$index]['product_taxable_amount'] = '0.00';
                            $cartProducts[$index]['product_total'] = $this->Roundamount($cartProducts[$index]['product_total']);
                            $j++;
                            $z++;
                    }
                    $cartProductsTotalArray['totalTaxAmount'] = $this->Roundamount($totalTaxAmount);
                    $cartProductsTotalArray['subTotalAmount'] = $this->Roundamount($subTotal);
                    $cartProductsTotalArray['totalaftershipping'] = $this->Roundamount($totalAfterShipping);
                    $cartProductsTotalArray['shipping_charge'] = '0.00';
                    $cartProductsTotalArray['total_discount'] = $this->Roundamount($totalProductDiscount);
                    $cartProductsTotalArray['total_membership_discount'] = $this->Roundamount($totalMembershipDiscount);
                    $cartProductsTotalArray['order_total'] = $this->Roundamount($orderTotal);
                    $cartProductsTotalArray['shipping_discount_in_amount'] = '0.00';
                    $retArray = array();
                    $retArray['cartdata'] = $cartProducts;
                    $retArray['cartdataTotal'] = $cartProductsTotalArray;
                    return $retArray;
            }
    }

}
