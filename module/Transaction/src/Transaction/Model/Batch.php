<?php

/**
 * This model is used for user transaction
 * @package    Transaction
 * @author     Icreon Tech - DG
 */

namespace Transaction\Model;

use Transaction\Module;
use Base\Model\BaseModel;
use Zend\Db\Adapter\Adapter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 * This model is used for user transaction
 * @package    Transaction
 * @author     Icreon Tech - DG
 */
class Batch extends BaseModel {

    protected $inputFilter;
    protected $adapter;

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    /**
     * This method is used to add filtering and validation to the form elements of create 
     * @param void
     * @return Array
     * @author Icreon Tech - DG
     */
    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }


    /**
     * Function used to check variables 
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function exchangeArray($data) {
        
    }

    
}