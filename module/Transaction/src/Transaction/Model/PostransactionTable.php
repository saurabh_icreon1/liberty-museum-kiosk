<?php

/**
 * This model is used for user. Transaction
 * @package    Transaction
 * @author     Icreon Tech - DG
 */

namespace Transaction\Model;

use Authorize\lib\AuthnetXML;
use Zend\Db\TableGateway\TableGateway;

/**
 * This model is used for user. Transaction
 * @package    Transaction
 * @author     Icreon Tech - DG
 */
class PostransactionTable {

    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
    }

    /**
     * Function for to getUserGiftToSoleif
     * @author Icreon Tech - DT
     * @return Arr
     * @param Array
     */
    public function getUserGiftToSoleif($dataArr = null) {
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        if (isset($dataArr['transaction_type'])) {
            $transaction_type = implode(",", $dataArr['transaction_type']);
        } else {
            $transaction_type = null;
        }
        $transaction_id = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $start_index = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $record_limit = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $sort_field = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : 'usr_transaction.transaction_date';
        $sort_order = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : 'desc';

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_tra_getUserGiftToSoleif(?,?,?,?,?,?,?)");
        $stmt->getResource()->bindParam(1, $user_id);
        $stmt->getResource()->bindParam(2, $transaction_type);
        $stmt->getResource()->bindParam(3, $start_index);
        $stmt->getResource()->bindParam(4, $record_limit);
        $stmt->getResource()->bindParam(5, $sort_field);
        $stmt->getResource()->bindParam(6, $sort_order);
        $stmt->getResource()->bindParam(7, $transaction_id);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get front end transaction list
     * @author Icreon Tech - DT
     * @return Arr
     * @param Array
     */
    public function getUserTransactions($dataArr = null) {
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        if (isset($dataArr['transaction_type'])) {
            $transaction_type = implode(",", $dataArr['transaction_type']);
        } else {
            $transaction_type = null;
        }

        $transaction_id = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $start_index = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $record_limit = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $sort_field = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : 'usr_transaction.transaction_date';
        $sort_order = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : 'desc';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_tra_getUserTransactions(?,?,?,?,?,?,?)");
        $stmt->getResource()->bindParam(1, $user_id);
        $stmt->getResource()->bindParam(2, $transaction_type);
        $stmt->getResource()->bindParam(3, $start_index);
        $stmt->getResource()->bindParam(4, $record_limit);
        $stmt->getResource()->bindParam(5, $sort_field);
        $stmt->getResource()->bindParam(6, $sort_order);
        $stmt->getResource()->bindParam(7, $transaction_id);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for bind field for call to stored procedure
     * @author Icreon Tech - DG
     * @return Void
     * @param object stm,array fields_value_arr
     */
    public function bindFieldArray($stmt, $fields_value_arr) {
        $i = 1;
        foreach ($fields_value_arr as $field_value) {
            $stmt->getResource()->bindParam($i, $field_value);
            $i++;
        }
    }

    /**
     * Function for append number of questions marks for stored procedure
     * @author Icreon Tech - DG
     * @return String
     * @param Array
     */
    public function appendQuestionMars($count) {
        $returnstr = '';
        for ($i = 1; $i <= $count; $i++) {
            $returnstr.='?,';
        }
        return rtrim($returnstr, ',');
    }

    /**
     * Function for to get transaction receipt
     * @author Icreon Tech - AS
     * @return Array
     * @param Array
     */
    public function getTransactionReceipt($dataArr) {
        $receipt_template_id = (isset($dataArr['receipt_template_id']) && $dataArr['receipt_template_id'] != '') ? $dataArr['receipt_template_id'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_tra_getTransactionReceipt(?)");
        $stmt->getResource()->bindParam(1, $receipt_template_id);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0];
        } else {
            return false;
        }
    }

    /**
     * Function for to get user transaction count
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function getUserTransactionCount($dataArr) {
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_tra_getUserTransactionCount(?)");
        $stmt->getResource()->bindParam(1, $user_id);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet[0]['total'];
    }

    /**
     * Function for get all contacts in create transaction
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function getAllContactSearch($transactionData) {

        $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_tra_getContactsTransaction(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $transactionData[0]);
        $stmt->getResource()->bindParam(2, (isset($transactionData[1]) and trim($transactionData[1]) != "") ? addslashes($transactionData[1]) : "");
        $stmt->getResource()->bindParam(3, $transactionData[2]);
        $stmt->getResource()->bindParam(4, $transactionData[3]);
        $stmt->getResource()->bindParam(5, $transactionData[4]);
        $stmt->getResource()->bindParam(6, $transactionData[5]);
        $stmt->getResource()->bindParam(7, $transactionData[6]);
        $stmt->getResource()->bindParam(8, $transactionData[7]);
        $stmt->getResource()->bindParam(9, $transactionData[8]);
        $stmt->getResource()->bindParam(10, $transactionData[9]);
        $stmt->getResource()->bindParam(11, $transactionData[10]);
        $stmt->getResource()->bindParam(12, $transactionData[11]);
        $stmt->getResource()->bindParam(13, $transactionData[12]);
        $stmt->getResource()->bindParam(14, $transactionData[13]);

        $stmt->getResource()->bindParam(15, $transactionData[14]);
        $stmt->getResource()->bindParam(16, $transactionData[15]);
        $stmt->getResource()->bindParam(17, $transactionData[16]);
        $stmt->getResource()->bindParam(18, $transactionData[17]);

        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get all transactions
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function getAllTransactions($transactionData) {
        $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_tra_getCrmTransactions(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, (isset($transactionData[0]) and trim($transactionData[0]) != "") ? addslashes($transactionData[0]) : "");
        $stmt->getResource()->bindParam(2, $transactionData[1]);
        $stmt->getResource()->bindParam(3, $transactionData[2]);
        $stmt->getResource()->bindParam(4, $transactionData[3]);
        $stmt->getResource()->bindParam(5, $transactionData[4]);
        $stmt->getResource()->bindParam(6, $transactionData[5]);
        $stmt->getResource()->bindParam(7, $transactionData[6]);
        $stmt->getResource()->bindParam(8, $transactionData[7]);
        $stmt->getResource()->bindParam(9, $transactionData[8]);
        $stmt->getResource()->bindParam(10, $transactionData[9]);
        $stmt->getResource()->bindParam(11, $transactionData[10]);
        $stmt->getResource()->bindParam(12, $transactionData[11]);
        $stmt->getResource()->bindParam(13, $transactionData[12]);
        $stmt->getResource()->bindParam(14, $transactionData[13]);
        $stmt->getResource()->bindParam(15, $transactionData[14]);
        $stmt->getResource()->bindParam(16, $transactionData[15]);
        $stmt->getResource()->bindParam(17, $transactionData[16]);
        $stmt->getResource()->bindParam(18, $transactionData[17]);
        $stmt->getResource()->bindParam(19, $transactionData[18]);
        $stmt->getResource()->bindParam(20, $transactionData[19]);
        $stmt->getResource()->bindParam(21, $transactionData[20]);
        $stmt->getResource()->bindParam(22, $transactionData[21]);
        $stmt->getResource()->bindParam(23, $transactionData[22]);
        $stmt->getResource()->bindParam(24, $transactionData[23]);
        $stmt->getResource()->bindParam(25, $transactionData[24]);
        $stmt->getResource()->bindParam(26, $transactionData[25]);
        $stmt->getResource()->bindParam(27, $transactionData[26]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * This function is used to get the saved search listing for transaction search
     * @param this will be an array.
     * @return this will return a title of the saved search
     * @author Icreon Tech -DT
     */
    public function getTransactionSavedSearch($transactionData = array()) {
        $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_tra_getTransactionSavedSearch(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $transactionData[0]);
        $stmt->getResource()->bindParam(2, $transactionData[1]);
        $stmt->getResource()->bindParam(3, $transactionData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
    }

    /**
     * This function will save the search title into the table for transaction search
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech -DT
     */
    public function saveTransactionSearch($transactionData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_insertCrmSearchTransaction(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update the search title into the table for transaction search
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech -DT
     */
    public function updateTransactionSearch($transactionData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_updateCrmSearchTransaction(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $stmt->getResource()->bindParam(7, $transactionData[6]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will delete the saved activity search titles
     * @param this will be an array having the params.
     * @return this will return a confirmation
     * @author Icreon Tech -DT
     */
    public function deleteSavedSearch($transactionData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_deleteCrmSearchTransaction(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for remove transaction
     * @author Icreon Tech - DT
     * @return Void
     * @param Array
     */
    public function removeTransactionById($transactionData) {
        $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_tra_deleteTransaction(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $transactionData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * This function will add donation product to cart
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function addToCartDonation($transactionData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_insertDonationAddToCart(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $stmt->getResource()->bindParam(7, $transactionData[6]);
            $stmt->getResource()->bindParam(8, $transactionData[7]);
            $stmt->getResource()->bindParam(9, $transactionData[8]);
            $stmt->getResource()->bindParam(10, $transactionData[9]);
            $stmt->getResource()->bindParam(11, $transactionData[10]);
            $stmt->getResource()->bindParam(12, $transactionData[11]);
            $stmt->getResource()->bindParam(13, $transactionData[12]);
            $stmt->getResource()->bindParam(14, $transactionData[13]);
            $stmt->getResource()->bindParam(15, $transactionData[14]);
            $stmt->getResource()->bindParam(16, $transactionData[15]);
            $stmt->getResource()->bindParam(17, $transactionData[16]);
            $stmt->getResource()->bindParam(18, $transactionData[17]);
            $stmt->getResource()->bindParam(19, $transactionData[18]);
            $stmt->getResource()->bindParam(20, $transactionData[19]);
            $stmt->getResource()->bindParam(21, $transactionData[20]);
            $stmt->getResource()->bindParam(22, $transactionData[21]);
			$stmt->getResource()->bindParam(23, $transactionData[22]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will add product to cart
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function addToCartProduct($transactionData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_insertProductAddToCart(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $stmt->getResource()->bindParam(7, $transactionData[6]);
            $stmt->getResource()->bindParam(8, $transactionData[7]);
            $stmt->getResource()->bindParam(9, $transactionData[8]);
            $stmt->getResource()->bindParam(10, $transactionData[9]);
            $stmt->getResource()->bindParam(11, $transactionData[10]);
            $stmt->getResource()->bindParam(12, $transactionData[11]);
            $stmt->getResource()->bindParam(13, $transactionData[12]);
            $stmt->getResource()->bindParam(14, $transactionData[13]);
            $stmt->getResource()->bindParam(15, $transactionData[14]);
            $stmt->getResource()->bindParam(16, $transactionData[15]);
            $stmt->getResource()->bindParam(17, $transactionData[16]);

            if (isset($transactionData[17]) && !empty($transactionData[17]))
                $param17 = $transactionData[17];
            else
                $param17 = '';

            $stmt->getResource()->bindParam(18, $param17);

            if (isset($transactionData[18]) && !empty($transactionData[18]))
                $param18 = $transactionData[18];
            else
                $param18 = '';
            $stmt->getResource()->bindParam(19, $param18);

            if (isset($transactionData[19]) && !empty($transactionData[19]))
                $param19 = $transactionData[19];
            else
                $param19 = '';
            $stmt->getResource()->bindParam(20, $param19);

            if (isset($transactionData[20]) && !empty($transactionData[20]))
                $param20 = $transactionData[20];
            else
                $param20 = '';
            $stmt->getResource()->bindParam(21, $param20);

            if (isset($transactionData[21]) && !empty($transactionData[21]))
                $param21 = $transactionData[21];
            else
                $param21 = '';
            $stmt->getResource()->bindParam(22, $param21);

            if (isset($transactionData[22]) && !empty($transactionData[22]))
                $param22 = $transactionData[22];
            else
                $param22 = '';
            $stmt->getResource()->bindParam(23, $param22);

            if (isset($transactionData[23]) && !empty($transactionData[23]))
                $param23 = $transactionData[23];
            else
                $param23 = '';
            $stmt->getResource()->bindParam(24, $param23);

			if (isset($transactionData[24]) && !empty($transactionData[24]))
                $param24 = $transactionData[24];
            else
                $param24 = '';
            $stmt->getResource()->bindParam(25, $param24);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will add  to cart donation notify
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function addToCartDonationNotify($transactionData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_insertDonationNotifyAddToCart(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $stmt->getResource()->bindParam(7, $transactionData[6]);
            $stmt->getResource()->bindParam(8, $transactionData[7]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will add fof to cart
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function addToCartFOF($transactionData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_insertFOFAddToCart(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $stmt->getResource()->bindParam(7, $transactionData[6]);
            $stmt->getResource()->bindParam(8, $transactionData[7]);
            $stmt->getResource()->bindParam(9, $transactionData[8]);
            $stmt->getResource()->bindParam(10, $transactionData[9]);
            $stmt->getResource()->bindParam(11, $transactionData[10]);
            $stmt->getResource()->bindParam(12, $transactionData[11]);
            $stmt->getResource()->bindParam(13, $transactionData[12]);
            $stmt->getResource()->bindParam(14, $transactionData[13]);
            $stmt->getResource()->bindParam(15, $transactionData[14]);
            $stmt->getResource()->bindParam(16, $transactionData[15]);
            $stmt->getResource()->bindParam(17, $transactionData[16]);
            $stmt->getResource()->bindParam(18, $transactionData[17]);
            $stmt->getResource()->bindParam(19, $transactionData[18]);
            $stmt->getResource()->bindParam(20, $transactionData[19]);
            $stmt->getResource()->bindParam(21, $transactionData[20]);
            $stmt->getResource()->bindParam(22, $transactionData[21]);
            $stmt->getResource()->bindParam(23, $transactionData[22]);
            $stmt->getResource()->bindParam(24, $transactionData[23]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will add manifest to cart
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function addToCartManifest($transactionData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_insertAddtionalManifestAddToCart(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $stmt->getResource()->bindParam(7, $transactionData[6]);
            $stmt->getResource()->bindParam(8, $transactionData[7]);
            $stmt->getResource()->bindParam(9, $transactionData[8]);
            $stmt->getResource()->bindParam(10, $transactionData[9]);
            $stmt->getResource()->bindParam(11, $transactionData[10]);
            $stmt->getResource()->bindParam(12, $transactionData[11]);
            $stmt->getResource()->bindParam(13, $transactionData[12]);
            $stmt->getResource()->bindParam(14, $transactionData[13]);
            $stmt->getResource()->bindParam(15, $transactionData[14]);
            $stmt->getResource()->bindParam(16, $transactionData[15]);
            $stmt->getResource()->bindParam(17, $transactionData[16]);
            $stmt->getResource()->bindParam(18, $transactionData[17]);
			$stmt->getResource()->bindParam(19, $transactionData[18]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for get all transactions
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function getAllUserWallOfHonors($transactionData) {
        $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_tra_getUserWallOfHonor(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $transactionData[0]);
        $stmt->getResource()->bindParam(2, $transactionData[1]);
        $stmt->getResource()->bindParam(3, $transactionData[2]);
        $stmt->getResource()->bindParam(4, $transactionData[3]);
        $stmt->getResource()->bindParam(5, $transactionData[4]);
        $stmt->getResource()->bindParam(6, $transactionData[5]);
        $stmt->getResource()->bindParam(7, $transactionData[6]);
        $stmt->getResource()->bindParam(8, $transactionData[7]);
        $stmt->getResource()->bindParam(9, $transactionData[8]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * This function will add fof to cart
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function searchTransactionDetails($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_viewCrmTransaction(?)');
            $stmt->getResource()->bindParam(1, $params['transactionId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will add woh to cart
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function addToCartWallOfHonorNaming($transactionData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_insertWOHAddToCart(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $stmt->getResource()->bindParam(7, $transactionData[6]);
            $stmt->getResource()->bindParam(8, $transactionData[7]);
            $stmt->getResource()->bindParam(9, $transactionData[8]);
            $stmt->getResource()->bindParam(10, $transactionData[9]);
            $stmt->getResource()->bindParam(11, $transactionData[10]);
            $stmt->getResource()->bindParam(12, $transactionData[11]);
            $stmt->getResource()->bindParam(13, $transactionData[12]);
            $stmt->getResource()->bindParam(14, $transactionData[13]);
            $stmt->getResource()->bindParam(15, $transactionData[14]);
            $stmt->getResource()->bindParam(16, $transactionData[15]);
            $stmt->getResource()->bindParam(17, $transactionData[16]);
            $stmt->getResource()->bindParam(18, $transactionData[17]);
            $stmt->getResource()->bindParam(19, $transactionData[18]);
            $stmt->getResource()->bindParam(20, $transactionData[19]);
            $stmt->getResource()->bindParam(21, $transactionData[20]);
            $stmt->getResource()->bindParam(22, $transactionData[21]);
            $stmt->getResource()->bindParam(23, $transactionData[22]);
            $stmt->getResource()->bindParam(24, $transactionData[23]);
            $stmt->getResource()->bindParam(25, $transactionData[24]);
            $stmt->getResource()->bindParam(26, $transactionData[25]);
            $stmt->getResource()->bindParam(27, $transactionData[26]);
            $stmt->getResource()->bindParam(28, $transactionData[27]);
            $stmt->getResource()->bindParam(29, $transactionData[28]);
            $stmt->getResource()->bindParam(30, $transactionData[29]);
            $stmt->getResource()->bindParam(31, $transactionData[30]);
            $stmt->getResource()->bindParam(32, $transactionData[31]);
            $stmt->getResource()->bindParam(33, $transactionData[32]);
            $stmt->getResource()->bindParam(34, $transactionData[33]);
            $stmt->getResource()->bindParam(35, $transactionData[34]);
            //Ticket-Id :130
            $stmt->getResource()->bindParam(36, $transactionData[35]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will add woh additional contact to cart
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function addToCartWallOfHonorAdditionalContact($transactionData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_insertWOHContactAddToCart(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $stmt->getResource()->bindParam(7, $transactionData[6]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will add woh to cart
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function addToCartBioCertificate($transactionData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_insertBioCertificateAddToCart(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $stmt->getResource()->bindParam(7, $transactionData[6]);
            $stmt->getResource()->bindParam(8, $transactionData[7]);
            $stmt->getResource()->bindParam(9, $transactionData[8]);
            $stmt->getResource()->bindParam(10, $transactionData[9]);
            $stmt->getResource()->bindParam(11, $transactionData[10]);
            $stmt->getResource()->bindParam(12, $transactionData[11]);
            $stmt->getResource()->bindParam(13, $transactionData[12]);
            $stmt->getResource()->bindParam(14, $transactionData[13]);
            $stmt->getResource()->bindParam(15, $transactionData[14]);
            $stmt->getResource()->bindParam(16, $transactionData[15]);
            $stmt->getResource()->bindParam(17, $transactionData[16]);
            $stmt->getResource()->bindParam(18, $transactionData[17]);
            $stmt->getResource()->bindParam(19, $transactionData[18]);
            $stmt->getResource()->bindParam(20, $transactionData[19]);
            $stmt->getResource()->bindParam(21, $transactionData[20]);
            $stmt->getResource()->bindParam(22, $transactionData[21]);
            $stmt->getResource()->bindParam(23, $transactionData[22]);
            $stmt->getResource()->bindParam(24, $transactionData[23]);
            $stmt->getResource()->bindParam(25, $transactionData[24]);
            $stmt->getResource()->bindParam(26, $transactionData[25]);
            $stmt->getResource()->bindParam(27, $transactionData[26]);
            $stmt->getResource()->bindParam(28, $transactionData[27]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will add transaction note
     * @param array
     * @return return last id data
     * @author Icreon Tech - AS
     */
    public function addTransactionNotes($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_addTransactionNote(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['transaction_id']);
            $stmt->getResource()->bindParam(2, $params['notify_note']);
            $stmt->getResource()->bindParam(3, $params['note']);
            $stmt->getResource()->bindParam(4, $params['added_by']);
            $stmt->getResource()->bindParam(5, $params['added_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will get transaction product details
     * @param array
     * @return return last id data
     * @author Icreon Tech - AS
     */
    public function searchTransactionProducts($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getTransactionProducts(?)');
            $stmt->getResource()->bindParam(1, $params['transactionId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get Cart LIst
     * @author Icreon Tech - DT
     * @return Arr
     * @param Array
     */
    public function getCartList($dataArr = null) {
        $user_id = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '-1';
        $session_id = (isset($dataArr['session_id']) && $dataArr['session_id'] != '') ? $dataArr['session_id'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_cart_getCartProducts(?,?)");
        $stmt->getResource()->bindParam(1, $user_id);
        $stmt->getResource()->bindParam(2, $session_id);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get woh letter
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function viewWohLetter($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getUserWoh(?)');
            $stmt->getResource()->bindParam(1, $params['woh_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get woh letter
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function getShipImage($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getUserProduct(?)');
            $stmt->getResource()->bindParam(1, $params['product_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get Cart LIst
     * @author Icreon Tech - DT
     * @return Arr
     * @param Array
     */
    public function removeFromCart($dataArr = null) {

        $cart_id = $dataArr['cart_id'];
        $productType = $dataArr['product_type'];
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_removeCartFront(?,?)");
        $stmt->getResource()->bindParam(1, $cart_id);
        $stmt->getResource()->bindParam(2, $productType);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
        return true;
    }

    /**
     * Function for to get Cart LIst
     * @author Icreon Tech - DT
     * @return Arr
     * @param Array
     */
    public function getInventoryProductAlreadyAdded($dataArr = null) {
        $user_id = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '-1';
        $productId = (isset($dataArr['productId']) && $dataArr['productId'] != '') ? $dataArr['productId'] : '';
        $source_type = (isset($dataArr['source_type']) && $dataArr['source_type'] != '') ? $dataArr['source_type'] : 'frontend';
        $userSessionId = (isset($dataArr['user_session_id']) && $dataArr['user_session_id'] != '') ? $dataArr['user_session_id'] : '';
        $stmt = $this->dbAdapter->createStatement();
        //$stmt->prepare("CALL usp_cart_getInventoryExist(?,?,?)");
        $stmt->prepare("CALL usp_cart_getInventoryExistInCart(?,?,?,?)");
        $stmt->getResource()->bindParam(1, $user_id);
        $stmt->getResource()->bindParam(2, $productId);
        $stmt->getResource()->bindParam(3, $source_type);
        $stmt->getResource()->bindParam(4, $userSessionId);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get Cart LIst
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function getWishListProductAlreadyAdded($dataArr = null) {
        $user_id = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '-1';
        $productId = (isset($dataArr['productId']) && $dataArr['productId'] != '') ? $dataArr['productId'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_getwishlistInventoryExist(?,?)");
        $stmt->getResource()->bindParam(1, $user_id);
        $stmt->getResource()->bindParam(2, $productId);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get Cart LIst
     * @author Icreon Tech - DT
     * @return Arr
     * @param Array
     */
    public function updateCartInventoryProductFront($dataArr = null) {

        $user_id = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '-1';
        $cart_id = (isset($dataArr['cart_id']) && $dataArr['cart_id'] != '') ? $dataArr['cart_id'] : '';
        $type_of_product = (isset($dataArr['type_of_product']) && $dataArr['type_of_product'] != '') ? $dataArr['type_of_product'] : '';
        $num_quantity = (isset($dataArr['num_quantity']) && $dataArr['num_quantity'] != '') ? $dataArr['num_quantity'] : '';
        $user_session_id = (isset($dataArr['user_session_id']) && $dataArr['num_quantity'] != '') ? $dataArr['user_session_id'] : '';

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_cart_updateInventoryProductFront(?,?,?,?,?)");
        $stmt->getResource()->bindParam(1, $user_id);
        $stmt->getResource()->bindParam(2, $cart_id);
        $stmt->getResource()->bindParam(3, $type_of_product);
        $stmt->getResource()->bindParam(4, $num_quantity);
        $stmt->getResource()->bindParam(5, $user_session_id);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
        return true;
    }

    /**
     * Function for to get Cart LIst
     * @author Icreon Tech - DT
     * @return Arr
     * @param Array
     */
    public function updateCartInventoryProduct($dataArr = null) {
        $user_id = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '-1';
        $productId = (isset($dataArr['productId']) && $dataArr['productId'] != '') ? $dataArr['productId'] : '';
        $num_quantity = (isset($dataArr['num_quantity']) && $dataArr['num_quantity'] != '') ? $dataArr['num_quantity'] : '';
        $source_type = (isset($dataArr['source_type']) && $dataArr['source_type'] != '') ? $dataArr['source_type'] : 'frontend';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_cart_updateInventoryProduct(?,?,?,?)");
        $stmt->getResource()->bindParam(1, $user_id);
        $stmt->getResource()->bindParam(2, $productId);
        $stmt->getResource()->bindParam(3, $num_quantity);
        $stmt->getResource()->bindParam(4, $source_type);
        $result = $stmt->execute();
        $statement = $result->getResource();
// $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return true;
    }

    /**
     * Function to approve woh details
     * @author Icreon Tech - DT
     * @return Arr
     * @param Array
     */
    public function updateWohDetails($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_updateUsrWoh(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['id']);
            $stmt->getResource()->bindParam(2, $params['first_name_one']);
            $stmt->getResource()->bindParam(3, $params['other_init_one']);
            $stmt->getResource()->bindParam(4, $params['other_name']);
            $stmt->getResource()->bindParam(5, $params['last_name_one']);
            $stmt->getResource()->bindParam(6, $params['other_init_two']);
            $stmt->getResource()->bindParam(7, $params['first_name_two']);
            $stmt->getResource()->bindParam(8, $params['last_name_two']);
            $stmt->getResource()->bindParam(9, $params['first_line']);
            $stmt->getResource()->bindParam(10, $params['second_line']);
            $stmt->getResource()->bindParam(11, $params['country_originUS']);
            $stmt->getResource()->bindParam(12, $params['country_origin']);
            $stmt->getResource()->bindParam(13, $params['donation_for']);
            $stmt->getResource()->bindParam(14, $params['donated_by']);
            $stmt->getResource()->bindParam(15, $params['modified_by']);
            $stmt->getResource()->bindParam(16, $params['modified_date']);
            $stmt->getResource()->bindParam(17, $params['final_name']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get Cart products
     * @author Icreon Tech - DT
     * @return Arr
     * @param Array
     */
    public function getCartProducts($dataArr = null) {
        $user_id = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '-1';
        $source_type = (isset($dataArr['source_type']) && $dataArr['source_type'] != '') ? $dataArr['source_type'] : '-1';
        $session_id = (isset($dataArr['user_session_id']) && $dataArr['user_session_id'] != '') ? $dataArr['user_session_id'] : '-1';
        $membership_percent_discount = (isset($dataArr['membership_percent_discount']) && $dataArr['membership_percent_discount'] != '' && $dataArr['membership_percent_discount'] != 0) ? $dataArr['membership_percent_discount'] : '';
        $stmt = $this->dbAdapter->createStatement();
        //$stmt->prepare("CALL usp_tra_getCartProducts_temp(?,?,?,?)");
        $stmt->prepare("CALL usp_kiosk_tra_getCartProducts(?,?,?,?)");
        $stmt->getResource()->bindParam(1, $user_id);
        $stmt->getResource()->bindParam(2, $source_type);
        $stmt->getResource()->bindParam(3, $session_id);
        $stmt->getResource()->bindParam(4, $membership_percent_discount);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //echo "<pre>";print_r($resultSet);die;
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for remove transaction
     * @author Icreon Tech - DT
     * @return Void
     * @param Array
     */
    public function removeProductToCart($transactionData) {
        $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_tra_deleteProductToCart(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $transactionData[0]);
        $stmt->getResource()->bindParam(2, $transactionData[1]);
        $stmt->getResource()->bindParam(3, $transactionData[2]);
        $stmt->getResource()->bindParam(4, $transactionData[3]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for to get Cart products
     * @author Icreon Tech - DT
     * @return Arr
     * @param Array
     */
    public function updateFofDetails($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_updateUsrFof(?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['id']);
            $stmt->getResource()->bindParam(2, $params['status']);
            $stmt->getResource()->bindParam(3, $params['donated_by']);
            $stmt->getResource()->bindParam(4, $params['people_photo']);
            $stmt->getResource()->bindParam(5, $params['is_people_photo_visible']);
            $stmt->getResource()->bindParam(6, $params['photo_caption']);
            $stmt->getResource()->bindParam(7, $params['is_photo_caption_visible']);
            $stmt->getResource()->bindParam(8, $params['product_status_id']);
            $stmt->getResource()->bindParam(9, $params['modified_by']);
            $stmt->getResource()->bindParam(10, $params['modified_date']);
            $stmt->getResource()->bindParam(11, $params['people_photo_json']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to update product status
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function updateProductStatus($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_updateProductStatus(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['productId']);
            $stmt->getResource()->bindParam(2, $params['productStatus']);
            $stmt->getResource()->bindParam(3, $params['modified_by']);
            $stmt->getResource()->bindParam(4, $params['modified_date']);
            $stmt->getResource()->bindParam(5, $params['table']);
            $stmt->getResource()->bindParam(6, $params['field']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to update product status
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function searchShippedProducts($params = array()) {
        try {
            $transactionId = (isset($params['transactionId'])) ? $params['transactionId'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getShippedProducts(?)');
            $stmt->getResource()->bindParam(1, $transactionId);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to get tax detail by shipping postal code
     * @author Icreon Tech - KK
     * @return Arr
     * @param Array
     */
    public function getTaxDetail($params = array()) {
        try {
            $shippingState = (isset($params['shipping_state'])) ? $params['shipping_state'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_getTaxDetailByCode(?,?)');
            $stmt->getResource()->bindParam(1, $params['postal_code']);
            $stmt->getResource()->bindParam(2, $shippingState);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get minimum Mebrership
     * @author Icreon Tech - DT
     * @return Arr
     * @param Array
     */
    public function getMinimumMembership() {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_getMinimumMmberShip()");
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function to update product status
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function getShipmentDetails($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getShipmentDetails(?)');
            $stmt->getResource()->bindParam(1, $params['shipmentId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will add final transaction record
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function saveFinalTransaction($transactionData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_insertTransactionDetail(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $stmt->getResource()->bindParam(7, $transactionData[6]);
            $stmt->getResource()->bindParam(8, $transactionData[7]);
            $stmt->getResource()->bindParam(9, $transactionData[8]);
            $stmt->getResource()->bindParam(10, $transactionData[9]);
            $stmt->getResource()->bindParam(11, $transactionData[10]);
            $stmt->getResource()->bindParam(12, $transactionData[11]);
            $stmt->getResource()->bindParam(13, $transactionData[12]);
            $stmt->getResource()->bindParam(14, $transactionData[13]);
            $stmt->getResource()->bindParam(15, $transactionData[14]);
            $stmt->getResource()->bindParam(16, $transactionData[15]);
            $stmt->getResource()->bindParam(17, $transactionData[16]);
            $stmt->getResource()->bindParam(18, $transactionData[17]);
            $stmt->getResource()->bindParam(19, $transactionData[18]);
            $stmt->getResource()->bindParam(20, $transactionData[19]);
            $stmt->getResource()->bindParam(21, $transactionData[20]);
            $stmt->getResource()->bindParam(22, $transactionData[21]);
            $stmt->getResource()->bindParam(23, $transactionData[22]);
            $stmt->getResource()->bindParam(24, $transactionData[23]);
            $stmt->getResource()->bindParam(25, $transactionData[24]);
            $stmt->getResource()->bindParam(26, $transactionData[25]);
            $stmt->getResource()->bindParam(27, $transactionData[26]);
            $stmt->getResource()->bindParam(28, $transactionData[27]);
            $stmt->getResource()->bindParam(29, $transactionData[28]);
			$stmt->getResource()->bindParam(30, $transactionData[29]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will add final transaction record
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function saveCartToTransaction($transactionData = array()) {
        try {

            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_insertCartToTransaction(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $stmt->getResource()->bindParam(7, $transactionData[6]);
            $stmt->getResource()->bindParam(8, $transactionData[7]);
            $stmt->getResource()->bindParam(9, $transactionData[8]);
            $stmt->getResource()->bindParam(10, $transactionData[9]);
            $stmt->getResource()->bindParam(11, $transactionData[10]);
            $stmt->getResource()->bindParam(12, $transactionData[11]);
            $stmt->getResource()->bindParam(13, $transactionData[12]);
            $stmt->getResource()->bindParam(14, $transactionData[13]);
            $stmt->getResource()->bindParam(15, $transactionData[14]);
            $stmt->getResource()->bindParam(16, $transactionData[15]);
            $stmt->getResource()->bindParam(17, $transactionData[16]);
            $stmt->getResource()->bindParam(18, $transactionData[17]);
            $stmt->getResource()->bindParam(19, $transactionData[18]);
            $stmt->getResource()->bindParam(20, $transactionData[19]);
            $stmt->getResource()->bindParam(21, $transactionData[20]);
            $stmt->getResource()->bindParam(22, $transactionData[21]);
            $stmt->getResource()->bindParam(23, $transactionData[22]);
            $stmt->getResource()->bindParam(24, $transactionData[23]);
            $stmt->getResource()->bindParam(25, $transactionData[24]);
            $stmt->getResource()->bindParam(26, $transactionData[25]);
            $stmt->getResource()->bindParam(27, $transactionData[26]);
            $stmt->getResource()->bindParam(28, $transactionData[27]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will add transaction billilng shipping address detail
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function saveTransactionBillingShippingDetail($transactionData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_insertTransactionBillingShipping(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $stmt->getResource()->bindParam(7, $transactionData[6]);
            $stmt->getResource()->bindParam(8, $transactionData[7]);
            $stmt->getResource()->bindParam(9, $transactionData[8]);
            $stmt->getResource()->bindParam(10, $transactionData[9]);
            $stmt->getResource()->bindParam(11, $transactionData[10]);
            $stmt->getResource()->bindParam(12, $transactionData[11]);
            $stmt->getResource()->bindParam(13, $transactionData[12]);
            $stmt->getResource()->bindParam(14, $transactionData[13]);
            $stmt->getResource()->bindParam(15, $transactionData[14]);
            $stmt->getResource()->bindParam(16, $transactionData[15]);
            $stmt->getResource()->bindParam(17, $transactionData[16]);
            $stmt->getResource()->bindParam(18, $transactionData[17]);
            $stmt->getResource()->bindParam(19, $transactionData[18]);
            $stmt->getResource()->bindParam(20, $transactionData[19]);
            $stmt->getResource()->bindParam(21, $transactionData[20]);
            $stmt->getResource()->bindParam(22, $transactionData[21]);
            $stmt->getResource()->bindParam(23, $transactionData[22]);
            $stmt->getResource()->bindParam(24, $transactionData[23]);
            $stmt->getResource()->bindParam(25, $transactionData[24]);
            $stmt->getResource()->bindParam(26, $transactionData[25]);
            $stmt->getResource()->bindParam(27, $transactionData[26]);
            $stmt->getResource()->bindParam(28, $transactionData[27]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will add transaction billilng shipping address detail
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function saveTransactionPaymentDetail($transactionData = array()) {
        try {

            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_insertTransactionPaymentInfo(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $stmt->getResource()->bindParam(7, $transactionData[6]);
            $stmt->getResource()->bindParam(8, $transactionData[7]);
            $stmt->getResource()->bindParam(9, $transactionData[8]);
            $stmt->getResource()->bindParam(10, $transactionData[9]);
            $stmt->getResource()->bindParam(11, $transactionData[10]);
            $stmt->getResource()->bindParam(12, $transactionData[11]);
            $stmt->getResource()->bindParam(13, $transactionData[12]);
            $stmt->getResource()->bindParam(14, $transactionData[13]);
            $stmt->getResource()->bindParam(15, $transactionData[14]);
            $stmt->getResource()->bindParam(16, $transactionData[15]);
            $stmt->getResource()->bindParam(17, $transactionData[16]);
            $stmt->getResource()->bindParam(18, $transactionData[17]);
            $stmt->getResource()->bindParam(19, $transactionData[18]);
            $stmt->getResource()->bindParam(20, $transactionData[19]);
            $stmt->getResource()->bindParam(21, $transactionData[20]);
            $stmt->getResource()->bindParam(22, $transactionData[21]);
            $stmt->getResource()->bindParam(23, $transactionData[22]);
            $stmt->getResource()->bindParam(24, $transactionData[23]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will add transaction note info 
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function saveTransactionNote($transactionData = array()) {
        try {

            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_insertTransactionNote(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to get product track detail
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function getProductTrackDetail($params = array()) {
        try {
            $productId = (isset($params['productId'])) ? $params['productId'] : null;
            $transactionId = (isset($params['transactionId'])) ? $params['transactionId'] : null;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getProductTrackDetail(?,?)');
            $stmt->getResource()->bindParam(1, $productId);
            $stmt->getResource()->bindParam(2, $transactionId);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to get product track detail
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function updateAllProduct($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_updateAllProduct(?,?)');
            $stmt->getResource()->bindParam(1, $params['transactionId']);
            $stmt->getResource()->bindParam(2, $params['status']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to get product track detail
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function getTransactionTrack($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getTransactionTrackId(?)');
            $stmt->getResource()->bindParam(1, $params['transactionId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to get product track detail
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function insertShipmentProduct($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_insertShipmentDetails(?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['productType']);
            $stmt->getResource()->bindParam(2, $params['productId']);
            $stmt->getResource()->bindParam(3, $params['productQty']);
            $stmt->getResource()->bindParam(4, $params['modified_by']);
            $stmt->getResource()->bindParam(5, $params['modified_date']);
            $stmt->getResource()->bindParam(6, $params['transactionId']);
            $stmt->getResource()->bindParam(7, $params['shipmentId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_PRODUCT_SHIPMENT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to get product track detail
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function insertShipment($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_insertShipment(?,?,?)');
            $stmt->getResource()->bindParam(1, $params['modified_by']);
            $stmt->getResource()->bindParam(2, $params['modified_date']);
            $stmt->getResource()->bindParam(3, $params['transactionId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERTED_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to get product track detail
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function getManifestImage($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getManifestImage(?)');
            $stmt->getResource()->bindParam(1, $params['id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to get manifest image edit flag
     * @author Icreon Tech - SR
     * @return Arr
     * @param Array
     */
    public function getManifestImageEditFlag($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getManifestImageEditFlag(?)');
            // $aa =" 'T715-30861136.TIF','T715-30861137.TIF'";
            $stmt->getResource()->bindParam(1, $params['image']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for get matching membership
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function getMatchingMembership($membershipData) {
        $procquesmarkapp = $this->appendQuestionMars(count($membershipData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_set_getKioskMatchingMembership(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $membershipData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet[0];
        else
            return false;
    }

    /**
     * This function will update the user membership
     * @param array.
     * @return boolean
     * @author Icreon Tech -DT
     */
    public function updateUserMembership($transactionData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_usr_updateContactMembership(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $stmt->getResource()->bindParam(7, $transactionData[6]);
            $stmt->getResource()->bindParam(8, $transactionData[7]);
            $stmt->getResource()->bindParam(9, $transactionData[8]);
            $result = $stmt->execute();

            // - start
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                $lastInsertedId = $resultSet['0']['LAST_INSERT_ID'];
            else
                $lastInsertedId = "";

            if (isset($lastInsertedId) and trim($lastInsertedId) != "") {
                $this->insertUserMembershipCards(trim($lastInsertedId), $transactionData);
            }
            // - end

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update the user membership
     * @param array.
     * @return boolean
     * @author Icreon Tech 
     */
    private function insertUserMembershipCards($userMembershipId, $transactionData = array()) {
        try {
            if (isset($transactionData[6]) and trim($transactionData[6]) != "" and isset($transactionData[7]) and trim($transactionData[7]) != "") {
                $dateFrom = trim($transactionData[6]);
                $dateTo = trim($transactionData[7]);
                $yearFrom = date("Y", strtotime($dateFrom));
                $yearTo = date("Y", strtotime($dateTo));
                $diffYear = $yearTo - $yearFrom;

                $memCardArr = array();
                $memCardArr['user_id'] = (isset($transactionData[0]) and trim($transactionData[0]) != "") ? $transactionData[0] : "";
                $memCardArr['user_membership_id'] = $userMembershipId;
                $memCardArr['membership_card_date_from'] = $dateFrom;
                $memCardArr['membership_card_date_to'] = $dateTo;


                if (isset($diffYear) and trim($diffYear) != "" and $diffYear > 0) {
                    // first entry - start
                    $memCardArr['membership_card_date_from'] = $dateFrom;
                    $memCardArr['membership_card_date_to'] = $yearFrom . "-12-31";
                    $this->insertMemberShipCardsRecords($memCardArr);
                    // first entry - end

                    for ($yr = 1; $yr <= trim($diffYear); $yr++) {
                        if ($yearFrom + $yr < $yearTo) {
                            // multi entry - start
                            $mYear = $yearFrom + $yr;
                            $memCardArr['membership_card_date_from'] = $mYear . "-01-01";
                            //;
                            $memCardArr['membership_card_date_to'] = $mYear . "-12-31";
                            $this->insertMemberShipCardsRecords($memCardArr);
                            // multi entry - end 
                        }
                    }

                    // end entry - start
                    $memCardArr['membership_card_date_from'] = $yearTo . "-01-01";
                    $memCardArr['membership_card_date_to'] = $dateTo;
                    $this->insertMemberShipCardsRecords($memCardArr);
                    // end entry - end
                } else {
                    $this->insertMemberShipCardsRecords($memCardArr);
                }
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to insert MemberShip Card Records
     * @author Icreon Tech - NS
     * @return boolean
     * @param Array
     */
    private function insertMemberShipCardsRecords($params = array()) {
        try {
            $params['user_id'] = (isset($params['user_id']) and trim($params['user_id']) != "") ? $params['user_id'] : "";
            $params['user_membership_id'] = (isset($params['user_membership_id']) and trim($params['user_membership_id']) != "") ? $params['user_membership_id'] : "";
            $params['membership_card_date_from'] = (isset($params['membership_card_date_from']) and trim($params['membership_card_date_from']) != "") ? $params['membership_card_date_from'] : "";
            $params['membership_card_date_to'] = (isset($params['membership_card_date_to']) and trim($params['membership_card_date_to']) != "") ? $params['membership_card_date_to'] : "";

            if ($params['user_id'] != "" and $params['user_membership_id'] != "" and $params['membership_card_date_from'] != "" and $params['membership_card_date_to'] != "" and $params['membership_card_date_from'] != $params['membership_card_date_to']) {
                $stmt = $this->dbAdapter->createStatement();
                $stmt->prepare('CALL usp_usr_insertContactMembershipCards(?,?,?,?)');
                $stmt->getResource()->bindParam(1, $params['user_id']);
                $stmt->getResource()->bindParam(2, $params['user_membership_id']);
                $stmt->getResource()->bindParam(3, $params['membership_card_date_from']);
                $stmt->getResource()->bindParam(4, $params['membership_card_date_to']);
                $result = $stmt->execute();
            }
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will get amount user purchased product till date
     * @param array.
     * @return boolean
     * @author Icreon Tech -DT
     */
    public function getTotalUserPurchased($membershipData) {
        //$procquesmarkapp = $this->appendQuestionMars(count($membershipData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_getTotalAmountUserPurchased(?,?,?)');
        $stmt->getResource()->bindParam(1, $membershipData['user_id']);
        $stmt->getResource()->bindParam(2, $membershipData['from_date']);
        $stmt->getResource()->bindParam(3, $membershipData['to_date']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet[0]['total_amount'];
        else
            return false;
    }

    /**
     * Function to get product track detail
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function getTransaction($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_rma_GetTransactionList(?)');
            $stmt->getResource()->bindParam(1, $params['transaction_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to get item status
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function getItemStatus($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_rma_GetItemStatus()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to insert rma
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function insertRma($params = array(0)) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_rma_insertRma(?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['transactionId']);
            $stmt->getResource()->bindParam(2, $params['shipmentId']);
            $stmt->getResource()->bindParam(3, $params['rmaDate']);
            $stmt->getResource()->bindParam(4, $params['rmaAction']);
            $stmt->getResource()->bindParam(5, $params['addedBy']);
            $stmt->getResource()->bindParam(6, $params['addedDate']);
            $stmt->getResource()->bindParam(7, $params['total_refund']);
            $stmt->getResource()->bindParam(8, $params['total_shipping']);
            $stmt->getResource()->bindParam(9, $params['total_tax']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will add product to cart
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function addToWishlistProduct($transactionData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_insertProductAddToWishlist(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $stmt->getResource()->bindParam(7, $transactionData[6]);
            $stmt->getResource()->bindParam(8, $transactionData[7]);
            $stmt->getResource()->bindParam(9, $transactionData[8]);
            $stmt->getResource()->bindParam(10, $transactionData[9]);
            $stmt->getResource()->bindParam(11, $transactionData[10]);
            $stmt->getResource()->bindParam(12, $transactionData[11]);
            $stmt->getResource()->bindParam(13, $transactionData[12]);
            $stmt->getResource()->bindParam(14, $transactionData[13]);
            $stmt->getResource()->bindParam(15, $transactionData[14]);
            $stmt->getResource()->bindParam(16, $transactionData[19]);
            $stmt->getResource()->bindParam(17, $transactionData[20]);
            $stmt->getResource()->bindParam(18, $transactionData[21]);
            $stmt->getResource()->bindParam(19, $transactionData[22]);
            $stmt->getResource()->bindParam(20, $transactionData[23]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get Cart products
     * @author Icreon Tech - DT
     * @return Arr
     * @param Array
     */
    public function getWishlistProducts($dataArr = null) {
        $user_id = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '-1';
        $source_type = (isset($dataArr['source_type']) && $dataArr['source_type'] != '') ? $dataArr['source_type'] : '-1';
        $membership_percent_discount = (isset($dataArr['membership_percent_discount']) && $dataArr['membership_percent_discount'] != '' && $dataArr['membership_percent_discount'] != 0) ? $dataArr['membership_percent_discount'] : '';
        $stmt = $this->dbAdapter->createStatement();
        //$stmt->prepare("CALL usp_tra_getWishlistProducts_temp(?,?,?)");
        $stmt->prepare("CALL usp_tra_getWishlistProducts(?,?,?)");
        $stmt->getResource()->bindParam(1, $user_id);
        $stmt->getResource()->bindParam(2, $source_type);
        $stmt->getResource()->bindParam(3, $membership_percent_discount);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get Cart LIst
     * @author Icreon Tech - DT
     * @return Arr
     * @param Array
     */
    public function removeFromWishlist($dataArr = null) {

        $cart_id = $dataArr['cart_id'];
        $productType = $dataArr['product_type'];
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_removeWishlistFront(?,?)");
        $stmt->getResource()->bindParam(1, $cart_id);
        $stmt->getResource()->bindParam(2, $productType);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
        return true;
    }

    /**
     * Function for to get Cart products
     * @author Icreon Tech - DT
     * @return Arr
     * @param Array
     */
    public function getTransactionDetailFront($dataArr = null) {
        $tran_id = (isset($dataArr['tran_id']) && $dataArr['tran_id'] != '') ? $dataArr['tran_id'] : '-1';

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_getTransactionDetailById(?)");
        $stmt->getResource()->bindParam(1, $tran_id);

        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get Cart products
     * @author Icreon Tech - DT
     * @return Arr
     * @param Array
     */
    public function getCrmRma($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_rma_getCrmRma(?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['startIndex']);
            $stmt->getResource()->bindParam(2, $params['recordLimit']);
            $stmt->getResource()->bindParam(3, $params['sortField']);
            $stmt->getResource()->bindParam(4, $params['sortOrder']);
            $stmt->getResource()->bindParam(5, $params['rma']);
            $stmt->getResource()->bindParam(6, (isset($params['name']) and trim($params['name']) != "") ? addslashes($params['name']) : "");
            $stmt->getResource()->bindParam(7, $params['transaction_id']);
            $stmt->getResource()->bindParam(8, $params['status']);
            $stmt->getResource()->bindParam(9, $params['action']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get Cart products
     * @author Icreon Tech - DT
     * @return Arr
     * @param Array
     */
    public function insertRmaProduct($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_rma_insertRmaProduct(?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['transaction_rma_id']);
            $stmt->getResource()->bindParam(2, $params['retQty']);
            $stmt->getResource()->bindParam(3, $params['retRsn']);
            $stmt->getResource()->bindParam(4, $params['retProid']);
            $stmt->getResource()->bindParam(5, $params['retProtype']);
            $stmt->getResource()->bindParam(6, $params['itemCondition']);
            $stmt->getResource()->bindParam(7, $params['isShipReturn']);
            $stmt->getResource()->bindParam(8, $params['addedBy']);
            $stmt->getResource()->bindParam(9, $params['addedDate']);
            $stmt->getResource()->bindParam(10, $params['refundAmount']);
            $stmt->getResource()->bindParam(11, $params['traProductId']);
            $stmt->getResource()->bindParam(12, $params['traProductType']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get Cart products
     * @author Icreon Tech - DT
     * @return Arr
     * @param Array
     */
    public function getCrmRmaProduct($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_rma_getCrmRmaProduct(?)');
            $stmt->getResource()->bindParam(1, $params['transaction_rma_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to rma action
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function getRmaAction($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_rma_getRmaAction()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to rma status
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function getRmaStatus($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_rma_getRmaStatus()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to update rma
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function updateRma($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_rma_updateRma(?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['rma_id']);
            $stmt->getResource()->bindParam(2, $params['rma_action']);
            $stmt->getResource()->bindParam(3, $params['rma_status']);
            $stmt->getResource()->bindParam(4, $params['bank_name']);
            $stmt->getResource()->bindParam(5, $params['check_no']);
            $stmt->getResource()->bindParam(6, $params['transaction']);
            $stmt->getResource()->bindParam(7, $params['amount']);
            $stmt->getResource()->bindParam(8, $params['modifiedBy']);
            $stmt->getResource()->bindParam(9, $params['modifiedDate']);
            $stmt->getResource()->bindParam(10, $params['transaction_id']);
            $stmt->getResource()->bindParam(11, $params['amount_shipping']);
            $stmt->getResource()->bindParam(12, $params['amount_tax']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to update rma product
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function updateRmaProduct($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_rma_updateRmaProduct(?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['retQty']);
            $stmt->getResource()->bindParam(2, $params['retRsn']);
            $stmt->getResource()->bindParam(3, $params['retProid']);
            $stmt->getResource()->bindParam(4, $params['itemCondition']);
            $stmt->getResource()->bindParam(5, $params['modifiedBy']);
            $stmt->getResource()->bindParam(6, $params['modifiedDate']);
            $stmt->getResource()->bindParam(7, $params['isShipReturn']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will add rma note
     * @param array
     * @return return last id data
     * @author Icreon Tech - AS
     */
    public function addRmaNotes($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_addRmaNote(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['transaction_rma_id']);
            $stmt->getResource()->bindParam(2, $params['notify_note']);
            $stmt->getResource()->bindParam(3, $params['note']);
            $stmt->getResource()->bindParam(4, $params['added_by']);
            $stmt->getResource()->bindParam(5, $params['added_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will add save bulk donations
     * @param array
     * @return return last id data
     * @author Icreon Tech - SK
     */
    public function saveTransactionDonationProdcts($transactionData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_insertDonationProducts(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $stmt->getResource()->bindParam(7, $transactionData[6]);
            $stmt->getResource()->bindParam(8, $transactionData[7]);
            $stmt->getResource()->bindParam(9, $transactionData[8]);
            $stmt->getResource()->bindParam(10, $transactionData[9]);
            $stmt->getResource()->bindParam(11, $transactionData[10]);
            $stmt->getResource()->bindParam(12, $transactionData[11]);
            $stmt->getResource()->bindParam(13, $transactionData[12]);
            $stmt->getResource()->bindParam(14, $transactionData[13]);
            $stmt->getResource()->bindParam(15, $transactionData[14]);
            $stmt->getResource()->bindParam(16, $transactionData[15]);
            $stmt->getResource()->bindParam(17, $transactionData[16]);
            $stmt->getResource()->bindParam(18, $transactionData[17]);
            $stmt->getResource()->bindParam(19, $transactionData[18]);
            $stmt->getResource()->bindParam(20, $transactionData[19]);
            $stmt->getResource()->bindParam(21, $transactionData[20]);
            $stmt->getResource()->bindParam(22, $transactionData[21]);
            $stmt->getResource()->bindParam(23, $transactionData[22]);
            $stmt->getResource()->bindParam(24, $transactionData[23]);
            $stmt->getResource()->bindParam(25, $transactionData[24]);
            $stmt->getResource()->bindParam(26, $transactionData[25]);
            $stmt->getResource()->bindParam(27, $transactionData[26]);
            $stmt->getResource()->bindParam(28, $transactionData[27]);
            $stmt->getResource()->bindParam(29, $transactionData[28]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will save bulk donation batches
     * @param array
     * @return return last id data
     * @author Icreon Tech - SK
     */
    public function saveDonationBatches($params = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($params));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_insertDonationBatches(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $params['payment_batch_id']);
            $stmt->getResource()->bindParam(2, $params['amount']);
            $stmt->getResource()->bindParam(3, $params['batch_date']);
            $stmt->getResource()->bindParam(4, $params['added_by']);
            $stmt->getResource()->bindParam(5, $params['added_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will return max donation batch number
     * @param array
     * @author Icreon Tech - SK
     */
    public function getMaxDonatioBatchNo($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getMaxDonationBatchNo()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will return bulk donation entries batch lists
     * @param array
     * @return return last id data
     * @author Icreon Tech - SK
     */
    public function getBulkEntries($transactionData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_getDonationBatches(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $stmt->getResource()->bindParam(7, $transactionData[6]);
            $stmt->getResource()->bindParam(8, $transactionData[7]);
            $stmt->getResource()->bindParam(9, $transactionData[8]);
            $stmt->getResource()->bindParam(10, $transactionData[9]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will return bulk donation entries batch detail
     * @param array
     * @return return last id data
     * @author Icreon Tech - SK
     */
    public function getDonationBatchDetail($param = array()) {
        try {
            $batch_group_id = (isset($param['payment_batch_id']) && $param['payment_batch_id'] != '') ? $param['payment_batch_id'] : '';
            $start_index = (isset($param['start_index']) && $param['start_index'] != '') ? $param['start_index'] : '0';
            $record_limit = (isset($param['record_limit']) && $param['record_limit'] != '') ? $param['record_limit'] : '10';
            $sort_field = (isset($param['sort_field']) && $param['sort_field'] != '') ? $param['sort_field'] : 'tbl_usr_users.contact_id';
            $sort_order = (isset($param['sort_order']) && $param['sort_order'] != '') ? $param['sort_order'] : 'asc';

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_getDonationBatchDetail(?,?,?,?,?)");
            $stmt->getResource()->bindParam(1, $batch_group_id);
            $stmt->getResource()->bindParam(2, $start_index);
            $stmt->getResource()->bindParam(3, $record_limit);
            $stmt->getResource()->bindParam(4, $sort_field);
            $stmt->getResource()->bindParam(5, $sort_order);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for get all transactions
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function getAllSageBatches($transactionData) {
        $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_tra_getSageBatch(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $transactionData[0]);
        $stmt->getResource()->bindParam(2, $transactionData[1]);
        $stmt->getResource()->bindParam(3, $transactionData[2]);
        $stmt->getResource()->bindParam(4, $transactionData[3]);
        $stmt->getResource()->bindParam(5, $transactionData[4]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get all transactions
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function getAllSageBatchesForTransaction($transactionData) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_sage_getTransactionsForSage(?,?,?,?,?,?,?)");
        $stmt->getResource()->bindParam(1, $transactionData[0]);
        $stmt->getResource()->bindParam(2, $transactionData[1]);
        $stmt->getResource()->bindParam(3, $transactionData[2]);
        $stmt->getResource()->bindParam(4, $transactionData[3]);
        $stmt->getResource()->bindParam(5, $transactionData[4]);
        $stmt->getResource()->bindParam(6, $transactionData[5]);
        $stmt->getResource()->bindParam(7, $transactionData[6]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * This function will save the search for rma 
     * @param $params as array
     * @return boolean value
     * @author Icreon Tech - AS
     */
    public function saveRmaSearch($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_rma_saveSearch(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['crm_user_id']);
            $stmt->getResource()->bindParam(2, $params['search_name']);
            $stmt->getResource()->bindParam(3, $params['search_query']);
            $stmt->getResource()->bindParam(4, $params['isActive']);
            $stmt->getResource()->bindParam(5, $params['isDelete']);
            $stmt->getResource()->bindParam(6, $params['added_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the saved search listing for rma
     * @param this will be an array.
     * @return $resultSet as array or boolean
     * @author Icreon Tech -AS
     */
    public function getRmaSavedSearch($params = array()) {

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_rma_getSavedSearch(?,?,?)');
            $stmt->getResource()->bindParam(1, $params['crm_user_id']);
            $stmt->getResource()->bindParam(2, $params['isActive']);
            $stmt->getResource()->bindParam(3, $params['searchId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will delete the saved search titles
     * @param $params as array
     * @return boolean value
     * @author Icreon Tech -AS
     */
    public function deleteRmaSearch($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_rma_deleteSavedSearch(?,?,?)');
            $stmt->getResource()->bindParam(1, $params['search_id']);
            $stmt->getResource()->bindParam(2, $params['isDelete']);
            $stmt->getResource()->bindParam(3, $params['modifiend_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
//$resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update the save search 
     * @param $params as array
     * @return boolean value
     * @author Icreon Tech - AS
     */
    public function updateRmaSearch($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_rma_updateSearch(?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['crm_user_id']);
            $stmt->getResource()->bindParam(2, $params['search_name']);
            $stmt->getResource()->bindParam(3, $params['search_query']);
            $stmt->getResource()->bindParam(4, $params['isActive']);
            $stmt->getResource()->bindParam(5, $params['isDelete']);
            $stmt->getResource()->bindParam(6, $params['modifiend_date']);
            $stmt->getResource()->bindParam(7, $params['search_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will return max  batch number
     * @param array
     * @author Icreon Tech - KK
     */
    public function getMaxTransactionBatchNo($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getMaxSageBatchNo()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update the sage batch no
     * @param $params as array
     * @return boolean value
     * @author Icreon Tech - KK
     */
    public function updateSageBatchNo($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_updateBatchNo(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['batch_no']);
            $stmt->getResource()->bindParam(2, $params['type_of_product']);
            $stmt->getResource()->bindParam(3, $params['cart_id']);
            $stmt->getResource()->bindParam(4, $params['transaction_id']);
            $stmt->getResource()->bindParam(5, $params['product_type_id']);
            $stmt->getResource()->bindParam(6, $params['product_mapping_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will Insert the batch no to sage batch table
     * @param this will be an array having the array to insert
     * @return this will return a confirmation
     * @author Icreon Tech -KK
     */
    public function saveSageBatchNo($transactionData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_insertSageBatch(?,?)");
            $stmt->getResource()->bindParam(1, $transactionData['batch_no']);
            $stmt->getResource()->bindParam(2, $transactionData['added_by']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will fetch notes of RMA
     * @param this will be an array having the array to insert
     * @return this will return a confirmation
     * @author Icreon Tech - AS
     */
    public function getCrmRmaNotes($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_rma_getCrmRmaNotes(?)');
            $stmt->getResource()->bindParam(1, $params['rma_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update transaction status
     * @param this will be an array having the array to insert
     * @return this will return a confirmation
     * @author Icreon Tech - AS
     */
    public function updateTransactionStatus($params = array()) {
        try {
            $params['modified_by'] = (isset($params['modified_by']) && !empty($params['modified_by'])) ? $params['modified_by'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_updateTransactionStatus(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['transactionId']);
            $stmt->getResource()->bindParam(2, $params['transactionStatus']);
            $stmt->getResource()->bindParam(3, $params['modified_by']);
            $stmt->getResource()->bindParam(4, $params['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update shipment tracking code
     * @param array
     * @return boolean
     * @author Icreon Tech - DT
     */
    public function updateShipmentTrackingCode($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_updateShipmentTracking(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['transaction_shipment_id']);
            $stmt->getResource()->bindParam(2, $params['tracking_code']);
            $stmt->getResource()->bindParam(3, $params['modified_by']);
            $stmt->getResource()->bindParam(4, $params['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will fetch transaction product shipped status
     * @param array
     * @return boolean
     * @author Icreon Tech - AS
     */
    public function searchProductsShipped($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getTraProShippedStatus(?,?)');
            $stmt->getResource()->bindParam(1, $params['transactionId']);
            $stmt->getResource()->bindParam(2, $params['productId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will Insert the batch no to sage batch table
     * @param this will be an array having the array to insert
     * @return this will return a confirmation
     * @author Icreon Tech -KK
     */
    public function checkFreeShipping($transactionData = array()) {
        $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_tra_checkFreeShipping(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $transactionData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet[0];
        else
            return array();
    }

    /**
     * This function will delete shipment details on error of shipment API
     * @param this will be an array having the array to insert
     * @return this will return a confirmation
     * @author Icreon Tech - AS
     */
    public function deleteShipment($param) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_deleteShipmentDetails(?)');
            $stmt->getResource()->bindParam(1, $param);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will fetch fof details by id
     * @param this will be an array 
     * @return this will return an array
     * @author Icreon Tech - AS
     */
    public function searchFofDetails($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getFofDetails(?)');
            $stmt->getResource()->bindParam(1, $param['id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will fetch bio details by id
     * @param this will be an array 
     * @return this will return an array
     * @author Icreon Tech - AS
     */
    public function searchBioDetails($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getBioDetails(?)');
            $stmt->getResource()->bindParam(1, $param['id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will fetch fof details by id
     * @param this will be an array 
     * @return this will return an array
     * @author Icreon Tech - AS
     */
    public function searchDocPass($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getPassDetails(?,?,?)');
            $stmt->getResource()->bindParam(1, $param['id']);
            $stmt->getResource()->bindParam(2, $param['product_type_id']);
            $stmt->getResource()->bindParam(3, $param['item_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get product status for view transaction
     * @author Icreon Tech - AS
     * @return Array
     * @param Array
     */
    public function getProductStatus() {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getViewProductStatus()");
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * This function will fetch fof details by id
     * @param this will be an array 
     * @return this will return an array
     * @author Icreon Tech - AS
     */
    public function searchDocShip($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getPassShipDetails(?,?)');
            $stmt->getResource()->bindParam(1, $param['id']);
            $stmt->getResource()->bindParam(2, $param['product_type_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for fetching program list
     * @return $resultSet as array or false as boolean 
     * @param $dataArr as an array
     * @author Icreon Tech - AS
     */
    public function getCampaignByProgram($dataArr = null) {
        $p_code = (isset($dataArr['p_code']) && $dataArr['p_code'] != '') ? $dataArr['p_code'] : '';
        $p_campaign_id = (isset($dataArr['p_campaign_id']) && $dataArr['p_campaign_id'] != '') ? $dataArr['p_campaign_id'] : '';
        $program_id = isset($dataArr['program_id']) ? $dataArr['program_id'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_getCampaignByProgram(?,?,?)');
        $stmt->getResource()->bindParam(1, $p_code);
        $stmt->getResource()->bindParam(2, $p_campaign_id);
        $stmt->getResource()->bindParam(3, $program_id);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for fetching program list
     * @return $resultSet as array or false as boolean 
     * @param $dataArr as an array
     * @author Icreon Tech - AS
     */
    public function getProgramWithCampaign() {

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_com_getProgramsWithCampaign()');
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for fetching shippment transaction detail
     * @return $resultSet as array or false as boolean 
     * @param $dataArr as an array
     * @author Icreon Tech - AS
     */
    public function getShippmentByTransShipment($dataArr) {
        $procquesmarkapp = $this->appendQuestionMars(count($dataArr));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_tra_getShipmentByTranShip(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $dataArr[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return $resultSet;
    }

    /**
     * Function for fetching holiday detail
     * @return $resultSet as array or false as boolean 
     * @param $dataArr as an array
     * @author Icreon Tech - AS
     */
    public function getHoliday($dataArr) {
        $procquesmarkapp = $this->appendQuestionMars(count($dataArr));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_tra_getHoliday(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $dataArr[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return $resultSet;
    }

    /**
     * Function for fetching shipping methods detail
     * @return $resultSet as array or false as boolean 
     * @param $dataArr as an array
     * @author Icreon Tech - AS
     */
    public function getShippingMethod($dataArr) {
        $procquesmarkapp = $this->appendQuestionMars(count($dataArr));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_tra_getShippingMethods(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $dataArr[0]);
        $stmt->getResource()->bindParam(2, $dataArr[1]);
        $stmt->getResource()->bindParam(3, $dataArr[2]);
        $stmt->getResource()->bindParam(4, $dataArr[3]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return $resultSet;
    }

    /**
     * This function will update the shipping type for shippment
     * @param array
     * @return this will return a confirmation
     * @author Icreon Tech -DT
     */
    public function updateShippmentType($transactionData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_updateShippmentType(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for get credit transaction for a transaction
     * @return array
     * @param $dataArr as an array
     * @author Icreon Tech - DT
     */
    public function getCreditTransactionPayment($dataArr) {
        $procquesmarkapp = $this->appendQuestionMars(count($dataArr));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_tra_getCreditTransactionPayment(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $dataArr[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return $resultSet;
    }

    /**
     * Function for get profile id and credit card id
     * @return array
     * @param $dataArr as an array
     * @author Icreon Tech - DT
     */
    public function getTransactionProfileCreditCartId($dataArr) {
        $transactionId = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $authorizeTransactionId = (isset($dataArr['authorize_transaction_id']) && $dataArr['authorize_transaction_id'] != '') ? $dataArr['authorize_transaction_id'] : '';

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_tra_getTransactionProfileCreditCartId(?,?)');
        $stmt->getResource()->bindParam(1, $transactionId);
        $stmt->getResource()->bindParam(2, $authorizeTransactionId);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return $resultSet;
    }

    /**
     * This function will add credit card receive payment
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function saveTransactionPaymentReceive($transactionData = array()) {
        try {

            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_insertCreditPayReceive(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $stmt->getResource()->bindParam(7, $transactionData[6]);
            $stmt->getResource()->bindParam(8, $transactionData[7]);
            $stmt->getResource()->bindParam(9, $transactionData[8]);
            $stmt->getResource()->bindParam(10, $transactionData[9]);
            $stmt->getResource()->bindParam(11, $transactionData[10]);
            $stmt->getResource()->bindParam(12, $transactionData[11]);
            $stmt->getResource()->bindParam(13, $transactionData[12]);
            $stmt->getResource()->bindParam(14, $transactionData[13]);
            $stmt->getResource()->bindParam(15, $transactionData[14]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for get credit transaction for a transaction
     * @return array
     * @param $dataArr as an array
     * @author Icreon Tech - DT
     */
    public function getShippmentAmount($dataArr) {
        $procquesmarkapp = $this->appendQuestionMars(count($dataArr));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_tra_getShippmentTotalAmount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $dataArr[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return $resultSet;
    }

    /**
     * This function will get all transaction payment made
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function getTransactionPayments($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getTransactionPayments(?)');
            $stmt->getResource()->bindParam(1, $params['transactionId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will get all transaction payment received
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function getTransactionPaymentsReceived($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getTransactionPaymentsReceive(?)');
            $stmt->getResource()->bindParam(1, $params['transactionId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will add coupon used log
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function saveCouponUsedArr($transactionData = array()) {
        try {

            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_prm_insertUserPromotionLog(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    public function updateNote($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_com_updateNote(?,?,?,?)");
            $stmt->getResource()->bindParam(1, $param['tableName']);
            $stmt->getResource()->bindParam(2, $param['tableField']);
            $stmt->getResource()->bindParam(3, $param['noteId']);
            $stmt->getResource()->bindParam(4, $param['val']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function getDonDetails($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getUserDon(?)');
            $stmt->getResource()->bindParam(1, $param['id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    public function updateDonDetails($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_updateUserDon(?,?,?)');
            $stmt->getResource()->bindParam(1, $param['in_honoree']);
            $stmt->getResource()->bindParam(2, $param['honor_memory_name']);
            $stmt->getResource()->bindParam(3, $param['id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
//            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function getDonNotify($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getDonNotify(?)');
            $stmt->getResource()->bindParam(1, $param['cart_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    public function updateCartCrmProduct($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_updateCrmProduct(?,?,?)');
            $stmt->getResource()->bindParam(1, $param['cartId']);
            $stmt->getResource()->bindParam(2, $param['qty']);
            $stmt->getResource()->bindParam(3, $param['typeOfProduct']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will get user Woh Contacts
     * @param array
     * @return return array of contacts
     * @author Icreon Tech - NS
     */
    public function getUserWohContact($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getWohContact(?)');
            $stmt->getResource()->bindParam(1, $params['woh_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet)) {
                return $resultSet;
            } else {
                return array();
            }
        } catch (Exception $e) {
            return array();
        }
    }

    /**
     * This function will get all shipping method
     * @param array
     * @return return array of contacts
     * @author Icreon Tech - NS
     */
    public function getAllShippingMethod() {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getAllShippingMethod()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet)) {
                return $resultSet;
            } else {
                return array();
            }
        } catch (Exception $e) {
            return array();
        }
    }

    /**
     * This function will get all shipping method
     * @param array
     * @return return array of contacts
     * @author Icreon Tech - NS
     */
    public function getBioProductId($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getBioProductId(?,?)');
            $stmt->getResource()->bindParam(1, $params['bio_certificate_panel_id']);
            $stmt->getResource()->bindParam(2, $params['bio_wohid']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet)) {
                return $resultSet;
            } else {
                return array();
            }
        } catch (Exception $e) {
            return array();
        }
    }

    /**
     * This function will get details of ship products
     * @param array
     * @return return array of contacts
     * @author Icreon Tech - AS
     */
    public function getProductsDetails($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getShipProducts(?)');
            $stmt->getResource()->bindParam(1, $params['product_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet)) {
                return $resultSet;
            } else {
                return array();
            }
        } catch (Exception $e) {
            return array();
        }
    }

    /**
     * This function will get details of ship products
     * @param array
     * @return return array of contacts
     * @author Icreon Tech - AS
     */
    public function insertTrackingCode($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_updateTrackingCode(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['track_code']);
            $stmt->getResource()->bindParam(2, $params['shipment_product_id']);
            $stmt->getResource()->bindParam(3, $params['modified_by']);
            $stmt->getResource()->bindParam(4, $params['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to get product track detail
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function getCustomTrack($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getTransactionTrackCode(?)');
            $stmt->getResource()->bindParam(1, $params['transactionId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to get bio product detail
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function getBioDetails($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_bioDetails(?)');
            $stmt->getResource()->bindParam(1, $params['id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to update the payment / payment received from authorize site.
     * @author Icreon Tech - SR
     * @return array
     * @param Array
     */
    public function updateTransactionBatchId($params = array()) {
        try {
            $batchId = (isset($params['payment_batch_id']) && $params['payment_batch_id'] != '') ? $params['payment_batch_id'] : '';
            $authorizeTransactionId = (isset($params['authorize_transaction_id']) && $params['authorize_transaction_id'] != '') ? $params['authorize_transaction_id'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_updateTransactionBatchId(?,?)');
            $stmt->getResource()->bindParam(1, $batchId);
            $stmt->getResource()->bindParam(2, $authorizeTransactionId);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            if ($result) {
                return $result;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to insert the batch into into master table.
     * @author Icreon Tech - SR
     * @return array
     * @param Array
     */
    public function insertTransactionBatchId($params = array()) {
        try {
            $batchId = (isset($params['batch_id']) && $params['batch_id'] != '') ? $params['batch_id'] : '';
            $settlementDate = (isset($params['settlement_date']) && $params['settlement_date'] != '') ? $params['settlement_date'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_insertTransactionBatchId(?,?)');
            $stmt->getResource()->bindParam(1, $batchId);
            $stmt->getResource()->bindParam(2, $settlementDate);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if ($resultSet) {
                return $resultSet;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to check if batch id exists in master table or not.
     * @author Icreon Tech - SR
     * @return array
     * @param Array
     */
    public function checkBatchId($params = array()) {
        try {
            $batchId = (isset($params['batch_id']) && $params['batch_id'] != '') ? $params['batch_id'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_checkBatchId(?)');
            $stmt->getResource()->bindParam(1, $batchId);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if ($resultSet) {
                return $resultSet;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the credit card id from card master table.
     * @author Icreon Tech - SR
     * @return array
     * @param Array
     */
    public function getCreditCardId($params = array()) {
        try {
            $cartType = (isset($params['cartType']) && $params['cartType'] != '') ? $params['cartType'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getPaymentCardType(?)');
            $stmt->getResource()->bindParam(1, $cartType);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to update the credit card id in transaction table
     * @author Icreon Tech - SR
     * @return array
     * @param Array
     */
    public function updateTransactionCardTypeInfo($params = array()) {
        try {
            $cartType = (isset($params['cartType']) && $params['cartType'] != '') ? $params['cartType'] : '';
            $authorizeTransactionId = (isset($params['authorize_transaction_id']) && $params['authorize_transaction_id'] != '') ? $params['authorize_transaction_id'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_updateTransactionCard(?,?)');
            $stmt->getResource()->bindParam(1, $cartType);
            $stmt->getResource()->bindParam(2, $authorizeTransactionId);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            if ($result) {
                return $result;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to select the batch id.
     * @author Icreon Tech - SR
     * @return array
     * @param Array
     */
    public function getBatchId($params = array()) {
        try {
            $batchId = (isset($params['batch_id']) && $params['batch_id'] != '') ? $params['batch_id'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getBatchId(?)');
            $stmt->getResource()->bindParam(1, $batchId);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if ($resultSet) {
                return $resultSet;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to update the campaign revenue goal
     * @author Icreon Tech - AS
     * @return array
     * @param Array
     */
    public function updateRevenueGoal($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_updateRevenueGoal(?,?)');
            $stmt->getResource()->bindParam(1, $params[26]);
            $stmt->getResource()->bindParam(2, $params[22]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            if ($result) {
                return $result;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get General Campaign
     * @author Icreon Tech - AS
     * @return array
     * @param Array
     */
    public function getGeneralCampaign($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $year = date('Y');
            $stmt->prepare('CALL usp_tra_getGeneralCampaign(?)');
            $stmt->getResource()->bindParam(1, $year);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to update the channel revenue received
     * @author Icreon Tech - AS
     * @return array
     * @param Array
     */
    public function updateChannelRevenue($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_updateChannelRevenue(?,?,?)');
            $stmt->getResource()->bindParam(1, $params[26]);
            $stmt->getResource()->bindParam(2, $params[22]);
            $stmt->getResource()->bindParam(3, $params[23]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            if ($result) {
                return $result;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    public function getBatchFreeTransactions($params = array()) {
        try {
            $start_index = isset($params['start_index']) ? $params['start_index'] : '';
            $record_limit = isset($params['record_limit']) ? $params['record_limit'] : '';
            $sort_field = isset($params['sort_field']) ? $params['sort_field'] : '';
            $sort_order = isset($params['sort_order']) ? $params['sort_order'] : '';

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getBatchFreeTransactions(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $start_index);
            $stmt->getResource()->bindParam(2, $record_limit);
            $stmt->getResource()->bindParam(3, $sort_field);
            $stmt->getResource()->bindParam(4, $sort_order);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if ($resultSet) {
                return $resultSet;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    public function assignBatchId($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_assignbatch(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['received_payment_id']);
            $stmt->getResource()->bindParam(2, $params['payment_batch_id']);
            $stmt->getResource()->bindParam(3, $params['modified_date']);
            $stmt->getResource()->bindParam(4, $params['modified_by']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            //$resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            /* if ($resultSet) {

              return $resultSet;
              } else {
              return false;
              } */
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the pending paymenst
     * @author Icreon Tech - AS
     * @return array
     * @param Array
     */
    public function getPendingPaymentRecords($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getPendingPayments(?)');
            $stmt->getResource()->bindParam(1, $params['transactionId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if ($resultSet) {
                return $resultSet;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get shipment product
     * @author Icreon Tech - AS
     * @return array
     * @param Array
     */
    public function getShippmentProduct($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getShipmentProduct(?)');
            $stmt->getResource()->bindParam(1, $params['shipment_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();
            if ($resultSet) {
                return $resultSet;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get payment received
     * @author Icreon Tech - AS
     * @return array
     * @param Array
     */
    public function getPaymentReceived($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getPaymentReceivedRecords(?,?)');
            $stmt->getResource()->bindParam(1, $params['table_auto_id']);
            $stmt->getResource()->bindParam(2, $params['table_type']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();
            if ($resultSet) {
                return $resultSet;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to update payment received
     * @author Icreon Tech - AS
     * @return array
     * @param Array
     */
    public function updatePaymentReceived($params = array()) {
        try {
            $isPaymentReceived = (isset($params['is_payment_received']) && $params['is_payment_received'] != '') ? $params['is_payment_received'] : '';
            $receivedPaymentId = (isset($params['received_payment_id']) && $params['received_payment_id'] != '') ? $params['received_payment_id'] : '';
            $authorizeTransactionId = (isset($params['authorize_transaction_id']) && $params['authorize_transaction_id'] != '') ? $params['authorize_transaction_id'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_updatePaymentReceivedRecords(?,?,?)');
            $stmt->getResource()->bindParam(1, $isPaymentReceived);
            $stmt->getResource()->bindParam(2, $receivedPaymentId);
            $stmt->getResource()->bindParam(3, $authorizeTransactionId);
            $result = $stmt->execute();
            $statement = $result->getResource();
            // $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC); 

            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to remove the authorize transation id.
     * @author Icreon Tech - SR
     * @return array
     * @param Array
     */
    public function removeAuthorizeTransactionId($params = array()) {
        try {
            $transactionId = (isset($params['transaction_id']) && $params['transaction_id'] != '') ? $params['transaction_id'] : '';

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_removePaymentAuthTransactionId(?)');
            $stmt->getResource()->bindParam(1, $transactionId);
            $result = $stmt->execute();
            $statement = $result->getResource();
            // $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC); 

            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used tvoid the authrize transaction
     * @author Icreon Tech - SR
     * @return array
     * @param Array
     */
    public function voidAuthorizeTransaction($param = array(), $config = array()) {

        $trans = array('transaction' => array('profileTransVoid' => array(
                    'customerProfileId' => $param['customerProfileId'],
                    'customerPaymentProfileId' => $param['paymentProfileId'],
                    'transId' => $param['authTransactionId']
                )
            )
        );
        $paymentTrans = new \AuthnetXML($config['authorize_net_payment_gateway']['API_LOGIN_ID'], $config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $config['authorize_net_payment_gateway']['ENV']);
        $paymentTrans->createCustomerProfileTransactionRequest($trans);


        return true;
    }

    /**
     * This function will get all transaction payment received
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function getTransactionReceivedPayment($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getTransactionReceivedPayment(?)');
            $stmt->getResource()->bindParam(1, $params['transactionId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will get all transaction payment received
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function getGroupId($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getUserCampGroupId(?,?)');
            $stmt->getResource()->bindParam(1, $params['user_id']);
            $stmt->getResource()->bindParam(2, $params['campaign_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to set the autocommit off for transactions tables
     * @param void
     * @return void
     * @author Icreon Tech - SR
     */
    public function offAutoCommit() {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL sp_auto_commit_off()');
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * This function is used to set the autocommit off for transactions tables
     * @param void
     * @return void
     * @author Icreon Tech - SR
     */
    public function onAutoCommit() {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL sp_auto_commit_on()');
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * This function will add fof to cart
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function getTransactionRmaStatus($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_get_transactionRmaStatus(?)');
            $stmt->getResource()->bindParam(1, $params['transactionId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function update actual shipping price
     * @param type $transactionData
     * @return boolean
     */
    public function updateActualShippingPrice($transactionData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_updateActualShippingPrice(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $stmt->getResource()->bindParam(4, $transactionData[3]);
            $stmt->getResource()->bindParam(5, $transactionData[4]);
            $stmt->getResource()->bindParam(6, $transactionData[5]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function update actual shipping price of products in transaction
     * @param type $transactionData
     * @return boolean
     */
    public function updateProductActualShipping($transactionData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($transactionData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_updateProductActualShipping(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $transactionData[0]);
            $stmt->getResource()->bindParam(2, $transactionData[1]);
            $stmt->getResource()->bindParam(3, $transactionData[2]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * get shipped products of transaction with quantity
     * @param type $shipmentData
     * @return boolean
     */
    public function getShipmentProductsQtyWise($shipmentData = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_getShipmentProductsQtyWise(?)");
            $stmt->getResource()->bindParam(1, $shipmentData['shipmentId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will get transaction RMA
     * @param array
     * @return return last id data
     * @author Icreon Tech - SR
     */
    public function checkRmaAlreadyCreated($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_rma_getRmaForProduct(?,?)');
            $stmt->getResource()->bindParam(1, $params['transaction_product_id']);
            $stmt->getResource()->bindParam(2, $params['product_type']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to update rma product
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function updateTransactionProductStatus($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_rma_updateTransactionProductStatus(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['transactionProductType']);
            $stmt->getResource()->bindParam(2, $params['transactionProductId']);
            $stmt->getResource()->bindParam(3, $params['transactionProductStatus']);
            $stmt->getResource()->bindParam(4, $params['refundAmount']);
            $stmt->getResource()->bindParam(5, $params['refundShipping']);
            $stmt->getResource()->bindParam(6, $params['refundTax']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get minimum Mebrership
     * @author Icreon Tech - DT
     * @return Arr
     * @param Array
     */
    public function getMinimumMembershipDiscount() {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_getMinimumMemberShipDiscount()");
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function used to get the user transaction details used for RMA
     * @author Icreon Tech - DT
     * @return Arr
     * @param Array
     */
    public function getUserTransactionDetails($params = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_get_usr_transactionDetails(?)");
        $stmt->getResource()->bindParam(1, $params['transaction_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function used to get the user transaction details used for RMA
     * @author Icreon Tech - DT
     * @return Arr
     * @param Array
     */
    public function updateTotalRevenueAmount($params = array()) {

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_updateTotalRevenue(?,?,?,?)");

            $campaign_id = (isset($params['campaign_id']) && $params['campaign_id'] != '' && $params['campaign_id'] != '0') ? $params['campaign_id'] : '';
            $channel_id = (isset($params['channel_id']) && $params['channel_id'] != '' && $params['channel_id'] != '0') ? $params['channel_id'] : '';
            $amount = (isset($params['amount']) && $params['amount'] != '') ? $params['amount'] : '';
            $amount_action = (isset($params['amount_action']) && $params['amount_action'] != '') ? $params['amount_action'] : '';

            $stmt->getResource()->bindParam(1, $campaign_id);
            $stmt->getResource()->bindParam(2, $channel_id);
            $stmt->getResource()->bindParam(3, $amount);
            $stmt->getResource()->bindParam(4, $amount_action);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function checkUserTransaction($params = array()) {
        $transaction_id = (isset($params['transaction_id']) && $params['transaction_id'] != '' && $params['transaction_id'] != '0') ? $params['transaction_id'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_tra_checkUserCreditTransaction(?)');
        $stmt->getResource()->bindParam(1, $transaction_id);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0];
        } else {
            return false;
        }
    }

    /**
     * This function is used to insert soft credit.
     * @author Icreon Tech - SR
     * @return array
     * @param Array
     */
    public function saveSoftCreditTransaction($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_insertSoftCreditTransaction(?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['user_id']);
            $stmt->getResource()->bindParam(2, $params['transaction_id']);
            $stmt->getResource()->bindParam(3, $params['provider_user_id']);
            $stmt->getResource()->bindParam(4, $params['amount']);
            $stmt->getResource()->bindParam(5, $params['notes']);
            $stmt->getResource()->bindParam(6, $params['credit_date']);
            $stmt->getResource()->bindParam(7, $params['added_date']);
            $stmt->getResource()->bindParam(8, $params['added_by']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if ($resultSet) {
                return $resultSet;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    public function getShippingMethodDoNow($dataArr) {
        $procquesmarkapp = $this->appendQuestionMars(count($dataArr));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_tra_getShippingMethodsDoNow(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $dataArr[0]);
        $stmt->getResource()->bindParam(2, $dataArr[1]);
        $stmt->getResource()->bindParam(3, $dataArr[2]);
        $stmt->getResource()->bindParam(4, $dataArr[3]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        return $resultSet;
    }
    /**
     * This function is used get the payment credit card no and authorize transaction id.
     * @author Icreon Tech - SR
     * @return array
     * @param Array
     */
    public function getPaymentProfileDetails($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getPaymentProfileDetails(?)');
            $stmt->getResource()->bindParam(1, $params['transaction_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if ($resultSet) {
                return $resultSet;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }
    /**
     * Function to update rma
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function updateRmaStatus($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_rma_updateRmaStatus(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['rma_id']);
            $stmt->getResource()->bindParam(2, $params['rma_action']);
            $stmt->getResource()->bindParam(3, $params['modifiedBy']);
            $stmt->getResource()->bindParam(4, $params['modifiedDate']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
	 /**
     * Function to update status of all product
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function updateInvoiceAllProduct($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_updateInvoiceProductStatus(?,?)');
            $stmt->getResource()->bindParam(1, $params['transactionId']);
            $stmt->getResource()->bindParam(2, $params['status']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    public function getTransactionProfileId($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getTransactionPaymentProfileId(?)');
            $stmt->getResource()->bindParam(1, $params['transactionId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if ($resultSet) {
                return $resultSet;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }/**
     * Function to mark manifest image
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function markManifestImage($params = array()) {
		try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_markManifestImage(?,?)');
			$stmt->getResource()->bindParam(1, $params['id']);
			$stmt->getResource()->bindParam(2, $params['status']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
	}    
    public function updateCartTableInformation($params = array()){
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_updateCartRecords(?,?,?,?,?,?,?,?,?)');
			$stmt->getResource()->bindParam(1, $params['cart_id']);
			$stmt->getResource()->bindParam(2, $params['is_pending_transaction']);
			$stmt->getResource()->bindParam(3, $params['pending_transaction_id']);
			$stmt->getResource()->bindParam(4, $params['product_type_id']);
			$stmt->getResource()->bindParam(5, $params['type_of_product']);
			$stmt->getResource()->bindParam(6, $params['user_id']);
			$stmt->getResource()->bindParam(7, $params['pending_transaction_date']);
			$stmt->getResource()->bindParam(8, $params['item_info']);
			$stmt->getResource()->bindParam(9, $params['product_mapping_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
        
    }
    /**
     * This function will add final transaction record
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function saveFinalCashTransaction($transactionData = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_insertCashTransactionDetail(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $stmt->getResource()->bindParam(1, $transactionData['user_id']);
            $stmt->getResource()->bindParam(2, $transactionData['amount']);
            $stmt->getResource()->bindParam(3, $transactionData['payment_mode_id']);
            $stmt->getResource()->bindParam(4, $transactionData['is_payment_status']);
            $stmt->getResource()->bindParam(5, $transactionData['transaction_date']);
            $stmt->getResource()->bindParam(6, $transactionData['billing_title']);
            $stmt->getResource()->bindParam(7, $transactionData['billing_first_name']);
            $stmt->getResource()->bindParam(8, $transactionData['billing_last_name']);
            $stmt->getResource()->bindParam(9, $transactionData['billing_company_name']);
            $stmt->getResource()->bindParam(10, $transactionData['billing_address']);
            $stmt->getResource()->bindParam(11, $transactionData['billing_city']);
            $stmt->getResource()->bindParam(12, $transactionData['billing_state']);
            $stmt->getResource()->bindParam(13, $transactionData['billing_postal_code']);
            $stmt->getResource()->bindParam(14, $transactionData['billing_country']);
            $stmt->getResource()->bindParam(15, $transactionData['billing_phone_no']);
            $stmt->getResource()->bindParam(16, $transactionData['shipping_address_type']);
            $stmt->getResource()->bindParam(17, $transactionData['shipping_title']);
            $stmt->getResource()->bindParam(18, $transactionData['shipping_first_name']);
            $stmt->getResource()->bindParam(19, $transactionData['shipping_last_name']);
            $stmt->getResource()->bindParam(20, $transactionData['shipping_company_name']);
            $stmt->getResource()->bindParam(21, $transactionData['shipping_address']);
            $stmt->getResource()->bindParam(22, $transactionData['shipping_city']);
            $stmt->getResource()->bindParam(23, $transactionData['shipping_state']);
            $stmt->getResource()->bindParam(24, $transactionData['shipping_postal_code']);
            $stmt->getResource()->bindParam(25, $transactionData['shipping_country']);
            $stmt->getResource()->bindParam(26, $transactionData['shipping_phone_no']);
            $stmt->getResource()->bindParam(27, $transactionData['added_date']);
            $stmt->getResource()->bindParam(28, $transactionData['modified_date']);
            $stmt->getResource()->bindParam(29, $transactionData['coupon_code']);
            $stmt->getResource()->bindParam(30, $transactionData['pick_up']);
            $stmt->getResource()->bindParam(31, $transactionData['shipping_method']);
            
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }    
	public function getOrderFileName($transactionData = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_getProductTransactionDetails(?)');
            $stmt->getResource()->bindParam(1, $transactionData['transaction_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if ($resultSet) {
                return $resultSet;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }	
	}    
	/**
     * This function will get transaction product details
     * @param array
     * @return return last id data
     * @author Icreon Tech - AS
     */
    public function searchCashTransactionProducts($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_kiosk_tra_getTransactionProducts(?)');
            $stmt->getResource()->bindParam(1, $params['transactionId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }        
    
	/**
     * This function wll update the user id in cart table for WOH
     * @param array
     * @return return void
     * @author Icreon Tech - SR
     */
    public function updateCartTableUserId($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pos_tra_updateCartUserId(?,?)');
            $stmt->getResource()->bindParam(1, $params['user_id']);
            $stmt->getResource()->bindParam(2, $params['session_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }            
 }
