<?php

/**
 * This model is used for user. Transaction
 * @package    Transaction
 * @author     Icreon Tech - DG
 */

namespace Transaction\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This model is used for user. Transaction
 * @package    Transaction
 * @author     Icreon Tech - DG
 */
class BatchTable {

    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
    }

        /**
     * Function to get Batch Incremented Last Id
     * @author Icreon Tech - SR
     * @return Array
     * @param Array
     */    
    public function getBatchIncrementedLastId() {
      try {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_tra_get_batchIncrementLastId()');
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if(isset($resultSet[0]['Auto_increment']) and trim($resultSet[0]['Auto_increment']) != "")
            return trim($resultSet[0]['Auto_increment']);
        else 
            return "1";
      }
      catch (Exception $e) {
            return false;
      }
    }
    
    /**
     * Function to get batch Id and transaction
     * @author Icreon Tech - SR
     * @return Array
     * @param Array
     */    
    public function getBatchListing($params = array()) {
        try {
            $payment_batch_id = (isset($params['payment_batch_id']) && $params['payment_batch_id'] != '') ? $params['payment_batch_id'] : '';
			$batch_id = (isset($params['batch_id']) && $params['batch_id'] != '') ? $params['batch_id'] : '';
			$check_available = (isset($params['check_available']) && $params['check_available'] != '') ? $params['check_available'] : '';
			$is_locked = (isset($params['is_locked']) && $params['is_locked'] != '') ? $params['is_locked'] : '';
            $settlementDate = (isset($params['statement_date']) && $params['statement_date'] != '') ? $params['statement_date'] : '';
            $startIndex = (isset($params['startIndex']) && $params['startIndex'] != '') ? $params['startIndex'] : '';
            $recordLimit = (isset($params['recordLimit']) && $params['recordLimit'] != '') ? $params['recordLimit'] : '';
            $sortField = (isset($params['sortField']) && $params['sortField'] != '') ? $params['sortField'] : '';
            $sortOrder = (isset($params['sortOrder']) && $params['sortOrder'] != '') ? $params['sortOrder'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_get_batchId(?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $payment_batch_id);
			$stmt->getResource()->bindParam(2, $batch_id);
			$stmt->getResource()->bindParam(3, $check_available);
			$stmt->getResource()->bindParam(4, $is_locked);
            $stmt->getResource()->bindParam(5, $settlementDate);
            $stmt->getResource()->bindParam(6, $startIndex);
            $stmt->getResource()->bindParam(7, $recordLimit);
            $stmt->getResource()->bindParam(8, $sortField);
            $stmt->getResource()->bindParam(9, $sortOrder);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
			return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    
    public function createBatchId($params = array()){
        try {
            $batch_id = $params['batch_id'];
            $settlementDate = $params['settlement_date'];
            $added_by = $params['added_by'];
            $added_date = $params['added_date'];
            $modified_by = $params['modified_by'];
            $modified_date = $params['modified_date'];
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_create_batchId(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $batch_id);
            $stmt->getResource()->bindParam(2, $settlementDate);
            $stmt->getResource()->bindParam(3, $added_by);
            $stmt->getResource()->bindParam(4, $added_date);
            $stmt->getResource()->bindParam(5, $modified_by);
            $stmt->getResource()->bindParam(6, $modified_date);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    
    public function updateBatchId($params = array()){
        try {
            $payment_batch_id = $params['payment_batch_id'];
            $batch_id = isset($params['batch_id'])?$params['batch_id']:'';
            $settlement_date = isset($params['settlement_date'])?$params['settlement_date']:'';
            $is_locked = isset($params['is_locked'])?$params['is_locked']:'';
            $is_deleted = isset($params['is_deleted'])?$params['is_deleted']:'';
            $modified_by = $params['modified_by'];
            $modified_date = $params['modified_date'];
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_update_batchId(?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $payment_batch_id);
            $stmt->getResource()->bindParam(2, $batch_id);
            $stmt->getResource()->bindParam(3, $settlement_date);
            $stmt->getResource()->bindParam(4, $is_locked);
            $stmt->getResource()->bindParam(5, $is_deleted);
            $stmt->getResource()->bindParam(6, $modified_by);
            $stmt->getResource()->bindParam(7, $modified_date);
            $result = $stmt->execute();
        } catch (Exception $e) {
            return false;
        }
    }

}