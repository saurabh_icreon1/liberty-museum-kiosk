<?php

/**
 * This model is used for user. Transaction
 * @package    Transaction
 * @author     Icreon Tech - DG
 */

namespace Transaction\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This model is used for user. Transaction
 * @package    Transaction
 * @author     Icreon Tech - DG
 */
class DonationTable {

    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
    }
    
    /**
     * Function for to update the billing address if exists
     * @author Icreon Tech - SR
     * @return true/false
     * @param $dataArr Array
     */
    
    public function updateBillingAddress($dataArr = array())
    {
        try {
                $stmt = $this->dbAdapter->createStatement();
                $stmt->prepare("CALL usp_tra_updateUserBillingAddress(?,?,?,?,?,?,?,?,?,?,?,?,?)");
                $stmt->getResource()->bindParam(1, $dataArr['first_name']);
                $stmt->getResource()->bindParam(2, $dataArr['last_name']);
                $stmt->getResource()->bindParam(3, $dataArr['address_1']);
                $stmt->getResource()->bindParam(4, $dataArr['address_2']);
                $stmt->getResource()->bindParam(5, $dataArr['zipcode']);
                $stmt->getResource()->bindParam(6, $dataArr['city']);
                $stmt->getResource()->bindParam(7, $dataArr['state']);
                $stmt->getResource()->bindParam(8, $dataArr['country']);
                $stmt->getResource()->bindParam(9, $dataArr['address_type']);
                $stmt->getResource()->bindParam(10, $dataArr['location']);
                $stmt->getResource()->bindParam(11, $dataArr['user_id']);
                $stmt->getResource()->bindParam(12, $dataArr['address_information_id']);
                $stmt->getResource()->bindParam(13, $dataArr['modified_date']);
                $result = $stmt->execute();
                $statement = $result->getResource();
                $statement->closeCursor();
                return true;
                } catch (Exception $e) {
                return false;
           }        
        
    }
    /**
     * Function for insert user data in table for signup
     * @author Icreon Tech - SR
     * @return Int
     * @param  $dataArr Array
     */
    public function saveDonationUser($dataArr = array()) {
       // asd($dataArr);
        $curDate = DATE_TIME_FORMAT;
       
        $sourceId = 3;
        $isLoginEnabled = 1;
        $firstName = ucfirst($dataArr['first_name']);
        $lastName = ucfirst($dataArr['last_name']);
        $emailId = trim($dataArr['email_id']);
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_saveDonationUser(?,?,?,?,?,?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $firstName);
        $stmt->getResource()->bindParam(2, $lastName);
        $stmt->getResource()->bindParam(3, $emailId);
        $stmt->getResource()->bindParam(4, $curDate);
        $stmt->getResource()->bindParam(5, $curDate);        
        $stmt->getResource()->bindParam(6, $sourceId);
        $stmt->getResource()->bindParam(7, $dataArr['is_login_enabled']);
        $stmt->getResource()->bindParam(8, $dataArr['is_active']);
        $stmt->getResource()->bindParam(9, $dataArr['is_activated']);
        $stmt->getResource()->bindParam(10, $dataArr['reason_id']);
        $stmt->getResource()->bindParam(11, $dataArr['email_verified']);

        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            return false;
        }
    }
    
   /**
     * Function used to get the user profile id.
     * @author Icreon Tech - SR
     * @return array
     * @param $dataArr Array
     */
    public function getUserProfileId($dataArr = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_user_getUserProfileId(?)');
        $stmt->getResource()->bindParam(1, $dataArr['user_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    } 
    
   /**
     * Function used to get the user billing address
     * @author Icreon Tech - SR
     * @return array
     * @param $dataArr Array
     */
    public function getUserBillingAddress($dataArr = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_user_getUserBillingAddress(?)');
        $stmt->getResource()->bindParam(1, $dataArr['user_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }    
    
    /**
     * Function for to insert default commu peferences
     * 
     * 
     * @author Icreon Tech - DG
     * @return Int
     * @param Array
     */
    public function insertDefaultDonateUserCommunicationPreferences($dataArr) {
        $addDate = DATE_TIME_FORMAT;
       // $privacy = '1,3,4,5,6';
        $annotationPrivacy = 1;
        $familyHistoryPrivacy = 1;

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_insertDefaultDonateUserCommunicationPreferences(?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $dataArr['user_id']);
        $stmt->getResource()->bindParam(2, $dataArr['privacy']);
        $stmt->getResource()->bindParam(3, $annotationPrivacy);
        $stmt->getResource()->bindParam(4, $familyHistoryPrivacy);
        $stmt->getResource()->bindParam(5, $addDate);
        $stmt->getResource()->bindParam(6, $addDate);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }
    
    /**
     * Function for to insert default commu peferences
     * 
     * 
     * @author Icreon Tech - DG
     * @return Int
     * @param Array
     */
    public function updateDonateUserCommunicationPreferences($dataArr) {
        $addDate = DATE_TIME_FORMAT;
       // $privacy = '1,3,4,5,6';

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_updateDonateUserCommunicationPreferences(?,?,?)');
        $stmt->getResource()->bindParam(1, $dataArr['user_id']);
        $stmt->getResource()->bindParam(2, $dataArr['privacy']);
        $stmt->getResource()->bindParam(3, $addDate);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }    
     /**
     * This function will add  to cart donation notify
     * @param array
     * @return return last id
     * @author Icreon Tech -DT
     */
    public function addDonationNotify($transactionData = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_insertDonationNotifyAddToTra(?,?,?,?,?,?,?)");
            $stmt->getResource()->bindParam(1, $transactionData['donation_product_id']);
            $stmt->getResource()->bindParam(2, $transactionData['name']);
            $stmt->getResource()->bindParam(3, $transactionData['email_id']);
            $stmt->getResource()->bindParam(4, $transactionData['added_by']);
            $stmt->getResource()->bindParam(5, $transactionData['added_date']);
            $stmt->getResource()->bindParam(6, $transactionData['modified_by']);
            $stmt->getResource()->bindParam(7, $transactionData['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }
     /**
     * Function to update new password
     * @author Icreon Tech - DG
     * @return Int
     * @param Array
     */
    public function updateDonatePassword($userArr) {
  
        if (!empty($userArr)) {
            if (isset($userArr['user_id']) && $userArr['user_id'] != '') {
                $user_id = $userArr['user_id'];
            } else {
                $user_id = '';
            }
            $salt = $this->generateRandomString();
            if (isset($userArr['password']) && $userArr['password'] != '') {
                $password = md5($userArr['password'] . $salt);
            } else {
                $password = '';
            }
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_updateDonateUserPassword(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $user_id);
            $stmt->getResource()->bindParam(2, $password);
            $stmt->getResource()->bindParam(3, $salt);
            $stmt->getResource()->bindParam(4, $userArr['security_question_id']);
            $stmt->getResource()->bindParam(5, $userArr['security_answer']);
            $result = $stmt->execute();
            $result->getResource();
        }
    }
    /**
     * Function for get random number
     * @author Icreon Tech - DG
     * @return array
     * @param Int
     */
    public function generateRandomString() {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ23456789";
        srand((double) microtime() * 1000000);
        $number = 0;
        $code = '';
        while ($number <= 7) {
            $num = rand() % 33;
            $tmp = substr($chars, $num, 1);
            $code = $code . $tmp;
            $number++;
        }
		 $code .= mt_rand().$_SERVER['SERVER_ADDR'];
		 $code  = md5(uniqid($code, true));
         return $code;
    }

}