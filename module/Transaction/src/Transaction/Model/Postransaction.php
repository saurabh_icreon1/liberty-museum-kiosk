<?php

/**
 * This model is used for user transaction
 * @package    Transaction
 * @author     Icreon Tech - DG
 */

namespace Transaction\Model;

use Transaction\Module;
use Base\Model\BaseModel;
use Zend\Db\Adapter\Adapter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 * This model is used for user transaction
 * @package    Transaction
 * @author     Icreon Tech - DG
 */
class Postransaction extends BaseModel {

    protected $inputFilter;
    protected $adapter;

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    /**
     * This method is used to add filtering and validation to the form elements of create 
     * @param void
     * @return Array
     * @author Icreon Tech - DG
     */
    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * This method is used to return the stored procedure array for search contact in create transaction
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getContactSearchArr($dataArr = array()) {

        $returnArr = array();
        $returnArr[] = (isset($dataArr['contact_id']) && $dataArr['contact_id'] != '') ? $dataArr['contact_id'] : '';
        $returnArr[] = (isset($dataArr['name_email']) && $dataArr['name_email'] != '') ? $dataArr['name_email'] : '';
        $returnArr[] = (isset($dataArr['phone']) && $dataArr['phone'] != '') ? $dataArr['phone'] : '';
        $returnArr[] = (isset($dataArr['zip_code']) && $dataArr['zip_code'] != '') ? $dataArr['zip_code'] : '';
        $returnArr[] = (isset($dataArr['state']) && $dataArr['state'] != '') ? $dataArr['state'] : '';
        $returnArr[] = (isset($dataArr['country']) && $dataArr['country'] != '') ? $dataArr['country'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $returnArr[] = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $returnArr[] = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        $returnArr[] = (isset($dataArr['membership_level']) && $dataArr['membership_level'] != '') ? $dataArr['membership_level'] : '';
        $returnArr[] = (isset($dataArr['last_transaction']) && $dataArr['last_transaction'] != '') ? $dataArr['last_transaction'] : '';
        $returnArr[] = (isset($dataArr['date_from']) && $dataArr['date_from'] != '') ? $dataArr['date_from'] : '';
        $returnArr[] = (isset($dataArr['date_to']) && $dataArr['date_to'] != '') ? $dataArr['date_to'] : '';
        $returnArr[] = (isset($dataArr['first_name']) && $dataArr['first_name'] != '') ? $dataArr['first_name'] : '';
        $returnArr[] = (isset($dataArr['last_name']) && $dataArr['last_name'] != '') ? $dataArr['last_name'] : '';
        $returnArr[] = (isset($dataArr['company_name']) && $dataArr['company_name'] != '') ? $dataArr['company_name'] : '';
        $returnArr[] = (isset($dataArr['email_address']) && $dataArr['email_address'] != '') ? $dataArr['email_address'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for search transaction
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getTransactionSearchArr($dataArr = array()) {

        $returnArr = array();
        $returnArr[] = (isset($dataArr['first_name']) && $dataArr['first_name'] != '') ? $dataArr['first_name'] : '';
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $returnArr[] = (isset($dataArr['campaign_title_id']) && $dataArr['campaign_title_id'] != '') ? $dataArr['campaign_title_id'] : '';
        $returnArr[] = (isset($dataArr['transaction_source_id']) && $dataArr['transaction_source_id'] != '') ? $dataArr['transaction_source_id'] : '';
        $returnArr[] = (isset($dataArr['received_date_from']) && $dataArr['received_date_from'] != '') ? $dataArr['received_date_from'] : '';
        $returnArr[] = (isset($dataArr['received_date_to']) && $dataArr['received_date_to'] != '') ? $dataArr['received_date_to'] : '';
        $returnArr[] = (isset($dataArr['item_status']) && $dataArr['item_status'] != '') ? $dataArr['item_status'] : '';
        $returnArr[] = (isset($dataArr['transaction_status_id']) && $dataArr['transaction_status_id'] != '') ? $dataArr['transaction_status_id'] : '';
        $returnArr[] = (isset($dataArr['product_type_id']) && $dataArr['product_type_id'] != '') ? $dataArr['product_type_id'] : '';
        $returnArr[] = (isset($dataArr['payment_mode_id']) && $dataArr['payment_mode_id'] != '') ? $dataArr['payment_mode_id'] : '';
        $returnArr[] = (isset($dataArr['transaction_amount_from']) && $dataArr['transaction_amount_from'] != '') ? $dataArr['transaction_amount_from'] : '';
        $returnArr[] = (isset($dataArr['transaction_amount_to']) && $dataArr['transaction_amount_to'] != '') ? $dataArr['transaction_amount_to'] : '';
        $returnArr[] = (isset($dataArr['product_ids']) && $dataArr['product_ids'] != '') ? $dataArr['product_ids'] : '';
        $returnArr[] = (isset($dataArr['omx_id']) && $dataArr['omx_id'] != '') ? $dataArr['omx_id'] : '';
        $returnArr[] = (isset($dataArr['pick_up']) && $dataArr['pick_up'] != '' && $dataArr['pick_up'] != '0') ? $dataArr['pick_up'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $returnArr[] = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $returnArr[] = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        $returnArr[] = (isset($dataArr['shipping_id']) && $dataArr['shipping_id'] != '') ? $dataArr['shipping_id'] : '';
        $returnArr[] = (isset($dataArr['type']) && $dataArr['type'] != '') ? $dataArr['type'] : '';
        $returnArr[] = (isset($dataArr['batch_id']) && $dataArr['batch_id'] != '') ? $dataArr['batch_id'] : '';
        $returnArr[] = (isset($dataArr['last_name']) && $dataArr['last_name'] != '') ? $dataArr['last_name'] : '';
        $returnArr[] = (isset($dataArr['company_name']) && $dataArr['company_name'] != '') ? $dataArr['company_name'] : '';
        $returnArr[] = (isset($dataArr['email']) && $dataArr['email'] != '') ? $dataArr['email'] : '';
        $returnArr[] = (isset($dataArr['product_category']) && $dataArr['product_category'] != '') ? $dataArr['product_category'] : '';
        $returnArr[] = (isset($dataArr['transaction_invoice_number']) && $dataArr['transaction_invoice_number'] != '') ? $dataArr['transaction_invoice_number'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for create search result
     * @param code
     * @return Void
     * @author Icreon Tech - DT

     */
    public function getAddTransactionSearchArr($dataArr = array()) {

        $returnArr = array();
        $returnArr[] = (isset($dataArr['crm_user_id']) && $dataArr['crm_user_id'] != '') ? $dataArr['crm_user_id'] : '';
        $returnArr[] = (isset($dataArr['title']) && $dataArr['title'] != '') ? $dataArr['title'] : '';
        $returnArr[] = (isset($dataArr['search_query']) && $dataArr['search_query'] != '') ? $dataArr['search_query'] : '';
        $returnArr[] = (isset($dataArr['is_active']) && $dataArr['is_active'] != '') ? $dataArr['is_active'] : '';
        $returnArr[] = (isset($dataArr['is_delete']) && $dataArr['is_delete'] != '') ? $dataArr['is_delete'] : '';
        //$returnArr[] = (isset($dataArr['added_date'])) ? $dataArr['added_date'] : (isset($dataArr['modified_date']))?$dataArr['modified_date']:'';
        if (isset($dataArr['added_date'])) {
            $returnArr[] = $dataArr['added_date'];
        } else {
            $returnArr[] = $dataArr['modified_date'];
        }
        if (isset($dataArr['transaction_search_id']) && $dataArr['transaction_search_id'] != '') {
            $returnArr[] = $dataArr['transaction_search_id'];
        }
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for transaction search
     * @param code
     * @return Void
     * @author Icreon Tech - DT

     */
    public function getInputFilterTransactionSearch($filterData) {
        $inputFilterData = array();
        if (is_array($filterData)) {
            foreach ($filterData as $key => $val) {
                $inputFilterData[$key] = $val;
            }
        } else {
            $inputFilterData = trim(addslashes(strip_tags($filterData)));
        }


        return $inputFilterData;
    }

    /**
     * This method is used to return the stored procedure array for saved search
     * @param Array
     * @return Array
     * @author Icreon Tech - DT

     */
    public function getTransactionSavedSearchArr($dataArr = array()) {

        $returnArr = array();
        $returnArr[] = (isset($dataArr['crm_user_id']) && $dataArr['crm_user_id'] != '') ? $dataArr['crm_user_id'] : '';
        $returnArr[] = (isset($dataArr['transaction_search_id']) && $dataArr['transaction_search_id'] != '') ? $dataArr['transaction_search_id'] : '';
        $returnArr[] = (isset($dataArr['is_active'])) ? $dataArr['is_active'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for delete saved search
     * @param Array
     * @return Array
     * @author Icreon Tech - DT

     */
    public function getDeleteTransactionSavedSearchArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['is_deleted']) && $dataArr['is_deleted'] != '') ? $dataArr['is_deleted'] : '';
        $returnArr[] = (isset($dataArr['modified_date']) && $dataArr['modified_date'] != '') ? $dataArr['modified_date'] : '';
        $returnArr[] = (isset($dataArr['transaction_search_id']) && $dataArr['transaction_search_id'] != '') ? $dataArr['transaction_search_id'] : '';
        return $returnArr;
    }

    /**
     * Function used to get array of elements need to call remove transaction
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getArrayForRemoveTransaction($dataArr) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        return $returnArr;
    }

    /**
     * Function used to validate and filter add to cart donation elements
     * @return null
     * @param $data array
     * @author Icreon Tech - DT
     */
    public function getInputFilterAddToCartDonation() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                        'name' => 'matching_gift',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'matching_gift_front',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'company',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'company_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'in_honoree',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'honor_memory_name',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_notify',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'program_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'campaign_id_front',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'campaign',
                        'required' => false
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'campaign_id',
                        'required' => false
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'amount',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'AMOUNT_EMPTY',
                                    ),
                                ),
                            )
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'other_amount',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'AMOUNT_EMPTY',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^-?\d*(\.\d{1,2})?$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'NUMERIC_VALUE'
                                    ),
                                ),
                            ),
                        ),
                    )));


            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * This method is used to return the stored procedure array add to cart donation array
     * @param Array
     * @return Array
     * @author Icreon Tech - DT

     */
    public function getAddToCartDonationArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '';
        $returnArr[] = (isset($dataArr['user_session_id']) && $dataArr['user_session_id'] != '') ? $dataArr['user_session_id'] : '';
        $returnArr[] = (isset($dataArr['product_mapping_id']) && $dataArr['product_mapping_id'] != '') ? $dataArr['product_mapping_id'] : NULL;
        $returnArr[] = (isset($dataArr['product_type_id']) && $dataArr['product_type_id'] != '') ? $dataArr['product_type_id'] : NULL;
        $returnArr[] = (isset($dataArr['product_id']) && $dataArr['product_id'] != '') ? $dataArr['product_id'] : NULL;
        $returnArr[] = (isset($dataArr['product_attribute_option_id']) && $dataArr['product_attribute_option_id'] != '') ? $dataArr['product_attribute_option_id'] : NULL;
        $returnArr[] = (isset($dataArr['program_id']) && $dataArr['program_id'] != '') ? $dataArr['program_id'] : NULL;
        $returnArr[] = (isset($dataArr['campaign_id_front']) && $dataArr['campaign_id_front'] != '') ? $dataArr['campaign_id_front'] : NULL;
        $returnArr[] = (isset($dataArr['in_honoree']) && $dataArr['in_honoree'] != '') ? $dataArr['in_honoree'] : '';
        $returnArr[] = (isset($dataArr['honor_memory_name']) && $dataArr['honor_memory_name'] != '') ? $dataArr['honor_memory_name'] : '';
        $returnArr[] = (isset($dataArr['amount']) && $dataArr['amount'] != '') ? $dataArr['amount'] : NULL;
        $returnArr[] = (isset($dataArr['company']) && $dataArr['company'] != '') ? $dataArr['company'] : NULL;
        $returnArr[] = (isset($dataArr['matching_gift']) && $dataArr['matching_gift'] != '') ? $dataArr['matching_gift'] : '0';
        $returnArr[] = (isset($dataArr['added_by']) && $dataArr['added_by'] != '') ? $dataArr['added_by'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : '';
        $returnArr[] = (isset($dataArr['modify_by']) && $dataArr['modify_by'] != '') ? $dataArr['modify_by'] : '';
        $returnArr[] = (isset($dataArr['modify_date']) && $dataArr['modify_date'] != '') ? $dataArr['modify_date'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array add to cart product array
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getAddToCartProductArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '';
        $returnArr[] = (isset($dataArr['user_session_id']) && $dataArr['user_session_id'] != '') ? $dataArr['user_session_id'] : '';
        $returnArr[] = (isset($dataArr['product_mapping_id']) && $dataArr['product_mapping_id'] != '') ? $dataArr['product_mapping_id'] : NULL;
        $returnArr[] = (isset($dataArr['product_id']) && $dataArr['product_id'] != '') ? $dataArr['product_id'] : NULL;
        $returnArr[] = (isset($dataArr['product_type_id']) && $dataArr['product_type_id'] != '') ? $dataArr['product_type_id'] : NULL;
        $returnArr[] = (isset($dataArr['product_attribute_option_id']) && $dataArr['product_attribute_option_id'] != '') ? $dataArr['product_attribute_option_id'] : NULL;
        $returnArr[] = (isset($dataArr['item_id']) && $dataArr['item_id'] != '') ? $dataArr['item_id'] : NULL;
        $returnArr[] = (isset($dataArr['item_type']) && $dataArr['item_type'] != '') ? $dataArr['item_type'] : NULL;
        $returnArr[] = (isset($dataArr['num_quantity']) && $dataArr['num_quantity'] != '') ? $dataArr['num_quantity'] : 1;
        $returnArr[] = (isset($dataArr['item_price']) && $dataArr['item_price'] != '') ? $dataArr['item_price'] : NULL;
        $returnArr[] = (isset($dataArr['cart_date']) && $dataArr['cart_date'] != '') ? $dataArr['cart_date'] : '';
        $returnArr[] = (isset($dataArr['added_by']) && $dataArr['added_by'] != '') ? $dataArr['added_by'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : '';
        $returnArr[] = (isset($dataArr['modify_by']) && $dataArr['modify_by'] != '') ? $dataArr['modify_by'] : '';
        $returnArr[] = (isset($dataArr['modify_date']) && $dataArr['modify_date'] != '') ? $dataArr['modify_date'] : '';
        $returnArr[] = (isset($dataArr['item_name']) && $dataArr['item_name'] != '') ? $dataArr['item_name'] : '';
        $returnArr[] = (isset($dataArr['item_info']) && $dataArr['item_info'] != '') ? $dataArr['item_info'] : '';
        $returnArr[] = (isset($dataArr['manifest_info']) && $dataArr['manifest_info'] != '') ? $dataArr['manifest_info'] : '';
        $returnArr[] = (isset($dataArr['manifest_type']) && $dataArr['manifest_type'] != '') ? $dataArr['manifest_type'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array add to cart donation notify array
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getAddToCartDonationNotifyArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['donation_product_id']) && $dataArr['donation_product_id'] != '') ? $dataArr['donation_product_id'] : '';
        $returnArr[] = (isset($dataArr['name']) && $dataArr['name'] != '') ? $dataArr['name'] : '';
        $returnArr[] = (isset($dataArr['email_id']) && $dataArr['email_id'] != '') ? $dataArr['email_id'] : '';
        $returnArr[] = (isset($dataArr['added_by']) && $dataArr['added_by'] != '') ? $dataArr['added_by'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : '';
        $returnArr[] = (isset($dataArr['modified_by']) && $dataArr['modified_by'] != '') ? $dataArr['modified_by'] : '';
        $returnArr[] = (isset($dataArr['modified_date']) && $dataArr['modified_date'] != '') ? $dataArr['modified_date'] : '';
        return $returnArr;
    }

    /**
     * Function used to validate and filter add to cart donation elements
     * @return null
     * @param $data array
     * @author Icreon Tech - DT
     */
    public function getInputFilterAddToCartFOF() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                        'name' => 'upload_photo',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'image_name',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'fof_image',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'donated_by',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'people_photo',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_people_photo_visible',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_people_photo_visible',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'photo_caption',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_photo_caption_visible',
                        'required' => false,
                    )));
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * This method is used to return the stored procedure array add to cart fof
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getAddToCartFOFArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '';
        $returnArr[] = (isset($dataArr['user_session_id']) && $dataArr['user_session_id'] != '') ? $dataArr['user_session_id'] : '';
        $returnArr[] = (isset($dataArr['image_name']) && $dataArr['image_name'] != '') ? $dataArr['image_name'] : NULL;
        $returnArr[] = (isset($dataArr['fof_image']) && $dataArr['fof_image'] != '') ? $dataArr['fof_image'] : '';
        $returnArr[] = (isset($dataArr['is_photo_uploaded']) && $dataArr['is_photo_uploaded'] != '') ? $dataArr['is_photo_uploaded'] : '1';
        $returnArr[] = (isset($dataArr['donated_by']) && $dataArr['donated_by'] != '') ? $dataArr['donated_by'] : '';
        $returnArr[] = (isset($dataArr['people_photo']) && $dataArr['people_photo'] != '') ? $dataArr['people_photo'] : '';
        $returnArr[] = (isset($dataArr['is_people_photo_visible']) && $dataArr['is_people_photo_visible'] != '') ? $dataArr['is_people_photo_visible'] : '0';
        $returnArr[] = (isset($dataArr['photo_caption']) && $dataArr['photo_caption'] != '') ? $dataArr['photo_caption'] : '';
        $returnArr[] = (isset($dataArr['is_photo_caption_visible']) && $dataArr['is_photo_caption_visible'] != '') ? $dataArr['is_photo_caption_visible'] : '0';
        $returnArr[] = (isset($dataArr['is_agree']) && $dataArr['is_agree'] != '') ? $dataArr['is_agree'] : '0';
        $returnArr[] = (isset($dataArr['status']) && $dataArr['status'] != '') ? $dataArr['status'] : '0';
        $returnArr[] = (isset($dataArr['product_mapping_id']) && $dataArr['product_mapping_id'] != '') ? $dataArr['product_mapping_id'] : NULL;
        $returnArr[] = (isset($dataArr['product_id']) && $dataArr['product_id'] != '') ? $dataArr['product_id'] : NULL;
        $returnArr[] = (isset($dataArr['product_type_id']) && $dataArr['product_type_id'] != '') ? $dataArr['product_type_id'] : NULL;
        $returnArr[] = (isset($dataArr['product_attribute_option_id']) && $dataArr['product_attribute_option_id'] != '') ? $dataArr['product_attribute_option_id'] : NULL;
        $returnArr[] = (isset($dataArr['product_sku']) && $dataArr['product_sku'] != '') ? $dataArr['product_sku'] : '';
        $returnArr[] = (isset($dataArr['num_quantity']) && $dataArr['num_quantity'] != '') ? $dataArr['num_quantity'] : 1;
        $returnArr[] = (isset($dataArr['product_price']) && $dataArr['product_price'] != '') ? $dataArr['product_price'] : 0;
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : '';
        $returnArr[] = (isset($dataArr['modify_date']) && $dataArr['modify_date'] != '') ? $dataArr['modify_date'] : '';
        $returnArr[] = (isset($dataArr['added_by']) && $dataArr['added_by'] != '') ? $dataArr['added_by'] : '';
        $returnArr[] = (isset($dataArr['modify_by']) && $dataArr['modify_by'] != '') ? $dataArr['modify_by'] : '';
        $returnArr[] = (isset($dataArr['people_photo_json']) && $dataArr['people_photo_json'] != '') ? $dataArr['people_photo_json'] : '';
        return $returnArr;
    }

    /**
     * Function used to validate and filter add to cart inventory elements
     * @return null
     * @param $data array
     * @author Icreon Tech - DT
     */
    public function getInputFilterAddToCartInventory() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                        'name' => 'category_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'CATEGORY_EMPTY',
                                    ),
                                ),
                            )
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'product_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'PRODUCT_EMPTY',
                                    ),
                                ),
                            )
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'num_quantity',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'Digits',
                                'options' => array(
                                    'messages' => array(
                                        'notDigits' => 'QUANTITY_NUMERIC',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * This method is used to return the stored procedure array for search bio certificate search add to cart
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getWOHBioCertSearchArr($dataArr = array()) {

        $returnArr = array();
        $returnArr[] = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '';
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $returnArr[] = (isset($dataArr['panel_no']) && $dataArr['panel_no'] != '') ? $dataArr['panel_no'] : '';
        /* if (isset($dataArr['bio_certificate_transaction_id']) && $dataArr['bio_certificate_transaction_id'] != '') {
          $returnArr[] = $dataArr['bio_certificate_transaction_id'];
          } else if (isset($dataArr['personalize_panel_transaction_id']) && $dataArr['personalize_panel_transaction_id'] != '') {
          $returnArr[] = $dataArr['personalize_panel_transaction_id'];
          } else {
          $returnArr[] = '';
          }

          if (isset($dataArr['bio_certificate_panel_id']) && $dataArr['bio_certificate_panel_id'] != '') {
          $returnArr[] = $dataArr['bio_certificate_panel_id'];
          } else if (isset($dataArr['personalize_panel_panel_id']) && $dataArr['personalize_panel_panel_id'] != '') {
          $returnArr[] = $dataArr['personalize_panel_panel_id'];
          } else {
          $returnArr[] = '';
          } */
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $returnArr[] = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $returnArr[] = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        if (isset($dataArr['search_type']) && $dataArr['search_type'] != '') {
            if ($dataArr['search_type'] == 'bio_certificate') {
                $returnArr[] = 1;
            }
            else if($dataArr['search_type'] == 'personalize'){
                $returnArr[] = 2;
            }
            else{
                $returnArr[] = 3;
            }
        }else{
            $returnArr[] = '';
        }
        $returnArr[] = (isset($dataArr['name']) && $dataArr['name'] != '') ? addslashes($dataArr['name']) : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array add to cart woh
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getAddToCartWallOfHonorArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '';
        $returnArr[] = (isset($dataArr['user_session_id']) && $dataArr['user_session_id'] != '') ? $dataArr['user_session_id'] : '';
        $returnArr[] = (isset($dataArr['donation_for']) && $dataArr['donation_for'] != '') ? $dataArr['donation_for'] : NULL;
        $returnArr[] = (isset($dataArr['first_name_one']) && $dataArr['first_name_one'] != '') ? $dataArr['first_name_one'] : '';
        $returnArr[] = (isset($dataArr['other_name']) && $dataArr['other_name'] != '') ? $dataArr['other_name'] : '';
        $returnArr[] = (isset($dataArr['other_init_one']) && $dataArr['other_init_one'] != '') ? $dataArr['other_init_one'] : '';
        $returnArr[] = (isset($dataArr['last_name_one']) && $dataArr['last_name_one'] != '') ? $dataArr['last_name_one'] : '';
        $returnArr[] = (isset($dataArr['first_name_two']) && $dataArr['first_name_two'] != '') ? $dataArr['first_name_two'] : '';
        $returnArr[] = (isset($dataArr['other_init_two']) && $dataArr['other_init_two'] != '') ? $dataArr['other_init_two'] : '';
        $returnArr[] = (isset($dataArr['last_name_two']) && $dataArr['last_name_two'] != '') ? $dataArr['last_name_two'] : '';
        $returnArr[] = (isset($dataArr['first_line']) && $dataArr['first_line'] != '') ? $dataArr['first_line'] : '';
        $returnArr[] = (isset($dataArr['second_line']) && $dataArr['second_line'] != '') ? $dataArr['second_line'] : '';
        $returnArr[] = (isset($dataArr['origin_country']) && $dataArr['origin_country'] != '') ? $dataArr['origin_country'] : '';
        $returnArr[] = (isset($dataArr['donated_by']) && $dataArr['donated_by'] != '') ? $dataArr['donated_by'] : '';
        $returnArr[] = (isset($dataArr['num_additional_certificate']) && $dataArr['num_additional_certificate'] != '') ? $dataArr['num_additional_certificate'] : NULL;
        $returnArr[] = (isset($dataArr['is_correct']) && $dataArr['is_correct'] != '') ? $dataArr['is_correct'] : '0';
        $returnArr[] = (isset($dataArr['is_agree']) && $dataArr['is_agree'] != '') ? $dataArr['is_agree'] : '0';
        $returnArr[] = (isset($dataArr['status']) && $dataArr['status'] != '') ? $dataArr['status'] : '0';
        $returnArr[] = (isset($dataArr['product_mapping_id']) && $dataArr['product_mapping_id'] != '') ? $dataArr['product_mapping_id'] : NULL;
        $returnArr[] = (isset($dataArr['product_id']) && $dataArr['product_id'] != '') ? $dataArr['product_id'] : NULL;
        $returnArr[] = (isset($dataArr['product_type_id']) && $dataArr['product_type_id'] != '') ? $dataArr['product_type_id'] : NULL;
        $returnArr[] = (isset($dataArr['product_attribute_option_id']) && $dataArr['product_attribute_option_id'] != '') ? $dataArr['product_attribute_option_id'] : NULL;
        $returnArr[] = (isset($dataArr['product_sku']) && $dataArr['product_sku'] != '') ? $dataArr['product_sku'] : NULL;
        $returnArr[] = (isset($dataArr['num_quantity']) && $dataArr['num_quantity'] != '') ? $dataArr['num_quantity'] : 1;
        $returnArr[] = (isset($dataArr['product_price']) && $dataArr['product_price'] != '') ? $dataArr['product_price'] : 0;
        $returnArr[] = (isset($dataArr['added_by']) && $dataArr['added_by'] != '') ? $dataArr['added_by'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : '';
        $returnArr[] = (isset($dataArr['modify_by']) && $dataArr['modify_by'] != '') ? $dataArr['modify_by'] : '';
        $returnArr[] = (isset($dataArr['modify_date']) && $dataArr['modify_date'] != '') ? $dataArr['modify_date'] : '';
        $returnArr[] = (isset($dataArr['final_name']) && $dataArr['final_name'] != '') ? $dataArr['final_name'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array add to cart woh additional contact
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getAddToCartWallOfHonorAdditionalContactArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['woh_id']) && $dataArr['woh_id'] != '') ? $dataArr['woh_id'] : NULL;
        $returnArr[] = (isset($dataArr['first_name']) && $dataArr['first_name'] != '') ? $dataArr['first_name'] : '';
        $returnArr[] = (isset($dataArr['last_name']) && $dataArr['last_name'] != '') ? $dataArr['last_name'] : '';
        $returnArr[] = (isset($dataArr['email_id']) && $dataArr['email_id'] != '') ? $dataArr['email_id'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : '';
        $returnArr[] = (isset($dataArr['modify_date']) && $dataArr['modify_date'] != '') ? $dataArr['modify_date'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array add to cart bio certificate
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getAddToCartWallOfHonorBioCertificate($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : NULL;
        $returnArr[] = (isset($dataArr['user_session_id']) && $dataArr['user_session_id'] != '') ? $dataArr['user_session_id'] : '';
        $returnArr[] = (isset($dataArr['bio_name']) && $dataArr['bio_name'] != '') ? $dataArr['bio_name'] : '';
        $returnArr[] = (isset($dataArr['immigrated_from']) && $dataArr['immigrated_from'] != '') ? $dataArr['immigrated_from'] : '';
        $returnArr[] = (isset($dataArr['method_of_travel']) && $dataArr['method_of_travel'] != '') ? $dataArr['method_of_travel'] : '';
        $returnArr[] = (isset($dataArr['port_of_entry']) && $dataArr['port_of_entry'] != '') ? $dataArr['port_of_entry'] : '';
        $returnArr[] = (isset($dataArr['date_of_entry_to_us']) && $dataArr['date_of_entry_to_us'] != '') ? $dataArr['date_of_entry_to_us'] : '';
        $returnArr[] = (isset($dataArr['additional_info']) && $dataArr['additional_info'] != '') ? $dataArr['additional_info'] : '';
        $returnArr[] = (isset($dataArr['is_finalize']) && $dataArr['is_finalize'] != '') ? $dataArr['is_finalize'] : '0';
        $returnArr[] = (isset($dataArr['product_mapping_id']) && $dataArr['product_mapping_id'] != '') ? $dataArr['product_mapping_id'] : '';
        $returnArr[] = (isset($dataArr['product_id']) && $dataArr['product_id'] != '') ? $dataArr['product_id'] : '';
        $returnArr[] = (isset($dataArr['product_type_id']) && $dataArr['product_type_id'] != '') ? $dataArr['product_type_id'] : '';
        $returnArr[] = (isset($dataArr['product_attribute_option_id']) && $dataArr['product_attribute_option_id'] != '') ? $dataArr['product_attribute_option_id'] : '';
        $returnArr[] = (isset($dataArr['bio_certificate_qty']) && $dataArr['bio_certificate_qty'] != '') ? $dataArr['bio_certificate_qty'] : '';
        $returnArr[] = (isset($dataArr['product_price']) && $dataArr['product_price'] != '') ? $dataArr['product_price'] : '';
        $returnArr[] = (isset($dataArr['product_sku']) && $dataArr['product_sku'] != '') ? $dataArr['product_sku'] : '';
        $returnArr[] = (isset($dataArr['wohid']) && $dataArr['wohid'] != '') ? $dataArr['wohid'] : '';
        $returnArr[] = (isset($dataArr['added_by']) && $dataArr['added_by'] != '') ? $dataArr['added_by'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : '';
        $returnArr[] = (isset($dataArr['modify_by']) && $dataArr['modify_by'] != '') ? $dataArr['modify_by'] : '';
        $returnArr[] = (isset($dataArr['modify_date']) && $dataArr['modify_date'] != '') ? $dataArr['modify_date'] : '';
        $returnArr[] = (isset($dataArr['bio_certificate_product_id']) && $dataArr['bio_certificate_product_id'] != '') ? $dataArr['bio_certificate_product_id'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array add to cart manifest
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getAddToCartManifestArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['cart_product_id']) && $dataArr['cart_product_id'] != '') ? $dataArr['cart_product_id'] : NULL;
        $returnArr[] = (isset($dataArr['first_image']) && $dataArr['first_image'] != '') ? $dataArr['first_image'] : '';
        $returnArr[] = (isset($dataArr['first_attribute_option_id']) && $dataArr['first_attribute_option_id'] != '') ? $dataArr['first_attribute_option_id'] : '';
        $returnArr[] = (isset($dataArr['second_image']) && $dataArr['second_image'] != '') ? $dataArr['second_image'] : '';
        $returnArr[] = (isset($dataArr['second_attribute_option_id']) && $dataArr['second_attribute_option_id'] != '') ? $dataArr['second_attribute_option_id'] : '';
        $returnArr[] = (isset($dataArr['additional_images']) && $dataArr['additional_images'] != '') ? $dataArr['additional_images'] : '';
        $returnArr[] = (isset($dataArr['num_additional_images']) && $dataArr['num_additional_images'] != '') ? $dataArr['num_additional_images'] : '';
        $returnArr[] = (isset($dataArr['added_by']) && $dataArr['added_by'] != '') ? $dataArr['added_by'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : '';
        $returnArr[] = (isset($dataArr['modified_by']) && $dataArr['modified_by'] != '') ? $dataArr['modified_by'] : '';
        $returnArr[] = (isset($dataArr['modified_date']) && $dataArr['modified_date'] != '') ? $dataArr['modified_date'] : '';
        $returnArr[] = (isset($dataArr['session_id']) && $dataArr['session_id'] != '') ? $dataArr['session_id'] : '';
        $returnArr[] = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '';
        return $returnArr;
    }

    /**
     * Function used to validate and filter add to cart passenger document
     * @return null
     * @param $data array
     * @author Icreon Tech - DT
     */
    public function getInputFilterAddToCartPassengerDocument() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                        'name' => 'passenger_id',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_first_image',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_second_image',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'centerImage',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'product_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_passenger_record',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'passenger_record_options',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_ship_image',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_related_product',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'related_product_name',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'related_product_name_ids',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'passenger_record_qty',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'Digits',
                                'options' => array(
                                    'messages' => array(
                                        'notDigits' => 'QUANTITY_NUMERIC',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'ship_image_qty',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'Digits',
                                'options' => array(
                                    'messages' => array(
                                        'notDigits' => 'QUANTITY_NUMERIC',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * Function used to check variables 
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function exchangeArray($data) {
        
    }

    public function getInputFilterAddToCartDonationFront() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                        'name' => 'matching_gift',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'company',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'company_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'in_honoree',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'honor_memory_name',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_notify',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'campaign',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'program_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'campaign_id_front',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'amount',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'DONATION_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'other_amount',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'AMOUNT_EMPTY',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^-?\d*(\.\d{1,2})?$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'NUMERIC_VALUE'
                                    ),
                                ),
                            ),
                        ),
                    )));



            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * Function used to validate and filter submit transaction
     * @return null
     * @param $data array
     * @author Icreon Tech - DT
     */
    public function getInputFilterSaveTransaction() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                        'name' => 'coupon_code',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'promotion_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'billing_existing_contact',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'shipping_existing_contact',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_email_confirmation',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'payment_mode_cash',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'payment_mode_chk',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'payment_mode_credit',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'shipping_state_text',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'shipping_state_select',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'billing_state_text',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'billing_state_select',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'shipping_location_type',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'billing_location_type',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'save_shipping_address',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'save_billing_address',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'shipping_state',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'shipping_country',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'billing_state',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'user_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'Please enter user id.',
                                    ),
                                ),
                            )
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'billing_first_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'BILLING_FIRST_NAME_EMPTY',
                                    ),
                                ),
                            )
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'billing_last_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'BILLING_LAST_NAME_EMPTY',
                                    ),
                                ),
                            )
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'billing_address',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'BILLING_ADDRESS_EMPTY',
                                    ),
                                ),
                            )
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'billing_city',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'BILLING_CITY_EMPTY',
                                    ),
                                ),
                            )
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'billing_zip_code',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'BILLING_ZIP_CODE_EMPTY',
                                    ),
                                ),
                            )
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'billing_country',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'BILLING_COUNTRY_EMPTY',
                                    ),
                                ),
                            )
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'billing_phone',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'shipping_first_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'SHIPPING_FIRST_NAME_EMPTY',
                                    ),
                                ),
                            )
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'shipping_last_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'SHIPPING_LAST_NAME_EMPTY',
                                    ),
                                ),
                            )
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'shipping_address',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'SHIPPING_ADDRESS_EMPTY',
                                    ),
                                ),
                            )
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'shipping_city',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'SHIPPING_CITY_EMPTY',
                                    ),
                                ),
                            )
                        ),
                    )));
            /* $inputFilter->add($factory->createInput(array(
              'name' => 'shipping_state',
              'required' => true,
              'filters' => array(
              array('name' => 'StripTags'),
              array('name' => 'StringTrim'),
              ),
              'validators' => array(
              array(
              'name' => 'NotEmpty',
              'options' => array(
              'messages' => array(
              'isEmpty' => 'SHIPPING_STATE_EMPTY',
              ),
              ),
              )
              ),
              ))); */
            $inputFilter->add($factory->createInput(array(
                        'name' => 'shipping_zip_code',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'SHIPPING_ZIP_CODE_EMPTY',
                                    ),
                                ),
                            )
                        ),
                    )));
            /* $inputFilter->add($factory->createInput(array(
              'name' => 'shipping_country',
              'required' => true,
              'filters' => array(
              array('name' => 'StripTags'),
              array('name' => 'StringTrim'),
              ),
              'validators' => array(
              array(
              'name' => 'NotEmpty',
              'options' => array(
              'messages' => array(
              'isEmpty' => 'SHIPPING_COUNTRY_EMPTY',
              ),
              ),
              )
              ),
              ))); */
            $inputFilter->add($factory->createInput(array(
                        'name' => 'shipping_phone',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'payment_mode_cash_amount',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^-?\d*(\.\d{1,2})?$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'PHONE_DIGITS'
                                    ),
                                ),
                            )
                        ),
                    )));
            /* $inputFilter->add($factory->createInput(array(
              'name' => 'payment_mode_chk_amount[]',
              'required' => true,
              'filters' => array(
              array('name' => 'StripTags'),
              array('name' => 'StringTrim'),
              ),
              'validators' => array(
              array(
              'name' => 'Digits',
              'options' => array(
              'messages' => array(
              'notDigits' => 'PHONE_DIGITS',
              ),
              ),
              ),
              ),
              ))); */


            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * This method is used to return the stored procedure array remove product from cart
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getArrayForRemoveCartProduct($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['cartId']) && $dataArr['cartId'] != '') ? $dataArr['cartId'] : NULL;
        $returnArr[] = (isset($dataArr['productTypeId']) && $dataArr['productTypeId'] != '') ? $dataArr['productTypeId'] : '';
        $returnArr[] = (isset($dataArr['productMappingId']) && $dataArr['productMappingId'] != '') ? $dataArr['productMappingId'] : '';
        $returnArr[] = (isset($dataArr['typeOfProduct']) && $dataArr['typeOfProduct'] != '') ? $dataArr['typeOfProduct'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for final transaction
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getFinalTransactionArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : NULL;
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $returnArr[] = (isset($dataArr['transaction_source_id']) && $dataArr['transaction_source_id'] != '') ? $dataArr['transaction_source_id'] : '';
        $returnArr[] = (isset($dataArr['num_items']) && $dataArr['num_items'] != '') ? $dataArr['num_items'] : 0;
        $returnArr[] = (isset($dataArr['sub_total_amount']) && $dataArr['sub_total_amount'] != '') ? $dataArr['sub_total_amount'] : 0;
        $returnArr[] = (isset($dataArr['total_discount']) && $dataArr['total_discount'] != '') ? $dataArr['total_discount'] : 0;
        $returnArr[] = (isset($dataArr['shipping_amount']) && $dataArr['shipping_amount'] != '') ? $dataArr['shipping_amount'] : 0;
        $returnArr[] = (isset($dataArr['handling_amount']) && $dataArr['handling_amount'] != '') ? $dataArr['handling_amount'] : 0;
        $returnArr[] = (isset($dataArr['total_tax']) && $dataArr['total_tax'] != '') ? $dataArr['total_tax'] : 0;
        $returnArr[] = (isset($dataArr['transaction_amount']) && $dataArr['transaction_amount'] != '') ? $dataArr['transaction_amount'] : 0;
        $returnArr[] = (isset($dataArr['transaction_type']) && $dataArr['transaction_type'] != '') ? $dataArr['transaction_type'] : '2';
        $returnArr[] = (isset($dataArr['transaction_status_id']) && $dataArr['transaction_status_id'] != '') ? $dataArr['transaction_status_id'] : '1';
        $returnArr[] = (isset($dataArr['transaction_date']) && $dataArr['transaction_date'] != '') ? $dataArr['transaction_date'] : '';
        $returnArr[] = (isset($dataArr['shipping_type_id']) && $dataArr['shipping_type_id'] != '') ? $dataArr['shipping_type_id'] : '';
        $returnArr[] = (isset($dataArr['pick_up']) && $dataArr['pick_up'] != '') ? $dataArr['pick_up'] : '0';
        $returnArr[] = (isset($dataArr['coupon_code']) && $dataArr['coupon_code'] != '') ? $dataArr['coupon_code'] : '';
        $returnArr[] = (isset($dataArr['is_free_shipping']) && $dataArr['is_free_shipping'] != '') ? $dataArr['is_free_shipping'] : '0';
        $returnArr[] = (isset($dataArr['added_by']) && $dataArr['added_by'] != '') ? $dataArr['added_by'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : '';
        $returnArr[] = (isset($dataArr['modify_by']) && $dataArr['modify_by'] != '') ? $dataArr['modify_by'] : '';
        $returnArr[] = (isset($dataArr['modify_date']) && $dataArr['modify_date'] != '') ? $dataArr['modify_date'] : '';
        $returnArr[] = (isset($dataArr['campaign_code']) && $dataArr['campaign_code'] != '') ? $dataArr['campaign_code'] : '';
        $returnArr[] = (isset($dataArr['campaign_id']) && $dataArr['campaign_id'] != '') ? $dataArr['campaign_id'] : '';
        $returnArr[] = (isset($dataArr['channel_id']) && $dataArr['channel_id'] != '') ? $dataArr['channel_id'] : '';
        $returnArr[] = (isset($dataArr['don_amount']) && $dataArr['don_amount'] != '') ? $dataArr['don_amount'] : 0;
        $returnArr[] = (isset($dataArr['group_id']) && $dataArr['group_id'] != '') ? $dataArr['group_id'] : '';
        $returnArr[] = (isset($dataArr['total_product_revenue']) && $dataArr['total_product_revenue'] != '') ? $dataArr['total_product_revenue'] : '';
        $returnArr[] = (isset($dataArr['invoice_number']) && $dataArr['invoice_number'] != '') ? $dataArr['invoice_number'] : '';
        $returnArr[] = (isset($dataArr['balance_amount']) && $dataArr['balance_amount'] != '') ? $dataArr['balance_amount'] : '0.00';
		$returnArr[] = (isset($dataArr['is_gift_shop_transaction']) && $dataArr['is_gift_shop_transaction'] != '') ? $dataArr['is_gift_shop_transaction'] : '0';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for final transaction
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    /*public function getInsertCartToTransactionArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['cart_id']) && $dataArr['cart_id'] != '') ? $dataArr['cart_id'] : NULL;
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $returnArr[] = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '';
        $returnArr[] = (isset($dataArr['product_type_id']) && $dataArr['product_type_id'] != '') ? $dataArr['product_type_id'] : '';
        $returnArr[] = (isset($dataArr['product_mapping_id']) && $dataArr['product_mapping_id'] != '') ? $dataArr['product_mapping_id'] : '';
        $returnArr[] = (isset($dataArr['product_id']) && $dataArr['product_id'] != '') ? $dataArr['product_id'] : '';
        $returnArr[] = (isset($dataArr['product_name']) && $dataArr['product_name'] != '') ? $dataArr['product_name'] : '';
        $returnArr[] = (isset($dataArr['product_option_name']) && $dataArr['product_option_name'] != '') ? $dataArr['product_option_name'] : '';
        $returnArr[] = (isset($dataArr['product_qty']) && $dataArr['product_qty'] != '') ? $dataArr['product_qty'] : 1;
        $returnArr[] = (isset($dataArr['product_amount']) && $dataArr['product_amount'] != '') ? $dataArr['product_amount'] : 0;
        $returnArr[] = (isset($dataArr['product_subtotal']) && $dataArr['product_subtotal'] != '') ? $dataArr['product_subtotal'] : 0;
        $returnArr[] = (isset($dataArr['product_discount']) && $dataArr['product_discount'] != '') ? $dataArr['product_discount'] : 0;
        $returnArr[] = (isset($dataArr['product_tax']) && $dataArr['product_tax'] != '') ? $dataArr['product_tax'] : 0;
        $returnArr[] = (isset($dataArr['product_shipping']) && $dataArr['product_shipping'] != '') ? $dataArr['product_shipping'] : 0;
        $returnArr[] = (isset($dataArr['product_handling']) && $dataArr['product_handling'] != '') ? $dataArr['product_handling'] : 0;
        $returnArr[] = (isset($dataArr['product_total']) && $dataArr['product_total'] != '') ? $dataArr['product_total'] : 0;
        $returnArr[] = (isset($dataArr['purchase_date']) && $dataArr['purchase_date'] != '') ? $dataArr['purchase_date'] : DATE_TIME_FORMAT;
        $returnArr[] = (isset($dataArr['is_free_shipping']) && $dataArr['is_free_shipping'] != '') ? $dataArr['is_free_shipping'] : '';
        if ($dataArr['product_type_id'] != 4) {
            $returnArr[] = (isset($dataArr['is_shippable']) && $dataArr['is_shippable'] == '1') ? '1' : '10';
        } else {
            $returnArr[] = '1';
        }
        $returnArr[] = (isset($dataArr['added_by']) && $dataArr['added_by'] != '') ? $dataArr['added_by'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : DATE_TIME_FORMAT;
        $returnArr[] = (isset($dataArr['modify_by']) && $dataArr['modify_by'] != '') ? $dataArr['modify_by'] : '';
        $returnArr[] = (isset($dataArr['modify_date']) && $dataArr['modify_date'] != '') ? $dataArr['modify_date'] : DATE_TIME_FORMAT;
        $returnArr[] = (isset($dataArr['type_of_product']) && $dataArr['type_of_product'] != '') ? $dataArr['type_of_product'] : '';
        $returnArr[] = (isset($dataArr['item_name']) && $dataArr['item_name'] != '') ? $dataArr['item_name'] : '';
        $returnArr[] = (isset($dataArr['item_info']) && $dataArr['item_info'] != '') ? $dataArr['item_info'] : '';
        $returnArr[] = (isset($dataArr['manifest_info']) && $dataArr['manifest_info'] != '') ? $dataArr['manifest_info'] : '';
        $returnArr[] = (isset($dataArr['manifest_type']) && $dataArr['manifest_type'] != '') ? $dataArr['manifest_type'] : '';
        return $returnArr;
    }*/

	public function getInsertCartToTransactionArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['cart_id']) && $dataArr['cart_id'] != '') ? $dataArr['cart_id'] : NULL;
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $returnArr[] = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '';
        $returnArr[] = (isset($dataArr['product_type_id']) && $dataArr['product_type_id'] != '') ? $dataArr['product_type_id'] : '';
        $returnArr[] = (isset($dataArr['product_mapping_id']) && $dataArr['product_mapping_id'] != '') ? $dataArr['product_mapping_id'] : '';
        $returnArr[] = (isset($dataArr['product_id']) && $dataArr['product_id'] != '') ? $dataArr['product_id'] : '';
        $returnArr[] = (isset($dataArr['product_name']) && $dataArr['product_name'] != '') ? $dataArr['product_name'] : '';
        $returnArr[] = (isset($dataArr['product_option_name']) && $dataArr['product_option_name'] != '') ? $dataArr['product_option_name'] : '';
        $returnArr[] = (isset($dataArr['product_qty']) && $dataArr['product_qty'] != '') ? $dataArr['product_qty'] : 1;
        $returnArr[] = (isset($dataArr['product_amount']) && $dataArr['product_amount'] != '') ? $dataArr['product_amount']-$dataArr['membership_indi_discount']: 0.00;
		
		$product_subtotal = (isset($dataArr['product_subtotal']) && $dataArr['product_subtotal'] != '') ? $dataArr['product_subtotal']-$dataArr['membership_discount'] : 0.00;

        $returnArr[] = $product_subtotal;

		$product_discount = (isset($dataArr['product_discount']) && $dataArr['product_discount'] != '') ? $dataArr['product_discount'] : 0.00;

        $returnArr[] = $product_discount;

		$tax = (isset($dataArr['product_taxable_amount']) && $dataArr['product_taxable_amount'] != '') ? $dataArr['product_taxable_amount'] : 0.00;

        $returnArr[] = $tax;

		if(isset($dataArr['shipping_discount_in_amount']) && $dataArr['shipping_discount_in_amount']!='')
		{
			$shipping = (isset($dataArr['shipping_price']) && $dataArr['shipping_price'] != '') ? $dataArr['shipping_price']-$dataArr['shipping_discount_in_amount'] : 0.00;
		}else{
			$shipping = (isset($dataArr['shipping_price']) && $dataArr['shipping_price'] != '') ? $dataArr['shipping_price'] : 0;
		}
		$returnArr[] = $shipping;
		
		$handling = (isset($dataArr['shipping_handling_price']) && $dataArr['shipping_handling_price'] != '') ? $dataArr['shipping_handling_price']+$dataArr['surcharge_price'] : 0.00;

        $returnArr[] = $handling;

        $returnArr[] = ($product_subtotal-$product_discount)+($shipping+$handling+$tax);
        /*$returnArr[] = (isset($dataArr['product_subtotal']) && $dataArr['product_subtotal'] != '') ? $dataArr['product_subtotal']-$dataArr['membership_discount'] : 0;
        $returnArr[] = (isset($dataArr['product_discount']) && $dataArr['product_discount'] != '') ? $dataArr['product_discount'] : 0;
        $returnArr[] = (isset($dataArr['product_taxable_amount']) && $dataArr['product_taxable_amount'] != '') ? $dataArr['product_taxable_amount'] : 0;
		if(isset($dataArr['shipping_discount_in_amount']) && $dataArr['shipping_discount_in_amount']!='')
		{
        $returnArr[] = (isset($dataArr['shipping_price']) && $dataArr['shipping_price'] != '') ? $dataArr['shipping_price']-$dataArr['shipping_discount_in_amount'] : 0;
		}else{
		$returnArr[] = (isset($dataArr['shipping_price']) && $dataArr['shipping_price'] != '') ? $dataArr['shipping_price'] : 0;
		}
        $returnArr[] = (isset($dataArr['shipping_handling_price']) && $dataArr['shipping_handling_price'] != '') ? $dataArr['shipping_handling_price']+$dataArr['surcharge_price'] : 0;
        $returnArr[] = (isset($dataArr['product_total']) && $dataArr['product_total'] != '') ? $dataArr['product_total'] : 0;*/
        $returnArr[] = (isset($dataArr['purchase_date']) && $dataArr['purchase_date'] != '') ? $dataArr['purchase_date'] : DATE_TIME_FORMAT;
        $returnArr[] = (isset($dataArr['is_free_shipping']) && $dataArr['is_free_shipping'] != '') ? $dataArr['is_free_shipping'] : '';
        if ($dataArr['product_type_id'] != 4) {
            if($dataArr['pick_up'] == 1)
                $returnArr[] = '4'; // Release status
            else
	            $returnArr[] = (isset($dataArr['is_shippable']) && $dataArr['is_shippable'] == '1') ? '1' : '10';
        } else {
            if($dataArr['pick_up'] == 1)
                $returnArr[] = '4'; // Release status
            else
                $returnArr[] = '1';
        }
        $returnArr[] = (isset($dataArr['added_by']) && $dataArr['added_by'] != '') ? $dataArr['added_by'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : DATE_TIME_FORMAT;
        $returnArr[] = (isset($dataArr['modify_by']) && $dataArr['modify_by'] != '') ? $dataArr['modify_by'] : '';
        $returnArr[] = (isset($dataArr['modify_date']) && $dataArr['modify_date'] != '') ? $dataArr['modify_date'] : DATE_TIME_FORMAT;
        $returnArr[] = (isset($dataArr['type_of_product']) && $dataArr['type_of_product'] != '') ? $dataArr['type_of_product'] : '';
        $returnArr[] = (isset($dataArr['item_name']) && $dataArr['item_name'] != '') ? $dataArr['item_name'] : '';
        $returnArr[] = (isset($dataArr['item_info']) && $dataArr['item_info'] != '') ? $dataArr['item_info'] : '';
        $returnArr[] = (isset($dataArr['manifest_info']) && $dataArr['manifest_info'] != '') ? $dataArr['manifest_info'] : '';
        $returnArr[] = (isset($dataArr['manifest_type']) && $dataArr['manifest_type'] != '') ? $dataArr['manifest_type'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for final transaction billing shipping address info
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getTransactionBillingShipping($dataArr = array()) {
        $dataArr['source_type'] = !empty($dataArr['source_type']) ? $dataArr['source_type'] : '';
        $returnArr = array();
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $returnArr[] = (isset($dataArr['billing_title']) && $dataArr['billing_title'] != '') ? $dataArr['billing_title'] : '';
        $returnArr[] = (isset($dataArr['billing_first_name']) && $dataArr['billing_first_name'] != '') ? $dataArr['billing_first_name'] : '';
        $returnArr[] = (isset($dataArr['billing_middle_name']) && $dataArr['billing_middle_name'] != '') ? $dataArr['billing_middle_name'] : '';
        $returnArr[] = (isset($dataArr['billing_last_name']) && $dataArr['billing_last_name'] != '') ? $dataArr['billing_last_name'] : '';
        $returnArr[] = (isset($dataArr['billing_company']) && $dataArr['billing_company'] != '') ? $dataArr['billing_company'] : '';
        if ($dataArr['source_type'] == 'frontend') {
            $address1 = !empty($dataArr['billing_address_1']) ? $dataArr['billing_address_1'] : '';
            $address2 = !empty($dataArr['billing_address_2']) ? "," . $dataArr['billing_address_2'] : '';

            $returnArr[] = $address1 . ' ' . $address2;
        } else {
            $returnArr[] = (isset($dataArr['billing_address']) && $dataArr['billing_address'] != '') ? $dataArr['billing_address'] : '';
        }
        $returnArr[] = (isset($dataArr['billing_city']) && $dataArr['billing_city'] != '') ? $dataArr['billing_city'] : '';
        $returnArr[] = (isset($dataArr['billing_state']) && $dataArr['billing_state'] != '') ? $dataArr['billing_state'] : '';
        $returnArr[] = (isset($dataArr['billing_zip_code']) && $dataArr['billing_zip_code'] != '') ? $dataArr['billing_zip_code'] : '';
        $returnArr[] = (isset($dataArr['billing_country']) && $dataArr['billing_country'] != '') ? $dataArr['billing_country'] : '';
        $returnArr[] = (isset($dataArr['billing_phone']) && $dataArr['billing_phone'] != '') ? $dataArr['billing_phone'] : '';
        $returnArr[] = (isset($dataArr['is_apo_po']) && $dataArr['is_apo_po'] != '') ? $dataArr['is_apo_po'] : '';
        $returnArr[] = (isset($dataArr['shipping_title']) && $dataArr['shipping_title'] != '') ? $dataArr['shipping_title'] : '';
        $returnArr[] = (isset($dataArr['shipping_first_name']) && $dataArr['shipping_first_name'] != '') ? $dataArr['shipping_first_name'] : '';
        $returnArr[] = (isset($dataArr['shipping_middle_name']) && $dataArr['shipping_middle_name'] != '') ? $dataArr['shipping_middle_name'] : '';
        $returnArr[] = (isset($dataArr['shipping_last_name']) && $dataArr['shipping_last_name'] != '') ? $dataArr['shipping_last_name'] : '';
        $returnArr[] = (isset($dataArr['shipping_company']) && $dataArr['shipping_company'] != '') ? $dataArr['shipping_company'] : '';
        if ($dataArr['source_type'] == 'frontend') {
            $address1ship = !empty($dataArr['shipping_address_1']) ? $dataArr['shipping_address_1'] : '';
            $address2ship = !empty($dataArr['shipping_address_2']) ? "," . $dataArr['shipping_address_2'] : '';
            $returnArr[] = $address1ship . ' ' . $address2ship;
        } else {
            $returnArr[] = (isset($dataArr['shipping_address']) && $dataArr['shipping_address'] != '') ? $dataArr['shipping_address'] : '';
        }
        $returnArr[] = (isset($dataArr['shipping_city']) && $dataArr['shipping_city'] != '') ? $dataArr['shipping_city'] : '';
        $returnArr[] = (isset($dataArr['shipping_state']) && $dataArr['shipping_state'] != '') ? $dataArr['shipping_state'] : '';
        $returnArr[] = (isset($dataArr['shipping_zip_code']) && $dataArr['shipping_zip_code'] != '') ? $dataArr['shipping_zip_code'] : '';
        $returnArr[] = (isset($dataArr['shipping_country']) && $dataArr['shipping_country'] != '') ? $dataArr['shipping_country'] : '';
        $returnArr[] = (isset($dataArr['shipping_phone']) && $dataArr['shipping_phone'] != '') ? $dataArr['shipping_phone'] : '';
        $returnArr[] = (isset($dataArr['added_by']) && $dataArr['added_by'] != '') ? $dataArr['added_by'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : DATE_TIME_FORMAT;
        $returnArr[] = (isset($dataArr['modify_by']) && $dataArr['modify_by'] != '') ? $dataArr['modify_by'] : '';
        $returnArr[] = (isset($dataArr['modify_date']) && $dataArr['modify_date'] != '') ? $dataArr['modify_date'] : DATE_TIME_FORMAT;
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for final transaction payment info
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getTransactionPaymentArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $returnArr[] = (isset($dataArr['payment_mode_id']) && $dataArr['payment_mode_id'] != '') ? $dataArr['payment_mode_id'] : '';
        $returnArr[] = (isset($dataArr['amount']) && $dataArr['amount'] != '') ? $dataArr['amount'] : '';
        $returnArr[] = (isset($dataArr['cheque_type_id']) && $dataArr['cheque_type_id'] != '') ? $dataArr['cheque_type_id'] : '';
        $returnArr[] = (isset($dataArr['cheque_number']) && $dataArr['cheque_number'] != '') ? $dataArr['cheque_number'] : '';
        $returnArr[] = (isset($dataArr['id_type_id']) && $dataArr['id_type_id'] != '') ? $dataArr['id_type_id'] : '';
        $returnArr[] = (isset($dataArr['id_number']) && $dataArr['id_number'] != '') ? $dataArr['id_number'] : '';
        $returnArr[] = (isset($dataArr['profile_id']) && $dataArr['profile_id'] != '') ? $dataArr['profile_id'] : '';
        $returnArr[] = (isset($dataArr['payment_id']) && $dataArr['payment_id'] != '') ? $dataArr['payment_id'] : '';
        $returnArr[] = (isset($dataArr['billing_id']) && $dataArr['billing_id'] != '') ? $dataArr['billing_id'] : '';
        $returnArr[] = (isset($dataArr['address']) && $dataArr['address'] != '') ? $dataArr['address'] : '';
        $returnArr[] = (isset($dataArr['city']) && $dataArr['city'] != '') ? $dataArr['city'] : '';
        $returnArr[] = (isset($dataArr['state']) && $dataArr['state'] != '') ? $dataArr['state'] : '';
        $returnArr[] = (isset($dataArr['country_id']) && $dataArr['country_id'] != '') ? $dataArr['country_id'] : '';
        $returnArr[] = (isset($dataArr['postal_code']) && $dataArr['postal_code'] != '') ? $dataArr['postal_code'] : '';
        $returnArr[] = (isset($dataArr['authorize_transaction_id']) && $dataArr['authorize_transaction_id'] != '') ? $dataArr['authorize_transaction_id'] : '';
        $returnArr[] = (isset($dataArr['cheque_date']) && $dataArr['cheque_date'] != '') ? $dataArr['cheque_date'] : '';
        $returnArr[] = (isset($dataArr['account_no']) && $dataArr['account_no'] != '') ? $dataArr['account_no'] : '';
        $returnArr[] = (isset($dataArr['added_by']) && $dataArr['added_by'] != '') ? $dataArr['added_by'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : DATE_TIME_FORMAT;
        $returnArr[] = (isset($dataArr['modify_by']) && $dataArr['modify_by'] != '') ? $dataArr['modify_by'] : '';
        $returnArr[] = (isset($dataArr['modify_date']) && $dataArr['modify_date'] != '') ? $dataArr['modify_date'] : DATE_TIME_FORMAT;
		$returnArr[] = (isset($dataArr['credit_card_type_id']) && $dataArr['credit_card_type_id'] != '') ? $dataArr['credit_card_type_id'] : '';
        $returnArr[] = (isset($dataArr['payment_profile_id']) && $dataArr['payment_profile_id'] != '') ? $dataArr['payment_profile_id'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for final transaction note info
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getTransactionNoteArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $returnArr[] = (isset($dataArr['note']) && $dataArr['note'] != '') ? $dataArr['note'] : '';
        $returnArr[] = (isset($dataArr['is_private']) && $dataArr['is_private'] != '') ? $dataArr['is_private'] : '';
        $returnArr[] = (isset($dataArr['added_by']) && $dataArr['added_by'] != '') ? $dataArr['added_by'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : DATE_TIME_FORMAT;
        $returnArr[] = (isset($dataArr['modify_date']) && $dataArr['modify_date'] != '') ? $dataArr['modify_date'] : DATE_TIME_FORMAT;
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for update user memberhsip
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getMatchingMembershipArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['minimun_donation_amount']) && $dataArr['minimun_donation_amount'] != '') ? $dataArr['minimun_donation_amount'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for update user membership
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getUpdateUserMembershipArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '';
        $returnArr[] = (isset($dataArr['membership_id']) && $dataArr['membership_id'] != '') ? $dataArr['membership_id'] : '';
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $returnArr[] = (isset($dataArr['transaction_date']) && $dataArr['transaction_date'] != '') ? $dataArr['transaction_date'] : '';
        $returnArr[] = (isset($dataArr['membership_type']) && $dataArr['membership_type'] != '') ? $dataArr['membership_type'] : '';
        $returnArr[] = (isset($dataArr['transaction_amount']) && $dataArr['transaction_amount'] != '') ? $dataArr['transaction_amount'] : '';
        $returnArr[] = (isset($dataArr['membership_date_from']) && $dataArr['membership_date_from'] != '') ? $dataArr['membership_date_from'] : '';
        $returnArr[] = (isset($dataArr['membership_date_to']) && $dataArr['membership_date_to'] != '') ? $dataArr['membership_date_to'] : '';
        $returnArr[] = (isset($dataArr['membership_shipped']) && $dataArr['membership_shipped'] != '') ? $dataArr['membership_shipped'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for update user membership
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getTotalUserPurchaseProductAmountArr($dataArr = array()) {
        $returnArr = array();
        $returnArr['user_id'] = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '';
        $returnArr['from_date'] = (isset($dataArr['from_date']) && $dataArr['from_date'] != '') ? $dataArr['from_date'] : '';
        $returnArr['to_date'] = (isset($dataArr['to_date']) && $dataArr['to_date'] != '') ? $dataArr['to_date'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array to save bulk donations
     * @param Array
     * @return Array
     * @author Icreon Tech - SK
     */
    public function getDonationProductArray($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['honoree_type']) && $dataArr['honoree_type'] != '') ? $dataArr['honoree_type'] : '';
        $returnArr[] = (isset($dataArr['honoree_name']) && $dataArr['honoree_name'] != '') ? $dataArr['honoree_name'] : '';
        $returnArr[] = (isset($dataArr['campaign_id']) && $dataArr['campaign_id'] != '') ? $dataArr['campaign_id'] : '';
        $returnArr[] = (isset($dataArr['pledge_transaction_id']) && $dataArr['pledge_transaction_id'] != '') ? $dataArr['pledge_transaction_id'] : '';
        $returnArr[] = (isset($dataArr['is_pledge_transaction']) && $dataArr['is_pledge_transaction'] != '') ? $dataArr['is_pledge_transaction'] : '0';
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $returnArr[] = (isset($dataArr['batch_group_id']) && $dataArr['batch_group_id'] != '') ? $dataArr['batch_group_id'] : '';
        $returnArr[] = (isset($dataArr['product_mapping_id']) && $dataArr['product_mapping_id'] != '') ? $dataArr['product_mapping_id'] : '';
        $returnArr[] = (isset($dataArr['product_type_id']) && $dataArr['product_type_id'] != '') ? $dataArr['product_type_id'] : '';
        $returnArr[] = (isset($dataArr['product_id']) && $dataArr['product_id'] != '') ? $dataArr['product_id'] : '';
        $returnArr[] = (isset($dataArr['product_name']) && $dataArr['product_name'] != '') ? $dataArr['product_name'] : '';
        $returnArr[] = (isset($dataArr['product_attribute_option_id']) && $dataArr['product_attribute_option_id'] != '') ? $dataArr['product_attribute_option_id'] : '';
        $returnArr[] = (isset($dataArr['product_sku']) && $dataArr['product_sku'] != '') ? $dataArr['product_sku'] : '';
        $returnArr[] = (isset($dataArr['product_price']) && $dataArr['product_price'] != '') ? $dataArr['product_price'] : '0';
        $returnArr[] = (isset($dataArr['product_sub_total']) && $dataArr['product_sub_total'] != '') ? $dataArr['product_sub_total'] : '0';
        $returnArr[] = (isset($dataArr['product_discount']) && $dataArr['product_discount'] != '') ? $dataArr['product_discount'] : '0';
        $returnArr[] = (isset($dataArr['product_tax']) && $dataArr['product_tax'] != '') ? $dataArr['product_tax'] : '0';
        $returnArr[] = (isset($dataArr['product_total']) && $dataArr['product_total'] != '') ? $dataArr['product_total'] : '';
        $returnArr[] = (isset($dataArr['product_status_id']) && $dataArr['product_status_id'] != '') ? $dataArr['product_status_id'] : '';
        $returnArr[] = (isset($dataArr['purchase_date']) && $dataArr['purchase_date'] != '') ? $dataArr['purchase_date'] : '';
        $returnArr[] = (isset($dataArr['is_deleted']) && $dataArr['is_deleted'] != '') ? $dataArr['is_deleted'] : '0';
        $returnArr[] = (isset($dataArr['added_by']) && $dataArr['added_by'] != '') ? $dataArr['added_by'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : '';
        $returnArr[] = (isset($dataArr['modified_date']) && $dataArr['modified_date'] != '') ? $dataArr['modified_date'] : '';
        $returnArr[] = (isset($dataArr['modified_by']) && $dataArr['modified_by'] != '') ? $dataArr['modified_by'] : '';
		$returnArr[] = (isset($dataArr['company_id']) && $dataArr['company_id'] != '') ? $dataArr['company_id'] : '';
		$returnArr[] = (isset($dataArr['company_name']) && $dataArr['company_name'] != '') ? $dataArr['company_name'] : '';
		$returnArr[] = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '';
		$returnArr[] = (isset($dataArr['num_quantity']) && $dataArr['num_quantity'] != '') ? $dataArr['num_quantity'] : '';
		return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array to search bulk donations entries
     * @param Array
     * @return Array
     * @author Icreon Tech - SK
     */
    public function getBulkDonationEntriesArray($dataArr = array()) {
        $returnArr = array();

        $returnArr[] = (isset($dataArr['payment_batch_id']) && $dataArr['payment_batch_id'] != '') ? $dataArr['payment_batch_id'] : '';
        $returnArr[] = (isset($dataArr['amount_from']) && $dataArr['amount_from'] != '') ? $dataArr['amount_from'] : '';
        $returnArr[] = (isset($dataArr['amount_to']) && $dataArr['amount_to'] != '') ? $dataArr['amount_to'] : '';
        $returnArr[] = (isset($dataArr['batch_date_from']) && $dataArr['batch_date_from'] != '') ? $dataArr['batch_date_from'] : '';
        $returnArr[] = (isset($dataArr['batch_date_to']) && $dataArr['batch_date_to'] != '') ? $dataArr['batch_date_to'] : '';
        $returnArr[] = (isset($dataArr['added_by']) && $dataArr['added_by'] != '') ? $dataArr['added_by'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $returnArr[] = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $returnArr[] = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';

        return $returnArr;
    }

    /**
     * Function used to validate and filter make authorize payment
     * @return null
     * @param $data array
     * @author Icreon Tech - DT
     */
    public function getInputFilterMakeAuthorizePayment() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'billing_state_auth',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'billing_state_auth_select',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'card_holder_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'CARD_HOLDER_NAME_EMPTY',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[a-zA-Z \',.-]+$/i',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'ALPHABET_ONLY'
                                    ),
                                ),
                            )
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'card_number',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'CARD_NUMBER_EMPTY',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'Digits',
                                'options' => array(
                                    'messages' => array(
                                        'notDigits' => 'CARD_NUMBER_DIGIT',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'expiry_month',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EXPIRY_MONTH_EMPTY',
                                    ),
                                ),
                            )
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'expiry_year',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EXPIRY_YEAR_EMPTY',
                                    ),
                                ),
                            )
                        ),
                    )));
            /* $inputFilter->add($factory->createInput(array(
                        'name' => 'cvv_number',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'CVV_EMPTY',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'Digits',
                                'options' => array(
                                    'messages' => array(
                                        'notDigits' => 'CVV_VALID',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]{3}+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'CVV_VALID'
                                    ),
                                ),
                            )
                        ),
                    ))); */
            $inputFilter->add($factory->createInput(array(
                        'name' => 'amount',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'AMOUNT_EMPTY',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^-?\d*(\.\d{1,2})?$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'NUMERIC_VALUE'
                                    ),
                                ),
                            )
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'billing_address_1',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'BILLING_ADDRESS_EMPTY',
                                    ),
                                ),
                            ) /*,
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[A-Za-z0-9\n\r ,.-]+$/i',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'ALPHA_NUMERIC_SPECIALSYMPOL'
                                    ),
                                ),
                            )*/
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'billing_city_auth',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'BILLING_CITY_EMPTY',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[A-Za-z0-9\n\r ,.-]+$/i',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'ALPHABET_ONLY'
                                    ),
                                ),
                            )
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'billing_zip_code_auth',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'BILLING_ZIP_CODE_EMPTY',
                                    ),
                                ),
                            )
                        ),
                    )));


            $inputFilter->add($factory->createInput(array(
                        'name' => 'billing_country_auth',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'BILLING_COUNTRY_EMPTY',
                                    ),
                                ),
                            )
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'existing_profile_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'CREDIT_CART_EXIST_EMPTY',
                                    ),
                                ),
                            )
                        ),
                    )));


            $inputFilter->add($factory->createInput(array(
                        'name' => 'exist_amount',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'AMOUNT_EMPTY',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^-?\d*(\.\d{1,2})?$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'NUMERIC_VALUE'
                                    ),
                                ),
                            )
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'pledge_transaction_id',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));



            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * Function used to validate and filter submit batch donations
     * @return null
     * @param $data array
     * @author Icreon Tech - SK
     */
    public function getInputFilterSaveDonationBatches() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                        'name' => 'contact_name[]',
                        'required' => false,
                            )
                    ));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'contact_name_id[]',
                        'required' => false,
                            )
                    ));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'campaign[]',
                        'required' => false,
                            )
                    ));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'campaign_id[]',
                        'required' => false,
                            )
                    ));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'payment_mode_id[]',
                        'required' => false,
                            )
                    ));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'amount[]',
                        'required' => false
                    )));


            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * This method is used to return the stored procedure array for sage
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getSageSearchArr($dataArr = array()) {

        $returnArr = array();
        $returnArr[] = (isset($dataArr['batch_no']) && $dataArr['batch_no'] != '' && $dataArr['batch_no'] != '0') ? $dataArr['batch_no'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $returnArr[] = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $returnArr[] = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for shippment id
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getShippmentIdArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['transaction_shipment_id']) && $dataArr['transaction_shipment_id'] != '') ? $dataArr['transaction_shipment_id'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for sage
     * @param array
     * @return array
     * @author Icreon Tech - KK

     */
    public function getSageBatchSearchArr($dataArr = array()) {

        $returnArr = array();
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $returnArr[] = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $returnArr[] = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        if (!empty($dataArr['transaction_date'])) {

            if (!empty($dataArr['transaction_time'])) {
                $returnArr[] = date('Y-m-d H:i:s', strtotime($dataArr['transaction_date'] . " " . $dataArr['transaction_time']));
            } else {
                $returnArr[] = date('Y-m-d H:i:s', strtotime($dataArr['transaction_date']));
            }
        } else {
            $returnArr[] = '';
        }
        $returnArr[] = !empty($dataArr['sage_batch_no']) ? $dataArr['sage_batch_no'] : NULL;
        $returnArr[] = !empty($dataArr['update']) ? $dataArr['update'] : NULL;
        return $returnArr;
    }

    /**
     * This method is used to check whether free shipping or not
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function checkFreeShippingArr($dataArr = array()) {

        $returnArr = array();
        $returnArr[] = (isset($dataArr['promotion_id']) && $dataArr['promotion_id'] != '') ? $dataArr['promotion_id'] : '';
        return $returnArr;
    }

    /**
     * This method is used to check holiday date
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getHolidayArr($dataArr = array()) {

        $returnArr = array();
        $returnArr[] = (isset($dataArr['holiday_date']) && $dataArr['holiday_date'] != '') ? $dataArr['holiday_date'] : '';
        return $returnArr;
    }

    /**
     * This method is used to get shipping method 
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getShippingMethodArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['pb_shipping_id']) && $dataArr['pb_shipping_id'] != '') ? $dataArr['pb_shipping_id'] : '1';
        $returnArr[] = (isset($dataArr['pb_shipping_type_id']) && $dataArr['pb_shipping_type_id'] != '') ? $dataArr['pb_shipping_type_id'] : '';
        $returnArr[] = (isset($dataArr['carrier_id']) && $dataArr['carrier_id'] != '') ? $dataArr['carrier_id'] : '';
        $returnArr[] = (isset($dataArr['service_type']) && $dataArr['service_type'] != '') ? $dataArr['service_type'] : '';
        return $returnArr;
    }

    /**
     * This method is used to get update shippment type array
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getUpdateShipmentTypeArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['transaction_shipment_id']) && $dataArr['transaction_shipment_id'] != '') ? $dataArr['transaction_shipment_id'] : '';
        $returnArr[] = (isset($dataArr['shipping_type_id']) && $dataArr['shipping_type_id'] != '') ? $dataArr['shipping_type_id'] : '';
        $returnArr[] = (isset($dataArr['modified_by']) && $dataArr['modified_by'] != '') ? $dataArr['modified_by'] : '';
        $returnArr[] = (isset($dataArr['modified_date']) && $dataArr['modified_date'] != '') ? $dataArr['modified_date'] : '';
        return $returnArr;
    }

    /**
     * This method is used to get credit transaction payment array
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getCreditTransactionPaymentArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for receive payment by credit array
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getTransactionPaymentReceiveArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['transaction_payment_id']) && $dataArr['transaction_payment_id'] != '') ? $dataArr['transaction_payment_id'] : '';
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $returnArr[] = (isset($dataArr['amount']) && $dataArr['amount'] != '') ? $dataArr['amount'] : 0;
        $returnArr[] = (isset($dataArr['id']) && $dataArr['id'] != '') ? $dataArr['id'] : '';
        $returnArr[] = (isset($dataArr['authorize_transaction_id']) && $dataArr['authorize_transaction_id'] != '') ? $dataArr['authorize_transaction_id'] : '';
        $returnArr[] = (isset($dataArr['added_by']) && $dataArr['added_by'] != '') ? $dataArr['added_by'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : DATE_TIME_FORMAT;
        $returnArr[] = (isset($dataArr['modify_by']) && $dataArr['modify_by'] != '') ? $dataArr['modify_by'] : '';
        $returnArr[] = (isset($dataArr['modify_date']) && $dataArr['modify_date'] != '') ? $dataArr['modify_date'] : DATE_TIME_FORMAT;
        return $returnArr;
    }

    /**
     * This method is used to get shippment amount array
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getShippmentAmountArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['shipment_id']) && $dataArr['shipment_id'] != '') ? $dataArr['shipment_id'] : '';
        return $returnArr;
    }

    /**
     * This method is used to save coupon used log
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getCouponUsedArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '';
        $returnArr[] = (isset($dataArr['promotion_id']) && $dataArr['promotion_id'] != '') ? $dataArr['promotion_id'] : '';
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : '';
        return $returnArr;
    }
    /**
     * This method is used to get update actual shipping price
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getUpdateActualShippingPriceArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['transaction_shipment_id']) && $dataArr['transaction_shipment_id'] != '') ? $dataArr['transaction_shipment_id'] : '';
        $returnArr[] = (isset($dataArr['actual_shipping_price']) && $dataArr['actual_shipping_price'] != '') ? $dataArr['actual_shipping_price'] : '';
        $returnArr[] = (isset($dataArr['carrier_id']) && $dataArr['carrier_id'] != '') ? $dataArr['carrier_id'] : '';
        $returnArr[] = (isset($dataArr['service_type']) && $dataArr['service_type'] != '') ? $dataArr['service_type'] : '';
        $returnArr[] = (isset($dataArr['modified_by']) && $dataArr['modified_by'] != '') ? $dataArr['modified_by'] : '';
        $returnArr[] = (isset($dataArr['modified_date']) && $dataArr['modified_date'] != '') ? $dataArr['modified_date'] : '';
        return $returnArr;
    }
    /**
     * This method is used to get update actual shipping price of products in transaction
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getUpdateProductActualShippingArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['product_type']) && $dataArr['product_type'] != '') ? $dataArr['product_type'] : '';
        $returnArr[] = (isset($dataArr['transaction_product_id']) && $dataArr['transaction_product_id'] != '') ? $dataArr['transaction_product_id'] : '';
        $returnArr[] = (isset($dataArr['actual_product_shipping']) && $dataArr['actual_product_shipping'] != '') ? $dataArr['actual_product_shipping'] : '';
        return $returnArr;
    }
	 /**
     * This method is used to prepare data array for saving soft credit data
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getUserSoftCreditArr($dataArr = array()) {
        $returnArr = array();
        $returnArr['transaction_id'] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $returnArr['provider_user_id'] = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '';
        $returnArr['amount'] = (isset($dataArr['transaction_amount']) && $dataArr['transaction_amount'] != '') ? $dataArr['transaction_amount'] : '';
        return $returnArr;
    }

   /**
     * Function used to check variable for user 
     * @param Array
     * @return array
     * @author Icreon Tech - AP
     */
    public function filterParam($data) {
        $parameter = array();

        $parameter['no_email'] = (isset($data['email_id']) && !empty($data['email_id'])) ? '0' : '1';
        $parameter['login_enable'] = (isset($data['login_enable'])) ? $data['login_enable'] : '0';
        $parameter['contact_type'] = (isset($data['contact_type'])) ? $data['contact_type'] : null;
        $parameter['salt'] = $this->generateRandomString();
        $parameter['verify_code'] = $this->generateRandomString();
        $password = $this->generateRandomString();
        $parameter['password'] = md5($password . $parameter['salt']);
        $parameter['genrated_password'] = $password; 
        $parameter['source_id'] = (isset($data['source_id'])) ? $data['source_id'] : null; 
        $parameter['add_date'] = $data['add_date'];
        $parameter['modified_date'] = $data['modified_date'];        
        return $parameter;
    } 
    
    /**
     * Function for get random number
     * @author Icreon Tech - DG
     * @return array
     * @param Int
     */
    public function generateRandomString() {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ23456789";
        srand((double) microtime() * 1000000);
        $i = 0;
        $code = '';

        while ($i <= 7) {
            $num = rand() % 33;
            $tmp = substr($chars, $num, 1);
            $code = $code . $tmp;
            $i++;
        }
        return $code;
    }    
    
}