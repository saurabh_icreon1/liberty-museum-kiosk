<?php
/**
 * This page is used for Report module autoloading classes before module intilize.
 * @package    Report
 * @author     Icreon Tech - DT
 */
return array(
    'Report\Module'                       => __DIR__ . '/Module.php',
    'Report\Controller\ReportController'   => __DIR__ . '/src/Report/Controller/ReportController.php',
    'Report\Model\Report'            => __DIR__ . '/src/Report/Model/Report.php',
    'Report\Model\ReportTable'                   => __DIR__ . '/src/Report/Model/ReportTable.php'
);
?>