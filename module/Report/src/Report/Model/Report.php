<?php

/**
 * This model is used for report module validation.
 * @package    Report_Report
 * @author     Icreon Tech - DT
 */

namespace Report\Model;

use Report\Module;
// Add these import statements
use Base\Model\BaseModel;
use Zend\Db\Adapter\Adapter;
// Add these import statements
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 * This class is used for report module validation.
 * @package    Report_Report
 * @author     Icreon Tech - DT
 */
class Report extends BaseModel {

    protected $inputFilter;
    protected $adapter;

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    /**
     * This method is used to return the stored procedure array for search reports
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getReportSearchArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '';
        $returnArr[] = (isset($dataArr['report_category_id']) && $dataArr['report_category_id'] != '') ? $dataArr['report_category_id'] : '';
        $returnArr[] = (isset($dataArr['is_main_report']) && $dataArr['is_main_report'] != '') ? $dataArr['is_main_report'] : '1';
        $returnArr[] = (isset($dataArr['mst_report_id']) && $dataArr['mst_report_id'] != '') ? $dataArr['mst_report_id'] : '';
        $returnArr[] = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $returnArr[] = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for get display columns
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getReportDisplayColumnArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for get order by columns for report
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getReportOrderyByColumnArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for get display group by fields
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getReportGroupByArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';

        $returnArr[] = (isset($dataArr['column_id']) && $dataArr['column_id'] != '') ? $dataArr['column_id'] : '';

        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for get filter value columns for report
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getReportFilterValueColumnArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for get order by columns for report
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getConditionsForGroupArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['filter_group_id']) && $dataArr['filter_group_id'] != '') ? $dataArr['filter_group_id'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for get order by columns for report
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getReportInfoArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for get order by columns for report
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getReportInfoSybuntArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for get order by columns for report
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getReportInfoLybuntArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for get order by columns for report
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getReportMembershipSummaryArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for get order by columns for report
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getReportRepeatTransactionArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for create report
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getCreateReportArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['mst_report_id']) && $dataArr['mst_report_id'] != '') ? $dataArr['mst_report_id'] : '';
        $returnArr[] = (isset($dataArr['report_name']) && $dataArr['report_name'] != '') ? $dataArr['report_name'] : '';
        $returnArr[] = (isset($dataArr['description']) && $dataArr['description'] != '') ? $dataArr['description'] : '';
        $returnArr[] = (isset($dataArr['report_header']) && $dataArr['report_header'] != '') ? $dataArr['report_header'] : '';
        $returnArr[] = (isset($dataArr['report_footer']) && $dataArr['report_footer'] != '') ? $dataArr['report_footer'] : '';
        $returnArr[] = (isset($dataArr['role_id']) && $dataArr['role_id'] != '') ? $dataArr['role_id'] : '';
        $returnArr[] = (isset($dataArr['is_main_report']) && $dataArr['is_main_report'] != '') ? $dataArr['is_main_report'] : '';
        $returnArr[] = (isset($dataArr['report_category_id']) && $dataArr['report_category_id'] != '') ? $dataArr['report_category_id'] : '';
        $returnArr[] = (isset($dataArr['added_by']) && $dataArr['added_by'] != '') ? $dataArr['added_by'] : '';
        $returnArr[] = (isset($dataArr['added_date']) && $dataArr['added_date'] != '') ? $dataArr['added_date'] : '';
        $returnArr[] = (isset($dataArr['modified_by']) && $dataArr['modified_by'] != '') ? $dataArr['modified_by'] : '';
        $returnArr[] = (isset($dataArr['modified_date']) && $dataArr['modified_date'] != '') ? $dataArr['modified_date'] : '';
        $returnArr[] = (isset($dataArr['action_type']) && $dataArr['action_type'] != '') ? $dataArr['action_type'] : '';

        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for create report  columns
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getCreateReportColumnsArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['column_id']) && $dataArr['column_id'] != '') ? $dataArr['column_id'] : '';
        $returnArr[] = (isset($dataArr['selected_in_display']) && $dataArr['selected_in_display'] != '') ? $dataArr['selected_in_display'] : '';
        $returnArr[] = (isset($dataArr['is_orderby']) && $dataArr['is_orderby'] != '') ? $dataArr['is_orderby'] : '';
        $returnArr[] = (isset($dataArr['order_by']) && $dataArr['order_by'] != '') ? $dataArr['order_by'] : '';
        $returnArr[] = (isset($dataArr['order_by_seq']) && $dataArr['order_by_seq'] != '') ? $dataArr['order_by_seq'] : '';
        $returnArr[] = (isset($dataArr['filter_applied']) && $dataArr['filter_applied'] != '') ? $dataArr['filter_applied'] : '';
        $returnArr[] = (isset($dataArr['filter_group_id']) && $dataArr['filter_group_id'] != '') ? $dataArr['filter_group_id'] : '';
        $returnArr[] = (isset($dataArr['filter_id']) && $dataArr['filter_id'] != '') ? $dataArr['filter_id'] : '';
        $returnArr[] = (isset($dataArr['filter_value']) && $dataArr['filter_value'] != '') ? $dataArr['filter_value'] : '';
        $returnArr[] = (isset($dataArr['filter_pattern']) && $dataArr['filter_pattern'] != '') ? $dataArr['filter_pattern'] : '';
        $returnArr[] = (isset($dataArr['is_having']) && $dataArr['is_having'] != '') ? $dataArr['is_having'] : '';
        $returnArr[] = (isset($dataArr['show_in_groupby']) && $dataArr['show_in_groupby'] != '') ? $dataArr['show_in_groupby'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for delete report columns
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getDeleteReportColumnsArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for delete report
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getArrayForRemoveReport($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for filter values
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getFilterValueArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['field_value_id']) && $dataArr['field_value_id'] != '') ? $dataArr['field_value_id'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for add roles
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getAddReportRoleArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['role_id']) && $dataArr['role_id'] != '') ? $dataArr['role_id'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for delete roles
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getDeleteReportRoleArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for report roles
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getReportRoleArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for report contact groups
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getReportContactGroup($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = '';
        $returnArr[] = '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for report contact ids
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getUserIdsByContactIds($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['contact_ids']) && $dataArr['contact_ids'] != '') ? $dataArr['contact_ids'] : '0';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for get order by columns for report
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getReportInfoTransactionSummaryArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for get order by columns for report
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getReportInfoContactReportArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for get order by columns for report
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getReportInfoContactTransactionArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for get order by columns for report
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getReportInfoTopDonationArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for get order by columns for report
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getReportInfoTransactionDetailArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for get order by columns for report
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getReportInfoTransactionOverDueArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for get order by columns for report
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getReportLapsedMembershipArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for get order by columns for report
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getReportMembershipDetailArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for get order by columns for report
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getReportReconcillationArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for get order by columns for report
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getReportCampDetailArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for get order by columns for report
     * @param array
     * @return array
     * @author Icreon Tech - DT

     */
    public function getProductRecordArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    /**
     * Function used to check variables report
     * @author Icreon Tech - DT
     * @return boolean
     * @param Array
     */
    public function exchangeArray($data) {
        
    }

    public function getReportInfoContactReportArray($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_column_list']) && $dataArr['report_column_list'] != '') ? $dataArr['report_column_list'] : '';
        $returnArr[] = (isset($dataArr['filter_condition']) && $dataArr['filter_condition'] != '') ? $dataArr['filter_condition'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    public function getReportInfoArray($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['mst_report_id']) && $dataArr['mst_report_id'] != '') ? $dataArr['mst_report_id'] : '';
        $returnArr[] = (isset($dataArr['report_column_list']) && $dataArr['report_column_list'] != '') ? $dataArr['report_column_list'] : '';
        $returnArr[] = (isset($dataArr['filter_condition']) && $dataArr['filter_condition'] != '') ? $dataArr['filter_condition'] : '';
        $returnArr[] = (isset($dataArr['order_by_condition']) && $dataArr['order_by_condition'] != '') ? $dataArr['order_by_condition'] : '';
        $returnArr[] = (isset($dataArr['having_condition']) && $dataArr['having_condition'] != '') ? $dataArr['having_condition'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    public function getReportInfoTransactionSummaryArray($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['mst_report_id']) && $dataArr['mst_report_id'] != '') ? $dataArr['mst_report_id'] : '';
        $returnArr[] = (isset($dataArr['report_column_list']) && $dataArr['report_column_list'] != '') ? $dataArr['report_column_list'] : '';
        $returnArr[] = (isset($dataArr['filter_condition']) && $dataArr['filter_condition'] != '') ? $dataArr['filter_condition'] : '';
        $returnArr[] = (isset($dataArr['order_by_condition']) && $dataArr['order_by_condition'] != '') ? $dataArr['order_by_condition'] : '';
        $returnArr[] = (isset($dataArr['having_condition']) && $dataArr['having_condition'] != '') ? $dataArr['having_condition'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    public function getReportInfoTransactionDetailArray($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['mst_report_id']) && $dataArr['mst_report_id'] != '') ? $dataArr['mst_report_id'] : '';
        $returnArr[] = (isset($dataArr['report_column_list']) && $dataArr['report_column_list'] != '') ? $dataArr['report_column_list'] : '';
        $returnArr[] = (isset($dataArr['filter_condition1']) && $dataArr['filter_condition1'] != '') ? $dataArr['filter_condition1'] : '';
        $returnArr[] = (isset($dataArr['filter_condition2']) && $dataArr['filter_condition2'] != '') ? $dataArr['filter_condition2'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    public function getReportInfoTransactionOverDueArray($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['mst_report_id']) && $dataArr['mst_report_id'] != '') ? $dataArr['mst_report_id'] : '';
        $returnArr[] = (isset($dataArr['report_column_list']) && $dataArr['report_column_list'] != '') ? $dataArr['report_column_list'] : '';
        $returnArr[] = (isset($dataArr['filter_condition1']) && $dataArr['filter_condition1'] != '') ? $dataArr['filter_condition1'] : '';
        $returnArr[] = (isset($dataArr['filter_condition2']) && $dataArr['filter_condition2'] != '') ? $dataArr['filter_condition2'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    public function getReportMembershipDetailArray($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['mst_report_id']) && $dataArr['mst_report_id'] != '') ? $dataArr['mst_report_id'] : '';
        $returnArr[] = (isset($dataArr['report_column_list']) && $dataArr['report_column_list'] != '') ? $dataArr['report_column_list'] : '';
        $returnArr[] = (isset($dataArr['filter_condition1']) && $dataArr['filter_condition1'] != '') ? $dataArr['filter_condition1'] : '';
        $returnArr[] = (isset($dataArr['filter_condition2']) && $dataArr['filter_condition2'] != '') ? $dataArr['filter_condition2'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    public function getReportMembershipSummaryArray($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['mst_report_id']) && $dataArr['mst_report_id'] != '') ? $dataArr['mst_report_id'] : '';
        $returnArr[] = (isset($dataArr['report_column_list']) && $dataArr['report_column_list'] != '') ? $dataArr['report_column_list'] : '';
        $returnArr[] = (isset($dataArr['filter_condition1']) && $dataArr['filter_condition1'] != '') ? $dataArr['filter_condition1'] : '';
        $returnArr[] = (isset($dataArr['filter_condition2']) && $dataArr['filter_condition2'] != '') ? $dataArr['filter_condition2'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    public function getProductRecordArray($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['mst_report_id']) && $dataArr['mst_report_id'] != '') ? $dataArr['mst_report_id'] : '';
        $returnArr[] = (isset($dataArr['report_column_list']) && $dataArr['report_column_list'] != '') ? $dataArr['report_column_list'] : '';
        $returnArr[] = (isset($dataArr['filter_condition1']) && $dataArr['filter_condition1'] != '') ? $dataArr['filter_condition1'] : '';
        $returnArr[] = (isset($dataArr['filter_condition2']) && $dataArr['filter_condition2'] != '') ? $dataArr['filter_condition2'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    public function getReportCampDetailArray($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_column_list']) && $dataArr['report_column_list'] != '') ? $dataArr['report_column_list'] : '';
        $returnArr[] = (isset($dataArr['filter_condition']) && $dataArr['filter_condition'] != '') ? $dataArr['filter_condition'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    public function getReportReconcillationArray($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['filter_condition']) && $dataArr['filter_condition'] != '') ? $dataArr['filter_condition'] : '';
        return $returnArr;
    }

    public function getReportInfoContactTransactionArray($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['mst_report_id']) && $dataArr['mst_report_id'] != '') ? $dataArr['mst_report_id'] : '';
        $returnArr[] = (isset($dataArr['report_column_list']) && $dataArr['report_column_list'] != '') ? $dataArr['report_column_list'] : '';
        $returnArr[] = (isset($dataArr['filter_condition1']) && $dataArr['filter_condition1'] != '') ? $dataArr['filter_condition1'] : '';
        $returnArr[] = (isset($dataArr['filter_condition2']) && $dataArr['filter_condition2'] != '') ? $dataArr['filter_condition2'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    public function getReportInfoTopDonationArray($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['mst_report_id']) && $dataArr['mst_report_id'] != '') ? $dataArr['mst_report_id'] : '';
        $returnArr[] = (isset($dataArr['report_column_list']) && $dataArr['report_column_list'] != '') ? $dataArr['report_column_list'] : '';
        $returnArr[] = (isset($dataArr['filter_condition1']) && $dataArr['filter_condition1'] != '') ? $dataArr['filter_condition1'] : '';
        $returnArr[] = (isset($dataArr['filter_condition2']) && $dataArr['filter_condition2'] != '') ? $dataArr['filter_condition2'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    public function getReportLapsedMembershipArray($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['mst_report_id']) && $dataArr['mst_report_id'] != '') ? $dataArr['mst_report_id'] : '';
        $returnArr[] = (isset($dataArr['report_column_list']) && $dataArr['report_column_list'] != '') ? $dataArr['report_column_list'] : '';
        $returnArr[] = (isset($dataArr['filter_condition1']) && $dataArr['filter_condition1'] != '') ? $dataArr['filter_condition1'] : '';
        $returnArr[] = (isset($dataArr['filter_condition2']) && $dataArr['filter_condition2'] != '') ? $dataArr['filter_condition2'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    public function getReportInfoLybuntArray($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['mst_report_id']) && $dataArr['mst_report_id'] != '') ? $dataArr['mst_report_id'] : '';
        $returnArr[] = (isset($dataArr['report_column_list']) && $dataArr['report_column_list'] != '') ? $dataArr['report_column_list'] : '';
        $returnArr[] = (isset($dataArr['filter_condition1']) && $dataArr['filter_condition1'] != '') ? $dataArr['filter_condition1'] : '';
        $returnArr[] = (isset($dataArr['filter_condition2']) && $dataArr['filter_condition2'] != '') ? $dataArr['filter_condition2'] : '';
        $returnArr[] = (isset($dataArr['year']) && $dataArr['year'] != '') ? $dataArr['year'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    public function getReportInfoSybuntArray($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['mst_report_id']) && $dataArr['mst_report_id'] != '') ? $dataArr['mst_report_id'] : '';
        $returnArr[] = (isset($dataArr['report_column_list']) && $dataArr['report_column_list'] != '') ? $dataArr['report_column_list'] : '';
        $returnArr[] = (isset($dataArr['filter_condition1']) && $dataArr['filter_condition1'] != '') ? $dataArr['filter_condition1'] : '';
        $returnArr[] = (isset($dataArr['filter_condition2']) && $dataArr['filter_condition2'] != '') ? $dataArr['filter_condition2'] : '';
        $returnArr[] = (isset($dataArr['year']) && $dataArr['year'] != '') ? $dataArr['year'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    public function getReportRepeatTransactionArray($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['report_id']) && $dataArr['report_id'] != '') ? $dataArr['report_id'] : '';
        $returnArr[] = (isset($dataArr['mst_report_id']) && $dataArr['mst_report_id'] != '') ? $dataArr['mst_report_id'] : '';
        $returnArr[] = (isset($dataArr['report_column_list']) && $dataArr['report_column_list'] != '') ? $dataArr['report_column_list'] : '';

        $returnArr[] = (isset($dataArr['initialDateRange']) && $dataArr['initialDateRange'] != '') ? $dataArr['initialDateRange'] : '';
        $returnArr[] = (isset($dataArr['secondDateRange']) && $dataArr['secondDateRange'] != '') ? $dataArr['secondDateRange'] : '';
        $returnArr[] = (isset($dataArr['filter_condition_group']) && $dataArr['filter_condition_group'] != '') ? $dataArr['filter_condition_group'] : '';
        $returnArr[] = (isset($dataArr['group_by']) && $dataArr['group_by'] != '') ? $dataArr['group_by'] : '';
        $returnArr[] = (isset($dataArr['having_1']) && $dataArr['having_1'] != '') ? $dataArr['having_1'] : '';
        $returnArr[] = (isset($dataArr['having_2']) && $dataArr['having_2'] != '') ? $dataArr['having_2'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

}