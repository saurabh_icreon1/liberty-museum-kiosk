<?php

/**
 * This model is used for Report module database related work.
 * @package    Report
 * @author     Icreon Tech - DT
 */

namespace Report\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\TableGateway\Feature\MasterSlaveFeature;

/**
 * This class is used for Report module database related work.
 * @package    Report_Report
 * @author     Icreon Tech - DT
 */
class ReportTable {

    protected $tableGateway;
    protected $dbAdapter;
	protected $slavedbAdapter;
	


    public function __construct(TableGateway $tableGateway,MasterSlaveFeature $feature) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter =$this->tableGateway->getAdapter();
		$this->slavedbAdapter=$feature->getSlaveAdapter();
	}

    /**
     * Function for get reports by report type id
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getReportsByReportType($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReportList(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $stmt->getResource()->bindParam(4, $reportData[3]);
        $stmt->getResource()->bindParam(5, $reportData[4]);
        $stmt->getResource()->bindParam(6, $reportData[5]);
        $stmt->getResource()->bindParam(7, $reportData[6]);
        $stmt->getResource()->bindParam(8, $reportData[7]);
        $stmt->getResource()->bindParam(9, $reportData[8]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return array();
    }

    /**
     * Function for get display columns for report
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getReportDisplayColumns($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReportDisplayColumns(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get group by columns for report
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getReportGroupBy($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReportGroupByColumns(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
		$stmt->getResource()->bindParam(2, $reportData[1]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        return $resultSet;
    }

    /**
     * Function for get order by columns for report
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getReportOrderByColumns($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReportOrderByColumns(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get filter and value columns for report
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getReportFilterValueColumns($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReportFilterValueColumns(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get conditions for group filter type
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getConditionsForGroup($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getConditionForGroup(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get report info for grid
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getReportInfo($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReport(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor(); 
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get report info for grid
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getReportInfoSybunt($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReportSybunt(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get report info for grid
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getReportInfoLybunt($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReportLybunt(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get report info for grid
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getReportMembershipSummary($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReportMembershipSummary(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get report info for grid
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getReportRepeatTransaction($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReportRepeatTransaction(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for insert report data
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function saveReport($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_insertReport(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $stmt->getResource()->bindParam(4, $reportData[3]);
        $stmt->getResource()->bindParam(5, $reportData[4]);
        $stmt->getResource()->bindParam(6, $reportData[5]);
        $stmt->getResource()->bindParam(7, $reportData[6]);
        $stmt->getResource()->bindParam(8, $reportData[7]);
        $stmt->getResource()->bindParam(9, $reportData[8]);
        $stmt->getResource()->bindParam(10, $reportData[9]);
        $stmt->getResource()->bindParam(11, $reportData[10]);
        $stmt->getResource()->bindParam(12, $reportData[11]);
        $stmt->getResource()->bindParam(13, $reportData[12]);
        $stmt->getResource()->bindParam(14, $reportData[13]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['REPORT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for insert report column data
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function saveReportColumns($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_insertReportColumnsInfo(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $stmt->getResource()->bindParam(4, $reportData[3]);
        $stmt->getResource()->bindParam(5, $reportData[4]);
        $stmt->getResource()->bindParam(6, $reportData[5]);
        $stmt->getResource()->bindParam(7, $reportData[6]);
        $stmt->getResource()->bindParam(8, $reportData[7]);
        $stmt->getResource()->bindParam(9, $reportData[8]);
        $stmt->getResource()->bindParam(10, $reportData[9]);
        $stmt->getResource()->bindParam(11, $reportData[10]);
        $stmt->getResource()->bindParam(12, $reportData[11]);
        $stmt->getResource()->bindParam(13, $reportData[12]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERTED_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for delete report column data
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function deleteReportColumns($reportData) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($reportData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_rep_deleteReportColumnsInfo(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $reportData[0]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for delete report data
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function removeReportById($reportData) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($reportData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_rep_deleteReport(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $reportData[0]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for get filter values
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function getFilterValues($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReportFilterValues(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        return $resultSet;
    }

    /**
     * Function for insert report role
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function saveReportRole($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_insertReportRole(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        // $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return true;
//        if ($resultSet) {
//            // return $resultSet[0]['LAST_INSERT_ID'];
//            return true;
//        } else {
//            throw new \Exception('There is some error.');
//        }
    }

    /**
     * Function for delete report roles data
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function deleteReportRoles($reportData) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($reportData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_rep_deleteReportRoles(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $reportData[0]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for get reports role
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getReportRoles($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReportRoles(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get reports contacts group
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getReportContactGroup($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL  usp_rep_getReportContact(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        return $resultSet;
    }

    /**
     * Function for get reports user ids
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getUserIdsByContactIds($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL  usp_usr_getUserIdsByContactIds(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        //$resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        return true;
    }

    /**
     * Function for get report info for grid
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getReportInfoTransactionSummary($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getTransactionSummary(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get report info for grid
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getReportInfoContactReport($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getContactReport(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get report info for grid
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getReportInfoContactTransaction($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getContactTransactionReport(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get report info for grid
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getReportInfoTopDonation($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getTopDonationReport(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get report info for grid
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getReportInfoTransactionDetail($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getTransactionDetailsReport(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get report info for grid
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getReportInfoTransactionOverDue($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getTransactionOverdueReport(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get report info for grid
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getReportLapsedMembership($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getLapsedMembershipReport(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get report info for grid
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getReportMembershipDetail($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getMembershipDetailReport(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }
    
    /**
     * Function for get report info for grid
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getReportReconcillation($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReconcillationReport(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        // asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }
    
    /**
     * Function for get report info for grid
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getReportCampDetail($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getCampDetailReport(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        // asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }
    
    /**
     * Function for append number of questions marks for stored procedure
     * @author Icreon Tech - DT
     * @return String
     * @param Array
     */
    public function appendQuestionMars($count) {
        $returnstr = '';
        for ($i = 1; $i <= $count; $i++) {
            $returnstr.='?,';
        }
        return rtrim($returnstr, ',');
    }
    
    /**
     * Function for get report info for grid
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getReportColumnInfo($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReportColumnInfo(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);        
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }
    
    /**
     * Function for get report product info for grid
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getReportProductReport($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReportProductReport(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportFilterConditions($filterParams = array()){

		$stmt = $this->dbAdapter->createStatement();
		
		$reportId = (isset($filterParams['reportId']) && $filterParams['reportId']!='')?$filterParams['reportId']:'';
		$mstReportId = (isset($filterParams['mstReportId']) && $filterParams['mstReportId']!='')?$filterParams['mstReportId']:'';
		$filterIds = (isset($filterParams['filterIds']) && $filterParams['filterIds']!='')?$filterParams['filterIds']:'';
		$conditionalColumn = (isset($filterParams['conditionalColumn']) && $filterParams['conditionalColumn']!='')?$filterParams['conditionalColumn']:'';

        $stmt->prepare("CALL usp_rep_getReportWhereCondition(?,?,?,?)");
        $stmt->getResource()->bindParam(1, $reportId);
		$stmt->getResource()->bindParam(2, $mstReportId);
        $stmt->getResource()->bindParam(3, $filterIds);
		$stmt->getResource()->bindParam(4, $conditionalColumn);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
	}

	public function getReportInfoContactReportSlv($reportData) {
		$procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getContactReport_slv(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
		$stmt->getResource()->bindParam(4, $reportData[3]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportInfoSlv($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReport_Slv(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
		$stmt->getResource()->bindParam(4, $reportData[3]);
        $stmt->getResource()->bindParam(5, $reportData[4]);
        $stmt->getResource()->bindParam(6, $reportData[5]);
		$stmt->getResource()->bindParam(7, $reportData[6]);
		$stmt->getResource()->bindParam(8, $reportData[7]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor(); 
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportHavingCondition($params = array()){
		$mstReportId = (isset($params['mstReportId']) && $params['mstReportId']!='')?$params['mstReportId']:'';
		$filterIds = (isset($params['filterIds']) && $params['filterIds']!='')?$params['filterIds']:'';
		$toReplace = (isset($params['toReplace']) && $params['toReplace']!='')?$params['toReplace']:'';
		
		$stmt = $this->dbAdapter->createStatement();
		
		$stmt->prepare("CALL usp_rep_getReportHavingCondition(?,?,?)");
        $stmt->getResource()->bindParam(1, $mstReportId);
        $stmt->getResource()->bindParam(2, $filterIds);
		$stmt->getResource()->bindParam(3, $toReplace);
        
		$result = $stmt->execute();
        
		$statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
	}

	public function getReportInfoTransactionSummarySlv($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getTransactionSummary_Slv(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
		$stmt->getResource()->bindParam(4, $reportData[3]);
        $stmt->getResource()->bindParam(5, $reportData[4]);
        $stmt->getResource()->bindParam(6, $reportData[5]);
		$stmt->getResource()->bindParam(7, $reportData[6]);
		$stmt->getResource()->bindParam(8, $reportData[7]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportInfoTransactionDetailSlv($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getTransactionDetailsReport_Slv(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
		$stmt->getResource()->bindParam(4, $reportData[3]);
        $stmt->getResource()->bindParam(5, $reportData[4]);
        $stmt->getResource()->bindParam(6, $reportData[5]);
		$stmt->getResource()->bindParam(7, $reportData[6]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportInfoTransactionOverDueSlv($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getTransactionOverdueReport_Slv(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
		$stmt->getResource()->bindParam(4, $reportData[3]);
        $stmt->getResource()->bindParam(5, $reportData[4]);
        $stmt->getResource()->bindParam(6, $reportData[5]);
		$stmt->getResource()->bindParam(7, $reportData[6]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }


	public function getReportMembershipDetailSlv($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getMembershipDetailReport_Slv(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
		$stmt->getResource()->bindParam(4, $reportData[3]);
        $stmt->getResource()->bindParam(5, $reportData[4]);
        $stmt->getResource()->bindParam(6, $reportData[5]);
		$stmt->getResource()->bindParam(7, $reportData[6]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportMembershipSummarySlv($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReportMembershipSummary_Slv(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
		$stmt->getResource()->bindParam(4, $reportData[3]);
        $stmt->getResource()->bindParam(5, $reportData[4]);
        $stmt->getResource()->bindParam(6, $reportData[5]);
		$stmt->getResource()->bindParam(7, $reportData[6]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportProductReportSlv($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReportProductReport_Slv(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
		$stmt->getResource()->bindParam(4, $reportData[3]);
        $stmt->getResource()->bindParam(5, $reportData[4]);
        $stmt->getResource()->bindParam(6, $reportData[5]);
		$stmt->getResource()->bindParam(7, $reportData[6]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportCampDetailSlv($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getCampDetailReport_Slv(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
		$stmt->getResource()->bindParam(4, $reportData[3]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        // asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportReconcillationSlv($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReconcillationReport_Slv(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        // asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportInfoContactTransactionSlv($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getContactTransactionReport_Slv(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
		$stmt->getResource()->bindParam(4, $reportData[3]);
        $stmt->getResource()->bindParam(5, $reportData[4]);
        $stmt->getResource()->bindParam(6, $reportData[5]);
		$stmt->getResource()->bindParam(7, $reportData[6]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportInfoTopDonationSlv($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getTopDonationReport_Slv(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
		$stmt->getResource()->bindParam(4, $reportData[3]);
        $stmt->getResource()->bindParam(5, $reportData[4]);
        $stmt->getResource()->bindParam(6, $reportData[5]);
		$stmt->getResource()->bindParam(7, $reportData[6]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }


	public function getReportLapsedMembershipSlv($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getLapsedMembershipReport_Slv(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
		$stmt->getResource()->bindParam(4, $reportData[3]);
        $stmt->getResource()->bindParam(5, $reportData[4]);
        $stmt->getResource()->bindParam(6, $reportData[5]);
		$stmt->getResource()->bindParam(7, $reportData[6]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportInfoLybuntSlv($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReportLybunt_Slv(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
		$stmt->getResource()->bindParam(4, $reportData[3]);
        $stmt->getResource()->bindParam(5, $reportData[4]);
        $stmt->getResource()->bindParam(6, $reportData[5]);
		$stmt->getResource()->bindParam(7, $reportData[6]);
		$stmt->getResource()->bindParam(8, $reportData[7]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }


	public function getReportInfoSybuntSlv($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReportSybunt_Slv(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
		$stmt->getResource()->bindParam(4, $reportData[3]);
        $stmt->getResource()->bindParam(5, $reportData[4]);
        $stmt->getResource()->bindParam(6, $reportData[5]);
		$stmt->getResource()->bindParam(7, $reportData[6]);
		$stmt->getResource()->bindParam(8, $reportData[7]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportRepeatTransactionSlv($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReportRepeatTransaction_Slv(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);

		$stmt->getResource()->bindParam(4, $reportData[3]);
        $stmt->getResource()->bindParam(5, $reportData[4]);
        $stmt->getResource()->bindParam(6, $reportData[5]);

		$stmt->getResource()->bindParam(7, $reportData[6]);
        $stmt->getResource()->bindParam(8, $reportData[7]);
        $stmt->getResource()->bindParam(9, $reportData[8]);

		$stmt->getResource()->bindParam(10, $reportData[9]);
        $stmt->getResource()->bindParam(11, $reportData[10]);

        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportInfoContactTransactionCount($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getContactTransactionReportCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportInfoContactTransactionSlvCount($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getContactTransactionReport_SlvCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
		$stmt->getResource()->bindParam(4, $reportData[3]);
        $stmt->getResource()->bindParam(5, $reportData[4]);
        $stmt->getResource()->bindParam(6, $reportData[5]);
		$stmt->getResource()->bindParam(7, $reportData[6]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportInfoContactReportCount($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getContactReportCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportInfoContactReportSlvCount($reportData) {
		$procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getContactReport_slvCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
		$stmt->getResource()->bindParam(4, $reportData[3]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportInfoTopDonationCount($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getTopDonationReportCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportInfoTopDonationSlvCount($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getTopDonationReport_SlvCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
		$stmt->getResource()->bindParam(4, $reportData[3]);
        $stmt->getResource()->bindParam(5, $reportData[4]);
        $stmt->getResource()->bindParam(6, $reportData[5]);
		$stmt->getResource()->bindParam(7, $reportData[6]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportInfoSybuntCount($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReportSybuntCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportInfoSybuntSlvCount($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReportSybunt_SlvCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
		$stmt->getResource()->bindParam(4, $reportData[3]);
        $stmt->getResource()->bindParam(5, $reportData[4]);
        $stmt->getResource()->bindParam(6, $reportData[5]);
		$stmt->getResource()->bindParam(7, $reportData[6]);
		$stmt->getResource()->bindParam(8, $reportData[7]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportInfoLybuntCount($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReportLybuntCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportInfoLybuntSlvCount($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReportLybunt_SlvCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
		$stmt->getResource()->bindParam(4, $reportData[3]);
        $stmt->getResource()->bindParam(5, $reportData[4]);
        $stmt->getResource()->bindParam(6, $reportData[5]);
		$stmt->getResource()->bindParam(7, $reportData[6]);
		$stmt->getResource()->bindParam(8, $reportData[7]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportInfoTransactionSummaryCount($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getTransactionSummaryCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportInfoTransactionSummarySlvCount($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getTransactionSummary_SlvCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
		$stmt->getResource()->bindParam(4, $reportData[3]);
        $stmt->getResource()->bindParam(5, $reportData[4]);
        $stmt->getResource()->bindParam(6, $reportData[5]);
		$stmt->getResource()->bindParam(7, $reportData[6]);
		$stmt->getResource()->bindParam(8, $reportData[7]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportRepeatTransactionCount($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReportRepeatTransactionCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportRepeatTransactionSlvCount($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReportRepeatTransaction_SlvCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);

		$stmt->getResource()->bindParam(4, $reportData[3]);
        $stmt->getResource()->bindParam(5, $reportData[4]);
        $stmt->getResource()->bindParam(6, $reportData[5]);

		$stmt->getResource()->bindParam(7, $reportData[6]);
        $stmt->getResource()->bindParam(8, $reportData[7]);
        $stmt->getResource()->bindParam(9, $reportData[8]);

		$stmt->getResource()->bindParam(10, $reportData[9]);
        $stmt->getResource()->bindParam(11, $reportData[10]);

        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportInfoTransactionDetailCount($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getTransactionDetailsReportCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportInfoTransactionDetailSlvCount($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getTransactionDetailsReport_SlvCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
		$stmt->getResource()->bindParam(4, $reportData[3]);
        $stmt->getResource()->bindParam(5, $reportData[4]);
        $stmt->getResource()->bindParam(6, $reportData[5]);
		$stmt->getResource()->bindParam(7, $reportData[6]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportInfoTransactionOverDueCount($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getTransactionOverdueReportCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportInfoTransactionOverDueSlvCount($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getTransactionOverdueReport_SlvCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
		$stmt->getResource()->bindParam(4, $reportData[3]);
        $stmt->getResource()->bindParam(5, $reportData[4]);
        $stmt->getResource()->bindParam(6, $reportData[5]);
		$stmt->getResource()->bindParam(7, $reportData[6]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportMembershipDetailCount($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getMembershipDetailReportCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportMembershipDetailSlvCount($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getMembershipDetailReport_SlvCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
		$stmt->getResource()->bindParam(4, $reportData[3]);
        $stmt->getResource()->bindParam(5, $reportData[4]);
        $stmt->getResource()->bindParam(6, $reportData[5]);
		$stmt->getResource()->bindParam(7, $reportData[6]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportMembershipSummaryCount($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReportMembershipSummaryCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportMembershipSummarySlvCount($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReportMembershipSummary_SlvCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
		$stmt->getResource()->bindParam(4, $reportData[3]);
        $stmt->getResource()->bindParam(5, $reportData[4]);
        $stmt->getResource()->bindParam(6, $reportData[5]);
		$stmt->getResource()->bindParam(7, $reportData[6]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportLapsedMembershipCount($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getLapsedMembershipReportCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportLapsedMembershipSlvCount($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getLapsedMembershipReport_SlvCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
		$stmt->getResource()->bindParam(4, $reportData[3]);
        $stmt->getResource()->bindParam(5, $reportData[4]);
        $stmt->getResource()->bindParam(6, $reportData[5]);
		$stmt->getResource()->bindParam(7, $reportData[6]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportProductReportCount($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReportProductReportCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportProductReportSlvCount($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReportProductReport_SlvCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
		$stmt->getResource()->bindParam(4, $reportData[3]);
        $stmt->getResource()->bindParam(5, $reportData[4]);
        $stmt->getResource()->bindParam(6, $reportData[5]);
		$stmt->getResource()->bindParam(7, $reportData[6]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportCampDetailCount($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getCampDetailReportCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        // asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportCampDetailSlvCount($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getCampDetailReport_SlvCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
		$stmt->getResource()->bindParam(4, $reportData[3]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        // asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportInfoCount($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReportCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor(); 
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

	public function getReportInfoSlvCount($reportData) {
        $procquesmarkapp = $this->appendQuestionMars(count($reportData));
        $stmt = $this->slavedbAdapter->createStatement();
        $stmt->prepare("CALL usp_rep_getReport_SlvCount(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $reportData[0]);
        $stmt->getResource()->bindParam(2, $reportData[1]);
        $stmt->getResource()->bindParam(3, $reportData[2]);
		$stmt->getResource()->bindParam(4, $reportData[3]);
        $stmt->getResource()->bindParam(5, $reportData[4]);
        $stmt->getResource()->bindParam(6, $reportData[5]);
		$stmt->getResource()->bindParam(7, $reportData[6]);
		$stmt->getResource()->bindParam(8, $reportData[7]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor(); 
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }
}