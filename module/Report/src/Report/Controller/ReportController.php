<?php
/**
 * This controller is used for report module
 * @package    Report_ReportController
 * @author     Icreon Tech - DT
 */

namespace Report\Controller;

use Base\Controller\BaseController;
use Base\Model\UploadHandler;
use Zend\View\Model\ViewModel;
use Report\Form\ReportForm;
use Report\Form\ReportOrder;
use Report\Form\ReportGroup;
use Report\Model\Report;
use Group\Model\Group;
use Zend\Http\Request;
use Zend\Authentication\AuthenticationService;
use Base\Model\SpreadsheetExcelReader;
use PHPExcel\Classes\PHPExcel;
use PHPExcel\Classes\PHPExcel\PHPExcel_IOFactory;

/**
 * This class is used to perform all reports of Report module
 *
 * Here we do create,edit,update,listing of report
 *
 * @category Zend
 * @package Report_ReportController
 * @link http://SITE_URL/crm-reports
 */
class ReportController extends BaseController {

    protected $_reportTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
	protected $_flashMessage = null;

    function __construct() {
		$this->_flashMessage = $this->flashMessenger();
        parent::__construct();
    }

    /**
     * This function is used to get table and config file object
     * @return     array
     * @param void
     * @author Icreon Tech - DT
     */
    public function getReportTable() {		
        $this->checkUserAuthentication();
        if (!$this->_reportTable) {
            $serviceManager = $this->getServiceLocator();
            $this->_reportTable = $serviceManager->get('Report\Model\ReportTable');
            $this->_translator = $serviceManager->get('translator');
            $this->_config = $serviceManager->get('Config');
            $this->_adapter = $serviceManager->get('dbAdapter');
			$this->_auth = new AuthenticationService();
        }
        return $this->_reportTable;
    }

    /**
     * This action is used to search reports
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function getReportListAction() {
        $params = $this->params()->fromRoute();

        if (!empty($params['report_type_id'])) {
            $reportTypeId = $params['report_type_id'];
            $masReportId = !empty($params['mst_report_id']) ? $this->decrypt($params['mst_report_id']) : '';
            $this->checkUserAuthentication();
            $this->getReportTable();
            $params = $this->params()->fromRoute();
            $reportListMessages = $this->_config['report_messages']['config']['report_list_message'];
            $commonMessages = $this->_config['report_messages']['config']['common_message'];
            $addReportCategoryUrl = $this->_config['add_report_category_url'];
            $reportCategoryHeading = $this->_config['report_category_heading'];

            $report = new Report($this->_adapter);
            $request = $this->getRequest();
            /* end  Saved search data */
            if ($request->isXmlHttpRequest() && $request->isPost()) {
                $response = $this->getResponse();
                $postArray = $request->getPost()->toArray();
                $searchParam = array();
                $limit = $request->getPost('rows');
                $page = $request->getPost('page');
                $start = $limit * $page - $limit; // do not put $limit*($page - 1)
                $searchParam['start_index'] = $start;
                $searchParam['record_limit'] = $limit;
                $searchParam['sort_field'] = $request->getPost('sidx');
                $searchParam['sort_order'] = $request->getPost('sord');
                $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'modified_date') ? 'tbl_rep.modified_date' : $searchParam['sort_field'];
                $searchParam['report_category_id'] = $reportTypeId;
                $searchParam['user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $searchParam['is_main_report'] = $request->getPost('is_main_report');
                $searchParam['mst_report_id'] = $masReportId;
                $searchReportArr = $report->getReportSearchArr($searchParam);
                $seachResult = $this->_reportTable->getReportsByReportType($searchReportArr);
				$countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
                $jsonResult = array();
                $arrCell = array();
                $subArrCell = array();
                $jsonResult['page'] = $request->getPost('page');
                $jsonResult['records'] = $countResult[0]->RecordCount;
                $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
                
                if (!empty($seachResult)) {
                    foreach ($seachResult as $val) {
                        $arrCell['id'] = $val['report_id'];
                        $encryptId = $this->encrypt($val['report_id']);
                        $viewLink = '<a href="' . $addReportCategoryUrl[$reportTypeId] . '/' . $encryptId . '" class="view-icon" alt="view report"><div class="tooltip">View<span></span></div></a>';
                        if ($val['is_main_report'] == 1) {
                            $deleteLink = '';
                        } else {
                            $deleteLink = '<a onclick="deleteReport(\'' . $encryptId . '\')" href="#delete_report_content" class="delete_report delete-icon" alt="delete report"><div class="tooltip">Delete<span></span></div></a>';
                        }
                        $actions = '<div class="action-col">' . $viewLink . $deleteLink . '</div>';
                        $reportNameLink = '<a href="' . $addReportCategoryUrl[$reportTypeId] . '/' . $encryptId . '" class="link txt-decoration-underline" >' . $this->chunkData($val['report_name'], 100) . '</a>';
                        $arrCell['cell'] = array($reportNameLink, $this->chunkData($val['report_name'], 100), $actions);
                        $subArrCell[] = $arrCell;
                        $jsonResult['rows'] = $subArrCell;
                    }
                } else {
                    $jsonResult['rows'] = array();
                }
                
                return $response->setContent(\Zend\Json\Json::encode($jsonResult));
            } else {
                $this->layout('crm');
                $messages = array();
                if ($this->_flashMessage->hasMessages()) {
                    $messages = $this->_flashMessage->getMessages();
                }
                $viewModel = new ViewModel();
                $response = $this->getResponse();
                $viewModel->setVariables(array(
                    'messages' => $messages,
                    'jsLangTranslate' => array_merge($reportListMessages, $commonMessages),
                    'redirectUrl' => $_SERVER['REDIRECT_URL'],
                    'reportCategoryId' => $this->encrypt($reportTypeId),
                    'reportCategoryHeading' => $reportCategoryHeading[$reportTypeId - 1]
                ));
                return $viewModel;
            }
        } else {
            return $this->redirect()->toRoute('dashboard');
        }
    }

    /**
     * This action is used to create new group
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function addReportAction() {
        $params = $this->params()->fromRoute();

        if (!empty($params['report_category_id'])) {
            $reportCategoryId = $this->decrypt($params['report_category_id']);
            $this->checkUserAuthentication();
            $this->getReportTable();
            $params = $this->params()->fromRoute();
            $reportListMessages = $this->_config['report_messages']['config']['report_list_message'];
            $commonMessages = $this->_config['report_messages']['config']['common_message'];
            $reportCategoryUrls = $this->_config['report_category_url'];
            $addReportCategoryUrl = $this->_config['add_report_category_url'];

            $report = new Report($this->_adapter);
            $request = $this->getRequest();
            /* end  Saved search data */
            if ($request->isXmlHttpRequest() && $request->isPost()) {
                $response = $this->getResponse();
                $postArray = $request->getPost()->toArray();
                $searchParam = array();
                $limit = $request->getPost('rows');
                $page = $request->getPost('page');
                $start = $limit * $page - $limit; // do not put $limit*($page - 1)
                $searchParam['start_index'] = $start;
                $searchParam['record_limit'] = $limit;
                $searchParam['sort_field'] = $request->getPost('sidx');
                $searchParam['sort_order'] = $request->getPost('sord');
                $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'modified_date') ? 'tbl_rep.modified_date' : $searchParam['sort_field'];
                $searchParam['report_category_id'] = $reportCategoryId;
                $searchParam['user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $searchParam['is_main_report'] = $request->getPost('is_main_report');
                
                $searchReportArr = $report->getReportSearchArr($searchParam);

                $seachResult = $this->_reportTable->getReportsByReportType($searchReportArr);
                $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
                $jsonResult = array();
                $arrCell = array();
                $subArrCell = array();
                $jsonResult['page'] = $request->getPost('page');
                $jsonResult['records'] = $countResult[0]->RecordCount;
                $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
                
                if (!empty($seachResult)) {
                    foreach ($seachResult as $val) {
                        $arrCell['id'] = $val['report_id'];
                        $encryptId = $this->encrypt($val['report_id']);
                        $viewLink = '<a href="' . $addReportCategoryUrl[$reportCategoryId] . '/' . $encryptId . '" class="view-icon" alt="view report"><div class="tooltip">View<span></span></div></a>';
                        $deleteLink = '<a onclick="deleteReport(\'' . $encryptId . '\')" href="#delete_report_content" class="delete_report delete-icon" alt="delete report"><div class="tooltip">Delete<span></span></div></a>';
                        $actions = '<div class="action-col">' . $viewLink . $deleteLink . '</div>';
                        $reportNameLink = '<a href="' . $addReportCategoryUrl[$reportCategoryId] . '/' . $encryptId . '" class="link txt-decoration-underline">' . $this->chunkData($val['report_name'], 100) . '</a> <br /> <a href="' . $reportCategoryUrls[$reportCategoryId] . '/' . $this->encrypt($val['mst_report_id']) . '" class="txt-decoration-underline">Existing Report(s)</a>';
                        $arrCell['cell'] = array($reportNameLink, $this->chunkData($val['report_name'], 100), $actions);
                        $subArrCell[] = $arrCell;
                        $jsonResult['rows'] = $subArrCell;
                    }
                } else {
                    $jsonResult['rows'] = array();
                }
                
                return $response->setContent(\Zend\Json\Json::encode($jsonResult));
            } else {
                $this->layout('crm');
                $messages = array();
                if ($this->_flashMessage->hasMessages()) {
                    $messages = $this->_flashMessage->getMessages();
                }
                $viewModel = new ViewModel();
                $response = $this->getResponse();
                $viewModel->setVariables(array(
                    'messages' => $messages,
                    'jsLangTranslate' => array_merge($reportListMessages, $commonMessages),
                    'redirectUrl' => $_SERVER['REDIRECT_URL']
                ));
                return $viewModel;
            }
        } else {
            return $this->redirect()->toRoute('dashboard');
        }
    }

    /**
     * This action is used to generate all reports grid
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function generateAllReportsAction() {
        $this->checkUserAuthentication();
        $this->getReportTable();
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $reportListMessages = $this->_config['report_messages']['config']['report_list_message'];
            $commonMessages = $this->_config['report_messages']['config']['common_message'];
            $reportCategoryHeading = $this->_config['report_category_heading'];
            $reportCategoryUrls = $this->_config['report_category_url'];
            $report = new Report($this->_adapter);
            $response = $this->getResponse();
            $postArray = $request->getPost()->toArray();
            $searchParam = array();
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit; // do not put $limit*($page - 1)
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'modified_date') ? 'tbl_rep.modified_date' : $searchParam['sort_field'];
            $searchParam['is_main_report'] = '0';
            $searchParam['user_id'] = $this->_auth->getIdentity()->crm_user_id;
            $searchReportArr = $report->getReportSearchArr($searchParam);
            $seachResult = $this->_reportTable->getReportsByReportType($searchReportArr);
            $jsonResult = array();
            $jsonResult['gridBlockId'] = 'reportTypeGrids';
            $jsonResult['gridId'] = 'list_grid';
            $jsonResult['redirectUrlArr'] = array();
            $jsonResult['postDataArr'] = array();
            $jsonResult['colNamesArr'] = array();
            $jsonResult['colModelsArr'] = array();
            $jsonResult['headingGrid'] = array();
            $jsonResult['sortNameArr'] = array();
            $jsonResult['sortOrdArr'] = array();
            $categoryTypeArr = array();
            if (!empty($seachResult)) {
                foreach ($seachResult as $categoryReport) {
                    if (!in_array($categoryReport['report_category_id'], $categoryTypeArr)) {
                        $categoryTypeArr[] = $categoryReport['report_category_id'];
                    }
                }
            }
            asort($categoryTypeArr);
            $index = 0;
            foreach ($categoryTypeArr as $categoryTypeId) {
                $jsonResult['redirectUrlArr'][] = $reportCategoryUrls[$categoryTypeId];
                $postDataArr = array();
                $postDataArr['is_main_report'] = '0';
                $jsonResult['postDataArr'][] = $postDataArr;
                $colNamesArr = array();
                $colNamesArr[] = $commonMessages['L_NAME'];
                $colNamesArr[] = $commonMessages['L_DESCRIPTION'];
                $colNamesArr[] = $commonMessages['L_ACTIONS'];
                $jsonResult['colNamesArr'][] = $colNamesArr;
                $colModelsArr = array();
                $colModelsArr[] = array('name' => 'Name', 'index' => 'tbl_rep.report_name', 'sortable' => true, 'cmTemplate' => array('title' => true), 'classes' => '');
                $colModelsArr[] = array('name' => 'Description', 'index' => 'tbl_rep.description', 'sortable' => true, 'cmTemplate' => array('title' => true), 'classes' => '');
                $colModelsArr[] = array('name' => 'Action', 'index' => 'tbl_rep.report_name', 'sortable' => true, 'cmTemplate' => array('title' => true), 'classes' => 'none');
                $jsonResult['colModelsArr'][] = $colModelsArr;
                $jsonResult['headingGrid'][] = $reportCategoryHeading[$index];
                $jsonResult['sortNameArr'][] = 'modified_date';
                $jsonResult['sortOrdArr'][] = 'desc';
                $index++;
            }

            $jsonResult['status'] = 'success';
            return $response->setContent(\Zend\Json\Json::encode($jsonResult));
        } else {
            return $this->redirect()->toRoute('dashboard');
        }
    }

    /**
     * This action is used to get report info
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function getReportInfoAction() {
        $params = $this->params()->fromRoute();
        if (!empty($params['report_category_id'])) {
            $reportCategoryId = $params['report_category_id'];
            $reportId = $this->decrypt($params['report_id']);
            
            $this->checkUserAuthentication();
            $this->getReportTable();
            $params = $this->params()->fromRoute();
            $reportCreateMessages = $this->_config['report_messages']['config']['report_create_message'];
            $commonMessages = $this->_config['report_messages']['config']['common_message'];
            $addReportCategoryUrl = $this->_config['add_report_category_url'];

            $report = new Report($this->_adapter);
            $request = $this->getRequest();
            $reportForm = new ReportForm();
            $reportOrder = new ReportOrder();
            $reportGenerateForm = $reportForm->add($reportOrder);
            /* end  Saved search data */
            if ($request->isXmlHttpRequest() && $request->isPost()) {
                $response = $this->getResponse();
                $postArray = $request->getPost()->toArray();
                $searchParam = array();
                $limit = $request->getPost('rows');
                $page = $request->getPost('page');
                $start = $limit * $page - $limit; // do not put $limit*($page - 1)
                $searchParam['start_index'] = $start;
                $searchParam['record_limit'] = $limit;
                $searchParam['sort_field'] = $request->getPost('sidx');
                $searchParam['sort_order'] = $request->getPost('sord');
                $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'modified_date') ? 'tbl_rep.modified_date' : $searchParam['sort_field'];
                $searchParam['report_category_id'] = $reportCategoryId;
                $searchParam['user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $searchParam['is_main_report'] = $request->getPost('is_main_report');
                $searchParam['mst_report_id'] = $masReportId;
                $searchReportArr = $report->getReportSearchArr($searchParam);
                $seachResult = $this->_reportTable->getReportsByReportType($searchReportArr);
                $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
                $jsonResult = array();
                $arrCell = array();
                $subArrCell = array();
                $jsonResult['page'] = $request->getPost('page');
                $jsonResult['records'] = $countResult[0]->RecordCount;
                $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
                if (!empty($seachResult)) {
                    foreach ($seachResult as $val) {
                        $arrCell['id'] = $val['report_id'];
                        $encryptId = $this->encrypt($val['report_id']);
                        $viewLink = '<a href="' . $addReportCategoryUrl[$reportCategoryId] . '/' . $encryptId . '" class="view-icon" alt="view report"><div class="tooltip">View<span></span></div></a>';
                        $deleteLink = '<a onclick="deleteReport(\'' . $encryptId . '\')" href="#delete_report_content" class="delete_report delete-icon" alt="delete report"><div class="tooltip">Delete<span></span></div></a>';
                        $actions = '<div class="action-col">' . $viewLink . $deleteLink . '</div>';
                        $reportNameLink = '<a href="/crm-report-info/' . $encryptId . '" class="link txt-decoration-underline">' . $this->chunkData($val['report_name'], 100) . '</a>';
                        $arrCell['cell'] = array($reportNameLink, $this->chunkData($val['report_name'], 100), $actions);
                        $subArrCell[] = $arrCell;
                        $jsonResult['rows'] = $subArrCell;
                    }
                } else {
                    $jsonResult['rows'] = array();
                }
                
                return $response->setContent(\Zend\Json\Json::encode($jsonResult));
            } else {
                $this->layout('crm');
                $messages = array();
                if ($this->_flashMessage->hasMessages()) {
                    $messages = $this->_flashMessage->getMessages();
                }
                $displaycolumnsArr = array();
                $displaycolumnsArr['report_id'] = $reportId;
                $reportDisplayColumnsArr = $report->getReportDisplayColumnArr($displaycolumnsArr);
                $displayColumns = $this->_reportTable->getReportDisplayColumns($reportDisplayColumnsArr);
                $displayColumnsArr = array();
                $displaySelectedColumnArr = array();
                $gridColumns = array();
                $gridColModelsArr = array();
                if (!empty($displayColumns)) {
                    foreach ($displayColumns as $displayColumn) {
                        $displayColumnsArr[$displayColumn['column_id']] = $displayColumn['display_column_name'];
                        if ($displayColumn['column_id'] == $displayColumn['rep_column_id']) {
                            $displaySelectedColumnArr[] = $displayColumn['column_id'];
                            $gridColumns[] = $displayColumn['display_column_name'];
                            $gridColModelsArr[] = array('name' => $displayColumn['display_column_name'], 'index' => 'sql_column_name', 'sortable' => false, 'cmTemplate' => array('title' => true), 'classes' => '');
                        }
                    }
                }

                $reportOrderyByColumnsArr = $report->getReportOrderyByColumnArr($displaycolumnsArr);
                $orderByColumns = $this->_reportTable->getReportOrderByColumns($reportOrderyByColumnsArr);
                
                $orderColumnsArr = array('' => 'Select');
                $orderSelectedColumnArr = array();
                if (!empty($orderByColumns)) {
                    foreach ($orderByColumns as $orderColumn) {
                        $orderColumnsArr[$orderColumn['column_id']] = $orderColumn['display_column_name'];
                        if ($orderColumn['column_id'] == $orderColumn['rep_column_id']) {
                            $orderSelectedColumnArr[] = $orderColumn;
                        }
                    }
                }


                $groupByColumnsArr = array();
                $groupByColumnsArr['report_id'] = $reportId;
                $reportGroupByArr = $report->getReportGroupByArr($groupByColumnsArr);
                $groupByColumns = $this->_reportTable->getReportGroupBy($reportGroupByArr);

                $groupByArr = array();
                $groupSelectedColumnArr = array();
                if (!empty($groupByColumns)) {
                    foreach ($groupByColumns as $groupByCol) {
                        $groupByArr[$groupByCol['column_id']] = $groupByCol['display_column_name'];
                        if ($groupByCol['column_id'] == $groupByCol['rep_column_id']) {
                            $groupSelectedColumnArr[] = $groupByCol['column_id'];
                        }
                    }
                }
                
                $reportFilterValueColumnsArr = $report->getReportFilterValueColumnArr($displaycolumnsArr);
                $filterValueColumns = $this->_reportTable->getReportFilterValueColumns($reportFilterValueColumnsArr);
                $isShowValueArr = array();
                if (!empty($filterValueColumns)) {
                    $filterIndex = 0;
                    $dateColJson = array();
                    foreach ($filterValueColumns as $filterColumn) {
                        $filterGroupArr = array();
                        $filterGroupArr['filter_group_id'] = $filterColumn['filter_group_id'];
                        $filterGroups = $report->getConditionsForGroupArr($filterGroupArr);
                        $groupFields = $this->_reportTable->getConditionsForGroup($filterGroups);
                        $groupConditionFields = array('' => 'Select');
                        $groupShowFields = array();
                        if (!empty($groupFields)) {
                            foreach ($groupFields as $field) {
                                $groupConditionFields[$field['filter_id']] = $field['filter_name'];
                                $isShowValueArr['filter_id_' . $field['filter_id']] = $field['is_show_value'];
                                $groupShowFields[$field['filter_id']] = $field['is_show_value'];
                            }
                        }

                        $filterValueColumns[$filterIndex]['filter_group_filters'] = $groupConditionFields;
                        $filterValueColumns[$filterIndex]['filter_field_show'] = $groupShowFields;
                        $reportFilterValue = $filterColumn['rep_filter_value'];
                        $filterValues = array('' => 'Select');
                        if ($filterColumn['field_value_id'] != 0 && !empty($filterColumn['field_value_id'])) {
                            $filterValueArr = array();

                            $filterValueArr['field_value_id'] = $filterColumn['field_value_id'];
                            $filterValuesArr = $report->getFilterValueArr($filterValueArr);
                            $filterValuesResult = $this->_reportTable->getFilterValues($filterValuesArr);
                            foreach ($filterValuesResult as $filterValue) {
                                $filterValues[$filterValue['filter_value']] = $filterValue['filter_value'];
                            }

                            if (!empty($reportFilterValue) && $filterColumn['column_type'] == 3) {
                                $reportFilterValueArr = array();
                                $reportFilterValueExplode = explode(',', $reportFilterValue);
                                foreach ($reportFilterValueExplode as $filterValueSelected) {
                                    $reportFilterValueArr[] = $filterValueSelected;
                                }
                                $reportFilterValue = $reportFilterValueArr;
                            }
                        }
                        $filterValueColumns[$filterIndex]['filter_values'] = $filterValues;
                        $filterValueColumns[$filterIndex]['rep_filter_value'] = $reportFilterValue;
                        /** Code for date validation  */
                        if ($filterColumn['date_validation'] == '1') {
                            $dateColJson[] = $filterColumn['column_id'];
                        }
                        $filterIndex++;
                    }
                }
                $dateColJson = \Zend\Json\Json::encode($dateColJson);
                $searchParam['report_id'] = $reportId;
                $searchParam['is_main_report'] = '0';
                $searchParam['user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $searchReportArr = $report->getReportSearchArr($searchParam);
                //echo $reportId;die;
                $reportInfo = $this->_reportTable->getReportsByReportType($searchReportArr);
                $reportInfo = $reportInfo[0];

                $userRoles = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getCrmMasterUserRole();
                $userRolesArr = array();
                if (!empty($userRoles)) {
                    foreach ($userRoles as $role) {
                        $userRolesArr[$role['role_id']] = $role['role'];
                    }
                }

                /* get roles for this report */
                $getReportRoleArr = array();
                $getReportRoleArr['report_id'] = $reportId;
                $reportRoleDataArr = $report->getReportRoleArr($getReportRoleArr);
                $reportRoles = $this->_reportTable->getReportRoles($reportRoleDataArr);
                $reportRolesArr = array();
                if (!empty($reportRoles)) {
                    foreach ($reportRoles as $roleUser) {
                        $reportRolesArr[] = $roleUser['role_id'];
                    }
                }
                /* End get roles for this report */

                $reportGenerateForm->get('role_id[]')->setAttribute('options', $userRolesArr);
                $reportGenerateForm->get('role_id[]')->setValue($reportRolesArr);

                $reportGenerateForm->get('report_id')->setValue($reportId);


                $reportGenerateForm->get('display_columns')->setAttribute('options', $displayColumnsArr);
                $reportGenerateForm->get('group_by_columns')->setAttribute('options', $groupByArr);

                $reportGenerateForm->get('display_columns')->setValue($displaySelectedColumnArr);
                $reportGenerateForm->get('group_by_columns')->setValue($groupSelectedColumnArr);


                $reportGenerateForm->get('report_title')->setValue($reportInfo['report_name']);
                $reportGenerateForm->get('report_description')->setValue($reportInfo['description']);
                $reportGenerateForm->get('report_header')->setValue($reportInfo['report_header']);
                $reportGenerateForm->get('report_footer')->setValue($reportInfo['report_footer']);


                /* Grid Info */
                $jsonResult = array();
                $jsonResult['gridBlockId'] = 'reportDetailGrids';
                $jsonResult['gridId'] = 'list_grid_info';
                $jsonResult['redirectUrlArr'] = array();
                $jsonResult['postDataArr'] = array();
                $jsonResult['colNamesArr'] = array();
                $jsonResult['headingGrid'] = array();
                $jsonResult['colModelsArr'] = array();
                $jsonResult['sortNameArr'] = array();
                $jsonResult['sortOrdArr'] = array();
                $categoryTypeArr = array();
                if (!empty($seachResult)) {
                    foreach ($seachResult as $categoryReport) {
                        if (!in_array($categoryReport['report_category_id'], $categoryTypeArr)) {
                            $categoryTypeArr[] = $categoryReport['report_category_id'];
                        }
                    }
                }

                $jsonResult['redirectUrlArr'][] = '/get-report-grid-info';
                $postDataArr = array();
                $postDataArr['report_id'] = $reportId;
                $jsonResult['postDataArr'][] = $postDataArr;
                $jsonResult['colNamesArr'][] = $gridColumns;
                $jsonResult['colModelsArr'][] = $gridColModelsArr;
                $jsonResult['headingGrid'][] = '';
                $jsonResult['sortNameArr'][] = '';
                $jsonResult['sortOrdArr'][] = '';
                $gridDataJson = \Zend\Json\Json::encode($jsonResult);
                $isGridLoad = 1;
                if (empty($gridColumns)) {
                    $isGridLoad = 0;
                }

                $isShowValueJson = \Zend\Json\Json::encode($isShowValueArr);
                //echo $gridDataJson;die;
                /* end Grid Info */
                $viewModel = new ViewModel();
                $response = $this->getResponse();
                $viewModel->setVariables(array(
                    'messages' => $messages,
                    'jsLangTranslate' => array_merge($reportCreateMessages, $commonMessages),
                    'reportCategoryId' => $this->encrypt($reportCategoryId),
                    'report_form' => $reportGenerateForm,
                    'orderColumnsArr' => $orderColumnsArr,
                    'orderSelectedColumnArr' => $orderSelectedColumnArr,
                    'filterValueColumns' => $filterValueColumns,
                    'gridDataJson' => $gridDataJson,
                    'isGridLoad' => $isGridLoad,
                    'isShowValueJson' => $isShowValueJson,
                    'reportInfo' => $reportInfo,
                    'groupByArr' => $groupByArr,
                    'repCategoryId' => $reportCategoryId,
                    'encryptReportId' => $this->encrypt($reportId),
                    'dateColJson' => $dateColJson
                ));
                return $viewModel;
            }
        } else {
            return $this->redirect()->toRoute('dashboard');
        }
    }

    /**
     * This action is used to add more order by
     * @param void
     * @return void
     * @author Icreon Tech - DT
     */
    public function addReportOrderByAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $totalReportOrderBy = $request->getPost('totalReportOrderBy');
            $this->checkUserAuthenticationAjx();
            $this->getReportTable();
            $report = new Report($this->_adapter);
            $reportCreateMessages = $this->_config['report_messages']['config']['report_create_message'];
            $commonMessages = $this->_config['report_messages']['config']['common_message'];
            $reportOrder = new ReportOrder();
            $displaycolumnsArr = array();
            $reportId = $request->getPost('reportId');
            $displaycolumnsArr['report_id'] = $reportId;
            $reportOrderyByColumnsArr = $report->getReportOrderyByColumnArr($displaycolumnsArr);
            $orderByColumns = $this->_reportTable->getReportOrderByColumns($reportOrderyByColumnsArr);
            $orderColumnsArr = array('' => 'Select');
            if (!empty($orderByColumns)) {
                foreach ($orderByColumns as $orderColumn) {
                    $orderColumnsArr[$orderColumn['column_id']] = $orderColumn['display_column_name'];
                }
            }
            $reportOrder->get('order_columns[]')->setAttributes(array('id' => 'order_columns_' + ($totalReportOrderBy + 1), 'options' => $orderColumnsArr));
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => array_merge($reportCreateMessages, $commonMessages),
                'reportOrder' => $reportOrder,
                'totalReportOrderBy' => $totalReportOrderBy
            ));
            return $viewModel;
        } else {
            return $this->redirect()->toRoute('getCrmMembership');
        }
    }

    /**
     * This action is used to generate report info
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function getReportGridInfoAction() {
		ini_set('max_execution_time',0);
		$this->checkUserAuthentication();
        $this->getReportTable();
        $report = new Report($this->_adapter);
        $request = $this->getRequest();
		
        if (($request->isXmlHttpRequest() && $request->isPost()) || $request->getPost('export') == 'excel') {
            $response = $this->getResponse();
            $postArray = $request->getPost()->toArray();			
			
			#################################### Excecute only over Export through 'RUN REPORT' ###############################

			if($request->getPost('searchString') && !is_null($request->getPost('searchString')) && $request->getPost('export') == 'excel'){
				$searchArray = ((array)json_decode($request->getPost('searchString')));
				$searchArray2 = array();
				foreach($searchArray as $key=>$value){
					if(is_object($value)){
						foreach($value as $k1=>$val1){
							$searchArray2[$key][$k1] = $val1;
						}
					}else{
						$searchArray2[$key] = $searchArray[$key];
					}
				}
				
				$postArray = array_merge($postArray,$searchArray2);
			}			
			#################################################################################################################
			
			$searchParam = array();
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit; // do not put $limit*($page - 1)
			
			$searchParam['record_limit'] = $limit;

			######################################## Export in each case ##################################################
			$isExport = $request->getPost('export');
            if ($isExport != "" && $isExport == "excel") {
				$chunksize = $this->_config['export']['chunk_size'];
                $export_limit = $this->_config['export']['export_limit'];
                $searchParam['record_limit'] = $chunksize;
            }
			##############################################################################################################

            $searchParam['start_index'] = $start;
            
            
			
			$postArray['data'] = (array)$postArray['data'];
			$reportId = isset($postArray['data']['report_id'])? $postArray['data']['report_id']:$postArray['report_id'];
			$mstReportId = $postArray['mst_report_id'];
            $searchParam['sort_field'] = $postArray['sidx'];
            $searchParam['sort_order'] = $postArray['sord'];
            $searchParam['report_id'] = $reportId;
			
			$number_of_pages = 1;
            $page_counter = 1;
			 $report_data = array('report_id' => $searchParam['report_id'], 'user_id' => $this->_auth->getIdentity()->crm_user_id, 'is_main_report' => '0');
            $searchReportArr = $report->getReportSearchArr($report_data);
            $reportResult = $this->_reportTable->getReportsByReportType($searchReportArr);
			
            $filename = str_replace(" ", "_", $reportResult[0]['report_name']) . "_" . EXPORT_DATE_TIME_FORMAT . ".csv";
            
            do {
                if ($reportResult[0]['mst_report_id'] == 3) {

					if(isset($postArray['isRunOnly']) && $postArray['isRunOnly']=='1'){
						
						##################################### Applied over 'RUN REPORT' ###################################

						// @$report_column_list: columns to fetch from DB Table to be displayed in grid through 'Run Report'

						if(isset($postArray['reportColumns']) && !empty($postArray['reportColumns'])){
							$report_column_list = implode(",",$postArray['reportColumns']);
							$searchParam['report_column_list'] = $report_column_list;
						}
						
						// @$filterIdArray: column ids on which filters are appiled through 'Run Report'

						if(isset($postArray['filterIds']) && !empty($postArray['filterIds'])){
							$filterIdArray = (array)$postArray['filterIds'];
						}else{
							$filterIdArray = array();
						}

						// @$filterValuesArray: associaive array of column ids of posted filters and values

						if(isset($postArray['filterValues']) && !empty($postArray['filterValues'])){
							$filterValuesArray = (array)$postArray['filterValues'];							
						}else{
							$filterValuesArray = array();
						}
						
						// @$filter_condition: returns where condition

						if(!empty($filterIdArray)){
							$conditionalColumn = '482';
							$filterIdArray1 = $filterIdArray; 
							unset($filterIdArray[$conditionalColumn]);
							$filterIdArray2 = array_diff_assoc($filterIdArray1,$filterIdArray); 
							
							$filter_condition1 = $this->getReportFilterConditon($reportId,$filterIdArray,$filterValuesArray,$mstReportId);
							$searchParam['filter_condition1'] = $filter_condition1;

							$filter_condition2 = $this->getReportFilterConditon($reportId,$filterIdArray2,$filterValuesArray,$mstReportId,$conditionalColumn);
							$searchParam['filter_condition2'] = $filter_condition2;
						}
						$searchParam['mst_report_id'] = $postArray['mst_report_id'];
						$searchReportArr = $report->getReportInfoContactTransactionArray($searchParam);

						$countResult = $this->_reportTable->getReportInfoContactTransactionSlvCount($searchReportArr);

						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportInfoContactTransactionSlv($searchReportArr);
						
					}else{
						$searchReportArr = $report->getReportInfoContactTransactionArr($searchParam);

						$countResult = $this->_reportTable->getReportInfoContactTransactionCount($searchReportArr);
						
						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportInfoContactTransaction($searchReportArr);
					}

                } elseif ($reportResult[0]['mst_report_id'] == 4) {

					if(isset($postArray['isRunOnly']) && $postArray['isRunOnly']=='1'){

						##################################### Applied over 'RUN REPORT' ###################################

						// @$report_column_list: columns to fetch from DB Table to be displayed in grid through 'Run Report'

						if(isset($postArray['reportColumns']) && !empty($postArray['reportColumns'])){
							$report_column_list = implode(",",$postArray['reportColumns']);
							$searchParam['report_column_list'] = $report_column_list;
						}
						
						// @$filterIdArray: column ids on which filters are appiled through 'Run Report'

						if(isset($postArray['filterIds']) && !empty($postArray['filterIds'])){
							$filterIdArray = (array)$postArray['filterIds'];
						}else{
							$filterIdArray = array();
						}

						// @$filterValuesArray: associaive array of column ids of posted filters and values

						if(isset($postArray['filterValues']) && !empty($postArray['filterValues'])){
							$filterValuesArray = (array)$postArray['filterValues'];							
						}else{
							$filterValuesArray = array();
						}
						
						// @$filter_condition: returns where condition

						$filter_condition = $this->getReportFilterConditon($reportId,$filterIdArray,$filterValuesArray,$mstReportId);
						
						$searchParam['filter_condition'] = $filter_condition;
						$searchReportArr = $report->getReportInfoContactReportArray($searchParam);

						$countResult = $this->_reportTable->getReportInfoContactReportSlvCount($searchReportArr);

						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportInfoContactReportSlv($searchReportArr);

						###############################################################################################
					}else{

						########################### Applied over 'SAVE AND RUN REPORT'  ###############################

						$searchReportArr = $report->getReportInfoContactReportArr($searchParam);

						$countResult = $this->_reportTable->getReportInfoContactReportCount($searchReportArr);
						
						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportInfoContactReport($searchReportArr);
						
						
						###############################################################################################
					}					
					
                } elseif ($reportResult[0]['mst_report_id'] == 5) {

					if(isset($postArray['isRunOnly']) && $postArray['isRunOnly']=='1'){
						
						##################################### Applied over 'RUN REPORT' ###################################

						// @$report_column_list: columns to fetch from DB Table to be displayed in grid through 'Run Report'

						if(isset($postArray['reportColumns']) && !empty($postArray['reportColumns'])){
							$report_column_list = implode(",",$postArray['reportColumns']);
							$searchParam['report_column_list'] = $report_column_list;
						}
						
						// @$filterIdArray: column ids on which filters are appiled through 'Run Report'

						if(isset($postArray['filterIds']) && !empty($postArray['filterIds'])){
							$filterIdArray = (array)$postArray['filterIds'];
						}else{
							$filterIdArray = array();
						}

						// @$filterValuesArray: associaive array of column ids of posted filters and values

						if(isset($postArray['filterValues']) && !empty($postArray['filterValues'])){
							$filterValuesArray = (array)$postArray['filterValues'];							
						}else{
							$filterValuesArray = array();
						}
						
						// @$filter_condition: returns where condition

						if(!empty($filterIdArray)){
							$conditionalColumn = "51,507";
							$filterIdArray1 = $filterIdArray;
							unset($filterIdArray['51']);
							unset($filterIdArray['507']);
							$filterIdArray2 = array_diff_assoc($filterIdArray1,$filterIdArray); 
							
							$filter_condition1 = $this->getReportFilterConditon($reportId,$filterIdArray,$filterValuesArray,$mstReportId);
							$searchParam['filter_condition1'] = $filter_condition1;

							$filter_condition2 = $this->getReportFilterConditon($reportId,$filterIdArray2,$filterValuesArray,$mstReportId,$conditionalColumn);
							$searchParam['filter_condition2'] = $filter_condition2;
						}
						$searchParam['mst_report_id'] = $postArray['mst_report_id'];

						$searchReportArr = $report->getReportInfoTopDonationArray($searchParam);

						$countResult = $this->_reportTable->getReportInfoTopDonationSlvCount($searchReportArr);

						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportInfoTopDonationSlv($searchReportArr);					
						
					}else{
						$searchReportArr = $report->getReportInfoTopDonationArr($searchParam);

						$countResult = $this->_reportTable->getReportInfoTopDonationCount($searchReportArr);

						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportInfoTopDonation($searchReportArr);						
						
					}

                                        
                } elseif ($reportResult[0]['mst_report_id'] == 6) {


					if(isset($postArray['isRunOnly']) && $postArray['isRunOnly']=='1'){
						
						##################################### Applied over 'RUN REPORT' ###################################

						// @$report_column_list: columns to fetch from DB Table to be displayed in grid through 'Run Report'

						if(isset($postArray['reportColumns']) && !empty($postArray['reportColumns'])){
							$report_column_list = implode(",",$postArray['reportColumns']);
							$searchParam['report_column_list'] = $report_column_list;
						}
						
						// @$filterIdArray: column ids on which filters are appiled through 'Run Report'

						if(isset($postArray['filterIds']) && !empty($postArray['filterIds'])){
							$filterIdArray = (array)$postArray['filterIds'];
						}else{
							$filterIdArray = array();
						}

						// @$filterValuesArray: associaive array of column ids of posted filters and values

						if(isset($postArray['filterValues']) && !empty($postArray['filterValues'])){
							$filterValuesArray = (array)$postArray['filterValues'];							
						}else{
							$filterValuesArray = array();
						}
						
						// @$filter_condition: returns where condition

						if(!empty($filterIdArray)){
							$conditionalColumn = "63";
							$filterIdArray1 = $filterIdArray;
							unset($filterIdArray['63']);
							
							$filterIdArray2 = array_diff_assoc($filterIdArray1,$filterIdArray); 
							
							$filter_condition1 = $this->getReportFilterConditon($reportId,$filterIdArray,$filterValuesArray,$mstReportId);
							$searchParam['filter_condition1'] = $filter_condition1;

							$filter_condition2 = $this->getReportFilterConditon($reportId,$filterIdArray2,$filterValuesArray,$mstReportId,$conditionalColumn);
							$searchParam['filter_condition2'] = $filter_condition2;
							
						}
						$searchParam['mst_report_id'] = $postArray['mst_report_id'];

						if(isset($filterValuesArray['63'])){
							$searchParam['year'] = $filterValuesArray[63];
						}
						
						$searchReportArr = $report->getReportInfoSybuntArray($searchParam);

						$countResults = $this->_reportTable->getReportInfoSybuntSlvCount($searchReportArr);
						
						$countResult = array();
						if(!empty($countResults)){
							$countResult[0]['RecordCount'] = count($countResults);
						}

						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportInfoSybuntSlv($searchReportArr);						
						
					}else{
						$searchReportArr = $report->getReportInfoSybuntArr($searchParam);
						$countResults = $this->_reportTable->getReportInfoSybuntCount($searchReportArr);
						
						$countResult = array();
						if(!empty($countResults)){
							$countResult[0]['RecordCount'] = count($countResults);
						}

						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportInfoSybunt($searchReportArr);
						
					}
                } elseif ($reportResult[0]['mst_report_id'] == 7) {

					if(isset($postArray['isRunOnly']) && $postArray['isRunOnly']=='1'){
						
						##################################### Applied over 'RUN REPORT' ###################################

						// @$report_column_list: columns to fetch from DB Table to be displayed in grid through 'Run Report'

						if(isset($postArray['reportColumns']) && !empty($postArray['reportColumns'])){
							$report_column_list = implode(",",$postArray['reportColumns']);
							$searchParam['report_column_list'] = $report_column_list;
						}
						
						// @$filterIdArray: column ids on which filters are appiled through 'Run Report'

						if(isset($postArray['filterIds']) && !empty($postArray['filterIds'])){
							$filterIdArray = (array)$postArray['filterIds'];
						}else{
							$filterIdArray = array();
						}

						// @$filterValuesArray: associaive array of column ids of posted filters and values

						if(isset($postArray['filterValues']) && !empty($postArray['filterValues'])){
							$filterValuesArray = (array)$postArray['filterValues'];							
						}else{
							$filterValuesArray = array();
						}
						
						// @$filter_condition: returns where condition

						if(!empty($filterIdArray)){
							$conditionalColumn = "72";
							$filterIdArray1 = $filterIdArray;
							unset($filterIdArray['72']);
							
							$filterIdArray2 = array_diff_assoc($filterIdArray1,$filterIdArray); 
							
							$filter_condition1 = $this->getReportFilterConditon($reportId,$filterIdArray,$filterValuesArray,$mstReportId);
							$searchParam['filter_condition1'] = $filter_condition1;

							$filter_condition2 = $this->getReportFilterConditon($reportId,$filterIdArray2,$filterValuesArray,$mstReportId,$conditionalColumn);
							$searchParam['filter_condition2'] = $filter_condition2;
						}
						$searchParam['mst_report_id'] = $postArray['mst_report_id'];

						if(isset($filterValuesArray['72'])){
							$searchParam['year'] = $filterValuesArray[72];
						}
						
						$searchReportArr = $report->getReportInfoLybuntArray($searchParam);

						$countResults = $this->_reportTable->getReportInfoLybuntSlvCount($searchReportArr);

						$countResult = array();
						if(!empty($countResults)){							
							$countResult[0]['RecordCount'] = count($countResults);
						}

						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportInfoLybuntSlv($searchReportArr);
						
					}else{
						$searchReportArr = $report->getReportInfoLybuntArr($searchParam);

						$countResults = $this->_reportTable->getReportInfoLybuntCount($searchReportArr);

						$countResult = array();
						if(!empty($countResults)){
							$countResult[0]['RecordCount'] = count($countResults);
						}

						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportInfoLybunt($searchReportArr);
					}

                    
                } elseif ($reportResult[0]['mst_report_id'] == 8) {
					if(isset($postArray['isRunOnly']) && $postArray['isRunOnly']=='1'){
						##################################### Applied over 'RUN REPORT' ###################################

						// @$report_column_list: columns to fetch from DB Table to be displayed in grid through 'Run Report'

						if(isset($postArray['reportColumns']) && !empty($postArray['reportColumns'])){
							$report_column_list = implode(",",$postArray['reportColumns']);
							$searchParam['report_column_list'] = $report_column_list;
						}
						
						// @$filterIdArray: column ids on which filters are appiled through 'Run Report'

						if(isset($postArray['filterIds']) && !empty($postArray['filterIds'])){
							$filterIdArray = (array)$postArray['filterIds'];
						}else{
							$filterIdArray = array();
						}

						// @$filterValuesArray: associaive array of column ids of posted filters and values

						if(isset($postArray['filterValues']) && !empty($postArray['filterValues'])){
							$filterValuesArray = (array)$postArray['filterValues'];							
						}else{
							$filterValuesArray = array();
						}
						
						// @$filter_condition: returns where condition

						if(!empty($filterIdArray)){
							$filter_condition = $this->getReportFilterConditon($reportId,$filterIdArray,$filterValuesArray,$mstReportId);
						}
	
						$having_condition = $this->getReportHavingConditon($filterIdArray,$filterValuesArray,$mstReportId);

						$searchParam['filter_condition'] = $filter_condition;
						$searchParam['mst_report_id'] = $postArray['mst_report_id'];
						$searchParam['order_by_condition'] = $postArray['OrderBy'];
						$searchParam['having_condition'] = $having_condition;

						$searchReportArr = $report->getReportInfoTransactionSummaryArray($searchParam);

						$countResults = $this->_reportTable->getReportInfoTransactionSummarySlvCount($searchReportArr);

						$countResult = array();
						if(!empty($countResults)){
							$countResult[0]['RecordCount'] = count($countResults);
						}

						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportInfoTransactionSummarySlv($searchReportArr);
						
					}else{
						$searchReportArr = $report->getReportInfoTransactionSummaryArr($searchParam);

						$countResults = $this->_reportTable->getReportInfoTransactionSummaryCount($searchReportArr);

						$countResult = array();
						if(!empty($countResults)){
							$countResult[0]['RecordCount'] = count($countResults);
						}

						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportInfoTransactionSummary($searchReportArr);						
					}
                    
                    
                } elseif ($reportResult[0]['mst_report_id'] == 9) {

					if(isset($postArray['isRunOnly']) && $postArray['isRunOnly']=='1'){
						
						##################################### Applied over 'RUN REPORT' ###################################

						// @$report_column_list: columns to fetch from DB Table to be displayed in grid through 'Run Report'

						if(isset($postArray['reportColumns']) && !empty($postArray['reportColumns'])){
							$report_column_list = implode(",",$postArray['reportColumns']);
							$searchParam['report_column_list'] = $report_column_list;
						}
						
						// @$filterIdArray: column ids on which filters are appiled through 'Run Report'

						if(isset($postArray['filterIds']) && !empty($postArray['filterIds'])){
							$filterIdArray = (array)$postArray['filterIds'];
						}else{
							$filterIdArray = array();
						}

						// @$filterValuesArray: associaive array of column ids of posted filters and values

						if(isset($postArray['filterValues']) && !empty($postArray['filterValues'])){
							$filterValuesArray = (array)$postArray['filterValues'];							
						}else{
							$filterValuesArray = array();
						}
						
						// @$filter_condition: returns where condition

						if(!empty($filterIdArray)){
							$conditionalColumn1 = "100";
							
							$filterIdArray1 = $filterIdArray;
							unset($filterIdArray['100']);
							
							$filterIdArray2 = array_diff_assoc($filterIdArray1,$filterIdArray); 
							
							$p_initialDateRange = $this->getReportFilterConditon($reportId,$filterIdArray2,$filterValuesArray,$mstReportId,$conditionalColumn1);
							$searchParam['initialDateRange'] = $p_initialDateRange;


							$conditionalColumn2 = "101";
							
							$filterIdArray3 = $filterIdArray1;
							unset($filterIdArray1['101']);
							
							$filterIdArray4 = array_diff_assoc($filterIdArray3,$filterIdArray1); 
							
							$p_secondDateRange = $this->getReportFilterConditon($reportId,$filterIdArray4,$filterValuesArray,$mstReportId,$conditionalColumn2);
							$searchParam['secondDateRange'] = $p_secondDateRange;


							$conditionalColumn3 = "104";
							
							$filterIdArray5 = $filterIdArray3;
							unset($filterIdArray3['104']);
							
							$filterIdArray6 = array_diff_assoc($filterIdArray5,$filterIdArray3); 
							
							$filter_condition_group = $this->getReportFilterConditon($reportId,$filterIdArray6,$filterValuesArray,$mstReportId,$conditionalColumn3);
							$searchParam['filter_condition_group'] = $filter_condition_group;

							$finalizeFilterIds = "'102'";

							$having_1 = $this->getReportHavingConditon($filterIdArray5,$filterValuesArray,$mstReportId,$finalizeFilterIds);
							$searchParam['having_1'] = $having_1;

							$finalizeFilterIds = "'103'";

							$having_2 = $this->getReportHavingConditon($filterIdArray5,$filterValuesArray,$mstReportId,$finalizeFilterIds);
							
							$searchParam['having_2'] = $having_2;

						}

						if(isset($postArray['groupByCols']) && !empty($postArray['groupByCols'])){							
							$groupColumnsArray = array();
							$groupColumnsArray['report_id'] = $reportId['report_id'];
							$groupColumnsArray['column_id'] = $postArray['groupByCols'];
							$reportGroupByArray = $report->getReportGroupByArr($groupColumnsArray);
							$groupByColumn = $this->_reportTable->getReportGroupBy($reportGroupByArray);
							$searchParam['group_by'] = $groupByColumn[0]['group_by_column'];
						}

						$searchParam['mst_report_id'] = $postArray['mst_report_id'];
						
						$searchReportArray = $report->getReportRepeatTransactionArray($searchParam);

						$countResults = $this->_reportTable->getReportRepeatTransactionSlvCount($searchReportArray);

						$countResult = array();
						if(!empty($countResults)){
							$countResult[0]['RecordCount'] = count($countResults);
						}
												
						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportRepeatTransactionSlv($searchReportArray);
					}else{
						$searchReportArr = $report->getReportRepeatTransactionArr($searchParam);

						$countResults = $this->_reportTable->getReportRepeatTransactionCount($searchReportArr);

						$countResult = array();
						if(!empty($countResults)){
							$countResult[0]['RecordCount'] = count($countResults);
						}

						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportRepeatTransaction($searchReportArr);
					}

                    
                } elseif ($reportResult[0]['mst_report_id'] == 10) {
					if(isset($postArray['isRunOnly']) && $postArray['isRunOnly']=='1'){
						
						##################################### Applied over 'RUN REPORT' ###################################

						// @$report_column_list: columns to fetch from DB Table to be displayed in grid through 'Run Report'

						if(isset($postArray['reportColumns']) && !empty($postArray['reportColumns'])){
							$report_column_list = implode(",",$postArray['reportColumns']);
							$searchParam['report_column_list'] = $report_column_list;
						}
						
						// @$filterIdArray: column ids on which filters are appiled through 'Run Report'

						if(isset($postArray['filterIds']) && !empty($postArray['filterIds'])){
							$filterIdArray = (array)$postArray['filterIds'];
						}else{
							$filterIdArray = array();
						}

						// @$filterValuesArray: associaive array of column ids of posted filters and values

						if(isset($postArray['filterValues']) && !empty($postArray['filterValues'])){
							$filterValuesArray = (array)$postArray['filterValues'];							
						}else{
							$filterValuesArray = array();
						}
						
						// @$filter_condition: returns where condition

						if(!empty($filterIdArray)){
							$conditionalColumn = '107';
							$filterIdArray1 = $filterIdArray; 
							unset($filterIdArray[$conditionalColumn]);
							$filterIdArray2 = array_diff_assoc($filterIdArray1,$filterIdArray); 
							
							$filter_condition1 = $this->getReportFilterConditon($reportId,$filterIdArray,$filterValuesArray,$mstReportId);
							$searchParam['filter_condition1'] = $filter_condition1;
							
							$filter_condition2 = $this->getReportFilterConditon($reportId,$filterIdArray2,$filterValuesArray,$mstReportId,$conditionalColumn);
							$searchParam['filter_condition2'] = $filter_condition2;
						}
						$searchParam['mst_report_id'] = $postArray['mst_report_id'];

						$searchReportArr = $report->getReportInfoTransactionDetailArray($searchParam);

						$countResult = $this->_reportTable->getReportInfoTransactionDetailSlvCount($searchReportArr);

						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportInfoTransactionDetailSlv($searchReportArr);
						
					}else{
						$searchReportArr = $report->getReportInfoTransactionDetailArr($searchParam);

						$countResult = $this->_reportTable->getReportInfoTransactionDetailCount($searchReportArr);

						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportInfoTransactionDetail($searchReportArr);
					}
                                                            
                } elseif ($reportResult[0]['mst_report_id'] == 14) {

					if(isset($postArray['isRunOnly']) && $postArray['isRunOnly']=='1'){
						
						##################################### Applied over 'RUN REPORT' ###################################

						// @$report_column_list: columns to fetch from DB Table to be displayed in grid through 'Run Report'

						if(isset($postArray['reportColumns']) && !empty($postArray['reportColumns'])){
							$report_column_list = implode(",",$postArray['reportColumns']);
							$searchParam['report_column_list'] = $report_column_list;
						}
						
						// @$filterIdArray: column ids on which filters are appiled through 'Run Report'

						if(isset($postArray['filterIds']) && !empty($postArray['filterIds'])){
							$filterIdArray = (array)$postArray['filterIds'];
						}else{
							$filterIdArray = array();
						}

						// @$filterValuesArray: associaive array of column ids of posted filters and values

						if(isset($postArray['filterValues']) && !empty($postArray['filterValues'])){
							$filterValuesArray = (array)$postArray['filterValues'];							
						}else{
							$filterValuesArray = array();
						}
						
						// @$filter_condition: returns where condition

						if(!empty($filterIdArray)){
							$conditionalColumn = '168';
							$filterIdArray1 = $filterIdArray; 
							unset($filterIdArray[$conditionalColumn]);
							$filterIdArray2 = array_diff_assoc($filterIdArray1,$filterIdArray); 
							
							$filter_condition1 = $this->getReportFilterConditon($reportId,$filterIdArray,$filterValuesArray,$mstReportId);
							$searchParam['filter_condition1'] = $filter_condition1;

							
							$filter_condition2 = $this->getReportFilterConditon($reportId,$filterIdArray2,$filterValuesArray,$mstReportId,$conditionalColumn);
							$searchParam['filter_condition2'] = $filter_condition2;
						}

						$searchParam['mst_report_id'] = $postArray['mst_report_id'];

						$searchReportArr = $report->getReportInfoTransactionOverDueArray($searchParam);

						$countResults = $this->_reportTable->getReportInfoTransactionOverDueSlvCount($searchReportArr);

						$countResult = array();
						if(!empty($countResults)){
							$countResult[0]['RecordCount'] = count($countResults);
						}

						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportInfoTransactionOverDueSlv($searchReportArr);
					}else{
						$searchReportArr = $report->getReportInfoTransactionOverDueArr($searchParam);

						$countResults = $this->_reportTable->getReportInfoTransactionOverDueCount($searchReportArr);

						$countResult = array();
						if(!empty($countResults)){
							$countResult[0]['RecordCount'] = count($countResults);
						}

						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportInfoTransactionOverDue($searchReportArr);
					}
                    
                } elseif ($reportResult[0]['mst_report_id'] == 23) {

					if(isset($postArray['isRunOnly']) && $postArray['isRunOnly']=='1'){
						
						##################################### Applied over 'RUN REPORT' ###################################

						// @$report_column_list: columns to fetch from DB Table to be displayed in grid through 'Run Report'

						if(isset($postArray['reportColumns']) && !empty($postArray['reportColumns'])){
							$report_column_list = implode(",",$postArray['reportColumns']);
							$searchParam['report_column_list'] = $report_column_list;
						}
						
						// @$filterIdArray: column ids on which filters are appiled through 'Run Report'

						if(isset($postArray['filterIds']) && !empty($postArray['filterIds'])){
							$filterIdArray = (array)$postArray['filterIds'];
						}else{
							$filterIdArray = array();
						}

						// @$filterValuesArray: associaive array of column ids of posted filters and values

						if(isset($postArray['filterValues']) && !empty($postArray['filterValues'])){
							$filterValuesArray = (array)$postArray['filterValues'];							
						}else{
							$filterValuesArray = array();
						}
						
						// @$filter_condition: returns where condition

						if(!empty($filterIdArray)){
							$conditionalColumn = "'306','307'";
							$filterIdArray1 = $filterIdArray;
							unset($filterIdArray['306']);
							unset($filterIdArray['307']);
							$filterIdArray2 = array_diff_assoc($filterIdArray1,$filterIdArray); 
							
							$filter_condition1 = $this->getReportFilterConditon($reportId,$filterIdArray,$filterValuesArray,$mstReportId);
							$searchParam['filter_condition1'] = $filter_condition1;
							
							$filter_condition2 = $this->getReportFilterConditon($reportId,$filterIdArray2,$filterValuesArray,$mstReportId,$conditionalColumn);
							$searchParam['filter_condition2'] = $filter_condition2;
						}
						$searchParam['mst_report_id'] = $postArray['mst_report_id'];

						$searchReportArr = $report->getReportMembershipDetailArray($searchParam);

						$countResult = $this->_reportTable->getReportMembershipDetailSlvCount($searchReportArr);

						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportMembershipDetailSlv($searchReportArr);
						
					}else{
						$searchReportArr = $report->getReportMembershipDetailArr($searchParam);

						$countResult = $this->_reportTable->getReportMembershipDetailCount($searchReportArr);

						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportMembershipDetail($searchReportArr);
					}

                } elseif ($reportResult[0]['mst_report_id'] == 24) {

					if(isset($postArray['isRunOnly']) && $postArray['isRunOnly']=='1'){
						
						##################################### Applied over 'RUN REPORT' ###################################

						// @$report_column_list: columns to fetch from DB Table to be displayed in grid through 'Run Report'

						if(isset($postArray['reportColumns']) && !empty($postArray['reportColumns'])){
							$report_column_list = implode(",",$postArray['reportColumns']);
							$searchParam['report_column_list'] = $report_column_list;
						}
						
						// @$filterIdArray: column ids on which filters are appiled through 'Run Report'

						if(isset($postArray['filterIds']) && !empty($postArray['filterIds'])){
							$filterIdArray = (array)$postArray['filterIds'];
						}else{
							$filterIdArray = array();
						}

						// @$filterValuesArray: associaive array of column ids of posted filters and values

						if(isset($postArray['filterValues']) && !empty($postArray['filterValues'])){
							$filterValuesArray = (array)$postArray['filterValues'];							
						}else{
							$filterValuesArray = array();
						}
						
						// @$filter_condition: returns where condition

						if(!empty($filterIdArray)){
							$conditionalColumn = "320";
							$filterIdArray1 = $filterIdArray; 
							unset($filterIdArray[$conditionalColumn]);
							$filterIdArray2 = array_diff_assoc($filterIdArray1,$filterIdArray); 
							
							$filter_condition1 = $this->getReportFilterConditon($reportId,$filterIdArray,$filterValuesArray,$mstReportId);
							$searchParam['filter_condition1'] = str_replace("`","",$filter_condition1);

							
							$filter_condition2 = $this->getReportFilterConditon($reportId,$filterIdArray2,$filterValuesArray,$mstReportId,$conditionalColumn);
							$searchParam['filter_condition2'] = str_replace("`","",$filter_condition2);
						}

						$searchParam['mst_report_id'] = $postArray['mst_report_id'];

						$searchReportArr = $report->getReportMembershipSummaryArray($searchParam);

						$countResult = $this->_reportTable->getReportMembershipSummarySlvCount($searchReportArr);

						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportMembershipSummarySlv($searchReportArr);
						
					}else{
						$searchReportArr = $report->getReportMembershipSummaryArr($searchParam);

						$countResult = $this->_reportTable->getReportMembershipSummaryCount($searchReportArr);

						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportMembershipSummary($searchReportArr);
					}

                } elseif ($reportResult[0]['mst_report_id'] == 25) {

					if(isset($postArray['isRunOnly']) && $postArray['isRunOnly']=='1'){
						
						##################################### Applied over 'RUN REPORT' ###################################

						// @$report_column_list: columns to fetch from DB Table to be displayed in grid through 'Run Report'

						if(isset($postArray['reportColumns']) && !empty($postArray['reportColumns'])){
							$report_column_list = implode(",",$postArray['reportColumns']);
							$searchParam['report_column_list'] = $report_column_list;
						}
						
						// @$filterIdArray: column ids on which filters are appiled through 'Run Report'

						if(isset($postArray['filterIds']) && !empty($postArray['filterIds'])){
							$filterIdArray = (array)$postArray['filterIds'];
						}else{
							$filterIdArray = array();
						}

						// @$filterValuesArray: associaive array of column ids of posted filters and values

						if(isset($postArray['filterValues']) && !empty($postArray['filterValues'])){
							$filterValuesArray = (array)$postArray['filterValues'];							
						}else{
							$filterValuesArray = array();
						}
						
						// @$filter_condition: returns where condition

						if(!empty($filterIdArray)){
							$conditionalColumn = "331";
							$filterIdArray1 = $filterIdArray; 
							unset($filterIdArray[$conditionalColumn]);
							$filterIdArray2 = array_diff_assoc($filterIdArray1,$filterIdArray); 
							
							$filter_condition1 = $this->getReportFilterConditon($reportId,$filterIdArray,$filterValuesArray,$mstReportId);
							$searchParam['filter_condition1'] = str_replace("`","",$filter_condition1);

							
							$filter_condition2 = $this->getReportFilterConditon($reportId,$filterIdArray2,$filterValuesArray,$mstReportId,$conditionalColumn);
							$searchParam['filter_condition2'] = str_replace("`","",$filter_condition2);
						}

						$searchParam['mst_report_id'] = $postArray['mst_report_id'];

						$searchReportArr = $report->getReportLapsedMembershipArray($searchParam);

						$countResult = $this->_reportTable->getReportLapsedMembershipSlvCount($searchReportArr);

						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportLapsedMembershipSlv($searchReportArr);

						
					}else{
						$searchReportArr = $report->getReportLapsedMembershipArr($searchParam);

						$countResult = $this->_reportTable->getReportLapsedMembershipCount($searchReportArr);

						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportLapsedMembership($searchReportArr);
						
					}

                } elseif ($reportResult[0]['mst_report_id'] == 26) {

					if(isset($postArray['isRunOnly']) && $postArray['isRunOnly']=='1'){
						
						##################################### Applied over 'RUN REPORT' ###################################

						// @$report_column_list: columns to fetch from DB Table to be displayed in grid through 'Run Report'

						if(isset($postArray['reportColumns']) && !empty($postArray['reportColumns'])){
							$report_column_list = implode(",",$postArray['reportColumns']);
							$searchParam['report_column_list'] = $report_column_list;
						}
						
						// @$filterIdArray: column ids on which filters are appiled through 'Run Report'

						if(isset($postArray['filterIds']) && !empty($postArray['filterIds'])){
							$filterIdArray = (array)$postArray['filterIds'];
						}else{
							$filterIdArray = array();
						}

						// @$filterValuesArray: associaive array of column ids of posted filters and values

						if(isset($postArray['filterValues']) && !empty($postArray['filterValues'])){
							$filterValuesArray = (array)$postArray['filterValues'];							
						}else{
							$filterValuesArray = array();
						}
						
						// @$filter_condition: returns where condition

						if(!empty($filterIdArray)){
							$conditionalColumn = "334";
							$filterIdArray1 = $filterIdArray; 
							unset($filterIdArray[$conditionalColumn]);
							$filterIdArray2 = array_diff_assoc($filterIdArray1,$filterIdArray); 
							
							$filter_condition1 = $this->getReportFilterConditon($reportId,$filterIdArray,$filterValuesArray,$mstReportId);
							$searchParam['filter_condition1'] = str_replace("`","",$filter_condition1);

							
							$filter_condition2 = $this->getReportFilterConditon($reportId,$filterIdArray2,$filterValuesArray,$mstReportId,$conditionalColumn);
							$searchParam['filter_condition2'] = str_replace("`","",$filter_condition2);
						}

						$searchParam['mst_report_id'] = $postArray['mst_report_id'];

						$searchReportArr = $report->getProductRecordArray($searchParam);

						$countResult = $this->_reportTable->getReportProductReportSlvCount($searchReportArr);

						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportProductReportSlv($searchReportArr);
						
					}else{
						$searchReportArr = $report->getProductRecordArr($searchParam);
						$countResult = $this->_reportTable->getReportProductReportCount($searchReportArr);
						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportProductReport($searchReportArr);
					}
					
                } elseif ($reportResult[0]['mst_report_id'] == 36) {
					
					if(isset($postArray['isRunOnly']) && $postArray['isRunOnly']=='1'){

						##################################### Applied over 'RUN REPORT' ###################################

						
						// @$filterIdArray: column ids on which filters are appiled through 'Run Report'

						if(isset($postArray['filterIds']) && !empty($postArray['filterIds'])){
							$filterIdArray = (array)$postArray['filterIds'];
						}else{
							$filterIdArray = array();
						}

						// @$filterValuesArray: associaive array of column ids of posted filters and values

						if(isset($postArray['filterValues']) && !empty($postArray['filterValues'])){
							$filterValuesArray = (array)$postArray['filterValues'];							
						}else{
							$filterValuesArray = array();
						}
						
						// @$filter_condition: returns where condition

						$filter_condition = $this->getReportFilterConditon($reportId,$filterIdArray,$filterValuesArray,$mstReportId);
						
						$searchParam['filter_condition'] = $filter_condition;

						$searchReportArr = $report->getReportReconcillationArray($searchParam);
						$seachResult = $this->_reportTable->getReportReconcillationSlv($searchReportArr);

						###############################################################################################
					}else{

						########################### Applied over 'SAVE AND RUN REPORT'  ###############################

						$searchReportArr = $report->getReportReconcillationArr($searchParam);
						$seachResult = $this->_reportTable->getReportReconcillation($searchReportArr);	
						
						###############################################################################################
					}
                    
                } elseif ($reportResult[0]['mst_report_id'] == 37) {

					if(isset($postArray['isRunOnly']) && $postArray['isRunOnly']=='1'){

						##################################### Applied over 'RUN REPORT' ###################################

						// @$report_column_list: columns to fetch from DB Table to be displayed in grid through 'Run Report'

						if(isset($postArray['reportColumns']) && !empty($postArray['reportColumns'])){
							$report_column_list = implode(",",$postArray['reportColumns']);
							$searchParam['report_column_list'] = $report_column_list;
						}
						
						// @$filterIdArray: column ids on which filters are appiled through 'Run Report'

						if(isset($postArray['filterIds']) && !empty($postArray['filterIds'])){
							$filterIdArray = (array)$postArray['filterIds'];
						}else{
							$filterIdArray = array();
						}

						// @$filterValuesArray: associaive array of column ids of posted filters and values

						if(isset($postArray['filterValues']) && !empty($postArray['filterValues'])){
							$filterValuesArray = (array)$postArray['filterValues'];							
						}else{
							$filterValuesArray = array();
						}
						
						// @$filter_condition: returns where condition

						$filter_condition = $this->getReportFilterConditon($reportId,$filterIdArray,$filterValuesArray,$mstReportId);
						
						$searchParam['filter_condition'] = $filter_condition;

						$searchReportArr = $report->getReportCampDetailArray($searchParam);

						$countResult = $this->_reportTable->getReportCampDetailSlvCount($searchReportArr);

						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportCampDetailSlv($searchReportArr);

						

						###############################################################################################
					}else{

						########################### Applied over 'SAVE AND RUN REPORT'  ###############################

						$searchReportArr = $report->getReportCampDetailArr($searchParam);

						$countResult = $this->_reportTable->getReportCampDetailCount($searchReportArr);

						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportCampDetail($searchReportArr);

						
						###############################################################################################
					}
                } else {

					if(isset($postArray['isRunOnly']) && $postArray['isRunOnly']=='1'){
						
						if(isset($postArray['reportColumns']) && !empty($postArray['reportColumns'])){
							$report_column_list = implode(",",$postArray['reportColumns']);
							$searchParam['report_column_list'] = $report_column_list;
						}
						$postArray['filterIds'] = (array)$postArray['filterIds'];
						if(isset($postArray['filterIds']) && !empty($postArray['filterIds'])){
							$filterIdArray = $postArray['filterIds'];
						}else{
							$filterIdArray = array();
						}
						$postArray['filterValues'] = (array)$postArray['filterValues'];
						
						if(isset($postArray['filterValues']) && !empty($postArray['filterValues'])){
							$filterValuesArray = $postArray['filterValues'];							
						}else{
							$filterValuesArray = array();
						}
						
						if(!empty($filterIdArray)){
							
							$filter_condition = $this->getReportFilterConditon($reportId,$filterIdArray,$filterValuesArray,$mstReportId);
						}else{
							$filter_condition = "1=1";
						}

						if(in_array($mstReportId,array(8,29,30))){
							$having_condition = $this->getReportHavingConditon($filterIdArray,$filterValuesArray,$mstReportId);
						}
						
						$searchParam['filter_condition'] = $filter_condition;
						$searchParam['mst_report_id'] = $postArray['mst_report_id'];
						$searchParam['order_by_condition'] = $postArray['OrderBy'];
						$searchParam['having_condition'] = $having_condition;
						
						$searchReportArr = $report->getReportInfoArray($searchParam);

						$countResult = $this->_reportTable->getReportInfoSlvCount($searchReportArr);

						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportInfoSlv($searchReportArr);

						
					}else{
						
						$searchReportArr = $report->getReportInfoArr($searchParam);

						$countResult = $this->_reportTable->getReportInfoCount($searchReportArr);
						
						if(!empty($countResult) && $countResult[0]['RecordCount']>'0')
							$seachResult = $this->_reportTable->getReportInfo($searchReportArr);					
						
					}
                }
                //$countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
                $jsonResult = array();
                $arrCell = array();
                $subArrCell = array();

                //$total = $countResult[0]->RecordCount;

				$total = $countResult[0]['RecordCount'];
				
                if ($isExport == "excel") {
                    if ($total > $chunksize) {
                        $number_of_pages = ceil($total / $chunksize);
                        $page_counter++;
                        $start = $chunksize * $page_counter - $chunksize;
                        $searchParam['start_index'] = $start;
                        $searchParam['page'] = $page_counter;                     
                    } else {
                        $number_of_pages = 1;
                        $page_counter++;
                    }
                } else {
                    $jsonResult['page'] = $request->getPost('page');
                    
					/*$jsonResult['records'] = $countResult[0]->RecordCount;
                    $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);*/

					$jsonResult['records'] = $countResult[0]['RecordCount'];
                    $jsonResult['total'] = ceil($countResult[0]['RecordCount'] / $limit);
                }
                if (!empty($seachResult)) {                   
                    $arrExport = array();
                    foreach ($seachResult as $result) {
                        $cellData = array();
                        foreach ($result as $val) {
							if(strpos($val,'<img')){
								preg_match_all('/<img[^>]+>/i',$val, $result);
								foreach($result[0] as $k1=>$v1){
									$img = str_replace('<img', '<img width="100px"',$val);
									$cellData[] = $img;
								}
							}else{
								$cellData[] = $val;
							}                            
                        }
                        if ($isExport == "excel") {
                            $arrExport[] = $result;
                        }
                        $arrCell['cell'] = $cellData;
                        $subArrCell[] = $arrCell;
                        $jsonResult['rows'] = $subArrCell;
                    }
                    if ($isExport == "excel") {
                        $this->arrayToCsv($arrExport, $filename, $page_counter);
                        $arrExport = array();
                    }
                } else {
                    $jsonResult['rows'] = array();
                }
            } while ($page_counter <= $number_of_pages && $isExport == "excel");
            if ($isExport == "excel") {
                $this->downloadSendHeaders($filename);
                die;
            } else {
                return $response->setContent(\Zend\Json\Json::encode($jsonResult));
            }
        } else {
            return $this->redirect()->toRoute('dashboard');
        }
    }

    /**
     * This action is used to save report info
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function saveReportAction() {
		
        $this->checkUserAuthentication();
        $this->getReportTable();
        $reportCreateMessages = $this->_config['report_messages']['config']['report_create_message'];
        $commonMessages = $this->_config['report_messages']['config']['common_message'];
        $report = new Report($this->_adapter);
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $postDataArr = $request->getPost()->toArray();
			$reportId = $postDataArr['report_id'];
            $searchParam['report_id'] = $reportId;
            $searchParam['is_main_report'] = '0';
            $searchParam['user_id'] = $this->_auth->getIdentity()->crm_user_id;
            $searchReportArr = $report->getReportSearchArr($searchParam);
            $reportInfo = $this->_reportTable->getReportsByReportType($searchReportArr);
            $reportInfo = $reportInfo[0];
            $createReportArr = array();
            $createReportArr['report_id'] = $reportId;
            $createReportArr['mst_report_id'] = $reportInfo['mst_report_id'];
            $createReportArr['report_name'] = $postDataArr['report_title'];
            $createReportArr['description'] = $postDataArr['report_description'];
            $createReportArr['report_header'] = $postDataArr['report_header'];
            $createReportArr['report_footer'] = $postDataArr['report_footer'];
            $createReportArr['role_id'] = '';
            $createReportArr['is_main_report'] = '0';
            $createReportArr['report_category_id'] = $reportInfo['report_category_id'];
            $createReportArr['added_by'] = $this->_auth->getIdentity()->crm_user_id;
            $createReportArr['added_date'] = DATE_TIME_FORMAT;
            $createReportArr['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
            $createReportArr['modified_date'] = DATE_TIME_FORMAT;
            $isNewReport = 0;
			
			$save_or_update = 0;
			if(isset($postDataArr['update_report']) || isset($postDataArr['save_copy'])){
				$save_or_update = 1;
			}
			
			if (isset($postDataArr['update_report']) || (isset($postDataArr['run_report']))) {
                $createReportArr['action_type'] = 'update';
            } else {
                $isNewReport = 1;
                $createReportArr['action_type'] = 'add';
            }
			if((isset($postDataArr['run_report']) && $postDataArr['run_report']) || ($save_or_update == 1)){ 
				
				$createReportArr = $report->getCreateReportArr($createReportArr);
				$reportId = $this->_reportTable->saveReport($createReportArr);
			}
            if (!empty($reportId) && ((isset($postDataArr['run_report']) && $postDataArr['run_report']) ||  ($save_or_update == 1))) {
                $displaycolumnsArr = array();
                $displaycolumnsArr['report_id'] = $postDataArr['report_id'];
                $reportDisplayColumnsArr = $report->getReportDisplayColumnArr($displaycolumnsArr);
                $displayColumns = $this->_reportTable->getReportDisplayColumns($reportDisplayColumnsArr);
                $reportOrderyByColumnsArr = $report->getReportOrderyByColumnArr($displaycolumnsArr);
                $orderByColumns = $this->_reportTable->getReportOrderByColumns($reportOrderyByColumnsArr);
                $reportGroupByArr = $report->getReportGroupByArr($displaycolumnsArr);
                $groupByColumns = $this->_reportTable->getReportGroupBy($reportGroupByArr);
                $reportFilterValueColumnsArr = $report->getReportFilterValueColumnArr($displaycolumnsArr);
                $filterValueColumns = $this->_reportTable->getReportFilterValueColumns($reportFilterValueColumnsArr);
                if (!empty($filterValueColumns)) {
                    $filterIndex = 0;
                    foreach ($filterValueColumns as $filterColumn) {
                        $filterGroupArr = array();
                        $filterGroupArr['filter_group_id'] = $filterColumn['filter_group_id'];
                        $filterGroups = $report->getConditionsForGroupArr($filterGroupArr);
                        $groupFields = $this->_reportTable->getConditionsForGroup($filterGroups);
                        $groupConditionFields = array();
                        $groupShowFields = array();
                        if (!empty($groupFields)) {
                            foreach ($groupFields as $field) {
                                $groupConditionFields[$field['filter_id']] = $field['filter_pattern'];
                                $groupShowFields[$field['filter_id']] = $field['is_show_value'];
                            }
                        }
                        $filterValueColumns[$filterIndex]['filter_group_filters'] = $groupConditionFields;
                        $filterValueColumns[$filterIndex]['filter_field_show'] = $groupShowFields;
                        $filterIndex++;
                    }
                }
                $reportColumnsDetails = array();
                if (!empty($displayColumns)) {
                    $index = 0;
                    /* For display columns */
                    foreach ($displayColumns as $displaycolumnsArr) {
                        if (!empty($postDataArr['display_columns'])) {
                            if (in_array($displaycolumnsArr['column_id'], $postDataArr['display_columns'])) {
                                $selectedDisplayColumn = (in_array($displaycolumnsArr['column_id'], $postDataArr['display_columns'])) ? 'Y' : 'N';
                                $displayColumnRes = array();
                                $displayColumnRes['report_id'] = $reportId;
                                $displayColumnRes['column_id'] = $displaycolumnsArr['column_id'];
                                $displayColumnRes['selected_in_display'] = $selectedDisplayColumn;
                                $displayColumnRes['show_in_groupby'] = 'N';
                                $displayColumnRes['is_orderby'] = 'N';
                                $displayColumnRes['order_by'] = '';
                                $displayColumnRes['order_by_seq'] = '';
                                $displayColumnRes['filter_applied'] = 'N';
                                $displayColumnRes['filter_group_id'] = '';
                                $displayColumnRes['filter_id'] = '';
                                $displayColumnRes['filter_value'] = '';
                                $displayColumnRes['filter_pattern'] = '';
                                $displayColumnRes['is_having'] = 'N';
                                $reportColumnsDetails[$displaycolumnsArr['column_id']] = $displayColumnRes;
                            }
                        }
                    }


                    /* End For display columns */
                }


                if (!empty($orderByColumns)) {
                    //$seqOrder = 0;
                    /* For order by columns */
                    foreach ($orderByColumns as $orderColumnsArr) {
                        if (!empty($postDataArr['order_columns'])) {
                            if (in_array($orderColumnsArr['column_id'], $postDataArr['order_columns'])) {

                                $seqOrder = (array_search($orderColumnsArr['column_id'], $postDataArr['order_columns']));
                                $isOrderByColumn = 'Y';
                                $orderByColumn = $postDataArr['order_order'][$seqOrder];
                                $orderBySequence = $seqOrder + 1;

                                if (isset($reportColumnsDetails[$orderColumnsArr['column_id']])) {
                                    $reportColumnsDetails[$orderColumnsArr['column_id']]['is_orderby'] = $isOrderByColumn;
                                    $reportColumnsDetails[$orderColumnsArr['column_id']]['order_by'] = $orderByColumn;
                                    $reportColumnsDetails[$orderColumnsArr['column_id']]['order_by_seq'] = $orderBySequence;
                                } else {
                                    $displayColumnRes = array();
                                    $displayColumnRes['report_id'] = $reportId;
                                    $displayColumnRes['column_id'] = $orderColumnsArr['column_id'];
                                    $displayColumnRes['selected_in_display'] = 'N';
                                    $displayColumnRes['show_in_groupby'] = 'N';
                                    $displayColumnRes['is_orderby'] = $isOrderByColumn;
                                    $displayColumnRes['order_by'] = $orderByColumn;
                                    $displayColumnRes['order_by_seq'] = $orderBySequence;
                                    $displayColumnRes['filter_applied'] = 'N';
                                    $displayColumnRes['filter_group_id'] = '';
                                    $displayColumnRes['filter_id'] = '';
                                    $displayColumnRes['filter_value'] = '';
                                    $displayColumnRes['filter_pattern'] = '';
                                    $displayColumnRes['is_having'] = 'N';
                                    $reportColumnsDetails[$orderColumnsArr['column_id']] = $displayColumnRes;
                                }

                                //$seqOrder++;
                            }
                        }
                    }
                    /* End For order by columns */
                }

                if (!empty($groupByColumns)) {
                    //$seqOrder = 0;
                    /* For order by columns */
                    foreach ($groupByColumns as $groupColumns) {
                        if (!empty($postDataArr['group_by_columns'])) {
                            if (in_array($groupColumns['column_id'], $postDataArr['group_by_columns'])) {
                                if (isset($reportColumnsDetails[$groupColumns['column_id']])) {
                                    $reportColumnsDetails[$groupColumns['column_id']]['show_in_groupby'] = 'Y';
                                } else {
                                    $displayColumnRes = array();
                                    $displayColumnRes['report_id'] = $reportId;
                                    $displayColumnRes['column_id'] = $groupColumns['column_id'];
                                    $displayColumnRes['selected_in_display'] = 'N';
                                    $displayColumnRes['show_in_groupby'] = 'Y';
                                    $displayColumnRes['is_orderby'] = 'N';
                                    $displayColumnRes['order_by'] = '';
                                    $displayColumnRes['order_by_seq'] = '';
                                    $displayColumnRes['filter_applied'] = 'N';
                                    $displayColumnRes['filter_group_id'] = '';
                                    $displayColumnRes['filter_id'] = '';
                                    $displayColumnRes['filter_value'] = '';
                                    $displayColumnRes['filter_pattern'] = '';
                                    $displayColumnRes['is_having'] = 'N';
                                    $reportColumnsDetails[$groupColumns['column_id']] = $displayColumnRes;
                                }

                                //$seqOrder++;
                            }
                        }
                    }
                    /* End For order by columns */
                }

                if (!empty($filterValueColumns)) {
                    $index = 0;
                    /* For filter by columns */
                    foreach ($filterValueColumns as $filterColumnsArr) {
                        $filterApplied = (!empty($postDataArr['filter_columns'][$index])) ? 'Y' : 'N';
                        $filterGroupId = $filterColumnsArr['filter_group_id'];
                        $filterId = (!empty($postDataArr['filter_columns'][$index])) ? $postDataArr['filter_columns'][$index] : '';
                        if (!empty($filterId)) {
                            $filterFieldShow = $filterColumnsArr['filter_field_show'][$filterId];
                            $filterValueName = 'filter_value_' . $filterColumnsArr['column_id'];
                            $filterValue = ((isset($postDataArr[$filterValueName]) && $postDataArr[$filterValueName]!='') && !empty($filterId) && $filterFieldShow == 'Y') ? $postDataArr[$filterValueName] : '';
                            if ($filterGroupId == 8 && !empty($filterId) && !empty($filterValue)) {
                                $filterPattern = $filterColumnsArr['filter_group_filters'][$filterId];
                                $filterValStrPatt = "";
                                $filterValStr = "";
                                $filterCond = '';
                                if ($filterId == 38) {
                                    $filterCond = ' OR ';
                                } else if ($filterId == 39) {
                                    $filterCond = ' AND ';
                                }
                                foreach ($filterValue as $filtVal) {
                                    $filterValStrPatt.=str_replace('<VALUE>', addslashes($filtVal), $filterPattern) . $filterCond;
                                    $filterValStr.=addslashes($filtVal) . ",";
                                }
                                $filterValStrPatt = rtrim($filterValStrPatt, $filterCond);
                                $filterValStrPatt = '(' . $filterValStrPatt . ')';
                                $filterValStr = rtrim($filterValStr, ',');

                                $filterValue = $filterValStr;
                                $filterPattern = $filterValStrPatt;
                            } else if ($filterId == 12 || $filterId == 16) {
                                $filterPattern = $filterColumnsArr['filter_group_filters'][$filterId];
                                $fromBetween = $filterValueName . '_from';
                                $toBetween = $filterValueName . '_to';
                                if (!empty($postDataArr[$fromBetween]) && !empty($postDataArr[$toBetween])) {
                                    $filterValue = $postDataArr[$fromBetween] . '#####' . $postDataArr[$toBetween];
                                    $filterPattern = str_replace(array('<VALUE1>', '<VALUE2>'), array(addslashes($postDataArr[$fromBetween]), addslashes($postDataArr[$toBetween])), $filterPattern);
                                } else {
                                    $filterValue = '';
                                    $filterPattern = '';
                                }
                            } else if ($filterFieldShow == 'N') {
                                $filterPattern = $filterColumnsArr['filter_group_filters'][$filterId];
                            } else if (!empty($filterId) && $filterValue != '') {
                                $filterPattern = $filterColumnsArr['filter_group_filters'][$filterId];
                                if (is_array($postDataArr[$filterValueName]) && !empty($postDataArr[$filterValueName])) {
                                    $filterValStrPatt = "";
                                    $filterValStr = "";
                                    foreach ($filterValue as $filtVal) {
                                        $filterValStrPatt.="'" . addslashes($filtVal) . "',";
                                        $filterValStr.=addslashes($filtVal) . ",";
                                    }
                                    $filterValStrPatt = rtrim($filterValStrPatt, ',');
                                    $filterValStr = rtrim($filterValStr, ',');
                                    $filterValue = $filterValStr;
                                    $filterPattern = str_replace('<VALUE>', $filterValStrPatt, $filterPattern);
                                } else {
                                    $filterPattern = str_replace('<VALUE>', addslashes($filterValue), $filterPattern);
                                }
                            } else if ($filterColumnsArr['column_type'] == 4 && $filterFieldShow == 'Y') {
                                $filterPattern = $filterColumnsArr['filter_group_filters'][$filterId];
                                $fromDateName = $filterValueName . '_from';
                                $toDateName = $filterValueName . '_to';
                                $fromDate = (!empty($postDataArr[$fromDateName])) ? $postDataArr[$fromDateName] : '';
                                $toDate = (!empty($postDataArr[$toDateName])) ? $postDataArr[$toDateName] : '';
                                if (!empty($fromDate) && !empty($toDate)) {
                                    $filterValue = $fromDate . ',' . $toDate;
                                    $dbFromDate = $this->DateFormat($fromDate, 'db_date_format');
                                    $dbToDate = $this->DateFormat($toDate, 'db_date_format');
                                    $filterPattern = str_replace(array('<VALUE1>', '<VALUE2>'), array($dbFromDate, $dbToDate), $filterPattern);
                                } else {
                                    $filterValue = '';
                                    $filterPattern = '';
                                }
                            }
                            else
                                $filterPattern = '';

                            if (($filterApplied != 'N' && $filterId != '' && $filterValue != '') || ($filterApplied != 'N' && $filterId != '' && $filterValue == '' && $filterFieldShow == 'N')) {
                                if (isset($reportColumnsDetails[$filterColumnsArr['column_id']])) {
                                    $reportColumnsDetails[$filterColumnsArr['column_id']]['filter_applied'] = $filterApplied;
                                    $reportColumnsDetails[$filterColumnsArr['column_id']]['filter_group_id'] = $filterGroupId;
                                    $reportColumnsDetails[$filterColumnsArr['column_id']]['filter_id'] = $filterId;
                                    $reportColumnsDetails[$filterColumnsArr['column_id']]['filter_value'] = $filterValue;
                                    $reportColumnsDetails[$filterColumnsArr['column_id']]['filter_pattern'] = $filterPattern;
                                    $reportColumnsDetails[$filterColumnsArr['column_id']]['is_having'] = $filterColumnsArr['is_having'];
                                } else {
                                    $displayColumnRes = array();
                                    $displayColumnRes['report_id'] = $reportId;
                                    $displayColumnRes['column_id'] = $filterColumnsArr['column_id'];
                                    $displayColumnRes['selected_in_display'] = 'N';
                                    $displayColumnRes['show_in_groupby'] = 'N';
                                    $displayColumnRes['is_orderby'] = 'N';
                                    $displayColumnRes['order_by'] = '';
                                    $displayColumnRes['order_by_seq'] = '';
                                    $displayColumnRes['filter_applied'] = $filterApplied;
                                    $displayColumnRes['filter_group_id'] = $filterGroupId;
                                    $displayColumnRes['filter_id'] = $filterId;
                                    $displayColumnRes['filter_value'] = $filterValue;
                                    $displayColumnRes['filter_pattern'] = $filterPattern;
                                    $displayColumnRes['is_having'] = $filterColumnsArr['is_having'];
                                    $reportColumnsDetails[$filterColumnsArr['column_id']] = $displayColumnRes;
                                }
                            }
                        }
                        $index++;
                    }
                    /* End For filter by columns */
                }

                /* delete report roles */
                $deleteReportRoleArr = array();
                $deleteReportRoleArr['report_id'] = $reportId;
                $deleteReportRoleDataArr = $report->getDeleteReportRoleArr($deleteReportRoleArr);
                $this->_reportTable->deleteReportRoles($deleteReportRoleDataArr);
                /* end delete report roles */


                /* Add roles for this report */
                if (!empty($postDataArr['role_id'])) {
                    foreach ($postDataArr['role_id'] as $userRole) {
                        $addReportRoleArr = array();
                        $addReportRoleArr['report_id'] = $reportId;
                        $addReportRoleArr['role_id'] = $userRole[0];
                        $addReportRoleDataArr = $report->getAddReportRoleArr($addReportRoleArr);
                        $this->_reportTable->saveReportRole($addReportRoleDataArr);
                    }
                }
                /* End add roles for this report */

                /* delete report columns */
                $deleteReportColumnArr = array();
                $deleteReportColumnArr['report_id'] = $reportId;
                $deleteReportColumnsArr = $report->getDeleteReportColumnsArr($deleteReportColumnArr);
                $this->_reportTable->deleteReportColumns($deleteReportColumnsArr);
                /* end delete report columns */

                /* add report columns */
                if (!empty($reportColumnsDetails)) {
                    foreach ($reportColumnsDetails as $reportColumn) {
                        $createReportColumnsArr = $report->getCreateReportColumnsArr($reportColumn);
                        $reportColumnId = $this->_reportTable->saveReportColumns($createReportColumnsArr);
                    }
                }

                /* end add report columns */

                $message = ($isNewReport) ? $reportCreateMessages['REPORT_CREATED_SUCCESSFULLY'] : $reportCreateMessages['REPORT_UPDATED_SUCCESSFULLY'];
                //$this->flashMessenger()->addMessage($reportCreateMessages['REPORT_CREATED_SUCCESSFULLY']);
                $messages = array('status' => "success", 'message' => $message, 'reportId' => $reportId,  'isRunOnly' => '0');
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }elseif(isset($postDataArr['save_and_run_report'])){
				$messages = array('status' => "success", 'reportId' => $reportId, 'isRunOnly' => '1');
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
			} else {
                $messages = array('status' => "error", 'message' => $reportCreateMessages['REPORT_CREATED_FAIL']);
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        } else {
            return $this->redirect()->toRoute('dashboard');
        }
    }

    /**
     * This action is used to delete report
     * @param  void
     * @return json format string
     * @author Icreon Tech - DT
     */
    public function deleteReportAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $reportId = $request->getPost('reportId');
            if ($reportId != '') {
                $reportId = $this->decrypt($reportId);
                $postArr = $request->getPost();
                $postArr['report_id'] = $reportId;
                $this->getReportTable();
                $reportListMessages = $this->_config['report_messages']['config']['report_list_message'];
                $commonMessages = $this->_config['report_messages']['config']['common_message'];
                $report = new Report($this->_adapter);
                $response = $this->getResponse();
                $searchParam['report_id'] = $reportId;
                $searchParam['is_main_report'] = '0';
                $searchParam['user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $searchReportArr = $report->getReportSearchArr($searchParam);
                $seachResult = $this->_reportTable->getReportsByReportType($searchReportArr);
                $message = $seachResult[0]['report_name'] . ' successfully deleted !';
                $reportArr = $report->getArrayForRemoveReport($postArr);
                $this->_reportTable->removeReportById($reportArr);

                $this->flashMessenger()->addMessage($message);
                $messages = array('status' => "success", 'message' => $message);
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                return $this->redirect()->toRoute('dashboard');
            }
        } else {
            return $this->redirect()->toRoute('dashboard');
        }
    }

    public function getReloadGridInfoAction() {
		$this->checkUserAuthentication();
        $this->getReportTable();
        $report = new Report($this->_adapter);
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $response = $this->getResponse();
            $postArray = $request->getPost()->toArray();
			if($request->getPost('isRunOnly')){
				parse_str($request->getPost('data'),$postedData);
			}
			
            $reportId = $postArray['report_id'];
            $displaycolumnsArr = array();
            $displaycolumnsArr['report_id'] = $reportId;
            $reportDisplayColumnsArr = $report->getReportDisplayColumnArr($displaycolumnsArr);
            $displayColumns = $this->_reportTable->getReportDisplayColumns($reportDisplayColumnsArr);
			
			$filterValues = array();
			$reportColumnList  = array();
			$group_by_column_id = array();
			$mst_report_id = '';

		
			
			if(isset($postArray['isRunOnly']) && $postArray['isRunOnly'] == 1){ 

				// Extract the columns to be displayed from the posted data

				$isAnySelectedColumn = 0;

				$displayColumnList = array();

				$reportColumnList = array();
				
							
				foreach($displayColumns as $key=>$value){
					if(in_array($value['column_id'],$postedData['display_columns'])){
						$displayColumns[$key]['rep_column_id'] = $value['column_id'];
						$isAnySelectedColumn = 1;
						$displayColumnList[] = $value['display_column_name'];
						
						if($value['mst_report_id']==9){
							if(isset($postedData['filter_columns']) && $postedData['filter_columns'][0]!=''){
								$displayColumnList[] = 'Initial Date Range';
							}
							if(isset($postedData['filter_columns']) && $postedData['filter_columns'][1]!=''){
								$displayColumnList[] = 'Second Date Range';
							}
							
							if(isset($postedData['filter_columns']) && $postedData['filter_columns'][0]!='' && $postedData['filter_columns'][1]!=''){
								$displayColumnList[] = '% Change';
							}
						}

						$reportColumnList[] = $value['sql_column_name']." As '".$value['display_column_name']."'";

						$mst_report_id = $value['mst_report_id'];
					}					
				}				

				if(!empty($displayColumnList))
					$displaySearchColumnArr = $displayColumnList;
				else
					$displaySearchColumn = array();

				// filterValues

				$filterValues = array();
				
				foreach($postedData as $key1=>$value1){
					if($value1!=''){
						if(preg_match('/^filter_value_/',$key1)){
							$str = str_replace('filter_value_','',$key1);
							if(is_array($value1))
								$filterValues[$str] = "'".implode("','",$value1)."'";
							else
								$filterValues[$str] = $value1;
						}
					}
				}

				ksort($filterValues);
				
				if(isset($postedData['group_by_columns'])){
					$group_by_column_id = "'".implode("','",$postedData['group_by_columns'])."'";
				}
			}else{

				// Extract the columns to be displayed from the saved report

				$isAnySelectedColumn = 0;
				if (!empty($displayColumns)) {
					foreach ($displayColumns as $dispColumn) {
						if ($dispColumn['column_id'] == $dispColumn['rep_column_id']) {
							$isAnySelectedColumn = 1;
						}
					}
					$searchParam[0] = $postArray['report_id'];
					$seachResult = $this->_reportTable->getReportColumnInfo($searchParam);
					$displaySearchColumnArr = explode(',', $seachResult[0]['report_column_list']);					
				} else {
					$displaySearchColumn = array();
				}				
			}
            
            $displayColumnsArr = array();
            $displaySelectedColumnArr = array();
            $gridColumns = array();
            $gridColModelsArr = array();
            $indexUsed = array();
            if (!empty($displayColumns)) {
				if($request->getPost('isRunOnly') == 1){
					foreach ($displayColumns as $displayColumn) {
						if(in_array($displayColumn['display_column_name'],$displaySearchColumnArr)){
							$displayColumnsArr[$displayColumn['column_id']] = $displayColumn['display_column_name'];
							if ($displayColumn['column_id'] == $displayColumn['rep_column_id']) {
								$displaySelectedColumnArr[] = $displayColumn['column_id'];
								if (!empty($displaySearchColumnArr)) {
									$indexOfColumn = array_search($displayColumn['display_column_name'], $displaySearchColumnArr);
									$gridColumns[$indexOfColumn] = $displayColumn['display_column_name'];
									$gridColModelsArr[$indexOfColumn] = array('name' => $displayColumn['display_column_name'], 'index' => $displayColumn['sql_column_name'], 'sortable' => false, 'cmTemplate' => array('title' => true), 'classes' => '');
									$indexUsed[] = $displayColumn['display_column_name'];
								} else {
									$gridColumns[] = $displayColumn['display_column_name'];
									$gridColModelsArr[] = array('name' => $displayColumn['display_column_name'], 'index' => $displayColumn['sql_column_name'], 'sortable' => false, 'cmTemplate' => array('title' => true), 'classes' => '');
								}
							}
						}
						
					}
				}else{
					foreach ($displayColumns as $displayColumn) {
						$displayColumnsArr[$displayColumn['column_id']] = $displayColumn['display_column_name'];
						if ($displayColumn['column_id'] == $displayColumn['rep_column_id']) {
							$displaySelectedColumnArr[] = $displayColumn['column_id'];
							if (!empty($displaySearchColumnArr)) {
								$indexOfColumn = array_search($displayColumn['display_column_name'], $displaySearchColumnArr);
								$gridColumns[$indexOfColumn] = $displayColumn['display_column_name'];
								$gridColModelsArr[$indexOfColumn] = array('name' => $displayColumn['display_column_name'], 'index' => $displayColumn['sql_column_name'], 'sortable' => false, 'cmTemplate' => array('title' => true), 'classes' => '');
								$indexUsed[] = $displayColumn['display_column_name'];
							} else {
								$gridColumns[] = $displayColumn['display_column_name'];
								$gridColModelsArr[] = array('name' => $displayColumn['display_column_name'], 'index' => $displayColumn['sql_column_name'], 'sortable' => false, 'cmTemplate' => array('title' => true), 'classes' => '');
							}
						}
					}
				}                
            }
			
            if (!empty($displaySearchColumnArr)) {
				foreach ($displaySearchColumnArr as $key => $dispCol) {
                    if (!in_array($dispCol, $gridColumns)) {
                        $gridColumns[] = $dispCol;
                        $gridColModelsArr[] = array('name' => $dispCol, 'index' => $dispCol, 'sortable' => false, 'cmTemplate' => array('title' => true), 'classes' => '');
                    }
                }
            }
			$reportOrderyByColumnsArr = $report->getReportOrderyByColumnArr($displaycolumnsArr);
            $orderByColumns = $this->_reportTable->getReportOrderByColumns($reportOrderyByColumnsArr);
			$orderColumnsArr = array('' => 'Select');
            $orderSelectedColumnArr = array();
            if (!empty($orderByColumns)) {
                foreach ($orderByColumns as $orderColumn) {
                    $orderColumnsArr[$orderColumn['column_id']] = $orderColumn['display_column_name'];
                    if ($orderColumn['column_id'] == $orderColumn['rep_column_id']) {
                        $orderSelectedColumnArr[] = $orderColumn;
                    }
                }
            }
			$orderbyArray = array();
			$orderby = "";
			if(isset($postArray['isRunOnly']) && $postArray['isRunOnly']==1 && isset($postedData['order_columns']) && !empty($postedData['order_columns'])){
				$postedOrderIdArray = $postedData['order_columns'];
				foreach($postedOrderIdArray as $k=>$v){
					foreach($orderByColumns as $k1=>$v1){
						if($v==$v1['column_id']){
							$orderbyArray[] = $v1['sql_column_name'].' '.$postedData['order_order'][$k];
						}
					}
				}
				if(!empty($orderbyArray)){
					$orderby = implode(", ",$orderbyArray);
				}

				
			}
			
            $reportFilterValueColumnsArr = $report->getReportFilterValueColumnArr($displaycolumnsArr);
            $filterValueColumns = $this->_reportTable->getReportFilterValueColumns($reportFilterValueColumnsArr);
			
			
            if (!empty($filterValueColumns)) {
                $filterIndex = 0;
                foreach ($filterValueColumns as $filterColumn) {
                    $filterGroupArr = array();
                    $filterGroupArr['filter_group_id'] = $filterColumn['filter_group_id'];
                    $filterGroups = $report->getConditionsForGroupArr($filterGroupArr);
                    $groupFields = $this->_reportTable->getConditionsForGroup($filterGroups);
                    $groupConditionFields = array('' => 'Select');
                    if (!empty($groupFields)) {
                        foreach ($groupFields as $field) {
                            $groupConditionFields[$field['filter_id']] = $field['filter_name'];
                        }
                    }
                    $filterValueColumns[$filterIndex]['filter_group_filters'] = $groupConditionFields;					
                    $filterIndex++;
                }
            }
			
			$filterIds = array();
			if($request->getPost('isRunOnly') == 1 && isset($postedData['filter_columns']) && !empty($postedData['filter_columns'])){
				$filterIds = array();
				foreach($filterValueColumns as $fIndex=>$fcolumn){
					if($postedData['filter_columns'][$fIndex]!=''){
						$filterIds[$fcolumn['column_id']] = $postedData['filter_columns'][$fIndex];
					}
				}
			}
			
            /* Grid Info */
            $jsonResult = array();
            $jsonResult['gridBlockId'] = 'reportDetailGrids';
            $jsonResult['is_columns'] = $isAnySelectedColumn;
            $jsonResult['gridId'] = 'list_grid_info';
            $jsonResult['redirectUrlArr'] = array();
            $jsonResult['postDataArr'] = array();
            $jsonResult['colNamesArr'] = array();
            $jsonResult['colModelsArr'] = array();
            $jsonResult['headingGrid'] = array();
            $jsonResult['sortNameArr'] = array();
            $jsonResult['sortOrdArr'] = array();
			


            $jsonResult['redirectUrlArr'][] = '/get-report-grid-info';
            $postDataArr = array();
            $postDataArr['report_id'] = $reportId;
            $postDataArr['report_name'] = $reportId;
            $jsonResult['postDataArr'][] = $postDataArr;

            ksort($gridColumns);
            ksort($gridColModelsArr);
            $colNamesArr = array();
            if (!empty($gridColumns)) {
                foreach ($gridColumns as $col) {
                    $colNamesArr[] = $col;
                }
            }
            $colModelsArr = array();
            if (!empty($gridColModelsArr)) {
                foreach ($gridColModelsArr as $col) {
                    $colModelsArr[] = $col;
                }
            }
            $jsonResult['colNamesArr'][] = $colNamesArr;
            $jsonResult['colModelsArr'][] = $colModelsArr;
            $jsonResult['headingGrid'][] = '';
            $jsonResult['sortNameArr'][] = '';
            $jsonResult['sortOrdArr'][] = '';
			$jsonResult['filterValuesArr'][] = $filterValues;
			$jsonResult['reportColumnArr'][] = $reportColumnList;
			$jsonResult['mst_report_id'][] = $mst_report_id;
			$jsonResult['filterIdArr'][] = $filterIds;
			$jsonResult['OrderByArr'][] = $orderby;
			$jsonResult['isRunOnly'][] = $request->getPost('isRunOnly');
			$jsonResult['groupByColArr'][] = $group_by_column_id;
			
			return $response->setContent(\Zend\Json\Json::encode($jsonResult));
        }
        /* end Grid Info */
    }

    /**
     * This action is used to add report contacts to geroup
     * @param  void
     * @return json format string
     * @author Icreon Tech - DT
     */
    public function addReportContactsToGroupAction() {
        $this->checkUserAuthentication();
        $this->getReportTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $reportGroup = new ReportGroup();
        $reportCreateMessages = $this->_config['report_messages']['config']['report_create_message'];
        $commonMessages = $this->_config['report_messages']['config']['common_message'];
        $report = new Report($this->_adapter);
        $searchParam = array();
        $searchParam['group_type'] = 1;
        $seachResult = $this->getServiceLocator()->get('Group\Model\GroupTable')->getAllGroup($searchParam);
        $groupDetailArr = array('' => 'Select');
        if (!empty($seachResult)) {
            foreach ($seachResult as $groupDetail) {
                $groupDetailArr[$groupDetail['group_id']] = $groupDetail['name'];
            }
        }
        $reportGroup->get('group_id')->setAttribute('options', $groupDetailArr);

        if ($request->isPost() && $request->isXmlHttpRequest()) {

            $group = new Group($this->_adapter);

            $postArray = $request->getPost()->toArray();
            $postArray['group_type'] = '1';
            if ($postArray['create_add_group'] != 1) {
                $postArray['group_id'] = '';
                $group->exchangeArrayCreateGroup($postArray);
                $groupId = $this->getServiceLocator()->get('Group\Model\GroupTable')->saveGroup($group);
            } else {
                $groupId = $postArray['group_id'];
            }
            $postArray['group_id'] = $groupId;
            $group->exchangeArrayCreateGroup($postArray);

            $searchParam = array();
            $searchParam['report_id'] = $postArray['report_id'];

            $getReportContactGroupArr = $report->getReportContactGroup($searchParam);
            switch ($searchParam['report_id']) {
                case '3' : $reportContactsGroup = $this->_reportTable->getReportInfoContactTransaction($getReportContactGroupArr);
                    break;
                case '4' : $reportContactsGroup = $this->_reportTable->getReportInfoContactReport($getReportContactGroupArr);
                    break;
                case '5' : $reportContactsGroup = $this->_reportTable->getReportInfoTopDonation($getReportContactGroupArr);
                    break;
                case '6' : $reportContactsGroup = $this->_reportTable->getReportInfoSybunt($getReportContactGroupArr);
                    break;
                case '7' : $reportContactsGroup = $this->_reportTable->getReportInfoLybunt($getReportContactGroupArr);
                    break;
                case '8' : $reportContactsGroup = $this->_reportTable->getReportInfoTransactionSummary($getReportContactGroupArr);
                    break;
                case '9' : $reportContactsGroup = $this->_reportTable->getReportRepeatTransaction($getReportContactGroupArr);
                    break;
                case '10' : $reportContactsGroup = $this->_reportTable->getReportInfoTransactionDetail($getReportContactGroupArr);
                    break;
                case '14' : $reportContactsGroup = $this->_reportTable->getReportInfoTransactionOverDue($getReportContactGroupArr);
                    break;
                case '23' : $reportContactsGroup = $this->_reportTable->getReportMembershipDetail($getReportContactGroupArr);
                    break;
                case '24' : $reportContactsGroup = $this->_reportTable->getReportMembershipSummary($getReportContactGroupArr);
                    break;
                case '25' : $reportContactsGroup = $this->_reportTable->getReportLapsedMembership($getReportContactGroupArr);
                    break;
                default : $reportContactsGroup = $this->_reportTable->getReportInfo($getReportContactGroupArr);
                    break;
            }
            //$reportContactsGroup = $this->_reportTable->getReportContactGroup($getReportContactGroupArr);
            if (!empty($reportContactsGroup)) {
                $contactIds = '';
                foreach ($reportContactsGroup as $contactId) {
                    $contactIds.=$contactId['Contact ID'] . ',';
                }
                $contactIds = rtrim($contactIds, ',');
                $searchParam = array();
                $searchParam['contact_ids'] = $contactIds;
                $getUserIdsByContactIdsArr = $report->getUserIdsByContactIds($searchParam);
                $insertData[] = $getUserIdsByContactIdsArr[0];
                $insertData[] = $group->group_id;
                $getUserIdsByContactIds = $this->_reportTable->getUserIdsByContactIds($insertData);

                /** insert into the change log */
                $changeLogArray = array();
                $changeLogArray['table_name'] = 'tbl_grp_change_logs';
                $changeLogArray['activity'] = (isset($group->group_id)) ? '2' : '1';/** 1 == for insert */
                $changeLogArray['id'] = $groupId;
                $changeLogArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
            }
            $messages = array('status' => "success", 'message' => $reportCreateMessages['CONTACTS_TO_GROUP'] . " " . $reportCreateMessages['ADDED_SUCCESSFULLY']);
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        } else {
            $this->layout('popup');
            $params = $this->params()->fromRoute();
            $reportId = $this->decrypt($params['report_id']);
            $reportGroup->get('report_id')->setValue($reportId);
            $viewModel = new ViewModel();
            $viewModel->setVariables(array(
                'jsLangTranslate' => array_merge($reportCreateMessages, $commonMessages),
                'reportGroup' => $reportGroup
            ));
            return $viewModel;
        }
    }

    public function createExcelAction() {
        $this->checkUserAuthentication();
        $this->getReportTable();
        $report = new Report($this->_adapter);
        $params = $this->params()->fromRoute();
        $reportId = $this->decrypt($params['report_id']);
        $searchParam['report_id'] = $reportId;
        $searchParam['start_index'] = 0;
        $searchParam['record_limit'] = 10;
        $searchReportArr = $report->getReportReconcillationArr($searchParam);
        $seachResult = $this->_reportTable->getReportReconcillation($searchReportArr);
		$this->downloadExcel($seachResult);
    }

    public function downloadExcel($seachResult) {
        $objPHPExcel = new \PHPExcel();
        $createWallOfHonorMessage = $this->_config['transaction_messages']['config']['wall_of_honour'];
        $filename = "reconcillation-report" . time() . ".xls";

        // Set document properties
        $objPHPExcel->getProperties()->setCreator("SOLEIF CRM")
                ->setLastModifiedBy("SOLEIF CRM")
                ->setTitle("Office 2007 XLSX WOH Document")
                ->setSubject("Office 2007 XLSX WOH Document")
                ->setDescription("Wall of Honor document for Office 2007 XLSX, generated by SOLEIF CRM.")
                ->setKeywords("office 2007 openxml php")
                ->setCategory("Wall of Honor");

        $batch_no = $seachResult[0]['Batch ID'];
        $gl_code = $seachResult[0]['GL Code'];
        $prog = $seachResult[0]['Program'];
        $dept = $seachResult[0]['Department'];
        $total = 0;
        $discount = 0;
        $tax = 0;
        $shipping = 0;
        $desg = '';
        $res = '';
        $allc = '';
        $traDate = '';
        $setDate = '';
        $glCodeData = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getGLCodes();

        $k = 1;

        $str = $objPHPExcel->setActiveSheetIndex();
        $str->setCellValue("A" . $k, "Batch ID");
        $str->setCellValue("B" . $k, "Gl Code");
        $str->setCellValue("C" . $k, "Department");
        $str->setCellValue("D" . $k, "Program");
        $str->setCellValue("E" . $k, "Designation");
        $str->setCellValue("F" . $k, "Restriction");
        $str->setCellValue("G" . $k, "Allocation");
        $str->setCellValue("H" . $k, "Transaction Date");
        $str->setCellValue("I" . $k, "Settlement Date");
        $str->setCellValue("J" . $k, "Total");
        foreach ($seachResult as $key => $val) {
            if ($batch_no == $val['Batch ID'] && $gl_code == $val['GL Code'] && $prog == $val['Program'] && $dept == $val['Department']) {
                $total = $total + $val['Amount'];
                $discount = $discount + $val['product_discount'];
                $tax = $tax + $val['product_tax'];
                $shipping = $shipping + ($val['product_shipping'] + $val['product_handling']);

                if (isset($val['Designation']) && !empty($val['Designation'])) {
                    $desg = $val['Designation'];
                }
                if (isset($val['Restriction'])) {
                    $res = $val['Restriction'];
                }
                if (isset($val['Alloctaion']) && !empty($val['Alloctaion'])) {
                    $allc = $val['Alloctaion'];
                }
                if(isset($val['Transaction Date']) && !empty($val['Transaction Date'])){
                $tranDate[] = substr($val['Transaction Date'],0,-9);
                }
                if(isset($val['settlement_date']) && !empty($val['settlement_date'])){
                $settDate[] = substr($val['settlement_date'],0,-9);
                }                
            } else {
				if(isset($val['Transaction Date']) && !empty($val['Transaction Date'])){
                $tranDate[] = substr($val['Transaction Date'],0,-9);
                }
                if(isset($val['settlement_date']) && !empty($val['settlement_date'])){
                $settDate[] = substr($val['settlement_date'],0,-9);
                }
				if(!empty($tranDate))
					$traDate = implode(', ', array_unique($tranDate));
				if(!empty($settDate))
					$setDate = implode(', ', array_unique($settDate));
                if ($total > 0) {
                    $k++;
                    $str = $objPHPExcel->setActiveSheetIndex();
                    $str->setCellValue("A" . $k, $batch_no);
                    $str->setCellValue("B" . $k, $gl_code);
                    $str->setCellValue("C" . $k, $dept);
                    $str->setCellValue("D" . $k, $prog);
                    $str->setCellValue("E" . $k, $desg);
                    $str->setCellValue("F" . $k, $res);
                    $str->setCellValue("G" . $k, $allc);
                    $str->setCellValue("H" . $k, $traDate);
                    $str->setCellValue("I" . $k, $setDate);
                    $str->setCellValue("J" . $k, $this->Currencyformat($total));
                }
                if ($discount > 0) {
                    $k++;
                    $str = $objPHPExcel->setActiveSheetIndex();
                    $str->setCellValue("A" . $k, $batch_no);
                    $str->setCellValue("B" . $k, $glCodeData[0]['gl_code_value']);
                    $str->setCellValue("C" . $k, $glCodeData[0]['department']);
                    $str->setCellValue("D" . $k, $prog);
                    $str->setCellValue("E" . $k, $glCodeData[0]['designation']);
                    $str->setCellValue("F" . $k, $glCodeData[0]['is_restriction']);
                    $str->setCellValue("G" . $k, $glCodeData[0]['allocation']);
                    $str->setCellValue("H" . $k, $traDate);
                    $str->setCellValue("I" . $k, $setDate);
                    $str->setCellValue("J" . $k, "(" . $this->Currencyformat($discount) . ")");
                }
                if ($tax > 0) {
                    $k++;
                    $str = $objPHPExcel->setActiveSheetIndex();
                    $str->setCellValue("A" . $k, $batch_no);
                    $str->setCellValue("B" . $k, $glCodeData[3]['gl_code_value']);
                    $str->setCellValue("C" . $k, $glCodeData[3]['department']);
                    $str->setCellValue("D" . $k, $prog);
                    $str->setCellValue("E" . $k, $glCodeData[3]['designation']);
                    $str->setCellValue("F" . $k, $glCodeData[3]['is_restriction']);
                    $str->setCellValue("G" . $k, $glCodeData[3]['allocation']);
                    $str->setCellValue("H" . $k, $traDate);
                    $str->setCellValue("I" . $k, $setDate);
                    $str->setCellValue("J" . $k, $this->Currencyformat($tax));
                }
                if ($shipping > 0) {
                    $k++;
                    $str = $objPHPExcel->setActiveSheetIndex();
                    $str->setCellValue("A" . $k, $batch_no);
                    $str->setCellValue("B" . $k, $glCodeData[4]['gl_code_value']);
                    $str->setCellValue("C" . $k, $glCodeData[4]['department']);
                    $str->setCellValue("D" . $k, $prog);
                    $str->setCellValue("E" . $k, $glCodeData[4]['designation']);
                    $str->setCellValue("F" . $k, $glCodeData[4]['is_restriction']);
                    $str->setCellValue("G" . $k, $glCodeData[4]['allocation']);
                    $str->setCellValue("H" . $k, $traDate);
                    $str->setCellValue("I" . $k, $setDate);
                    $str->setCellValue("J" . $k, $this->Currencyformat($shipping));
                }
//                if ($batchTotal > 0) {
//                    $k++;
//                    $str = $objPHPExcel->setActiveSheetIndex();
//                    $str->setCellValue("A" . $k, '');
//                    $str->setCellValue("B" . $k, '');
//                    $str->setCellValue("C" . $k, '');
//                    $str->setCellValue("D" . $k, '');
//                    $str->setCellValue("E" . $k, '');
//                    $str->setCellValue("F" . $k, '');
//                    $str->setCellValue("G" . $k, '');
//                    $str->setCellValue("H" . $k, round($batchTotal,2));
//                }

                $batch_no = $val['Batch ID'];
                $gl_code = $val['GL Code'];
                $prog = $val['Program'];
                $dept = $val['Department'];
                $total = 0;
                $discount = 0;
                $tax = 0;
                $shipping = 0;
                $desg = '';
                $res = '';
                $allc = '';
                $total = $total + $val['Amount'];
                $discount = $discount + $val['product_discount'];
                $tax = $tax + $val['product_tax'];
                $shipping = $shipping + ($val['product_shipping'] + $val['product_handling']);
                $tranDate = '';
                $settDate = '';
            }
        }
        $traDate = implode(',', $tranDate);
        $setDate = implode(',', $settDate);
        if ($total > 0) {
            $k++;
            $str = $objPHPExcel->setActiveSheetIndex();
            $str->setCellValue("A" . $k, $batch_no);
            $str->setCellValue("B" . $k, $gl_code);
            $str->setCellValue("C" . $k, $dept);
            $str->setCellValue("D" . $k, $prog);
            $str->setCellValue("E" . $k, $desg);
            $str->setCellValue("F" . $k, $res);
            $str->setCellValue("G" . $k, $allc);
            $str->setCellValue("H" . $k, $traDate);
            $str->setCellValue("I" . $k, $setDate);
            $str->setCellValue("J" . $k, $this->Currencyformat($total));
        }
        if ($discount > 0) {
            $k++;
            $str = $objPHPExcel->setActiveSheetIndex();
            $str->setCellValue("A" . $k, $batch_no);
            $str->setCellValue("B" . $k, $glCodeData[0]['gl_code_value']);
            $str->setCellValue("C" . $k, $glCodeData[0]['department']);
            $str->setCellValue("D" . $k, $prog);
            $str->setCellValue("E" . $k, $glCodeData[0]['designation']);
            $str->setCellValue("F" . $k, $glCodeData[0]['is_restriction']);
            $str->setCellValue("G" . $k, $glCodeData[0]['allocation']);
            $str->setCellValue("H" . $k, $traDate);
            $str->setCellValue("I" . $k, $setDate);
            $str->setCellValue("J" . $k, "(" . $this->Currencyformat($discount) . ")");
        }
        if ($tax > 0) {
            $k++;
            $str = $objPHPExcel->setActiveSheetIndex();
            $str->setCellValue("A" . $k, $batch_no);
            $str->setCellValue("B" . $k, $glCodeData[3]['gl_code_value']);
            $str->setCellValue("C" . $k, $glCodeData[3]['department']);
            $str->setCellValue("D" . $k, $prog);
            $str->setCellValue("E" . $k, $glCodeData[3]['designation']);
            $str->setCellValue("F" . $k, $glCodeData[3]['is_restriction']);
            $str->setCellValue("G" . $k, $glCodeData[3]['allocation']);
            $str->setCellValue("H" . $k, $traDate);
            $str->setCellValue("I" . $k, $setDate);
            $str->setCellValue("J" . $k, $this->Currencyformat($tax));
        }
        if ($shipping > 0) {
            $k++;
            $str = $objPHPExcel->setActiveSheetIndex();
            $str->setCellValue("A" . $k, $batch_no);
            $str->setCellValue("B" . $k, $glCodeData[4]['gl_code_value']);
            $str->setCellValue("C" . $k, $glCodeData[4]['department']);
            $str->setCellValue("D" . $k, $prog);
            $str->setCellValue("E" . $k, $glCodeData[4]['designation']);
            $str->setCellValue("F" . $k, $glCodeData[4]['is_restriction']);
            $str->setCellValue("G" . $k, $glCodeData[4]['allocation']);
            $str->setCellValue("H" . $k, $traDate);
            $str->setCellValue("I" . $k, $setDate);
            $str->setCellValue("J" . $k, $this->Currencyformat($shipping));
        }
//                if ($batchTotal > 0) {
//                    $k++;
//                    $str = $objPHPExcel->setActiveSheetIndex();
//                    $str->setCellValue("A" . $k, '');
//                    $str->setCellValue("B" . $k, '');
//                    $str->setCellValue("C" . $k, '');
//                    $str->setCellValue("D" . $k, '');
//                    $str->setCellValue("E" . $k, '');
//                    $str->setCellValue("F" . $k, '');
//                    $str->setCellValue("G" . $k, '');
//                    $str->setCellValue("H" . $k, round($batchTotal,2));
//                }
// Add data
        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle('Reconcillation Report');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        // Redirect output to a client�s web browser (Excel5)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $objWriter = new \PHPExcel_IOFactory;
        $objWriter = $objWriter->createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }

	public function getReportFilterConditon($reportId,$filterIdArray,$filterValuesArray,$mstReportId,$conditionalColumn=''){
		
		$this->getReportTable();
		$filter_pattern = '';
		
		if(!empty($filterIdArray)){
			$filterIds = implode(",",array_keys($filterIdArray));
		
			$seachResult = array();
			$filterParams = array();
			if(!is_null($conditionalColumn) && $conditionalColumn!=''){
				$filterParams['conditionalColumn'] = $conditionalColumn;
			}
			$filterParams['reportId'] = $reportId;
			$filterParams['mstReportId'] = $mstReportId;
			$filterParams['filterIds'] = $filterIds;
			
			$seachResult = $this->_reportTable->getReportFilterConditions($filterParams);
			$filter_pattern_array = array();
			foreach($filterIdArray as $key1=>$value1){
				foreach($seachResult as $key2=>$value2){
					if($key1==$value2['column_id'] && $value1==$value2['filter_id'] && (!isset($filterValuesArray[$key1]) && !isset($filterValuesArray[$key1.'_from']))){
						$filter_pattern_array[] = $value2['filter_pattern'];
					}
				}
			}

			foreach($filterValuesArray as $key=>$value){
				foreach($seachResult as $key3=>$value3){
					if(!strpos($value3['filter_pattern'],'SUM')){
						if($key == $value3['column_id'] && $value3['filter_id']==$filterIdArray[$key]){
							if(preg_match("/<VALUE>/",$value3['filter_pattern'])){
								if(strpos($value3['filter_pattern'],"%")){
									if(strpos($value,",")){
										$valArray = explode(",",$value);
										$filterStr = array();
										foreach($valArray as $keys=>$values){
											$filterStr[] = str_replace("<VALUE>",str_replace("'","",$values),$value3['filter_pattern']);
										}
										$filter_pattern_array[] = "(".implode(" OR ",$filterStr).")";
									}else{							
										$filter_pattern_array[] = str_replace("<VALUE>",str_replace("'","",$value),$value3['filter_pattern']);
									}						
								}else{
									$filter_pattern_array[] = str_replace("<VALUE>",$value,$value3['filter_pattern']);
								}						
							}elseif(!strpos($value3['filter_pattern'],"<VALUE1>") && !strpos($value3['filter_pattern'],"<VALUE>")){
								$filter_pattern_array[] = $value3['filter_pattern'];
							}					
						
						}elseif(preg_match("/<VALUE1>/",$value3['filter_pattern']) && str_replace("_from","",$key) == $value3['column_id'] && $value3['filter_id']==$filterIdArray[str_replace("_from","",$key)]){

							$from_date = strpos($filterValuesArray[$value3['column_id']."_from"],'/')?$this->DateFormat($filterValuesArray[$value3['column_id']."_from"], 'db_date_format'):$filterValuesArray[$value3['column_id']."_from"];
							
							$to_date = strpos($filterValuesArray[$value3['column_id']."_to"],'/')?$this->DateFormat($filterValuesArray[$value3['column_id']."_to"], 'db_date_format'):$filterValuesArray[$value3['column_id']."_to"];

							$filter_pattern_array[] = str_replace(array("<VALUE1>","<VALUE2>"),array($from_date,$to_date),$value3['filter_pattern']);
						}
					}				
				}
			}
			$filter_pattern_array = array_unique($filter_pattern_array);
			
			if(count($filter_pattern_array)>1){
				$filter_pattern = implode(" AND ",$filter_pattern_array);
			}else{
				$filter_pattern = $filter_pattern_array[0];
			}

			$filter_pattern = "(".str_replace("`","",$filter_pattern).")";
		}

		return $filter_pattern;

	}

	public function getReportHavingConditon($filterIdArray,$filterValuesArray,$mstReportId,$finalizeFilterIds = ""){

		$this->getReportTable();
		$params = array();
		if($finalizeFilterIds==''){
			$filterIds = implode(",",array_keys($filterIdArray));
		}else{
			$params['toReplace'] = '1';
			$filterIds = $finalizeFilterIds;
		}

		$seachResult = array();

		
		$params['mstReportId'] = $mstReportId;
		$params['filterIds'] = $filterIds;
		
		$seachResult = $this->_reportTable->getReportHavingCondition($params);
		
		$havingPattern ='';

		$havingPatternArray = array();
		if(!empty($seachResult) && !empty($filterIdArray)){
			foreach($filterIdArray as $key=>$value){			
				foreach($seachResult as $key1=>$value1){
					$having_pattern = $value1['having_pattern'];
					
					if($value1['column_id']==$key && $value1['filter_id']==$value){
						if(strpos($having_pattern,"<VALUE>")){
							$havingPatternArray[] = str_replace("<VALUE>",$filterValuesArray[$key],$having_pattern);
						}elseif(strpos($having_pattern,"<VALUE1>")){
							$havingPatternArray[] = str_replace(array("<VALUE1>","<VALUE2>"),array($filterValuesArray[$value1['column_id']."_from"],$filterValuesArray[$value1['column_id']."_to"]),$having_pattern);
						}
					}
				}				
			}
			$havingPattern = implode(' AND ',$havingPatternArray);
			$havingPattern = str_replace("`","",$havingPattern);
		}
			 
		
		return $havingPattern;
	}

}
