<?php

/**
 * This page is used for Create and edit Report form.
 * @package    Report_ReportForm
 * @author     Icreon Tech - DT
 */

namespace Report\Form;

use Zend\Form\Form;

/**
 * This class is used for Create and edit Report form.
 * @package    ReportForm
 * @author     Icreon Tech - DT
 */
class ReportForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('create_report');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'report_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'report_id'
            )
        ));
        /*$this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'display_columns[]',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'display_columns_1',
                'class' => 'checkbox e2'
            )
        ));*/

        $this->add(array(
            'type' => 'MultiCheckbox',
            'name' => 'display_columns',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                //'value' => '', //set checked to '1'
                'class' => 'e4 display_columns',
            )
        ));
        $this->add(array(
            'type' => 'MultiCheckbox',
            'name' => 'group_by_columns',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                //'value' => '', //set checked to '1'
                'class' => 'e4 group_by_columns',
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'filter_columns[]',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'filter_columns_1',
                'class' => 'e1'
            )
        ));
        $this->add(array(
            'name' => 'report_title',
            'attributes' => array(
                'type' => 'text',
                'id' => 'report_title'
            )
        ));
        $this->add(array(
            'name' => 'report_description',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'report_description',
                'class' => 'width-90'
            )
        ));
        $this->add(array(
            'name' => 'report_header',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'report_header',
                'class' => 'width-90'
            )
        ));
        $this->add(array(
            'name' => 'report_footer',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'report_footer',
                'class' => 'width-90'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'role_id[]',
            'attributes' => array(
                'multiple' => 'multiple',
                'id' => 'role_id',
                'size' => '4',
                'class' => 'e1 select-w-320'
                
            )
        ));

        $this->add(array(
            'name' => 'run_report',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'run_report',
                'class' => 'save-btn',
                'value' => 'Save and Run Report'
            )
        ));
        
        $this->add(array(
            'name' => 'update_report',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'update_report',
                'class' => 'save-btn',
                'value' => 'Update Report'
            )
        ));

        $this->add(array(
            'name' => 'save_copy',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'save_copy',
                'class' => 'save-btn m-l-10',
                'value' => 'Save a Copy'
            )
        ));

		$this->add(array(
            'name' => 'save_and_run_report',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'save_and_run_report',
                'class' => 'm-l-10 save-btn',
                'value' => 'Run Report'
            )
        ));


    }

}
