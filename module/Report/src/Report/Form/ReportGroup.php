<?php

/**
 * This page is used to contacts add to group in contact report
 * @package    Report_ReportGroup
 * @author     Icreon Tech - DT
 */

namespace Report\Form;

use Zend\Form\Form;

/**
 * This page is used to contacts add to group in contact report
 * @package    ReportGroup
 * @author     Icreon Tech - DT
 */
class ReportGroup extends Form {

    public function __construct($name = null) {
        parent::__construct('report_contact_group');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'report_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'report_id',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'create_add_group',
            'options' => array(
                'value_options' => array(
                    '1' => 'Add to Group',
                    '2' => 'Create Group'
                ),
            ),
            'attributes' => array(
                'id' => 'create_add_group',
                'class' => 'e3',
                'value' => '1',
                'onchange' => 'showCreateGroup(this.value)'
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'group_id',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'group_id',
                'class' => 'e1 select-w-320',
            )
        ));

        $this->add(array(
            'name' => 'group_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'group_name'
            )
        ));

        $this->add(array(
            'name' => 'group_description',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'group_description',
            )
        ));
        $this->add(array(
            'name' => 'saveGroup',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'saveGroup',
                'class' => 'save-btn',
                'value' => 'Submit'
            )
        ));
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'id' => 'cancel',
                'class' => 'save-btn',
                'value' => 'Cancel'
            )
        ));
    }

}