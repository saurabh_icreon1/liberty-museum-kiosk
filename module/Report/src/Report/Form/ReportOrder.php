<?php

/**
 * This page is used for Create and edit Report form Order.
 * @package    Report_ReportForm
 * @author     Icreon Tech - DT
 */

namespace Report\Form;

use Zend\Form\Form;

/**
 * This class is used for Create and edit Report form Order.
 * @package    ReportForm
 * @author     Icreon Tech - DT
 */
class ReportOrder extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('report_order');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'order_columns[]',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'order_columns_1',
                'class' => 'e1'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'order_order[]',
            'options' => array(
                'value_options' => array(
                    'ASC' => 'ASC',
                    'DESC' => 'DESC'
                ),
            ),
            'attributes' => array(
                'id' => 'order_order_1',
                'class' => 'e1'
            )
        ));

    }

}
