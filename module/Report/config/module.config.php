<?php

/**
 * This page is used for report module configuration details.
 * @package    Report
 * @author     Icreon Tech - DT
 */
return array(
    'controllers' => array(
        'invokables' => array(
            'Report\Controller\Report' => 'Report\Controller\ReportController'
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'getReportList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-contact-report[/:mst_report_id]',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'getReportList',
                        'report_type_id' => 1
                    ),
                ),
            ),
            'getTransactionReportList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-transaction-report[/:mst_report_id]',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'getReportList',
                        'report_type_id' => 2
                    ),
                ),
            ),
            'getCampaignReportList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-campaign-report[/:mst_report_id]',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'getReportList',
                        'report_type_id' => 3
                    ),
                ),
            ),
            'getMatchingGiftReportList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-matching-gift-report[/:mst_report_id]',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'getReportList',
                        'report_type_id' => 4
                    ),
                ),
            ),
            'getPledgeReportList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-pledge-report[/:mst_report_id]',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'getReportList',
                        'report_type_id' => 5
                    ),
                ),
            ),
            'getMembershipReportList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-membership-report[/:mst_report_id]',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'getReportList',
                        'report_type_id' => 6
                    ),
                ),
            ),
            'getProductReportList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-product-report[/:mst_report_id]',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'getReportList',
                        'report_type_id' => 7
                    ),
                ),
            ),
            'getComparisionReportList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-comparison-report[/:mst_report_id]',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'getReportList',
                        'report_type_id' => 8
                    ),
                ),
            ),
            'getWohReportList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-woh-report[/:mst_report_id]',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'getReportList',
                        'report_type_id' => 9
                    ),
                ),
            ),
            'getFinanceReportList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-finance-report[/:mst_report_id]',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'getReportList',
                        'report_type_id' => 10
                    ),
                ),
            ),
            'getStockReportList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-stock-report[/:mst_report_id]',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'getReportList',
                        'report_type_id' => 11
                    ),
                ),
            ),
            'getQuizReportList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-quiz-report[/:mst_report_id]',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'getReportList',
                        'report_type_id' => 12
                    ),
                ),
            ),
            'getContactReportInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-contact-report-info/:report_id',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'getReportInfo',
                        'report_category_id' => 1
                    ),
                ),
            ),
            'getTransactionReportInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-transaction-report-info/:report_id',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'getReportInfo',
                        'report_category_id' => 2
                    ),
                ),
            ),
            'getCampaignReportInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-campaign-report-info/:report_id',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'getReportInfo',
                        'report_category_id' => 3
                    ),
                ),
            ),
            'getMatchingGiftReportInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-matching-gift-report-info/:report_id',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'getReportInfo',
                        'report_category_id' => 4
                    ),
                ),
            ),
            'getPledgeReportInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-pledge-report-info/:report_id',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'getReportInfo',
                        'report_category_id' => 5
                    ),
                ),
            ),
            'getMembershipReportInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-membership-report-info/:report_id',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'getReportInfo',
                        'report_category_id' => 6
                    ),
                ),
            ),
            'getProductReportInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-product-report-info/:report_id',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'getReportInfo',
                        'report_category_id' => 7
                    ),
                ),
            ),
            'getComparisonReportInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-comparison-report-info/:report_id',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'getReportInfo',
                        'report_category_id' => 8
                    ),
                ),
            ),
            'getWohReportInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-woh-report-info/:report_id',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'getReportInfo',
                        'report_category_id' => 9
                    ),
                ),
            ),
            'getFinanceReportInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-finance-report-info/:report_id',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'getReportInfo',
                        'report_category_id' => 10
                    ),
                ),
            ),
            'getStockReportInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-stock-report-info/:report_id',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'getReportInfo',
                        'report_category_id' => 11
                    ),
                ),
            ),
            'getQuizReportInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-quiz-report-info/:report_id',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'getReportInfo',
                        'report_category_id' => 12
                    ),
                ),
            ),
            'addReport' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-report[/:report_category_id]',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'addReport'
                    ),
                ),
            ),
            'generateAllReports' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/generate-all-reports',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'generateAllReports',
                    ),
                ),
            ),
            'addReportOrderBy' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-report-order-by',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'addReportOrderBy',
                    ),
                ),
            ),
            'getReportGridInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-report-grid-info',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'getReportGridInfo',
                    ),
                ),
            ),
            'saveReport' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/save-report',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'saveReport',
                    ),
                ),
            ),
            'deleteReport' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-report',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'deleteReport',
                    ),
                ),
            ),
            'getReloadGridInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-reload-grid-info',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'getReloadGridInfo',
                    ),
                ),
            ),
            'addReportContactsToGroup' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-report-contacts-to-group[/:report_id]',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'addReportContactsToGroup',
                    ),
                ),
            ),
            'createExcel' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-excel[/:report_id]',
                    'defaults' => array(
                        'controller' => 'Report\Controller\Report',
                        'action' => 'createExcel',
                    ),
                ),
            ),
        )
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'navigation' => array(
        'default' => array(
            array(
                'label' => 'Report',
                'route' => 'getReportList',
                'pages' => array(
                    array(
                        'label' => 'Search',
                        'route' => 'getReportList',
                        'pages' => array(
                            array(
                                'label' => 'View',
                                'route' => 'getReportInfo',
                                'action' => 'getReportInfo',
                            )
                        ),
                    ),
                )
            )
        ),
    ),
    'report_messages' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
    'report_category_url' => array(
        1=>'/crm-contact-report',
        2=>'/crm-transaction-report',
        3=>'/crm-campaign-report',
        4=>'/crm-matching-gift-report',
        5=>'/crm-pledge-report',
        6=>'/crm-membership-report',
        7=>'/crm-product-report',
        8=>'/crm-comparison-report',
        9=>'/crm-woh-report',
        10=>'/crm-finance-report',
        11=>'/crm-stock-report',
        12=>'/crm-quiz-report'
    ),
    'add_report_category_url' => array(
        1=>'/crm-contact-report-info',
        2=>'/crm-transaction-report-info',
        3=>'/crm-campaign-report-info',
        4=>'/crm-matching-gift-report-info',
        5=>'/crm-pledge-report-info',
        6=>'/crm-membership-report-info',
        7=>'/crm-product-report-info',
        8=>'/crm-comparison-report-info',
        9=>'/crm-woh-report-info',
        10=>'/crm-finance-report-info',
        11=>'/crm-stock-report-info',
        12=>'/crm-quiz-report-info'
    ),
    'report_category_heading' => array(
        'Contact Report',
        'Transaction Report',
        'Campaign Report',
        'Matching Gifts Report',
        'Pledge Report',
        'Membership Report',
        'Product Report',
        'Comparison Report',
        'WOH Report',
        'Finance Report',
        'Stock Report',
        'Quiz Report'
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
            'navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'report' => __DIR__ . '/../view'
        ),
        'template_map' => array(
            'layout/custom' => __DIR__ . '/../../module/Common/layout/crm.phtml',
            'error/404' => __DIR__ . '/../../module/Common/error/404.phtml',
            'error/index' => __DIR__ . '/../../module/Common/error/index.phtml',
        ),
    ),
    'global_config' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
);
