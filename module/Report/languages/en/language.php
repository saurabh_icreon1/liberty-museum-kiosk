<?php

/**
 * This file is used for error messages and labels message on view pages for report module
 * @package    Report
 * @author     Icreon Tech - DT
 */
return array(
    'common_message' => array(
        'L_NAME' => 'Name',
        'L_DESCRIPTION' => 'Description',
        'L_ACTIONS' => 'Action (s)',
        "NO_RECORD_FOUND" => "No Record Found",
        'L_REPORTS' => 'Reports',
    ),
    'report_list_message' => array(
        'L_NEW_CONTACT_REPORT' => 'New Contact Report',
        'L_VIEW_ALL_REPORTS' => 'View All Reports',
        'DELETE_REPORT_ALERT' => "Are you sure you want to delete this report ?",
        "REPORT_DELETED_SUCCESSFULLY" => "Report successfully deleted !",
        "L_CREATE_NEW_REPORT_FROM_TEMPLATE" => 'Create new Report from Template',
    ),
    'report_create_message' => array(
        'L_DISPLAY_COLUMNS' => 'Display Columns',
        'L_GROUP_BY_COLUMNS' => 'Group By Columns',
        'L_ORDER_BY_COLUMNS' => 'Order by columns',
        'L_COLUMN' => 'Column',
        'L_ORDER' => 'Order',
        'L_SET_FILTERS' => 'Set Filters',
        'L_GENERAL_SETTING' => 'General Settings',
        'L_REPORT_TITLE' => 'Report Title',
        'L_REPORT_DESCRIPTION' => 'Report Description',
        'L_REPORT_HEADER' => 'Report Header',
        'L_REPORT_FOOTER' => 'Report Footer',
        'L_ROLE' => 'Role',
        'TITLE_EMPTY' => 'Please enter title',
        'ROLE_EMPTY' => 'Please select at least one role',
        'REPORT_CREATED_FAIL' =>  'Unable to create report',
        'REPORT_CREATED_SUCCESSFULLY' => 'Report copy successfully created.',
        'REPORT_UPDATED_SUCCESSFULLY' => 'Report successfully updated.',
        "GROUP_NAME_EMPTY" => "Please enter name.",
        "GROUP_DESCRIPTION_EMPTY" => "Please enter description.",
        "GROUP_DESCRIPTION_VALID" => "Please enter valid details.",
        "GROUP_NAME_VALID" => "Please enter a valid name",
        "GROUP_NAME_ALREADY_EXIST" => "Group name already exist.",
        "GROUP_CREATED_SUCCESSFULLY" => "Group created successfully.",
        "GROUP_UPDATED_SUCCESSFULLY" => "Group Updated Successfully!",
        "L_SELECT_GROUP" => "Select Group",
        "L_NAME" => "Name",
        "L_DESCRIPTION" => "Description",
        "GROUP_NAME_SELECT" => "Please select group.",
        "CONTACTS_TO_GROUP" => "Contacts to group",
        "ADDED_SUCCESSFULLY" => "added successfully!",
        "UPDATED_SUCCESSFULLY" => "updated successfully!",
    )
);
?>