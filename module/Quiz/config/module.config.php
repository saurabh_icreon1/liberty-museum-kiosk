<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'Quiz\Controller\Quiz' => 'Quiz\Controller\QuizController'
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'getQuiz' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-quizes',
                    'defaults' => array(
                        'controller' => 'Quiz\Controller\Quiz',
                        'action' => 'get-crm-quizes',
                    ),
                ),
            ),
            'addCrmQuiz' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-crm-quiz',
                    'defaults' => array(
                        'controller' => 'Quiz\Controller\Quiz',
                        'action' => 'add-crm-quiz',
                    ),
                ),
            ),
            'editCrmQuiz' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-quiz[/:id]',
                    'defaults' => array(
                        'controller' => 'Quiz\Controller\Quiz',
                        'action' => 'edit-crm-quiz',
                    ),
                ),
            ),
            'getCrmQuizInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-quiz-info[/:id]',
                    'defaults' => array(
                        'controller' => 'Quiz\Controller\Quiz',
                        'action' => 'get-crm-quiz-info',
                    ),
                ),
            ),
            'crmQuiz' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-quiz[/:id][/:mode]',
                    'defaults' => array(
                        'controller' => 'Quiz\Controller\Quiz',
                        'action' => 'crmQuiz',
                    ),
                ),
            ),            
            'userquiz' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-quizes[/:user_id]',
                    'defaults' => array(
                        'controller' => 'Quiz\Controller\Quiz',
                        'action' => 'userQuiz',
                    ),
                ),
            ),
            'deleteQuiz' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-quiz',
                    'defaults' => array(
                        'controller' => 'Quiz\Controller\Quiz',
                        'action' => 'delete-quiz',
                    ),
                ),
            ),
            'uploadQuizImage' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-quiz-image',
                    'defaults' => array(
                         'controller' => 'Quiz\Controller\Quiz',
                        'action' => 'uploadQuizImage',
                    ),
                ),
            ),
             'createQuizQuestions' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-crm-quiz-questions[/:id]',
                    'defaults' => array(
                        'controller' => 'Quiz\Controller\Quiz',
                        'action' => 'createQuizQuestions',
                    ),
                ),
            ),
             'addchoice' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-choice[/:counter][/:container][/:divid]',
                    'defaults' => array(
                      
                         'controller' => 'Quiz\Controller\Quiz',
                        'action' => 'add-choice',
                    ),
                ),
            ),
            'getQuestionsGrid' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-questions-grid[/:quizId]',
                    'defaults' => array(                      
                        'controller' => 'Quiz\Controller\Quiz',
                        'action' => 'get-questions-grid',
                    ),
                ),
            ),
             'getQuestionsGridEdit' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-questions-grid-edit[/:quizId]',
                    'defaults' => array(                      
                        'controller' => 'Quiz\Controller\Quiz',
                        'action' => 'get-questions-grid-edit',
                    ),
                ),
            ),
            'deleteQuizQuestion' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-quiz-question',
                    'defaults' => array(
                        'controller' => 'Quiz\Controller\Quiz',
                        'action' => 'delete-quiz-question',
                    ),
                ),
            ),
            'getQuizQuestionsInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-quiz-questions-info[/:id]',
                    'defaults' => array(
                        'controller' => 'Quiz\Controller\Quiz',
                        'action' => 'get-quiz-questions-info',
                    ),
                ),
            ),
            
            'addCrmSearchQuiz' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-crm-search-quiz',
                    'defaults' => array(
                        'controller' => 'Quiz\Controller\Quiz',
                        'action' => 'addCrmSearchQuiz',
                    ),
                ),
            ),
            'getCrmSearchSavedQuiz' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-search-saved-quiz',
                    'defaults' => array(
                        'controller' => 'Quiz\Controller\Quiz',
                        'action' => 'getCrmSearchSavedQuiz',
                    ),
                ),
            ),
            'getCrmQuizSavedSearchList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-quiz-saved-search-list',
                    'defaults' => array(
                        'controller' => 'Quiz\Controller\Quiz',
                        'action' => 'getQuizSavedSearchList',
                    ),
                ),
            ),
            'deleteCrmSearchQuiz' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-search-quiz',
                    'defaults' => array(
                        'controller' => 'Quiz\Controller\Quiz',
                        'action' => 'delete-crm-search-quiz',
                    ),
                ),
            ),
             'createQuizEditQuestions' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-quiz-edit-question[/:quizId][/:questionId]',
                    'defaults' => array(
                        'controller' => 'Quiz\Controller\Quiz',
                        'action' => 'createQuizEditQuestions',
                    ),
                ),
            ),
             'getQuizzes' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/quizzes',
                    'defaults' => array(
                        'controller' => 'Quiz\Controller\Quiz',
                        'action' => 'get-quizzes',
                    ),
                ),
            ),
            'getQuizList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-quiz-list',
                    'defaults' => array(
                        'controller' => 'Quiz\Controller\Quiz',
                        'action' => 'get-quiz-list-front',
                    ),
                ),
            ),
            'getQuizInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/quiz-info[/:id]',
                    'defaults' => array(
                        'controller' => 'Quiz\Controller\Quiz',
                        'action' => 'get-quiz-info',
                    ),
                ),
            ),
            'getQuizPlay' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/quiz-play[/:id][/:questionid]',
                    'defaults' => array(
                        'controller' => 'Quiz\Controller\Quiz',
                        'action' => 'get-quiz-play',
                    ),
                ),
            ),
             'getQuizResult' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/quiz-result[/:id][/:quizUserId]',
                    'defaults' => array(
                        'controller' => 'Quiz\Controller\Quiz',
                        'action' => 'get-quiz-result',
                    ),
                ),
            ),
            
           
            
            
            
            
            
            
            
            
            
            
            
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'navigation' => array(
        'default' => array(
            array(
                'label' => 'Quizzes',
                'route' => 'getQuiz',
                'pages' => array(
                    array(
                        'label' => 'Add Quiz',
                        'route' => 'addCrmQuiz',
                        'pages' => array(
                            array(
                                'label' => 'Create Quiz Question',
                                'route' => 'createQuizQuestions',
                                'pages' => array()
                            )
                        )
                    ),
                    array(
                        'label' => 'View Quiz',
                        'route' => 'getCrmQuizInfo',
                        'pages' => array()
                    ),
                     array(
                        'label' => 'Edit Quiz',
                        'route' => 'editCrmQuiz',
                        'pages' => array()
                    ),
                     array(
                        'label' => 'Quiz Questions',
                        'route' => 'getQuizQuestionsInfo',
                        'pages' => array()
                    ),
                    array(
                        'label' => 'Change Log',
                        'route' => 'getquizchangelog',
                        'pages' => array()
                    ),
                    array(
                        'label' => 'Search',
                        'route' => 'getQuiz',
                        'pages' => array(),
                    )
                ),
            ),
            
        ),
    ),
    'quiz_messages' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'quiz' => __DIR__ . '/../view'
        ),
        'template_map' => array(
            'layout/custom' => __DIR__ . '/../../module/Common/layout/crm.phtml',
            'error/404' => __DIR__ . '/../../module/Common/error/404.phtml',
            'error/index' => __DIR__ . '/../../module/Common/error/index.phtml',
        ),
    ),
    'global_config' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
    'quiz_images_dimension'=>array(
        'small_width'=>80,
        'small_height'=>80,
        'medium_width'=>185,
        'medium_height'=>185,
        'large_width'=>280,
        'large_height'=>190,
        
    )
);
