<?php

/**
 * This form is used to search Quiz record
 * @package    Campaign
 * @author     Icreon Tech - KK
 */

namespace Quiz\Form;

use Zend\Form\Form;

class SearchQuizForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('search_quiz');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'quiz_title',
            'attributes' => array(
                'type' => 'text',
                'id' => 'quiz_title'
            )
        ));

       

        

        $this->add(array(
            'name' => 'start_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'start_date',
                'class' => 'calendar-input width-97'
            )
        ));
        

        $this->add(array(
            'name' => 'end_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'end_date',
                'class' => 'calendar-input width-97'
            )
        ));
       


        $this->add(array(
            'type' => 'Select',
            'name' => 'main_category_id',
            'options' => array(
                'value_options' => array(),
            ),
            'attributes' => array(
                'id' => 'main_category_id',
                'class' => 'e1 select-w-320',
                'value' => '',
                'readonly' => 'true'
            )
        ));
        
         $this->add(array(
            'type' => 'Select',
            'name' => 'quiz_status',
            'options' => array(
                'value_options' => array(   
                    ''=>'Select',
                    '1' => 'Published',
                    '0'=>'Un-published'
                    ),
            ),
            'attributes' => array(
                'id' => 'quiz_status',
                'class' => 'e1 select-w-320',
                'value' => '',
                'readonly' => 'true'
            )
        ));
        
        $this->add(array(
            'type' => 'Select',
            'name' => 'added_date_range',
            'attributes' => array(
                'id' => 'added_date_range',
                'class' => 'e1 select-w-320',
                'onChange' => 'showDate(this.id,"date_range_div")'
            )
        ));
        
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'avail_member',
            'checked_value' => '1',
            'unchecked_value' => '',
            'attributes' => array(
                'class'=>'checkbox',
                'id' => 'avail_member',
                'class' =>'checkbox e2'
            )
            
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_featured',
            'checked_value' => '1',
            'unchecked_value' => '',
            'attributes' => array(
                'class'=>'checkbox',
                'id' => 'is_featured',
                'class' =>'checkbox e2'
            )
            
        ));
        
        
        
       
        $this->add(array(
             'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'avail_non_member',
            'checked_value' => '2',
            'unchecked_value' => '',
            'attributes' => array(
                'id' => 'avail_non_member',
                
                'class' =>'checkbox e2'
            )
        )); 
         $this->add(array(
               'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'avail_anonymous',
             'checked_value' => '3',
            'unchecked_value' => '',
            'attributes' => array(
                'id' => 'avail_anonymous',
               
                'class' =>'checkbox e2'
            )
        )); 
        
        

        


        $this->add(array(
            'name' => 'searchbuttonquiz',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'SEARCH',
                'id' => 'searchbuttonquiz',
                'class' => 'search-btn'
            ),
        ));

         $this->add(array(
            'type' => 'Select',
            'name' => 'saved_search',
            'options' => array(
                'value_options' => array(

                ),
            ),
            'attributes' => array(
                'value' => '',
                'class' => 'e1 select-w-320',
                'id' => 'saved_search',
                'onChange' =>'getSavedQuizSearchResult();'
            ),
        ));
        $this->add(array(
            'name' => 'sendMail',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Add User',
                'id' => 'sendMail',
                'class' => 'save-btn'
                 ),
        ));
        
        
        $this->add(array(
            'type' => 'Select',
            'name' => 'quiz_expired',
            'options' => array(
                'value_options' => array(   
                    ''=>'All',
                    '1' => 'Not Expired',
                    '2'=>'Expired'
                    ),
            ),
            'attributes' => array(
                'id' => 'quiz_expired',
                'class' => 'e1 select-w-320',
                'value' => '',
                'readonly' => 'true'
            )
        ));
        
        $this->add(array(
            'type' => 'Select',
            'name' => 'quiz_available_to',
            'options' => array(
                'value_options' => array(   
                    '0'=>'All',
                    '1' => 'Logged in Accounts Only',
                    '2'=>'Members Only'
                    ),
            ),
            'attributes' => array(
                'id' => 'quiz_available_to',
                'class' => 'e1 select-w-320',
                'value' => '',
                'readonly' => 'true'
            )
        ));
        
        
    }

}