<?php

/**
 * This form is used to save search Quiz for admin
 * @package    Campaign
 * @author     Icreon Tech - AS
 */

namespace Quiz\Form;

use Zend\Form\Form;

class SaveSearchQuizForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('save_search');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'save_search_as',
            'attributes' => array(
                'type' => 'text',
                'id' => 'save_search_as'
            )
        ));
        $this->add(array(
            'name' => 'savebutton',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Save',
                'id' => 'savebutton',
                'class' => 'save-btn m-l-15',
            ),
        ));
        $this->add(array(
            'name' => 'search_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'search_id',
                'class' => 'save-btn m-l-5'
            ),
        ));
        $this->add(array(
            'name' => 'search_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'search_name'
            )
        ));
        $this->add(array(
            'name' => 'deletebutton',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Delete',
                'id' => 'deletebutton',
                'class' => 'save-btn m-l-5',
                'style' => 'display:none;',
            ),
        ));
    }

}