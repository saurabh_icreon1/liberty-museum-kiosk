<?php

/**
 * This form is used for create Quiz Form
 * @package    User
 * @author     Icreon Tech - KK
 */

namespace Quiz\Form;

use Zend\Form\Form;

/** This class is used for create form elements for Quiz questions section
 * @author     Icreon Tech - KK
 */
class CreateQuizQuestionsForm extends Form {

    public function __construct($name = null) {
        /* we want to ignore the name passed */
        parent::__construct('create_quiz_questions');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');


        $this->add(array(
            'name' => 'quiz_question',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'quiz_question',
                 'class' => 'myTextEditor width-90 textInMce'
            )
        ));
        
         $this->add(array(
            'name' => 'quiz_question_edit',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'quiz_question_edit',
                 'class' => 'myTextEditor width-90 textInMce'
            )
        ));
        $this->add(array(
            'name' => 'quiz_question_choice',
            'attributes' => array(
                'type' => 'text',
                'id' => 'quiz_question_choice'
            )
        ));
        
         $this->add(array(
            'name' => 'quiz_question_choice_edit',
            'attributes' => array(
                'type' => 'text',
                'id' => 'quiz_question_choice_edit'
            )
        ));
        
         $this->add(array(
            'name' => 'question_choice[]',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'question_choice_0'
            )
        ));
          $this->add(array(
            'name' => 'correct_index',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'correct_index'
            )
        ));
          
          
          $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'correct_choices[]',
            'options' => array(
                'value_options' => array(
                    
                ),
            ),
            'attributes' => array(
                'class' => 'e3',
                'value' => '1', //set checked to '1'
               
            )
        ));
          
          
          
           $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_randomized',
            'checked_value' => '1',
            'unchecked_value' => '',
            'attributes' => array(
                'class' => 'checkbox',
                'id' => 'is_randomized',
                'class' => 'checkbox e2'
            )
        ));
           
           
          
         
         
       
        
         $this->add(array(
            'name' => 'quiz_id_hidden',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'quiz_id_hidden'
            )
        ));
         
         $this->add(array(
            'name' => 'quiz_id_encrypted_hidden',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'quiz_id_encrypted_hidden'
            )
        ));

       
        
        $this->add(array(
            'name' => 'save_question',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'id' => 'save_question',
                'class' => 'save-btn'
            ),
        ));
        
         $this->add(array(
            'name' => 'save_question_edit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'id' => 'save_question_edit',
                'class' => 'save-btn m-l-10'
            ),
        ));
        
        
         $this->add(array(
            'name' => 'add_another_question',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save and Add Another Question',
                'id' => 'add_another_question',
                'class' => 'save-btn add-address'
            ),
        ));
        
        $this->add(array(
            'name' => 'saveandpublish',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save & publish',
                'id' => 'saveandpublish',
                'class' => 'save-btn m-l-10'
            ),
        ));
        $this->add(array(
            'name' => 'quizpreview',
            'attributes' => array(
                'type' => 'hidden',
                'value' => 'Preview',
                'id' => 'quizpreview',
                'class' => 'save-btn m-l-10'
            ),
        ));

        $this->add(array(
            'name' => 'cancelbutton',
            'attributes' => array(
                'type' => 'button',
                'value' => 'CANCEL',
                'id' => 'cancelbutton',
                'class' => 'cancel-btn m-l-10',
                'onclick' => 'window.location.href="/crm-quizes"'
            ),
        ));
        
         
    }

}