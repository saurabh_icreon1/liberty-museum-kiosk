<?php

/**
 * This form is used for create Quiz Form
 * @package    User
 * @author     Icreon Tech - KK
 */

namespace Quiz\Form;

use Zend\Form\Form;

/** This class is used for create form elements for Quiz section
 * @author     Icreon Tech - KK
 */
class CreateQuizForm extends Form {

    public function __construct($name = null) {
        /* we want to ignore the name passed */
        parent::__construct('create_quiz');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->add(array(
            'name' => 'quiz_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'quiz_id'
            )
        ));
        $this->add(array(
            'name' => 'quiz_title',
            'attributes' => array(
                'type' => 'text',
                'id' => 'quiz_title'
            )
        ));
        $this->add(array(
            'name' => 'quiz_tagline',
            'attributes' => array(
                'type' => 'text',
                'id' => 'quiz_tagline'
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'main_category_id',
            'options' => array(
                'value_options' => array(),
            ),
            'attributes' => array(
                'id' => 'main_category_id',
                'class' => 'e1 select-w-320',
                'value' => '',
                'readonly' => 'true'
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'is_published',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    '1' => 'Publish',
                    '0' => 'Un Publish'
                ),
            ),
            'attributes' => array(
                'id' => 'is_published',
                'class' => 'e1 select-w-320',
                'value' => '',
                'readonly' => 'true'
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'additional_category',
            'options' => array(
                'value_options' => array(),
            ),
            'attributes' => array(
                'id' => 'additional_category',
                'class' => 'e1',
                'multiple' => 'multiple',
                'value' => '',
                'readonly' => 'true'
            )
        ));

        $this->add(array(
            'name' => 'expiration_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'expiration_date',
                'class' => 'width-155 cal-icon'
            )
        ));
        $this->add(array(
            'name' => 'expiration_time',
            'attributes' => array(
                'type' => 'text',
                'id' => 'expiration_time',
                'class' => 'width-108 m-l-20 time-icon'
            )
        ));

        $this->add(array(
            'name' => 'brief_description',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'brief_description'
            )
        ));

        $this->add(array(
            'name' => 'full_description',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'full_description'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'avail_member',
            'checked_value' => '1',
            'unchecked_value' => '',
            'attributes' => array(
                'class' => 'checkbox',
                'id' => 'avail_member',
                'class' => 'availability checkbox e2'
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'avail_non_member',
            'checked_value' => '2',
            'unchecked_value' => '',
            'attributes' => array(
                'id' => 'avail_non_member',
                'class' => 'availability checkbox e2'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'avail_anonymous',
            'checked_value' => '3',
            'unchecked_value' => '',
            'attributes' => array(
                'id' => 'avail_anonymous',
                'class' => ' availability checkbox e2'
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'avail_member_demo',
            'checked_value' => '1',
            'unchecked_value' => '',
            'attributes' => array(
                'class' => 'checkbox',
                'id' => 'avail_member_demo',
                'class' => 'availability_demo checkbox e2'
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'avail_non_member_demo',
            'checked_value' => '2',
            'unchecked_value' => '',
            'attributes' => array(
                'id' => 'avail_non_member_demo',
                'class' => 'availability_demo checkbox e2'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'avail_anonymous_demo',
            'checked_value' => '3',
            'unchecked_value' => '',
            'attributes' => array(
                'id' => 'avail_anonymous_demo',
                'class' => ' availability_demo checkbox e2'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_featured',
            'checked_value' => '1',
            'unchecked_value' => '',
            'attributes' => array(
                'class' => 'checkbox',
                'id' => 'is_featured',
                'class' => 'checkbox e2'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_retakeble_quiz',
            'checked_value' => '3',
            'unchecked_value' => '',
            'attributes' => array(
                'id' => 'is_retakeble_quiz',
                'class' => 'checkbox e2'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'enable_answers',
            'checked_value' => '3',
            'unchecked_value' => '',
            'attributes' => array(
                'id' => 'enable_answers',
                'class' => 'checkbox e2'
            )
        ));

        $this->add(array(
            'name' => 'main_image',
            'attributes' => array(
                'type' => 'file',
                'value' => 'upload',
                'id' => 'main_image',
                'class' => 'attachmentfiles'
            ),
        ));

        $this->add(array(
            'name' => 'main_image_hidden',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'main_image_hidden'
            )
        ));
        $this->add(array(
            'name' => 'new_image_hidden',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'new_image_hidden'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'gender',
            'checked_value' => '3',
            'unchecked_value' => '',
            'attributes' => array(
                'id' => 'gender',
                'class' => 'checkbox e2'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'grade_or_class',
            'checked_value' => '3',
            'unchecked_value' => '',
            'attributes' => array(
                'id' => 'grade_or_class',
                'class' => 'checkbox e2'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'born_place',
            'checked_value' => '3',
            'unchecked_value' => '',
            'attributes' => array(
                'id' => 'born_place',
                'class' => 'checkbox e2'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'residence_place',
            'checked_value' => '3',
            'unchecked_value' => '',
            'attributes' => array(
                'id' => 'residence_place',
                'class' => 'checkbox e2'
            )
        ));



        $this->add(array(
            'name' => 'savebutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'id' => 'savebutton',
                'class' => 'save-btn'
            ),
        ));

        $this->add(array(
            'name' => 'saveandaddquestion',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save & add questions',
                'id' => 'saveandaddquestion',
                'class' => 'save-btn m-l-10'
            ),
        ));



        $this->add(array(
            'name' => 'updatebutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Update',
                'id' => 'updatebutton',
                'class' => 'save-btn m-r-10'
            ),
        ));

        $this->add(array(
            'name' => 'updateandpublish',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Update & Publish',
                'id' => 'updateandpublish',
                'class' => 'save-btn'
            ),
        ));

        $this->add(array(
            'name' => 'previewbutton',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Preview',
                'id' => 'previewbutton',
                'class' => 'save-btn m-l-10'
            ),
        ));

        $this->add(array(
            'name' => 'cancelbutton',
            'attributes' => array(
                'type' => 'button',
                'value' => 'CANCEL',
                'id' => 'cancelbutton',
                'class' => 'cancel-btn m-l-10',
                'onclick' => 'window.location.href="/crm-quizes"'
            ),
        ));
        
    }

}