<?php

/**
 * This controller is used for Quiz apges
 * @category   Zend
 * @package    Quiz_QuizController
 * @version    2.2
 * @author     Icreon Tech - AG
 */

namespace Quiz\Controller;

use Base\Model\UploadHandler;
use Base\Controller\BaseController;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Zend\View\Model\ViewModel;
use Quiz\Model\Quiz;
use Quiz\Form\CreateQuizForm;
use Quiz\Form\SearchQuizForm;
use Quiz\Form\SaveSearchQuizForm;
use Quiz\Form\CreateQuizQuestionsForm;
use Quiz\Form\EditQuizQuestionsForm;
use Zend\Http\Request;
use Zend\Authentication\AuthenticationService;
use stdClass;

class QuizController extends BaseController {

    protected $_quizTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_flashMessage = null;

    function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        parent::__construct();
    }

    /**
     * This function is used to get table and config file object
     * @return     array
     * @param void
     * @author Icreon Tech - AS
     */
    public function getQuizTable() {
        if (!$this->_quizTable) {
            $sm = $this->getServiceLocator();
            $this->_quizTable = $sm->get('Quiz\Model\QuizTable');
            $this->_translator = $sm->get('translator');
            $this->_config = $sm->get('Config');
            $this->_adapter = $sm->get('dbAdapter');
            $this->_auth = new AuthenticationService();
        }
        return $this->_quizTable;
    }

    /**
     * This action is used to display the Quiz pages
     * @param void
     * @return array
     * @author Icreon Tech - AS
     */
    public function getCrmQuizesAction() {
        $request = $this->getRequest();
        $this->checkUserAuthentication();
        $this->getQuizTable();
        $Quiz = new Quiz($this->_adapter);
        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $response = $this->getResponse();
            $postArray = $request->getPost()->toArray();
            parse_str($request->getPost('searchString'), $searchParam);

            $dashlet = $request->getPost('crm_dashlet');
            $dashletSearchId = $request->getPost('searchId');
            $dashletId = $request->getPost('dashletId');
            $dashletSearchType = $request->getPost('searchType');
            $dashletSearchString = $request->getPost('dashletSearchString');
            if (isset($dashletSearchString) && $dashletSearchString != '') {
                $searchParam = unserialize($dashletSearchString);
            }

            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit; // do not put $limit*($page - 1)
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            if (isset($searchParam['added_date_range']) && $searchParam['added_date_range'] != 1 && $searchParam['added_date_range'] != '') {
                $dateRange = $this->getDateRange($searchParam['added_date_range']);
                $searchParam['start_date'] = $this->DateFormat($dateRange['from'], 'db_date_format_from');
                $searchParam['end_date'] = $this->DateFormat($dateRange['to'], 'db_date_format_to');
            } else if (isset($searchParam['added_date_range']) && $searchParam['added_date_range'] == '') {
                $searchParam['start_date'] = '';
                $searchParam['end_date'] = '';
            } else {
                if (isset($searchParam['start_date']) && $searchParam['start_date'] != '') {
                    $searchParam['start_date'] = $this->DateFormat($searchParam['start_date'], 'db_date_format_from');
                }
                if (isset($searchParam['end_date']) && $searchParam['end_date'] != '') {
                    $searchParam['end_date'] = $this->DateFormat($searchParam['end_date'], 'db_date_format_to');
                }
            }
            $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'modified_date') ? 'modified_date' : $searchParam['sort_field'];
            $searchQuizArr = $Quiz->getQuizSearchArr($searchParam);

            $seachResult = $this->getQuizTable()->getAllQuiz($searchQuizArr);
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $dashletResult = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getTableStruct(array('user_id' => $this->_auth->getIdentity()->crm_user_id, 'search_id' => $dashletSearchId, 'dash_id' => $dashletId, 'search_type' => $dashletSearchType));
            $dashletColumnName = array();
            if (!empty($dashletResult)) {
                $dashletColumn = explode(",", $dashletResult[0]['table_column_names']);
                foreach ($dashletColumn as $val) {
                    if (strpos($val, ".")) {
                        $dashletColumnName[] = trim(strstr($val, ".", false), ".");
                    } else {
                        $dashletColumnName[] = trim($val);
                    }
                }
            }
            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $jsonResult['page'] = $request->getPost('page');
            $jsonResult['records'] = $countResult[0]->RecordCount;
            $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
            if (!empty($seachResult)) {
                foreach ($seachResult as $val) {
                    $dashletCell = array();
                    $arrCell['id'] = $val['quiz_id'];
                    $encryptId = $this->encrypt($val['quiz_id']);
                    $viewLink = '<a href="/crm-quiz/' . $encryptId . '/' . $this->encrypt('view') . '" class="view-icon" alt="view activity"><div class="tooltip">View<span></span></div></a>';
                    $publish = $val['quiz_id'];
                    $editLink = '<a href="/crm-quiz/' . $encryptId . '/' . $this->encrypt('edit') . '" class="edit-icon" alt="Edit Quiz"><div class="tooltip">Edit<span></span></div></a>';
                    $deleteLink = '<a onclick="deleteQuiz(\'' . $encryptId . '\')" href="#delete_quiz_content" class="delete_quiz delete-icon" alt="delete quiz"><div class="tooltip">Delete<span></span></div></a>';
                    $actions = '<div class="action-col">' . $viewLink . $editLink . $deleteLink . '</div>';
                    $expirationDate = ($val['expiration_date'] == 0) ? 'N/A' : $this->OutputDateFormat($val['expiration_date'], 'dateformatampm');
                    if (false !== $dashletColumnKey = array_search('quiz_title', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = '<a href="/crm-quiz/' . $encryptId . '/' . $this->encrypt('view') . '" class="txt-decoration-underline">' . $val['quiz_title'] . '</a>';
                    if (false !== $dashletColumnKey = array_search('category_name', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $val['category_name'];

                    if (false !== $dashletColumnKey = array_search('expiration_date', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $expirationDate;
                    if (false !== $dashletColumnKey = array_search('featuredText', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $val['featuredText'];
                    if (false !== $dashletColumnKey = array_search('is_published', $dashletColumnName))
                        $dashletCell[$dashletColumnKey] = $val['publishedText'];

                    if (isset($dashlet) && $dashlet == 'dashlet') {
                        $arrCell['cell'] = $dashletCell;
                    } else {
                        $arrCell['cell'] = array($val['quiz_title'], $val['category_name'], $expirationDate, $val['featuredText'], $val['publishedText'], $actions);
                    }

                    $subArrCell[] = $arrCell;
                    $jsonResult['rows'] = $subArrCell;
                }
            } else {
                $jsonResult['rows'] = array();
            }
            return $response->setContent(\Zend\Json\Json::encode($jsonResult));
        } else {
            $this->layout('crm');
            $params = $this->params()->fromRoute();
            $viewModel = new ViewModel();
            $response = $this->getResponse();
            $searchQuizForm = new SearchQuizForm();
            $saveSearchQuizForm = new SaveSearchQuizForm();
            $response = $this->getResponse();
            $Quiz = new Quiz($this->_adapter);

            $getSearchArray = array();
            $getSearchArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
            $getSearchArray['is_active'] = '1';
            $quizSavedSearchArray = $Quiz->getQuizSavedSearchArr($getSearchArray);
            $quizSearchArray = $this->_quizTable->getQuizSavedSearch($quizSavedSearchArray);
            $quizSearchList = array();
            $quizSearchList[''] = 'Select';
            foreach ($quizSearchArray as $key => $val) {
                $quizSearchList[$val['quiz_search_id']] = stripslashes($val['title']);
            }
            $searchQuizForm->get('saved_search')->setAttribute('options', $quizSearchList);

            $categoriesArray = $this->getQuizTable()->getQuizCategories();
            $categoryDesc = array();
            $categoryName = array("" => "Select One");
            foreach ($categoriesArray as $key => $val) {
                $categoryName[$val['quiz_category_id']] = stripslashes($val['category_name']);
                $categoryDesc[$val['quiz_category_id']] = stripslashes($val['category_description']);
            }
            $searchQuizForm->get('main_category_id')->setAttribute('options', $categoryName);

            $caseDateRange = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTransactionRange();
            $dateRange = array("" => "Select One");
            foreach ($caseDateRange as $key => $val) {
                $dateRange[$val['range_id']] = $val['range'];
            }
            $searchQuizForm->get('added_date_range')->setAttribute('options', $dateRange);


            $messages = array();
            if ($this->_flashMessage->hasMessages()) {
                $messages = $this->_flashMessage->getMessages();
            }
            $viewModel->setVariables(array(
                'search_quiz_form' => $searchQuizForm,
                'save_search_quiz_form' => $saveSearchQuizForm,
                'messages' => $messages,
                'categoryDesc' => $categoryDesc,
                'jsLangTranslate' => array_merge($this->_config['quiz_messages']['config']['create_quiz'], $this->_config['quiz_messages']['config']['search_quiz'], $this->_config['quiz_messages']['config']['user_search_quiz'], $this->_config['quiz_messages']['config']['view_quiz'])
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to Add  Quiz 
     * @param void
     * @return array
     * @author Icreon Tech - KK
     */
    public function addCrmQuizAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getQuizTable();
        $quizForm = new CreateQuizForm();
        $Quiz = new Quiz($this->_adapter);
        $categoriesArray = $this->getQuizTable()->getQuizCategories();
        $quizCreateMessages = $this->_config['quiz_messages']['config']['search_quiz'];
        $categoryName = array("" => "Select One");
        $categoryDesc = array();
        foreach ($categoriesArray as $key => $val) {
            $categoryName[$val['quiz_category_id']] = stripslashes($val['category_name']);
            $categoryDesc[$val['quiz_category_id']] = stripslashes($val['category_description']);
        }
        $quizForm->get('main_category_id')->setAttribute('options', $categoryName);

        unset($categoryName['']);
        $quizForm->get('additional_category')->setAttribute('options', $categoryName);
        if ($request->isPost()) {
            $post = $request->getPost();
            $quizForm->setData($request->getPost());
            $quizForm->setInputFilter($Quiz->getInputFilterCreateQuiz());
            if (!$quizForm->isValid()) {
                $errors = $quizForm->getMessages();
                // asd($errors);
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'continue') {
                        foreach ($row as $typeError => $rower) {
                            if ($typeError != 'notInArray') {
                                $msg [$key] = $quizCreateMessages[$rower];
                            }
                        }
                    }
                }
            }
            if (!empty($msg)) {
                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $createQuizFormArr = $quizForm->getData();
                $createQuizFormArr['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                $createQuizFormArr['added_date'] = DATE_TIME_FORMAT;
                $createQuizFormArr['modify_by'] = $this->_auth->getIdentity()->crm_user_id;
                $createQuizFormArr['modify_date'] = DATE_TIME_FORMAT;
                $createQuizArr = $Quiz->getCreateQuizArr($createQuizFormArr);
                $createQuizArr[3] = $this->InputDateFormat($createQuizArr[3], 'dateformatampm');
                $quizId = $this->_quizTable->saveQuiz($createQuizArr);
                $changeLogArray = array();
                $changeLogArray['table_name'] = 'tbl_qui_change_logs';
                $changeLogArray['activity'] = '1';
                $changeLogArray['id'] = $quizId;
                $changeLogArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                if (!empty($createQuizFormArr['additional_category'])) {
                    $additionalCatdata['quiz_id'] = $quizId;
                    $additionalCatdata['added_by'] = $createQuizFormArr['added_by'];
                    $additionalCatdata['added_date'] = $createQuizFormArr['added_date'];
                    $additionalCatdata['modify_by'] = $createQuizFormArr['modify_by'];
                    $additionalCatdata['modify_date'] = $createQuizFormArr['modify_date'];
                    foreach ($createQuizFormArr['additional_category'] as $categoryData) {
                        if (!empty($categoryData)) {
                            $additionalCatdata['additional_category_id'] = $categoryData;
                            $this->_quizTable->insertAdditionalCategories($additionalCatdata);
                        }
                    }
                }
                $fileName = $createQuizFormArr['main_image_hidden'];
                $this->moveFileFromTempToQuiz($fileName);
                $encryptquizid = $this->encrypt($quizId);
                $this->flashMessenger()->addMessage($quizCreateMessages['QUIZ_ADDED']);
                $messages = array('status' => "success", 'message' => $quizCreateMessages['QUIZ_ADDED'], 'quiz_id' => $encryptquizid);
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        } else {
            $this->layout('crm');
            $messages = array();
            if ($this->_flashMessage->hasMessages()) {
                $messages = $this->_flashMessage->getMessages();
            }
            $viewModel = new ViewModel();
            $viewModel->setVariables(array(
                'createQuizForm' => $quizForm,
                'messages' => $messages,
                'categoryDesc' => $categoryDesc,
                'jsLangTranslate' => array_merge($this->_config['quiz_messages']['config']['create_quiz'], $this->_config['quiz_messages']['config']['search_quiz'], $this->_config['quiz_messages']['config']['user_search_quiz'], $this->_config['quiz_messages']['config']['view_quiz'])
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to Edit Quiz
     * @param void
     * @return array
     * @author Icreon Tech - KK
     */
    public function editCrmQuizAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getQuizTable();
        $quizCreateMessages = array_merge($this->_config['quiz_messages']['config']['create_quiz'], $this->_config['quiz_messages']['config']['search_quiz']);
        $quizForm = new CreateQuizForm();
        $Quiz = new Quiz($this->_adapter);
        $categoriesArray = $this->getQuizTable()->getQuizCategories();
        $categoryName = array("" => "Select One");
        $categoryDesc = array();
        foreach ($categoriesArray as $key => $val) {
            $categoryName[$val['quiz_category_id']] = stripslashes($val['category_name']);
            $categoryDesc[$val['quiz_category_id']] = stripslashes($val['category_description']);
        }
        $quizForm->get('main_category_id')->setAttribute('options', $categoryName);
        unset($categoryName['']);
        $quizForm->get('additional_category')->setAttribute('options', $categoryName);
        $mode = (isset($params['mode']) && $params['mode'] != '') ? $params['mode'] : $this->encrypt("view");
        if ($request->isPost()) {
            $post = $request->getPost();
            $quizForm->setData($request->getPost());
            $quizForm->setInputFilter($Quiz->getInputFilterUpdateQuiz());
            if (!$quizForm->isValid()) {
                $errors = $quizForm->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'continue') {
                        foreach ($row as $typeError => $rower) {
                            if ($typeError != 'notInArray') {
                                $msg [$key] = $quizCreateMessages[$rower];
                            }
                        }
                    }
                }
            }
            if (!empty($msg)) {
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $editQuizFormArr = $quizForm->getData();
                if (isset($editQuizFormArr['quiz_id']) && $editQuizFormArr['quiz_id'] != '' && $editQuizFormArr['quiz_id'] != '0') {
                    $editQuizFormArr['modify_by'] = $this->_auth->getIdentity()->crm_user_id;
                    $editQuizFormArr['modify_date'] = DATE_TIME_FORMAT;
                    $editQuizArr = $Quiz->getUpdateQuizArr($editQuizFormArr);
                    $editQuizArr[5] = $this->InputDateFormat($editQuizArr[5], 'dateformatampm');
                    $this->_quizTable->UpdateQuiz($editQuizArr);
                    $quizId = $editQuizFormArr['quiz_id'];
                    $changeLogArray = array();
                    $changeLogArray['table_name'] = 'tbl_qui_change_logs';
                    $changeLogArray['activity'] = '2';
                    $changeLogArray['id'] = $quizId;
                    $changeLogArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                    $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                    $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                    $additionalCatdata['quiz_id'] = $quizId;
                    $this->_quizTable->updateAdditionalCategories($additionalCatdata);
                    if (!empty($editQuizFormArr['additional_category'])) {
                        foreach ($editQuizFormArr['additional_category'] as $categoryData) {
                            if (!empty($categoryData)) {
                                $additionalCatdata['additional_category_id'] = $categoryData;
                                $additionalCatdata['added_by'] = $editQuizFormArr['modify_by'];
                                $additionalCatdata['added_date'] = $editQuizFormArr['modify_date'];
                                $additionalCatdata['modify_by'] = $editQuizFormArr['modify_by'];
                                $additionalCatdata['modify_date'] = $editQuizFormArr['modify_date'];
                                $this->_quizTable->insertAdditionalCategories($additionalCatdata);
                            }
                        }
                    }
                    if (isset($editQuizFormArr['new_image_hidden']) && $editQuizFormArr['new_image_hidden'] != '') {
                        $fileName = $editQuizFormArr['new_image_hidden'];
                        $this->moveFileFromTempToQuiz($fileName);
                        //unlink($this->_config['assets_upload_dir'].trim($editQuizFormArr['main_image_hidden']));
                    }
                    $encryptquizid = $this->encrypt($quizId);
                    $this->flashMessenger()->addMessage($quizCreateMessages['QUIZ_UPDATED']);
                    $messages = array('status' => "success", 'message' => $quizCreateMessages['QUIZ_UPDATED'], 'quiz_id' => $encryptquizid);
                } else {
                    $this->flashMessenger()->addMessage($quizCreateMessages['QUIZ_UPDATED_ERROR']);
                    $messages = array('status' => "error", 'message' => $quizCreateMessages['QUIZ_UPDATED_ERROR'], 'quiz_id' => $encryptquizid);
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        } else {
            $messages = array();
            if ($this->_flashMessage->hasMessages()) {
                $messages = $this->_flashMessage->getMessages();
            }
            $params = $this->params()->fromRoute();
            $quizId = $this->decrypt($params['id']);
            $quizDetailArr = array();
            $quizDetailArr[] = $quizId;
            $quizData = $this->_quizTable->getquizById($quizDetailArr);
            $quizAdditionalCategoryData = $this->_quizTable->getquizAdditionalCategoriesById($quizDetailArr);
            $quizsArray = $Quiz->processQuizArray($quizData);
            $quizAdditionalCategories = $Quiz->getquizAdditionalCategoriesArray($quizAdditionalCategoryData);
            $quizForm->get('quiz_id')->setValue($quizId);
            $quizForm->get('quiz_title')->setValue($quizsArray[$quizId]['detail']['quiz_title']);
            $quizForm->get('quiz_tagline')->setValue($quizsArray[$quizId]['detail']['quiz_tag_line']);
            $quizForm->get('main_category_id')->setValue($quizsArray[$quizId]['detail']['quiz_category_id']);
            $quizForm->get('is_published')->setValue($quizsArray[$quizId]['detail']['is_published']);
            $quizForm->get('additional_category')->setValue($quizAdditionalCategories);
            $expirationTime = explode(' ', $this->OutputDateFormat($quizsArray[$quizId]['detail']['expiration_date'], 'dateformatampm'));
            $quizForm->get('expiration_date')->setValue($expirationTime[0]);
            $quizForm->get('expiration_time')->setValue($expirationTime[1] . " " . $expirationTime[2]);
            $quizForm->get('brief_description')->setValue($quizsArray[$quizId]['detail']['brief_description']);
            $quizForm->get('full_description')->setValue($quizsArray[$quizId]['detail']['full_description']);
            $quizForm->get('avail_member')->setValue(strpos($quizsArray[$quizId]['detail']['availability'], '1') === false ? '' : '1');
            $quizForm->get('avail_non_member')->setValue(strpos($quizsArray[$quizId]['detail']['availability'], '2') === false ? '' : '1');
            $quizForm->get('avail_anonymous')->setValue(strpos($quizsArray[$quizId]['detail']['availability'], '3') === false ? '' : '1');
            $quizForm->get('avail_member_demo')->setValue(strpos($quizsArray[$quizId]['detail']['demographic_availability'], '1') === false ? '' : '1');
            $quizForm->get('avail_non_member_demo')->setValue(strpos($quizsArray[$quizId]['detail']['demographic_availability'], '2') === false ? '' : '1');
            $quizForm->get('avail_anonymous_demo')->setValue(strpos($quizsArray[$quizId]['detail']['demographic_availability'], '3') === false ? '' : '1');
            $quizForm->get('is_featured')->setValue($quizsArray[$quizId]['detail']['is_featured']);
            $quizForm->get('is_retakeble_quiz')->setValue($quizsArray[$quizId]['detail']['is_retakeble_quiz']);
            $quizForm->get('enable_answers')->setValue($quizsArray[$quizId]['detail']['enable_answers']);
            $quizForm->get('main_image')->setValue($quizsArray[$quizId]['detail']['main_image']);
            $quizForm->get('main_image_hidden')->setValue($quizsArray[$quizId]['detail']['main_image']);
            $quizForm->get('gender')->setValue($quizsArray[$quizId]['detail']['gender']);
            $quizForm->get('gender')->setAttribute('checked', $quizsArray[$quizId]['detail']['gender']);
            $quizForm->get('grade_or_class')->setValue($quizsArray[$quizId]['detail']['grade_or_class']);
            $quizForm->get('grade_or_class')->setAttribute('checked', $quizsArray[$quizId]['detail']['grade_or_class']);
            $quizForm->get('born_place')->setValue($quizsArray[$quizId]['detail']['born_place']);
            $quizForm->get('born_place')->setAttribute('checked', $quizsArray[$quizId]['detail']['born_place']);
            $quizForm->get('residence_place')->setValue($quizsArray[$quizId]['detail']['residence_place']);
            $quizForm->get('residence_place')->setAttribute('checked', $quizsArray[$quizId]['detail']['residence_place']);
            $quiz_image_path = '';
            if ($quizsArray[$quizId]['detail']['main_image'] != '') {
                $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration
                $quiz_image_path = file_exists($upload_file_path['assets_upload_dir'] . "quiz/thumbnail/" . $quizsArray[$quizId]['detail']['main_image']) ? $upload_file_path['assets_url'] . "quiz/thumbnail/" . $quizsArray[$quizId]['detail']['main_image'] : '';
            }
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'createQuizForm' => $quizForm,
                'jsLangTranslate' => array_merge($this->_config['quiz_messages']['config']['create_quiz'], $this->_config['quiz_messages']['config']['search_quiz'], $this->_config['quiz_messages']['config']['user_search_quiz'], $this->_config['quiz_messages']['config']['view_quiz']),
                'messages' => $messages,
                'quiz_id' => $quizId,
                'encrypt_quiz_id' => $params['id'],
                'quiz_image' => $quizsArray[$quizId]['detail']['main_image'],
                'quiz_image_path' => $quiz_image_path,
                'mode' => $mode,
                'quizData' => $quizsArray,
                'categoryDesc' => $categoryDesc
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to Get the Quiz info
     * @param void
     * @return array
     * @author Icreon Tech - KK
     */
    public function getCrmQuizInfoAction() {
        $this->checkUserAuthentication();
        $params = $this->params()->fromRoute();
        $this->getQuizTable();
        $Quiz = new Quiz($this->_adapter);
        $quizMessages = array_merge($this->_config['quiz_messages']['config']['create_quiz'], $this->_config['quiz_messages']['config']['view_quiz'], $this->_config['quiz_messages']['config']['search_quiz']);
        if (isset($params['id']) && $params['id'] != '') {
            $quizId = $this->decrypt($params['id']);
            $quizDetailArr = array();
            $quizDetailArr[] = $quizId;
            $quizData = $this->_quizTable->getquizById($quizDetailArr);
            $quizAdditionalCategoryData = $this->_quizTable->getquizAdditionalCategoriesById($quizDetailArr);
            $quizAdditionalCategories = "";
            $quizAdditionalCategories = $Quiz->getquizAdditionalCategoriesString($quizAdditionalCategoryData);
            $quizsArray = $Quiz->processQuizArray($quizData);


            $quiz_image_path = '';
            if ($quizsArray[$quizId]['detail']['main_image'] != '') {
                $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration

                $quiz_image_path = file_exists($upload_file_path['assets_upload_dir'] . "quiz/thumbnail/" . $quizsArray[$quizId]['detail']['main_image']) ? $upload_file_path['assets_url'] . "quiz/thumbnail/" . $quizsArray[$quizId]['detail']['main_image'] : '';
            }


            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => $quizMessages,
                'quiz_id' => $quizId,
                'encrypt_quiz_id' => $params['id'],
                'quiz_data' => $quizsArray,
                'quiz_addtnl_category' => $quizAdditionalCategories,
                'quiz_image_path' => $quiz_image_path,
                'login_user_detail' => $this->_auth->getIdentity()
            ));
            return $viewModel;
        }
    }

    /**
     * This function is used to get user quiz
     * @return     array
     * @param void
     * @author Icreon Tech - DG
     */
    public function userQuizAction() {
        $this->checkFrontUserAuthentication();
        $sm = $this->getServiceLocator();
        $this->_config = $sm->get('Config');

        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $this->layout('layout');
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $params = $this->params()->fromRoute();
        $userQuizes = $this->getServiceLocator()->get('Quiz\Model\QuizTable')->getUserQuizes(array('user_id' => $this->decrypt($params['user_id'])));
        $viewModel->setVariables(array(
            'userQuizes' => $userQuizes,
            'jsLangTranslate' => $this->_config['quiz_messages']['config']['user_quiz'],
            'facebookAppId' => $this->_config['facebook_app']['app_id'],
			'allowed_IP' => $this->_config['allowed_ip']['ip']
                )
        );
        return $viewModel;
    }

    /**
     * This action is used to delete Quiz
     * @param  void
     * @return json format string
     * @author Icreon Tech - KK
     */
    public function deleteQuizAction() {
        $request = $this->getRequest();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $quizid = $request->getPost('quizid');
            if ($quizid != '') {
                $quizid = $this->decrypt($quizid);
                $postArr = $request->getPost();
                $postArr['quiz_id'] = $quizid;
                $this->getQuizTable();
                $quizSearchMessages = $this->_config['quiz_messages']['config']['search_quiz'];
                $Quiz = new Quiz($this->_adapter);
                $response = $this->getResponse();
                $quizArr = $Quiz->getArrayForRemoveQuiz($postArr);
                $this->_quizTable->removeQuizById($quizArr);
                $changeLogArray = array();
                $changeLogArray['table_name'] = 'tbl_qui_change_logs';
                $changeLogArray['activity'] = '3';
                $changeLogArray['id'] = $quizid;
                $changeLogArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                $this->flashMessenger()->addMessage($quizSearchMessages['QUIZ_DELETE']);
                $messages = array('status' => "success", 'message' => $quizSearchMessages['QUIZ_DELETE']);
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                return $this->redirect()->toRoute('crm-quizes');
            }
        } else {
            return $this->redirect()->toRoute('crm-quizes');
        }
    }

    /**
     * This action is used to show all quiz detail
     * @param void
     * @return array
     * @author Icreon Tech - AG
     */
    public function crmQuizAction() {
        $this->checkUserAuthentication();
        $this->getQuizTable();
        $quizMessages = array_merge($this->_config['quiz_messages']['config']['create_quiz'], $this->_config['quiz_messages']['config']['view_quiz']);
        $quiz = new Quiz($this->_adapter);
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();
        $this->layout('crm');
        $quizId = $this->decrypt($params['id']);
        $mode = (isset($params['mode']) && $params['mode'] != '') ? $this->decrypt($params['mode']) : 'view';
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'jsLangTranslate' => $quizMessages,
            'quiz_id' => $quizId,
            'encrypt_quiz_id' => $params['id'],
            'module_name' => $this->encrypt('quiz'),
            'mode' => $mode,
            'login_user_detail' => $this->_auth->getIdentity(),
            'encrypted_source_type_id' => $this->encrypt('3')
        ));
        return $viewModel;
    }

    /**
     * This action is used to upload file for activity
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function uploadQuizImageAction() {
        $fileInputNames = array_keys($_FILES);
        $fileInputName = $fileInputNames[0];
        $fileExt = $this->GetFileExt($_FILES[$fileInputName]['name']);
        $filename = $this->GenerateMediaFileName($fileExt[1]);    //Generate file name
        $_FILES[$fileInputName]['name'] = $filename;                 // assign name to file variable
        $this->getQuizTable();
        $quizCreateMessage = $this->_config['quiz_messages']['config']['create_quiz'];
        $uploadFilePath = $this->_config['file_upload_path'];   // get file upload configuration

        $fileUploadInformation = $this->_config['file_upload_information'];
        $errorMessages = array(
            'post_max_size' => $quizCreateMessage['ATTACHMENT_POST_SIZE_EXCEED'], // Set error messages
            'max_file_size' => sprintf($quizCreateMessage['FILE_SIZE_BIG'], ($fileUploadInformation['max_allowed_file_size'] / 1024 / 1024)),
            'accept_file_types' => $quizCreateMessage['FILE_TYPE_NOT_ALLOWED'],
            2 => sprintf($quizCreateMessage['FILE_SIZE_BIG'], ($fileUploadInformation['max_allowed_file_size'] / 1024 / 1024))
        );
        $options = array(
            'upload_dir' => $uploadFilePath['temp_upload_dir'],
            'param_name' => $fileInputName, //file input name                      // Set configuration
            'inline_file_types' => $fileUploadInformation['file_types_allowed'],
            'accept_file_types' => $fileUploadInformation['file_types_allowed'],
            'max_file_size' => $fileUploadInformation['max_allowed_file_size'],
            'image_versions' => array(
                'large' => array(
                    'max_width' => $this->_config['quiz_images_dimension']['large_width'],
                    'max_height' => $this->_config['quiz_images_dimension']['large_height'],
                    'jpeg_quality' => 80
                ),
                'thumbnail' => array(
                    'max_width' => $this->_config['quiz_images_dimension']['small_width'],
                    'max_height' => $this->_config['quiz_images_dimension']['small_width']
                ),
                'medium' => array(
                    'max_width' => $this->_config['quiz_images_dimension']['medium_width'],
                    'max_height' => $this->_config['quiz_images_dimension']['medium_height']
                )
            )
        );
        $uploadHandler = new UploadHandler($options, true, $errorMessages);


        $fileResponse = $uploadHandler->jsonResponceData;
        if (isset($fileResponse[$fileInputName][0]->error)) {
            $messages = array('status' => "error", 'message' => $fileResponse[$fileInputName][0]->error);
        } else {
            $messages = array('status' => "success", 'message' => 'File uploaded', 'filename' => $fileResponse[$fileInputName][0]->name);
        }
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This function is used to move file from temp to quiz folder
     * @return json
     * @param 1 for add , 2 for edit
     * @author Icreon Tech - KK
     */
    public function moveFileFromTempToQuiz($filename, $oldAttachmentDetail = array(), $operation = 1) {
        $this->getQuizTable();
        $uploadFilePath = $this->_config['file_upload_path'];   // get file upload configuration
        $tempDir = $uploadFilePath['temp_upload_dir'];
        $tempThumbnailDir = $uploadFilePath['temp_upload_thumbnail_dir'];
        $quizDir = $uploadFilePath['assets_upload_dir'] . "/quiz/";
        $quizThumbnailDir = $uploadFilePath['assets_upload_dir'] . "/quiz/thumbnail/";

        $tempLargeDir = $tempDir . 'large/';

        $tempMediumDir = $tempDir . 'medium/';

        $quizLargeDir = $uploadFilePath['assets_upload_dir'] . "/quiz/large/";

        $quizMediumDir = $uploadFilePath['assets_upload_dir'] . "/quiz/medium/";


        if ($filename != '') {
            $tempFile = $tempDir . $filename;
            $tempThumbnailFile = $tempThumbnailDir . $filename;

            $tempLargeFile = $tempLargeDir . $filename;

            $tempMediumFile = $tempMediumDir . $filename;

            $quizFilename = $quizDir . $filename;

            $quizThumbnailFilename = $quizThumbnailDir . $filename;

            $quizMediumFilename = $quizMediumDir . $filename;

            $quizLargeFilename = $quizLargeDir . $filename;

            if (file_exists($tempFile)) {
                if (copy($tempFile, $quizFilename)) {
                    unlink($tempFile);
                }
            }
            if (file_exists($tempThumbnailFile)) {
                if (copy($tempThumbnailFile, $quizThumbnailFilename)) {
                    unlink($tempThumbnailFile);
                }
            }

            if (file_exists($tempMediumFile)) {
                if (copy($tempMediumFile, $quizMediumFilename)) {
                    unlink($tempMediumFile);
                }
            }

            if (file_exists($tempLargeFile)) {
                if (copy($tempLargeFile, $quizLargeFilename)) {
                    unlink($tempLargeFile);
                }
            }


            if ($operation == 2) {
                $removeFiles = array_diff($oldAttachmentDetail, $attachmentDetail);
                if (count($removeFiles) > 0) {
                    foreach ($removeFiles as $oldfilename) {
                        $activityOldFilename = $quizDir . $oldfilename;
                        $activityOldThumbnailFilename = $quizThumbnailDir . $oldfilename;
                        if (file_exists($activityOldFilename)) {
                            unlink($activityOldFilename);
                        }
                        if (file_exists($activityOldThumbnailFilename)) {
                            unlink($activityOldThumbnailFilename);
                        }
                    }
                }
            }
        }
    }

    /**
     * This action is used to add the quiz questions
     * @param void
     * @return array
     * @author Icreon Tech - AG
     */
    public function createQuizQuestionsAction() {
        $this->checkUserAuthentication();
        $this->getQuizTable();
        $quizMessages = array_merge($this->_config['quiz_messages']['config']['create_quiz'], $this->_config['quiz_messages']['config']['view_quiz'], $this->_config['quiz_messages']['config']['user_search_quiz']);
        $quiz = new Quiz($this->_adapter);
        $params = $this->params()->fromRoute();
        $createQuizQuestionform = new CreateQuizQuestionsForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $quizCreateMessages = $this->_config['quiz_messages']['config']['search_quiz'];
        if ($request->isPost()) {
            $postData = $request->getPost();
            $createQuizQuestionform->setData($request->getPost());
            $createQuizQuestionform->setInputFilter($quiz->getInputFilterCreateQuizQuestions());
            if (!$createQuizQuestionform->isValid()) {
                $errors = $createQuizQuestionform->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'continue') {
                        foreach ($row as $typeError => $rower) {
                            if ($typeError != 'notInArray') {
                                $msg [$key] = $quizMessages[$rower];
                            }
                        }
                    }
                }
            }

            if (!empty($msg)) {
                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {

                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $createQuizFormArr = $createQuizQuestionform->getData();
                if (isset($createQuizFormArr['case_contact_id']) && $createQuizFormArr['case_contact_id'] != '' && $createQuizFormArr['case_contact_id'] != '0') {
                    //for edit
                    $createQuizFormArr['modify_by'] = $this->_auth->getIdentity()->crm_user_id;
                    $createQuizFormArr['modify_date'] = DATE_TIME_FORMAT;
                    // $editCaseArr = $Quiz->getCreateEditArr($createQuizFormArr);
                    //$this->_quizTable->editCase($editCaseArr);
                    $this->flashMessenger()->addMessage($quizMessages['QUIZ_UPDATED']);
                    $messages = array('status' => "success", 'message' => $quizMessages['QUIZ_UPDATED']);
                } else {
                    //for add
                    $dataPost = $postData->toArray();
                    $createQuizFormArr['quiz_id'] = $createQuizFormArr['quiz_id_hidden'];
                    $createQuizFormArr['question_title'] = $createQuizFormArr['quiz_question'];
                    $createQuizFormArr['is_randomized'] = $createQuizFormArr['is_randomized'];
                    $createQuizFormArr['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                    $createQuizFormArr['added_date'] = DATE_TIME_FORMAT;
                    $createQuizFormArr['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                    $createQuizFormArr['modified_date'] = DATE_TIME_FORMAT;
                    $createQuizQuestionArr = $quiz->getCreateQuizQuestionArr($createQuizFormArr);
                    $quizQuestionId = $this->_quizTable->saveQuizQuestions($createQuizQuestionArr);
                    if (!empty($dataPost['question_choice'])) {
                        $questionOption['quiz_question_id'] = $quizQuestionId;
                        $questionOption['added_by'] = $createQuizFormArr['added_by'];
                        $questionOption['added_date'] = $createQuizFormArr['added_date'];
                        $questionOption['modified_by'] = $createQuizFormArr['modified_by'];
                        $questionOption['modified_date'] = $createQuizFormArr['modified_date'];
                        $i = 0;
                        foreach ($dataPost['question_choice'] as $optionTitle) {
                            if (!empty($optionTitle)) {
                                $questionOption['option_title'] = $optionTitle;
                                $optionId = $this->_quizTable->insertQuizOptions($questionOption);
                                if ($i == $createQuizFormArr['correct_index']) {
                                    $corrrectOptionId = $optionId;
                                }
                                $i++;
                            }
                        }

                        if (!empty($corrrectOptionId)) {
                            $correctoptionUpdate['quiz_question_id'] = $quizQuestionId;
                            $correctoptionUpdate['option_id'] = $corrrectOptionId;
                            $correctoptionUpdate['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                            $correctoptionUpdate['modified_date'] = DATE_TIME_FORMAT;
                            $this->_quizTable->updateQuizCorrectOptions($correctoptionUpdate);
                        }
                    }
                    $encryptquizid = $this->encrypt($createQuizFormArr['quiz_id_hidden']);
                    $this->flashMessenger()->addMessage($quizCreateMessages['QUIZ_ADDED']);
                    $messages = array('status' => "success", 'message' => $quizCreateMessages['QUIZ_ADDED'], 'quiz_id' => $encryptquizid);
                }

                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        } else {
            $this->layout('crm');
            $quizId = $this->decrypt($params['id']);

            $searchParam['quiz_id'] = $quizId;
            $searchQuizArr = $quiz->getQuizSearchArr($searchParam);
            $seachResult = $this->getQuizTable()->getAllQuiz($searchQuizArr);
            $createQuizQuestionform->get('quiz_id_hidden')->setAttribute('value', $quizId);
            $createQuizQuestionform->get('quiz_id_encrypted_hidden')->setAttribute('value', $params['id']);

            $messages = array();
            if ($this->_flashMessage->hasMessages()) {
                $messages = $this->_flashMessage->getMessages();
            }
            $viewModel = new ViewModel();
            $viewModel->setVariables(array(
                'jsLangTranslate' => $quizMessages,
                'quiz_id' => $quizId,
                'messages' => $messages,
                'encrypt_quiz_id' => $params['id'],
                'question_form' => $createQuizQuestionform,
                'quiz_data' => $seachResult[0]
            ));
            return $viewModel;
        }
    }

    /**
     * This Action is used to dispaly add choice  form elements
     * @author Icreon Tech-KK
     * @return Phone Form
     */
    public function addChoiceAction() {
        $CreateQuizQuestionsForm = new CreateQuizQuestionsForm();
        $this->getConfig();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $data['counter'] = isset($params['counter']) ? $params['counter'] : '';
        $data['container'] = isset($params['container']) ? $params['container'] : '';
        $data['divid'] = isset($params['divid']) ? $params['divid'] : '';
        $form_phone->get('quiz_question_choice[]')->setAttribute('id', 'quiz_question_choice_1_' . $data['counter'] . '');
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);

        $viewModel->setVariables(array(
            'CreateQuizQuestionsForm' => $CreateQuizQuestionsForm,
            'jsLangTranslate' => $this->_config['quiz_messages']['config']['create_quiz'],
            'params' => $data,
                )
        );
        return $viewModel;
    }

    /**
     * This function is used to move file from temp to quiz folder
     * @return html
     * @param void
     * @author Icreon Tech - AG
     */
    public function getQuestionsGridAction() {
        $this->checkUserAuthentication();
        $params = $this->params()->fromRoute();
        $this->getQuizTable();
        $Quiz = new Quiz($this->_adapter);
        $quizMessages = array_merge($this->_config['quiz_messages']['config']['create_quiz'], $this->_config['quiz_messages']['config']['view_quiz']);
        if (isset($params['quizId']) && $params['quizId'] != '') {
            $quizId = $this->decrypt($params['quizId']);
            $quizDetailArr = array();
            $quizDetailArr[] = $quizId;
            $quizData = $this->_quizTable->getquizById($quizDetailArr);
            $quizsArray = $Quiz->processQuizArray($quizData);
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => $quizMessages,
                'quiz_id' => $quizId,
                'encrypt_quiz_id' => $params['quizId'],
                'quiz_data' => $quizsArray,
                'login_user_detail' => $this->_auth->getIdentity()
            ));
            return $viewModel;
        }
    }

    /**
     * This function is used to display the grid for questions
     * @return html
     * @param void
     * @author Icreon Tech - KK
     */
    public function getQuestionsGridEditAction() {
        $this->checkUserAuthentication();
        $params = $this->params()->fromRoute();
        $this->getQuizTable();
        $Quiz = new Quiz($this->_adapter);
        $quizMessages = array_merge($this->_config['quiz_messages']['config']['create_quiz'], $this->_config['quiz_messages']['config']['view_quiz']);
        if (isset($params['quizId']) && $params['quizId'] != '') {
            $quizId = $this->decrypt($params['quizId']);
            $quizDetailArr = array();
            $quizDetailArr[] = $quizId;
            $quizData = $this->_quizTable->getquizById($quizDetailArr);
            $quizsArray = $Quiz->processQuizArray($quizData);
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => $quizMessages,
                'quiz_id' => $quizId,
                'encrypt_quiz_id' => $params['quizId'],
                'quiz_data' => $quizsArray,
                'login_user_detail' => $this->_auth->getIdentity()
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to delete Quiz
     * @param  void
     * @return json format string
     * @author Icreon Tech - KK
     */
    public function deleteQuizQuestionAction() {
        $request = $this->getRequest();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $questionid = $request->getPost('questionid');
            if ($questionid != '') {
                $questionid = $this->decrypt($questionid);

                $postArr = $request->getPost();
                $postArr['question_id'] = $questionid;
                $this->getQuizTable();
                $quizSearchMessages = $this->_config['quiz_messages']['config']['search_quiz'];
                $Quiz = new Quiz($this->_adapter);

                $response = $this->getResponse();

                $quizArr = $Quiz->getArrayForRemoveQuizQuestion($postArr);
                $this->_quizTable->removeQuizQuestionById($quizArr);
                $this->flashMessenger()->addMessage($quizSearchMessages['QUIZ_QUESTION_DELETE']);
                $messages = array('status' => "success", 'message' => $quizSearchMessages['QUIZ_QUESTION_DELETE']);

                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                return $this->redirect()->toRoute('crm-quizes');
            }
        } else {
            return $this->redirect()->toRoute('crm-quizes');
        }
    }

    /**
     * This action is used to view/add/delete the quiz questions
     * @param void
     * @return array
     * @author Icreon Tech - AG
     */
    public function getQuizQuestionsInfoAction() {
        $this->checkUserAuthentication();
        $this->getQuizTable();
        $quizMessages = array_merge($this->_config['quiz_messages']['config']['create_quiz'], $this->_config['quiz_messages']['config']['view_quiz'], $this->_config['quiz_messages']['config']['user_search_quiz']);
        $quiz = new Quiz($this->_adapter);
        $params = $this->params()->fromRoute();
        $createQuizQuestionform = new CreateQuizQuestionsForm();
        $request = $this->getRequest();
        $postArray = $request->getPost()->toArray();
        $response = $this->getResponse();
        $quizCreateMessages = $this->_config['quiz_messages']['config']['search_quiz'];
        if ($request->isPost() && isset($postArray['quiz_id_hidden']) && $postArray['quiz_id_hidden'] != '') {
            $postData = $request->getPost();
            $createQuizQuestionform->setData($request->getPost());
            $createQuizQuestionform->setInputFilter($quiz->getInputFilterCreateQuizQuestions());
            if (!$createQuizQuestionform->isValid()) {
                $errors = $createQuizQuestionform->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'continue') {
                        foreach ($row as $typeError => $rower) {
                            if ($typeError != 'notInArray') {
                                $msg [$key] = $quizMessages[$rower];
                            }
                        }
                    }
                }
            }
            if (!empty($msg)) {
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $createQuizFormArr = $createQuizQuestionform->getData();
                //for add
                $dataPost = $postData->toArray();

                $createQuizFormArr['quiz_id'] = $createQuizFormArr['quiz_id_hidden'];
                $createQuizFormArr['question_title'] = $createQuizFormArr['quiz_question'];
                $createQuizFormArr['is_randomized'] = $createQuizFormArr['is_randomized'];
                $createQuizFormArr['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                $createQuizFormArr['added_date'] = DATE_TIME_FORMAT;
                $createQuizFormArr['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                $createQuizFormArr['modified_date'] = DATE_TIME_FORMAT;
                $createQuizQuestionArr = $quiz->getCreateQuizQuestionArr($createQuizFormArr);
                $quizQuestionId = $this->_quizTable->saveQuizQuestions($createQuizQuestionArr);
                if (!empty($dataPost['question_choice'])) {
                    $questionOption['quiz_question_id'] = $quizQuestionId;
                    $questionOption['added_by'] = $createQuizFormArr['added_by'];
                    $questionOption['added_date'] = $createQuizFormArr['added_date'];
                    $questionOption['modified_by'] = $createQuizFormArr['modified_by'];
                    $questionOption['modified_date'] = $createQuizFormArr['modified_date'];
                    $i = 0;
                    foreach ($dataPost['question_choice'] as $optionTitle) {
                        if (!empty($optionTitle)) {
                            $questionOption['option_title'] = $optionTitle;
                            $optionId = $this->_quizTable->insertQuizOptions($questionOption);
                            if ($i == $createQuizFormArr['correct_index']) {
                                $corrrectOptionId = $optionId;
                            }
                            $i++;
                        }
                    }

                    if (!empty($corrrectOptionId)) {
                        $correctoptionUpdate['quiz_question_id'] = $quizQuestionId;
                        $correctoptionUpdate['option_id'] = $corrrectOptionId;
                        $correctoptionUpdate['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                        $correctoptionUpdate['modified_date'] = DATE_TIME_FORMAT;
                        $this->_quizTable->updateQuizCorrectOptions($correctoptionUpdate);
                    }
                }
                $encryptquizid = $this->encrypt($createQuizFormArr['quiz_id']);
                //$this->flashMessenger()->addMessage($quizCreateMessages['QUIZ_QUESTION_ADDED']);
                $messages = array('status' => "success", 'message' => $quizCreateMessages['QUIZ_QUESTION_ADDED'], 'quiz_id' => $encryptquizid);


                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        } else {
            $quizId = $this->decrypt($params['id']);
            $searchParam['quiz_id'] = $quizId;
            $searchQuizArr = $quiz->getQuizSearchArr($searchParam);
            $seachResult = $this->getQuizTable()->getAllQuiz($searchQuizArr);
            $createQuizQuestionform->get('quiz_id_hidden')->setAttribute('value', $quizId);
            $createQuizQuestionform->get('quiz_id_encrypted_hidden')->setAttribute('value', $params['id']);
            $messages = array();
            if ($this->_flashMessage->hasMessages()) {
                $messages = $this->_flashMessage->getMessages();
            }
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => $quizMessages,
                'quiz_id' => $quizId,
                'messages' => $messages,
                'encrypt_quiz_id' => $params['id'],
                'question_form' => $createQuizQuestionform,
                'quiz_data' => $seachResult[0]
            ));
            return $viewModel;
        }
    }

    /**
     * This Action is used to save the quiz search
     * @param void
     * @return this will be json format with comfirmation message
     * @author Icreon Tech -AG
     */
    public function addCrmSearchQuizAction() {
        $this->checkUserAuthentication();
        $this->getQuizTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $quiz = new Quiz($this->_adapter);
        $quizMessages = array_merge($this->_config['quiz_messages']['config']['create_quiz'], $this->_config['quiz_messages']['config']['view_quiz'], $this->_config['quiz_messages']['config']['user_search_quiz']);
        if ($request->isPost()) {
            parse_str($request->getPost('searchString'), $searchParam);
            $search_name = $request->getPost('search_name');
            $messages = array();
            parse_str($request->getPost('searchString'), $searchParam);
            $searchParam['search_title'] = $search_name;
            $searchParam = $quiz->getInputFilterQuizSearch($searchParam);
            $search_name = $quiz->getInputFilterQuizSearch($search_name);
            if (trim($search_name) != '') {
                $saveSearchParam['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $saveSearchParam['title'] = $search_name;
                $saveSearchParam['search_query'] = serialize($searchParam);
                $saveSearchParam['is_active'] = 1;
                $saveSearchParam['is_deleted'] = '0';
                if ($request->getPost('search_id') != '') {
                    $saveSearchParam['modified_date'] = DATE_TIME_FORMAT;
                    $saveSearchParam['quiz_search_id'] = $request->getPost('search_id');
                    $quizUpdateArray = $quiz->getAddQuizSearchArr($saveSearchParam);
                    if ($this->_quizTable->updateQuizSearch($quizUpdateArray) == true) {
                        $this->flashMessenger()->addMessage($quizMessages['SEARCH_UPDATED_MSG']);
                        $messages = array('status' => "success");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                } else {
                    $saveSearchParam['added_date'] = DATE_TIME_FORMAT;
                    $searchAddArray = $quiz->getAddQuizSearchArr($saveSearchParam);
                    if ($this->_quizTable->saveQuizSearch($searchAddArray) == true) {
                        $this->flashMessenger()->addMessage($quizMessages['SEARCH_ADDED_MSG']);
                        $messages = array('status' => "success");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                }
            }
        }
    }

    /**
     * This Action is used to get the saved quiz search
     * @param void
     * @return this will be json format
     * @author Icreon Tech - AG
     */
    public function getCrmSearchSavedQuizAction() {
        $this->checkUserAuthentication();
        $this->getQuizTable();
        $Quiz = new Quiz($this->_adapter);
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            if ($request->getPost('search_id') != '') {
                $search_array = array();
                $search_array['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $search_array['quiz_search_id'] = $request->getPost('search_id');
                $search_array['is_active'] = '1';
                $quizSavedSearchArray = $Quiz->getQuizSavedSearchArr($search_array);
                $quizSearchArray = $this->_quizTable->getQuizSavedSearch($quizSavedSearchArray);
                $searchResult = json_encode(unserialize($quizSearchArray[0]['search_query']));
                return $response->setContent($searchResult);
            }
        }
    }

    /**
     * This function is used to display the grid for questions edit
     * @return html
     * @param void
     * @author Icreon Tech - KK
     */
    public function createQuizEditQuestionsAction() {
        $this->checkUserAuthentication();
        $params = $this->params()->fromRoute();
        $this->getQuizTable();
        $Quiz = new Quiz($this->_adapter);
        $createQuizQuestionform = new EditQuizQuestionsForm();
        $quizMessages = array_merge($this->_config['quiz_messages']['config']['create_quiz'], $this->_config['quiz_messages']['config']['view_quiz'], $this->_config['quiz_messages']['config']['search_quiz']);
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $postData = $request->getPost();
            $createQuizQuestionform->setData($request->getPost());
            $createQuizQuestionform->setInputFilter($Quiz->getInputFilterEditQuizQuestions());
            if (!$createQuizQuestionform->isValid()) {
                $errors = $createQuizQuestionform->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'continue') {
                        foreach ($row as $typeError => $rower) {
                            if ($typeError != 'notInArray') {
                                $msg [$key] = $quizMessages[$rower];
                            }
                        }
                    }
                }
            }

            if (!empty($msg)) {
                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $createQuizFormArr = $createQuizQuestionform->getData();
                $dataPost = $postData->toArray();
                $createQuizFormArr['quiz_id'] = $createQuizFormArr['quiz_id_hidden'];
                $createQuizFormArr['question_title'] = $createQuizFormArr['quiz_question_edit'];
                $createQuizFormArr['is_randomized'] = $createQuizFormArr['is_randomized'];
                $createQuizFormArr['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                $createQuizFormArr['added_date'] = DATE_TIME_FORMAT;
                $createQuizFormArr['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                $createQuizFormArr['modified_date'] = DATE_TIME_FORMAT;
                $quizQuestionId = $createQuizFormArr['question_id'];
                $questionTitle = $createQuizFormArr['quiz_question_edit'];
                $updateQuizQuestion['question_id'] = $quizQuestionId;
                $updateQuizQuestion['question_title'] = $questionTitle;
                $updateQuizQuestion['is_randomized'] = $createQuizFormArr['is_randomized_question'];
                $updateQuizQuestion['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                $updateQuizQuestion['modified_date'] = DATE_TIME_FORMAT;
                $this->_quizTable->updateCrmQuizQuestion($updateQuizQuestion);
                if (!empty($dataPost['question_choice_new'])) {
                    $questionOption['quiz_question_id'] = $quizQuestionId;
                    $questionOption['added_by'] = $createQuizFormArr['added_by'];
                    $questionOption['added_date'] = $createQuizFormArr['added_date'];
                    $questionOption['modified_by'] = $createQuizFormArr['modified_by'];
                    $questionOption['modified_date'] = $createQuizFormArr['modified_date'];
                    $i = 0;
                    foreach ($dataPost['question_choice_new'] as $optionTitle) {
                        if (!empty($optionTitle)) {
                            $questionOption['option_title'] = $optionTitle;
                            $optionId = $this->_quizTable->insertQuizOptions($questionOption);
                            if ($dataPost['choice_type'] == 'new') {
                                if ($i == $createQuizFormArr['correct_index_question']) {
                                    $corrrectOptionId = $optionId;
                                }
                            }
                            $i++;
                        }
                    }

                    if (!empty($corrrectOptionId)) {
                        $correctoptionUpdate['quiz_question_id'] = $quizQuestionId;
                        $correctoptionUpdate['option_id'] = $corrrectOptionId;
                        $correctoptionUpdate['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                        $correctoptionUpdate['modified_date'] = DATE_TIME_FORMAT;
                        $this->_quizTable->updateQuizCorrectOptions($correctoptionUpdate);
                    }
                }

                $allChoices = explode(",", $createQuizFormArr['all_choice']);
                $allChoicesRemain = explode(",", $createQuizFormArr['all_choice_remain']);
                $deleteOptionArray = array_diff($allChoices, $allChoicesRemain);
                if (!empty($deleteOptionArray)) {
                    foreach ($deleteOptionArray as $deleteoption) {
                        $deleeoptionArray['option_id'] = $deleteoption;
                        $this->_quizTable->deletQuestionOption($deleeoptionArray);
                    }
                }
                if (!empty($dataPost['question_choice'])) {

                    $updateOption['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                    $updateOption['modified_date'] = DATE_TIME_FORMAT;
                    $updateoption = 0;
                    foreach ($dataPost['question_choice'] as $optionTitle) {
                        $updateOption['option_title'] = $optionTitle;
                        $updateOption['option_id'] = $allChoicesRemain[$updateoption];
                        $this->_quizTable->updateQuestionOptions($updateOption);
                        $updateoption++;
                    }
                    if ($dataPost['choice_type'] == 'old') {

                        if (!empty($createQuizFormArr['correct_index_question'])) {
                            $correctoptionUpdate['quiz_question_id'] = $quizQuestionId;
                            $correctoptionUpdate['option_id'] = $createQuizFormArr['correct_index_question'];
                            $correctoptionUpdate['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                            $correctoptionUpdate['modified_date'] = DATE_TIME_FORMAT;
                            $this->_quizTable->updateQuizCorrectOptions($correctoptionUpdate);
                        }
                    }
                }

                $encryptquizid = $this->encrypt($createQuizFormArr['quiz_id_hidden']);
                $this->flashMessenger()->addMessage($quizMessages['QUIZ_QUESTION_UPDATED']);
                $messages = array('status' => "success", 'message' => $quizMessages['QUIZ_QUESTION_UPDATED'], 'quiz_id' => $encryptquizid);
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        } else {
            if (isset($params['quizId']) && $params['quizId'] != '') {

                $quizId = $this->decrypt($params['quizId']);
                $createQuizQuestionform->get('quiz_id_hidden')->setAttribute('value', $quizId);
                $createQuizQuestionform->get('quiz_id_encrypted_hidden')->setAttribute('value', $params['quizId']);

                $questionId = $this->decrypt($params['questionId']);
                $quizDetailArr = array();
                $quizDetailArr[] = $quizId;
                $quizDetailArr[] = $questionId;
                $quizData = $this->_quizTable->getquizById($quizDetailArr);
                $quizsArray = $Quiz->processQuizArray($quizData);
                $question = $quizsArray[$quizId]['quizQuestion'][$questionId]['question'];
                $is_randomized = $quizsArray[$quizId]['quizQuestion'][$questionId]['is_randomized'];

                $correct_option_id = $quizsArray[$quizId]['quizQuestion'][$questionId]['correct_option_id'];
                $all_keys = array_keys($quizsArray[$quizId]['quizQuestion'][$questionId]['option']);
                $createQuizQuestionform->get('quiz_question_edit')->setAttribute('value', $question);
                $createQuizQuestionform->get('question_id')->setAttribute('value', $questionId);
                $createQuizQuestionform->get('quiz_id_hidden')->setAttribute('value', $quizId);
                $createQuizQuestionform->get('quiz_id_encrypted_hidden')->setAttribute('value', $params['quizId']);
                $createQuizQuestionform->get('question_id_encrypted')->setAttribute('value', $params['questionId']);


                $createQuizQuestionform->get('is_randomized_question')->setAttribute('checked', $is_randomized);
                $createQuizQuestionform->get('correct_index_question')->setAttribute('value', $correct_option_id);
                $createQuizQuestionform->get('choice_type')->setAttribute('value', 'old');
                $createQuizQuestionform->get('all_choice')->setAttribute('value', implode(",", $all_keys));
                $createQuizQuestionform->get('all_choice_remain')->setAttribute('value', implode(",", $all_keys));
                $viewModel = new ViewModel();
                $viewModel->setTerminal(true);
                $viewModel->setVariables(array(
                    'jsLangTranslate' => $quizMessages,
                    'quiz_id' => $quizId,
                    'encrypt_quiz_id' => $params['quizId'],
                    'question_id' => $questionId,
                    'encrypt_question_id' => $params['questionId'],
                    'quiz_data' => $quizsArray,
                    'question_form' => $createQuizQuestionform,
                    'login_user_detail' => $this->_auth->getIdentity()
                ));
                return $viewModel;
            }
        }
    }

    /**
     * This Action is used to delete the saved quiz search
     * @param void
     * @return this will be a confirmation message in json
     * @author Icreon Tech - AG
     */
    public function deleteCrmSearchQuizAction() {
        $this->checkUserAuthentication();
        $this->getQuizTable();
        $quiz = new Quiz($this->_adapter);
        $request = $this->getRequest();
        $response = $this->getResponse();
        $quizMessages = array_merge($this->_config['quiz_messages']['config']['create_quiz'], $this->_config['quiz_messages']['config']['view_quiz'], $this->_config['quiz_messages']['config']['search_quiz']);
        if ($request->isPost()) {
            $delete_array = array();
            $delete_array['is_deleted'] = 1;
            $delete_array['modified_date'] = DATE_TIME_FORMAT;
            $delete_array['quiz_search_id'] = $request->getPost('search_id');
            $quiz_delete_saved_search_array = $quiz->getDeleteQuizSavedSearchArr($delete_array);
            $this->_quizTable->deleteSavedSearch($quiz_delete_saved_search_array);
            $messages = array('status' => "success");
            $this->flashMessenger()->addMessage($quizMessages['SEARCH_DELETE_CONFIRM']);
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This action is used to display the Quiz pages on front end
     * @param void
     * @return array
     * @author Icreon Tech - KK
     */
    public function getQuizzesAction() {

        $request = $this->getRequest();
        //$this->checkUserAuthentication();
        $this->getQuizTable();
        $Quiz = new Quiz($this->_adapter);
        $params = $this->params()->fromRoute();
        $viewModel = new ViewModel();
        $response = $this->getResponse();
        $searchQuizForm = new SearchQuizForm();
        $response = $this->getResponse();
        $Quiz = new Quiz($this->_adapter);
        $categoriesArray = $this->getQuizTable()->getQuizCategories();
        $categoryName = array("" => "Select One");
        $categoryDesc = array();
        foreach ($categoriesArray as $key => $val) {
            $categoryName[$val['quiz_category_id']] = stripslashes($val['category_name']);
            $categoryDesc[$val['quiz_category_id']] = stripslashes($val['category_description']);
        }
        $searchQuizForm->get('main_category_id')->setAttribute('options', $categoryName);
        $messages = array();
        $viewModel->setVariables(array(
            'search_quiz_form' => $searchQuizForm,
            'messages' => $messages,
            'categoryDesc' => $categoryDesc,
            'file_upload_path' => $this->_config['file_upload_path'],
            'jsLangTranslate' => array_merge($this->_config['quiz_messages']['config']['quiz_front_end'], $this->_config['quiz_messages']['config']['search_quiz'])
        ));
        return $viewModel;
    }

    /**
     * This action is used to display the Quiz pages on front end
     * @param void
     * @return array
     * @author Icreon Tech - KK
     */
    public function getQuizListFrontAction() {

        $request = $this->getRequest();
        $this->getQuizTable();
        $Quiz = new Quiz($this->_adapter);
        $viewModel = new ViewModel();
        $Quiz = new Quiz($this->_adapter);
        parse_str($request->getPost('searchString'), $searchParam);
        $limit = $this->_config['grid_config']['numRecPerPage'];
        $page = $request->getPost('page');
        $start = $limit * $page - $limit; // do not put $limit*($page - 1)
        $searchParam['start_index'] = $start;
        $searchParam['record_limit'] = $limit;
        $searchParam['sort_field'] = $searchParam['sort_field'];
        $searchParam['sort_order'] = $searchParam['sort_order'];
        $searchParam['user_id'] = !empty($this->_auth->getIdentity()->user_id) ? $this->_auth->getIdentity()->user_id : '0';
        $searchQuizArr = $Quiz->getQuizSearchArrFe($searchParam);
        // asd($searchQuizArr);
        $seachResult = $this->getQuizTable()->getAllQuizFrontEnd($searchQuizArr);
        $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
        $resultArray = array();
        if (!empty($searchParam['user_id'])) {
            if (!empty($seachResult)) {
                foreach ($seachResult as $quizData) {
                    $quizAnswer['quiz_id'] = $quizData['quiz_id'];
                    $quizAnswer['user_id'] = $searchParam['user_id'];
                    $answersQuizArr = $Quiz->getQuizUserAnswers($quizAnswer);
                    $quizResultAnswers = $this->getQuizTable()->getAllQuizAnswers($answersQuizArr);
                    $quizData['userresult'] = $quizResultAnswers;
                    $resultArray[] = $quizData;
                }
            }
            $seachResult = $resultArray;
        }
        $jsonResult = array();

        $jsonResult['page'] = $page;
        $jsonResult['limit'] = $limit;
        $jsonResult['records'] = $countResult[0]->RecordCount;
        $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
        $messages = array();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'facebookAppId' => $this->_config['facebook_app']['app_id'],
            'seachresult' => $seachResult,
            'resultcount' => $jsonResult,
            'searchParam' => $searchParam,
            'messages' => $messages,
            'file_upload_path' => $this->_config['file_upload_path'],
            'login_user_detail' => $this->_auth->getIdentity(),
            'jsLangTranslate' => array_merge($this->_config['quiz_messages']['config']['quiz_front_end'], $this->_config['quiz_messages']['config']['search_quiz']),
			'allowed_IP' => $this->_config['allowed_ip']['ip']
        ));
        return $viewModel;
    }

    /**
     * This action is used to display the Quiz Info on front end
     * @param void
     * @return array
     * @author Icreon Tech - KK
     */
    function getQuizInfoAction() {

        $params = $this->params()->fromRoute();
        $this->getQuizTable();
        $Quiz = new Quiz($this->_adapter);
        $quizMessages = array_merge($this->_config['quiz_messages']['config']['quiz_front_end'], $this->_config['quiz_messages']['config']['view_quiz']);
        if (isset($params['id']) && $params['id'] != '') {
            $quizId = $this->decrypt($params['id']);
            $quizDetailArr = array();
            $quizDetailArr[] = $quizId;
            $quizDetailArr[] = DATE_TIME_FORMAT;
            $quizData = $this->_quizTable->getQuizInfoById($quizDetailArr);
            $quizAdditionalCategoryData = $this->_quizTable->getquizAdditionalCategoriesById($quizDetailArr);
            $quizAdditionalCategories = "";
            $quizAdditionalCategories = $Quiz->getquizAdditionalCategoriesString($quizAdditionalCategoryData);
            $quizAnswer['quiz_id'] = $quizData[0]['quiz_id'];
            $quizAnswer['user_id'] = !empty($this->_auth->getIdentity()->user_id) ? $this->_auth->getIdentity()->user_id : '0';
            $quizAnswer['user_quiz_id'] = '';
            $quizResultAnswers = array();
            if (!empty($quizAnswer['user_id'])) {
                $answersQuizArr = $Quiz->getQuizUserAnswers($quizAnswer);
                $quizResultAnswers = $this->getQuizTable()->getAllQuizAnswers($answersQuizArr);
            } else {
                $quizAnswer['user_session_id'] = session_id();
                $answersQuizArr = $Quiz->getQuizUserAnswers($quizAnswer);
                $quizResultAnswers = $this->getQuizTable()->getAllQuizAnswers($answersQuizArr);
            }

            $demoQuestion = array();
            $countQuizDemoAttempt = array();
            if (!empty($quizResultAnswers)) {
                $userQuiz_id = $quizResultAnswers[0]['user_quiz_id'];


                if (!empty($quizData[0]['gender'])) {
                    $demoQuestion[] = "'" . $quizData[0]['gender'] . "'";
                }


                if (!empty($quizData[0]['born_place'])) {
                    $demoQuestion[] = "'" . $quizData[0]['born_place'] . "'";
                }

                if (!empty($quizData[0]['residence_place'])) {
                    $demoQuestion[] = "'" . $quizData[0]['residence_place'] . "'";
                }

                if (!empty($quizData[0]['grade_or_class'])) {
                    $demoQuestion[] = "'" . $quizData[0]['grade_or_class'] . "'";
                }
                $demoQuestionsDisplay = '';
                if (!empty($demoQuestion)) {
                    $demoQuestionsDisplay = implode(",", $demoQuestion);
                }
                $quizDetailArr['is_demographic_question'] = '1';
                $quizDetailArr['demographic_question'] = $demoQuestionsDisplay;


                $QuizArr = $Quiz->getQuizQuestionArrFe($quizDetailArr);
                $questionArray = $this->_quizTable->getQuizQuestionInfoById($QuizArr);
                
                $countQuizDemoAttempt = $this->_quizTable->getQuizDemographicAttempt($userQuiz_id);
                //echo count($demoQuestion);
                //$countQuizDemoAttempt[0]['total_counter'];
            }
            $viewModel = new ViewModel();
            $viewModel->setVariables(array(
                'jsLangTranslate' => $quizMessages,
                'quiz_id' => $quizId,
                'encrypt_quiz_id' => $params['id'],
                'quizData' => $quizData[0],
                'userresult' => $quizResultAnswers,
                'demoQuestion' => $demoQuestion,
                'countQuizDemoAttempt' => $countQuizDemoAttempt,
                'file_upload_path' => $this->_config['file_upload_path'],
                'quiz_addtnl_category' => $quizAdditionalCategories,
                'login_user_detail' => $this->_auth->getIdentity()
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to play the Quiz  on front end
     * @param void
     * @return array
     * @author Icreon Tech - KK
     */
    function getQuizPlayAction() {
        $params = $this->params()->fromRoute();
        $this->getQuizTable();
        $request = $this->getRequest();
        $Quiz = new Quiz($this->_adapter);
        $quizMessages = array_merge($this->_config['quiz_messages']['config']['quiz_front_end'], $this->_config['quiz_messages']['config']['view_quiz']);
        if (isset($params['id']) && $params['id'] != '' && $request->isPost()) {

            $quizId = $this->decrypt($params['id']);
            $quizUserId = '';
            if ($request->getPost('start_quiz') == '1') {
                $insertUserArray['quiz_id'] = $quizId;
                $insertUserArray['user_id'] = !empty($this->_auth->getIdentity()->user_id) ? $this->_auth->getIdentity()->user_id : '0';
                $insertUserArray['play_date'] = DATE_TIME_FORMAT;
                $insertUserArray['user_session_id'] = session_id();
                $quizUserId = $this->_quizTable->insertUserQuiz($insertUserArray);
            }


            $retake = $request->getPost('retake');
            if (!empty($retake)) {
                $quizUserId = $request->getPost('quizUserId');
                //echo "here";
            } else {
                if ($request->getPost('quizUserId') != '') {
                    $quizUserId = $request->getPost('quizUserId');
                    $questionId = $this->decrypt($request->getPost('questionId'));
                    $optionId = $this->decrypt($request->getPost('optionId'));
                    $correct_option = $this->decrypt($request->getPost('correct_option'));

                    if ($correct_option == $optionId) {
                        $isCorrect = '1';

                        //$this->_quizTable->updateQuizCorrectCount($quizUserId);
                    } else {
                        $isCorrect = '0';
                    }
                    $insertOptionArray['user_quiz_id'] = $quizUserId;
                    $insertOptionArray['quiz_id'] = $quizId;
                    $insertOptionArray['quiz_question_id'] = $questionId;
                    $insertOptionArray['quiz_option_id'] = $optionId;
                    $insertOptionArray['is_correct'] = $isCorrect;
                    $insertOptionArray['play_date'] = DATE_TIME_FORMAT;

                    if ($request->getPost('demographic_type') == '1') {
                        $insertOptionArray['is_demographic'] = '1';
                    } else {
                        $insertOptionArray['is_demographic'] = '0';
                    }
                    $quizUserOptionId = $this->_quizTable->insertUserQuizOptions($insertOptionArray);
                }
            }
            $start_demo = $request->getPost('start_demo');
            $startIndex = $request->getPost('start_index');
            $startLimit = $request->getPost('start_limit');
            $quizDetailArr = array();
            $quizDetailArr[] = $quizId;
            $quizDetailArr[] = DATE_TIME_FORMAT;
            $quizData = $this->_quizTable->getQuizInfoById($quizDetailArr);
            $demoQuestion = array();
            if (!empty($quizData[0]['gender'])) {
                $demoQuestion[] = "'" . $quizData[0]['gender'] . "'";
            }


            if (!empty($quizData[0]['born_place'])) {
                $demoQuestion[] = "'" . $quizData[0]['born_place'] . "'";
            }

            if (!empty($quizData[0]['residence_place'])) {
                $demoQuestion[] = "'" . $quizData[0]['residence_place'] . "'";
            }

            if (!empty($quizData[0]['grade_or_class'])) {
                $demoQuestion[] = "'" . $quizData[0]['grade_or_class'] . "'";
            }
            $demoQuestionsDisplay = '';
            if (!empty($demoQuestion)) {
                $demoQuestionsDisplay = implode(",", $demoQuestion);
            }
            $quizAdditionalCategoryData = $this->_quizTable->getquizAdditionalCategoriesById($quizDetailArr);
            $quizAdditionalCategories = "";
            $quizAdditionalCategories = $Quiz->getquizAdditionalCategoriesString($quizAdditionalCategoryData);
            $quizQuestionData = $this->_quizTable->getQuizInfoById($quizDetailArr);
            $quizDetailArr['quiz_id'] = $quizId;
            $quizDetailArr['start_index'] = $startIndex;
            $quizDetailArr['start_limit'] = $startLimit;
            if ($start_demo == '1') {
                $quizDetailArr['is_demographic_question'] = '1';
                $quizDetailArr['demographic_question'] = $demoQuestionsDisplay;
            }
            $QuizArr = $Quiz->getQuizQuestionArrFe($quizDetailArr);
            $questionArray = $this->_quizTable->getQuizQuestionInfoById($QuizArr);
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $totalQuest = $countResult[0]->RecordCount;
            $questionId = $this->encrypt($questionArray[0]['quiz_question_id']);
            $questionOptions = explode("###", $questionArray[0]['optionTitles']);
            $questionOptionsIds = explode("###", $questionArray[0]['optionIds']);
            $newOptionArray = array_combine($questionOptionsIds, $questionOptions);
            if ($questionArray[0]['is_randomized'] == '1') {
                uksort($newOptionArray, function() {
                            return rand() > rand();
                        });
            }
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => $quizMessages,
                'quiz_id' => $quizId,
                'encrypt_quiz_id' => $params['id'],
                'quizData' => $quizData[0],
                'quizOptionArray' => $newOptionArray,
                'quizQuestionArray' => $questionArray[0],
                'question_number' => $startIndex + 1,
                'questionId' => $questionId,
                'totalQuestions' => $totalQuest,
                'quizUserId' => $quizUserId,
                'demoQuestionsDisplay' => $demoQuestionsDisplay,
                'file_upload_path' => $this->_config['file_upload_path'],
                'start_demo' => $start_demo,
                'quiz_addtnl_category' => $quizAdditionalCategories,
                'login_user_detail' => $this->_auth->getIdentity()
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to display the Quiz result on front end
     * @param void
     * @return array
     * @author Icreon Tech - KK
     */
    function getQuizResultAction() {
        $params = $this->params()->fromRoute();
        $this->getQuizTable();
        $Quiz = new Quiz($this->_adapter);
        $quizMessages = array_merge($this->_config['quiz_messages']['config']['quiz_front_end'], $this->_config['quiz_messages']['config']['view_quiz']);
        if (isset($params['id']) && $params['id'] != '') {
            $quizId = $this->decrypt($params['id']);
            $quizUserId = '';
            if (isset($params['quizUserId'])) {
                $quizUserId = $this->decrypt($params['quizUserId']);
            }


            $quizDetailArr = array();
            $quizDetailArr[] = $quizId;
            $quizDetailArr[] = DATE_TIME_FORMAT;
            $quizData = $this->_quizTable->getQuizInfoById($quizDetailArr);
            $quizAdditionalCategoryData = $this->_quizTable->getquizAdditionalCategoriesById($quizDetailArr);
            $quizAdditionalCategories = "";
            $quizAdditionalCategories = $Quiz->getquizAdditionalCategoriesString($quizAdditionalCategoryData);
            $viewModel = new ViewModel();
            $quizAnswer['quiz_id'] = $quizData[0]['quiz_id'];
            $quizAnswer['user_id'] = !empty($this->_auth->getIdentity()->user_id) ? $this->_auth->getIdentity()->user_id : '0';
            $quizAnswer['user_quiz_id'] = $quizUserId;
            if (!empty($quizAnswer['user_id']) || !empty($quizAnswer['user_quiz_id'])) {
                $answersQuizArr = $Quiz->getQuizUserAnswers($quizAnswer);
                $quizResultAnswers = $this->getQuizTable()->getAllQuizAnswers($answersQuizArr);
                $quizData['userresult'] = $quizResultAnswers;
            }
            $quizPlayed['quiz_id'] = $quizAnswer['quiz_id'];
            $quizPLayedArray = $Quiz->getQuizUserAnswers($quizPlayed);
            $quizPlayedResult = $this->getQuizTable()->getAllQuizAnswers($quizPLayedArray);
            $quizPlayedBY = count($quizPlayedResult);
            $totalQuestions = $quizData[0]['num_questions'];
            $quizPlayed['quiz_id'] = $quizAnswer['quiz_id'];
            $quizPLayedArray = $Quiz->getQuizUserAnswers($quizPlayed);
            $quizPlayedResult = $this->getQuizTable()->getAllQuizAnswersGraph($quizPLayedArray);

            $newArray = array();
            if (!empty($quizPlayedResult)) {
                foreach ($quizPlayedResult as $result) {
                    $newArray[$result['num_correct']] = round(($result['questions_attempt'] / $quizPlayedBY) * 100);
                    $percenrage = round(($result['questions_attempt'] / $quizPlayedBY) / 100);
                }
            }
            for ($i = 0; $i <= $totalQuestions; $i++) {
                if (array_key_exists($i, $newArray)) {
                    
                } else {
                    $newArray[$i] = 0;
                }
            }
            ksort($newArray);
            $questioByQuestionArray['quiz_id'] = $quizId;
            $questioByQuestionArray['user_id'] = !empty($this->_auth->getIdentity()->user_id) ? $this->_auth->getIdentity()->user_id : '0';
            $questioByQuestionArray['user_quiz_id'] = $quizUserId;
            $filteredArrayQuestion = $Quiz->getQuizUserAnswersTable($questioByQuestionArray);
            $quizResultQuestionByQuestion = $this->getQuizTable()->getAllQuizAnswersQuestionsByQuestions($filteredArrayQuestion);
            $newDemoArray = array();
            $demoQuestion = array();
            $demoArray = array();
            if (!empty($quizData[0]['gender'])) {
                $demoQuestion[] = "'" . $quizData[0]['gender'] . "'";
            }

            if (!empty($quizData[0]['born_place'])) {
                $demoQuestion[] = "'" . $quizData[0]['born_place'] . "'";
            }

            if (!empty($quizData[0]['residence_place'])) {
                $demoQuestion[] = "'" . $quizData[0]['residence_place'] . "'";
            }

            if (!empty($quizData[0]['grade_or_class'])) {
                $demoQuestion[] = "'" . $quizData[0]['grade_or_class'] . "'";
            }
            $demoQuestionsDisplay = '';
            if (!empty($demoQuestion)) {


                $demographicStat = $this->_quizTable->getQuizDemographicStat($quizId);
                if (!empty($demographicStat)) {
                    foreach ($demographicStat as $demodata) {
                        $demoArray[$demodata['quiz_question_id']][$demodata['quiz_option_id']] = $demodata;
                    }
                }

                $demoQuestionsDisplay = implode(",", $demoQuestion);


                $quizDetailArrDem['start_index'] = '0';
                $quizDetailArrDem['start_limit'] = count($demoQuestion);
                $quizDetailArrDem['is_demographic_question'] = '1';
                $quizDetailArrDem['demographic_question'] = $demoQuestionsDisplay;


                $QuizDemoArr = $Quiz->getQuizQuestionArrFe($quizDetailArrDem);
                $questionArrayDemo = $this->_quizTable->getQuizQuestionInfoById($QuizDemoArr);

                foreach ($questionArrayDemo as $quizDataDemo) {
                    $questionOptions = explode("###", $quizDataDemo['optionTitles']);
                    $questionOptionsIds = explode("###", $quizDataDemo['optionIds']);
                    $newOptionArray = array_combine($questionOptionsIds, $questionOptions);
                    $quizDataDemo['questionOptionArray'] = $newOptionArray;
                    $newDemoArray[] = $quizDataDemo;
                }
            }
            //asd($newDemoArray);

            $colorArray = array_fill(0, count($newArray), '#DDDAC8');
            if (!empty($quizData['userresult'])) {
                $colorArray[$quizData['userresult'][0]['num_correct']] = '#D2A932';
            }

            $viewModel->setVariables(array(
                'jsLangTranslate' => $quizMessages,
                'quiz_id' => $quizId,
                'encrypt_quiz_id' => $params['id'],
                'quiz_user_id' => $quizUserId,
                'quiz_data' => $quizData,
                'graphResult' => $newArray,
                'colorarray' => json_encode($colorArray),
                'graphResultencode' => json_encode($newArray),
                'graphticks' => json_encode(array_keys($newArray)),
                'quizResultQuestionByQuestion' => $quizResultQuestionByQuestion,
                'newdemoArray' => $newDemoArray,
                'demoArray' => $demoArray,
                'file_upload_path' => $this->_config['file_upload_path'],
                'quiz_addtnl_category' => $quizAdditionalCategories,
                'login_user_detail' => $this->_auth->getIdentity()
            ));
            return $viewModel;
        }
    }

}