<?php

/**
 * This model is used for Quiz validation.
 * @category   Zend
 * @package    Quiz
 * @author     Icreon Tech - AS
 */

namespace Quiz\Model;

use Quiz\Module;
use Base\Model\BaseModel;
use Zend\Db\Adapter\Adapter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Quiz extends BaseModel {

    protected $inputFilter;
    protected $adapter;

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    /**
     * Function used to check variables cases     
     * @param Array
     * @return array
     * @author Icreon Tech - AS
     */
    public function exchangeArray($param = array()) {
        
    }

    public function getInputFilterCrmGallery() {
        
    }

    /**
     * This method is used to return the stored procedure array for search Quiz
     * @param array
     * @return array
     * @author Icreon Tech - KK

     */
    public function getQuizSearchArr($dataArr = array()) {

        $returnArr = array();
        $returnArr[] = (isset($dataArr['quiz_title']) && $dataArr['quiz_title'] != '') ? $dataArr['quiz_title'] : '';
        $returnArr[] = (isset($dataArr['main_category_id']) && $dataArr['main_category_id'] != '') ? $dataArr['main_category_id'] : '';
        $returnArr[] = (isset($dataArr['start_date']) && $dataArr['start_date'] != '') ? $dataArr['start_date'] : '';
        $returnArr[] = (isset($dataArr['end_date']) && $dataArr['end_date'] != '') ? $dataArr['end_date'] : '';
        $returnArr[] = (isset($dataArr['avail_member']) && $dataArr['avail_member'] != '') ? $dataArr['avail_member'] : '';
        $returnArr[] = (isset($dataArr['avail_non_member']) && $dataArr['avail_non_member'] != '') ? $dataArr['avail_non_member'] : '';
        $returnArr[] = (isset($dataArr['avail_anonymous']) && $dataArr['avail_anonymous'] != '') ? $dataArr['avail_anonymous'] : '';
        $returnArr[] = (isset($dataArr['quiz_status']) && $dataArr['quiz_status'] != '') ? $dataArr['quiz_status'] : '';
        $returnArr[] = (isset($dataArr['is_featured']) && $dataArr['is_featured'] != '') ? $dataArr['is_featured'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $returnArr[] = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $returnArr[] = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        $returnArr[] = (isset($dataArr['quiz_id']) && $dataArr['quiz_id'] != '') ? $dataArr['quiz_id'] : '';

        return $returnArr;
    }

    /**
     * Function used to get array of elements need to call remove quiz
     * @param Array
     * @return Array
     * @author Icreon Tech - KK
     */
    public function getArrayForRemoveQuiz($dataArr) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['quiz_id']) && $dataArr['quiz_id'] != '') ? $dataArr['quiz_id'] : '';
        return $returnArr;
    }

    /**
     * This method is used to call add Quiz procedure parameter
     * @param array
     * @return array
     * @author Icreon Tech - KK
     */
    public function getCreateQuizArr($data) {
        $returnArr = array();
        $returnArr[] = (isset($data['quiz_title']) && !empty($data['quiz_title'])) ? $data['quiz_title'] : '';
        $returnArr[] = (isset($data['quiz_tagline']) && !empty($data['quiz_tagline'])) ? $data['quiz_tagline'] : '';
        $returnArr[] = (isset($data['main_category_id']) && !empty($data['main_category_id'])) ? $data['main_category_id'] : '';
        if (isset($data['expiration_date']) && !empty($data['expiration_time'])) {

            $returnArr[] = $data['expiration_date'] . " " . $data['expiration_time'];
        } else {
            $returnArr[] = '';
        }
        $returnArr[] = (isset($data['brief_description']) && !empty($data['brief_description'])) ? $data['brief_description'] : '';
        $returnArr[] = (isset($data['full_description']) && !empty($data['full_description'])) ? $data['full_description'] : '';
        if (isset($data['avail_member']) && !empty($data['avail_member'])) {
            $avail_member[] = '1';
        }
        if (isset($data['avail_non_member']) && !empty($data['avail_non_member'])) {
            $avail_member[] = '2';
        }
        if (isset($data['avail_anonymous']) && !empty($data['avail_anonymous'])) {
            $avail_member[] = '3';
        }
        if (!empty($avail_member)) {
            $returnArr[] = implode(',', $avail_member);
        } else {
            $returnArr[] = '';
        }
        $returnArr[] = (isset($data['is_featured']) && !empty($data['is_featured'])) ? $data['is_featured'] : '';
        $returnArr[] = (isset($data['is_retakeble_quiz']) && !empty($data['is_retakeble_quiz'])) ? $data['is_retakeble_quiz'] : '';
        $returnArr[] = (isset($data['enable_answers']) && !empty($data['enable_answers'])) ? $data['enable_answers'] : '';
        $returnArr[] = (isset($data['main_image_hidden']) && !empty($data['main_image_hidden'])) ? $data['main_image_hidden'] : '';
        $returnArr[] = (isset($data['gender']) && !empty($data['gender'])) ? '1' : '';
        $returnArr[] = (isset($data['born_place']) && !empty($data['born_place'])) ? '3' : '';
        $returnArr[] = (isset($data['residence_place']) && !empty($data['residence_place'])) ? '4' : '';
        $returnArr[] = (isset($data['grade_or_class']) && !empty($data['grade_or_class'])) ? '5' : '';
        
        $returnArr[] = (isset($data['added_by']) && !empty($data['added_by'])) ? $data['added_by'] : '';
        $returnArr[] = (isset($data['added_date']) && !empty($data['added_date'])) ? $data['added_date'] : '';
        $returnArr[] = (isset($data['modify_by']) && !empty($data['modify_by'])) ? $data['modify_by'] : '';
        $returnArr[] = (isset($data['modify_date']) && !empty($data['modify_date'])) ? $data['modify_date'] : '';
        if (isset($data['avail_member_demo']) && !empty($data['avail_member_demo'])) {
            $avail_member_demo[] = '1';
        }
        if (isset($data['avail_non_member_demo']) && !empty($data['avail_non_member_demo'])) {
            $avail_member_demo[] = '2';
        }
        if (isset($data['avail_anonymous_demo']) && !empty($data['avail_anonymous_demo'])) {
            $avail_member_demo[] = '3';
        }
        if (!empty($avail_member_demo)) {
            $returnArr[] = implode(',', $avail_member_demo);
        } else {
            $returnArr[] = '';
        }
        return $returnArr;
    }

    /**
     * This method is used to call edit Quiz procedure parameter
     * @param array
     * @return array
     * @author Icreon Tech - KK
     */
    public function getUpdateQuizArr($data) {
        $returnArr = array();
        $returnArr[] = (isset($data['quiz_id']) && !empty($data['quiz_id'])) ? $data['quiz_id'] : '';
        $returnArr[] = (isset($data['quiz_title']) && !empty($data['quiz_title'])) ? $data['quiz_title'] : '';
        if ((isset($data['updateandpublish']) && $data['updateandpublish'] == 'Update & Publish') || $data['is_published'] == '1') {
            $returnArr[] = 1;
        } else {
            $returnArr[] = 0;
        }
        $returnArr[] = (isset($data['quiz_tagline']) && !empty($data['quiz_tagline'])) ? $data['quiz_tagline'] : '';
        $returnArr[] = (isset($data['main_category_id']) && !empty($data['main_category_id'])) ? $data['main_category_id'] : '';
        if (isset($data['expiration_date']) && !empty($data['expiration_time'])) {

            $returnArr[] = $data['expiration_date'] . " " . $data['expiration_time'];
        } else {
            $returnArr[] = '';
        }
        $returnArr[] = (isset($data['brief_description']) && !empty($data['brief_description'])) ? $data['brief_description'] : '';
        $returnArr[] = (isset($data['full_description']) && !empty($data['full_description'])) ? $data['full_description'] : '';
        if (isset($data['avail_member']) && !empty($data['avail_member'])) {
            $avail_member[] = '1';
        }
        if (isset($data['avail_non_member']) && !empty($data['avail_non_member'])) {
            $avail_member[] = '2';
        }
        if (isset($data['avail_anonymous']) && !empty($data['avail_anonymous'])) {
            $avail_member[] = '3';
        }
        if (!empty($avail_member)) {
            $returnArr[] = implode(',', $avail_member);
        } else {
            $returnArr[] = '';
        }
        $returnArr[] = (isset($data['is_featured']) && !empty($data['is_featured'])) ? $data['is_featured'] : '';
        $returnArr[] = (isset($data['is_retakeble_quiz']) && !empty($data['is_retakeble_quiz'])) ? $data['is_retakeble_quiz'] : '';
        $returnArr[] = (isset($data['enable_answers']) && !empty($data['enable_answers'])) ? $data['enable_answers'] : '';
        $returnArr[] = (isset($data['new_image_hidden']) && !empty($data['new_image_hidden'])) ? $data['new_image_hidden'] : $data['main_image_hidden'];
        $returnArr[] = (isset($data['gender']) && !empty($data['gender'])) ? '1' : '';
        $returnArr[] = (isset($data['born_place']) && !empty($data['born_place'])) ? '3' : '';
        $returnArr[] = (isset($data['residence_place']) && !empty($data['residence_place'])) ? '4' : '';
        $returnArr[] = (isset($data['grade_or_class']) && !empty($data['grade_or_class'])) ? '5' : '';
        $returnArr[] = (isset($data['modify_by']) && !empty($data['modify_by'])) ? $data['modify_by'] : '';
        $returnArr[] = (isset($data['modify_date']) && !empty($data['modify_date'])) ? $data['modify_date'] : '';
        if ((isset($data['updateandpublish']) && $data['updateandpublish'] == 'Update & Publish') || (isset($data['is_published']) && $data['is_published'] == 1)) {
            $returnArr[] = DATE_TIME_FORMAT;
        } else {
            $returnArr[] = DATE_TIME_FORMAT;
        }
        if (isset($data['avail_member_demo']) && !empty($data['avail_member_demo'])) {
            $avail_member_demo[] = '1';
        }
        if (isset($data['avail_non_member_demo']) && !empty($data['avail_non_member_demo'])) {
            $avail_member_demo[] = '2';
        }
        if (isset($data['avail_anonymous_demo']) && !empty($data['avail_anonymous_demo'])) {
            $avail_member_demo[] = '3';
        }
        if (!empty($avail_member_demo)) {
            $returnArr[] = implode(',', $avail_member_demo);
        } else {
            $returnArr[] = '';
        }
        return $returnArr;
    }

    /**
     * This method is used to add filtering and validation to the form elements of create Quiz
     * @param void
     * @return Array
     * @author Icreon Tech - KK
     */
    public function getInputFilterCreateQuiz() {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'quiz_title',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'TITLE_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_published',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'additional_category',
                        'required' => false,
                    )));
            
            


            $inputFilter->add($factory->createInput(array(
                        'name' => 'quiz_tagline',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'QUIZ_TAGLINE_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'main_category_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'QUIZ_CATEGORY_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'expiration_date',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'QUIZ_EXPIRATION_DATE_MSG',
                                    ),
                                    'inarrayvalidator' => false,
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'expiration_time',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'QUIZ_EXPIRATION_TIME_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));



            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    /**
     * This method is used to add filtering and validation to the form elements of create Quiz
     * @param void
     * @return Array
     * @author Icreon Tech - KK
     */
    public function getInputFilterUpdateQuiz() {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_published',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'PUBLISH_STATUS_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'quiz_title',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'TITLE_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'quiz_tagline',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'QUIZ_TAGLINE_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'main_category_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'QUIZ_CATEGORY_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'expiration_date',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'QUIZ_EXPIRATION_DATE_MSG',
                                    ),
                                    'inarrayvalidator' => false,
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'expiration_time',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'QUIZ_EXPIRATION_TIME_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'additional_category',
                        'required' => false,
                    )));
             

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    /**
     * This method is used to add filtering and validation to the form elements of create Quiz questions
     * @param void
     * @return Array
     * @author Icreon Tech - KK
     */
    public function getInputFilterCreateQuizQuestions() {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'correct_choices[]',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'question_choice[]',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'quiz_question',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'PLEASE_ENTER_QUIZ_QUESTION'
                                    )
                                )
                            )
                        )
                    )));




            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    /**
     * This method is used to call add Quiz Question parameter
     * @param array
     * @return array
     * @author Icreon Tech - KK
     */
    public function getCreateQuizQuestionArr($data) {
        $returnArr = array();
        $returnArr[] = (isset($data['quiz_id']) && !empty($data['quiz_id'])) ? $data['quiz_id'] : '';
        $returnArr[] = (isset($data['question_title']) && !empty($data['question_title'])) ? $data['question_title'] : '';
        $returnArr[] = (isset($data['is_randomized']) && !empty($data['is_randomized'])) ? $data['is_randomized'] : '';
        $returnArr[] = (isset($data['added_by']) && !empty($data['added_by'])) ? $data['added_by'] : '';
        $returnArr[] = (isset($data['added_date']) && !empty($data['added_date'])) ? $data['added_date'] : '';
        $returnArr[] = (isset($data['modified_by']) && !empty($data['modified_by'])) ? $data['modified_by'] : '';
        $returnArr[] = (isset($data['modified_date']) && !empty($data['modified_date'])) ? $data['modified_date'] : '';
        return $returnArr;
    }

    /**
     * This method is used to process the quiz array
     * @param array
     * @return array
     * @author Icreon Tech - AG
     */
    public function processQuizArray($result) {
        $newQuizArray = array();
        if (!empty($result)) {
            foreach ($result as $key => $val) {
                $newQuizArray[$val['quiz_id']]['detail'] = $val;
                $newQuizArray[$val['quiz_id']]['quizQuestion'][$val['question_id']]['question'] = $val['question_title'];
                $newQuizArray[$val['quiz_id']]['quizQuestion'][$val['question_id']]['question_image'] = $val['question_image'];
                $newQuizArray[$val['quiz_id']]['quizQuestion'][$val['question_id']]['correct_option_id'] = $val['correct_option_id'];
                $newQuizArray[$val['quiz_id']]['quizQuestion'][$val['question_id']]['is_randomized'] = isset($val['is_randomized']) ? $val['is_randomized'] : '';
                $newQuizArray[$val['quiz_id']]['quizQuestion'][$val['question_id']]['option'][$val['quiz_option_id']]['option_title'] = $val['option_title'];
            }
        }
        return $newQuizArray;
    }

    /**
     * This method is used to get the additional queries string by array
     * @param array
     * @return array
     * @author Icreon Tech - AG
     */
    public function getquizAdditionalCategoriesString($quizAdditionalCategoryArray) {
        $quizAdditionalCategories = "";
        if (!empty($quizAdditionalCategoryArray)) {
            foreach ($quizAdditionalCategoryArray as $key => $val) {
                $quizAdditionalCategories.=$val['category_name'] . ", ";
            }
        }
        $quizAdditionalCategories = rtrim($quizAdditionalCategories, ', ');
        return $quizAdditionalCategories;
    }

    /**
     * This method is used to get the additional queries array
     * @param array
     * @return array
     * @author Icreon Tech - AG
     */
    public function getquizAdditionalCategoriesArray($quizAdditionalCategoryArray) {
        $quizAdditionalCategories = array();
        if (!empty($quizAdditionalCategoryArray)) {
            foreach ($quizAdditionalCategoryArray as $key => $val) {
                $quizAdditionalCategories[] = $val['quiz_category_id'];
            }
        }
        return $quizAdditionalCategories;
    }

    /**
     * Function used to get array of elements need to call remove quiz question
     * @param Array
     * @return Array
     * @author Icreon Tech - KK
     */
    public function getArrayForRemoveQuizQuestion($dataArr) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['question_id']) && $dataArr['question_id'] != '') ? $dataArr['question_id'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for save quiz search
     * @param array
     * @return array
     * @author Icreon Tech - AG
     */
    public function getInputFilterQuizSearch($filterData) {
        $inputFilterData = array();
        if (is_array($filterData)) {
            foreach ($filterData as $key => $val) {
                $inputFilterData[$key] = trim(addslashes(strip_tags($val)));
            }
        } else {
            $inputFilterData = trim(addslashes(strip_tags($filterData)));
        }
        return $inputFilterData;
    }

    /**
     * This method is used to return the stored procedure array for save quiz search
     * @param array
     * @return array
     * @author Icreon Tech - AG
     */
    public function getAddQuizSearchArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['crm_user_id']) && $dataArr['crm_user_id'] != '') ? $dataArr['crm_user_id'] : '';
        $returnArr[] = (isset($dataArr['title']) && $dataArr['title'] != '') ? $dataArr['title'] : '';
        $returnArr[] = (isset($dataArr['search_query']) && $dataArr['search_query'] != '') ? $dataArr['search_query'] : '';
        $returnArr[] = (isset($dataArr['is_active']) && $dataArr['is_active'] != '') ? $dataArr['is_active'] : '';
        $returnArr[] = (isset($dataArr['is_deleted']) && $dataArr['is_deleted'] != '') ? $dataArr['is_deleted'] : '';
        if (isset($dataArr['added_date'])) {
            $returnArr[] = $dataArr['added_date'];
        } else {
            $returnArr[] = $dataArr['modified_date'];
        }
        if (isset($dataArr['quiz_search_id']) && $dataArr['quiz_search_id'] != '') {
            $returnArr[] = $dataArr['quiz_search_id'];
        }
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for saved search
     * @param Array
     * @return Array
     * @author Icreon Tech - AG
     */
    public function getQuizSavedSearchArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['crm_user_id']) && $dataArr['crm_user_id'] != '') ? $dataArr['crm_user_id'] : '';
        $returnArr[] = (isset($dataArr['quiz_search_id']) && $dataArr['quiz_search_id'] != '') ? $dataArr['quiz_search_id'] : '';
        $returnArr[] = (isset($dataArr['is_active'])) ? $dataArr['is_active'] : '';
        return $returnArr;
    }

    /**
     * This method is used to add filtering and validation to the form elements of create Quiz questions
     * @param void
     * @return Array
     * @author Icreon Tech - KK
     */
    public function getInputFilterEditQuizQuestions() {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'correct_choices_new[]',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'correct_choices[]',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_randomized',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'question_choice[]',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'quiz_question_edit',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'PLEASE_ENTER_QUIZ_QUESTION'
                                    )
                                )
                            )
                        )
                    )));




            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    /**
     * This method is used to return the stored procedure array for delete saved search
     * @param Array
     * @return Array
     * @author Icreon Tech - AG
     */
    public function getDeleteQuizSavedSearchArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['is_deleted']) && $dataArr['is_deleted'] != '') ? $dataArr['is_deleted'] : '';
        $returnArr[] = (isset($dataArr['modified_date']) && $dataArr['modified_date'] != '') ? $dataArr['modified_date'] : '';
        $returnArr[] = (isset($dataArr['quiz_search_id']) && $dataArr['quiz_search_id'] != '') ? $dataArr['quiz_search_id'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for search Quiz in FE
     * @param array
     * @return array
     * @author Icreon Tech - KK

     */
    public function getQuizSearchArrFe($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['quiz_title']) && $dataArr['quiz_title'] != '') ? $dataArr['quiz_title'] : '';
        $returnArr[] = (isset($dataArr['main_category_id']) && $dataArr['main_category_id'] != '') ? $dataArr['main_category_id'] : '';
        $returnArr[] = (isset($dataArr['quiz_expired']) && $dataArr['quiz_expired'] != '') ? $dataArr['quiz_expired'] : '';
        $returnArr[] = (isset($dataArr['quiz_available_to']) && !empty($dataArr['quiz_available_to'])) ? $dataArr['quiz_available_to'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $returnArr[] = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $returnArr[] = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        $returnArr[] = (isset($dataArr['quiz_id']) && $dataArr['quiz_id'] != '') ? $dataArr['quiz_id'] : '';
        $returnArr[] = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '';

        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for search Quiz in FE
     * @param array
     * @return array
     * @author Icreon Tech - KK

     */
    public function getQuizQuestionArrFe($dataArr = array()) {

        $returnArr = array();
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr[] = (isset($dataArr['start_limit']) && $dataArr['start_limit'] != '') ? $dataArr['start_limit'] : '1';
        $returnArr[] = (isset($dataArr['quiz_id']) && $dataArr['quiz_id'] != '') ? $dataArr['quiz_id'] : '';
        $returnArr[] = (isset($dataArr['is_demographic_question']) && $dataArr['is_demographic_question'] != '') ? $dataArr['is_demographic_question'] : '0';
        $returnArr[] = (isset($dataArr['demographic_question']) && $dataArr['demographic_question'] != '') ? $dataArr['demographic_question'] : '';
      

        return $returnArr;
    }
    
    /**
     * This method is used to return the stored procedure array for search Quiz in FE
     * @param array
     * @return array
     * @author Icreon Tech - KK

     */
    
    public function getQuizUserAnswers($dataArr = array()) {

        $returnArr = array();
        $returnArr[] = (isset($dataArr['quiz_id']) && $dataArr['quiz_id'] != '') ? $dataArr['quiz_id'] : '';
        $returnArr[] = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '';
        $returnArr[] = (isset($dataArr['user_quiz_id']) && $dataArr['user_quiz_id'] != '') ? $dataArr['user_quiz_id'] : '';
        $returnArr[] = (isset($dataArr['user_session_id']) && $dataArr['user_session_id'] != '') ? $dataArr['user_session_id'] : '';
        
        return $returnArr;
    }
    
    /**
     * This method is used to return the stored procedure array for search Quiz in FE
     * @param array
     * @return array
     * @author Icreon Tech - KK

     */
    public function getQuizQuestionArrResponse($dataArr = array()) {

        $returnArr = array();
        $returnArr[] = (isset($dataArr['quiz_id']) && $dataArr['quiz_id'] != '') ? $dataArr['quiz_id'] : '';
        $returnArr[] = (isset($dataArr['question_id']) && $dataArr['question_id'] != '') ? $dataArr['question_id'] : '';
        $returnArr[] = (isset($dataArr['user_quiz_id']) && $dataArr['user_quiz_id'] != '') ? $dataArr['user_quiz_id'] : '';
        $returnArr[] = (isset($dataArr['is_correct']) && $dataArr['is_correct'] != '') ? $dataArr['is_correct'] : '';
      

        return $returnArr;
    }
    
    /**
     * This method is used to return the stored procedure array for search Quiz in FE
     * @param array
     * @return array
     * @author Icreon Tech - KK

     */
    
    public function getQuizUserAnswersTable($dataArr = array()) {

        $returnArr = array();
        $returnArr[] = (isset($dataArr['quiz_id']) && $dataArr['quiz_id'] != '') ? $dataArr['quiz_id'] : '';
        $returnArr[] = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '';
        $returnArr[] = (isset($dataArr['user_quiz_id']) && $dataArr['user_quiz_id'] != '') ? $dataArr['user_quiz_id'] : '';
        
        

        return $returnArr;
    }


}