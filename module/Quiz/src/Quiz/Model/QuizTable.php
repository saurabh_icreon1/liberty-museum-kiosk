<?php

/**
 * This model is used for Quiz.
 * @category   Zend
 * @package    Quizze
 * @author     Icreon Tech - AS
 */

namespace Quiz\Model;

use Zend\Db\TableGateway\TableGateway;

class QuizTable {

    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
        $this->connection = $this->tableGateway->getAdapter()->getDriver()->getConnection();
    }

    /**
     * Function for get Quiz data
     * @author Icreon Tech - KK
     * @return Array
     * @param Array
     */
    public function getAllQuiz($quizData) {
        $procquesmarkapp = $this->appendQuestionMars(count($quizData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_qui_getCrmQuizzes(" . $procquesmarkapp . ")");
        //$stmt->prepare("CALL usp_qui_getCrmQuizzes()");
        $stmt->getResource()->bindParam(1, $quizData[0]);
        $stmt->getResource()->bindParam(2, $quizData[1]);
        $stmt->getResource()->bindParam(3, $quizData[2]);
        $stmt->getResource()->bindParam(4, $quizData[3]);
        $stmt->getResource()->bindParam(5, $quizData[4]);
        $stmt->getResource()->bindParam(6, $quizData[5]);
        $stmt->getResource()->bindParam(7, $quizData[6]);
        $stmt->getResource()->bindParam(8, $quizData[7]);
        $stmt->getResource()->bindParam(9, $quizData[8]);
        $stmt->getResource()->bindParam(10, $quizData[9]);
        $stmt->getResource()->bindParam(11, $quizData[10]);
        $stmt->getResource()->bindParam(12, $quizData[11]);
        $stmt->getResource()->bindParam(13, $quizData[12]);
        $stmt->getResource()->bindParam(14, $quizData[13]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for append number of questions marks for stored procedure
     * @author Icreon Tech - KK
     * @return String
     * @param Array
     */
    public function appendQuestionMars($count) {
        $returnstr = '';
        for ($i = 1; $i <= $count; $i++) {
            $returnstr.='?,';
        }
        return rtrim($returnstr, ',');
    }

    /**
     * Function for to get Quiz Xategories
     * @author Icreon Tech - KK
     * @return Array
     * @param Array
     */
    public function getQuizCategories() {

        $result = $this->connection->execute("CALL usp_qui_getQuizCategories()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for remove Quiz
     * @author Icreon Tech - KK
     * @return Void
     * @param Array
     */
    public function removeQuizById($quizData) {
        $procquesmarkapp = $this->appendQuestionMars(count($quizData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_qui_deleteQuiz(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $quizData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * This function will fetch the user quizes
     * @param ship name
     * @return this will return user quizes
     * @author Icreon Tech -DG
     */
    public function getUserQuizes($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getUserQuizes(?)');
            $stmt->getResource()->bindParam(1, $param['user_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for insert Quiz data
     * @author Icreon Tech - KK
     * @return Int
     * @param Array
     */
    public function saveQuiz($quizData) {
        $procquesmarkapp = $this->appendQuestionMars(count($quizData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_qui_inserCrmQuiz(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $quizData[0]);
        $stmt->getResource()->bindParam(2, $quizData[1]);
        $stmt->getResource()->bindParam(3, $quizData[2]);
        $stmt->getResource()->bindParam(4, $quizData[3]);
        $stmt->getResource()->bindParam(5, $quizData[4]);
        $stmt->getResource()->bindParam(6, $quizData[5]);
        $stmt->getResource()->bindParam(7, $quizData[6]);
        $stmt->getResource()->bindParam(8, $quizData[7]);
        $stmt->getResource()->bindParam(9, $quizData[8]);
        $stmt->getResource()->bindParam(10, $quizData[9]);
        $stmt->getResource()->bindParam(11, $quizData[10]);
        $stmt->getResource()->bindParam(12, $quizData[11]);
        $stmt->getResource()->bindParam(13, $quizData[12]);
        $stmt->getResource()->bindParam(14, $quizData[13]);
        $stmt->getResource()->bindParam(15, $quizData[14]);
        $stmt->getResource()->bindParam(16, $quizData[15]);
        $stmt->getResource()->bindParam(17, $quizData[16]);
        $stmt->getResource()->bindParam(18, $quizData[17]);
        $stmt->getResource()->bindParam(19, $quizData[18]);
        $stmt->getResource()->bindParam(20, $quizData[19]);
     

        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for insert Quiz data
     * @author Icreon Tech - KK
     * @return Int
     * @param Array
     */
    public function updateQuiz($quizData) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($quizData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_qui_editCrmQuiz(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $quizData[0]);
            $stmt->getResource()->bindParam(2, $quizData[1]);
            $stmt->getResource()->bindParam(3, $quizData[2]);
            $stmt->getResource()->bindParam(4, $quizData[3]);
            $stmt->getResource()->bindParam(5, $quizData[4]);
            $stmt->getResource()->bindParam(6, $quizData[5]);
            $stmt->getResource()->bindParam(7, $quizData[6]);
            $stmt->getResource()->bindParam(8, $quizData[7]);
            $stmt->getResource()->bindParam(9, $quizData[8]);
            $stmt->getResource()->bindParam(10, $quizData[9]);
            $stmt->getResource()->bindParam(11, $quizData[10]);
            $stmt->getResource()->bindParam(12, $quizData[11]);
            $stmt->getResource()->bindParam(13, $quizData[12]);
            $stmt->getResource()->bindParam(14, $quizData[13]);
            $stmt->getResource()->bindParam(15, $quizData[14]);
            $stmt->getResource()->bindParam(16, $quizData[15]);
            $stmt->getResource()->bindParam(17, $quizData[16]);
            $stmt->getResource()->bindParam(18, $quizData[17]);
            $stmt->getResource()->bindParam(19, $quizData[18]);
            $stmt->getResource()->bindParam(20, $quizData[19]);
            $stmt->getResource()->bindParam(21, $quizData[20]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to insert Quiz Additional Categories
     * @author Icreon Tech - KK
     * @return Boolean
     * @param Array
     */
    public function insertAdditionalCategories($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_qui_insertQuizAdditionalCategories(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['quiz_id']);
            $stmt->getResource()->bindParam(2, $params['additional_category_id']);
            $stmt->getResource()->bindParam(3, $params['added_by']);
            $stmt->getResource()->bindParam(4, $params['added_date']);
            $stmt->getResource()->bindParam(5, $params['modify_by']);
            $stmt->getResource()->bindParam(6, $params['modify_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to update Quiz Additional Categories
     * @author Icreon Tech - KK
     * @return Boolean
     * @param Array
     */
    public function updateAdditionalCategories($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_qui_updateQuizAdditionalCategories(?)');
            $stmt->getResource()->bindParam(1, $params['quiz_id']);
            //$stmt->getResource()->bindParam(2, $params['modify_by']);
            // $stmt->getResource()->bindParam(3, $params['modify_date']);
            //$stmt->getResource()->bindParam(4, $params['additional_category_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for insert Quiz data
     * @author Icreon Tech - KK
     * @return Int
     * @param Array
     */
    public function saveQuizQuestions($quizData) {
        $procquesmarkapp = $this->appendQuestionMars(count($quizData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_qui_inserCrmQuizQuestion(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $quizData[0]);
        $stmt->getResource()->bindParam(2, $quizData[1]);
        $stmt->getResource()->bindParam(3, $quizData[2]);
        $stmt->getResource()->bindParam(4, $quizData[3]);
        $stmt->getResource()->bindParam(5, $quizData[4]);
        $stmt->getResource()->bindParam(6, $quizData[5]);
        $stmt->getResource()->bindParam(7, $quizData[6]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for to insert Quiz Questions Options
     * @author Icreon Tech - KK
     * @return Boolean
     * @param Array
     */
    public function insertQuizOptions($params = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_qui_insertQuizQuestionOptions(?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $params['quiz_question_id']);
        $stmt->getResource()->bindParam(2, $params['option_title']);
        $stmt->getResource()->bindParam(3, $params['added_by']);
        $stmt->getResource()->bindParam(4, $params['added_date']);
        $stmt->getResource()->bindParam(5, $params['modified_by']);
        $stmt->getResource()->bindParam(6, $params['modified_date']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for to insert Quiz Questions Options
     * @author Icreon Tech - KK
     * @return Boolean
     * @param Array
     */
    public function updateQuizCorrectOptions($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_qui_updateQuizCorrectOption(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['quiz_question_id']);
            $stmt->getResource()->bindParam(2, $params['option_id']);
            $stmt->getResource()->bindParam(3, $params['modified_by']);
            $stmt->getResource()->bindParam(4, $params['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for get Quiz data on the base of id
     * @author Icreon Tech - AG
     * @return Int
     * @param Array
     */
    public function getQuizById($quizData) {
        //$procquesmarkapp = $this->appendQuestionMars(count($quizData));
        $stmt = $this->dbAdapter->createStatement();
        if (!isset($quizData[1])) {
            $quizData[1] = '';
        }


        $stmt->prepare("CALL usp_qui_getCrmQuizInfo(?,?)");
        $stmt->getResource()->bindParam(1, $quizData[0]);
        $stmt->getResource()->bindParam(2, $quizData[1]);

        $result = $stmt->execute();

        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get Quiz's additional categories data on the base of quiz id
     * @author Icreon Tech - AG
     * @return Int
     * @param Array
     */
    public function getquizAdditionalCategoriesById($quizData) {
        // $procquesmarkapp = $this->appendQuestionMars(count($quizData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_qui_getCrmQuizAddtnlCategoryInfo(?)");
        $stmt->getResource()->bindParam(1, $quizData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for remove Quiz Question
     * @author Icreon Tech - KK
     * @return Void
     * @param Array
     */
    public function removeQuizQuestionById($quizData) {
        $procquesmarkapp = $this->appendQuestionMars(count($quizData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_qui_deleteQuizQuestion(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $quizData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * This function will save the search title into the table for quiz search
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech - AG
     */
    public function saveQuizSearch($pledgeData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($pledgeData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_qui_insertCrmSearchQuiz(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $pledgeData[0]);
            $stmt->getResource()->bindParam(2, $pledgeData[1]);
            $stmt->getResource()->bindParam(3, $pledgeData[2]);
            $stmt->getResource()->bindParam(4, $pledgeData[3]);
            $stmt->getResource()->bindParam(5, $pledgeData[4]);
            $stmt->getResource()->bindParam(6, $pledgeData[5]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the saved search listing for quiz search
     * @param this will be an array.
     * @return this will return a title of the saved search
     * @author Icreon Tech -AG
     */
    public function getQuizSavedSearch($quizData = array()) {
        $procquesmarkapp = $this->appendQuestionMars(count($quizData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_qui_getCrmSearchQuiz(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $quizData[0]);
        $stmt->getResource()->bindParam(2, $quizData[1]);
        $stmt->getResource()->bindParam(3, $quizData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
    }

    /**
     * This function will update the search title into the table for quiz search
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech - AG
     */
    public function updateQuizSearch($quizData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($quizData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_qui_updateCrmSearchQuiz(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $quizData[0]);
            $stmt->getResource()->bindParam(2, $quizData[1]);
            $stmt->getResource()->bindParam(3, $quizData[2]);
            $stmt->getResource()->bindParam(4, $quizData[3]);
            $stmt->getResource()->bindParam(5, $quizData[4]);
            $stmt->getResource()->bindParam(6, $quizData[5]);
            $stmt->getResource()->bindParam(7, $quizData[6]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for remove Quiz Question Option
     * @author Icreon Tech - KK
     * @return Void
     * @param Array
     */
    public function deletQuestionOption($quizData) {

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_qui_deleteQuizQuestionOption(?)");
        $stmt->getResource()->bindParam(1, $quizData['option_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for Update Quiz Question Option
     * @author Icreon Tech - KK
     * @return Void
     * @param Array
     */
    public function updateQuestionOptions($quizData) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_qui_updateQuizQuestionOption(?,?,?,?)");
        $stmt->getResource()->bindParam(1, $quizData['option_id']);
        $stmt->getResource()->bindParam(2, $quizData['option_title']);
        $stmt->getResource()->bindParam(3, $quizData['modified_by']);
        $stmt->getResource()->bindParam(4, $quizData['modified_date']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for Update Quiz Question Option
     * @author Icreon Tech - KK
     * @return Void
     * @param Array
     */
    public function updateCrmQuizQuestion($quizData) {

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_qui_editCrmQuizQuestion(?,?,?,?,?)");
        $stmt->getResource()->bindParam(1, $quizData['question_id']);
        $stmt->getResource()->bindParam(2, $quizData['question_title']);
        $stmt->getResource()->bindParam(3, $quizData['is_randomized']);
        $stmt->getResource()->bindParam(4, $quizData['modified_by']);
        $stmt->getResource()->bindParam(5, $quizData['modified_date']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * This function will delete the saved quiz search titles
     * @param this will be an array having the params.
     * @return this will return a confirmation
     * @author Icreon Tech - AG
     */
    public function deleteSavedSearch($quizData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($quizData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_ple_deleteCrmSearchQuiz(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $quizData[0]);
            $stmt->getResource()->bindParam(2, $quizData[1]);
            $stmt->getResource()->bindParam(3, $quizData[2]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for get Quiz data
     * @author Icreon Tech - KK
     * @return Array
     * @param Array
     */
    public function getAllQuizFrontEnd($quizData) {
        $procquesmarkapp = $this->appendQuestionMars(count($quizData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_qui_getQuizzes(" . $procquesmarkapp . ")");
        //$stmt->prepare("CALL usp_qui_getCrmQuizzes()");
        $stmt->getResource()->bindParam(1, $quizData[0]);
        $stmt->getResource()->bindParam(2, $quizData[1]);
        $stmt->getResource()->bindParam(3, $quizData[2]);
        $stmt->getResource()->bindParam(4, $quizData[3]);
        $stmt->getResource()->bindParam(5, $quizData[4]);
        $stmt->getResource()->bindParam(6, $quizData[5]);
        $stmt->getResource()->bindParam(7, $quizData[6]);
        $stmt->getResource()->bindParam(8, $quizData[7]);
        $stmt->getResource()->bindParam(9, $quizData[8]);
        $stmt->getResource()->bindParam(10, $quizData[9]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get Quiz data on the base of id
     * @author Icreon Tech - AG
     * @return Int
     * @param Array
     */
    public function getQuizInfoById($quizData = array()) {
        // $procquesmarkapp = $this->appendQuestionMars(count($quizData));
        $stmt = $this->dbAdapter->createStatement();

        $stmt->prepare("CALL usp_qui_getQuizInfo(?,?)");
        $stmt->getResource()->bindParam(1, $quizData[0]);
        if (isset($quizData[1]) && !empty($quizData[1])) {
            $stmt->getResource()->bindParam(2, $quizData[1]);
        } else {
            $paramSecond = "";
            $stmt->getResource()->bindParam(2, $paramSecond);
        }
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get Quiz Question and option data on the base of id
     * @author Icreon Tech - KK
     * @return Array
     * @param Array
     */
    public function getQuizQuestionInfoById($quizData) {
        //$procquesmarkapp = $this->appendQuestionMars(count($quizData));
        $stmt = $this->dbAdapter->createStatement();

        $stmt->prepare("CALL usp_qui_getQuizQuestions(?,?,?,?,?)");
        $stmt->getResource()->bindParam(1, $quizData[0]);
        $stmt->getResource()->bindParam(2, $quizData[1]);
        $stmt->getResource()->bindParam(3, $quizData[2]);
        $stmt->getResource()->bindParam(4, $quizData[3]);
        $stmt->getResource()->bindParam(5, $quizData[4]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        //asd($resultSet);
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for to insert Quiz played by
     * @author Icreon Tech - KK
     * @return Int
     * @param Array
     */
    public function insertUserQuiz($params = array()) {

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_qui_insertUserQuiz(?,?,?,?)');
        $stmt->getResource()->bindParam(1, $params['quiz_id']);
        $stmt->getResource()->bindParam(2, $params['user_id']);
        $stmt->getResource()->bindParam(3, $params['play_date']);
        $stmt->getResource()->bindParam(4, $params['user_session_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for to insert Quiz played by
     * @author Icreon Tech - KK
     * @return Int
     * @param Array
     */
    public function insertUserQuizOptions($params = array()) {

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_qui_insertUserQuizOption(?,?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $params['user_quiz_id']);
        $stmt->getResource()->bindParam(2, $params['quiz_id']);
        $stmt->getResource()->bindParam(3, $params['quiz_question_id']);
        $stmt->getResource()->bindParam(4, $params['quiz_option_id']);
        $stmt->getResource()->bindParam(5, $params['is_correct']);
        $stmt->getResource()->bindParam(6, $params['play_date']);
        $stmt->getResource()->bindParam(7, $params['is_demographic']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for update quiz correct count  on the basis of quiz user id
     * @author Icreon Tech - KK
     * @return Int
     * @param Array
     */
    public function updateQuizCorrectCount($quizUserId) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_qui_updateQuizCorrectCount(?)");
        $stmt->getResource()->bindParam(1, $quizUserId);
        $result = $stmt->execute();
        return true;
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get Quiz data
     * @author Icreon Tech - KK
     * @return Array
     * @param Array
     */
    public function getAllQuizAnswers($quizData) {

        $procquesmarkapp = $this->appendQuestionMars(count($quizData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_qui_getQuizzesUserAnswers(?,?,?,?)");
        //$stmt->prepare("CALL usp_qui_getCrmQuizzes()");
        $stmt->getResource()->bindParam(1, $quizData[0]);
        $stmt->getResource()->bindParam(2, $quizData[1]);
        $stmt->getResource()->bindParam(3, $quizData[2]);
        $stmt->getResource()->bindParam(4, $quizData[3]);

        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get Quiz Question and option data on the base of id
     * @author Icreon Tech - KK
     * @return Array
     * @param Array
     */
    public function getQuizUserResponseInfo($quizData) {
        //$procquesmarkapp = $this->appendQuestionMars(count($quizData));
        $stmt = $this->dbAdapter->createStatement();

        $stmt->prepare("CALL usp_qui_getQuizzesUserResponse(?,?,?,?)");
        $stmt->getResource()->bindParam(1, $quizData[0]);
        $stmt->getResource()->bindParam(2, $quizData[1]);
        $stmt->getResource()->bindParam(3, $quizData[2]);
        $stmt->getResource()->bindParam(4, $quizData[3]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get Quiz data
     * @author Icreon Tech - KK
     * @return Array
     * @param Array
     */
    public function getAllQuizAnswersGraph($quizData) {

        $procquesmarkapp = $this->appendQuestionMars(count($quizData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_qui_getQuizzesUserAnswersGraph(?,?,?,?)");
        //$stmt->prepare("CALL usp_qui_getCrmQuizzes()");
        $stmt->getResource()->bindParam(1, $quizData[0]);
        $stmt->getResource()->bindParam(2, $quizData[1]);
        $stmt->getResource()->bindParam(3, $quizData[2]);
        $stmt->getResource()->bindParam(4, $quizData[3]);

        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get Quiz data
     * @author Icreon Tech - KK
     * @return Array
     * @param Array
     */
    public function getAllQuizAnswersQuestionsByQuestions($quizData) {

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_qui_getQuizzesUserAnswersTable(?,?,?)");
        //$stmt->prepare("CALL usp_qui_getCrmQuizzes()");
        $stmt->getResource()->bindParam(1, $quizData[0]);
        $stmt->getResource()->bindParam(2, $quizData[1]);
        $stmt->getResource()->bindParam(3, $quizData[2]);


        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get Quiz data
     * @author Icreon Tech - KK
     * @return Array
     * @param Array
     */
    public function getQuizDemographicStat($quizId) {

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_qui_getUserDemographicStat(?)");
        $stmt->getResource()->bindParam(1, $quizId);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get Quiz data
     * @author Icreon Tech - KK
     * @return Array
     * @param Array
     */
    public function getQuizDemographicAttempt($userQuizId) {

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_qui_getUserDemographicAttempt(?)");
        $stmt->getResource()->bindParam(1, $userQuizId);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

}