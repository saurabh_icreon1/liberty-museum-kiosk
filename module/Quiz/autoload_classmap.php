<?php 
return array(
    'Quiz\Module'                       => __DIR__ . '/Module.php',
    'Quiz\Controller\QuizController'   => __DIR__ . '/src/Quiz/Controller/QuizController.php',
    'Quiz\Model\Quiz'            => __DIR__ . '/src/Quiz/Model/Quiz.php',
    'Quiz\Model\QuizTable'                   => __DIR__ . '/src/Quiz/Model/QuizTable.php'
);
?>