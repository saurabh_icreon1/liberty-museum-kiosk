<?php 
return array(
    'Blog\Module'                       => __DIR__ . '/Module.php',
    'Blog\Controller\BlogController'   => __DIR__ . '/src/Blog/Controller/BlogController.php',
    'Blog\Model\Blog'            => __DIR__ . '/src/Blog/Model/Blog.php',
    'Blog\Model\BlogTable'                   => __DIR__ . '/src/Blog/Model/BlogTable.php'
);
?>