<?php

    /**
     * This form is used for Add a Blog.
     * @package    Blog
     * @author     Icreon Tech - AS
     */
 
namespace Blog\Form;

use Zend\Form\Form;

class SearchFrontBlogsForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('search_front_blog');
        $this->setAttribute('method', 'post');
        
        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type' => 'text',
                'id' => 'blog_title',
                'value' => '' ,
                'class' => 'button',
				'placeholder' => 'Search...'
            )
        ));
        
        $this->add(array(
            'name' => 'search',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'search',
                'value' => '',
                'class' => 'blog-arrow button'
            )
        ));
       
    }
}
