<?php

    /**
     * This form is used for Add a Blog.
     * @package    Blog_AddBlogForm
     * @author     Icreon Tech - AG
     */
 
namespace Blog\Form;

use Zend\Form\Form;

class AddBlogForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('create-Blog');
        $this->setAttribute('method', 'post');
	 
        
        $this->add(array(
            'name' => 'blog_title',
            'attributes' => array(
                'type' => 'text',
                'id' => 'blog_title'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'category_id',	
            'options' => array(
                'value_options'=> array(
               ),
            ),
            'attributes' => array(
                'id' => 'category_id',
                'class'=>'e1 select-w-320'
           )
        ));
        $this->add(array(
            'name' => 'main_image',
            'attributes' => array(
                'type' => 'file',
                'value' => 'upload',
                'id' => 'main_image',
                'class' => 'attachmentfiles'
            ),
        ));

        $this->add(array(
            'name' => 'main_image_hidden',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'main_image_hidden'
            )
        ));
       $this->add(array(
            'name' => 'blog_content',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'blog_content',
                'class' => 'myTextEditor width-90'
            )
        ));
        
                    
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_featured',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'class'=>'checkbox',
                'id' => 'is_featured',
                'class' =>'checkbox'
            )
            
        ));
        
        $this->add(array(
            'name' => 'tag',
            'attributes' => array(
                'type' => 'text',
                'id' => 'tag'
            )
        ));  
        $this->add(array(
            'name' => 'tag_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'tag_id'
            )
        )); 
        $this->add(array(
            'name' => 'blog_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'blog_id'
            )
        )); 
        $this->add(array(
            'name' => 'publish_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'publish_date',
                'class' => 'width-128 cal-icon'
            )
        ));        
        $this->add(array(
            'name' => 'publish_time',
            'attributes' => array(
                'type' => 'text',
                'id' => 'publish_time',
                'class' => 'width-128 time-icon',
                'onkeyUp'=>'disableType(event)'
            )
        ));
        $this->add(array(
            'name' => 'author_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'author_name',
                'value' => 'The Statue of Liberty-Ellis Island Foundation',
                'onfocus' => "this.value=''",
            )
        ));
        
        $this->add(array(
            'name' => 'save',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'save',
                'value' => 'SAVE',
                'class' => 'save-btn'
            )
        ));
        $this->add(array(
            'name' => 'saveandpublish',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'saveandpublish',
                'value' => 'SAVE AND PUBLISH',
                'class' => 'save-btn m-l-20'
            )
        ));
                
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'id' => 'cancel',
                'value' => 'CANCEL',
                'onclick' => 'window.location.href="/crm-blogs"',
                'class' => 'cancel-btn m-l-20'
            )
       ));
    }
}
