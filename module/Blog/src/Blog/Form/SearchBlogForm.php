<?php

    /**
     * This form is used for Add a Blog.
     * @package    Blog
     * @author     Icreon Tech - AS
     */
 
namespace Blog\Form;

use Zend\Form\Form;

class SearchBlogForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('search_blog');
        $this->setAttribute('method', 'post');
	 
        
        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type' => 'text',
                'id' => 'blog_title',
                'class' => 'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'author_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'author_name'
            )
        ));
        $this->add(array(
            'type' => 'select',
            'name' => 'category',	
            'options' => array(
                'value_options'=> array(
               ),
            ),
            'attributes' => array(
                'id' => 'category',
                'class'=>'e1 select-w-320'
           )
        ));
        
               
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_featured',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'class'=>'checkbox',
                'id' => 'is_featured',
                'class' =>'e2'
            )
            
        ));
        
        $this->add(array(
            'name' => 'tag',
            'attributes' => array(
                'type' => 'text',
                'id' => 'tag'
            )
        ));
        $this->add(array(
            'name' => 'tag_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'tag_id'
            )
        ));
        $this->add(array(
            'type' => 'select',
            'name' => 'publish_date',	
            'options' => array(
                'value_options'=> array(
               ),
            ),
            'attributes' => array(
                'id' => 'publish_date',
                'class'=>'e1 select-w-320'
           )
        ));
        $this->add(array(
            'type' => 'select',
            'name' => 'status',	
            'options' => array(
                'value_options'=> array(
                    '' => 'Select',
                    '1' => 'Publish',
                    '0' => 'Unpublish'
               ),
            ),
            'attributes' => array(
                'id' => 'status',
                'class'=>'e1 select-w-320'
           )
        ));
        
        $this->add(array(
            'type' => 'Select',
            'name' => 'saved_search',
            'options' => array(
                'value_options' => array(

                ),
            ),
            'attributes' => array(
                'value' => '',
                'class' => 'e1 select-w-320',
                'id' => 'saved_search',
                'onChange' =>'getSavedBlogSearchResult();'
            ),
        ));
        
        $this->add(array(
            'name' => 'date_from',
            'attributes' => array(
                'type' => 'text',
                'id' => 'date_from',
                'class' => 'width-104 cal-icon'
            )
        ));
        $this->add(array(
            'name' => 'date_to',
            'attributes' => array(
                'type' => 'text',
                'id' => 'date_to',
                'class' => 'width-104 cal-icon'
            )
        ));
        
        $this->add(array(
            'name' => 'search',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'search',
                'value' => 'search',
                'class' => 'search-btn'
            )
        ));
       
    }
}
