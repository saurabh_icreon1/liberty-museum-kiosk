<?php

/**
 * This controller is used for Blog pages
 * @category   Zend
 * @package    Blog_BlogController
 * @version    2.2
 * @author     Icreon Tech - AG
 */

namespace Blog\Controller;

use Base\Model\UploadHandler;
use Base\Controller\BaseController;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Zend\View\Model\ViewModel;
use Blog\Model\Blog;
use Blog\Form\AddBlogForm;
use Blog\Form\EditBlogForm;
use Blog\Form\SearchBlogForm;
use Blog\Form\SaveSearchBlogForm;
use Blog\Form\SearchFrontBlogsForm;
use Zend\Http\Request;
use Zend\Authentication\AuthenticationService;
use stdClass;
use Cms\Controller\CmsController;

class BlogController extends BaseController {

    protected $_blogTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_flashMessage = null;

    function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        parent::__construct();
    }

    /**
     * This function is used to get table and config file object
     * @return     array
     * @param void
     * @author Icreon Tech - AS
     */
    public function getBlogTable() {
        if (!$this->_blogTable) {
            $sm = $this->getServiceLocator();
            $this->_blogTable = $sm->get('Blog\Model\BlogTable');
            $this->_translator = $sm->get('translator');
            $this->_config = $sm->get('Config');
            $this->_adapter = $sm->get('dbAdapter');
            $this->_auth = new AuthenticationService();
        }
        return $this->_blogTable;
    }

    /**
     * This action is used to listing of Blog pages
     * @param void
     * @return array
     * @author Icreon Tech - AS
     */
    public function getCrmBlogsAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getBlogTable();
        $searchBlogForm = new searchBlogForm();
        $saveSearchBlogForm = new SaveSearchBlogForm();
        $blogCreateMessages = array_merge($this->_config['Blog_messages']['config']['BLOG_CREATE_MESSAGE'], $this->_config['Blog_messages']['config']['SEARCH_MESSAGES']);
        $blog = new Blog($this->_adapter);
        $this->layout('crm');
        $getTransactionRange = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTransactionRange();
        $transactionRange = array("" => "Select One");
        foreach ($getTransactionRange as $key => $val) {
            $transactionRange[$val['range_id']] = $val['range'];
        }
        $searchBlogForm->get('publish_date')->setAttribute('options', $transactionRange);
        $getBlogCategories = $this->getServiceLocator()->get('Common\Model\CommonTable')->getBlogCategories();
        $blogCategoriesArray = array();
        $blogCategoriesDescription = array();
        $blogCategoriesArray[] = "Select";
        if ($getBlogCategories != false) {
            foreach ($getBlogCategories as $key => $val) {
                $blogCategoriesArray[$val['category_id']] = $val['category_name'];
                $blogCategoriesDescription[$val['category_id']] = $val['category_description'];
            }
        }
        $searchBlogForm->get('category')->setAttribute('options', $blogCategoriesArray);

        $getSearchArray = array();
        $getSearchArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
        $getSearchArray['is_active'] = '1';
        $blogSavedSearchArray = $blog->getBlogSavedSearchArr($getSearchArray);
        $blogSearchArray = $this->_blogTable->getBlogSavedSearch($blogSavedSearchArray);
        $blogSearchList = array();
        $blogSearchList[''] = 'Select';
        foreach ($blogSearchArray as $key => $val) {
            $blogSearchList[$val['blog_search_id']] = stripslashes($val['title']);
        }
        $searchBlogForm->get('saved_search')->setAttribute('options', $blogSearchList);
        $blogArray = $this->getBlogTable()->getBlogTags();
        $tags = '';
        foreach ($blogArray as $key) {
            $tags = $tags . ",\"" . $key['tag'] . "\"";
        }
        $tags = substr($tags, 2);
        $tags = substr($tags, 0, -1);
        $messages = '';
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'tags' => $tags,
            'searchBlogForm' => $searchBlogForm,
            'save_search_blog_form' => $saveSearchBlogForm,
            'jsLangTranslate' => $blogCreateMessages,
            'messages' => $messages,
            'blogCategoriesDescription' => $blogCategoriesDescription
        ));
        return $viewModel;
    }

    public function addCrmBlogAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getBlogTable();
        $blogCreateMessages = $this->_config['Blog_messages']['config']['BLOG_CREATE_MESSAGE'];
        $blog = new Blog($this->_adapter);
        $addBlogForm = new AddBlogForm();
        $getBlogCategories = $this->getServiceLocator()->get('Common\Model\CommonTable')->getBlogCategories();
        $blogCategoriesArray = array();
        $blogCategoriesDescription = array();
        $blogCategoriesArray[''] = 'Select';
        if ($getBlogCategories !== false) {
            foreach ($getBlogCategories as $key => $val) {
                $blogCategoriesArray[$val['category_id']] = $val['category_name'];
                $blogCategoriesDescription[$val['category_id']] = $val['category_description'];
            }
        }
        $addBlogForm->get('category_id')->setAttribute('options', $blogCategoriesArray);
        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $addBlogForm->setInputFilter($blog->getInputFilterCreateBlog());
            $addBlogForm->setData($request->getPost());
            if (!$addBlogForm->isValid()) {
                $errors = $addBlogForm->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row)) {
                        foreach ($row as $type_error => $rower) {
                            if ($type_error != 'notInArray') {
                                $msg [$key] = $blogCreateMessages[$rower];
                            }
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages) && count($messages) > 0) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $addBlogFormArr = $addBlogForm->getData();
                $addBlogFormArr['is_deleted'] = '0';
                $addBlogFormArr['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                $addBlogFormArr['added_date'] = DATE_TIME_FORMAT;
                $addBlogFormArr['modify_by'] = $this->_auth->getIdentity()->crm_user_id;
                $addBlogFormArr['modify_date'] = DATE_TIME_FORMAT;
                $fileName = $addBlogFormArr['main_image_hidden'];
                $this->moveFileFromTempToBlog($fileName);
                $createBlogArr = $blog->getCreateBlogArr($addBlogFormArr);
                $blogId = $this->_blogTable->saveBlog($createBlogArr);
                if ($blogId) {
                    if (isset($addBlogFormArr['tag_id']) && !empty($addBlogFormArr['tag_id'])) {
                        $tagsArray = explode(',', $addBlogFormArr['tag_id']);
                        foreach ($tagsArray as $key => $val) {
                            if ($val != '') {
                                $tagArray = array('blogId' => $blogId, 'tagContent' => $val);
                                $tagArray['is_deleted'] = '0';
                                $tagArray['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                                $tagArray['added_date'] = DATE_TIME_FORMAT;
                                $tagArray['modify_by'] = $this->_auth->getIdentity()->crm_user_id;
                                $tagArray['modify_date'] = DATE_TIME_FORMAT;
                                $blogTagArr = $blog->getCreateBlogTagsArr($tagArray);
                                $this->_blogTable->saveBlogTags($blogTagArr);
                            }
                        }
                    }

                    $changeLogArray = array();
                    $changeLogArray['table_name'] = 'tbl_blg_change_logs';
                    $changeLogArray['activity'] = '1';
                    $changeLogArray['id'] = $blogId;
                    $changeLogArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                    $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                    $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                    $this->flashMessenger()->addMessage($blogCreateMessages['BLOG_CREATE_SUCCESS']);
                    $messages = array('status' => "success", 'message' => $blogCreateMessages['BLOG_CREATE_SUCCESS']);
                } else {
                    $this->flashMessenger()->addMessage($blogCreateMessages['BLOG_CREATE_ERROR']);
                    $messages = array('status' => "success", 'message' => $blogCreateMessages['BLOG_CREATE_ERROR']);
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $blogArray = $this->getBlogTable()->getBlogTags();
        $tags = '';
        foreach ($blogArray as $key) {
            $tags = $tags . ",\"" . $key['tag'] . "\"";
        }
        $tags = substr($tags, 2);
        $tags = substr($tags, 0, -1);
        $this->layout('crm');
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'create_blog_form' => $addBlogForm,
            'tags' => $tags,
            'jsLangTranslate' => $blogCreateMessages,
            'blogCategoriesDescription' => $blogCategoriesDescription
        ));
        return $viewModel;
    }

    public function editCrmBlogAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        $this->getBlogTable();
        $blogCreateMessages = $this->_config['Blog_messages']['config']['BLOG_CREATE_MESSAGE'];
        $blog = new Blog($this->_adapter);
        $editBlogForm = new EditBlogForm();
        $getBlogCategories = $this->getServiceLocator()->get('Common\Model\CommonTable')->getBlogCategories();
        $blogCategoriesArray = array();
        $blogCategoriesDescription = array();
        if ($getBlogCategories !== false) {
            foreach ($getBlogCategories as $key => $val) {
                $blogCategoriesArray[$val['category_id']] = $val['category_name'];
                $blogCategoriesDescription[$val['category_id']] = $val['category_description'];
            }
        }
        $editBlogForm->get('category_id')->setAttribute('options', $blogCategoriesArray);
        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $editBlogForm->setInputFilter($blog->getInputFilterCreateBlog());
            $editBlogForm->setData($request->getPost());
            if (!$editBlogForm->isValid()) {
                $errors = $editBlogForm->getMessages();
                foreach ($errors as $key => $row) {
                    if (!empty($row)) {
                        foreach ($row as $type_error => $rower) {
                            if ($type_error != 'notInArray') {
                                $msg [$key] = $blogCreateMessages[$rower];
                            }
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages) && count($messages) > 0) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $editBlogFormArr = $editBlogForm->getData();
                $blogId = $editBlogFormArr['blog_id'];
                $editBlogFormArr['is_deleted'] = '0';
                $editBlogFormArr['modify_by'] = $this->_auth->getIdentity()->crm_user_id;
                $editBlogFormArr['modify_date'] = DATE_TIME_FORMAT;
                $editBlogArr = $blog->getEditBlogArr($editBlogFormArr);
                if (isset($editBlogFormArr['new_image_hidden']) && $editBlogFormArr['new_image_hidden'] != '') {
                    $fileName = $editBlogFormArr['new_image_hidden'];
                    $this->moveFileFromTempToBlog($fileName);
                    //unlink($this->_config['assets_upload_dir'].trim($editBlogFormArr['main_image_hidden']));
                }
                $editBlogArr[5] = $this->InputDateFormat($editBlogArr[5], 'dateformatampm');
                $this->_blogTable->updateBlog($editBlogArr);
                if (isset($editBlogFormArr['tag']) && !empty($editBlogFormArr['tag'])) {
                    $this->_blogTable->deleteBlogTags($blogId);
                    $tagsArray = explode(',', $editBlogFormArr['tag']);
                    foreach ($tagsArray as $key => $val) {
                        if ($val != '') {
                            $tagArray = array('blogId' => $blogId, 'tagContent' => $val);
                            $tagArray['is_deleted'] = '0';
                            $tagArray['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                            $tagArray['added_date'] = DATE_TIME_FORMAT;
                            $tagArray['modify_by'] = $this->_auth->getIdentity()->crm_user_id;
                            $tagArray['modify_date'] = DATE_TIME_FORMAT;
                            $blogTagArr = $blog->getCreateBlogTagsArr($tagArray);
                            $this->_blogTable->saveBlogTags($blogTagArr);
                        }
                    }
                }
                $changeLogArray = array();
                $changeLogArray['table_name'] = 'tbl_blg_change_logs';
                $changeLogArray['activity'] = '2';
                $changeLogArray['id'] = $blogId;
                $changeLogArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                if ((isset($editBlogFormArr['updateandpublish']) && $editBlogFormArr['updateandpublish'] == 'UPDATEANDPUBLISH')) {
                    $this->flashMessenger()->addMessage($blogCreateMessages['BLOG_PUBLISHED_SUCCESS']);
                    $messages = array('status' => "success", 'message' => $blogCreateMessages['BLOG_PUBLISHED_SUCCESS']);
                } else {
                    $this->flashMessenger()->addMessage($blogCreateMessages['BLOG_UPDATE_SUCCESS']);
                    $messages = array('status' => "success", 'message' => $blogCreateMessages['BLOG_UPDATE_SUCCESS']);
                }


                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        } else if (isset($params['id']) && $params['id'] != '') {
            $blogId = $this->decrypt($params['id']);
            $mode = (isset($params['mode']) && $params['mode'] != '') ? $params['mode'] : $this->encrypt("edit");
            $blogData = $this->_blogTable->getBlogById(array($blogId));
            $blog_image_path = '';
            if ($blogData['blog_image'] != '') {
                $upload_file_path = $this->_config['file_upload_path'];
                $blog_image_path = file_exists($upload_file_path['assets_upload_dir'] . "blog/medium/" . $blogData['blog_image']) ? $upload_file_path['assets_url'] . "blog/medium/" . $blogData['blog_image'] : '';
            }
            $blogTagsData = $this->_blogTable->getBlogTagsByBlogId(array($blogId));
            $blogTags = $blog->getCreateBlogTagsStringArr($blogTagsData);
            if (isset($blogTags['tags']) && !empty($blogTags['tags'])) {
                $tags = $blogTags['tags'];
            } else {
                $tags = "";
            }
            $editBlogForm->get('blog_id')->setValue($blogId);
            $editBlogForm->get('blog_title')->setValue($blogData['blog_title']);
            $editBlogForm->get('category_id')->setValue($blogData['category_id']);
            $editBlogForm->get('blog_content')->setValue($blogData['blog_content']);
            $editBlogForm->get('is_featured')->setValue($blogData['is_featured']);
            $editBlogForm->get('status')->setValue($blogData['is_published']);
            $editBlogForm->get('tag')->setValue($tags);
            $dateArr = $this->OutputDateFormat($blogData['publish_date'], 'dateformatampm');
            $dateArr = explode(' ', $dateArr);
            $editBlogForm->get('publish_date')->setValue($dateArr[0]);
            $editBlogForm->get('publish_time')->setValue($dateArr[1] . " " . $dateArr[2]);
            $editBlogForm->get('author_name')->setValue($blogData['author_name']);
            $editBlogForm->get('main_image')->setValue($blogData['blog_image']);
            $editBlogForm->get('main_image_hidden')->setValue($blogData['blog_image']);
            $blogArray = $this->getBlogTable()->getBlogTags();
            $tags = '';
            foreach ($blogArray as $key) {
                $tags = $tags . ",\"" . $key['tag'] . "\"";
            }
            $tags = substr($tags, 2);
            $tags = substr($tags, 0, -1);
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'edit_blog_form' => $editBlogForm,
                'jsLangTranslate' => $blogCreateMessages,
                'blog_id' => $blogId,
                'tags' => $tags,
                'blog_image' => $blogData['blog_image'],
                'encrypt_blog_id' => $params['id'],
                'blog_image_path' => $blog_image_path,
                'blogData' => $blogData,
                'blogCategoriesDescription' => $blogCategoriesDescription
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to show all blogs detail
     * @param void
     * @return array
     * @author Icreon Tech - AG
     */
    public function getCrmBlogInfoAction() {
        $this->checkUserAuthentication();
        $params = $this->params()->fromRoute();
        $this->getBlogTable();
        $blog = new Blog($this->_adapter);
        $blogViewMessages = $this->_config['Blog_messages']['config']['BLOG_CREATE_MESSAGE'];
        if (isset($params['id']) && $params['id'] != '') {
            $blogId = $this->decrypt($params['id']);
            $blogDetailArr = array();
            $blogDetailArr[] = $blogId;
            $blogData = $this->_blogTable->getBlogById($blogDetailArr);
            $blog_image_path = '';
            if ($blogData['blog_image'] != '') {
                $upload_file_path = $this->_config['file_upload_path'];
                $blog_image_path = file_exists($upload_file_path['assets_upload_dir'] . "blog/medium/" . $blogData['blog_image']) ? $upload_file_path['assets_url'] . "blog/medium/" . $blogData['blog_image'] : '';
            }
            $blogTagsData = $this->_blogTable->getBlogTagsByBlogId(array($blogId));
            $blogTags = $blog->getCreateBlogTagsStringArr($blogTagsData);
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => $blogViewMessages,
                'blog_id' => $blogId,
                'encrypt_blog_id' => $params['id'],
                'blog_data' => $blogData,
                'blog_tags' => (isset($blogTags['tags']) && !empty($blogTags['tags']) ? $blogTags['tags'] : ''),
                'blog_image_path' => $blog_image_path,
                'login_user_detail' => $this->_auth->getIdentity()
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to show all blogs detail
     * @param void
     * @return array
     * @author Icreon Tech - AG
     */
    public function crmBlogAction() {
        $this->checkUserAuthentication();
        $this->getBlogTable();
        $blogCreateMessages = array_merge($this->_config['Blog_messages']['config']['BLOG_CREATE_MESSAGE'], $this->_config['Blog_messages']['config']['SEARCH_MESSAGES']);
        $blog = new Blog($this->_adapter);
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();
        $this->layout('crm');
        $blogId = $this->decrypt($params['id']);
        $blogDetailArr = array();
        $blogDetailArr[] = $blogId;
        $blogData = $this->_blogTable->getBlogById($blogDetailArr);
        $mode = (isset($params['mode']) && $params['mode'] != '') ? $this->decrypt($params['mode']) : 'view';
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'jsLangTranslate' => $blogCreateMessages,
            'blog_id' => $blogId,
            'encrypt_blog_id' => $params['id'],
            'blog_data' => $blogData,
            'module_name' => $this->encrypt('blog'),
            'mode' => $mode,
            'login_user_detail' => $this->_auth->getIdentity(),
            'encrypted_source_type_id' => $this->encrypt('3')
        ));
        return $viewModel;
    }

    /**
     * This action is used to listing of Blog pages
     * @param void
     * @return array
     * @author Icreon Tech - AS
     */
    public function getCrmBlogsListAction() {
        $this->checkUserAuthentication();
        $this->getBlogTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        parse_str($request->getPost('searchString'), $searchParam);

        $dashlet = $request->getPost('crm_dashlet');
        $dashletSearchId = $request->getPost('searchId');
        $dashletId = $request->getPost('dashletId');
        $dashletSearchType = $request->getPost('searchType');
        $dashletSearchString = $request->getPost('dashletSearchString');
        if (isset($dashletSearchString) && $dashletSearchString != '') {
            $searchParam = unserialize($dashletSearchString);
        }
        if (isset($searchParam['publish_date']) && $searchParam['publish_date'] != 1 && $searchParam['publish_date'] != '') {
            $dateRange = $this->getDateRange($searchParam['publish_date']);
            $searchParam['date_from'] = $this->DateFormat($dateRange['from'], 'db_date_format_from');
            $searchParam['date_to'] = $this->DateFormat($dateRange['to'], 'db_date_format_to');
        } else if (isset($searchParam['publish_date']) && $searchParam['publish_date'] == '') {
            $searchParam['date_from'] = '';
            $searchParam['date_to'] = '';
        } else {
            if (isset($searchParam['date_from']) && $searchParam['date_from'] != '') {
                $searchParam['date_from'] = $this->DateFormat($searchParam['date_from'], 'db_date_format_from');
            }
            if (isset($searchParam['date_to']) && $searchParam['date_to'] != '') {
                $searchParam['date_to'] = $this->DateFormat($searchParam['date_to'], 'db_date_format_to');
            }
        }
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        $searchParam['startIndex'] = $start;
        $searchParam['recordLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');
        $params = $this->params()->fromRoute();
        if (isset($searchParam['tag']) && !empty($searchParam['tag'])) {
            $searchParam['tag'] = str_replace(",", "','", $searchParam['tag']);
        }
        $searchResult = $this->getBlogTable()->getCrmBlogs($searchParam);
        
        $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
        $dashletResult = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getTableStruct(array('user_id' => $this->_auth->getIdentity()->crm_user_id, 'search_id' => $dashletSearchId, 'dash_id' => $dashletId, 'search_type' => $dashletSearchType));
        $dashletColumnName = array();
        if (!empty($dashletResult)) {
            $dashletColumn = explode(",", $dashletResult[0]['table_column_names']);
            foreach ($dashletColumn as $val) {
                if (strpos($val, ".")) {
                    $dashletColumnName[] = trim(strstr($val, ".", false), ".");
                } else {
                    $dashletColumnName[] = trim($val);
                }
            }
        }
        $jsonResult = array();
        $arrCell = array();
        $subArrCell = array();
        $jsonResult['page'] = $page;
        $jsonResult['records'] = $countResult[0]->RecordCount;
        $jsonResult['total'] = @ceil($countResult[0]->RecordCount / $limit);
        if (count($searchResult) > 0) {
            foreach ($searchResult as $val) {
                $dashletCell = array();
                $arrCell['id'] = $val['blog_id'];
                if ($val['is_published'] == 1) {
                    $isPublish = "Published";
                } else {
                    $isPublish = "Not Published";
                }
                $view = "<a class='view-icon' href='/crm-blog/" . $this->encrypt($val['blog_id']) . "/" . $this->encrypt('view') . "'><div class='tooltip'>View<span></span></div></a>";
                $edit = "<a class='edit-icon' href='/crm-blog/" . $this->encrypt($val['blog_id']) . "/" . $this->encrypt('edit') . "'><div class='tooltip'>Edit<span></span></div></a>";
                $delete = "<a class='delete-icon delete_blog' href='#delete_blog' onclick=deleteBlog('" . $val['blog_id'] . "')><div class='tooltip'>Delete<span></span></div></a>";

                if (false !== $dashletColumnKey = array_search('blog_title', $dashletColumnName))
                    $dashletCell[$dashletColumnKey] = '<a href="/crm-blog/' . $this->encrypt($val['blog_id']) . '/' . $this->encrypt('view') . '" class="txt-decoration-underline">' . $val['blog_title'] . '</a>';
                if (false !== $dashletColumnKey = array_search('category_name', $dashletColumnName))
                    $dashletCell[$dashletColumnKey] = $val['category_name'];

                if (false !== $dashletColumnKey = array_search('author_name', $dashletColumnName))
                    $dashletCell[$dashletColumnKey] = $val['author_name'];
                if (false !== $dashletColumnKey = array_search('publish_date', $dashletColumnName))
                    $dashletCell[$dashletColumnKey] = $this->OutputDateFormat($val['publish_date'], 'dateformatampm');
                if (false !== $dashletColumnKey = array_search('is_published', $dashletColumnName))
                    $dashletCell[$dashletColumnKey] = $isPublish;

                if (isset($dashlet) && $dashlet == 'dashlet') {
                    $arrCell['cell'] = $dashletCell;
                } else {
                    $arrCell['cell'] = array($val['blog_title'], $val['category_name'], $val['author_name'], $this->OutputDateFormat($val['publish_date'], 'dateformatampm'), $isPublish, $view . $edit . $delete);
                }

                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
        }
        return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    /**
     * This action is used to delete a blog
     * @param void
     * @return array
     * @author Icreon Tech - AS
     */
    public function deleteCrmBlogAction() {
        $this->checkUserAuthentication();
        $this->getBlogTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $blogCreateMessages = array_merge($this->_config['Blog_messages']['config']['BLOG_CREATE_MESSAGE'], $this->_config['Blog_messages']['config']['SEARCH_MESSAGES']);
        if ($request->isPost()) {
            $dataParam['blogId'] = $request->getPost('blog_id');
            $dataParam['isDelete'] = 1;
            $dataParam['modifiedBy'] = $this->_auth->getIdentity()->crm_user_id;
            $dataParam['modifiendDate'] = DATE_TIME_FORMAT;
            $this->getBlogTable()->deleteCrmBlog($dataParam);
            $this->flashMessenger()->addMessage($blogCreateMessages['BLOG_DELETED']);
            $messages = array('status' => "success");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This action is used for autosuggest blog title
     * @param void
     * @return array
     * @author Icreon Tech - AS
     */
    public function getBlogTitleAction() {
        $this->checkUserAuthentication();
        $this->getBlogTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $blogCreateMessages = array_merge($this->_config['Blog_messages']['config']['BLOG_CREATE_MESSAGE'], $this->_config['Blog_messages']['config']['SEARCH_MESSAGES']);
        if ($request->isPost()) {
            $searchParam['startIndex'] = '';
            $searchParam['recordLimit'] = '';
            $searchParam['sortField'] = '';
            $searchParam['sortOrder'] = '';
            $searchParam['title'] = $request->getPost('blog_title');
            $searchParam['category'] = '';
            $searchParam['status'] = '';
            $searchParam['is_featured'] = '';
            $searchParam['date_from'] = '';
            $searchParam['date_to'] = '';
            $result = $this->getBlogTable()->getCrmBlogs($searchParam);
            $blogList = array();
            if (!empty($result)) {
                $i = 0;
                foreach ($result as $key => $val) {
                    $blogList[$i] = array();
                    $blogList[$i]['blog_title'] = $val['blog_title'];
                    $i++;
                }
            }
            return $response->setContent(\Zend\Json\Json::encode($blogList));
        }
    }

    /**
     * This action is used for display blog post
     * @param void
     * @return array
     * @author Icreon Tech - AS
     */
    public function getBlogsAction() {
        $request = $this->getRequest();
        $this->getBlogTable();
        $blogViewMessages = $this->_config['Blog_messages']['config']['VIEW_MESSAGES'];
        $blog = new Blog($this->_adapter);
        $this->layout('layout');
        $request = $this->getRequest();
        $SearchFrontBlogsForm = new SearchFrontBlogsForm();
        $blogPostData = $this->_blogTable->getBlogPostData();
        $blogFeaturedData = $this->_blogTable->getBlogFeaturedPostData();
        $blogRecentData = $this->_blogTable->getBlogRecentPostData();
        $blogCategoriesData = $this->_blogTable->getBlogCategories();

        if (isset($blogPostData[0]['blog_image']) && !empty($blogPostData[0]['blog_image'])) {
            $upload_file_path = $this->_config['file_upload_path'];
            $blog_image_path = $upload_file_path['assets_url'] . "blog/medium/";
            $blog_image_exist = $upload_file_path['assets_upload_dir'] . "blog/medium/";
        } else {
            $blog_image_path = '';
            $blog_image_exist = '';
        }
        
        $objCMS = new CmsController();
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'jsLangTranslate' => $blogViewMessages,
            'blogPostData' => $blogPostData,
            'blogFeaturedData' => $blogFeaturedData,
            'blogRecentData' => $blogRecentData,
            'blogCategoriesData' => $blogCategoriesData,
            'blog_image_path' => $blog_image_path,
            'SearchFrontBlogsForm' => $SearchFrontBlogsForm,
            'blog_image_exist' => $blog_image_exist,
            'blogFeaturedDataHTML' => $objCMS->blogFeaturedData($this->getServiceLocator()->get('Blog\Model\BlogTable')->getBlogPostData(),$this->getServiceLocator()->get('Blog\Model\BlogTable')->getBlogFeaturedPostData(),$this->_config['file_upload_path']),
            'blogRecentDataHTML' => $objCMS->blogRecentData($this->getServiceLocator()->get('Blog\Model\BlogTable')->getBlogPostData(),$this->getServiceLocator()->get('Blog\Model\BlogTable')->getBlogRecentPostData(),$this->_config['file_upload_path'])        
        ));
        return $viewModel;
    }

    /**
     * This action is used for display blog post by a id
     * @param void
     * @return array
     * @author Icreon Tech - AS
     */
    public function getBlogInfoAction() {
        $this->getBlogTable();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $blog = new Blog($this->_adapter);
        $blogViewMessages = $this->_config['Blog_messages']['config']['VIEW_MESSAGES'];
        if (isset($params['id']) && $params['id'] != '') {
            $SearchFrontBlogsForm = new SearchFrontBlogsForm();
            $blogPostData = $this->_blogTable->getBlogPostDataById($this->decrypt($params['id']));
            $blogPostDataPre = $this->_blogTable->getPreBlogById($this->decrypt($params['id']));
            if (empty($blogPostDataPre))
                $blogPostDataPre[0] = array();
            $blogPostDataNxt = $this->_blogTable->getNxtBlogById($this->decrypt($params['id']));

            if (empty($blogPostDataNxt))
                $blogPostDataNxt[0] = array();

            $blogFeaturedData = $this->_blogTable->getBlogFeaturedPostData();
            $blogRecentData = $this->_blogTable->getBlogRecentPostData();
            $blogCategoriesData = $this->_blogTable->getBlogCategories();
            $upload_file_path = $this->_config['file_upload_path'];
            $blog_image_path = $upload_file_path['assets_url'] . "blog/medium/";
            $blog_image_exist = $upload_file_path['assets_upload_dir'] . "blog/medium/";
            $this->layout('layout');
            $viewModel = new ViewModel();
            $viewModel->setVariables(array(
                'jsLangTranslate' => $blogViewMessages,
                'blogPostData' => $blogPostData,
                'blogFeaturedData' => $blogFeaturedData,
                'blogRecentData' => $blogRecentData,
                'blogCategoriesData' => $blogCategoriesData,
                'blog_image_path' => $blog_image_path,
                'SearchFrontBlogsForm' => $SearchFrontBlogsForm,
                'blogPostDataPre' => $blogPostDataPre[0],
                'blogPostDataNxt' => $blogPostDataNxt[0],
                'blog_image_exist' => $blog_image_exist
            ));
            return $viewModel;
        } else {
            return $this->redirect()->toRoute('getBlogs');
        }
    }

    /**
     * This action is used for display blog post's of a particular category
     * @param void
     * @return array
     * @author Icreon Tech - AS
     */
    public function getBlogListAction() {
        $this->getBlogTable();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $blog = new Blog($this->_adapter);
        $blogViewMessages = $this->_config['Blog_messages']['config']['VIEW_MESSAGES'];
        if (isset($params['id']) && $params['id'] != '') {
            $SearchFrontBlogsForm = new SearchFrontBlogsForm();
            $blogPostDataArray = $this->_blogTable->getBlogPostDataByCategoryId($this->decrypt($params['id']));
            $blogFeaturedData = $this->_blogTable->getBlogFeaturedPostData();
            $blogRecentData = $this->_blogTable->getBlogRecentPostData();
            $blogCategoriesData = $this->_blogTable->getBlogCategories();
            $blogPostData = $blog->getBlogImagesArr($blogPostDataArray, $this->_config['file_upload_path']);
            $upload_file_path = $this->_config['file_upload_path'];
            $blog_image_path = $upload_file_path['assets_url'] . "blog/medium/";
            $blog_image_exist = $upload_file_path['assets_upload_dir'] . "blog/medium/";
            $this->layout('layout');
            $viewModel = new ViewModel();
            $viewModel->setVariables(array(
                'activeId' => $this->decrypt($params['id']),
                'jsLangTranslate' => $blogViewMessages,
                'blogPostData' => $blogPostDataArray,
                'blogFeaturedData' => $blogFeaturedData,
                'blogRecentData' => $blogRecentData,
                'blogCategoriesData' => $blogCategoriesData,
                'SearchFrontBlogsForm' => $SearchFrontBlogsForm,
                'blog_image_path' => $blog_image_path,
                'blog_image_exist' => $blog_image_exist,
                'activeCategory' => $this->decrypt($params['id'])
            ));
            return $viewModel;
        } else {
            return $this->redirect()->toRoute('getBlogs');
        }
    }

    /**
     * This action is used for display seacrh results
     * @param void
     * @return array
     * @author Icreon Tech - AS
     */
    public function blogSearchAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $request->getPost('search_text');
        $this->getBlogTable();
        $blog = new Blog($this->_adapter);
        $blogViewMessages = $this->_config['Blog_messages']['config']['VIEW_MESSAGES'];
        if (isset($params) && $params != '') {
            $searchDetailArr = array();
            $searchDetailArr[] = $params;
            $blogsData = $this->_blogTable->getSearchBlogs($searchDetailArr);
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => $blogViewMessages,
                'blogsData' => $blogsData,
                'searchedText' => $params,
            ));
            return $viewModel;
        }
    }

    /**
     * This Action is used to get the saved blog search
     * @param void
     * @return this will be json format
     * @author Icreon Tech - AG
     */
    public function getCrmSearchSavedBlogAction() {
        $this->checkUserAuthentication();
        $this->getBlogTable();
        $Blog = new Blog($this->_adapter);
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            if ($request->getPost('search_id') != '') {
                $search_array = array();
                $search_array['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $search_array['blog_search_id'] = $request->getPost('search_id');
                $search_array['is_active'] = '1';
                $blogSavedSearchArray = $Blog->getBlogSavedSearchArr($search_array);
                $blogSearchArray = $this->_blogTable->getBlogSavedSearch($blogSavedSearchArray);
                $searchResult = json_encode(unserialize($blogSearchArray[0]['search_query']));
                return $response->setContent($searchResult);
            }
        }
    }

    /**
     * This Action is used to save the blog search
     * @param void
     * @return this will be json format with comfirmation message
     * @author Icreon Tech -AG
     */
    public function addCrmSearchBlogAction() {
        $this->checkUserAuthentication();
        $this->getBlogTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $blog = new Blog($this->_adapter);
        $blogMessages = array_merge($this->_config['Blog_messages']['config']['BLOG_CREATE_MESSAGE'], $this->_config['Blog_messages']['config']['SEARCH_MESSAGES']);
        if ($request->isPost()) {
            parse_str($request->getPost('searchString'), $searchParam);
            $search_name = $request->getPost('search_name');
            $messages = array();
            parse_str($request->getPost('searchString'), $searchParam);
            $searchParam['search_title'] = $search_name;
            $searchParam = $blog->getInputFilterBlogSearch($searchParam);
            $search_name = $blog->getInputFilterBlogSearch($search_name);
            if (trim($search_name) != '') {
                $saveSearchParam['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $saveSearchParam['title'] = $search_name;
                $saveSearchParam['search_query'] = serialize($searchParam);
                $saveSearchParam['is_active'] = 1;
                $saveSearchParam['is_deleted'] = '0';
                if ($request->getPost('search_id') != '') {
                    $saveSearchParam['modified_date'] = DATE_TIME_FORMAT;
                    $saveSearchParam['blog_search_id'] = $request->getPost('search_id');
                    $blogUpdateArray = $blog->getAddBlogSearchArr($saveSearchParam);
                    if ($this->_blogTable->updateBlogSearch($blogUpdateArray) == true) {
                        $this->flashMessenger()->addMessage($blogMessages['SEARCH_UPDATED_MSG']);
                        $messages = array('status' => "success");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                } else {
                    $saveSearchParam['added_date'] = DATE_TIME_FORMAT;
                    $searchAddArray = $blog->getAddBlogSearchArr($saveSearchParam);
                    if ($this->_blogTable->saveBlogSearch($searchAddArray) == true) {
                        $this->flashMessenger()->addMessage($blogMessages['SEARCH_ADDED_MSG']);
                        $messages = array('status' => "success");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                }
            }
        }
    }

    /**
     * This Action is used to delete the saved pledge search
     * @param void
     * @return this will be a confirmation message in json
     * @author Icreon Tech - AG
     */
    public function deleteCrmSearchBlogAction() {
        $this->checkUserAuthentication();
        $this->getBlogTable();
        $blog = new Blog($this->_adapter);
        $request = $this->getRequest();
        $response = $this->getResponse();
        $blogMessages = array_merge($this->_config['Blog_messages']['config']['BLOG_CREATE_MESSAGE'], $this->_config['Blog_messages']['config']['SEARCH_MESSAGES']);
        if ($request->isPost()) {
            $delete_array = array();
            $delete_array['is_deleted'] = 1;
            $delete_array['modified_date'] = DATE_TIME_FORMAT;
            $delete_array['blog_search_id'] = $request->getPost('search_id');
            $blog_delete_saved_search_array = $blog->getDeleteBlogSavedSearchArr($delete_array);
            $this->_blogTable->deleteSavedSearch($blog_delete_saved_search_array);
            $messages = array('status' => "success");
            $this->flashMessenger()->addMessage($blogMessages['SEARCH_DELETE_CONFIRM']);
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This action is used to upload file for activity
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function uploadBlogImageAction() {
        $this->checkUserAuthentication();
        $fileInputNames = array_keys($_FILES);
        $fileInputName = $fileInputNames[0];
        $fileExt = $this->GetFileExt($_FILES[$fileInputName]['name']);
        $filename = $this->GenerateMediaFileName($fileExt[1]);    //Generate file name
        $_FILES[$fileInputName]['name'] = $filename;                 // assign name to file variable
        $this->getBlogTable();
        $blogMessages = array_merge($this->_config['Blog_messages']['config']['BLOG_CREATE_MESSAGE'], $this->_config['Blog_messages']['config']['SEARCH_MESSAGES']);
        $uploadFilePath = $this->_config['file_upload_path'];   // get file upload configuration

        $fileUploadInformation = $this->_config['file_upload_information'];
        $errorMessages = array(
            'post_max_size' => $blogMessages['ATTACHMENT_POST_SIZE_EXCEED'], // Set error messages
            'max_file_size' => sprintf($blogMessages['FILE_SIZE_BIG'], ($fileUploadInformation['max_allowed_file_size'] / 1024 / 1024)),
            'accept_file_types' => $blogMessages['FILE_TYPE_NOT_ALLOWED'],
            2 => sprintf($blogMessages['FILE_SIZE_BIG'], ($fileUploadInformation['max_allowed_file_size'] / 1024 / 1024))
        );
        $options = array(
            'upload_dir' => $uploadFilePath['temp_upload_dir'],
            'param_name' => $fileInputName, //file input name                      // Set configuration
            'inline_file_types' => $fileUploadInformation['file_types_allowed'],
            'accept_file_types' => $fileUploadInformation['file_types_allowed'],
            'max_file_size' => $fileUploadInformation['max_allowed_file_size'],
            'image_versions' => array(
                'blog' => array(
                    'max_width' => 520,
                    'max_height' => 250,
                    'jpeg_quality' => 80
                ),
                'thumbnail' => array(
                    'max_width' => 80,
                    'max_height' => 80
                )
            )
        );
        $uploadHandler = new UploadHandler($options, true, $errorMessages);
        $fileResponse = $uploadHandler->jsonResponceData;
        if (isset($fileResponse[$fileInputName][0]->error)) {
            $messages = array('status' => "error", 'message' => $fileResponse[$fileInputName][0]->error);
        } else {
            $messages = array('status' => "success", 'message' => 'File uploaded', 'filename' => $fileResponse[$fileInputName][0]->name);
        }
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This function is used to move file from temp to blog folder
     * @return json
     * @param 1 for add , 2 for edit
     * @author Icreon Tech - KK
     */
    public function moveFileFromTempToBlog($filename, $oldAttachmentDetail = array(), $operation = 1) {
        $uploadFilePath = $this->_config['file_upload_path'];   // get file upload configuration
        $tempDir = $uploadFilePath['temp_upload_dir'];
        $tempThumbnailDir = $uploadFilePath['temp_upload_thumbnail_dir'];
        $quizDir = $uploadFilePath['assets_upload_dir'] . "/blog/";
        $resizedImageDir = $uploadFilePath['assets_upload_dir'] . "/medium/";
        $quizThumbnailDir = $uploadFilePath['assets_upload_dir'] . "/blog/thumbnail/";

        $tempMediumDir = $tempDir . 'blog/';
        $quizMediumDir = $uploadFilePath['assets_upload_dir'] . "/blog/medium/";

        if ($filename != '') {
            $tempFile = $tempDir . $filename;
            $tempThumbnailFile = $tempThumbnailDir . $filename;
            $quizFilename = $quizDir . $filename;
            $quizThumbnailFilename = $quizThumbnailDir . $filename;
            $tempMediumFile = $tempMediumDir . $filename;
            $quizMediumFilename = $quizMediumDir . $filename;


            if (file_exists($tempFile)) {
                if (copy($tempFile, $quizFilename)) {
                    unlink($tempFile);
                }
            }
            if (file_exists($tempThumbnailFile)) {
                if (copy($tempThumbnailFile, $quizThumbnailFilename)) {
                    unlink($tempThumbnailFile);
                }
            }

            if (file_exists($tempMediumFile)) {
                if (copy($tempMediumFile, $quizMediumFilename)) {
                    unlink($tempMediumFile);
                }
            }

            if ($operation == 2) {
                $removeFiles = array_diff($oldAttachmentDetail, $attachmentDetail);
                if (count($removeFiles) > 0) {
                    foreach ($removeFiles as $oldfilename) {
                        $activityOldFilename = $quizDir . $oldfilename;
                        $activityOldThumbnailFilename = $quizThumbnailDir . $oldfilename;
                        if (file_exists($activityOldFilename)) {
                            unlink($activityOldFilename);
                        }
                        if (file_exists($activityOldThumbnailFilename)) {
                            unlink($activityOldThumbnailFilename);
                        }
                    }
                }
            }
        }
    }

    /**
     * This action is used checking blog title
     * @return json format data
     * @param void
     * @author Icreon Tech - AS
     */
    public function checkBlogTitleAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        $this->getBlogTable();
        $blog = new Blog($this->_adapter);
        $blogViewMessages = $this->_config['Blog_messages']['config']['VIEW_MESSAGES'];
        $params['id'] = $request->getPost('id') ? $request->getPost('id') : '';
        $searchResult = $this->_blogTable->checkBlogTitleUnique($params);
        if ($searchResult[0]['isAvailable'] > 0) {
            $msg = $blogViewMessages['URL_AVAILABLE'];
            $messages = array('status' => "success", 'message' => $msg);
        } else {
            $msg = $blogViewMessages['URL_EXIST'];
            $messages = array('status' => "error", 'message' => $msg);
        }
        return $response->setContent(\Zend\Json\Json::encode($messages));
    }

    /**
     * This action is used checking blog title
     * @return json format data
     * @param void
     * @author Icreon Tech - AS
     */
    public function blogMoreEntriesAction() {
        $request = $this->getRequest();
        $this->getBlogTable();
        $blogViewMessages = $this->_config['Blog_messages']['config']['VIEW_MESSAGES'];
        $blog = new Blog($this->_adapter);
        $this->layout('layout');
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $today = getdate();
        $monthNames = Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
        $today = getdate();
        if (isset($params["month"]))
            $cMonth = $this->decrypt($params["month"]);
        if (isset($params["year"]))
            $cYear = $this->decrypt($params["year"]);
        $date['prev_year'] = $cYear;
        $date['next_year'] = $cYear;
        $date['prev_month'] = $cMonth - 1;
        $date['next_month'] = $cMonth + 1;
        if ($date['prev_month'] == 0) {
            $date['prev_month'] = 12;
            $date['prev_year'] = $cYear - 1;
        }
        if ($date['next_month'] == 13) {
            $date['next_month'] = 1;
            $date['next_year'] = $cYear + 1;
        }

        $title = $monthNames[$cMonth - 1] . ' ' . $cYear;
        $num_days = cal_days_in_month(CAL_GREGORIAN, $cMonth, $cYear);
        $param['dateFrom'] = date("{$cYear}-{$cMonth}-01 00:00:00");
        $param['dateTo'] = date("{$cYear}-{$cMonth}-$num_days 12:59:59");
        $blogPostData = $this->_blogTable->getBlogPostDataByDate($param);
        $postData[0] = '';
        $postData[1] = '';
        $postData[3] = 'asc';
        $postData[2] = 'tbl_membership.minimun_donation_amount';
        $membershipData = $this->getServiceLocator()->get('User\Model\MembershipTable')->getAllMembership($postData);
        $i = 0;
        foreach ($membershipData as $key) {
            if ($key['membership_id'] != '1' && $key['minimun_donation_amount'] > 0) {
                if ($i == 0) {
                    $min_donation = $key['minimun_donation_amount'];
                } else {
                    if ($min_donation > $key['minimun_donation_amount']) {
                        $min_donation = $key['minimun_donation_amount'];
                    }
                }
                $i++;
            }
        }
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'jsLangTranslate' => $blogViewMessages,
            'month' => $title,
            'date' => $date,
            'blogPostData' => $blogPostData,
            'minDonation' => $min_donation
        ));
        return $viewModel;
    }

}