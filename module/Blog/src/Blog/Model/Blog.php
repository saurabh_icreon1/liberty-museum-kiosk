<?php

/**
 * This model is used for Blog validation.
 * @category   Zend
 * @package    Blog
 * @author     Icreon Tech - AS
 */

namespace Blog\Model;

use Blog\Module;
use Base\Model\BaseModel;
use Zend\Db\Adapter\Adapter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Blog extends BaseModel {

    protected $inputFilter;
    protected $adapter;

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    /**
     * Function used to check variables cases     
     * @param Array
     * @return array
     * @author Icreon Tech - AS
     */
    public function exchangeArray($param = array()) {
        
    }

    public function getInputFilterCrmGallery() {
        
    }

    /**
     * This method is used to add filtering and validation to the form elements of create crm blog post
     * @param void
     * @return Array
     * @author Icreon Tech - AG
     */
    public function getInputFilterCreateBlog() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                        'name' => 'blog_title',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'BLOG_TITLE',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'category_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'CATEGORY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'blog_content',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'CONTENT',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'publish_date',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'PUBLISH_DATE',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'publish_time',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'PUBLISH_TIME',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'author_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'AUTHOR_NAME',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_featured',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'status',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'tag',
                        'required' => false,
                    )));
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * This method is used to call add blog procedure parameter
     * @param code
     * @return Void
     * @author Icreon Tech - AG
     */
    public function getCreateBlogArr($data) {
        $returnArr = array();
        $returnArr['blog_title'] = (isset($data['blog_title']) && !empty($data['blog_title'])) ? $data['blog_title'] : '';
        $returnArr['category_id'] = (isset($data['category_id']) && !empty($data['category_id'])) ? $data['category_id'] : '';
        $returnArr['blog_content'] = (isset($data['blog_content']) && !empty($data['blog_content'])) ? addslashes($data['blog_content']) : '';
        $returnArr['author_name'] = (isset($data['author_name']) && !empty($data['author_name'])) ? $data['author_name'] : '';
        if (isset($data['publish_date']) && !empty($data['publish_date'])) {
            $returnArr['publish_date'] = date('Y-m-d H:i:s', strtotime($data['publish_date'] . " " . $data['publish_time']));
        } else {
            $returnArr['publish_date'] = '';
        }
        if (isset($data['save']) && $data['save'] == 'SAVE') {
            $returnArr['saveandpublish'] = (isset($data['status']) && !empty($data['status'])) ? $data['status'] : '0';
        } else if (isset($data['saveandpublish']) && $data['saveandpublish'] == 'SAVE AND PUBLISH') {
            $returnArr['saveandpublish'] = 1;
        }
        $returnArr['is_featured'] = (isset($data['is_featured'])) ? $data['is_featured'] : '';
        $returnArr['is_deleted'] = (isset($data['is_deleted'])) ? $data['is_deleted'] : '';
        $returnArr['added_by'] = (isset($data['added_by']) && !empty($data['added_by'])) ? $data['added_by'] : '';
        $returnArr['added_date'] = (isset($data['added_date']) && !empty($data['added_date'])) ? $data['added_date'] : '';
        $returnArr['modify_by'] = (isset($data['modify_by']) && !empty($data['modify_by'])) ? $data['modify_by'] : '';
        $returnArr['modify_date'] = (isset($data['modify_date']) && !empty($data['modify_date'])) ? $data['modify_date'] : '';
        $returnArr['main_image_hidden'] = (isset($data['main_image_hidden']) && !empty($data['main_image_hidden'])) ? $data['main_image_hidden'] : '';
        return $returnArr;
    }

    /**
     * This method is used to call add blog's tags procedure parameter
     * @param code
     * @return Void
     * @author Icreon Tech - AG
     */
    public function getCreateBlogTagsArr($data) {
        $returnArr = array();
        $returnArr[] = (isset($data['blogId']) && !empty($data['blogId'])) ? $data['blogId'] : '';
        $returnArr[] = (isset($data['tagContent']) && !empty($data['tagContent'])) ? $data['tagContent'] : '';
        $returnArr[] = (isset($data['is_deleted'])) ? $data['is_deleted'] : '';
        $returnArr[] = (isset($data['added_by']) && !empty($data['added_by'])) ? $data['added_by'] : '';
        $returnArr[] = (isset($data['added_date']) && !empty($data['added_date'])) ? $data['added_date'] : '';
        $returnArr[] = (isset($data['modify_by']) && !empty($data['modify_by'])) ? $data['modify_by'] : '';
        $returnArr[] = (isset($data['modify_date']) && !empty($data['modify_date'])) ? $data['modify_date'] : '';
        return $returnArr;
    }

    /**
     * This method is used to call get blog's tags as a string
     * @param code
     * @return Void
     * @author Icreon Tech - AG
     */
    public function getCreateBlogTagsStringArr($data) {
        $returnArray = '';
        $tagIdString = '';
        $tagsString = '';
        $i = 0;
        if (isset($data) && !empty($data)) {
            foreach ($data as $key => $val) {
                if ($i == 0) {
                    $tagsString.= $val['tag'];
                    $tagIdString.= $val['bog_tag_id'];
                } else {
                    $tagsString.= ',' . $val['tag'];
                    $tagIdString.= ',' . $val['bog_tag_id'];
                }
                ++$i;
            }
            $returnArray['tagId'] = $tagIdString;
            $returnArray['tags'] = $tagsString;
        }
        return $returnArray;
    }

    /**
     * This method is used to call edit blog procedure parameter
     * @param code
     * @return Void
     * @author Icreon Tech - AG
     */
    public function getEditBlogArr($data) {
        $returnArr = array();
        $returnArr[] = (isset($data['blog_id']) && !empty($data['blog_id'])) ? $data['blog_id'] : '';
        $returnArr[] = (isset($data['blog_title']) && !empty($data['blog_title'])) ? $data['blog_title'] : '';
        $returnArr[] = (isset($data['category_id']) && !empty($data['category_id'])) ? $data['category_id'] : '';
        $returnArr[] = (isset($data['blog_content']) && !empty($data['blog_content'])) ? $data['blog_content'] : '';
        $returnArr[] = (isset($data['author_name']) && !empty($data['author_name'])) ? $data['author_name'] : '';
        if (isset($data['publish_date']) && !empty($data['publish_date'])) {
            $returnArr[] = $data['publish_date'] . " " . $data['publish_time'];
        } else {
            $returnArr[] = '';
        }
        if (isset($data['updateandpublish']) && $data['updateandpublish'] == 'UPDATE AND PUBLISH') {
            $returnArr[] = 1;
        } else if (isset($data['update']) && $data['update'] == 'UPDATE') {
            $returnArr[] = (isset($data['status']) && !empty($data['status'])) ? $data['status'] : '0';
        }
        $returnArr[] = (isset($data['is_featured']) && !empty($data['is_featured'])) ? $data['is_featured'] : '0';
        $returnArr[] = (isset($data['is_deleted'])) ? $data['is_deleted'] : '';
        $returnArr[] = (isset($data['modify_by']) && !empty($data['modify_by'])) ? $data['modify_by'] : '';
        $returnArr[] = (isset($data['modify_date']) && !empty($data['modify_date'])) ? $data['modify_date'] : '';
        if (isset($data['new_image_hidden']) && !empty($data['new_image_hidden'])) {
            $returnArr[] = $data['new_image_hidden'];
        } else if (isset($data['main_image_hidden']) && !empty($data['main_image_hidden'])) {
            $returnArr[] = $data['main_image_hidden'];
        } else {
            $returnArr[] = '';
        }
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for saved search
     * @param Array
     * @return Array
     * @author Icreon Tech - AG
     */
    public function getBlogSavedSearchArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['crm_user_id']) && $dataArr['crm_user_id'] != '') ? $dataArr['crm_user_id'] : '';
        $returnArr[] = (isset($dataArr['quiz_search_id']) && $dataArr['quiz_search_id'] != '') ? $dataArr['quiz_search_id'] : '';
        $returnArr[] = (isset($dataArr['is_active'])) ? $dataArr['is_active'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for delete saved search
     * @param Array
     * @return Array
     * @author Icreon Tech - AG
     */
    public function getDeleteBlogSavedSearchArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['is_deleted']) && $dataArr['is_deleted'] != '') ? $dataArr['is_deleted'] : '';
        $returnArr[] = (isset($dataArr['modified_date']) && $dataArr['modified_date'] != '') ? $dataArr['modified_date'] : '';
        $returnArr[] = (isset($dataArr['blog_search_id']) && $dataArr['blog_search_id'] != '') ? $dataArr['blog_search_id'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for save blog search
     * @param array
     * @return array
     * @author Icreon Tech - AG
     */
    public function getInputFilterBlogSearch($filterData) {
        $inputFilterData = array();
        if (is_array($filterData)) {
            foreach ($filterData as $key => $val) {
                $inputFilterData[$key] = trim(addslashes(strip_tags($val)));
            }
        } else {
            $inputFilterData = trim(addslashes(strip_tags($filterData)));
        }
        return $inputFilterData;
    }

    /**
     * This method is used to return the stored procedure array for save blog search
     * @param array
     * @return array
     * @author Icreon Tech - AG
     */
    public function getAddBlogSearchArr($dataArr = array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['crm_user_id']) && $dataArr['crm_user_id'] != '') ? $dataArr['crm_user_id'] : '';
        $returnArr[] = (isset($dataArr['title']) && $dataArr['title'] != '') ? $dataArr['title'] : '';
        $returnArr[] = (isset($dataArr['search_query']) && $dataArr['search_query'] != '') ? $dataArr['search_query'] : '';
        $returnArr[] = (isset($dataArr['is_active']) && $dataArr['is_active'] != '') ? $dataArr['is_active'] : '';
        $returnArr[] = (isset($dataArr['is_deleted']) && $dataArr['is_deleted'] != '') ? $dataArr['is_deleted'] : '';
        if (isset($dataArr['added_date'])) {
            $returnArr[] = $dataArr['added_date'];
        } else {
            $returnArr[] = $dataArr['modified_date'];
        }
        if (isset($dataArr['blog_search_id']) && $dataArr['blog_search_id'] != '') {
            $returnArr[] = $dataArr['blog_search_id'];
        }
        return $returnArr;
    }

    public function getBlogImagesArr($dataArr = array(), $upload_file_path) {
        $returnArr = array();
        if (!empty($dataArr)) {
            foreach ($dataArr as $key => $val) {
                $returnArr[$key]['blog_id'] = $val['blog_id'];
                $returnArr[$key]['tags'] = $val['tags'];
                $returnArr[$key]['blog_title'] = $val['blog_title'];
                $returnArr[$key]['blog_content'] = $val['blog_content'];
                $returnArr[$key]['publish_date'] = $val['publish_date'];
                $returnArr[$key]['blog_image'] = file_exists($upload_file_path['assets_upload_dir'] . "blog/" . $val['blog_image']) ? $upload_file_path['assets_url'] . "blog/" . $val['blog_image'] :  $upload_file_path."/images/no-img-blog.jpg";
            }
        }
        return $returnArr;
    }

}