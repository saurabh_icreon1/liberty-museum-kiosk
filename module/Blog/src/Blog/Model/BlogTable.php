<?php

/**
 * This model is used for Blog.
 * @category   Zend
 * @package    Blog
 * @author     Icreon Tech - AS
 */

namespace Blog\Model;

use Zend\Db\TableGateway\TableGateway;

class BlogTable {

    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
    }

    /**
     * Function for insert blog post data
     * @author Icreon Tech - AG
     * @return Int
     * @param Array
     */
    public function saveBlog($blogData) {
        $procquesmarkapp = $this->appendQuestionMars(count($blogData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_blo_insertCrmBlog(?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $stmt->getResource()->bindParam(1, $blogData['blog_title']);
        $stmt->getResource()->bindParam(2, $blogData['category_id']);
        $stmt->getResource()->bindParam(3, $blogData['blog_content']);
        $stmt->getResource()->bindParam(4, $blogData['author_name']);
        $stmt->getResource()->bindParam(5, $blogData['publish_date']);
        $stmt->getResource()->bindParam(6, $blogData['saveandpublish']);
        $stmt->getResource()->bindParam(7, $blogData['is_featured']);
        $stmt->getResource()->bindParam(8, $blogData['is_deleted']);
        $stmt->getResource()->bindParam(9, $blogData['added_by']);
        $stmt->getResource()->bindParam(10, $blogData['added_date']);
        $stmt->getResource()->bindParam(11, $blogData['modify_by']);
        $stmt->getResource()->bindParam(12, $blogData['modify_date']);
        $stmt->getResource()->bindParam(13, $blogData['main_image_hidden']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for update blog post data
     * @author Icreon Tech - AG
     * @return Int
     * @param Array
     */
    public function updateBlog($blogData) {
        $procquesmarkapp = $this->appendQuestionMars(count($blogData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_blo_updateCrmBlog(?,?,?,?,?,?,?,?,?,?,?,?)");
        $stmt->getResource()->bindParam(1, $blogData[0]);
        $stmt->getResource()->bindParam(2, $blogData[1]);
        $stmt->getResource()->bindParam(3, $blogData[2]);
        $stmt->getResource()->bindParam(4, $blogData[3]);
        $stmt->getResource()->bindParam(5, $blogData[4]);
        $stmt->getResource()->bindParam(6, $blogData[5]);
        $stmt->getResource()->bindParam(7, $blogData[6]);
        $stmt->getResource()->bindParam(8, $blogData[7]);
        $stmt->getResource()->bindParam(9, $blogData[8]);
        $stmt->getResource()->bindParam(10, $blogData[9]);
        $stmt->getResource()->bindParam(11, $blogData[10]);
        $stmt->getResource()->bindParam(12, $blogData[11]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        //$resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        return true;
    }

    /**
     * Function for insert blog post tags
     * @author Icreon Tech - AG
     * @return Int
     * @param Array
     */
    public function saveBlogTags($blogTagData) {
        $procquesmarkapp = $this->appendQuestionMars(count($blogTagData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_blo_insertCrmBlogTag(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $blogTagData[0]);
        $stmt->getResource()->bindParam(2, $blogTagData[1]);
        $stmt->getResource()->bindParam(3, $blogTagData[2]);
        $stmt->getResource()->bindParam(4, $blogTagData[3]);
        $stmt->getResource()->bindParam(5, $blogTagData[4]);
        $stmt->getResource()->bindParam(6, $blogTagData[5]);
        $stmt->getResource()->bindParam(7, $blogTagData[6]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for get Blog data on the base of id
     * @author Icreon Tech - AG
     * @return Int
     * @param Array
     */
    public function getBlogById($blogData) {
        $procquesmarkapp = $this->appendQuestionMars(count($blogData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_blo_getCrmBlogInfo(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $blogData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet[0];
        else
            return false;
    }

    /**
     * Function for get Blog's tags on the base of blog id
     * @author Icreon Tech - AG
     * @return Int
     * @param Array
     */
    public function getBlogTagsByBlogId($blogTagData) {
        $procquesmarkapp = $this->appendQuestionMars(count($blogTagData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_blo_getCrmBlogTagsInfo(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $blogTagData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for append number of questions marks for stored procedure
     * @author Icreon Tech - DT
     * @return String
     * @param Array
     */
    public function appendQuestionMars($count) {
        $returnstr = '';
        for ($i = 1; $i <= $count; $i++) {
            $returnstr.='?,';
        }
        return rtrim($returnstr, ',');
    }

    /**
     * Function for blog listing
     * @author Icreon Tech - AS
     * @return String
     * @param Array
     */
    public function getCrmBlogs($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_blo_getCrmBlogs(?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['startIndex']);
            $stmt->getResource()->bindParam(2, $params['recordLimit']);
            $stmt->getResource()->bindParam(3, $params['sortField']);
            $stmt->getResource()->bindParam(4, $params['sortOrder']);
            $stmt->getResource()->bindParam(5, $params['title']);
            $stmt->getResource()->bindParam(6, $params['category']);
            $stmt->getResource()->bindParam(7, $params['status']);
            $stmt->getResource()->bindParam(8, $params['is_featured']);
            $stmt->getResource()->bindParam(9, $params['date_from']);
            $stmt->getResource()->bindParam(10, $params['date_to']);
            $stmt->getResource()->bindParam(11, $params['tag']);
            $stmt->getResource()->bindParam(12, (isset($params['author_name']) and trim($params['author_name']) != "")?addslashes($params['author_name']):"");
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will delete the tags for a blog
     * @param this will be an array having the params.
     * @return this will return a confirmation
     * @author Icreon Tech - AG
     */
    public function deleteBlogTags($blogId) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_blo_deleteTags(?)");
            $stmt->getResource()->bindParam(1, $blogId);
            $result = $stmt->execute();
            $statement = $result->getResource();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to delete blog
     * @author Icreon Tech - AS
     * @return String
     * @param Array
     */
    public function deleteCrmBlog($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_blo_deleteCrmBlogs(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['blogId']);
            $stmt->getResource()->bindParam(2, $params['isDelete']);
            $stmt->getResource()->bindParam(3, $params['modifiedBy']);
            $stmt->getResource()->bindParam(4, $params['modifiendDate']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to get blogs listing
     * @author Icreon Tech - AS
     * @return String
     * @param Array
     */
    public function getBlogTags() {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_blo_getCrmBlogTags()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the saved search listing for blog search
     * @param this will be an array.
     * @return this will return a title of the saved search
     * @author Icreon Tech -AG
     */
    public function getBlogSavedSearch($blogData = array()) {
        $procquesmarkapp = $this->appendQuestionMars(count($blogData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_qui_getCrmSearchBlog(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $blogData[0]);
        $stmt->getResource()->bindParam(2, $blogData[1]);
        $stmt->getResource()->bindParam(3, $blogData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
    }

    /**
     * This function will update the search title into the table for blog search
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech - AG
     */
    public function updateBlogSearch($blogData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($blogData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_qui_updateCrmSearchBlog(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $blogData[0]);
            $stmt->getResource()->bindParam(2, $blogData[1]);
            $stmt->getResource()->bindParam(3, $blogData[2]);
            $stmt->getResource()->bindParam(4, $blogData[3]);
            $stmt->getResource()->bindParam(5, $blogData[4]);
            $stmt->getResource()->bindParam(6, $blogData[5]);
            $stmt->getResource()->bindParam(7, $blogData[6]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will save the search title into the table for blog search
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech - AG
     */
    public function saveBlogSearch($pledgeData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($pledgeData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_qui_insertCrmSearchBlog(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $pledgeData[0]);
            $stmt->getResource()->bindParam(2, $pledgeData[1]);
            $stmt->getResource()->bindParam(3, $pledgeData[2]);
            $stmt->getResource()->bindParam(4, $pledgeData[3]);
            $stmt->getResource()->bindParam(5, $pledgeData[4]);
            $stmt->getResource()->bindParam(6, $pledgeData[5]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will delete the saved blog search titles
     * @param this will be an array having the params.
     * @return this will return a confirmation
     * @author Icreon Tech - AG
     */
    public function deleteSavedSearch($pledgeData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($pledgeData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_ple_deleteCrmSearchBlog(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $pledgeData[0]);
            $stmt->getResource()->bindParam(2, $pledgeData[1]);
            $stmt->getResource()->bindParam(3, $pledgeData[2]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to get blogs post data
     * @author Icreon Tech - AS
     * @return String
     * @param Array
     */
    public function getBlogPostData() {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_blo_getBlogInfo()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            //asd($resultSet);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to get blogs featured post data
     * @author Icreon Tech - AS
     * @return String
     * @param Array
     */
    public function getBlogFeaturedPostData() {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_blo_getFeaturedBlogs()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to get blogs recent post data
     * @author Icreon Tech - AS
     * @return String
     * @param Array
     */
    public function getBlogRecentPostData() {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_blo_getRecentBlogs()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to get blogs categories data
     * @author Icreon Tech - AS
     * @return String
     * @param Array
     */
    public function getBlogCategories() {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_blo_getBlogCategory()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for get Blog search data
     * @author Icreon Tech - AG
     * @return Int
     * @param Array
     */
    public function getSearchBlogs($blogData) {
        $procquesmarkapp = $this->appendQuestionMars(count($blogData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_blo_getBlogSearchList(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $blogData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get Blog data on the base of category id
     * @author Icreon Tech - AG
     * @return Int
     * @param Array
     */
    public function getBlogPostDataByCategoryId($blogData) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_blo_getBlogList(?)");
        $stmt->getResource()->bindParam(1, $blogData);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get Blog data on the base of id
     * @author Icreon Tech - AG
     * @return Int
     * @param Array
     */
    public function getBlogPostDataById($blogData) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_blo_getBlogData(?)");
        $stmt->getResource()->bindParam(1, $blogData);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * This function is used to update page menus
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function checkBlogTitleUnique($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_blo_getblogtitle(?,?)');
            $stmt->getResource()->bindParam(1, $params['title']);
            $stmt->getResource()->bindParam(2, $params['id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to get blogs post data more entries
     * @author Icreon Tech - AS
     * @return String
     * @param Array
     */
    public function getBlogPostDataByDate($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_blo_getBlogMoreEntries(?,?)');
            $stmt->getResource()->bindParam(1, $params['dateFrom']);
            $stmt->getResource()->bindParam(2, $params['dateTo']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * Function to get pre blogs post 
     * @author Icreon Tech - AS
     * @return String
     * @param Array
     */
    public function getPreBlogById($params = array()){
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_blo_getPreBlogById(?)');
            $stmt->getResource()->bindParam(1, $params);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            //asd($resultSet);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * Function to get next blogs post 
     * @author Icreon Tech - AS
     * @return String
     * @param Array
     */
    public function getNxtBlogById($params = array()){
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_blo_getNxtBlogById(?)');
            $stmt->getResource()->bindParam(1, $params);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

}