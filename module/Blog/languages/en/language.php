<?php

return array(
    'BLOG_CREATE_MESSAGE' => array(
        "L_BLOG" => "Blog",
        "L_CMS" => "CMS",
        "L_DETAILS" => "Details",
        "L_EDIT" => "Edit",
        "L_CHANGE_LOG" => "Change Log",
        "L_BLOG_INFO" => "Blog Information",
        "MAIN_IMAGE" => "Main Image",
        "ATTACHMENT_POST_SIZE_EXCEED" => "The uploaded file exceeds the post_max_size directive in php.ini",
        "FILE_SIZE_BIG" => "The specified file is larger than the max supported file size : %d MB",
        "FILE_TYPE_NOT_ALLOWED" => "File type not allowed",
        "L_AUTHOR_NAME" => "Author Name",
        "L_PUBLISH_DATE_TIME" => "Publish Date & Time",
        "L_FEATURED" => "Featured",
        "L_SELECT_TAG" => "Select Tag(s)",
        "L_TAGS" => "Tag(s)",
        "L_CONTENT" => "Content",
        "L_CATEGORY" => "Category",
        "L_TITLE_MUST_BE_UNIQUE" => "Title must be unique",
        "L_TITLE" => "Title",
        "L_BLOG_INFO" => "Blog Information",
        "L_STATUS" => "Status",
        "BLOG_TITLE" => "Please enter blog title",
        "ERROR_URL" => "Please enter a unique title",
        "MESSAGE_ALPHANUMERIC" => "Please enter valid details",
        "CATEGORY" => "Please select the category",
        "CONTENT" => "Please enter the content",
        "AUTHOR_NAME" => "Please enter the author name",
        "PUBLISH_DATE" => "Please enter publish date",
        "PUBLISH_TIME" => "Please enter publish time",
        "BLOG_CREATE_SUCCESS" => "Blog post created successfully!!",
        "BLOG_UPDATE_SUCCESS" => "Blog post updated successfully!!",
        "BLOG_PUBLISHED_SUCCESS" => "Blog successfully published!!",
        "BLOG_CREATE_ERROR" => "Error in blog post creation",
        "ACTIONS" => "Action(s)",
        "L_SAVED_SEARCHES" => "Saved searches",
        "L_SAVE_SEARCH_AS" => "Save search as",
        "SEARCH_NAME_MSG" => "Please enter search name",
        "SEARCH_UPDATED_MSG" => "Search Updated Successfully",
        "SEARCH_ADDED_MSG" => "Search Added Successfully",
        "CONF_SEARCH_DELETE" => "Are you sure you want to delete this search ?",
        "SEARCH_DELETE_CONFIRM" => "Search is deleted successfully.",
    ),
    'SEARCH_MESSAGES' => array(
        "HEADER_TEXT" => "Search Blogs by filling the following filters, or just do a blank search for the full Blogs list. If you do not find the Blog you seek, you can <a href='/create-crm-blog'>Create New Blog Post</a>",
        "FILTER_BLOG_BY" => "Filter Blog by",
        "PUBLISH_DATE_RANGE" => "Publish Date Range",
        "PUBLISH_DATE" => "Publish Date",
        "FROM" => "From",
        "TO" => "To",
        "STATUS" => "Status",
        "FEATURED" => "Featured",
        "TAG" => "Tag(s)",
        "BLOG_DELETED" => "Blog Deleted Successfully !",
        "SEARCH_RESULTS" => "Search Results",
        "NO_RECORD_FOUND" => "No Record Found",
        "CONF_MSG" => "Are you sure you want to delete this blog ?",
        "SEARCH_TEXT" => "Below are the records that match the search criteria you entered. If you do not find the Blog you seek, try <a href='/create-crm-blog'>Create New Blog Post</a>."
    ),
    'VIEW_MESSAGES' => array(
        "L_BLOG" => "Blogs",
        "L_SEARCH" => "Search",
        "L_CATEGORIES" => "Categories",
        "L_FEATURED_POSTS" => "FEATURED POSTS",
        "L_RECENT_POSTS" => "RECENT POSTS",
        "L_MORE_POSTS" => "More Entries",
        "MESSAGE_ALPHANUMERIC" => "Please enter valid details",
        "BLOG_TITLE" => "Please enter search criteria",
        "SEARCH_RESULTS" => "Search Results",
        "RESULTS_FOUND" => "records found with the term",
        "CATEGORY_FOUND" => "records found with the category",
        "NO_RESULTS_FOUND" => "no records found with the term",
        "READ_MORE" => "READ More",
        "HEADER_TEXT" => "Search Blogs by filling the following filters, or just do a blank search for the full Blogs list. If you do not find the Blog you seek, you can <a href='/create-crm-blog'>Create New Blog Post</a>",
        "FILTER_BLOG_BY" => "Filter Blog by",
        "PUBLISH_DATE_RANGE" => "Publish Date Range",
        "PUBLISH_DATE" => "Publish Date",
        "FROM" => "From",
        "URL_EXIST" => "TITLE available",
        "URL_AVAILABLE" => "TITLE already exist",
        "ERROR_URL" => "Please enter a unique title",
        "TO" => "To",
        "STATUS" => "Status",
        "FEATURED" => "Featured",
        "TAG" => "Tag(s)",
        "BLOG_DELETED" => "Blog Deleted Successfully !",
        "SEARCH_RESULTS" => "Search Results",
        "NO_RECORD_FOUND" => "No Record Found",
        "CONF_MSG" => "Are you sure you want to delete this blog ?",
        "SEARCH_TEXT" => "Below are the records that match the search criteria you entered. If you do not find the Blog you seek, try <a href='/create-crm-blog'>Create New Blog Post .</a>"
    )
);
?>