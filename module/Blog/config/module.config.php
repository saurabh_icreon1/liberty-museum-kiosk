<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'Blog\Controller\Blog' => 'Blog\Controller\BlogController'
        ),
    ),
    'router' => array(
        'routes' => array(
            'getCrmBlogs' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-blogs',
                    'defaults' => array(
                        'controller' => 'Blog\Controller\Blog',
                        'action' => 'getCrmBlogs',
                    ),
                ),
            ),
            'addCrmBlogs' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-crm-blog',
                    'defaults' => array(
                        'controller' => 'Blog\Controller\Blog',
                        'action' => 'addCrmBlog',
                    ),
                ),
            ),
            'editCrmBlogs' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-blog[/:id]',
                    'defaults' => array(
                        'controller' => 'Blog\Controller\Blog',
                        'action' => 'editCrmBlog',
                    ),
                ),
            ),
            'uploadBlogImage' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-blog-image',
                    'defaults' => array(
                         'controller' => 'Blog\Controller\Blog',
                        'action' => 'uploadBlogImage',
                    ),
                ),
            ),
            'getCrmBlogInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-blog-info[/:id]',
                    'defaults' => array(
                        'controller' => 'Blog\Controller\Blog',
                        'action' => 'getCrmBlogInfo',
                    ),
                ),
            ),
            'getCrmBlogsList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/list-blogs',
                    'defaults' => array(
                        'controller' => 'Blog\Controller\Blog',
                        'action' => 'getCrmBlogsList',
                    ),
                ),
            ),
            'crmBlog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-blog[/:id][/:mode]',
                    'defaults' => array(
                        'controller' => 'Blog\Controller\Blog',
                        'action' => 'crmBlog',
                    ),
                ),
            ),
            'deleteCrmBlog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-blog',
                    'defaults' => array(
                        'controller' => 'Blog\Controller\Blog',
                        'action' => 'deleteCrmBlog',
                    ),
                ),
            ),
            'getBlogTitle' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-blog-title',
                    'defaults' => array(
                        'controller' => 'Blog\Controller\Blog',
                        'action' => 'getBlogTitle',
                    ),
                ),
            ),
            'getBlogs' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/blogs',
                    'defaults' => array(
                        'controller' => 'Blog\Controller\Blog',
                        'action' => 'getBlogs',
                    ),
                ),
            ),
            'blogSearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/blog-search',
                    'defaults' => array(
                        'controller' => 'Blog\Controller\Blog',
                        'action' => 'blogSearch',
                    ),
                ),
            ),
            'getBlogInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/blog[/:id]',
                    'defaults' => array(
                        'controller' => 'Blog\Controller\Blog',
                        'action' => 'getBlogInfo',
                    ),
                ),
            ),
            'getBlogList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/blog-list[/:id]',
                    'defaults' => array(
                        'controller' => 'Blog\Controller\Blog',
                        'action' => 'getBlogList',
                    ),
                ),
            ),
            'addCrmSearchBlog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-crm-search-blog',
                    'defaults' => array(
                        'controller' => 'Blog\Controller\Blog',
                        'action' => 'addCrmSearchBlog',
                    ),
                ),
            ),
            'getCrmSearchSavedBlog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-search-saved-blog',
                    'defaults' => array(
                        'controller' => 'Blog\Controller\Blog',
                        'action' => 'getCrmSearchSavedBlog',
                    ),
                ),
            ),
            'getCrmBlogSavedSearchList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-blog-saved-search-list',
                    'defaults' => array(
                        'controller' => 'Blog\Controller\Blog',
                        'action' => 'getBlogSavedSearchList',
                    ),
                ),
            ),
            'deleteCrmSearchBlog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-search-blog',
                    'defaults' => array(
                        'controller' => 'Blog\Controller\Blog',
                        'action' => 'delete-crm-search-blog',
                    ),
                ),
            ),
            'checkblogtitle' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/check-blog-title[/:title]',
                    'defaults' => array(
                        'controller' => 'Blog\Controller\Blog',
                        'action' => 'checkBlogTitle',
                    ),
                ),
            ),
            'blogMoreEntries' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/blog-more-entries[/:month][/:year]',
                    'defaults' => array(
                        'controller' => 'Blog\Controller\Blog',
                        'action' => 'blogMoreEntries',
                    ),
                ),
            ),
            
        )
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    
    'navigation' => array(
        'default' => array(
            array(
                'label' => 'Blogs',
                'route' => 'getCrmBlogs',
                'pages' => array(
                    array(
                        'label' => 'Add Blog',
                        'route' => 'addCrmBlogs',
                        'pages' => array()
                    ),
                    array(
                        'label' => 'View Blog',
                        'route' => 'getCrmBlogInfo',
                        'pages' => array()
                    ),
                     array(
                        'label' => 'Edit Blog',
                        'route' => 'editCrmBlogs',
                        'pages' => array()
                    ),
                    array(
                        'label' => 'Change Log',
                        'route' => 'getblogchangelog',
                        'pages' => array()
                    ),
                    array(
                        'label' => 'Search',
                        'route' => 'getCrmBlogs',
                        'pages' => array(),
                    )
                ),
            ),
            
        ),
    ),
    
    'Blog_messages' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'blog' => __DIR__ . '/../view'
        ),
        'template_map' => array(
            'layout/custom' => __DIR__ . '/../../module/Common/layout/crm.phtml',
            'error/404' => __DIR__ . '/../../module/Common/error/404.phtml',
            'error/index' => __DIR__ . '/../../module/Common/error/index.phtml',
        ),
    ),
    'global_config' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
);
