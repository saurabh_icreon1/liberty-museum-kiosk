<?php

/**
 * This model is used for user validation.
 * @category   Zend
 * @package    Cases_Case
 * @version    2.2
 * @author     Icreon Tech - DT
 */

namespace Pledge\Model;

use Pledge\Module;
// Add these import statements
use Base\Model\BaseModel;
use Zend\Db\Adapter\Adapter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Pledge extends BaseModel {

    protected $inputFilter;
    protected $adapter;

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    /**
     * This method is used to convert form post array of create case to Object of class type Cases
     * @param code
     * @return Void
     * @author Icreon Tech - DT
     */
    public function exchangeArrayCreateCase($data) {
        
    }

    /**
     * Function used to check variables cases
     * @author Icreon Tech - DT
     * @return boolean
     * @param Array
     */
    public function exchangeArray($data) {
        
    }

    /**
     * This method is used to add filtering and validation to the form elements of create pledge
     * @param void
     * @return Array
     * @author Icreon Tech - AG
     */
    public function getInputFilterCreatePledge() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $factory = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                        'name' => 'contact_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'CONTACT_NAME_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'assigned_to',
                        'required' => false
                        
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'pledge_balance_amount',
                        'required' => false
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'payment_due_days',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'PAYMENT_DUE_DATE_EMPTY',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'VALID_NUMBER'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'currency_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'contact_name_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'assigned_to_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'campaign_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'pledge_amount',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'PLEDGE_AMOUNT_EMPTY',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9 .,]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'VALID_AMOUNT'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'payment_start_date',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'PAYMENT_START_DATE_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'pledge_made_date',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'PLEDGE_MADE_DATE_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'acknowledgment_date',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'contribution_type_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'campaign_name',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'num_installment',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'NUM_INSTALLMENT_EMPTY',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'VALID_NUMBER'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'installment_amount',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'INSTALLMENT_AMOUNT_EMPTY',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9 ,]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'VALID_AMOUNT'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'installment_in',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'INSTALLMENT_PERIOD_EMPTY',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'VALID_NUMBER'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'installment_type_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'INSTALLMENT_CYCLE_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'honoree_name',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'HONOREE_NAME_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            /*
              $inputFilter->add($factory->createInput(array(
              'name' => 'reminder_days',
              'required' => false,
              'filters' => array(
              array('name' => 'StripTags'),
              array('name' => 'StringTrim'),
              ),
              'validators' => array(
              array(
              'name' => 'NotEmpty',
              'options' => array(
              'messages' => array(
              'isEmpty' => 'REMINDER_DAYS_EMPTY',
              ),
              ),
              ),
              array(
              'name' => 'regex', false,
              'options' => array(
              'pattern' => '/^[0-9]+$/',
              'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'VALID_NUMBER'
              ),
              ),
              ),
              ),
              )));

              $inputFilter->add($factory->createInput(array(
              'name' => 'num_reminder',
              'required' => false,
              'filters' => array(
              array('name' => 'StripTags'),
              array('name' => 'StringTrim'),
              ),
              'validators' => array(
              array(
              'name' => 'NotEmpty',
              'options' => array(
              'messages' => array(
              'isEmpty' => 'NUMBER_REMINDER_EMPTY',
              ),
              ),
              ),
              array(
              'name' => 'regex', false,
              'options' => array(
              'pattern' => '/^[0-9]+$/',
              'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'VALID_NUMBER'
              ),
              ),
              ),
              ),
              )));


              $inputFilter->add($factory->createInput(array(
              'name' => 'additional_reminder_days',
              'required' => false,
              'filters' => array(
              array('name' => 'StripTags'),
              array('name' => 'StringTrim'),
              ),
              'validators' => array(
              array(
              'name' => 'NotEmpty',
              'options' => array(
              'messages' => array(
              'isEmpty' => 'ADDITIONAL_REMINDER_DAYS_EMPTY',
              ),
              ),
              ),
              array(
              'name' => 'regex', false,
              'options' => array(
              'pattern' => '/^[0-9]+$/',
              'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'VALID_NUMBER'
              ),
              ),
              ),
              ),
              ))); */
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    /**
     * This method is used to call add pledge procedure parameter
     * @param code
     * @return Void
     * @author Icreon Tech - AG
     */
    public function getCreatePledgeArr($data) {
        $returnArr = array();
        //$returnArr[]=(isset($data['pledge_id']) && !empty($data['pledge_id']))?$data['pledge_id']:'';
        $returnArr[] = (isset($data['contact_name_id']) && !empty($data['contact_name_id'])) ? $data['contact_name_id'] : '';
        $returnArr[] = (isset($data['assigned_to_id']) && !empty($data['assigned_to_id'])) ? $data['assigned_to_id'] : '';
        $returnArr[] = (isset($data['currency_id']) && !empty($data['currency_id'])) ? $data['currency_id'] : '';
        $returnArr[] = (isset($data['pledge_amount']) && !empty($data['pledge_amount'])) ? $data['pledge_amount'] : '';
        $returnArr[] = (isset($data['num_installment']) && !empty($data['num_installment'])) ? $data['num_installment'] : '';
        $returnArr[] = (isset($data['installment_amount']) && !empty($data['installment_amount'])) ? $data['installment_amount'] : '';
        $returnArr[] = (isset($data['installment_in']) && !empty($data['installment_in'])) ? $data['installment_in'] : '';
        $returnArr[] = (isset($data['installment_type_id']) && !empty($data['installment_type_id'])) ? $data['installment_type_id'] : '';
        $returnArr[] = (isset($data['payment_due_days']) && !empty($data['payment_due_days'])) ? $data['payment_due_days'] : '';
        if (isset($data['pledge_made_date']) && !empty($data['pledge_made_date'])) {
            $returnArr[] = date('Y-m-d', strtotime($data['pledge_made_date']));
        } else {
            $returnArr[] = '';
        }
        if (isset($data['payment_start_date']) && !empty($data['payment_start_date'])) {
            $returnArr[] = date('Y-m-d', strtotime($data['payment_start_date']));
        } else {
            $returnArr[] = '';
        }
        if (isset($data['acknowledgment_date']) && !empty($data['acknowledgment_date'])) {
            $returnArr[] = date('Y-m-d', strtotime($data['acknowledgment_date']));
        } else {
            $returnArr[] = '';
        }
        $returnArr[] = (isset($data['contribution_type_id']) && !empty($data['contribution_type_id'])) ? $data['contribution_type_id'] : '';
        $returnArr[] = (isset($data['campaign_id']) && !empty($data['campaign_id'])) ? $data['campaign_id'] : '';
        $returnArr[] = (isset($data['in_honoree']) && !empty($data['in_honoree'])) ? $data['in_honoree'] : '';
        $returnArr[] = (isset($data['honoree_name']) && !empty($data['honoree_name'])) ? $data['honoree_name'] : '';
        $returnArr[] = (isset($data['is_reminder']) && !empty($data['is_reminder'])) ? $data['is_reminder'] : '';
        $returnArr[] = (isset($data['staff_reminder']) && !empty($data['staff_reminder'])) ? $data['staff_reminder'] : '';
        $returnArr[] = (isset($data['contact_reminder']) && !empty($data['contact_reminder'])) ? $data['contact_reminder'] : '';
        $returnArr[] = (isset($data['reminder_days']) && !empty($data['reminder_days'])) ? $data['reminder_days'] : '';
        $returnArr[] = (isset($data['num_reminder']) && !empty($data['num_reminder'])) ? $data['num_reminder'] : '';
        $returnArr[] = (isset($data['additional_reminder_days']) && !empty($data['additional_reminder_days'])) ? $data['additional_reminder_days'] : '';
        $returnArr[] = (isset($data['pledge_status_id']) && !empty($data['pledge_status_id'])) ? $data['pledge_status_id'] : '';

        $returnArr[] = (isset($data['added_by']) && !empty($data['added_by'])) ? $data['added_by'] : '';
        $returnArr[] = (isset($data['added_date']) && !empty($data['added_date'])) ? $data['added_date'] : '';

        $returnArr[] = (isset($data['modify_by']) && !empty($data['modify_by'])) ? $data['modify_by'] : '';
        $returnArr[] = (isset($data['modify_date']) && !empty($data['modify_date'])) ? $data['modify_date'] : '';


        //asd($returnArr);
        return $returnArr;
    }

    /**
     * This method is used to call add pledge procedure parameter
     * @param code
     * @return Void
     * @author Icreon Tech - AG
     */
    public function getUpdatePledgeArr($data) {
        $returnArr = array();
        $returnArr[] = (isset($data['pledge_id']) && !empty($data['pledge_id'])) ? $data['pledge_id'] : '';
        $returnArr[] = (isset($data['contact_name_id']) && !empty($data['contact_name_id'])) ? $data['contact_name_id'] : '';
        $returnArr[] = (isset($data['assigned_to_id']) && !empty($data['assigned_to_id'])) ? $data['assigned_to_id'] : '';
        $returnArr[] = (isset($data['currency_id']) && !empty($data['currency_id'])) ? $data['currency_id'] : '';
        $returnArr[] = (isset($data['pledge_amount']) && !empty($data['pledge_amount'])) ? $data['pledge_amount'] : '';
        $returnArr[] = (isset($data['num_installment']) && !empty($data['num_installment'])) ? $data['num_installment'] : '';
        $returnArr[] = (isset($data['installment_amount']) && !empty($data['installment_amount'])) ? $data['installment_amount'] : '';
        $returnArr[] = (isset($data['installment_in']) && !empty($data['installment_in'])) ? $data['installment_in'] : '';
        $returnArr[] = (isset($data['installment_type_id']) && !empty($data['installment_type_id'])) ? $data['installment_type_id'] : '';
        $returnArr[] = (isset($data['payment_due_days']) && !empty($data['payment_due_days'])) ? $data['payment_due_days'] : '';
        if (isset($data['pledge_made_date']) && !empty($data['pledge_made_date'])) {
            $returnArr[] = date('Y-m-d', strtotime($data['pledge_made_date']));
        } else {
            $returnArr[] = '';
        }
        if (isset($data['payment_start_date']) && !empty($data['payment_start_date'])) {
            $returnArr[] = date('Y-m-d', strtotime($data['payment_start_date']));
        } else {
            $returnArr[] = '';
        }
        if (isset($data['acknowledgment_date']) && !empty($data['acknowledgment_date'])) {
            $returnArr[] = date('Y-m-d', strtotime($data['acknowledgment_date']));
        } else {
            $returnArr[] = '';
        }
        $returnArr[] = (isset($data['contribution_type_id']) && !empty($data['contribution_type_id'])) ? $data['contribution_type_id'] : '';
        $returnArr[] = (isset($data['campaign_id']) && !empty($data['campaign_id'])) ? $data['campaign_id'] : '';
        $returnArr[] = (isset($data['in_honoree']) && !empty($data['in_honoree'])) ? $data['in_honoree'] : '';
        $returnArr[] = (isset($data['honoree_name']) && !empty($data['honoree_name'])) ? $data['honoree_name'] : '';
        $returnArr[] = (isset($data['is_reminder']) && !empty($data['is_reminder'])) ? $data['is_reminder'] : '';
        $returnArr[] = (isset($data['staff_reminder']) && !empty($data['staff_reminder'])) ? $data['staff_reminder'] : '';
        $returnArr[] = (isset($data['contact_reminder']) && !empty($data['contact_reminder'])) ? $data['contact_reminder'] : '';
        $returnArr[] = (isset($data['reminder_days']) && !empty($data['reminder_days'])) ? $data['reminder_days'] : '';
        $returnArr[] = (isset($data['num_reminder']) && !empty($data['num_reminder'])) ? $data['num_reminder'] : '';
        $returnArr[] = (isset($data['additional_reminder_days']) && !empty($data['additional_reminder_days'])) ? $data['additional_reminder_days'] : '';
        $returnArr[] = (isset($data['pledge_status_id']) && !empty($data['pledge_status_id'])) ? $data['pledge_status_id'] : '';
        $returnArr[] = (isset($data['modify_by']) && !empty($data['modify_by'])) ? $data['modify_by'] : '';
        $returnArr[] = (isset($data['modify_date']) && !empty($data['modify_date'])) ? $data['modify_date'] : '';


        //asd($returnArr);
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for get the pledge
     * @param code
     * @return Void
     * @author Icreon Tech - AG
     */
    public function getPledgeSearchArr($dataArr=array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['contact_name']) && $dataArr['contact_name'] != '') ? $dataArr['contact_name'] : '';
        $returnArr[] = (isset($dataArr['pledge_id']) && $dataArr['pledge_id'] != '') ? $dataArr['pledge_id'] : '';
        $returnArr[] = (isset($dataArr['payment_scheduled']) && $dataArr['payment_scheduled'] != '') ? $dataArr['payment_scheduled'] : '';
        $returnArr[] = (isset($dataArr['pledge_amount_from']) && $dataArr['pledge_amount_from'] != '') ? $dataArr['pledge_amount_from'] : '';
        $returnArr[] = (isset($dataArr['pledge_amount_to']) && $dataArr['pledge_amount_to'] != '') ? $dataArr['pledge_amount_to'] : '';
        $returnArr[] = (isset($dataArr['pledge_status_id']) && $dataArr['pledge_status_id'] != '' && $dataArr['pledge_status_id'] != '0') ? $dataArr['pledge_status_id'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $returnArr[] = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $returnArr[] = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        $returnArr[] = (isset($dataArr['payment_schedule_id']) && $dataArr['payment_schedule_id'] != '') ? $dataArr['payment_schedule_id'] : '';
		$returnArr[] = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '';

        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for get the pledge's transactions
     * @param code
     * @return Void
     * @author Icreon Tech - AG
     */
    public function getPledgeTransactionsSearchArr($dataArr=array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['pledge_id']) && $dataArr['pledge_id'] != '') ? $dataArr['pledge_id'] : '';
        $returnArr[] = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : 'due_date';
        $returnArr[] = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : 'ASC';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : 0;
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        return $returnArr;
    }

    /**
     * This method is used to convert form post array of create case to Object of class type Cases
     * @param code
     * @return Void
     * @author Icreon Tech - DT
     */
    public function exchangeArrayLead($data) {
        $parameter = array();
        $parameter['contact_name'] = (isset($data['contact_name']) && !empty($data['contact_name'])) ? $data['contact_name'] : '';
        $parameter['contact_name_id'] = (isset($data['contact_name_id']) && !empty($data['contact_name_id'])) ? $data['contact_name_id'] : '';
        $parameter['assigned_to'] = (isset($data['assigned_to']) && !empty($data['assigned_to'])) ? $data['assigned_to'] : '';
        $parameter['assigned_to_id'] = (isset($data['assigned_to_id']) && !empty($data['assigned_to_id'])) ? $data['assigned_to_id'] : '';
        $parameter['title'] = (isset($data['title']) && !empty($data['title'])) ? $data['title'] : '';
        $parameter['first_name'] = (isset($data['first_name']) && !empty($data['first_name'])) ? $data['first_name'] : '';
        $parameter['middle_name'] = (isset($data['middle_name']) && !empty($data['middle_name'])) ? $data['middle_name'] : '';
        $parameter['last_name'] = (isset($data['last_name']) && !empty($data['last_name'])) ? $data['last_name'] : '';
        $parameter['suffix'] = (isset($data['suffix']) && !empty($data['suffix'])) ? $data['suffix'] : '';
        $parameter['email_address'] = (isset($data['email_address']) && !empty($data['email_address'])) ? $data['email_address'] : '';
        $parameter['opportunity_amount'] = (isset($data['opportunity_amount']) && !empty($data['opportunity_amount'])) ? $data['opportunity_amount'] : '';
        $parameter['lead_type'] = (isset($data['lead_type']) && !empty($data['lead_type'])) ? $data['lead_type'] : '';
        $parameter['lead_source'] = (isset($data['lead_source']) && !empty($data['lead_source'])) ? $data['lead_source'] : '';
        $parameter['case_contact_id'] = (isset($data['case_contact_id']) && !empty($data['case_contact_id'])) ? $data['case_contact_id'] : '';
        $parameter['status'] = (isset($data['status']) && $data['status'] != "") ? $data['status'] : '';
        return $parameter;
    }


    

     /**
     * This method is used to return the stored procedure array for get the pledge's transactions
     * @param code
     * @return Void
     * @author Icreon Tech - AG
     */
    public function updatePledgeTransactionStatusArr($dataArr=array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['pledge_transaction_id']) && $dataArr['pledge_transaction_id'] != '') ? $dataArr['pledge_transaction_id'] : '';
        $returnArr[] = (isset($dataArr['status']) && $dataArr['status'] != '') ? $dataArr['status'] : '0';
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $returnArr[] = (isset($dataArr['modify_by']) && $dataArr['modify_by'] != '') ? $dataArr['modify_by'] : '';
        $returnArr[] = (isset($dataArr['modify_date']) && $dataArr['modify_date'] != '') ? $dataArr['modify_date'] : '';
        return $returnArr;
    }
    
    /**
     * This method is used to return the stored procedure array for update pledge transaction amount
     * @param code
     * @return Void
     * @author Icreon Tech - AG
     */
    public function updatePledgeTransactionAmountArr($dataArr=array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['pledge_transaction_id']) && $dataArr['pledge_transaction_id'] != '') ? $dataArr['pledge_transaction_id'] : '';
        $returnArr[] = (isset($dataArr['amount_pledge']) && $dataArr['amount_pledge'] != '') ? $dataArr['amount_pledge'] : '';
        $returnArr[] = (isset($dataArr['remaining_amount_pledge']) && $dataArr['remaining_amount_pledge'] != '') ? $dataArr['remaining_amount_pledge'] : '';
        $returnArr[] = (isset($dataArr['status']) && $dataArr['status'] != '') ? $dataArr['status'] : '';
        $returnArr[] = (isset($dataArr['transaction_id']) && $dataArr['transaction_id'] != '') ? $dataArr['transaction_id'] : '';
        $returnArr[] = (isset($dataArr['modify_by']) && $dataArr['modify_by'] != '') ? $dataArr['modify_by'] : '';
        $returnArr[] = (isset($dataArr['modify_date']) && $dataArr['modify_date'] != '') ? $dataArr['modify_date'] : '';
        return $returnArr;
    }



    

    /**
     * This method is used to return the stored procedure array for saved search
     * @param Array
     * @return Array
     * @author Icreon Tech - AG
     */
    public function getPledgeSavedSearchArr($dataArr=array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['crm_user_id']) && $dataArr['crm_user_id'] != '') ? $dataArr['crm_user_id'] : '';
        $returnArr[] = (isset($dataArr['pledge_search_id']) && $dataArr['pledge_search_id'] != '') ? $dataArr['pledge_search_id'] : '';
        $returnArr[] = (isset($dataArr['is_active'])) ? $dataArr['is_active'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for create pledge
     * @param array
     * @return array
     * @author Icreon Tech - AG
     */
    public function getInputFilterPledgeSearch($filterData) {
        $inputFilterData = array();
        if (is_array($filterData)) {
            foreach ($filterData as $key => $val) {
                $inputFilterData[$key] = trim(addslashes(strip_tags($val)));
            }
        } else {
            $inputFilterData = trim(addslashes(strip_tags($filterData)));
        }
        return $inputFilterData;
    }

    /**
     * This method is used to return the stored procedure array for create search result
     * @param array
     * @return array
     * @author Icreon Tech - AG
     */
    public function getAddPledgeSearchArr($dataArr=array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['crm_user_id']) && $dataArr['crm_user_id'] != '') ? $dataArr['crm_user_id'] : '';
        $returnArr[] = (isset($dataArr['title']) && $dataArr['title'] != '') ? $dataArr['title'] : '';
        $returnArr[] = (isset($dataArr['search_query']) && $dataArr['search_query'] != '') ? $dataArr['search_query'] : '';
        $returnArr[] = (isset($dataArr['is_active']) && $dataArr['is_active'] != '') ? $dataArr['is_active'] : '';
        $returnArr[] = (isset($dataArr['is_delete']) && $dataArr['is_delete'] != '') ? $dataArr['is_delete'] : '';
        if (isset($dataArr['added_date'])) {
            $returnArr[] = $dataArr['added_date'];
        } else {
            $returnArr[] = $dataArr['modified_date'];
        }
        if (isset($dataArr['pledge_search_id']) && $dataArr['pledge_search_id'] != '') {
            $returnArr[] = $dataArr['pledge_search_id'];
        }
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for delete saved search
     * @param Array
     * @return Array
     * @author Icreon Tech - AG
     */
    public function getDeletePledgeSavedSearchArr($dataArr=array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['is_deleted']) && $dataArr['is_deleted'] != '') ? $dataArr['is_deleted'] : '';
        $returnArr[] = (isset($dataArr['modified_date']) && $dataArr['modified_date'] != '') ? $dataArr['modified_date'] : '';
        $returnArr[] = (isset($dataArr['pledge_search_id']) && $dataArr['pledge_search_id'] != '') ? $dataArr['pledge_search_id'] : '';
        return $returnArr;
    }

    /**
     * Function used to get array of elements need to call remove group
     * @param Array
     * @return Array
     * @author Icreon Tech - AG
     */
    public function getArrayForRemovePledge($dataArr) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['pledgeId']) && $dataArr['pledgeId'] != '') ? $dataArr['pledgeId'] : '';
        return $returnArr;
    }

    /**
     * This method is used to return the stored procedure array for insert the pledge's transactions
     * @param array
     * @return array
     * @author Icreon Tech -ST
     */
    public function exchangeTransArr($dataArr=array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['pledgeId']) && $dataArr['pledgeId'] != '') ? $dataArr['pledgeId'] : '0';
        $returnArr[] = (isset($dataArr['dueDate']) && $dataArr['dueDate'] != '') ? $dataArr['dueDate'] : '';
        $returnArr[] = (isset($dataArr['amountPledge']) && $dataArr['amountPledge'] != '') ? $dataArr['amountPledge'] : '';
        $returnArr[] = (isset($dataArr['remainingAmount']) && $dataArr['remainingAmount'] != '') ? $dataArr['remainingAmount'] : '';
        $returnArr[] = (isset($dataArr['status']) && $dataArr['status'] != '') ? $dataArr['status'] : 0;
        $returnArr[] = (isset($dataArr['addedBy']) && $dataArr['addedBy'] != '') ? $dataArr['addedBy'] : '';
        $returnArr[] = (isset($dataArr['addedDate']) && $dataArr['addedDate'] != '') ? $dataArr['addedDate'] : '';
        $returnArr[] = (isset($dataArr['modifiedDate']) && $dataArr['modifiedDate'] != '') ? $dataArr['modifiedDate'] : '';
        return $returnArr;
    }

    /**
     * This method is used to add filtering and validation to the form elements of make pledge payment
     * @param void
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getInputFilterSavePledgeTransaction() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'account_number',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'ACCOUNT_NUMBER_EMPTY',
                                    ),
                                ),
                            )
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'check_number',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[0-9]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'CHECK_NUMBER_VALID'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'CHECK_NUMBER_EMPTY',
                                    ),
                                ),
                            ),
                            
                        ),
                    )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

     /**
     * This method is used to return the stored procedure array for change pledge status
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getUpdatePledgeStatusArr($dataArr=array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['pledgeId']) && $dataArr['pledgeId'] != '') ? $dataArr['pledgeId'] : '';
        $returnArr[] = (isset($dataArr['pledgeStatus']) && $dataArr['pledgeStatus'] != '') ? $dataArr['pledgeStatus'] : '';
        $returnArr[] = (isset($dataArr['modify_by']) && $dataArr['modify_by'] != '') ? $dataArr['modify_by'] : '';
        $returnArr[] = (isset($dataArr['modify_date']) && $dataArr['modify_date'] != '') ? $dataArr['modify_date'] : '';
        return $returnArr;
    }
    
    /**
     * This method is used to return the stored procedure array for check active pledge for user
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getCheckUserActivePledgeArr($dataArr=array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['pledgeId']) && $dataArr['pledgeId'] != '') ? $dataArr['pledgeId'] : '';
        $returnArr[] = (isset($dataArr['userId']) && $dataArr['userId'] != '') ? $dataArr['userId'] : '';
        return $returnArr;
    }



}