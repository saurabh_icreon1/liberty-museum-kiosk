<?php

/**
 * This model is used for user.
 * @category   Zend
 * @package    Cases_Case
 * @version    2.2
 * @author     Icreon Tech - DT
 */

namespace Pledge\Model;

use Zend\Db\TableGateway\TableGateway;

class PledgeTable {

    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
    }

    /**
     * Function for to get conatct list
     * @author Icreon Tech - DT
     * @return Arr
     * @param Array
     */
    public function getContact($dataArr = null) {
        
        $dataArr['membership_cards_year'] = (isset($dataArr['membership_cards_year']) and trim($dataArr['membership_cards_year']) != "") ? trim($dataArr['membership_cards_year']) : "";
        $dataArr['membership_cards_package_shipped'] = (isset($dataArr['membership_cards_package_shipped']) and trim($dataArr['membership_cards_package_shipped']) != "") ? trim($dataArr['membership_cards_package_shipped']) : "";

        $procquesmarkapp = $this->appendQuestionMars(count($dataArr));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_usr_getContacts(" . $procquesmarkapp . ")");
        $this->bindFieldArray($stmt, $dataArr);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for bind field for call to stored procedure
     * @author Icreon Tech - DT
     * @return Void
     * @param object stm,array fields_value_arr
     */
    public function bindFieldArray($stmt, $fields_value_arr) {
        $i = 1;
        foreach ($fields_value_arr as $field_value) {
            $stmt->getResource()->bindParam($i, $field_value);
            $i++;
        }
    }

    /**
     * Function for append number of questions marks for stored procedure
     * @author Icreon Tech - DT
     * @return String
     * @param Array
     */
    public function appendQuestionMars($count) {
        $returnstr = '';
        for ($i = 1; $i <= $count; $i++) {
            $returnstr.='?,';
        }
        return rtrim($returnstr, ',');
    }

    /**
     * Function for insert pledge data
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function savePledge($pledgeData) {

        $procquesmarkapp = $this->appendQuestionMars(count($pledgeData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_ple_insertPledge(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $pledgeData[0]);
        $stmt->getResource()->bindParam(2, $pledgeData[1]);
        $stmt->getResource()->bindParam(3, $pledgeData[2]);
        $stmt->getResource()->bindParam(4, $pledgeData[3]);
        $stmt->getResource()->bindParam(5, $pledgeData[4]);
        $stmt->getResource()->bindParam(6, $pledgeData[5]);
        $stmt->getResource()->bindParam(7, $pledgeData[6]);
        $stmt->getResource()->bindParam(8, $pledgeData[7]);
        $stmt->getResource()->bindParam(9, $pledgeData[8]);
        $stmt->getResource()->bindParam(10, $pledgeData[9]);
        $stmt->getResource()->bindParam(11, $pledgeData[10]);
        $stmt->getResource()->bindParam(12, $pledgeData[11]);
        $stmt->getResource()->bindParam(13, $pledgeData[12]);
        $stmt->getResource()->bindParam(14, $pledgeData[13]);
        $stmt->getResource()->bindParam(15, $pledgeData[14]);
        $stmt->getResource()->bindParam(16, $pledgeData[15]);
        $stmt->getResource()->bindParam(17, $pledgeData[16]);
        $stmt->getResource()->bindParam(18, $pledgeData[17]);
        $stmt->getResource()->bindParam(19, $pledgeData[18]);
        $stmt->getResource()->bindParam(20, $pledgeData[19]);
        $stmt->getResource()->bindParam(21, $pledgeData[20]);
        $stmt->getResource()->bindParam(22, $pledgeData[21]);
        $stmt->getResource()->bindParam(23, $pledgeData[22]);
        $stmt->getResource()->bindParam(24, $pledgeData[23]);
        $stmt->getResource()->bindParam(25, $pledgeData[24]);
        $stmt->getResource()->bindParam(26, $pledgeData[25]);
        $stmt->getResource()->bindParam(27, $pledgeData[26]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for get pledge data
     * @author Icreon Tech - AG
     * @return Int
     * @param Array
     */
    public function getAllPledge($pledgeData) {

        $procquesmarkapp = $this->appendQuestionMars(count($pledgeData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_ple_getPledges(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, (isset($pledgeData[0]) and trim($pledgeData[0]) != "")?addslashes($pledgeData[0]):"");
        $stmt->getResource()->bindParam(2, $pledgeData[1]);
        $stmt->getResource()->bindParam(3, $pledgeData[2]);
        $stmt->getResource()->bindParam(4, $pledgeData[3]);
        $stmt->getResource()->bindParam(5, $pledgeData[4]);
        $stmt->getResource()->bindParam(6, $pledgeData[5]);
        $stmt->getResource()->bindParam(7, $pledgeData[6]);
        $stmt->getResource()->bindParam(8, $pledgeData[7]);
        $stmt->getResource()->bindParam(9, $pledgeData[8]);
        $stmt->getResource()->bindParam(10, $pledgeData[9]);
        $stmt->getResource()->bindParam(11, $pledgeData[10]);
        $stmt->getResource()->bindParam(12, $pledgeData[11]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * This function is used to get the saved search listing for pledge search
     * @param this will be an array.
     * @return this will return a title of the saved search
     * @author Icreon Tech -AG
     */
    public function getPledgeSavedSearch($pledgeData = array()) {
        $procquesmarkapp = $this->appendQuestionMars(count($pledgeData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_cas_getPledgeSavedSearch(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $pledgeData[0]);
        $stmt->getResource()->bindParam(2, $pledgeData[1]);
        $stmt->getResource()->bindParam(3, $pledgeData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
    }

    /**
     * This function will save the search title into the table for pledge search
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech - AG
     */
    public function savePledgeSearch($pledgeData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($pledgeData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_ple_insertCrmSearchPledge(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $pledgeData[0]);
            $stmt->getResource()->bindParam(2, $pledgeData[1]);
            $stmt->getResource()->bindParam(3, $pledgeData[2]);
            $stmt->getResource()->bindParam(4, $pledgeData[3]);
            $stmt->getResource()->bindParam(5, $pledgeData[4]);
            $stmt->getResource()->bindParam(6, $pledgeData[5]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update the search title into the table for pledge search
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech - AG
     */
    public function updatePledgeSearch($pledgeData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($pledgeData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_ple_updateCrmSearchPledge(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $pledgeData[0]);
            $stmt->getResource()->bindParam(2, $pledgeData[1]);
            $stmt->getResource()->bindParam(3, $pledgeData[2]);
            $stmt->getResource()->bindParam(4, $pledgeData[3]);
            $stmt->getResource()->bindParam(5, $pledgeData[4]);
            $stmt->getResource()->bindParam(6, $pledgeData[5]);
            $stmt->getResource()->bindParam(7, $pledgeData[6]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will delete the saved pledge search titles
     * @param this will be an array having the params.
     * @return this will return a confirmation
     * @author Icreon Tech - AG
     */
    public function deleteSavedSearch($pledgeData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($pledgeData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_ple_deleteCrmSearchPledge(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $pledgeData[0]);
            $stmt->getResource()->bindParam(2, $pledgeData[1]);
            $stmt->getResource()->bindParam(3, $pledgeData[2]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for remove pledge 
     * @author Icreon Tech - AG
     * @return Void
     * @param Array
     */
    public function removePledgeById($pledgeData) {
        $procquesmarkapp = $this->appendQuestionMars(count($pledgeData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_ple_deletePledge(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $pledgeData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for get pledge data on the base of id
     * @author Icreon Tech - AG
     * @return Int
     * @param Array
     */
    public function getPledgeById($pledgeData) {
        $procquesmarkapp = $this->appendQuestionMars(count($pledgeData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_ple_getPledgeinfo(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $pledgeData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet[0];
        else
            return false;
    }

    /**
     * Function for Update pledge data
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function updatePledge($pledgeData) {
        $procquesmarkapp = $this->appendQuestionMars(count($pledgeData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_ple_updatePledge(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $pledgeData[0]);
        $stmt->getResource()->bindParam(2, $pledgeData[1]);
        $stmt->getResource()->bindParam(3, $pledgeData[2]);
        $stmt->getResource()->bindParam(4, $pledgeData[3]);
        $stmt->getResource()->bindParam(5, $pledgeData[4]);
        $stmt->getResource()->bindParam(6, $pledgeData[5]);
        $stmt->getResource()->bindParam(7, $pledgeData[6]);
        $stmt->getResource()->bindParam(8, $pledgeData[7]);
        $stmt->getResource()->bindParam(9, $pledgeData[8]);
        $stmt->getResource()->bindParam(10, $pledgeData[9]);
        $stmt->getResource()->bindParam(11, $pledgeData[10]);
        $stmt->getResource()->bindParam(12, $pledgeData[11]);
        $stmt->getResource()->bindParam(13, $pledgeData[12]);
        $stmt->getResource()->bindParam(14, $pledgeData[13]);
        $stmt->getResource()->bindParam(15, $pledgeData[14]);
        $stmt->getResource()->bindParam(16, $pledgeData[15]);
        $stmt->getResource()->bindParam(17, $pledgeData[16]);
        $stmt->getResource()->bindParam(18, $pledgeData[17]);
        $stmt->getResource()->bindParam(19, $pledgeData[18]);
        $stmt->getResource()->bindParam(20, $pledgeData[19]);
        $stmt->getResource()->bindParam(21, $pledgeData[20]);
        $stmt->getResource()->bindParam(22, $pledgeData[21]);
        $stmt->getResource()->bindParam(23, $pledgeData[22]);
        $stmt->getResource()->bindParam(24, $pledgeData[23]);
        $stmt->getResource()->bindParam(25, $pledgeData[24]);
        $stmt->getResource()->bindParam(26, $pledgeData[25]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        //$resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
    }

    /**
     * Function for get pledge transactions data on the base of id
     * @author Icreon Tech - AG
     * @return Int
     * @param Array
     */
    public function getPledgeTransactions($pledgeData) {

        $procquesmarkapp = $this->appendQuestionMars(count($pledgeData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_ple_getPledgeTransactions(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $pledgeData[0]);
        $stmt->getResource()->bindParam(2, $pledgeData[1]);
        $stmt->getResource()->bindParam(3, $pledgeData[2]);
        $stmt->getResource()->bindParam(4, $pledgeData[3]);
        $stmt->getResource()->bindParam(5, $pledgeData[4]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        //asd($resultSet);
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for insert pledge tranasction data
     * @author Icreon Tech - ST
     * @return Int
     * @param Array
     */
    public function savePledgeTranasction($pledgeTransData) {
        $procquesmarkapp = $this->appendQuestionMars(count($pledgeTransData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_ple_insertPledgeTransaction(?,?,?,?,?,?,?,?)");
        $stmt->getResource()->bindParam(1, $pledgeTransData[0]);
        $stmt->getResource()->bindParam(2, $pledgeTransData[1]);
        $stmt->getResource()->bindParam(3, $pledgeTransData[2]);
        $stmt->getResource()->bindParam(4, $pledgeTransData[3]);
        $stmt->getResource()->bindParam(5, $pledgeTransData[4]);
        $stmt->getResource()->bindParam(6, $pledgeTransData[5]);
        $stmt->getResource()->bindParam(7, $pledgeTransData[6]);
        $stmt->getResource()->bindParam(8, $pledgeTransData[7]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for get pledge transactions data on the base of id
     * @author Icreon Tech - AG
     * @return Int
     * @param Array
     */
    public function getPledgeTransactionsById($pledgeId = '', $pledgeStatus = '', $pledgeTransactionId = '') {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_ple_getPledgeTransactionsById(?,?,?)");
        $stmt->getResource()->bindParam(1, $pledgeId);
        $stmt->getResource()->bindParam(2, $pledgeStatus);
        $stmt->getResource()->bindParam(3, $pledgeTransactionId);

        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        //asd($resultSet);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * This function will delete the saved pledge search titles
     * @param this will be an array having the params.
     * @return this will return a confirmation
     * @author Icreon Tech - AG
     */
    public function deletePledgeTransactions($pledgeId = 0) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_ple_deleteTransactions(?)");
            $stmt->getResource()->bindParam(1, $pledgeId);
            $result = $stmt->execute();
            $statement = $result->getResource();

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function getPledgeScheduleIds($pledgeStartDate = NULL, $pledgeEndDate = NULL) {

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_ple_getTransactionPleId(?,?)");
        $stmt->getResource()->bindParam(1, $pledgeStartDate);
        $stmt->getResource()->bindParam(2, $pledgeEndDate);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get pledge transactions data on the base of id
     * @author Icreon Tech - AG
     * @return Int
     * @param Array
     */
    public function getTotalPledgeTransactions($pledgeId) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_ple_getTotalPledgeTransactions(?)");
        $stmt->getResource()->bindParam(1, $pledgeId);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetch(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * This function will update the pledge transaction status
     * @param array
     * @return this will return a confirmation
     * @author Icreon Tech - DT
     */
    public function updatePledgeTransactionStatus($pledgeData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($pledgeData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_ple_updatePledgeTransactionStatus(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $pledgeData[0]);
            $stmt->getResource()->bindParam(2, $pledgeData[1]);
            $stmt->getResource()->bindParam(3, $pledgeData[2]);
            $stmt->getResource()->bindParam(4, $pledgeData[3]);
            $stmt->getResource()->bindParam(5, $pledgeData[4]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update the pledge transaction amount
     * @param array
     * @return this will return a confirmation
     * @author Icreon Tech - DT
     */
    public function updatePledgeTransactionAmount($pledgeData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($pledgeData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_ple_updatePledgeTransactionAmount(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $pledgeData[0]);
            $stmt->getResource()->bindParam(2, $pledgeData[1]);
            $stmt->getResource()->bindParam(3, $pledgeData[2]);
            $stmt->getResource()->bindParam(4, $pledgeData[3]);
            $stmt->getResource()->bindParam(5, $pledgeData[4]);
            $stmt->getResource()->bindParam(6, $pledgeData[5]);
            $stmt->getResource()->bindParam(7, $pledgeData[6]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update the pledge status
     * @param array
     * @return this will return a confirmation
     * @author Icreon Tech - DT
     */
    public function updatePledgeStatus($pledgeData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($pledgeData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_ple_updatePledgeStatus(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $pledgeData[0]);
            $stmt->getResource()->bindParam(2, $pledgeData[1]);
            $stmt->getResource()->bindParam(3, $pledgeData[2]);
            $stmt->getResource()->bindParam(4, $pledgeData[3]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function getPledgeCount($param) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getPledgeCount(?)");
        $stmt->getResource()->bindParam(1, $param['user_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['totalRecord'];
        } else {
            return false;
        }
    }

    /**
     * This function use for check user already has active pledge or not
     * @param array
     * @return this will return a confirmation
     * @author Icreon Tech - DT
     */
    public function checkUserActivePledge($pledgeData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($pledgeData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_ple_getTotalUserActivePledge(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $pledgeData[0]);
            $stmt->getResource()->bindParam(2, $pledgeData[1]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

}

