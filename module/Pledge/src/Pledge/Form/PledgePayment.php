<?php

/**
 * This form is used for pledge payment.
 * @category   Zend
 * @package    Pledge_PledgePayment
 * @author     Icreon Tech - DT
 */

namespace Pledge\Form;

use Zend\Form\Form;

class PledgePayment extends Form {

    public function __construct($name = null) {
        parent::__construct('pledge_transaction');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'pledge_transaction_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'pledge_transaction_id',
				
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'paid_by',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'paid_by',
                'class' => 'e1 select-w-320',
                'onchange' => 'getPaymentDetail(this.value)'
            )
        ));
        $this->add(array(
            'name' => 'account_number',
            'attributes' => array(
                'type' => 'text',
                'id' => 'account_number',

            )
        ));
        $this->add(array(
            'name' => 'check_number',
            'attributes' => array(
                'type' => 'text',
                'id' => 'check_number',

            )
        ));
        $this->add(array(
            'name' => 'pledge_submit',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'pledge_submit',
                'class' => 'save-btn',
                'value' => 'SUBMIT'
            )
        ));
        $this->add(array(
            'name' => 'pledge_cancel',
            'attributes' => array(
                'type' => 'button',
                'id' => 'pledge_cancel',
                'class' => 'save-btn',
                'value' => 'Cancel'
            )
        ));
    }

}

