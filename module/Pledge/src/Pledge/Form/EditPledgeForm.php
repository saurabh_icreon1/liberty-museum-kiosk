<?php

/**
 * This form is used for Edit Pledge.
 * @package    Pledge_EditPledgeForm
 * @author     Icreon Tech - AP
 */
namespace Pledge\Form;
use Zend\Form\Form;

class EditPledgeForm extends Form
{

	public function __construct($name = null) {
		// we want to ignore the name passed
		parent::__construct('edit-pledge');
		$this->setAttribute('method', 'post');
		$this->add(array(
			'name' => 'is_trans_update',
			'attributes' => array(
				'type' => 'hidden',				 
				'id' => 'is_trans_update',
				'value'=>'0'
			)
		));
		$this->add(array(
			'name' => 'contact_name',
			'attributes' => array(
				'type' => 'text',
				'id' => 'contact_name',
				'class' => 'search-icon'
			)
		));
		$this->add(array(
			'name' => 'contact_name_id',
			'attributes' => array(
				'type' => 'hidden',
				'id' => 'contact_name_id'
			)
		));
		$this->add(array(
			'name' => 'assigned_to',
			'attributes' => array(
				'type' => 'text',
				'id' => 'assigned_to',
				'class' => 'search-icon'
			)
		));
		$this->add(array(
			'name' => 'assigned_to_id',
			'attributes' => array(
				'type' => 'hidden',
				'id' => 'assigned_to_id'
			)
		));

		$this->add(array(
			'name' => 'payment_due_days',
			'attributes' => array(
				'type' => 'text',
				'class' => 'width-155',
				'id' => 'payment_due_days',
			)
		));
		$this->add(array(
			'name' => 'payment_due_days_hid',
			'attributes' => array(
				'type' => 'hidden',
				'class' => 'width-155',
				'id' => 'payment_due_days_hid',
			)
		));

		$this->add(array(
			'type' => 'Zend\Form\Element\Select',
			'name' => 'currency_id',
			'options' => array(
				'value_options' => array(
				),
			),
			'attributes' => array(
				'id' => 'currency_id',
				'class' => 'e1 select-w-150'
			)
		));

		$this->add(array(
			'name' => 'pledge_amount',
			'attributes' => array(
				'type' => 'text',
				'id' => 'pledge_amount',
				'class' => 'width-120 m-l-25'
			)
		));

		$this->add(array(
			'name' => 'payment_start_date',
			'attributes' => array(
				'type' => 'text',
				'class' => 'cal-icon',
				'id' => 'payment_start_date'
			)
		));
		$this->add(array(
			'name' => 'pledge_made_date',
			'attributes' => array(
				'type' => 'text',
				'class' => 'cal-icon',
				'id' => 'pledge_made_date'
			)
		));
		$this->add(array(
			'name' => 'acknowledgment_date',
			'attributes' => array(
				'type' => 'text',
				'class' => 'cal-icon',
				'id' => 'acknowledgment_date'
			)
		));

		$this->add(array(
			'type' => 'Zend\Form\Element\Select',
			'name' => 'contribution_type_id',
			'options' => array(
				'value_options' => array(
				),
			),
			'attributes' => array(
				'id' => 'contribution_type_id',
				'class' => 'e1'
			)
		));

		$this->add(array(
			'name' => 'campaign_name',
			'attributes' => array(
				'type' => 'text',
				'id' => 'campaign_name',
				'class' => 'search-icon'
			)
		));
		$this->add(array(
			'name' => 'campaign_id',
			'attributes' => array(
				'type' => 'hidden',
				'id' => 'campaign_id',
			)
		));
		$this->add(array(
			'name' => 'num_installment',
			'attributes' => array(
				'type' => 'text',
				'class' => 'width-97',
				'id' => 'num_installment',
			)
		));
		$this->add(array(
			'name' => 'installment_amount',
			'attributes' => array(
				'type' => 'text',
				'class' => 'width-97',
				'id' => 'installment_amount',
			)
		));
		$this->add(array(
			'name' => 'installment_in',
			'attributes' => array(
				'type' => 'text',
				'class' => 'width-97 m-r-15',
				'id' => 'installment_in',
			)
		));
		$this->add(array(
			'type' => 'Zend\Form\Element\Select',
			'name' => 'installment_type_id',
			'options' => array(
				'value_options' => array(
				),
			),
			'attributes' => array(
				'id' => 'installment_type_id',
				'class' => 'e1 select-w-150 left select2-offscreen'
			)
		));

		$this->add(array(
			'type' => 'Zend\Form\Element\Radio',
			'name' => 'in_honoree',
			'options' => array(
				'value_options' => array(
					'1' => 'In Honor of',
					'2' => 'In Memory of',
					'3' => 'None'
				),
			),
			'attributes' => array(
				'id' => 'in_honoree',
				'value' => '', //set checked to '1'
				'class' => 'e3 honoree',
				'onClick' => 'showHonoreeName(this)'
			)
		));

		$this->add(array(
			'name' => 'honoree_name',
			'attributes' => array(
				'type' => 'text',
				'id' => 'honoree_name',
			)
		));

		$this->add(array(
			'type' => 'Zend\Form\Element\Checkbox',
			'name' => 'is_reminder',
			'checked_value' => '1',
			'unchecked_value' => '0',
			'attributes' => array(
				'class' => 'checkbox',
				'id' => 'is_reminder',
				'class' => 'e2'
			)
		));

		$this->add(array(
			'type' => 'Zend\Form\Element\Checkbox',
			'name' => 'staff_reminder',
			'checked_value' => '1',
			'unchecked_value' => '0',
			'attributes' => array(
				'class' => 'checkbox',
				'id' => 'staff_reminder',
				'class' => 'e2'
			)
		));

		$this->add(array(
			'type' => 'Zend\Form\Element\Checkbox',
			'name' => 'contact_reminder',
			'checked_value' => '1',
			'unchecked_value' => '0',
			'attributes' => array(
				'class' => 'checkbox',
				'id' => 'contact_reminder',
				'class' => 'e2'
			)
		));

		$this->add(array(
			'name' => 'reminder_days',
			'attributes' => array(
				'type' => 'text',
				'id' => 'reminder_days',
			)
		));

		$this->add(array(
			'name' => 'num_reminder',
			'attributes' => array(
				'type' => 'text',
				'id' => 'num_reminder',
			)
		));

		$this->add(array(
			'name' => 'additional_reminder_days',
			'attributes' => array(
				'type' => 'text',
				'id' => 'additional_reminder_days',
			)
		));

		$this->add(array(
			'name' => 'pledge_id',
			'attributes' => array(
				'type' => 'hidden',
				'id' => 'pledge_id',
			)
		));
		
		$this->add(array(
			'name' => 'save',
			'attributes' => array(
				'type' => 'submit',
				'id' => 'update',
				'class' => 'save-btn',
				'value' => 'UPDATE'
			)
		));
		$this->add(array(
			'name' => 'cancel',
			'attributes' => array(
				'type' => 'button',
				'id' => 'cancel',
				'class' => 'cancel-btn m-l-20',
				'value' => 'CANCEL',
				'onclick' => 'window.location.href="/pledges"'
			)
		));
	}

}
