<?php

/**
 * This form is used for create Search pledge.
 * @category   Zend
 * @package    Cases_SearchCaseForm
 * @author     Icreon Tech - AG
 */

namespace Pledge\Form;

use Zend\Form\Form;

class SearchPledgeForm extends Form {

    public function __construct($name = null) {
        parent::__construct('search_pledge');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'contact_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'contact_name',
				
            )
        ));
        $this->add(array(
            'name' => 'pledge_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'pledge_id'
            )
        ));
        $this->add(array(
            'name' => 'pledge_amount_from',
            'attributes' => array(
                'type' => 'text',
                'class'=>'width-108',
                'id' => 'pledge_amount_from'
            )
        ));
        $this->add(array(
            'name' => 'pledge_amount_to',
            'attributes' => array(
                'type' => 'text',
                'class'=>'width-108',
                'id' => 'pledge_amount_to'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'payment_scheduled',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'payment_scheduled',
                'class' => 'e1 select-w-320'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'pledge_status_id',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'pledge_status_id',
                'class' => 'e1 select-w-320'
            )
        ));

        $this->add(array(
            'name' => 'searchpledgebutton',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'searchpledgebutton',
                'class' => 'search-btn',
                'value' => 'SEARCH'
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'saved_search',
            'options' => array(
                'value_options' => array(

                ),
            ),
            'attributes' => array(
                'value' => '',
                'class' => 'e1 select-w-320',
                'id' => 'saved_search',
                'onChange' =>'getSavedPledgeSearchResult();'
            ),
        ));
    }

}

