<?php

/**
 * This controller is used create pledges
 * @category   Zend
 * @package    Pledges_PledgeController
 * @version    2.2
 * @author     Icreon Tech - DT
 */

namespace Pledge\Controller;

use Base\Controller\BaseController;
use Zend\View\Model\ViewModel;
use Pledge\Form\CreatePledgeForm;
use Pledge\Form\EditPledgeForm;
use Pledge\Form\SearchPledgeForm;
use Pledge\Form\SaveSearchForm;
use User\Form\ContactPhonenoForm;
use User\Form\ContactAddressesForm;
use Pledge\Form\PledgePayment;
use Pledge\Model\Pledge;
use Transaction\Model\Transaction;
use Zend\Http\Request;
use Zend\Authentication\AuthenticationService;

class PledgeController extends BaseController {

    protected $_pledgeTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_flashMessage = null;

    function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        parent::__construct();
    }

    /**
     * This function is used to get table and config file object
     * @return     array
     * @param void
     * @author Icreon Tech - DT
     */
    public function getPledgeTable() {
        $this->checkUserAuthentication();
        if (!$this->_pledgeTable) {
            $sm = $this->getServiceLocator();
            $this->_pledgeTable = $sm->get('Pledge\Model\PledgeTable');
            $this->_translator = $sm->get('translator');
            $this->_config = $sm->get('Config');
            $this->_adapter = $sm->get('dbAdapter');
            $this->_auth = new AuthenticationService();
        }
        return $this->_pledgeTable;
    }

    /**
     * This action is used to create Pledge
     * @return json 
     * @param void
     * @author Icreon Tech - AP
     */
    public function addPledgeAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getPledgeTable();
        $pledge_create_messages = $this->_config['Pledge_messages']['config']['Pledge_create_message'];
        $pledge = new Pledge($this->_adapter);
        $create_pledge_form = new CreatePledgeForm();
        $getCurrencies = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCurrencies();
        $currenciesArray = array();
        if ($getCurrencies !== false) {
            foreach ($getCurrencies as $key => $val) {
                $currenciesArray[$val['currency_id']] = $val['currency_symbol'];
            }
        }
        $create_pledge_form->get('currency_id')->setAttribute('options', $currenciesArray);
        $getContributionTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getContributionTypes();
        $contributionTypesArray = array();
        $contributionTypesDescripton = array();
        $contributionTypesArray[''] = 'Select';
        if ($getContributionTypes !== false) {
            foreach ($getContributionTypes as $key => $val) {
                $contributionTypesArray[$val['contribution_type_id']] = $val['contribution_type'];
                $contributionTypesDescripton[$val['contribution_type_id']] = stripslashes($val['type_description']);
            }
        }
        $create_pledge_form->get('contribution_type_id')->setAttribute('options', $contributionTypesArray);
        $create_pledge_form->get('in_honoree')->setValue('1');
        $getInstallmentTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getInstallmentTypes();
        $InstallmentTypesArray = array();
        $InstallmentTypesArray[''] = 'Select';
        if ($getInstallmentTypes !== false) {
            foreach ($getInstallmentTypes as $key => $val) {
                $InstallmentTypesArray[$val['installment_type_id']] = $val['installment_type'];
            }
        }
        $create_pledge_form->get('installment_type_id')->setAttribute('options', $InstallmentTypesArray);
        $messages = "";
        if ($request->isPost()) {
            $create_pledge_form->setInputFilter($pledge->getInputFilterCreatePledge());
            $create_pledge_form->setData($request->getPost());
            if (!$create_pledge_form->isValid()) {
                $errors = $create_pledge_form->getMessages();

                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && ($key != 'in_honoree' && ($key!='payment_due_days' && $request->getPost('installment_type_id')==1))) {
                        foreach ($row as $type_error => $rower) {
                            if ($type_error != 'notInArray') {
                                $msg [$key] = $pledge_create_messages[$rower];
                            }
                        }
                    }
                }
				if(!empty($msg))
					$messages = array('status' => "error", 'message' => $msg);
            }
            $create_pledge_form_arr = $create_pledge_form->getData();
            //asd($create_pledge_form_arr);
            $postedData = array();
            $postedData['userId'] = $create_pledge_form_arr['contact_name_id'];
            if (!$this->getActivePledgeForUser($postedData)) {
                $msg = array();
                $msg ['contact_name'] = $pledge_create_messages['ALREADY_ACTIVE_PLEDGE'];
                $messages = array('status' => "error", 'message' => $msg);
            }



            if (!empty($messages) && count($messages) > 0) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {

                $pledgeStatusArray = $this->getServiceLocator()->get('Common\Model\CommonTable')->getPledgeStatus();
                $create_pledge_form_arr['pledge_status_id'] = '1';
                $create_pledge_form_arr['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                $create_pledge_form_arr['added_date'] = DATE_TIME_FORMAT;
                $create_pledge_form_arr['modify_by'] = $this->_auth->getIdentity()->crm_user_id;
                $create_pledge_form_arr['modify_date'] = DATE_TIME_FORMAT;
                $create_pledge_arr = $pledge->getCreatePledgeArr($create_pledge_form_arr);

                if ($create_pledge_form_arr['pledge_amount'] != ($create_pledge_form_arr['installment_amount'] * $create_pledge_form_arr['num_installment'])) {
                    $messages = array('status' => "error", 'message' => $pledge_create_messages['PLEDGE_DATA_ERROR']);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                }

                $pledge_id = $this->_pledgeTable->savePledge($create_pledge_arr);

                if ($pledge_id) {
                    $pledgeTransactionArr = array();
                    $pleInstallmentNo = $create_pledge_form_arr['num_installment'];
                    $pleAmount = $create_pledge_form_arr['pledge_amount'];
                    $pleInstallmentAmount = $create_pledge_form_arr['installment_amount'];
                    $pleInstallmentGap = $create_pledge_form_arr['installment_in'];
                    $paymentDueDay = $create_pledge_form_arr['payment_due_days'];
                    $pleInstallmentCycle = $create_pledge_form_arr['installment_type_id'];
                    $nextDueDate = date('Y-m-d', strtotime($create_pledge_form_arr['payment_start_date']));

                    $pledgeTransactionArr['pledgeId'] = $pledge_id;
                    $pledgeTransactionArr['dueDate'] = $nextDueDate;
                    $pledgeTransactionArr['amountPledge'] = $pleInstallmentAmount;
                    $pledgeTransactionArr['remainingAmount'] = ($pleAmount - $pleInstallmentAmount);
                    $pledgeTransactionArr['status'] = '0';
                    $pledgeTransactionArr['addedBy'] = $this->_auth->getIdentity()->crm_user_id;
                    $pledgeTransactionArr['addedDate'] = DATE_TIME_FORMAT;
                    $pledgeTransactionArr['modifiedBy'] = $this->_auth->getIdentity()->crm_user_id;
                    $pledgeTransactionArr['modifiedDate'] = DATE_TIME_FORMAT;

                    $pledgeTransInsertArr = $pledge->exchangeTransArr($pledgeTransactionArr);
                    $pledge_trans_id = $this->_pledgeTable->savePledgeTranasction($pledgeTransInsertArr);

                    for ($i = 0; $i < $pleInstallmentNo - 1; $i++) {
                        $date = strtotime($nextDueDate);
                        switch ($pleInstallmentCycle) {
                            case 1:
                                $intervalVar = "+" . $pleInstallmentGap . " day";
                                $date = strtotime($intervalVar, $date);
                                break;
                            case 2:
                                $intervalVar = "+" . ($pleInstallmentGap) . " week";
                                $date = strtotime($intervalVar, $date);
                                break;
                            case 3:
                                $intervalVar = "+" . $pleInstallmentGap . " month";
                                $date = strtotime($intervalVar, $date);
                                break;
                            default:
                                $intervalVar = "+" . $pleInstallmentGap . " year";
                                $date = strtotime($intervalVar, $date);
                        }

                        if ($i == 0 && $pleInstallmentCycle != 1) {
                            $date = strtotime("+" . $paymentDueDay . " day", $date);
                        }
                        $nextDueDate = date('Y-m-d', $date);
                        $pledgeTransactionArr['dueDate'] = $nextDueDate;
                        $pledgeTransactionArr['remainingAmount'] = ($pledgeTransactionArr['remainingAmount'] - $pleInstallmentAmount);
                        $pledgeTransInsertArr = $pledge->exchangeTransArr($pledgeTransactionArr);

                        $pledge_trans_id = $this->_pledgeTable->savePledgeTranasction($pledgeTransInsertArr);
                    }
                    //Code for Change log entry
                    $changeLogArray = array();
                    $changeLogArray['table_name'] = 'tbl_ple_change_logs';
                    $changeLogArray['activity'] = '1';
                    $changeLogArray['id'] = $pledge_id;
                    $changeLogArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                    $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                    $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                    $this->flashMessenger()->addMessage($pledge_create_messages['PLEDGE_CREATED_SUCCESSFULLY']);
                    $messages = array('status' => "success", 'message' => $pledge_create_messages['PLEDGE_CREATED_SUCCESSFULLY']);
                } else {
                    $this->flashMessenger()->addMessage($pledge_create_messages['PLEDGE_CREATED_ERROR']);
                    $messages = array('status' => "success", 'message' => $pledge_create_messages['PLEDGE_CREATED_ERROR']);
                }

                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $params = $this->params()->fromRoute();
        $contact_id = isset($params['contact_id']) ? $this->decrypt($params['contact_id']) : null;
        if ($contact_id != '') {
            $get_contact_arr = $this->getServiceLocator()->get('User\Model\ContactTable')->getContact(array('user_id' => $contact_id));
            $full_name = '';

            if ($get_contact_arr[0]['first_name'] != '')
                $full_name.= $get_contact_arr[0]['first_name'];
            if ($get_contact_arr[0]['middle_name'] != '')
                $full_name.= " " . $get_contact_arr[0]['middle_name'];
            if ($get_contact_arr[0]['last_name'] != '')
                $full_name.= " " . $get_contact_arr[0]['last_name'];

            if ($full_name == '' && $get_contact_arr[0]['company_name'] != '')
                $full_name = $get_contact_arr[0]['company_name'];

            $create_pledge_form->get('contact_name')->setValue($full_name);
            $create_pledge_form->get('contact_name_id')->setValue($contact_id);
        }

        $this->layout('crm');
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'create_pledge_form' => $create_pledge_form,
            'jsLangTranslate' => $pledge_create_messages,
            'contributionTypesDescripton' => $contributionTypesDescripton
        ));
        return $viewModel;
    }

    /**
     * This action is used to listing of pledges
     * @return json
     * @param void
     * @author Icreon Tech - AG
     */
    public function getPledgesAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $this->getPledgeTable();
        $params = $this->params()->fromRoute();
        $pledge_create_messages = $this->_config['Pledge_messages']['config']['Pledge_create_message'];
        $pledge = new Pledge($this->_adapter);
        $searchPledgeForm = new SearchPledgeForm();
        $savePledgeForm = new SaveSearchForm();
        $transactionDateRange = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTransactionRange();
        $pledgeDateRangeArray = array(1, 2, 3, 4);
        $pledgePaymentRangeArray = array(1 => "All");
        foreach ($transactionDateRange as $key => $val) {
            if (in_array($key, $pledgeDateRangeArray)) {
                $pledgePaymentRangeArray[$val['range_id']] = $val['range'];
            }
        }
        $searchPledgeForm->get('payment_scheduled')->setAttribute('options', $pledgePaymentRangeArray);
        $pledgeAllStatusArray = $this->getServiceLocator()->get('Common\Model\CommonTable')->getPledgeStatus();
        $pledgeStatusArray = array(0 => "All");
        $pledgeStatusDesc = array();
        foreach ($pledgeAllStatusArray as $key => $val) {
            $pledgeStatusArray[$val['pledge_status_id']] = $val['pledge_status'];
            $pledgeStatusDesc[$val['pledge_status_id']] = $val['status_description'];
        }
        $searchPledgeForm->get('pledge_status_id')->setAttribute('options', $pledgeStatusArray);        
        if (isset($params['pledge_status']) && $this->decrypt($params['pledge_status']) == 'due') {
            $searchPledgeForm->get('pledge_status_id')->setValue('5');
        }
        $getSearchArray = array();
        $getSearchArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
        $getSearchArray['is_active'] = '1';
        $pledgeSavedSearchArray = $pledge->getPledgeSavedSearchArr($getSearchArray);
        $pledgeSearchArray = $this->_pledgeTable->getPledgeSavedSearch($pledgeSavedSearchArray);
        $pledgeSearchList = array();
        $pledgeSearchList[''] = 'Select';
        foreach ($pledgeSearchArray as $key => $val) {
            $pledgeSearchList[$val['pledge_search_id']] = stripslashes($val['title']);
        }
        $searchPledgeForm->get('saved_search')->setAttribute('options', $pledgeSearchList);

        if (($request->isXmlHttpRequest() && $request->isPost()) || $request->getPost('export') == 'excel') {
            $response = $this->getResponse();
            $post_array = $request->getPost()->toArray();
            parse_str($request->getPost('searchString'), $searchParam);

            $dashlet = $request->getPost('crm_dashlet');
            $dashletSearchId = $request->getPost('searchId');
            $dashletId = $request->getPost('dashletId');
            $dashletSearchType = $request->getPost('searchType');
            $dashletSearchString = $request->getPost('dashletSearchString');
            if (isset($dashletSearchString) && $dashletSearchString != '') {
                $searchParam = unserialize($dashletSearchString);
            }
            $isExport = $request->getPost('export');
            if ($isExport != "" && $isExport == "excel") {
                $chunksize = $this->_config['export']['chunk_size'];
                $export_limit = $this->_config['export']['export_limit'];

                $searchParam['recordLimit'] = $chunksize;
            }
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit;
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            if (isset($searchParam['payment_scheduled']) && $searchParam['payment_scheduled'] != 1) {
                $paymentScheduledDateRange = $this->getDateRange($searchParam['payment_scheduled']);
                $searchResult = $this->_pledgeTable->getPledgeScheduleIds($this->DateFormat($paymentScheduledDateRange['from'], 'db_date_format'), $this->DateFormat($paymentScheduledDateRange['to'], 'db_date_format'));
                $strIds = "";
                if (count($searchResult) > 0 && is_array($searchResult)) {
                    foreach ($searchResult as $val) {
                        $idsArr[] = $val['pledge_id'];
                    }
                    $strIds = implode(',', $idsArr);
                    $searchParam['payment_schedule_id'] = $strIds;
                }
            }
            $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'modified_date') ? 'tbl_pledge.modified_date' : $searchParam['sort_field'];
            $page_counter = 1;
            do {
                $searchPledgeArr = $pledge->getPledgeSearchArr($searchParam);
                if (isset($strIds)) {
                    if (strlen($strIds)) {
                        $searchResult = $this->_pledgeTable->getAllPledge($searchPledgeArr);
                    } else {
                        $searchResult = array();
                    }
                } else {
                    $searchResult = $this->_pledgeTable->getAllPledge($searchPledgeArr);
                }
                $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
                $dashletResult = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getTableStruct(array('user_id' => $this->_auth->getIdentity()->crm_user_id, 'search_id' => $dashletSearchId, 'dash_id' => $dashletId, 'search_type' => $dashletSearchType));
                $dashletColumnName = array();
                if (!empty($dashletResult)) {
                    $dashletColumn = explode(",", $dashletResult[0]['table_column_names']);
                    foreach ($dashletColumn as $val) {
                        if (strpos($val, ".")) {
                            $dashletColumnName[] = trim(strstr($val, ".", false), ".");
                        } else {
                            $dashletColumnName[] = trim($val);
                        }
                    }
                }
                $jsonResult = array();
                $arrCell = array();
                $subArrCell = array();
                $total = $countResult[0]->RecordCount;
                if ($isExport == "excel") {

                    /* if ($total > $export_limit) {
                      $totalExportedFiles = ceil($total / $export_limit);
                      $exportzip = 1;
                      }
                     * */

                    if ($total > $chunksize) {
                        $number_of_pages = ceil($total / $chunksize);
                        //echo $number_of_pages .'--'.$page_counter;
                        //echo "<br>";
                        $page_counter++;
                        $start = $chunksize * $page_counter - $chunksize;
                        $searchParam['start_index'] = $start;
                        $searchParam['page'] = $page_counter;
                    } else {
                        $number_of_pages = 1;
                        $page_counter++;
                    }
                } else {
                    $page_counter = $page;
                    $number_of_pages = ceil($total / $limit);
                    $jsonResult['records'] = $countResult[0]->RecordCount;
                    $jsonResult['page'] = $request->getPost('page');
                    $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
                }

                //$jsonResult['page'] = $request->getPost('page');
                //$jsonResult['records'] = $countResult[0]->RecordCount;
                //$jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
                if (!empty($searchResult)) {
                    $arrExport = array();
                    foreach ($searchResult as $val) {
                        $dashletCell = array();
                        $arrCell['id'] = $val['pledge_id'];
                        $caseContactId = '<span class="plus-sign"><a href="/create-activity/' . $this->encrypt($val['user_id']) . '/' . $this->encrypt('3') . '/' . $this->encrypt($val['pledge_id']) . '"><div class="tooltip">Add activity<span></span></div></a></span>';
                        if (false !== $dashletColumnKey = array_search('pledge_id', $dashletColumnName))
                            $dashletCell[$dashletColumnKey] = '<a href="/get-pledge-info/' . $this->encrypt($val['pledge_id']) . '/' . $this->encrypt('view') . '" class="txt-decoration-underline">' . $val['pledge_id'] . '</a>';

                        $encrypt_id = $this->encrypt($val['pledge_id']);
                        $view_link = '<a href="/get-pledge-info/' . $encrypt_id . '/' . $this->encrypt('view') . '" class="view-icon"><div class="tooltip">View<span></span></div></a>';
                        $edit_link = '<a href="/get-pledge-info/' . $encrypt_id . '/' . $this->encrypt('edit') . '" class="edit-icon"><div class="tooltip">Edit<span></span></div></a>';
                        $delete_link = '<a onclick="deletePledge(\'' . $encrypt_id . '\')" href="#delete_pledge_content" class="delete_pledge delete-icon"><div class="tooltip">Delete<span></span></div></a>';
                        $cancel_link = '';
                        if ($val['pledge_status'] != 3 && $val['pledge_status'] != 2) {
                            $cancel_link = '<a onclick="cancelPledgeStatus(\'' . $encrypt_id . '\')" href="#cancel_pledge" class="cancel_pledge_status cancel-icon" style="margin-left:7px!important;"><div class="tooltip">Cancel<span></span></div></a>';
                        }
                        $actions = '<div class="action-col">' . $view_link . $edit_link . $delete_link . $cancel_link . '</div>';
                        $pledgeAddedDate = ($val['added_date'] == 0) ? 'N/A' : $this->OutputDateFormat($val['added_date'], 'dateformatampm');
                        if (false !== $dashletColumnKey = array_search('added_date', $dashletColumnName))
                            $dashletCell[$dashletColumnKey] = $pledgeAddedDate;

                        if ($val['pledge_status'] == 1) {
                            $status = "Pending";
                        }
                        if ($val['pledge_status'] == 2) {
                            $status = "Completed";
                        }
                        if ($val['pledge_status'] == 3) {
                            $status = "Canceled";
                        }
                        if ($val['pledge_status'] == 4) {
                            $status = "In progress";
                        }
                        if ($val['pledge_status'] == 5) {
                            $status = "Overdue";
                        }

                        $balanceAmount = isset($val['BalanceAmount']) ? $val['BalanceAmount'] : '0.00';


                        $fullName = (!empty($val['full_name'])) ? $val['full_name'] : $val['company_name'];
                        if (false !== $dashletColumnKey = array_search('full_name', $dashletColumnName))
                            $dashletCell[$dashletColumnKey] = $fullName;
                        if (false !== $dashletColumnKey = array_search('pledge_for', $dashletColumnName))
                            $dashletCell[$dashletColumnKey] = $val['pledge_for'];
                        if (false !== $dashletColumnKey = array_search('balance', $dashletColumnName))
                            $dashletCell[$dashletColumnKey] = $balanceAmount;
                        if (false !== $dashletColumnKey = array_search('pledge_status_id', $dashletColumnName))
                            $dashletCell[$dashletColumnKey] = $status;
                        if (false !== $dashletColumnKey = array_search('pledge_amount', $dashletColumnName))
                            $dashletCell[$dashletColumnKey] = "$ ".$val['pledge_amount'];
                        
                        if (isset($dashlet) && $dashlet == 'dashlet') {
                            $arrCell['cell'] = $dashletCell;
                        } else if ($isExport == "excel") {
                            $arrExport[] = array('Pledge' => $val['pledge_id'], 'Name' => $fullName, 'Total Amount' => "$ ".$val['pledge_amount'], 'Balance Amount' => $balanceAmount, 'Pledge For' => $val['pledge_for'], 'Date' => $pledgeAddedDate, 'Status' => $status);
                        } else {
                            $arrCell['cell'] = array($caseContactId . $val['pledge_id'], $fullName, $val['pledge_amount'], $balanceAmount, $val['pledge_for'], $pledgeAddedDate, $status, $actions);
                        }


                        $subArrCell[] = $arrCell;
                        $jsonResult['rows'] = $subArrCell;
                    }
                    if ($isExport == "excel") {
                        $filename = "pledge_export_" . date("Y-m-d") . ".csv";
                        $this->arrayToCsv($arrExport, $filename, $page_counter);
                    }
                } else {
                    $jsonResult['rows'] = array();
                }
            } while ($page_counter <= $number_of_pages && $isExport == "excel");

            if ($isExport == "excel") {
                $this->downloadSendHeaders("pledge_export_" . date("Y-m-d") . ".csv");
                die;
            } else {
                $response->setContent(\Zend\Json\Json::encode($jsonResult));
                return $response;
            }

            //return $response->setContent(\Zend\Json\Json::encode($jsonResult));
        }
        $this->layout('crm');
        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        $viewModel = new ViewModel();
        $response = $this->getResponse();
        $viewModel->setVariables(array(
            'form' => $searchPledgeForm,
            'search_form' => $savePledgeForm,
            'messages' => $messages,
            'jsLangTranslate' => $pledge_create_messages,
            'pledgeStatusDesc' => $pledgeStatusDesc
        ));
        return $viewModel;
    }

    /**
     * This Action is used to save the pledge search
     * @param void
     * @return this will be json format with comfirmation message
     * @author Icreon Tech -AG
     */
    public function addCrmSearchPledgeAction() {
        $this->checkUserAuthentication();
        $this->getPledgeTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $pledge = new Pledge($this->_adapter);
        $pledgeSearchMessages = $this->_config['Pledge_messages']['config']['Pledge_create_message'];
        if ($request->isPost()) {
            parse_str($request->getPost('searchString'), $searchParam);
            $search_name = $request->getPost('search_name');
            $messages = array();
            parse_str($request->getPost('searchString'), $searchParam);
            $searchParam['search_title'] = $search_name;
            $searchParam = $pledge->getInputFilterPledgeSearch($searchParam);
            $search_name = $pledge->getInputFilterPledgeSearch($search_name);
            if (trim($search_name) != '') {
                $saveSearchParam['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $saveSearchParam['title'] = $search_name;
                $saveSearchParam['search_query'] = serialize($searchParam);
                $saveSearchParam['is_active'] = 1;
                $saveSearchParam['is_delete'] = '0';
                if ($request->getPost('search_id') != '') {
                    $saveSearchParam['modified_date'] = DATE_TIME_FORMAT;
                    $saveSearchParam['pledge_search_id'] = $request->getPost('search_id');
                    $pledgeUpdateArray = $pledge->getAddPledgeSearchArr($saveSearchParam);
                    if ($this->_pledgeTable->updatePledgeSearch($pledgeUpdateArray) == true) {
                        $this->flashMessenger()->addMessage($pledgeSearchMessages['SAVE_UPDATED_MSG']);
                        $messages = array('status' => "success");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                } else {
                    $saveSearchParam['added_date'] = DATE_TIME_FORMAT;
                    $searchAddArray = $pledge->getAddPledgeSearchArr($saveSearchParam);
                    if ($this->_pledgeTable->savePledgeSearch($searchAddArray) == true) {
                        $this->flashMessenger()->addMessage($pledgeSearchMessages['SAVE_SUCCESS_MSG']);
                        $messages = array('status' => "success");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                }
            }
        }
    }

    /**
     * This Action is used to get the saved pledge search
     * @param void
     * @return this will be json format
     * @author Icreon Tech - AG
     */
    public function getPledgeSearchSavedParamAction() {
        $this->checkUserAuthentication();
        $this->getPledgeTable();
        $pledge = new Pledge($this->_adapter);
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            if ($request->getPost('search_id') != '') {
                $search_array = array();
                $search_array['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $search_array['pledge_search_id'] = $request->getPost('search_id');
                $search_array['is_active'] = '1';
                $pledgeSavedSearchArray = $pledge->getPledgeSavedSearchArr($search_array);
                $pledgeSearchArray = $this->_pledgeTable->getPledgeSavedSearch($pledgeSavedSearchArray);
                $searchResult = json_encode(unserialize($pledgeSearchArray[0]['search_query']));
                return $response->setContent($searchResult);
            }
        }
    }

    /**
     * This Action is used to delete the saved pledge search
     * @param void
     * @return this will be a confirmation message in json
     * @author Icreon Tech - AG
     */
    public function deletePledgeSearchAction() {
        $this->checkUserAuthentication();
        $this->getPledgeTable();
        $pledge = new Pledge($this->_adapter);
        $request = $this->getRequest();
        $response = $this->getResponse();
        $pledgeSearchMessages = $this->_config['Pledge_messages']['config']['Pledge_create_message'];
        if ($request->isPost()) {
            $delete_array = array();
            $delete_array['is_deleted'] = 1;
            $delete_array['modified_date'] = DATE_TIME_FORMAT;
            $delete_array['pledge_search_id'] = $request->getPost('search_id');
            $pledge_delete_saved_search_array = $pledge->getDeletePledgeSavedSearchArr($delete_array);
            $this->_pledgeTable->deleteSavedSearch($pledge_delete_saved_search_array);
            $messages = array('status' => "success");
            $this->flashMessenger()->addMessage($pledgeSearchMessages['SEARCH_DELETE_CONFIRM']);
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This action is used to delete pledge
     * @param  void
     * @return     json format string
     * @author Icreon Tech - AG
     */
    public function deletePledgeAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $pledgeId = $request->getPost('pledgeId');
            if ($pledgeId != '') {
                $pledgeId = $this->decrypt($pledgeId);
                $post_arr = $request->getPost();
                $post_arr['pledgeId'] = $pledgeId;
                $this->getPledgeTable();
                $pledgeSearchMessages = $this->_config['Pledge_messages']['config']['Pledge_create_message'];
                $pledge = new Pledge($this->_adapter);
                $response = $this->getResponse();
                $pledgeArr = $pledge->getArrayForRemovePledge($post_arr);
                $this->_pledgeTable->removePledgeById($pledgeArr);
                $changeLogArray = array();
                $changeLogArray['table_name'] = 'tbl_ple_change_logs';
                $changeLogArray['activity'] = '3';
                $changeLogArray['id'] = $pledgeId;
                $changeLogArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                $this->flashMessenger()->addMessage($pledgeSearchMessages['PLEDGE_DELETED_SUCCESSFULLY']);
                $messages = array('status' => "success", 'message' => $pledgeSearchMessages['PLEDGE_DELETED_SUCCESSFULLY']);
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                return $this->redirect()->toRoute('pledges');
            }
        } else {
            return $this->redirect()->toRoute('pledges');
        }
    }

    /**
     * This action is used to show all pledge detail
     * @param void
     * @return array
     * @author Icreon Tech - AG
     */
    public function getPledgeInfoAction() {
        $this->checkUserAuthentication();
        $this->getPledgeTable();
        $pledgeViewMessages = array_merge($this->_config['Pledge_messages']['config']['Pledge_view_message'], $this->_config['Pledge_messages']['config']['Pledge_create_message']);
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();
        $this->layout('crm');
        $pledgeId = $this->decrypt($params['pledge_id']);
        $pledgeDetailArr = array();
        $pledgeDetailArr[] = $pledgeId;
        $pledgeData = $this->_pledgeTable->getPledgeById($pledgeDetailArr);
        /* Get number of activity in user id in this particular pledge */
        $activityArr = array();
        $activityArr['sourceType'] = 3;
        $activityArr['activitySourceId'] = $pledgeData['pledge_id'];
        $activityArr['userId'] = '';
        $activityCount = $this->getServiceLocator()->get('Activity\Model\ActivityTable')->getActivityCount($activityArr);
        /* End get number of activity in user id in this particular pledge */
        $mode = (isset($params['mode']) && $params['mode'] != '') ? $this->decrypt($params['mode']) : 'view';
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'jsLangTranslate' => $pledgeViewMessages,
            'pledge_id' => $pledgeId,
            'encrypt_pledge_id' => $params['pledge_id'],
            'pledge_data' => $pledgeData,
            'module_name' => $this->encrypt('pledge'),
            'mode' => $mode,
            'login_user_detail' => $this->_auth->getIdentity(),
            'encrypted_source_type_id' => $this->encrypt('3'),
            'encrypted_contact_name_id' => $pledgeData['contact_name_id'],
            'totalActivityCount' => $activityCount,
        ));
        return $viewModel;
    }

    /**
     * This action is used to get edit case detail page
     * @return string
     * @param void
     * @author Icreon Tech - ST
     */
    public function editPledgeAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        $this->getPledgeTable();
        $pledge_create_messages = $this->_config['Pledge_messages']['config']['Pledge_create_message'];
        $pledge = new Pledge($this->_adapter);
        $edit_pledge_form = new EditPledgeForm();
        $getCurrencies = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCurrencies();
        $currenciesArray = array();
        if ($getCurrencies !== false) {
            foreach ($getCurrencies as $key => $val) {
                $currenciesArray[$val['currency_id']] = $val['currency_symbol'];
            }
        }
        $edit_pledge_form->get('currency_id')->setAttribute('options', $currenciesArray);
        $getContributionTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getContributionTypes();
        $contributionTypesArray = array();
        $contributionTypesDescripton = array();
        $contributionTypesArray[''] = 'Select';
        if ($getContributionTypes !== false) {
            foreach ($getContributionTypes as $key => $val) {
                $contributionTypesArray[$val['contribution_type_id']] = $val['contribution_type'];
                $contributionTypesDescripton[$val['contribution_type_id']] = stripslashes($val['type_description']);
            }
        }
        $edit_pledge_form->get('contribution_type_id')->setAttribute('options', $contributionTypesArray);
        $getInstallmentTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getInstallmentTypes();
        $InstallmentTypesArray = array();
        $InstallmentTypesArray[''] = 'Select';
        if ($getInstallmentTypes !== false) {
            foreach ($getInstallmentTypes as $key => $val) {
                $InstallmentTypesArray[$val['installment_type_id']] = $val['installment_type'];
            }
        }
        $edit_pledge_form->get('installment_type_id')->setAttribute('options', $InstallmentTypesArray);
        if ($request->isXmlHttpRequest() && $request->isPost()) {

            $edit_pledge_form->setInputFilter($pledge->getInputFilterCreatePledge());
            $edit_pledge_form->setData($request->getPost());
            if (!$edit_pledge_form->isValid()) {
                $errors = $edit_pledge_form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && ($key != 'in_honoree' && ($key != 'in_honoree' && ($key!='payment_due_days' && $request->getPost('installment_type_id')==1)))) {
                        foreach ($row as $type_error => $rower) {
                            if ($type_error != 'notInArray') {
                                $msg [$key] = $pledge_create_messages[$rower];
                            }
                        }
                    }
                }
				if(!empty($msg))
					$messages = array('status' => "error", 'message' => $msg);
            }


            $create_pledge_form_arr = $edit_pledge_form->getData();
            $postedData = array();
            $postedData['pledgeId'] = $create_pledge_form_arr['pledge_id'];
            $postedData['userId'] = $create_pledge_form_arr['contact_name_id'];
            if (!$this->getActivePledgeForUser($postedData)) {
                $msg = array();
                $msg ['contact_name'] = $pledge_create_messages['ALREADY_ACTIVE_PLEDGE'];
                $messages = array('status' => "error", 'message' => $msg);
            }

            /* if (($create_pledge_form_arr['num_installment'] * $create_pledge_form_arr['installment_amount'])!=$create_pledge_form_arr['installment_amount']) {
              $msg = array();
              $msg ['num_installment'] = $pledge_create_messages['PLEDGE_EDIT_ERROR'];
              $messages = array('status' => "error", 'message' => $msg);
              } */

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $postedData = $request->getPost();


                $pledgeStatusArray = $this->getServiceLocator()->get('Common\Model\CommonTable')->getPledgeStatus();
                $create_pledge_form_arr['pledge_status_id'] = $pledgeStatusArray[0]['pledge_status_id'];
                $create_pledge_form_arr['modify_by'] = $this->_auth->getIdentity()->crm_user_id;
                $create_pledge_form_arr['modify_date'] = DATE_TIME_FORMAT;
                $create_pledge_arr = $pledge->getUpdatePledgeArr($create_pledge_form_arr);
                $this->_pledgeTable->updatePledge($create_pledge_arr);
                $pledgeId = $create_pledge_form_arr['pledge_id'];


                if ($create_pledge_form_arr['is_trans_update'] == 1) {

                    $pledgeTransData = $this->_pledgeTable->getPledgeTransactionsById($pledgeId, '1');

                    if (count($pledgeTransData)) {
                        $paidPledgeAmount = 0;
                        $i = 0;
                        foreach ($pledgeTransData as $key => $transactionData) {
                            $paidPledgeAmount +=$transactionData['amount_pledge'];
                        }
                    }
                    $nextDueDate = $create_pledge_form_arr['payment_start_date'];
                    $paidCount = count($pledgeTransData);
                    $pledgeTransData1 = $this->_pledgeTable->deletePledgeTransactions($pledgeId);

                    $pledgeTransactionArr = array();

                    if ($create_pledge_form_arr['num_installment'] == $postedData['num_installment_hidden']) {
                        $pleInstallmentNo = $create_pledge_form_arr['num_installment'] - $paidCount;
                    } else {
                        $pleInstallmentNo = $create_pledge_form_arr['num_installment'];
                    }

                    $pleAmount = $create_pledge_form_arr['pledge_amount'];
                    $pleInstallmentAmount = $create_pledge_form_arr['installment_amount'];
                    $pleInstallmentGap = $create_pledge_form_arr['installment_in'];
                    $paymentDueDay = $create_pledge_form_arr['payment_due_days'];
                    $pleInstallmentCycle = $create_pledge_form_arr['installment_type_id'];
                    $pledgeTransactionArr['pledgeId'] = $pledgeId;
                    $pledgeTransactionArr['amountPledge'] = $pleInstallmentAmount;
                    $pledgeTransactionArr['status'] = '0';
                    $pledgeTransactionArr['addedBy'] = $this->_auth->getIdentity()->crm_user_id;
                    $pledgeTransactionArr['addedDate'] = DATE_TIME_FORMAT;
                    $pledgeTransactionArr['modifiedBy'] = $this->_auth->getIdentity()->crm_user_id;
                    $pledgeTransactionArr['modifiedDate'] = DATE_TIME_FORMAT;

                    if (isset($postedData['balance_amount']) && $postedData['balance_amount'] != "")
                        $pledgeTransactionArr['remainingAmount'] = $postedData['balance_amount'];
                    else {
                        $pledgeTransactionArr['remainingAmount'] = $postedData['pledge_amount'];
                    }

                    for ($i = 0; $i < $pleInstallmentNo; $i++) {
                        $date = strtotime($nextDueDate);
                        switch ($pleInstallmentCycle) {
                            case 1:
                                $intervalVar = "+" . $pleInstallmentGap . " day";
                                $date = strtotime($intervalVar, $date);
                                break;
                            case 2:
                                $intervalVar = "+" . ($pleInstallmentGap) . " week";
                                $date = strtotime($intervalVar, $date);
                                break;
                            case 3:
                                $intervalVar = "+" . $pleInstallmentGap . " month";
                                $date = strtotime($intervalVar, $date);
                                break;
                            default:
                                $intervalVar = "+" . $pleInstallmentGap . " year";
                                $date = strtotime($intervalVar, $date);
                        }

                        if ($i == 0 && $pleInstallmentCycle != 1) {
                            $date = strtotime("+" . $paymentDueDay . " day", $date);
                        }
                        $nextDueDate = date('Y-m-d', $date);
                        $pledgeTransactionArr['dueDate'] = $nextDueDate;
                        $pledgeTransactionArr['remainingAmount'] = ($pledgeTransactionArr['remainingAmount'] - $pleInstallmentAmount);


                        $pledgeTransInsertArr = $pledge->exchangeTransArr($pledgeTransactionArr);

                        $pledge_id = $this->_pledgeTable->savePledgeTranasction($pledgeTransInsertArr);
                    }
                }

                ///////////////////////////////////			


                $changeLogArray = array();
                $changeLogArray['table_name'] = 'tbl_ple_change_logs';
                $changeLogArray['activity'] = '2'; // need to check
                $changeLogArray['id'] = $create_pledge_arr[0];
                $changeLogArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                $this->flashMessenger()->addMessage($pledge_create_messages['PLEDGE_UPDATED_SUCCESSFULLY']);
                $messages = array('status' => "success", 'message' => $pledge_create_messages['PLEDGE_UPDATED_SUCCESSFULLY']);
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        } else if (isset($params['pledge_id']) && $params['pledge_id'] != '') {
            $pledge_id = $this->decrypt($params['pledge_id']);
            $pledge_data = $this->_pledgeTable->getPledgeById(array($pledge_id));
            $mode = (isset($params['mode']) && $params['mode'] != '') ? $params['mode'] : $this->encrypt("view");
            $edit_pledge_form->get('pledge_id')->setValue($pledge_id);

            if ($pledge_data['user_type'] != '1') {
                $edit_pledge_form->get('contact_name')->setValue($pledge_data['company_name']);
            } else {
                $edit_pledge_form->get('contact_name')->setValue($pledge_data['contact_name']);
            }

            $edit_pledge_form->get('contact_name_id')->setValue($pledge_data['contact_name_id']);
            $edit_pledge_form->get('assigned_to')->setValue($pledge_data['assigned_to']);
            $edit_pledge_form->get('assigned_to_id')->setValue($pledge_data['assigned_to_id']);
            $edit_pledge_form->get('payment_due_days')->setValue($pledge_data['payment_due_days']);
            $edit_pledge_form->get('payment_due_days_hid')->setValue($pledge_data['payment_due_days']);
            $edit_pledge_form->get('pledge_amount')->setValue($pledge_data['pledge_amount']);
            $edit_pledge_form->get('payment_start_date')->setValue($this->DateFormat($pledge_data['payment_start_date'], 'calender'));
            $edit_pledge_form->get('pledge_made_date')->setValue($this->DateFormat($pledge_data['pledge_made_date'], 'calender'));
            $edit_pledge_form->get('contribution_type_id')->setValue($pledge_data['contribution_type_id']);
            $edit_pledge_form->get('acknowledgment_date')->setValue($this->DateFormat($pledge_data['acknowledgment_date'], 'calender'));
            $edit_pledge_form->get('campaign_name')->setValue($pledge_data['campaign']);
            $edit_pledge_form->get('num_installment')->setValue($pledge_data['num_installment']);
            $edit_pledge_form->get('installment_amount')->setValue($pledge_data['installment_amount']);
            $edit_pledge_form->get('installment_in')->setValue($pledge_data['installment_in']);
            $edit_pledge_form->get('installment_type_id')->setValue($pledge_data['installment_type_id']);
            $edit_pledge_form->get('in_honoree')->setValue($pledge_data['in_honoree']);
            $honoreeName=($pledge_data['in_honoree']!='3')?$pledge_data['honoree_name']:'';
            $edit_pledge_form->get('honoree_name')->setValue($honoreeName);
            $edit_pledge_form->get('is_reminder')->setValue($pledge_data['is_reminder']);
            $edit_pledge_form->get('staff_reminder')->setValue($pledge_data['staff_reminder']);
            $edit_pledge_form->get('contact_reminder')->setValue($pledge_data['contact_reminder']);
            $edit_pledge_form->get('reminder_days')->setValue($pledge_data['reminder_days']);
            $edit_pledge_form->get('num_reminder')->setValue($pledge_data['num_reminder']);
            $edit_pledge_form->get('additional_reminder_days')->setValue($pledge_data['additional_reminder_days']);

            $pledgeTransData = $this->_pledgeTable->getPledgeTransactionsById($pledge_id, '1');
            $paidPledgeAmount = '0';
            if (!empty($pledgeTransData)) {
                $paidPledgeAmount = 0;

                foreach ($pledgeTransData as $key => $transactionData) {
                    $paidPledgeAmount +=$transactionData['amount_pledge'];
                }
            }


            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => $pledge_create_messages,
                'edit_pledge_form' => $edit_pledge_form,
                'pledge_id' => $pledge_id,
                'encrypt_lead_id' => $params['pledge_id'],
                'mode' => $mode,
                'paidPledgeAmount' => $paidPledgeAmount,
                'pledgeAmount' => $pledge_data['pledge_amount'],
                'dueDays' => $pledge_data['payment_due_days'],
                'pledgeData' => $pledge_data,
                'contributionTypesDescripton' => $contributionTypesDescripton
            ));
            return $viewModel;
        }
    }

    /**

     * This action is used to show view pledge detail on ajax call
     * @return     array
     * @author Icreon Tech - AG
     */
    public function viewPledgeDetailAction() {
        $this->checkUserAuthentication();
        $this->getPledgeTable();
        $pledgeViewMessages = $this->_config['Pledge_messages']['config']['Pledge_view_message'];
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();
        if (isset($params['pledge_id']) && $params['pledge_id'] != '') {
            $pledge_id = $this->decrypt($params['pledge_id']);
            $pledgeDetailArr = array();
            $pledgeDetailArr[] = $pledge_id;
            $pledgeData = $this->_pledgeTable->getPledgeById($pledgeDetailArr);
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => $pledgeViewMessages,
                'pledge_id' => $pledge_id,
                'encrypt_pledge_id' => $params['pledge_id'],
                'pledge_data' => $pledgeData,
                'login_user_detail' => $this->_auth->getIdentity()
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to show view pledge transactions list on ajax call
     * @return     array
     * @author Icreon Tech - AG
     */
    public function getPledgeTransactionsAction() {
        $this->checkUserAuthentication();
        $this->getPledgeTable();
        $pledge = new Pledge($this->_adapter);
        $pledgeViewMessages = $this->_config['Pledge_messages']['config']['Pledge_view_message'];
        $request = $this->getRequest();
        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $response = $this->getResponse();
            $post_array = $request->getPost()->toArray();
            parse_str($request->getPost('searchString'), $searchParam);
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit;
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            $searchParam['pledge_id'] = $request->getPost('pledge_id');
            if (isset($searchParam['pledge_id']) && $searchParam['pledge_id'] != '') {
                $pledgeDetailArr = array();
                $searchPledgeTransactionsArr = $pledge->getPledgeTransactionsSearchArr($searchParam);
                $pledgeData = $this->_pledgeTable->getPledgeTransactions($searchPledgeTransactionsArr);
                $countResult = $this->_pledgeTable->getTotalPledgeTransactions($searchParam['pledge_id']);
                $jsonResult = array();
                $arrCell = array();
                $subArrCell = array();
                $dueArrCell = array();
                $jsonResult['page'] = $request->getPost('page');
                $jsonResult['records'] = $countResult['total'];
                $jsonResult['total'] = ceil($countResult['total'] / $limit);
                if (!empty($pledgeData)) {
                    $isDue = 0;
                    $pledgeDetailArr = array();
                    $pledgeDetailArr[] = $request->getPost('pledge_id');
                    $pledgeDataResult = $this->_pledgeTable->getPledgeById($pledgeDetailArr);
                    foreach ($pledgeData as $val) {
                        //asd($pledgeData);
                        $arrCell['id'] = $val['pledge_id'];
                        $dueDate = $this->Dateformat($val['due_date'], 'displayformat');
                        $totalAmount = $val['pledge_amount'];
                        $amountPledged = $val['amount_pledge'];
                        $remainingAmount = $val['remaining_amount'];
                        $status = $val['status'];
                        if ($status == 0 && $isDue == 0 && $pledgeDataResult['pledge_status_id'] != 3) {
                            $isDue = 1;
                            $encryptPledgeTransactionId = $this->encrypt($val['pledge_transaction_id']);
                            $status = "<select name='pledge_status' id='pledge_status' onchange='payPledgeAmount(this.value,\"$encryptPledgeTransactionId\")' class='e1' style='width:100px;'><option value='0'>Due</option><option value='1'>Paid</option></select>";
                            $transactionNumber = $val['transaction_id'];
                            $arrCell['cell'] = array($dueDate, $totalAmount, $amountPledged, $remainingAmount, $status, $transactionNumber);
                            $dueArrCell = $arrCell;
                        } else if ($status != 0) {
                            $status = "Paid";
                            $transactionNumber = $val['transaction_id'];
                            $arrCell['cell'] = array($dueDate, $totalAmount, $amountPledged, $remainingAmount, $status, $transactionNumber);
                            $subArrCell[] = $arrCell;
                        }
                    }
                    if (!empty($dueArrCell)) {
                        array_unshift($subArrCell, $dueArrCell);
                    }
                    $jsonResult['rows'] = $subArrCell;
                } else {
                    $jsonResult['rows'] = array();
                }
                return $response->setContent(\Zend\Json\Json::encode($jsonResult));
            }
        }
        $params = $this->params()->fromRoute();
        $pledge_id = $this->decrypt($params['pledge_id']);
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => $pledgeViewMessages,
            'pledge_id' => $pledge_id,
            'encrypt_pledge_id' => $params['pledge_id'],
            'login_user_detail' => $this->_auth->getIdentity()
        ));
        return $viewModel;
    }

    /** 	 
     * This Action is used to generte the select option after click on move checkbox
     * @param void
     * @return this will be a string having the all options
     * @author Icreon Tech - DT
     */
    public function getPledgeSavedSearchListAction() {
        $this->checkUserAuthentication();
        $this->getPledgeTable();
        $pledge = new Pledge($this->_adapter);
        $request = $this->getRequest();
        $response = $this->getResponse();

        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $search_array = array();
            $pledgeSavedSearchArray = array();
            $search_array['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
            $search_array['pledge_search_id'] = '';
            $search_array['is_active'] = '1';
            $pledgeSavedSearchParamsArray = $pledge->getPledgeSavedSearchArr($search_array);
            $pledgeSearchArray = $this->_pledgeTable->getPledgeSavedSearch($pledgeSavedSearchParamsArray);
            foreach ($pledgeSearchArray as $key => $val) {
                $pledgeSavedSearchArray[$val['pledge_search_id']] = $val['title'];
            }
            $searchResult = json_encode($pledgeSavedSearchArray);
            return $response->setContent($searchResult);
        }
    }

    /**
     * This Action is used to get pledge payment Detail
     * @param Void
     * @return String
     * @author Icreon Tech - DT
     */
    public function getPledgePaymentDetailAction() {
        $this->checkUserAuthenticationAjx();
        $this->layout('popup');
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getPledgeTable();
        $pledgePayment = new PledgePayment();
        $pledgeTransactionMessage = $this->_config['Pledge_messages']['config']['Pledge_transaction_message'];
        $pledge = new Pledge($this->_adapter);


        $getPaymentModes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getPaymentMode();
        $paymentModesArray = array();
        $paymentModesArray[''] = 'Select';
        if ($getPaymentModes !== false) {
            foreach ($getPaymentModes as $key => $val) {
                if ($val['payment_mode_id'] != 4) {
                    $paymentModesArray[$val['payment_mode_id']] = $val['payment_mode'];
                }
            }
        }
        $pledgePayment->get('paid_by')->setAttribute('options', $paymentModesArray);

        if ($request->isPost()) {
            $pledgePayment->setData($request->getPost());
            $validationGroups = array();
            $paidBy = $request->getPost('paid_by');
            if ($paidBy == 2) {
                $validationGroups[] = 'account_number';
            } else if ($paidBy == 3) {
                $validationGroups[] = 'check_number';
            }
            $pledgePayment->setValidationGroup($validationGroups);
            $pledgePayment->setInputFilter($pledge->getInputFilterSavePledgeTransaction());
            if (!$pledgePayment->isValid()) {
                $errors = $pledgePayment->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'continue') {
                        foreach ($row as $typeError => $rower) {
                            if ($typeError != 'notInArray') {
                                $msg [$key] = $pledgeTransactionMessage[$rower];
                            }
                        }
                    }
                }
            }
            if (!empty($msg)) {
                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $postArr = $request->getPost()->toArray();
                $saveTransactionFormArr = $postArr;
                $transactionId = $this->savePledgeTransaction($saveTransactionFormArr);
                $dateTime = DATE_TIME_FORMAT;

                $crmUserId = !(empty($this->_auth->getIdentity()->crm_user_id)) ? $this->_auth->getIdentity()->crm_user_id : '';
                /* Add payment information  of transaction */
                $pledgeTransData = $this->_pledgeTable->getPledgeTransactionsById('', '', $postArr['pledge_transaction_id']);
                $paymentTransactionData = array();
                $paymentTransactionData['amount'] = $pledgeTransData[0]['amount_pledge'];
                $paymentTransactionData['transaction_id'] = $transactionId;
                $paymentTransactionData['payment_mode_id'] = $postArr['paid_by'];
                if ($postArr['paid_by'] == 3) {
                    $paymentTransactionData['cheque_number'] = $postArr['check_number'];
                } else if ($postArr['paid_by'] == 2) {
                    $paymentTransactionData['account_no'] = $postArr['account_number'];
                }
                $paymentTransactionData['added_by'] = $crmUserId;
                $paymentTransactionData['added_date'] = $dateTime;
                $paymentTransactionData['modify_by'] = $crmUserId;
                $paymentTransactionData['modify_date'] = $dateTime;
                $transaction = new Transaction($this->_adapter);
                $transactionCheckPaymentArr = $transaction->getTransactionPaymentArr($paymentTransactionData);
                $transactionPaymentId = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->saveTransactionPaymentDetail($transactionCheckPaymentArr);
                /* End of Payment Information */

                //$responseArr = $this->saveFinalTransaction($saveTransactionFormArr);
                $messages = array('status' => "success", 'message' => $pledgeTransactionMessage['SUCCESSFULLY_UPDATED_PLEDGE']);
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        } else {
            $params = $this->params()->fromRoute();
            $pledgeTransactionId = $this->decrypt($params['pledge_transaction_id']);
            $pledgePayment->get('pledge_transaction_id')->setValue($pledgeTransactionId);

            $pledgeTransData = $this->_pledgeTable->getPledgeTransactionsById('', '', $pledgeTransactionId);
            $viewModel = new ViewModel();
            $viewModel->setVariables(array(
                'jsLangTranslate' => $pledgeTransactionMessage,
                'pledgePayment' => $pledgePayment,
                'encryptUserId' => $this->encrypt($pledgeTransData[0]['user_id']),
                'encryptPledgeTransactionId' => $this->encrypt($pledgeTransData[0]['pledge_transaction_id'])
            ));
            return $viewModel;
        }
    }

    /**
     * This function is used to save pledge Transaction
     * @param Void
     * @return String
     * @author Icreon Tech - DT
     */
    public function savePledgeTransaction($postArr) {
        $this->getPledgeTable();
        $pledge = new Pledge($this->_adapter);
        $pledgeTransData = $this->_pledgeTable->getPledgeTransactionsById('', '', $postArr['pledge_transaction_id']);

        $dateTime = DATE_TIME_FORMAT;

        $crmUserId = !(empty($this->_auth->getIdentity()->crm_user_id)) ? $this->_auth->getIdentity()->crm_user_id : '';
        /* Add transaction in main table */
        $finalTransaction = array();
        $finalTransaction['user_id'] = $pledgeTransData[0]['user_id'];
        $finalTransaction['transaction_source_id'] = $this->_config['transaction_source']['transaction_source_id'];
        $finalTransaction['num_items'] = 1;
        $finalTransaction['sub_total_amount'] = $pledgeTransData[0]['amount_pledge'];
        $finalTransaction['shipping_amount'] = 0;
        $finalTransaction['total_tax'] = 0;
        $finalTransaction['transaction_amount'] = $pledgeTransData[0]['amount_pledge'];
        $finalTransaction['transaction_date'] = $dateTime;
        $finalTransaction['transaction_type'] = '1';
        $finalTransaction['added_by'] = $crmUserId;
        $finalTransaction['added_date'] = $dateTime;
        $finalTransaction['modify_by'] = $crmUserId;
        $finalTransaction['modify_date'] = $dateTime;

        $transaction = new Transaction($this->_adapter);
        $finalTransactionArr = $transaction->getFinalTransactionArr($finalTransaction);
        $transactionId = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->saveFinalTransaction($finalTransactionArr);

        /* End add transaction in main table */
        /* Add Donation Product */
        $donationProductData = array();
        $donationProductData['transaction_id'] = $transactionId;
        $donationProductData['pledge_transaction_id'] = $pledgeTransData[0]['pledge_transaction_id'];
        $donationProductData['is_pledge_transaction'] = '1';
        $donationProductData['product_mapping_id'] = 1;
        $donationProductData['product_type_id'] = 2;
        $donationProductData['product_price'] = $pledgeTransData[0]['amount_pledge'];
        $donationProductData['product_sub_total'] = $pledgeTransData[0]['amount_pledge'];
        $donationProductData['product_discount'] = 0;
        $donationProductData['product_tax'] = 0;
        $donationProductData['product_total'] = $pledgeTransData[0]['amount_pledge'];
        $donationProductData['product_status_id'] = 10;
        $donationProductData['purchase_date'] = $dateTime;
        $donationProductData['is_deleted'] = 0;
        $donationProductData['added_by'] = $crmUserId;
        $donationProductData['added_date'] = $dateTime;
        $donationProductData['modify_by'] = $crmUserId;
        $donationProductData['modified_date'] = $dateTime;
		$donationProductData['user_id'] = $pledgeTransData[0]['user_id'];
		$donationProductData['product_id'] = 8;
		$donationProductData['product_name'] = 'Donation';

        $donationProductArray = $transaction->getDonationProductArray($donationProductData);
        $donationProductId = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->saveTransactionDonationProdcts($donationProductArray);
        /* End add Donation Product */


        /* Update pledge transaction status */
        $updatePledgeStatusArray = array();
        $updatePledgeStatusArray['pledge_transaction_id'] = $pledgeTransData[0]['pledge_transaction_id'];
        $updatePledgeStatusArray['status'] = '1';
        $updatePledgeStatusArray['transaction_id'] = $transactionId;
        $updatePledgeStatusArray['modify_by'] = $crmUserId;
        $updatePledgeStatusArray['modify_date'] = $dateTime;
        $updatePledgeTransactionStatusArr = $pledge->updatePledgeTransactionStatusArr($updatePledgeStatusArray);
        $pledgeTransactionId = $this->_pledgeTable->updatePledgeTransactionStatus($updatePledgeTransactionStatusArr);
        /* End update pledge transaction status */
        return $transactionId;
    }

    /**
     * This action is used to change pledge status
     * @return string
     * @param void
     * @author Icreon Tech - DT
     */
    public function changePledgeStatusAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        $this->getPledgeTable();
        $pledge_create_messages = $this->_config['Pledge_messages']['config']['Pledge_create_message'];
        $pledge = new Pledge($this->_adapter);

        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $postedData = $request->getPost()->toArray();
            $postedData['pledgeId'] = $this->decrypt($postedData['pledgeId']);
            $postedData['modify_by'] = $this->_auth->getIdentity()->crm_user_id;
            $postedData['modify_date'] = DATE_TIME_FORMAT;
            $updatePledgeStatusArr = $pledge->getUpdatePledgeStatusArr($postedData);
            $this->_pledgeTable->updatePledgeStatus($updatePledgeStatusArr);
            $this->flashMessenger()->addMessage($pledge_create_messages['PLEDGE_UPDATED_SUCCESSFULLY']);
            $messages = array('status' => "success", 'message' => $pledge_create_messages['PLEDGE_UPDATED_SUCCESSFULLY']);
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }
    public function contactPledgesAction() {
        $this->getPledgeTable();
        $this->layout('crm');
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        $user_id = $params['userid'];
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => $this->_config['lead_messages']['config']['lead_search'],
            'user_id' => $user_id,
                )
        );
        return $viewModel;
    }

    public function searchContactPledgesAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getPledgeTable();
        $pledge = new Pledge($this->_adapter);
        $pledge_create_messages = $this->_config['Pledge_messages']['config']['Pledge_create_message'];
        $params = $this->params()->fromRoute();
        $post_array = $request->getPost()->toArray();
        parse_str($request->getPost('searchString'), $searchParam);
        $searchParam['user_id'] = $this->decrypt($params['userid']);
        $searchParam['sort_field'] = 'tbl_pledge.modified_date';

        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        $searchParam['start_index'] = $start;
        $searchParam['record_limit'] = $limit;
        $searchParam['sort_field'] = $request->getPost('sidx');
        $searchParam['sort_order'] = $request->getPost('sord');

        $searchPledgeArr = $pledge->getPledgeSearchArr($searchParam);
        $searchResult = $this->_pledgeTable->getAllPledge($searchPledgeArr);

        $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
        $jsonResult = array();
        $arrCell = array();
        $subArrCell = array();
        $jsonResult['records'] = $countResult[0]->RecordCount;
        $jsonResult['page'] = $request->getPost('page');
        $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);

        if (!empty($searchResult)) {
            $arrExport = array();
            foreach ($searchResult as $val) {
                $arrCell['id'] = $val['pledge_id'];
                $encrypt_id = $this->encrypt($val['pledge_id']);
                $view_link = '<a href="/get-pledge-info/' . $encrypt_id . '/' . $this->encrypt('view') . '" class="view-icon"><div class="tooltip">View<span></span></div></a>';
                $actions = '<div class="action-col">' . $view_link . '</div>';
                $pledgeAddedDate = ($val['added_date'] == 0) ? 'N/A' : $this->OutputDateFormat($val['added_date'], 'dateformatampm');

                if ($val['pledge_status'] == 1) {
                    $status = "Pending";
                }
                if ($val['pledge_status'] == 2) {
                    $status = "Completed";
                }
                if ($val['pledge_status'] == 3) {
                    $status = "Canceled";
                }
                if ($val['pledge_status'] == 4) {
                    $status = "In progress";
                }
                if ($val['pledge_status'] == 5) {
                    $status = "Overdue";
                }

                $balanceAmount = isset($val['BalanceAmount']) ? $val['BalanceAmount'] : '0.00';

                $caseContactId = '<span class="plus-sign"><a href="/create-activity/' . $this->encrypt($val['user_id']) . '/' . $this->encrypt('3') . '/' . $this->encrypt($val['pledge_id']) . '"><div class="tooltip">Add activity<span></span></div></a></span>';
                $fullName = (!empty($val['full_name'])) ? $val['full_name'] : $val['company_name'];
                if (isset($dashlet) && $dashlet == 'dashlet') {
                    $arrCell['cell'] = array($caseContactId . $val['pledge_id'], $fullName, $val['pledge_for'], $pledgeAddedDate);
                } else {
                    $arrCell['cell'] = array($caseContactId . $val['pledge_id'], $fullName, $val['pledge_amount'], $balanceAmount, $val['pledge_for'], $pledgeAddedDate, $status, $actions);
                }


                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
        } else {
            $jsonResult['rows'] = array();
        }

        $response->setContent(\Zend\Json\Json::encode($jsonResult));
        return $response;
    }

    /**
     * This action is used to check user active pledge
     * @return string
     * @param void
     * @author Icreon Tech - DT
     */
    public function checkUserActivePledgeAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        $this->getPledgeTable();
        $pledge_create_messages = $this->_config['Pledge_messages']['config']['Pledge_create_message'];


        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $postedData = $request->getPost()->toArray();
            $postedData['pledgeId'] = $postedData['pledgeId'];
            $postedData['userId'] = $postedData['userId'];
            if ($this->getActivePledgeForUser($postedData)) {
                $messages = array('status' => "success");
            } else {
                $msg = array();
                $msg ['contact_name'] = $pledge_create_messages['ALREADY_ACTIVE_PLEDGE'];
                $messages = array('status' => "error", 'message' => $msg);
            }
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This action is used to check user active pledge
     * @return string
     * @param void
     * @author Icreon Tech - DT
     */
    public function getActivePledgeForUser($postedData) {
        $pledge = new Pledge($this->_adapter);
        $checkUserActivePledgeArr = $pledge->getCheckUserActivePledgeArr($postedData);
        $activePledge = $this->_pledgeTable->checkUserActivePledge($checkUserActivePledgeArr);
        //asd($activePledge);
        if (!empty($activePledge)) {
            return false;
        }
        return true;
    }
}

