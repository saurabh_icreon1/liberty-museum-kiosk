<?php 
return array(
    'Pledge\Module'                       => __DIR__ . '/Module.php',
    'Pledge\Controller\PledgeController'   => __DIR__ . '/src/Pledge/Controller/PledgeController.php',
    'Pledge\Model\Pledge'            => __DIR__ . '/src/Pledge/Model/Pledge.php',
    'Pledge\Model\PledgeTable'                   => __DIR__ . '/src/Pledge/Model/PledgeTable.php'
);
?>