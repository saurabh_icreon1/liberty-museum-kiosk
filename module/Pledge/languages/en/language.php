<?php

return array(
    'Pledge_create_message' => array(
        "L_PLEDGE" => "Pledge",
        "L_SYMBOL" => "»",
        "L_DASHBOARD" => "Dashboard",
        "L_CREATE" => "Create",
        "L_EDIT" => "Edit",
        "L_LIST" => "List",
        "L_SYMBOL" => ">>",
        "L_CREATE_PLEDGE" => "Create Pledge",
        "L_EDIT_PLEDGE" => "Edit Pledge",
        "L_PLEDGE_DETAILS" => "Pledge Details",
        "L_SELECT_CONTACT" => "Select Contact",
        "L_OR" => "OR",
        "L_CREATE_NEW_CONTACT" => "Create New Contact",
        "L_ASSIGN_TO" => "Assigned To",
        "L_ENTER_MARIA_CLICK" => "Please enter 'Maria' and click",
        "L_PAYMENT_DUE_ON_THE" => "Payment are due on the",
        "L_DAY_OF_THE_PERIOD" => "day of the period",
        "L_APPLIES_TO_WEEK_MONTH_YEARLY_PAYMENT" => "This applies to weekly, monthly and yearly payments.",
        "L_TOTAL_PLEDGE_AMOUNT" => "Total Pledge Amount",
        "L_PAYMENT_START" => "Payment Start",
        "L_DATE_FIRST_PLEDGE_PAYMENT" => "Date of first pledge payment.",
        "L_PLEDGE_MADE" => "Pledge Made",
        "L_PLEDGE_MADE_BY_CONTRIBUTOR" => "Date when pledge was made by the contributor.",
        "L_CONTRIBUTION_TYPE" => "Contribution Type",
        "L_DEFAULT_CONTRIBUTION_TYPE" => "Sets the default contribution type for payments against this pledge.",
        "L_ACKNOWLEDGMENT_DATE" => "Acknowledgment Date",
        "L_ACKNOWLEDGMENT_DATE_TEXT" => "Date when an acknowledgment of the pledge was sent.",
        "L_COMPAIGN" => "Campaign",
        "L_COMPAIGN_TEXT" => "Please enter 'Sandy' and click",
        "L_TO_BE_PAID" => "To be paid in",
        "L_INSTALLMENT_OF" => "Installment of $",
        "L_EVERY" => "Every",
        "L_HONOREE_DETAILS" => "Honoree Details",
        "L_IN_HONOR_OF" => "In Honor of",
        "L_IN_MEMORY_OF" => "In Memory of",
        "L_NONE" => "None",
        "L_NAME" => "Name",
        "L_REMINDER" => "Reminder",
        "L_TURN_ON_REMINDER_SETTINGS" => "Turn On Reminder Settings",
        "L_STAFF" => "Staff",
        "L_CONTACT" => "Contact",
        "L_SEND_INITIAL_REMINDER" => "Send Initial Reminder",
        "L_SEND_INITIAL_REMINDER_TEXT" => "Days prior to each scheduled payment due date.",
        "L_SEND_UP_TO" => "Send Up to",
        "L_SEND_UP_TO_TEXT" => "Reminders for each scheduled payment.",
        "L_SEND_ADDITIONAL_REMINDER" => "Send Additional Reminder",
        "L_SEND_ADDITIONAL_REMINDER_TEXT" => "Days after the last one sent, up to the maximum number of reminders.",
        "L_SAVE" => "Save",
        "L_CANCEL" => "Cancel",
        "L_SEARCH_PLEDGE" => "Search Pledge",
        "L_CONTACT_NAME_EMAIL" => "Name/Company or Email",
        "L_PLEDGE_HASH" => "Pledge #",
        "L_PAYMENT_SCHEDULE" => "Payment Scheduled",
        "L_PLEDGE_AMOUNT" => "Pledges Amount",
        "L_FROM" => "From",
        "L_TO" => "To",
        "L_PLEDGE_STATUS" => "Pledge Status",
        "L_SAVED_SEARCHES" => "Saved Searches",
        "L_SAVE_SEARCH_AS" => "Save Search As",
        "L_SEARCH_RESULTS" => "Search Results :",
        "L_TOTAL_AMOUNT" => "Total Amount",
        "L_BLANCE_AMOUNT" => "Balance Amount",
        "L_PLEDGE_FOR" => "Pledge for",
        "L_DATE" => "Date",
        "L_STATUS" => "Status",
        "L_ACTIONS" => "Action (s)",
        "CONTACT_NAME_EMPTY" => "Please enter contact name",
        "ASSIGNED_TO_NAME_EMPTY" => "Please enter assigned to name.",
        "PLEDGE_AMOUNT_EMPTY" => "Please enter pledge amount.",
        "NUM_INSTALLMENT_EMPTY" => "Please enter number of installments.",
        "INSTALLMENT_AMOUNT_EMPTY" => "Please enter installment amount",
        "INSTALLMENT_PERIOD_EMPTY" => "Please enter installment period",
        "INSTALLMENT_CYCLE_EMPTY" => "Please enter installment cycle",
        "PAYMENT_DUE_DATE_EMPTY" => "Please enter payment due days",
        "PLEDGE_MADE_DATE_EMPTY" => "Please enter pledge made date",
        "PAYMENT_START_DATE_EMPTY" => "Please enter payment start date",
        "HONOREE_NAME_EMPTY" => "Please enter honoree name",
        "REMINDER_DAYS_EMPTY" => "Please enter reminder days",
        "NUMBER_REMINDER_EMPTY" => "Please enter number of reminders",
        "ADDITIONAL_REMINDER_DAYS_EMPTY" => "Please enter additional reminder days",
        "VALID_AMOUNT" => "Please enter a valid amount.",
        "VALID_NUMBER" => "Please provide the correct number.",
        "PLEDGE_UPDATED_SUCCESSFULLY" => "Pledge updated successfully !",
        "PLEDGE_CREATED_SUCCESSFULLY" => "A new record of Pledge added successfully !",
        'SEARCH_NAME_MSG' => "Please enter search name",
        "SAVE_SUCCESS_MSG" => "Search is saved successfully.",
        "SAVE_UPDATED_MSG" => "Search is updated successfully.",
        "ERROR_SAVING_SEARCH" => "Error in saving the search.",
        "DELETE_ALERT" => "Are you sure you want to delete this record?",
        "DELETE_CONFIRM" => "Record is deleted successfully.",
        "SEARCH_DELETE_CONFIRM" => "Search is deleted successfully.",
        "DELETE_PLEDGE_ALERT" => "Are You Sure you want to delete this Pledge ?",
        "PLEDGE_DELETED_SUCCESSFULLY" => "Pledge successfully deleted !",
        "PLEDGE_DATA_ERROR" => "The installment period and amount do not match",
        "NO_RECORD_FOUND" => "No Record Found.",
        "PLEDGE_EDIT_ERROR" => "Installment Period & Installment Amount does not match with Balance Amount.",
        "PLEDGE_DATA_ERROR_COMPLETE" => "This pledge has more than this amount in paid status. <br>Please enter more than this value.",
        "CANCEL_PLEDGE_ALERT" => "Are you sure you want to cancel this pledge ?",
        "ALREADY_ACTIVE_PLEDGE" =>"Pledge exists for this contact, select other contact."
    ),
    'Pledge_view_message' => array(
        "L_PLEDGE" => "Pledge",
        "L_SEARCH" => "Search",
        "L_VIEW" => "View",
        "L_DETAILS" => "Details",
        "L_EDIT" => "Edit",
        "L_TRANSACTIONS" => "Transactions",
        "L_ACTIVITIES" => "Activities",
        "L_CHANGE_LOG" => "Change Log",
        "L_PLEDGE_DETAILS" => "Pledge Details",
        "L_PLEDGE_HASH" => "Pledge #",
        "L_CONTACT_NAME" => "Contact Name",
        "L_ASSIGNED_TO" => "Assigned to",
        "L_TOTAL_PLEDGE_AMOUNT" => "Total Pledge Amount",
        "L_TO_BE_PAID_IN" => "To be paid in",
        "L_PAYMENTS_ARE_DUE_ON" => "Payment are due on",
        "L_APPLIES_TO_WEEK_MONTH_YEARLY_PAYMENT" => "This applies to weekly, monthly and yearly payments.",
        "L_PLEDGE_MADE" => "Pledge Made",
        "L_PLEDGE_MADE_BY_CONTRIBUTOR" => "Date when pledge was made by the contributor.",
        "L_PAYMENT_START" => "Payment Start",
        "L_DATE_FIRST_PLEDGE_PAYMENT" => "Date of first pledge payment.",
        "L_ACKNOWLEDGMENT_DATE" => "Acknowledgment Date",
        "L_ACKNOWLEDGMENT_DATE_TEXT" => "Date when an acknowledgment of the pledge was sent.",
        "L_CONTRIBUTION_TYPE" => "Contribution Type",
        "L_DEFAULT_CONTRIBUTION_TYPE" => "Sets the default contribution type for payments against this pledge.",
        "L_COMPAIGN" => "Campaign",
        "L_HONOREE_DETAILS" => "Honoree Details",
        "L_NAME" => "Name",
        "L_IN_HONOR_OF" => "In Honor of",
        "L_REMINDER" => "Reminder",
        "L_TURN_ON_REMINDER_SETTINGS" => "Turn On Reminder Settings",
        "L_STAFF" => "Staff",
        "L_CONTACT" => "Contact",
        "L_SEND_INITIAL_REMINDER" => "Send Initial Reminder",
        "L_SEND_INITIAL_REMINDER_TEXT" => "Days prior to each scheduled payment due date.",
        "L_SEND_UP_TO" => "Send Up to",
        "L_SEND_UP_TO_TEXT" => "Reminders for each scheduled payment.",
        "L_SEND_ADDITIONAL_REMINDER" => "Send Additional Reminder",
        "L_SEND_ADDITIONAL_REMINDER_TEXT" => "Days after the last one sent, up to the maximum number of reminders.",
        "L_IN_MEMORY_OF" => "In Memory of",
        "L_NONE" => "None",
        "L_INSTALLMENT_OF" => "Installment of $",
        "L_EVERY" => "Every",
        "NO_RECORD_FOUND" => "No Record Found.",
        'ACCOUNT_NUMBER_EMPTY' => 'Please enter account number.',
        'ACCOUNT_NUMBER_VALID' => 'Please enter valid account number.',
        'CHECK_NUMBER_EMPTY' => "Please enter cheque number.",
        'CHECK_NUMBER_VALID' => "Please enter valid cheque number.",
        'SUCCESSFULLY_UPDATED_PLEDGE' => 'Pledge updated successfully !'
    ),
    'Pledge_transaction_message' => array(
        'PAID_BY' => 'Paid By',
        'BANK_ACCOUNT_NO' => 'Bank Acc No:',
        'CHECK_NO' => 'Check No:',
        'ACCOUNT_NUMBER_EMPTY' => 'Please enter account number.',
        'ACCOUNT_NUMBER_VALID' => 'Please enter valid account number.',
        'CHECK_NUMBER_EMPTY' => "Please enter cheque number.",
        'CHECK_NUMBER_VALID' => "Please enter valid cheque number.",
        'SUCCESSFULLY_UPDATED_PLEDGE' => 'Pledge updated successfully !'
    )
);
?>