<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'Pledge\Controller\Pledge' => 'Pledge\Controller\PledgeController'
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'createPledge' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-pledge[/:contact_id]',
                    'defaults' => array(
                        'controller' => 'Pledge\Controller\Pledge',
                        'action' => 'addPledge',
                    ),
                ),
            ),
            'editPledge' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-pledge[/:pledge_id][/:mode]',
                    'defaults' => array(
                        'controller' => 'Pledge\Controller\pledge',
                        'action' => 'editPledge',
                    ),
                ),
            ),
            'pledges' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/pledges[/:pledge_status]',
                    'defaults' => array(
                        'controller' => 'Pledge\Controller\pledge',
                        'action' => 'getPledges',
                    ),
                ),
            ),
            'savePledgeSearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-crm-search-pledges',
                    'defaults' => array(
                        'controller' => 'Pledge\Controller\pledge',
                        'action' => 'addCrmSearchPledge',
                    ),
                ),
            ),
            'getPledgeSearchSavedParam' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-pledge-search-saved-param',
                    'defaults' => array(
                        'controller' => 'Pledge\Controller\pledge',
                        'action' => 'getPledgeSearchSavedParam',
                    ),
                ),
            ),
            'getPledgeSavedSearchList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-pledge-saved-search-list',
                    'defaults' => array(
                        'controller' => 'Pledge\Controller\pledge',
                        'action' => 'getPledgeSavedSearchList',
                    ),
                ),
            ),
            'deletePledgeSearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-pledge-search',
                    'defaults' => array(
                        'controller' => 'Pledge\Controller\pledge',
                        'action' => 'deletePledgeSearch',
                    ),
                ),
            ),
            'deletePledge' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-pledge',
                    'defaults' => array(
                        'controller' => 'Pledge\Controller\pledge',
                        'action' => 'deletePledge',
                    ),
                ),
            ),
            'getPledgeInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-pledge-info[/:pledge_id][/:mode]',
                    'defaults' => array(
                        'controller' => 'Pledge\Controller\pledge',
                        'action' => 'getPledgeInfo',
                    ),
                ),
            ),
            'viewPledgeDetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-pledge-detail[/:pledge_id]',
                    'defaults' => array(
                        'controller' => 'Pledge\Controller\pledge',
                        'action' => 'viewPledgeDetail',
                    ),
                ),
            ),
            'getPledgeTransactions' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-pledge-transactions[/:pledge_id]',
                    'defaults' => array(
                        'controller' => 'Pledge\Controller\pledge',
                        'action' => 'getPledgeTransactions',
                    ),
                ),
            ),
            'getPledgeActivities' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/pledge-activities[/]',
                    'defaults' => array(
                        'controller' => 'Activity\Controller\Activity',
                        'action' => 'getContactActivities',
                    ),
                ),
            ),
            'getPledgePaymentDetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-pledge-payment-detail[/:pledge_transaction_id]',
                    'defaults' => array(
                        'controller' => 'Pledge\Controller\pledge',
                        'action' => 'getPledgePaymentDetail',
                    ),
                ),
            ),
            'changePledgeStatus' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/change-pledge-status',
                    'defaults' => array(
                        'controller' => 'Pledge\Controller\pledge',
                        'action' => 'changePledgeStatus',
                    ),
                ),
            ),
            'contactpledges' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/contact-pledges[/:userid]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Pledge\Controller',
                        'controller' => 'Pledge',
                        'action' => 'contactPledges',
                    ),
                ),
            ),
            'searchcontactpledges' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/search-contact-pledges[/:userid]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Pledge\Controller',
                        'controller' => 'Pledge',
                        'action' => 'searchContactPledges',
                    ),
                ),
            ),
            'checkUserActivePledge' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/check-user-active-pledge',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Pledge\Controller',
                        'controller' => 'Pledge',
                        'action' => 'checkUserActivePledge',
                    ),
                ),
            ),
            
        )
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'navigation' => array(
        'default' => array(
            array(
                'label' => 'Pledge',
                'route' => 'pledges',
                'pages' => array(
                    array(
                        'label' => 'Search',
                        'route' => 'pledges',
                        'pages' => array(
                            array(
                                'label' => 'View',
                                'route' => 'viewPledgeDetail',
                                'action' => 'viewPledgeDetail',
                            ),
                            array(
                                'label' => 'Edit',
                                'route' => 'editPledge',
                                'action' => 'editPledge',
                            ),
                            array(
                                'label' => 'Change Log',
                                'route' => 'pledgechangelog',
                                'action' => 'change-log'
                            ),
                            array(
                                'label' => 'Transactions',
                                'route' => 'getPledgeTransactions',
                                'action' => 'getPledgeTransactions'
                            ),
                            array(
                                'label' => 'Activities',
                                'route' => 'getPledgeActivities',
                                'action' => 'getContactActivities'
                            ),
                        ),
                    ),
                    array(
                        'label' => 'Create',
                        'route' => 'createPledge',
                        'action' => 'addPledge',
                    )
                ),
            ),
        )
    ),
    'Pledge_messages' => array(
        'config' => include __DIR__ . '/../languages/en/language.php',
        'navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory'
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'Pledge' => __DIR__ . '/../view'
        ),
        'template_map' => array(
            'layout/custom' => __DIR__ . '/../../module/Common/layout/crm.phtml',
            'error/404' => __DIR__ . '/../../module/Common/error/404.phtml',
            'error/index' => __DIR__ . '/../../module/Common/error/index.phtml',
        ),
    ),
    'global_config' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
);

