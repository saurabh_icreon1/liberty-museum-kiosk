<?php

return array(
    "log" => array(
       "PAGE_TITLE" => "Change Log",
       "PASSENGER"  => "Passenger",
        "DASHBOARD"  => "Dashboard",
        "CELTIC"  => "Celtic",
        "CHANGE_LOG"  => "Change Log",
        "DETAILS"  => "Details",
        "EDIT"  => "Edit",
        "NO_RECORD_FOUND" => "No Record Found"
    ),
);
?>