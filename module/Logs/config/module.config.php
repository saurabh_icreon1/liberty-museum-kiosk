<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
return array(
    'controllers' => array(
        'invokables' => array(
            'Logs\Controller\Changelog' => 'Logs\Controller\ChangelogController',
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'changelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/ship-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'change-log',
                    ),
                ),
            ),
            'getchangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/ship-get-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'get-change-log',
                    ),
                ),
            ),
            'casechangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/case-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'change-log',
                    ),
                ),
            ),
            'getcasechangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/case-get-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'get-change-log',
                    ),
                ),
            ),
            'changelogpassenger' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/passenger-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'change-log',
                    ),
                ),
            ),
            'getchangelogpassenger' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/passenger-get-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'get-change-log',
                    ),
                ),
            ),
            'changeloguser' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'change-log',
                    ),
                ),
            ),
            'getchangeloguser' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-get-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'get-change-log',
                    ),
                ),
            ),
          'changelogprogram' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/program-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'change-log',
                    ),
                ),
            ),
           'getchangelogprogram' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/program-get-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'get-change-log',
                    ),
                ),
            ),
            'getchangeloggroup' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/group-get-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'get-change-log',
                    ),
                ),
            ),
            'campaignchangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/campaign-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'change-log',
                    ),
                ),
            ),
            'getcampaignchangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/campaign-get-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'get-change-log',
                    ),
                ),
            ),
            'shiplinechangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/shipline-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'change-log',
                    ),
                ),
            ),
            'getshiplinechangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/shipline-get-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'get-change-log',
                    ),
                ),
            ),
            'groupchangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/group-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'change-log',
                    ),
                ),
            ),
            'getgroupchangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/group-get-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'get-change-log',
                    ),
                ),
            ),
            'leadchangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/lead-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'change-log',
                    ),
                ),
            ),
            'getleadchangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/lead-get-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'get-change-log',
                    ),
                ),
            ),
            'activitychangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/activity-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'change-log',
                    ),
                ),
            ),
            'getactivitychangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/activity-get-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'get-change-log',
                    ),
                ),
			),	
			'pledgechangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/pledge-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'change-log',
                    ),
                ),
            ),
            'getpledgechangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/pledge-get-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'get-change-log',
                    ),
                ),
            ),
            'productchangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/product-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'change-log',
                    ),
                ),
            ),
            'getproductchangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/product-get-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'get-change-log',
                    ),
                ),
            ),
           'crmuserchangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crmuser-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'change-log',
                    ),
                ),
            ),
            'getcrmuserchangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crmuser-get-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'get-change-log',
                    ),
                ),
            ),
           'crmpromotionchangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/promotion-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'change-log',
                    ),
                ),
            ),
            'getpromotionchangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/promotion-get-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'get-change-log',
                    ),
                ),
            ),
            'getcrmuserchangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crmuser-get-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'get-change-log',
                    ),
                ),
            ),
            'crmgallerychangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crmgallery-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'change-log',
                    ),
                ),
            ),
            'getcrmgallerychangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crmgallery-get-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'get-change-log',
                    ),
                ),
            ),
            'blogchangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/blog-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'change-log',
                    ),
                ),
            ),
            'getblogchangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/blog-get-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'get-change-log',
                    ),
                ),
            ),          
            'quizchangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/quiz-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'change-log',
                    ),
                ),
            ),
            'getquizchangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/quiz-get-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'get-change-log',
                    ),
                ),
            ),
            'familyhistorychangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/family-history-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'change-log',
                    ),
                ),
            ),
            'getfamilyhistorychangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/family-history-get-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'get-change-log',
                    ),
                ),
            ),
            'transactionchangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/transaction-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'change-log',
                    ),
                ),
            ),
            'gettransactionchangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/transaction-get-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'get-change-log',
                    ),
                ),
            ),
            'manifestchangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/manifest-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'change-log',
                    ),
                ),
            ),
            'manifestgetchangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/manifest-get-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'get-change-log',
                    ),
                ),
            ),  
            'rmachangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/rma-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'change-log',
                    ),
                ),
            ),
            'rmagetchangelog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/rma-get-change-log[/:module][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Logs\Controller',
                        'controller' => 'Changelog',
                        'action' => 'get-change-log',
                    ),
                ),
            ),
            
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'change_log_tables' => array(
        'passenger' => 'tbl_pas_change_logs',
        'group' => 'tbl_grp_change_logs',
        'ship' => 'tbl_shp_ship_change_logs',
        'case' => 'tbl_cas_change_logs',
        'campaign' => 'tbl_cam_change_logs',
        'user' => 'tbl_usr_change_logs',
        'shipline' => 'tbl_shp_shipline_change_logs',
        'lead' => 'tbl_led_change_logs',
        'activity' => 'tbl_act_change_logs',
	'pledge' => 'tbl_ple_change_logs',
	'product' => 'tbl_pro_change_logs',
        'crmuser' => 'tbl_crm_user_change_logs',
        'promotion' => 'tbl_prm_change_logs',
        'crmgallery' => 'tbl_cms_gallery_change_logs',
        'blog' => 'tbl_blg_change_logs',
        'quiz' => 'tbl_qui_change_logs',
        'family-history' => 'tbl_usr_family_histories_change_logs',
        'transaction' => 'tbl_tra_change_logs',
        'manifest' => 'tbl_manifest_change_logs',
        'rma' => 'tbl_tra_rma_change_logs',
        'program' => 'tbl_mst_program_change_logs'
    ),
    'ChangelogMessages' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'logs' => __DIR__ . '/../view'
        ),
    ),
);
