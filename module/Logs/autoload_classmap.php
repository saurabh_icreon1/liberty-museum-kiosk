<?php 
return array(
    'Logs\Module'                       => __DIR__ . '/Module.php',
    'Logs\Controller\PassengerController'   => __DIR__ . '/src/Logs/Controller/ChangelogController.php',
    'Logs\Model\Changelog'            => __DIR__ . '/src/Logs/Model/Changelog.php',
    'Logs\Model\ChangelogTable'                   => __DIR__ . '/src/Logs/Model/ChangelogTable.php',
   
);
?>