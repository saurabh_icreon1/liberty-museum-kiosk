<?php
/**
* This is used for Logs module.
* @package    Logs
* @author     Icreon Tech -SR.
*/ 
 
namespace Logs\Model;
use Zend\Db\TableGateway\TableGateway;
class ChangelogTable
{
    protected $tableGateway;
    protected $connection; 
    protected $dbAdapter;
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
		$this->connection=$this->tableGateway->getAdapter()->getDriver()->getConnection(); 
    }
    
    /**
    * This function is used to get the change log result
    * @params this will be an array.
    * @return this will return all records of change logs
    * @author Icreon Tech -SR
    */ 
    public function getChangeLog($params = array())
    {
     
        $result = $this->connection->execute("CALL usp_log_getChangeLogs('".$params['logTable']."','".$params['id']."','".$params['crm_user_id']."','".$params['sortField']."','".$params['sortOrder']."','".$params['recOffset']."','".$params['recLimit']."')");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_OBJ);
        
        $statement->closeCursor();
        return $resultSet; 
    } 
    /**
    * This function is used to insert the records into the change log table
    * @params this will be an array.
    * @return this will return true
    * @author Icreon Tech -SR
    */ 
    public function insertChangeLog($params = array())
    {
		try {
	            $stmt = $this->dbAdapter->createStatement();
	            $stmt->prepare('CALL usp_pas_insertChangeLog(?,?,?,?,?)');
	            
	            /*$i=1;echo "<pre>";
	            print_r($params);
	            foreach($params as $key=>$val)
	            {
	               // echo $i."=>". $val;
	                //echo "<br>";
	               // echo '$stmt->getResource()->bindParam('.$i.', '.$val.')';
	               $stmt->getResource()->bindParam($i, $val);
	                $i++;
	            }*/

	            $stmt->getResource()->bindParam(1, $params['table_name']);
	            $stmt->getResource()->bindParam(2, $params['activity']);
	            $stmt->getResource()->bindParam(3, $params['id']);
	            $stmt->getResource()->bindParam(4, $params['crm_user_id']);
	            $stmt->getResource()->bindParam(5, $params['added_date']);
	            $result = $stmt->execute();
	            $statement = $result->getResource();
	          //  $resultSet = $statement->fetchAll(\PDO::FETCH_OBJ);
	            $statement->closeCursor();

            return true;
            } catch (Exception $e) {
            return false;
            }
        }        
       
}