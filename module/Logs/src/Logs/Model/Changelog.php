<?php
/**
* This is used for Logs module.
* @package    Logs
* @author     Icreon Tech -SR.
*/
 
namespace Logs\Model;
use Logs\Module;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\Adapter\Adapter;

class Changelog implements InputFilterAwareInterface 
{
    protected $inputFilter;
    protected $adapter;
    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    public function exchangeArray($data)
    {
 
    }
 
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory     = new InputFactory();
				

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}