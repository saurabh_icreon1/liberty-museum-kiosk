<?php

/**
 * This controller is used for Change Log module.
 * @package    Logs
 * @author     Icreon Tech -SR.
 */

namespace Logs\Controller;

use Base\Controller\BaseController;
use Zend\View\Model\ViewModel;
use Logs\Model\Changelog;
use Zend\Mvc\Controller\Plugin\Params;
use Zend\Authentication\AuthenticationService;

class ChangelogController extends BaseController {

    protected $_logTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_authservice = null;

    public function __construct() {
        $auth = new AuthenticationService();
        $this->auth = $auth;
        if ($this->auth->hasIdentity() === false) {
            //return $this->redirect()->toRoute('crm');
        }
    }

    /**
     * This function is used to get config variables
     * @return     array 
     * @param 
     * @author Icreon Tech - DG
     */
    public function getConfig() {
        $sm = $this->getServiceLocator();
        $this->_translator = $sm->get('translator');
        $this->_config = $sm->get('Config');
    }

    /**
     * This function is used to get table and config file object
     * @param array $form
     * @return     array 
     */
    public function getLogTable() {
        if (!$this->_logTable) {
            $sm = $this->getServiceLocator();
            $this->_logTable = $sm->get('Logs\Model\ChangelogTable');
            $this->_translator = $sm->get('translator');
            $this->_config = $sm->get('Config');
            $this->_adapter = $sm->get('dbAdapter');
        }

        return $this->_logTable;
    }

    /**
     * This Action is used to get the change log for passenger
     * @params this will pass passenger_id int
     * @return
     * @author Icreon Tech -SR
     */
    public function changeLogAction() {

        //$this->layout('crm');
        $this->getLogTable();
        $params = $this->params()->fromRoute();
        // $searchParam['passenger_id'] = $this->decrypt($params['id']);
        // $recordArray = $this->getServiceLocator()->get('Passenger\Model\DocumentTable')->searchCrmPassenger($searchParam);

        $id = $params['id'];
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'id' => $id,
            'moduleName' => $params['module'],
            'orgModuleName' => $this->decrypt($params['module']),
            'jsLangTranslate' => $this->_config['ChangelogMessages']['config']['log'],
            'source' => $this->decrypt($params['module'])
                //'recordArray' => $recordArray
        ));
        return $viewModel;
    }

    /**
     * This function is used to get the log listing
     * @params this will be an array $searchParam.
     * @return this will return records in json format
     * @author Icreon Tech -SR
     */
    public function getchangeLogAction() {
        $this->getLogTable();
        $request = $this->getRequest();
        $response = $this->getResponse();

        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        $this->getConfig();
        $searchParam = array();

        $this->decrypt($request->getPost('module'));
        $searchParam['logTable'] = $this->_config['change_log_tables'][$this->decrypt($request->getPost('module'))];
        if ($searchParam['logTable'] == 'tbl_manifest_change_logs') {
            $idParam = explode('~~', $request->getPost('id'));
            $searchParam['id'] = $this->decrypt($idParam[0]) . $this->decrypt($idParam[1]);
        } else {
            $searchParam['id'] = $this->decrypt($request->getPost('id'));
        }

        $searchParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
        $searchParam['recOffset'] = $start;
        $searchParam['recLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');
        $seachResult = $this->getLogTable()->getChangeLog($searchParam);
        $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
        $jsonResult = array();
        $arrCell = array();
        $subArrCell = array();
        $jsonResult['page'] = $page;
        $jsonResult['records'] = $countResult[0]->RecordCount;
        $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);

        if (count($seachResult) > 0) {
            foreach ($seachResult as $val) {
                $arrCell['id'] = $val->change_log_id;
                if ($val->activity == 1)
                    $activity = "Insert";
                if ($val->activity == 2)
                    $activity = "Update";
                if ($val->activity == 3)
                    $activity = "Delete";
                
                if($val->CreatedBy=='')
                    $val->CreatedBy = '--';

                $arrCell['cell'] = array($activity, $val->CreatedBy, $val->UpdatedBy, $this->OutputDateFormat($val->added_date, 'dateformatampm'));
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
        }
        return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

}
