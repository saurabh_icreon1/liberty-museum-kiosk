<?php

/**
 * This file defines all the controller and model
 * @package    User
 * @author     Icreon Tech - DG
 */

return array(
    'User\Module' => __DIR__ . '/Module.php',
    'User\Controller\SignupController' => __DIR__ . '/src/User/Controller/SignupController.php',
    'User\Controller\LoginController' => __DIR__ . '/src/User/Controller/LoginController.php',
    'User\Controller\ContactController' => __DIR__ . '/src/User/Controller/ContactController.php',
    'User\Controller\MembershipController' => __DIR__ . '/src/User/Controller/MembershipController.php',
    'User\Controller\RelationshipController' => __DIR__ . '/src/User/Controller/RelationshipController.php',
    'User\Controller\MergecontactController' => __DIR__ . '/src/User/Controller/MergecontactController.php',
    'User\Controller\SurveyController' => __DIR__ . '/src/User/Controller/SurveyController.php',
    'User\Model\User' => __DIR__ . '/src/User/Model/User.php',
    'User\Model\Contact' => __DIR__ . '/src/User/Model/Contact.php',
    'User\Model\Survey' => __DIR__ . '/src/User/Model/Survey.php',
    'User\Model\Membership' => __DIR__ . '/src/User/Model/Membership.php',
    'User\Model\Relationship' => __DIR__ . '/src/User/Model/Relationship.php',
    'User\Model\MergeContact' => __DIR__ . '/src/User/Model/MergeContact.php',
    'User\Model\UserTable' => __DIR__ . '/src/User/Model/UserTable.php',
    'User\Model\ContactTable' => __DIR__ . '/src/User/Model/ContactTable.php',
    'User\Model\SurveyTable' => __DIR__ . '/src/User/Model/SurveyTable.php',
    'User\Model\MembershipTable' => __DIR__ . '/src/User/Model/MembershipTable.php',
    'User\Model\RelationshipTable' => __DIR__ . '/src/User/Model/RelationshipTable.php',
    'User\Model\MergeContactTable' => __DIR__ . '/src/User/Model/MergeContactTable.php',
    'User\Model\AuthStorage' => __DIR__ . '/src/User/Model/AuthStorage.php',
);
?>