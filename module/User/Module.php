<?php

/**
 * This module file is used for initailize all obj of model and configuration setting
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\I18n\Translator\Translator;
use Zend\Validator\AbstractValidator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use User\Model\User;
use User\Model\UserTable;
use User\Model\Contact;
use User\Model\ContactTable;
use User\Model\Membership;
use User\Model\MembershipTable;
use User\Model\Survey;
use User\Model\SurveyTable;
use User\Model\Relationship;
use User\Model\RelationshipTable;
use User\Model\Development;
use User\Model\DevelopmentTable;
use User\Model\MergeContact;
use User\Model\MergeContactTable;
use User\Model\Crmuser;
use User\Model\CrmuserTable;
use User\Model\Fof;
use User\Model\FofTable;
use User\Model\Woh;
use User\Model\WohTable;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;

/**
 * This module file is used for initailize all obj of model and configuration setting
 * @package    User
 * @author     Icreon Tech - DG
 */
class Module implements AutoloaderProviderInterface, ConfigProviderInterface, ViewHelperProviderInterface {

    public function onBootstrap(MvcEvent $env) {
		//error_reporting(E_ALL);
        $translator = $env->getApplication()->getServiceManager()->get('translator');
        $eventManager = $env->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $translator->addTranslationFile(
                'phpArray', './module/User/languages/en/language.php', 'default', 'en_US'
        );
        
        $viewModel = $env->getApplication()->getMvcEvent()->getViewModel();
        $sm = $env->getApplication()->getServiceManager();        
        $this->_config = $sm->get('Config');
        $viewModel->imageFilePath = $imageFilePath = $this->_config['img_file_path']['path'];
        //AbstractValidator::setDefaultTranslator($translator);
    }

    public function authPreDispatch($event) {
                
    }
    
    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Get Autoloader Configuration
     * @return array
     */
    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    /**
     * Get Service Configuration
     *
     * @return array
     */
    public function getServiceConfig() {
        return array(
            'factories' => array(
                'User\Model\UserTable' => function($sm) {
                    $tableGateway = $sm->get('UserTableGateway');
                    $table = new UserTable($tableGateway);
                    return $table;
                },
                'UserTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new User($dbAdapter));
                    return new TableGateway('tbl_usr_users', $dbAdapter, null, $resultSetPrototype);
                },
                'User\Model\ContactTable' => function($sm) {
                    $tableGateway = $sm->get('ContactTableGateway');
                    $table = new ContactTable($tableGateway);
                    return $table;
                },
                'ContactTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Contact($dbAdapter));
                    return new TableGateway('tbl_usr_users', $dbAdapter, null, $resultSetPrototype);
                },
                'User\Model\MembershipTable' => function($sm) {
                    $tableGateway = $sm->get('MembershipTableGateway');
                    $table = new MembershipTable($tableGateway);
                    return $table;
                },
                'MembershipTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Membership($dbAdapter));
                    return new TableGateway('tbl_usr_memberships', $dbAdapter, null, $resultSetPrototype);
                },
                'User\Model\SurveyTable' => function($sm) {
                    $tableGateway = $sm->get('SurveyTableGateway');
                    $table = new SurveyTable($tableGateway);
                    return $table;
                },
                'SurveyTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Survey($dbAdapter));
                    return new TableGateway('tbl_usr_surveys', $dbAdapter, null, $resultSetPrototype);
                },
                'User\Model\DevelopmentTable' => function($sm) {
                    $tableGateway = $sm->get('DevelopmentTableGateway');
                    $table = new DevelopmentTable($tableGateway);
                    return $table;
                },
                'DevelopmentTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Development($dbAdapter));
                    return new TableGateway('tbl_usr_developments', $dbAdapter, null, $resultSetPrototype);
                },
                'User\Model\RelationshipTable' => function($sm) {
                    $tableGateway = $sm->get('RelationshipTableGateway');
                    $table = new RelationshipTable($tableGateway);
                    return $table;
                },
                'RelationshipTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Relationship($dbAdapter));
                    return new TableGateway('tbl_usr_relationships', $dbAdapter, null, $resultSetPrototype);
                },
                'User\Model\MergeContactTable' => function($sm) {
                    $tableGateway = $sm->get('MergeContactTableGateway');
                    $table = new MergeContactTable($tableGateway);
                    return $table;
                },
                'MergeContactTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new MergeContact($dbAdapter));
                    return new TableGateway('tbl_usr_users', $dbAdapter, null, $resultSetPrototype);
                },
                'User\Model\AuthStorage' => function($sm) {
                    return new \User\Model\AuthStorage();
                },
                'AuthServiceByUsername' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $dbTableAuthAdapter = new DbTableAuthAdapter($dbAdapter, 'tbl_usr_users', 'username', 'password', "MD5(CONCAT(?,salt))");
                    $select = $dbTableAuthAdapter->getDbSelect();
                    $select->where('is_delete = "0" and is_login_enabled="1" ');
                    $authService = new AuthenticationService();
                    $authService->setAdapter($dbTableAuthAdapter);
                    $authService->setStorage($sm->get('User\Model\AuthStorage'));
                    return $authService;
                },
                'AuthServiceByEmail' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $dbTableAuthAdapter = new DbTableAuthAdapter($dbAdapter, 'tbl_usr_users', 'email_id', 'password', "MD5(CONCAT(?,salt))");
                    $select = $dbTableAuthAdapter->getDbSelect();
                    $select->where('is_delete = "0" and is_login_enabled="1" ');
                    
                    $authService = new AuthenticationService();
                    $authService->setAdapter($dbTableAuthAdapter);
                    $authService->setStorage($sm->get('User\Model\AuthStorage'));
                    return $authService;
                },
                'AuthServiceByEmailCrm' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $dbTableAuthAdapter = new DbTableAuthAdapter($dbAdapter, 'tbl_crm_users', 'crm_email_id', 'crm_password', "MD5(CONCAT(?,crm_salt))");

                    $authService = new AuthenticationService();
                    $authService->setAdapter($dbTableAuthAdapter);
                    $authService->setStorage($sm->get('User\Model\AuthStorage'));
                    return $authService;
                },
                'User\Model\CrmuserTable' => function($sm) {
                    $tableGateway = $sm->get('CrmuserTableGateway');
                    $table = new CrmuserTable($tableGateway);
                    return $table;
                },
                'CrmuserTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Crmuser($dbAdapter));
                    return new TableGateway('tbl_crm_users', $dbAdapter, null, $resultSetPrototype);
                },
                'User\Model\FofTable' => function($sm) {
                    $tableGateway = $sm->get('FofTableGateway');
                    $table = new FofTable($tableGateway);
                    return $table;
                },
                'FofTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new User($dbAdapter));
                    return new TableGateway('tbl_tra_cart_flag_of_faces', $dbAdapter, null, $resultSetPrototype);
                },
                'User\Model\WohTable' => function($sm) {
                    $tableGateway = $sm->get('WohTableGateway');
                    $table = new WohTable($tableGateway);
                    return $table;
                },
                'WohTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new User($dbAdapter));
                    return new TableGateway('tbl_tra_cart_wall_of_honor', $dbAdapter, null, $resultSetPrototype);
                },   
				'UserMembership' => function ($sm) {
					return new Service\UserMembership();
				},				
                'dbAdapter' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    return $dbAdapter;
                },
            ),
        );
    }

    /**
     * Get View Helper Configuration
     *
     * @return array
     */
    public function getViewHelperConfig() {
        return array(
            'factories' => array(
            // the array key here is the name you will call the view helper by in your view scripts
//                'absoluteUrl' => function($sm) {
//                    $locator = $sm->getServiceLocator(); // $sm is the view helper manager, so we need to fetch the main service manager
//                    return new AbsoluteUrl($locator->get('Request'));
//                },
            ),
        );
    }

    /**
     * Get Controller Configuration
     *
     * @return array
     */
    public function getControllerConfig() {
        return array();
    }

    public function loadConfiguration(MvcEvent $env) {
        $application = $env->getApplication();
        $serviceManager = $application->getServiceManager();
        $sharedManager = $application->getEventManager()->getSharedManager();
        $router = $serviceManager->get('router');
        $request = $serviceManager->get('request');

//        $matchedRoute = $router->match($request);
//        if (null !== $matchedRoute) {
//            $sharedManager->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function($e) use ($serviceManager) {
//                        $serviceManager->get('ControllerPluginManager')->get('AclPlugin')
//                                ->doAuthorization($e);
//                    }, 2
//            );
//        }
    }

    public function loadCommonViewVars(MvcEvent $env) {
        $env->getViewModel()->setVariables(array(
            'auth' => $env->getApplication()->getServiceManager()->get('AuthService')
        ));
    }

}
