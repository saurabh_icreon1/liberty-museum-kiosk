<?php

/**
 * This model is used for user.
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This model is used for user.
 * @package    User
 * @author     Icreon Tech - DG
 */
class SurveyTable {

    protected $tableGateway;
    protected $connection;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
        $this->connection = $this->tableGateway->getAdapter()->getDriver()->getConnection();
    }

    /**
     * Function for get Security Question
     * @author Icreon Tech - DG
     * @return array
     */
    public function getSurveyQuestions() {
        $result = $this->connection->execute('CALL usp_com_getSurveyQuestion');
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();

        if (!$resultSet) {
            throw new \Exception("Could not find record.");
        }
        return $resultSet;
    }

    /**
     * Function for insert user survey
     * @author Icreon Tech - DG
     * @return array
     */
    public function insertUserSurvey($dataArr) {
		 
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
		$additional_info = isset($dataArr['additional_info']) ? $dataArr['additional_info'] : null;
        $survey_question = isset($dataArr['survey_question']) ? $dataArr['survey_question'] : null;
        $survey_answer = isset($dataArr['survey_answer']) ? $dataArr['survey_answer'] : '0';
        $added_date = DATE_TIME_FORMAT;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_insertUserSurvey(?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $user_id);
		$stmt->getResource()->bindParam(2, $additional_info);
        $stmt->getResource()->bindParam(3, $survey_question);
        $stmt->getResource()->bindParam(4, $survey_answer);
        $stmt->getResource()->bindParam(5, $added_date);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for delete user survey
     * @author Icreon Tech - DG
     * @return array
     */
    public function deleteUserSurvey($dataArr) {
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_deleteUserSurvey(?)');
        $stmt->getResource()->bindParam(1, $user_id);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for get user survey
     * @author Icreon Tech - DG
     * @return array
     */
    public function getUserSurvey($dataArr) {
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_getUserSurvey(?)');
        $stmt->getResource()->bindParam(1, $user_id);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
    }
    
    /**
     * Function for get user is exits ?
     * @author Icreon Tech - DG
     * @return array
     */
    public function isUserSurvey($dataArr) {
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_isUserSurvey(?)');
        $stmt->getResource()->bindParam(1, $user_id);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet[0]['totalRecord'];
    }

}