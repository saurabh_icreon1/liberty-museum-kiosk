<?php

/**
 * This model is used for crm user and role validation and data filter.
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace User\Model;

use Base\Model\BaseModel;
use User\Model;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\Adapter\Adapter;

/**
 * This class is used for crm user and role validation and data filter.
 * @package    User
 * @author     Icreon Tech - AP
 */
class Crmuser extends BaseModel {

    protected $inputFilter;
    protected $adapter;
    public $first_name;
    public $middle_name;
    public $last_name;
    public $email_id;
    public $role;
    public $password;
    public $password_conf;
    public $status;
    public $address;
    public $country_id;
    public $city;
    public $state;
    public $zip;
    public $virtualimage;

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    /**
     * Function used to check validation for crm create user form
     * @param Array
     * @return array
     * @author Icreon Tech - AP
     */
    public function getInputFilter() {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                        'name' => 'state',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'state_select',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'crm_email_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_EMAIL',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'Db\NoRecordExists',
                                'options' => array(
                                    'table' => 'tbl_crm_users',
                                    'field' => 'crm_email_id',
                                    'adapter' => $this->adapter,
                                    'messages' => array(
                                        'recordFound' => 'EMAIL_ID_ALREADY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'password',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_PASSWORD'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    //'pattern' => '/^(?=.*\d)(?=.*[a-z]).{6,25}$/',
                                    'pattern' => '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,25}$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'PASSWORD_INVALID'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'messages' => array(
                                        'stringLengthTooShort' => 'PASSWORD_LENGTH_MIN',
                                        'stringLengthTooLong' => 'PASSWORD_LENGTH_MAX'
                                    ),
                                    'encoding' => 'UTF-8',
                                    'min' => 6,
                                    'max' => 25,
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'password_conf',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_CON_PASSWORD'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'Identical',
                                'options' => array(
                                    'token' => 'password',
                                    'messages' => array(
                                        \Zend\Validator\Identical::NOT_SAME => 'PASSWORD_CONFIRM_NOT_MATCHED'
                                    ),
                                ),
                            ),
                            ))));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'status',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'role',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'report_to',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'last_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_LAST_NAME',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'phone_no',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'address',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_ADDRESS',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'city',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_CITY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            
            $inputFilter->add($factory->createInput(array(
                        'name' => 'zip',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_ZIP',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'country_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'virtualface',
                        'required' => false,
//                        'filters' => array(
//                            array('name' => 'StripTags'),
//                            array('name' => 'StringTrim'),
//                        ),
//                        'validators' => array(
//                            array(
//                                'name' => 'NotEmpty',
//                                'options' => array(
//                                    'messages' => array(
//                                        'isEmpty' => 'EMPTY_Image',
//                                    ),
//                                ),
//                            ),
//                    
//                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                'name'     => 'csrf',
                'required' => false,
                'validators' => array(
                            array(
                                'name' => 'csrf',
                                'options' => array(
                                    'messages' => array(
                                        'notSame' => 'NOT_SAME',
                                    ),
                                ),
                            ),
                        ),
            )));


            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * Function used to check validation for crm edit user form
     * @param Array	 
     * @return array
     * @author Icreon Tech - AP
     */
    public function getEditFromInputFilter($crm_user_id='') {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                        'name' => 'crm_email_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_EMAIL',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'Db\NoRecordExists',
                                'options' => array(
                                    'table' => 'tbl_crm_users',
                                    'field' => 'crm_email_id',
                                    'adapter' => $this->adapter,
                                    'exclude' => array('field' => 'crm_user_id', 'value' => $crm_user_id),
                                    'messages' => array(
                                        'recordFound' => 'EMAIL_ID_ALREADY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'state_select',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'status',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'role',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'report_to',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'last_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_LAST_NAME',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'phone_no',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'address',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_ADDRESS',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'city',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_CITY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'state',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
//                        'validators' => array(
//                            array(
//                                'name' => 'NotEmpty',
//                                'options' => array(
//                                    'messages' => array(
//                                        'isEmpty' => 'EMPTY_STATE',
//                                    ),
//                                ),
//                            ),
//                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'zip',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_ZIP',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'country_id',
                        'required' => false,
                    )));
					
            $inputFilter->add($factory->createInput(array(
                'name'     => 'csrf',
                'required' => false,
                'validators' => array(
                            array(
                                'name' => 'csrf',
                                'options' => array(
                                    'messages' => array(
                                        'notSame' => 'NOT_SAME',
                                    ),
                                ),
                            ),
                        ),
            )));				
					
            $inputFilter->add($factory->createInput(array(
                        'name' => 'virtualface',
                        'required' => false,
//                        'filters' => array(
//                            array('name' => 'StripTags'),
//                            array('name' => 'StringTrim'),
//                        ),
//                        'validators' => array(
//                            array(
//                                'name' => 'NotEmpty',
//                                'options' => array(
//                                    'messages' => array(
//                                        'isEmpty' => 'EMPTY_Image',
//                                    ),
//                                ),
//                            ),
//                    
//                        ),
                    )));



            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * Function used to check validation for crm user role form
     * @param Array
     * @return array
     * @author Icreon Tech - AP
     */
    public function getRoleInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
			$inputFilter->add($factory->createInput(array(
                        'name' => 'role_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_ROLE_NAME',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'Db\NoRecordExists',
                                'options' => array(
                                    'table' => 'tbl_acl_roles',
                                    'field' => 'role_name',
                                    'adapter' => $this->adapter,
									'exclude' => array(
										'field' => 'is_deleted',
										'value' => 1
									),
                                    'messages' => array(
                                        'recordFound' => 'ROLE_ALREADY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                'name'     => 'csrf',
                'required' => true,
                'validators' => array(
                            array(
                                'name' => 'csrf',
                                'options' => array(
                                    'messages' => array(
                                        'notSame' => 'NOT_SAME',
                                    ),
                                ),
                            ),
                        ),
            )));



            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    /**
     * Function used to for save serach input filter
     * @param Array
     * @return array
     * @author Icreon Tech - AP
     */
    public function getInputFilterCrmSaveSearch($filterData) {
        $inputFilterData = array();
        if (is_array($filterData)) {
            foreach ($filterData as $key => $val) {
                $inputFilterData[$key] = trim(addslashes(strip_tags($val)));
            }
        } else {
            $inputFilterData = trim(addslashes(strip_tags($filterData)));
        }
        return $this->inputFilterCrmSearch = $inputFilterData;
    }

    /**
     * Function used to check variable for update user 
     * @param Array
     * @return array
     * @author Icreon Tech - AP
     */
    public function filterUpdatedUserParam($param =array()) {

        $parameter['activity'] = (isset($param['activity'])) ? $param['activity'] : null;
        $parameter['crm_user_id'] = (isset($param['ids'])) ? implode(',', $param['ids']) : null;
        $parameter['modified_date'] = $param['modified_date'];
        return $parameter;
    }

    /**
     * Function used to check variable for user  role
     * @param Array 
     * @return array
     * @author Icreon Tech - AP
     */
    public function filterRoleParam($data) {
        $parameter = array();

        $parameter['role'] = (isset($data['role_name'])) ? $data['role_name'] : null;


        $parameter['add_date'] = $data['add_date'];
        $parameter['modified_date'] = isset($data['modified_date']) ? $data['modified_date'] : null;

        $parameter['role_id'] = (isset($data['role_id'])) ? $data['role_id'] : null;

        return $parameter;
    }

    /**
     * Function used to check variable for crm user 
     * @param Array
     * @return array
     * @author Icreon Tech - AP
     */
    public function filterParam($data) {
        $parameter = array();

        $parameter['crm_email_id'] = (isset($data['crm_email_id'])) ? $data['crm_email_id'] : null;
        if (isset($data['password'])) {
            $parameter['crm_password'] = $data['password'];
            $parameter['salt'] = $this->generateRandomString();
            $password = $parameter['crm_password'];
            $parameter['password'] = md5($password . $parameter['salt']);
            $parameter['genrated_password'] = $parameter['password'];
        }
        $parameter['crm_is_active'] = (isset($data['status'])) ? $data['status'] : null;
        $parameter['crm_user_role'] = (isset($data['role'])) ? implode(',', $data['role']) : null;
        $parameter['report_crm_user_id'] = (isset($data['report_to'])) ? $data['report_to_id'] : null;
        $parameter['crm_first_name'] = (isset($data['first_name'])) ? $data['first_name'] : null;
        $parameter['crm_last_name'] = (isset($data['last_name'])) ? $data['last_name'] : null;
        $parameter['crm_phone'] = (isset($data['phone_no'])) ? $data['phone_no'] : null;
        $parameter['crm_address'] = (isset($data['address'])) ? $data['address'] : null;
        $parameter['crm_city'] = (isset($data['city'])) ? $data['city'] : null;
        $parameter['crm_state'] = (isset($data['state'])) ? $data['state'] : '0';
        $parameter['crm_zip_code'] = (isset($data['zip'])) ? $data['zip'] : null;
        $parameter['crm_country_id'] = (isset($data['country_id'])) ? $data['country_id'] : null;
        $parameter['crm_user_picture'] = (isset($data['thumbnail_virtualface'])) ? $data['thumbnail_virtualface'] : null;

        $parameter['add_date'] = isset($data['add_date']) ? $data['add_date'] : null;
        $parameter['modified_date'] = isset($data['modified_date']) ? $data['modified_date'] : null;
        $parameter['crm_user_id'] = (isset($data['crm_user_id'])) ? $data['crm_user_id'] : null;

        return $parameter;
    }

    /**
     * Function for get random number
     * @param void
     * @return array
     * @author Icreon Tech - AP
     */
    public function generateRandomString() {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ23456789";
        srand((double) microtime() * 1000000);
        $var = 0;
        $code = '';
        while ($var <= 7) {
            $num = rand() % 33;
            $tmp = substr($chars, $num, 1);
            $code = $code . $tmp;
            $var++;
        }
        return $code;
    }

    /**
     * Function used to check variables cases
     * @param Array
     * @return void
     * @author Icreon Tech - AP
     */
    public function exchangeArray($data=array()) {
        $parameter = array();
        if (isset($data['password'])) {
            $parameter['crm_password'] = $data['password'];
            $parameter['salt'] = $this->generateRandomString();
            $password = $parameter['crm_password'];
            $parameter['password'] = md5($password . $parameter['salt']);
            $parameter['genrated_password'] = $parameter['password'];
        }
        $parameter['modified_date'] = isset($data['modified_date']) ? $data['modified_date'] : null;
        $parameter['modified_by'] = isset($data['modified_by']) ? $data['modified_by'] : null;
        $parameter['crm_user_id'] = (isset($data['crm_user_id'])) ? $data['crm_user_id'] : null;

        return $parameter;
    }

    /**
     * Function used to filter data
     * @param Array
     * @return array
     * @author Icreon Tech - AP
     */
    public function getPasswordInputFilter() {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                        'name' => 'password',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_PASSWORD'
                                    ),
                                ),
                            ),                            
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'messages' => array(
                                        'stringLengthTooShort' => 'PASSWORD_LENGTH_MIN',
                                        'stringLengthTooLong' => 'PASSWORD_LENGTH_MAX'
                                    ),
                                    'encoding' => 'UTF-8',
                                    'min' => 6,
                                    'max' => 25,
                                ),
                            ),
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    //'pattern' => '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,25}$/',
                                    'pattern' => '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,25}$/',
                                    //'pattern' => '/^(?=.*\d)(?=.*[a-z]).{6,25}$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'PASSWORD_INVALID'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'password_conf',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_CON_PASSWORD'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'Identical',
                                'options' => array(
                                    'token' => 'password',
                                    'messages' => array(
                                        \Zend\Validator\Identical::NOT_SAME => 'PASSWORD_CONFIRM_NOT_MATCHED'
                                    ),
                                ),
                            ),
                            ))));
            $inputFilter->add($factory->createInput(array(
                'name'     => 'csrf',
                'required' => true,
                'validators' => array(
                            array(
                                'name' => 'csrf',
                                'options' => array(
                                    'messages' => array(
                                        'notSame' => 'NOT_SAME',
                                    ),
                                ),
                            ),
                        ),
            )));


            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
    
    public function getCrmuserdashlets($params = array()){
        $returnArray = array();
        $returnArray[] = (isset($params['role_id']) && $params['role_id']!='')?$params['role_id']:'';
        $returnArray[] = (isset($params['dashlet_id']) && $params['dashlet_id']!='')?$params['dashlet_id']:'';
        return $returnArray;
    }

}