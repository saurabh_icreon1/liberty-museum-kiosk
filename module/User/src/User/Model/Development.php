<?php

/**
 * This model is used for user Development.
 * @package    User_Development
 * @author     Icreon Tech - DG
 */

namespace User\Model;

use User\Module;
// Add these import statements
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\Adapter\Adapter;

/**
 * This model is used for user Development.
 * @package    User_Development
 * @author     Icreon Tech - DG
 */
class Development implements InputFilterAwareInterface {

    protected $inputFilter;
    protected $adapter;

    public function __construct(Adapter $adapter) {

        $this->adapter = $adapter;
    }

    public function exchangeArray($data) {
        
    }

    public function getInputFilter() {
        
    }

    public function exchangeArrayDevelopmet($data) {
        
    }

    public function getInputFilterDevelopmet() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'total_given_amount',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'total_no_of_gifts',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'last_gift_date',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'last_gift_amount',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'largest_gift_date',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'largest_gift_amount',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'total_income',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'total_real_estate_value',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'total_stock_value',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'total_pension_value',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'total_political_non_profit_giving_amount',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'income_portion_of_egc_calculation',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'pol_non_profit_giving_portion_of_egc_calc',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'rating_summary_giving_capacity_rating',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'rating_summary_giving_capacity_range',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'estimated_giving_capacity',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'propensity_to_give_score',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'propensity_to_give_score_part_2',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'combined_propensity_to_give_score',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'planned_giving_bequest',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'planned_giving_annuity',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'planned_giving_trust',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'rating_summary_influence',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'rating_summary_inclination',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'total_quality_of_match',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'charitable_donations_of_qom',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'trustees_qom',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'guidestar_directors_qom',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'guidestar_foundations_qom',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'hoover_business_information_qom',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'household_profile_qom',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'lexis_nexis_qom',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'major_donor_qom',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'philanthropic_donations_qom',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'volunteers_and_directors_qom',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'wealth_id_securities_qom',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'bio_information',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'date',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'amount',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'transaction',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'campaign_code',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'sort_by',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'description',
                        'required' => false,
                    )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add content to these methods:
    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    // Add the following method:
    public function getArrayCopy() {
        return get_object_vars($this);
    }

    /*
     * Refine Development Data Array Variables
     * @return Array
     * @author Icreon Tech - DG
     * 
     */

    public function getDevelopmentDataArray($form_data) {
        $development_form = array();
        $development_form['total_given_amount'] = $form_data['total_given_amount'];
        $development_form['total_no_of_gifts'] = $form_data['total_no_of_gifts'];
        $development_form['last_gift_date'] = $form_data['last_gift_date'];
        $development_form['last_gift_amount'] = $form_data['last_gift_amount'];
        $development_form['largest_gift_date'] = $form_data['largest_gift_date'];
        $development_form['largest_gift_amount'] = $form_data['largest_gift_amount'];
        $development_form['total_income'] = $form_data['total_income'];
        $development_form['total_real_estate_value'] = $form_data['total_real_estate_value'];
        $development_form['total_stock_value'] = $form_data['total_stock_value'];
        $development_form['total_pension_value'] = $form_data['total_pension_value'];
        $development_form['total_political_non_profit_giving_amount'] = $form_data['total_political_non_profit_giving_amount'];
        $development_form['income_portion_of_egc_calculation'] = $form_data['income_portion_of_egc_calculation'];
        $development_form['pol_non_profit_giving_portion_of_egc_calc'] = $form_data['pol_non_profit_giving_portion_of_egc_calc'];
        $development_form['rating_summary_giving_capacity_rating'] = $form_data['rating_summary_giving_capacity_rating'];
        $development_form['rating_summary_giving_capacity_range'] = $form_data['rating_summary_giving_capacity_range'];
        $development_form['estimated_giving_capacity'] = $form_data['estimated_giving_capacity'];
        $development_form['propensity_to_give_score'] = $form_data['propensity_to_give_score'];
        $development_form['propensity_to_give_score_part_2'] = $form_data['propensity_to_give_score_part_2'];
        $development_form['combined_propensity_to_give_score'] = $form_data['combined_propensity_to_give_score'];
        $development_form['planned_giving_bequest'] = $form_data['planned_giving_bequest'];
        $development_form['planned_giving_annuity'] = $form_data['planned_giving_annuity'];
        $development_form['planned_giving_trust'] = $form_data['planned_giving_trust'];
        $development_form['rating_summary_influence'] = $form_data['rating_summary_influence'];
        $development_form['rating_summary_inclination'] = $form_data['rating_summary_inclination'];
        $development_form['total_quality_of_match'] = $form_data['total_quality_of_match'];
        $development_form['charitable_donations_of_qom'] = $form_data['charitable_donations_of_qom'];
        $development_form['trustees_qom'] = $form_data['trustees_qom'];
        $development_form['guidestar_directors_qom'] = $form_data['guidestar_directors_qom'];
        $development_form['guidestar_foundations_qom'] = $form_data['guidestar_foundations_qom'];
        $development_form['hoover_business_information_qom'] = $form_data['hoover_business_information_qom'];
        $development_form['household_profile_qom'] = $form_data['household_profile_qom'];
        $development_form['lexis_nexis_qom'] = $form_data['lexis_nexis_qom'];
        $development_form['major_donor_qom'] = $form_data['major_donor_qom'];
        $development_form['philanthropic_donations_qom'] = $form_data['philanthropic_donations_qom'];
        $development_form['volunteers_and_directors_qom'] = $form_data['volunteers_and_directors_qom'];
        $development_form['wealth_id_securities_qom'] = $form_data['wealth_id_securities_qom'];
        $development_form['bio_information'] = $form_data['bio_information'];
        $development_form['description'] = $form_data['description'];        
        return $development_form;
    }

}