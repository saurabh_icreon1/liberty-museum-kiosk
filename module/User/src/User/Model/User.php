<?php

/**
 * This model is used for user validation.
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Model;

use User\Module;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\Adapter\Adapter;

/**
 * This model is used for user validation.
 * @package    User
 * @author     Icreon Tech - DG
 */
class User implements InputFilterAwareInterface {

    protected $inputFilter;
    protected $adapter;
    public $first_name;
    public $middle_name;
    public $last_name;
    public $email_id;
    public $email_id_conf;
    public $password;
    public $password_conf;
    public $age;
    public $abecome_18ge;
    public $country_id;
    public $security_question_id;
    public $security_answer;
    public $agree;
    public $verify_code;
    public $user_name;
    public $rememberme;
    public $email;
    public $confirm_email;
    public $user_id;
    public $conf_email_id;
    public $title;
    public $contact_id;
    public $suffix;
    public $security_question;
    public $confirm_security_answer;
    public $upload_profile_image;
    public $upload_profile_image_id;
    public $phone_id;
    public $extension;
    public $phone_no;
    public $area_code;
    public $country_code;
    public $phone_type;
    public $corporate_company_name;
    public $corporate_legal_name;
    public $corporate_sic_code;
    public $is_primary;

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    /**
     * Function used to check variable for user forgot form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function exchangeArrayForgot($data) {
        if (!empty($data['email_id'])) {
            $this->email_id = (isset($data['email_id'])) ? $data['email_id'] : null;
        }
    }

    /**
     * Function used to check validation for user forgot form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function getInputFilterForgot() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'csrf',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'csrf',
                                'options' => array(
                                    'messages' => array(
                                        'notSame' => 'CSRF_NOT_SAME',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'email_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMAIL_EMPTY',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'EmailAddress',
                                'options' => array(
                                    'messages' => array(
                                        \Zend\Validator\EmailAddress::INVALID_FORMAT => "EMAIL_INVALID",
                                    ),
                                ),
                            ),
                        ),
                    )));
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * Function used to check validation for token
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function addTokenInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'csrf'
//                        'required' => false
//                        'validators' => array(
//                            array(
//                                'name' => 'csrf',
//                                'options' => array(
//                                    'messages' => array(
//                                        'notSame' => 'CSRF_NOT_SAME',
//                                    ),
//                                ),
//                            ),
//                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'first_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_FIRST_NAME',
                                    ),
                                ),
                            ),
                        ),
                    )));
//            $inputFilter->add($factory->createInput(array(
//                        'name' => 'middle_name',
//                        'required' => false,
//                    )));
//            $inputFilter->add($factory->createInput(array(
//                        'name' => 'middle_name',
//                        'required' => true,
//                        'filters' => array(
//                            array('name' => 'StripTags'),
//                            array('name' => 'StringTrim'),
//                        ),
//                        'validators' => array(
//                            array(
//                                'name' => 'NotEmpty',
//                                'options' => array(
//                                    'messages' => array(
//                                        'isEmpty' => 'EMPTY_MIDDLE_NAME',
//                                    ),
//                                ),
//                            ),
//                        ),
//                    )));
//            $inputFilter->add($factory->createInput(array(
//                        'name' => 'last_name',
//                        'required' => true,
//                        'filters' => array(
//                            array('name' => 'StripTags'),
//                            array('name' => 'StringTrim'),
//                        ),
//                        'validators' => array(
//                            array(
//                                'name' => 'NotEmpty',
//                                'options' => array(
//                                    'messages' => array(
//                                        'isEmpty' => 'EMPTY_LAST_NAME',
//                                    ),
//                                ),
//                            ),
//                        ),
//                    )));
//            $inputFilter->add($factory->createInput(array(
//                        'name' => 'card_type',
//                        'required' => true,
//                        'filters' => array(
//                            array('name' => 'StripTags'),
//                            array('name' => 'StringTrim'),
//                        ),
//                        'validators' => array(
//                            array(
//                                'name' => 'NotEmpty',
//                                'options' => array(
//                                    'messages' => array(
//                                        'isEmpty' => 'EMPTY_CARD_TYPE',
//                                    ),
//                                ),
//                            ),
//                        ),
//                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'card_number',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_CARD_NUMBER',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'expiration_month',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_EXP_MONTH',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'expiration_year',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_EXP_YEAR',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'card_ccv',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_CCV',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'credit_card_type',
                        'required' => false
                    )));

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * Function used to check variables for password form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function exchangeArrayPassword($data) {
        if (!empty($data['email_id'])) {
            $this->email_id = (isset($data['email_id'])) ? $data['email_id'] : null;
        }
        if (!empty($data['password'])) {
            $this->password = (isset($data['password'])) ? $data['password'] : null;
        }
        if (!empty($data['password_conf'])) {
            $this->password_conf = (isset($data['password_conf'])) ? $data['password_conf'] : null;
        }
    }

    /**
     * Function used to check validation for user password form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function getInputFilterPassword() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'csrf',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'csrf',
                                'options' => array(
                                    'messages' => array(
                                        'notSame' => 'CSRF_NOT_SAME',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'code',
                        'required' => true,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'email_id',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMAIL_EMPTY'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'EmailAddress',
                                'options' => array(
                                    'messages' => array(
                                        \Zend\Validator\EmailAddress::INVALID_FORMAT => "EMAIL_INVALID",
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'password',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'PASSWORD_EMPTY'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^(?=.*\d)(?=.*[a-zA-z]).{6,25}$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'PASSWORD_INVALID'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'messages' => array(
                                        'stringLengthTooShort' => 'PASSWORD_LENGTH_MIN',
                                        'stringLengthTooLong' => 'PASSWORD_LENGTH_MAX'
                                    ),
                                    'encoding' => 'UTF-8',
                                    'min' => 6,
                                    'max' => 25,
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'password_conf',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'PASSWORD_CONFIRM_EMPTY'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'Identical',
                                'options' => array(
                                    'token' => 'password',
                                    'messages' => array(
                                        \Zend\Validator\Identical::NOT_SAME => 'PASSWORD_CONFIRM_NOT_MATCHED'
                                    ),
                                ),
                            ),
                            ))));
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * Function used to check variables for update email form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function exchangeArrayUpdateEmail($data) {
        if (!empty($data['email'])) {
            $this->email = (isset($data['email'])) ? $data['email'] : null;
        }
        if (!empty($data['confirm_email'])) {
            $this->confirm_email = (isset($data['confirm_email'])) ? $data['confirm_email'] : null;
        }
        if (!empty($data['user_id'])) {
            $this->user_id = (isset($data['user_id'])) ? $data['user_id'] : null;
        }
    }

    /**
     * Function used to check validation for update email form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function getInputFilterUpdateEmail() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'user_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'USER_ID_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'email',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMAIL_EMPTY',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'EmailAddress',
                                'options' => array(
                                    'messages' => array(
                                        \Zend\Validator\EmailAddress::INVALID_FORMAT => "EMAIL_INVALID",
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'Db\NoRecordExists',
                                'options' => array(
                                    'table' => 'tbl_usr_users',
                                    'field' => 'email_id',
                                    'adapter' => $this->adapter,
                                    'messages' => array(
                                        'recordFound' => 'EMAIL_ID_ALREADY',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'confirm_email',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'CONFIRM_EMAIL_EMPTY',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'Identical',
                                'options' => array(
                                    'token' => 'email',
                                    'messages' => array(
                                        \Zend\Validator\Identical::NOT_SAME => 'CONFIRM_EMAIL_NOT_MATCHED'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'csrf',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'csrf',
                                'options' => array(
                                    'messages' => array(
                                        'notSame' => 'CSRF_NOT_SAME',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * Function used to check variables for user login form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function exchangeArrayLogin($data) {
        if (!empty($data['user_name'])) {
            $this->user_name = (isset($data['user_name'])) ? $data['user_name'] : null;
        }
        if (!empty($data['password'])) {
            $this->password = (isset($data['password'])) ? $data['password'] : null;
        }
        if (!empty($data['rememberme'])) {
            $this->rememberme = (isset($data['rememberme'])) ? $data['rememberme'] : null;
        }
    }

    /**
     * Function used to check validation for user login form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function getInputFilterLogin() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'user_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'USERNAME_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'password',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'PASSWORD_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            /*
              $inputFilter->add($factory->createInput(array(
              'name' => 'csrf',
              'required' => true,
              'validators' => array(
              array(
              'name' => 'csrf',
              'options' => array(
              'messages' => array(
              'notSame' => 'CSRF_NOT_SAME',
              ),
              ),
              ),
              ),
              ))); */

            $inputFilter->add($factory->createInput(array(
                        'name' => 'rememberme',
                        'required' => false,
                    )));
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * Function used to check variable for user email vewrification form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function exchangeArrayEmailVerify($data) {
        if (!empty($data['referal_path_url'])) {
            $this->referal_path_url = (isset($data['referal_path_url'])) ? $data['referal_path_url'] : "";
        }
        if (!empty($data['verify_code'])) {
            $this->verify_code = (isset($data['verify_code'])) ? $data['verify_code'] : null;
        }
    }

    /**
     * Function used to check validation for email verification form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function getInputFilterEmailVerify() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'verify_code',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'ENTER_VERIFY_CODE',
                                    ),
                                ),
                            ),
                        ),
                    )));
           /* $inputFilter->add($factory->createInput(array(
                        'name' => 'csrf',
                        'required' => false,
                       'validators' => array(
                            array(
                                'name' => 'csrf',
                                'options' => array(
                                    'messages' => array(
                                        'notSame' => 'CSRF_NOT_SAME',
                                    ),
                                ),
                            ),
                        ), 
                    )));
            */

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * Function used to check variables user signup form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function exchangeArray($data) {
          if (!empty($data['referal_url_path'])) {
            $this->referal_url_path = (isset($data['referal_url_path'])) ? $data['referal_url_path'] : null;
        }
        
        if (!empty($data['first_name'])) {
            $this->first_name = (isset($data['first_name'])) ? $data['first_name'] : null;
        }
        if (!empty($data['middle_name'])) {
            $this->middle_name = (isset($data['middle_name'])) ? $data['middle_name'] : null;
        }
        if (!empty($data['last_name'])) {
            $this->last_name = (isset($data['last_name'])) ? $data['last_name'] : null;
        }
        if (!empty($data['email_id'])) {
            $this->email_id = (isset($data['email_id'])) ? $data['email_id'] : null;
        }
        if (!empty($data['email_id_conf'])) {
            $this->email_id_conf = (isset($data['email_id_conf'])) ? $data['email_id_conf'] : null;
        }
        if (!empty($data['password'])) {
            $this->password = (isset($data['password'])) ? $data['password'] : null;
        }
        if (!empty($data['password_conf'])) {
            $this->password_conf = (isset($data['password_conf'])) ? $data['password_conf'] : null;
        }
        if (!empty($data['country_id'])) {
            $this->country_id = (isset($data['country_id'])) ? $data['country_id'] : null;
        }
        if (!empty($data['postal_code'])) {
            $this->postal_code = (isset($data['postal_code'])) ? $data['postal_code'] : null;
        }
        if (!empty($data['security_question_id'])) {
            $this->security_question_id = (isset($data['security_question_id'])) ? $data['security_question_id'] : null;
        }
        if (!empty($data['security_answer'])) {
            $this->security_answer = (isset($data['security_answer'])) ? $data['security_answer'] : null;
        }
        if (!empty($data['agree'])) {
            $this->agree = (isset($data['agree'])) ? $data['agree'] : null;
        }
    }

    /**
     * Function used to check variables user basic info form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function exchangeEditBasicInfoArray($data) {
        if (!empty($data['upload_profile_image'])) {
            $this->upload_profile_image = (isset($data['upload_profile_image'])) ? $data['upload_profile_image'] : null;
        }
        if (!empty($data['upload_profile_image_id'])) {
            $this->upload_profile_image_id = (isset($data['upload_profile_image_id'])) ? $data['upload_profile_image_id'] : null;
        }
        if (!empty($data['corporate_company_name'])) {
            $this->corporate_company_name = (isset($data['corporate_company_name'])) ? $data['corporate_company_name'] : null;
        }
        if (!empty($data['corporate_legal_name'])) {
            $this->corporate_legal_name = (isset($data['corporate_legal_name'])) ? $data['corporate_legal_name'] : null;
        }
        if (!empty($data['corporate_sic_code'])) {
            $this->corporate_sic_code = (isset($data['corporate_sic_code'])) ? $data['corporate_sic_code'] : null;
        }
        if (!empty($data['title'])) {
            $this->title = (isset($data['title'])) ? $data['title'] : null;
        }
        if (!empty($data['suffix'])) {
            $this->suffix = (isset($data['suffix'])) ? $data['suffix'] : null;
        }
        if (!empty($data['first_name'])) {
            $this->first_name = (isset($data['first_name'])) ? $data['first_name'] : null;
        }
        if (!empty($data['middle_name'])) {
            $this->middle_name = (isset($data['middle_name'])) ? $data['middle_name'] : null;
        }
        if (!empty($data['last_name'])) {
            $this->last_name = (isset($data['last_name'])) ? $data['last_name'] : null;
        }
        if (!empty($data['email_id'])) {
            $this->email_id = (isset($data['email_id'])) ? $data['email_id'] : null;
        }
        if (!empty($data['conf_email_id'])) {
            $this->conf_email_id = (isset($data['conf_email_id'])) ? $data['conf_email_id'] : null;
        }
        if (!empty($data['username'])) {
            $this->username = (isset($data['username'])) ? $data['username'] : null;
        }
        if (!empty($data['country_id'])) {
            $this->country_id = (isset($data['country_id'])) ? $data['country_id'] : null;
        }
        if (!empty($data['postal_code'])) {
            $this->postal_code = (isset($data['postal_code'])) ? $data['postal_code'] : null;
        }
        if (!empty($data['security_question'])) {
            $this->security_question = (isset($data['security_question'])) ? $data['security_question'] : null;
        }
        if (!empty($data['security_answer'])) {
            $this->security_answer = (isset($data['security_answer'])) ? $data['security_answer'] : null;
        }
    }

    /**
     * Function used to check variables user update password form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function exchangeUpdatePasswordArray($data) {
        if (!empty($data['contact_id'])) {
            $this->contact_id = (isset($data['contact_id'])) ? $data['contact_id'] : null;
        }
        if (!empty($data['password'])) {
            $this->password = (isset($data['password'])) ? $data['password'] : null;
        }
        if (!empty($data['password_conf'])) {
            $this->password_conf = (isset($data['password_conf'])) ? $data['password_conf'] : null;
        }
        if (!empty($data['security_answer'])) {
            $this->security_answer = (isset($data['security_answer'])) ? $data['security_answer'] : null;
        }
    }

    /**
     * Function used to check variables user update security questyion form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function exchangeEditSecurityQuestionArray($data) {
        if (!empty($data['contact_id'])) {
            $this->contact_id = (isset($data['contact_id'])) ? $data['contact_id'] : null;
        }
        if (!empty($data['password'])) {
            $this->password = (isset($data['password'])) ? $data['password'] : null;
        }
        if (!empty($data['security_question'])) {
            $this->security_question = (isset($data['security_question'])) ? $data['security_question'] : null;
        }
        if (!empty($data['security_answer'])) {
            $this->security_answer = (isset($data['security_answer'])) ? $data['security_answer'] : null;
        }
        if (!empty($data['confirm_security_answer'])) {
            $this->confirm_security_answer = (isset($data['confirm_security_answer'])) ? $data['confirm_security_answer'] : null;
        }
    }

    /**
     * Function used to check variables user update email info
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function exchangeEditEmailArray($data) {
        if (!empty($data['contact_id'])) {
            $this->contact_id = (isset($data['contact_id'])) ? $data['contact_id'] : null;
        }
        if (!empty($data['password'])) {
            $this->password = (isset($data['password'])) ? $data['password'] : null;
        }
        if (!empty($data['email_id'])) {
            $this->email_id = (isset($data['email_id'])) ? $data['email_id'] : null;
        }
        if (!empty($data['email_id_conf'])) {
            $this->email_id_conf = (isset($data['email_id_conf'])) ? $data['email_id_conf'] : null;
        }
        if (!empty($data['confirm_security_answer'])) {
            $this->confirm_security_answer = (isset($data['confirm_security_answer'])) ? $data['confirm_security_answer'] : null;
        }
    }

    /**
     * Function used to check variables user basic info form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function userBasicInfoData($data) {
        $userBasicInfoData = array();
        $userBasicInfoData['profile_image'] = (isset($data->upload_profile_image_id)) ? $data->upload_profile_image_id : null;
        $userBasicInfoData['title'] = (isset($data->title)) ? $data->title : null;
        $userBasicInfoData['first_name'] = (isset($data->first_name)) ? $data->first_name : null;
        $userBasicInfoData['middle_name'] = (isset($data->middle_name)) ? $data->middle_name : null;
        $userBasicInfoData['last_name'] = (isset($data->last_name)) ? $data->last_name : null;
        $userBasicInfoData['suffix'] = (isset($data->suffix)) ? $data->suffix : null;
        $userBasicInfoData['country_id'] = (isset($data->country_id)) ? $data->country_id : null;
        $userBasicInfoData['postal_code'] = (isset($data->postal_code)) ? $data->postal_code : null;
        $userBasicInfoData['security_question'] = (isset($data->security_question)) ? $data->security_question : null;
        $userBasicInfoData['security_answer'] = (isset($data->security_answer)) ? $data->security_answer : null;
        $userBasicInfoData['corporate_company_name'] = (isset($data->corporate_company_name)) ? $data->corporate_company_name : null;
        $userBasicInfoData['corporate_legal_name'] = (isset($data->corporate_legal_name)) ? $data->corporate_legal_name : null;
        $userBasicInfoData['corporate_sic_code'] = (isset($data->corporate_sic_code)) ? $data->corporate_sic_code : null;
        return $userBasicInfoData;
    }

    // Add content to these methods:
    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    /**
     * Function used to check validation for user signup form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function getInputFilter($emailNot = false) {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'csrf',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'csrf',
                                'options' => array(
                                    'messages' => array(
                                        'notSame' => 'CSRF_NOT_SAME',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'first_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'FIRST_NAME',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'messages' => array(
                                        'stringLengthTooShort' => 'FIRST_NAME_LENGTH_MIN',
                                        'stringLengthTooLong' => 'FIRST_NAME_LENGTH_MAX'
                                    ),
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 50,
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'middle_name',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'last_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'LAST_NAME'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'messages' => array(
                                        'stringLengthTooShort' => 'LAST_NAME_LENGTH_MIN',
                                        'stringLengthTooLong' => 'LAST_NAME_LENGTH_MAX'
                                    ),
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 20,
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'email_id',
                        'required' => $emailNot,
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMAIL_ID'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'EmailAddress',
                                'options' => array(
                                    'messages' => array(
                                        \Zend\Validator\EmailAddress::INVALID_FORMAT => "EMAIL_ID_VALID",
                                    ),
                                ),
                            ),
//                            array(
//                                'name' => 'Db\NoRecordExists',
//                                'options' => array(
//                                    'table' => 'tbl_usr_users',
//                                    'field' => 'email_id',
//                                    'adapter' => $this->adapter,
//                                    //'include' => array ('field' => 'is_delete', 'value' => '0'),
//                                    'messages' => array(
//                                        'recordFound' => 'EMAIL_ID_ALREADY',
//                                    ),
//                                ),
//                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'email_id_conf',
                        'required' => $emailNot,
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'CONFIRM_EMAIL_ID'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'Identical',
                                'options' => array(
                                    'token' => 'email_id',
                                    'messages' => array(
                                        \Zend\Validator\Identical::NOT_SAME => 'CONFIRM_EMAIL_ID_NOT_MATCH'
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'password',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'PASSWORD'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^(?=.*\d)(?=.*[a-zA-Z]).{6,25}$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'PASSWORD_VALID'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'messages' => array(
                                        'stringLengthTooShort' => 'PASSWORD_LENGTH_MIN',
                                        'stringLengthTooLong' => 'PASSWORD_LENGTH_MAX'
                                    ),
                                    'encoding' => 'UTF-8',
                                    'min' => 6,
                                    'max' => 25,
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'password_conf',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'PASSWORD_CONFIRM'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'Identical',
                                'options' => array(
                                    'token' => 'password',
                                    'messages' => array(
                                        \Zend\Validator\Identical::NOT_SAME => 'PASSWORD_CONFIRM_NOT_MATCHED'
                                    ),
                                ),
                            ),
                            ))));



            $inputFilter->add($factory->createInput(array(
                        'name' => 'country_id',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'COUNTRY'
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'postal_code',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'security_question_id',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'SECURITY_QUESTION'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'security_answer',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'SECURITY_ANSWER'
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'captcha',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'CAPTCHA_EMPTY'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'agree',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'InArray',
                                'options' => array(
                                    'haystack' => array(1),
                                    'messages' => array(
                                        'notInArray' => 'AGREE_TERMS'
                                    ),
                                ),
                            ),
                        ),
                    )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    /**
     * Function used to check validation for user basic info form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function getInputFilterEditBasicInfo() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'csrf',
                        'required' => false
//                        'validators' => array(
//                            array(
//                                'name' => 'csrf',
//                                'options' => array(
//                                    'messages' => array(
//                                        'notSame' => 'CSRF_NOT_SAME',
//                                    ),
//                                ),
//                            ),
//                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'contact_id',
                        'required' => true,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'upload_profile_image',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'upload_profile_image_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'title',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_sic_code',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_company_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'COMPANY_NAME_EMPTY',
                                    ),
                                ),
                            )
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_legal_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'LEGAL_NAME_EMPTY',
                                    ),
                                ),
                            )
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'first_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_FIRST_NAME',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'messages' => array(
                                        'stringLengthTooShort' => 'FIRST_NAME_LENGTH_MIN',
                                        'stringLengthTooLong' => 'FIRST_NAME_LENGTH_MAX'
                                    ),
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 50,
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'middle_name',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'last_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_LAST_NAME'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'messages' => array(
                                        'stringLengthTooShort' => 'LAST_NAME_LENGTH_MIN',
                                        'stringLengthTooLong' => 'LAST_NAME_LENGTH_MAX'
                                    ),
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 20,
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'email_id',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_EMAIL_ID'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'EmailAddress',
                                'options' => array(
                                    'messages' => array(
                                        \Zend\Validator\EmailAddress::INVALID_FORMAT => "INVALID_EMAIL",
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'conf_email_id',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_CONFIRM_EMAIL_ID'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'Identical',
                                'options' => array(
                                    'token' => 'email_id',
                                    'messages' => array(
                                        \Zend\Validator\Identical::NOT_SAME => 'CONFIRM_EMAIL_ID_NOT_MATCH'
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'suffix',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'username',
                        'required' => false,
                    )));



//            $inputFilter->add($factory->createInput(array(
//                        'name' => 'country_id',
//                        'required' => true,
//                        'validators' => array(
//                            array(
//                                'name' => 'NotEmpty',
//                                'options' => array(
//                                    'messages' => array(
//                                        'isEmpty' => 'EMPTY_COUNTRY'
//                                    ),
//                                ),
//                            ),
//                        ),
//                    )));
//
//            $inputFilter->add($factory->createInput(array(
//                        'name' => 'postal_code',
//                        'required' => false,
//                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'security_question',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'security_answer',
                        'required' => false,
                    )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    /**
     * Function used to check validation for user update password
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function getInputFilterUpdatePassword() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'csrf',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'csrf',
                                'options' => array(
                                    'messages' => array(
                                        'notSame' => 'CSRF_NOT_SAME',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'contact_id',
                        'required' => true,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'password',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_PASSWORD'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^(?=.*\d)(?=.*[a-zA-Z]).{6,25}$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'INVALID_PASSWORD_CRITERIA'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'messages' => array(
                                        'stringLengthTooShort' => 'PASSWORD_LENGTH_MIN',
                                        'stringLengthTooLong' => 'PASSWORD_LENGTH_MAX'
                                    ),
                                    'encoding' => 'UTF-8',
                                    'min' => 6,
                                    'max' => 25,
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'password_conf',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_CONFIRM_PASSWORD'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'Identical',
                                'options' => array(
                                    'token' => 'password',
                                    'messages' => array(
                                        \Zend\Validator\Identical::NOT_SAME => 'PASSWORD_CONFIRM_NOT_MATCHED'
                                    ),
                                ),
                            ),
                            ))));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'security_answer',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            /* array(
                              'name' => 'NotEmpty',
                              'options' => array(
                              'messages' => array(
                              'isEmpty' => 'EMPTY_SECURITY_ANSWER'
                              ),
                              ),
                              ), */
                            array(
                                'name' => 'Db\RecordExists',
                                'options' => array(
                                    'table' => 'tbl_usr_users',
                                    'field' => 'security_answer',
                                    'adapter' => $this->adapter,
                                    'messages' => array(
                                        'noRecordFound' => 'NOT_MATCH_SECURITY_ANSWER',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    /**
     * Function used to check validation for user update security question
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function getInputFilterEditUserSecurityQuestion() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'contact_id',
                        'required' => true,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'security_question',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_SECURITY_QUESTION'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'security_answer',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_SECURITY_ANSWER'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'password',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_PASSWORD'
                                    ),
                                ),
                            ),
                        ),
                    )));


            $inputFilter->add($factory->createInput(array(
                        'name' => 'confirm_security_answer',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            /* array(
                              'name' => 'NotEmpty',
                              'options' => array(
                              'messages' => array(
                              'isEmpty' => 'EMPTY_SECURITY_ANSWER'
                              ),
                              ),
                              ), */
                            array(
                                'name' => 'Db\RecordExists',
                                'options' => array(
                                    'table' => 'tbl_usr_users',
                                    'field' => 'security_answer',
                                    'adapter' => $this->adapter,
                                    'messages' => array(
                                        'noRecordFound' => 'NOT_MATCH_SECURITY_ANSWER',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    /**
     * Function used to check validation for user update email
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function getInputFilterEditEmailInfo() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'csrf',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'csrf',
                                'options' => array(
                                    'messages' => array(
                                        'notSame' => 'CSRF_NOT_SAME',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'contact_id',
                        'required' => true,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'email_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_EMAIL',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'EmailAddress',
                                'options' => array(
                                    'messages' => array(
                                        \Zend\Validator\EmailAddress::INVALID_FORMAT => "INVALID_EMAIL",
                                    ),
                                ),
                            ),
//                            array(
//                                'name' => 'Db\NoRecordExists',
//                                'options' => array(
//                                    'table' => 'tbl_usr_users',
//                                    'field' => 'email_id',
//                                    //'include' => array ('field' => 'is_delete', 'value' => 1),
//                                    'adapter' => $this->adapter,
//                                    'messages' => array(
//                                        'recordFound' => 'EMAIL_ID_ALREADY',
//                                    ),
//                                ),
//                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'email_id_conf',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_CONFIRM_EMAIL',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'Identical',
                                'options' => array(
                                    'token' => 'email_id',
                                    'messages' => array(
                                        \Zend\Validator\Identical::NOT_SAME => 'CONFIRM_EMAIL_ID_NOT_MATCH'
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'password',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_PASSWORD'
                                    ),
                                ),
                            ),
                        ),
                    )));



            $inputFilter->add($factory->createInput(array(
                        'name' => 'confirm_security_answer',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            /* array(
                              'name' => 'NotEmpty',
                              'options' => array(
                              'messages' => array(
                              'isEmpty' => 'EMPTY_SECURITY_ANSWER'
                              ),
                              ),
                              ), */
                            array(
                                'name' => 'Db\RecordExists',
                                'options' => array(
                                    'table' => 'tbl_usr_users',
                                    'field' => 'security_answer',
                                    'adapter' => $this->adapter,
                                    'messages' => array(
                                        'noRecordFound' => 'NOT_MATCH_SECURITY_ANSWER',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy() {
        return get_object_vars($this);
    }

    /**
     * This method is used to convert form post array of login to Object of class type User for crm
     * @param code
     * @return Void
     * @author Icreon Tech - DT
     */
    public function exchangeArrayLoginCrm($data) {
        if (!empty($data['user_name'])) {
            $this->user_name = (isset($data['user_name'])) ? $data['user_name'] : null;
        }
        if (!empty($data['password'])) {
            $this->password = (isset($data['password'])) ? $data['password'] : null;
        }
    }

    /**
     * This method is used to add filtering and validation to the form elements of login for crm
     * @param code
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getInputFilterLoginCrm() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'user_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'USERNAME_EMPTY',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'EmailAddress',
                                'options' => array(
                                    'messages' => array(
                                        'emailAddressInvalidFormat' => 'INVALID_EMAIL_ADDRESS',
                                        'emailAddressInvalidHostname' => 'INVALID_EMAIL_HOSTNAME',
                                        'hostnameUnknownTld' => 'INVALID_EMAIL_HOSTNAME',
                                        'hostnameLocalNameNotAllowed' => 'INVALID_EMAIL_HOSTNAME',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'password',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'PASSWORD_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * This method is used to add filtering and validation to the form elements for forgot password for crm
     * @param code
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getInputFilterForgotCrm() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'email_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMAIL_EMPTY',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'EmailAddress',
                                'options' => array(
                                    'messages' => array(
                                        \Zend\Validator\EmailAddress::INVALID_FORMAT => "EMAIL_INVALID",
                                    ),
                                ),
                            ),
                        ),
                    )));
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * This method is used to convert form post array of forgot to Object of class type User for crm
     * @param code
     * @return Void
     * @author Icreon Tech - DT
     */
    public function exchangeArrayForgotCrm($data) {
        if (!empty($data['email_id'])) {
            $this->email_id = (isset($data['email_id'])) ? $data['email_id'] : null;
        }
    }

    /**
     * This method is used to add filtering and validation to the form elements for forgot password for crm
     * @param code
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getCrmInputFilterPassword() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'code',
                        'required' => true,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'email_id',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMAIL_EMPTY'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'EmailAddress',
                                'options' => array(
                                    'messages' => array(
                                        \Zend\Validator\EmailAddress::INVALID_FORMAT => "EMAIL_INVALID",
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'password',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'PASSWORD_EMPTY'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,25}$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'PASSWORD_INVALID'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'messages' => array(
                                        'stringLengthTooShort' => 'PASSWORD_LENGTH_MIN',
                                        'stringLengthTooLong' => 'PASSWORD_LENGTH_MAX'
                                    ),
                                    'encoding' => 'UTF-8',
                                    'min' => 6,
                                    'max' => 25,
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'password_conf',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'PASSWORD_CONFIRM_EMPTY'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'Identical',
                                'options' => array(
                                    'token' => 'password',
                                    'messages' => array(
                                        \Zend\Validator\Identical::NOT_SAME => 'PASSWORD_CONFIRM_NOT_MATCHED'
                                    ),
                                ),
                            ),
                            ))));
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * This method is used to convert form post array of reset forget password to Object of class type User for crm
     * @param code
     * @return Void
     * @author Icreon Tech - DT
     */
    public function exchangeArrayPasswordCrm($data) {
        if (!empty($data['email_id'])) {
            $this->email_id = (isset($data['email_id'])) ? $data['email_id'] : null;
        }
        if (!empty($data['password'])) {
            $this->password = (isset($data['password'])) ? $data['password'] : null;
        }
        if (!empty($data['password_conf'])) {
            $this->password_conf = (isset($data['password_conf'])) ? $data['password_conf'] : null;
        }
    }

    /**
     * This method is used to get user phone info
     * @param code
     * @return Void
     * @author Icreon Tech - DG
     */
    public function exchangeArrayUserPhoneInfo($data) {
        if (!empty($data['phone_id'])) {
            $this->phone_id = (isset($data['phone_id'])) ? $data['phone_id'] : null;
        }
        if (!empty($data['phone_type'])) {
            $this->phone_type = (isset($data['phone_type'])) ? $data['phone_type'] : null;
        }
        if (!empty($data['country_code'])) {
            $this->country_code = (isset($data['country_code'])) ? $data['country_code'] : null;
        }
        if (!empty($data['area_code'])) {
            $this->area_code = (isset($data['area_code'])) ? $data['area_code'] : null;
        }
        if (!empty($data['phone_no'])) {
            $this->phone_no = (isset($data['phone_no'])) ? $data['phone_no'] : null;
        }
        if (!empty($data['extension'])) {
            $this->extension = (isset($data['extension'])) ? $data['extension'] : null;
        }
        if (!empty($data['is_primary'])) {
            $this->is_primary = (isset($data['is_primary'])) ? $data['is_primary'] : null;
        }
    }

    /**
     * This method is used to get user phone info
     * @param code
     * @return Array
     * @author Icreon Tech - DG
     */
    public function getUserPhoneInfoInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'csrf',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'csrf',
                                'options' => array(
                                    'messages' => array(
                                        'notSame' => 'CSRF_NOT_SAME',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'phone_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'phone_type',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'country_code',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'country_code',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'area_code',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'phone_no',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'extension',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_primary',
                        'required' => false,
                    )));
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * This method is used to get user address info
     * @param code
     * @return Array
     * @author Icreon Tech - DG
     */
    public function getUserAddressInfoInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'csrf',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'csrf',
                                'options' => array(
                                    'messages' => array(
                                        'notSame' => 'CSRF_NOT_SAME',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'address_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'state_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'addressinfo_country',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'addressinfo_location_type',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'addressinfo_location_billing',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'addressinfo_location_shipping',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_primary',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'title',
                        'required' => false,
                    )));            
            
            $inputFilter->add($factory->createInput(array(
                        'name' => 'first_name',
                        'required' => false,
                    )));            
            $inputFilter->add($factory->createInput(array(
                        'name' => 'middle_name',
                        'required' => false,
                    )));            
            $inputFilter->add($factory->createInput(array(
                        'name' => 'last_name',
                        'required' => false,
                    )));                        
            $inputFilter->add($factory->createInput(array(
                        'name' => 'addressinfo_street_address_1',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'addressinfo_street_address_2',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'addressinfo_city',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'addressinfo_state',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'addressinfo_zip',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'address_state_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_apo_po_record',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'address_state_po[]',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'address_state_apo[]',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'address_country_id[]',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_apo_po[]',
                        'required' => false,
                    )));

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    public function getInputFilterContactUs() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'csrf',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'csrf',
                                'options' => array(
                                    'messages' => array(
                                        'notSame' => 'CSRF not same',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'first_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'FIRST_NAME',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'messages' => array(
                                        'stringLengthTooShort' => 'FIRST_NAME_LENGTH_MIN',
                                        'stringLengthTooLong' => 'FIRST_NAME_LENGTH_MAX'
                                    ),
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 50,
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'last_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'LAST_NAME'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'messages' => array(
                                        'stringLengthTooShort' => 'LAST_NAME_LENGTH_MIN',
                                        'stringLengthTooLong' => 'LAST_NAME_LENGTH_MAX'
                                    ),
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 20,
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'email_id',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMAIL_ID'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'EmailAddress',
                                'options' => array(
                                    'messages' => array(
                                        \Zend\Validator\EmailAddress::INVALID_FORMAT => "EMAIL_ID_VALID",
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'captcha',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'CAPTCHA_EMPTY'
                                    ),
                                ),
                            ),
                        ),
                    )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
    /**
     * Function used to check validation for user visit form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function getInputFilterVisitRecord() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'csrf',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'csrf',
                                'options' => array(
                                    'messages' => array(
                                        'notSame' => 'CSRF_NOT_SAME',
                                    ),
                                ),
                            ),
                        ),
                    )));
        
             $inputFilter->add($factory->createInput(array(
                        'name' => 'visiting_with',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_VISITING'
                                    ),
                                ),
                            ),
                        ),
                    )));
             
             $inputFilter->add($factory->createInput(array(
                        'name' => 'birth_year',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_BIRTH_YEAR'
                                    ),
                                ),
                            ),
                        ),
                    )));
 
            $inputFilter->add($factory->createInput(array(
                        'name' => 'immigration_year',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'port_of_entry',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'occupation',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'matrial_status',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMPTY_MATARIAL_STATUS'
                                    ),
                                ),
                            ),
                        ),
                    )));
           $inputFilter->add($factory->createInput(array(
                        'name' => 'gender',
                        'required' => true,
                    )));

          

            $inputFilter->add($factory->createInput(array(
                        'name' => 'message',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'MESSAGE_EMPTY'
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'messages' => array(
                                        'stringLengthTooShort' => 'MESSAGE_LENGTH_MIN',
                                        'stringLengthTooLong' => 'MESSAGE_LENGTH_MAX'
                                    ),
                                    'encoding' => 'UTF-8',
                                    'min' => 5,
                                    'max' => 200,
                                ),
                            ),
                        ),
                    )));

      
            
            $inputFilter->add($factory->createInput(array(
                        'name' => 'birth_country',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'COUNTRY'
                                    ),
                                ),
                            ),
                        ),
                    )));

        $inputFilter->add($factory->createInput(array(
                        'name' => 'agree',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'InArray',
                                'options' => array(
                                    'haystack' => array(1),
                                    'messages' => array(
                                        'notInArray' => 'AGREE_TERMS'
                                    ),
                                ),
                            ),
                        ),
                    )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}