<?php

/**
 * This model is used for woh.
 * @category   Zend
 * @package    User
 * @author     Icreon Tech - NS
 */

namespace User\Model;

use Zend\Db\TableGateway\TableGateway;

class WohTable {

    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
    }

    /**
     * This function is used to insert User Woh
     * @param $params
     * @return boolean 
     * @author Icreon Tech - NS
     */
    public function insertUserWoh($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_woh_insertUserWoh(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['user_id']);
            $stmt->getResource()->bindParam(2, $param['user_session_id']);
            $stmt->getResource()->bindParam(3, $param['donation_for']);
            $stmt->getResource()->bindParam(4, $param['first_name_one']);
            $stmt->getResource()->bindParam(5, $param['other_name']);
            $stmt->getResource()->bindParam(6, $param['other_init_one']);
            $stmt->getResource()->bindParam(7, $param['last_name_one']);
            $stmt->getResource()->bindParam(8, $param['first_name_two']);
            $stmt->getResource()->bindParam(9, $param['other_init_two']);
            $stmt->getResource()->bindParam(10, $param['last_name_two']);
            $stmt->getResource()->bindParam(11, $param['first_line']);
            $stmt->getResource()->bindParam(12, $param['second_line']);
            $stmt->getResource()->bindParam(13, $param['origin_country']);
            $stmt->getResource()->bindParam(14, $param['donated_by']);
            $stmt->getResource()->bindParam(15, $param['num_additional_certificate']);
            $stmt->getResource()->bindParam(16, $param['is_correct']);
            $stmt->getResource()->bindParam(17, $param['is_agree']);
            $stmt->getResource()->bindParam(18, $param['product_mapping_id']);
            $stmt->getResource()->bindParam(19, $param['product_id']);
            $stmt->getResource()->bindParam(20, $param['product_type_id']);
            $stmt->getResource()->bindParam(21, $param['product_attribute_option_id']);
            $stmt->getResource()->bindParam(22, $param['added_by']);
            $stmt->getResource()->bindParam(23, $param['added_date']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will add woh to cart
     * @param array
     * @return return last id
     * @author Icreon Tech - NS
     */
    public function insertToCartWallOfHonorNaming($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_insertWOHAddToCart_ksk(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $stmt->getResource()->bindParam(1, $params['user_id']);
            $stmt->getResource()->bindParam(2, $params['user_session_id']);
            $stmt->getResource()->bindParam(3, $params['donated_for']);
            $stmt->getResource()->bindParam(4, $params['first_name_one']);
            $stmt->getResource()->bindParam(5, $params['other_name']);
            $stmt->getResource()->bindParam(6, $params['other_init_one']);
            $stmt->getResource()->bindParam(7, $params['last_name_one']);
            $stmt->getResource()->bindParam(8, $params['first_name_two']);
            $stmt->getResource()->bindParam(9, $params['other_init_two']);
            $stmt->getResource()->bindParam(10, $params['last_name_two']);
            $stmt->getResource()->bindParam(11, $params['first_line']);
            $stmt->getResource()->bindParam(12, $params['second_line']);
            $stmt->getResource()->bindParam(13, $params['origin_country']);
            $stmt->getResource()->bindParam(14, $params['donated_by']);
            $stmt->getResource()->bindParam(15, $params['num_additional_certificate']);
            $stmt->getResource()->bindParam(16, $params['is_correct']);
            $stmt->getResource()->bindParam(17, $params['is_agree']);
            $stmt->getResource()->bindParam(18, $params['status']);
            $stmt->getResource()->bindParam(19, $params['product_mapping_id']);
            $stmt->getResource()->bindParam(20, $params['product_id']);
            $stmt->getResource()->bindParam(21, $params['product_type_id']);
            $stmt->getResource()->bindParam(22, $params['product_attribute_option_id']);
            $stmt->getResource()->bindParam(23, $params['product_sku']);
            $stmt->getResource()->bindParam(24, $params['num_quantity']);
            $stmt->getResource()->bindParam(25, $params['product_price']);
            $stmt->getResource()->bindParam(26, $params['added_by']);
            $stmt->getResource()->bindParam(27, $params['added_date']);
            $stmt->getResource()->bindParam(28, $params['modified_by']);
            $stmt->getResource()->bindParam(29, $params['modified_date']);
            $stmt->getResource()->bindParam(30, $params['final_name']);
			$stmt->getResource()->bindParam(31, $params['cart_source_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will add woh additional contact to cart
     * @param array
     * @return return last id
     * @author Icreon Tech - NS
     */
    public function insertToCartWallOfHonorAdditionalContact($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_insertWOHContactAddToCart(?,?,?,?,?,?,?)");
            $stmt->getResource()->bindParam(1, $params['woh_id']);
            $stmt->getResource()->bindParam(2, $params['first_name']);
            $stmt->getResource()->bindParam(3, $params['last_name']);
            $stmt->getResource()->bindParam(4, $params['email_id']);
            $stmt->getResource()->bindParam(5, $params['added_date']);
            $stmt->getResource()->bindParam(6, $params['modified_date']);
            $stmt->getResource()->bindParam(7, $params['cart_source_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will get User Woh Count
     * @return this will return arrry
     * @author Icreon Tech - NS
     */
    public function getUserWohCount($param = array()) {

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getUserWohCount(?,?,?,?,?,?,?,?,?,?,?,?)');

            $is_panel_num = (isset($param['is_panel_num']) && $param['is_panel_num'] != '') ? $param['is_panel_num'] : null;
            $userId = (isset($param['user_id']) && $param['user_id'] != '') ? $param['user_id'] : null;
            $last_name = (isset($param['last_name']) && $param['last_name'] != '') ? addslashes($param['last_name']) : '';
            $first_name = (isset($param['first_name']) && $param['first_name'] != '') ? addslashes($param['first_name']) : '';
            $other_part_name = (isset($param['other_part_name']) && $param['other_part_name'] != '') ? addslashes($param['other_part_name']) : '';
            $country = (isset($param['country']) && $param['country'] != '') ? addslashes($param['country']) : '';
            $donated_by = (isset($param['donated_by']) && $param['donated_by'] != '') ? addslashes($param['donated_by']) : '';
            $panel_number = (isset($param['panel_number']) && $param['panel_number'] != '') ? $param['panel_number'] : '';

            $start_index = (isset($param['start_index']) && $param['start_index'] != '' && !is_null($param['start_index'])) ? $param['start_index'] : '';
            $record_limit = (isset($param['record_limit']) && $param['record_limit'] != '' && !is_null($param['record_limit'])) ? $param['record_limit'] : '';
            $sort_field = (isset($param['sort_field']) && $param['sort_field'] != '' && !is_null($param['sort_field'])) ? $param['sort_field'] : '';
            $sort_order = (isset($param['sort_order']) && $param['sort_order'] != '' && !is_null($param['sort_order'])) ? $param['sort_order'] : '';

            if (isset($last_name) and trim($last_name) != "") {
                $last_name = trim($last_name);
            }
            if (isset($first_name) and trim($first_name) != "") {
                $first_name = trim($first_name);
            }
            if (isset($other_part_name) and trim($other_part_name) != "") {
                $other_part_name = trim($other_part_name);
            }
            if (isset($country) and trim($country) != "") {
                $country = trim($country);
            }
            if (isset($donated_by) and trim($donated_by) != "") {
                $donated_by = trim($donated_by);
            }
            if (isset($panel_number) and trim($panel_number) != "") {
                $panel_number = trim($panel_number);
            }

            $stmt->getResource()->bindParam(1, $last_name);
            $stmt->getResource()->bindParam(2, $first_name);
            $stmt->getResource()->bindParam(3, $other_part_name);
            $stmt->getResource()->bindParam(4, $country);
            $stmt->getResource()->bindParam(5, $donated_by);
            $stmt->getResource()->bindParam(6, $panel_number);
            $stmt->getResource()->bindParam(7, $start_index);
            $stmt->getResource()->bindParam(8, $record_limit);
            $stmt->getResource()->bindParam(9, $sort_field);
            $stmt->getResource()->bindParam(10, $sort_order);
            $stmt->getResource()->bindParam(11, $userId);
            $stmt->getResource()->bindParam(12, $is_panel_num);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * This function will return wall of honour
     * @return this will return arrry
     * @author Icreon Tech - NS
     */
    public function getUserWohFront($param = array()) {

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getUserWohFront(?,?,?,?,?,?,?,?,?,?,?,?)');

            $is_panel_num = (isset($param['is_panel_num']) && $param['is_panel_num'] != '') ? $param['is_panel_num'] : null;
            $userId = (isset($param['user_id']) && $param['user_id'] != '') ? $param['user_id'] : null;
            $last_name = (isset($param['last_name']) && $param['last_name'] != '') ? addslashes($param['last_name']) : '';
            $first_name = (isset($param['first_name']) && $param['first_name'] != '') ? addslashes($param['first_name']) : '';
            $other_part_name = (isset($param['other_part_name']) && $param['other_part_name'] != '') ? addslashes($param['other_part_name']) : '';
            $country = (isset($param['country']) && $param['country'] != '') ? addslashes($param['country']) : '';
            $donated_by = (isset($param['donated_by']) && $param['donated_by'] != '') ? addslashes($param['donated_by']) : '';
            $panel_number = (isset($param['panel_number']) && $param['panel_number'] != '') ? $param['panel_number'] : '';

            $start_index = (isset($param['start_index']) && $param['start_index'] != '' && !is_null($param['start_index'])) ? $param['start_index'] : '';
            $record_limit = (isset($param['record_limit']) && $param['record_limit'] != '' && !is_null($param['record_limit'])) ? $param['record_limit'] : '';
            $sort_field = (isset($param['sort_field']) && $param['sort_field'] != '' && !is_null($param['sort_field'])) ? $param['sort_field'] : '';
            $sort_order = (isset($param['sort_order']) && $param['sort_order'] != '' && !is_null($param['sort_order'])) ? $param['sort_order'] : '';

            if (isset($last_name) and trim($last_name) != "") {
                $last_name = trim($last_name);
            }
            if (isset($first_name) and trim($first_name) != "") {
                $first_name = trim($first_name);
            }
            if (isset($other_part_name) and trim($other_part_name) != "") {
                $other_part_name = trim($other_part_name);
            }
            if (isset($country) and trim($country) != "") {
                $country = trim($country);
            }
            if (isset($donated_by) and trim($donated_by) != "") {
                $donated_by = trim($donated_by);
            }
            if (isset($panel_number) and trim($panel_number) != "") {
                $panel_number = trim($panel_number);
            }

            $stmt->getResource()->bindParam(1, $last_name);
            $stmt->getResource()->bindParam(2, $first_name);
            $stmt->getResource()->bindParam(3, $other_part_name);
            $stmt->getResource()->bindParam(4, $country);
            $stmt->getResource()->bindParam(5, $donated_by);
            $stmt->getResource()->bindParam(6, $panel_number);
            $stmt->getResource()->bindParam(7, $start_index);
            $stmt->getResource()->bindParam(8, $record_limit);
            $stmt->getResource()->bindParam(9, $sort_field);
            $stmt->getResource()->bindParam(10, $sort_order);
            $stmt->getResource()->bindParam(11, $userId);
            $stmt->getResource()->bindParam(12, $is_panel_num);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * This function will return wall of honour
     * @return this will return arrry
     * @author Icreon Tech - NS
     */
    public function getUserWoh($param = array()) {

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getUserWoh(?,?,?,?,?,?,?,?,?,?,?,?)');

            $is_panel_num = (isset($param['is_panel_num']) && $param['is_panel_num'] != '') ? $param['is_panel_num'] : null;
            $userId = (isset($param['user_id']) && $param['user_id'] != '') ? $param['user_id'] : null;
            $last_name = (isset($param['last_name']) && $param['last_name'] != '') ? addslashes($param['last_name']) : '';
            $first_name = (isset($param['first_name']) && $param['first_name'] != '') ? addslashes($param['first_name']) : '';
            $other_part_name = (isset($param['other_part_name']) && $param['other_part_name'] != '') ? addslashes($param['other_part_name']) : '';
            $country = (isset($param['country']) && $param['country'] != '') ? addslashes($param['country']) : '';
            $donated_by = (isset($param['donated_by']) && $param['donated_by'] != '') ? addslashes($param['donated_by']) : '';
            $panel_number = (isset($param['panel_number']) && $param['panel_number'] != '') ? $param['panel_number'] : '';

            $start_index = (isset($param['start_index']) && $param['start_index'] != '' && !is_null($param['start_index'])) ? $param['start_index'] : '';
            $record_limit = (isset($param['record_limit']) && $param['record_limit'] != '' && !is_null($param['record_limit'])) ? $param['record_limit'] : '';
            $sort_field = (isset($param['sort_field']) && $param['sort_field'] != '' && !is_null($param['sort_field'])) ? $param['sort_field'] : '';
            $sort_order = (isset($param['sort_order']) && $param['sort_order'] != '' && !is_null($param['sort_order'])) ? $param['sort_order'] : '';

            if (isset($last_name) and trim($last_name) != "") {
                $last_name = trim($last_name);
            }
            if (isset($first_name) and trim($first_name) != "") {
                $first_name = trim($first_name);
            }
            if (isset($other_part_name) and trim($other_part_name) != "") {
                $other_part_name = trim($other_part_name);
            }
            if (isset($country) and trim($country) != "") {
                $country = trim($country);
            }
            if (isset($donated_by) and trim($donated_by) != "") {
                $donated_by = trim($donated_by);
            }
            if (isset($panel_number) and trim($panel_number) != "") {
                $panel_number = trim($panel_number);
            }

            $stmt->getResource()->bindParam(1, $last_name);
            $stmt->getResource()->bindParam(2, $first_name);
            $stmt->getResource()->bindParam(3, $other_part_name);
            $stmt->getResource()->bindParam(4, $country);
            $stmt->getResource()->bindParam(5, $donated_by);
            $stmt->getResource()->bindParam(6, $panel_number);
            $stmt->getResource()->bindParam(7, $start_index);
            $stmt->getResource()->bindParam(8, $record_limit);
            $stmt->getResource()->bindParam(9, $sort_field);
            $stmt->getResource()->bindParam(10, $sort_order);
            $stmt->getResource()->bindParam(11, $userId);
            $stmt->getResource()->bindParam(12, $is_panel_num);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will get User Woh Product Attributes
     * @param array
     * @return return last id
     * @author Icreon Tech - NS
     */
    public function getUserWohProductAttributes($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getUserWohProductAttributes(?,?)');

            $stmt->getResource()->bindParam(1, $param['product_id']);
            $stmt->getResource()->bindParam(2, $param['product_type_id']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will get User Searches
     * @param array
     * @return return array
     * @author Icreon Tech - DG
     */
    /*public function getUserSearches($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getUserWohSearch(?,?)');

            $stmt->getResource()->bindParam(1, $param['w_user_id']);
            $stmt->getResource()->bindParam(2, $param['woh_search_id']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } else {
            return array();
        }
    }*/

    public function getUserSearches($dataArr = array()) {
        $user_id = isset($dataArr['w_user_id']) ? $dataArr['w_user_id'] : null;
        $wohSearchId = isset($dataArr['woh_search_id']) ? $dataArr['woh_search_id'] : null;
        $start_index = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $record_limit = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $sort_field = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $sort_order = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_getUserWohSearch(?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $user_id);
        $stmt->getResource()->bindParam(2, $wohSearchId);
        $stmt->getResource()->bindParam(3, $start_index);
        $stmt->getResource()->bindParam(4, $record_limit);
        $stmt->getResource()->bindParam(5, $sort_field);
        $stmt->getResource()->bindParam(6, $sort_order);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return array();
        }
    }

    /**
     * This function will save User Searches
     * @param array
     * @return return array
     * @author Icreon Tech - NS
     */
    public function saveUserSearches($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_insertWohSearch(?,?,?,?,?)');

            $stmt->getResource()->bindParam(1, $param['user_id']);
            $stmt->getResource()->bindParam(2, $param['title']);
            $stmt->getResource()->bindParam(3, $param['search_query']);
            $stmt->getResource()->bindParam(4, $param['save_date']);
            $stmt->getResource()->bindParam(5, $param['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will delete User Searches
     * @param array
     * @return return array
     * @author Icreon Tech - NS
     */
    public function deleteUserSearches($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_deleteUserWohSearch(?)');
            $stmt->getResource()->bindParam(1, $param['woh_search_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for get  All Woh Records
     * @author Icreon Tech - NS
     * @return Array
     * @param Array
     */
	public function getAllWohRecords($wohData) {
        try {
            $w_name_or_email = isset($wohData['name_or_email']) ? addslashes($wohData['name_or_email']) : '';
            $w_transaction_id = isset($wohData['transaction_id']) ? $wohData['transaction_id'] : '';
            $w_panel_number = isset($wohData['panel_number']) ? $wohData['panel_number'] : '';
            $w_transaction_date_from = isset($wohData['transaction_date_from']) ? $wohData['transaction_date_from'] : '';
            $w_transaction_date_to = isset($wohData['transaction_date_to']) ? $wohData['transaction_date_to'] : '';
            $w_panel_assigned = isset($wohData['panel_assigned']) ? $wohData['panel_assigned'] : '';
            $w_honoree_name = isset($wohData['honoree_name']) ? addslashes($wohData['honoree_name']) : '';
            $w_start_Index = isset($wohData['start_index']) ? $wohData['start_index'] : '';
            $w_record_limit = isset($wohData['record_limit']) ? $wohData['record_limit'] : '';
            $w_sort_field = isset($wohData['sort_field']) ? $wohData['sort_field'] : '';
            $w_sort_order = isset($wohData['sort_order']) ? $wohData['sort_order'] : '';
            $w_transaction_year = isset($wohData['transaction_year']) ? $wohData['transaction_year'] : '';
            $w_approval_req = isset($wohData['approval_required']) ? $wohData['approval_required'] : '';
            $w_woh_id = isset($wohData['woh_id']) ? $wohData['woh_id'] : '';
			$w_woh_status = isset($wohData['item_status']) ? $wohData['item_status'] : '';
			$w_woh_status_to_leave = isset($wohData['woh_status_to_leave']) ? $wohData['woh_status_to_leave'] : '';
            if (isset($w_name_or_email) and trim($w_name_or_email) != '') {
                $w_name_or_email = trim($w_name_or_email);
            }

            if (isset($w_transaction_id) and trim($w_transaction_id) != '') {
                $w_transaction_id = trim($w_transaction_id);
            }

            if (isset($w_panel_number) and trim($w_panel_number) != '') {
                $w_panel_number = trim($w_panel_number);
            }

            if (isset($w_transaction_date_from) and trim($w_transaction_date_from) != '') {
                $w_transaction_date_from = trim($w_transaction_date_from);
            }

            if (isset($w_transaction_date_to) and trim($w_transaction_date_to) != '') {
                $w_transaction_date_to = trim($w_transaction_date_to);
            }

            if (isset($w_panel_assigned) and trim($w_panel_assigned) != '') {
                $w_panel_assigned = trim($w_panel_assigned);
            }

            if (isset($w_honoree_name) and trim($w_honoree_name) != '') {
                $w_honoree_name = trim($w_honoree_name);
            }

            if (isset($w_start_Index) and trim($w_start_Index) != '') {
                $w_start_Index = trim($w_start_Index);
            }

            if (isset($w_record_limit) and trim($w_record_limit) != '') {
                $w_record_limit = trim($w_record_limit);
            }

            if (isset($w_sort_field) and trim($w_sort_field) != '') {
                $w_sort_field = trim($w_sort_field);
            }

            if (isset($w_sort_order) and trim($w_sort_order) != '') {
                $w_sort_order = trim($w_sort_order);
            }

            if (isset($w_transaction_year) and trim($w_transaction_year) != '') {
                $w_transaction_year = trim($w_transaction_year);
            }

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_tra_getWoh(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            $stmt->getResource()->bindParam(1, $w_name_or_email);
            $stmt->getResource()->bindParam(2, $w_transaction_id);
            $stmt->getResource()->bindParam(3, $w_panel_number);
            $stmt->getResource()->bindParam(4, $w_transaction_date_from);
            $stmt->getResource()->bindParam(5, $w_transaction_date_to);
            $stmt->getResource()->bindParam(6, $w_panel_assigned);
            $stmt->getResource()->bindParam(7, $w_honoree_name);
            $stmt->getResource()->bindParam(8, $w_start_Index);
            $stmt->getResource()->bindParam(9, $w_record_limit);
            $stmt->getResource()->bindParam(10, $w_sort_field);
            $stmt->getResource()->bindParam(11, $w_sort_order);
            $stmt->getResource()->bindParam(12, $w_transaction_year);
            $stmt->getResource()->bindParam(13, $w_approval_req);
            $stmt->getResource()->bindParam(14, $w_woh_id);
            $stmt->getResource()->bindParam(15, $w_woh_status);
			$stmt->getResource()->bindParam(16, $w_woh_status_to_leave);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for update Woh Record
     * @author Icreon Tech - NS
     * @return Array
     * @param Array
     */
    public function updateWohRecord($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_tra_updateWoh(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['woh_id']);
            $stmt->getResource()->bindParam(2, $param['panel_number']);
            $stmt->getResource()->bindParam(3, $param['modified_by']);
            $stmt->getResource()->bindParam(4, $param['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will get User Woh from transactions
     * @param array
     * @return return last id
     * @author Icreon Tech - NS
     */
    public function getUserWohTraDetailById($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_getWohTraDetailById(?)');
            $stmt->getResource()->bindParam(1, $param['woh_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * This function will get User Woh from transactions
     * @param array
     * @return return last id
     * @author Icreon Tech - NS
     */
    public function getWohCms(){
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_getWohCms()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
	//inscribe key for previous transaction
    public function getWohProductsWithoutInscribeKey($param = array()) {

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_woh_getWohProductsWithoutInscribeKey()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            //asd($resultSet);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    public function updateWohProductInscribeKey($param = array()) {
        
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_woh_updateWohProductsInscribeKey(?,?)');
            $stmt->getResource()->bindParam(1, $param['woh_id']);
            $stmt->getResource()->bindParam(2, $param['inscribe_key']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            //$resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

}

