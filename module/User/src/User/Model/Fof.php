<?php

/**
 * This model is used for user fof validation.
 * @category   Zend
 * @package    User
 * @author     Icreon Tech - NS
 */

namespace User\Model;

use User\Module;
// Add these import statements
use Base\Model\BaseModel;
use Zend\Db\Adapter\Adapter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Fof extends BaseModel {

    protected $inputFilter;
    protected $adapter;

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    /**
     * Function used to check variables cases     
     * @param Array
     * @return array
     * @author Icreon Tech - NS
     */
    public function exchangeArray($param = array()) {
        $parameter = array();
        $parameter['user_id'] = (isset($param['user_id'])) ? $param['user_id'] : '';
        $parameter['user_session_id'] = (isset($param['user_session_id'])) ? $param['user_session_id'] : '';
        $parameter['image_name'] = (isset($param['image_name'])) ? $param['image_name'] : '';
        $parameter['fof_image'] = (isset($param['fof_image'])) ? $param['fof_image'] : '';
        if (isset($param['is_photo_uploaded']) and $param['is_photo_uploaded'] == "1") {
            $parameter['is_photo_uploaded'] = 2;
        } else {
            $parameter['is_photo_uploaded'] = 1;
        }
        $parameter['donated_by'] = (isset($param['donated_by'])) ? $param['donated_by'] : '';
        $parameter['people_photo'] = (isset($param['people_photo'])) ? $param['people_photo'] : '';
        $parameter['is_people_photo_visible'] = (isset($param['is_people_photo_visible'])) ? $param['is_people_photo_visible'] : 0;
        $parameter['photo_caption'] = (isset($param['photo_caption'])) ? $param['photo_caption'] : '';
        $parameter['is_photo_caption_visible'] = (isset($param['is_photo_caption_visible'])) ? $param['is_photo_caption_visible'] : 0;
        $parameter['is_agree'] = (isset($param['is_agree'])) ? $param['is_agree'] : 0;
        $parameter['added_date'] = (isset($param['added_date'])) ? $param['added_date'] : '';
        $parameter['added_by'] = (isset($param['added_by'])) ? $param['added_by'] : '';
        $parameter['product_mapping_id'] = (isset($param['product_mapping_id'])) ? $param['product_mapping_id'] : 0;
        $parameter['product_id'] = (isset($param['product_id'])) ? $param['product_id'] : 0;
        $parameter['product_type_id'] = (isset($param['product_type_id'])) ? $param['product_type_id'] : 0;
        $parameter['product_sku'] = (isset($param['product_sku'])) ? $param['product_sku'] : '';
        $parameter['people_photo_json'] = (isset($param['people_photo_json'])) ? $param['people_photo_json'] : '';
        return $parameter;
    }

    public function getInputFilterAds($param = array()) {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'image_name',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_photo_uploaded',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'donated_by',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'people_photo',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_people_photo_visible',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'photo_caption',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_photo_caption_visible',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_agree',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

}
