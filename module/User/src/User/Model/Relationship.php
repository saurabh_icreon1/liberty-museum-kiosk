<?php

/**
 * This model is used for user Relationship.
 * @package    User_Relationship
 * @author     Icreon Tech - DG
 */

namespace User\Model;

use User\Module;
// Add these import statements
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\Adapter\Adapter;

/**
 * This model is used for user Relationship.
 * @package    User_Relationship
 * @author     Icreon Tech - DG
 */
class Relationship implements InputFilterAwareInterface {

    protected $inputFilter;
    protected $adapter;

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    /**
     * Function used to check variables user signup form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function exchangeArray($data) {
        
    }

    // Add content to these methods:
    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    /**
     * Function used to check validation for user signup form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'agree',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'InArray',
                                'options' => array(
                                    'haystack' => array(1),
                                    'messages' => array(
                                        'notInArray' => 'AGREE_TERMS'
                                    ),
                                ),
                            ),
                        ),
                    )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    /**
     * Function used to check variables for user create relationship
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function exchangeArrayCreateUserRelation($data) {
        $this->type = (isset($data['type'])) ? $data['type'] : null;
        $this->notes = (isset($data['notes'])) ? $data['notes'] : null;
        $this->user_relationship_id = (isset($data['user_relationship_id'])) ? $data['user_relationship_id'] : null;
        $this->enabled = (isset($data['enabled'])) ? $data['enabled'] : null;
        $this->user_id = (isset($data['user_id'])) ? $data['user_id'] : null;
        $this->relative_user_id = (isset($data['relative_user_id'])) ? $data['relative_user_id'] : null;
    }

    /**
     * Function used to check variables for user edit relationship
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function exchangeArrayEditUserRelation($data) {
        $this->type = (isset($data['type'])) ? $data['type'] : null;
        $this->notes = (isset($data['notes'])) ? $data['notes'] : null;
        $this->user_relationship_id = (isset($data['user_relationship_id'])) ? $data['user_relationship_id'] : null;
        $this->enabled = (isset($data['enabled'])) ? $data['enabled'] : null;
    }

    /**
     * Function used to check validation for user edit relationship form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function getInputFilterEditUserRelation() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'notes',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'user_relationship_id',
                        'required' => true,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'type',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'RELATIONSHIP_TYPE_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'enabled',
                        'required' => false,
                    )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    /**
     * Function used to check validation for user create relationship form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function getInputFilterCreateUserRelation() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'notes',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'relative_user_id',
                        'required' => true,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'user_id',
                        'required' => true,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'contact',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'CONTACT_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'type',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'RELATIONSHIP_TYPE_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'enabled',
                        'required' => false,
                    )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy() {
        return get_object_vars($this);
    }

}