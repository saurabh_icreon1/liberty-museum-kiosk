<?php

/**
 * This model is used for conatc .
 * @package    User_ContactTable
 * @author     Icreon Tech - DG
 */

namespace User\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This model is used for conatc .
 * @package    User_ContactTable
 * @author     Icreon Tech - DG
 */
class ContactTable {

    protected $tableGateway;
    protected $connection;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
        $this->connection = $this->tableGateway->getAdapter()->getDriver()->getConnection();
    }

    /**
     * Function for to get conatct
     * @author Icreon Tech - NS
     * @return Arr
     * @param Array
     */
    public function getSearchContact($dataArr = null) { 
        
        $membership_cards_year = (isset($dataArr['membership_cards_year']) and trim($dataArr['membership_cards_year']) != "") ? trim($dataArr['membership_cards_year']) : "";
        $membership_cards_package_shipped = (isset($dataArr['membership_cards_package_shipped']) and trim($dataArr['membership_cards_package_shipped']) != "") ? trim($dataArr['membership_cards_package_shipped']) : "";

        if (isset($dataArr['user_email']) && $dataArr['user_email'] != '') {
            $user_email = addslashes($dataArr['user_email']);
        } else {
            $user_email = '';
        }
        if (isset($dataArr['transaction']) && $dataArr['transaction'] != '') {
            $transaction = $dataArr['transaction'];
        } else {
            $transaction = '';
        }
        if (isset($dataArr['phone']) && $dataArr['phone'] != '') {
            $phone = $dataArr['phone'];
        } else {
            $phone = '';
        }
        if (isset($dataArr['association']) && $dataArr['association'] != '') {
            $association = $dataArr['association'];
        } else {
            $association = '';
        }
        if (isset($dataArr['last_transaction']) && $dataArr['last_transaction'] != '') {
            $last_transaction = $dataArr['last_transaction'];
        } else {
            $last_transaction = '';
        }
        if (isset($dataArr['afihc']) && $dataArr['afihc'] != '') {
            $afihc = $dataArr['afihc'];
        } else {
            $afihc = '';
        }
        if (isset($dataArr['source']) && $dataArr['source'] != '') {
            $source = $dataArr['source'];
        } else {
            $source = '';
        }
        if (isset($dataArr['membership_package_shipped']) && $dataArr['membership_package_shipped'] != '') {
            $membership_package_shipped = $dataArr['membership_package_shipped'];
        } else {
            $membership_package_shipped = '';
        }
        if (isset($dataArr['groups']) && $dataArr['groups'] != '') {
            $groups = implode(",", $dataArr['groups']);
        } else {
            $groups = '';
        }
        if (isset($dataArr['gender']) && $dataArr['gender'] != '') {
            if ($dataArr['gender'] == 'male')
                $gender = 1;
            else
                $gender = 2;
        } else {
            $gender = null;
        }
        if (isset($dataArr['state']) && $dataArr['state'] != '') {
            $state = $dataArr['state'];
        } else {
            $state = '';
        }
        if (isset($dataArr['from_date']) && $dataArr['from_date'] != '') {
            $from_date = $dataArr['from_date'];
        } else {
            $from_date = '';
        }
        if (isset($dataArr['to_date']) && $dataArr['to_date'] != '') {
            $to_date = $dataArr['to_date'];
        } else {
            $to_date = '';
        }
        if (isset($dataArr['membership_id']) && $dataArr['membership_id'] != '') {
            $membership_id = $dataArr['membership_id'];
        } else {
            $membership_id = '';
        }
        if (isset($dataArr['type']) && $dataArr['type'] != '') {
            $type = $dataArr['type'];
        } else {
            $type = '';
        }
        if (isset($dataArr['company_id']) && $dataArr['company_id'] != '') {
            $company = $dataArr['company_id'];
        } else {
            $company = '';
        }

        if (isset($dataArr['interests']) && $dataArr['interests'] != '') {
            $interests = implode(",", $dataArr['interests']);
        } else {
            $interests = '';
        }

        if (isset($dataArr['zip']) && $dataArr['zip'] != '') {
            $zip = $dataArr['zip'];
        } else {
            $zip = '';
        }
        if (isset($dataArr['country_id']) && $dataArr['country_id'] != '') {
            $country_id = $dataArr['country_id'];
        } else {
            $country_id = '';
        }

        if (isset($dataArr['transaction']) && $dataArr['transaction'] != '') {
            $transaction = $dataArr['transaction'];
        } else {
            $transaction = '';
        }

        if (isset($dataArr['membership_year']) && $dataArr['membership_year'] != '') {
            $membership_year = $dataArr['membership_year'];
        } else {
            $membership_year = '';
        }

        if (isset($dataArr['membership_title']) && $dataArr['membership_title'] != '') {
            $membership_title = $dataArr['membership_title'];
        } else {
            $membership_title = '';
        }
        
        // registration date - start
        if (isset($dataArr['registration_date_from']) && $dataArr['registration_date_from'] != '') {
            $registration_date_from = $dataArr['registration_date_from'];
        } else {
            $registration_date_from = '';
        }
        if (isset($dataArr['registration_date_to']) && $dataArr['registration_date_to'] != '') {
            $registration_date_to = $dataArr['registration_date_to'];
        } else {
            $registration_date_to = '';
        }
        // registration date - end

        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        $omx_num = isset($dataArr['omx_num']) ? $dataArr['omx_num'] : null;
        $start_index = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $record_limit = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $sort_field = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $sort_order = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        $contactId = (isset($dataArr['contact_id']) && $dataArr['contact_id'] != '') ? $dataArr['contact_id'] : '';
        $status = (isset($dataArr['status']) && $dataArr['status'] != '') ? $dataArr['status'] : '';
        $newContact = (isset($dataArr['new_contact'])&& $dataArr['new_contact'] != '') ? $dataArr['new_contact'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_getSearchContacts(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $user_email);
        $stmt->getResource()->bindParam(2, $transaction);
        $stmt->getResource()->bindParam(3, $phone);
        $stmt->getResource()->bindParam(4, $association);
        $stmt->getResource()->bindParam(5, $last_transaction);
        $stmt->getResource()->bindParam(6, $afihc);
        $stmt->getResource()->bindParam(7, $source);
        $stmt->getResource()->bindParam(8, $membership_package_shipped);
        $stmt->getResource()->bindParam(9, $groups);
        $stmt->getResource()->bindParam(10, $gender);
        $stmt->getResource()->bindParam(11, $state);
        $stmt->getResource()->bindParam(12, $from_date);
        $stmt->getResource()->bindParam(13, $to_date);
        $stmt->getResource()->bindParam(14, $membership_id);
        $stmt->getResource()->bindParam(15, $type);
        $stmt->getResource()->bindParam(16, $company);
        $stmt->getResource()->bindParam(17, $interests);
        $stmt->getResource()->bindParam(18, $zip);
        $stmt->getResource()->bindParam(19, $country_id);
        $stmt->getResource()->bindParam(20, $membership_year);
        $stmt->getResource()->bindParam(21, $user_id);
        $stmt->getResource()->bindParam(22, $start_index);
        $stmt->getResource()->bindParam(23, $record_limit);
        $stmt->getResource()->bindParam(24, $sort_field);
        $stmt->getResource()->bindParam(25, $sort_order);
        $stmt->getResource()->bindParam(26, $contactId);
        $stmt->getResource()->bindParam(27, $status);
        $stmt->getResource()->bindParam(28, $omx_num);
        $stmt->getResource()->bindParam(29, $membership_title);
        $stmt->getResource()->bindParam(30, $newContact);
        $stmt->getResource()->bindParam(31, $membership_cards_year);
        $stmt->getResource()->bindParam(32, $membership_cards_package_shipped);
        $stmt->getResource()->bindParam(33, $registration_date_from);
        $stmt->getResource()->bindParam(34, $registration_date_to);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
       // print_r($resultSet); die();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }
    
    /**
     * Function for to get conatct
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getContact($dataArr = null) { 

        $first_name = (isset($dataArr['first_name']) and trim($dataArr['first_name']) != "") ? trim($dataArr['first_name']) : "";
        $last_name = (isset($dataArr['last_name']) and trim($dataArr['last_name']) != "") ? trim($dataArr['last_name']) : "";
        $company_name = (isset($dataArr['company_name']) and trim($dataArr['company_name']) != "") ? trim($dataArr['company_name']) : "";
        $email_address = (isset($dataArr['email_address']) and trim($dataArr['email_address']) != "") ? trim($dataArr['email_address']) : "";

        $membership_cards_year = (isset($dataArr['membership_cards_year']) and trim($dataArr['membership_cards_year']) != "") ? trim($dataArr['membership_cards_year']) : "";
        $membership_cards_package_shipped = (isset($dataArr['membership_cards_package_shipped']) and trim($dataArr['membership_cards_package_shipped']) != "") ? trim($dataArr['membership_cards_package_shipped']) : "";

        if (isset($dataArr['user_email']) && $dataArr['user_email'] != '') {
            $user_email = addslashes($dataArr['user_email']);
        } else {
            $user_email = '';
        }
        if (isset($dataArr['transaction']) && $dataArr['transaction'] != '') {
            $transaction = $dataArr['transaction'];
        } else {
            $transaction = '';
        }
        if (isset($dataArr['phone']) && $dataArr['phone'] != '') {
            $phone = $dataArr['phone'];
        } else {
            $phone = '';
        }
        if (isset($dataArr['association']) && $dataArr['association'] != '') {
            $association = $dataArr['association'];
        } else {
            $association = '';
        }
        if (isset($dataArr['last_transaction']) && $dataArr['last_transaction'] != '') {
            $last_transaction = $dataArr['last_transaction'];
        } else {
            $last_transaction = '';
        }
        if (isset($dataArr['afihc']) && $dataArr['afihc'] != '') {
            $afihc = $dataArr['afihc'];
        } else {
            $afihc = '';
        }
        if (isset($dataArr['source']) && $dataArr['source'] != '') {
            $source = $dataArr['source'];
        } else {
            $source = '';
        }
        if (isset($dataArr['membership_package_shipped']) && $dataArr['membership_package_shipped'] != '') {
            $membership_package_shipped = $dataArr['membership_package_shipped'];
        } else {
            $membership_package_shipped = '';
        }
        if (isset($dataArr['groups']) && $dataArr['groups'] != '') {
            $groups = implode(",", $dataArr['groups']);
        } else {
            $groups = '';
        }
        if (isset($dataArr['corporate_tags']) && $dataArr['corporate_tags'] != '') {
            $corporate_tags = implode(",", $dataArr['corporate_tags']);
        } else {
            $corporate_tags = '';
        }
        if (isset($dataArr['gender']) && $dataArr['gender'] != '') {
            if ($dataArr['gender'] == 'male')
                $gender = 1;
            else
                $gender = 2;
        } else {
            $gender = null;
        }
        if (isset($dataArr['state']) && $dataArr['state'] != '') {
            $state = $dataArr['state'];
        } else {
            $state = '';
        }
        if (isset($dataArr['from_date']) && $dataArr['from_date'] != '') {
            $from_date = $dataArr['from_date'];
        } else {
            $from_date = '';
        }
        if (isset($dataArr['to_date']) && $dataArr['to_date'] != '') {
            $to_date = $dataArr['to_date'];
        } else {
            $to_date = '';
        }
        if (isset($dataArr['membership_id']) && $dataArr['membership_id'] != '') {
            $membership_id = $dataArr['membership_id'];
        } else {
            $membership_id = '';
        }
        if (isset($dataArr['type']) && $dataArr['type'] != '') {
            $type = $dataArr['type'];
        } else {
            $type = '';
        }
        if (isset($dataArr['company_id']) && $dataArr['company_id'] != '') {
            $company = $dataArr['company_id'];
        } else {
            $company = '';
        }

        if (isset($dataArr['interests']) && $dataArr['interests'] != '') {
            $interests = implode(",", $dataArr['interests']);
        } else {
            $interests = '';
        }

        if (isset($dataArr['zip']) && $dataArr['zip'] != '') {
            $zip = $dataArr['zip'];
        } else {
            $zip = '';
        }
        if (isset($dataArr['country_id']) && $dataArr['country_id'] != '') {
            $country_id = $dataArr['country_id'];
        } else {
            $country_id = '';
        }

        if (isset($dataArr['transaction']) && $dataArr['transaction'] != '') {
            $transaction = $dataArr['transaction'];
        } else {
            $transaction = '';
        }

        if (isset($dataArr['membership_year']) && $dataArr['membership_year'] != '') {
            $membership_year = $dataArr['membership_year'];
        } else {
            $membership_year = '';
        }

        if (isset($dataArr['membership_title']) && $dataArr['membership_title'] != '') {
            $membership_title = $dataArr['membership_title'];
        } else {
            $membership_title = '';
        }
        
        // registration date - start
        if (isset($dataArr['registration_date_from']) && $dataArr['registration_date_from'] != '') {
            $registration_date_from = $dataArr['registration_date_from'];
        } else {
            $registration_date_from = '';
        }
        if (isset($dataArr['registration_date_to']) && $dataArr['registration_date_to'] != '') {
            $registration_date_to = $dataArr['registration_date_to'];
        } else {
            $registration_date_to = '';
        }
        // registration date - end

        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        $omx_num = isset($dataArr['omx_num']) ? $dataArr['omx_num'] : null;
        $start_index = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $record_limit = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $sort_field = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $sort_order = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        $contactId = (isset($dataArr['contact_id']) && $dataArr['contact_id'] != '') ? $dataArr['contact_id'] : '';
        $status = (isset($dataArr['status']) && $dataArr['status'] != '') ? $dataArr['status'] : '';
        $newContact = (isset($dataArr['new_contact'])&& $dataArr['new_contact'] != '') ? $dataArr['new_contact'] : '';
        $stmt = $this->dbAdapter->createStatement();
       
        $stmt->prepare('CALL usp_usr_getContacts(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $user_email);
        $stmt->getResource()->bindParam(2, $transaction);
        $stmt->getResource()->bindParam(3, $phone);
        $stmt->getResource()->bindParam(4, $association);
        $stmt->getResource()->bindParam(5, $last_transaction);
        $stmt->getResource()->bindParam(6, $afihc);
        $stmt->getResource()->bindParam(7, $source);
        $stmt->getResource()->bindParam(8, $membership_package_shipped);
        $stmt->getResource()->bindParam(9, $groups);
        $stmt->getResource()->bindParam(10, $gender);
        $stmt->getResource()->bindParam(11, $state);
        $stmt->getResource()->bindParam(12, $from_date);
        $stmt->getResource()->bindParam(13, $to_date);
        $stmt->getResource()->bindParam(14, $membership_id);
        $stmt->getResource()->bindParam(15, $type);
        $stmt->getResource()->bindParam(16, $company);
        $stmt->getResource()->bindParam(17, $interests);
        $stmt->getResource()->bindParam(18, $zip);
        $stmt->getResource()->bindParam(19, $country_id);
        $stmt->getResource()->bindParam(20, $membership_year);
        $stmt->getResource()->bindParam(21, $user_id);
        $stmt->getResource()->bindParam(22, $start_index);
        $stmt->getResource()->bindParam(23, $record_limit);
        $stmt->getResource()->bindParam(24, $sort_field);
        $stmt->getResource()->bindParam(25, $sort_order);
        $stmt->getResource()->bindParam(26, $contactId);
        $stmt->getResource()->bindParam(27, $status);
        $stmt->getResource()->bindParam(28, $omx_num);
        $stmt->getResource()->bindParam(29, $membership_title);
        $stmt->getResource()->bindParam(30, $newContact);
        $stmt->getResource()->bindParam(31, $membership_cards_year);
        $stmt->getResource()->bindParam(32, $membership_cards_package_shipped);
        $stmt->getResource()->bindParam(33, $registration_date_from);
        $stmt->getResource()->bindParam(34, $registration_date_to);
        
        $stmt->getResource()->bindParam(35, $first_name);
        $stmt->getResource()->bindParam(36, $last_name);
        $stmt->getResource()->bindParam(37, $company_name);
        $stmt->getResource()->bindParam(38, $email_address);
        $stmt->getResource()->bindParam(39, $corporate_tags);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }
    
     /**
     * Function for to update Contact Membership Cards
     * @author Icreon Tech - NS
     * @return Arr
     * @param Array
     */
    public function updateContactMembershipCards($params = array()) {
 
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_updateContactMembershipCards(?,?,?)');
            $stmt->getResource()->bindParam(1, $params['user_ids']);
            $stmt->getResource()->bindParam(2, $params['membership_package_value']);
            $stmt->getResource()->bindParam(3, $params['year']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    

    /**
     * Function for to update conatct search
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function updateContactSearch($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_updateContactSearch(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['crm_user_id']);
            $stmt->getResource()->bindParam(2, $params['search_name']);
            $stmt->getResource()->bindParam(3, $params['search_query']);
            $stmt->getResource()->bindParam(4, $params['modifiend_date']);
            $stmt->getResource()->bindParam(5, $params['search_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to insert conatct search
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function saveContactSearch($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_saveContactSearch(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['crm_user_id']);
            $stmt->getResource()->bindParam(2, $params['search_name']);
            $stmt->getResource()->bindParam(3, $params['search_query']);
            $stmt->getResource()->bindParam(4, $params['added_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to delete saved search
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function deleteSearch($params = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_deleteContactSearch(?,?,?,?)');
        $stmt->getResource()->bindParam(1, $params['crm_user_id']);
        $stmt->getResource()->bindParam(2, $params['is_delete']);
        $stmt->getResource()->bindParam(3, $params['modified_date']);
        $stmt->getResource()->bindParam(4, $params['search_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for to delete user website
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function deleteWebsite($params = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_deleteWebsite(?)');
        $stmt->getResource()->bindParam(1, $params['user_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for to get saved conatct search
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getSavedSearch($params = array()) {

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_getSavedSearch(?,?,?)');
        $stmt->getResource()->bindParam(1, $params['crm_user_id']);
        $stmt->getResource()->bindParam(2, $params['is_active']);
        $stmt->getResource()->bindParam(3, $params['contact_search_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to insert contact notes
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function insertNotes($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_insertNotes(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['user_id']);
            $stmt->getResource()->bindParam(2, $params['notes']);
            $stmt->getResource()->bindParam(3, $params['notes_private']);
            $stmt->getResource()->bindParam(4, $params['added_by']);
            $stmt->getResource()->bindParam(5, $params['add_date']);
            $stmt->getResource()->bindParam(6, $params['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to insert contact 
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function insertContact($params = array()) {
        $isActive = '1';
        $isActiveted = '1';
        $emailVerified = '1';

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_insertContact(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $params['contact_type']);
        $stmt->getResource()->bindParam(2, $params['title']);
        $stmt->getResource()->bindParam(3, $params['first_name']);
        $stmt->getResource()->bindParam(4, $params['middle_name']);
        $stmt->getResource()->bindParam(5, $params['last_name']);
        $stmt->getResource()->bindParam(6, $params['suffix']);
        $stmt->getResource()->bindParam(7, $params['email_id']);
        $stmt->getResource()->bindParam(8, $params['no_email_address']);
        $stmt->getResource()->bindParam(9, $params['password']);
        $stmt->getResource()->bindParam(10, $params['salt']);
        $stmt->getResource()->bindParam(11, $params['demographics_gender']);
        $stmt->getResource()->bindParam(12, $params['upload_profile_image_id']);
        $stmt->getResource()->bindParam(13, $params['demographics_marital_status']);
        $stmt->getResource()->bindParam(14, $params['demographics_age_limit']);
        $stmt->getResource()->bindParam(15, $params['demographics_income_range']);
        $stmt->getResource()->bindParam(16, $params['demographics_ethnicity']);
        $stmt->getResource()->bindParam(17, $params['demographics_nationality']);
        $stmt->getResource()->bindParam(18, $params['source']);
        $stmt->getResource()->bindParam(19, $params['webaccesss_security_question']);
        $stmt->getResource()->bindParam(20, $params['webaccesss_security_answer']);
        $stmt->getResource()->bindParam(21, $params['webaccesss_enabled_login']);
        $stmt->getResource()->bindParam(22, $params['add_date']);
        $stmt->getResource()->bindParam(23, $params['modified_date']);
        $stmt->getResource()->bindParam(24, $params['corporate_company_name']);
        $stmt->getResource()->bindParam(25, $params['corporate_legal_name']);
        $stmt->getResource()->bindParam(26, $params['corporate_sic_code']);
        $stmt->getResource()->bindParam(27, $params['username']);
        $stmt->getResource()->bindParam(28, $params['don_not_have_website']);
        $stmt->getResource()->bindParam(29, $params['verify_code']);
        $stmt->getResource()->bindParam(30, $isActive);
        $stmt->getResource()->bindParam(31, $isActiveted);
        $stmt->getResource()->bindParam(32, $emailVerified);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            return false;
        }
    }

    /**
     * Function for to insert contact address info
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function insertAddressInfo($params = array()) {
        try {

            $stmt = $this->dbAdapter->createStatement();
            $title = isset($params['title']) && !is_null($params['title']) ? $params['title'] : '';
            $firstName = isset($params['first_name']) && !is_null($params['first_name']) ? $params['first_name'] : '';
			$middleName = isset($params['middle_name']) && !is_null($params['middle_name']) ? $params['middle_name'] : '';
            $lastName = isset($params['last_name']) && !is_null($params['last_name']) ? $params['last_name'] : '';
            
            $addressinfo_location_type = isset($params['addressinfo_location_type']) && !is_null($params['addressinfo_location_type']) ? $params['addressinfo_location_type'] : '';
            $addressinfo_location = isset($params['addressinfo_location']) && !is_null($params['addressinfo_location']) ? $params['addressinfo_location'] : '';
            $addressinfo_street_address_1 = isset($params['addressinfo_street_address_1']) && !is_null($params['addressinfo_street_address_1']) ? rtrim($params['addressinfo_street_address_1'], ',') : '';
            $addressinfo_street_address_2 = isset($params['addressinfo_street_address_2']) && !is_null($params['addressinfo_street_address_2']) ? rtrim($params['addressinfo_street_address_2'], ',') : '';
            $addressinfo_city = isset($params['addressinfo_city']) && !is_null($params['addressinfo_city']) ? $params['addressinfo_city'] : '';
            $addressinfo_state = isset($params['addressinfo_state']) && !is_null($params['addressinfo_state']) ? $params['addressinfo_state'] : '';
            $addressinfo_zip = isset($params['addressinfo_zip']) && !is_null($params['addressinfo_zip']) ? $params['addressinfo_zip'] : '';
            $addressinfo_country = isset($params['addressinfo_country']) && !is_null($params['addressinfo_country']) ? $params['addressinfo_country'] : '';
            $address_type = isset($params['address_type']) && !is_null($params['address_type']) ? $params['address_type'] : '';
            $params['add_date'] = DATE_TIME_FORMAT;
            $params['modified_date'] = DATE_TIME_FORMAT;
            $stmt->prepare('CALL usp_usr_insertAddressInformations(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['user_id']);
            $stmt->getResource()->bindParam(2, $addressinfo_location_type);
            $stmt->getResource()->bindParam(3, $addressinfo_location);
            $stmt->getResource()->bindParam(4, $addressinfo_street_address_1);
            $stmt->getResource()->bindParam(5, $addressinfo_street_address_2);
            $stmt->getResource()->bindParam(6, $addressinfo_city);
            $stmt->getResource()->bindParam(7, $addressinfo_state);
            $stmt->getResource()->bindParam(8, $addressinfo_zip);
            $stmt->getResource()->bindParam(9, $addressinfo_country);
            $stmt->getResource()->bindParam(10, $params['add_date']);
            $stmt->getResource()->bindParam(11, $params['modified_date']);
            $stmt->getResource()->bindParam(12, $address_type);
            $stmt->getResource()->bindParam(13, $title);
            $stmt->getResource()->bindParam(14, $firstName);
            $stmt->getResource()->bindParam(15, $middleName);
            $stmt->getResource()->bindParam(16, $lastName);
            $result = $stmt->execute();
            $statement = $result->getResource();
            //$resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();            
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * update address info
     * @param type $params
     * @return boolean
     */
    public function updateAddressInfo($params = array())
    {
        $resp = false;
        $addressinfo_id = isset($params['addressinfo_id']) && !is_null($params['addressinfo_id']) ? $params['addressinfo_id'] : '';
        if (trim($addressinfo_id) != '')
        {
            $title = isset($params['title']) && !is_null($params['title']) ? $params['title'] : '';
            $firstName = isset($params['first_name']) && !is_null($params['first_name']) ? $params['first_name'] : '';
            //  $middleName = isset($params['middle_name']) && !is_null($params['middle_name']) ? $params['middle_name'] : '';
            $lastName = isset($params['last_name']) && !is_null($params['last_name']) ? $params['last_name'] : '';

            $addressinfo_location_type = isset($params['addressinfo_location_type']) && !is_null($params['addressinfo_location_type']) ? $params['addressinfo_location_type'] : '';
            $addressinfo_location = isset($params['addressinfo_location']) && !is_null($params['addressinfo_location']) ? $params['addressinfo_location'] : '';
            $addressinfo_street_address_1 = isset($params['addressinfo_street_address_1']) && !is_null($params['addressinfo_street_address_1']) ? rtrim($params['addressinfo_street_address_1'], ',') : '';
            $addressinfo_street_address_2 = isset($params['addressinfo_street_address_2']) && !is_null($params['addressinfo_street_address_2']) ? rtrim($params['addressinfo_street_address_2'], ',') : '';
            $addressinfo_city = isset($params['addressinfo_city']) && !is_null($params['addressinfo_city']) ? $params['addressinfo_city'] : '';
            $addressinfo_state = isset($params['addressinfo_state']) && !is_null($params['addressinfo_state']) ? $params['addressinfo_state'] : '';
            $addressinfo_zip = isset($params['addressinfo_zip']) && !is_null($params['addressinfo_zip']) ? $params['addressinfo_zip'] : '';
            $addressinfo_country = isset($params['addressinfo_country']) && !is_null($params['addressinfo_country']) ? $params['addressinfo_country'] : '';
            $address_type = isset($params['address_type']) && !is_null($params['address_type']) ? $params['address_type'] : '';
            $params['add_date'] = DATE_TIME_FORMAT;
            $params['modified_date'] = DATE_TIME_FORMAT;
            try
            {

                $stmt = $this->dbAdapter->createStatement();

                $stmt->prepare('CALL usp_usr_updateAddressInformations(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
                $stmt->getResource()->bindParam(1, $addressinfo_id);
                $stmt->getResource()->bindParam(2, $params['user_id']);
                $stmt->getResource()->bindParam(3, $addressinfo_location_type);
                $stmt->getResource()->bindParam(4, $addressinfo_location);
                $stmt->getResource()->bindParam(5, $addressinfo_street_address_1);
                $stmt->getResource()->bindParam(6, $addressinfo_street_address_2);
                $stmt->getResource()->bindParam(7, $addressinfo_city);
                $stmt->getResource()->bindParam(8, $addressinfo_state);
                $stmt->getResource()->bindParam(9, $addressinfo_zip);
                $stmt->getResource()->bindParam(10, $addressinfo_country);
                $stmt->getResource()->bindParam(11, $params['add_date']);
                $stmt->getResource()->bindParam(12, $params['modified_date']);
                $stmt->getResource()->bindParam(13, $address_type);
                $stmt->getResource()->bindParam(14, $title);
                $stmt->getResource()->bindParam(15, $firstName);
                $stmt->getResource()->bindParam(16, $lastName);

                //$stmt->getResource()->bindParam(16, $lastName);
                $result = $stmt->execute();
                $statement = $result->getResource();
                //$resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
                $statement->closeCursor();
                $resp = true;
            }
            catch (Exception $e)
            {
                $resp = false;
            }
        }
        
        return $resp;
    }

    /**
     * Function for to insert alternate conact detail
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function insertAlternateContacts($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_insertAlternateContacts(?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['user_id']);
            $stmt->getResource()->bindParam(2, $params['alt_name']);
            $stmt->getResource()->bindParam(3, $params['alt_email']);
            $stmt->getResource()->bindParam(4, $params['alt_phoneno']);
            $stmt->getResource()->bindParam(5, $params['alt_relationship']);
            $stmt->getResource()->bindParam(6, $params['add_date']);
            $stmt->getResource()->bindParam(7, $params['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to insert contact Interest
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function insertInterests($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_insertInterests(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['user_id']);
            $stmt->getResource()->bindParam(2, $params['interest_id']);
            $stmt->getResource()->bindParam(3, $params['add_date']);
            $stmt->getResource()->bindParam(4, $params['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to insert phone information
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function insertPhoneInformations($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_insertPhoneInformations(?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['user_id']);
            $stmt->getResource()->bindParam(2, $params['phone_type']);
            $stmt->getResource()->bindParam(3, $params['country_code']);
            $stmt->getResource()->bindParam(4, $params['area_code']);
            $stmt->getResource()->bindParam(5, $params['phone_no']);
            $stmt->getResource()->bindParam(6, $params['continfo_primary']);
            $stmt->getResource()->bindParam(7, $params['extension']);
            $stmt->getResource()->bindParam(8, $params['add_date']);
            $stmt->getResource()->bindParam(9, $params['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to insert phone information
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function insertCommunicationPreferences($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_insertCommunicationPreferences(?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['user_id']);
            $stmt->getResource()->bindParam(2, $params['communicationpref_email_greetings']);
            $stmt->getResource()->bindParam(3, $params['communicationpref_custome_email_greetings']);
            $stmt->getResource()->bindParam(4, $params['communicationpref_postal_greetings']);
            $stmt->getResource()->bindParam(5, $params['communicationpref_custome_postal_greetings']);
            $stmt->getResource()->bindParam(6, $params['communicationpref_address']);
            $stmt->getResource()->bindParam(7, $params['communicationpref_custome_address_greetings']);
            $stmt->getResource()->bindParam(8, $params['communicationpref_privacy']);
            $stmt->getResource()->bindParam(9, $params['communicationpref_preferred_method']);
            $stmt->getResource()->bindParam(10, $params['add_date']);
            $stmt->getResource()->bindParam(11, $params['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to insert phone information
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function insertGroups($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_insertGroups(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['user_id']);
            $stmt->getResource()->bindParam(2, $params['groups']);
            $stmt->getResource()->bindParam(3, $params['add_date']);
            $stmt->getResource()->bindParam(4, $params['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to insert phone information
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function insertEducation($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_insertEducations(?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['user_id']);
            $stmt->getResource()->bindParam(2, $params['edu_degree_level']);
            $stmt->getResource()->bindParam(3, $params['edu_institution']);
            $stmt->getResource()->bindParam(4, $params['edu_attended_year']);
            $stmt->getResource()->bindParam(5, $params['edu_major']);
            $stmt->getResource()->bindParam(6, $params['edu_gpa']);
            $stmt->getResource()->bindParam(7, $params['add_date']);
            $stmt->getResource()->bindParam(8, $params['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to insert user tags
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function insertUserTags($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_insertTags(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['user_id']);
            $stmt->getResource()->bindParam(2, $params['tag_id']);
            $stmt->getResource()->bindParam(3, $params['add_date']);
            $stmt->getResource()->bindParam(4, $params['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to insert user tags
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function insertMasterTags($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_com_insertTags(?,?,?)');
            $stmt->getResource()->bindParam(1, $params['tagsandgroups_company_name']);
            $stmt->getResource()->bindParam(2, $params['add_date']);
            $stmt->getResource()->bindParam(3, $params['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if ($resultSet) {
                return $resultSet[0]['LAST_INSERT_ID'];
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to insert master companies
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function insertMasterCompanies($params = array()) {
        try {
            $addedDate = DATE_TIME_FORMAT;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_com_insertCompanies(?,?,?)');
            $stmt->getResource()->bindParam(1, $params['company_name']);
            $stmt->getResource()->bindParam(2, $addedDate);
            $stmt->getResource()->bindParam(3, $addedDate);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if ($resultSet) {
                return $resultSet[0]['LAST_INSERT_ID'];
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to insert public documen t
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function insertDocument($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_insertDocuments(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['user_id']);
            $stmt->getResource()->bindParam(2, $params['publicdocs_description']);
            $stmt->getResource()->bindParam(3, $params['publicdocs_upload']);
            $stmt->getResource()->bindParam(4, $params['add_date']);
            $stmt->getResource()->bindParam(5, $params['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to insert public documen t
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function insertAfihc($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_insertAfihc(?,?,?)');
            $stmt->getResource()->bindParam(1, $params['user_id']);
            $stmt->getResource()->bindParam(2, $params['afihc']);
            $stmt->getResource()->bindParam(3, $params['add_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to insert websits url
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function insertWebsite($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_insertWebsites(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['user_id']);
            $stmt->getResource()->bindParam(2, $params['website']);
            $stmt->getResource()->bindParam(3, $params['add_date']);
            $stmt->getResource()->bindParam(4, $params['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to delete contact
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function deleteContact($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_deleteContact(?)');
            $stmt->getResource()->bindParam(1, $params['conatact_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get alternate contact
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getUserAlternateContact($params = array()) {
        try {
            $user_id = isset($params['user_id']) ? $params['user_id'] : null;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getAlternateContacts(?)');
            $stmt->getResource()->bindParam(1, $user_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet[0];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get education
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getUserEducations($params = array()) {
        try {
            $user_id = isset($params['user_id']) ? $params['user_id'] : null;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getEducations(?)');
            $stmt->getResource()->bindParam(1, $user_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get notes
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getUserNotes($params = array()) {
        try {
            $user_id = isset($params['user_id']) ? $params['user_id'] : null;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getNotes(?)');
            $stmt->getResource()->bindParam(1, $user_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get user document
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getUserDocuments($params = array()) {
        try {
            $user_id = isset($params['user_id']) ? $params['user_id'] : null;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getDocuments(?)');
            $stmt->getResource()->bindParam(1, $user_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet[0];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get user tags
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getUserTags($params = array()) {
        try {
            $user_id = isset($params['user_id']) ? $params['user_id'] : null;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getTags(?)');
            $stmt->getResource()->bindParam(1, $user_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get user groups
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getUserGroups($params = array()) {
        try {
            $user_id = isset($params['user_id']) ? $params['user_id'] : null;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getGroups(?)');
            $stmt->getResource()->bindParam(1, $user_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get user interest
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getUserInterests($params = array()) {
        try {
            $user_id = isset($params['user_id']) ? $params['user_id'] : null;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getInterests(?)');
            $stmt->getResource()->bindParam(1, $user_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get user address info
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getUserAddressInformations($params = array()) {
        try {
            
            $user_id = (isset($params['user_id']) and trim($params['user_id']) != "") ? $params['user_id'] : null;
            $address_information_id = (isset($params['address_information_id']) and trim($params['address_information_id']) != "") ? $params['address_information_id'] : null;
            $addressLocation = (isset($params['address_location']) and trim($params['address_location']) != "") ? $params['address_location'] : null;
            
            if($user_id != null or $address_information_id != null or $addressLocation != null) {
                
                $stmt = $this->dbAdapter->createStatement();
                $stmt->prepare('CALL usp_usr_getAddressInformations(?,?,?)');
                $stmt->getResource()->bindParam(1, $user_id);
                $stmt->getResource()->bindParam(2, $address_information_id);
                $stmt->getResource()->bindParam(3, $addressLocation);
                $result = $stmt->execute();
                $statement = $result->getResource();
                $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
                $statement->closeCursor();
                if (!empty($resultSet))
                    return $resultSet;
                else
                    return false;
                
            } 
            else {
                 return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get user PhoneInformations
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getUserPhoneInformations($params = array()) {
        try {
            $userId = isset($params['user_id']) ? $params['user_id'] : null;
            $phoneId = isset($params['phone_id']) ? $params['phone_id'] : null;

            if (isset($params['is_primary']) and trim($params['is_primary']) != "" and is_numeric(trim($params['is_primary']))) {
                $isPrimary = trim($params['is_primary']);
            } else {
                $isPrimary = '';
            }

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getPhoneInformations(?,?,?)');
            $stmt->getResource()->bindParam(1, $userId);
            $stmt->getResource()->bindParam(2, $phoneId);
            $stmt->getResource()->bindParam(3, $isPrimary);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get user website
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getUserWebsites($params = array()) {
        try {
            $user_id = isset($params['user_id']) ? $params['user_id'] : null;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getWebsites(?)');
            $stmt->getResource()->bindParam(1, $user_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get user Afihc
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getUserAfihc($params = array()) {
        try {
            $user_id = isset($params['user_id']) ? $params['user_id'] : null;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getAfihc(?)');
            $stmt->getResource()->bindParam(1, $user_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get user Afihc
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getUserDetail($params = array()) {
        try {
            $user_id = isset($params['user_id']) ? $params['user_id'] : null;
            $currentTime = DATE_TIME_FORMAT;
            $front_end = (isset($params['front_end']) && !empty($params['front_end'])) ? $params['front_end'] : null;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getUserDetail(?,?,?)');
            $stmt->getResource()->bindParam(1, $user_id);
            $stmt->getResource()->bindParam(2, $currentTime);
            $stmt->getResource()->bindParam(3, $front_end);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet[0];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * Function for to get user Afihc
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getUserMemberShipDetail($params = array()) {
        try {
            $user_id = isset($params['user_id']) ? $params['user_id'] : null;
            $currentTime = DATE_TIME_FORMAT;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getUserMemberShipDetail(?,?)');
            $stmt->getResource()->bindParam(1, $user_id);
            $stmt->getResource()->bindParam(2, $currentTime);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet[0];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }    

    /**
     * Function used to get user communication preferences
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getCommunicationPreferences($params = array()) {
        try {
            $user_id = isset($params['user_id']) ? $params['user_id'] : null;
            $emailId = isset($params['email_id']) ? $params['email_id'] : null;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getCommunicationPreferences(?,?)');
            $stmt->getResource()->bindParam(1, $user_id);
            $stmt->getResource()->bindParam(2, $emailId);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet[0];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function used to update  user communication preferences
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function updateCommunicationPreferences($params = array()) {
        try {
            $communicationPreferenceId = isset($params['communicationPreferenceId']) ? $params['communicationPreferenceId'] : null;
            $updatedPrivacy = isset($params['updatedPrivacy']) ? $params['updatedPrivacy'] : null;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_updateCommunicationPreferences(?,?)');
            $stmt->getResource()->bindParam(1, $communicationPreferenceId);
            $stmt->getResource()->bindParam(2, $updatedPrivacy);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function used to update user detail 
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function updateUserDetails($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_updateContact(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['user_id']);
            $stmt->getResource()->bindParam(2, $params['title']);
            $stmt->getResource()->bindParam(3, $params['first_name']);
            $stmt->getResource()->bindParam(4, $params['middle_name']);
            $stmt->getResource()->bindParam(5, $params['last_name']);
            $stmt->getResource()->bindParam(6, $params['suffix']);
            $stmt->getResource()->bindParam(7, $params['email_id']);
            $stmt->getResource()->bindParam(8, $params['no_email_address']);
            $stmt->getResource()->bindParam(9, $params['demographics_gender']);
            $stmt->getResource()->bindParam(10, $params['upload_profile_image_id']);
            $stmt->getResource()->bindParam(11, $params['demographics_marital_status']);
            $stmt->getResource()->bindParam(12, $params['demographics_age_limit']);
            $stmt->getResource()->bindParam(13, $params['demographics_income_range']);
            $stmt->getResource()->bindParam(14, $params['demographics_ethnicity']);
            $stmt->getResource()->bindParam(15, $params['demographics_nationality']);
            $stmt->getResource()->bindParam(16, $params['source']);
            $stmt->getResource()->bindParam(17, $params['webaccesss_security_question']);
            $stmt->getResource()->bindParam(18, $params['webaccesss_security_answer']);
            $stmt->getResource()->bindParam(19, $params['modified_date']);
            $stmt->getResource()->bindParam(20, $params['corporate_company_name']);
            $stmt->getResource()->bindParam(21, $params['corporate_legal_name']);
            $stmt->getResource()->bindParam(22, $params['corporate_sic_code']);
            $stmt->getResource()->bindParam(23, $params['contact_status']);
            $stmt->getResource()->bindParam(24, $params['reason']);
            $stmt->getResource()->bindParam(25, $params['reason_other']);
            $stmt->getResource()->bindParam(26, $params['webaccesss_enabled_login']);
            $stmt->getResource()->bindParam(27, $params['email_verification']);
            $stmt->getResource()->bindParam(28, $params['don_not_have_website']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function used to delete user detail 
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function deleteUserDetails($params = array()) {
        try {
            $user_id = isset($params['user_id']) ? $params['user_id'] : null;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_deleteUserDetails(?)');
            $stmt->getResource()->bindParam(1, $user_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function used to get user searches
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getUserSearches($dataArr = array()) {
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        $type = isset($dataArr['type']) ? $dataArr['type'] : null;
        $start_index = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $record_limit = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $sort_field = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $sort_order = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_getUserSearch(?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $user_id);
        $stmt->getResource()->bindParam(2, $type);
        $stmt->getResource()->bindParam(3, $start_index);
        $stmt->getResource()->bindParam(4, $record_limit);
        $stmt->getResource()->bindParam(5, $sort_field);
        $stmt->getResource()->bindParam(6, $sort_order);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to insert popup contact
     * @author Icreon Tech - DT
     * @return Arr
     * @param Array
     */
    public function insertPopupContact($params = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_insertPopupContact(?,?,?,?,?,?,?,?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $params['title']);
        $stmt->getResource()->bindParam(2, $params['first_name']);
        $stmt->getResource()->bindParam(3, $params['middle_name']);
        $stmt->getResource()->bindParam(4, $params['last_name']);
        $stmt->getResource()->bindParam(5, $params['suffix']);
        $stmt->getResource()->bindParam(6, $params['email_id']);
        $stmt->getResource()->bindParam(7, $params['salt']);
        $stmt->getResource()->bindParam(8, $params['password']);
        $stmt->getResource()->bindParam(9, $params['source_id']);
        $stmt->getResource()->bindParam(10, $params['webaccesss_enabled_login']);
        $stmt->getResource()->bindParam(11, $params['verify_code']);
        $stmt->getResource()->bindParam(11, $params['is_active']);
        $stmt->getResource()->bindParam(12, $params['add_date']);
        $stmt->getResource()->bindParam(13, $params['modified_date']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            return false;
        }
    }

    /**
     * Function used to get user visitation
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getUserVisitation($dataArr = array()) {
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        $start_index = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $record_limit = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $sort_field = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $sort_order = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_pos_usr_getUserVisitation(?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $user_id);
        $stmt->getResource()->bindParam(2, $start_index);
        $stmt->getResource()->bindParam(3, $record_limit);
        $stmt->getResource()->bindParam(4, $sort_field);
        $stmt->getResource()->bindParam(5, $sort_order);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function used to update contact public doc
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function deleteContactPublicDoc($dataArr) {
        $userId = isset($dataArr['userId']) ? $dataArr['userId'] : null;
        $updatedDoc = (isset($dataArr['updatedDoc']) && $dataArr['updatedDoc'] != '') ? $dataArr['updatedDoc'] : '';
        $modifiedDate = DATE_TIME_FORMAT;

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_deleteContactPublicDoc(?,?,?)');
        $stmt->getResource()->bindParam(1, $userId);
        $stmt->getResource()->bindParam(2, $updatedDoc);
        $stmt->getResource()->bindParam(3, $modifiedDate);
        $result = $stmt->execute();
    }

    /**
     * Function used to get user searches
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function isUserSearches($params = array()) {
        $user_id = isset($params['user_id']) ? $params['user_id'] : null;

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_isUserSearch(?)');
        $stmt->getResource()->bindParam(1, $user_id);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet[0]['totalRecord'];
    }

    /**
     * Function used to get user searches
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function updateContactId($params = array()) {
        $userId = isset($params['user_id']) ? $params['user_id'] : null;
        //$contactId = isset($params['contact_id']) ? $params['contact_id'] : null;
        $curDate = date("Ym");
        $contactId = $curDate . "" . str_pad($userId, 10, 0, STR_PAD_LEFT);
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_updateContactId(?,?)');
        $stmt->getResource()->bindParam(1, $userId);
        $stmt->getResource()->bindParam(2, $contactId);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    public function updateMembershipSaveSearch($params = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_update_membership_save_search(?,?,?,?)');
        $stmt->getResource()->bindParam(1, $params['membership_search_id']);
        $stmt->getResource()->bindParam(2, $params['bonus_saved_search_in_days']);
        $stmt->getResource()->bindParam(3, $params['max_saved_search_in_days']);
        $stmt->getResource()->bindParam(4, $params['modified_date']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    public function updatePrimaryAddressInformations($params = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $modiliedDate = DATE_TIME_FORMAT;
        $stmt->prepare('CALL usp_usr_updatePrimaryAddressInformation(?,?)');
        $stmt->getResource()->bindParam(1, $params['user_id']);
        $stmt->getResource()->bindParam(2, $modiliedDate);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    public function updateUserPrimaryPhone($params = array()) {
        $stmt = $this->dbAdapter->createStatement();

        $stmt->prepare('CALL usp_usr_updatePrimaryPhoneno(?)');
        $stmt->getResource()->bindParam(1, $params['user_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for to insert phone information from checkout
     * @author Icreon Tech - SR
     * @return Arr
     * @param Array
     */
    public function insertPhoneInformationsCheckout($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_insertPhoneInformationsCheckout(?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['user_id']);
            $stmt->getResource()->bindParam(2, $params['phone_type']);
            $stmt->getResource()->bindParam(3, $params['country_code']);
            $stmt->getResource()->bindParam(4, $params['area_code']);
            $stmt->getResource()->bindParam(5, $params['phone_no']);
            $stmt->getResource()->bindParam(6, $params['continfo_primary']);
            $stmt->getResource()->bindParam(7, $params['extension']);
            $stmt->getResource()->bindParam(8, $params['add_date']);
            $stmt->getResource()->bindParam(9, $params['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
    public function updateStatusUpdate($params = array()){
        try {
            //asd($params);
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_updateLockStatus(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['user_id']);
            $stmt->getResource()->bindParam(2, $params['status']);
            $stmt->getResource()->bindParam(3, $params['active']);
            $stmt->getResource()->bindParam(4, $params['modify_date']);            
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }


	public function getRecentContact($dataArr = null) {         
        
        $start_index = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $record_limit = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $sort_field = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $sort_order = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        $newContact = (isset($dataArr['new_contact'])&& $dataArr['new_contact'] != '') ? $dataArr['new_contact'] : '';
        
		$stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_getRecentContacts(?,?,?,?,?)');
        
        $stmt->getResource()->bindParam(1, $start_index);
        $stmt->getResource()->bindParam(2, $record_limit);
        $stmt->getResource()->bindParam(3, $sort_field);
        $stmt->getResource()->bindParam(4, $sort_order);        
        $stmt->getResource()->bindParam(5, $newContact);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

	

	public function mergeDuplicateEmailRecord($params = array())
    {
        try
		{
			$stmt = $this->dbAdapter->createStatement();
			$stmt->prepare('CALL usp_usr_mergeDuplicateEmailRecord(?,?)');
			$stmt->getResource()->bindParam(1, $params['old_user_id']);
			$stmt->getResource()->bindParam(2, $params['new_user_id']);
			$result = $stmt->execute();
			$statement = $result->getResource();
			$statement->closeCursor();
			return $result;
		}
		catch (Exception $e)
		{
			return false;
		}
    }
    /**
     * Function to get minimum membership discount
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getMinimumDiscountAmount() {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getMinMembershipDiscountAmount()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet[0];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }
    /**
     * Function to get the user visitation counts
     * @author Icreon Tech - SR
     * @return Arr
     * @param Array
     */        
	public function getUserVisitationCount($param = array()){
        try {
			$user_id = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '';        
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getUserVisitationCount(?)');
            $stmt->getResource()->bindParam(1, $user_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet[0];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }	
	}
	
}