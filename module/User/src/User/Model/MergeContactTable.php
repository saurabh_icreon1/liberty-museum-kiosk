<?php

/**
 * This model is used for find and merge duplicate contact
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This model is used for find and merge duplicate contact
 * @package    User
 * @author     Icreon Tech - DG
 */
class MergeContactTable {

    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
    }

    /**
     * Function for to insert dedupe exception
     * @author Icreon Tech - DG
     * @return array
     */
    public function insertDedupeException($dataArr) {
        $userId = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        $duplicateUserId = isset($dataArr['duplicate_user_id']) ? $dataArr['duplicate_user_id'] : null;
        $addedDate = DATE_TIME_FORMAT;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_insertDedupeException(?,?,?)');
        $stmt->getResource()->bindParam(1, $userId);
        $stmt->getResource()->bindParam(2, $duplicateUserId);
        $stmt->getResource()->bindParam(3, $addedDate);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for get duplicate contact
     * @author Icreon Tech - DG
     * @return array
     */
    public function getDuplicateContact($rule, $group) {
        $stmt = $this->dbAdapter->createStatement();
        $rule = (isset($rule)) ? $rule : null;
        $group = (isset($group)) ? $group : null;
        $stmt->prepare('CALL usp_usr_getDuplicateContact(?,?)');
        $stmt->getResource()->bindParam(1, $rule);
        $stmt->getResource()->bindParam(2, $group);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for get Dedupe Exception
     * @author Icreon Tech - DG
     * @return array
     */
    public function getDedupeException() {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_getDedupeException()');
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for edit duplicate Contact
     * @author Icreon Tech - DG
     * @return array
     */
    public function editDuplicateContact($dataArr) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_updateDuplicateContact(?,?,?,?,?,?,?,?,?,?,?,?,?)');
        $modifiedDate = DATE_TIME_FORMAT;
        $stmt->getResource()->bindParam(1, $dataArr['userId']);
        $stmt->getResource()->bindParam(2, $dataArr['title']);
        $stmt->getResource()->bindParam(3, $dataArr['source']);
        $stmt->getResource()->bindParam(4, $dataArr['income_range']);
        $stmt->getResource()->bindParam(5, $dataArr['security_question']);
        $stmt->getResource()->bindParam(6, $dataArr['security_answer']);
        $stmt->getResource()->bindParam(7, $dataArr['suffix']);
        $stmt->getResource()->bindParam(8, $dataArr['email_id']);
        $stmt->getResource()->bindParam(9, $dataArr['gender']);
        $stmt->getResource()->bindParam(10, $dataArr['marital_status']);
        $stmt->getResource()->bindParam(11, $dataArr['nationality']);
        $stmt->getResource()->bindParam(12, $dataArr['ethnicity']);
        $stmt->getResource()->bindParam(13, $modifiedDate);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for merge duplicate Contact
     * @author Icreon Tech - DG
     * @return array
     */
    public function mergeDuplicateContact($dataArr) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_mergeDuplicateContact(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
        $modifiedDate = DATE_TIME_FORMAT;
        $outInterest = '';
        $stmt->getResource()->bindParam(1, $dataArr['userId']);
        $stmt->getResource()->bindParam(2, $dataArr['duplicateUserId']);
        $stmt->getResource()->bindParam(3, $dataArr['transaction']);
        $stmt->getResource()->bindParam(4, $dataArr['development']);
        $stmt->getResource()->bindParam(5, $dataArr['activity']);        
        $stmt->getResource()->bindParam(6, $dataArr['case']);
        $stmt->getResource()->bindParam(7, $dataArr['realtionship']);
        $stmt->getResource()->bindParam(8, $dataArr['membership']);
        $stmt->getResource()->bindParam(9, $dataArr['interest']);
        $stmt->getResource()->bindParam(10, $outInterest);
        $stmt->getResource()->bindParam(11, $dataArr['tags']);
        $stmt->getResource()->bindParam(12, $dataArr['group']);
        $stmt->getResource()->bindParam(13, $outGroup);
        $stmt->getResource()->bindParam(14, $dataArr['communication_pre']);
        $stmt->getResource()->bindParam(15, $dataArr['education']);
        $stmt->getResource()->bindParam(16, $dataArr['alternate_contact']);
        $stmt->getResource()->bindParam(17, $dataArr['afihc']);
        $stmt->getResource()->bindParam(18, $dataArr['user_notes']);
        $stmt->getResource()->bindParam(19, $dataArr['user_docs']);
        $stmt->getResource()->bindParam(20, $dataArr['user_searches']);
        $stmt->getResource()->bindParam(21, $dataArr['survey']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for merge phone number of Contacts
     * @author Icreon Tech - DG
     * @return array
     */
    public function mergePhoneNumber($dataArr) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_mergeDuplicatePhone(?,?,?)');
        $stmt->getResource()->bindParam(1, $dataArr['userId']);
        $stmt->getResource()->bindParam(2, $dataArr['duplicateUserId']);
        $stmt->getResource()->bindParam(3, $dataArr['phone_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }
    
    /**
     * Function for merge address of Contacts
     * @author Icreon Tech - DG
     * @return array
     */
    public function mergeAddress($dataArr) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_mergeDuplicateAddress(?,?,?)');
        $stmt->getResource()->bindParam(1, $dataArr['userId']);
        $stmt->getResource()->bindParam(2, $dataArr['duplicateUserId']);
        $stmt->getResource()->bindParam(3, $dataArr['address_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

}