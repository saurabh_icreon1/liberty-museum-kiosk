<?php

/**
 * This model is used for store session and set remember me.
 * @package    User_AuthStorage
 * @author     Icreon Tech - DG
 */

namespace User\Model;

use Zend\Authentication\Storage;

/**
 * This model is used for store session and set remember me.
 * @package    User_AuthStorage
 * @author     Icreon Tech - DG
 */
class AuthStorage extends Storage\Session {

    public function setRememberMe($rememberMe = 0, $time = 1209600) { // For 2 weeks
        if ($rememberMe == 1) {
            $this->session->getManager()->rememberMe($time);
        }
    }

    public function forgetMe() {
        $this->session->getManager()->forgetMe();
    }

}