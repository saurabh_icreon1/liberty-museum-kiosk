<?php

/**
 * This model is used for user.
 * @package    User_User
 * @author     Icreon Tech - DG
 */

namespace User\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This model is used for user.
 * @package    User_User
 * @author     Icreon Tech - DG
 */
class UserTable {

    protected $tableGateway;
    protected $dbAdapter;
    protected $connection;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
        $this->connection = $this->tableGateway->getAdapter()->getDriver()->getConnection();
    }

    /**
     * Function for update user block account
     * @author Icreon Tech - DG
     * @param Array
     */
    public function userLockedUpdate($dataArr) {
        if (!empty($dataArr)) {
            if (isset($dataArr['email_id']))
                $email_id = $dataArr['email_id'];
            else
                $email_id = '';

            if (isset($dataArr['username']))
                $username = $dataArr['username'];
            else
                $username = '';

            if (isset($dataArr['is_blocked']))
                $is_blocked = $dataArr['is_blocked'];
            else
                $is_blocked = '0';

            $blocked_date = DATE_TIME_FORMAT;

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_updateBlockedStatus(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $email_id);
            $stmt->getResource()->bindParam(2, $username);
            $stmt->getResource()->bindParam(3, $is_blocked);
            $stmt->getResource()->bindParam(4, $blocked_date);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
        }
    }

    /**
     * Function for update user block account
     * @author Icreon Tech - DG
     * @param Array
     */
    public function updateUserLogins($dataArr) {
        if (!empty($dataArr)) {
            if (isset($dataArr['user_id']))
                $user_id = $dataArr['user_id'];
            else
                $user_id = '';

            if (isset($dataArr['active']))
                $active = $dataArr['active'];
            else
                $active = '0';

            $logout_date = DATE_TIME_FORMAT;

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_updateUserLogins(?,?,?)');
            $stmt->getResource()->bindParam(1, $user_id);
            $stmt->getResource()->bindParam(2, $active);
            $stmt->getResource()->bindParam(3, $logout_date);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
        }
    }

    /**
     * Function for to get no of blocked count
     * @author Icreon Tech - DG
     * @return Int
     * @param Array
     */
    public function getBlockCount($dataArr) {
        if (!empty($dataArr)) {
            if (isset($dataArr['email_id']))
                $email_id = $dataArr['email_id'];
            else
                $email_id = $dataArr['email_id'];

            if (isset($dataArr['login_state']))
                $login_state = $dataArr['login_state'];
            else
                $login_state = '';

            if (isset($dataArr['before_thirty_time']))
                $before_thirty_time = $dataArr['before_thirty_time'];
            else
                $before_thirty_time = '';

            if (isset($dataArr['current_time']))
                $current_time = $dataArr['current_time'];
            else
                $current_time = '';

            $result = $this->connection->execute("CALL usp_usr_getBlockedCount('" . $email_id . "','" . $login_state . "','" . $before_thirty_time . "','" . $current_time . "')");
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet[0];
        }
    }

    /**
     * Function for insert login log
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function insertLogins($loginsArr) {
        if (!empty($loginsArr)) {
            if (isset($loginsArr['user_id'])) {
                $user_id = $loginsArr['user_id'];
            } else {
                $user_id = '';
            }
            if (isset($loginsArr['email_id'])) {
                $email_id = $loginsArr['email_id'];
            } else {
                $email_id = '';
            }
            if (isset($loginsArr['login_date'])) {
                $login_date = $loginsArr['login_date'];
            } else {
                $login_date = '';
            }
            if (isset($loginsArr['login_state'])) {
                $login_state = $loginsArr['login_state'];
            } else {
                $login_state = '';
            }
            if (isset($loginsArr['active'])) {
                $active = $loginsArr['active'];
            } else {
                $active = 0;
            }
            if (isset($loginsArr['ip'])) {
                $ip = $loginsArr['ip'];
            } else {
                $ip = '';
            }
            if (isset($loginsArr['last_update_date'])) {
                $last_update_date = $loginsArr['last_update_date'];
            } else {
                $last_update_date = '';
            }
            if (isset($loginsArr['logout_date'])) {
                $logout_date = $loginsArr['logout_date'];
            } else {
                $logout_date = '';
            }
            if (isset($loginsArr['source_id'])) {
                $source_id = $loginsArr['source_id'];
            } else {
                $source_id = '';
            }

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_insertUserLog(?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $user_id);
            $stmt->getResource()->bindParam(2, $email_id);
            $stmt->getResource()->bindParam(3, $login_date);
            $stmt->getResource()->bindParam(4, $login_state);
            $stmt->getResource()->bindParam(5, $active);
            $stmt->getResource()->bindParam(6, $ip);
            $stmt->getResource()->bindParam(7, $last_update_date);
            $stmt->getResource()->bindParam(8, $logout_date);
            $stmt->getResource()->bindParam(9, $source_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if ($resultSet) {
                return $resultSet[0];
            } else {
                return false;
            }
        }
    }

    /**
     * Function for insert login log
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function insertForgot($dataArr) {
        if (!empty($dataArr)) {
            if (isset($dataArr['user_id'])) {
                $user_id = $dataArr['user_id'];
            } else {
                $user_id = '';
            }
            if (isset($dataArr['code'])) {
                $code = $dataArr['code'];
            } else {
                $code = '';
            }
            $creation_date = DATE_TIME_FORMAT;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_insertForgot(?,?,?)');
            $stmt->getResource()->bindParam(1, $user_id);
            $stmt->getResource()->bindParam(2, $code);
            $stmt->getResource()->bindParam(3, $creation_date);
            $result = $stmt->execute();
            $statement = $result->getResource();
        }
    }

    /**
     * Function for get user data 
     * @author Icreon Tech - DG
     * @return Int
     * @param Array
     */
    public function getUser($userArr) {
        if (isset($userArr)) {

            if (isset($userArr['email_id']) && $userArr['email_id'] != '') {
                $email_id = $userArr['email_id'];
            } else {
                $email_id = '';
            }

            if (isset($userArr['username']) && $userArr['username'] != '') {
                $username = $userArr['username'];
            } else {
                $username = '';
            }

            if (isset($userArr['user_id']) && $userArr['user_id'] != '') {
                $id = (int) $userArr['user_id'];
            } else {
                $id = '';
            }

            if (isset($userArr['verify_code']) && $userArr['verify_code'] != '') {
                $verify_code = $userArr['verify_code'];
            } else {
                $verify_code = '';
            }

			if (isset($userArr['include_deleted']) && $userArr['include_deleted'] != '') {
                $include_deleted = $userArr['include_deleted'];
            } else {
                $include_deleted = '';
            }

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getUser(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $username);
            $stmt->getResource()->bindParam(2, $email_id);
            $stmt->getResource()->bindParam(3, $id);
            $stmt->getResource()->bindParam(4, $verify_code);
            $stmt->getResource()->bindParam(5, $include_deleted);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if ($resultSet) {
                return $resultSet[0];
            } else {
                return false;
            }
        }
    }

    
    /**
     * Function for get user data 
     * @author Icreon Tech - NS
     * @return Int
     * @param Array
     */
    public function getUserForgotDetails($dataArr) {
        $user_id = (isset($dataArr['user_id']) and trim($dataArr['user_id']) != "") ? trim($dataArr['user_id']) : "";
        $current_datetime = (isset($dataArr['current_datetime']) and trim($dataArr['current_datetime']) != "") ? $dataArr['current_datetime'] : "";

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_getUserForgot(?,?)');
        $stmt->getResource()->bindParam(1, $user_id);
        $stmt->getResource()->bindParam(2, $current_datetime);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet)) {
           return (isset($resultSet[0]['flagDiffDates']) and trim($resultSet[0]['flagDiffDates']) != "" and trim($resultSet[0]['flagDiffDates']) == "1") ? 1 : 0;
        } else {
           return 0;
        }
    }
    
    /**
     * Function for get user data and password code
     * @author Icreon Tech - DG
     * @return Int
     * @param Array
     */
    public function getForgot($userArr) {
        if (isset($userArr)) {

            if (isset($userArr['email_id']) && $userArr['email_id'] != '') {
                $email_id = $userArr['email_id'];
            } else {
                $email_id = '';
            }

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getForgot(?)');
            $stmt->getResource()->bindParam(1, $email_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if ($resultSet) {
                return $resultSet[0];
            } else {
                return false;
            }
        }
    }

    /**
     * Function to update new password
     * @author Icreon Tech - DG
     * @return Int
     * @param Array
     */
    public function updatePassword($userArr) {
        if (isset($userArr)) {
            if (isset($userArr['user_id']) && $userArr['user_id'] != '') {
                $user_id = $userArr['user_id'];
            } else {
                $user_id = '';
            }
            $salt = $this->generateRandomString();
            if (isset($userArr['new_password']) && $userArr['new_password'] != '') {
                $new_password = md5($userArr['new_password'] . $salt);
            } else {
                $new_password = '';
            }
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_updatePassword(?,?,?)');
            $stmt->getResource()->bindParam(1, $user_id);
            $stmt->getResource()->bindParam(2, $new_password);
            $stmt->getResource()->bindParam(3, $salt);
            $result = $stmt->execute();
            $result->getResource();
        }
    }

    /**
     * Function for insert user data in table for signup
     * @author Icreon Tech - DG
     * @return Int
     * @param Array
     */
    public function saveUser($user) {
        $curDate = DATE_TIME_FORMAT;
        $verify_code = $this->generateRandomString();
        $email_verify = 1;
        $salt = $this->generateRandomString();
        $password = md5($user->password . $salt);
        $first_name = ucfirst($user->first_name);
        if (isset($user->middle_name) && $user->middle_name != '') {
            $middle_name = ucfirst($user->middle_name);
        } else {
            $middle_name = '';
        }
        $last_name = ucfirst($user->last_name);
        $email_id = $user->email_id;
        $security_question_id = $user->security_question_id;
        $security_answer = $user->security_answer;
        $agree = $user->agree;


        $sourceId = 3;
        $isLoginEnabled = 1;

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_saveUser(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $first_name);
        $stmt->getResource()->bindParam(2, $middle_name);
        $stmt->getResource()->bindParam(3, $last_name);
        $stmt->getResource()->bindParam(4, $email_id);
        $stmt->getResource()->bindParam(5, $password);
        $stmt->getResource()->bindParam(6, $salt);
        $stmt->getResource()->bindParam(7, $security_question_id);
        $stmt->getResource()->bindParam(8, $security_answer);
        $stmt->getResource()->bindParam(9, $agree);
        $stmt->getResource()->bindParam(10, $email_verify);
        $stmt->getResource()->bindParam(11, $verify_code);
        $stmt->getResource()->bindParam(12, $curDate);
        $stmt->getResource()->bindParam(13, $curDate);        
        $stmt->getResource()->bindParam(14, $sourceId);
        $stmt->getResource()->bindParam(15, $isLoginEnabled);
        $stmt->getResource()->bindParam(16, $user->terms_version_id);
        $stmt->getResource()->bindParam(17, $curDate);
        $stmt->getResource()->bindParam(18, $user->username);
     
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            return false;
        }
    }

    /**
     * Function for to insert default commu peferences
     * 
     * 
     * @author Icreon Tech - DG
     * @return Int
     * @param Array
     */
    public function insertDefaultContactCommunicationPreferences($dataArr) {
        $addDate = DATE_TIME_FORMAT;
        $privacy = null;
        $annotationPrivacy = 1;
        $familyHistoryPrivacy = 1;

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_insertDefaultCommunicationPreferences(?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $dataArr['user_id']);
        $stmt->getResource()->bindParam(2, $privacy);
        $stmt->getResource()->bindParam(3, $annotationPrivacy);
        $stmt->getResource()->bindParam(4, $familyHistoryPrivacy);
        $stmt->getResource()->bindParam(5, $addDate);
        $stmt->getResource()->bindParam(6, $addDate);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for to activate user email id
     * 
     * 
     * @author Icreon Tech - DG
     * @return Int
     * @param Array
     */
    public function activateUser($userArr) {
        $verify_code = $userArr['verify_code'];
        $is_activated = 1;
        $is_active = 1;

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_activateUser(?,?,?)');
        $stmt->getResource()->bindParam(1, $verify_code);
        $stmt->getResource()->bindParam(2, $is_activated);
        $stmt->getResource()->bindParam(3, $is_active);
        $result = $stmt->execute();
        $statement = $result->getResource();

        $statement->closeCursor();
        if ($statement) {
            return true;
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for to activate user email id
     * 
     * 
     * @author Icreon Tech - DG
     * @return Int
     * @param Array
     */
    public function updateEmail($userArr) {
        $email = $userArr['email'];
        $user_id = $userArr['user_id'];
        $email_verified = 1;

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_updateEmail(?,?,?)');
        $stmt->getResource()->bindParam(1, $user_id);
        $stmt->getResource()->bindParam(2, $email);
        $stmt->getResource()->bindParam(3, $email_verified);
        $result = $stmt->execute();
        $statement = $result->getResource();

        $statement->closeCursor();
        if ($statement) {
            return $this->getUser($userArr);
        } else {
            return false;
        }
    }
	
 

    /**
     * Function for to verify updated email
     * 
     * 
     * @author Icreon Tech - DG
     * @return Int
     * @param Array
     */
    public function updateEmailInfo($userArr) {
        $alternateEmailId = $userArr['alternate_email_id'];
        $userId = $userArr['user_id'];
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_updateEmailInfo(?,?)');
        $stmt->getResource()->bindParam(1, $userId);
        $stmt->getResource()->bindParam(2, $alternateEmailId);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for get random number
     * @author Icreon Tech - DG
     * @return array
     * @param Int
     */
    public function generateRandomString() {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ23456789";
        srand((double) microtime() * 1000000);
        $number = 0;
        $code = '';
        while ($number <= 7) {
            $num = rand() % 33;
            $tmp = substr($chars, $num, 1);
            $code = $code . $tmp;
            $number++;
        }
		 $code .= mt_rand().$_SERVER['SERVER_ADDR'];
		 $code  = md5(uniqid($code, true));
         return $code;
    }

    /**
     * Function for get user data  for crm
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function getUserCrm($userArr) {
        if (isset($userArr)) {

            if (isset($userArr['crm_full_name']) && $userArr['crm_full_name'] != '') {
                $crm_full_name = $userArr['crm_full_name'];
            } else {
                $crm_full_name = '';
            }
            if (isset($userArr['crm_email_id']) && $userArr['crm_email_id'] != '') {
                $crm_email_id = $userArr['crm_email_id'];
            } else {
                $crm_email_id = '';
            }
            if (isset($userArr['crm_user_id']) && $userArr['crm_user_id'] != '') {
                $crm_user_id = $userArr['crm_user_id'];
            } else {
                $crm_user_id = '';
            }

            $result = $this->connection->execute("CALL usp_usr_getCrmUser('" . $crm_full_name . "','" . $crm_email_id . "','" . $crm_user_id . "','')");
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (isset($resultSet[0]))
                return $resultSet[0];
            return array();
        }
    }

    /**
     * Function for insert login log for crm
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function insertLoginsCrm($loginsArr) {
        if (!empty($loginsArr)) {
            if (isset($loginsArr['crm_user_id'])) {
                $crm_user_id = $loginsArr['crm_user_id'];
            } else {
                $crm_user_id = '';
            }
            if (isset($loginsArr['crm_email_id'])) {
                $crm_email_id = $loginsArr['crm_email_id'];
            } else {
                $crm_email_id = '';
            }
            if (isset($loginsArr['crm_login_date'])) {
                $crm_login_date = $loginsArr['crm_login_date'];
            } else {
                $crm_login_date = '';
            }
            if (isset($loginsArr['crm_login_state'])) {
                $crm_login_state = $loginsArr['crm_login_state'];
            } else {
                $crm_login_state = '';
            }
            if (isset($loginsArr['crm_active'])) {
                $crm_active = $loginsArr['crm_active'];
            } else {
                $crm_active = 0;
            }
            if (isset($loginsArr['crm_ip'])) {
                $crm_ip = $loginsArr['crm_ip'];
            } else {
                $crm_ip = '';
            }
            if (isset($loginsArr['crm_last_update_date'])) {
                $crm_last_update_date = $loginsArr['crm_last_update_date'];
            } else {
                $crm_last_update_date = '';
            }
            if (isset($loginsArr['crm_logout_date'])) {
                $crm_logout_date = $loginsArr['crm_logout_date'];
            } else {
                $crm_logout_date = '';
            }
            $result = $this->connection->execute("CALL usp_usr_insertCrmUserLog(' " . $crm_user_id . " ','" . $crm_email_id . "','" . $crm_login_date . "','" . $crm_login_state . "','" . $crm_active . "','" . $crm_ip . "','" . $crm_last_update_date . "','" . $crm_logout_date . "')");
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet[0];
        }
    }

    /**
     * Function for to get no of blocked count for crm
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function getBlockCountCrm($dataArr) {
        if (!empty($dataArr)) {
            if (isset($dataArr['email_id']))
                $email_id = $dataArr['email_id'];
            else
                $email_id = $dataArr['email_id'];

            if (isset($dataArr['login_state']))
                $login_state = $dataArr['login_state'];
            else
                $login_state = '';

            if (isset($dataArr['before_thirty_time']))
                $before_thirty_time = $dataArr['before_thirty_time'];
            else
                $before_thirty_time = '';

            if (isset($dataArr['current_time']))
                $current_time = $dataArr['current_time'];
            else
                $current_time = '';

            $result = $this->connection->execute("CALL usp_usr_getBlockedCrmCount('" . $email_id . "','" . $login_state . "','" . $before_thirty_time . "','" . $current_time . "')");
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet[0];
        }
    }

    /**
     * Function for update user block account for crm
     * @author Icreon Tech - DT
     * @return Void
     * @param Array
     */
    public function userLockedUpdateCrm($dataArr) {
        if (!empty($dataArr)) {
            if (isset($dataArr['crm_email_id']))
                $crm_email_id = $dataArr['crm_email_id'];
            else
                $crm_email_id = '';

            if (isset($dataArr['is_blocked']))
                $is_blocked = $dataArr['is_blocked'];
            else
                $is_blocked = '0';

            $blocked_date = DATE_TIME_FORMAT;

            $result = $this->connection->execute("CALL usp_usr_updateBlockCrmStatus('" . $crm_email_id . "','" . $is_blocked . "','" . $blocked_date . "')");
            $statement = $result->getResource();
            $statement->closeCursor();
        }
    }

    /**
     * Function for insert forgot password code for crm
     * @author Icreon Tech - DT
     * @return Void
     * @param Array
     */
    public function insertForgotCrm($dataArr) {
        if (!empty($dataArr)) {
            if (isset($dataArr['crm_user_id'])) {
                $crm_user_id = $dataArr['crm_user_id'];
            } else {
                $crm_user_id = '';
            }
            if (isset($dataArr['code'])) {
                $code = $dataArr['code'];
            } else {
                $code = '';
            }
            $creation_date = DATE_TIME_FORMAT;
            $result = $this->connection->execute("CALL usp_usr_insertCrmForgot(' " . $crm_user_id . " ','" . $code . "','" . $creation_date . "')");
            $result->getResource();
        }
    }

    /**
     * Function for get user data and password code for crm
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function getForgotCrm($userArr) {
        if (isset($userArr)) {

            if (isset($userArr['crm_email_id']) && $userArr['crm_email_id'] != '') {
                $crm_email_id = $userArr['crm_email_id'];
            } else {
                $crm_email_id = '';
            }

            if (isset($userArr['code']) && $userArr['code'] != '') {
                $code = $userArr['code'];
            } else {
                $code = '';
            }
            $result = $this->connection->execute("CALL usp_usr_getCrmForgot('" . $crm_email_id . "','" . $code . "')");
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet[0];
            else
                return false;
        }
    }

    /**
     * Function to update new password for crm
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function updatePasswordCrm($userArr) {
        if (isset($userArr)) {
            if (isset($userArr['crm_user_id']) && $userArr['crm_user_id'] != '') {
                $crm_user_id = $userArr['crm_user_id'];
            } else {
                $crm_user_id = '';
            }
            $salt = $this->generateRandomString();
            if (isset($userArr['new_password']) && $userArr['new_password'] != '') {
                $new_password = md5($userArr['new_password'] . $salt);
            } else {
                $new_password = '';
            }
            $result = $this->connection->execute("CALL usp_usr_updateCrmPassword('" . $crm_user_id . "','" . $new_password . "','" . $salt . "')");
            $result->getResource();
        }
    }

    /**
     * Function for update user block account for crm
     * @author Icreon Tech - DT
     * @param Array
     */
    public function updateCrmUserLogins($dataArr) {
        if (!empty($dataArr)) {
            if (isset($dataArr['crm_user_id']))
                $crm_user_id = $dataArr['crm_user_id'];
            else
                $crm_user_id = '';

            if (isset($dataArr['crm_active']))
                $crm_active = $dataArr['crm_active'];
            else
                $crm_active = '0';

            $logout_date = DATE_TIME_FORMAT;
            $result = $this->connection->execute("CALL usp_usr_updateCrmUserLogins('" . $crm_user_id . "','" . $crm_active . "','" . $logout_date . "')");
            $statement = $result->getResource();
            $statement->closeCursor();
        }
    }

    /**
     * This function will fetch the user FOF 
     * @param ship name
     * @return this will return user fof 
     * @author Icreon Tech -DG
     */
    public function getUserFof($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getUserFof(?)');
            $stmt->getResource()->bindParam(1, $param['user_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will fetch the user FOF search
     * @param ship name
     * @return this will return user fof search
     * @author Icreon Tech -DG
     */
    public function getUserFofSearch($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getUserFofSearch(?)');
            $stmt->getResource()->bindParam(1, $param['user_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will fetch the user woh search
     * @param ship name
     * @return this will return user fof search
     * @author Icreon Tech -DG
     */
    public function getUserWohSearches($param = array()) {
        try {
            $searchId = null;
            $userId = $param['user_id'];
            
            $start_index = (isset($param['start_index']) && $param['start_index'] != '') ? $param['start_index'] : '';
            $record_limit = (isset($param['record_limit']) && $param['record_limit'] != '') ? $param['record_limit'] : '';
            $sort_field = (isset($param['sort_field']) && $param['sort_field'] != '') ? $param['sort_field'] : '';
            $sort_order = (isset($param['sort_order']) && $param['sort_order'] != '') ? $param['sort_order'] : '';


            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getUserWohSearch(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $userId);
            $stmt->getResource()->bindParam(2, $searchId);
            $stmt->getResource()->bindParam(3, $start_index);
            $stmt->getResource()->bindParam(4, $record_limit);
            $stmt->getResource()->bindParam(5, $sort_field);
            $stmt->getResource()->bindParam(6, $sort_order);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will fetch the user woh 
     * @param ship name
     * @return this will return user woh
     * @author Icreon Tech -DG
     */
    public function getUserWoh($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getUserWoh(?)');
            $stmt->getResource()->bindParam(1, $param['user_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will fetch the woh 
     * @param ship name
     * @return this will return user woh
     * @author Icreon Tech -DG
     */
    public function getWoh($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getWoh(?)');
            $stmt->getResource()->bindParam(1, $param['woh_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to delete the fof saved search 
     * @param this will be an array having the detele condition parameters, user_id int, search_id int .
     * @return this will return the confirmation
     * @author Icreon Tech -DG
     */
    public function deleteFofSearch($param = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_deleteFofSearch(?,?,?)');
        $stmt->getResource()->bindParam(1, $param['user_id']);
        $stmt->getResource()->bindParam(2, $param['is_delete']);
        $stmt->getResource()->bindParam(3, $param['search_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * This function is used to delete the woh saved search 
     * @param this will be an array having the detele condition parameters, user_id int, search_id int .
     * @return this will return the confirmation
     * @author Icreon Tech -DG
     */
    public function deleteWohSearch($param = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_deleteWohSearch(?,?,?)');
        $stmt->getResource()->bindParam(1, $param['user_id']);
        $stmt->getResource()->bindParam(2, $param['is_delete']);
        $stmt->getResource()->bindParam(3, $param['search_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * This function will fetch the user fof setting
     * @param ship name
     * @return this will return user fof setting
     * @author Icreon Tech -DG
     */
    public function getFof($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getUserFofSetting(?)');
            $stmt->getResource()->bindParam(1, $param['fof_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will fetch the user fof setting for cart
     * @param ship name
     * @return this will return user fof setting for cart
     * @author Icreon Tech -DT
     */
    public function getFofCart($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getUserFofSettingCart(?)');
            $stmt->getResource()->bindParam(1, $param['fof_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update the user fof setting
     * @param ship name
     * @return this will return user fof setting
     * @author Icreon Tech -DG
     */
    public function updateUserFofSetting($param = array()) {
        try {
            $modifiedDate = DATE_TIME_FORMAT;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_updateUserFofSetting(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['fof_id']);
            $stmt->getResource()->bindParam(2, $param['is_people_photo_visible']);
            $stmt->getResource()->bindParam(3, $param['is_photo_caption_visible']);
            $stmt->getResource()->bindParam(4, $modifiedDate);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update the update woh
     * @param ship name
     * @return this will return boolean
     * @author Icreon Tech -DG
     */
    public function updateWoh($param = array()) {
        try {
            $modifiedDate = DATE_TIME_FORMAT;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_updateWoh(?,?,?)');
            $stmt->getResource()->bindParam(1, $param['woh_id']);
            $stmt->getResource()->bindParam(2, $param['donated_by']);
            $stmt->getResource()->bindParam(3, $modifiedDate);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will fetch bio woh
     * @param ship name
     * @return this will return arrry
     * @author Icreon Tech -DG
     */
    public function getBioWoh($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getBioWoh(?)');
            $stmt->getResource()->bindParam(1, $param['bio_certificate_product_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update bio woh
     * @param ship name
     * @return this will return arrry
     * @author Icreon Tech -DG
     */
    public function updateBioWoh($params = array()) {
        try {
            $bio_certificate_id = isset($params['bio_certificate_id']) ? $params['bio_certificate_id'] : null;
            $name = isset($params['name']) ? $params['name'] : null;
            $immigrated_from = isset($params['immigrated_from']) ? $params['immigrated_from'] : null;
            $ship_method = isset($params['ship_method']) ? $params['ship_method'] : null;
            $port_of_entry = isset($params['port_of_entry']) ? $params['port_of_entry'] : null;
            $date_of_entry = isset($params['date_of_entry']) ? $params['date_of_entry'] : null;
            $additional_info = isset($params['additional_info']) ? $params['additional_info'] : null;
            $finalize = isset($params['finalize']) ? '1' : '0';
            $addDate = DATE_TIME_FORMAT;
            $woh_id = isset($params['woh_id']) ? $params['woh_id'] : null;

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_updateBioWoh(?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $bio_certificate_id);
            $stmt->getResource()->bindParam(2, $name);
            $stmt->getResource()->bindParam(3, $immigrated_from);
            $stmt->getResource()->bindParam(4, $ship_method);
            $stmt->getResource()->bindParam(5, $port_of_entry);
            $stmt->getResource()->bindParam(6, $date_of_entry);
            $stmt->getResource()->bindParam(7, $finalize);
            $stmt->getResource()->bindParam(8, $additional_info);
            $stmt->getResource()->bindParam(9, $addDate);
            $stmt->getResource()->bindParam(10, $woh_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will check email exits
     * @param array
     * @return this will return boolean
     * @author Icreon Tech -DG
     */
    public function isEmailExits($params = array()) {
        try {
            $emailId = isset($params['email_id']) ? $params['email_id'] : null;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_isEmailExits(?)');
            $stmt->getResource()->bindParam(1, $emailId);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for insert user membership data
     * @author Icreon Tech - DG
     * @return Int
     * @param Array
     */
    public function saveUserMembership($params) {
        $userId = isset($params['user_id']) ? $params['user_id'] : null;
        $membershipId = isset($params['membership_id']) ? $params['membership_id'] : null;
        $membershipDateFrom = isset($params['membership_date_from']) ? $params['membership_date_from'] : null;
        $membershipDateTo = isset($params['membership_date_to']) ? $params['membership_date_to'] : null;
		$ignore_update = isset($params['ignore_update']) ? $params['ignore_update'] : 0;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_usr_insertUserMembership(?,?,?,?,?)");
        $stmt->getResource()->bindParam(1, $userId);
        $stmt->getResource()->bindParam(2, $membershipId);
        $stmt->getResource()->bindParam(3, $membershipDateFrom);
        $stmt->getResource()->bindParam(4, $membershipDateTo);
        $stmt->getResource()->bindParam(5, $ignore_update);
        $result = $stmt->execute();
        $statement = $result->getResource();
        
        // - start
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            $lastInsertedId = $resultSet['0']['LAST_INSERT_MEMBERSHIP_ID'];
        else 
            $lastInsertedId = "";

        if(isset($lastInsertedId) and trim($lastInsertedId) != "") { 
            $this->insertUserMembershipCards(trim($lastInsertedId),$params);
        }
        // - end
            

    }
    
    
     /**
     * This function will update the user membership
     * @param array.
     * @return boolean
     * @author Icreon Tech 
     */    
    private function insertUserMembershipCards($userMembershipId,$transactionData = array()) {
        try {
            if(isset($transactionData['membership_date_from']) and trim($transactionData['membership_date_from']) != "" and isset($transactionData['membership_date_to']) and trim($transactionData['membership_date_to']) != "") {
                $dateFrom = trim($transactionData['membership_date_from']);
                $dateTo = trim($transactionData['membership_date_to']);
                $yearFrom = date("Y",  strtotime($dateFrom));
                $yearTo = date("Y",  strtotime($dateTo));
                $diffYear = $yearTo - $yearFrom;
                
                $memCardArr = array();
                $memCardArr['user_id'] = (isset($transactionData['user_id']) and trim($transactionData['user_id']) != "") ? $transactionData['user_id'] : "";
                $memCardArr['user_membership_id'] = $userMembershipId;
                $memCardArr['membership_card_date_from'] = $dateFrom;
                $memCardArr['membership_card_date_to'] = $dateTo;
                
                
                if(isset($diffYear) and trim($diffYear) != "" and $diffYear > 0) {
                     // first entry - start
                     $memCardArr['membership_card_date_from'] = $dateFrom;
                     $memCardArr['membership_card_date_to'] = $yearFrom."-12-31";
                     $this->insertMemberShipCardsRecords($memCardArr);
                     // first entry - end
                     
                     for($yr=1; $yr <= trim($diffYear); $yr++) {
                         if($yearFrom + $yr < $yearTo) {
                             // multi entry - start
                             $mYear = $yearFrom + $yr;
                             $memCardArr['membership_card_date_from'] = $mYear."-01-01";;
                             $memCardArr['membership_card_date_to'] = $mYear."-12-31";
                             $this->insertMemberShipCardsRecords($memCardArr);
                            // multi entry - end 
                         }
                     }
                     
                     // end entry - start
                     $memCardArr['membership_card_date_from'] = $yearTo."-01-01";
                     $memCardArr['membership_card_date_to'] = $dateTo;
                     $this->insertMemberShipCardsRecords($memCardArr);
                     // end entry - end
                }
                else {
                     $this->insertMemberShipCardsRecords($memCardArr);
                }
            }
        }
         catch (Exception $e) {
            return false;
        }
    }
    
     /**
     * Function to insert MemberShip Card Records
     * @author Icreon Tech - NS
     * @return boolean
     * @param Array
     */
    private function insertMemberShipCardsRecords($params = array()) {
        try {
            $params['user_id'] = (isset($params['user_id']) and trim($params['user_id']) != "") ? $params['user_id'] : "";
            $params['user_membership_id'] = (isset($params['user_membership_id']) and trim($params['user_membership_id']) != "") ? $params['user_membership_id'] : "";
            $params['membership_card_date_from'] = (isset($params['membership_card_date_from']) and trim($params['membership_card_date_from']) != "") ? $params['membership_card_date_from'] : "";
            $params['membership_card_date_to'] = (isset($params['membership_card_date_to']) and trim($params['membership_card_date_to']) != "") ? $params['membership_card_date_to'] : "";
            
           if($params['user_id'] != "" and $params['user_membership_id'] != "" and $params['membership_card_date_from'] != "" and $params['membership_card_date_to'] != "" and $params['membership_card_date_from'] != $params['membership_card_date_to']) { 
                $stmt = $this->dbAdapter->createStatement();
                $stmt->prepare('CALL usp_usr_insertContactMembershipCards(?,?,?,?)');
                $stmt->getResource()->bindParam(1, $params['user_id']);
                $stmt->getResource()->bindParam(2, $params['user_membership_id']);
                $stmt->getResource()->bindParam(3, $params['membership_card_date_from']);
                $stmt->getResource()->bindParam(4, $params['membership_card_date_to']);
                $result = $stmt->execute();
            }            
            return true;
        } catch (Exception $e) {
                return false;
        }
    }
    

    /**
     * Function used to insert facebook user 
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function addSocialAccountUser($params = array()) {
        $facebookId = isset($params['facebook_id']) ? $params['facebook_id'] : null;
        $firstName = isset($params['first_name']) ? $params['first_name'] : null;
        $lastName = isset($params['last_name']) ? $params['last_name'] : null;
        $emailId = isset($params['email_id']) ? $params['email_id'] : null;
        $gender = isset($params['gender']) ? $params['gender'] : null;
        $emailVerified = isset($params['email_verified']) ? $params['email_verified'] : null;
        $isActivated = isset($params['is_activated']) ? $params['is_activated'] : null;
        $isActive = isset($params['is_active']) ? $params['is_active'] : null;
        $socialAccountType = isset($params['social_account_type']) ? $params['social_account_type'] : null;
        $socialAccountImage = isset($params['profile_iamge']) ? $params['profile_iamge'] : null;
        $addDate = isset($params['added_date']) ? $params['added_date'] : null;
        $modifiedDate = isset($params['modified_date']) ? $params['modified_date'] : null;
        $termsVersionId = isset($params['terms_version_id']) ? $params['terms_version_id'] : null;
        $termsAcceptDate = isset($params['terms_accept_date']) ? $params['terms_accept_date'] : null;
        $sourceId = 3;
        $isLoginEnabled = 1;
        
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_addSocialAccountUser(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $facebookId);
        $stmt->getResource()->bindParam(2, $firstName);
        $stmt->getResource()->bindParam(3, $lastName);
        $stmt->getResource()->bindParam(4, $emailId);
        $stmt->getResource()->bindParam(5, $gender);
        $stmt->getResource()->bindParam(6, $emailVerified);
        $stmt->getResource()->bindParam(7, $isActivated);
        $stmt->getResource()->bindParam(8, $isActive);
        $stmt->getResource()->bindParam(9, $socialAccountType);
        $stmt->getResource()->bindParam(10, $socialAccountImage);
        $stmt->getResource()->bindParam(11, $addDate);
        $stmt->getResource()->bindParam(12, $modifiedDate);
        $stmt->getResource()->bindParam(13, $sourceId);
        $stmt->getResource()->bindParam(14, $isLoginEnabled);
        $stmt->getResource()->bindParam(15, $termsVersionId);
        $stmt->getResource()->bindParam(16, $termsAcceptDate);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            return false;
        }
    }

    /**
     * Function used to get facebook user 
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getSocialAccountUser($params = array()) {
        $facebookId = isset($params['facebookId']) ? $params['facebookId'] : null;
        $facebookEmail = isset($params['facebookEmail']) ? $params['facebookEmail'] : null;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_getSocialAccountUser(?,?)');
        $stmt->getResource()->bindParam(1, $facebookId);
        $stmt->getResource()->bindParam(2, $facebookEmail);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0];
        } else {
            return false;
        }
    }

    /**
     * Function used to update user basic info
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function updateUserInfo($params = array()) {       
        if (isset($params['age']) && $params['age'] == 2) {
            $params['age'] = 1;
        } else {
            $params['age'] = 0;
        }
        $modifiedDate = DATE_TIME_FORMAT;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_updateUserInfo(?,?,?,?,?,?,?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $params['contact_id']);
        $stmt->getResource()->bindParam(2, $params['profile_image']);
        $stmt->getResource()->bindParam(3, $params['title']);
        $stmt->getResource()->bindParam(4, $params['first_name']);
        $stmt->getResource()->bindParam(5, $params['middle_name']);
        $stmt->getResource()->bindParam(6, $params['last_name']);
        $stmt->getResource()->bindParam(7, $params['suffix']);
        $stmt->getResource()->bindParam(8, $modifiedDate);
        $stmt->getResource()->bindParam(9, $params['corporate_company_name']);
        $stmt->getResource()->bindParam(10, $params['corporate_legal_name']);
        $stmt->getResource()->bindParam(11, $params['corporate_sic_code']);
        $stmt->getResource()->bindParam(12, $params['typeSubmit']); 
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if(isset($resultSet[0]) and !empty($resultSet[0])) {
          return $resultSet[0];
        }
        else {
           return array();
        }
    }

    /**
     * Function used to update user security question
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function updateUserSecurityQuestionInfo($params = array()) {
        $userId = isset($params['user_id']) ? $params['user_id'] : null;
        $securityQuestion = isset($params['security_question']) ? $params['security_question'] : null;
        $securityAnswer = isset($params['security_answer']) ? $params['security_answer'] : null;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_updateUserSecurityQuestionInfo(?,?,?)');
        $stmt->getResource()->bindParam(1, $userId);
        $stmt->getResource()->bindParam(2, $securityQuestion);
        $stmt->getResource()->bindParam(3, $securityAnswer);
//        $result = $stmt->execute();
//        $statement = $result->getResource();
//        $statement->closeCursor();
        
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if(isset($resultSet[0]) and !empty($resultSet[0])) {
          return $resultSet[0];
        }
        else {
           return array();
        }
    }

    /**
     * Function used to update user email info
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function updateUserEmailInfo($params = array()) {
        $userId = isset($params['user_id']) ? $params['user_id'] : null;
        $emailId = isset($params['email_id']) ? $params['email_id'] : null;
        $verificationCode = isset($params['verification_code']) ? $params['verification_code'] : null;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_updateUserEmailInfo(?,?,?)');
        $stmt->getResource()->bindParam(1, $userId);
        $stmt->getResource()->bindParam(2, $emailId);
        $stmt->getResource()->bindParam(3, $verificationCode);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function used to delete user phone info
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function deleteUserPhoneInfo($params = array()) {
        $phoneId = isset($params['phone_id']) ? $params['phone_id'] : null;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_deleteUserPhoneInfo(?)');
        $stmt->getResource()->bindParam(1, $phoneId);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function used to delete user address info
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function deleteUserAddressInfo($params = array()) {
        $addressId = isset($params['address_id']) ? $params['address_id'] : null;
        $isDelete = isset($params['is_delete']) ? $params['is_delete'] : null;
        $modifiedDate = DATE_TIME_FORMAT;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_deleteUserAddressInfo(?,?,?)');
        $stmt->getResource()->bindParam(1, $addressId);
        $stmt->getResource()->bindParam(2, $isDelete);
        $stmt->getResource()->bindParam(3, $modifiedDate);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for to update phone information
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function updatePhoneInformations($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_updatePhoneInformations(?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['phone_id']);
            $stmt->getResource()->bindParam(2, $params['phone_type']);
            $stmt->getResource()->bindParam(3, $params['country_code']);
            $stmt->getResource()->bindParam(4, $params['area_code']);
            $stmt->getResource()->bindParam(5, $params['phone_no']);
            $stmt->getResource()->bindParam(6, $params['continfo_primary']);
            $stmt->getResource()->bindParam(7, $params['extension']);
            $stmt->getResource()->bindParam(8, $params['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to update address information
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function updateAddressInformations($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            
            $title = isset($params['title']) && !is_null($params['title']) ? $params['title'] : '';
            $firstName = isset($params['first_name']) && !is_null($params['first_name']) ? $params['first_name'] : '';
            $middleName = isset($params['middle_name']) && !is_null($params['middle_name']) ? $params['middle_name'] : '';
            $lastName = isset($params['last_name']) && !is_null($params['last_name']) ? $params['last_name'] : '';            
            
            $address_type = isset($params['address_type']) && !is_null($params['address_type']) ? $params['address_type'] : '';
            $stmt->prepare('CALL usp_usr_updateAddressInfo(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['address_id']);
            $stmt->getResource()->bindParam(2, $params['addressinfo_location_type']);
            $stmt->getResource()->bindParam(3, $params['addressinfo_location']);
            $stmt->getResource()->bindParam(4, $params['addressinfo_street_address_1']);
            $stmt->getResource()->bindParam(5, $params['addressinfo_street_address_2']);
            $stmt->getResource()->bindParam(6, $params['addressinfo_city']);
            $stmt->getResource()->bindParam(7, $params['addressinfo_state']);
            $stmt->getResource()->bindParam(8, $params['addressinfo_zip']);
            $stmt->getResource()->bindParam(9, $params['addressinfo_country']);
            $stmt->getResource()->bindParam(10, $params['modified_date']);
            $stmt->getResource()->bindParam(11, $address_type);
            $stmt->getResource()->bindParam(12, $title);
            $stmt->getResource()->bindParam(13, $firstName);
            $stmt->getResource()->bindParam(14, $middleName);
            $stmt->getResource()->bindParam(15, $lastName);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

       /**
     * Function for to update address information
     * @author Icreon Tech - SR
     * @return Arr
     * @param Array
     */ 
    public function updateTransactionAddressInformations($params = array()) {
        try {

            $title = isset($params['title']) && !is_null($params['title']) ? $params['title'] : '';
            $firstName = isset($params['first_name']) && !is_null($params['first_name']) ? $params['first_name'] : '';
          //  $middleName = isset($params['middle_name']) && !is_null($params['middle_name']) ? $params['middle_name'] : '';
            $lastName = isset($params['last_name']) && !is_null($params['last_name']) ? $params['last_name'] : '';            
            
            $stmt = $this->dbAdapter->createStatement();
            $address_type = isset($params['address_type']) && !is_null($params['address_type']) ? $params['address_type'] : '';
            $stmt->prepare('CALL usp_usr_updateTransactionAddressInfo(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['address_id']);
            $stmt->getResource()->bindParam(2, $params['addressinfo_location_type']);
            $stmt->getResource()->bindParam(3, $params['addressinfo_location']);
            $stmt->getResource()->bindParam(4, $params['addressinfo_street_address_1']);
            $stmt->getResource()->bindParam(5, $params['addressinfo_street_address_2']);
            $stmt->getResource()->bindParam(6, $params['addressinfo_city']);
            $stmt->getResource()->bindParam(7, $params['addressinfo_state']);
            $stmt->getResource()->bindParam(8, $params['addressinfo_zip']);
            $stmt->getResource()->bindParam(9, $params['addressinfo_country']);
            $stmt->getResource()->bindParam(10, $params['modified_date']);
            $stmt->getResource()->bindParam(11, $address_type);
            
            $stmt->getResource()->bindParam(12, $title);
            $stmt->getResource()->bindParam(13, $firstName);
            $stmt->getResource()->bindParam(14, $lastName);
            $stmt->getResource()->bindParam(15, $lastName);
            
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }    
    
    /**
     * Function for to update user demographics
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function updateUserDemographics($params = array()) {
        try {
            $modifiedDate = DATE_TIME_FORMAT;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_updateUserDemographhics(?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['user_id']);
            $stmt->getResource()->bindParam(2, $params['gender']);
            $stmt->getResource()->bindParam(3, $params['have_child']);
            $stmt->getResource()->bindParam(4, $params['demographics_income_range']);
            $stmt->getResource()->bindParam(5, $params['demographics_nationality']);
            $stmt->getResource()->bindParam(6, $params['demographics_ethnicity']);
            $stmt->getResource()->bindParam(7, $params['demographics_marital_status']);
            $stmt->getResource()->bindParam(8, $modifiedDate);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to delete education
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function deleteEducation($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_deleteUserEducation(?)');
            $stmt->getResource()->bindParam(1, $params['user_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function used to update  user communication preferences
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function updateCommunicationPreferences($params = array()) {
        try {
            $userId = isset($params['user_id']) ? $params['user_id'] : null;
            $privacy = isset($params['privacy']) ? $params['privacy'] : null;
            $annotationPrivacy = isset($params['annotation_privacy']) ? $params['annotation_privacy'] : null;
            $familyHistoryPrivacy = isset($params['family_history_privacy']) ? $params['family_history_privacy'] : null;
            $modifiedDate = DATE_TIME_FORMAT;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_updateUserPreferences(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $userId);
            $stmt->getResource()->bindParam(2, $privacy);
            $stmt->getResource()->bindParam(3, $annotationPrivacy);
            $stmt->getResource()->bindParam(4, $familyHistoryPrivacy);
            $stmt->getResource()->bindParam(5, $modifiedDate);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function used to insert token info
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function insertTokenInfo($params = array()) {
        try { 
            $userId = isset($params['user_id']) ? $params['user_id'] : null;
            $profileId = isset($params['profile_id']) ? $params['profile_id'] : null;
            $addedBy = isset($params['added_by']) ? $params['added_by'] : null;
            $modifiedBy = isset($params['modified_by']) ? $params['modified_by'] : null;
            $billingAddressId = isset($params['billing_address_id']) ? $params['billing_address_id'] : null;
            $creditCardType = isset($params['credit_card_type_id']) ? $params['credit_card_type_id'] : null;
            $creditCardNo = isset($params['credit_card_no']) ? $params['credit_card_no'] : null;
            
            $addedDate = DATE_TIME_FORMAT;
            $modifiedDate = DATE_TIME_FORMAT;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_insertToken(?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $userId);
            $stmt->getResource()->bindParam(2, $profileId);
            $stmt->getResource()->bindParam(3, $addedBy);
            $stmt->getResource()->bindParam(4, $addedDate);
            $stmt->getResource()->bindParam(5, $modifiedBy);
            $stmt->getResource()->bindParam(6, $modifiedDate);
            $stmt->getResource()->bindParam(7, $billingAddressId);
            $stmt->getResource()->bindParam(8, $creditCardType);
            $stmt->getResource()->bindParam(9, $creditCardNo);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function used to get token info
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getUserTokenInformations($params = array()) {
        try {
            $userId = isset($params['user_id']) ? $params['user_id'] : null;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getTokenInfo(?)');
            $stmt->getResource()->bindParam(1, $userId);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function used to delete token info
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function deleteUserTokenInfo($params = array()) {
        try {
            $paymentProfileId = isset($params['paymentProfileId']) ? $params['paymentProfileId'] : null;
            $profileId = isset($params['profileId']) ? $params['profileId'] : null;
            $isDelete = isset($params['isDelete']) ? $params['isDelete'] : null;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_deleteTokenInfo(?,?,?)');
            $stmt->getResource()->bindParam(1, $paymentProfileId);
            $stmt->getResource()->bindParam(2, $profileId);
            $stmt->getResource()->bindParam(3, $isDelete);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
        } catch (Exception $e) {
            return false;
        }
    }

     /**
     * Function used to update user association
     * @author Icreon Tech - DT
     * @return void
     * @param Array
     */
    public function updateUserAssociation($params = array()) {
        $userId = isset($params['user_id']) ? $params['user_id'] : '';
        $association = isset($params['association']) ? $params['association'] : '0';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_updateUserAssociation(?,?)');

        $stmt->getResource()->bindParam(1, $userId);
        $stmt->getResource()->bindParam(2, $association);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }
	
	/**
     * Function for to delete social account
     * 
     * 
     * @author Icreon Tech - SR
     * @return Int
     * @param $user_id
     */
    public function updateSocialInfo($user_id) {
        
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_updateUserSocialInfo(?)');
        $stmt->getResource()->bindParam(1, $user_id);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
        if ($statement) {
            return true;
        } else {
            return false;
        }
    }
	
	
    /**
     * Function for to get social account
     * @author Icreon Tech - SR
     * @return Int
     * @param $user_id
     */
    public function getSocialInfo($user_id) {
        
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_getUserSocialInfo(?)');
        $stmt->getResource()->bindParam(1, $user_id);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
             return $resultSet[0];
        } else {
                return false;
        }
    }
    
    /**
     * Function for to get social account
     * @author Icreon Tech - SR
     * @return Int
     * @param $user_id
     */
    public function getCreditCards($param = array()) {
        
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_get_creditCart()');
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
             return $resultSet;
        } else {
                return false;
        }
    }    
    
     /**
     * Function used to get token info
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getTokenDetails($params = array()) {
        try {
            $tokenId = isset($params['profile_id']) ? $params['profile_id'] : null;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getTokenDetails(?)');
            $stmt->getResource()->bindParam(1, $tokenId);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    /**
     * this function update address_information_id of given profile_id
     * @param type $params
     * @return boolean
     */
    public function updatePaymentProfileAddressInfo($params = array())
    {
        $tokenId = isset($params['profile_id']) ? $params['profile_id'] : null;
        $addressId = isset($params['profile_id']) ? $params['address_id'] : null;
        if (intval($tokenId) > 0 && intval($addressId) > 0)
        {
            try
            {
                $stmt = $this->dbAdapter->createStatement();
                $stmt->prepare('UPDATE tbl_tra_payment_profiles SET address_information_id = ? WHERE profile_id = ?');
                $stmt->getResource()->bindParam(1, $addressId);
                $stmt->getResource()->bindParam(2, $tokenId);
                $result = $stmt->execute();
                return $result;
            }
            catch (Exception $e)
            {
                return false;
            }
        }
    }
	/**
     * Function used to get token info
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getUserContactDetails($params = array()) {
        try {
            $userId = isset($params['contact_user_id']) ? $params['contact_user_id'] : null;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getUserInfo(?)');
            $stmt->getResource()->bindParam(1, $userId);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet[0];
        } catch (Exception $e) {
            return false;
        }
    }
     /**
     * Function for insert user data in table for signup
     * @author Icreon Tech - DG
     * @return Int
     * @param Array
     */
    public function saveUserVisitationRecord($param) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_ksk_saveUserVisitationRecord(?,?,?,?,?,?,?,?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $param['user_id']);
        $stmt->getResource()->bindParam(2, $param['arrival_date']);
        $stmt->getResource()->bindParam(3, $param['visiting_with']);
        $stmt->getResource()->bindParam(4, $param['gender']);
        $stmt->getResource()->bindParam(5, $param['birth_year']);
        $stmt->getResource()->bindParam(6, $param['matrial_status']);
        $stmt->getResource()->bindParam(7, $param['ethnicity']);
        $stmt->getResource()->bindParam(8, $param['birth_country']);
        $stmt->getResource()->bindParam(9, $param['immigration_year']);
        $stmt->getResource()->bindParam(10, $param['port_of_entry']);
        $stmt->getResource()->bindParam(11, $param['occupation']);
        $stmt->getResource()->bindParam(12, $param['message']);
        $stmt->getResource()->bindParam(13, $param['created_at']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            return false;
        }
    }
    /**
     * This function will check username
     * @param array
     * @return this will return boolean
     * @author Icreon Tech -DG
     */
    public function isUsernameExist($params = array()) {
        try {
            $username = isset($params['username']) ? $params['username'] : null;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_isUsernameExist(?)');
            $stmt->getResource()->bindParam(1, $username);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

}