<?php

/**
 * This model is used for user woh validation.
 * @category   Zend
 * @package    User
 * @author     Icreon Tech - NS
 */

namespace User\Model;

use User\Module;
// Add these import statements
use Base\Model\BaseModel;
use Zend\Db\Adapter\Adapter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Woh extends BaseModel {
    
   
    protected $inputFilter;
    protected $adapter;

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }
    
    /**
     * Function used to check exchange Add To Cart Wall Of Honor Array  
     * @param Array
     * @return array
     * @author Icreon Tech - NS
     */
    public function exchangeAddToCartWallOfHonorArray($param = array()) {      
        
        $parameter = array();
        $parameter['user_id'] = (isset($param['user_id'])) ? $param['user_id'] : '';
        $parameter['user_session_id'] = (isset($param['user_session_id'])) ? $param['user_session_id'] : '';
        $parameter['donated_for'] = (isset($param['donated_for'])) ? $param['donated_for'] : '';
        $parameter['first_name_one'] = (isset($param['first_name_one'])) ? $param['first_name_one'] : '';
        $parameter['other_name'] = (isset($param['other_name'])) ? $param['other_name'] : '';
        $parameter['other_init_one'] = (isset($param['other_init_one'])) ? $param['other_init_one'] : '';       
        $parameter['last_name_one'] = (isset($param['last_name_one'])) ? $param['last_name_one'] : '';        
        $parameter['first_name_two'] = (isset($param['first_name_two'])) ? $param['first_name_two'] : '';       
        $parameter['other_init_two'] = (isset($param['other_init_two'])) ? $param['other_init_two'] : ''; 
        $parameter['last_name_two'] = (isset($param['last_name_two'])) ? $param['last_name_two'] : ''; 
        $parameter['first_line'] = (isset($param['first_line'])) ? $param['first_line'] : ''; 
        $parameter['second_line'] = (isset($param['second_line'])) ? $param['second_line'] : '';  
        $parameter['origin_country'] = (isset($param['other_country'])) ? $param['other_country'] : ''; 
        $parameter['donated_by'] = (isset($param['donated_by'])) ? $param['donated_by'] : ''; 
        $parameter['num_additional_certificate'] = (isset($param['num_of_duplicate_copies_of_certificate'])) ? $param['num_of_duplicate_copies_of_certificate'] : 0; 
        $parameter['is_correct'] = (isset($param['i_confirm'])) ? $param['i_confirm'] : 0; 
        $parameter['is_agree'] = (isset($param['is_agree'])) ? $param['is_agree'] : 0; 
        $parameter['status'] = (isset($param['status'])) ? $param['status'] : 0;  
        $parameter['product_mapping_id'] = (isset($param['product_mapping_id'])) ? $param['product_mapping_id'] : 0; 
        $parameter['product_id'] = (isset($param['product_id'])) ? $param['product_id'] : 0; 
        $parameter['product_type_id'] = (isset($param['product_type_id'])) ? $param['product_type_id'] : 0; 
        $parameter['product_attribute_option_id'] = (isset($param['product_attribute_option_id'])) ? $param['product_attribute_option_id'] : 0; 
        $parameter['product_sku'] = (isset($param['product_sku'])) ? $param['product_sku'] : 0; 
        $parameter['num_quantity'] = (isset($param['num_quantity'])) ? $param['num_quantity'] : 0; 
        $parameter['product_price'] = (isset($param['product_price'])) ? $param['product_price'] : 0;
        $parameter['added_by'] = (isset($param['added_by'])) ? $param['added_by'] : 0; 
        $parameter['added_date'] = (isset($param['added_date'])) ? $param['added_date'] : ''; 
        $parameter['modified_by'] = (isset($param['modified_by'])) ? $param['modified_by'] : 0; 
        $parameter['modified_date'] = (isset($param['modified_date'])) ? $param['modified_date'] : ''; 	   
        $parameter['final_name'] = (isset($param['final_name'])) ? $param['final_name'] : ''; 	  
       return $parameter;
    }
    
    
    /**
    * This method is used to return the stored procedure array add to cart woh additional contact
    * @param Array
    * @return Array
    * @author Icreon Tech - NS
    */
    public function exchangeAddToCartWallOfHonorAdditionalContactArr($param = array()) {
        $parameter = array();
        $parameter['woh_id'] = (isset($param['woh_id'])) ? $param['woh_id'] : ''; 	
        $parameter['first_name'] = (isset($param['first_name'])) ? $param['first_name'] : '';
        $parameter['last_name'] = (isset($param['last_name'])) ? $param['last_name'] : '';
        $parameter['email_id'] = (isset($param['email_id'])) ? $param['email_id'] : '';
        $parameter['added_date'] = (isset($param['added_date'])) ? $param['added_date'] : '';
        $parameter['modified_date'] = (isset($param['modified_date'])) ? $param['modified_date'] : '';
        $parameter['cart_source_id'] = (isset($param['cart_source_id'])) ? $param['cart_source_id'] : '';
        return $parameter;
    }
	 //Ticket-Id :130
    /**
     * this function is used to creat woh inscribed key
     * @param type $finalName
     */
    public function getInscribeKey($finalName = "") 
    {
        $inscribeKey = "";
        if(trim($finalName) !="")
        {
            $nameStr = $finalName;
            
            if(strripos($nameStr,"<br>",-1))
            {
                $firstLineArr = explode("<br>", $nameStr);
                $nameStr = $firstLineArr[0];
            }
            if(strripos($nameStr,"family",-1))
            {
                $offset = strripos($nameStr,"family",-1);
                $nameStr =  substr($nameStr ,0, $offset-1); 
            }
            $nameStr = str_ireplace(array("the"),"",$nameStr);
            //$nameStr = str_ireplace('\'','"',$nameStr);
           
            $nameStr=trim($nameStr);
            $nameArr = explode(" ", $nameStr);
            $countOfArr = count($nameArr);
            if($countOfArr > 0)
            {
                $lastIndex = $countOfArr - 1;
                
                if($countOfArr > 2 && $nameArr[$lastIndex-1] == "De")
                {
                    $lastName1 = explode("-" , $nameArr[$lastIndex-1]);
                    $inscribeKey = $lastName1[0];
                }
                $lastName2 = explode("-" , $nameArr[$lastIndex]);
                if($inscribeKey !="")
                {
                    $inscribeKey = $inscribeKey." ";
                }
                $inscribeKey = $inscribeKey.$lastName2[0];
            }
            
        }
        return $inscribeKey ; 
    }
  
}
