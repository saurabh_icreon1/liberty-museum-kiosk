<?php

/**
 * This model is used for crm user and crm user roles.
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace User\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This class is used for crm user and crm user roles.
 * @package    User
 * @author     Icreon Tech - AP
 */
class CrmuserTable {

    protected $tableGateway;
    protected $dbAdapter;
    protected $connection;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
        $this->connection = $this->tableGateway->getAdapter()->getDriver()->getConnection();
    }

    /**
     * Function for to insert crm users
     * @param array
     * @return boolean
     * @author Icreon Tech - AP
     */
    public function insertUser($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_insertCrmUser(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_email_id']);
            $stmt->getResource()->bindParam(2, $param['genrated_password']);
            $stmt->getResource()->bindParam(3, $param['crm_first_name']);
            $stmt->getResource()->bindParam(4, $param['crm_last_name']);
            $stmt->getResource()->bindParam(5, $param['crm_phone']);
            $stmt->getResource()->bindParam(6, $param['crm_user_picture']);
            $stmt->getResource()->bindParam(7, $param['crm_address']);
            $stmt->getResource()->bindParam(8, $param['crm_city']);
            $stmt->getResource()->bindParam(9, $param['crm_state']);
            $stmt->getResource()->bindParam(10, $param['crm_country_id']);
            $stmt->getResource()->bindParam(11, $param['crm_zip_code']);
            $stmt->getResource()->bindParam(12, $param['crm_is_active']);
            $stmt->getResource()->bindParam(13, $param['add_date']);
            $stmt->getResource()->bindParam(14, $param['modified_date']);
            $stmt->getResource()->bindParam(15, $param['report_crm_user_id']);
            $stmt->getResource()->bindParam(16, $param['crm_user_role']);
            $stmt->getResource()->bindParam(17, $param['salt']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to insert crm users roles
     * @param Array
     * @return boolean
     * @author Icreon Tech - AP
     */
    public function insertCrmUserRole($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_insertCrmUserRole(?,?)');
            $stmt->getResource()->bindParam(1, $param['user_id']);
            $stmt->getResource()->bindParam(2, $param['role_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to insert role
     * @param Array
     * @return boolean
     * @author Icreon Tech - AP
     */
    public function insertUserRole($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_insertCrmUserMasterRole(?,?)');
            $stmt->getResource()->bindParam(1, $param['role']);
            $stmt->getResource()->bindParam(2, $param['add_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to update role
     * @param Array
     * @return boolean
     * @author Icreon Tech - AP   
     */
    public function updateUserRole($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_updateCrmUserMasterRole(?,?,?)');
            $stmt->getResource()->bindParam(1, $param['role_id']);
            $stmt->getResource()->bindParam(2, $param['role']);
            $stmt->getResource()->bindParam(3, $param['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to update crm users
     * @param Array
     * @return Array
     * @author Icreon Tech - AP 
     */
    public function updateUser($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_updateCrmUser(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_email_id']);
            $stmt->getResource()->bindParam(2, $param['crm_first_name']);
            $stmt->getResource()->bindParam(3, $param['crm_last_name']);
            $stmt->getResource()->bindParam(4, $param['crm_phone']);
            $stmt->getResource()->bindParam(5, $param['crm_user_picture']);
            $stmt->getResource()->bindParam(6, $param['crm_address']);
            $stmt->getResource()->bindParam(7, $param['crm_city']);
            $stmt->getResource()->bindParam(8, $param['crm_state']);
            $stmt->getResource()->bindParam(9, $param['crm_country_id']);
            $stmt->getResource()->bindParam(10, $param['crm_zip_code']);
            $stmt->getResource()->bindParam(11, $param['crm_is_active']);
            $stmt->getResource()->bindParam(12, $param['modified_date']);
            $stmt->getResource()->bindParam(13, $param['report_crm_user_id']);
            $stmt->getResource()->bindParam(14, $param['crm_user_id']);
            $stmt->getResource()->bindParam(15, $param['crm_user_role']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to update crm users status
     * @param Array
     * @return Boolean
     * @author Icreon Tech - AP
     */
    public function updateUsers($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_updateCrmUserStatus(?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $stmt->getResource()->bindParam(2, $param['activity']);
            $stmt->getResource()->bindParam(3, $param['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get crm user deatails
     * @param Array
     * @return Array
     * @author Icreon Tech - AP 
     */
    public function getCrmUserDetails($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getCrmUserInfo(?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get crm user 
     * @param Array
     * @return Array
     * @author Icreon Tech - AP     
     */
    public function getCrmUsers() {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getCrmUsers()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the crm users seached records
     * @param this will be an array.
     * @return this will return search result of crm users 
     * @author Icreon Tech -AP
     */
    public function searchCrmUser($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getCrmuserSearch(?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, (isset($param['name_email']) and trim($param['name_email']) != "") ? addslashes($param['name_email']) : "");
            $stmt->getResource()->bindParam(2, $param['role']);
            $stmt->getResource()->bindParam(3, $param['status']);
            $stmt->getResource()->bindParam(4, $param['startIndex']);
            $stmt->getResource()->bindParam(5, $param['recordLimit']);
            $stmt->getResource()->bindParam(6, $param['sortField']);
            $stmt->getResource()->bindParam(7, $param['sortOrder']);
            $stmt->getResource()->bindParam(8, $param['currentTime']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            //asd($resultSet);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to save searchcrm users records
     * @param this will be an array.
     * @return Boolean
     * @author Icreon Tech -AP
     */
    public function saveCrmUserSearch($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_saveCrmUserSearch(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $stmt->getResource()->bindParam(2, $param['search_name']);
            $stmt->getResource()->bindParam(3, $param['search_query']);
            $stmt->getResource()->bindParam(4, $param['isActive']);
            $stmt->getResource()->bindParam(5, $param['isDelete']);
            $stmt->getResource()->bindParam(6, $param['added_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to update searchcrm users records
     * @param this will be an array.
     * @return Boolean
     * @author Icreon Tech -AP
     */
    public function updateCrmUserSearch($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_updateCrmUserSearch(?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $stmt->getResource()->bindParam(2, $param['search_name']);
            $stmt->getResource()->bindParam(3, $param['search_query']);
            $stmt->getResource()->bindParam(4, $param['isActive']);
            $stmt->getResource()->bindParam(5, $param['isDelete']);
            $stmt->getResource()->bindParam(6, $param['modified_date']);
            $stmt->getResource()->bindParam(7, $param['search_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to delete crm user search
     * @param array.
     * @return boolean
     * @author Icreon Tech -AP
     */
    public function deleteCrmUserSearch($param = array()) {

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_deleteCrmUserSearch(?,?,?)');
            $stmt->getResource()->bindParam(1, $param['search_id']);
            $stmt->getResource()->bindParam(2, $param['isDelete']);
            $stmt->getResource()->bindParam(3, $param['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get save search list of crm users
     * @param array.
     * @return Array 
     * @author Icreon Tech -AP
     */
    public function getCrmUserSaveSearch($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getCrmUserSearchInfo(?)');
            $stmt->getResource()->bindParam(1, $param['search_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to search crm user role
     * @param array.
     * @return Array
     * @author Icreon Tech -AP
     */
    public function searchCrmUserRoles($param = array()) {
        try {
            
            $roleId = (isset($param['roleId']) && $param['roleId'] != '') ? $param['roleId'] : '';
            
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getCrmMaterRoles(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['startIndex']);
            $stmt->getResource()->bindParam(2, $param['recordLimit']);
            $stmt->getResource()->bindParam(3, $param['sortField']);
            $stmt->getResource()->bindParam(4, $param['sortOrder']);
            $stmt->getResource()->bindParam(5, $roleId);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get crm user roles
     * @param array.
     * @return Array
     * @author Icreon Tech -AP
     */
    public function getCrmMasterUserRole($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getCrmUserRoles()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to delete role
     * @param array.
     * @return Boolean
     * @author Icreon Tech -AP
     */
    public function deleteCrmUserMasterRole($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_deleteCrmUserRoles(?)');
            $stmt->getResource()->bindParam(1, $param['role_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the crm users seached records
     * @param this will be an array.
     * @return this will return search result of crm users 
     * @author Icreon Tech -AP
     */
    public function searchCrmUsersById($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getCrmuserById(?)');
            $stmt->getResource()->bindParam(1, $param['ids']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to update crm user assignment
     * @param array.
     * @return Boolean
     * @author Icreon Tech -AP
     */
    public function updateCrmUserAssignedTo($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_updateCrmUserAssignedTO(?,?)');
            $stmt->getResource()->bindParam(1, $param['assigned_to_id']);
            $stmt->getResource()->bindParam(2, $param['user_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for get activities data
     * @param Array
     * @return Array
     * @author Icreon Tech - AP    
     */
    public function getCrmUserAssignments($param = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_usr_getCrmUserAssignmentSearch(?,?,?,?,?,?,?,?,?,?,?,?,?)");
        $stmt->getResource()->bindParam(1, $param['crm_user_id']);
        $stmt->getResource()->bindParam(2, $param['startIndex']);
        $stmt->getResource()->bindParam(3, $param['recordLimit']);
        $stmt->getResource()->bindParam(4, $param['sortField']);
        $stmt->getResource()->bindParam(5, $param['sortOrder']);
        $stmt->getResource()->bindParam(6, $param['source']);
        $stmt->getResource()->bindParam(7, $param['activity_type']);
        $stmt->getResource()->bindParam(8, $param['subject']);
        $stmt->getResource()->bindParam(9, $param['with']);
        $stmt->getResource()->bindParam(10, $param['scheduled']);
        $stmt->getResource()->bindParam(11, $param['from']);
        $stmt->getResource()->bindParam(12, $param['to']);
        $stmt->getResource()->bindParam(13, $param['assigned']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        //asd($resultSet);
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * This function is used to save searchcrm users assignments
     * @param this will be an array.
     * @return Boolean
     * @author Icreon Tech -AP
     */
    public function saveCrmUserAssignments($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_insertCrmUserAssignmentSearch(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $stmt->getResource()->bindParam(2, $param['search_name']);
            $stmt->getResource()->bindParam(3, $param['search_query']);
            $stmt->getResource()->bindParam(4, $param['isActive']);
            $stmt->getResource()->bindParam(5, $param['isDelete']);
            $stmt->getResource()->bindParam(6, $param['added_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to update searchcrm users records
     * @param this will be an array.
     * @return Boolean
     * @author Icreon Tech -AP
     */
    public function updateCrmUserAssignmentSearch($param = array()) {

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_updateCrmUserAssignmentSearch(?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $stmt->getResource()->bindParam(2, $param['search_name']);
            $stmt->getResource()->bindParam(3, $param['search_query']);
            $stmt->getResource()->bindParam(4, $param['isActive']);
            $stmt->getResource()->bindParam(5, $param['isDelete']);
            $stmt->getResource()->bindParam(6, $param['modified_date']);
            $stmt->getResource()->bindParam(7, $param['search_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();

            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /*
     * This function is used to get save search list of crm user assignments
     * @param array.
     * @return Array 
     * @author Icreon Tech -AP
     */

    public function getCrmUserAssignmentSaveSearch($param = array()) {

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getCrmUserAssignmentSearchInfo(?)');
            $stmt->getResource()->bindParam(1, $param['search_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to delete crm user assignment search
     * @param array.
     * @return boolean
     * @author Icreon Tech -AP
     */
    public function deleteCrmUserAssignmentSearch($param = array()) {

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_deleteCrmUserAssignmentSearch(?,?,?)');
            $stmt->getResource()->bindParam(1, $param['search_id']);
            $stmt->getResource()->bindParam(2, $param['isDelete']);
            $stmt->getResource()->bindParam(3, $param['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to delete crm user role
     * @param array
     * @return boolean
     * @author Icreon -AP
     */
    public function deleteCrmUserRole($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_deleteCrmUserRole(?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function is used to change crm user password
     * @param array
     * @return boolean
     * @author Icreon -AP
     */
    public function changeCrmUserPassword($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_updateCrmUserPassword(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $stmt->getResource()->bindParam(2, $param['genrated_password']);
            $stmt->getResource()->bindParam(3, $param['salt']);
            $stmt->getResource()->bindParam(4, $param['modified_date']);
            $stmt->getResource()->bindParam(5, $param['modified_by']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to get module actions
     * @return Array
     * @author Icreon Tech - SK 
     */
    public function getCrmUserModuleActions($module_id = null, $action_id = null) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getCrmUserModuleActions(?,?)');
            $stmt->getResource()->bindParam(1, $module_id);
            $stmt->getResource()->bindParam(2, $action_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to get crm user role permission deatails
     * @return Array
     * @author Icreon Tech - SK 
     */
    public function getCrmUserRolePermissions($role_id = null, $module_id = null, $action_id = null) {
        try {
            $stmt = $this->dbAdapter->createStatement();

            if (is_array($role_id))
                $roleId = implode(",", $role_id);
            else
                $roleId = $role_id;

            $stmt->prepare('CALL usp_usr_getCrmUserRolePermission(?,?,?)');
            $stmt->getResource()->bindParam(1, $roleId);
            $stmt->getResource()->bindParam(2, $module_id);
            $stmt->getResource()->bindParam(3, $action_id);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to insert crm user role permission
     * @return boolean
     * @author Icreon Tech - SK 
     */
    public function insertCrmUserRolePermission($data) {
        try {
            //ticket 73 private group
            $data["allow_private"] = isset($data["allow_private"]) ? $data["allow_private"] : '0';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_insertCrmUserRolePermission(?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $data['module_id']);
            $stmt->getResource()->bindParam(2, $data['role_id']);
            $stmt->getResource()->bindParam(3, $data['is_module_allowed']);
            $stmt->getResource()->bindParam(4, $data['added_by']);
            $stmt->getResource()->bindParam(5, $data['module_added_date']);
            $stmt->getResource()->bindParam(6, $data['modified_by']);
            $stmt->getResource()->bindParam(7, $data['modified_date']);
            $stmt->getResource()->bindParam(8, $data['action_id']);
            $stmt->getResource()->bindParam(9, $data['is_action_allowed']);
            $stmt->getResource()->bindParam(10, $data['action_added_date']);
            $stmt->getResource()->bindParam(11, $data['allow_private']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to update crm user role permission
     * @param Array
     * @return boolean
     * @author Icreon Tech - SK   
     */
    public function updateCrmUserRolePermission($data) {
        try {
            //ticket 73 private group
            $data["allow_private"] = isset($data["allow_private"]) ? $data["allow_private"] : '0';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_updateCrmUserRolePermission(?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $data['role_module_id']);
            $stmt->getResource()->bindParam(2, $data['is_module_allowed']);
            $stmt->getResource()->bindParam(3, $data['module_modified_date']);
            $stmt->getResource()->bindParam(4, $data['role_module_action_id']);
            $stmt->getResource()->bindParam(5, $data['is_action_allowed']);
            $stmt->getResource()->bindParam(6, $data['action_modified_date']);
            $stmt->getResource()->bindParam(7, $data['allow_private']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to check crm user role permission
     * @return array
     * @author Icreon Tech - SK   
     */
    function checkCrmUserPermission($role_id = null, $module_id = null, $action_id = null) {
        try {

            $moduleActions = $this->getCrmUserModuleActions($module_id, $action_id);
            $moduleList = array();
            $actionList = array();

            foreach ($moduleActions as $key => $value) {
                $moduleList[$value['module_name']] = $value['module_id'];
                $actionList[$value['action_title']] = $value['action_id'];
            }

            $userRolePermission = array();
            $modulePermissions = array();
            $moduleActionPermissions = array();
            //ticket 73 private group
            $allowPrivate = array();

            if (!is_null($role_id) && $role_id != '') {
                $crmUserRolePermissions = $this->getCrmUserRolePermissions($role_id, $module_id, $action_id);
                foreach ($crmUserRolePermissions as $key => $value) {
                    if (isset($modulePermissions[$value['module_id']]) && $modulePermissions[$value['module_id']] != $value['is_module_allowed'])
                        $modulePermissions[$value['module_id']] = 0;
                    else
                        $modulePermissions[$value['module_id']] = $value['is_module_allowed'];

                    if (isset($moduleActionPermissions[$value['module_id']][$value['action_id']]) && $moduleActionPermissions[$value['module_id']][$value['action_id']] != $value['is_action_allowed'])
                        $moduleActionPermissions[$value['module_id']][$value['action_id']] = 0;
                    else
                        $moduleActionPermissions[$value['module_id']][$value['action_id']] = $value['is_action_allowed'];
                    if (isset($allowPrivate[$value['module_id']]) && $allowPrivate[$value['module_id']] != $value['is_allow_private'])
                        $allowPrivate[$value['module_id']] = 0;
                    else
                        $allowPrivate[$value['module_id']] = $value['is_allow_private'];
                }
            }

            $userRolePermission['modulePermissions'] = $modulePermissions;
            $userRolePermission['moduleActionPermissions'] = $moduleActionPermissions;
            $userRolePermission['moduleActions'] = $moduleActions;
            $userRolePermission['moduleList'] = $moduleList;
            $userRolePermission['actionList'] = $actionList;
            $userRolePermission['allowPrivate'] = $allowPrivate;

            return $userRolePermission;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to get module action detail
     * @param Array
     * @return boolean
     * @author Icreon Tech - DT
     */
    public function getModuleAction($data) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getModuleAction(?,?,?,?)');
            $module_name = $data[0];
            $controller_name = $data[1];
            $action_name = $data[2];
            $action_url = isset($data[3]) ? $data[3] : '';
            $stmt->getResource()->bindParam(1, $data[0]);
            $stmt->getResource()->bindParam(2, $data[1]);
            $stmt->getResource()->bindParam(3, $data[2]);
            $stmt->getResource()->bindParam(4, $data[3]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetch(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to get dashlet list
     * @param Array
     * @return array
     * @author Icreon Tech - SK
     */
    public function getDashletsList($data) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($data));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_usr_getCrmUserDashletSettings(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $data[0]);
            $stmt->getResource()->bindParam(2, $data[1]);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet;
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for append number of questions marks for stored procedure
     * @author Icreon Tech - DG
     * @return String
     * @param Array
     */
    public function appendQuestionMars($count) {
        $returnstr = '';
        for ($i = 1; $i <= $count; $i++) {
            $returnstr.='?,';
        }
        return rtrim($returnstr, ',');
    }

    /**
     * Function to insert dashlet permission (CRM)
     * @author Icreon Tech - SK
     * @param Array
     */
    public function insertDashletSettings($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_usr_insertCrmUserDashletSettings(?,?,?,?,?,?,?)");
            $stmt->getResource()->bindParam(1, $params['dashlet_id']);
            $stmt->getResource()->bindParam(2, $params['role_id']);
            $stmt->getResource()->bindParam(3, $params['is_allowed']);
            $stmt->getResource()->bindParam(4, $params['added_by']);
            $stmt->getResource()->bindParam(5, $params['added_date']);
            $stmt->getResource()->bindParam(6, $params['modified_by']);
            $stmt->getResource()->bindParam(7, $params['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to update dashlet permission (CRM)
     * @author Icreon Tech - SK
     * @param Array
     */
    public function updateDashletSettings($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_usr_updateCrmUserDashletSettings(?,?,?,?,?,?,?,?)");
            $stmt->getResource()->bindParam(1, $params['dashlet_id']);
            $stmt->getResource()->bindParam(2, $params['role_id']);
            $stmt->getResource()->bindParam(3, $params['is_allowed']);
            $stmt->getResource()->bindParam(4, $params['added_by']);
            $stmt->getResource()->bindParam(5, $params['added_date']);
            $stmt->getResource()->bindParam(6, $params['modified_by']);
            $stmt->getResource()->bindParam(7, $params['modified_date']);
            $stmt->getResource()->bindParam(8, $params['role_dashlet_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to getCrmUserAvailableDashlets
     * @param Array
     * @return boolean
     * @author Icreon Tech - DG
     */
    public function getCrmUserAvailableDashlets($data) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getCrmUserAvailableDashlets(?)');
            $stmt->getResource()->bindParam(1, $data['user_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to getCrmUserDashlets
     * @param Array
     * @return boolean
     * @author Icreon Tech - DG
     */
    public function getCrmUserDashlets($data) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getCrmUserDashlets(?)');
            $stmt->getResource()->bindParam(1, $data['user_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to getCrmUserDashlets
     * @param Array
     * @return boolean
     * @author Icreon Tech - DG
     */
    public function getCrmSaveSearches($data) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_crm_getSaveSearches(?)');
            $stmt->getResource()->bindParam(1, $data['user_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to insert CRM user dashlet
     * @param Array
     * @return boolean
     * @author Icreon Tech - DG
     */
    public function insertCrmUserDashlets($data) {
        try {
            $userId = isset($data['user_id']) ? $data['user_id'] : null;
            $dashletId = isset($data['dashlet_id']) ? $data['dashlet_id'] : null;
            $visibilitySetting = isset($data['visibility_setting']) ? $data['visibility_setting'] : null;
            $searchId = isset($data['search_id']) ? $data['search_id'] : null;
            $searchType = isset($data['search_type']) ? $data['search_type'] : null;
            $modifiedDate = DATE_TIME_FORMAT;
            $visibilityDashboard = isset($data['visibility_dashboard']) ? $data['visibility_dashboard'] : null;
            $position = isset($data['position']) ? $data['position'] : null;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_insertCrmUserDashlets(?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $userId);
            $stmt->getResource()->bindParam(2, $dashletId);
            $stmt->getResource()->bindParam(3, $visibilitySetting);
            $stmt->getResource()->bindParam(4, $searchId);
            $stmt->getResource()->bindParam(5, $searchType);
            $stmt->getResource()->bindParam(6, $modifiedDate);
            $stmt->getResource()->bindParam(7, $visibilityDashboard);
            $stmt->getResource()->bindParam(8, $position);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to insert CRM user dashlet
     * @param Array
     * @return boolean
     * @author Icreon Tech - DG
     */
    public function insertCrmUserSearchDashlets($data) {
        try {
            $modifiedDate = DATE_TIME_FORMAT;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_insertCrmUserSearchDashlets(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $data['user_id']);
            $stmt->getResource()->bindParam(2, $data['dashlet_id']);
            $stmt->getResource()->bindParam(3, $data['position']);
            $stmt->getResource()->bindParam(4, $modifiedDate);
            $stmt->getResource()->bindParam(5, $data['search_id']);
            $stmt->getResource()->bindParam(6, $data['search_type']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to delete CRM user dashlet
     * @param Array
     * @return boolean
     * @author Icreon Tech - DG
     */
    public function deleteCrmUserDashlet($params) {
        try {
            $dashletId = isset($params['dashlet_id']) ? $params['dashlet_id'] : null;
            $searchId = isset($params['search_id']) ? $params['search_id'] : null;
            $searchType = isset($params['search_type']) ? $params['search_type'] : null;
            $userId = isset($params['user_id']) ? $params['user_id'] : null;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_deleteCrmUserDashlets(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $dashletId);
            $stmt->getResource()->bindParam(2, $userId);
            $stmt->getResource()->bindParam(3, $searchId);
            $stmt->getResource()->bindParam(4, $searchType);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function to get CRM disabled module actions
     * @param Array
     * @return Array
     * @author Icreon Tech - SK
     */
    public function getCrmDisableModuleAction($params) {
        try {
            $module_id = (isset($params['module_id']) && $params['module_id'] != '') ? $params['module_id'] : '';
            $action_id = (isset($params['action_id']) && $params['action_id'] != '') ? $params['action_id'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getCrmDisableModuleAction(?,?)');
            $stmt->getResource()->bindParam(1, $module_id);
            $stmt->getResource()->bindParam(2, $action_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for to get top conatct
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getTopContact($dataArr = null) {

        if (isset($dataArr['from_date']) && $dataArr['from_date'] != '') {
            $from_date = $dataArr['from_date'];
        } else {
            $from_date = '';
        }
        if (isset($dataArr['to_date']) && $dataArr['to_date'] != '') {
            $to_date = $dataArr['to_date'];
        } else {
            $to_date = '';
        }

        $startIndex = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $recordLimit = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $sortField = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $sortOrder = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_tra_getTopContacts(?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $startIndex);
        $stmt->getResource()->bindParam(2, $recordLimit);
        $stmt->getResource()->bindParam(3, $sortField);
        $stmt->getResource()->bindParam(4, $sortOrder);
        $stmt->getResource()->bindParam(5, $from_date);
        $stmt->getResource()->bindParam(6, $to_date);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get top selling product
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getTopSellingProduct($dataArr = null) {
        $startIndex = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $recordLimit = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $sortField = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $sortOrder = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        $fromDate = (isset($dataArr['from_date']) && $dataArr['from_date'] != '') ? $dataArr['from_date'] : '';
        $toDate = (isset($dataArr['to_date']) && $dataArr['to_date'] != '') ? $dataArr['to_date'] : '';

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_tra_getTopSellingProducts(?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $startIndex);
        $stmt->getResource()->bindParam(2, $recordLimit);
        $stmt->getResource()->bindParam(3, $sortField);
        $stmt->getResource()->bindParam(4, $sortOrder);
        $stmt->getResource()->bindParam(5, $fromDate);
        $stmt->getResource()->bindParam(6, $toDate);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to getTransactionQueue
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getTransactionByStatus($dataArr = null) {
        $transStatus = (isset($dataArr['item_status']) && $dataArr['item_status'] != '') ? $dataArr['item_status'] : '';

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_tra_getTransactionByStatus(?)');
        $stmt->getResource()->bindParam(1, $transStatus);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to getTransactionQueue
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getTransactionQueue($dataArr = null) {
        $itemStatus = (isset($dataArr['item_status']) && $dataArr['item_status'] != '') ? $dataArr['item_status'] : '';
        $startIndex = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $recordLimit = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $sortField = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $sortOrder = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';

        $fromDate = (isset($dataArr['from_date']) && $dataArr['from_date'] != '') ? $dataArr['from_date'] : '';
        $toDate = (isset($dataArr['to_date']) && $dataArr['to_date'] != '') ? $dataArr['to_date'] : '';

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_tra_getTransactionQues(?,?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $startIndex);
        $stmt->getResource()->bindParam(2, $recordLimit);
        $stmt->getResource()->bindParam(3, $sortField);
        $stmt->getResource()->bindParam(4, $sortOrder);
        $stmt->getResource()->bindParam(5, $itemStatus);
        $stmt->getResource()->bindParam(6, $fromDate);
        $stmt->getResource()->bindParam(7, $toDate);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to getTransactionSummary
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getTransactionSummary($dataArr = null) {
        $fromDate = (isset($dataArr['from_date']) && $dataArr['from_date'] != '') ? $dataArr['from_date'] : '';
        $toDate = (isset($dataArr['to_date']) && $dataArr['to_date'] != '') ? $dataArr['to_date'] : '';
        $startIndex = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $recordLimit = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $sortField = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $sortOrder = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        $currDate = (isset($dataArr['current_date']) && $dataArr['current_date'] != '') ? $dataArr['current_date'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_tra_getTransactionSummary(?,?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $startIndex);
        $stmt->getResource()->bindParam(2, $recordLimit);
        $stmt->getResource()->bindParam(3, $sortField);
        $stmt->getResource()->bindParam(4, $sortOrder);
        $stmt->getResource()->bindParam(5, $toDate);
        $stmt->getResource()->bindParam(6, $fromDate);
        $stmt->getResource()->bindParam(7, $currDate);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
	$statement->closeCursor();
        //asd($resultSet);
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get New Contact
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getNewContacts($dataArr = null) {
        $date = $dataArr['date'];
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_tra_getNewContact(?)');
        $stmt->getResource()->bindParam(1, $date);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get New Annotations
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getNewAnnotations($dataArr = null) {
        $date = $dataArr['date'];
        $type = (isset($dataArr['type'])) ? $dataArr['type'] : '';
        $status = (isset($dataArr['status'])) ? $dataArr['status'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_tra_getNewAnnotations(?,?,?)');
        $stmt->getResource()->bindParam(1, $date);
        $stmt->getResource()->bindParam(2, $type);
        $stmt->getResource()->bindParam(3, $status);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get New Assignments
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getNewAssignments($dataArr = null) {
        $userId = $dataArr['user_id'];
        $date = $dataArr['date'];
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_tra_getNewAssignments(?,?)');
        $stmt->getResource()->bindParam(1, $userId);
        $stmt->getResource()->bindParam(2, $date);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get New Membership
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getNewMemberships($dataArr = null) {
        $date = $dataArr['date'];
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_tra_getNewMemberships(?)');
        $stmt->getResource()->bindParam(1, $date);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get Monthly Transaction
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getMonthlyTransactions($dataArr = null) {
        $to = $dataArr['to'];
        $from = $dataArr['from'];
        $totalSec = $dataArr['totalSec'];
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_tra_getMonthlyTransactionSummary(?,?,?)');
        $stmt->getResource()->bindParam(1, $to);
        $stmt->getResource()->bindParam(2, $from);
        $stmt->getResource()->bindParam(3, $totalSec);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get duw pledge
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function duePledges($dataArr = null) {

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_tra_duePledges()');
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get table structure
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function getMstTableStruct($params = array()) {
        $stmt = $this->dbAdapter->createStatement();
        if (isset($params['dash_id']) && !empty($params['dash_id'])) {
            $params['dash_id'] = $params['dash_id'];
        }
        if (isset($params['search_id']) && !empty($params['search_id'])) {
            $params['search_id'] = $params['search_id'];
        } else {
            $params['search_id'] = '0';
        }
        if (isset($params['search_type']) && !empty($params['search_type'])) {
            $params['search_type'] = $params['search_type'];
        } else {
            $params['search_type'] = '0';
        }
        $stmt->prepare('CALL usp_usr_getMstTableFields(?,?,?)');
        $stmt->getResource()->bindParam(1, $params['dash_id']);
        $stmt->getResource()->bindParam(2, $params['search_id']);
        $stmt->getResource()->bindParam(3, $params['search_type']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for to get table structure
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function getTableStruct($params = array()) {
        $stmt = $this->dbAdapter->createStatement();
        if (isset($params['user_id']) && !empty($params['user_id'])) {
            $params['user_id'] = $params['user_id'];
        }
        if (isset($params['dash_id']) && !empty($params['dash_id'])) {
            $params['dash_id'] = $params['dash_id'];
        }
        if (isset($params['search_id']) && !empty($params['search_id'])) {
            $params['search_id'] = $params['search_id'];
        } else {
            $params['search_id'] = '0';
        }
        if (isset($params['search_type']) && !empty($params['search_type'])) {
            $params['search_type'] = $params['search_type'];
        } else {
            $params['search_type'] = '0';
        }
        $stmt->prepare('CALL usp_usr_getTableFields(?,?,?,?)');
        $stmt->getResource()->bindParam(1, $params['user_id']);
        $stmt->getResource()->bindParam(2, $params['dash_id']);
        $stmt->getResource()->bindParam(3, $params['search_id']);
        $stmt->getResource()->bindParam(4, $params['search_type']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
    }

    /**
     * Function for to update crm dashlet
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function updateCrmDashlet($params = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_updateCrmDashlet(?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $params['col']);
        $stmt->getResource()->bindParam(2, $params['newSeq']);
        $stmt->getResource()->bindParam(3, $params['id']);
        $stmt->getResource()->bindParam(4, $params['sortField']);
        $stmt->getResource()->bindParam(5, $params['visible']);
        $stmt->getResource()->bindParam(6, $params['modifiedDate']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
        return true;
    }

    /**
     * Function for to insert crm dashlet
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function insertCrmDashlet($params = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_insertCrmDashlet(?,?,?,?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $params['col']);
        $stmt->getResource()->bindParam(2, $params['newSeq']);
        $stmt->getResource()->bindParam(3, $params['userId']);
        $stmt->getResource()->bindParam(4, $params['sortField']);
        $stmt->getResource()->bindParam(5, $params['visible']);
        $stmt->getResource()->bindParam(6, $params['modifiedDate']);
        $stmt->getResource()->bindParam(7, $params['dashId']);
        $stmt->getResource()->bindParam(8, $params['searchId']);
        $stmt->getResource()->bindParam(9, $params['searchType']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
        return true;
    }

    /**
     * Function for to insert crm dashlet
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function updateCrmUserDashlet($params = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_updateCrmUserDashlet(?)');
        $stmt->getResource()->bindParam(1, $params['user_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
        return true;
    }

    /**
     * Function for to fetching system dashlet title
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function getSystemDashletTitle($param) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_getSystemDashletTitle(?)');
        $stmt->getResource()->bindParam(1, $param);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
    }

    /**
     * Function for to fetch single dashlet
     * @author Icreon Tech - AS
     * @return Arr
     * @param Array
     */
    public function getCrmUserDashlet($param = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_getUserDashlet(?,?)');
        $stmt->getResource()->bindParam(1, $param['dash_id']);
        $stmt->getResource()->bindParam(2, $param['user_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
    }
}