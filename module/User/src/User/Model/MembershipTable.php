<?php

/**
 * This model is used for user Membership.
 * @package    User_Membership
 * @author     Icreon Tech - DG
 */

namespace User\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This model is used for user Membership.
 * @package    User_Membership
 * @author     Icreon Tech - DG
 */
class MembershipTable {

    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
    }

    /**
     * Function used to get user membership
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getUserMemberships($dataArr = array()) {
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        $start_index = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $record_limit = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $sort_field = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $sort_order = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_getUserMemberships(?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $user_id);
        $stmt->getResource()->bindParam(2, $start_index);
        $stmt->getResource()->bindParam(3, $record_limit);
        $stmt->getResource()->bindParam(4, $sort_field);
        $stmt->getResource()->bindParam(5, $sort_order);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function used to update user membership
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function userUpdateMembership($dataArr = array()) {
        try {
            $user_membership_id = (isset($dataArr['user_membership_id']) && $dataArr['user_membership_id'] != '') ? $dataArr['user_membership_id'] : '';
            $membership_shipped = (isset($dataArr['membership_shipped']) && $dataArr['membership_shipped'] != '') ? $dataArr['membership_shipped'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_updateUserMembership(?,?)');
            $stmt->getResource()->bindParam(1, $user_membership_id);
            $stmt->getResource()->bindParam(2, $membership_shipped);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * Function used to update user membership auto renew
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function updateAutoRenewMembership($dataArr = array()) {
        try {
            $userId = $dataArr['user_id'];
            $autoRenew = $dataArr['auto_renew'];
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL updateAutoRenewMembership(?,?)');
            $stmt->getResource()->bindParam(1, $userId);
            $stmt->getResource()->bindParam(2, $autoRenew);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * Function for to get user campaign count
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function getUserMembershipCount($dataArr) {
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        $not_in_membership = isset($dataArr['not_in_membership']) ? $dataArr['not_in_membership'] : null;
        
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_usr_getUserMembershipCount(?,?)");
        $stmt->getResource()->bindParam(1, $user_id);
        $stmt->getResource()->bindParam(2, $not_in_membership);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet[0]['total'];
    }
    
    /**
     * Function for get all membership
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function getAllMembership($membershipData) {
        //$procquesmarkapp=$this->appendQuestionMars(count($membershipData));
         
        $membershipData['currentYear'] = isset($membershipData['currentYear'])?$membershipData['currentYear']:'';
        $membershipData['isActive'] = isset($membershipData['isActive'])?$membershipData['isActive']:'';
        
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_set_getCrmMembership(?,?,?,?,?,?)");
        $stmt->getResource()->bindParam(1, $membershipData[0]);
        $stmt->getResource()->bindParam(2, $membershipData[1]);
        $stmt->getResource()->bindParam(3, $membershipData[2]);
        $stmt->getResource()->bindParam(4, $membershipData[3]);
        $stmt->getResource()->bindParam(5, $membershipData['currentYear']);
        $stmt->getResource()->bindParam(6, $membershipData['isActive']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }
    
    /**
     * Function for get all membership
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function getAutoRenewedUser($data) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_crm_getAutoRenewedUser(?)");
        $stmt->getResource()->bindParam(1, $data['currDate']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

}