<?php

/**
 * This model is used for user validation.
 * @package    User_Contact
 * @author     Icreon Tech - DG
 */

namespace User\Model;

use User\Module;
// Add these import statements
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\Adapter\Adapter;

/**
 * This model is used for user validation.
 * @package    User_Contact
 * @author     Icreon Tech - DG
 */
class Contact implements InputFilterAwareInterface {

    protected $inputFilter;
    protected $adapter;
    protected $email_id;

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    // Add the following method:
    public function getArrayCopy() {
        return get_object_vars($this);
    }

    // Add content to these methods:
    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    /**
     * Function used to check validation 
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function getInputFilter() {
        
    }

    /**
     * Function used to check variable for user create contact form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function exchangeArray($data) {
        if (!empty($data['email_id'])) {
            $this->email_id = (isset($data['email_id'])) ? $data['email_id'] : null;
        }
    }

    /**
     * Function used to check validation for Create Contact
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function getInputFilterCreateContactIndividual() {
        //no_email_address
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'contact_type',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'first_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'FIRST_NAME_EMPTY',
                                    ),
                                ),
                            ),
//                          array(
//                                'name' => 'Alpha',
//                                'options' => array(
//                                    'messages' => array(
//                                         \Zend\I18n\Filter\Alpha::NOT_ALPHA => "VALID_NAME",
//                                    ),
//                                ),
//                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'middle_name',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'last_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'LAST_NAME_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'email_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMAIL_EMPTY',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'EmailAddress',
                                'options' => array(
                                    'messages' => array(
                                        \Zend\Validator\EmailAddress::INVALID_FORMAT => "EMAIL_INVALID",
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'country_code',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'area_code',
                        'required' => false,
                    )));
            $phonenoFilter = new InputFilter();
            $inputFilter->add($factory->createInput(array(
                        'name' => 'phone_no[0]',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'Enter Phone no',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'phone_type',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'continfo_primary',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'suffix',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'source',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'upload_profile_image',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'title',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'bulk_mail',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'no_email_address',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'afihc',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'alt_name',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'alt_phoneno',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'alt_email',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'EmailAddress',
                                'options' => array(
                                    'messages' => array(
                                        \Zend\Validator\EmailAddress::INVALID_FORMAT => "EMAIL_INVALID",
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'alt_relationship',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'addressinfo_location_type',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'addressinfo_another_contact_address',
                        'required' => false,
                    )));

            // $post["main"]
            $addressFilter = new InputFilter();
            $addressFilter->add($factory->createInput(array(
                        'name' => '0',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                    'message' => array('isEmpty' => 'Enter stree 1',),
                                ),
                            ),
                        ),
                    )));

            //          $inputFilter->add($factory->createInput(array(
            //               'name' => 'addressinfo_street_address_1',
            //              'required' => true,
//                 'validators' => array(
//                            array(
//                                'name' => 'NotEmpty',
//                                'options' => array(
//                                    'messages' => array(
//                                        'isEmpty' => 'EMAIL_EMPTY',
//                                    ),
//                                ),
//                            ),
            //    ),
            //          )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'addressinfo_city',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'addressinfo_zip',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'addressinfo_street_address_2',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'addressinfo_state',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'addressinfo_country',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'addressinfo_search_contact',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'edu_degree_level',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'edu_institution',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'edu_attended_year',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'edu_major',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'edu_gpa',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'communicationpref_email_greetings',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'interests',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'communicationpref_postal_greetings',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'communicationpref_address',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'communicationpref_custome_email_greetings',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'communicationpref_custome_postal_greetings',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'communicationpref_custome_address_greetings',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'demographics_age',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'AGE_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'demographics_marital_status',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'demographics_age_limit',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'demographics_income_range',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'demographics_nationality',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'demographics_ethnicity',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'demographics_year',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'tagsandgroups_company_name',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'tag_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'webaccesss_security_question',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'webaccesss_security_answer',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'webaccesss_enabled_login',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'publicdocs_description',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'publicdocs_upload',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'notes',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'notes_private',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'addressinfo_location',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'communicationpref_privacy',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'communicationpref_preferred_method',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'demographics_gender',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'tagsandgroups_groups',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'phoneno',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'education',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'street_address',
                        'required' => false,
                    )));
            $inputFilter->add($addressFilter, "addressinfo_street_address_1");
            $inputFilter->add($phonenoFilter, "phone_no");
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    /**
     * Function used to check variable for user create contact form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function exchangeArrayCreateContactIndividual($data) {
        if (!empty($data['email_id'])) {
            $this->email_id = (isset($data['email_id'])) ? $data['email_id'] : null;
        }
    }

    /**
     * Function used to check validation for Create Contact
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function getInputFilterCreateContactCorporate() {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_contact_type',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_sic_code',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_addressinfo_location_type',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_source',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'country_code',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'area_code',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'phone_no',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_addressinfo_search_contact',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'phone_type',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'continfo_primary',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_upload_profile_image',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_company_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'COMPANY_NAME_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_legal_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'LEGAL_NAME_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_email_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMAIL_EMPTY',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'EmailAddress',
                                'options' => array(
                                    'messages' => array(
                                        \Zend\Validator\EmailAddress::INVALID_FORMAT => "EMAIL_INVALID",
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_addressinfo_street_address_1[0]',
                        'required' => true,
                        'filters' => array(
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'Enter street address 1',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_addressinfo_country',
                        'required' => true,
                        'filters' => array(
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'CORPORATE_COUNTRY_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_bulk_mail',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_no_email_address',
                        'filters' => array(
                        //   array('name' => 'StripTags'), 
                        //   array('name' => 'StringTrim'), 
                        ),
                        'validators' => array(
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'website',
                        'required' => false,
                        'filters' => array(
                        // array('name' => 'StripTags'), 
                        //  array('name' => 'StringTrim'), 
                        ),
                        'validators' => array(
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'don_not_have_website',
                        'filters' => array(
                        //  array('name' => 'StripTags'), 
                        // array('name' => 'StringTrim'), 
                        ),
                        'validators' => array(
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_addressinfo_location',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_addressinfo_location_type',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_communicationpref_privacy',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_communicationpref_preferred_method',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_tagsandgroups_groups',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'phone_no',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_street_address',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_addressinfo_another_contact_address',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_addressinfo_city',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_addressinfo_zip',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_addressinfo_street_address_2',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_addressinfo_state',
                        'required' => false,
                    )));


            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_addressinfo_search_Contact',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_communicationpref_email_greetings',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_communicationpref_postal_greetings',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_communicationpref_address',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_communicationpref_custome_email_greetings',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_communicationpref_custome_postal_greetings',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_communicationpref_custome_address_greetings',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_tagsandgroups_company_name',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_webaccesss_security_question',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_webaccesss_security_answer',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_webaccesss_enabled_login',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_publicdocs_description',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'publicdocs_upload',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_notes',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'corporate_notes_private',
                        'required' => false,
                    )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    /**
     * Function used to check variable for user create contact form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function exchangeArrayCreateContactCorporate($data) {
        if (!empty($data['email_id'])) {
            $this->email_id = (isset($data['email_id'])) ? $data['email_id'] : null;
        }
    }

    /**
     * Function used to check validation for search Contact
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function getInputFilterContactSearch($filterData) {
        $inputFilterData = array();
        if (is_array($filterData)) {
            foreach ($filterData as $key => $val) {
                if (!is_array($val))
                    $inputFilterData[$key] = trim(addslashes(strip_tags($val)));
            }
        } else {
            $inputFilterData = trim(addslashes(strip_tags($filterData)));
        }
        return $this->inputFilterCrmSearch = $inputFilterData;
    }

    /**
     * Function for get random number
     * @author Icreon Tech - DG
     * @return array
     * @param Int
     */
    public function generateRandomString() {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ23456789";
        srand((double) microtime() * 1000000);
        $i = 0;
        $code = '';

        while ($i <= 7) {
            $num = rand() % 33;
            $tmp = substr($chars, $num, 1);
            $code = $code . $tmp;
            $i++;
        }
        return $code;
    }

    /**
     * Function used to check variable for user create contact form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function filterParam($data) {
        $parameter = array();
        $parameter['corporate_company_name'] = (isset($data['corporate_company_name'])) ? $data['corporate_company_name'] : null;
        $parameter['corporate_legal_name'] = (isset($data['corporate_legal_name'])) ? $data['corporate_legal_name'] : null;
        $parameter['corporate_sic_code'] = (isset($data['corporate_sic_code'])) ? $data['corporate_sic_code'] : null;
//        $parameter['country_id'] = (isset($data['country_id'])) ? $data['country_id'] : null;
//        $parameter['zip_code'] = (isset($data['zip_code'])) ? $data['zip_code'] : null;

        $parameter['contact_type'] = (isset($data['contact_type'])) ? $data['contact_type'] : null;
        $parameter['upload_profile_image'] = (isset($data['upload_profile_image'])) ? $data['upload_profile_image'] : null;
        $parameter['first_name'] = (isset($data['first_name'])) ? $data['first_name'] : null;
        $parameter['middle_name'] = (isset($data['middle_name'])) ? $data['middle_name'] : null;
        $parameter['last_name'] = (isset($data['last_name'])) ? $data['last_name'] : null;
        $parameter['title'] = (isset($data['title'])) ? $data['title'] : null;
        $parameter['suffix'] = (isset($data['suffix'])) ? $data['suffix'] : null;
        $parameter['email_id'] = (isset($data['email_id'])) ? $data['email_id'] : null;
        $parameter['no_email_address'] = (isset($data['no_email_address'])) ? $data['no_email_address'] : '0';
        $parameter['source'] = (isset($data['source'])) ? $data['source'] : null;
        $parameter['afihc'] = (isset($data['afihc'])) ? $data['afihc'] : null;
        $parameter['webaccesss_security_question'] = (isset($data['webaccesss_security_question'])) ? $data['webaccesss_security_question'] : null;
        $parameter['webaccesss_security_answer'] = (isset($data['webaccesss_security_answer'])) ? $data['webaccesss_security_answer'] : null;
        $parameter['webaccesss_enabled_login'] = (isset($data['webaccesss_enabled_login'])) ? $data['webaccesss_enabled_login'] : '0';
        $parameter['demographics_gender'] = (isset($data['demographics_gender'])) ? $data['demographics_gender'] : null;
        $parameter['demographics_marital_status'] = (isset($data['demographics_marital_status'])) ? $data['demographics_marital_status'] : null;
        $parameter['demographics_age_limit'] = (isset($data['demographics_age_limit'])) ? $data['demographics_age_limit'] : '0';
        $parameter['demographics_income_range'] = (isset($data['demographics_income_range'])) ? $data['demographics_income_range'] : null;
        $parameter['demographics_nationality'] = (isset($data['demographics_nationality'])) ? $data['demographics_nationality'] : null;
        $parameter['demographics_ethnicity'] = (isset($data['demographics_ethnicity'])) ? $data['demographics_ethnicity'] : null;
        $parameter['upload_profile_image_id'] = (isset($data['upload_profile_image_id'])) ? $data['upload_profile_image_id'] : null;
        $parameter['publicdocs_upload_count'] = (isset($data['publicdocs_upload_count'])) ? $data['publicdocs_upload_count'] : null;
        $parameter['don_not_have_website'] = (isset($data['don_not_have_website'])) ? $data['don_not_have_website'] : '0';
        $parameter['salt'] = $this->generateRandomString();
        $parameter['verify_code'] = $this->generateRandomString();
        $password = $this->generateRandomString();
        $parameter['password'] = md5($password . $parameter['salt']);
        $parameter['genrated_password'] = $password;

        $parameter['add_date'] = $data['add_date'];
        $parameter['modified_date'] = $data['modified_date'];

        $parameter['user_id'] = (isset($data['user_id'])) ? $data['user_id'] : null;
        $parameter['alt_name'] = (isset($data['alt_name'])) ? $data['alt_name'] : null;
        $parameter['alt_email'] = (isset($data['alt_email'])) ? $data['alt_email'] : null;
        $parameter['alt_phoneno'] = (isset($data['alt_phoneno'])) ? $data['alt_phoneno'] : null;
        $parameter['alt_relationship'] = (isset($data['alt_relationship'])) ? $data['alt_relationship'] : null;

        $parameter['added_by'] = (isset($data['added_by'])) ? $data['added_by'] : null;
        $parameter['notes'] = (isset($data['notes'])) ? $data['notes'] : null;
        $parameter['notes_private'] = (isset($data['notes_private'])) ? $data['notes_private'] : null;

        $parameter['communicationpref_email_greetings'] = (isset($data['communicationpref_email_greetings'])) ? $data['communicationpref_email_greetings'] : null;
        $parameter['communicationpref_postal_greetings'] = (isset($data['communicationpref_postal_greetings'])) ? $data['communicationpref_postal_greetings'] : null;
        $parameter['communicationpref_address'] = (isset($data['communicationpref_address'])) ? $data['communicationpref_address'] : null;
        $parameter['communicationpref_custome_email_greetings'] = (isset($data['communicationpref_custome_email_greetings'])) ? $data['communicationpref_custome_email_greetings'] : null;
        $parameter['communicationpref_custome_postal_greetings'] = (isset($data['communicationpref_custome_postal_greetings'])) ? $data['communicationpref_custome_postal_greetings'] : null;
        $parameter['communicationpref_custome_address_greetings'] = (isset($data['communicationpref_custome_address_greetings'])) ? $data['communicationpref_custome_address_greetings'] : null;
        if (isset($data['communicationpref_preferred_method']) && count($data['communicationpref_preferred_method']) > 0) {
            $parameter['communicationpref_preferred_method'] = implode(",", $data['communicationpref_preferred_method']);
        } else {
            $parameter['communicationpref_preferred_method'] = null;
        }
        if (isset($data['communicationpref_privacy']) && count($data['communicationpref_privacy']) > 0) {
            $parameter['communicationpref_privacy'] = implode(",", $data['communicationpref_privacy']);
        } else {
            $parameter['communicationpref_privacy'] = null;
        }

        $parameter['tag_id'] = (isset($data['tag_id'])) ? $data['tag_id'] : null;
        $parameter['tagsandgroups_company_name'] = (isset($data['tagsandgroups_company_name'])) ? $data['tagsandgroups_company_name'] : null;
        $parameter['publicdocs_description'] = (isset($data['publicdocs_description'])) ? $data['publicdocs_description'] : null;
        
        $parameter['contact_status'] = (isset($data['contact_status'])) ? $data['contact_status'] : null;
        $parameter['reason'] = (isset($data['reason'])) ? $data['reason'] : null;
        $parameter['reason_other'] = (isset($data['reason_other'])) ? $data['reason_other'] : null;
        return $parameter;
    }

    /**
     * Function used to check variable for user create contact form
     * @author Icreon Tech - DG
     * @return boolean
     * @param Array
     */
    public function filterParamCorporate($data) {
        $parameter = array();
        $parameter['contact_type'] = (isset($data['corporate_contact_type'])) ? $data['corporate_contact_type'] : null;
        $parameter['corporate_company_name'] = (isset($data['corporate_company_name'])) ? $data['corporate_company_name'] : null;
        $parameter['corporate_legal_name'] = (isset($data['corporate_legal_name'])) ? $data['corporate_legal_name'] : null;
        $parameter['corporate_sic_code'] = (isset($data['corporate_sic_code'])) ? $data['corporate_sic_code'] : null;
        $parameter['upload_profile_image_id'] = (isset($data['corporate_upload_profile_image_id'])) ? $data['corporate_upload_profile_image_id'] : null;
//        $parameter['country_id'] = (isset($data['corporate_country_id'])) ? $data['corporate_country_id'] : null;
//        $parameter['zip_code'] = (isset($data['corporate_zip_code'])) ? $data['corporate_zip_code'] : null;
        $parameter['email_id'] = (isset($data['corporate_email_id'])) ? $data['corporate_email_id'] : null;
        $parameter['no_email_address'] = (isset($data['corporate_no_email_address'])) ? $data['corporate_no_email_address'] : '0';
        $parameter['don_not_have_website'] = (isset($data['don_not_have_website'])) ? $data['don_not_have_website'] : '0';
        $parameter['source'] = (isset($data['corporate_source'])) ? $data['corporate_source'] : null;

        $parameter['webaccesss_security_question'] = (isset($data['corporate_webaccesss_security_question'])) ? $data['corporate_webaccesss_security_question'] : null;
        $parameter['webaccesss_security_answer'] = (isset($data['corporate_webaccesss_security_answer'])) ? $data['corporate_webaccesss_security_answer'] : null;
        $parameter['webaccesss_enabled_login'] = (isset($data['corporate_webaccesss_enabled_login'])) ? $data['corporate_webaccesss_enabled_login'] : '0';
        $parameter['cor_publicdocs_upload_count'] = (isset($data['cor_publicdocs_upload_count'])) ? $data['cor_publicdocs_upload_count'] : null;

        $parameter['verify_code'] = $this->generateRandomString();
        $parameter['salt'] = $this->generateRandomString();
        $password = $this->generateRandomString();
        $parameter['password'] = md5($password . $parameter['salt']);
        $parameter['genrated_password'] = $password;

        $parameter['add_date'] = $data['add_date'];
        $parameter['modified_date'] = $data['modified_date'];

        $parameter['user_id'] = (isset($data['user_id'])) ? $data['user_id'] : null;


        $parameter['added_by'] = (isset($data['added_by'])) ? $data['added_by'] : null;
        $parameter['notes'] = (isset($data['corporate_notes'])) ? $data['corporate_notes'] : null;
        $parameter['notes_private'] = (isset($data['corporate_notes_private'])) ? $data['corporate_notes_private'] : null;

        $parameter['communicationpref_email_greetings'] = (isset($data['corporate_communicationpref_email_greetings'])) ? $data['corporate_communicationpref_email_greetings'] : null;
        $parameter['communicationpref_postal_greetings'] = (isset($data['corporate_communicationpref_postal_greetings'])) ? $data['corporate_communicationpref_postal_greetings'] : null;
        $parameter['communicationpref_address'] = (isset($data['corporate_communicationpref_address'])) ? $data['corporate_communicationpref_address'] : null;
        $parameter['communicationpref_custome_email_greetings'] = (isset($data['corporate_communicationpref_custome_email_greetings'])) ? $data['corporate_communicationpref_custome_email_greetings'] : null;
        $parameter['communicationpref_custome_postal_greetings'] = (isset($data['corporate_communicationpref_custome_postal_greetings'])) ? $data['corporate_communicationpref_custome_postal_greetings'] : null;
        $parameter['communicationpref_custome_address_greetings'] = (isset($data['corporate_communicationpref_custome_address_greetings'])) ? $data['corporate_communicationpref_custome_address_greetings'] : null;
        if (isset($data['corporate_communicationpref_preferred_method']) && count($data['corporate_communicationpref_preferred_method']) > 0) {
            $parameter['communicationpref_preferred_method'] = implode(",", $data['corporate_communicationpref_preferred_method']);
        } else {
            $parameter['communicationpref_preferred_method'] = null;
        }
        if (isset($data['corporate_communicationpref_privacy']) && count($data['corporate_communicationpref_privacy']) > 0) {
            $parameter['communicationpref_privacy'] = implode(",", $data['corporate_communicationpref_privacy']);
        } else {
            $parameter['communicationpref_privacy'] = null;
        }

        $parameter['tag_id'] = (isset($data['corporate_tag_id'])) ? $data['corporate_tag_id'] : null;
        $parameter['tagsandgroups_company_name'] = (isset($data['corporate_tagsandgroups_company_name'])) ? $data['corporate_tagsandgroups_company_name'] : null;
        $parameter['corporate_publicdocs_description'] = (isset($data['corporate_publicdocs_description'])) ? $data['corporate_publicdocs_description'] : null;
        
        $parameter['contact_status'] = (isset($data['contact_status'])) ? $data['contact_status'] : null;
        $parameter['reason'] = (isset($data['reason'])) ? $data['reason'] : null;
        $parameter['reason_other'] = (isset($data['reason_other'])) ? $data['reason_other'] : null;
        
//        if (isset($data['publicdocs_upload']) && count($data['publicdocs_upload']) > 0) {
//            $parameter['publicdocs_upload'] = implode(",", $data['publicdocs_upload']);
//        } else {
//            $parameter['publicdocs_upload'] = null;
//        }
        return $parameter;
    }

    /**
     * Function used to check validation for Create Popup Contact
     * @author Icreon Tech - DT
     * @return boolean
     * @param Array
     */
    public function getInputFilterCreatePopupContact() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'title',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'first_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'FIRST_NAME_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'middle_name',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'last_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'LAST_NAME_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'suffix',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'email_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'EMAIL_EMPTY',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'EmailAddress',
                                'options' => array(
                                    'messages' => array(
                                        \Zend\Validator\EmailAddress::INVALID_FORMAT => "EMAIL_INVALID",
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'Db\NoRecordExists',
                                'options' => array(
                                    'table' => 'tbl_usr_users',
                                    'field' => 'email_id',
                                    'adapter' => $this->adapter,
                                    'exclude' => array(
                                        'field' => 'is_delete',
                                        'value' => '1'
                                    ),
                                    'messages' => array(
                                        'recordFound' => 'EMAIL_ID_ALREADY',
                                    ),
                                ),
                            ),
                            
                        ),
                    )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    /**
     * Function used to check variable for user create popup contact form
     * @author Icreon Tech - DT
     * @return boolean
     * @param Array
     */
    public function filterParamPopup($data) {
        $parameter = array();
        $parameter['title'] = (isset($data['title'])) ? $data['title'] : null;
        $parameter['first_name'] = (isset($data['first_name'])) ? $data['first_name'] : null;
        $parameter['middle_name'] = (isset($data['middle_name'])) ? $data['middle_name'] : null;
        $parameter['last_name'] = (isset($data['last_name'])) ? $data['last_name'] : null;
        $parameter['suffix'] = (isset($data['suffix'])) ? $data['suffix'] : null;
        $parameter['email_id'] = (isset($data['email_id'])) ? $data['email_id'] : null;
        $parameter['verify_code'] = '';//$this->generateRandomString();
        $parameter['salt'] = $this->generateRandomString();
        $password = $this->generateRandomString();
        $parameter['password'] = md5($password . $parameter['salt']);
        $parameter['genrated_password'] = $password;
        $parameter['source_id'] = 3;
        $parameter['webaccesss_enabled_login'] = '1';
        $parameter['is_active'] = '1';
        $parameter['add_date'] = $data['add_date'];
        $parameter['modified_date'] = $data['modified_date'];
        return $parameter;
    }

}