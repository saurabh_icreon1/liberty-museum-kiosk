<?php

/**
 * This model is used for Development.
 * @package    User/DevelopmentTable
 * @author     Icreon Tech - DG
 */

namespace User\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This model is used for Development.
 * @package    User/DevelopmentTable
 * @author     Icreon Tech - DG
 */
class DevelopmentTable {

    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
    }

    /**
     * Function for get user insert user org
     * @author Icreon Tech - DG
     * @return array
     */
    public function insertUserOrganisationGift($dataArr) {
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        $year = isset($dataArr['year']) ? $dataArr['year'] : null;
        $organization = isset($dataArr['organization']) ? $dataArr['organization'] : null;
        $organization_type = isset($dataArr['organization_type']) ? $dataArr['organization_type'] : null;
        $amount = isset($dataArr['amount']) ? $dataArr['amount'] : null;
        $added_by = isset($dataArr['added_by']) ? $dataArr['added_by'] : null;
        $added_date = DATE_TIME_FORMAT;

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_insertUserOrganisationGift(?,?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $user_id);
        $stmt->getResource()->bindParam(2, $year);
        $stmt->getResource()->bindParam(3, $organization);
        $stmt->getResource()->bindParam(4, $organization_type);
        $stmt->getResource()->bindParam(5, $amount);
        $stmt->getResource()->bindParam(6, $added_by);
        $stmt->getResource()->bindParam(7, $added_date);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for get user insert user job history
     * @author Icreon Tech - DG
     * @return array
     */
    public function insertUserJobHistory($dataArr) {
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        $job_title = isset($dataArr['job_title']) ? $dataArr['job_title'] : null;
        $period_from = isset($dataArr['period_from']) ? $dataArr['period_from'] : null;
        $period_to = isset($dataArr['period_to']) ? $dataArr['period_to'] : null;
        $company_id = isset($dataArr['company_id']) ? $dataArr['company_id'] : null;
        $industry = isset($dataArr['industry']) ? $dataArr['industry'] : null;
        $added_date = DATE_TIME_FORMAT;

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_insertUserJobHistory(?,?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $user_id);
        $stmt->getResource()->bindParam(2, $period_from);
        $stmt->getResource()->bindParam(3, $period_to);
        $stmt->getResource()->bindParam(4, $company_id);
        $stmt->getResource()->bindParam(5, $job_title);
        $stmt->getResource()->bindParam(6, $industry);
        $stmt->getResource()->bindParam(7, $added_date);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for get user delete user job history
     * @author Icreon Tech - DG
     * @return array
     */
    public function deleteUserJobHistory($dataArr) {
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_deleteUserJobHistory(?)');
        $stmt->getResource()->bindParam(1, $user_id);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for get user delete user org
     * @author Icreon Tech - DG
     * @return array
     */
    public function deleteUserOrganisationGift($dataArr) {
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_deleteUserOrganisationGift(?)');
        $stmt->getResource()->bindParam(1, $user_id);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for get user Development Detail
     * @author Icreon Tech - DG
     * @return array
     */
    public function getUserDevelopment($dataArr) {
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_getUserDevelopment(?)');
        $stmt->getResource()->bindParam(1, $user_id);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0];
        } else {
            return false;
        }
    }

    /**
     * Function for insert user Development
     * @author Icreon Tech - DG
     * @return array
     */
    public function insertUserDevelopment($dataArr) {
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        $total_given_amount = isset($dataArr['total_given_amount']) ? $dataArr['total_given_amount'] : null;
        $total_no_of_gifts = isset($dataArr['total_no_of_gifts']) ? $dataArr['total_no_of_gifts'] : null;
        $last_gift_date = isset($dataArr['last_gift_date']) ? $dataArr['last_gift_date'] : null;
        $last_gift_amount = isset($dataArr['last_gift_amount']) ? $dataArr['last_gift_amount'] : null;
        $largest_gift_date = isset($dataArr['largest_gift_date']) ? $dataArr['largest_gift_date'] : null;
        $largest_gift_amount = isset($dataArr['largest_gift_amount']) ? $dataArr['largest_gift_amount'] : null;
        $total_income = isset($dataArr['total_income']) ? $dataArr['total_income'] : null;
        $total_real_estate_value = isset($dataArr['total_real_estate_value']) ? $dataArr['total_real_estate_value'] : null;
        $total_stock_value = isset($dataArr['total_stock_value']) ? $dataArr['total_stock_value'] : null;
        $total_pension_value = isset($dataArr['total_pension_value']) ? $dataArr['total_pension_value'] : null;
        $total_political_non_profit_giving_amount = isset($dataArr['total_political_non_profit_giving_amount']) ? $dataArr['total_political_non_profit_giving_amount'] : null;
        $income_portion_of_egc_calculation = isset($dataArr['income_portion_of_egc_calculation']) ? $dataArr['income_portion_of_egc_calculation'] : null;
        $pol_non_profit_giving_portion_of_egc_calc = isset($dataArr['pol_non_profit_giving_portion_of_egc_calc']) ? $dataArr['pol_non_profit_giving_portion_of_egc_calc'] : null;
        $rating_summary_giving_capacity_rating = isset($dataArr['rating_summary_giving_capacity_rating']) ? $dataArr['rating_summary_giving_capacity_rating'] : null;
        $rating_summary_giving_capacity_range = isset($dataArr['rating_summary_giving_capacity_range']) ? $dataArr['rating_summary_giving_capacity_range'] : null;
        $estimated_giving_capacity = isset($dataArr['estimated_giving_capacity']) ? $dataArr['estimated_giving_capacity'] : null;
        $propensity_to_give_score = isset($dataArr['propensity_to_give_score']) ? $dataArr['propensity_to_give_score'] : null;
        $propensity_to_give_score_part_2 = isset($dataArr['propensity_to_give_score_part_2']) ? $dataArr['propensity_to_give_score_part_2'] : null;
        $combined_propensity_to_give_score = isset($dataArr['combined_propensity_to_give_score']) ? $dataArr['combined_propensity_to_give_score'] : null;
        $planned_giving_bequest = isset($dataArr['planned_giving_bequest']) ? $dataArr['planned_giving_bequest'] : null;
        $planned_giving_annuity = isset($dataArr['planned_giving_annuity']) ? $dataArr['planned_giving_annuity'] : null;
        $planned_giving_trust = isset($dataArr['planned_giving_trust']) ? $dataArr['planned_giving_trust'] : null;
        $rating_summary_influence = isset($dataArr['rating_summary_influence']) ? $dataArr['rating_summary_influence'] : null;
        $rating_summary_inclination = isset($dataArr['rating_summary_inclination']) ? $dataArr['rating_summary_inclination'] : null;
        $total_quality_of_match = isset($dataArr['total_quality_of_match']) ? $dataArr['total_quality_of_match'] : null;
        $charitable_donations_of_qom = isset($dataArr['charitable_donations_of_qom']) ? $dataArr['charitable_donations_of_qom'] : null;
        $trustees_qom = isset($dataArr['trustees_qom']) ? $dataArr['trustees_qom'] : null;
        $guidestar_directors_qom = isset($dataArr['guidestar_directors_qom']) ? $dataArr['guidestar_directors_qom'] : null;
        $guidestar_foundations_qom = isset($dataArr['guidestar_foundations_qom']) ? $dataArr['guidestar_foundations_qom'] : null;
        $hoover_business_information_qom = isset($dataArr['hoover_business_information_qom']) ? $dataArr['hoover_business_information_qom'] : null;
        $household_profile_qom = isset($dataArr['household_profile_qom']) ? $dataArr['household_profile_qom'] : null;
        $lexis_nexis_qom = isset($dataArr['lexis_nexis_qom']) ? $dataArr['lexis_nexis_qom'] : null;
        $major_donor_qom = isset($dataArr['major_donor_qom']) ? $dataArr['major_donor_qom'] : null;
        $philanthropic_donations_qom = isset($dataArr['philanthropic_donations_qom']) ? $dataArr['philanthropic_donations_qom'] : null;
        $volunteers_and_directors_qom = isset($dataArr['volunteers_and_directors_qom']) ? $dataArr['volunteers_and_directors_qom'] : null;
        $wealth_id_securities_qom = isset($dataArr['wealth_id_securities_qom']) ? $dataArr['wealth_id_securities_qom'] : null;
        $bio_information = isset($dataArr['bio_information']) ? $dataArr['bio_information'] : null;
        $description = isset($dataArr['description']) ? $dataArr['description'] : null;
        $publicdocs_upload = isset($dataArr['publicdocs_upload']) ? $dataArr['publicdocs_upload'] : null;
        $added_by = isset($dataArr['added_by']) ? $dataArr['added_by'] : null;
        $added_date = DATE_TIME_FORMAT;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_insertUserDevelopment(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $user_id);
        $stmt->getResource()->bindParam(2, $total_given_amount);
        $stmt->getResource()->bindParam(3, $total_no_of_gifts);
        $stmt->getResource()->bindParam(4, $last_gift_date);
        $stmt->getResource()->bindParam(5, $last_gift_amount);
        $stmt->getResource()->bindParam(6, $largest_gift_date);
        $stmt->getResource()->bindParam(7, $largest_gift_amount);
        $stmt->getResource()->bindParam(8, $total_income);
        $stmt->getResource()->bindParam(9, $total_real_estate_value);
        $stmt->getResource()->bindParam(10, $total_stock_value);
        $stmt->getResource()->bindParam(11, $total_pension_value);
        $stmt->getResource()->bindParam(12, $total_political_non_profit_giving_amount);
        $stmt->getResource()->bindParam(13, $income_portion_of_egc_calculation);
        $stmt->getResource()->bindParam(14, $pol_non_profit_giving_portion_of_egc_calc);
        $stmt->getResource()->bindParam(15, $rating_summary_giving_capacity_rating);
        $stmt->getResource()->bindParam(16, $rating_summary_giving_capacity_range);
        $stmt->getResource()->bindParam(17, $estimated_giving_capacity);
        $stmt->getResource()->bindParam(18, $propensity_to_give_score);
        $stmt->getResource()->bindParam(19, $propensity_to_give_score_part_2);
        $stmt->getResource()->bindParam(20, $combined_propensity_to_give_score);
        $stmt->getResource()->bindParam(21, $planned_giving_bequest);
        $stmt->getResource()->bindParam(22, $planned_giving_annuity);
        $stmt->getResource()->bindParam(23, $planned_giving_trust);
        $stmt->getResource()->bindParam(24, $rating_summary_influence);
        $stmt->getResource()->bindParam(25, $rating_summary_inclination);
        $stmt->getResource()->bindParam(26, $total_quality_of_match);
        $stmt->getResource()->bindParam(27, $charitable_donations_of_qom);
        $stmt->getResource()->bindParam(28, $trustees_qom);
        $stmt->getResource()->bindParam(29, $guidestar_directors_qom);
        $stmt->getResource()->bindParam(30, $guidestar_foundations_qom);
        $stmt->getResource()->bindParam(31, $hoover_business_information_qom);
        $stmt->getResource()->bindParam(32, $household_profile_qom);
        $stmt->getResource()->bindParam(33, $lexis_nexis_qom);
        $stmt->getResource()->bindParam(34, $major_donor_qom);
        $stmt->getResource()->bindParam(35, $philanthropic_donations_qom);
        $stmt->getResource()->bindParam(36, $volunteers_and_directors_qom);
        $stmt->getResource()->bindParam(37, $wealth_id_securities_qom);
        $stmt->getResource()->bindParam(38, $bio_information);
        $stmt->getResource()->bindParam(39, $description);
        $stmt->getResource()->bindParam(40, $publicdocs_upload);
        $stmt->getResource()->bindParam(41, $added_by);
        $stmt->getResource()->bindParam(42, $added_date);

        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            return false;
        }
    }

    /**
     * 
     * Function for update user Development
     * @author Icreon Tech - DG
     * @return array
     */
    public function updateUserDevelopment($dataArr) {
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        $total_given_amount = isset($dataArr['total_given_amount']) ? $dataArr['total_given_amount'] : null;
        $total_no_of_gifts = isset($dataArr['total_no_of_gifts']) ? $dataArr['total_no_of_gifts'] : null;
        $last_gift_date = isset($dataArr['last_gift_date']) ? $dataArr['last_gift_date'] : null;
        $last_gift_amount = isset($dataArr['last_gift_amount']) ? $dataArr['last_gift_amount'] : null;
        $largest_gift_date = isset($dataArr['largest_gift_date']) ? $dataArr['largest_gift_date'] : null;
        $largest_gift_amount = isset($dataArr['largest_gift_amount']) ? $dataArr['largest_gift_amount'] : null;
        $total_income = isset($dataArr['total_income']) ? $dataArr['total_income'] : null;
        $total_real_estate_value = isset($dataArr['total_real_estate_value']) ? $dataArr['total_real_estate_value'] : null;
        $total_stock_value = isset($dataArr['total_stock_value']) ? $dataArr['total_stock_value'] : null;
        $total_pension_value = isset($dataArr['total_pension_value']) ? $dataArr['total_pension_value'] : null;
        $total_political_non_profit_giving_amount = isset($dataArr['total_political_non_profit_giving_amount']) ? $dataArr['total_political_non_profit_giving_amount'] : null;
        $income_portion_of_egc_calculation = isset($dataArr['income_portion_of_egc_calculation']) ? $dataArr['income_portion_of_egc_calculation'] : null;
        $pol_non_profit_giving_portion_of_egc_calc = isset($dataArr['pol_non_profit_giving_portion_of_egc_calc']) ? $dataArr['pol_non_profit_giving_portion_of_egc_calc'] : null;
        $rating_summary_giving_capacity_rating = isset($dataArr['rating_summary_giving_capacity_rating']) ? $dataArr['rating_summary_giving_capacity_rating'] : null;
        $rating_summary_giving_capacity_range = isset($dataArr['rating_summary_giving_capacity_range']) ? $dataArr['rating_summary_giving_capacity_range'] : null;
        $estimated_giving_capacity = isset($dataArr['estimated_giving_capacity']) ? $dataArr['estimated_giving_capacity'] : null;
        $propensity_to_give_score = isset($dataArr['propensity_to_give_score']) ? $dataArr['propensity_to_give_score'] : null;
        $propensity_to_give_score_part_2 = isset($dataArr['propensity_to_give_score_part_2']) ? $dataArr['propensity_to_give_score_part_2'] : null;
        $combined_propensity_to_give_score = isset($dataArr['combined_propensity_to_give_score']) ? $dataArr['combined_propensity_to_give_score'] : null;
        $planned_giving_bequest = isset($dataArr['planned_giving_bequest']) ? $dataArr['planned_giving_bequest'] : null;
        $planned_giving_annuity = isset($dataArr['planned_giving_annuity']) ? $dataArr['planned_giving_annuity'] : null;
        $planned_giving_trust = isset($dataArr['planned_giving_trust']) ? $dataArr['planned_giving_trust'] : null;
        $rating_summary_influence = isset($dataArr['rating_summary_influence']) ? $dataArr['rating_summary_influence'] : null;
        $rating_summary_inclination = isset($dataArr['rating_summary_inclination']) ? $dataArr['rating_summary_inclination'] : null;
        $total_quality_of_match = isset($dataArr['total_quality_of_match']) ? $dataArr['total_quality_of_match'] : null;
        $charitable_donations_of_qom = isset($dataArr['charitable_donations_of_qom']) ? $dataArr['charitable_donations_of_qom'] : null;
        $trustees_qom = isset($dataArr['trustees_qom']) ? $dataArr['trustees_qom'] : null;
        $guidestar_directors_qom = isset($dataArr['guidestar_directors_qom']) ? $dataArr['guidestar_directors_qom'] : null;
        $guidestar_foundations_qom = isset($dataArr['guidestar_foundations_qom']) ? $dataArr['guidestar_foundations_qom'] : null;
        $hoover_business_information_qom = isset($dataArr['hoover_business_information_qom']) ? $dataArr['hoover_business_information_qom'] : null;
        $household_profile_qom = isset($dataArr['household_profile_qom']) ? $dataArr['household_profile_qom'] : null;
        $lexis_nexis_qom = isset($dataArr['lexis_nexis_qom']) ? $dataArr['lexis_nexis_qom'] : null;
        $major_donor_qom = isset($dataArr['major_donor_qom']) ? $dataArr['major_donor_qom'] : null;
        $philanthropic_donations_qom = isset($dataArr['philanthropic_donations_qom']) ? $dataArr['philanthropic_donations_qom'] : null;
        $volunteers_and_directors_qom = isset($dataArr['volunteers_and_directors_qom']) ? $dataArr['volunteers_and_directors_qom'] : null;
        $wealth_id_securities_qom = isset($dataArr['wealth_id_securities_qom']) ? $dataArr['wealth_id_securities_qom'] : null;
        $bio_information = isset($dataArr['bio_information']) ? $dataArr['bio_information'] : null;
        $description = isset($dataArr['description']) ? $dataArr['description'] : null;
        $publicdocs_upload = isset($dataArr['publicdocs_upload']) ? $dataArr['publicdocs_upload'] : null;
        $modified_by = isset($dataArr['added_by']) ? $dataArr['added_by'] : null;
        $modified_date = DATE_TIME_FORMAT;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_updateUserDevelopment(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $user_id);
        $stmt->getResource()->bindParam(2, $total_given_amount);
        $stmt->getResource()->bindParam(3, $total_no_of_gifts);
        $stmt->getResource()->bindParam(4, $last_gift_date);
        $stmt->getResource()->bindParam(5, $last_gift_amount);
        $stmt->getResource()->bindParam(6, $largest_gift_date);
        $stmt->getResource()->bindParam(7, $largest_gift_amount);
        $stmt->getResource()->bindParam(8, $total_income);
        $stmt->getResource()->bindParam(9, $total_real_estate_value);
        $stmt->getResource()->bindParam(10, $total_stock_value);
        $stmt->getResource()->bindParam(11, $total_pension_value);
        $stmt->getResource()->bindParam(12, $total_political_non_profit_giving_amount);
        $stmt->getResource()->bindParam(13, $income_portion_of_egc_calculation);
        $stmt->getResource()->bindParam(14, $pol_non_profit_giving_portion_of_egc_calc);
        $stmt->getResource()->bindParam(15, $rating_summary_giving_capacity_rating);
        $stmt->getResource()->bindParam(16, $rating_summary_giving_capacity_range);
        $stmt->getResource()->bindParam(17, $estimated_giving_capacity);
        $stmt->getResource()->bindParam(18, $propensity_to_give_score);
        $stmt->getResource()->bindParam(19, $propensity_to_give_score_part_2);
        $stmt->getResource()->bindParam(20, $combined_propensity_to_give_score);
        $stmt->getResource()->bindParam(21, $planned_giving_bequest);
        $stmt->getResource()->bindParam(22, $planned_giving_annuity);
        $stmt->getResource()->bindParam(23, $planned_giving_trust);
        $stmt->getResource()->bindParam(24, $rating_summary_influence);
        $stmt->getResource()->bindParam(25, $rating_summary_inclination);
        $stmt->getResource()->bindParam(26, $total_quality_of_match);
        $stmt->getResource()->bindParam(27, $charitable_donations_of_qom);
        $stmt->getResource()->bindParam(28, $trustees_qom);
        $stmt->getResource()->bindParam(29, $guidestar_directors_qom);
        $stmt->getResource()->bindParam(30, $guidestar_foundations_qom);
        $stmt->getResource()->bindParam(31, $hoover_business_information_qom);
        $stmt->getResource()->bindParam(32, $household_profile_qom);
        $stmt->getResource()->bindParam(33, $lexis_nexis_qom);
        $stmt->getResource()->bindParam(34, $major_donor_qom);
        $stmt->getResource()->bindParam(35, $philanthropic_donations_qom);
        $stmt->getResource()->bindParam(36, $volunteers_and_directors_qom);
        $stmt->getResource()->bindParam(37, $wealth_id_securities_qom);
        $stmt->getResource()->bindParam(38, $bio_information);
        $stmt->getResource()->bindParam(39, $description);
        $stmt->getResource()->bindParam(40, $publicdocs_upload);
        $stmt->getResource()->bindParam(41, $modified_by);
        $stmt->getResource()->bindParam(42, $modified_date);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * 
     * Function for import user Development
     * @author Icreon Tech - DG
     * @return array
     */
    public function importUserDevelopment($dataArr) {
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        $total_given_amount = isset($dataArr['total_giving_amount']) ? $dataArr['total_giving_amount'] : null;
        $total_no_of_gifts = isset($dataArr['total_number_of_gifts']) ? $dataArr['total_number_of_gifts'] : null;
        $last_gift_date = isset($dataArr['last_gift_date']) ? $dataArr['last_gift_date'] : null;
        $last_gift_amount = isset($dataArr['last_gift_amount']) ? $dataArr['last_gift_amount'] : null;
        $largest_gift_date = isset($dataArr['largest_gift_date']) ? $dataArr['largest_gift_date'] : null;
        $largest_gift_amount = isset($dataArr['largest_gift_amount']) ? $dataArr['largest_gift_amount'] : null;
        $total_income = isset($dataArr['total_income']) ? $dataArr['total_income'] : null;
        $total_real_estate_value = isset($dataArr['total_real_estate_value']) ? $dataArr['total_real_estate_value'] : null;
        $total_stock_value = isset($dataArr['total_stock_value']) ? $dataArr['total_stock_value'] : null;
        $total_pension_value = isset($dataArr['total_pension_value']) ? $dataArr['total_pension_value'] : null;
        $total_political_non_profit_giving_amount = isset($dataArr['total_political_non_profit_giving_amount']) ? $dataArr['total_political_non_profit_giving_amount'] : null;
        $pol_non_profit_giving_portion_of_egc_calc = isset($dataArr['pol_non_profit_giving_portion_of_egc_calc']) ? $dataArr['pol_non_profit_giving_portion_of_egc_calc'] : null;
        $rating_summary_giving_capacity_rating = isset($dataArr['rating_summary_giving_capacity_rating']) ? $dataArr['rating_summary_giving_capacity_rating'] : null;
        $rating_summary_giving_capacity_range = isset($dataArr['rating_summary_giving_capacity_range']) ? $dataArr['rating_summary_giving_capacity_range'] : null;
        $estimated_giving_capacity = isset($dataArr['estimated_giving_capacity']) ? $dataArr['estimated_giving_capacity'] : null;
        $propensity_to_give_score = isset($dataArr['propensity_to_give_score']) ? $dataArr['propensity_to_give_score'] : null;
        $propensity_to_give_score_part_2 = isset($dataArr['propensity_to_give_score_part_2']) ? $dataArr['propensity_to_give_score_part_2'] : null;
        $hoover_business_information_qom = isset($dataArr['hoovers_business_information_qom']) ? $dataArr['hoovers_business_information_qom'] : null;
        $combined_propensity_to_give_score = isset($dataArr['combined_propensity_to_give_score']) ? $dataArr['combined_propensity_to_give_score'] : null;
        $planned_giving_bequest = isset($dataArr['planned_giving_bequest']) ? $dataArr['planned_giving_bequest'] : null;
        $planned_giving_annuity = isset($dataArr['planned_giving_annuity']) ? $dataArr['planned_giving_annuity'] : null;
        $planned_giving_trust = isset($dataArr['planned_giving_trust']) ? $dataArr['planned_giving_trust'] : null;
        $rating_summary_influence = isset($dataArr['rating_summary_influence']) ? $dataArr['rating_summary_influence'] : null;
        $rating_summary_inclination = isset($dataArr['rating_summary_inclination']) ? $dataArr['rating_summary_inclination'] : null;
        $total_quality_of_match = isset($dataArr['total_quality_of_match']) ? $dataArr['total_quality_of_match'] : null;
        $trustees_qom = isset($dataArr['trustees_qom']) ? $dataArr['trustees_qom'] : null;
        $guidestar_directors_qom = isset($dataArr['guidestar_directors_qom']) ? $dataArr['guidestar_directors_qom'] : null;
        $guidestar_foundations_qom = isset($dataArr['guidestar_foundations_qom']) ? $dataArr['guidestar_foundations_qom'] : null;
        $household_profile_qom = isset($dataArr['household_profile_qom']) ? $dataArr['household_profile_qom'] : null;
        $lexis_nexis_qom = isset($dataArr['lexis_nexis_qom']) ? $dataArr['lexis_nexis_qom'] : null;
        $major_donor_qom = isset($dataArr['major_donor_qom']) ? $dataArr['major_donor_qom'] : null;
        $philanthropic_donations_qom = isset($dataArr['philanthropic_donations_qom']) ? $dataArr['philanthropic_donations_qom'] : null;
        $wealth_id_securities_qom = isset($dataArr['wealthid_securities_qom']) ? $dataArr['wealthid_securities_qom'] : null;
        $modified_by = isset($dataArr['added_by']) ? $dataArr['added_by'] : null;
        $modified_date = DATE_TIME_FORMAT;

        $stmt = $this->dbAdapter->createStatement();
        if (isset($dataArr['count']) && $dataArr['count'] > 0) {
            $stmt->prepare('CALL usp_usr_updateDevelopmentImport(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
        } else {
            $stmt->prepare('CALL usp_usr_insertDevelopmentImport(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
        }
        $income_portion_of_egc_calculation = isset($dataArr['income_portion_of_egc_calculation']) ? $dataArr['income_portion_of_egc_calculation'] : null;
        $charitable_donations_of_qom = isset($dataArr['charitable_donations_of_qom']) ? $dataArr['charitable_donations_of_qom'] : null;        
        $volunteers_and_directors_qom = isset($dataArr['volunteers_and_directors_qom']) ? $dataArr['volunteers_and_directors_qom'] : null;
        $stmt->getResource()->bindParam(1, $total_given_amount);
        $stmt->getResource()->bindParam(2, $total_no_of_gifts);
        $stmt->getResource()->bindParam(3, $last_gift_date);
        $stmt->getResource()->bindParam(4, $last_gift_amount);
        $stmt->getResource()->bindParam(5, $largest_gift_date);
        $stmt->getResource()->bindParam(6, $largest_gift_amount);
        $stmt->getResource()->bindParam(7, $total_income);
        $stmt->getResource()->bindParam(8, $total_real_estate_value);
        $stmt->getResource()->bindParam(9, $total_stock_value);
        $stmt->getResource()->bindParam(10, $total_pension_value);
        $stmt->getResource()->bindParam(11, $total_political_non_profit_giving_amount);
        $stmt->getResource()->bindParam(12, $pol_non_profit_giving_portion_of_egc_calc);
        $stmt->getResource()->bindParam(13, $rating_summary_giving_capacity_rating);
        $stmt->getResource()->bindParam(14, $rating_summary_giving_capacity_range);
        $stmt->getResource()->bindParam(15, $estimated_giving_capacity);
        $stmt->getResource()->bindParam(16, $propensity_to_give_score);
        $stmt->getResource()->bindParam(17, $propensity_to_give_score_part_2);
        $stmt->getResource()->bindParam(18, $combined_propensity_to_give_score);
        $stmt->getResource()->bindParam(19, $planned_giving_bequest);
        $stmt->getResource()->bindParam(20, $planned_giving_annuity);
        $stmt->getResource()->bindParam(21, $planned_giving_trust);
        $stmt->getResource()->bindParam(22, $rating_summary_influence);
        $stmt->getResource()->bindParam(23, $rating_summary_inclination);
        $stmt->getResource()->bindParam(24, $total_quality_of_match);
        $stmt->getResource()->bindParam(25, $trustees_qom);
        $stmt->getResource()->bindParam(26, $guidestar_directors_qom);
        $stmt->getResource()->bindParam(27, $guidestar_foundations_qom);
        $stmt->getResource()->bindParam(28, $hoover_business_information_qom);
        $stmt->getResource()->bindParam(29, $household_profile_qom);
        $stmt->getResource()->bindParam(30, $lexis_nexis_qom);
        $stmt->getResource()->bindParam(31, $major_donor_qom);
        $stmt->getResource()->bindParam(32, $philanthropic_donations_qom);
        $stmt->getResource()->bindParam(33, $wealth_id_securities_qom);
        $stmt->getResource()->bindParam(34, $modified_by);
        $stmt->getResource()->bindParam(35, $modified_date);
        $stmt->getResource()->bindParam(36, $user_id);
        $stmt->getResource()->bindParam(37, $income_portion_of_egc_calculation);
        $stmt->getResource()->bindParam(38, $charitable_donations_of_qom);
        $stmt->getResource()->bindParam(39, $volunteers_and_directors_qom);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for get user job history
     * @author Icreon Tech - DG
     * @return array
     */
    public function getUserJobHistory($dataArr) {
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_getUserJobHistory(?)');
        $stmt->getResource()->bindParam(1, $user_id);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function for get user OrganisationGift
     * @author Icreon Tech - DG
     * @return array
     */
    public function getUserOrganisationGift($dataArr) {
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_getUserOrganisationGift(?)');
        $stmt->getResource()->bindParam(1, $user_id);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }
    
    /**
     * Function used to update contact development doc
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function updateContactDevelopmentDoc($dataArr) {
        $userId = isset($dataArr['userId']) ? $dataArr['userId'] : null;
        $updatedDoc = (isset($dataArr['updatedDoc']) && $dataArr['updatedDoc'] != '') ? $dataArr['updatedDoc'] : '';
        $modifiedDate = DATE_TIME_FORMAT;
        
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_deleteContactDevelopmentDoc(?,?,?)');
        $stmt->getResource()->bindParam(1, $userId);
        $stmt->getResource()->bindParam(2, $updatedDoc);
        $stmt->getResource()->bindParam(3, $modifiedDate);
        $result = $stmt->execute();
    }
    
    /**
     * Function for get user Development Detail
     * @author Icreon Tech - DG
     * @return array
     */
    public function isUserDevelopment($dataArr) {
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_isUserDevelopment(?)');
        $stmt->getResource()->bindParam(1, $user_id);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet[0]['totalRecord'];
    }
	/**
     * Function for get user soft credit 
     * @author Icreon Tech - DG
     * @return array
     */
    public function getSoftCreditReceived($param = array()){
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_softCreditReceived(?,?,?,?,?)');
		$stmt->getResource()->bindParam(1, $param['user_id']);
		$stmt->getResource()->bindParam(2, $param['startIndex']);
        $stmt->getResource()->bindParam(3, $param['recordLimit']);
        $stmt->getResource()->bindParam(4, $param['sortField']);
        $stmt->getResource()->bindParam(5, $param['sortOrder']);
		$result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }
	/**
     * Function for get user soft credit provider
     * @author Icreon Tech - DG
     * @return array
     */
     public function getSoftCreditProvider($param = array()){
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_softCreditProvider(?,?,?,?,?)');
		$stmt->getResource()->bindParam(1, $param['user_id']);
		$stmt->getResource()->bindParam(2, $param['startIndex']);
        $stmt->getResource()->bindParam(3, $param['recordLimit']);
        $stmt->getResource()->bindParam(4, $param['sortField']);
        $stmt->getResource()->bindParam(5, $param['sortOrder']);
		$result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }
	/**
     * Function for get user soft credit provider
     * @author Icreon Tech - DG
     * @return array
     */
     public function getSoftCreditTransaction($param = array()){
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_tra_getCrmTransactionsCredit(?,?,?,?,?,?,?,?,?)');
		$stmt->getResource()->bindParam(1, $param['name_company_email']);
		$stmt->getResource()->bindParam(2, $param['user_contact_id']);
		$stmt->getResource()->bindParam(3, $param['user_transaction_id']);
		$stmt->getResource()->bindParam(4, $param['received_date_from']);
		$stmt->getResource()->bindParam(5, $param['received_date_to']);
		$stmt->getResource()->bindParam(6, $param['startIndex']);
        $stmt->getResource()->bindParam(7, $param['recordLimit']);
        $stmt->getResource()->bindParam(8, $param['sortField']);
        $stmt->getResource()->bindParam(9, $param['sortOrder']);
		$result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }
	/**
     * Function for to delete soft credit
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
   public function deleteSoftCredit($params = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_deleteContactSoftCredit(?,?,?)');
        $stmt->getResource()->bindParam(1, $params['soft_credit_id']);
        $stmt->getResource()->bindParam(2, $params['is_delete']);
        $stmt->getResource()->bindParam(3, $params['modified_date']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

}