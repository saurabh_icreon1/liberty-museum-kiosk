<?php

/**
 * This model is used for user Relationship.
 * @package    User_Relationship
 * @author     Icreon Tech - DG
 */

namespace User\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This model is used for user Relationship.
 * @package    User_Relationship
 * @author     Icreon Tech - DG
 */
class RelationshipTable {

    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
    }

    /**
     * Function used to get user Relationship
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getUserRelationships($dataArr = array()) {
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        $start_index = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $record_limit = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $sort_field = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $sort_order = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';

        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_getUserRelationships(?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $user_id);
        $stmt->getResource()->bindParam(2, $start_index);
        $stmt->getResource()->bindParam(3, $record_limit);
        $stmt->getResource()->bindParam(4, $sort_field);
        $stmt->getResource()->bindParam(5, $sort_order);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return false;
        }
    }

    /**
     * Function used to get user Relationship info
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function getUserRelationshipInfo($dataArr = array()) {
        $user_relationship_id = isset($dataArr['user_relationship_id']) ? $dataArr['user_relationship_id'] : null;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_getUserRelationshipInfo(?)');
        $stmt->getResource()->bindParam(1, $user_relationship_id);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0];
        } else {
            return false;
        }
    }

    /**
     * Function used to update user relationship
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function updateUserRelationship($dataArr = array()) {
        try {
            $user_membership_id = (isset($dataArr['user_relationship_id']) && $dataArr['user_relationship_id'] != '') ? $dataArr['user_relationship_id'] : '';
            $type = (isset($dataArr['type']) && $dataArr['type'] != '') ? $dataArr['type'] : '';
            $notes = (isset($dataArr['notes']) && $dataArr['notes'] != '') ? $dataArr['notes'] : '';
            $enabled = (isset($dataArr['enabled']) && $dataArr['enabled'] != '') ? $dataArr['enabled'] : '';
            $modified_date = DATE_TIME_FORMAT;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_updateUserRelationships(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $user_membership_id);
            $stmt->getResource()->bindParam(2, $type);
            $stmt->getResource()->bindParam(3, $notes);
            $stmt->getResource()->bindParam(4, $enabled);
            $stmt->getResource()->bindParam(5, $modified_date);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function used to create user relationship
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function insertUserRelationships($dataArr = array()) {
        try {
            $relative_user_id = (isset($dataArr['relative_user_id']) && $dataArr['relative_user_id'] != '') ? $dataArr['relative_user_id'] : '';
            $type = (isset($dataArr['type']) && $dataArr['type'] != '') ? $dataArr['type'] : '';
            $notes = (isset($dataArr['notes']) && $dataArr['notes'] != '') ? $dataArr['notes'] : '';
            $enabled = (isset($dataArr['enabled']) && $dataArr['enabled'] != '') ? $dataArr['enabled'] : '0';
            $user_id = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '';
            $added_date = DATE_TIME_FORMAT;
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_insertUserRelationships(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $relative_user_id);
            $stmt->getResource()->bindParam(2, $type);
            $stmt->getResource()->bindParam(3, $notes);
            $stmt->getResource()->bindParam(4, $enabled);
            $stmt->getResource()->bindParam(5, $user_id);
            $stmt->getResource()->bindParam(6, $added_date);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if ($resultSet) {
                return $resultSet[0]['LAST_INSERT_ID'];
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function used to delete  user relationship
     * @author Icreon Tech - DG
     * @return Arr
     * @param Array
     */
    public function deleteUserRelationship($dataArr = array()) {
        try {
            $user_relationship_id = (isset($dataArr['relationship_id']) && $dataArr['relationship_id'] != '') ? $dataArr['relationship_id'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_deleteUserRelationship(?)');
            $stmt->getResource()->bindParam(1, $user_relationship_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * Function for to get user relationship count
     * @author Icreon Tech - DG
     * @return Array
     * @param Array
     */
    public function getUserRelationshipCount($dataArr) {
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_usr_getUserRealtionshipCount(?)");
        $stmt->getResource()->bindParam(1, $user_id);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet[0]['total'];
    }

}