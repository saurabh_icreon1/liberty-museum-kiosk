<?php

/**
 * This model is used for fof.
 * @category   Zend
 * @package    User
 * @author     Icreon Tech - NS
 */

namespace User\Model;

use Zend\Db\TableGateway\TableGateway;

class FofTable {
    
    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
    }
	
	 /**
     * This function is used to insert User Fof
     * @param $params
     * @return boolean 
     * @author Icreon Tech - NS
     */
    public function insertUserFof($param = array()) { 
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_fof_insertUserFof(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['user_id']);
            $stmt->getResource()->bindParam(2, $param['user_session_id']);
            $stmt->getResource()->bindParam(3, $param['image_name']);
            $stmt->getResource()->bindParam(4, $param['fof_image']);
            $stmt->getResource()->bindParam(5, $param['is_photo_uploaded']);
            $stmt->getResource()->bindParam(6, $param['donated_by']);
            $stmt->getResource()->bindParam(7, $param['people_photo']);
            $stmt->getResource()->bindParam(8, $param['is_people_photo_visible']);
            $stmt->getResource()->bindParam(9, $param['photo_caption']);
            $stmt->getResource()->bindParam(10, $param['is_photo_caption_visible']);
            $stmt->getResource()->bindParam(11, $param['is_agree']);
            $stmt->getResource()->bindParam(12, $param['product_mapping_id']);
            $stmt->getResource()->bindParam(13, $param['product_id']);
            $stmt->getResource()->bindParam(14, $param['product_type_id']);
            $stmt->getResource()->bindParam(15, $param['product_sku']);
            $stmt->getResource()->bindParam(16, $param['added_date']);
            $stmt->getResource()->bindParam(17, $param['added_by']);
            $stmt->getResource()->bindParam(18, $param['people_photo_json']);
            $stmt->getResource()->bindParam(19, $param['campaign_id']);
            $stmt->getResource()->bindParam(20, $param['campaign_code']);
            $stmt->getResource()->bindParam(21, $param['promotion_id']);
            $stmt->getResource()->bindParam(22, $param['coupon_code']);
            $stmt->getResource()->bindParam(23, $param['user_group_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
	
	
	/**
	* This function is used to get User Fof Product Id
	* @param $params
	* @return boolean 
	* @author Icreon Tech - NS
	*/
    public function getUserFofProductId($param = array()) {
        try {
            if(isset($param['product_type_id']) and trim($param['product_type_id']) != "" ) {
                $param['product_type_id'] = trim($param['product_type_id']);

                $stmt = $this->dbAdapter->createStatement();
                $stmt->prepare('CALL usp_user_getProductId(?)');
                $stmt->getResource()->bindParam(1, $param['product_type_id']);
                $result = $stmt->execute();
                $statement = $result->getResource();
                $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
                $statement->closeCursor();
                return $resultSet[0]['ProductId'];
            }
            else {
                return 0;    
            }
        } catch (Exception $e) {
            return 0;
        }
    }
	
    
      
     /**
     * This function will get User FOF images
     * @param array
     * @return return array
     * @author Icreon Tech - NS
     */
    public function getFOFImages($param = array()) {
        try {
		
            if(isset($param['start_index']) and trim($param['start_index']) != "") { $startIndex = trim($param['start_index']); } else { $startIndex = 0; }
            if(isset($param['search_flag_status']) and trim($param['search_flag_status']) != "") { $searchFlagStatus = trim($param['search_flag_status']); } else { $searchFlagStatus = 0; }		  
            if(isset($param['people_photo']) and trim($param['people_photo']) != "") { $param['people_photo'] = addslashes($param['people_photo']); } else { $param['people_photo'] = ""; }

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getFofImages(?,?,?,?,?)');

            $stmt->getResource()->bindParam(1, $param['people_photo']);
            $stmt->getResource()->bindParam(2, $param['fof_id']);
            $stmt->getResource()->bindParam(3, $param['record_limit']);
			$stmt->getResource()->bindParam(4, $startIndex);
			$stmt->getResource()->bindParam(5, $searchFlagStatus);
            
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    
     /**
     * This function will get User Searches
     * @param array
     * @return return array
     * @author Icreon Tech - DG
     */
    /*public function getUserSearches($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getFofSearch(?,?)');

            $stmt->getResource()->bindParam(1, $param['fof_search_id']);
            $stmt->getResource()->bindParam(2, $param['user_id']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    public function getUserSearches($dataArr = array()) {
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        $fofSearchId = isset($dataArr['fof_search_id']) ? $dataArr['fof_search_id'] : null;
        $start_index = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $record_limit = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $sort_field = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $sort_order = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_getFofSearch(?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $fofSearchId);
        $stmt->getResource()->bindParam(2, $user_id);
        $stmt->getResource()->bindParam(3, $start_index);
        $stmt->getResource()->bindParam(4, $record_limit);
        $stmt->getResource()->bindParam(5, $sort_field);
        $stmt->getResource()->bindParam(6, $sort_order);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return array();
        }
    }
	*/
	 public function getUserSearches($dataArr = array()) {
        $user_id = isset($dataArr['user_id']) ? $dataArr['user_id'] : null;
        $fofSearchId = isset($dataArr['fof_search_id']) ? $dataArr['fof_search_id'] : null;
        $start_index = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $record_limit = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $sort_field = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $sort_order = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_usr_getFofSearch(?,?,?,?,?,?)');
        $stmt->getResource()->bindParam(1, $fofSearchId);
        $stmt->getResource()->bindParam(2, $user_id);
        $stmt->getResource()->bindParam(3, $start_index);
        $stmt->getResource()->bindParam(4, $record_limit);
        $stmt->getResource()->bindParam(5, $sort_field);
        $stmt->getResource()->bindParam(6, $sort_order);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet;
        } else {
            return array();
        }
    }

      /**
     * This function will save User Searches
     * @param array
     * @return return array
     * @author Icreon Tech - NS
     */
    public function saveUserSearches($param = array()) {

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_insertFofSearch(?,?,?,?,?)');

            $stmt->getResource()->bindParam(1, $param['user_id']);
            $stmt->getResource()->bindParam(2, $param['title']);
            $stmt->getResource()->bindParam(3, $param['search_query']);
            $stmt->getResource()->bindParam(4, $param['save_date']);
            $stmt->getResource()->bindParam(5, $param['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    
      /**
     * This function will delete User Searches
     * @param array
     * @return return array
     * @author Icreon Tech - NS
     */
    public function deleteUserSearches($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_deleteFofSearch(?)');
            $stmt->getResource()->bindParam(1, $param['fof_search_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
 }   