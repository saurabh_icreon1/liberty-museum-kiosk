<?php

/**
 * This controller is used for crm users.
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace User\Controller;

use Base\Controller\BaseController;
use Zend\Authentication\AuthenticationService;
use Zend\View\Model\ViewModel;
use Base\Model\UploadHandler;
use User\Model\Crmuser;
use Activity\Model\Activity;
use Zend\Mvc\Controller\Plugin\Params;
use Zend\Http\Request;
use User\Form\CrmUserForm;
use User\Form\CrmUserChangePasswordForm;
use User\Form\SearchCrmUserForm;
use User\Form\SaveSearchCrmUserForm;
use User\Form\DeleteUserForm;
use User\Form\UserRoleForm;
use User\Form\UpdateCrmUserForm;
use User\Form\SearchAssignmentForm;
use User\Form\UserRolePermissionForm;
use User\Form\DashletSettingForm;
use Cases\Model\Cases;
use Pledge\Model\Pledge;
use Transaction\Model\Transaction;

/**
 * This class is used to add edit search list crm users and crm user roles.
 * @package    User
 * @author     Icreon Tech - AP
 */
class CrmuserController extends BaseController {

    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_flashMessage = null;
    protected $_authservice = null;
    public $auth = null;

    public function __construct() {
        $auth = new AuthenticationService();
        $this->_flashMessage = $this->flashMessenger();
        $this->auth = $auth;
    }

    /**
     * This function is used to get config variables
     * @return array 
     * @author Icreon Tech - AP
     */
    public function getConfig() {
        $sm = $this->getServiceLocator();
        $this->_translator = $sm->get('translator');
        $this->_config = $sm->get('Config');
    }

    /**
     * This function is used to get table connections
     * @return     array 
     * @author Icreon Tech - AP
     */
    public function getTables() {
        $sm = $this->getServiceLocator();
        $this->_userTable = $sm->get('User\Model\CrmuserTable');
        $this->_adapter = $sm->get('dbAdapter');
        return $this->_userTable;
    }

    /**
     * This Action is used to create crm user
     * @param array
     * @return array
     * @author Icreon Tech-AP    
     */
    public function addCrmUserAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getConfig();
        $this->getTables();
        $crmMessages = array_merge($this->_config['user_messages']['config']['common'], $this->_config['user_messages']['config']['crm-create-user']);
        $applicationConfigIds = $this->_config['application_config_ids'];
        $uSId = $applicationConfigIds['united_state_country_id'];
        $request = $this->getRequest();
        $form = new CrmUserForm();
        $crmUserObj = new Crmuser($this->_adapter);
        $form->get('status')->setAttribute('value', 1);
        $state = $this->getServiceLocator()->get('Common\Model\CommonTable')->getStates();
        $stateList = array();
        $stateList[''] = 'Select State';
        foreach ($state as $key => $val) {
            $stateList[$val['state']] = $val['state'];
        }
        $form->get('state_select')->setAttribute('options', $stateList);


        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
        $userRole = $this->getTables()->getCrmMasterUserRole();
        if (!empty($userRole)) {
            $userRoleList = array();
            foreach ($userRole as $key => $val) {
                $userRoleList[$val['role_id']] = $val['role'];
            }
            $form->get('role')->setAttribute('options', $userRoleList);
        }
        $countryList = array();
        foreach ($country as $key => $val) {
            $countryList[$val['country_id']] = $val['name'];
        }
        $form->get('country_id')->setAttribute('options', $countryList);
        $response = $this->getResponse();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            $form->setInputFilter($crmUserObj->getInputFilter());
            if (!$form->isValid()) {
                $errors = $form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submitbutton') {
                        foreach ($row as $rower) {
                            $msg [$key] = $crmMessages[$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $formData = $form->getData();
                $date = DATE_TIME_FORMAT;
                $formData['add_date'] = $date;
                $formData['modified_date'] = $date;
                $formData['state'] = ($formData['country_id'] == $uSId) ? $formData['state_select'] : $formData['state'];
                $filterParam = $crmUserObj->filterParam($formData);
                $userId = $this->getTables()->insertUser($filterParam);
                if (!empty($formData['thumbnail_virtualface'])) {
                    $this->moveFileFromTempToUser($formData['thumbnail_virtualface'], $userId, '');
                }
                if ($userId) {

                    foreach ($formData['role'] as $val) {
                        $roleParam['user_id'] = $userId;
                        $roleParam['role_id'] = $val;
                        $this->getTables()->insertCrmUserRole($roleParam);
                    }
                    $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                    $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                    $userDataArr['first_name'] = $filterParam['crm_first_name'];
                    $userDataArr['last_name'] = $filterParam['crm_last_name'];
                    $userDataArr['email_id'] = $filterParam['crm_email_id'];
                    $userDataArr['password'] = $filterParam['crm_password'];
                    $crmUserCreateTempId = 23;
                    $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $crmUserCreateTempId, $this->_config['email_options']);

                    /** insert into the change log */
                    $data['activity'] = '1';
                    $data['id'] = $userId;
                    $this->changeLog($data);
                    $this->flashMessenger()->addMessage($crmMessages['CREATE_CRM_USER_SUCCESS']);
                    $messages = array('status' => "success", 'message' => $crmMessages['CREATE_CRM_USER_SUCCESS']);
                } else {
                    $this->flashMessenger()->addMessage($crmMessages['CREATE_CRM_USER_FAIL']);
                    $messages = array('status' => "error", 'message' => $crmMessages['CREATE_CRM_USER_FAIL']);
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'form' => $form,
            'jsLangTranslate' => $crmMessages,
            'userRole' => $userRole
        ));
        return $viewModel;
    }

    /**
     * This action is used to edit crm user details
     * @param void
     * @return json
     * @author Icreon Tech - AP
     */
    public function editCrmUserAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $crmMessages = array_merge($this->_config['user_messages']['config']['common'], $this->_config['file_upload_path'], $this->_config['user_messages']['config']['crm-create-user']);
        $applicationConfigIds = $this->_config['application_config_ids'];
        $uSId = $applicationConfigIds['united_state_country_id'];
        $param = $this->params()->fromRoute();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $response = $this->getResponse();
        $messages = array();
        $crmUserPicture = '';
        $form = new CrmUserForm();
        $crmuser = new Crmuser($this->_adapter);
        $userRole = $this->getTables()->getCrmMasterUserRole();
        if (!empty($userRole)) {
            $userRoleList = array();
            foreach ($userRole as $key => $val) {
                $userRoleList[$val['role_id']] = $val['role'];
            }
            $form->get('role')->setAttribute('options', $userRoleList);
        }

        $state = $this->getServiceLocator()->get('Common\Model\CommonTable')->getStates();
        $stateList = array();
        $stateList[''] = 'Select State';
        foreach ($state as $key => $val) {
            $stateList[$val['state']] = $val['state'];
        }
        $form->get('state_select')->setAttribute('options', $stateList);


        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
        $countryList = array();
        foreach ($country as $key => $val) {
            $countryList[$val['country_id']] = $val['name'];
        }
        $form->get('country_id')->setAttribute('options', $countryList);
        if (isset($param['id']) && !empty($param['id'])) {
            $param['id'];
            $searchParam['crm_user_id'] = $this->decrypt($param['id']);
            if ($searchParam['crm_user_id'] == '') {
                return $this->redirect()->toRoute('crmusers');
            }
            $searchResult = $this->getTables()->getCrmUserDetails($searchParam);
            $roles = explode(',', $searchResult[0]['role_id']);
            if (isset($searchResult[0]['crm_email_id']) && !empty($searchResult[0]['crm_email_id'])) {
                $form->get('crm_email_id')->setAttribute('value', $searchResult[0]['crm_email_id']);
            }
            if (isset($searchResult[0]['crm_is_active']) && !empty($searchResult[0]['crm_is_active'])) {
                $form->get('status')->setAttribute('value', $searchResult[0]['crm_is_active']);
            }
            if (isset($searchResult[0]['role_id']) && !empty($searchResult[0]['role_id'])) {
                $form->get('role')->setValue($roles);
            }
            if (isset($searchResult[0]['report_crm_user_id']) && !empty($searchResult[0]['report_crm_user_id'])) {
                $form->get('report_to_id')->setAttribute('value', $searchResult[0]['report_crm_user_id']);
                $form->get('report_to')->setAttribute('value', $searchResult[0]['user_name']);
            }
            if (isset($searchResult[0]['crm_first_name']) && !empty($searchResult[0]['crm_first_name'])) {
                $form->get('first_name')->setAttribute('value', $searchResult[0]['crm_first_name']);
            }
            if (isset($searchResult[0]['crm_last_name']) && !empty($searchResult[0]['crm_last_name'])) {
                $form->get('last_name')->setAttribute('value', $searchResult[0]['crm_last_name']);
            }
            if (isset($searchResult[0]['crm_phone']) && !empty($searchResult[0]['crm_phone'])) {
                $form->get('phone_no')->setAttribute('value', $searchResult[0]['crm_phone']);
            }
            if (isset($searchResult[0]['crm_address']) && !empty($searchResult[0]['crm_address'])) {
                $form->get('address')->setAttribute('value', $searchResult[0]['crm_address']);
            }
            if (isset($searchResult[0]['crm_city']) && !empty($searchResult[0]['crm_city'])) {
                $form->get('city')->setAttribute('value', $searchResult[0]['crm_city']);
            }
            if (isset($searchResult[0]['crm_state']) && !empty($searchResult[0]['crm_state'])) {
                $form->get('state')->setAttribute('value', $searchResult[0]['crm_state']);
                $form->get('state_select')->setAttribute('value', $searchResult[0]['crm_state']);
            }
            if (isset($searchResult[0]['crm_zip_code']) && !empty($searchResult[0]['crm_zip_code'])) {
                $form->get('zip')->setAttribute('value', $searchResult[0]['crm_zip_code']);
            }
            if (isset($searchResult[0]['crm_country_id']) && !empty($searchResult[0]['crm_country_id'])) {
                $form->get('country_id')->setAttribute('value', $searchResult[0]['crm_country_id']);
            }
            if (isset($searchResult[0]['crm_user_id']) && !empty($searchResult[0]['crm_user_id'])) {
                $form->get('crm_user_id')->setAttribute('value', $searchResult[0]['crm_user_id']);
            }
            if (isset($searchResult[0]['crm_user_picture']) && !empty($searchResult[0]['crm_user_picture'])) {
                $crmUserPicture = $searchResult[0]['crm_user_picture'];
                $form->get('thumbnail_virtualface')->setAttribute('value', $searchResult[0]['crm_user_picture']);
                $form->get('old_thumbnail_virtualface')->setAttribute('value', $searchResult[0]['crm_user_picture']);
            }
            $imagPath = 'crmuser/' . $this->folderStructByUserId($searchResult[0]['crm_user_id']) . 'image/thumbnail/' . $searchResult[0]['crm_user_picture'];
        }
        if ($request->isPost()) {
            $form->setData($request->getPost());
            $form->setInputFilter($crmuser->getEditFromInputFilter($request->getPost('crm_user_id')));
            if (!$form->isValid()) {
                $errors = $form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'savecontact') {
                        foreach ($row as $rower) {
                            $msg [$key] = $crmMessages[$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $formData = $form->getData();
                $date = DATE_TIME_FORMAT;
                $formData['modified_date'] = $date;
                if ($formData['thumbnail_virtualface']) {
                    $this->moveFileFromTempToUser($formData['thumbnail_virtualface'], $formData['crm_user_id'], $formData['old_thumbnail_virtualface']); //$searchResult[0]['crm_user_picture']
                }
                $formData['state'] = ($formData['country_id'] == $uSId) ? $formData['state_select'] : $formData['state'];
                $filterParam = $crmuser->filterParam($formData);
                $lastUpdatedId = $this->getTables()->updateUser($filterParam);
                if ($lastUpdatedId) {
                    $data['activity'] = '2';
                    $data['id'] = $filterParam['crm_user_id'];
                    $this->changeLog($data);
                    $data['crm_user_id'] = $filterParam['crm_user_id'];
                    $this->getTables()->deleteCrmUserRole($data);
                    foreach ($formData['role'] as $val) {
                        $roleParam['user_id'] = $filterParam['crm_user_id'];
                        $roleParam['role_id'] = $val;
                        $this->getTables()->insertCrmUserRole($roleParam);
                    }
                    $this->flashMessenger()->addMessage($crmMessages['UPDATE_CRM_USER_SUCCESS']);
                    $messages = array('status' => "success", 'message' => $crmMessages['UPDATE_CRM_USER_SUCCESS']);
                } else {
                    $this->flashMessenger()->addMessage($crmMessages['CREATE_CRM_USER_FAIL']);
                    $messages = array('status' => "error", 'message' => $crmMessages['CREATE_CRM_USER_FAIL']);
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }

        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'form' => $form,
            'jsLangTranslate' => array_merge($crmMessages, $applicationConfigIds),
            'crm_user_id' => $searchParam['crm_user_id'],
            'crm_user_picture' => $crmUserPicture,
            'image_path' => $crmMessages['assets_url'] . $imagPath,
            'userRole' => $userRole,
            'searchResult' => $searchResult
        ));
        return $viewModel;
    }

    /**
     * This action is used to get crm user details
     * @param void
     * @return json
     * @author Icreon Tech - AP
     */
    public function getCrmUserInfoAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $param = $this->params()->fromRoute();
        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
        $searchParam['crm_user_id'] = $this->decrypt($param['id']);
        if ($searchParam['crm_user_id'] == '') {
            return $this->redirect()->toRoute('crmusers');
        }
        $searchResult = $this->getTables()->getCrmUserDetails($searchParam);
        $imagPath = 'crmuser/' . $this->folderStructByUserId($searchResult[0]['crm_user_id']) . 'image/thumbnail/' . $searchResult[0]['crm_user_picture'];

        foreach ($country as $key => $val) {
            if ($val['country_id'] == $searchResult[0]['crm_country_id']) {
                $searchResult[0]['country_name'] = $val['name'];
            }
        }
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'campaign_id' => $param['id'],
            'searchResult' => $searchResult[0],
            'jsLangTranslate' => $crmMessages = array_merge($this->_config['file_upload_path'], $this->_config['user_messages']['config']['crm-create-user']),
            'image_path' => $crmMessages['assets_url'] . $imagPath
                ,
        ));
        return $viewModel;
    }

    /**
     * This action is used to get crm users
     * @param void
     * @return json
     * @author Icreon Tech - AP
     */
    public function getCrmUsersAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $this->layout('crm');
        $searchCrmForm = new SearchCrmUserForm();
        $saveSearchForm = new SaveSearchCrmUserForm();
        $updateCrmUserForm = new UpdateCrmUserForm();
        $deleteUserForm = new DeleteUserForm();
        $saveSearch = $this->getTables()->getCrmUserSaveSearch();

        $userRole = $this->getTables()->getCrmMasterUserRole();
        $userRoleList = array();
        $userRoleList[''] = 'Select';
        foreach ($userRole as $key => $val) {
            $userRoleList[$val['role_id']] = $val['role'];
        }
        $saveSearchList[''] = 'Select';
        foreach ($saveSearch as $key => $val) {
            $saveSearchList[$val['crm_user_search_id']] = $val['title'];
        }
        $searchCrmForm->get('saved_search')->setAttribute('options', $saveSearchList);
        $searchCrmForm->get('role')->setAttribute('options', $userRoleList);
        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'searchCrmForm' => $searchCrmForm,
            'saveSearchForm' => $saveSearchForm,
            'updateCrmUserForm' => $updateCrmUserForm,
            'deleteUserForm' => $deleteUserForm,
            'messages' => $messages,
            'jsLangTranslate' => $this->_config['user_messages']['config']['crm-search_contact']
        ));
        return $viewModel;
    }

    /**
     * This action is used to search crm users 
     * @param void
     * @return json
     * @author Icreon Tech - AP
     */
    public function getSearchCrmUsersAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        parse_str($request->getPost('searchString'), $searchParam);
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        $crmMessages = $this->_config['user_messages']['config']['crm-search_contact'];
        $searchParam['name_email'] = isset($searchParam['name_email']) ? $searchParam['name_email'] : '';
        $searchParam['role'] = isset($searchParam['role']) ? $searchParam['role'] : '';
        $searchParam['status'] = isset($searchParam['status']) ? $searchParam['status'] : '1';

        $isExport = $request->getPost('export');
        if ($isExport != "" && $isExport == "excel") {
            $chunksize = $this->_config['export']['chunk_size'];
            $export_limit = $this->_config['export']['export_limit'];

            $searchParam['recordLimit'] = $chunksize;
        }

        $searchParam['startIndex'] = $start;
        $searchParam['recordLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');
        $searchParam['currentTime'] = DATE_TIME_FORMAT;

        $page_counter = 1;
        $number_of_pages = 1;
        do {
            $searchResult = $this->getTables()->searchCrmUser($searchParam);
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $total = $countResult[0]->RecordCount;

            if ($isExport == "excel") {
                $filename = "crm_user_export_" . EXPORT_DATE_TIME_FORMAT . ".csv";
                if ($total > $chunksize) {
                    $number_of_pages = ceil($total / $chunksize);
                    $page_counter++;
                    $start = $chunksize * $page_counter - $chunksize;
                    $searchParam['startIndex'] = $start;
                    $searchParam['page'] = $page_counter;
                } else {
                    $number_of_pages = 1;
                    $page_counter++;
                }
            } else {
                $jsonResult['page'] = $page;
                $jsonResult['records'] = $countResult[0]->RecordCount;
                $jsonResult['total'] = @ceil($countResult[0]->RecordCount / $limit);
            }
        } while ($page_counter <= $number_of_pages && $isExport == "excel");



        if (count($searchResult) > 0) {
            $arrExport = array();
            foreach ($searchResult as $val) {
//                $cur_date_time = date("Y-m-d H:i", strtotime(DATE_TIME_FORMAT));
//                $loginDate = date("Y-m-d H:i", strtotime($val['lastlogin']));
//                $roundDate = round((strtotime($cur_date_time) - strtotime($loginDate)) / 60);
//                $date = floor($roundDate / 1440);
//                $hours = floor(($roundDate - $date * 1440) / 60);
//                $minutes = $roundDate - ($date * 1440) - ($hours * 60);
                $dateDiff = $val['lastlogin'];
//                  $minutes = $loginDate%60;
//                  $loginDate = $loginDate/60;
//                  $hours = $loginDate%60;
//                  $loginDate = $loginDate/60;
//                  $date = $loginDate%24;
//			if($date>0)
//                
//                $dateDiff = $date.' days '. $hours . ' hours ' . $minutes . ' minutes';
//				else
//				$dateDiff = $hours . ' hours ' . $minutes . ' minutes';
                //$dateDiff = $hours . ' hours ' . $minutes . ' minutes';
                $arrCell['id'] = $val['crm_user_id'];
                $encrypt_id = $this->encrypt($val['crm_user_id']);
                $view = '<a href="/get-crmuser-detail/' . $encrypt_id . '/' . $this->encrypt('view') . '" class="view-icon"><div class="tooltip">View<span></span></div></a>';

                $edit = '<a class="edit-icon" href="/get-crmuser-detail/' . $encrypt_id . '/' . $this->encrypt('edit') . '"><div class="tooltip">Edit<span></span></div></a>';
                if ($val['crm_is_active'] == '1') {
                    $status = $crmMessages['L_ACTIVE'];
                } else {
                    $status = $crmMessages['L_BLOCKED'];
                }
                if ($isExport == "excel") {
                    $arrExport[] = array("Name" => $val['crm_user_id'], "Name" => (!empty($val['username']) ? stripslashes($val['username']) : 'N/A'), "Status" => (!empty($status) ? stripslashes($status) : 'N/A'), "Role" => (!empty($val['role_name']) ? $val['role_name'] : 'N/A'), "Last Access" => (!empty($val['lastlogin']) ? $dateDiff : 'N/A'));
                }
                $arrCell['cell'] = array($val['crm_user_id'], !empty($val['username']) ? stripslashes($val['username']) : 'N/A', !empty($status) ? stripslashes($status) : 'N/A', !empty($val['role_name']) ? $val['role_name'] : 'N/A', !empty($val['lastlogin']) ? $dateDiff : 'N/A', $view . $edit);
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
            if ($isExport == "excel") {
                $this->arrayToCsv($arrExport, $filename, $page_counter);
            }
        } else {
            $jsonResult['rows'] = array();
        }
        if ($isExport == "excel") {
            $this->downloadSendHeaders($filename);
            die;
        } else {
            return $response->setContent(\Zend\Json\Json::encode($jsonResult));
        }
    }

    /**
     * This Action is used for save seach  the crm users  record
     * @param array $saveSearchParam
     * @return comfirmation message
     * @author Icreon Tech -AP
     */
    public function saveCrmUserSearchAction() {
        $this->checkUserAuthentication();
        $this->getTables();
        $this->getConfig();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $crmMessages = $this->_config['user_messages']['config']['crm-search_contact'];
        $crmUserObj = new Crmuser($this->_adapter);
        if ($request->isPost()) {
            parse_str($request->getPost('searchString'), $searchParam);
            $searchName = $request->getPost('search_name');
            $searchParam['search_title'] = $searchName;
            $searchParam = $crmUserObj->getInputFilterCrmSaveSearch($searchParam);
            if (trim($searchName) != '') {
                $saveSearchParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $saveSearchParam['isActive'] = 1;
                $saveSearchParam['isDelete'] = 0;
                $saveSearchParam['search_name'] = $searchName;
                $saveSearchParam['search_query'] = serialize($searchParam);
                if ($request->getPost('search_id') != '') {
                    $saveSearchParam['modified_date'] = DATE_TIME_FORMAT;
                    $saveSearchParam['search_id'] = $request->getPost('search_id');
                    if ($this->getTables()->updateCrmUserSearch($saveSearchParam) == true) {
                        $messages = array('status' => "success");
                        $this->flashMessenger()->addMessage($crmMessages['UPDATE_SUCCESS_MSG']);
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                } else {
                    $saveSearchParam['added_date'] = DATE_TIME_FORMAT;
                    if ($this->getTables()->saveCrmUserSearch($saveSearchParam) == true) {
                        $messages = array('status' => "success");
                        $this->flashMessenger()->addMessage($crmMessages['SAVE_SUCCESS_MSG']);
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                }
            }
        }
    }

    /**
     * This Action is used for delete crm user search
     * @param array
     * @return Json responce
     * @author Icreon Tech -AP
     */
    public function deleteCrmUserSearchAction() {
        $this->checkUserAuthentication();
        $this->getTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam['search_id'] = $request->getPost('search_id');
            $dataParam['isDelete'] = 1;
            $dataParam['modified_date'] = DATE_TIME_FORMAT;
            $this->getTables()->deleteCrmUserSearch($dataParam);
            $this->flashMessenger()->addMessage('Search is deleted successfully!');
            $messages = array('status' => "success");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This Action is used for edit crm user status
     * @param array
     * @return Json responce
     * @author Icreon Tech -AP
     */
    public function editCrmUserStatusAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $this->layout('popup');
        $request = $this->getRequest();
        $response = $this->getResponse();
        $param = $this->params()->fromRoute();
        $messages = array();
        $crmUserObj = new Crmuser($this->_adapter);
        $crmMessages = array_merge($this->_config['user_messages']['config']['common'], $this->_config['user_messages']['config']['crm-search_contact']);
        $deleteUserForm = new DeleteUserForm();
        if (isset($param['ids'])) {
            $userName = array();
            $userIds = array();
            $searchResult = $this->getTables()->searchCrmUsersById($param);
            foreach ($searchResult as $val) {
                $userName[] = $val['username'];
                $userIds[] = $val['crm_user_id'];
            }

            $crmUserName = implode(', ', $userName);
            $crmuserIds = implode(',', $userIds);
        }
        if ($request->isPost()) {
            $formData = $request->getPost();
            if (isset($formData['activity'])) {
                $date = DATE_TIME_FORMAT;
                $formData['modified_date'] = $date;
                $filterParam = $crmUserObj->filterUpdatedUserParam($formData);
                $lastUpdatedId = $this->getTables()->updateUsers($filterParam);
                if ($lastUpdatedId) {
                    if ($filterParam['activity'] == 'block') {
                        $this->flashMessenger()->addMessage($crmMessages['CRM_USER_BLOCKED']);
                        $messages = array('status' => "success", 'message' => $crmMessages['CRM_USER_BLOCKED']);
                    } else if ($filterParam['activity'] == 'active') {
                        $this->flashMessenger()->addMessage($crmMessages['CRM_USER_UNBLOCKED']);
                        $messages = array('status' => "success", 'message' => $crmMessages['CRM_USER_UNBLOCKED']);
                    }
                } else {
                    $messages = array('status' => "error", 'message' => $crmMessages['CRM_USER_UNBLOCKED']);
                }
            } else {
                $dataParam['assigned_to_id'] = $formData['assigned_to_id'];
                $dataParam['user_id'] = $formData['user_id'];
                $return = $this->getTables()->updateCrmUserAssignedTo($dataParam);
                if ($return) {
                    $this->flashMessenger()->addMessage($crmMessages['L_DELETE_USER']);
                    $messages = array('status' => 'success');
                }
            }
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
        $deleteUserForm->get('user_id')->setAttribute('value', $crmuserIds);

        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'deleteUserForm' => $deleteUserForm,
            'jsLangTranslate' => $crmMessages,
            'crmUserName' => $crmUserName
        ));
        return $viewModel;
    }

    /**
     * This Action is used to get save search record 
     * @param search_id
     * @return array
     * @author Icreon Tech -AP
     */
    public function getCrmUserSearchInfoAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $param = $request->getPost();
            $searchData['search_id'] = $param['search_id'];
            if ($searchData['search_id'] != '') {
                $saveSearchResult = $this->getTables()->getCrmUserSaveSearch($searchData);
                $searchResult = json_encode(unserialize($saveSearchResult[0]['search_query']));
                return $response->setContent($searchResult);
            }
        }
    }

    /**
     * This action is used to upload file
     * @param void
     * @return json
     * @author Icreon Tech - AP
     */
    public function uploadCrmUserProfileAction() {
        $fileExt = $this->GetFileExt($_FILES['virtualface']['name']);
        $filename = $this->GenerateMediaFileName($fileExt[1]);    /* Generate file name */
        $_FILES['virtualface']['name'] = $filename;                 /* assign name to file variable */
        $this->getConfig();
        $uploadFilePath = $this->_config['file_upload_path'];   /* get file upload configuration */
        $errorMessages = array(
            'post_max_size' => 'The uploaded file exceeds the post_max_size directive in php.ini', /* Set error messages */
            'max_file_size' => 'File is too big',
            'min_file_size' => 'File is too small',
            'accept_file_types' => 'Filetype not allowed',
        );
        $options = array(
            'upload_dir' => $uploadFilePath['temp_upload_dir'],
            'param_name' => 'virtualface', /* file input name                      Set configuration */
            'inline_file_types' => $uploadFilePath['file_types_allowed'],
            'accept_file_types' => $uploadFilePath['file_types_allowed'],
            'max_file_size' => $uploadFilePath['max_allowed_file_size'],
            'min_file_size' => $uploadFilePath['min_allowed_file_size']
        );
        $uploadHandler = new UploadHandler($options, true, $errorMessages);

        $fileResponse = $uploadHandler->jsonResponceData;

        if (isset($fileResponse['virtualface'][0]->error)) {
            $messages = array('status' => "error", 'message' => $fileResponse['virtualface'][0]->error);
        } else {
            $messages = array(
                'status' => "success",
                'message' => 'Image uploaded',
                'filename' => $fileResponse['virtualface'][0]->name
            );
        }
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This action is used to move file from temp to campaign folder
     * @param 1 for add , 2 for edit
     * @return json
     * @author Icreon Tech - AP
     */
    public function moveFileFromTempToUser($filename, $crmUerId, $oldFile) {
        $this->getTables();
        $temp_dir = $this->_config['file_upload_path']['temp_upload_dir'];
        $temp_thumbnail_dir = $this->_config['file_upload_path']['temp_upload_thumbnail_dir'];
        $crm_user_dir = $this->_config['file_upload_path']['assets_upload_dir'] . "crmuser/";
        $crm_user_image = 'image/';
        $crm_user_img_thumb = 'thumbnail/';
        $folder_struc_by_crm_user = $this->FolderStructByUserId($crmUerId);
        $crm_user_image_dir = $crm_user_dir . $folder_struc_by_crm_user . $crm_user_image;
        $crm_user_thumb_image_dir = $crm_user_dir . $folder_struc_by_crm_user . $crm_user_image . $crm_user_img_thumb;
        if (!is_dir($crm_user_image_dir)) {
            mkdir($crm_user_image_dir, 0777, TRUE);
        }
        if (!is_dir($crm_user_thumb_image_dir)) {
            mkdir($crm_user_thumb_image_dir, 0777, TRUE);
        }
        $temp_file = $temp_dir . $filename;
        $temp_thumbnail_file = $temp_thumbnail_dir . $filename;
        $crm_user_filename = $crm_user_image_dir . $filename;
        $crm_user_thumbnail_filename = $crm_user_thumb_image_dir . $filename;
        if (file_exists($temp_file)) {
            if (copy($temp_file, $crm_user_filename)) {
                unlink($temp_file);
            }
        }
        if (file_exists($temp_thumbnail_file)) {
            if (copy($temp_thumbnail_file, $crm_user_thumbnail_filename)) {
                unlink($temp_thumbnail_file);
            }
        }

        if ($oldFile) {
            if ($oldFile != $filename) {
                $old_crm_user_image_dir = $crm_user_dir . $folder_struc_by_crm_user . $oldFile;
                $old_crm_user_thumb_image_dir = $crm_user_dir . $folder_struc_by_crm_user . $crm_user_img_thumb . $oldFile;
                if (file_exists($old_crm_user_image_dir)) {
                    unlink($old_crm_user_image_dir);
                }
                if (file_exists($old_crm_user_thumb_image_dir)) {
                    unlink($old_crm_user_thumb_image_dir);
                }
            }
        }
    }

    /**
     * This action is used to add crm user roles 
     * @param array
     * @return array
     * @author Icreon Tech-AP
     */
    public function addCrmUserMasterRoleAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $this->layout('crm');
        $request = $this->getRequest();
        $userRoleForm = new UserRoleForm();
        $userroles = new Crmuser($this->_adapter);
        $roleMessages = array_merge($this->_config['user_messages']['config']['common'], $this->_config['user_messages']['config']['crm-user-role']);
        $viewModel = new ViewModel();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $userRoleForm->setInputFilter($userroles->getRoleInputFilter());
            $userRoleForm->setData($request->getPost());
            if (!$userRoleForm->isValid()) {
                $errors = $userRoleForm->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'savecontact') {
                        foreach ($row as $rower) {
                            $msg [$key] = $roleMessages[$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $formData = $userRoleForm->getData();
                $date = DATE_TIME_FORMAT;
                $formData['add_date'] = $date;
                $filterParam = $userroles->filterRoleParam($formData);
                $roleId = $this->getTables()->insertUserRole($filterParam);
                if ($roleId) {
                    $this->flashMessenger()->addMessage($roleMessages['CREATE_USER_ROLE_SUCCESS']);

                    $messages = array(
                        'status' => "success",
                        'message' => $roleMessages['CREATE_USER_ROLE_SUCCESS']
                    );
                } else {
                    $this->flashMessenger()->addMessage($roleMessages['CREATE_USER_ROLE_FAIL']);

                    $messages = array(
                        'status' => "error",
                        'message' => $roleMessages['CREATE_USER_ROLE_FAIL']
                    );
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $flashMessages = array();
        if ($this->_flashMessage->hasMessages()) {
            $flashMessages = $this->_flashMessage->getMessages();
        }
        $viewModel->setVariables(array(
            'form' => $userRoleForm,
            'jsLangTranslate' => $roleMessages,
            'messages' => $flashMessages
        ));
        return $viewModel;
    }

    /**
     * This action is used to edit master user roles 
     * @param role_id and role_name
     * @return Json 
     * @author Icreon Tech-AP
     */
    public function editCrmUserMasterRoleAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $postData = array();
            $date = DATE_TIME_FORMAT;
            $postData['role_id'] = $request->getPost('ID');
            $postData['role'] = $request->getPost('Role_Name');
            $postData['modified_date'] = $date;
            $this->getTables()->updateUserRole($postData);
            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This action is used list crm user roles in grid
     * @return json
     * @author Icreon Tech-AP
     */
    public function getCrmUserMasterRoleAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        $roleMessages = $this->_config['user_messages']['config']['crm-user-role'];
        $searchParam['startIndex'] = $start;
        $searchParam['recordLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');
        $searchResult = $this->getTables()->searchCrmUserRoles($searchParam);
        $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
        $jsonResult = array();
        $arrCell = array();
        $subArrCell = array();
        $jsonResult['page'] = $page;
        $jsonResult['records'] = $countResult[0]->RecordCount;
        $jsonResult['total'] = @ceil($countResult[0]->RecordCount / $limit);

        if (count($searchResult) > 0) {
            foreach ($searchResult as $val) {
                $arrCell['id'] = $val['role_id'];
                $permisions = "<a class='' href='/get-crm-user-role-permission/" . $this->encrypt($val['role_id']) . "'>Permissions</a>";
                $delete = "<a class='delete_role delete-icon' href='#delete_role' onclick=deleteRole('" . $this->encrypt($val['role_id']) . "')><div class='tooltip'>Delete<span></span></div></a>";
                $arrCell['cell'] = array($val['role_id'], !empty($val['role']) ? stripslashes($val['role']) : 'N/A', $delete . '   ' . $permisions);
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
        } else {
            $arrCell['cell'] = array($roleMessages ['NO_RECORD_FOUND']);
            $subArrCell[] = $arrCell;
            $jsonResult['rows'] = $subArrCell;
        }
        return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    /**
     * This Action is used to delete the role
     * @param pass an array $dataParam
     * @return confirmation message in json
     * @author Icreon Tech - AP
     */
    public function deleteCrmUserMasterRoleAction() {
        $this->checkUserAuthentication();
        $this->getTables();
        $this->getConfig();
        $roleMessages = $this->_config['user_messages']['config']['crm-user-role'];
        $request = $this->getRequest();

        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam['role_id'] = $this->decrypt($request->getPost('role_id'));
            $this->getTables()->deleteCrmUserMasterRole($dataParam);
            $this->flashMessenger()->addMessage($roleMessages['DELETE_CONFIRM']);
            $messages = array('status' => "success");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This action is used to show all crm user detail
     * @return     array
     * @author Icreon Tech - AP
     */
    public function getCrmuserDetailAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getConfig();
        $this->getTables();
        $crmUserMsg = array_merge($this->_config['user_messages']['config']['common'], $this->_config['user_messages']['config']['crm-create-user']);
        $params = $this->params()->fromRoute();
        $searchParam['crm_user_id'] = $this->decrypt($params['id']);

        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'crm_user_id' => $searchParam['crm_user_id'],
            'mode' => $this->decrypt($params['mode']),
            'id' => $params['id'],
            'moduleName' => $this->encrypt('crmuser'),
            'jsLangTranslate' => $crmUserMsg
        ));
        return $viewModel;
    }

    /**
     * This action is used to get save search details
     * @param   void
     * @return  array
     * @author  Icreon Tech - AP
     */
    public function getSavedCrmUserSearchSelectAction() {
        $this->checkUserAuthentication();
        $this->getTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $saveSearch = $this->getTables()->getCrmUserSaveSearch();
            $options = '';
            // $options .="<option value=''>" . $productMsg['SELECT'] . "</option>";
            foreach ($saveSearch as $val) {
                $options .="<option value='" . $val['crm_user_search_id'] . "'>" . $val['title'] . "</option>";
            }
            return $response->setContent($options);
        }
    }

    /**
     * This function is used to insert into the change log
     * @param  array
     * @return void
     * @author Icreon Tech - AP
     */
    public function changeLOg($param = array()) {
        $changeLogArray = array();
        $changeLogArray['table_name'] = 'tbl_crm_user_change_logs';
        $changeLogArray['activity'] = $param['activity'];/** 1 == for insert,2 == for update, 3 == for delete */
        $changeLogArray['id'] = $param['id'];
        $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
        $changeLogArray['added_date'] = DATE_TIME_FORMAT;
        $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
    }

    /**
     * This function is used to list my assignments
     * @param  array
     * @return void
     * @author Icreon Tech - AP
     */
    public function getCrmUserAssignmentAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getConfig();
        $this->getTables();
        $crmMessages = $this->_config['user_messages']['config']['crm-my-assignments'];

        $searchForm = new SearchAssignmentForm();
        $saveSearchForm = new SaveSearchCrmUserForm();
        $getActivityType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getActivityType();
        $activityType = array();
        $activityType[''] = 'Select';
        if ($getActivityType !== false) {
            foreach ($getActivityType as $key => $val) {
                $activityType[$val['activity_type_id']] = $val['activity_type'];
            }
        }
        $searchForm->get('activity_type')->setAttribute('options', $activityType);
        $getActivityFollowupTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getActivitySourceTypes();
        $activityFollowupTypes = array();
        $activityFollowupTypes[''] = 'Select';
        if ($getActivityFollowupTypes !== false) {
            foreach ($getActivityFollowupTypes as $key => $val) {
                $activityFollowupTypes[$val['activity_source_type_id']] = $val['activity_source_type'];
            }
        }
        $searchForm->get('source')->setAttribute('options', $activityFollowupTypes);
        $saveSearch = array();
        $saveSearch[''] = 'Select';
        $saveSearchRecord = $this->getTables()->getCrmUserAssignmentSaveSearch();
        if ($saveSearchRecord) {
            foreach ($saveSearchRecord as $key => $val) {
                $saveSearch[$val['assign_search_id']] = $val['title'];
            }
        }
        $searchForm->get('saved_search')->setAttribute('options', $saveSearch);
        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'jsLangTranslate' => $crmMessages,
            'searchForm' => $searchForm,
            'saveSearchForm' => $saveSearchForm,
            'messages' => $messages
        ));
        return $viewModel;
    }

    /**
     * This function is used to search my assignments
     * @param  array
     * @return Json
     * @author Icreon Tech - AP
     */
    public function getCrmUserAssignmentSearchAction() {
        $this->checkUserAuthentication();
        $params = $this->params()->fromRoute();
        $this->getConfig();
        $this->getTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        $crmAssignmentMessages = $this->_config['user_messages']['config']['crm-my-assignments'];
        parse_str($request->getPost('searchString'), $searchParam);

        $dashlet = $request->getPost('crm_dashlet');
        $dashletSearchId = $request->getPost('searchId');
        $dashletId = $request->getPost('dashletId');
        $dashletSearchType = $request->getPost('searchType');
        $dashletSearchString = $request->getPost('dashletSearchString');
        if (isset($dashletSearchString) && $dashletSearchString != '') {
            $searchParam = unserialize($dashletSearchString);
        }

        $isExport = $request->getPost('export');
        if ($isExport != "" && $isExport == "excel") {
            $chunksize = $this->_config['export']['chunk_size'];
            $export_limit = $this->_config['export']['export_limit'];

            $searchParam['recordLimit'] = $chunksize;
        }

        $searchParam['startIndex'] = $start;
        $searchParam['recordLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');



        if (isset($searchParam['scheduled']) && $searchParam['scheduled'] != 1 && $searchParam['scheduled'] != '') {
            $searchParam['dateType'] = $this->getDateRange($searchParam['scheduled']);
            $searchParam['from'] = $this->DateFormat($date_range['from'], 'db_date_format_from');
            $searchParam['to'] = $this->DateFormat($date_range['to'], 'db_date_format_to');
        } else if (isset($searchParam['scheduled']) && $searchParam['scheduled'] == '') {
            $searchParam['from'] = '';
            $searchParam['to'] = '';
        } else {
            if (isset($searchParam['from']) && $searchParam['from'] != '') {
                $searchParam['from'] = $this->DateFormat($searchParam['from'], 'db_date_format_from');
            }
            if (isset($searchParam['to']) && $searchParam['to'] != '') {
                $searchParam['to'] = $this->DateFormat($searchParam['to'], 'db_date_format_to');
            }
        }


        if (isset($searchParam['added_date_range']) && $searchParam['added_date_range'] != 1 && $searchParam['added_date_range'] != '') {
            $date_range = $this->getDateRange($searchParam['added_date_range']);
            $searchParam['from'] = date("Y-m-d H:m:s", strtotime($date_range['from']));
            $searchParam['to'] = date("Y-m-d H:m:s", strtotime($date_range['to']));
            $searchParam['scheduled'] = $searchParam['added_date_range'];
        } else if (isset($searchParam['added_date_range']) && $searchParam['added_date_range'] == '') {
            $searchParam['from'] = '';
            $searchParam['to'] = '';
            $searchParam['scheduled'] = $searchParam['added_date_range'];
        } else {
            if (isset($searchParam['added_date_from']) && $searchParam['added_date_from'] != '') {
                $searchParam['from'] = date("Y-m-d H:m:s", strtotime($searchParam['added_date_from']));
                $searchParam['scheduled'] = $searchParam['added_date_range'];
            }
            if (isset($searchParam['added_date_to']) && $searchParam['added_date_to'] != '') {
                $searchParam['to'] = date("Y-m-d H:m:s", strtotime($searchParam['added_date_to']));
                $searchParam['scheduled'] = $searchParam['added_date_range'];
            }
        }

        $searchParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;

        $page_counter = 1;
        $number_of_pages = 1;
        do {

            $dashletResult = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getTableStruct(array('user_id' => $this->auth->getIdentity()->crm_user_id, 'search_id' => $dashletSearchId, 'dash_id' => $dashletId, 'search_type' => $dashletSearchType));
            $newExplodedColumnName = array();
            if (!empty($dashletResult)) {
                $explodedColumnName = explode(",", $dashletResult[0]['table_column_names']);
                foreach ($explodedColumnName as $val) {
                    if (strpos($val, ".")) {
                        $newExplodedColumnName[] = trim(strstr($val, ".", false), ".");
                    } else {
                        $newExplodedColumnName[] = trim($val);
                    }
                }
            }
            $record = $this->getTables()->getCrmUserAssignments($searchParam);
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $total = $countResult[0]->RecordCount;
            if ($isExport == "excel") {
                $filename = "my_assignment_" . EXPORT_DATE_TIME_FORMAT . ".csv";
                if ($total > $chunksize) {
                    $number_of_pages = ceil($total / $chunksize);
                    $page_counter++;
                    $start = $chunksize * $page_counter - $chunksize;
                    $searchParam['startIndex'] = $start;
                    $searchParam['page'] = $page_counter;
                } else {
                    $number_of_pages = 1;
                    $page_counter++;
                }
            } else {
                $page_counter = $page;
                $jsonResult['page'] = $page;
                $jsonResult['records'] = $countResult[0]->RecordCount;
                $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
            }
        } while ($page_counter <= $number_of_pages && $isExport == "excel");
        if ($record > 0) {
            $arrExport = array();
            foreach ($record as $val) {
                $dashletCell = array();
                $link = '';
                if ($val['module'] == 'case') {
                    $link = "/get-case-detail/";
                } else if ($val['module'] == 'pledges') {
                    $link = "/get-pledge-info/";
                } else if ($val['module'] == 'lead') {
                    $link = "/view-lead/";
                } else if ($val['module'] == 'activity') {
                    $link = "/get-activity-detail/";
                }
                $view = "<a class='view-icon' href='" . $link . $this->encrypt($val['source_id']) . "/" . $this->encrypt('view') . "'><div class='tooltip'>View<span></span></div></a>";

                $dashletSource = array_search('source', $newExplodedColumnName);
                if ($dashletSource !== false)
                    $dashletCell[$dashletSource] = "<a class='txt-decoration-underline' href='" . $link . $this->encrypt($val['source_id']) . "/" . $this->encrypt('view') . "'>" . $val['source'] . "</a>";

                $dashletUserWith = array_search('user_with', $newExplodedColumnName);
                if ($dashletUserWith !== false)
                    $dashletCell[$dashletUserWith] = $val['user_with'];

                $dashletSubject = array_search('subject', $newExplodedColumnName);
                if ($dashletSubject !== false)
                    $dashletCell[$dashletSubject] = $val['subject'];

                $dashletActivityType = array_search('activity_type', $newExplodedColumnName);
                if ($dashletActivityType !== false)
                    $dashletCell[$dashletActivityType] = $val['activity_type'];

                $scheduledDate = !empty($val['scheduled_date']) ? $this->DateFormat($val['scheduled_date']) : 'N/A';
                $dashletscheduledDate = array_search('scheduled_date', $newExplodedColumnName);
                if ($dashletscheduledDate !== false){
                  $dashletCell[$dashletscheduledDate] = $this->OutputDateFormat($scheduledDate, 'dateformatampm');  
                }
                if (isset($dashlet) && $dashlet == 'dashlet') {
                    $arrCell['cell'] = $dashletCell;
                } else if ($isExport == "excel") {
                    $arrExport[] = array("Source" => (!empty($val['source']) ? ucfirst($val['source']) : 'N/A'), "Type" => (!empty($val['activity_type']) ? $val['activity_type'] : 'N/A'), "Subject" => (!empty($val['subject']) ? $val['subject'] : 'N/A'), "With" => (!empty($val['user_with']) ? $val['user_with'] : 'N/A'), "Scheduled On" => (!empty($val['scheduled_date']) ? $this->OutputDateFormat($val['scheduled_date'], 'dateformatampm') : 'N/A'));
                } else {
                    $arrCell['cell'] = array(!empty($val['source']) ? ucfirst($val['source']) : 'N/A', !empty($val['activity_type']) ? $val['activity_type'] : 'N/A', !empty($val['subject']) ? $val['subject'] : 'N/A', !empty($val['user_with']) ? $val['user_with'] : 'N/A', !empty($val['scheduled_date']) ? $this->OutputDateFormat($val['scheduled_date'], 'dateformatampm') : 'N/A', $view);
                }

                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
            if ($isExport == "excel") {
                $this->arrayToCsv($arrExport, $filename, $page_counter);
            }
        } else {
            $jsonResult['rows'] = array();
        }
        if ($isExport == "excel") {
            $this->downloadSendHeaders($filename);
            die;
        } else {
            return $response->setContent(\Zend\Json\Json::encode($jsonResult));
        }
    }

    /**
     * This Action is used for save seach  the my assignments
     * @param array $saveSearchParam
     * @return comfirmation message
     * @author Icreon Tech -AP
     */
    public function addCrmUserAssignmentSearchAction() {
        $this->checkUserAuthentication();
        $this->getTables();
        $this->getConfig();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $crmUserObj = new Crmuser($this->_adapter);
        $crmMessages = $this->_config['user_messages']['config']['crm-my-assignments'];
        if ($request->isPost()) {
            parse_str($request->getPost('searchString'), $searchParam);
            $searchName = $request->getPost('search_name');
            $searchParam['search_title'] = $searchName;
            $searchParam = $crmUserObj->getInputFilterCrmSaveSearch($searchParam);
            if (trim($searchName) != '') {
                $saveSearchParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $saveSearchParam['isActive'] = 1;
                $saveSearchParam['isDelete'] = 0;
                $saveSearchParam['search_name'] = $searchName;
                $saveSearchParam['search_query'] = serialize($searchParam);
                if ($request->getPost('search_id') != '') {
                    $saveSearchParam['modified_date'] = DATE_TIME_FORMAT;
                    $saveSearchParam['search_id'] = $request->getPost('search_id');
                    if ($this->getTables()->updateCrmUserAssignmentSearch($saveSearchParam) == true) {
                        $messages = array('status' => "success");
                        $this->flashMessenger()->addMessage($crmMessages['UPDATE_SUCCESS_MSG']);
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                } else {
                    $saveSearchParam['added_date'] = DATE_TIME_FORMAT;
                    if ($this->getTables()->saveCrmUserAssignments($saveSearchParam) == true) {
                        $messages = array('status' => "success");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                }
            }
        }
    }

    /**
     * This Action is used to get save search assignments record 
     * @param search_id
     * @return array
     * @author Icreon Tech -AP
     */
    public function getCrmUserAssignmentSearchInfoAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $param = $request->getPost();
            $searchData['search_id'] = $param['search_id'];
            if ($searchData['search_id'] != '') {
                $saveSearchResult = $this->getTables()->getCrmUserAssignmentSaveSearch($searchData);
                $searchResult = json_encode(unserialize($saveSearchResult['0']['search_query']));
                return $response->setContent($searchResult);
            }
        }
    }

    /**
     * This Action is used for delete crm user assignment search
     * @return Json responce
     * @author Icreon Tech -AP
     */
    public function deleteCrmUserAssignmentSearchAction() {
        $this->checkUserAuthentication();
        $this->getTables();
        $this->getConfig();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam['search_id'] = $request->getPost('search_id');
            $dataParam['isDelete'] = 1;
            $dataParam['modified_date'] = DATE_TIME_FORMAT;
            $this->getTables()->deleteCrmUserAssignmentSearch($dataParam);
            $this->flashMessenger()->addMessage('Search is deleted successfully!');
            $messages = array('status' => "success");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This action is used to get save search details
     * @return     array
     * @author Icreon Tech - AP
     */
    public function getSavedCrmUserAssignmentSearchSelectAction() {
        $this->checkUserAuthentication();
        $this->getTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {

            $saveSearchRecord = $this->getTables()->getCrmUserAssignmentSaveSearch();
            $options = '';
            foreach ($saveSearchRecord as $val) {
                $options .="<option value='" . $val['assign_search_id'] . "'>" . $val['title'] . "</option>";
            }
            return $response->setContent($options);
        }
    }

    /**
     * This action is used to change crm user pasword
     * @param array
     * @return     array
     * @author Icreon Tech - AP
     */
    public function changeCrmUserPasswordAction() {
        $this->checkUserAuthentication();
        $this->getTables();
        $this->getConfig();
        $this->layout('popup');
        $crmUserObj = new Crmuser($this->_adapter);
        $form = new CrmUserChangePasswordForm();
        $viewModel = new ViewModel();
        $crmMessages = array_merge($this->_config['user_messages']['config']['common'], $this->_config['user_messages']['config']['crm-create-user']);
        $param = $this->params()->fromRoute();
        if (isset($param['id'])) {
            $crm_user_id = $param['id'];
            $form->get('crm_user_id')->setAttribute('value', $crm_user_id);
        }
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $form->setInputFilter($crmUserObj->getPasswordInputFilter());
            $form->setData($request->getPost());
            if (!$form->isValid()) {
                $errors = $form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submitbutton') {
                        foreach ($row as $rower) {
                            $msg [$key] = $crmMessages[$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $formData = $form->getData();
                $date = DATE_TIME_FORMAT;
                $formData['modified_date'] = $date;
                $formData['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                $filterParam = $crmUserObj->exchangeArray($formData);
                $result = $this->getTables()->changeCrmUserPassword($filterParam);
                if ($result) {
                    /** insert into the change log */
                    $data['activity'] = '2';
                    $data['id'] = $filterParam['crm_user_id'];
                    $this->changeLog($data);
                    $this->flashMessenger()->addMessage($crmMessages['CRM_USER_PASSWORD_CHANGED_SUCCESS']);
                    $messages = array('status' => "success", 'message' => $crmMessages['CRM_USER_PASSWORD_CHANGED_SUCCESS']);
                } else {
                    $this->flashMessenger()->addMessage($crmMessages['CRM_USER_PASSWORD_CHANGED_FAIL']);
                    $messages = array('status' => "error", 'message' => $crmMessages['CRM_USER_PASSWORD_CHANGED_FAIL']);
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $viewModel->setVariables(array(
            'form' => $form,
            'jsLangTranslate' => $crmMessages)
        );
        return $viewModel;
    }

    /**
     * This action is used to get the permission for User Roles.
     * @author Icreon Tech - SK
     * */
    public function getCrmUserRolePermissionAction() {
        if ($this->auth->hasIdentity() === false) {
            return $this->redirect()->toRoute('crm');
        }
        $this->layout('crm');
        $this->getConfig();
        $viewModel = new ViewModel();

        $request = $this->getRequest();
        $response = $this->getResponse();

        $params = $this->params()->fromRoute();
        if (isset($params['role_id']))
            $role_id = $this->decrypt($params['role_id']);
        else
            $role_id = null;

        $data = array();
        $data['role_id'] = $role_id;

        $message = $message_status = '';
        $form = new UserRolePermissionForm();

        $role_modules_actions = $this->getTables()->getCrmUserModuleActions();
        
        $role_module_action = array();
        $role_modules = array();
        $role_action = array();
        $module_parent_module = array();

        foreach ($role_modules_actions as $key => $value) {
            $role_module_action[$value['parent_module_id']][$value['module_id']][$value['action_id']] = $value['action_title'];

            $role_modules[$value['module_id']] = $value['module_name'];

            $role_action[$value['action_id']] = $value['action_title'];

            $module_parent_module[$value['module_id']] = $value['parent_module_id'];
        }


        $role_permission = $this->getTables()->getCrmUserRolePermissions($role_id);
       
        $userRoleResult = array();
        $roleSearchParam = array();
        $roleSearchParam['roleId'] = $role_id;
        $roleSearchParam['startIndex'] = 0;
        $roleSearchParam['recordLimit'] = 1;
        $roleSearchParam['sortField'] = '';
        $roleSearchParam['sortOrder'] = '';  
        if(!empty($role_id))
            $userRoleResult = $this->getTables()->searchCrmUserRoles($roleSearchParam);
        
        $module_permission = array();
        $module_action_permission = array();
        $role_module_ids = array();
        $role_module_action_ids = array();
        //ticket 73 private group
        $isPrivateGroup = 0;

        foreach ($role_permission as $key => $value) {
            $module_permission[$value['module_id']] = $value['is_module_allowed'];
            $module_action_permission[$value['module_id']][$value['action_id']] = $value['is_action_allowed'];
            $role_module_ids[$value['module_id']] = $value['role_module_id'];
            $role_module_action_ids[$value['module_id']][$value['action_id']] = $value['role_module_action_id'];
            //ticket 73 private group
            if($value['module_id'] == 3 && isset($value['is_allow_private']) && $value['is_allow_private'] == 1)
            {
              $isPrivateGroup = 1;  
            }
        }

        $elementToHide = $this->addUserPermissionFormElementsToForm($form, $role_module_action, $role_modules, $role_action, $module_permission, $module_action_permission);
        //ticket 73 private group
        if($isPrivateGroup == 1)
        {
            $form->get('Group[allow_private]')->setChecked(true);
        }
        $crmUserObj = new Crmuser($this->_adapter);
        $param['role_id'] = $role_id;
        $flag = 0;
        $crmuserdashletArray = $crmUserObj->getCrmuserdashlets($param);
        $defaultDashletsSettings = $this->getTables()->getDashletsList($crmuserdashletArray);
        if (empty($defaultDashletsSettings)) {
            $flag = 1;
            $defaultDashletsSettings = $this->getTables()->getDashletsList(array('', ''));
        }
        if ($request->isPost()) {
            if ($request->getPost('module_dashlet_submit')) {
                $message = $this->_config['user_messages']['config']['crm-user-role-permission']['PERMISSION_UPDATED_SUCCESSFULLY'];
                $message_status = 'success';
                $form_data = $request->getPost()->toArray();
                $role_id = $this->decrypt($form_data['role_id']);
                foreach ($defaultDashletsSettings as $key => $value) {
                    if ($form_data['dashlet'][$value['dashlet_id']] != '')
                        $is_allowed = $form_data['dashlet'][$value['dashlet_id']];
                    else
                        $is_allowed = 1;
                    $dashletdata['dashlet_id'] = $value['dashlet_id'];
                    $dashletdata['role_id'] = $role_id;
                    $dashletdata['is_allowed'] = $is_allowed;
                    $dashletdata['added_by'] = $this->auth->getIdentity()->crm_user_id;
                    $dashletdata['added_date'] = DATE_TIME_FORMAT;
                    $dashletdata['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                    $dashletdata['modified_date'] = DATE_TIME_FORMAT;
                    if ($value['role_dashlet_id'] == '') {
                        $this->getTables()->insertDashletSettings($dashletdata);
                    } else {
                        if ($flag == 1) {
                            $dashletdata['role_dashlet_id'] = $value['role_dashlet_id'];
                            $this->getTables()->insertDashletSettings($dashletdata);
                        } else {
                            $dashletdata['role_dashlet_id'] = $value['role_dashlet_id'];
                            $this->getTables()->updateDashletSettings($dashletdata);
                        }
                    }
                }
                $this->flashMessenger()->addMessage($message);

                $messages = array(
                    'status' => "success",
                    'message' => $message,
                );
                $dashletform = new DashletSettingForm();
                $param['role_id'] = $role_id;
                $crmuserdashletArray = $crmUserObj->getCrmuserdashlets($param);

                $defaultDashletsSettings = $this->getTables()->getDashletsList($crmuserdashletArray);
                $dashletform->get('role_id')->setAttribute('value', $this->encrypt($role_id));
                $this->addDashletPermissionFormElementsToForm($dashletform, $defaultDashletsSettings);
            } elseif ($request->getPost('module_permission_submit')) {
                //ticket 73 private group
                $isPrivateGroup = 0;
                $message = $this->_config['user_messages']['config']['crm-user-role-permission']['PERMISSION_UPDATED_SUCCESSFULLY'];
                $message_status = 'success';
                $form_data = $request->getPost()->toArray();
                $role_id = $form_data['role_id'];

                for ($i = 1; $i < 7; $i++) {
                    foreach ($form_data as $key1 => $value1) {
                        if ($key1 != 'role_id' && $key1 != 'module_permission_submit') {
                            if (!isset($form_data[$key1][$i])) {
                                $form_data[$key1][$i] = 1;
                            }
                        }
                    }
                }
                foreach ($form_data as $key => $value) {
                    if ($key != 'role_id' && $key != 'module_permission_submit') {

                        $data = array();

                        $module_id = array_search(str_replace("_", " ", $key), $role_modules);
                        //ticket 73 private group
                        if (isset($form_data[$key]['allow_private']) && $form_data[$key]['allow_private'] == 1) 
                        {
                            $allow_private = 1;
                            $isPrivateGroup = 1;
                        }
                        else
                        {
                            $allow_private = 0;
                        }
                        if (isset($form_data[$role_modules[$module_parent_module[$module_id]]]['access'])) {
                            $data['module_id'] = $module_id;
                            $data['role_id'] = $role_id;
                            $data['is_module_allowed'] = $form_data[$role_modules[$module_parent_module[$module_id]]]['access'];
                            $data['added_by'] = $this->auth->getIdentity()->crm_user_id;
                            $data['module_added_date'] = DATE_TIME_FORMAT;
                            $data['action_id'] = null;
                            $data['is_action_allowed'] = null;
                            $data['action_added_date'] = null;
                            $data['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                            $data['modified_date'] = DATE_TIME_FORMAT;
                            //ticket 73 private group
                            $data['allow_private'] = $allow_private;

                            if (empty($role_permission) || !isset($role_module_ids[$module_id])) {
                                $this->getTables()->insertCrmUserRolePermission($data);
                            } else {
                                $data['role_module_id'] = $role_module_ids[$module_id];
                                $data['module_modified_date'] = DATE_TIME_FORMAT;
                                $data['role_module_action_id'] = null;
                                $this->getTables()->updateCrmUserRolePermission($data);
                            }
                        }
                        foreach ($value as $action_id => $action_value) {
                            //ticket 73 private group
                            if ($action_id != 'access' && $action_id != 'allow_private') {
                                $data['module_id'] = $module_id;
                                $data['role_id'] = $role_id;
                                $data['is_module_allowed'] = null;
                                $data['added_by'] = $this->auth->getIdentity()->crm_user_id;
                                $data['module_added_date'] = DATE_TIME_FORMAT;
                                $data['action_id'] = $action_id;
                                $data['modified_by'] = $this->auth->getIdentity()->crm_user_id;
                                $data['modified_date'] = DATE_TIME_FORMAT;

                                $isActionAllowed = $action_value;
                                if ((isset($form_data[$role_modules[$module_parent_module[$module_id]]]['access']) && $form_data[$role_modules[$module_parent_module[$module_id]]]['access'] == 0)) {
                                    $isActionAllowed = 0;
                                }
                                $data['is_action_allowed'] = $isActionAllowed;
                                $data['action_added_date'] = DATE_TIME_FORMAT;

                                if (empty($role_permission) || !isset($role_module_action_ids[$module_id][$action_id])) {
                                    $this->getTables()->insertCrmUserRolePermission($data);
                                } else {
                                    $data['role_module_action_id'] = $role_module_action_ids[$module_id][$action_id];
                                    $data['action_modified_date'] = DATE_TIME_FORMAT;
                                    $data['role_module_id'] = null;
                                    $this->getTables()->updateCrmUserRolePermission($data);
                                }
                            }
                        }
                    }
                }
                $this->flashMessenger()->addMessage($message);

                $messages = array(
                    'status' => "success",
                    'message' => $message,
                );

                $form = new UserRolePermissionForm();

                $role_permission = $this->getTables()->getCrmUserRolePermissions($role_id);

                $module_permission = array();
                $module_action_permission = array();

                foreach ($role_permission as $key => $value) {
                    $module_permission[$value['module_id']] = $value['is_module_allowed'];
                    $module_action_permission[$value['module_id']][$value['action_id']] = $value['is_action_allowed'];
                }

                $elementToHide = $this->addUserPermissionFormElementsToForm($form, $role_module_action, $role_modules, $role_action, $module_permission, $module_action_permission);
                //ticket 73 private group
                if($isPrivateGroup == 1)
                {
                    $form->get('Group[allow_private]')->setChecked(true);
                }
                else
                {
                    $form->get('Group[allow_private]')->setChecked(false);
                }
            }
        }

        $form->get('role_id')->setAttribute('value', $role_id);

        $dashletform = new DashletSettingForm();

        $dashletform->get('role_id')->setAttribute('value', $this->encrypt($role_id));
        $this->addDashletPermissionFormElementsToForm($dashletform, $defaultDashletsSettings);

        $flashMessages = array();
        if ($this->_flashMessage->hasMessages()) {
            $flashMessages = $this->_flashMessage->getMessages();
        }

        $viewModel->setVariables(array(
            'form' => $form,
            'jsLangTranslate' => $this->_config['user_messages']['config']['crm-user-role-permission'],
            'message' => $message,
            'message_status' => $message_status,
            'role_module_action' => $role_module_action,
            'role_modules' => $role_modules,
            'role_action' => $role_action,
            'dashletform' => $dashletform,
            'defaultDashletsSettings' => $defaultDashletsSettings,
            'elementToHide' => $elementToHide,
            'userRoleResult'=>$userRoleResult
                )
        );
        return $viewModel;
    }

    /**
     * This action is used for add user role permission form element
     * @return     void
     * @param object form, array role permission
     * @author Icreon Tech - SK
     */
    public function addUserPermissionFormElementsToForm($form, $role_module_action, $role_modules, $role_action, $module_permission, $module_action_permission) {
        $elementToHide = array();
        foreach ($role_module_action as $parent_module_id => $submodules) {

            $access_element = $role_modules[$parent_module_id] . '[access]';

            if (!empty($module_permission) && isset($module_permission[$parent_module_id]))
                $module_access = $module_permission[$parent_module_id];
            else
                $module_access = 1;

            $form->add(array(
                'type' => 'Zend\Form\Element\Select',
                'name' => $access_element,
                'options' => array(
                    'value_options' => array(
                        '1' => 'Enable',
                        '0' => 'Disable',
                    ),
                ),
                'attributes' => array(
                    'id' => $access_element,
                    'value' => $module_access,
                    'class' => 'e1 select-w-100',
                )
            ));
            
            foreach ($role_action as $action_id => $action_title) {

                $parent_module_action_element = $role_modules[$parent_module_id] . '[' . $action_id . ']';

                if (!empty($module_action_permission) && isset($module_action_permission[$parent_module_id][$action_id]))
                    $parent_module_action_access = $module_action_permission[$parent_module_id][$action_id];
                else
                    $parent_module_action_access = 1;

                $form->add(array(
                    'type' => 'Zend\Form\Element\Select',
                    'name' => $parent_module_action_element,
                    'options' => array(
                        'value_options' => array(
                            '1' => 'Yes',
                            '0' => 'No',
                        ),
                    ),
                    'attributes' => array(
                        'id' => $parent_module_action_element,
                        'value' => $parent_module_action_access,
                        'class' => 'e1 select-w-100',
                    )
                ));
                $params['module_id'] = $parent_module_id;
                $params['action_id'] = $action_id;
                $getDisabledModuleActions = $this->getTables()->getCrmDisableModuleAction($params);

                if (!empty($getDisabledModuleActions) && $getDisabledModuleActions[0]['is_disabled'] == '1') {
                    $elementToHide[] = $parent_module_action_element;
                }
            }
            foreach ($submodules as $submodule_id => $submodule_name) {
                if ($submodule_id != $parent_module_id && count($submodules) > 1) {
                    //ticket 73 private group
                    if($submodule_id == 3 && $parent_module_id == 1)
                    {
                        $allow_private_name = $role_modules[$submodule_id]."[allow_private]";
                        $form->add(array(
                            'type' => 'Zend\Form\Element\Checkbox',
                            'name' => $allow_private_name,
                            'checked_value' => '1',
                            'unchecked_value' => '0',
                            'attributes' => array(
                                'class' => 'address-checkbox',
                                'id' => $allow_private_name,
                                'value' => "0"
                            )
                        ));
                    }
                    //ticket 73 private group ends
                    foreach ($submodule_name as $action_id => $action_name) {
                        $submodule_action_element = $role_modules[$submodule_id] . '[' . $action_id . ']';

                        if (!empty($module_action_permission) && isset($module_action_permission[$submodule_id][$action_id]))
                            $submodule_action_access = $module_action_permission[$submodule_id][$action_id];
                        else
                            $submodule_action_access = 1;

                        $form->add(array(
                            'type' => 'Zend\Form\Element\Select',
                            'name' => $submodule_action_element,
                            'options' => array(
                                'value_options' => array(
                                    '1' => 'Yes',
                                    '0' => 'No',
                                ),
                            ),
                            'attributes' => array(
                                'id' => $submodule_action_element,
                                'value' => $submodule_action_access,
                                'class' => 'e1 select-w-100',
                            )
                        ));
                        $params['module_id'] = $submodule_id;
                        $params['action_id'] = $action_id;
                        $getDisabledModuleActions = $this->getTables()->getCrmDisableModuleAction($params);
                        if (!empty($getDisabledModuleActions) && $getDisabledModuleActions[0]['is_disabled'] == '1') {
                            $elementToHide[] = $submodule_action_element;
                        }
                    }
                }
            }
        }
        return $elementToHide;
    }

    /**
     * This action is used to configure dashboard
     * @author Icreon Tech - DG
     * 
     */
    public function configureDashboardAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getTables();
        $this->getConfig();

        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $response = $this->getResponse();

        $userId = $this->auth->getIdentity()->crm_user_id;
        $getCrmUserAvaliableDashlets = $this->getTables()->getCrmUserAvailableDashlets(array('user_id' => $userId));
        $getCrmUserDashlets = $this->getTables()->getCrmUserDashlets(array('user_id' => $userId));

        $getCrmSavedSearches = $this->getTables()->getCrmSaveSearches(array('user_id' => $userId));
        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $this->getTables()->updateCrmUserDashlet(array('user_id' => $userId));
            $formData = $request->getPost();

            $insertData = array();
            if (!empty($formData['leftIdsArr'])) {
                foreach ($formData['leftIdsArr'] as $val) {
                    if (strpos($val, "_") !== false) {
                        $dashletValues = explode("_", $val);
                        $this->getTables()->insertCrmUserDashlets(array('visibility_dashboard' => '1', 'visibility_setting' => '1', 'user_id' => $userId, 'dashlet_id' => $dashletValues[0], 'position' => 1, 'search_id' => $dashletValues[1], 'search_type' => $dashletValues[2]));
                    } else {
                        $this->getTables()->insertCrmUserDashlets(array('visibility_dashboard' => '1', 'visibility_setting' => '1', 'user_id' => $userId, 'dashlet_id' => $val, 'position' => 1, 'search_id' => '', 'search_type' => ''));
                    }
                }
            }

            if (!empty($formData['rightIdsArr'])) {
                foreach ($formData['rightIdsArr'] as $val) {
                    if (strpos($val, "_") !== false) {
                        $dashletValues = explode("_", $val);
                        $this->getTables()->insertCrmUserDashlets(array('visibility_dashboard' => '1', 'visibility_setting' => '1', 'user_id' => $userId, 'dashlet_id' => $dashletValues[0], 'position' => 2, 'search_id' => $dashletValues[1], 'search_type' => $dashletValues[2]));
                    } else {
                        $this->getTables()->insertCrmUserDashlets(array('visibility_dashboard' => '1', 'visibility_setting' => '1', 'user_id' => $userId, 'dashlet_id' => $val, 'position' => 2, 'search_id' => '', 'search_type' => ''));
                    }
                }
            }
            $messages = array('status' => "success");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
        $dahsletIds = array();
        $dahsletSearchIds = array();
        $dahsletSearchType = array();
        if (!empty($getCrmUserDashlets)) {
            foreach ($getCrmUserDashlets as $val) {
                $dahsletIds[] = $val['dashlet_id'];
            }
            foreach ($getCrmUserDashlets as $val) {
                $dahsletSearchIds[] = $val['search_id'];
            }
            foreach ($getCrmUserDashlets as $val) {
                $dahsletSearchType[] = $val['search_type'];
            }
        }
        $viewModel->setVariables(array(
            'getCrmUserAvaliableDashlets' => $getCrmUserAvaliableDashlets,
            'dahsletIds' => $dahsletIds,
            'dahsletSearchIds' => $dahsletSearchIds,
            'dahsletSearchType' => $dahsletSearchType,
            'getCrmUserDashlets' => $getCrmUserDashlets,
            'crmSavedSearches' => $getCrmSavedSearches,
            'jsLangTranslate' => $this->_config['user_messages']['config']['configure_dashboard'])
        );
        return $viewModel;
    }

    /**
     * Dashboard page
     * @author Icreon Tech - DG
     * @param code
     * @return Array
     */
    public function dashboardAction() {
        $auth = new AuthenticationService();
        $this->auth = $auth;
        $this->checkUserAuthentication();
        $this->layout('crm');
        $viewModel = new ViewModel();
        $this->getTables();
        $this->getConfig();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $userId = $this->auth->getIdentity()->crm_user_id;
        $getCrmUserDashlets = array();

        $getCrmUserDashlets = $this->getTables()->getCrmUserDashlets(array('user_id' => $userId));
        $getCrmSavedSearches = $this->getTables()->getCrmSaveSearches(array('user_id' => $userId));
        $pendingTransaction = 0;
        $reviewQueTransaction = 0;
        $newAnnotation = 0;
        $newContact = 0;
        $newMembership = 0;
        $duePledges = 0;
        $newAssignments = 0;
        $amountStr = '';

        $monthArr = array();
        $amountArr = array();
        if (!empty($getCrmUserDashlets)) {
            foreach ($getCrmUserDashlets as $val) {
                if ($val['dashlet_id'] == 5) {
                    $searchParam['item_status'] = '1';
                    $getTrasactionProductArr = $this->getTables()->getTransactionByStatus($searchParam);
                    $searchCountResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
                    $pendingTransaction = $searchCountResult[0]->RecordCount;

                    $searchParam['item_status'] = '2';
                    $getTrasactionProductArr = $this->getTables()->getTransactionByStatus($searchParam);
                    $searchCountResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
                    $reviewQueTransaction = $searchCountResult[0]->RecordCount;

                    $searchParamAnnotation['type'] = '1';
                    $searchParamAnnotation['status'] = '0';
                    $searchParamAnnotation['date'] = DATE_TIME_FORMAT;
                    $annotationResult = $this->getTables()->getNewAnnotations($searchParamAnnotation);
                    $newAnnotation = $annotationResult[0]['new_annotation'];

                    $newContactResult = $this->getTables()->getNewContacts(array('date' => DATE_TIME_FORMAT));
                    $newContact = $newContactResult[0]['new_contact'];

                    $newMembershipResult = $this->getTables()->getNewMemberships(array('date' => DATE_TIME_FORMAT));
                    $newMembership = $newMembershipResult[0]['new_membership'];

                    $newAssignmentsResult = $this->getTables()->getNewAssignments(array('user_id' => $userId, 'date' => DATE_TIME_FORMAT));
                    $searchCountResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
                    $newAssignments = $searchCountResult[0]->RecordCount;


                    $duePledgeResult = $this->getTables()->duePledges();
                    $duePledges = $duePledgeResult[0]['due_pledge'];
                }
              if ($val['dashlet_id'] == 2) {
                    //$dateRange = $this->getDateRange(4);
                    $dateRange = $this->getDateRange(12);
                    $diifTime = $this->getTimezoneDiff($dateRange['from']);
                    $totalSeconds = $diifTime;
                    $monthlyTransactionsResult = $this->getTables()->getMonthlyTransactions(array('from' => $this->DateFormat($dateRange['from'], 'db_date_format_from'), 'to' => $this->DateFormat($dateRange['to'], 'db_date_format_to'),'totalSec'=>$totalSeconds));
                    $i =0;
                    $uniqYear = array();
                    $uniqMonthYear = array();
                    //asd($monthlyTransactionsResult,0);
                    $monthRangeArr = array();
                    $fromDate = $dateRange['from'];
                    $toDate = $dateRange['to'];
                    $start    = (new \DateTime($fromDate))->modify('first day of this month');
                    $end      = (new \DateTime($toDate))->modify('first day of this month');
                    $interval = \DateInterval::createFromDateString('1 month');
                    $period   = new \DatePeriod($start, $interval, $end);
                    foreach ($period as $dt) {
                        $monthRangeArr[$dt->format("M Y")] = "";
                        $monthRangeNameArr[] = $dt->format("M");
                    }
                    $curr_month_year = date('M Y',strtotime($this->OutputDateFormat($toDate, 'dateformatampm')));
                    if(!array_key_exists($curr_month_year,$monthRangeArr)){
                        $monthRangeArr[$curr_month_year] = "";
                        $monthRangeNameArr[] =  date('M',strtotime($this->OutputDateFormat($toDate, 'dateformatampm')));
                    }
                    //asd($monthRangeArr,0);
                    //asd($monthRangeNameArr);

                    foreach($monthlyTransactionsResult as $key => $val){
                        $monthYearData[$i]['months'] = date('M',strtotime($this->OutputDateFormat($val['transaction_date'], 'dateformatampm')));
                        $monthYearData[$i]["month_year"] = date('M Y',strtotime($this->OutputDateFormat($val['transaction_date'], 'dateformatampm')));
                        $monthYearData[$i]["transaction_amount"] = $val['transaction_amount'];
                        $monthYearData[$i]["year"] = date('Y',strtotime($this->OutputDateFormat($val['transaction_date'], 'dateformatampm')));
                       /* if(!in_array($monthYearData[$i]["year"], $uniqYear)){
                          $uniqYear[] = $monthYearData[$i]["year"] ;
                        }
                        if(!in_array($monthYearData[$i]["month_year"], $uniqMonthYear)){
                            $uniqMonthYear[] = $monthYearData[$i]["month_year"];
                        }*/
                        $i++;
                    }
                    $amountArr = array();
                    foreach($monthYearData as $key => $val){
                        /*if(!empty($amountArr[$val["month_year"]])){
                             $amountArr[$val["month_year"]] =  $amountArr[$val["month_year"]] + $val["transaction_amount"];
                        }else{
                            $amountArr[$val["month_year"]] =  $val["transaction_amount"];
                        }*/ 
                        if(array_key_exists($val["month_year"],$monthRangeArr)){
                            if(!empty($amountArr[$val["month_year"]])){
                             $amountArr[$val["month_year"]] =  $amountArr[$val["month_year"]] + $val["transaction_amount"];
                             $monthRangeArr[$val["month_year"]] = $amountArr[$val["month_year"]] + $val["transaction_amount"];
                        }else{
                            $amountArr[$val["month_year"]] =  $val["transaction_amount"];
                             $monthRangeArr[$val["month_year"]] = $val["transaction_amount"];
                        }
                            
                        }
                        
                    }
                    //asd($monthRangeArr,0);
                    //asd($monthRangeNameArr);
                     if (!empty($monthlyTransactionsResult)) {
                        foreach ($monthRangeArr as $key => $val) {
                            $dateSep = @explode(' ',$key);
                            $datetext = $dateSep[0]."-".substr($dateSep[1],-2);
                            $monthArrNew[] =$datetext;
                            $amountArr[] = $val['transaction_amount'];
                        }
                         $amountStr = implode(",", $amountArr);
                         $amountStr = "'" . $amountStr . "'";
                    }
 
                    /*if (!empty($monthlyTransactionsResult)) {
                        foreach ($monthYearData as $val) {
                            $monthArrNew[] = $val['months']."-".substr($val['year'], -2); 
                            $amountArr[] = $val['transaction_amount'];
                        }
                         $amountStr = implode(",", $amountArr);
                         $amountStr = "'" . $amountStr . "'";
                    }*/
                }
            }
        }
        $receivedDateRange = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTransactionRange();
        $dateRange = array("" => "Select One");
        foreach ($receivedDateRange as $key => $val) {
            $dateRange[$val['range_id']] = $val['range'];
        }
        $viewModel->setVariables(array(
            'getCrmUserDashlets' => $getCrmUserDashlets,
            'getCrmSavedSearches' => $getCrmSavedSearches,
            'pendingTransaction' => $pendingTransaction,
            'reviewQueTransaction' => $reviewQueTransaction,
            'newAnnotation' => $newAnnotation,
            'duePledges' => $duePledges,
            'newAssignments' => $newAssignments,
            'newMembership' => $newMembership,
            'newContact' => $newContact,
            'amountArr' => $amountStr,
            'monthArr' => json_encode($monthArrNew),
            'jsLangTranslate' => $this->_config['user_messages']['config']['crm_dashboard'],
            'dateRange' => $dateRange
        ));
        return $viewModel;
    }

    /**
     * Delete Dashboard user Dashlet
     * @author Icreon Tech - DG
     * @param code
     * @return Array
     */
    public function deleteUserDashletAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $auth = new AuthenticationService();
        $this->auth = $auth;
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam['dashlet_id'] = $request->getPost('dashlet_id');
            $dataParam['search_id'] = $request->getPost('search_id');
            $dataParam['search_type'] = $request->getPost('search_type');
            $dataParam['user_id'] = $this->auth->getIdentity()->crm_user_id;
            $this->getTables()->deleteCrmUserDashlet($dataParam);
            $messages = array('status' => "success");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This action is used for add user role permission form element
     * @return     void
     * @param object form, array role permission
     * @author Icreon Tech - SK
     */
    public function addDashletPermissionFormElementsToForm($form, $settingArray) {
        if (!empty($settingArray)) {
            foreach ($settingArray as $key => $value) {
                $access = (!is_null($value['is_dashlet_allowed']) && $value['is_dashlet_allowed'] != '') ? $value['is_dashlet_allowed'] : 1;
                $access_element = 'dashlet[' . $value['dashlet_id'] . ']';
                $form->add(array(
                    'type' => 'Zend\Form\Element\Select',
                    'name' => $access_element,
                    'options' => array(
                        'value_options' => array(
                            '1' => 'Enable',
                            '0' => 'Disable',
                        ),
                    ),
                    'attributes' => array(
                        'id' => $access_element,
                        'value' => $access,
                        'class' => 'e1 select-w-100',
                    )
                ));
            }
        }
    }

    /**
     * This action is used for display top 10 contact
     * @return     void
     * @param object form, array 
     * @author Icreon Tech - DG
     */
    public function topContactsAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        parse_str($request->getPost('searchString'), $searchParam);
        $dashlet = $request->getPost('crm_dashlet');
        $dashletId = $request->getPost('dashletId');
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        $searchParam['start_index'] = $start;
        $searchParam['record_limit'] = $limit;
        $searchParam['sort_field'] = $request->getPost('sidx');
        $searchParam['sort_order'] = $request->getPost('sord');
        $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'modified_date') ? 'users.added_date' : $searchParam['sort_field'];
        if (isset($searchParam['added_date_range']) && $searchParam['added_date_range'] != 1 && $searchParam['added_date_range'] != '') {
            $date_range = $this->getDateRange($searchParam['added_date_range']);
            $searchParam['from_date'] = $this->DateFormat($date_range['from'], 'db_date_format');
            $searchParam['to_date'] = $this->DateFormat($date_range['to'], 'db_date_format');
        } else if (isset($searchParam['added_date_range']) && $searchParam['added_date_range'] == '') {
            $searchParam['from_date'] = '';
            $searchParam['to_date'] = '';
        } else {
            if (isset($searchParam['added_date_from']) && $searchParam['added_date_from'] != '') {
                $searchParam['from_date'] = $this->DateFormat($searchParam['added_date_from'], 'db_date_format');
            }
            if (isset($searchParam['added_date_to']) && $searchParam['added_date_to'] != '') {
                $searchParam['to_date'] = $this->DateFormat($searchParam['added_date_to'], 'db_date_format');
            }
        }

        $dashletResult = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getTableStruct(array('user_id' => $this->auth->getIdentity()->crm_user_id, 'dash_id' => $dashletId));
        $newExplodedColumnName = array();
        if (!empty($dashletResult)) {
            $explodedColumnName = explode(",", $dashletResult[0]['table_column_names']);
            foreach ($explodedColumnName as $val) {
                if (strpos($val, ".")) {
                    $newExplodedColumnName[] = trim(strstr($val, ".", false), ".");
                } else {
                    $newExplodedColumnName[] = trim($val);
                }
            }
        }
        $getContactArr = $this->getTables()->getTopContact($searchParam);
        $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
        $jsonResult = array();
        $arrCell = array();
        $subArrCell = array();
        $jsonResult['records'] = $countResult[0]->RecordCount;
        $jsonResult['page'] = $request->getPost('page');
        $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);


        if (!empty($getContactArr)) {
            foreach ($getContactArr as $val) {
                $dashletCell = array();
                $view = $this->encrypt('view');
                $arrCell['id'] = $val['user_id'];
                $dashletContactId = array_search('contact_id', $newExplodedColumnName);
                if ($dashletContactId !== false)
                    $dashletCell[$dashletContactId] = "<a href='/view-contact/" . $this->encrypt($val['user_id']) . "/" . $view . "' class='txt-decoration-underline'>" . $val['contact_id'] . "</a>";

                if ($val['full_name'] != '') {
                    $name = $val['full_name'];
                    $name = $name;
                    $dashletFullName = array_search('full_name', $newExplodedColumnName);
                    if ($dashletFullName !== false)
                        $dashletCell[$dashletFullName] = $name;
                } else {
                    $name = $val['company_name'];

                    $name = $name;
                    $dashletFullName = array_search('full_name', $newExplodedColumnName);
                    if ($dashletFullName !== false)
                        $dashletCell[$dashletFullName] = $name;
                }
                if ($val['email_id'] != '') {
                    $val['email_id'] = $val['email_id'];
                    $dashletEmail = array_search('email_id', $newExplodedColumnName);
                    if ($dashletEmail !== false)
                        $dashletCell[$dashletEmail] = $val['email_id'];
                } else {
                    $val['email_id'] = 'N/A';
                    $dashletEmail = array_search('email_id', $newExplodedColumnName);
                    if ($dashletEmail !== false)
                        $dashletCell[$dashletEmail] = $val['email_id'];
                }

                if ($val['user_type'] == 1) {
                    $userType = 'Individual';
                    $dashletUserType = array_search('user_type', $newExplodedColumnName);
                    if ($dashletUserType !== false)
                        $dashletCell[$dashletUserType] = $userType;
                }
                if ($val['user_type'] == 2) {
                    $userType = 'Corporate';
                    $dashletUserType = array_search('user_type', $newExplodedColumnName);
                    if ($dashletUserType !== false)
                        $dashletCell[$dashletUserType] = $userType;
                }
                if ($val['user_type'] == 3) {
                    $userType = 'Foundation';
                    $dashletUserType = array_search('user_type', $newExplodedColumnName);
                    if ($dashletUserType !== false)
                        $dashletCell[$dashletUserType] = $userType;
                }
                if ($val['total_transaction_amount'] != '') {
                    $total_transaction = "$ " . $val['total_transaction_amount'];
                    $dashletTotalTransaction = array_search('total_transaction_amount', $newExplodedColumnName);
                    if ($dashletTotalTransaction !== false)
                        $dashletCell[$dashletTotalTransaction] = $total_transaction;
                }
                else {
                    $total_transaction = 'N / A';
                    $dashletTotalTransaction = array_search('total_transaction_amount', $newExplodedColumnName);
                    if ($dashletTotalTransaction !== false)
                        $dashletCell[$dashletTotalTransaction] = $total_transaction;
                }


                if (isset($dashlet) && $dashlet == 'dashlet') {
                    $membershipAfhicStr = 'M : ' . $val['membership_id'] . "<br>A : ";
                    if (!empty($val['afihc_numbers'])) {
                        $afhicArr = explode(',', $val['afihc_numbers']);
                        $membershipAfhicStr.=$afhicArr[0] . "";
                    } else {
                        $membershipAfhicStr.='N/A';
                    }

                    $arrCell['cell'] = $dashletCell;
                    //$arrCell['cell'] = array($name, $userType, $membershipAfhicStr, $val['email_id']);
                }
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
        }
        $response->setContent(\Zend\Json\Json::encode($jsonResult));
        return $response;
    }

    /**
     * This action is used for display top selling product
     * @return     void
     * @param object form, array 
     * @author Icreon Tech - DG
     */
    public function topSellingProductsAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        parse_str($request->getPost('searchString'), $searchParam);
        $dashlet = $request->getPost('crm_dashlet');
        $dashletId = $request->getPost('dashletId');
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;

        $searchParam['start_index'] = $start;
        $searchParam['record_limit'] = $limit;
        $searchParam['sort_field'] = $request->getPost('sidx');
        $searchParam['sort_order'] = $request->getPost('sord');
        $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'modified_date') ? 'users.added_date' : $searchParam['sort_field'];
        if (isset($searchParam['added_date_range']) && $searchParam['added_date_range'] != 1 && $searchParam['added_date_range'] != '') {
            $date_range = $this->getDateRange($searchParam['added_date_range']);
            $searchParam['from_date'] = $this->DateFormat($date_range['from'], 'db_date_format_from');
            $searchParam['to_date'] = $this->DateFormat($date_range['to'], 'db_date_format_to');
        } else if (isset($searchParam['added_date_range']) && $searchParam['added_date_range'] == '') {
            $searchParam['from_date'] = date("m/d/Y", strtotime(DATE_TIME_FORMAT . '-1 month'));
            $searchParam['from_date'] = $this->DateFormat($searchParam['from_date'], 'db_date_format_from');
            $searchParam['to_date'] = date("m/d/Y", strtotime(DATE_TIME_FORMAT));
            $searchParam['to_date'] = $this->DateFormat($searchParam['to_date'], 'db_date_format_to');
        } else if (!isset($searchParam['added_date_range']) || empty($searchParam['added_date_range'])) {
            $searchParam['from_date'] = date("m/d/Y", strtotime(DATE_TIME_FORMAT . '-1 month'));
            $searchParam['from_date'] = $this->DateFormat($searchParam['from_date'], 'db_date_format_from');
            $searchParam['to_date'] = date("m/d/Y", strtotime(DATE_TIME_FORMAT));
            $searchParam['to_date'] = $this->DateFormat($searchParam['to_date'], 'db_date_format_to');
        } else {
            if (isset($searchParam['added_date_from']) && $searchParam['added_date_from'] != '') {
                $searchParam['from_date'] = $this->DateFormat($searchParam['added_date_from'], 'db_date_format_from');
            }
            if (isset($searchParam['added_date_to']) && $searchParam['added_date_to'] != '') {
                $searchParam['to_date'] = $this->DateFormat($searchParam['added_date_to'], 'db_date_format_to');
            }
        }

        $dashletResult = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getTableStruct(array('user_id' => $this->auth->getIdentity()->crm_user_id, 'dash_id' => $dashletId));
        $newExplodedColumnName = array();
        if (!empty($dashletResult)) {
            $explodedColumnName = explode(",", $dashletResult[0]['table_column_names']);
            foreach ($explodedColumnName as $val) {
                if (strpos($val, ".")) {
                    $newExplodedColumnName[] = trim(strstr($val, ".", false), ".");
                } else {
                    $newExplodedColumnName[] = trim($val);
                }
            }
        }
        $getProductArr = $this->getTables()->getTopSellingProduct($searchParam);
        $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
        $jsonResult = array();
        $arrCell = array();
        $subArrCell = array();
        $jsonResult['records'] = $countResult[0]->RecordCount;
        $jsonResult['page'] = $request->getPost('page');
        $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);

        if (!empty($getProductArr)) {
            foreach ($getProductArr as $val) {
                $arrCell['id'] = $val['transaction_product_id'];
                $dashletCell = array();

                if (isset($dashlet) && $dashlet == 'dashlet') {

                    $dashletProName = array_search('product_name', $newExplodedColumnName);
                    if ($dashletProName !== false)
                        $dashletCell[$dashletProName] = '<a href="/view-crm-product/' . $this->encrypt($val['product_id']) . '/' . $this->encrypt('view') . '" class="txt-decoration-underline">' . $val['product_name'] . '</a>';

                    $dashletTotalQty = array_search('total_qty', $newExplodedColumnName);
                    if ($dashletTotalQty !== false)
                        $dashletCell[$dashletTotalQty] = $val['total_qty'];

                    $dashletAmt = array_search('total_amount', $newExplodedColumnName);
                    if ($dashletAmt !== false)
                        $dashletCell[$dashletAmt] = "$ " . $val['total_amount'];

                    $arrCell['cell'] = $dashletCell;
                }
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
        }
        $response->setContent(\Zend\Json\Json::encode($jsonResult));
        return $response;
    }

    /**
     * This action is used for display transaction queue
     * @return     void
     * @param object form, array 
     * @author Icreon Tech - DG
     */
    public function transactionQueueAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        parse_str($request->getPost('searchString'), $searchParam);
        $dashlet = $request->getPost('crm_dashlet');
        $itemStatus = $request->getPost('item_status');
        $dashletId = $request->getPost('dashletId');
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;

        $searchParam['item_status'] = isset($itemStatus) ? $itemStatus : null;
        $searchParam['start_index'] = $start;
        $searchParam['record_limit'] = $limit;
        $searchParam['sort_field'] = $request->getPost('sidx');
        $searchParam['sort_order'] = $request->getPost('sord');
        $searchParam['sort_field'] = (isset($searchParam['sort_field'])) ? $searchParam['sort_field'] : 'tra_pro.modified_date';

        if (isset($searchParam['added_date_range']) && $searchParam['added_date_range'] != 1 && $searchParam['added_date_range'] != '') {
            $date_range = $this->getDateRange($searchParam['added_date_range']);
            $searchParam['from_date'] = $this->DateFormat($date_range['from'], 'db_date_format_from');
            $searchParam['to_date'] = $this->DateFormat($date_range['to'], 'db_date_format_to');
        } else if (isset($searchParam['added_date_range']) && $searchParam['added_date_range'] == '') {
            $searchParam['from_date'] = '';
            $searchParam['to_date'] = '';
        } else {
            if (isset($searchParam['added_date_from']) && $searchParam['added_date_from'] != '') {
                $searchParam['from_date'] = $this->DateFormat($searchParam['added_date_from'], 'db_date_format_from');
            }
            if (isset($searchParam['added_date_to']) && $searchParam['added_date_to'] != '') {
                $searchParam['to_date'] = $this->DateFormat($searchParam['added_date_to'], 'db_date_format_to');
            }
        }

        $dashletResult = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getTableStruct(array('user_id' => $this->auth->getIdentity()->crm_user_id, 'dash_id' => $dashletId));
        $newExplodedColumnName = array();
        if (!empty($dashletResult)) {
            $explodedColumnName = explode(",", $dashletResult[0]['table_column_names']);
            foreach ($explodedColumnName as $val) {
                if (strpos($val, ".")) {
                    $newExplodedColumnName[] = trim(strstr($val, ".", false), ".");
                } else {
                    $newExplodedColumnName[] = trim($val);
                }
            }
        }
        $getTrasactionProductArr = $this->getTables()->getTransactionQueue($searchParam);
        $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
        $jsonResult = array();
        $arrCell = array();
        $subArrCell = array();
        $jsonResult['records'] = $countResult[0]->RecordCount;
        $jsonResult['page'] = $request->getPost('page');
        $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);

        if (!empty($getTrasactionProductArr)) {
            foreach ($getTrasactionProductArr as $val) {
                $dashletCell = array();
                $arrCell['id'] = $val['transaction_product_id'];

                $dashletTransactionId = array_search('transaction_id', $newExplodedColumnName);

                if ($dashletTransactionId !== false)
                    $dashletCell[$dashletTransactionId] = '<a href="get-transaction-detail/' . $this->encrypt($val['transaction_id']) . '/' . $this->encrypt('view') . '" class="txt-decoration-underline">' . $val['transaction_id'] . '</a>';
                $dashletProName = array_search('product_name', $newExplodedColumnName);
                if ($dashletProName !== false)
                    $dashletCell[$dashletProName] = $val['product_name'];

                $dashletProTotal = array_search('product_total', $newExplodedColumnName);
                if ($dashletProTotal !== false)
                    $dashletCell[$dashletProTotal] = "$ " . $val['product_total'];

                if (isset($dashlet) && $dashlet == 'dashlet') {
                    $arrCell['cell'] = $dashletCell;
                }
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
        }
        $response->setContent(\Zend\Json\Json::encode($jsonResult));
        return $response;
    }

    /**
     * This action is used for display transaction summary
     * @return     void
     * @param object form, array 
     * @author Icreon Tech - DG
     */
    public function transactionSummaryAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        parse_str($request->getPost('searchString'), $searchParam);
        $dashlet = $request->getPost('crm_dashlet');
        $dashletId = $request->getPost('dashletId');
        $trnasactionDatePeriod = $request->getPost('transaction_date_period');

        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;

        if (isset($trnasactionDatePeriod) && $trnasactionDatePeriod != '') {
            $dateRange = $this->getDateRange($trnasactionDatePeriod);
            $searchParam['from_date'] = $this->DateFormat($dateRange['from'], 'db_date_format_from');
            $searchParam['to_date'] = $this->DateFormat($dateRange['to'], 'db_date_format_to');
            
                $dayRangeArr = array();
				$fromDate = date("Y-m-d",strtotime($dateRange['from']));
                $toDate =  date("Y-m-d",strtotime($dateRange['to']));
				do{
                   $dayRangeNameArr[] = date('l',strtotime($this->OutputDateFormat($fromDate, 'dateformatampm')));
                   $dayRangeArr[date('l',strtotime($this->OutputDateFormat($fromDate, 'dateformatampm')))] = "";
		$weeklyDateRangeArr[date("Y-m-d",strtotime($this->OutputDateFormat($fromDate, 'dateformatampm')))]['transactions'] = 0;
		$weeklyDateRangeArr[date("Y-m-d",strtotime($this->OutputDateFormat($fromDate, 'dateformatampm')))]['transaction_amount']=  0;
		$weeklyDateRangeArr[date("Y-m-d",strtotime($this->OutputDateFormat($fromDate, 'dateformatampm')))]['transaction_date'] = date("Y-m-d",strtotime($this->OutputDateFormat($fromDate, 'dateformatampm')));
						$fromDate = date("Y-m-d", strtotime("+1 day", strtotime($fromDate)));  
                }while( $fromDate <= $toDate );

        }
        $searchParam['start_index'] = $start;
        $searchParam['record_limit'] = $limit;
        $searchParam['sort_field'] = $request->getPost('sidx');
        $searchParam['sort_order'] = $request->getPost('sord');
        $searchParam['sort_field'] = (isset($searchParam['sort_field'])) ? $searchParam['sort_field'] : 'transaction_date';

        if (isset($searchParam['added_date_range']) && $searchParam['added_date_range'] != 1 && $searchParam['added_date_range'] != '') {
            $date_range = $this->getDateRange($searchParam['added_date_range']);
            $searchParam['from_date'] = $this->DateFormat($date_range['from'], 'db_date_format_from');
            $searchParam['to_date'] = $this->DateFormat($date_range['to'], 'db_date_format_to');
        } else if (isset($searchParam['added_date_range']) && $searchParam['added_date_range'] == '') {
            $searchParam['from_date'] = '';
            $searchParam['to_date'] = '';
        } else {
            if (isset($searchParam['added_date_from']) && $searchParam['added_date_from'] != '') {
                $searchParam['from_date'] = $this->DateFormat($searchParam['added_date_from'], 'db_date_format_from');
            }
            if (isset($searchParam['added_date_to']) && $searchParam['added_date_to'] != '') {
                $searchParam['to_date'] = $this->DateFormat($searchParam['added_date_to'], 'db_date_format_to');
            }
        }
        $dashletResult = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getTableStruct(array('user_id' => $this->auth->getIdentity()->crm_user_id, 'dash_id' => $dashletId));
        $newExplodedColumnName = array();
        if (!empty($dashletResult)) {
            $explodedColumnName = explode(",", $dashletResult[0]['table_column_names']);
            foreach ($explodedColumnName as $val) {
                if (strpos($val, ".")) {
                    $newExplodedColumnName[] = trim(strstr($val, ".", false), ".");
                } else {
                    $newExplodedColumnName[] = trim($val);
                }
            }
        }
        if (!isset($searchParam['from_date']) || empty($searchParam['from_date'])) {
            $searchParam['current_date'] = DATE_TIME_FORMAT;
        }
        
                
        //asd($searchParam);
        $getTrasactionProductArr = $this->getTables()->getTransactionSummary($searchParam);
    
		$transactionData = array();
		$i=0;
		foreach($getTrasactionProductArr as $key=>$value){
			$transaction_date = $this->OutputDateFormat($value['transaction_date'], 'dateformatampm');
			$transactionDateArray = explode(' ',$transaction_date);
			
			$transactionData[$transactionDateArray[0]][$i]['transactions'] = $value['transactions'];

			$transactionData[$transactionDateArray[0]][$i]['transaction_amount'] = $value['transaction_amount'];

			$transactionData[$transactionDateArray[0]][$i]['transaction_date'] = $transactionDateArray[0];

			$i++;

		}

		$getTrasactionProductArray = array();
		
		foreach($transactionData as $k=>$v){
			$transactions = 0;
			$transaction_amount = 0;
			foreach($v as $k1=>$v1){
				$transactions+= $v1['transactions'];
				$transaction_amount+= $v1['transaction_amount'];
			}
			$getTrasactionProductArray[$k]['transactions'] = $transactions;
			$getTrasactionProductArray[$k]['transaction_amount'] = $transaction_amount;			
			$getTrasactionProductArray[$k]['transaction_date'] = $k;

			unset($transactions);
			unset($transaction_amount);
		}
		$resultArr = array();
		$resultArr = $getTrasactionProductArray;
		$resultTempArr = array();
		
		 if (isset($trnasactionDatePeriod) && $trnasactionDatePeriod == 13) {
		     /*if( strtolower($request->getPost('sord')) == 'desc' && strtolower($request->getPost('sidx')) == 'transaction_date')
				$weeklyDateRangeArr = array_reverse($weeklyDateRangeArr, true);*/
				
			$resultArr = array_merge($weeklyDateRangeArr,$getTrasactionProductArray);
		}
		$resultArr =  $this->customArraySort($resultArr, strtolower($request->getPost('sidx')), strtolower($request->getPost('sord')));
		//$countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
        $countResult = 1;
        $jsonResult = array();
        $arrCell = array();
        $subArrCell = array();
        /*$jsonResult['records'] = $countResult[0]->RecordCount;
        $jsonResult['page'] = $request->getPost('page');
        $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
        */
	$jsonResult['records'] = count($resultArr);
	$jsonResult['page'] = $request->getPost('page');
	$jsonResult['total'] = ceil(count($resultArr) / $limit);


        if (!empty($resultArr)) {
            foreach ($resultArr as $val) {
                $arrCell['id'] = $val['transaction_date'];
                $dashletCell = array();
                if (isset($dashlet) && $dashlet == 'dashlet') {
                    if ($val['transactions'] != '') {
                        $val['transactions'] = $val['transactions'];
                        $dashletTransaction = array_search('transactions', $newExplodedColumnName);
                        if ($dashletTransaction !== false)
                            $dashletCell[$dashletTransaction] = $val['transactions'];
                    } else {
                        $val['transactions'] = '';
                        $dashletTransaction = array_search('transactions', $newExplodedColumnName);
                        if ($dashletTransaction !== false)
                            $dashletCell[$dashletTransaction] = $val['transactions'];
                    }
                    if ($val['transaction_amount'] != '') {
                        $val['transaction_amount'] = $val['transaction_amount'];
                        $dashletTransactionAmt = array_search('transaction_amount', $newExplodedColumnName);
                        if ($dashletTransactionAmt !== false)
                            $dashletCell[$dashletTransactionAmt] = $this->Currencyformat($val['transaction_amount']);
                    } else {
                        $val['transaction_amount'] = 'N/A';
                        $dashletTransactionAmt = array_search('transaction_amount', $newExplodedColumnName);
                        if ($dashletTransactionAmt !== false)
                            $dashletCell[$dashletTransactionAmt] = $this->Currencyformat($val['transaction_amount']);
                    }

                    if (isset($trnasactionDatePeriod) && $trnasactionDatePeriod == 13) {
                        $val['transaction_date'] = date('l', strtotime($val['transaction_date']));
                        $dashletTransactionDt = array_search('transaction_date', $newExplodedColumnName);
                        if ($dashletTransactionDt !== false)
                            $dashletCell[$dashletTransactionDt] = $val['transaction_date'];
                    } else {
                        $val['transaction_date'] = $val['transaction_date'];
                        $dashletTransactionDt = array_search('transaction_date', $newExplodedColumnName);
                        if ($dashletTransactionDt !== false)
                            $dashletCell[$dashletTransactionDt] = $val['transaction_date'];
                    }

                    $arrCell['cell'] = $dashletCell;
                }
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
        }
        $response->setContent(\Zend\Json\Json::encode($jsonResult));
        return $response;
    }

    /**
     * This action is used for set the dashlet visible setting
     * @return     void
     * @param object form, array 
     * @author Icreon Tech - DG
     */
    public function dashletVisiblitySettingAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getTables();
        $this->getConfig();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $response = $this->getResponse();
        $userId = $this->auth->getIdentity()->crm_user_id;
        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $formData = $request->getPost();
            if (!empty($formData)) {
                $this->getTables()->insertCrmUserDashlets(array('user_id' => $userId, 'dashlet_id' => $formData['dashletId'], 'visibility_setting' => $formData['status'], 'search_id' => $formData['searchId'], 'search_type' => $formData['searchType']));
            }
            $messages = array('status' => "success");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This action is used for set the dashboard setting
     * @return     void
     * @param object form, array 
     * @author Icreon Tech - DG
     */
    public function saveDashboardSettingAction() {
        $auth = new AuthenticationService();
        $this->auth = $auth;
        $this->checkUserAuthentication();
        $this->layout('crm');
        $viewModel = new ViewModel();
        $this->getTables();
        $this->getConfig();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $userId = $this->auth->getIdentity()->crm_user_id;
        $viewModel->setTerminal(true);
        $param = $this->params()->fromRoute();
        if ($param['encrypted_param'] != '') {
            $encryptedParam = $this->decrypt($param['encrypted_param']);
            $encryptedParamArr = explode("@@", $encryptedParam);
        }
        if ($encryptedParamArr[0] != '13') {
            $title = $this->getTables()->getSystemDashletTitle($encryptedParamArr[0]);
            $title = $title[0]['dashlet_name'];
        } else {
            $result = $this->getTables()->getCrmSaveSearches(array('user_id' => $userId));
            foreach ($result as $key => $val) {
                if ($val['search_id'] == $encryptedParamArr[1] && $val['search_type'] == $encryptedParamArr[2]) {
                    $title = $val['title'] . "  - " . $val['search_name'];
                }
            }
        }
        $viewModel->setVariables(array(
            'jsLangTranslate' => $this->_config['user_messages']['config']['search_contact'],
            'param' => $encryptedParamArr,
            'visible' => $param['dashlet'],
            'isDragged' => $param['isDragged'],
            'title' => $title
        ));
        return $viewModel;
    }

    /**
     * This action is used for set the dashboard setting
     * @return     void
     * @param object form, array 
     * @author Icreon Tech - AS
     */
    public function listTableFieldsAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $param = $this->params()->fromRoute();
        $param['user_id'] = $this->auth->getIdentity()->crm_user_id;
        /* Mst Value */
        $searchResult = $this->getTables()->getMstTableStruct($param);
        $tableMst['column_name'] = explode(",", $searchResult[0]['table_column']);
        $tableMst['display_column'] = explode(",", $searchResult[0]['display_column']);
        /* Current Value */
        $searchResult = $this->getTables()->getTableStruct($param);

        if (isset($searchResult) && !empty($searchResult)) {
            $table['column_name'] = explode(",", $searchResult[0]['table_column_names']);
            $table['display_column'] = explode(",", $searchResult[0]['display_column_names']);
            $tableMst['column_name'] = array_diff($tableMst['column_name'], $table['column_name']);
            $tableMst['display_column'] = array_diff($tableMst['display_column'], $table['display_column']);
            $data['column_name'] = array_merge($table['column_name'], $tableMst['column_name']);
            $data['display_column'] = array_merge($table['display_column'], $tableMst['display_column']);
            $data['count'] = count($table['column_name']);
            $data['sort_field'] = $searchResult[0]['sort_order'];
            $data['id'] = $searchResult[0]['crm_user_dashlet_id'];
        } else {
            $data['column_name'] = $tableMst['column_name'];
            $data['display_column'] = $tableMst['display_column'];
            $data['count'] = '0';
            $data['sort_field'] = '';
            $data['id'] = '';
        }
        $data['dashId'] = $param['dash_id'];
        $data['searchId'] = $param['search_id'];
        $data['searchType'] = $param['search_type'];
        $data['visible'] = $param['visible'];
        $jsonResult = array();
        $arrCell = array();
        $subArrCell = array();
        if (count($data) > 0) {
            for ($i = 0; $i < count($data['column_name']); $i++) {
                if ($data['display_column'][$i] != '') {
                    $arrCell['id'] = $i;
                    $hidden = "<input type='hidden' name='existId' id='existId' value='" . $data['id'] . "' >";
                    $dashID = "<input type='hidden' name='dashId' id='dashId' value='" . $data['dashId'] . "' >";
                    $searchID = "<input type='hidden' name='searchId' id='searchId' value='" . $data['searchId'] . "' >";
                    $searchType = "<input type='hidden' name='searchType' id='searchType' value='" . $data['searchType'] . "' >";
                    $visible = "<input type='hidden' name='visible' id='visible' value='" . $data['visible'] . "' >";
                    /* Checkbox */

                    if ($i < $data['count']) {
                        $check = '<div><input type="checkbox" onclick="clickCheck(' . $i . ')" class="check-dash" name="id[]" id="check_dash' . $i . '" value="' . $data['column_name'][$i] . '" checked="checked"><label>&nbsp;&nbsp;&nbsp;&nbsp;(' . $data['display_column'][$i] . ')</label></div>';
                    } else {
                        $check = '<div><input type="checkbox" onclick="clickCheck(' . $i . ')" class="check-dash" name="id[]" id="check_dash' . $i . '" value="' . $data['column_name'][$i] . '"><label>&nbsp;&nbsp;&nbsp;&nbsp;(' . $data['display_column'][$i] . ')</label></div>';
                    }
                    /* Text box */
                    if ($i < $data['count']) {
                        $text = "<div><input type='text' id='name" . $i . "' name='title[]' maxlength='20' value='" . $data['display_column'][$i] . "'><div>";
                    } else {
                        $text = "<div><input type='text' id='name" . $i . "' disabled='disabled' name='title[]' maxlength='20' value='" . $data['display_column'][$i] . "'></div>";
                    }
                    /* radio button  */
                    if ($data['sort_field'] == $data['column_name'][$i] && $i < $data['count']) {
                        $radio = "<div><input id='radio" . $i . "' type='radio' name='sort' value='" . $data['column_name'][$i] . "' checked='checked'></div>";
                    } else if ($i < $data['count']) {
                        $radio = "<div><input id='radio" . $i . "' type='radio' name='sort' value='" . $data['column_name'][$i] . "'></div>";
                    } else {
                        $radio = "<div><input id='radio" . $i . "' type='radio' name='sort' value='" . $data['column_name'][$i] . "' disabled='disabled'></div>";
                    }
                    $arrCell['cell'] = array($hidden, $dashID, $searchID, $searchType, $visible, $check, $text, $radio);
                    $subArrCell[] = $arrCell;
                    $jsonResult['rows'] = $subArrCell;
                }
            }
        }
        return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    /**
     * This action is used for set the dashboard setting
     * @return     void
     * @param object form, array 
     * @author Icreon Tech - AS
     */
    public function updateDashletTableAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $param = $this->params()->fromRoute();
        $columnName = $request->getPost('id');
        $dispName = $request->getPost('title');
        $updateData['col'] = '';
        $updateData['newSeq'] = '';
        for ($i = 0; $i < count($columnName); $i++) {
            $updateData['col'] = $updateData['col'] . trim($columnName[$i]) . ",";
            $updateData['newSeq'] = $updateData['newSeq'] . trim($dispName[$i]) . ",";
        }
        $updateData['id'] = $request->getPost('existId');
        $updateData['col'] = trim($updateData['col'], ",");
        $updateData['newSeq'] = trim($updateData['newSeq'], ",");
        $updateData['sortField'] = $request->getPost('sort');
        $updateData['visible'] = $request->getPost('visible');
        $updateData['modifiedDate'] = DATE_TIME_FORMAT;
        $updateData['userId'] = $this->auth->getIdentity()->crm_user_id;
        $updateData['dashId'] = $request->getPost('dashId');
        $updateData['searchId'] = $request->getPost('searchId');
        $updateData['searchType'] = $request->getPost('searchType');
        /* For update query */
        if (isset($updateData['id']) && !empty($updateData['id'])) {
            $this->getTables()->updateCrmDashlet($updateData);
            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
        /* For insert query */ else {
            $this->getTables()->insertCrmDashlet($updateData);
            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This action is used for set the system dashlet
     * @return     void
     * @param object form, array 
     * @author Icreon Tech - AS
     */
    public function updateSystemDashletAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $searchParams['dash_id'] = $request->getPost('id');
        $searchParams['user_id'] = $this->auth->getIdentity()->crm_user_id;
        $searchResult = $this->getTables()->getMstTableStruct($searchParams);
        $insertParams['col'] = $searchResult[0]['table_column'];
        $insertParams['newSeq'] = $searchResult[0]['display_column'];
        $insertParams['dashId'] = $request->getPost('id');
        $insertParams['userId'] = $this->auth->getIdentity()->crm_user_id;
        $insertParams['sortField'] = $searchResult[0]['sort_order'];
        $insertParams['visible'] = '1';
        $insertParams['modifiedDate'] = DATE_TIME_FORMAT;
        $insertParams['searchId'] = '0';
        $insertParams['searchType'] = '0';
        $searchDash = $this->getTables()->getCrmUserDashlet($searchParams);
        if (isset($searchDash) && !empty($searchDash)) {
            $insertParams['id'] = $searchDash[0]['crm_user_dashlet_id'];
            $this->getTables()->updateCrmDashlet($insertParams);
            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        } else {
            $this->getTables()->insertCrmDashlet($insertParams);
            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    public function recentContactAction() {
        $this->checkUserAuthentication();
        try {
            $request = $this->getRequest();
            $response = $this->getResponse();
            $this->getConfig();
            $messages = array();
            $params = $this->params()->fromRoute();
            $dashlet = $request->getPost('crm_dashlet');
            $dashletSearchId = $request->getPost('searchId');
            $dashletId = $request->getPost('dashletId');
            $dashletSearchType = $request->getPost('searchType');
            $dashletSearchString = $request->getPost('dashletSearchString');
            if (isset($dashletSearchString) && $dashletSearchString != '') {
                $searchParam = unserialize($dashletSearchString);
            }
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit;
            $isExport = $request->getPost('export');
            if ($isExport != "" && $isExport == "excel") {
                $chunksize = $this->_config['export']['chunk_size'];
                $export_limit = $this->_config['export']['export_limit'];

                $searchParam['recordLimit'] = $chunksize;
            }
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');

            if ($this->decrypt($params['new_contact']) == 1) {
                $searchParam['new_contact'] = date('Y-m-d');
            }

            $page_counter = 1;
            do {
                $get_contact_arr = $this->getServiceLocator()->get('User\Model\ContactTable')->getRecentContact($searchParam);
                $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
                $dashletResult = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getTableStruct(array('user_id' => $this->auth->getIdentity()->crm_user_id, 'search_id' => $dashletSearchId, 'dash_id' => $dashletId, 'search_type' => $dashletSearchType));
                $newExplodedColumnName = array();
                if (!empty($dashletResult)) {
                    $explodedColumnName = explode(",", $dashletResult[0]['table_column_names']);
                    foreach ($explodedColumnName as $val) {
                        if (strpos($val, ".")) {
                            $newExplodedColumnName[] = trim(strstr($val, ".", false), ".");
                        } else {
                            $newExplodedColumnName[] = trim($val);
                        }
                    }
                }
                $total = $countResult[0]->RecordCount; //$totUsers; 
                $jsonResult = array();
                $arrCell = array();
                $subArrCell = array();
                if ($isExport == "excel") {
                    if ($total > $chunksize) {
                        $number_of_pages = ceil($total / $chunksize);
                        $page_counter++;
                        $start = $chunksize * $page_counter - $chunksize;
                        $searchParam['start_index'] = $start;
                        $searchParam['page'] = $page_counter;
                    } else {
                        $number_of_pages = 1;
                        $page_counter++;
                    }
                } else {
                    $page_counter = $page;
                    $number_of_pages = ceil($total / $limit);
                    $jsonResult['records'] = $countResult[0]->RecordCount; //$totUsers;
                    $jsonResult['page'] = $request->getPost('page');
                    $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit); // $totUsers 
                }

                if (!empty($get_contact_arr)) {
                    $arrExport = array();
                    foreach ($get_contact_arr as $key => $val) {
                        $dashletCell = array();
                        $view = $this->encrypt('view');
                        $edit = $this->encrypt('edit');


                        $addressAddress = (isset($val['address_name1']) and trim($val['address_name1']) != "") ? trim($val['address_name1']) . " " . ((isset($val['address_name2']) and trim($val['address_name2']) != "") ? "," . trim($val['address_name2']) : "") : trim($val['address_name1']);
                        $addressAddress = (isset($addressAddress) and trim($addressAddress) != "") ? trim($addressAddress) : "N/A";
                        $addressCity = (isset($val['city_name']) and trim($val['city_name']) != "") ? trim($val['city_name']) : "N/A";
                        $addressStateName = (isset($val['state_name']) and trim($val['state_name']) != "") ? trim($val['state_name']) : "N/A";
                        $addressZipCode = (isset($val['zipcode_name']) and trim($val['zipcode_name']) != "") ? trim($val['zipcode_name']) : "N/A";


                        if ($val['is_active'] == '1') {
                            $class = 'blue-icon';
                        } else {
                            $class = '';
                        }
                        if ($val['is_blocked'] == 1) {
                            $lock = '<a class="unlock-icon" style="margin-left: 11px;" onclick="lockUserStatus(\'' . $this->encrypt($val['user_id']) . '\',\'' . $this->encrypt(0) . '\');" href="javascript:void(0);" id="lock-batch-link-97">Lock<div class="tooltip">Unlock User<span></span></div></a>';
                        } else {
                            $lock = '<a class="lock-icon" style="margin-left: 11px;" onclick="lockUserStatus(\'' . $this->encrypt($val['user_id']) . '\',\'' . $this->encrypt(1) . '\');" href="javascript:void(0);" id="lock-batch-link-97">Lock<div class="tooltip">Lock User<span></span></div></a>';
                        }

                        $action = "<div class='action-col search-result " . $class . "'>" . $lock . "<a href='/view-contact/" . $this->encrypt($val['user_id']) . "/" . $view . "' class='view-icon'>View<div class='tooltip'>View<span></span></div></a><a href='/view-contact/" . $this->encrypt($val['user_id']) . "/" . $edit . "' class='edit-icon'>Edit<div class='tooltip'>Edit<span></span></div></a><a href='#delete_contact_content' onclick='deleteContact(" . $val['user_id'] . ");' class='delete_contact delete-icon'>Delete<div class='tooltip'>Delete<span></span></div></a></div>";

                        $arrCell['id'] = $val['user_id'];
                        if ($val['user_type'] == 1) {
                            $userType = 'Individual';
                            $dashletUserType = array_search('user_type', $newExplodedColumnName);
                            if ($dashletUserType !== false)
                                $dashletCell[$dashletUserType] = $userType;
                        }
                        if ($val['user_type'] == 2) {
                            $userType = 'Corporate';
                            $dashletUserType = array_search('user_type', $newExplodedColumnName);
                            if ($dashletUserType !== false)
                                $dashletCell[$dashletUserType] = $userType;
                        }
                        if ($val['user_type'] == 3) {
                            $userType = 'Foundation';
                            $dashletUserType = array_search('user_type', $newExplodedColumnName);
                            if ($dashletUserType !== false)
                                $dashletCell[$dashletUserType] = $userType;
                        }
                        if ($val['level_color']) {
                            $level_color = '<div class="full"><span class="label1" style="background: ' . $val['level_color'] . '"></span></div>';
                        } else {
                            $level_color = '';
                        }

                        $addActivity = $caseContactId = '<span class="plus-sign top"><a href="/create-activity/' . $this->encrypt($val['user_id']) . '/' . $this->encrypt('1') . '"><div class="tooltip">Add activity<span></span></div></a></span>';
                        $omxId = (isset($val['omx_customer_number']) && !empty($val['omx_customer_number'])) ? 'OMX (' . $val['omx_customer_number'] . ')' : '';
                        if ($val['full_name'] != '') {
                            $name = $val['full_name'];
                            $dashletFullName = array_search('full_name', $newExplodedColumnName);
                            if ($dashletFullName !== false)
                                $dashletCell[$dashletFullName] = $name;
                        } else {
                            $name = $val['company_name'];
                            $dashletFullName = array_search('full_name', $newExplodedColumnName);
                            if ($dashletFullName !== false)
                                $dashletCell[$dashletFullName] = $name;
                        }

                        if ($val['email_id'] != '') {
                            $val['email_id'] = $val['email_id'];
                            $dashletEmail = array_search('email_id', $newExplodedColumnName);
                            if ($dashletEmail !== false)
                                $dashletCell[$dashletEmail] = $val['email_id'];
                        } else {
                            $val['email_id'] = 'N/A';
                            $dashletEmail = array_search('email_id', $newExplodedColumnName);
                            if ($dashletEmail !== false)
                                $dashletCell[$dashletEmail] = $val['email_id'];
                        }
                        if ($val['country_name'] != '') {
                            $val['country_name'] = $val['country_name'];
                            $dashletCountryName = array_search('country_name', $newExplodedColumnName);
                            if ($dashletCountryName !== false) {
                                $dashletCell[$dashletCountryName] = $val['country_name'];
                            }
                        } else {
                            $val['country_name'] = 'N/A';
                            $dashletCountryName = array_search('country_name', $newExplodedColumnName);
                            if ($dashletCountryName !== false) {
                                $dashletCell[$dashletCountryName] = $val['country_name'];
                            }
                        }
                        if ($val['transaction_id'] != '') {
                            $val['transaction_id'] = $val['transaction_id'];
                            $dashletTransactionId = array_search('transaction_id', $newExplodedColumnName);
                            if ($dashletTransactionId !== false)
                                $dashletCell[$dashletTransactionId] = $val['transaction_id'];
                        } else {
                            $val['transaction_id'] = 'N/A';
                            $dashletTransactionId = array_search('transaction_id', $newExplodedColumnName);
                            if ($dashletTransactionId !== false)
                                $dashletCell[$dashletTransactionId] = $val['transaction_id'];
                        }
                        if ($val['transaction_amount'] != '') {
                            $val['transaction_amount'] = "$ " . $val['transaction_amount'];
                            $dashletTransactionAmt = array_search('transaction_amount', $newExplodedColumnName);
                            if ($dashletTransactionAmt !== false)
                                $dashletCell[$dashletTransactionAmt] = $val['transaction_amount'];
                        } else {
                            $val['transaction_amount'] = 'N/A';
                            $dashletTransactionAmt = array_search('transaction_amount', $newExplodedColumnName);
                            if ($dashletTransactionAmt !== false)
                                $dashletCell[$dashletTransactionAmt] = $val['transaction_amount'];
                        }
                        if ($val['transaction_date'] != '') {
                            $transactionDate = $this->OutputDateFormat($val['transaction_date'], 'dateformatampm');
                            $dashletTransactionDt = array_search('transaction_date', $newExplodedColumnName);
                            if ($dashletTransactionDt !== false)
                                $dashletCell[$dashletTransactionDt] = $transactionDate;
                        } else {
                            $transactionDate = 'N/A';
                            $dashletTransactionDt = array_search('transaction_date', $newExplodedColumnName);
                            if ($dashletTransactionDt !== false)
                                $dashletCell[$dashletTransactionDt] = $transactionDate;
                        }

                        if ($val['membership_title'] != '') {
                            $val['membership_title'] = $val['membership_title'];
                            $dashletMembership = array_search('membership_title', $newExplodedColumnName);
                            if ($dashletMembership !== false)
                                $dashletCell[$dashletMembership] = $val['membership_title'];
                        } else {
                            $val['membership_title'] = 'N/A';
                            $dashletMembership = array_search('membership_title', $newExplodedColumnName);
                            if ($dashletMembership !== false)
                                $dashletCell[$dashletMembership] = $val['membership_title'];
                        }


                        if ($val['contact_id'] != '') {
                            $dashletContactId = array_search('contact_id', $newExplodedColumnName);
                            if ($dashletContactId !== false)
                                $dashletCell[$dashletContactId] = "<a class='txt-decoration-underline' href='/view-contact/" . $this->encrypt($val['user_id']) . "/" . $view . "' >" . $val['contact_id'] . "</a>";
                        }
                        if (isset($dashlet) && $dashlet == 'dashlet') {
                            $arrCell['cell'] = $dashletCell;
                        } else if ($isExport == "excel") {
                            $arrExport[] = array('Name' => $name, 'Email' => $val['email_id']/* , 'User Type' => $userType */, 'Membership Level' => $val['membership_title'], 'Address' => $addressAddress, 'City' => $addressCity, 'State' => $addressStateName, 'Zip Code' => $addressZipCode, 'Country' => $val['country_name'], /* 'Last Transaction#' => $val['transaction_id'], */ 'Last Transaction Date' => $transactionDate/* , 'Last Transction Amount' => $val['transaction_amount'] */);
                        } else {
                            $omxId2 = (isset($omxId) and trim($omxId) != "") ? $omxId . "<br>" : "";
                            $arrCell['cell'] = array($val['contact_id'] . "<br>" . $omxId2 . $addActivity, $name, $val['email_id'], /* $userType, */ $val['membership_title'] . " " . $level_color, $addressAddress, $addressCity, $addressStateName, $addressZipCode, (isset($val['country_code']) and trim($val['country_code']) != "") ? trim($val['country_code']) : "N/A", /* $val['transaction_id'], */ $transactionDate/* , $val['transaction_amount'] */, $action);
                        }
                        $subArrCell[] = $arrCell;
                        $jsonResult['rows'] = $subArrCell;
                    }
                    if ($isExport == "excel") {
                        $filename = "contact_export_" . date("Y-m-d") . ".csv";
                        $this->arrayToCsv($arrExport, $filename, $page_counter);
                    }
                }
            } while ($page_counter <= $number_of_pages && $isExport == "excel");
            if ($isExport == "excel") {
                $this->downloadSendHeaders("contact_export_" . date("Y-m-d") . ".csv");
                die;
            } else {
                $response->setContent(\Zend\Json\Json::encode($jsonResult));
                return $response;
            }
        } catch (Exception $e) {
            throw new \Exception('There is some error.');
        }
    }
	//this function is used to sort multidimension array based on key
   function customArraySort($array, $on='transaction_date', $order='asc'){
    $new_array = array();
    $sortable_array = array();

    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case 'asc':
                \asort($sortable_array);
                break;
            case 'desc':
                \arsort($sortable_array);
                break;
				
			default:
				\asort($sortable_array);
                break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }
    return $new_array;
	}

}
