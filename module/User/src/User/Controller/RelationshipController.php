<?php

/**
 * This controller is used to display user Relationship
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Controller;

use Base\Controller\BaseController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Zend\Mvc\Controller\Plugin\Params;
use Zend\Http\Request;
use Zend\Authentication\AuthenticationService;
use User\Model\Relationship;
use User\Form\EditUserRelationshipForm;
use User\Form\CreateUserRelationshipForm;

/**
 * This controller is used to display user Relationship
 * @package    User
 * @author     Icreon Tech - DG
 */
class RelationshipController extends BaseController {

    protected $_relationshipTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_authservice = null;

    public function __construct() {
        //if already login, redirect to success page
        $this->_flashMessage = $this->flashMessenger();
        $auth = new AuthenticationService();
        $this->auth = $auth;
    }

    public function indexAction() {
        
    }

    /**
     * This functikon is used to get table connections
     * @return     array 
     * @author Icreon Tech - DG
     */
    public function getTables() {
        $sm = $this->getServiceLocator();
        $this->_relationshipTable = $sm->get('User\Model\RelationshipTable');
        $this->_adapter = $sm->get('dbAdapter');
        return $this->_relationshipTable;
    }

    /**
     * This functikon is used to get config variables
     * @return     array 
     * @author Icreon Tech - DG
     */
    public function getConfig() {
        $sm = $this->getServiceLocator();
        $this->_translator = $sm->get('translator');
        $this->_config = $sm->get('Config');
    }

    /**
     * This Action is used to get users relationship
     * @author Icreon Tech-DG
     * @return Array
     */
    public function getUserRelationshipsAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getConfig();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $response = $this->getResponse();
        $messages = array();
        $params = $this->params()->fromRoute();
        $user_id = $params['user_id'];
        $messages = '';
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $messages = array();
            $searchParam = array();
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit;
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['user_id'] = $this->decrypt($user_id);
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'usr_relation.added_date') ? 'usr_relation.added_date' : $searchParam['sort_field'];
            $searches_arr = $this->getTables()->getUserRelationships($searchParam);
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $jsonResult['page'] = $request->getPost('page');
            $jsonResult['records'] = $countResult[0]->RecordCount;            
            $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
            if (!empty($searches_arr)) {
                foreach ($searches_arr as $val) {
                    $arrCell['id'] = $val['user_relationship_id'];
                    $relationship = $val['relationship'] . ' ' . $val['first_name'];
                    $relationshipId = $val['user_relationship_id'];
                    $relationshipId = $this->encrypt($relationshipId);
                    //$action = "<div class='action-col'><a href='/user-relationships-information/" . $this->encrypt($val['user_relationship_id']) . "' class='view-icon'>View<div class='tooltip'>View<span></span></div></a><a href='/edit-user-relationships/" . $this->encrypt($val['user_relationship_id']) . "' class='edit-icon'>Edit<div class='tooltip'>Edit<span></span></div></a><a href='#delete_relationship_content' onclick='deleteRelationship(" . $val['user_relationship_id'] . ");' class='delete_relationship delete-icon'>Delete<div class='tooltip'>Delete<span></span></div></a></div>";
                    $action = '<div class="action-col"><a onclick="viewUserRelation(\'' . $relationshipId . '\');" href="javascript:void(0);" class="view-icon">View<div class="tooltip">View<span></span></div></a><a onclick="editUserRelation(\'' . $relationshipId . '\');" href="javascript:void(0);" class="edit-icon">Edit<div class="tooltip">Edit<span></span></div></a><a href="#delete_relationship_content" onclick="deleteRelationship(' . $val['user_relationship_id'] . ');" class="delete_relationship delete-icon">Delete<div class="tooltip">Delete<span></span></div></a></div>';
                    $arrCell['cell'] = array($relationship, $val['email_id'], $val['city'], $val['state'], $action);
                    $subArrCell[] = $arrCell;
                    $jsonResult['rows'] = $subArrCell;
                }
            }
            $response->setContent(\Zend\Json\Json::encode($jsonResult));
            return $response;
        } else {
            $viewModel->setVariables(array(
                'user_id' => $user_id,
                'messages' => $messages,
                'jsLangTranslate' => $this->_config['user_messages']['config']['user_relationship'])
            );
            return $viewModel;
        }
    }

    /**
     * This Action is used to delete user relatyionship
     * @author Icreon Tech-DG
     */
    public function deleteUserRelationshipAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam['relationship_id'] = $request->getPost('relationship_id');
            $this->getTables()->deleteUserRelationship($dataParam);
            $this->flashMessenger()->addMessage($this->_config['user_messages']['config']['user_relationship']['RECORD_DELETED']);
            $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['user_relationship']['RECORD_DELETED']);
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This Action is used to VIEW user relatyionship
     * @author Icreon Tech-DG
     */
    public function getUserRelationshipInfoAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getConfig();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $response = $this->getResponse();
        $messages = array();
        $params = $this->params()->fromRoute();
        $relationship_id = $this->decrypt($params['relationship_id']);
        $user_relationship_info = $this->getTables()->getUserRelationshipInfo(array('user_relationship_id' => $relationship_id));
        $viewModel->setVariables(array(
            'relationship_id' => $params['relationship_id'],
            'user_relationship_info' => $user_relationship_info,
            'jsLangTranslate' => $this->_config['user_messages']['config']['user_relationship'])
        );
        return $viewModel;
    }

    /**
     * This Action is used to update user relationship
     * @author Icreon Tech-DG
     * @return Array
     * @param keyword
     */
    public function editUserRelationshipsAction() {
        $this->checkUserAuthentication();
        $form = new EditUserRelationshipForm();
        $this->layout('crm');
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $this->getConfig();
        $this->getTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $params = $this->params()->fromRoute();
        $relationship_id = $this->decrypt($params['relationship_id']);
        $relationship = new Relationship($this->_adapter);
        
        
        
        $user_relationship_info = $this->getTables()->getUserRelationshipInfo(array('user_relationship_id' => $relationship_id));
        
        $get_relations = $this->getServiceLocator()->get('Common\Model\CommonTable')->getRelations(array('relation_id'=>$user_relationship_info['relationship_id']));
        $mst_relations_list = array();
		$mst_relationsDesc = array();
        $mst_relations_list[''] = 'Please Select';
        foreach ($get_relations as $key => $val) {
            $mst_relations_list[$val['relationship_id']] = $val['relationship'];
			$mst_relationsDesc[$val['relationship_id']] = $val['description'];
        }
        
        $form->get('notes')->setAttribute('value', $user_relationship_info['relationship_note']);
        $form->get('enabled')->setAttribute('value', $user_relationship_info['relationship_is_enable']);
        $form->get('type')->setAttribute('options', $mst_relations_list);
        $form->get('type')->setAttribute('value', $user_relationship_info['relationship_id']);
        $form->get('user_id')->setAttribute('value', $this->encrypt($user_relationship_info['user_id']));
        $form->get('user_relationship_id')->setAttribute('value', $params['relationship_id']);

        if ($request->isPost()) {
            $form->setInputFilter($relationship->getInputFilterEditUserRelation());
            $form->setData($request->getPost());

            if (!$form->isValid()) {
                $errors = $form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            $msg [$key] = $this->_config['user_messages']['config']['edit_user_relationship'][$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $relationship->exchangeArrayEditUserRelation($form->getData());
                $formData = array();
                $formData['user_relationship_id'] = $this->decrypt($relationship->user_relationship_id);
                $formData['type'] = $relationship->type;
                $formData['notes'] = $relationship->notes;
                $formData['enabled'] = $relationship->enabled;
                $last_updated = $this->getTables()->updateUserRelationship($formData);
                $this->flashMessenger()->addMessage($this->_config['user_messages']['config']['edit_user_relationship']['RECORED_UPDATED']);
                $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['edit_user_relationship']['RECORED_UPDATED'],'view'=>$this->encrypt('relation'));
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $viewModel->setVariables(array(
            'relationship_id' => $params['relationship_id'],
            'form' => $form,
            'full_name' => $user_relationship_info['full_name'],
			'mst_relationsDesc' => $mst_relationsDesc,
            'jsLangTranslate' => $this->_config['user_messages']['config']['edit_user_relationship'])
        );
        return $viewModel;
    }

    /**
     * This Action is used to create user relationship
     * @author Icreon Tech-DG
     * @return Array
     * @param keyword
     */
    public function addUserRelationshipAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $form = new CreateUserRelationshipForm();
        $viewModel = new ViewModel();
        $this->getConfig();
        $this->getTables();
        $viewModel->setTerminal(true);
        $request = $this->getRequest();

        $response = $this->getResponse();
        $messages = array();
        $params = $this->params()->fromRoute();
        $user_id = $this->decrypt($params['user_id']);
        $relationship = new Relationship($this->_adapter);
        $get_relations = $this->getServiceLocator()->get('Common\Model\CommonTable')->getRelations('');
        $mst_relations_list = array();
		$mst_relationsDesc = array();
        $mst_relations_list[''] = 'Please Select';
        foreach ($get_relations as $key => $val) {
            $mst_relations_list[$val['relationship_id']] = $val['relationship'];
			$mst_relationsDesc[$val['relationship_id']] = $val['description'];
        }
        $form->get('type')->setAttribute('options', $mst_relations_list);
        $form->get('user_id')->setAttribute('value', $params['user_id']);

        if ($request->isPost()) {
            $form->setInputFilter($relationship->getInputFilterCreateUserRelation());
            $form->setData($request->getPost());

            if (!$form->isValid()) {
                $errors = $form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            $msg [$key] = $this->_config['user_messages']['config']['create_user_relationship'][$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $relationship->exchangeArrayCreateUserRelation($form->getData());
                $formData = array();
                $formData['user_id'] = $this->decrypt($relationship->user_id);
                $formData['type'] = $relationship->type;
                $formData['notes'] = $relationship->notes;
                $formData['enabled'] = $relationship->enabled;
                $formData['relative_user_id'] = $relationship->relative_user_id;
                $this->getTables()->insertUserRelationships($formData);
                /** insert into the change log */
                $changeLogArray = array();
                $changeLogArray['table_name'] = 'tbl_usr_change_logs';
                $changeLogArray['activity'] = '2';/** 2 == for update */
                $changeLogArray['id'] = $formData['user_id'];
                $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                /** end insert into the change log */
                $this->flashMessenger()->addMessage($this->_config['user_messages']['config']['create_user_relationship']['RECORED_SAVED']);
                $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['create_user_relationship']['RECORED_SAVED'],'view'=>$this->encrypt('relation'));
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $viewModel->setVariables(array(
            'form' => $form,
			'mst_relationsDesc' => $mst_relationsDesc,
            'jsLangTranslate' => $this->_config['user_messages']['config']['create_user_relationship'])
        );
        return $viewModel;
    }

}
