<?php

/**
 * This controller is used to find and merge duplicate record
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Controller;

use Base\Controller\BaseController;
use Zend\Authentication\AuthenticationService;
use Zend\View\Model\ViewModel;
use User\Model\MergeContact;
use Zend\Mvc\Controller\Plugin\Params;
use Zend\Http\Request;
use User\Form\RuleGroupForm;
use User\Model\Contact;

/**
 * This controller is used to find and merge duplicate record
 * @package    User
 * @author     Icreon Tech - DG
 */
class MergecontactController extends BaseController {

    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_mergecontactTable = null;

    public function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        $auth = new AuthenticationService();
        $this->auth = $auth;
    }

    public function indexAction() {
        
    }

    /**
     * This functikon is used to get table connections
     * @return     array 
     * @author Icreon Tech - DG
     */
    public function getTables() {
        $sm = $this->getServiceLocator();
        $this->_mergecontactTable = $sm->get('User\Model\MergeContactTable');
        $this->_adapter = $sm->get('dbAdapter');
        return $this->_mergecontactTable;
    }

    /**
     * This functikon is used to get config variables
     * @return     array 
     * @author Icreon Tech - DG
     */
    public function getConfig() {
        $sm = $this->getServiceLocator();
        $this->_translator = $sm->get('translator');
        $this->_config = $sm->get('Config');
    }

    /**
     * This Action is used to display the find and merge rule
     * @author Icreon Tech-DG
     */
    public function getMergeRuleAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $response = $this->getResponse();
        $this->getConfig();
        $viewModel = new ViewModel();

        $form = new RuleGroupForm();
        $getGroups = $this->getServiceLocator()->get('Common\Model\CommonTable')->getGroups();
        $groupList = array();
        $groupList[''] = '';
        foreach ($getGroups as $key => $val) {
            $groupId = $this->encrypt($val['group_id']);
            $groupList[$groupId] = $val['name'];
        }
        $form->get('group_id_name')->setAttribute('options', $groupList);
        $form->get('group_id_address')->setAttribute('options', $groupList);
        $form->get('group_id_phone')->setAttribute('options', $groupList);
        $form->get('group_id_last_name_address')->setAttribute('options', $groupList);
        $form->get('group_id_last_name_phone')->setAttribute('options', $groupList);

        $viewModel->setVariables(array(
            'form' => $form,
            'name' => $this->encrypt('name'),
            'address' => $this->encrypt('address'),
            'phone' => $this->encrypt('phone'),
            'last_name_address' => $this->encrypt('last_name_address'),
            'last_name_phone' => $this->encrypt('last_name_phone'),
            'jsLangTranslate' => $this->_config['user_messages']['config']['find_merge']
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to get duplicate contact list
     * @author Icreon Tech-DG
     */
    public function getDuplicateContactAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $messages = '';
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        $getCombinations = array();
        $this->getConfig();
        $params = $this->params()->fromRoute();
        $rule = $this->decrypt($params['rule']);
        if (isset($params['group'])) {
            $group = $this->decrypt($params['group']);
        } else {
            $group = '';
        }

        $duplicateCombination = array();
        $dedupeUsersEncrypted = array();

        $duplicateUsers = $this->getTables()->getDuplicateContact($rule, $group);

        $dedupeExceptionUser = $this->getTables()->getDedupeException();

        if (!empty($dedupeExceptionUser)) {
            foreach ($dedupeExceptionUser as $key => $val) {
                $dedupeUsers[$key]['first_user_id'] = $this->encrypt($val['first_user_id']);
                $dedupeUsers[$key]['second_user_id'] = $this->encrypt($val['second_user_id']);
            }
            foreach ($dedupeUsers as $val) {
                $dedupeUsersEncrypted[] = implode(",", $val);
            }
        }

        if (!empty($duplicateUsers)) {
            $users = array();
            foreach ($duplicateUsers as $key => $val) {
                $users[$key]['full_name'] = $val['full_name'];
                if ($rule == 'last_name_address' || $rule == 'last_name_phone')
                    $users[$key]['first_name'] = $val['first_name'];
                $users[$key]['user_id'] = $this->encrypt($val['user_id']);
            }
            foreach ($users as $key => $val) {
                $name = strtolower($val['full_name']);
                $duplicateCombination[$name][] = $val;
            }

            foreach ($duplicateCombination as $key => $val) {
                $arr = array();
                $arr = $duplicateCombination[$key];
                $getCombinations[$key] = $this->getCombinations($val, 2);
            }
        }
        $viewModel->setVariables(array(
            'rule' => $params['rule'],
            'group' => (isset($params['group'])) ? $params['group'] : null,
            'getCombinations' => $getCombinations,
            'dedupeUsersEncrypted' => $dedupeUsersEncrypted,
            'messages' => $messages,
            'jsLangTranslate' => $this->_config['user_messages']['config']['find_merge']
                )
        );
        return $viewModel;
    }

    /**
     * This function is used to permutation and combination of an array
     * @author Icreon Tech-DG
     * @return Array
     * @param Array
     */
    public function getCombinations($base, $n) {
        $baselen = count($base);
        if ($baselen == 0) {
            return;
        }
        if ($n == 1) {
            $return = array();
            foreach ($base as $b) {
                $return[] = array($b);
            }
            return $return;
        } else {
            $oneLevelLower = $this->getCombinations($base, $n - 1);
            $newCombs = array();
            foreach ($oneLevelLower as $oll) {
                $lastEl = $oll[$n - 2];
                $found = false;
                foreach ($base as $key => $b) {
                    if ($b == $lastEl) {
                        $found = true;
                        continue;
                    }
                    if ($found == true) {
                        if ($key < $baselen) {
                            $tmp = $oll;
                            $newCombination = array_slice($tmp, 0);
                            $newCombination[] = $b;
                            $newCombs[] = array_slice($newCombination, 0);
                        }
                    }
                }
            }
        }
        return $newCombs;
    }

    /**
     * This Action is used to insert dedupe exception
     * @author Icreon Tech-DG
     * @return JSON
     * @param INT (userid and duplicate userid)
     */
    public function addDedupeExceptionAction() {
        $this->checkUserAuthentication();
        $this->layout('popup');
        $response = $this->getResponse();
        $request = $this->getRequest();
        $this->getConfig();
        $messages = array();
        if ($request->isPost()) {
            $postedData = $request->getPost();
            $userId = $postedData['userId'];
            $duplicateUserId = $postedData['duplicateUserId'];
            $this->getTables()->insertDedupeException(array('user_id' => $this->decrypt($userId), 'duplicate_user_id' => $this->decrypt($duplicateUserId)));
            $this->flashMessenger()->addMessage($this->_config['user_messages']['config']['find_merge']['SUCCESS_DEDUPE_MARK']);
            $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['find_merge']['SUCCESS_DEDUPE_MARK']);
        } else {
            $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['find_merge']['ERROR_MSG']);
        }
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This Action is used to get duplicate contact info
     * @author Icreon Tech-DG
     */
    public function getDuplicateContactInfoAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $this->getConfig();
        $params = $this->params()->fromRoute();
        $messages = '';
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        $duplicateUserId = $params['duplicate_user_id'];
        $userId = $params['user_id'];
        $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->decrypt($params['user_id'])));
        $duplicateUserDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->decrypt($params['duplicate_user_id'])));

        $duplicateUserAlternateContact = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAlternateContact(array('user_id' => $this->decrypt($duplicateUserId)));
        $userAlternateContact = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAlternateContact(array('user_id' => $this->decrypt($userId)));

        $duplicateUserAfihc = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAfihc(array('user_id' => $this->decrypt($duplicateUserId)));
        $userAfihc = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAfihc(array('user_id' => $this->decrypt($userId)));

        $duplicateUserNotes = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserNotes(array('user_id' => $this->decrypt($duplicateUserId)));
        $duplicateUserDocs = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDocuments(array('user_id' => $this->decrypt($duplicateUserId)));

        $duplicateUserTags = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserTags(array('user_id' => $this->decrypt($duplicateUserId)));
        $userTags = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserTags(array('user_id' => $this->decrypt($userId)));

        $duplicateUserInterest = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserInterests(array('user_id' => $this->decrypt($duplicateUserId)));
        $userInterest = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserInterests(array('user_id' => $this->decrypt($userId)));

        $duplicateUserGroups = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserGroups(array('user_id' => $this->decrypt($duplicateUserId)));
        $userGroups = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserGroups(array('user_id' => $this->decrypt($userId)));

        $duplicateUserEducation = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserEducations(array('user_id' => $this->decrypt($duplicateUserId)));
        $userEducation = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserEducations(array('user_id' => $this->decrypt($userId)));

        $duplicateUserComm = $this->getServiceLocator()->get('User\Model\ContactTable')->getCommunicationPreferences(array('user_id' => $this->decrypt($duplicateUserId)));
        $userComm = $this->getServiceLocator()->get('User\Model\ContactTable')->getCommunicationPreferences(array('user_id' => $this->decrypt($userId)));

        $duplicateUserAdd = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations(array('user_id' => $this->decrypt($duplicateUserId)));
        $userAdd = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations(array('user_id' => $this->decrypt($userId)));

        $duplicateUserPhone = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserPhoneInformations(array('user_id' => $this->decrypt($duplicateUserId)));
        $userPhone = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserPhoneInformations(array('user_id' => $this->decrypt($userId)));

        $duplicateUserDevelopmentCount = $this->getServiceLocator()->get('User\Model\DevelopmentTable')->isUserDevelopment(array('user_id' => $this->decrypt($duplicateUserId)));
        $duplicateUserSurveyCount = $this->getServiceLocator()->get('User\Model\SurveyTable')->isUserSurvey(array('user_id' => $this->decrypt($duplicateUserId)));
        $duplicateUserSearchesCount = $this->getServiceLocator()->get('User\Model\ContactTable')->isUserSearches(array('user_id' => $this->decrypt($duplicateUserId)));

        $userTransactionCount = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getUserTransactionCount(array('user_id' => $this->decrypt($userId)));
        $userCampaignCount = $this->getServiceLocator()->get('Campaign\Model\CampaignTable')->getUserCampaignCount(array('user_id' => $this->decrypt($userId)));
        $userMembershipCount = $this->getServiceLocator()->get('User\Model\MembershipTable')->getUserMembershipCount(array('user_id' => $this->decrypt($userId)));
        $userRelationshipCount = $this->getServiceLocator()->get('User\Model\RelationshipTable')->getUserRelationshipCount(array('user_id' => $this->decrypt($userId)));
        $userCaseCount = $this->getServiceLocator()->get('Cases\Model\CasesTable')->getCaseCount(array('user_id' => $this->decrypt($userId)));

        $activityArr = array();
        $activityArr['sourceType'] = 1;
        $activityArr['userId'] = $this->decrypt($userId);
        $userActivityCount = $this->getServiceLocator()->get('Activity\Model\ActivityTable')->getActivityCount($activityArr);

        $activityArr['userId'] = $this->decrypt($duplicateUserId);
        $duplicateUserActivityCount = $this->getServiceLocator()->get('Activity\Model\ActivityTable')->getActivityCount($activityArr);
        $duplicateUserTransactionCount = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getUserTransactionCount(array('user_id' => $this->decrypt($duplicateUserId)));
        $duplicateUserCampaignCount = $this->getServiceLocator()->get('Campaign\Model\CampaignTable')->getUserCampaignCount(array('user_id' => $this->decrypt($duplicateUserId)));
        $duplicateUserMembershipCount = $this->getServiceLocator()->get('User\Model\MembershipTable')->getUserMembershipCount(array('user_id' => $this->decrypt($duplicateUserId)));
        $duplicateUserRelationshipCount = $this->getServiceLocator()->get('User\Model\RelationshipTable')->getUserRelationshipCount(array('user_id' => $this->decrypt($duplicateUserId)));
        $duplicateUserCaseCount = $this->getServiceLocator()->get('Cases\Model\CasesTable')->getCaseCount(array('user_id' => $this->decrypt($duplicateUserId)));

        $viewModel->setVariables(array(
            'user_id' => $params['user_id'],
            'duplicate_user_id' => $params['duplicate_user_id'],
            'rule' => $params['rule'],
            'group' => (isset($params['group'])) ? $params['group'] : null,
            'viewMode' => $this->encrypt('view'),
            'userDetail' => $userDetail,
            'duplicateUserDetail' => $duplicateUserDetail,
            'duplicateUserAlternateContact' => $duplicateUserAlternateContact,
            'userAlternateContact' => $userAlternateContact,
            'duplicateUserAfihc' => $duplicateUserAfihc,
            'userAfihc' => $userAfihc,
            'duplicateUserNotes' => $duplicateUserNotes,
            'duplicateUserDocs' => $duplicateUserDocs,
            'duplicateUserTags' => $duplicateUserTags,
            'userTags' => $userTags,
            'duplicateUserInterest' => $duplicateUserInterest,
            'userInterest' => $userInterest,
            'duplicateUserGroups' => $duplicateUserGroups,
            'userGroups' => $userGroups,
            'duplicateUserEducation' => $duplicateUserEducation,
            'userEducation' => $userEducation,
            'duplicateUserComm' => $duplicateUserComm,
            'userComm' => $userComm,
            'duplicateUserAdd' => $duplicateUserAdd,
            'userAdd' => $userAdd,
            'duplicateUserPhone' => $duplicateUserPhone,
            'userPhone' => $userPhone,
            'userTransactionCount' => $userTransactionCount,
            'userCampaignCount' => $userCampaignCount,
            'userMembershipCount' => $userMembershipCount,
            'userCaseCount' => $userCaseCount,
            'userActivityCount' => $userActivityCount,
            'userRelationshipCount' => $userRelationshipCount,
            'duplicateUserTransactionCount' => $duplicateUserTransactionCount,
            'duplicateUserCampaignCount' => $duplicateUserCampaignCount,
            'duplicateUserMembershipCount' => $duplicateUserMembershipCount,
            'duplicateUserCaseCount' => $duplicateUserCaseCount,
            'duplicateUserActivityCount' => $duplicateUserActivityCount,
            'duplicateUserRelationshipCount' => $duplicateUserRelationshipCount,
            'duplicateUserDevelopmentCount' => $duplicateUserDevelopmentCount,
            'duplicateUserSearchesCount' => $duplicateUserSearchesCount,
            'duplicateUserSurveyCount' => $duplicateUserSurveyCount,
            'messages' => $messages,
            'jsLangTranslate' => $this->_config['user_messages']['config']['merge_contact']
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to get duplicate contact info
     * @author Icreon Tech-DG
     */
    public function flipContactAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $this->getConfig();
        $params = $this->params()->fromRoute();
        $duplicateUserId = $params['duplicate_user_id'];
        $userId = $params['user_id'];
        $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->decrypt($params['user_id'])));
        $duplicateUserDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->decrypt($params['duplicate_user_id'])));

        $duplicateUserAlternateContact = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAlternateContact(array('user_id' => $this->decrypt($duplicateUserId)));
        $userAlternateContact = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAlternateContact(array('user_id' => $this->decrypt($userId)));

        $duplicateUserAfihc = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAfihc(array('user_id' => $this->decrypt($duplicateUserId)));
        $userAfihc = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAfihc(array('user_id' => $this->decrypt($userId)));

        $duplicateUserNotes = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserNotes(array('user_id' => $this->decrypt($duplicateUserId)));
        $duplicateUserDocs = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDocuments(array('user_id' => $this->decrypt($duplicateUserId)));

        $duplicateUserTags = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserTags(array('user_id' => $this->decrypt($duplicateUserId)));
        $userTags = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserTags(array('user_id' => $this->decrypt($userId)));

        $duplicateUserInterest = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserInterests(array('user_id' => $this->decrypt($duplicateUserId)));
        $userInterest = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserInterests(array('user_id' => $this->decrypt($userId)));

        $duplicateUserGroups = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserGroups(array('user_id' => $this->decrypt($duplicateUserId)));
        $userGroups = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserGroups(array('user_id' => $this->decrypt($userId)));

        $duplicateUserEducation = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserEducations(array('user_id' => $this->decrypt($duplicateUserId)));
        $userEducation = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserEducations(array('user_id' => $this->decrypt($userId)));

        $duplicateUserComm = $this->getServiceLocator()->get('User\Model\ContactTable')->getCommunicationPreferences(array('user_id' => $this->decrypt($duplicateUserId)));
        $userComm = $this->getServiceLocator()->get('User\Model\ContactTable')->getCommunicationPreferences(array('user_id' => $this->decrypt($userId)));

        $duplicateUserAdd = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations(array('user_id' => $this->decrypt($duplicateUserId)));
        $userAdd = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations(array('user_id' => $this->decrypt($userId)));

        $duplicateUserPhone = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserPhoneInformations(array('user_id' => $this->decrypt($duplicateUserId)));
        $userPhone = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserPhoneInformations(array('user_id' => $this->decrypt($userId)));

        $duplicateUserDevelopmentCount = $this->getServiceLocator()->get('User\Model\DevelopmentTable')->isUserDevelopment(array('user_id' => $this->decrypt($duplicateUserId)));
        $duplicateUserSurveyCount = $this->getServiceLocator()->get('User\Model\SurveyTable')->isUserSurvey(array('user_id' => $this->decrypt($duplicateUserId)));
        $duplicateUserSearchesCount = $this->getServiceLocator()->get('User\Model\ContactTable')->isUserSearches(array('user_id' => $this->decrypt($duplicateUserId)));

        $userTransactionCount = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getUserTransactionCount(array('user_id' => $this->decrypt($userId)));
        $userCampaignCount = $this->getServiceLocator()->get('Campaign\Model\CampaignTable')->getUserCampaignCount(array('user_id' => $this->decrypt($userId)));
        $userMembershipCount = $this->getServiceLocator()->get('User\Model\MembershipTable')->getUserMembershipCount(array('user_id' => $this->decrypt($userId)));
        $userRelationshipCount = $this->getServiceLocator()->get('User\Model\RelationshipTable')->getUserRelationshipCount(array('user_id' => $this->decrypt($userId)));
        $userCaseCount = $this->getServiceLocator()->get('Cases\Model\CasesTable')->getCaseCount(array('user_id' => $this->decrypt($userId)));

        $activityArr = array();
        $activityArr['sourceType'] = 1;
        $activityArr['userId'] = $this->decrypt($userId);
        $userActivityCount = $this->getServiceLocator()->get('Activity\Model\ActivityTable')->getActivityCount($activityArr);

        $activityArr['userId'] = $this->decrypt($duplicateUserId);
        $duplicateUserActivityCount = $this->getServiceLocator()->get('Activity\Model\ActivityTable')->getActivityCount($activityArr);
        $duplicateUserTransactionCount = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getUserTransactionCount(array('user_id' => $this->decrypt($duplicateUserId)));
        $duplicateUserCampaignCount = $this->getServiceLocator()->get('Campaign\Model\CampaignTable')->getUserCampaignCount(array('user_id' => $this->decrypt($duplicateUserId)));
        $duplicateUserMembershipCount = $this->getServiceLocator()->get('User\Model\MembershipTable')->getUserMembershipCount(array('user_id' => $this->decrypt($duplicateUserId)));
        $duplicateUserRelationshipCount = $this->getServiceLocator()->get('User\Model\RelationshipTable')->getUserRelationshipCount(array('user_id' => $this->decrypt($duplicateUserId)));
        $duplicateUserCaseCount = $this->getServiceLocator()->get('Cases\Model\CasesTable')->getCaseCount(array('user_id' => $this->decrypt($duplicateUserId)));

        $viewModel->setVariables(array(
            'userDetail' => $userDetail,
            'duplicateUserDetail' => $duplicateUserDetail,
            'duplicateUserAlternateContact' => $duplicateUserAlternateContact,
            'userAlternateContact' => $userAlternateContact,
            'duplicateUserAfihc' => $duplicateUserAfihc,
            'userAfihc' => $userAfihc,
            'duplicateUserNotes' => $duplicateUserNotes,
            'duplicateUserDocs' => $duplicateUserDocs,
            'duplicateUserTags' => $duplicateUserTags,
            'userTags' => $userTags,
            'duplicateUserInterest' => $duplicateUserInterest,
            'userInterest' => $userInterest,
            'duplicateUserGroups' => $duplicateUserGroups,
            'userGroups' => $userGroups,
            'duplicateUserEducation' => $duplicateUserEducation,
            'userEducation' => $userEducation,
            'duplicateUserComm' => $duplicateUserComm,
            'userComm' => $userComm,
            'duplicateUserAdd' => $duplicateUserAdd,
            'userAdd' => $userAdd,
            'duplicateUserPhone' => $duplicateUserPhone,
            'userPhone' => $userPhone,
            'userTransactionCount' => $userTransactionCount,
            'userCampaignCount' => $userCampaignCount,
            'userMembershipCount' => $userMembershipCount,
            'userCaseCount' => $userCaseCount,
            'userActivityCount' => $userActivityCount,
            'userRelationshipCount' => $userRelationshipCount,
            'duplicateUserTransactionCount' => $duplicateUserTransactionCount,
            'duplicateUserCampaignCount' => $duplicateUserCampaignCount,
            'duplicateUserMembershipCount' => $duplicateUserMembershipCount,
            'duplicateUserCaseCount' => $duplicateUserCaseCount,
            'duplicateUserActivityCount' => $duplicateUserActivityCount,
            'duplicateUserRelationshipCount' => $duplicateUserRelationshipCount,
            'duplicateUserDevelopmentCount' => $duplicateUserDevelopmentCount,
            'duplicateUserSearchesCount' => $duplicateUserSearchesCount,
            'duplicateUserSurveyCount' => $duplicateUserSurveyCount,
            'jsLangTranslate' => $this->_config['user_messages']['config']['merge_contact']
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to  merge duplicate users
     * @author Icreon Tech-DG
     * @return JSON
     * @param userId, duplicateUserId INT
     */
    public function editMergeContactAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $this->getConfig();
        $params = $this->params()->fromRoute();

        if ($request->isPost()) {
            $postedData = $request->getPost();
            if (!empty($postedData)) {
                $userId = $postedData['user_id'];
                $duplicateUserId = $postedData['duplicate_user_id'];
                $duplicateUserDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->decrypt($duplicateUserId)));
                $editData = array();
                if (isset($postedData['title'])) {
                    $editData['title'] = (isset($duplicateUserDetail['title'])) ? $duplicateUserDetail['title'] : null;
                }
                if (isset($postedData['source'])) {
                    $editData['source'] = (isset($duplicateUserDetail['source_id'])) ? $duplicateUserDetail['source_id'] : null;
                }
                if (isset($postedData['suffix'])) {
                    $editData['suffix'] = (isset($duplicateUserDetail['suffix'])) ? $duplicateUserDetail['suffix'] : null;
                }
                if (isset($postedData['email_id'])) {
                    $editData['email_id'] = (isset($duplicateUserDetail['email_id'])) ? $duplicateUserDetail['email_id'] : null;
                }
                if (isset($postedData['income_range'])) {
                    $editData['income_range'] = (isset($duplicateUserDetail['income_range_id'])) ? $duplicateUserDetail['income_range_id'] : null;
                }
                if (isset($postedData['security_question'])) {
                    $editData['security_question'] = (isset($duplicateUserDetail['security_question_id'])) ? $duplicateUserDetail['security_question_id'] : null;
                    $editData['security_answer'] = (isset($duplicateUserDetail['security_answer'])) ? $duplicateUserDetail['security_answer'] : null;
                }
                if (isset($postedData['gender'])) {
                    $editData['gender'] = (isset($duplicateUserDetail['gender'])) ? $duplicateUserDetail['gender'] : null;
                }
                if (isset($postedData['marital_status'])) {
                    $editData['marital_status'] = (isset($duplicateUserDetail['marital_status'])) ? $duplicateUserDetail['marital_status'] : null;
                }
                if (isset($postedData['nationality'])) {
                    $editData['nationality'] = (isset($duplicateUserDetail['nationality_id'])) ? $duplicateUserDetail['nationality_id'] : null;
                }
                if (isset($postedData['ethnicity'])) {
                    $editData['ethnicity'] = (isset($duplicateUserDetail['ethnicity_id'])) ? $duplicateUserDetail['ethnicity_id'] : null;
                }
                $editData['userId'] = $this->decrypt($userId);
                $this->getTables()->editDuplicateContact($editData);

                $mergeData = array();
                if (isset($postedData['transaction'])) {
                    $mergeData['transaction'] = (isset($postedData['transaction'])) ? $postedData['transaction'] : null;
                }
                if (isset($postedData['development'])) {
                    $mergeData['development'] = (isset($postedData['development'])) ? $postedData['development'] : null;
                    $this->mergePrivateDocument(array('userId' => $mergeData['userId'], 'duplicateUserId' => $mergeData['duplicateUserId']));
                }
                if (isset($postedData['activity'])) {
                    $mergeData['activity'] = (isset($postedData['activity'])) ? $postedData['activity'] : null;
                }
                if (isset($postedData['case'])) {
                    $mergeData['case'] = (isset($postedData['case'])) ? $postedData['case'] : null;
                }
                if (isset($postedData['realtionship'])) {
                    $mergeData['realtionship'] = (isset($postedData['realtionship'])) ? $postedData['realtionship'] : null;
                }
                if (isset($postedData['membership'])) {
                    $mergeData['membership'] = (isset($postedData['membership'])) ? $postedData['membership'] : null;
                }
                if (isset($postedData['interest'])) {
                    $mergeData['interest'] = (isset($postedData['interest'])) ? $postedData['interest'] : null;
                }
                if (isset($postedData['tags'])) {
                    $mergeData['tags'] = (isset($postedData['tags'])) ? $postedData['tags'] : null;
                }
                if (isset($postedData['group'])) {
                    $mergeData['group'] = (isset($postedData['group'])) ? $postedData['group'] : null;
                }
                if (isset($postedData['communication_pre'])) {
                    $mergeData['communication_pre'] = (isset($postedData['communication_pre'])) ? $postedData['communication_pre'] : null;
                }
                if (isset($postedData['education'])) {
                    $mergeData['education'] = (isset($postedData['education'])) ? $postedData['education'] : null;
                }
                if (isset($postedData['alternate_contact'])) {
                    $mergeData['alternate_contact'] = (isset($postedData['alternate_contact'])) ? $postedData['alternate_contact'] : null;
                }
                if (isset($postedData['afihc'])) {
                    $mergeData['afihc'] = (isset($postedData['afihc'])) ? $postedData['afihc'] : null;
                }
                if (isset($postedData['user_notes'])) {
                    $mergeData['user_notes'] = (isset($postedData['user_notes'])) ? $postedData['user_notes'] : null;
                }
                if (isset($postedData['user_docs'])) {
                    $mergeData['user_docs'] = (isset($postedData['user_docs'])) ? $postedData['user_docs'] : null;
                    $this->mergePublicDocument(array('userId' => $this->decrypt($userId), 'duplicateUserId' => $this->decrypt($duplicateUserId)));
                }
                if (isset($postedData['user_searches'])) {
                    $mergeData['user_searches'] = (isset($postedData['user_searches'])) ? $postedData['user_searches'] : null;
                }
                if (isset($postedData['survey'])) {
                    $mergeData['survey'] = (isset($postedData['survey'])) ? $postedData['survey'] : null;
                }
                $mergeData['userId'] = $this->decrypt($userId);
                $mergeData['duplicateUserId'] = $this->decrypt($duplicateUserId);

                $this->getTables()->mergeDuplicateContact($mergeData);

                $phoneMerge = array();
                $phoneMerge['userId'] = $this->decrypt($userId);
                $phoneMerge['duplicateUserId'] = $this->decrypt($duplicateUserId);
                if (isset($postedData['phone_info']) && count($postedData['phone_info']) > 0) {
                    foreach ($postedData['phone_info'] as $val) {
                        $phoneMerge['phone_id'] = $val;
                        $this->getTables()->mergePhoneNumber($phoneMerge);
                    }
                }

                $addressMerge = array();
                $addressMerge['userId'] = $this->decrypt($userId);
                $addressMerge['duplicateUserId'] = $this->decrypt($duplicateUserId);
                if (isset($postedData['address_info']) && count($postedData['address_info']) > 0) {
                    foreach ($postedData['address_info'] as $val) {
                        $addressMerge['address_info'] = $val;
                        $this->getTables()->mergeAddress($addressMerge);
                    }
                }
                $this->getServiceLocator()->get('User\Model\ContactTable')->deleteContact(array('conatact_id' => $this->decrypt($duplicateUserId)));

                /** insert into the change log */
                $changeLogArray = array();
                $changeLogArray['table_name'] = 'tbl_usr_change_logs';
                $changeLogArray['activity'] = '2';/** 2 == for update */
                $changeLogArray['id'] = $this->decrypt($userId);
                $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);

                $this->flashMessenger()->addMessage($this->_config['user_messages']['config']['merge_contact']['SUCCESS_MERGE']);

                if (isset($params['next'])) {
                    $rule = $this->decrypt($params['rule']);
                    if (isset($params['group'])) {
                        $group = $this->decrypt($params['group']);
                    } else {
                        $group = '';
                    }
                    $nextMergeUser = $this->getNextPairForMerge($rule, $group);
                    if (!empty($nextMergeUser)) {
                        $nextUsers = explode(",", $nextMergeUser[0]);
                        $messages = array('status' => "success", 'next' => true, 'user_id' => $this->encrypt($nextUsers[0]), 'duplicate_user_id' => $this->encrypt($nextUsers[1]));
                    } else {
                        $messages = array('status' => "success", 'next' => false);
                    }
                } else {
                    $messages = array('status' => "success");
                }
            } else {
                $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['merge_contact']['ERROR_MSG']);
            }
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This function is used to get next pair
     * @author Icreon Tech-DG
     * @return JSON
     * @param userId, duplicateUserId INT
     */
    public function getNextPairForMerge($rule, $group) {
        $duplicateCombination = array();
        $dedupeUsersEncrypted = array();
        $duplicateUsers = $this->getTables()->getDuplicateContact($rule, $group);
        $dedupeExceptionUser = $this->getTables()->getDedupeException();

        if (!empty($dedupeExceptionUser)) {
            foreach ($dedupeExceptionUser as $val) {
                $dedupeUsersEncrypted[] = implode(",", $val);
            }
        }
        if (!empty($duplicateUsers)) {
            foreach ($duplicateUsers as $key => $val) {
                $name = strtolower($val['full_name']);
                $duplicateCombination[$name][] = $val;
            }
            foreach ($duplicateCombination as $key => $val) {
                $arr = array();
                $arr = $duplicateCombination[$key];
                $getCombinations[$key] = $this->getCombinations($val, 2);
            }
        }

        $nextMergeUser = array();
        if (!empty($getCombinations)) {
            foreach ($getCombinations as $key => $value) {
                foreach ($value as $k => $usrVal) {
                    $pair1 = $usrVal[0]['user_id'] . "," . $usrVal[1]['user_id'];
                    $pair2 = $usrVal[1]['user_id'] . "," . $usrVal[0]['user_id'];
                    if (!empty($dedupeUsersEncrypted)) {
                        if (!in_array($pair1, $dedupeUsersEncrypted) && !in_array($pair2, $dedupeUsersEncrypted)) {
                            $nextMergeUser[] = $usrVal[0]['user_id'] . "," . $usrVal[1]['user_id'];
                        }
                    } else {
                        $nextMergeUser[] = $usrVal[0]['user_id'] . "," . $usrVal[1]['user_id'];
                    }
                }
            }
        }
        return $nextMergeUser;
    }

    /**
     * This Action is used to  move duplicate users documents
     * @author Icreon Tech-DG
     * @return JSON
     * @param userId, duplicateUserId INT
     */
    public function mergePrivateDocument($users) {
        $this->getConfig();
        $path = $this->_config['file_upload_path']['assets_upload_dir'] . "user/";
        $folderStrucByUser = $this->FolderStructByUserId($users['userId']);
        $folderStrucByDuplicateUser = $this->FolderStructByUserId($users['duplicateUserId']);

        $userDir = $path . $folderStrucByUser . "development_doc";
        $duplicateUserDir = $path . $folderStrucByDuplicateUser . "development_doc";
        if (!is_dir($userDir)) {
            mkdir($userDir, 0777, TRUE);
        }
        if (is_dir($duplicateUserDir)) {
            if ($dh = opendir($duplicateUserDir)) {
                while (($file = readdir($dh)) !== false) {
                    if ($file == ".")
                        continue;
                    if ($file == "..")
                        continue;

                    if (copy("$duplicateUserDir/$file", "$userDir/$file")) {
                        echo "Files Copyed Successfully";
                    } else {
                        echo "File Not Copy";
                    }
                }
                closedir($dh);
            }
        }
    }

    /**
     * This Action is used to  move duplicate users documents
     * @author Icreon Tech-DG
     * @return JSON
     * @param userId, duplicateUserId INT
     */
    public function mergePublicDocument($users) {
        $this->getConfig();
        $path = $this->_config['file_upload_path']['assets_upload_dir'] . "user/";
        $folderStrucByUser = $this->FolderStructByUserId($users['userId']);
        $folderStrucByDuplicateUser = $this->FolderStructByUserId($users['duplicateUserId']);

        $userDir = $path . $folderStrucByUser . "public_doc";
        $duplicateUserDir = $path . $folderStrucByDuplicateUser . "public_doc";
        if (!is_dir($userDir)) {
            mkdir($userDir, 0777, TRUE);
        }
        if (is_dir($duplicateUserDir)) {
            if ($dh = opendir($duplicateUserDir)) {
                while (($file = readdir($dh)) !== false) {
                    if ($file == ".")
                        continue;
                    if ($file == "..")
                        continue;

                    if (copy("$duplicateUserDir/$file", "$userDir/$file")) {
                        echo "Files Copyed Successfully";
                    } else {
                        echo "File Not Copy";
                    }
                }
                closedir($dh);
            }
        }
    }

	public function getDuplicateContactRecordAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $messages = '';
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        $getCombinations = array();
        $this->getConfig();
        $params = $this->params()->fromRoute();

		$request = $this->getRequest();
		
        if($request->getPost('user_id1') && $request->getPost('user_id2')){
			$userDetail1 = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $request->getPost('user_id1')));

			$userDetail2 = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $request->getPost('user_id2')));
		}elseif($request->getPost('user_contact_id1') && $request->getPost('user_contact_id2')){
			$userDetail1 = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $request->getPost('user_contact_id1')));

			$userDetail2 = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $request->getPost('user_contact_id2')));
		}

		$viewModel->setVariables(array(
			'userDetail1' => $userDetail1,
			'userDetail2' => $userDetail2,
            'jsLangTranslate' => $this->_config['user_messages']['config']['find_merge']
                )
        );
        return $viewModel;
    }

	public function mergeContactRecordAction(){
		$this->checkUserAuthentication();
		$request = $this->getRequest();
		$response = $this->getResponse();
		$userId = $this->Decrypt($request->getPost('userId'));
		$duplicateUserId = $this->Decrypt($request->getPost('duplicateUserId'));
		$this->getConfig();

		$this->getServiceLocator()->get('User\Model\ContactTable')->mergeDuplicateEmailRecord(array('old_user_id' => $duplicateUserId, 'new_user_id' => $userId));

		$messages = array('status' => "success", 'message' => "Contact records merged successfully");
		$response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
	}

}

