<?php

/**
 * This controller is used for registration, mail verification and account activation.
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Controller;

use Base\Controller\BaseController;
use Zend\Authentication\AuthenticationService;
use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\Plugin\Params;
use Zend\Http\Request;
use Zend\Session\Container as SessionContainer;
use User\Model\User;
use User\Form\UserForm;
use User\Form\UpdateEmailForm;
use User\Form\ActivationForm;
use User\Form\ResendActivationCodeForm;
use User\Form\UserVisitRecordForm;
use Zend\Captcha\Image as CaptchaImage;


/**
 * This controller is used for registration, mail verification and account activation.
 * @author     Icreon Tech - DG
 */
class SignupController extends BaseController {

    protected $_userTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_authservice = null;
    protected $_countryTable;

    public function __construct() {
        //if already login, redirect to success page        
    }

    public function indexAction() {
        
    }

    /**
     * This functikon is used to get table and config file object
     * @return     array 
     * @param array $form
     * @author Icreon Tech - DG
     */
    public function getUserTable() {
        if (!$this->_userTable) {
            $sm = $this->getServiceLocator();
            $this->_userTable = $sm->get('User\Model\UserTable');
            $this->_translator = $sm->get('translator');
            $this->_config = $sm->get('Config');
            $this->_adapter = $sm->get('dbAdapter');
        }
        return $this->_userTable;
    }
    /**
     * This function is used to get auth object
     * @return     array 
     * @param array
     * @author  Icreon Tech - DG
     */
    public function getAuthServiceByUsername() {
        if (!$this->_authservice) {
            $this->_authservice = $this->getServiceLocator()
                    ->get('AuthServiceByUsername');
        }
        return $this->_authservice;
    }

    /**
     * This action is used for registration and insert user data into table.
     * @return     array 
     * @param array $form
     * @author Icreon Tech - DG
     */
    public function signupAction() {
                  /*if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
                                    $ipList=explode(", ",$_SERVER['HTTP_X_FORWARDED_FOR']);
                    }elseif(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'])){
                           $ipList[]=$_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];}
                    else{
                         $ipList[]=$_SERVER['REMOTE_ADDR'];
                   }
                   $allowed_ip = (in_array($this->_config['allowed_ip']['ip'],$ipList))?'1':'0';
                   */
                   
        $auth = new AuthenticationService();
        if ($auth->hasIdentity() === true) {
            $user = $auth->getIdentity();
            if (isset($user->email_id)) {
                $this->checkFrontUserSessionPopup();
            }
        }
        $this->getUserTable();
        
        // add - n - start
        $params = $this->params()->fromRoute();
       //asd($params,2);
        $referalUrlPN = (isset($params['referalUrl']) and trim($params['referalUrl']) != "") ? trim($params['referalUrl']) : "";
        // add - n - end
        
        $form = new UserForm($this->_config['file_upload_path']);
        $form->get('referal_url_path')->setValue($referalUrlPN);
        $this->getUserTable();

        $request = $this->getRequest();
        $user = new User($this->_adapter);
        $viewModel = new ViewModel();
        $this->layout('web-popup');
        $response = $this->getResponse();
        $messages = array();
        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
        $country_list = array();
        $country_list[''] = 'SELECT COUNTRY';
        foreach ($country as $key => $val) {
            $country_list[$val['country_id']] = $val['name'];
        }
        $form->get('country_id')->setAttribute('options', $country_list);
        //for poup form
        $form->get('country_id_username')->setAttribute('options', $country_list);
    
        
        $sec_question = $this->getServiceLocator()->get('Common\Model\QuestionTable')->getSecurityQuestion();
        $sec_question_list = array();
        $sec_question_list[''] = 'Select Security Question';
        foreach ($sec_question as $key => $val) {
            $sec_question_list[$val['security_question_id']] = $val['security_question'];
        }
        $form->get('security_question_id')->setAttribute('options', $sec_question_list);
        
        $state = $this->getServiceLocator()->get('Common\Model\CommonTable')->getStates();
        $stateList = array();
        $stateList[''] = 'SELECT STATE';
        foreach ($state as $key => $val) {
           $stateList[$val['state_id']] = $val['state'];
        }
        $form->get('state_id')->setAttribute('options', $stateList);
        
        if ($request->isPost()) {
            
//            $messages = array('status' => "success", 'message' =>"hello", 'allowed_ip'=>'1','data'=>'100');                
//            $response->setContent(\Zend\Json\Json::encode($messages));
//            return $response;
                
            $form->setData($request->getPost());
            $emailNot = $request->getPost('email_not');
            $emailFlag = true;
            if(!empty($emailNot)){
                $emailFlag = false;
            }
            $form->setInputFilter($user->getInputFilter($emailFlag));
            if (!$form->isValid()) {
                $errors = $form->getMessages();
                $msg = array();

                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            if ($key == 'captcha') {
                                $msg [$key] = $this->_config['user_messages']['config']['user_signup']['CAPTCHA_EMPTY'];
								//continue;
                            } else {
                                if(isset($this->_config['user_messages']['config']['user_signup'][$rower]) and trim($this->_config['user_messages']['config']['user_signup'][$rower]) != "") {
                                  $msg[$key] = $this->_config['user_messages']['config']['user_signup'][$rower];                                 
                                }
                            }
                        }
                    }
                }
		if(!empty($msg))
                    $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else { 
                $user->exchangeArray($form->getData());
                $user->username = $request->getPost('username');
                if (isset($user->email_id) && $user->email_id != '') {
                    $isEmailExits = $this->getServiceLocator()->get('User\Model\UserTable')->isEmailExits(array('email_id' => $user->email_id));
                    if ($isEmailExits) {
                        $messages = array('status' => "error", 'result' => $isEmailExits, 'message' => $this->_config['user_messages']['config']['user_signup']['EMAIL_ID_ALREADY']);
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                }
                if (isset($user->username) && $user->username != '') {
                    $isUsernameExist = $this->getServiceLocator()->get('User\Model\UserTable')->isUsernameExist(array('username' => $user->username));
                    if ($isUsernameExist) {
                        $messages = array('status' => "error", 'result' => $isUsernameExist, 'message' => $this->_config['user_messages']['config']['user_signup']['USERNAME_ALREADY']);
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                }

                $user->terms_version_id = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCurrentTermsVersion();
                $user_id = $this->getUserTable()->saveUser($user);
                if ($user_id) {
                    $this->getServiceLocator()->get('User\Model\ContactTable')->updateContactId(array('user_id'=>$user_id));
                    
                    $addressForm = array();
                    $curDate = DATE_TIME_FORMAT;
                    $addressForm['first_name'] = $request->getPost('first_name');
                    $addressForm['last_name'] = trim($request->getPost('middle_name').' '.$request->getPost('last_name'));
                    $addressForm['addressinfo_location_type'] = null;
                    $addressForm['addressinfo_street_address_1'] = $request->getPost('address_1') ;
                    $addressForm['addressinfo_city'] = $request->getPost('city');
                    $addressForm['addressinfo_zip'] = isset($user->postal_code)?$user->postal_code:'';
                    $addressForm['addressinfo_street_address_2'] = $request->getPost('address_2') ;;
                    $addressForm['addressinfo_state'] = $request->getPost('state_id') ;;
                    $addressForm['addressinfo_country'] = $request->getPost('country_id_username') ;
                    $addressForm['addressinfo_location'] = '1,3';
                    $addressForm['user_id'] = $user_id;
                    $addressForm['add_date'] = $curDate;
                    $addressForm['modified_date'] = $curDate;
                   // asd($addressForm);
            
                    $this->getServiceLocator()->get('User\Model\ContactTable')->insertAddressInfo($addressForm);

                    
                    $this->getServiceLocator()->get('User\Model\UserTable')->insertDefaultContactCommunicationPreferences(array('user_id' => $user_id));
                    $membershipDetailArr = array();
                    $membershipDetailArr[] = 1;
                    $getMemberShip = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);

                    $userMembershipData = array();
                    $userMembershipData['user_id'] = $user_id;
                    $userMembershipData['membership_id'] = $getMemberShip['membership_id'];
					
					$userMembershipInfo = $this->getServiceLocator()->get('UserMembership')->getUserMembership($getMemberShip);
					$userMembershipData['membership_date_from'] = $userMembershipInfo['membership_date_from'];
                    $userMembershipData['membership_date_to'] = $userMembershipInfo['membership_date_to'];
                    $this->getServiceLocator()->get('User\Model\UserTable')->saveUserMembership($userMembershipData);

                    $userDataArr = $this->getUserTable()->getUser(array('user_id' => $user_id));
                    $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                    $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                    $signup_email_id_template_int = 1;
                    
                  
                     
                    // add - link to activation link - end
                    //$this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $signup_email_id_template_int, $this->_config['email_options']);
                }
				/*$ipList=explode(", ",$_SERVER['HTTP_X_FORWARDED_FOR']);
				if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
					$ipList[]=$_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];*/
                               // $allowed_ip = (in_array($this->_config['allowed_ip']['ip'],$ipList))?'1':'0';
                                
                                 //$allowed_ip = (in_array($this->_config['allowed_ip']['ip'],$ipList))?'1':'0';
                                
                                //$allowed_ip = 1;
                                // Start code to redirect the page
                                 $allowed_ip ='1';
                                if($allowed_ip=='1'){ 
                                        
                                        $aLoginRedirect = $request->getPost('referal_url_path');
                                        $user->referal_url_path = $aLoginRedirect = $this->decrypt($aLoginRedirect);
                                        $isParentWindow = 1;
                                        
                                        
					$form_data = $form->getData();
                                        if($form_data['email_id'] !=""){
                                            $this->directLoginBySignup($form_data['email_id'],$form_data['password']); 
                                        }else
                                              $this->directLoginByUsernameSignup($form_data['username'],$form_data['password']); 
                                        
                                        $referalUrl = (isset($params['referalUrl']) and trim($params['referalUrl']) != "" and trim($params['referalUrl']) != "0") ? trim($params['referalUrl']) : $aLoginRedirect;
                                        

                                        if($referalUrl != "") {
                                            if (strpos($referalUrl, "view-woh-products") !== false) {
                                                $user->referal_url_path = $referalUrl."@@rd";
                                                $isParentWindow = 0;                  
                                            }
                                            else if (strpos($referalUrl, "add-to-cart") !== false) {
                                                $user->referal_url_path = $this->_config['js_file_path']['path']."/shop";
                                                $isParentWindow = 1;
                                            }
                                        }
				}

                                if(Empty($user->referal_url_path)) {
                                        $isParentWindow = 1;
                                        $user->referal_url_path = 'profile';
                                }
                                
                $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['user_signup']['USER_SIGNUP_SUCCESS'], 'allowed_ip'=>$allowed_ip,'data'=>$this->encrypt($user_id));                
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $viewModel->setVariables(array(
            'form' => $form,
            'response' => $response,
            'jsLangTranslate' => $this->_config['user_messages']['config']['user_signup'])
        );

        return $viewModel;
    }

    /**
     * This action is used to verify user email id and activate account
     * @return     array 
     * @param array $form
     * @author Icreon tech - DG
     */
    public function activationAction() {
        $auth = new AuthenticationService();
        if ($auth->hasIdentity() === true) {
            $user = $auth->getIdentity();
            if (isset($user->email_id)) {
                return $this->redirect()->toRoute('user-profile');
            }
        }

        $form = new ActivationForm();
        $viewModel = new ViewModel();
        $this->getUserTable();
        $request = $this->getRequest();

        $response = $this->getResponse();
        $messages = array();
        $user = new User($this->_adapter);
        $params = $this->params()->fromRoute();
        
        // n - start
        $referalUrl = (isset($params['referalUrl']) and trim($params['referalUrl']) != "" and trim($params['referalUrl']) != "0") ? trim($params['referalUrl']) : "";
        if($referalUrl != "") {
            if (strpos($this->decrypt($referalUrl), "view-woh-products") !== false) {
                $referalUrl = $referalUrl."@@rd";
            }
            else if (strpos($this->decrypt($referalUrl), "add-to-cart") !== false) {
                $referalUrl = $this->encrypt($this->_config['js_file_path']['path']."/shop");
            }
        }
        
        $form->get('referal_path_url')->setValue($referalUrl);
        // n - end
        
        if (isset($params['code']) && $params['code'] != '' && $params['code'] != 'r1') {
            $verify_code = $params['code'];
            if (isset($verify_code) && $verify_code != '') {
                $userDataArr = $this->getUserTable()->getUser(array('verify_code' => $verify_code));
                if (empty($userDataArr)) {
                    $msg = $this->_config['user_messages']['config']['user_activation']['WRONG_VERIFICATION_DONE'];
                    $viewModel->setVariables(array(
                        'referalUrl' => $referalUrl,
                        'form' => $form,
                        'error_msg' => $msg,
                        'jsLangTranslate' => $this->_config['user_messages']['config']['user_activation'])
                    );
                    return $viewModel;
                } else {

                    if (isset($userDataArr['is_activated']) && $userDataArr['is_activated'] == '1') {
                        $messages = $this->_config['user_messages']['config']['user_activation']['VERIFICATION_ALREADY_DONE'];
                    } else {
                        $activateUser = $this->getUserTable()->activateUser(array('verify_code' => $verify_code));
                        $messages = $this->_config['user_messages']['config']['user_activation']['VERIFICATION_DONE'];
                    }

                    $viewModel->setVariables(array(
                        'referalUrl' => $referalUrl,
                        'verification_done' => true,
                        'success_msg' => $messages,
                        'jsLangTranslate' => $this->_config['user_messages']['config']['user_activation'],
                        'verification_code' => $verify_code)
                    );
                    return $viewModel;
                }
            }
        }

        if ($request->isPost()) {
            $form->setData($request->getPost());
            $form->setInputFilter($user->getInputFilterEmailVerify());
            if (!$form->isValid()) {
                $errors = $form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            $msg [$key] = $this->_config['user_messages']['config']['user_activation'][$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $user->exchangeArrayEmailVerify($form->getData());
                $userDataArr = $this->getUserTable()->getUser(array('verify_code' => $user->verify_code));

                if (empty($userDataArr)) {
                    $msg = array();
                    $msg [] = $this->_config['user_messages']['config']['user_activation']['WRONG_VERIFICATION_DONE'];
                    $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['user_activation']['WRONG_VERIFICATION_DONE']);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                } else {
                    if (isset($userDataArr['is_activated']) && $userDataArr['is_activated'] == '1') {
                        $success_messages = $this->_config['user_messages']['config']['user_activation']['VERIFICATION_ALREADY_DONE'];
                    } else {
                        $activateUser = $this->getUserTable()->activateUser(array('verify_code' => $user->verify_code));
                        $success_messages = $this->_config['user_messages']['config']['user_activation']['VERIFICATION_DONE'];
                    }
                    $messages = array('status' => "success", 'message' => $success_messages, 'user_id' => $this->encrypt($userDataArr['user_id']), 'referalUrlPN' => $user->referal_path_url);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                }
            }
        }
      
        $viewModel->setVariables(array(
            'form' => $form,
            'referalUrl' => $referalUrl,
            'response' => $response,
            'jsLangTranslate' => array_merge($this->_config['user_messages']['config']['forgot_password'],$this->_config['user_messages']['config']['user_activation']))
        );
        return $viewModel;
    }

    /**
     * This action is used to update user email in DB
     * @author Icreon Tech - DG
     * @return Array
     */
    public function updateEmailAction() {
        $auth = new AuthenticationService();
        if ($auth->hasIdentity() === true) {
            $user = $auth->getIdentity();
            if (isset($user->email_id)) {
                return $this->redirect()->toRoute('user-profile');
            }
        }

        $form = new UpdateEmailForm();
        $this->getUserTable();

        $user_config_msg = $this->_config['user_messages']['config']['update_email'];
        $request = $this->getRequest();
        $user = new User($this->_adapter);
        $viewModel = new ViewModel();

        $response = $this->getResponse();
        $messages = array();
        $params = $this->params()->fromRoute();
        if (isset($params['id']) && $params['id'] != '') {
            $form->get('user_id')->setValue($params['id']);
        }

        if ($request->isPost()) {
            $form->setData($request->getPost());
            $form->setInputFilter($user->getInputFilterUpdateEmail());
            if (!$form->isValid()) {
                $errors = $form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            $msg [$key] = $this->_config['user_messages']['config']['update_email'][$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $user->exchangeArrayUpdateEmail($form->getData());
                $userDataArr = $this->getUserTable()->updateEmail(array('email' => $user->email, 'user_id' => $this->decrypt($user->user_id)));

                if ($userDataArr) {
                    $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                    $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                    $signup_email_id_template_int = 1;
                    if ($userDataArr['user_type'] != 1) {
                        $userDataArr['first_name'] = $userDataArr['company_name'];
                    }
                    $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $signup_email_id_template_int, $this->_config['email_options']);


                    $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['update_email']['EMAIL_UPDATE_SUCCESS']);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                } else {
                    $msg = array();
                    $msg [] = $this->_config['user_messages']['config']['update_email']['EMAIL_NOT_UPDATE'];
                    $messages = array('status' => "error", 'message' => $msg);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                }
            }
        }

        $viewModel->setVariables(array(
            'form' => $form,
            'response' => $response,
            'jsLangTranslate' => $user_config_msg)
        );
        return $viewModel;
    }

    /**
     * This action is used to send email for verification
     * @author Icreon Tech - DG
     * @param json
     * @return Int
     */
    public function verifyEmailAction() {
        $auth = new AuthenticationService();
        if ($auth->hasIdentity() === true) {
            $user = $auth->getIdentity();
            if (isset($user->email_id)) {
                return $this->redirect()->toRoute('user-profile');
            }
        }
        $form = new ActivationForm();
        $viewModel = new ViewModel();
        $this->getUserTable();
        $request = $this->getRequest();

        $response = $this->getResponse();
        $messages = array();
        $user = new User($this->_adapter);

        $params = $this->params()->fromRoute();
        if (isset($params['id']) && $params['id'] != '') {
            $user_id = $params['id'];
        }
        if ($request->isPost()) {
            $form->setData($request->getPost());
            $form->setInputFilter($user->getInputFilterEmailVerify());
            if (!$form->isValid()) {
                $errors = $form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            $msg [$key] = $this->_config['user_messages']['config']['user_activation'][$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $user->exchangeArrayEmailVerify($form->getData());
                $userDataArr = $this->getUserTable()->getUser(array('verify_code' => $user->verify_code));

                if (empty($userDataArr)) {
                    $msg = array();
                    $msg [] = $this->_config['user_messages']['config']['user_activation']['WRONG_VERIFICATION_DONE'];
                    $messages = array('status' => "error", 'message' => $msg);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                } else {
                    $activateUser = $this->getUserTable()->activateUser(array('verify_code' => $user->verify_code));
                    $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['user_activation']['VERIFICATION_DONE'], 'user_id' => $this->encrypt($userDataArr['user_id']));
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                }
            }
        }
        $viewModel->setVariables(array(
            'form' => $form,
            'response' => $response,
            'user_id' => $user_id,
            'jsLangTranslate' => $this->_config['user_messages']['config']['user_activation'])
        );
        return $viewModel;
    }

    /**
     * This action is used to resend email for verification
     * @author Icreon Tech - DG
     * @param json
     * @return Int
     */
    public function resendEmailAction() {
        $auth = new AuthenticationService();
        if ($auth->hasIdentity() === true) {
            $user = $auth->getIdentity();
            if (isset($user->email_id)) {
                return $this->redirect()->toRoute('user-profile');
            }
        }
        $request = $this->getRequest();
        $response = $this->getResponse();
        $user_id = $request->getPost('user_id');
        if ($user_id) {
            $userDataArr = $this->getUserTable()->getUser(array('user_id' => $this->decrypt($user_id)));
            $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
            $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
            $signup_email_id_template_int = 1;
            if ($userDataArr['user_type'] != 1) {
                $userDataArr['first_name'] = $userDataArr['company_name'];
            }
            $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $signup_email_id_template_int, $this->_config['email_options']);
            $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['user_activation']['RESEND_MAIL_DONE']);
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
        die;
    }

    /**
     * This action is used to check is email exits
     * @author Icreon Tech - DG
     * @param json
     * @return boolean
     */
    public function isEmailExitsAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $viewModel = new ViewModel();
        if ($request->isPost()) {
            $form_data = $request->getPost();
            $form_data['email_id'];
            $isEmailExits = $this->getServiceLocator()->get('User\Model\UserTable')->isEmailExits(array('email_id' => $form_data['email_id']));
            $messages = array('status' => "success", 'result' => $isEmailExits);
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This action is used to verify user updated email id and activate account
     * @return     array 
     * @param array $form
     * @author Icreon tech - DG
     */
    public function verifyUpdateEmailAction() {
        $auth = new AuthenticationService();
        if ($auth->hasIdentity() === true) {
            $user = $auth->getIdentity();
            if (isset($user->email_id)) {
                //return $this->redirect()->toRoute('user-profile');
                
                $userArr = $auth->getIdentity();
                if (isset($userArr->crm_email_id)) {
                    $crm_user_id = $userArr->crm_user_id;
                    $this->getServiceLocator()->get('User\Model\UserTable')->updateCrmUserLogins(array('crm_user_id' => $crm_user_id, 'crm_active' => '0'));
                } else {
                    $user_id = $userArr->user_id;
                    $this->getServiceLocator()->get('User\Model\UserTable')->updateUserLogins(array('user_id' => $user_id, 'active' => '0'));
                }
                $this->getSessionStorage()->forgetMe();
                $auth->clearIdentity();
                $session = new SessionContainer();
                $session->getManager()->destroy(array('send_expire_cookie' => true, 'clear_storage' => true));
                $session->getManager()->getStorage()->clear();
            }
        }
        $viewModel = new ViewModel();
        $this->getUserTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        $errorMessage = '';
        $successMessages = '';
        if (isset($params['verification_code']) && $params['verification_code'] != '') {
            $verify_code = $params['verification_code'];
            if (isset($verify_code) && $verify_code != '') {
                $userDataArr = $this->getUserTable()->getUser(array('verify_code' => $verify_code));
                if (empty($userDataArr)) {
                    $errorMessage = $this->_config['user_messages']['config']['user_activation']['WRONG_VERIFICATION_DONE'];
                } else {
                    $alternateEmailId = $userDataArr['alternate_email_id'];
                    $isEmail = $this->getServiceLocator()->get('User\Model\UserTable')->isEmailExits(array('email_id' => $alternateEmailId));
                    if (!empty($isEmail)) {
                        $errorMessage = $this->_config['user_messages']['config']['user_activation']['EMAIL_ALREADY_EXITS'];
                    } else {
                        $this->getServiceLocator()->get('User\Model\UserTable')->updateEmailInfo(array('alternate_email_id' => $alternateEmailId, 'user_id' => $userDataArr['user_id']));
                        $successMessages = $this->_config['user_messages']['config']['user_activation']['EMAIL_UPDATED_SUCCESS'];
                    }
                }
            }
        }
        $viewModel->setVariables(array(
            'successMessages' => $successMessages,
            'errorMessage' => $errorMessage,
            'jsLangTranslate' => $this->_config['user_messages']['config']['user_activation'])
        );
        return $viewModel;
    }
    
     /**
     * This function is used to get auth object
     * @return     array 
     * @param array
     * @author  Icreon Tech - DG
     */
    public function getSessionStorage() {
        if (!$this->_storage) {
            $this->_storage = $this->getServiceLocator()
                    ->get('User\Model\AuthStorage');
        }
        return $this->_storage;
    }
    

    /**
     * This action is used to refresh captcha
     * @return     array 
     * @param array $form
     * @author Icreon tech - DG
     */
    public function refreshCaptchaAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getUserTable();
        $dirdata = './public';
        //pass captcha image options
        $captchaImage = new CaptchaImage(array(
                    'font' => $dirdata . '/fonts/opensans-bold-webfont.ttf',
                    'width' => 200,
                    'Expiration' => '100',
                    'height' => 80,
                    'WordLen' => 6,
                    'dotNoiseLevel' => 20,
                    'lineNoiseLevel' => 2)
        );
        $captchaImage->setImgDir($this->_config['file_upload_path']['assets_upload_dir'] . "captcha/");
        $captchaImage->setImgUrl($this->_config['file_upload_path']['assets_url'] . "captcha/");
        $captchaImage->generate();
        $messages = array('status' => "success", 'captchaId' => $captchaImage->getId(), 'path' => $this->_config['file_upload_path']['assets_url'] . "captcha/");
        return $response->setContent(\Zend\Json\Json::encode($messages));
    }

	public function resendActivationCodeAction() {
        $form = new ResendActivationCodeForm();
        $this->getUserTable();

        $request = $this->getRequest();
        $user = new User($this->_adapter);
        $viewModel = new ViewModel();
        $this->layout('web-popup');
        $response = $this->getResponse();
        $messages = array();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            $form->setInputFilter($user->getInputFilterForgot());
            if (!$form->isValid()) {
                $errors = $form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            $msg [$key] = $this->_config['user_messages']['config']['forgot_password'][$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $user->exchangeArrayForgot($form->getData());

                $userDataArr = $this->getUserTable()->getUser(array('email_id' => $user->email_id,'include_deleted'=>1));
				
                if (empty($userDataArr)) {
                    $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['forgot_password']['EMAIL_NOT_EXITS']);
                }elseif($userDataArr['is_activated'] == '1' && $userDataArr['is_active'] == '1' && $userDataArr['is_delete']=='0'){
					$messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['forgot_password']['EMAIL_ALREADY_ACTIVATED']);
				}elseif($userDataArr['is_delete']=='1'){
					$messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['forgot_password']['ACCOUNT_DELETED']);
				}else {
                    $userDataArr = $this->getUserTable()->getUser(array('user_id' => $userDataArr['user_id']));
                    $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                    $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                    $signup_email_id_template_int = 1;
                    $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $signup_email_id_template_int, $this->_config['email_options']);

                    $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['user_activation']['VERIFICATION_DONE_LINK_SENT_SUCCESS']);
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $viewModel->setVariables(array(
            'form' => $form,
            'response' => $response,
            'jsLangTranslate' => array_merge($this->_config['user_messages']['config']['forgot_password'],$this->_config['user_messages']['config']['user_activation']))
        );

        return $viewModel;
    }

	public function getAuthServiceByEmail() { 
        if (!$this->_authservice) { 
            $this->_authservice = $this->getServiceLocator()->get('AuthServiceByEmail');
        }
		
        return $this->_authservice;
    }

	public function directLoginBySignup($user_name,$password) {
        
        $auth = new AuthenticationService();
        
        $this->getUserTable();
        
          
        $this->getAuthServiceByEmail()->getAdapter()
                                        ->setIdentity($user_name)
                                        ->setCredential($password);

        $result = $this->getAuthServiceByEmail()->authenticate();
        $columnsToReturn = array('user_id', 'user_type', 'title', 'first_name', 'middle_name', 'last_name', 'username', 'email_id', 'email_verified', 'verification_code', 'is_activated', 'is_active', 'is_delete', 'is_blocked', 'blocked_date', 'social_account_id', 'social_account_type', 'company_name', 'user_membership_id', 'terms_version_id');
		$user = $this->getAuthServiceByEmail()->getAdapter()->getResultRowObject($columnsToReturn);

		$this->getAuthServiceByEmail()->getStorage()->write($user);
               
    }
    public function directLoginByUsernameSignup($user_name,$password) {
        
        $auth = new AuthenticationService();
        
        $this->getUserTable();
   
        $this->getAuthServiceByUsername()->getAdapter()
                            ->setIdentity($user_name)
                            ->setCredential($password);
        $result = $this->getAuthServiceByUsername()->authenticate();
  
        $columnsToReturn = array('user_id', 'user_type', 'title', 'first_name', 'middle_name', 'last_name', 'username', 'email_id', 'email_verified', 'verification_code', 'is_activated', 'is_active', 'is_delete', 'is_blocked', 'blocked_date', 'social_account_id', 'social_account_type', 'company_name', 'user_membership_id', 'terms_version_id');
		$user = $this->getAuthServiceByUsername()->getAdapter()->getResultRowObject($columnsToReturn);

		$this->getAuthServiceByUsername()->getStorage()->write($user);
               
    }
     /**
     * This action is used for storing user visitation record after sign up.
     * @return  array 
     * @param array $form
     * @author Icreon Tech - DG
     */
    public function visitationRecordAction(){
         $auth = new AuthenticationService();
        if ($auth->hasIdentity() === true) {
            $userDetails = $auth->getIdentity();
            if (isset($userDetails->email_id)) {
               // $this->checkFrontUserSessionPopup();
            }
        }
        $currDate = date('Y-m-d',strtotime(DATE_TIME_FORMAT));
        $this->getUserTable();
        
        // add - n - start
        $params = $this->params()->fromRoute();

        $form = new UserVisitRecordForm();
        $this->getUserTable();
 
        $request = $this->getRequest();
        $user = new User($this->_adapter);
        $viewModel = new ViewModel();
        $this->layout('web-popup');
        $response = $this->getResponse();
        $messages = array();
        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
        $configVal = $this->_config['user_visit_record'];
        $country_list = array();
        $country_list[''] = 'SELECT COUNTRY';
        foreach ($country as $key => $val) {
            $country_list[$val['country_id']] = $val['name'];
        }
        $configVal['birth_year'][''] = 'SELECT BIRTH YEAR';
        $configVal['immagration_year'][''] = 'SELECT YEAR';
        $configVal['occupation'][''] = 'SELECT OCCUPATION';
        $form->get('birth_country')->setAttribute('options', $country_list);
        $form->get('birth_year')->setAttribute('options', $configVal['birth_year']);
        $form->get('immigration_year')->setAttribute('options', $configVal['immagration_year']);
        $form->get('occupation')->setAttribute('options', $configVal['occupation']);
        $form->get('arrival_date')->setAttribute('value',$currDate);
        
       if($request->isGet()){
            $urlParams = $this->params()->fromRoute();
            //$userId = $this->decrypt($urlParams['user_id']);
            $searchParam['user_id'] = $userDetails->user_id;
            $userRecord = $this->getServiceLocator()->get('Kiosk\Model\KioskReservationTable')->getUserVisitRecord($searchParam); 
            $userVisitRecord = $userRecord[0];
            $form->get('user_id')->setAttribute('value',$userDetails->user_id);
            $form->get('visiting_with')->setAttribute('value', $userVisitRecord['visiting_with']);
            $form->get('birth_year')->setAttribute('value', $userVisitRecord['birth_year']);
            $form->get('gender')->setAttribute('value', $userVisitRecord['gender']);
            //$form->get('ethnicity')->setAttribute('value', $userVisitRecord['ethnicity']);
            //$form->get('nationality')->setAttribute('value', $userVisitRecord['nationality']);
            $form->get('birth_country')->setAttribute('value', $userVisitRecord['birth_country']);
            $form->get('immigration_year')->setAttribute('value', $userVisitRecord['immigration_year']);
            $form->get('port_entry')->setAttribute('value', $userVisitRecord['port_entry']);
            $form->get('occupation')->setAttribute('value', $userVisitRecord['occupation']);
            $form->get('message')->setAttribute('value', $userVisitRecord['message']);
			
			$getNationality = $this->getServiceLocator()->get('Common\Model\CommonTable')->getNationality('');
			$nationality = array();
			$nationalityDesc = array();
			$nationality[' '] = 'Select';
			
			foreach ($getNationality as $key => $val) {
				$nationality[$val['nationality_id']] = $val['nationality'];
				$nationalityDesc[$val['nationality_id']] = $val['description'];
			}
			$form->get('nationality')->setAttribute('options', $nationality);

			$getEthinicity = $this->getServiceLocator()->get('Common\Model\CommonTable')->getEthinicity('');
			$ethnicity = array();
			$ethnicityDesc = array();
			$ethnicity[' '] = 'Select';
			foreach ($getEthinicity as $key => $val) {
				$ethnicity[$val['ethnicity_id']] = $val['ethnicity'];
				$ethnicityDesc[$val['ethnicity_id']] = $val['description'];
			}
			$form->get('ethnicity')->setAttribute('options', $ethnicity);

          
        }


        if ($request->isPost()) {
            $form->setData($request->getPost());
            $form->setInputFilter($user->getInputFilterVisitRecord());
            if (!$form->isValid()) {
                $errors = $form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            if(isset($this->_config['user_messages']['config']['user_signup'][$rower]) and trim($this->_config['user_messages']['config']['user_signup'][$rower]) != "") {
                                  $msg[$key] = $this->_config['user_messages']['config']['user_signup'][$rower];                                 
                                }
                            
                        }
                    }
                }
	  if(!empty($msg))
		$messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
   
                $formData = $form->getData();
                $saveParam['user_id'] = $formData['user_id'];
                $saveParam['arrival_date'] = date('Y-m-d',  strtotime(DATE_TIME_FORMAT));
                $saveParam['visiting_with'] = $formData['visiting_with'];
                $saveParam['gender'] = $formData['gender'];
                $saveParam['birth_year'] = $formData['birth_year'];
                $saveParam['matrial_status'] = $formData['matrial_status'];
                $saveParam['ethnicity'] = $formData['ethnicity'];
                $saveParam['birth_country'] = $formData['birth_country'];
                $saveParam['immigration_year'] = $formData['immigration_year'];
                $saveParam['port_of_entry'] = $formData['port_entry'];
                $saveParam['occupation'] = $formData['occupation'];
                $saveParam['message'] = $formData['message'];
                $saveParam['created_at'] = date('Y-m-d H:i:s',  strtotime(DATE_TIME_FORMAT));
                //Data saving function
                
                $userVistorId = $this->getUserTable()->saveUserVisitationRecord($saveParam);

                // End code to redirect the page 
				$emailId = "";
                 if($userDetails->email_id){
					$emailId = $userDetails->email_id;
				 }              
                $messages = array('status' => "success", 'email_id'=>$emailId);                
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $viewModel->setVariables(array(
            'form' => $form,
            'response' => $response,
            'jsLangTranslate' => $this->_config['user_messages']['config']['user_visit_record'],
            'currDate' => $currDate
        ));

        return $viewModel;
        
    }
     /**
     * This action is used to create user name and save user data
     * @param array
     * @return array
     * @author Icreon Tech-AP
     */
    public function userAddressInfoAction() {
        $currDate = date('Y-m-d',strtotime(DATE_TIME_FORMAT));
        $this->getUserTable();
        
        // add - n - start
        $params = $this->params()->fromRoute();
        $form = new UserAddressForm();
        $this->getUserTable();
 
        $request = $this->getRequest();
        $user = new User($this->_adapter);
        $viewModel = new ViewModel();
        $this->layout('popup');
        $response = $this->getResponse();
        $messages = array();
        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
        $configVal = $this->_config['user_visit_record'];
        $country_list = array();
        $country_list[''] = 'SELECT COUNTRY';
        foreach ($country as $key => $val) {
            $country_list[$val['country_id']] = $val['name'];
        }
         if($request->isGet()){
            $urlParams = $this->params()->fromRoute();
          
        }
        if ($request->isPost()) {
                
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            
        }
        $flashMessages = array();
        if ($this->_flashMessage->hasMessages()) {
            $flashMessages = $this->_flashMessage->getMessages();
        }
        $viewModel->setVariables(array(
            'form' => $contactForm,
            'jsLangTranslate' => $kioskMessages,
            'messages' => $flashMessages,
            'contactName' =>$currentContactName
        ));
        return $viewModel;
    }
    /**
     * This action is used to check is username exits
     * @author Icreon Tech - DG
     * @param json
     * @return boolean
     */
    public function isUsernameExistAction() {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $viewModel = new ViewModel();
        if ($request->isPost()) {
            $form_data = $request->getPost();
            $isUsernameExist = $this->getServiceLocator()->get('User\Model\UserTable')->isUsernameExist(array('username' => $form_data['username']));
            $messages = array('status' => "success", 'result' => $isUsernameExist);
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

}
