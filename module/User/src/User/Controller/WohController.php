<?php

/**
 * This controller is used to display Woh detail
 * @package    User
 * @author     Icreon Tech - NS
 */

namespace User\Controller;

use Base\Model\UploadHandler;
use Base\Controller\BaseController;
use Zend\Authentication\AuthenticationService;
use Zend\View\Model\ViewModel;
use User\Model\Woh;
use Zend\Mvc\Controller\Plugin\Params;
use Zend\Http\Request;
use User\Form\WohForm;
use User\Form\WohSearchForm;
use User\Form\WohBioCertificateForm;
use User\Form\WohSearchSaveForm;
use Transaction\Model\Transaction;
use Zend\Session\Container;
use Cms\Controller\CmsController;

/**
 * This controller is used for wall of honour
 * @package    User
 * @author     Icreon Tech - NS
 */
class WohController extends BaseController {

    
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_authservice = null;
    protected $_wohTable = null;
    protected $_productTable = null;
    protected $_transactionTable = null;
    protected $_cmsTable = null;
    public $auth = null;

    public function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        $auth = new AuthenticationService();
        $this->auth = $auth;
        
        $moduleSession = new Container('moduleSession');
        $moduleSession->moduleGroup;
    }

    /**
     * This function is used to get table connections
     * @return     array 
     * @author Icreon Tech - NS
     */
    public function getWohTables() {
        $sm = $this->getServiceLocator();
        $this->_wohTable = $sm->get('User\Model\WohTable');
        $this->_userTable = $sm->get('User\Model\UserTable');
        $this->_productTable = $sm->get('Product\Model\ProductTable');
        $this->_transactionTable = $sm->get('Transaction\Model\TransactionTable');
        $this->_cmsTable = $sm->get('Cms\Model\CmsTable');
        $this->_adapter = $sm->get('dbAdapter');
        $this->_auth = new AuthenticationService();
        return $this->_wohTable;
    }

    /**
     * This function is used to get config variables
     * @return     array 
     * @author Icreon Tech - NS
     */
    public function getConfig() {
        $sm = $this->getServiceLocator();
        $this->_translator = $sm->get('translator');
        $this->_config = $sm->get('Config');
    }

    /**
     * This function is used to index Woh
     * @return index Page      
     * @author Icreon Tech - NS
     */
    public function indexAction() {
        
    }

    /**
     * This function is used to about Woh
     * @return About Us Page      
     * @author Icreon Tech - NS
     */
    public function aboutWohAction() {
        $this->getWohTables();
        $this->getConfig();
        $jsLangTranslate = $this->_config['user_messages']['config']['user_woh'];

        $param = array();
        $param['sort_field'] = 'usr_wall_of_honor.woh_id';
        $param['sort_order'] = 'desc';
        $param['record_limit'] = 6;
        $wohData = $this->getWohTables()->getUserWoh($param);

        $wohArrHonoreeName = array();
        if (!empty($wohData)) {
            foreach ($wohData as $key => $val) {
                $namingConventionArr = $this->getNamingConventionArr($val['attribute_option_name'], '', $val);
                $wohArrHonoreeName[$key]['honoree_name'] = $namingConventionArr['honoree_name'];
            }
        }

        $searchWohForm = new WohSearchForm();
        $searchWohForm->setAttribute('action', '/search-woh');

        $searchParam = array();
        $searchParam['type'] = '';
        $searchParam['status'] = 0;
        $searchParam['content_id'] = '14';
        $searchParam['startIndex'] = "";
        $searchParam['recordLimit'] = "";
        $searchParam['sortField'] = "";
        $searchParam['sortOrder'] = "";
        $pageContentArr = $this->_cmsTable->getCrmContents($searchParam);
        $content_title = isset($pageContentArr[0]['content_title']) ? $pageContentArr[0]['content_title'] : '';
        $description = isset($pageContentArr[0]['description']) ? $pageContentArr[0]['description'] : '';

        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'translate' => $jsLangTranslate,
            'searchWohForm' => $searchWohForm,
            'wohArrHonoreeName' => $wohArrHonoreeName,
            'content_title' => $content_title,
            'description' => $description,
        ));
        return $viewModel;
    }

    /**
     * This function is used to search Woh
     * @return Search Page      
     * @author Icreon Tech - NS
     */
    public function searchWohAction() {
	    $moduleSession = new Container('moduleSession');
	    $this->layout('wohlayout');
	    $moduleSession->moduleGroup;
        if($moduleSession->moduleGroup!=3)
            $this->checkFrontUserAuthentication();
            
        $this->getWohTables();
        $this->getConfig();


        $request = $this->getRequest();
        $params = $this->params()->fromRoute();

		$posTransactionSession = new Container('posTransactionSession'); /* pos session */
        //$posTransactionSession->posTransactionData = array();

        $first_name = $request->getPost('first_name');
        $first_name = isset($first_name)?$first_name:'';

        $first_name_initial = $request->getPost('first_name_initial');
        $first_name_initial = isset($first_name_initial)?$first_name_initial:$first_name;

        $last_name = $request->getPost('last_name');
        $last_name = isset($last_name)?$last_name:'';

        $panel_number = $request->getPost('panel_number');
        $panel_number = isset($panel_number)?$panel_number:'';

            
            
            $other_part_name = '';
            $country = '';
            $donated_by = '';
             
            //// Start ----
            $other_part_name = $request->getPost('other_part_name');
            $other_part_name = isset($other_part_name)?$other_part_name:'';

            $country = $request->getPost('country');
            $country = isset($country)?$country:'';

            $donated_by = $request->getPost('donated_by');
            $donated_by_add = $request->getPost('donated_by_add');
            $donated_by_add = isset($donated_by_add)?$donated_by_add:'';
            $donated_by = isset($donated_by)?$donated_by:$donated_by_add;
            //// End ----
            
            
        $jsLangTranslate = $this->_config['user_messages']['config']['user_woh'];

        $page = 1;
        $sort_field = $param['sort_field'] = 'usr_wall_of_honor.woh_id';
        $sort_order = $param['sort_order'] = 'asc';
            $numRecPerPage = 6;
        $limit = $param['record_limit'] = $numRecPerPage;
        $start_index = ($page - 1) * $numRecPerPage;

        $wohSearchSaveForm = new WohSearchSaveForm();
        $searchWohForm = new WohSearchForm();


        $searchWohForm->get('page')->setAttribute('value', $page);
        $searchWohForm->get('sort_field')->setAttribute('value', $sort_field);
        $searchWohForm->get('sort_order')->setAttribute('value', $sort_order);
        $searchWohForm->get('start_index')->setAttribute('value', $start_index);
        $searchWohForm->get('limit')->setAttribute('value', $limit);



        $click_search = false;
            if((isset($first_name_initial) and trim($first_name_initial) != "")  or (isset($last_name) and trim($last_name) != "") or (isset($panel_number) and trim($panel_number) != "") or (isset($other_part_name) and trim($other_part_name) != "")  or (isset($country) and trim($country) != "")  or (isset($donated_by) and trim($donated_by) != "")) {
            $click_search = true;
        }

        if ($this->_auth->hasIdentity() == true and isset($this->_auth->getIdentity()->user_id)) {
            $data = array();
            $data['w_user_id'] = $this->_auth->getIdentity()->user_id;
            $data['woh_search_id'] = '';
            $saved_searches = $this->getWohTables()->getUserSearches($data);

                   $memberShipConfig =  $this->getMembershipId($this->_auth->getIdentity()->user_id, 'Wall of Honor');
                   $numMemberWallOfHonour = isset($memberShipConfig)?$memberShipConfig:'';
             }
             else {
            $saved_searches = array();
            $numMemberWallOfHonour = '';
        }



             
            if(isset($params['woh_search_id']) and trim($params['woh_search_id']) != "") { 
            $woh_search_id = trim($params['woh_search_id']);
            $search_id = $this->decrypt($woh_search_id);

            if ($search_id != "0") {
                $dataParam = array();
                $dataParam['w_user_id'] = "";
                $dataParam['woh_search_id'] = $search_id;

                $arrSavedSearches = $this->getWohTables()->getUserSearches($dataParam);
                $savedSearchesSearchQuery = isset($arrSavedSearches[0]['search_query']) ? $arrSavedSearches[0]['search_query'] : '';
                if ($savedSearchesSearchQuery != '') {
                    $savedSearchesSearchQueryUnserialize = unserialize($savedSearchesSearchQuery);
                    $click_search = true;
                   
                } else {
                    $savedSearchesSearchQueryUnserialize = array();
                }
            } else {
                $savedSearchesSearchQueryUnserialize = array();
            }

            $last_name = isset($savedSearchesSearchQueryUnserialize['last_name']) ? $savedSearchesSearchQueryUnserialize['last_name'] : '';
            $first_name_initial = isset($savedSearchesSearchQueryUnserialize['first_name']) ? $savedSearchesSearchQueryUnserialize['first_name'] : '';
            $other_part_name = isset($savedSearchesSearchQueryUnserialize['other_part_name']) ? $savedSearchesSearchQueryUnserialize['other_part_name'] : '';
            $country = isset($savedSearchesSearchQueryUnserialize['country']) ? $savedSearchesSearchQueryUnserialize['country'] : '';
            $donated_by = isset($savedSearchesSearchQueryUnserialize['donated_by']) ? $savedSearchesSearchQueryUnserialize['donated_by'] : '';
            $panel_number = isset($savedSearchesSearchQueryUnserialize['panel_number']) ? $savedSearchesSearchQueryUnserialize['panel_number'] : '';
        }


           if(isset($params['woh_id']) and trim($params['woh_id']) != "") {    
            $woh_id = trim($params['woh_id']);
            $wall_of_honour_id = $this->decrypt($woh_id);

                if($wall_of_honour_id != "0") {
                $wohArrRecord = $this->_userTable->getWoh(array('woh_id' => $wall_of_honour_id));
                $click_search = true;
                }
                else {
                $wohArrRecord = array();
            }
                if(isset($wohArrRecord) and count($wohArrRecord) > 0) {
                    $donated_by = isset($wohArrRecord[0]['donated_by'])?$wohArrRecord[0]['donated_by']:'';

                    $first_name_one = isset($wohArrRecord[0]['first_name_one'])?$wohArrRecord[0]['first_name_one']:'';
                    $first_name_two = isset($wohArrRecord[0]['first_name_two'])?$wohArrRecord[0]['first_name_two']:'';

                    if($first_name_one != '') { $first_name_initial = $first_name_one; }
                    if($first_name_two != '') { $first_name_initial = $first_name_two; }


                    $last_name_one = isset($wohArrRecord[0]['last_name_one'])?$wohArrRecord[0]['last_name_one']:'';
                    $last_name_two = isset($wohArrRecord[0]['last_name_two'])?$wohArrRecord[0]['last_name_two']:'';

                    if($last_name_one != '') { $last_name = $last_name_one; }
                    if($last_name_two != '') { $last_name = $last_name_two; }


                    $other_init_one = isset($wohArrRecord[0]['other_init_one'])?$wohArrRecord[0]['other_init_one']:'';
                    $other_name = isset($wohArrRecord[0]['other_name'])?$wohArrRecord[0]['other_name']:'';
                    $other_init_two = isset($wohArrRecord[0]['other_init_two'])?$wohArrRecord[0]['other_init_two']:'';
                    $first_line = isset($wohArrRecord[0]['first_line'])?$wohArrRecord[0]['first_line']:'';
                    $second_line = isset($wohArrRecord[0]['second_line'])?$wohArrRecord[0]['second_line']:'';

                    if($other_init_one != '') { $other_part_name = $other_init_one; }
                    if($other_name != '') { $other_part_name = $other_name; }
                    if($other_init_two != '') { $other_part_name = $other_init_two; }
                    if($first_line != '') { $other_part_name = $first_line; }
                    if($second_line != '') { $other_part_name = $second_line; }

                    $panel_number = isset($wohArrRecord[0]['panel_no'])?$wohArrRecord[0]['panel_no']:'';

                    $country = isset($wohArrRecord[0]['origin_country'])?$wohArrRecord[0]['origin_country']:'';
         
            }
        }


            $first_name_initial_p = 'First Name';   
            $last_name_p = 'Last Name'; 
            $panel_number_p = 'Panel #';  
            $other_part_name_p = 'Other Part of Name';  
            $country_p = 'Country of Origin';   
            $donated_by_p = 'Donated By';  

            $searchWohForm->get('first_name')->setAttribute('placeholder', $first_name_initial_p);
            $searchWohForm->get('last_name')->setAttribute('placeholder', $last_name_p);            
            $searchWohForm->get('panel_number')->setAttribute('placeholder', $panel_number_p);
            $searchWohForm->get('other_part_name')->setAttribute('placeholder', $other_part_name_p);
            $searchWohForm->get('country')->setAttribute('placeholder', $country_p);
            $searchWohForm->get('donated_by')->setAttribute('placeholder', $donated_by_p);
            
            $wohSearchSaveForm->get('save_search_as')->setAttribute('placeholder', 'Save this search');
       
        $searchWohForm->get('first_name')->setAttribute('value', $first_name_initial);
        $searchWohForm->get('last_name')->setAttribute('value', $last_name);
        $searchWohForm->get('panel_number')->setAttribute('value', $panel_number);
        $searchWohForm->get('other_part_name')->setAttribute('value', $other_part_name);
        $searchWohForm->get('country')->setAttribute('value', $country);
        $searchWohForm->get('donated_by')->setAttribute('value', $donated_by);

       $messages = array();  
       if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }

        $objWohCMS = new CmsController();
        
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'jsLangTranslate' => $jsLangTranslate,
            'searchWohForm' => $searchWohForm,
            'saved_searches' => $saved_searches,
            'click_search' => $click_search,
            'numMemberWallOfHonour' => $numMemberWallOfHonour,
            'wohSearchSaveForm' => $wohSearchSaveForm,
            'messages' => $messages,
            'moduleGroup'=>$moduleSession->moduleGroup,
            'wallOfHonorListingSearch' => $objWohCMS->wallOfHonorListingSearch('search-woh',array('first_name'=>$first_name_initial,'last_name' => $last_name,'panel_number' => $panel_number,'other_part_name' => $other_part_name,'country' => $country,'donated_by' => $donated_by),$this->_config['js_file_path']['path'],$this->_config['file_versions']['js_version'])
        ));
        return $viewModel;
    }

    
    /**
     * This function is used to save Woh Search 
     * @return Search Page      
     * @author Icreon Tech - NS
     */
    public function saveWohSearchAction() {
        try {
            $this->getConfig();
            $this->getWohTables();
            $request = $this->getRequest();
            $response = $this->getResponse();
            $messages = array();

            if ($request->isPost()) {
                $searchName = $request->getPost('search_name');
                parse_str($request->getPost('searchString'), $searchParam);
                if (trim($searchName) != '') {
                    $saveSearchParam = array();
                    $saveSearchParam['user_id'] = $this->_auth->getIdentity()->user_id;
                    $saveSearchParam['title'] = $searchName;
                    $saveSearchParam['search_query'] = serialize($searchParam);
                    $saveSearchParam['save_date'] = DATE_TIME_FORMAT;
                    $saveSearchParam['modified_date'] = DATE_TIME_FORMAT;
                    
                    $memberShipConfig =  $this->getMembershipId($this->_auth->getIdentity()->user_id, 'Wall of Honor');
                    $data = array();
                    $data['w_user_id'] = $this->_auth->getIdentity()->user_id;
                    $data['woh_search_id'] = '';
                    $savedSearches = $this->getWohTables()->getUserSearches($data);
                    
                    if($memberShipConfig<=count($savedSearches)){
                        $messages = array('status' => "exceed");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }elseif ($this->getWohTables()->saveUserSearches($saveSearchParam) == true) {
                                 $jsLangTranslate = $this->_config['user_messages']['config']['user_woh'];
                                 //$this->flashMessenger()->addMessage($jsLangTranslate['WOH_SEARCH_SUCCESS_MSG']);

                                // start 
                                  if($this->_auth->hasIdentity()==true and isset($this->_auth->getIdentity()->user_id)){
                                         $data = array();
                                         $data['w_user_id'] = $this->_auth->getIdentity()->user_id;
                                         $data['woh_search_id'] = '';
                                         $savedSearches = $this->getWohTables()->getUserSearches($data);

                                          $htmlData = '';
                                          if(count($savedSearches) > 0 and $savedSearches != false) {
                                              if(isset($savedSearches) and count($savedSearches)<4) { $scrollCls = ''; } else  { $scrollCls = 'scroll-pane featured-scroll'; }
                                              $htmlData = '<div class="dropdown">';
                                              $htmlData .= '<div class="'.$scrollCls.'">';
                                              $htmlData .= '<ul>';
                                              foreach($savedSearches as $vals) {                    
                                                  $htmlData .= '<li>';
                                                  $htmlData .= '<span>'.$vals['title'].'</span>';
                                                  $htmlData .= '<a href="#deletesearch" class="deletesearch sn-close" onClick="deleteSaveSearch('."'".$this->encrypt($vals['woh_search_id'])."'".');"></a>';
                                                  $htmlData .= '<a class="sn-search" onclick="viewSavedSearch('."'".$this->encrypt($vals['woh_search_id'])."'".');"></a>';
                                                  $htmlData .= '</li>';
                    }
                                              $htmlData .= '</ul>'; 
                                              $htmlData .= '</div>';
                                              $htmlData .= '</div>';
                }
                                          else {
                                              $htmlData = '';
            }
                                   }
                                   else {
                                         $htmlData = '';
                                   }
                                  // end
                     
                     
                                 $messages = array('status' => "success",'message' => $jsLangTranslate['WOH_SEARCH_SUCCESS_MSG'],'dataresult' => $htmlData);
                                 return $response->setContent(\Zend\Json\Json::encode($messages));
                             }
                         }
                    }
               }
             catch(Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to DELETE Woh Search 
     * @return Search Page      
     * @author Icreon Tech - NS
     */
      public function deleteWohSearchAction(){
        try {
                $this->getConfig();
                $this->getWohTables();
            $response = $this->getResponse();
            $request = $this->getRequest();
                if($request->isPost()){
                $data = $request->getPost()->toArray();
                $data['woh_search_id'] = (isset($data['woh_search_id']) and trim($data['woh_search_id']) != "") ? $this->decrypt($data['woh_search_id']) : ""; 
                $this->getWohTables()->deleteUserSearches($data);
                    
                    $jsLangTranslate = $this->_config['user_messages']['config']['user_woh'];
                   // $this->flashMessenger()->addMessage($jsLangTranslate['WOH_SEARCH_SUCCESS_DELETED_MSG']);
                    
                    // start 
                    if($this->_auth->hasIdentity()==true and isset($this->_auth->getIdentity()->user_id)){
                           $data = array();
                           $data['w_user_id'] = $this->_auth->getIdentity()->user_id;
                           $data['woh_search_id'] = '';
                           $savedSearches = $this->getWohTables()->getUserSearches($data);
                           
                            $htmlData = '';
                            if(count($savedSearches) > 0 and $savedSearches != false) {
                                if(isset($savedSearches) and count($savedSearches)<4) { $scrollCls = ''; } else  { $scrollCls = 'scroll-pane featured-scroll'; }

                                $htmlData = '<div class="dropdown">';
                                $htmlData .= '<div class="'.$scrollCls.'">';
                                $htmlData .= '<ul>';
                                foreach($savedSearches as $vals) {                    
                                    $htmlData .= '<li>';
                                    $htmlData .= '<span>'.$vals['title'].'</span>';
                                    $htmlData .= '<a href="#deletesearch" class="deletesearch sn-close" onClick="deleteSaveSearch('."'".$this->encrypt($vals['woh_search_id'])."'".');"></a>';
                                    $htmlData .= '<a class="sn-search" onclick="viewSavedSearch('."'".$this->encrypt($vals['woh_search_id'])."'".');"></a>';
                                    $htmlData .= '</li>';
                                }
                                $htmlData .= '</ul>'; 
                                $htmlData .= '</div>';
                                $htmlData .= '</div>';
                            }
                            else {
                                $htmlData = '';
                            }
                     }
                     else {
                           $htmlData = '';
                     }
                    // end
                    $messages = array('status' => "success", 'message' => $jsLangTranslate['WOH_SEARCH_SUCCESS_DELETED_MSG'],'dataresult' => $htmlData);
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
            }
            catch(Exception $e) {
            return false;
        }
    }

    
      
    /**
     * This function is used to get Saved Woh Search Select
     * @return Search Page      
     * @author Icreon Tech - NS
     */
    public function getSavedWohSearchSelectAction() {
        try {
            $this->getConfig();
            $this->getWohTables();
            $request = $this->getRequest();
            $response = $this->getResponse();
            if ($request->isPost()) {
                if ($request->getPost('woh_search_id') != '') {
                    $search_id = $request->getPost('woh_search_id');
                    $dataParam = array();
                    $dataParam['w_user_id'] = $this->_auth->getIdentity()->user_id;
                    $dataParam['woh_search_id'] = $this->decrypt($search_id);
                    $saved_searches = $this->getWohTables()->getUserSearches($dataParam);
                    $searchResult = $saved_searches[0];

                    $searchResult2 = json_encode(unserialize($searchResult['search_query']));
                    return $response->setContent($searchResult2);
                }
            }
         }
        catch(Exception $e) {
            return false;
        }
    }

    
    
    /**
     * This function is used to get Woh List
     * @return Search Page
     * @author Icreon Tech - NS
     */
    public function getWohListAction(){
        $this->getConfig();
        $this->getWohTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $woh_message = $this->_config['user_messages']['config']['user_woh'];
        $page = 1;
        $view_woh_type = "1";
        $sort_field = $param['sort_field'] = 'usr_wall_of_honor.woh_id';
        $sort_order = $param['sort_order'] = 'desc';
        $numRecPerPage = 10;
        $limit = $param['record_limit'] = $numRecPerPage;
        $start_index = ($page - 1) * $numRecPerPage;
        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $data = $request->getPost()->toArray();
            parse_str($data['searchString']);

            $view_woh_type = isset($view_woh_type)?$view_woh_type:"1";
            $param['last_name'] = $last_name;
            $param['first_name'] = $first_name;
            $param['other_part_name'] = $other_part_name;
            $param['country'] = $country;

            $param['donated_by'] = $donated_by;
            $param['panel_number'] = $panel_number;

            $param['is_panel_num'] = "1";

            $param['sort_field'] = $sort_field;
            $param['sort_order'] = $sort_order;



            $param['record_limit'] = '';
            $wohDataCount = $this->getWohTables()->getUserWohCount($param);
            $countResult = (isset($wohDataCount[0]['countWohId']) and trim($wohDataCount[0]['countWohId']) != "") ? trim($wohDataCount[0]['countWohId']) : "0"; //$this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $totalRecordsWoh = $countResult; //$countResult[0]->RecordCount;
            $totalRecordsWoh = isset($totalRecordsWoh)?$totalRecordsWoh:"0";
          
            
            $numRecPerPage = 6;
            $param['record_limit'] = $limit;

            $start_index = $param['start_index'] = ($page - 1) * $limit;
            $wohData = $this->getWohTables()->getUserWohFront($param);
            //asd($wohData);
            $param2 = array();
            $param2['product_id'] = '';
            $param2['product_type_id'] = 6;
            $BioPersonalizeData = $this->getWohTables()->getUserWohProductAttributes($param2);

            $bio_certificate_product_id = isset($BioPersonalizeData[0]['bio_certificate_product_id'])?$BioPersonalizeData[0]['bio_certificate_product_id']:'';
            $bio_certificate_name = isset($BioPersonalizeData[0]['bio_certificate_name'])?$BioPersonalizeData[0]['bio_certificate_name']:'';
            $bio_certificate_image_name = isset($BioPersonalizeData[0]['bio_certificate_image_name'])?$BioPersonalizeData[0]['bio_certificate_image_name']:'';

            $personalized_panel_product_id = isset($BioPersonalizeData[0]['personalized_panel_product_id'])?$BioPersonalizeData[0]['personalized_panel_product_id']:'';
            $personalized_panel_product_name = isset($BioPersonalizeData[0]['personalized_panel_product_name'])?$BioPersonalizeData[0]['personalized_panel_product_name']:'';
            $personalized_panel_image_name = isset($BioPersonalizeData[0]['personalized_panel_image_name'])?$BioPersonalizeData[0]['personalized_panel_image_name']:'';


                if(!empty($wohData)) {
                $wohArr = $wohData;

                foreach ($wohArr as $key => $val) {


                    $wohArr[$key]['honoree_name'] = $val['final_name']; 

                    $wohArr[$key]['bio_certificate_product_id'] = $bio_certificate_product_id;
                    $wohArr[$key]['bio_certificate_name'] = $bio_certificate_name;
                    $wohArr[$key]['bio_certificate_image_name'] = $bio_certificate_image_name;
                    $wohArr[$key]['bio_certificate_info_required'] = 1;

                    $wohArr[$key]['personalized_panel_product_id'] = $personalized_panel_product_id;
                    $wohArr[$key]['personalized_panel_product_name'] = $personalized_panel_product_name;
                    $wohArr[$key]['personalized_panel_image_name'] = $personalized_panel_image_name;
                    $wohArr[$key]['personalized_panel_info_required'] = 2;

                    $param3 = array();
                    $param3['product_id'] = $val['dc_product_id'];
                    $param3['product_type_id'] = '';
                    $DuplicateData = $this->getWohTables()->getUserWohProductAttributes($param3);

                         $wohArr[$key]['duplicate_certificate_product_id'] = isset($DuplicateData[0]['duplicate_certificate_product_id'])?$DuplicateData[0]['duplicate_certificate_product_id']:'';
                         $wohArr[$key]['duplicate_certificate_product_name'] = isset($DuplicateData[0]['duplicate_certificate_product_name'])?$DuplicateData[0]['duplicate_certificate_product_name']:'';
                         $wohArr[$key]['duplicate_certificate_image_name'] = isset($DuplicateData[0]['duplicate_certificate_image_name'])?$DuplicateData[0]['duplicate_certificate_image_name']:'';
                    $wohArr[$key]['duplicate_certificate_info_required'] = 2;
                }
                    
                }
                else {
                $wohArr = array();
            }


            if($this->_auth->hasIdentity()== true and isset($this->_auth->getIdentity()->user_id))
                $loggedInUserId = $this->_auth->getIdentity()->user_id;
            else
                $loggedInUserId = '';

				
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => $woh_message,
                'wohArr' => $wohArr,
                'sort_field' => $sort_field,
                'sort_order' => $sort_order,
                'page' => $page,
                'start_index'=> $start_index,
                'limit' => $limit,
                'loggedInUserId' => $loggedInUserId,
                'file_upload_path_assets_url' => $this->_config['file_upload_path']['assets_url'].'product/thumbnail/',
                'totalRecordsWoh' => $totalRecordsWoh,
                'view_woh_type' => $view_woh_type,
                'facebookAppId' => $this->_config['facebook_app']['app_id'],
                'file_upload_path' => $this->_config['file_upload_path']['assets_upload_dir'],
                'woh_file_assets_path' => $this->_config['file_upload_path']['assets_url'],
                
            ));
            return $viewModel;
        }
    }

    /**
     * This function is used to more Woh List
     * @return Search Page
     * @author Icreon Tech - NS
     */
    public function moreWohListAction() {
        $this->getConfig();
        $this->getWohTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $woh_message = $this->_config['user_messages']['config']['user_woh'];
        $page = 1;
        $view_woh_type = "1";
        $sort_field = $param['sort_field'] = 'usr_wall_of_honor.woh_id';
        $sort_order = $param['sort_order'] = 'desc';
        $numRecPerPage = 6;
        $limit = $param['record_limit'] = $numRecPerPage;
        $start_index = ($page - 1) * $numRecPerPage;
        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $data = $request->getPost()->toArray();
            parse_str($data['searchString']);

            $view_woh_type = isset($view_woh_type)?$view_woh_type:"1";
            $param['last_name'] = $last_name;
            $param['first_name'] = $first_name;
            $param['other_part_name'] = $other_part_name;
            $param['country'] = $country;

            $param['donated_by'] = $donated_by;
            $param['panel_number'] = $panel_number;

            $param['is_panel_num'] = "1";

            $param['sort_field'] = $sort_field;
            $param['sort_order'] = $sort_order;


            $numRecPerPage = 10;
            $param['record_limit'] = $limit;

            $start_index = $param['start_index'] = ($page - 1) * $limit;


            $wohData = $this->getWohTables()->getUserWohFront($param);

            $param2 = array();
            $param2['product_id'] = '';
            $param2['product_type_id'] = 6;
            $BioPersonalizeData = $this->getWohTables()->getUserWohProductAttributes($param2);

            $bio_certificate_product_id = isset($BioPersonalizeData[0]['bio_certificate_product_id'])?$BioPersonalizeData[0]['bio_certificate_product_id']:'';
            $bio_certificate_name = isset($BioPersonalizeData[0]['bio_certificate_name'])?$BioPersonalizeData[0]['bio_certificate_name']:'';
            $bio_certificate_image_name = isset($BioPersonalizeData[0]['bio_certificate_image_name'])?$BioPersonalizeData[0]['bio_certificate_image_name']:'';

            $personalized_panel_product_id = isset($BioPersonalizeData[0]['personalized_panel_product_id'])?$BioPersonalizeData[0]['personalized_panel_product_id']:'';
            $personalized_panel_product_name = isset($BioPersonalizeData[0]['personalized_panel_product_name'])?$BioPersonalizeData[0]['personalized_panel_product_name']:'';
            $personalized_panel_image_name = isset($BioPersonalizeData[0]['personalized_panel_image_name'])?$BioPersonalizeData[0]['personalized_panel_image_name']:'';


                if(!empty($wohData)) {
                $wohArr = $wohData;
                foreach ($wohArr as $key => $val) {

                    $wohArr[$key]['honoree_name'] =  $val['final_name']; 

                    $wohArr[$key]['bio_certificate_product_id'] = $bio_certificate_product_id;
                    $wohArr[$key]['bio_certificate_name'] = $bio_certificate_name;
                    $wohArr[$key]['bio_certificate_image_name'] = $bio_certificate_image_name;
                    $wohArr[$key]['bio_certificate_info_required'] = 1;

                    $wohArr[$key]['personalized_panel_product_id'] = $personalized_panel_product_id;
                    $wohArr[$key]['personalized_panel_product_name'] = $personalized_panel_product_name;
                    $wohArr[$key]['personalized_panel_image_name'] = $personalized_panel_image_name;
                    $wohArr[$key]['personalized_panel_info_required'] = 2;

                    $param3 = array();
                    $param3['product_id'] = $val['dc_product_id'];
                    $param3['product_type_id'] = '';
                    $DuplicateData = $this->getWohTables()->getUserWohProductAttributes($param3);

                        $wohArr[$key]['duplicate_certificate_product_id'] = isset($DuplicateData[0]['duplicate_certificate_product_id'])?$DuplicateData[0]['duplicate_certificate_product_id']:'';
                        $wohArr[$key]['duplicate_certificate_product_name'] = isset($DuplicateData[0]['duplicate_certificate_product_name'])?$DuplicateData[0]['duplicate_certificate_product_name']:'';
                        $wohArr[$key]['duplicate_certificate_image_name'] = isset($DuplicateData[0]['duplicate_certificate_image_name'])?$DuplicateData[0]['duplicate_certificate_image_name']:'';
                    $wohArr[$key]['duplicate_certificate_info_required'] = 2;
                }
                }
                else {
                $wohArr = array();
            }


            if($this->_auth->hasIdentity()== true and isset($this->_auth->getIdentity()->user_id))
                $loggedInUserId = $this->_auth->getIdentity()->user_id;
            else
                $loggedInUserId = '';

            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => $woh_message,
                'wohArr' => $wohArr,
                'sort_field' => $sort_field,
                'sort_order' => $sort_order,
                'page' => $page,
                'start_index'=> $start_index,
                'limit' => $limit,
                'loggedInUserId' => $loggedInUserId,
                'file_upload_path_assets_url' => $this->_config['file_upload_path']['assets_url'].'product/thumbnail/',
                'view_woh_type' => $view_woh_type,
                'facebookAppId' => $this->_config['facebook_app']['app_id'],
				'allowed_IP' => $this->_config['allowed_ip']['ip'],
				'file_upload_path' => $this->_config['file_upload_path']['assets_upload_dir']
            ));
            return $viewModel;
        }
    }


    /**
     * This function is used to view Woh Products
     * @return     array
     * @author Icreon Tech - NS
     */
    public function viewWohProductsAction() {

        $this->getConfig();
        $this->getWohTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->layout('popup');
        
        
        // route - start
        $wohid = '';
        $productid = '';
        $inforeq = '';
        $productType = 0;
        $bioProdId = '';

        $params = $this->params()->fromRoute();
        if(isset($params['wohParams']) and trim($params['wohParams']) != "") { 
            $wohParams = trim($params['wohParams']);
            $wohParams = $this->decrypt($wohParams);
            $expWohParams = explode("^",$wohParams); 
 
            $wohid = isset($expWohParams[0])?$expWohParams[0]:'';
            $productid = isset($expWohParams[1])?$expWohParams[1]:'';
            $inforeq = isset($expWohParams[2])?$expWohParams[2]:'';
            $productType = isset($expWohParams[3])?$expWohParams[3]:0;
            $bioProdId = isset($expWohParams[4])?$expWohParams[4]:'';
            if($bioProdId == "none") { $bioProdId = ''; }
        }
        // route - end
        

        $wohArrRecord = $this->_userTable->getWoh(array('woh_id' => $wohid));
        $panelNo = isset($wohArrRecord[0]['panel_no']) ? $wohArrRecord[0]['panel_no'] : '';

        $paramProductInfo = array();
        $paramProductInfo['id'] = trim($productid);
        $productResult = $this->_productTable->getProductInfo($paramProductInfo);
        if (!empty($productResult)) {
            $productResult2 = $productResult[0];
            }
            else {
            $productResult2 = array();
        }
       

        $discount = 0;
       
        /*if(isset($this->_auth->getIdentity()->user_id)) {
            $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->_auth->getIdentity()->user_id));

                if(isset($userDetail['membership_discount']) and trim($userDetail['membership_discount']) != "") {
                $discount = $userDetail['membership_discount'];
            }
        }*/
        if(isset($this->_auth->getIdentity()->user_id)) {
            if (defined('MEMBERSHIP_DISCOUNT')) {
                    $discount = MEMBERSHIP_DISCOUNT;
            }
        }

        $paramsRelatedProducts = array();
        $paramsRelatedProducts['id'] = $productid;
        $paramsRelatedProducts['in_stock_flag'] = "1";
        $relatedProduct = $this->_productTable->getRelatedProduct($paramsRelatedProducts);

        $woh_message = $this->_config['user_messages']['config']['user_woh'];

        $wohBioCertificateForm = new WohBioCertificateForm();
        $wohBioCertificateForm->get('wohid')->setAttribute('value', $wohid);
        $wohBioCertificateForm->get('productid')->setAttribute('value', $productid);
        $wohBioCertificateForm->get('inforeq')->setAttribute('value', $inforeq);
        $wohBioCertificateForm->get('bio_certificate_qty')->setAttribute('value', "1");

        if(isset($bioProdId) and $bioProdId != "") {
            $bioWohData = $this->getServiceLocator()->get('User\Model\UserTable')->getBioWoh(array('bio_certificate_product_id' => $bioProdId));
            if(isset($bioWohData) and is_array($bioWohData)) {
                $wohBioCertificateForm->get('bio_certificate_prod_id')->setAttribute('value', $bioWohData[0]['bio_certificate_product_id']);
            }
        }
        
        $arrWallOfHonor = $this->getServiceLocator()->get('User\Model\WohTable')->getUserWohTraDetailById(array('woh_id'=>$wohid));       
        $honorNameWallOfHonor = isset($arrWallOfHonor[0]['final_name'])?$arrWallOfHonor[0]['final_name']:'&nbsp;';
        $panelNoWallOfHonor = isset($arrWallOfHonor[0]['panel_no'])?$arrWallOfHonor[0]['panel_no']:'&nbsp;';
        $countryOfOriginWallOfHonor = isset($arrWallOfHonor[0]['origin_country'])?$arrWallOfHonor[0]['origin_country']:'&nbsp;';
        

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => $woh_message,
            'productResult' => $productResult2,
            'file_root_upload_path_assets_url' => $this->_config['file_upload_path']['assets_upload_dir'] . 'product/',
            'file_upload_path_assets_url' => $this->_config['file_upload_path']['assets_url'] . 'product/',
            'file_root_upload_path_assets_url_thumbnail' => $this->_config['file_upload_path']['assets_upload_dir'] . 'product/',
            'file_upload_path_assets_url_thumbnail' => $this->_config['file_upload_path']['assets_url'] . 'product/',
            'inforeq' => $inforeq,
            'wohid' => $wohid,
            'wohBioCertificateForm' => $wohBioCertificateForm,
            'discount_membership' => $discount,
            'file_upload_path_assets_url' => $this->_config['file_upload_path']['assets_url'] . 'product/',
            'relatedProduct' => $relatedProduct,
            'productType' => $productType,
            'panelNo' => $panelNo,
            'honorNameWallOfHonor' => $honorNameWallOfHonor,
            'panelNoWallOfHonor' => $panelNoWallOfHonor,
            'countryOfOriginWallOfHonor' => $countryOfOriginWallOfHonor
        ));
        return $viewModel;
    }

    /**
     * This function is used to add To Cart Woh Products
     * @return     array
     * @author Icreon Tech - NS
     */
    public function addToCartWohProductsAction() {

        $this->getConfig();
        $this->getWohTables();
        $request = $this->getRequest();
        $response = $this->getResponse();

        try {
            $wohid = $request->getPost('wohid');
            $productid = $request->getPost('productid');
            $inforeq = $request->getPost('inforeq');
            $prodtype = $request->getPost('prodtype');
            $numQty = $request->getPost('numQty');
            $numQty = isset($numQty)?$numQty:"1";
            if(isset($numQty)) {
                if(trim($numQty) == "" || trim($numQty) == "0") {
                    $numQty = "1";
                }
            }
            else {
                 $numQty = "1";
            }
            

            $paramProductInfo = array();
            $paramProductInfo['id'] = trim($productid);
            $productResult = $this->_productTable->getProductInfo($paramProductInfo);


            $transaction = new Transaction($this->_adapter);
            
            /* add campaign,promotion and user group id in cart start */
            $promotionId = "";
            $couponCode = "";
            $user_group_id = "";

            $this->campaignSession = new Container('campaign');
            $campaign_code = (isset($this->campaignSession->campaign_code) && !empty($this->campaignSession->campaign_code) ? $this->campaignSession->campaign_code : "");

            $campaign_id = isset($this->campaignSession->campaign_id) && !empty($this->campaignSession->campaign_id) ? $this->campaignSession->campaign_id : 0;
            $channel_id = (isset($this->campaignSession->channel_id) && !empty($this->campaignSession->channel_id) ? $this->campaignSession->channel_id : 0);

            if ($campaign_id != 0 && $channel_id != 0)
            {
                $campResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getChannelPromo(array('campaign_id' => $campaign_id, 'channel_id' => $channel_id));

                if (isset($campResult[0]['promotion_id']) && !empty($campResult[0]['promotion_id']))
                {
                    $couponCode = $campResult[0]['coupon_code'];
                    $promotionId = $campResult[0]['promotion_id'];
                }
                else
                {
                    $couponCode = "";
                    $promotionId = 0;
                }

                $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '';
                if (isset($user_id) && $user_id != '')
                {
                    $searchParam['user_id'] = $user_id;
                    $searchParam['campaign_id'] = $campaign_id;
                    $cmTable = $this->getServiceLocator()->get('Campaign\Model\CampaignTable');
                    $campaign_arr = $cmTable->getUserCampaigns($searchParam);
                    if ($campaign_arr && isset($campaign_arr[0]['group_id']))
                    {
                        $user_group_id = $campaign_arr[0]['group_id'];
                    }
                    //echo "<pre>";var_dump($campaign_arr);die;
                }
            }


            //echo "<pre>";print_r($addToCartProductArr);die;
            /* add campaign,promotion and user group id in cart ends */

            $addToCartProductId = 0;
            if (!empty($productResult) and isset($inforeq) and trim($inforeq) == "2") {

                $inventoryFormArr = array();
                $inventoryFormArr['user_id'] = $this->_auth->getIdentity()->user_id;
                $inventoryFormArr['user_session_id'] = session_id();
                $inventoryFormArr['product_mapping_id'] = 7;
                $inventoryFormArr['product_id'] = $productResult[0]['product_id'];
                $inventoryFormArr['product_type_id'] = 6;
                $inventoryFormArr['product_attribute_option_id'] = NULL;
                $inventoryFormArr['item_id'] = $wohid;
                if (isset($prodtype) and trim($prodtype) != "") {
                    if (trim($prodtype) == "1") {
                        $prodtypeVal = '7';
                        $inventoryFormArr['item_info'] = '2';
                    }
                    if (trim($prodtype) == "2") {
                        $prodtypeVal = '8';
                    }
                    $inventoryFormArr['item_type'] = $prodtypeVal;
                }

                $inventoryFormArr['num_quantity'] = $numQty; //1;
                $inventoryFormArr['item_price'] = isset($productResult[0]['product_sell_price']) ? $productResult[0]['product_sell_price'] : $productResult[0]['product_sell_price'];
                $inventoryFormArr['product_sku'] = $productResult[0]['product_sku'];
                $inventoryFormArr['product_price'] = isset($productResult[0]['product_sell_price']) ? $productResult[0]['product_sell_price'] : $productResult[0]['product_sell_price'];
                $inventoryFormArr['added_by'] = '0';
                $inventoryFormArr['added_date'] = DATE_TIME_FORMAT;
                $inventoryFormArr['modify_by'] = '0';
                $inventoryFormArr['modify_date'] = DATE_TIME_FORMAT;
                $inventoryFormArr['cart_date'] = DATE_TIME_FORMAT;
                $addToCartProductArr = $transaction->getAddToCartProductArr($inventoryFormArr);
                
                $addToCartProductArr[] = $campaign_id;
                $addToCartProductArr[] = $campaign_code;
                $addToCartProductArr[] = $promotionId;
                $addToCartProductArr[] = $couponCode;
                $addToCartProductArr[] = $user_group_id;
				$addToCartProductArr[] = '6';
                
                $addToCartProductId = $this->_transactionTable->addToCartProduct($addToCartProductArr);
            }

            if (!empty($productResult) and isset($inforeq) and trim($inforeq) == "1") {

                $filter = new \Zend\Filter\StripTags();

                $bio_certificate_prod_id = $request->getPost('bio_certificate_prod_id');
                $bio_certificate_prod_id = $filter->filter($bio_certificate_prod_id);
                
                
                $bio_name = "";                
                $immigrated_from = "";                
                $method_of_travel = "";                 
                $port_of_entry = "";               
                $date_of_entry_to_us = "";                 
                $additional_info = "";

                $bio_certificate_qty = $request->getPost('bio_certificate_qty');
                $bio_certificate_qty = (isset($bio_certificate_qty) && $bio_certificate_qty!='' && $bio_certificate_qty!=0)?$bio_certificate_qty:"1";
                
                $bio_certificate_qty = $filter->filter($bio_certificate_qty);

                $paramsArr = array();
                $paramsArr['user_id'] = $this->_auth->getIdentity()->user_id;
                $paramsArr['user_session_id'] = session_id();
                $paramsArr['bio_name'] = $bio_name;
                $paramsArr['immigrated_from'] = $immigrated_from;
                $paramsArr['method_of_travel'] = $method_of_travel;
                $paramsArr['port_of_entry'] = $port_of_entry;
                $paramsArr['date_of_entry_to_us'] = $this->DateFormat($date_of_entry_to_us, 'db_datetime_format');  //;
                $paramsArr['additional_info'] = $additional_info;
                $paramsArr['is_finalize'] = 0;
                $paramsArr['product_mapping_id'] = $productResult[0]['product_mapping_id'];
                $paramsArr['product_id'] = $productResult[0]['product_id'];
                $paramsArr['product_type_id'] = $productResult[0]['product_type_id'];
                $paramsArr['product_attribute_option_id'] = NULL;
                $paramsArr['bio_certificate_qty'] = $bio_certificate_qty;
                $paramsArr['product_price'] = NULL;
                $paramsArr['product_sku'] = NULL;
                $paramsArr['added_by'] = '0';
                $paramsArr['added_date'] = DATE_TIME_FORMAT;
                $paramsArr['modify_by'] = '0';
                $paramsArr['modify_date'] = DATE_TIME_FORMAT;
                $paramsArr['wohid'] = $wohid;
                $paramsArr['bio_certificate_product_id'] = $bio_certificate_prod_id;

                $addToCartBioCertificateArr = $transaction->getAddToCartWallOfHonorBioCertificate($paramsArr);
                
                $addToCartBioCertificateArr[] = $campaign_id;
                $addToCartBioCertificateArr[] = $campaign_code;
                $addToCartBioCertificateArr[] = $promotionId;
                $addToCartBioCertificateArr[] = $couponCode;
                $addToCartBioCertificateArr[] = $user_group_id;
				$addToCartBioCertificateArr[] = '6';
             
                $addToCartBioCertificateId = $this->_transactionTable->addToCartBioCertificate($addToCartBioCertificateArr);
            }

            $messages = array('status' => "success");
        } catch (Exception $e) {
            $messages = array('status' => "error");
        }

        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This function is used to add Woh
     * @return     array
     * @author Icreon Tech - NS
     */
    public function addWohAction() {
		$this->layout('wohlayout');
        $this->getWohTables();
        $this->getConfig();
        //$this->checkFrontUserAuthentication();
        $productMappingId = 7;
        $prodMappArr = array($productMappingId);
        $getProductMappingTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getProductsMappingType($prodMappArr);
        $productParam = array();
        $productParam['productType'] = $getProductMappingTypes[0]['product_type_id'];/** product */
        $productResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getProductAttributeDetails($productParam);


        $productOptionsArr = array();
        $totalOptions = count($productResult);
        for ($i = $totalOptions - 1; $i >= 0; $i--) {
            $product = $productResult[$i];
            if (($product['additional_product_1'] != 0 && $product['additional_product_3'] == 0) && !isset($productOptionsArr[$product['product_id']])) {
                $productOptionInfo = array();
                $productOptionInfo['product_id'] = $product['product_id'];
                $productOptionInfo['product_sku'] = $product['product_sku'];
                $productOptionInfo['product_type_id'] = $product['product_type_id'];
                $productOptionInfo['product_list_price'] = $product['product_list_price'];
                $productOptionInfo['product_cost'] = $product['product_cost'];
                $productOptionInfo['product_sell_price'] = $product['product_sell_price'];
                $productOptionInfo['is_membership_discount'] = $product['is_membership_discount'];
                $productOptionInfo['product_name'] = $product['product_name'];
                $productOptionInfo['attribute_id'] = $product['attribute_id'];
                $productOptionInfo['options'] = array();
                $productOptionsArr[$product['product_id']] = $productOptionInfo;
            }
            if ($product['attribute_option_id'] != '') {
                $optionArr = array();
                $optionArr['attribute_option_id'] = $product['attribute_option_id'];
                $optionArr['option_name'] = $product['option_name'];
                $optionArr['option_cost'] = $product['option_cost'];
                $optionArr['option_price'] = $product['additional_option_price'];
                $productOptionsArr[$product['product_id']]['options'][] = $optionArr;
                if (isset($product['additional_option_price']) and trim($product['additional_option_price']) != "") {
                    $productOptionsArr[$product['product_id']]['options_prices'][] = $product['additional_option_price'];
                }
            }
        }
        asort($productOptionsArr);

        $discountValue = 0;
        /*if (isset($this->_auth->getIdentity()->user_id)) {
            $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->_auth->getIdentity()->user_id));

            if (isset($userDetail['membership_discount']) and trim($userDetail['membership_discount']) != "") {
                $discountValue = trim($userDetail['membership_discount']);
            }
        }*/
        if (!empty($this->auth->getIdentity()->user_id)) {
            if (defined('MEMBERSHIP_DISCOUNT')) {
                $discountValue = MEMBERSHIP_DISCOUNT;
            }
        } 

        $jsLangTranslate = $this->_config['user_messages']['config']['user_woh'];

        $logged_in_flag = false;
        if (!isset($this->_auth->getIdentity()->user_id)) {
            $logged_in_flag = false;
        } else {
            $logged_in_flag = true;
        }

            
            // Search Form - start
                                         			     
            $page = 1;
            $sort_field = 'usr_wall_of_honor.woh_id';
            $sort_order = 'desc';
            $numRecPerPage = 6;
            $limit = $numRecPerPage;
            $start_index = ($page - 1) * $numRecPerPage;
        
            $searchWohForm = new WohSearchForm();
            $searchWohForm->setAttribute('action', '/search-woh');
            $searchWohForm->get('page')->setAttribute('value', $page);
            $searchWohForm->get('sort_field')->setAttribute('value', $sort_field);
            $searchWohForm->get('sort_order')->setAttribute('value', $sort_order);
            $searchWohForm->get('start_index')->setAttribute('value', $start_index);
            $searchWohForm->get('limit')->setAttribute('value', $limit);
				
            $searchWohForm->get('first_name')->setAttribute('placeholder', 'First Name');
            $searchWohForm->get('last_name')->setAttribute('placeholder', 'Last Name');            
            $searchWohForm->get('panel_number')->setAttribute('placeholder', 'Panel #');
            $searchWohForm->get('other_part_name')->setAttribute('placeholder', 'Other Part of Name');
            $searchWohForm->get('country')->setAttribute('placeholder', 'Country of Origin');
            $searchWohForm->get('donated_by_add')->setAttribute('placeholder', 'Donated By');
	
            // Search Form - end
         $returnCmsWoh = $this->getWohTables()->getWohCms();
         $objWohCMS = new CmsController();
         
        $viewModel = new ViewModel();

        $viewModel->setVariables(array(
            'jsLangTranslate' => $jsLangTranslate,
            'wohForm' => new WohForm(),
            'attibuteOptionsArr' => $productOptionsArr,
            'logged_in_flag' => $logged_in_flag,
            'discountValue' => $discountValue,
            'searchWohForm' => $searchWohForm,
            'returnCmsWoh' => $returnCmsWoh[0],
            'origin_country_usa' => $this->_config['transaction_config']['origin_country']
            //,'wallOfHonorListingSearch' => $objWohCMS->wallOfHonorListingSearch('','',$this->_config['js_file_path']['path'],$this->_config['file_versions']['js_version'])
        ));
        return $viewModel;
    }

    /**
     * This function is used to add Woh
     * @return     array
     * @author Icreon Tech - NS
     */
    public function addWohProcessAction() {
        try {

            $this->getWohTables();
            $this->getConfig();
            $request = $this->getRequest();
            $response = $this->getResponse();
            $wohObj = new Woh($this->_adapter);

            $wohListPagesMessages = $this->_config['user_messages']['config']['user_woh'];

            $params = array();
            $woh_main_product = $request->getPost('woh_main_product');
            $params['woh_main_product'] = $woh_main_product;
            $params['product_attribute_option_id'] = $request->getPost('product_attribute_option_id');
            $params['first_name_one'] = $request->getPost('first_name_one');
            $params['first_name_two'] = $request->getPost('first_name_two');
            $params['other_init_one'] = $request->getPost('other_init_one');
            $params['other_init_two'] = $request->getPost('other_init_two');
            $params['other_name'] = $request->getPost('other_name');
            $params['last_name_one'] = $request->getPost('last_name_one');
            $params['last_name_two'] = $request->getPost('last_name_two');
            $params['first_line'] = $request->getPost('first_line');
            $params['second_line'] = $request->getPost('second_line');
            $params['is_usa'] = $request->getPost('is_usa');
            $params['other_country'] = $request->getPost('other_country');
            $params['donated_by'] = $request->getPost('donated_by');
            $params['donated_for'] = $request->getPost('donated_for');

            $params['first_name'] = $request->getPost('first_name');
            $params['last_name'] = $request->getPost('last_name');
            $params['email_id'] = $request->getPost('email_id');

            $params['num_of_duplicate_copies_of_certificate'] = $request->getPost('num_of_duplicate_copies_of_certificate');
            $params['i_confirm'] = $request->getPost('i_confirm');
            $params['is_agree'] = $request->getPost('is_agree');
            $submitorder = $request->getPost('submitorder');


            $isUsa = $request->getPost('is_usa');
            if(isset($isUsa) and trim($isUsa) != "" and trim($isUsa)=="1") {  $params['other_country'] = $this->_config['transaction_config']['origin_country']; }

            if (!isset($submitorder)) {
                $messages = array('status' => "error", 'message' => '');
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {

                $user_id = '';
                if (!isset($this->_auth->getIdentity()->user_id)) {
                    $user_id = '';
                } else {
                    $user_id = $this->_auth->getIdentity()->user_id;
                }
                $params['user_id'] = $user_id;
                $params['user_session_id'] = session_id();
                $params['added_by'] = 0;
                $params['added_date'] = DATE_TIME_FORMAT;
                $params['modified_by'] = 0;
                $params['modified_date'] = DATE_TIME_FORMAT;
                $params['product_mapping_id'] = 7;
                $params['product_type_id'] = 6;
                $params['num_quantity'] = 1;
                $expWohMainProduct = explode("^", $woh_main_product);
                $params['product_id'] = isset($expWohMainProduct[0]) ? $expWohMainProduct[0] : 0;
                $params['product_attribute_option_id'] = isset($expWohMainProduct[1]) ? $expWohMainProduct[1] : 0;

                // n - start
                $proDetails = array();
                $proDetails['id'] = $params['product_id'];
                $productResultAttr = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProductInfo($proDetails);
                // n - end

                $attrParams = array();
                $attrParams['startIndex'] = '';
                $attrParams['recordLimit'] = '';
                $attrParams['sortField'] = '';
                $attrParams['sortOrder'] = '';
                $attrParams['attributeId'] = isset($expWohMainProduct[1]) ? $expWohMainProduct[1] : 0;
                $attrParams['option'] = "1";
                $prodAttrOption = $this->getServiceLocator()->get('Product\Model\AttributeTable')->searchAttributeOptions($attrParams);
                $wohHonoreeName = $this->getNamingConventionArr($prodAttrOption[0]['option_name'], $params, $params);

                if (isset($wohHonoreeName['honoree_name']) and trim($wohHonoreeName['honoree_name']) != "") {
                    $params['final_name'] = trim($wohHonoreeName['honoree_name']);
                } else {
                    $params['final_name'] = '';
                }

                $productParam = array();
                $productParam['productType'] = 6;/** product */
                $productResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getProductAttributeDetails($productParam);
                $params['product_sku'] = $productResultAttr[0]['product_sku']; //$productResult[0]['product_sku'];/** product */
                $filterDataWoh = $wohObj->exchangeAddToCartWallOfHonorArray($params);
                
                /*add campaign,promotion and user group id in cart start*/
                $promotionId="";
                $couponCode="";
                $user_group_id="";
               
                $this->campaignSession = new Container('campaign');
                $campaign_code = (isset($this->campaignSession->campaign_code) &&!empty($this->campaignSession->campaign_code) ? $this->campaignSession->campaign_code : "");
               
                $campaign_id = isset($this->campaignSession->campaign_id) &&!empty($this->campaignSession->campaign_id) ? $this->campaignSession->campaign_id : 0;
                $channel_id = (isset($this->campaignSession->channel_id) &&!empty($this->campaignSession->channel_id) ?                 $this->campaignSession->channel_id : 0);
                 
                if($campaign_id !=0 && $channel_id !=0)
                {
                    $campResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getChannelPromo(array('campaign_id' => $campaign_id, 'channel_id' => $channel_id));
                
                    if (isset($campResult[0]['promotion_id']) &&!empty($campResult[0]['promotion_id']))
                    {
                        $couponCode = $campResult[0]['coupon_code'];
                        $promotionId = $campResult[0]['promotion_id'];
                    }
                    else
                    {
                        $couponCode= "";
                        $promotionId = 0;
                    }
                    
                    $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '';
                    if (isset($user_id) && $user_id != '')
                    {
                        $searchParam['user_id'] = $user_id;
                        $searchParam['campaign_id'] = $campaign_id;
                        $cmTable = $this->getServiceLocator()->get('Campaign\Model\CampaignTable');
                        $campaign_arr = $cmTable->getUserCampaigns($searchParam);
                        if($campaign_arr && isset($campaign_arr[0]['group_id']))
                        {
                            $user_group_id = $campaign_arr[0]['group_id'];
                        }
                        //echo "<pre>";var_dump($campaign_arr);die;
                    }
                }
                
                $filterDataWoh['campaign_id']=$campaign_id;
                $filterDataWoh['campaign_code']=$campaign_code;
                $filterDataWoh['promotion_id']=$promotionId;
                $filterDataWoh['coupon_code']=$couponCode;
                $filterDataWoh['user_group_id']=$user_group_id;
                
                //Ticket-Id :130
                $inscribeKey = $wohObj->getInscribeKey($filterDataWoh['final_name']);
                $filterDataWoh['inscribe_key']=$inscribeKey;
				$filterDataWoh['cart_source_id']='6';
                /*add campaign,promotion and user group id in cart ends*/
                $returnDataWoh = $this->getWohTables()->insertToCartWallOfHonorNaming($filterDataWoh);


                for ($i = 0; $i < count($params['first_name']); $i++) {
                    if(isset($params['email_id'][$i]) and trim($params['email_id'][$i]) != "") { 
                      $params2 = array('woh_id' => $returnDataWoh, 'first_name' => $params['first_name'][$i], 'last_name' => $params['last_name'][$i], 'email_id' => $params['email_id'][$i], 'added_date' => DATE_TIME_FORMAT, 'modified_date' => DATE_TIME_FORMAT, 'cart_source_id' => '6');
                      $filterDataWohAddContact = $wohObj->exchangeAddToCartWallOfHonorAdditionalContactArr($params2);
                      $returnDataWohAddContact = $this->getWohTables()->insertToCartWallOfHonorAdditionalContact($filterDataWohAddContact);
                    }
                }


                $paramProductInfo2 = array();
                $paramProductInfo2['id'] = $params['product_id'];
                $productResult2 = $this->_productTable->getProductInfo($paramProductInfo2);

                if (isset($productResult2[0]['additional_product_1']) and trim($productResult2[0]['additional_product_1']) != "" and isset($params['num_of_duplicate_copies_of_certificate']) and trim($params['num_of_duplicate_copies_of_certificate']) != "" and trim($params['num_of_duplicate_copies_of_certificate']) > 0) {
                    //Duplicate Certificate Add
                    $paramsArr = array();
                    $paramsArr['productId'] = trim($productResult2[0]['additional_product_1']);
                    $paramsArr['item_id'] = $returnDataWoh;
                    $paramsArr['item_type'] = '7';
                    $paramsArr['item_info'] = '1';
                    $paramsArr['user_id'] = $user_id;
                    $paramsArr['num_quantity'] = trim($params['num_of_duplicate_copies_of_certificate']);
                    $this->addInventoryProductToCart($paramsArr);
                    //End duplicate Certificate Add
                }


                /// frames //// start
                $product_woh_frame_price_id = $request->getPost('product_woh_frame_price_id');
                $product_woh_frame_price_qty0 = $request->getPost('product_woh_frame_price_qty');
                if(isset($product_woh_frame_price_qty0) and trim($product_woh_frame_price_qty0) != "") { $product_woh_frame_price_qty = $product_woh_frame_price_qty0; }
                else { $product_woh_frame_price_qty = "1"; }
                
                if (isset($product_woh_frame_price_id) and trim($product_woh_frame_price_id) != "" and isset($product_woh_frame_price_qty) and trim($product_woh_frame_price_qty) != "") {
                    $paramProductInfo = array();
                    $paramProductInfo['id'] = trim($product_woh_frame_price_id);
                    $productResult = $this->_productTable->getProductInfo($paramProductInfo);
                    $transaction = new Transaction($this->_adapter);
                    $addToCartProductId = 0;
                    if (!empty($productResult)) {
                        $inventoryFormArr = array();
                        $inventoryFormArr['user_id'] = $user_id;
                        $inventoryFormArr['user_session_id'] = session_id();
                        $inventoryFormArr['product_mapping_id'] = 4;
                        $inventoryFormArr['product_id'] = $productResult[0]['product_id'];
                        $inventoryFormArr['product_type_id'] = 1;
                        $inventoryFormArr['product_attribute_option_id'] = NULL;
                        $inventoryFormArr['item_id'] = $returnDataWoh;
                         
                        $inventoryFormArr['num_quantity'] = isset($product_woh_frame_price_qty)?$product_woh_frame_price_qty:"1";
                        $inventoryFormArr['item_price'] = isset($productResult[0]['product_sell_price']) ? $productResult[0]['product_sell_price'] : $productResult[0]['product_sell_price'];
                        $inventoryFormArr['product_sku'] = $productResult[0]['product_sku'];
                        $inventoryFormArr['product_price'] = isset($productResult[0]['product_sell_price']) ? $productResult[0]['product_sell_price'] : $productResult[0]['product_sell_price'];
                        $inventoryFormArr['added_by'] = '0';
                        $inventoryFormArr['added_date'] = DATE_TIME_FORMAT;
                        $inventoryFormArr['modify_by'] = '0';
                        $inventoryFormArr['modify_date'] = DATE_TIME_FORMAT;
                        $inventoryFormArr['cart_date'] = DATE_TIME_FORMAT;
                        $inventoryFormArr['item_type'] = '4';//ticket:1230
                        $addToCartProductArr = $transaction->getAddToCartProductArr($inventoryFormArr);
                        /*add campaign,promotion and user group id in cart start*/
                        $promotionId="";
                        $couponCode="";
                        $user_group_id="";

                        $this->campaignSession = new Container('campaign');
                        $campaign_code = (isset($this->campaignSession->campaign_code) &&!empty($this->campaignSession->campaign_code) ? $this->campaignSession->campaign_code : "");

                        $campaign_id = isset($this->campaignSession->campaign_id) &&!empty($this->campaignSession->campaign_id) ? $this->campaignSession->campaign_id : 0;
                        $channel_id = (isset($this->campaignSession->channel_id) &&!empty($this->campaignSession->channel_id) ?                 $this->campaignSession->channel_id : 0);

                        if($campaign_id !=0 && $channel_id !=0)
                        {
                            $campResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getChannelPromo(array('campaign_id' => $campaign_id, 'channel_id' => $channel_id));

                            if (isset($campResult[0]['promotion_id']) &&!empty($campResult[0]['promotion_id']))
                            {
                                $couponCode = $campResult[0]['coupon_code'];
                                $promotionId = $campResult[0]['promotion_id'];
                            }
                            else
                            {
                                $couponCode= "";
                                $promotionId = 0;
                            }

                            $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '';
                            if (isset($user_id) && $user_id != '')
                            {
                                $searchParam['user_id'] = $user_id;
                                $searchParam['campaign_id'] = $campaign_id;
                                $cmTable = $this->getServiceLocator()->get('Campaign\Model\CampaignTable');
                                $campaign_arr = $cmTable->getUserCampaigns($searchParam);
                                if($campaign_arr && isset($campaign_arr[0]['group_id']))
                                {
                                    $user_group_id = $campaign_arr[0]['group_id'];
                                }
                                //echo "<pre>";var_dump($campaign_arr);die;
                            }
                        }

                        $addToCartProductArr[]=$campaign_id;
                        $addToCartProductArr[]=$campaign_code;
                        $addToCartProductArr[]=$promotionId;
                        $addToCartProductArr[]=$couponCode;
                        $addToCartProductArr[]=$user_group_id;
						$addToCartProductArr[]='6';

                        //echo "<pre>";print_r($addToCartProductArr);die;
                        /*add campaign,promotion and user group id in cart ends*/
                        $addToCartProductId = $this->_transactionTable->addToCartProduct($addToCartProductArr);
                    }
                }
                /// frames //// end

                if ($returnDataWoh != false) {
                    $messages = array('status' => "success");
                } else {
                    $messages = array('status' => "error");
                }

                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This action is used to add inventory product to cart
     * @return int
     * @param array
     * @author Icreon Tech - NS
     */
    public function addInventoryProductToCart($params = array()) {
        $this->getWohTables();
        $searchParam['id'] = $params['productId'];
        $productResult = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProductInfo($searchParam);
        $transaction = new Transaction($this->_adapter);
        $addToCartProductId = 0;
        if (!empty($productResult)) {
            $inventoryFormArr = array();
            $inventoryFormArr['user_id'] = $params['user_id'];
            $inventoryFormArr['user_session_id'] = session_id();
            $inventoryFormArr['product_mapping_id'] = isset($params['product_mapping_id']) ? $params['product_mapping_id'] : $productResult[0]['product_mapping_id'];
            $inventoryFormArr['product_id'] = isset($params['product_id']) ? $params['product_id'] : $productResult[0]['product_id'];
            $inventoryFormArr['product_type_id'] = isset($params['product_type_id']) ? $params['product_type_id'] : $productResult[0]['product_type_id'];
            $inventoryFormArr['product_attribute_option_id'] = isset($params['product_attribute_option_id']) ? $params['product_attribute_option_id'] : NULL;
            $inventoryFormArr['item_id'] = isset($params['item_id']) ? $params['item_id'] : NULL;
            $inventoryFormArr['item_type'] = isset($params['item_type']) ? $params['item_type'] : NULL;
            $inventoryFormArr['item_info'] = isset($params['item_info']) ? $params['item_info'] : NULL;
            $inventoryFormArr['num_quantity'] = isset($params['num_quantity']) ? $params['num_quantity'] : 1;
            $inventoryFormArr['item_price'] = isset($params['item_price']) ? $params['item_price'] : NULL;
            $inventoryFormArr['product_sku'] = $productResult[0]['product_sku'];
            $inventoryFormArr['product_price'] = isset($params['product_price']) ? $params['product_price'] : $productResult[0]['product_sell_price'];
            $inventoryFormArr['added_by'] = '0';
            $inventoryFormArr['added_date'] = DATE_TIME_FORMAT;
            $inventoryFormArr['modify_by'] = '0';
            $inventoryFormArr['modify_date'] = DATE_TIME_FORMAT;
            $inventoryFormArr['cart_date'] = DATE_TIME_FORMAT;
            $addToCartProductArr = $transaction->getAddToCartProductArr($inventoryFormArr);
            /*add campaign,promotion and user group id in cart start*/
            $promotionId="";
            $couponCode="";
            $user_group_id="";

            $this->campaignSession = new Container('campaign');
            $campaign_code = (isset($this->campaignSession->campaign_code) &&!empty($this->campaignSession->campaign_code) ? $this->campaignSession->campaign_code : "");

            $campaign_id = isset($this->campaignSession->campaign_id) &&!empty($this->campaignSession->campaign_id) ? $this->campaignSession->campaign_id : 0;
            $channel_id = (isset($this->campaignSession->channel_id) &&!empty($this->campaignSession->channel_id) ?                 $this->campaignSession->channel_id : 0);

            if($campaign_id !=0 && $channel_id !=0)
            {
                $campResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getChannelPromo(array('campaign_id' => $campaign_id, 'channel_id' => $channel_id));

                if (isset($campResult[0]['promotion_id']) &&!empty($campResult[0]['promotion_id']))
                {
                    $couponCode = $campResult[0]['coupon_code'];
                    $promotionId = $campResult[0]['promotion_id'];
                }
                else
                {
                    $couponCode= "";
                    $promotionId = 0;
                }

                $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '';
                if (isset($user_id) && $user_id != '')
                {
                    $searchParam['user_id'] = $user_id;
                    $searchParam['campaign_id'] = $campaign_id;
                    $cmTable = $this->getServiceLocator()->get('Campaign\Model\CampaignTable');
                    $campaign_arr = $cmTable->getUserCampaigns($searchParam);
                    if($campaign_arr && isset($campaign_arr[0]['group_id']))
                    {
                        $user_group_id = $campaign_arr[0]['group_id'];
                    }
                    //echo "<pre>";var_dump($campaign_arr);die;
                }
            }

            $addToCartProductArr[]=$campaign_id;
            $addToCartProductArr[]=$campaign_code;
            $addToCartProductArr[]=$promotionId;
            $addToCartProductArr[]=$couponCode;
            $addToCartProductArr[]=$user_group_id;
			$addToCartProductArr[]='6';

            //echo "<pre>";print_r($addToCartProductArr);die;
            /*add campaign,promotion and user group id in cart ends*/
            $addToCartProductId = $this->_transactionTable->addToCartProduct($addToCartProductArr);
        }
        return $addToCartProductId;
    }

    /**
     * This action is used to generate wall of honor on basis of naming convention
     * @param void
     * @return void
     * @author Icreon Tech - DT
     */
    public function generateWohFormByNamingConventionAction() {
        $this->getWohTables();
        $this->getConfig();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost() && $request->isXmlHttpRequest()) {

            $createTransactionMessage = $this->_config['user_messages']['config']['user_woh'];
            $wallOfHonor = new WohForm();
            $productId = $request->getPost('product_id');
            $attributeId = $request->getPost('attribute_id');
            $optionId = $request->getPost('option_id');
            $optionName = $request->getPost('option_name');
            $attributeName = $request->getPost('attribute_type');
            $formatName = $request->getPost('format_name');
            $wallOfHonor->get('product_attribute_option_id')->setValue($optionId);
            $namingConventionArr = $this->getNamingConventionArr($optionName, $attributeName, '');

            $paramProductInfo = array();
            $paramProductInfo['id'] = $productId;
            $this->getWohTables();
            $productResult = $this->_productTable->getProductInfo($paramProductInfo);
            $additional_product_1_id = $productResult[0]['additional_product_1'];

            $productDuplicateSellPrice = 0;
            $paramProductInfoDuplicate = array();
            $paramProductInfoDuplicate['id'] = $additional_product_1_id;
            $productResultDuplicateRecord = $this->_productTable->getProductInfo($paramProductInfoDuplicate);
            $productDuplicateSellPrice = isset($productResultDuplicateRecord[0]['product_sell_price']) ? $productResultDuplicateRecord[0]['product_sell_price'] : 0;
            $productIsMembershipDiscount = isset($productResultDuplicateRecord[0]['is_membership_discount']) ? $productResultDuplicateRecord[0]['is_membership_discount'] : 0;

            $discountVal = 0;
            if (isset($this->_auth->getIdentity()->user_id)) {
                $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->_auth->getIdentity()->user_id));

                if (isset($userDetail['membership_discount']) and trim($userDetail['membership_discount']) != "") {
                    $discountVal = trim($userDetail['membership_discount']);
                }
            }


            if ($productIsMembershipDiscount == "1" and $discountVal != 0) {
                $productDuplicateSellPrice = $productDuplicateSellPrice - $productDuplicateSellPrice * $discountVal / 100;
            }

            $paramsRelatedProd = array();
            $paramsRelatedProd['id'] = $productId;
            $paramsRelatedProd['in_stock_flag'] = "1";
            $relatedProduct = $this->_productTable->getRelatedProduct($paramsRelatedProd);
            $totalRelatedProduct = "0";
            if(isset($relatedProduct) and is_array($relatedProduct) and count($relatedProduct) > 0) {
                $totalRelatedProduct = count($relatedProduct);
            }
            else {
                $totalRelatedProduct = "0";
            }

            //$productId
            $wallOfHonor->get('product_number_duplicate_copies_certificate')->setAttribute('value', $productDuplicateSellPrice);

            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => $createTransactionMessage,
                'wallOfHonor' => $wallOfHonor,
                'attributeId' => $attributeId,
                'optionId' => $optionId,
                'optionName' => $optionName,
                'formatName' => $formatName,
                'namingConventionArr' => $namingConventionArr,
                'totalRelatedProduct' => $totalRelatedProduct
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to add more additional contact
     * @param void
     * @return void
     * @author Icreon Tech - NS
     */
    public function addAdditionalContactAction() {
        $this->getWohTables();
        $this->getConfig();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $totalSaveSearch = $request->getPost('totalSaveSearch');
            $msgArr = $this->_config['user_messages']['config']['user_woh'];
            $wallOfHonorAdditionalContact = new WohForm();
            $firstNameId = 'first_name_' . ($totalSaveSearch + 1);
            $lastNameId = 'last_name_' . ($totalSaveSearch + 1);
            $emailId = 'email_id_' . ($totalSaveSearch + 1);
            $wallOfHonorAdditionalContact->get('first_name[]')->setAttributes(array('id' => $firstNameId));
            $wallOfHonorAdditionalContact->get('last_name[]')->setAttributes(array('id' => $lastNameId));
            $wallOfHonorAdditionalContact->get('email_id[]')->setAttributes(array('id' => $emailId));
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => $msgArr,
                'wallOfHonorAdditionalContact' => $wallOfHonorAdditionalContact,
                'totalSaveSearch' => $totalSaveSearch
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to get frames
     * @param void
     * @return void
     * @author Icreon Tech - NS
     */
    public function getWohFramesAction() {
        $this->getWohTables();
        $this->getConfig();
        $jsLangTranslate = $this->_config['user_messages']['config']['user_woh'];

        $request = $this->getRequest();

        $discount = 0;
        /*if (isset($this->_auth->getIdentity()->user_id)) {
            $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->_auth->getIdentity()->user_id));

            if (isset($userDetail['membership_discount']) and trim($userDetail['membership_discount']) != "") {
                $discount = $userDetail['membership_discount'];
            }
        }*/
        if (defined('MEMBERSHIP_DISCOUNT')) {
			$discount = MEMBERSHIP_DISCOUNT;
		}        
        /*set minimum amount for membership discount starts*/
        $minimumMemberShipDiscount = 0;
        $memberShipDiscount = $this->getServiceLocator()->get('User\Model\ContactTable')->getMinimumDiscountAmount();
        if(isset($memberShipDiscount['discount']))
        {
          $minimumMemberShipDiscount = $memberShipDiscount['discount'];
        }
        /*set minimum amount for membership discount ends*/
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $product_id = $request->getPost('product_id');
            
            if(isset($product_id) and trim($product_id) != "") {
                $params = array();
                $params['id'] = $product_id;
                $params['in_stock_flag'] = "1";
                $relatedProduct = $this->_productTable->getRelatedProduct($params);
            }
            else {
                $relatedProduct = array(); 
            }
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'relatedProduct' => $relatedProduct,
                'file_upload_path_assets_url' => $this->_config['file_upload_path']['assets_url'].'product/thumbnail/',
                'file_upload_path_assets_upload_dir' => $this->_config['file_upload_path']['assets_upload_dir'].'product/thumbnail/',
                'file_upload_path_assets_url_main' => $this->_config['file_upload_path']['assets_url'].'product/',
                'file_upload_path_assets_upload_dir_main' => $this->_config['file_upload_path']['assets_upload_dir'].'product/',
                'discounts' => $discount,
                'minimumMemberShipDiscount' => $minimumMemberShipDiscount,
                'jsLangTranslate' => $jsLangTranslate
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to get more products
     * @param void
     * @return void
     * @author Icreon Tech - NS
     */
    public function getWohOtherProductsAction() {
        $this->getWohTables();
        $this->getConfig();
        $jsLangTranslate = $this->_config['user_messages']['config']['user_woh'];
        $request = $this->getRequest();
        $wohid = $request->getPost('wohid');

        $discount = 0;
        if (isset($this->_auth->getIdentity()->user_id)) {
            $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->_auth->getIdentity()->user_id));

            if (isset($userDetail['membership_discount']) and trim($userDetail['membership_discount']) != "") {
                $discount = $userDetail['membership_discount'];
            }
        }

        $wohArrRecord = $this->_userTable->getWoh(array('woh_id' => $wohid));                
        $honoreeName = isset($wohArrRecord[0]['final_name']) ? $wohArrRecord[0]['final_name'] : '';


        $paramProductInfo1 = array();
        $paramProductInfo1['id'] = "35";
        $productResult1 = $this->_productTable->getProductInfo($paramProductInfo1);
        $productResult10 = $productResult1[0];

        $paramProductInfo2 = array();
        $paramProductInfo2['id'] = "37";
        $productResult2 = $this->_productTable->getProductInfo($paramProductInfo2);
        $productResult20 = $productResult2[0];

        $moreProduct = array();
        array_push($moreProduct, $productResult10);
        array_push($moreProduct, $productResult20);

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'moreProduct' => $moreProduct,
            'file_upload_path_assets_url_thumbnail' => $this->_config['file_upload_path']['assets_url'] . 'product/thumbnail/',
            'file_upload_path_assets_url_full' => $this->_config['file_upload_path']['assets_url'] . 'product/',
            'discounts' => $discount,
            'wohid' => $wohid,
            'jsLangTranslate' => $jsLangTranslate,
            'honoreeName' => $honoreeName
        ));
        return $viewModel;
    }
    
    public function getMembershipId($userId, $param) {
        $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $userId));
        $membershipDetailArr = array();

        $membershipConfig = array();
        $membershipDetailArr = array();
        $savedSearchInDays = '';
        $savedSearchTypeIds = '';
        if ($userDetail['membership_id'] != '') {
            $membershipDetailArr[] = $userDetail['membership_id'];
            $getMembershipDetail = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
            if ($param == 'annotation') {
                $membershipConfig['is_annotation'] = $getMembershipDetail['is_annotation'];
            } else {
                $savedSearchInDays = explode(',', $getMembershipDetail['savedSearchInDays']);
                $savedSearchTypeIds = explode(',', $getMembershipDetail['savedSearchTypeIds']);
                $maxSearchInDays = explode(',', $getMembershipDetail['maxSavedSearchInDays']);
                
                $searchSaveType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getSearchSaveTypes();
                foreach($searchSaveType as $index=>$data){
                    $searchSaveTypes[$data['saved_search_type']] = $data['saved_search_type_id'];
                }
                @$saveSearchTypePS = $searchSaveTypes[$param];
                
                foreach ($savedSearchTypeIds as $key => $val) {
                    if ($val == $saveSearchTypePS) {
                            @$maxSearchCountPS = ($maxSearchInDays[$key]>$savedSearchInDays[$key])?$maxSearchInDays[$key]:$savedSearchInDays[$key];
                            
                    }
                    $membershipConfig[$val] = $savedSearchInDays[$key];
                }
            }
        }
        return @$maxSearchCountPS;
    }
    /**
     * This action is used to show the woh image
     * @param void
     * @return void
     * @author Icreon Tech - SR
     */
	public function viewWohImageAction()
	{
        $this->getConfig();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();	
        $this->layout('popup');
        $assetsPath = $this->_config['file_upload_path']['assets_url'];
		$viewModel = new ViewModel();
       $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
			'panel_image'=> $this->decrypt($params['panel_id']),
			'assetsPath'=>$assetsPath
        ));
        return $viewModel;
	}
}

