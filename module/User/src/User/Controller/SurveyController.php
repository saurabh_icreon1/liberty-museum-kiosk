<?php

/**
 * This controller is used to display user survey
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Controller;

use Base\Controller\BaseController;
use Zend\Authentication\AuthenticationService;
use Zend\View\Model\ViewModel;
use User\Model\Survey;
use Zend\Mvc\Controller\Plugin\Params;
use Zend\Http\Request;
use User\Form\SurveyForm;

/**
 * This controller is used to display user survey
 * @package    User
 * @author     Icreon Tech - DG
 */
class SurveyController extends BaseController {

    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_surveyTable = null;
    protected $_flashMessage = null;

    public function __construct() {
        $auth = new AuthenticationService();
        $this->auth = $auth;
        $this->_flashMessage = $this->flashMessenger();
    }

    public function indexAction() {
        
    }

    /**
     * This functikon is used to get table connections
     * @return     array 
     * @author Icreon Tech - DG
     */
    public function getTables() {
        $sm = $this->getServiceLocator();
        $this->_surveyTable = $sm->get('User\Model\SurveyTable');
        $this->_adapter = $sm->get('dbAdapter');
        return $this->_surveyTable;
    }

    /**
     * This functikon is used to get config variables
     * @return     array 
     * @author Icreon Tech - DG
     */
    public function getConfig() {
        $sm = $this->getServiceLocator();
        $this->_translator = $sm->get('translator');
        $this->_config = $sm->get('Config');
    }

    /**
     * This Action is used to display the user development
     * @author Icreon Tech-DG
     */
    public function getUserSurveyAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getConfig();
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $survey_messages = $this->_config['user_messages']['config']['survey'];

        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = '';
        $form = new SurveyForm();
        $survey_questions = $this->getTables()->getSurveyQuestions();

        $check_box_question = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }

        foreach ($survey_questions as $val) {
            if ($val['section_type'] == 2) {
                $check_box_question[] = $val['survey_question_id'];
            }
        }

        $params = $this->params()->fromRoute();
        $data = array();
        $data['user_id'] = $this->decrypt($params['user_id']);
        $user_survey = $this->getTables()->getUserSurvey($data);
        $surveyImmigrate = NULL;
        $this->addSurveyFormElementsToForm($form, $survey_questions);
        if (!empty($user_survey) && count($user_survey) > 0) {

            foreach ($user_survey as $val) {
                $form->get($val['survey_question_id'])->setAttribute('value', $val['survey_answer']);
                if (!empty($val['additional_info'])) {
                    $surveyImmigrate = $val['additional_info'];
                }
            }
        }


        $form->get('user_id')->setAttribute('value', $params['user_id']);

        if ($request->isPost()) {
            $data['user_id'] = $this->decrypt($params['user_id']);
            $form_data = $request->getPost();
            unset($form_data['submit']);
            unset($form_data['user_id']);
            if (!empty($form_data)) {
                /** insert into the change log */
                $changeLogArray = array();
                $changeLogArray['table_name'] = 'tbl_usr_change_logs';
                $changeLogArray['activity'] = '2';/** 2 == for update */
                $changeLogArray['id'] = $data['user_id'];
                $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                /** end insert into the change log */
                $this->getTables()->deleteUserSurvey($data);

                foreach ($form_data as $key => $val) {

                    if ($key == "4") {
                        $data['additional_info'] = isset($form_data['survey_immigrate']) ? $form_data['survey_immigrate'] : "";
                        $data['survey_question'] = $key;
                        $data['survey_answer'] = $val;
                        $this->getTables()->insertUserSurvey($data);
                    } else if ($key != "survey_immigrate") {
                        if (in_array($key, $check_box_question)) {
                            if ($val) {
                                $data['additional_info'] = "";
                                $data['survey_question'] = $key;
                                $data['survey_answer'] = '1';
                                $this->getTables()->insertUserSurvey($data);
                            }
                        } else {
                            $data['additional_info'] = "";
                            $data['survey_question'] = $key;
                            $data['survey_answer'] = $val;
                            $this->getTables()->insertUserSurvey($data);
                        }
                    }
                }
            }
            $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['survey']['SURVEY_UPDATED_SUCCESSFULLY']);
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
        $viewModel->setVariables(array(
            'form' => $form,
            'messages' => $messages,
            'survey_questions' => $survey_questions,
            'jsLangTranslate' => $survey_messages,
            'surveyImmigrate' => $surveyImmigrate
                )
        );
        return $viewModel;
    }

    /**
     * This action is used for add survey form element to form
     * @return     void
     * @param object form, array survey questions
     * @author Icreon Tech - AP
     */
    public function addSurveyFormElementsToForm($form, $survey_questions, $input_type = 'checkbox') {
        foreach ($survey_questions as $questions) {
            $section_id = $questions['survey_section_id'];
            //$element_name = 'survey_question_' . $questions['survey_question_id'];
            $element_name = $questions['survey_question_id'];
            $value = $questions['survey_question_id'];
            if ($input_type != 'hidden') {
                if ($questions['section_type'] == 1) {
                    $form->add(array(
                        'type' => 'Zend\Form\Element\Radio',
                        'name' => $element_name,
                        'options' => array(
                            'value_options' => array(
                                '0' => 'No',
                                '1' => 'Yes'
                            )
                        ),
                        'attributes' => array(
                            'id' => $element_name,
                            'class' => 'e3 rad',
                        )
                    ));
                } else {
                    $form->add(array(
                        'type' => 'Zend\Form\Element\Checkbox',
                        'name' => $element_name,
                        'attributes' => array(
                            'id' => $element_name,
                            'class' => 'e2 checkbox'
                        )
                    ));
                }
            } else {
                $form->add(array(
                    'type' => 'hidden',
                    'name' => $element_name,
                    'attributes' => array(
                        'id' => $element_name
                    )
                ));
            }
        }
    }

    /**
     * This Action is used to add edit user survey info
     * @author Icreon Tech-DG
     */
    public function addEditUserSurveyInfoAction(){
        $this->checkFrontUserAuthentication();
        $auth = new AuthenticationService();
        $this->getConfig();
        $this->getTables();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $survey_messages = $this->_config['user_messages']['config']['survey'];

        $request = $this->getRequest();
        $response = $this->getResponse();
        $form = new SurveyForm();
        $survey_questions = $this->getTables()->getSurveyQuestions();
        $check_box_question = array();
        $flashMessages = '';
        if ($this->_flashMessage->hasMessages()) {
            $flashMessages = $this->_flashMessage->getMessages();
        }
        foreach ($survey_questions as $val) {
            if ($val['section_type'] == 2) {
                $check_box_question[] = $val['survey_question_id'];
            }
        }
        $params = $this->params()->fromRoute();
        $data = array();
        $data['user_id'] = $this->decrypt($params['user_id']);
        $user_survey = $this->getTables()->getUserSurvey($data);
        $surveyImmigrate = NULL;
        $this->addSurveyFormElementsToForm($form, $survey_questions);
        if (!empty($user_survey) && count($user_survey) > 0) {
            foreach ($user_survey as $val) {
                $form->get($val['survey_question_id'])->setAttribute('value', $val['survey_answer']);
                if (!empty($val['additional_info'])) {
                    $surveyImmigrate = $val['additional_info'];
                }
            }
        }

        $form->get('user_id')->setAttribute('value', $params['user_id']);

        if ($request->isPost()) {
            $data['user_id'] = $this->decrypt($params['user_id']);
            $form_data = $request->getPost();
            unset($form_data['submit']);
            unset($form_data['user_id']);
            if (!empty($form_data)) {                
                $this->getTables()->deleteUserSurvey($data);
                foreach ($form_data as $key => $val) {
                    if ($key == "4") {
                        $data['additional_info'] = isset($form_data['survey_immigrate']) ? $form_data['survey_immigrate'] : "";
                        $data['survey_question'] = $key;
                        $data['survey_answer'] = $val;
                        $this->getTables()->insertUserSurvey($data);
                    } else if ($key != "survey_immigrate") {
                        if (in_array($key, $check_box_question)) {
                            if ($val) {
                                $data['additional_info'] = "";
                                $data['survey_question'] = $key;
                                $data['survey_answer'] = '1';
                                $this->getTables()->insertUserSurvey($data);
                            }
                        } else {
                            $data['additional_info'] = "";
                            $data['survey_question'] = $key;
                            $data['survey_answer'] = $val;
                            $this->getTables()->insertUserSurvey($data);
                        }
                    }
                }
            }
            $this->flashMessenger()->addMessage($this->_config['user_messages']['config']['survey']['SURVEY_UPDATED_SUCCESSFULLY']);
            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
        $viewModel->setVariables(array(
            'form' => $form,
            'flashMessages' => $flashMessages,
            'survey_questions' => $survey_questions,
            'jsLangTranslate' => $survey_messages,
            'surveyImmigrate' => $surveyImmigrate
                )
        );
        return $viewModel;
    }
}