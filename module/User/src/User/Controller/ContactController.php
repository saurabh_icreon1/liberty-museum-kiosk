<?php

/**
 * This controller is used for add, edit and search contact
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Controller;

use Base\Model\UploadHandler;
use Base\Controller\BaseController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Zend\Mvc\Controller\Plugin\Params;
use Zend\Http\Request;
use Zend\Authentication\AuthenticationService;
use User\Model\Contact;
use User\Model\User;
use Common\Model\Country;
use Common\Model\Common;
use Logs\Model\Changelog;
use User\Form\CorporateContactAddressesForm;
use User\Form\ContactIndividualForm;
use User\Form\ContactCorporteFoundationForm;
use User\Form\ContactPhonenoForm;
use User\Form\ContactWebsiteForm;
use User\Form\ContactAddressesForm;
use User\Form\ContactUploadForm;
use User\Form\SearchContactForm;
use User\Form\SaveSearchForm;
use User\Form\ContactEducationForm;
use User\Form\ContactAfihcForm;
use User\Form\ContactPopupForm;
use User\Form\ContactStatusForm;
use User\Form\CorporateUploadForm;
use User\Form\CorporatePhonenoForm;
use User\Form\UpdateContactUserForm;
use Passenger\Form\UserFamilyHistoryBonus;
use User\Form\UserSaveSearchBonus;
use Base\Model\SpreadsheetExcelReader;

/**
 * This controller is used for add, edit and search contact
 * @package    User
 * @author     Icreon Tech - DG
 */
class ContactController extends BaseController {

    protected $_contactTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_authservice = null;
    protected $_userTable = null;

    public function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        $auth = new AuthenticationService();
        $this->auth = $auth;
        if ($this->auth->hasIdentity() === false) {
            //return $this->redirect()->toRoute('crm');
        }
    }

    public function indexAction() {
        
    }

    /**
     * This functikon is used to get config variables
     * @return     array
     * @author Icreon Tech - DG
     */
    public function getConfig() {
        $sm = $this->getServiceLocator();
        $this->_translator = $sm->get('translator');
        $this->_config = $sm->get('Config');
    }

    /**
     * This functikon is used to get model
     * @return     array 
     * @author Icreon Tech - DG
     */
    public function getContactTable() {
        if (!$this->_contactTable) {
            $sm = $this->getServiceLocator();
            $this->_contactTable = $sm->get('User\Model\ContactTable');
            $this->_adapter = $sm->get('dbAdapter');
        }
        return $this->_contactTable;
    }

    /**
     * This functikon is used to get model
     * @return     array 
     * @author Icreon Tech - DG
     */
    public function getUserTable() {
        if (!$this->_userTable) {
            $sm = $this->getServiceLocator();
            $this->_userTable = $sm->get('User\Model\UserTable');
        }
        return $this->_userTable;
    }

    /**
     * This Action is used to get contact listing and search contact
     * @author Icreon Tech-DG
     * @return (JSON)
     */
    public function getContactAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $searchContactForm = new SearchContactForm();
        $saveSearchForm = new SaveSearchForm();
        $this->getConfig();
        $this->getContactTable();
        $params = $this->params()->fromRoute();
        $messages = '';
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        $newContact = $this->encrypt(0);
        if (isset($params['new_contact']) && $params['new_contact'] != '') {
            $newContact = $this->encrypt(1);
        }

        $dataParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
        $dataParam['is_active'] = '1';
        $dataParam['contact_search_id'] = '';
        $crmSearchArray = $this->getContactTable()->getSavedSearch($dataParam);
        $srmSearchList = array();
        $srmSearchList[''] = 'Select';
        if (!empty($crmSearchArray)) {
            foreach ($crmSearchArray as $key => $val) {
                $srmSearchList[$val['contact_search_id']] = stripslashes($val['title']);
            }
        }
        $searchContactForm->get('saved_search')->setAttribute('options', $srmSearchList);
        $getSource = $this->getServiceLocator()->get('Common\Model\CommonTable')->getSource('');
        $sourceList = array();
        $sourceList[''] = 'Please Select';
        foreach ($getSource as $key => $val) {
            $sourceList[$val['source_id']] = $val['source_name'];
        }
        $searchContactForm->get('source')->setAttribute('options', $sourceList);

        $getMembership = $this->getServiceLocator()->get('Common\Model\CommonTable')->getMembership();
        $membership_level = array();
        $membership_level[''] = 'Select';
        foreach ($getMembership as $key => $value) {
            $membership_level[$value['membership_id']] = $value['membership_title'];
        }
        $searchContactForm->get('membership_title')->setAttribute('options', $membership_level);
        $year = array();
        $year[''] = 'Select';
        for ($i = (date("Y") - 20); $i <= date("Y"); $i++) {
            $year[$i] = $i;
        }
        $searchContactForm->get('membership_year')->setAttribute('options', $year);
        $getUserType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getType();
        $userType = array();
        $userType[''] = 'Select Type';
        foreach ($getUserType as $key => $val) {
            $userType[$val['user_type_id']] = $val['user_type'];
        }
        $searchContactForm->get('type')->setAttribute('options', $userType);

        $get_user_groups = $this->getServiceLocator()->get('Common\Model\CommonTable')->getGroups();
        $user_group = array();
        foreach ($get_user_groups as $key => $val) {
            $user_group[$val['group_id']] = $val['name'];
        }
        $searchContactForm->get('groups')->setAttribute('options', $user_group);
        
        $get_tag = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTags();
        $user_tag_list = array();
        foreach ($get_tag as $key => $val) {
            $user_tag_list[$val['tag_id']] = $val['tag_name'];
        }
        $searchContactForm->get('corporate_tags')->setAttribute('options', $user_tag_list);

        $get_companies = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCompanies();
        $company_name = array();
        foreach ($get_companies as $key => $val) {
            $company_name[$val['company_id']] = $val['company_name'];
        }
        $searchContactForm->get('company')->setAttribute('options', $company_name);

        $get_transaction_range = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTransactionRange();
        $transaction_range = array();
        $transaction_range[' '] = 'Select';
        foreach ($get_transaction_range as $key => $val) {
            $transaction_range[$val['range_id']] = $val['range'];
        }
        $searchContactForm->get('last_transaction')->setAttribute('options', $transaction_range);
        $searchContactForm->get('registration_date')->setAttribute('options', $transaction_range);
        //$searchContactForm->get('registration_date')->setValue('3');
        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
        $country_list = array();
        foreach ($country as $key => $val) {
            $country_list[$val['country_id']] = $val['name'];
        }
        $searchContactForm->get('country_id')->setAttribute('options', $country_list);


        $state = $this->getServiceLocator()->get('Common\Model\CommonTable')->getStates();
        $stateList = array();
        $stateList[''] = 'Select State';
        foreach ($state as $key => $val) {
            $stateList[$val['state_code']] = $val['state'] . " - " . $val['state_code'];
        }
        $searchContactForm->get('state_select')->setAttribute('options', $stateList);

        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $response = $this->getResponse();

        $viewModel->setVariables(array(
            'search_contact_form' => $searchContactForm,
            'search_form' => $saveSearchForm,
            'country_list' => $country_list,
            'get_membership' => $getMembership,
            'messages' => $messages,
            'company_name' => $company_name,
            'jsLangTranslate' => $this->_config['user_messages']['config']['search_contact'],
            'newContact' => $newContact)
        );
        return $viewModel;
    }

    /**
     * This Action is used to get search contact
     * @author Icreon Tech-DG
     * @return (JSON)
     */
    public function searchContactAction() {
        $this->checkUserAuthentication();
        try {
            $request = $this->getRequest();
            $response = $this->getResponse();
            $this->getConfig();
            $messages = array();
            $params = $this->params()->fromRoute();
            parse_str($request->getPost('searchString'), $searchParam);
            $filePath = 'assets/export/';
            $dashlet = $request->getPost('crm_dashlet');
            $dashletSearchId = $request->getPost('searchId');
            $dashletId = $request->getPost('dashletId');
            $dashletSearchType = $request->getPost('searchType');
            $dashletSearchString = $request->getPost('dashletSearchString');
            if (isset($dashletSearchString) && $dashletSearchString != '') {
                $searchParam = unserialize($dashletSearchString);
            }
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit;
            $isExport = $request->getPost('export');
            if ($isExport != "" && $isExport == "excel") {
                ini_set('max_execution_time',0);
                $chunksize = $this->_config['export']['chunk_size'];
                $export_limit = $this->_config['export']['export_limit'];

                $searchParam['recordLimit'] = $chunksize;
            }
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            if (isset($searchParam['last_transaction']) && $searchParam['last_transaction'] != 1 && $searchParam['last_transaction'] != '') {
                $date_range = $this->getDateRange($searchParam['last_transaction']);
                $searchParam['from_date'] = $this->DateFormat($date_range['from'], 'db_date_format_from');
                $searchParam['to_date'] = $this->DateFormat($date_range['to'], 'db_date_format_to');
            } else if (isset($searchParam['last_transaction']) && $searchParam['last_transaction'] == '') {
                $searchParam['from_date'] = '';
                $searchParam['to_date'] = '';
            } else {
				if (isset($searchParam['from_date']) && $searchParam['from_date'] != '') {
                    $searchParam['from_date'] = $this->DateFormat($searchParam['from_date'], 'db_date_format_from');
                }
                if (isset($searchParam['to_date']) && $searchParam['to_date'] != '') {
                    $searchParam['to_date'] = $this->DateFormat($searchParam['to_date'], 'db_date_format_to');
                }
            }
                        
            // registration date - start
            if (isset($searchParam['registration_date']) && $searchParam['registration_date'] != 1 && $searchParam['registration_date'] != '') {
                $date_range = $this->getDateRange($searchParam['registration_date']);
                $searchParam['registration_date_from'] = $this->DateFormat($date_range['from'], 'db_date_format_from');
                $searchParam['registration_date_to'] = $this->DateFormat($date_range['to'], 'db_date_format_to');
            } else if (isset($searchParam['registration_date']) && $searchParam['registration_date'] == '') {
                $searchParam['registration_date_from'] = '';
                $searchParam['registration_date_to'] = '';
            } else {
                if (isset($searchParam['registration_date_from']) && $searchParam['registration_date_from'] != '') {
                    $searchParam['registration_date_from'] = $this->DateFormat($searchParam['registration_date_from'], 'db_date_format_from');
                }
                if (isset($searchParam['registration_date_to']) && $searchParam['registration_date_to'] != '') {
                    $searchParam['registration_date_to'] = $this->DateFormat($searchParam['registration_date_to'], 'db_date_format_to');
                }
            }
            // registration date - end

            if (isset($searchParam['added_date_range']) && $searchParam['added_date_range'] != 1 && $searchParam['added_date_range'] != '') {
                $date_range = $this->getDateRange($searchParam['added_date_range']);
                $searchParam['from_date'] = $this->DateFormat($date_range['from'], 'db_date_format_from');
                $searchParam['to_date'] = $this->DateFormat($date_range['to'], 'db_date_format_to');
            } else if (isset($searchParam['added_date_range']) && $searchParam['added_date_range'] == '') {
                $searchParam['from_date'] = '';
                $searchParam['to_date'] = '';
            } else {
                if (isset($searchParam['added_date_from']) && $searchParam['added_date_from'] != '') {
                    $searchParam['from_date'] = $this->DateFormat($searchParam['added_date_from'], 'db_date_format_from');
                }
                if (isset($searchParam['added_date_to']) && $searchParam['added_date_to'] != '') {
                    $searchParam['to_date'] = $this->DateFormat($searchParam['added_date_to'], 'db_date_format_to');
                }
            }
            $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'modified_date') ? 'users.added_date' : $searchParam['sort_field'];
            if (isset($searchParam['state'])) {
                $searchParam['state'] = (isset($searchParam['is_usa']) && $searchParam['is_usa'] == '1') ? $searchParam['state_select'] : $searchParam['state'];
            }
            if ($this->_flashMessage->hasMessages()) {
                $searchParam['sort_field'] = 'users.added_date';
                $searchParam['sort_order'] = 'desc';
            }
            $searchParam['new_contact'] = 0;

            if ($this->decrypt($params['new_contact']) == 1 && !isset($searchParam['added_date_range'])) {
                $searchParam['new_contact'] = DATE_TIME_FORMAT;
            }
            $page_counter = 1;
            //ticket:976 Export on CSV
            $fileCounter = 1;
            $filename =  "contact_export_" . EXPORT_DATE_TIME_FORMAT;
            do {
              //  $totUsers = 0;
                $searchParam['user_email'] = (isset($searchParam['user_email']) and trim($searchParam['user_email']) != "") ? trim(preg_replace('/ +/', ' ', $searchParam['user_email'])) : "";
                
                // added - new -start 
                $searchParam['first_name'] = (isset($searchParam['first_name']) and trim($searchParam['first_name']) != "") ? trim(preg_replace('/ +/', ' ', $searchParam['first_name'])) : "";
                $searchParam['last_name'] = (isset($searchParam['last_name']) and trim($searchParam['last_name']) != "") ? trim(preg_replace('/ +/', ' ', $searchParam['last_name'])) : "";
                $searchParam['company_name'] = (isset($searchParam['company_name']) and trim($searchParam['company_name']) != "") ? trim(preg_replace('/ +/', ' ', $searchParam['company_name'])) : "";
                $searchParam['email_address'] = (isset($searchParam['email_address']) and trim($searchParam['email_address']) != "") ? trim(preg_replace('/ +/', ' ', $searchParam['email_address'])) : "";
                // added - new - end
                
		$get_contact_arr = $this->getContactTable()->getContact($searchParam);
                //  if($totUsers == 0) { $totUsers = (isset($get_contact_arr[0]['totUsers']) and trim($get_contact_arr[0]['totUsers']) != "") ? trim($get_contact_arr[0]['totUsers']) : 0; }
                
                //asd($get_contact_arr);
                $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
                $dashletResult = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getTableStruct(array('user_id' => $this->auth->getIdentity()->crm_user_id, 'search_id' => $dashletSearchId, 'dash_id' => $dashletId, 'search_type' => $dashletSearchType));
                $newExplodedColumnName = array();
                if (!empty($dashletResult)) {
                    $explodedColumnName = explode(",", $dashletResult[0]['table_column_names']);
                    foreach ($explodedColumnName as $val) {
                        if (strpos($val, ".")) {
                            $newExplodedColumnName[] = trim(strstr($val, ".", false), ".");
                        } else {
                            $newExplodedColumnName[] = trim($val);
                        }
                    }
                }
                $total = $countResult[0]->RecordCount; //$totUsers; 
                $jsonResult = array();
                $arrCell = array();
                $subArrCell = array();
                if ($isExport == "excel") {
                    if ($total > $chunksize) {
                        $number_of_pages = ceil($total / $chunksize);
                        $page_counter++;
                        $start = $chunksize * $page_counter - $chunksize;
                        $searchParam['start_index'] = $start;
                        $searchParam['page'] = $page_counter;
                    } else {
                        $number_of_pages = 1;
                        $page_counter++;
                    }
                } else {
                    $page_counter = $page;
                    $number_of_pages = ceil($total / $limit);
                    $jsonResult['records'] = $countResult[0]->RecordCount; //$totUsers;
                    $jsonResult['page'] = $request->getPost('page');
                    $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit); // $totUsers 
                }

                if (!empty($get_contact_arr)) {
                    $arrExport = array();
                    foreach ($get_contact_arr as $key => $val) {
                        $dashletCell = array();
                        $view = $this->encrypt('view');
                        $edit = $this->encrypt('edit');
                        
                        
                        $addressAddress = (isset($val['address_name1']) and trim($val['address_name1']) != "") ? trim($val['address_name1'])." ".((isset($val['address_name2']) and trim($val['address_name2']) != "") ? ",".trim($val['address_name2']):"") : trim($val['address_name1']);
                        $addressAddress = (isset($addressAddress) and trim($addressAddress) != "") ? trim($addressAddress) : "N/A";
                        $addressCity = (isset($val['city_name']) and trim($val['city_name']) != "") ? trim($val['city_name']) : "N/A";
                        $addressStateName = (isset($val['state_name']) and trim($val['state_name']) != "") ? trim($val['state_name']) : "N/A";
                        $addressZipCode = (isset($val['zipcode_name']) and trim($val['zipcode_name']) != "") ? trim($val['zipcode_name']) : "N/A";


                        if ($val['is_active'] == '1') {
                            $class = 'blue-icon';
                        } else {
                            $class = '';
                        }
                        if ($val['is_blocked'] == 1) {
                            $lock = '<a href="#unlock_contact_content" class="unlock_contact unlock-icon" style="margin-left: 11px;" onclick="unlockUserStatus(\'' . $this->encrypt($val['user_id']) . '\',\'' . $this->encrypt(0) . '\');" href="javascript:void(0);" id="lock-batch-link-97">Lock<div class="tooltip">Unlock User<span></span></div></a>';
                        } else {
                            $lock = '<a href="#lock_contact_content" class="lock_contact lock-icon" style="margin-left: 11px;" onclick="lockUserStatus(\'' . $this->encrypt($val['user_id']) . '\',\'' . $this->encrypt(1) . '\');" href="javascript:void(0);" id="lock-batch-link-97">Lock<div class="tooltip">Lock User<span></span></div></a>';
                        }

                        $action = "<div class='action-col search-result " . $class . "'>" . $lock . "<a href='/view-contact/" . $this->encrypt($val['user_id']) . "/" . $view . 
                                "' class='view-icon'>View<div class='tooltip'>View<span></span></div></a><a href='/view-contact/" . $this->encrypt($val['user_id']) . "/" . $edit . 
                                "' class='edit-icon'>Edit<div class='tooltip'>Edit<span></span></div></a>
                                <a href='#delete_contact_content' onclick='deleteContact(" . $val['user_id'] . ");' class='delete_contact delete-icon'>Delete<div class='tooltip'>Delete<span></span></div></a>
                                </div>";

                        $arrCell['id'] = $val['user_id'];
                        if ($val['user_type'] == 1) {
                            $userType = 'Individual';
                            $dashletUserType = array_search('user_type', $newExplodedColumnName);
                            if ($dashletUserType !== false)
                                $dashletCell[$dashletUserType] = $userType;
                        }
                        if ($val['user_type'] == 2) {
                            $userType = 'Corporate';
                            $dashletUserType = array_search('user_type', $newExplodedColumnName);
                            if ($dashletUserType !== false)
                                $dashletCell[$dashletUserType] = $userType;
                        }
                        if ($val['user_type'] == 3) {
                            $userType = 'Foundation';
                            $dashletUserType = array_search('user_type', $newExplodedColumnName);
                            if ($dashletUserType !== false)
                                $dashletCell[$dashletUserType] = $userType;
                        }
                        if ($val['level_color']) {
                            $level_color = '<div class="full"><span class="label1" style="background: ' . $val['level_color'] . '"></span></div>';
                        } else {
                            $level_color = '';
                        }

                        $addActivity = $caseContactId = '<span class="plus-sign top"><a href="/create-activity/' . $this->encrypt($val['user_id']) . '/' . $this->encrypt('1') . '"><div class="tooltip">Add activity<span></span></div></a></span>';
                        $omxId = (isset($val['omx_customer_number']) && !empty($val['omx_customer_number'])) ? 'OMX (' . $val['omx_customer_number'] . ')' : '';
                        if ($val['user_type'] == '1') {
                            $name = $val['full_name'];
                            $dashletFullName = array_search('full_name', $newExplodedColumnName);
                            if ($dashletFullName !== false)
                                $dashletCell[$dashletFullName] = $name;
                        } else {
                            $name = $val['company_name'];
                            $dashletFullName = array_search('full_name', $newExplodedColumnName);
                            if ($dashletFullName !== false)
                                $dashletCell[$dashletFullName] = $name;
                        }

                        if ($val['email_id'] != '') {
                            $val['email_id'] = $val['email_id'];
                            $dashletEmail = array_search('email_id', $newExplodedColumnName);
                            if ($dashletEmail !== false)
                                $dashletCell[$dashletEmail] = $val['email_id'];
                        } else {
                            $val['email_id'] = 'N/A';
                            $dashletEmail = array_search('email_id', $newExplodedColumnName);
                            if ($dashletEmail !== false)
                                $dashletCell[$dashletEmail] = $val['email_id'];
                        }
                        if ($val['country_name'] != '') {
                            $val['country_name'] = $val['country_name'];
                            $dashletCountryName = array_search('country_name', $newExplodedColumnName);
                            if ($dashletCountryName !== false) {
                                $dashletCell[$dashletCountryName] = $val['country_name'];
                            }
                        } else {
                            $val['country_name'] = 'N/A';
                            $dashletCountryName = array_search('country_name', $newExplodedColumnName);
                            if ($dashletCountryName !== false) {
                                $dashletCell[$dashletCountryName] = $val['country_name'];
                            }
                        }
                        if ($val['transaction_id'] != '') {
                            $val['transaction_id'] = $val['transaction_id'];
                            $dashletTransactionId = array_search('transaction_id', $newExplodedColumnName);
                            if ($dashletTransactionId !== false)
                                $dashletCell[$dashletTransactionId] = $val['transaction_id'];
                        } else {
                            $val['transaction_id'] = 'N/A';
                            $dashletTransactionId = array_search('transaction_id', $newExplodedColumnName);
                            if ($dashletTransactionId !== false)
                                $dashletCell[$dashletTransactionId] = $val['transaction_id'];
                        }
                        if ($val['transaction_amount'] != '') {
                            $val['transaction_amount'] = "$ " . $val['transaction_amount'];
                            $dashletTransactionAmt = array_search('transaction_amount', $newExplodedColumnName);
                            if ($dashletTransactionAmt !== false)
                                $dashletCell[$dashletTransactionAmt] = $val['transaction_amount'];
                        } else {
                            $val['transaction_amount'] = 'N/A';
                            $dashletTransactionAmt = array_search('transaction_amount', $newExplodedColumnName);
                            if ($dashletTransactionAmt !== false)
                                $dashletCell[$dashletTransactionAmt] = $val['transaction_amount'];
                        }
                        if ($val['transaction_date'] != '') {
                            $transactionDate = $this->OutputDateFormat($val['transaction_date'], 'dateformatampm');
                            $dashletTransactionDt = array_search('transaction_date', $newExplodedColumnName);
                            if ($dashletTransactionDt !== false)
                                $dashletCell[$dashletTransactionDt] = $transactionDate;
                        } else {
                            $transactionDate = 'N/A';
                            $dashletTransactionDt = array_search('transaction_date', $newExplodedColumnName);
                            if ($dashletTransactionDt !== false)
                                $dashletCell[$dashletTransactionDt] = $transactionDate;
                        }

                        if ($val['membership_title'] != '') {
                            $val['membership_title'] = $val['membership_title'];
                            $dashletMembership = array_search('membership_title', $newExplodedColumnName);
                            if ($dashletMembership !== false)
                                $dashletCell[$dashletMembership] = $val['membership_title'];
                        } else {
                            $val['membership_title'] = 'N/A';
                            $dashletMembership = array_search('membership_title', $newExplodedColumnName);
                            if ($dashletMembership !== false)
                                $dashletCell[$dashletMembership] = $val['membership_title'];
                        }


                        if ($val['contact_id'] != '') {
                            $dashletContactId = array_search('contact_id', $newExplodedColumnName);
                            if ($dashletContactId !== false)
                                $dashletCell[$dashletContactId] = "<a class='txt-decoration-underline' href='/view-contact/" . $this->encrypt($val['user_id']) . "/" . $view . "' >" . $val['contact_id'] . "</a>";
                        }
                        if (isset($dashlet) && $dashlet == 'dashlet') {
                            $arrCell['cell'] = $dashletCell;
                        } else if ($isExport == "excel") {
                            $arrExport[] = array('Name' => $name, 'Email' => $val['email_id']/*, 'User Type' => $userType*/, 'Membership Level' => $val['membership_title'], 'Address' => $addressAddress, 'City' => $addressCity, 'State' => $addressStateName, 'Zip Code' => $addressZipCode, 'Country' => $val['country_name'], /*'Last Transaction#' => $val['transaction_id'],*/ 'Last Transaction Date' => $transactionDate/*, 'Last Transction Amount' => $val['transaction_amount']*/);
                        } else {
                            $omxId2 = (isset($omxId) and trim($omxId) != "") ? $omxId . "<br>" : "";
                            $arrCell['cell'] = array($val['contact_id'] . "<br>" . $omxId2 . $addActivity, $name, $val['email_id'], /*$userType,*/ $val['membership_title'] . " " . $level_color, $addressAddress, $addressCity, $addressStateName, $addressZipCode, (isset($val['country_code']) and trim($val['country_code']) != "") ? trim($val['country_code']) : "N/A", /*$val['transaction_id'],*/ $transactionDate/*, $val['transaction_amount']*/, $action);
                        }
                        $subArrCell[] = $arrCell;
                        $jsonResult['rows'] = $subArrCell;
                    }
                    if ($isExport == "excel") {
                        //$filename = "contact_export_" . date("Y-m-d") . ".csv";
                        //$this->arrayToCsv($arrExport, $filename, $page_counter);
                        //ticket:976 Export on CSV
                        $fileLimit = $this->_config['csv_file']['max_records'];
                        $fileCounter = $this->exportCSV($arrExport,$filename,$fileLimit,$fileCounter);
                    }
                }
            } while ($page_counter <= $number_of_pages && $isExport == "excel");
            if ($isExport == "excel") {
                $this->downloadSendHeaders($filename.".zip");
                //ticket:976 Export on CSV
                //removing temp files
                $tempfilePath = $this->_config['file_upload_path']['temp_data_upload_dir'] . 'export/';
                //sudo chmod -R ug+rw export/   folder should have this permission
                $tempFiles = glob($tempfilePath .$filename. '*' , GLOB_BRACE);
                foreach ($tempFiles as $file)
                {
                    if (is_file($file))
                    {
                        unlink($file);
                    }
                }
                die;
            } else {
                $response->setContent(\Zend\Json\Json::encode($jsonResult));
                return $response;
            }
        } catch (Exception $e) {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * This Action is used to get contact listing and search contact
     * @author Icreon Tech-DG
     * @return (JSON)
     */
    public function getContactMembershipPackageAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $searchContactForm = new SearchContactForm();
        $saveSearchForm = new SaveSearchForm();
        $this->getConfig();
        $this->getContactTable();
        $params = $this->params()->fromRoute();
        $messages = '';
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        $newContact = $this->encrypt(0);
        if (isset($params['new_contact']) && $params['new_contact'] != '') {
            $newContact = $this->encrypt(1);
        }

        $dataParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
        $dataParam['is_active'] = '1';
        $dataParam['contact_search_id'] = '';
        $crmSearchArray = $this->getContactTable()->getSavedSearch($dataParam);
        $srmSearchList = array();
        $srmSearchList[''] = 'Select';
        if (!empty($crmSearchArray)) {
            foreach ($crmSearchArray as $key => $val) {
                $srmSearchList[$val['contact_search_id']] = stripslashes($val['title']);
            }
        }
        $searchContactForm->get('saved_search')->setAttribute('options', $srmSearchList);
        $getSource = $this->getServiceLocator()->get('Common\Model\CommonTable')->getSource('');
        $sourceList = array();
        $sourceList[''] = 'Please Select';
        foreach ($getSource as $key => $val) {
            $sourceList[$val['source_id']] = $val['source_name'];
        }
        $searchContactForm->get('source')->setAttribute('options', $sourceList);

        $getMembership = $this->getServiceLocator()->get('Common\Model\CommonTable')->getMembership();
        $membership_level = array();
        $membership_level[''] = 'Select';
        foreach ($getMembership as $key => $value) {
            $membership_level[$value['membership_id']] = $value['membership_title'];
        }
        $searchContactForm->get('membership_title')->setAttribute('options', $membership_level);
        $year = array();
        $year[''] = 'Select';
        for ($i = (date("Y") - 10); $i <= (date("Y") + 10); $i++) {
            $year[$i] = $i;
        }
        $searchContactForm->get('membership_year')->setAttribute('options', $year);
        $searchContactForm->get('membership_cards_year')->setAttribute('options', $year);
        $getUserType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getType();
        $userType = array();
        $userType[''] = 'Select Type';
        foreach ($getUserType as $key => $val) {
            $userType[$val['user_type_id']] = $val['user_type'];
        }
        $searchContactForm->get('type')->setAttribute('options', $userType);

        $get_user_groups = $this->getServiceLocator()->get('Common\Model\CommonTable')->getGroups();
        $user_group = array();
        foreach ($get_user_groups as $key => $val) {
            $user_group[$val['group_id']] = $val['name'];
        }
        $searchContactForm->get('groups')->setAttribute('options', $user_group);

        $get_companies = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCompanies();
        $company_name = array();
        foreach ($get_companies as $key => $val) {
            $company_name[$val['company_id']] = $val['company_name'];
        }
        $searchContactForm->get('company')->setAttribute('options', $company_name);

        $get_transaction_range = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTransactionRange();
        $transaction_range = array();
        $transaction_range[' '] = 'Select';
        foreach ($get_transaction_range as $key => $val) {
            $transaction_range[$val['range_id']] = $val['range'];
        }
        $searchContactForm->get('last_transaction')->setAttribute('options', $transaction_range);
        $searchContactForm->get('registration_date')->setAttribute('options', $transaction_range);
        $searchContactForm->get('registration_date')->setValue('2');
        //$searchContactForm->get('last_transaction')->setValue('2');
        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
        $country_list = array();
        foreach ($country as $key => $val) {
            $country_list[$val['country_id']] = $val['name'];
        }
        $searchContactForm->get('country_id')->setAttribute('options', $country_list);


        $state = $this->getServiceLocator()->get('Common\Model\CommonTable')->getStates();
        $stateList = array();
        $stateList[''] = 'Select State';
        foreach ($state as $key => $val) {
            $stateList[$val['state_code']] = $val['state'] . " - " . $val['state_code'];
        }
        $searchContactForm->get('state_select')->setAttribute('options', $stateList);

        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $response = $this->getResponse();

        $viewModel->setVariables(array(
            'search_contact_form' => $searchContactForm,
            'search_form' => $saveSearchForm,
            'country_list' => $country_list,
            'get_membership' => $getMembership,
            'messages' => $messages,
            'company_name' => $company_name,
            'jsLangTranslate' => $this->_config['user_messages']['config']['search_contact'],
            'newContact' => $newContact,
            'updateContactUserForm' => new UpdateContactUserForm())
        );
        return $viewModel;
    }

    /**
     * This Action is used to get search contact
     * @author Icreon Tech-NS
     * @return (JSON)
     */
    public function searchContactMembershipPackageAction() {
        $this->checkUserAuthentication();
        try {
            $request = $this->getRequest();
            $response = $this->getResponse();
            $this->getConfig();
            $messages = array();
            $params = $this->params()->fromRoute();

            parse_str($request->getPost('searchString'), $searchParam);
            $filePath = 'assets/export/';
            $dashlet = $request->getPost('crm_dashlet');
            $dashletSearchId = $request->getPost('searchId');
            $dashletId = $request->getPost('dashletId');
            $dashletSearchType = $request->getPost('searchType');
            $dashletSearchString = $request->getPost('dashletSearchString');
            if (isset($dashletSearchString) && $dashletSearchString != '') {
                $searchParam = unserialize($dashletSearchString);
            }
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit;
            $isExport = $request->getPost('export');
            if ($isExport != "" && $isExport == "excel") {
                $chunksize = $this->_config['export']['chunk_size'];
                $export_limit = $this->_config['export']['export_limit'];

                $searchParam['recordLimit'] = $chunksize;
            }
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            if (isset($searchParam['last_transaction']) && $searchParam['last_transaction'] != 1 && $searchParam['last_transaction'] != '') {
                $date_range = $this->getDateRange($searchParam['last_transaction']);
                $searchParam['from_date'] = $this->DateFormat($date_range['from'], 'db_date_format');
                $searchParam['to_date'] = $this->DateFormat($date_range['to'], 'db_date_format');
            } else if (isset($searchParam['last_transaction']) && $searchParam['last_transaction'] == '') {
                $searchParam['from_date'] = '';
                $searchParam['to_date'] = '';
            } else {
                if (isset($searchParam['from_date']) && $searchParam['from_date'] != '') {
                    $searchParam['from_date'] = $this->DateFormat($searchParam['from_date'], 'db_date_format');
                }
                if (isset($searchParam['to_date']) && $searchParam['to_date'] != '') {
                    $searchParam['to_date'] = $this->DateFormat($searchParam['to_date'], 'db_date_format');
                }
            }

            // registration date - start
            if (isset($searchParam['registration_date']) && $searchParam['registration_date'] != 1 && $searchParam['registration_date'] != '') {
                $date_range = $this->getDateRange($searchParam['registration_date']);
                $searchParam['registration_date_from'] = $this->DateFormat($date_range['from'], 'db_date_format_from');
                $searchParam['registration_date_to'] = $this->DateFormat($date_range['to'], 'db_date_format_to');
            } else if (isset($searchParam['registration_date']) && $searchParam['registration_date'] == '') {
                $searchParam['registration_date_from'] = '';
                $searchParam['registration_date_to'] = '';
            } else {
                if (isset($searchParam['registration_date_from']) && $searchParam['registration_date_from'] != '') {
                    $searchParam['registration_date_from'] = $this->DateFormat($searchParam['registration_date_from'], 'db_date_format_from');
                }
                if (isset($searchParam['registration_date_to']) && $searchParam['registration_date_to'] != '') {
                    $searchParam['registration_date_to'] = $this->DateFormat($searchParam['registration_date_to'], 'db_date_format_to');
                }
            }
            // registration date - end

            if (isset($searchParam['added_date_range']) && $searchParam['added_date_range'] != 1 && $searchParam['added_date_range'] != '') {
                $date_range = $this->getDateRange($searchParam['added_date_range']);
                $searchParam['from_date'] = $this->DateFormat($date_range['from'], 'db_date_format');
                $searchParam['to_date'] = $this->DateFormat($date_range['to'], 'db_date_format');
            } else if (isset($searchParam['added_date_range']) && $searchParam['added_date_range'] == '') {
                $searchParam['from_date'] = '';
                $searchParam['to_date'] = '';
            } else {
                if (isset($searchParam['added_date_from']) && $searchParam['added_date_from'] != '') {
                    $searchParam['from_date'] = $this->DateFormat($searchParam['added_date_from'], 'db_date_format');
                }
                if (isset($searchParam['added_date_to']) && $searchParam['added_date_to'] != '') {
                    $searchParam['to_date'] = $this->DateFormat($searchParam['added_date_to'], 'db_date_format');
                }
            }
            $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'modified_date') ? 'users.added_date' : $searchParam['sort_field'];
            if (isset($searchParam['state'])) {
                $searchParam['state'] = (isset($searchParam['is_usa']) && $searchParam['is_usa'] == '1') ? $searchParam['state_select'] : $searchParam['state'];
            }
            if ($this->_flashMessage->hasMessages()) {
                $searchParam['sort_field'] = 'users.added_date';
                $searchParam['sort_order'] = 'desc';
            }
            $searchParam['new_contact'] = 0;
            if ($this->decrypt($params['new_contact']) == 1) {
                $searchParam['new_contact'] = DATE_TIME_FORMAT;
            }

            // n - start
            if (isset($searchParam['membership_cards_package_shipped']) and trim($searchParam['membership_cards_package_shipped']) != "") {
                $membershipCardsPackageShipped = trim($searchParam['membership_cards_package_shipped']);
                if ($membershipCardsPackageShipped == "1") {
                    $membershipCardsPackageShipped = "";
                } else if ($membershipCardsPackageShipped == "2") {
                    $membershipCardsPackageShipped = "0";
                } else if ($membershipCardsPackageShipped == "3") {
                    $membershipCardsPackageShipped = "1";
                } else {
                    $membershipCardsPackageShipped = "";
                }
                $searchParam['membership_cards_package_shipped'] = $membershipCardsPackageShipped;
            } else {
                $searchParam['membership_cards_package_shipped'] = "";
            }
            // n - end

            $page_counter = 1;
            do {
                $get_contact_arr = $this->getContactTable()->getContact($searchParam);                
                $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
                $dashletResult = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getTableStruct(array('user_id' => $this->auth->getIdentity()->crm_user_id, 'search_id' => $dashletSearchId, 'dash_id' => $dashletId, 'search_type' => $dashletSearchType));
                $newExplodedColumnName = array();
                if (!empty($dashletResult)) {
                    $explodedColumnName = explode(",", $dashletResult[0]['table_column_names']);
                    foreach ($explodedColumnName as $val) {
                        if (strpos($val, ".")) {
                            $newExplodedColumnName[] = trim(strstr($val, ".", false), ".");
                        } else {
                            $newExplodedColumnName[] = trim($val);
                        }
                    }
                }
                $total = $countResult[0]->RecordCount;
                $jsonResult = array();
                $arrCell = array();
                $subArrCell = array();
                if ($isExport == "excel") {
                    if ($total > $chunksize) {
                        $number_of_pages = ceil($total / $chunksize);
                        $page_counter++;
                        $start = $chunksize * $page_counter - $chunksize;
                        $searchParam['start_index'] = $start;
                        $searchParam['page'] = $page_counter;
                    } else {
                        $number_of_pages = 1;
                        $page_counter++;
                    }
                } else {
                    $page_counter = $page;
                    $number_of_pages = ceil($total / $limit);
                    $jsonResult['records'] = $countResult[0]->RecordCount;
                    $jsonResult['page'] = $request->getPost('page');
                    $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
                }

                if (!empty($get_contact_arr)) {
                    $arrExport = array();
                    foreach ($get_contact_arr as $key => $val) {
                        $dashletCell = array();
                        $view = $this->encrypt('view');
                        $edit = $this->encrypt('edit');
                        if ($val['is_active'] == '1') {
                            $class = 'blue-icon';
                        } else {
                            $class = '';
                        }
                        $action = "<div class='action-col search-result " . $class . "'><a href='/view-contact/" . $this->encrypt($val['user_id']) . "/" . $view . "' class='view-icon'>View<div class='tooltip'>View<span></span></div></a><a href='/view-contact/" . $this->encrypt($val['user_id']) . "/" . $edit . "' class='edit-icon'>Edit<div class='tooltip'>Edit<span></span></div></a><a href='#delete_contact_content' onclick='deleteContact(" . $val['user_id'] . ");' class='delete_contact delete-icon'>Delete<div class='tooltip'>Delete<span></span></div></a></div>";
                        $arrCell['id'] = $val['user_id'];
                        if ($val['user_type'] == 1) {
                            $userType = 'Individual';
                            $dashletUserType = array_search('user_type', $newExplodedColumnName);
                            if ($dashletUserType !== false)
                                $dashletCell[$dashletUserType] = $userType;
                        }
                        if ($val['user_type'] == 2) {
                            $userType = 'Corporate';
                            $dashletUserType = array_search('user_type', $newExplodedColumnName);
                            if ($dashletUserType !== false)
                                $dashletCell[$dashletUserType] = $userType;
                        }
                        if ($val['user_type'] == 3) {
                            $userType = 'Foundation';
                            $dashletUserType = array_search('user_type', $newExplodedColumnName);
                            if ($dashletUserType !== false)
                                $dashletCell[$dashletUserType] = $userType;
                        }
                        if ($val['level_color']) {
                            $level_color = '<div class="full"><span class="label1" style="background: ' . $val['level_color'] . '"></span></div>';
                        } else {
                            $level_color = '';
                        }

                        $addActivity = $caseContactId = '<span class="plus-sign top"><a href="/create-activity/' . $this->encrypt($val['user_id']) . '/' . $this->encrypt('1') . '"><div class="tooltip">Add activity<span></span></div></a></span>';
                        $omxId = (isset($val['omx_customer_number']) && !empty($val['omx_customer_number'])) ? 'OMX (' . $val['omx_customer_number'] . ')' : '';
                        if ($val['full_name'] != '') {
                            $name = $val['full_name'];
                            $dashletFullName = array_search('full_name', $newExplodedColumnName);
                            if ($dashletFullName !== false)
                                $dashletCell[$dashletFullName] = $name;
                        } else {
                            $name = $val['company_name'];
                            $dashletFullName = array_search('full_name', $newExplodedColumnName);
                            if ($dashletFullName !== false)
                                $dashletCell[$dashletFullName] = $name;
                        }

                        if ($val['email_id'] != '') {
                            $val['email_id'] = $val['email_id'];
                            $dashletEmail = array_search('email_id', $newExplodedColumnName);
                            if ($dashletEmail !== false)
                                $dashletCell[$dashletEmail] = $val['email_id'];
                        } else {
                            $val['email_id'] = 'N/A';
                            $dashletEmail = array_search('email_id', $newExplodedColumnName);
                            if ($dashletEmail !== false)
                                $dashletCell[$dashletEmail] = $val['email_id'];
                        }
                        if ($val['country_name'] != '') {
                            $val['country_name'] = $val['country_name'];
                            $dashletCountryName = array_search('country_name', $newExplodedColumnName);
                            if ($dashletCountryName !== false) {
                                $dashletCell[$dashletCountryName] = $val['country_name'];
                            }
                        } else {
                            $val['country_name'] = 'N/A';
                            $dashletCountryName = array_search('country_name', $newExplodedColumnName);
                            if ($dashletCountryName !== false) {
                                $dashletCell[$dashletCountryName] = $val['country_name'];
                            }
                        }
                        if ($val['transaction_id'] != '') {
                            $val['transaction_id'] = $val['transaction_id'];
                            $dashletTransactionId = array_search('transaction_id', $newExplodedColumnName);
                            if ($dashletTransactionId !== false)
                                $dashletCell[$dashletTransactionId] = $val['transaction_id'];
                        } else {
                            $val['transaction_id'] = 'N/A';
                            $dashletTransactionId = array_search('transaction_id', $newExplodedColumnName);
                            if ($dashletTransactionId !== false)
                                $dashletCell[$dashletTransactionId] = $val['transaction_id'];
                        }
                        if ($val['transaction_amount'] != '') {
                            $val['transaction_amount'] = "$ " . $val['transaction_amount'];
                            $dashletTransactionAmt = array_search('transaction_amount', $newExplodedColumnName);
                            if ($dashletTransactionAmt !== false)
                                $dashletCell[$dashletTransactionAmt] = $val['transaction_amount'];
                        } else {
                            $val['transaction_amount'] = 'N/A';
                            $dashletTransactionAmt = array_search('transaction_amount', $newExplodedColumnName);
                            if ($dashletTransactionAmt !== false)
                                $dashletCell[$dashletTransactionAmt] = $val['transaction_amount'];
                        }
                        if ($val['transaction_date'] != '') {
                            $transactionDate = $this->OutputDateFormat($val['transaction_date'], 'dateformatampm');
                            $dashletTransactionDt = array_search('transaction_date', $newExplodedColumnName);
                            if ($dashletTransactionDt !== false)
                                $dashletCell[$dashletTransactionDt] = $transactionDate;
                        } else {
                            $transactionDate = 'N/A';
                            $dashletTransactionDt = array_search('transaction_date', $newExplodedColumnName);
                            if ($dashletTransactionDt !== false)
                                $dashletCell[$dashletTransactionDt] = $transactionDate;
                        }

                        if ($val['membership_title'] != '') {
                            $val['membership_title'] = $val['membership_title'];
                            $dashletMembership = array_search('membership_title', $newExplodedColumnName);
                            if ($dashletMembership !== false)
                                $dashletCell[$dashletMembership] = $val['membership_title'];
                        } else {
                            $val['membership_title'] = 'N/A';
                            $dashletMembership = array_search('membership_title', $newExplodedColumnName);
                            if ($dashletMembership !== false)
                                $dashletCell[$dashletMembership] = $val['membership_title'];
                        }


                        if ($val['contact_id'] != '') {
                            $dashletContactId = array_search('contact_id', $newExplodedColumnName);
                            if ($dashletContactId !== false)
                                $dashletCell[$dashletContactId] = "<a class='txt-decoration-underline' href='/view-contact/" . $this->encrypt($val['user_id']) . "/" . $view . "' >" . $val['contact_id'] . "</a>";
                        }
                        if (isset($dashlet) && $dashlet == 'dashlet') {
                            $arrCell['cell'] = $dashletCell;
                        } else if ($isExport == "excel") {
                            $arrExport[] = array('Name' => $name, 'Email' => $val['email_id'], 'User Type' => $userType, 'Membership Level' => $val['membership_title'], 'Last Transaction#' => $val['transaction_id'], 'Last Transaction Date' => $transactionDate, 'Last Transction Amount' => $val['transaction_amount'], 'Address 1' => $val['address_name1'], 'Address 2' => $val['address_name2'], 'City' => $val['city_name'], 'State' => $val['state_name'], 'Country' => $val['country_name'], 'Postal code' => $val['zipcode_name']);
                        } else {
                            $omxId2 = (isset($omxId) and trim($omxId) != "") ? $omxId . "<br>" : "";
                            $arrCell['cell'] = array($val['user_id'], $val['contact_id'] . "<br>" . $omxId2 . $addActivity, $name, $val['email_id'], $userType, $val['membership_title'] . " " . $level_color, $val['country_name'], $val['transaction_id'], $transactionDate, $val['transaction_amount'], $action);
                        }
                        $subArrCell[] = $arrCell;
                        $jsonResult['rows'] = $subArrCell;
                    }
                    if ($isExport == "excel") {
                        $filename = "contact_export_" . date("Y-m-d") . ".csv";
                        $this->arrayToCsv($arrExport, $filename, $page_counter);
                    }
                }
            } while ($page_counter <= $number_of_pages && $isExport == "excel");
            if ($isExport == "excel") {
                $this->downloadSendHeaders("contact_export_" . date("Y-m-d") . ".csv");
                die;
            } else {
                $response->setContent(\Zend\Json\Json::encode($jsonResult));
                return $response;
            }
        } catch (Exception $e) {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * This Action is used for update Contact User Membership Package Status
     * @param array
     * @return Json responce
     * @author Icreon Tech -NS
     */
    public function updateContactMembershipPackageStatusAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $contactMessages = array_merge($this->_config['user_messages']['config']['common'], $this->_config['user_messages']['config']['search_contact']);

        if ($request->isPost()) {
            $formData = $request->getPost();
            if (isset($formData['membership_package_value']) and trim($formData['membership_package_value']) != "") {

                $membershipUpdt = array();
                $membershipUpdt['membership_package_value'] = trim($formData['membership_package_value']);
                $membershipUpdt['user_ids'] = (isset($formData['ids']) and is_array($formData['ids']) and count($formData['ids']) > 0) ? implode(",", $formData['ids']) : null;
                $membershipUpdt['year'] = date("Y", strtotime(DATE_TIME_FORMAT));

                $status = $this->getContactTable()->updateContactMembershipCards($membershipUpdt);

                if ($status) {
                    if (trim($formData['membership_package_value']) == "0") {
                        $this->flashMessenger()->addMessage($contactMessages['L_UPDATE_OPTION_PENDING']);
                        $messages = array('status' => "success", 'message' => $contactMessages['L_UPDATE_OPTION_PENDING']);
                    } elseif (trim($formData['membership_package_value']) == "1") {
                        $this->flashMessenger()->addMessage($contactMessages['L_UPDATE_OPTION_SENT']);
                        $messages = array('status' => "success", 'message' => $contactMessages['L_UPDATE_OPTION_SENT']);
                    } else {
                        $this->flashMessenger()->addMessage($contactMessages['L_UPDATE_OPTION_ERROR']);
                        $messages = array('status' => "error", 'message' => $contactMessages['L_UPDATE_OPTION_ERROR']);
                    }
                } else {
                    $this->flashMessenger()->addMessage($contactMessages['L_UPDATE_OPTION_ERROR']);
                    $messages = array('status' => "error", 'message' => $contactMessages['L_UPDATE_OPTION_ERROR']);
                }
            } else {
                $this->flashMessenger()->addMessage($contactMessages['L_UPDATE_OPTION_ERROR']);
                $messages = array('status' => 'error');
            }

            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This Action is used to create contact
     * @author Icreon Tech-DG
     * @return (JSON)
     */
    public function createContactAction() {
        $this->layout('crm');
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getContactTable();
        $form_individual = new ContactIndividualForm();
        $form_cororporate_foundation = new ContactCorporteFoundationForm();
        $form_phone = new ContactPhonenoForm();
        $corporateFormPhone = new CorporatePhonenoForm();
        $form_education = new ContactEducationForm();
        $form_upload = new ContactUploadForm();
        $form_cor_upload = new CorporateUploadForm();
        $form_address = new ContactAddressesForm();
        $corporate_form_address = new CorporateContactAddressesForm();
        $form_website = new ContactWebsiteForm();
        $form_individual->add($form_phone);
        $form_cororporate_foundation->add($form_cor_upload);
        $phoneTypeCorporate = array('' => 'Select', 'main' => 'Main', 'direct' => 'Direct', 'fax' => 'Fax', 'others' => 'Others');
        $phoneTypeIndividual = array('' => 'Select', 'home' => 'Home', 'cell' => 'Mobile', 'work' => 'Work', 'fax' => 'Fax', 'others' => 'Others');
        $form_cororporate_foundation->add($corporateFormPhone);
        $form_individual->add($form_education);
        $form_cororporate_foundation->add($form_website);
        $form_individual->add($form_upload);
        $form_cororporate_foundation->add($form_upload);
        $form_individual->add($form_address);
        $form_cororporate_foundation->add($form_address);
        $form_cororporate_foundation->add($corporate_form_address);
        $request = $this->getRequest();
        $genratedPasswordCRMContact = "";
        $verifyCodeCRMContact = "";
        $viewModel = new ViewModel();
        $contact = new Contact($this->_adapter);
        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
        $country_list = array();
        $country_list[''] = 'Select Country';
        foreach ($country as $key => $val) {
            $country_list[$val['country_id']] = $val['name'];
        }
        $form_address->get('addressinfo_country[]')->setAttribute('options', $country_list);
        $corporate_form_address->get('corporate_addressinfo_country[]')->setAttribute('options', $country_list);

        $state = $this->getServiceLocator()->get('Common\Model\CommonTable')->getStates();
        $stateList = array();
        $stateList[''] = 'Select State';
        foreach ($state as $key => $val) {
            $stateList[$val['state_code']] = $val['state'] . " - " . $val['state_code'];
        }

        $form_address->get('address_state_id[]')->setAttribute('options', $stateList);

        $apoPoStates = $this->_config['transaction_config']['apo_po_state'];
        $apoPoStatesList[''] = 'Select State';

        foreach ($apoPoStates as $k => $v) {
            $apoPoStatesList[$k] = $v;
        }
        $form_address->get('address_state_apo[]')->setAttribute('options', $apoPoStatesList);

        $form_address->get('address_country_id[]')->setAttribute('options', array(228 => 'UNITED STATES'));

        $corporate_form_address->get('corporate_address_state_id[]')->setAttribute('options', $stateList);
        $corporate_form_address->get('corporate_address_state_apo[]')->setAttribute('options', $apoPoStatesList);
        $corporate_form_address->get('corporate_address_country_id[]')->setAttribute('options', array(228 => 'UNITED STATES'));

        $get_user_type = $this->getServiceLocator()->get('Common\Model\CommonTable')->getType();
        $user_type = array();
        foreach ($get_user_type as $key => $val) {
            $user_type[$val['user_type_id']] = $val['user_type'];
        }
        $form_individual->get('contact_type')->setAttribute('options', $user_type);
        $form_cororporate_foundation->get('corporate_contact_type')->setAttribute('options', $user_type, 'selected', '2');

        $getNationality = $this->getServiceLocator()->get('Common\Model\CommonTable')->getNationality('');
        $nationality = array();
        $nationalityDesc = array();
        $nationality[''] = 'Select';
        foreach ($getNationality as $key => $val) {
            $nationality[$val['nationality_id']] = $val['nationality'];
            $nationalityDesc[$val['nationality_id']] = $val['description'];
        }
        $form_individual->get('demographics_nationality')->setAttribute('options', $nationality);

        $getEthinicity = $this->getServiceLocator()->get('Common\Model\CommonTable')->getEthinicity('');
        $ethnicity = array();
        $ethnicityDesc = array();
        $ethnicity[''] = 'Select';
        foreach ($getEthinicity as $key => $val) {
            $ethnicity[$val['ethnicity_id']] = $val['ethnicity'];
            $ethnicityDesc[$val['ethnicity_id']] = $val['description'];
        }
        $form_individual->get('demographics_ethnicity')->setAttribute('options', $ethnicity);

        $get_title = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTitle();
        $user_title = array();
        $user_title[''] = 'None';
        foreach ($get_title as $key => $val) {
            $user_title[$val['title']] = $val['title'];
        }
        $form_individual->get('title')->setAttribute('options', $user_title);
        $form_address->get('addressinfo_title[]')->setAttribute('options', $user_title);        
        $corporate_form_address->get('corporate_addressinfo_title[]')->setAttribute('options', $user_title); 
     //   $form_cororporate_foundation->get('addressinfo_title[]')->setAttribute('options', $user_title);        
        //$form_individual->get('country_id')->setAttribute('options', $country_list);
        //$form_cororporate_foundation->get('corporate_country_id')->setAttribute('options', $country_list);



        $get_suffix = $this->getServiceLocator()->get('Common\Model\CommonTable')->getSuffix();
        $user_suffix = array();
        $user_suffix[''] = 'None';
        foreach ($get_suffix as $key => $val) {
            $user_suffix[$val['suffix']] = $val['suffix'];
        }
        $form_individual->get('suffix')->setAttribute('options', $user_suffix);

        $get_source = $this->getServiceLocator()->get('Common\Model\CommonTable')->getSource('');
        $source_list = array();
        $source_list[''] = 'Select Source';
        foreach ($get_source as $key => $val) {
            $source_list[$val['source_id']] = $val['source_name'];
        }
        $form_individual->get('source')->setAttribute('options', $source_list);
        $form_individual->get('source')->setValue(1);
        $form_cororporate_foundation->get('corporate_source')->setAttribute('options', $source_list);
        $form_cororporate_foundation->get('corporate_source')->setValue(1);

        $get_tag = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTags();
        $user_tag = array();
        foreach ($get_tag as $key => $val) {
            $user_tag[$val['tag_id']] = $val['tag_name'];
        }

        $get_location_type = $this->getServiceLocator()->get('Common\Model\CommonTable')->getLocationType('');
        $location_type = array();
        $location_typeDesc = array();
        $location_type[''] = 'Select';
        foreach ($get_location_type as $key => $val) {
            $location_type[$val['location_id']] = $val['location'];
            $location_typeDesc[$val['location_id']] = $val['description'];
        }
        $form_address->get('addressinfo_location_type[]')->setAttribute('options', $location_type);

        $corporate_form_address->get('corporate_addressinfo_location_type[]')->setAttribute('options', $location_type);

        $get_user_groups = $this->getServiceLocator()->get('Common\Model\CommonTable')->getGroups();
        $user_group = array();
        foreach ($get_user_groups as $key => $val) {
            if ($val['group_type'] == '1') {
                $user_group[$val['group_id']] = $val['name'];
            }
        }
        $form_individual->get('tagsandgroups_groups_from')->setAttribute('options', $user_group);
        $form_cororporate_foundation->get('cor_tagsandgroups_groups_from')->setAttribute('options', $user_group);


        $get_income_range = $this->getServiceLocator()->get('Common\Model\CommonTable')->getIncomeRange();
        $income_range = array();
        $income_range[''] = 'Select';
        foreach ($get_income_range as $key => $val) {
            $income_range[$val['income_range_id']] = $val['income_range'];
        }
        $form_individual->get('demographics_income_range')->setAttribute('options', $income_range);

        $get_education_list = $this->getServiceLocator()->get('Common\Model\CommonTable')->getEducationDegreeList('');
        $education_list = array();
        $education_desc = array();
        $education_list[''] = 'Select';
        foreach ($get_education_list as $key => $val) {
            $education_list[$val['education_id']] = $val['education'];
            $education_desc[$val['education_id']] = $val['description'];
        }
        $form_education->get('edu_degree_level[]')->setAttribute('options', $education_list);
        $get_address_greeting = $this->getServiceLocator()->get('Common\Model\CommonTable')->getAddressGreeting(array('contact_type' => 1));
        $get_email_greeting = $this->getServiceLocator()->get('Common\Model\CommonTable')->getEmailGreeting(array('contact_type' => 1));
        $get_postal_greeting = $this->getServiceLocator()->get('Common\Model\CommonTable')->getPostalGreeting(array('contact_type' => 1));
        $address_greeting = array();
        $address_greeting[''] = 'Select';
        foreach ($get_address_greeting as $key => $val) {
            $address_greeting[$val['addressee_greeting_id']] = $val['heading'];
        }

        $postal_greeting = array();
        $postal_greeting[''] = 'Select';
        foreach ($get_postal_greeting as $key => $val) {
            $postal_greeting[$val['postal_greeting_id']] = $val['heading'];
        }

        $email_greeting = array();
        $email_greeting[''] = 'Select';
        foreach ($get_email_greeting as $key => $val) {
            $email_greeting[$val['email_greeting_id']] = $val['heading'];
        }

        $form_individual->get('communicationpref_email_greetings')->setAttribute('options', $email_greeting);
        $form_individual->get('communicationpref_postal_greetings')->setAttribute('options', $postal_greeting);
        $form_individual->get('communicationpref_address')->setAttribute('options', $address_greeting);


        $corpotateAddressGreeting = $this->getServiceLocator()->get('Common\Model\CommonTable')->getAddressGreeting(array('contact_type' => 2));
        $corporateEmailGreeting = $this->getServiceLocator()->get('Common\Model\CommonTable')->getEmailGreeting(array('contact_type' => 2));
        $corporatePostalGreeting = $this->getServiceLocator()->get('Common\Model\CommonTable')->getPostalGreeting(array('contact_type' => 2));
        $corAddressGreeting[''] = 'Select';
        foreach ($corpotateAddressGreeting as $key => $val) {
            $corAddressGreeting[$val['addressee_greeting_id']] = $val['heading'];
        }

        $corPostalGreeting = array();
        $corPostalGreeting[''] = 'Select';
        foreach ($corporatePostalGreeting as $key => $val) {
            $corPostalGreeting[$val['postal_greeting_id']] = $val['heading'];
        }

        $corEmailGreeting = array();
        $corEmailGreeting[''] = 'Select';
        foreach ($corporateEmailGreeting as $key => $val) {
            $corEmailGreeting[$val['email_greeting_id']] = $val['heading'];
        }

        $form_cororporate_foundation->get('corporate_communicationpref_email_greetings')->setAttribute('options', $corEmailGreeting);
        $form_cororporate_foundation->get('corporate_communicationpref_postal_greetings')->setAttribute('options', $corPostalGreeting);
        $form_cororporate_foundation->get('corporate_communicationpref_address')->setAttribute('options', $corAddressGreeting);

        $sec_question = $this->getServiceLocator()->get('Common\Model\QuestionTable')->getSecurityQuestion();
        $sec_question_list = array();
        $sec_question_list[''] = 'Select Security Question';
        foreach ($sec_question as $key => $val) {
            $sec_question_list[$val['security_question_id']] = $val['security_question'];
        }
        $form_individual->get('webaccesss_security_question')->setAttribute('options', $sec_question_list);
        $form_individual->get('webaccesss_enabled_login')->setValue('1');
        $form_cororporate_foundation->get('corporate_webaccesss_enabled_login')->setValue('1');
        $form_cororporate_foundation->get('corporate_webaccesss_security_question')->setAttribute('options', $sec_question_list);
        $response = $this->getResponse();
        $messages = array();
        /** add private note* */
        $moduleInfoArr = array();
        $moduleInfoArr[] = 'User';
        $moduleInfoArr[] = 'Contact';
        $moduleInfoArr[] = 'create-contact-private-note';
        $moduleDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getModuleAction($moduleInfoArr);

        $auth = new AuthenticationService();

        $userDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getCrmUserDetails(array('crm_user_id' => $auth->getIdentity()->crm_user_id));

        $roleDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->checkCrmUserPermission($userDetail[0]['role_id']);

        $addPrivateNotePermission = isset($roleDetail['moduleActionPermissions'][$moduleDetail['module_id']][$moduleDetail['action_id']]) ? $roleDetail['moduleActionPermissions'][$moduleDetail['module_id']][$moduleDetail['action_id']] : 1;

        $get_contact_arr = array();

        /** end of add private note * */
        if ($request->isPost()) {


//            if ($request->getPost("corporate_submit") == 'Save') {
//                $form_cororporate_foundation->setInputFilter($contact->getInputFilterCreateContactCorporate());
//                $form_cororporate_foundation->setData($request->getPost());
//
//                if (!$form_cororporate_foundation->isValid()) {
//                    $errors = $form_cororporate_foundation->getMessages();
//
//                    $msg = array();
//
//                    foreach ($errors as $key => $row) {
//                        if (!empty($row) && $key != 'submit') {
//                            foreach ($row as $rower) {
//                                $msg [$key] = $this->_config['user_messages']['config']['create_contact'][$rower];
//                            }
//                        }
//                    }
//                    $messages = array('status' => "error", 'message' => $msg);
//                }
//            } else {
//                $form_individual->setInputFilter($contact->getInputFilterCreateContactIndividual());
//                $form_individual->setData($request->getPost());
//
//                if (!$form_individual->isValid()) {
//                    $errors = $form_individual->getMessages();
//
//                    $msg = array();
//
//                    foreach ($errors as $key => $row) {
//                        if (!empty($row) && $key != 'submit') {
//                            foreach ($row as $rower) {
//                                $msg [$key] = $this->_config['user_messages']['config']['create_contact'][$rower];
//                            }
//                        }
//                    }
//                    $messages = array('status' => "error", 'message' => $msg);
//                }
//            }


            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $form_data = $request->getPost();

                $date = DATE_TIME_FORMAT;
                $form_data['add_date'] = $date;
                $form_data['modified_date'] = $date;
                if (isset($form_data['corporate_email_id']) && $form_data['corporate_email_id'] != '') {
                    $userDataArr = $this->getUserTable()->getUser(array('email_id' => $form_data['corporate_email_id']));
                    if (!empty($userDataArr)) {
                        $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['create_contact']['EMAIL_EXITS'], 'email_exits' => true);
                        $response->setContent(\Zend\Json\Json::encode($messages));
                        return $response;
                    }
                } else if (isset($form_data['email_id']) && $form_data['email_id'] != '') {
                    $userDataArr = $this->getUserTable()->getUser(array('email_id' => $form_data['email_id']));
                    if (!empty($userDataArr)) {
                        $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['create_contact']['EMAIL_EXITS'], 'email_exits' => true);
                        $response->setContent(\Zend\Json\Json::encode($messages));
                        return $response;
                    }
                }
                if (isset($form_data['contact_type'])) {
                    $filterParam = $contact->filterParam($form_data);
                    if ($filterParam['no_email_address']) {
                        $filterParam['username'] = $filterParam['first_name'] . "" . $filterParam['last_name'] . $this->GetRandomAlphaNumeric(5);
                    } else {
                        $filterParam['username'] = null;
                    }
                } else {
                    $filterParam = $contact->filterParamCorporate($form_data);
                    if ($filterParam['no_email_address']) {
                        $filterParam['username'] = trim($filterParam['corporate_company_name']) . $this->GetRandomAlphaNumeric(5);
                    } else {
                        $filterParam['username'] = null;
                    }
                }

                //$filterParam = $contact->filterParam($form_data);
                $genratedPasswordCRMContact = (isset($filterParam['genrated_password']) and trim($filterParam['genrated_password']) != "") ? trim($filterParam['genrated_password']) : "";
                $verifyCodeCRMContact = (isset($filterParam['verify_code']) and trim($filterParam['verify_code']) != "") ? trim($filterParam['verify_code']) : "";
                
                if(isset($form_data['corporate_contact_type']) && ($form_data['corporate_contact_type'] == 2 || $form_data['corporate_contact_type'] == 3))
                    $filterParam['email_id'] = $form_data['corporate_email_id'];
                
                
                
                $user_id = $this->getContactTable()->insertContact($filterParam);
                if ($user_id) {
                    $this->getServiceLocator()->get('User\Model\ContactTable')->updateContactId(array('user_id' => $user_id));

                    $membershipDetailArr = array();
                    $membershipDetailArr[] = 1;
                    $getMemberShip = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);

                    $userMembershipData = array();
                    $userMembershipData['user_id'] = $user_id;
                    $userMembershipData['membership_id'] = $getMemberShip['membership_id'];
					$userMembershipInfo = $this->getServiceLocator()->get('UserMembership')->getUserMembership($getMemberShip);
					$userMembershipData['membership_date_from'] = $userMembershipInfo['membership_date_from'];
                    $userMembershipData['membership_date_to'] = $userMembershipInfo['membership_date_to'];
                    /*$startDate = date("Y-m-d");
                    if ($getMemberShip['validity_type'] == 1) {
                        $userMembershipData['membership_date_from'] = $startDate;
                        $startDay = $this->_config['financial_year']['srart_day'];
                        $startMonth = $this->_config['financial_year']['srart_month'];
                        $startYear = date("Y");
                        $startDate = $startYear . "-" . $startMonth . "-" . $startDay;
                        $addYear = $getMemberShip['validity_time'] - date("Y");
                        $futureDate = date('Y-m-d', strtotime('+' . $addYear . ' year', strtotime($startDate)));
                        $userMembershipData['membership_date_to'] = $futureDate;
                    } else if ($getMemberShip['validity_type'] == 2) {
                        $userMembershipData['membership_date_from'] = $startDate;
                        $futureDate = date('Y-m-d', strtotime('+' . $getMemberShip['validity_time'] . ' year', strtotime($startDate)));
                        $userMembershipData['membership_date_to'] = $futureDate;
                    }*/
                    $this->getServiceLocator()->get('User\Model\UserTable')->saveUserMembership($userMembershipData);

                    /* if ($filterParam['email_id']) {
                      $userDataArr = $this->getUserTable()->getUser(array('user_id' => $user_id));
                      $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                      $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                      $userDataArr['genrated_password'] = $filterParam['genrated_password'];
                      $userDataArr['verification_code'] = $filterParam['verify_code'];
                      $signup_email_id_template_int = 29;
                      $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $signup_email_id_template_int, $this->_config['email_options']);
                      } */
                    $form_data['user_id'] = $user_id;
                    $form_data['added_by'] = $this->auth->getIdentity()->crm_user_id;
                    if (isset($form_data['contact_type'])) {
                        $filterParam = $contact->filterParam($form_data);
                        $this->getContactTable()->insertAlternateContacts($filterParam);
                        $this->getContactTable()->insertAfihc($filterParam);

                        if (!empty($form_data['edu_degree_level'])) {
                            $edu_form = array();
                            for ($i = 0; $i < count($form_data['edu_degree_level']); $i++) {
                                $edu_form[$i]['edu_degree_level'] = isset($form_data['edu_degree_level'][$i]) ? $form_data['edu_degree_level'][$i] : null;
                                $edu_form[$i]['edu_institution'] = isset($form_data['edu_institution'][$i]) ? $form_data['edu_institution'][$i] : null;
                                $edu_form[$i]['edu_attended_year'] = isset($form_data['edu_attended_year'][$i]) ? $form_data['edu_attended_year'][$i] : null;
                                $edu_form[$i]['edu_major'] = isset($form_data['edu_major'][$i]) ? $form_data['edu_major'][$i] : null;
                                $edu_form[$i]['edu_gpa'] = isset($form_data['edu_gpa'][$i]) ? $form_data['edu_gpa'][$i] : null;
                            }
                            foreach ($edu_form as $val) {
                                $val['user_id'] = $user_id;
                                $val['add_date'] = $date;
                                $val['modified_date'] = $date;
                                $this->getContactTable()->insertEducation($val);
                            }
                        }
                        if (!empty($form_data['tagsandgroups_groups_to'])) {
                            foreach ($form_data['tagsandgroups_groups_to'] as $val) {
                                $value['user_id'] = $user_id;
                                $value['add_date'] = $date;
                                $value['modified_date'] = $date;
                                $value['groups'] = $val;
                                $this->getContactTable()->insertGroups($value);
                            }
                        }
                        $address_form = array();
                        //if(!empty($form_data['primary_location_count'])){
                        for ($i = 0; $i < count($form_data['addressinfo_country']); $i++) {
                            $address_form[$i]['addressinfo_location_type'] = isset($form_data['addressinfo_location_type'][$i]) ? $form_data['addressinfo_location_type'][$i] : null;
                            
                            $address_form[$i]['title'] = isset($form_data['addressinfo_title'][$i]) ? rtrim($form_data['addressinfo_title'][$i], ',') : '';
                            $address_form[$i]['first_name'] = isset($form_data['addressinfo_first_name'][$i]) ? rtrim($form_data['addressinfo_first_name'][$i], ',') : '';
                            $address_form[$i]['last_name'] = isset($form_data['addressinfo_last_name'][$i]) ? rtrim($form_data['addressinfo_last_name'][$i], ',') : '';
                            
                            $address_form[$i]['addressinfo_street_address_1'] = isset($form_data['addressinfo_street_address_1'][$i]) ? $form_data['addressinfo_street_address_1'][$i] : null;
                            $address_form[$i]['addressinfo_city'] = isset($form_data['addressinfo_city'][$i]) ? $form_data['addressinfo_city'][$i] : null;
                            $address_form[$i]['addressinfo_zip'] = isset($form_data['addressinfo_zip'][$i]) ? $form_data['addressinfo_zip'][$i] : null;
                            $address_form[$i]['addressinfo_street_address_2'] = isset($form_data['addressinfo_street_address_2'][$i]) ? $form_data['addressinfo_street_address_2'][$i] : null;

                            $address_form[$i]['addressinfo_state'] = isset($form_data['addressinfo_state'][$i]) ? $form_data['addressinfo_state'][$i] : null;

                            if ((isset($form_data['addressinfo_location_shipping'][$i]) && $form_data['addressinfo_location_shipping'][$i] != '') && (isset($form_data['is_apo_po'][$i]) && $form_data['is_apo_po'][$i] != 1)) {
                                $address_form[$i]['addressinfo_country'] = isset($form_data['address_country_id'][$i]) ? $form_data['address_country_id'][$i] : null;
                            } else {
                                $address_form[$i]['addressinfo_country'] = isset($form_data['addressinfo_country'][$i]) ? $form_data['addressinfo_country'][$i] : null;
                            }

                            if (isset($form_data['addressinfo_location_shipping'][$i]) && (isset($form_data['is_apo_po'][$i]) && $form_data['is_apo_po'][$i] != ''))
                                $address_form[$i]['address_type'] = $form_data['is_apo_po'][$i];
                        }
                        //}
                        $k = 0;
                        for ($count = 0; $count < $form_data['primary_location_count']; $count++) {
                            $key = 'primary_location' . $count;
                            
                            if (isset($form_data[$key])) {
                                $addressType = $form_data[$key];
                                if ($count == $form_data['address_primary_index']) {
                                    $address_form[$k]['addressinfo_location'] = "1," . $addressType;
                                } else {
                                    $address_form[$k]['addressinfo_location'] = $addressType;
                                }
                                if ($address_form[$k]['addressinfo_location'] != '') {
                                    $address_form[$k]['addressinfo_location'] = rtrim($address_form[$k]['addressinfo_location'], ",");
                                }
                                $k++;
                            }
//                            else {
//                                if ($count == $form_data['address_primary_index']) {
//                                    $address_form[$count]['addressinfo_location'] = "1";
//                                } else {
//                                    $address_form[$count]['addressinfo_location'] = null;
//                                }
//                            }
                        }
                        $phon_form = array();
                        for ($i = 0; $i < count($form_data['country_code']); $i++) {
                            if ($form_data['country_code'][$i] != '' || $form_data['area_code'][$i] != '' || $form_data['phone_no'][$i] != '' || $form_data['phone_type'][$i] != '') {
                                $phon_form[$i]['country_code'] = isset($form_data['country_code'][$i]) ? $form_data['country_code'][$i] : null;
                                $phon_form[$i]['area_code'] = isset($form_data['area_code'][$i]) ? $form_data['area_code'][$i] : null;
                                $phon_form[$i]['phone_no'] = isset($form_data['phone_no'][$i]) ? $form_data['phone_no'][$i] : null;
                                $phon_form[$i]['extension'] = isset($form_data['extension'][$i]) ? $form_data['extension'][$i] : null;
                                $phon_form[$i]['phone_type'] = isset($form_data['phone_type'][$i]) ? $form_data['phone_type'][$i] : null;

                                $primaryPhone = $form_data['primary-phone'];
                                if ($i == $primaryPhone) {
                                    $phon_form[$i]['continfo_primary'] = 1;
                                } else {
                                    $phon_form[$i]['continfo_primary'] = null;
                                }
                            }
                        }
                    } else {
                        $filterParam = $contact->filterParamCorporate($form_data);
                        if (!empty($form_data['cor_tagsandgroups_groups_to'])) {
                            foreach ($form_data['cor_tagsandgroups_groups_to'] as $val) {
                                $value['user_id'] = $user_id;
                                $value['add_date'] = $date;
                                $value['modified_date'] = $date;
                                $value['groups'] = $val;
                                $this->getContactTable()->insertGroups($value);
                            }
                        }
                        if ($form_data['don_not_have_website'] == 0) {
                            if (!empty($form_data['website']) && $form_data['website'][0] != '') {
                                foreach ($form_data['website'] as $val) {
                                    $website['user_id'] = $user_id;
                                    $website['add_date'] = $date;
                                    $website['modified_date'] = $date;
                                    $website['website'] = $val;
                                    $this->getContactTable()->insertWebsite($website);
                                }
                            }
                        }

                        $address_form = array();
                        
                        for ($i = 0; $i < count($form_data['corporate_addressinfo_country']); $i++) {
                            $address_form[$i]['addressinfo_location_type'] = isset($form_data['corporate_addressinfo_location_type'][$i]) ? $form_data['corporate_addressinfo_location_type'][$i] : null;
                            
                            $address_form[$i]['title'] = isset($form_data['corporate_addressinfo_title'][$i]) ? rtrim($form_data['corporate_addressinfo_title'][$i], ',') : '';
                            $address_form[$i]['first_name'] = isset($form_data['corporate_addressinfo_first_name'][$i]) ? rtrim($form_data['corporate_addressinfo_first_name'][$i], ',') : '';
                            $address_form[$i]['last_name'] = isset($form_data['corporate_addressinfo_last_name'][$i]) ? rtrim($form_data['corporate_addressinfo_last_name'][$i], ',') : '';                          
                            
                            $address_form[$i]['addressinfo_street_address_1'] = isset($form_data['corporate_addressinfo_street_address_1'][$i]) ? $form_data['corporate_addressinfo_street_address_1'][$i] : null;
                            $address_form[$i]['addressinfo_city'] = isset($form_data['corporate_addressinfo_city'][$i]) ? $form_data['corporate_addressinfo_city'][$i] : null;
                            $address_form[$i]['addressinfo_zip'] = isset($form_data['corporate_addressinfo_zip'][$i]) ? $form_data['corporate_addressinfo_zip'][$i] : null;
                            $address_form[$i]['addressinfo_street_address_2'] = isset($form_data['corporate_addressinfo_street_address_2'][$i]) ? $form_data['corporate_addressinfo_street_address_2'][$i] : null;

                            $address_form[$i]['addressinfo_state'] = isset($form_data['corporate_addressinfo_state'][$i]) ? $form_data['corporate_addressinfo_state'][$i] : null;
                            if ((isset($form_data['corporate_addressinfo_location_shipping'][$i]) && $form_data['corporate_addressinfo_location_shipping'][$i] != '') && (isset($form_data['corporate_is_apo_po'][$i]) && $form_data['corporate_is_apo_po'][$i] != 1)) {
                                $address_form[$i]['addressinfo_country'] = isset($form_data['corporate_address_country_id'][$i]) ? $form_data['corporate_address_country_id'][$i] : null;
                            } else {
                                $address_form[$i]['addressinfo_country'] = isset($form_data['corporate_addressinfo_country'][$i]) ? $form_data['corporate_addressinfo_country'][$i] : null;
                            }
                            //$address_form[$i]['addressinfo_country'] = isset($form_data['corporate_addressinfo_country'][$i]) ? $form_data['corporate_addressinfo_country'][$i] : null;

                            if ((isset($form_data['corporate_addressinfo_location_shipping'][$i]) && $form_data['corporate_addressinfo_location_shipping'][$i] != '') && isset($form_data['corporate_is_apo_po'][$i]))
                                $address_form[$i]['address_type'] = $form_data['corporate_is_apo_po'][$i];
                        }
                        $j = 0;

                        for ($count = 0; $count < $form_data['corporate_primary_location_count']; $count++) {
                            $key = 'corporate_primary_location' . $count;
                            
                            if (isset($form_data[$key])) {
                                $addressTypeCor = $form_data[$key];
                                if ($count == $form_data['corporate_address_primary_index']) {
                                    $address_form[$j]['addressinfo_location'] = "1," . $addressTypeCor;
                                } else {
                                    $address_form[$j]['addressinfo_location'] = $addressTypeCor;
                                }
                                if ($address_form[$j]['addressinfo_location'] != '') {
                                    $address_form[$j]['addressinfo_location'] = rtrim($address_form[$j]['addressinfo_location'], ",");
                                }
                                $j++;
                            }
//                            else {
//                                if ($count == $form_data['corporate_address_primary_index']) {
//                                    $address_form[$count]['addressinfo_location'] = "1";
//                                } else {
//                                    $address_form[$count]['addressinfo_location'] = null;
//                                }
//                            }
                        }
                        $phon_form = array();
                        for ($i = 0; $i < count($form_data['corporate_country_code']); $i++) {
                            if ($form_data['corporate_country_code'][$i] != '' || $form_data['corporate_area_code'][$i] != '' || $form_data['corporate_phone_no'][$i] != '' || $form_data['corporate_phone_type'][$i] != '') {
                                $phon_form[$i]['country_code'] = isset($form_data['corporate_country_code'][$i]) ? $form_data['corporate_country_code'][$i] : null;
                                $phon_form[$i]['area_code'] = isset($form_data['corporate_area_code'][$i]) ? $form_data['corporate_area_code'][$i] : null;
                                $phon_form[$i]['phone_no'] = isset($form_data['corporate_phone_no'][$i]) ? $form_data['corporate_phone_no'][$i] : null;
                                $phon_form[$i]['extension'] = isset($form_data['corporate_extension'][$i]) ? $form_data['corporate_extension'][$i] : null;
                                $phon_form[$i]['phone_type'] = isset($form_data['corporate_phone_type'][$i]) ? $form_data['corporate_phone_type'][$i] : null;
                                $primaryPhone = $form_data['corporate-primary-phone'];
                                if ($i == $primaryPhone) {
                                    $phon_form[$i]['continfo_primary'] = 1;
                                } else {
                                    $phon_form[$i]['continfo_primary'] = null;
                                }
                            }
                        }
                    }
                  //  asd($address_form);
                    
                    if (!empty($address_form)) {
                        $isPrimaryAddress = true;
                        foreach ($address_form as $key => $addVal) {
                            if ($addVal['addressinfo_location'] != '') {
                                $explodedAddress = explode(",", $addVal['addressinfo_location']);
                                if (in_array(1, $explodedAddress)) {
                                    $isPrimaryAddress = false;
                                }
                            }
                        }
                        foreach ($address_form as $key => $addVal) {
                            $addVal['user_id'] = $user_id;
                            $addVal['add_date'] = $date;
                            $addVal['modified_date'] = $date;
                            if ($isPrimaryAddress && $key == 0) {
                                $addVal['addressinfo_location'] = 1;
                            }
                            if (isset($addVal['addressinfo_country']) && $addVal['addressinfo_country']) {
                                $this->getContactTable()->insertAddressInfo($addVal);
                            }
                        }
                    }

                    if ($filterParam['notes'] != '') {
                        $this->getContactTable()->insertNotes($filterParam);
                    }

                    if (!empty($phon_form)) {
                        foreach ($phon_form as $val) {
                            $val['user_id'] = $user_id;
                            $val['add_date'] = $date;
                            $val['modified_date'] = $date;
                            $this->getContactTable()->insertPhoneInformations($val);
                        }
                    }
                    $this->getContactTable()->insertCommunicationPreferences($filterParam);

                    if ($filterParam['tag_id'] != '') {
                        $postedTags = explode(",", $filterParam['tag_id']);
                        $mstTags = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTags();
                        $tags = array();
                        if (!empty($mstTags)) {
                            foreach ($mstTags as $val) {
                                $key = $val['tag_id'];
                                $tags[$key] = $val['tag_name'];
                            }
                        }
                        foreach ($postedTags as $val) {
                            if ($val) {
                                if (in_array($val, $tags)) {
                                    $tagId = array_search($val, $tags);
                                    $filterParam['tag_id'] = $tagId;
                                    $filterParam['user_id'] = $user_id;
                                    $this->getContactTable()->insertUserTags($filterParam);
                                } else {
                                    $filterParam['tagsandgroups_company_name'] = $val;
                                    $tagId = $this->getContactTable()->insertMasterTags($filterParam);
                                    $filterParam['tag_id'] = $tagId;
                                    $filterParam['user_id'] = $user_id;
                                    $this->getContactTable()->insertUserTags($filterParam);
                                }
                            }
                        }
                    }


                    if (isset($filterParam['publicdocs_description']) && $filterParam['publicdocs_description'] != '') {
                        $allFilesNames = '';
                        for ($count = 0; $count <= $filterParam['publicdocs_upload_count']; $count++) {
                            $key = 'publicdocs_upload_file' . $count;
                            $filesName = $form_data[$key];
                            if ($filesName != '') {
                                $allFilesNames .= $filesName . ",";
                                $this->movePublicDocFileFromTempToContact($filesName, $user_id);
                            }
                        }
                        if ($allFilesNames != '') {
                            $allFilesNames = rtrim($allFilesNames, ",");
                            $filterParam['user_id'] = $user_id;
                            $filterParam['publicdocs_upload'] = $allFilesNames;
                            $this->getContactTable()->insertDocument($filterParam);
                        } else {
                            if (!empty($user_document) && $user_document['uploaded_files'] != '') {
                                $allFilesNames = $allFilesNames . "" . $user_document['uploaded_files'];
                                $filterParam['publicdocs_upload'] = $allFilesNames;
                            } else {
                                $filterParam['publicdocs_upload'] = '';
                            }
                            $filterParam['user_id'] = $user_id;
                            $this->getContactTable()->insertDocument($filterParam);
                        }
                    } else if (isset($filterParam['corporate_publicdocs_description']) && $filterParam['corporate_publicdocs_description'] != '') {
                        $allFilesNames = '';
                        for ($count = 0; $count <= $filterParam['cor_publicdocs_upload_count']; $count++) {
                            $key = 'cor_publicdocs_upload_file' . $count;
                            $filesName = $form_data[$key];
                            if ($filesName != '') {
                                $allFilesNames .= $filesName . ",";
                                $this->movePublicDocFileFromTempToContact($filesName, $user_id);
                            }
                        }
                        if ($allFilesNames != '') {
                            $allFilesNames = rtrim($allFilesNames, ",");
                            $filterParam['user_id'] = $user_id;
                            $filterParam['publicdocs_upload'] = $allFilesNames;
                            $this->getContactTable()->insertDocument($filterParam);
                        } else {
                            if (!empty($user_document) && $user_document['uploaded_files'] != '') {
                                $allFilesNames = $allFilesNames . "" . $user_document['uploaded_files'];
                                $filterParam['publicdocs_upload'] = $allFilesNames;
                            } else {
                                $filterParam['publicdocs_upload'] = '';
                            }
                            $filterParam['user_id'] = $user_id;
                            $this->getContactTable()->insertDocument($filterParam);
                        }
                    } else {
                        if (isset($form_data['contact_type'])) {
                            $allFilesNames = '';
                            for ($count = 0; $count <= $filterParam['publicdocs_upload_count']; $count++) {
                                $key = 'publicdocs_upload_file' . $count;
                                $filesName = $form_data[$key];
                                if ($filesName != '') {
                                    $allFilesNames .= $filesName . ",";
                                    $this->movePublicDocFileFromTempToContact($filesName, $user_id);
                                }
                            }
                            if ($allFilesNames != '') {
                                $allFilesNames = rtrim($allFilesNames, ",");
                                $filterParam['user_id'] = $user_id;
                                $filterParam['publicdocs_upload'] = $allFilesNames;
                                $this->getContactTable()->insertDocument($filterParam);
                            }
                        } else {
                            $allFilesNames = '';
                            for ($count = 0; $count <= $filterParam['cor_publicdocs_upload_count']; $count++) {
                                $key = 'cor_publicdocs_upload_file' . $count;
                                $filesName = $form_data[$key];
                                if ($filesName != '') {
                                    $allFilesNames .= $filesName . ",";
                                    $this->movePublicDocFileFromTempToContact($filesName, $user_id);
                                }
                            }
                            if ($allFilesNames != '') {
                                $allFilesNames = rtrim($allFilesNames, ",");
                                $filterParam['user_id'] = $user_id;
                                $filterParam['publicdocs_upload'] = $allFilesNames;
                                $this->getContactTable()->insertDocument($filterParam);
                            }
                        }
                    }

                    /** insert into the change log */
                    $changeLogArray = array();
                    $changeLogArray['table_name'] = 'tbl_usr_change_logs';
                    $changeLogArray['activity'] = '1';/** 1 == for insert */
                    $changeLogArray['id'] = $user_id;
                    $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                    $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                    $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                    if ($filterParam['upload_profile_image_id'] != '') {
                        $this->moveFileFromTempToContact($filterParam['upload_profile_image_id'], $user_id);
                    }
                    /** end insert into the change log */
                    if ($filterParam['email_id']) {
                        $userDataArr = $this->getUserTable()->getUser(array('user_id' => $user_id));
                        $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                        $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                        $userDataArr['genrated_password'] = $genratedPasswordCRMContact; //$filterParam['genrated_password'];
                        $userDataArr['verification_code'] = $verifyCodeCRMContact; //$filterParam['verify_code'];
                        $signup_email_id_template_int = 29;
                        $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $signup_email_id_template_int, $this->_config['email_options']);
                    }
                    $this->flashMessenger()->addMessage($this->_config['user_messages']['config']['create_contact']['CREATE_CONTACT_SUCCESS']);
                    $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['create_contact']['CREATE_CONTACT_SUCCESS']);
                } else {
                    $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['create_contact']['CREATE_CONTACT_FAIL']);
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }

        $viewModel->setVariables(array(
            'form' => $form_individual,
            'form_cororporate_foundation' => $form_cororporate_foundation,
            'jsLangTranslate' => $this->_config['user_messages']['config']['create_contact'],
            'user_list' => $get_contact_arr,
            'phoneTypeCorporate' => $phoneTypeCorporate,
            'phoneTypeIndividual' => $phoneTypeIndividual,
            'user_tag' => $user_tag,
            'addPrivateNotePermission' => $addPrivateNotePermission,
            'education_desc' => $education_desc,
            'ethnicityDesc' => $ethnicityDesc,
            'nationalityDesc' => $nationalityDesc,
            'location_typeDesc' => $location_typeDesc,
            'country_list' => $country_list,
            'apoPoStatesList' => $apoPoStatesList,
                )
        );
        return $viewModel;
    }

    /**
     * upload user image
     * @return JSON
     * @author Icreon Tech - DG
     */
    public function uploadUserThumbnailAction() {
        $this->checkUserAuthentication();
        $this->getConfig();

        if (isset($_FILES['corporate_upload_profile_image']['name'][0]) && $_FILES['corporate_upload_profile_image']['name'][0] != '') {
            $image_name_param = 'corporate_upload_profile_image';
        } else {
            $image_name_param = 'upload_profile_image';
        }

        $fileExt = $this->GetFileExt($_FILES[$image_name_param]['name']);
        $filename = $this->GenerateMediaFileName($fileExt[1]);    //Generate file name
        $_FILES[$image_name_param]['name'] = $filename;                 // assign name to file variable

        $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration
        $error_messages = array(
            'post_max_size' => 'The uploaded file exceeds the post_max_size directive in php.ini', // Set error messages
            'max_file_size' => 'File is too big',
            'min_file_size' => 'File is too small',
            'accept_file_types' => 'Filetype not allowed',
        );
        $options = array(
            'upload_dir' => $upload_file_path['temp_upload_dir'],
            'param_name' => $image_name_param, //file input name                      // Set configuration
            'inline_file_types' => $upload_file_path['file_types_allowed'],
            'accept_file_types' => $upload_file_path['file_types_allowed'],
            'max_file_size' => $upload_file_path['max_allowed_file_size'],
            'min_file_size' => $upload_file_path['min_allowed_file_size'],
            'image_versions' => array(
                'medium' => array(
                    'max_width' => $this->_config['file_upload_path']['user_medium_width'],
                    'max_height' => $this->_config['file_upload_path']['user_medium_height']
                ),
                'thumbnail' => array(
                    'max_width' => $this->_config['file_upload_path']['user_thumb_width'],
                    'max_height' => $this->_config['file_upload_path']['user_thumb_height']
            ))
        );
        $upload_handler = new UploadHandler($options, true, $error_messages);
        $file_response = $upload_handler->jsonResponceData;

        if (isset($file_response[$image_name_param][0]->error)) {
            $messages = array('status' => "error", 'message' => $file_response[$image_name_param][0]->error);
        } else {
            $messages = array('status' => "success", 'message' => 'Image Uploaded', 'filename' => $file_response[$image_name_param][0]->name);
        }
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This Action is used to edit contact
     * @author Icreon Tech-DG
     * @return JSON
     */
    public function editContactAction() {
        $this->layout('crm');
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getContactTable();
        $applicationConfigIds = $this->_config['application_config_ids'];
        $uSId = $applicationConfigIds['united_state_country_id'];
        $form_individual = new ContactIndividualForm();
        $form_cororporate_foundation = new ContactCorporteFoundationForm();
        $form_afihc = new ContactAfihcForm();
        $form_individual->add($form_afihc);
        $form_phone = new ContactPhonenoForm();
        $corporateFormPhone = new CorporatePhonenoForm();
        $form_education = new ContactEducationForm();
        $formContactStatus = new ContactStatusForm();
        $form_upload = new ContactUploadForm();
        $form_address = new ContactAddressesForm();
        $corporate_form_address = new CorporateContactAddressesForm();
        $form_website = new ContactWebsiteForm();
        $form_individual->add($formContactStatus);
        $form_cororporate_foundation->add($formContactStatus);
        $form_individual->add($form_phone);
        $form_cororporate_foundation->add($corporateFormPhone);
        $form_individual->add($form_education);
        $form_cororporate_foundation->add($form_website);
        $form_individual->add($form_upload);
        $form_cororporate_foundation->add($form_upload);
        $form_individual->add($form_address);
        $form_cororporate_foundation->add($form_address);
        $form_cororporate_foundation->add($corporate_form_address);
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $contact = new Contact($this->_adapter);
        $phoneTypeCorporate = array('' => 'Select', 'main' => 'Main', 'direct' => 'Direct', 'fax' => 'Fax', 'others' => 'Others');
        $phoneTypeIndividual = array('' => 'Select', 'home' => 'Home', 'cell' => 'Mobile', 'work' => 'Work', 'fax' => 'Fax', 'others' => 'Others');

        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
        $country_list = array();
        $country_list[''] = 'Select Country';
        foreach ($country as $key => $val) {
            $country_list[$val['country_id']] = $val['name'];
        }
        $form_address->get('addressinfo_country[]')->setAttribute('options', $country_list);
        $corporate_form_address->get('corporate_addressinfo_country[]')->setAttribute('options', $country_list);

        //$form_individual->get('country_id')->setAttribute('options', $country_list);
        //$form_cororporate_foundation->get('corporate_country_id')->setAttribute('options', $country_list);

        $state = $this->getServiceLocator()->get('Common\Model\CommonTable')->getStates();
        $stateList = array();
        $stateList[''] = 'Select State';
        foreach ($state as $key => $val) {
            $stateList[$val['state_code']] = $val['state'] . " - " . $val['state_code'];
        }
        $form_address->get('address_state_id[]')->setAttribute('options', $stateList);

        $apoPoStates = $this->_config['transaction_config']['apo_po_state'];
        $apoPoStatesList[''] = 'Select State';

        foreach ($apoPoStates as $k => $v) {
            $apoPoStatesList[$k] = $v;
        }
        $form_address->get('address_state_apo[]')->setAttribute('options', $apoPoStatesList);

        $form_address->get('address_country_id[]')->setAttribute('options', array(228 => 'UNITED STATES'));

        $corporate_form_address->get('corporate_address_state_id[]')->setAttribute('options', $stateList);
        $corporate_form_address->get('corporate_address_state_apo[]')->setAttribute('options', $apoPoStatesList);
        $corporate_form_address->get('corporate_address_country_id[]')->setAttribute('options', array(228 => 'UNITED STATES'));


        $form_cororporate_foundation->get('corporate_submit')->setValue('UPDATE');
        $form_individual->get('submit')->setValue('UPDATE');

        $get_user_type = $this->getServiceLocator()->get('Common\Model\CommonTable')->getType();
        $user_type = array();
        foreach ($get_user_type as $key => $val) {
            $user_type[$val['user_type_id']] = $val['user_type'];
        }
        $form_individual->get('contact_type')->setAttribute('options', $user_type);
        $form_cororporate_foundation->get('corporate_contact_type')->setAttribute('options', $user_type, 'selected', '2');

        $get_title = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTitle();
        $user_title = array();
        $user_title[''] = 'None';
        foreach ($get_title as $key => $val) {
            $user_title[$val['title']] = $val['title'];
        }
        $form_individual->get('title')->setAttribute('options', $user_title);
        

        $form_address->get('addressinfo_title[]')->setAttribute('options', $user_title);

        $corporate_form_address->get('corporate_addressinfo_title[]')->setAttribute('options', $user_title);     

        $get_suffix = $this->getServiceLocator()->get('Common\Model\CommonTable')->getSuffix();
        $user_suffix = array();
        $user_suffix[''] = 'None';
        foreach ($get_suffix as $key => $val) {
            $user_suffix[$val['suffix']] = $val['suffix'];
        }
        $form_individual->get('suffix')->setAttribute('options', $user_suffix);

        $get_tag = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTags();
        $user_tag_list = array();
        foreach ($get_tag as $key => $val) {
            $user_tag_list[$val['tag_id']] = $val['tag_name'];
        }

        $getMstGroups = $this->getServiceLocator()->get('Common\Model\CommonTable')->getGroups();
        $getGroups = array();
        foreach ($getMstGroups as $key => $val) {
            if ($val['group_type'] == '1') {
                $getGroups[$val['group_id']] = $val['name'];
            }
        }



        $get_income_range = $this->getServiceLocator()->get('Common\Model\CommonTable')->getIncomeRange();
        $income_range = array();
        $income_range[''] = 'Select';
        foreach ($get_income_range as $key => $val) {
            $income_range[$val['income_range_id']] = $val['income_range'];
        }
        $form_individual->get('demographics_income_range')->setAttribute('options', $income_range);

        $getUserReason = $this->getServiceLocator()->get('Common\Model\CommonTable')->getUserReason();
        $reasonType = array();
        $reasonType[''] = 'Select';
        foreach ($getUserReason as $key => $val) {
            $reasonType[$val['user_reason_id']] = $val['user_reason'];
        }
        $form_individual->get('contactstatus')->get('reason')->setAttribute('options', $reasonType);
        $form_cororporate_foundation->get('contactstatus')->get('reason')->setAttribute('options', $reasonType);

        $get_address_greeting = $this->getServiceLocator()->get('Common\Model\CommonTable')->getAddressGreeting(array('contact_type' => 1));
        $get_email_greeting = $this->getServiceLocator()->get('Common\Model\CommonTable')->getEmailGreeting(array('contact_type' => 1));
        $get_postal_greeting = $this->getServiceLocator()->get('Common\Model\CommonTable')->getPostalGreeting(array('contact_type' => 1));
        $address_greeting = array();
        $address_greeting[''] = 'Select';
        foreach ($get_address_greeting as $key => $val) {
            $address_greeting[$val['addressee_greeting_id']] = $val['heading'];
        }

        $postal_greeting = array();
        $postal_greeting[''] = 'Select';
        foreach ($get_postal_greeting as $key => $val) {
            $postal_greeting[$val['postal_greeting_id']] = $val['heading'];
        }

        $email_greeting = array();
        $email_greeting[''] = 'Select';
        foreach ($get_email_greeting as $key => $val) {
            $email_greeting[$val['email_greeting_id']] = $val['heading'];
        }
        //$address_greeting['Customized'] = 'Customized';
        $form_individual->get('communicationpref_email_greetings')->setAttribute('options', $email_greeting);
        $form_individual->get('communicationpref_postal_greetings')->setAttribute('options', $postal_greeting);
        $form_individual->get('communicationpref_address')->setAttribute('options', $address_greeting);

        $corpotateAddressGreeting = $this->getServiceLocator()->get('Common\Model\CommonTable')->getAddressGreeting(array('contact_type' => 2));
        $corporateEmailGreeting = $this->getServiceLocator()->get('Common\Model\CommonTable')->getEmailGreeting(array('contact_type' => 2));
        $corporatePostalGreeting = $this->getServiceLocator()->get('Common\Model\CommonTable')->getPostalGreeting(array('contact_type' => 2));
        $corAddressGreeting[''] = 'Select';
        foreach ($corpotateAddressGreeting as $key => $val) {
            $corAddressGreeting[$val['addressee_greeting_id']] = $val['heading'];
        }

        $corPostalGreeting = array();
        $corPostalGreeting[''] = 'Select';
        foreach ($corporatePostalGreeting as $key => $val) {
            $corPostalGreeting[$val['postal_greeting_id']] = $val['heading'];
        }

        $corEmailGreeting = array();
        $corEmailGreeting[''] = 'Select';
        foreach ($corporateEmailGreeting as $key => $val) {
            $corEmailGreeting[$val['email_greeting_id']] = $val['heading'];
        }

        $form_cororporate_foundation->get('corporate_communicationpref_email_greetings')->setAttribute('options', $corEmailGreeting);
        $form_cororporate_foundation->get('corporate_communicationpref_postal_greetings')->setAttribute('options', $corPostalGreeting);
        $form_cororporate_foundation->get('corporate_communicationpref_address')->setAttribute('options', $corAddressGreeting);

        $sec_question = $this->getServiceLocator()->get('Common\Model\QuestionTable')->getSecurityQuestion();
        $sec_question_list = array();
        $sec_question_list[''] = 'Select Security Question';
        foreach ($sec_question as $key => $val) {
            $sec_question_list[$val['security_question_id']] = $val['security_question'];
        }
        $form_individual->get('webaccesss_security_question')->setAttribute('options', $sec_question_list);
        $form_cororporate_foundation->get('corporate_webaccesss_security_question')->setAttribute('options', $sec_question_list);

        $response = $this->getResponse();
        $messages = array();
        $params = $this->params()->fromRoute();

        if (isset($params['id']) && $params['id'] != '') {
            $user_id = $this->decrypt($params['id']);
            $user_detail = $this->getContactTable()->getUserDetail(array('user_id' => $user_id));

            $getNationality = $this->getServiceLocator()->get('Common\Model\CommonTable')->getNationality(array('nationality_id' => $user_detail['nationality_id']));
            $nationality = array();
            $nationalityDesc = array();
            $nationality[''] = 'Select';
            foreach ($getNationality as $key => $val) {
                $nationality[$val['nationality_id']] = $val['nationality'];
                $nationalityDesc[$val['nationality_id']] = $val['description'];
            }
            $form_individual->get('demographics_nationality')->setAttribute('options', $nationality);

            $getEthinicity = $this->getServiceLocator()->get('Common\Model\CommonTable')->getEthinicity(array('ethnicity_id' => $user_detail['ethnicity_id']));
            $ethnicity = array();
            $ethnicityDesc = array();
            $ethnicity[''] = 'Select';
            foreach ($getEthinicity as $key => $val) {
                $ethnicity[$val['ethnicity_id']] = $val['ethnicity'];
                $ethnicityDesc[$val['ethnicity_id']] = $val['description'];
            }
            $form_individual->get('demographics_ethnicity')->setAttribute('options', $ethnicity);



            $get_source = $this->getServiceLocator()->get('Common\Model\CommonTable')->getSource(array('source_id' => $user_detail['source_id']));
            $source_list = array();
            $source_list[''] = 'Select Source';
            foreach ($get_source as $key => $val) {
                $source_list[$val['source_id']] = $val['source_name'];
            }
            $form_individual->get('source')->setAttribute('options', $source_list);
            $form_cororporate_foundation->get('corporate_source')->setAttribute('options', $source_list);

            $user_address_info = $this->getContactTable()->getUserAddressInformations(array('user_id' => $user_id));
            //asd($user_address_info);
            $locIds = '';
            if (!empty($user_address_info)) {
                $locType = array();
                foreach ($user_address_info as $val) {
                    if ($val['location_id'] != '')
                        $locType[] = $val['location_id'];
                }
                if (!empty($locType)) {
                    $locIds = implode(",", $locType);
                }
            }
            $get_location_type = $this->getServiceLocator()->get('Common\Model\CommonTable')->getLocationType(array('location_ids' => $locIds));

            $location_type = array();
            $location_typeDesc = array();
            $location_type[''] = 'Select';
            foreach ($get_location_type as $key => $val) {
                $location_type[$val['location_id']] = $val['location'];
                $location_typeDesc[$val['location_id']] = $val['description'];
            }
            $form_address->get('addressinfo_location_type[]')->setAttribute('options', $location_type);
            $corporate_form_address->get('corporate_addressinfo_location_type[]')->setAttribute('options', $location_type);

            $alternate_contact = $this->getContactTable()->getUserAlternateContact(array('user_id' => $user_id));
            $user_education = $this->getContactTable()->getUserEducations(array('user_id' => $user_id));

            $eduIds = '';
            if (!empty($user_education)) {
                $eduType = array();
                foreach ($user_education as $val) {
                    $eduType[] = $val['education_id'];
                }
                if (!empty($eduType)) {
                    $eduIds = implode(",", $eduType);
                }
            }

            $get_education_list = $this->getServiceLocator()->get('Common\Model\CommonTable')->getEducationDegreeList(array('education_id' => $eduIds));
            $education_list = array();
            $education_desc = array();
            $education_list[''] = 'Select';
            foreach ($get_education_list as $key => $val) {
                $education_list[$val['education_id']] = $val['education'];
                $education_desc[$val['education_id']] = $val['description'];
            }
            $form_education->get('edu_degree_level[]')->setAttribute('options', $education_list);
            $user_notes = $this->getContactTable()->getUserNotes(array('user_id' => $user_id));
            $user_document = $this->getContactTable()->getUserDocuments(array('user_id' => $user_id));
            $user_tag = $this->getContactTable()->getUserTags(array('user_id' => $user_id));
            $user_group = $this->getContactTable()->getUserGroups(array('user_id' => $user_id));
            $user_phone_info = $this->getContactTable()->getUserPhoneInformations(array('user_id' => $user_id));
            $user_website = $this->getContactTable()->getUserWebsites(array('user_id' => $user_id));
            $user_afhic = $this->getContactTable()->getUserAfihc(array('user_id' => $user_id));
            $user_communication_preferences = $this->getContactTable()->getCommunicationPreferences(array('user_id' => $user_id));
            $docFiles = array();
            if (!empty($user_document)) {
                if ($user_document['uploaded_files'] != '') {
                    $docFiles = explode(",", $user_document['uploaded_files']);
                }
            }
            $folder_struc_by_user = $this->FolderStructByUserId($user_id);
            $uploadedFilePath = $this->_config['file_upload_path']['assets_url'] . 'user/' . $folder_struc_by_user;
            $assets_upload_dir = $this->_config['file_upload_path']['assets_upload_dir'] . 'user/' . $folder_struc_by_user;
        }
        $form_individual->get('contactstatus')->get('reason')->setValue($user_detail['reason_id']);
        $form_individual->get('contactstatus')->get('reason_other')->setValue($user_detail['reason']);


        $form_cororporate_foundation->get('contactstatus')->get('reason')->setValue($user_detail['reason_id']);
        $form_cororporate_foundation->get('contactstatus')->get('reason_other')->setValue($user_detail['reason']);

        if ($user_detail['email_verified'] == '0' && ($user_detail['reason_id'] < 0 || $user_detail['reason_id'] == '')) {
            $form_individual->get('contactstatus')->get('contact_status')->setValue(2);
            $form_cororporate_foundation->get('contactstatus')->get('contact_status')->setValue(2);
        } else {
            $form_individual->get('contactstatus')->get('contact_status')->setValue($user_detail['is_active']);
            $form_cororporate_foundation->get('contactstatus')->get('contact_status')->setValue($user_detail['is_active']);
        }

        $form_individual->get('upload_profile_image_id')->setValue($user_detail['profile_image']);
        $form_individual->get('contact_id')->setValue($params['id']);
        $form_individual->get('contact_type')->setValue($user_detail['user_type']);
        //$form_individual->get('country_id')->setValue($user_detail['country_id']);
        //$form_individual->get('zip_code')->setValue($user_detail['zip_code']);
        $form_individual->get('first_name')->setValue($user_detail['first_name']);
        $form_individual->get('middle_name')->setValue($user_detail['middle_name']);
        $form_individual->get('last_name')->setValue($user_detail['last_name']);
        $form_individual->get('title')->setValue($user_detail['title']);
        $form_individual->get('suffix')->setValue($user_detail['suffix']);
        $form_individual->get('email_id')->setValue($user_detail['email_id']);
        $form_individual->get('no_email_address')->setValue($user_detail['no_email_address']);
        $form_individual->get('demographics_gender')->setValue($user_detail['gender']);
        $form_individual->get('demographics_marital_status')->setValue($user_detail['marital_status']);
        $form_individual->get('demographics_age_limit')->setValue($user_detail['children_below_eighteen']);


        $form_individual->get('demographics_income_range')->setValue($user_detail['income_range_id']);
        $form_individual->get('demographics_ethnicity')->setValue($user_detail['ethnicity_id']);
        $form_individual->get('demographics_nationality')->setValue($user_detail['nationality_id']);
        $form_individual->get('source')->setValue($user_detail['source_id']);
        $form_individual->get('webaccesss_security_question')->setValue($user_detail['security_question_id']);
        $form_individual->get('webaccesss_security_answer')->setValue($user_detail['security_answer']);
        $form_individual->get('webaccesss_enabled_login')->setValue($user_detail['is_login_enabled']);
        //$form_individual->get('contactstatus')->get('contact_status')->setValue($user_detail['is_active']);
        $form_individual->get('alt_name')->setValue($alternate_contact['name']);
        $form_individual->get('alt_email')->setValue($alternate_contact['email_id']);
        $form_individual->get('alt_phoneno')->setValue($alternate_contact['phone']);
        $form_individual->get('alt_relationship')->setValue($alternate_contact['relationship']);

        if (!empty($user_tag)) {
            $userTagsIndividual = '';
            foreach ($user_tag as $val) {
                $userTagsIndividual .= $val['tag_name'] . ",";
            }
            $form_individual->get('tagsandgroups_company_name')->setValue(rtrim($userTagsIndividual, ","));
        }

        $form_individual->get('communicationpref_email_greetings')->setValue($user_communication_preferences['email_greeting_id']);
        $form_individual->get('communicationpref_custome_email_greetings')->setValue($user_communication_preferences['custom_email_greeting']);
        $form_individual->get('communicationpref_postal_greetings')->setValue($user_communication_preferences['postal_greeting_id']);
        $form_individual->get('communicationpref_custome_postal_greetings')->setValue($user_communication_preferences['custom_postal_greeting']);
        $form_individual->get('communicationpref_address')->setValue($user_communication_preferences['addressee_greeting_id']);
        $form_individual->get('communicationpref_custome_address_greetings')->setValue($user_communication_preferences['custom_addressee']);
        if ($user_communication_preferences['preferred_method'] != '') {
            $preferred_method = explode(",", $user_communication_preferences['preferred_method']);
            $form_individual->get('communicationpref_preferred_method')->setValue($preferred_method);
            $form_cororporate_foundation->get('corporate_communicationpref_preferred_method')->setValue($preferred_method);
        }
        if ($user_communication_preferences['privacy'] != '') {
            $privacy = explode(",", $user_communication_preferences['privacy']);
            $form_individual->get('communicationpref_privacy')->setValue($privacy);
            $form_cororporate_foundation->get('corporate_communicationpref_privacy')->setValue($privacy);
        }

        $form_individual->get('publicdocs_description')->setValue($user_document['description']);

        if (!empty($user_group)) {
            foreach ($user_group as $val) {
                $group[$val['group_id']] = $val['name'];
            }
            $group_list_from = array_diff($getGroups, $group);
            $form_individual->get('tagsandgroups_groups_to')->setAttribute('options', $group);
            $form_individual->get('tagsandgroups_groups_from')->setAttribute('options', $group_list_from);

            $form_cororporate_foundation->get('cor_tagsandgroups_groups_to')->setAttribute('options', $group);
            $form_cororporate_foundation->get('cor_tagsandgroups_groups_from')->setAttribute('options', $group_list_from);
        } else {
            $form_individual->get('tagsandgroups_groups_from')->setAttribute('options', $getGroups);
            $form_cororporate_foundation->get('cor_tagsandgroups_groups_from')->setAttribute('options', $getGroups);
        }

        $form_cororporate_foundation->get('corporate_upload_profile_image_id')->setValue($user_detail['profile_image']);
        $form_cororporate_foundation->get('corporate_contact_type')->setValue($user_detail['user_type']);
        $form_cororporate_foundation->get('corporate_email_id')->setValue($user_detail['email_id']);
        //$form_cororporate_foundation->get('corporate_country_id')->setValue($user_detail['country_id']);
        //$form_cororporate_foundation->get('corporate_zip_code')->setValue($user_detail['zip_code']);
        $form_cororporate_foundation->get('corporate_company_name')->setValue($user_detail['company_name']);
        $form_cororporate_foundation->get('corporate_sic_code')->setValue($user_detail['sic_code']);
        $form_cororporate_foundation->get('corporate_source')->setValue($user_detail['source_id']);
        $form_cororporate_foundation->get('corporate_legal_name')->setValue($user_detail['legal_name']);
        $form_cororporate_foundation->get('corporate_no_email_address')->setValue($user_detail['no_email_address']);
        $form_cororporate_foundation->get('website')->get('don_not_have_website')->setValue($user_detail['is_website']);
        $form_cororporate_foundation->get('corporate_webaccesss_security_question')->setValue($user_detail['security_question_id']);
        $form_cororporate_foundation->get('corporate_webaccesss_security_answer')->setValue($user_detail['security_answer']);
        $form_cororporate_foundation->get('corporate_webaccesss_enabled_login')->setValue($user_detail['is_login_enabled']);
        $form_cororporate_foundation->get('corporate_communicationpref_email_greetings')->setValue($user_communication_preferences['email_greeting_id']);
        $form_cororporate_foundation->get('corporate_communicationpref_custome_email_greetings')->setValue($user_communication_preferences['custom_email_greeting']);
        $form_cororporate_foundation->get('corporate_communicationpref_postal_greetings')->setValue($user_communication_preferences['postal_greeting_id']);
        $form_cororporate_foundation->get('corporate_communicationpref_custome_postal_greetings')->setValue($user_communication_preferences['custom_postal_greeting']);
        $form_cororporate_foundation->get('corporate_communicationpref_address')->setValue($user_communication_preferences['addressee_greeting_id']);
        $form_cororporate_foundation->get('corporate_communicationpref_custome_address_greetings')->setValue($user_communication_preferences['custom_addressee']);
        if (!empty($user_tag)) {
            $userTags = '';
            foreach ($user_tag as $val) {
                $userTags .= $val['tag_name'] . ",";
            }
            $form_cororporate_foundation->get('corporate_tagsandgroups_company_name')->setValue(rtrim($userTags, ","));
        }

        $form_cororporate_foundation->get('corporate_publicdocs_description')->setValue($user_document['description']);


        $form_cororporate_foundation->add(array(
            'type' => 'text',
            'name' => 'corporate_contact_type',
            'attributes' => array(
                'class' => 'e1 select-w-320',
                'id' => 'corporate_contact_type',
                'value' => $user_type[$user_detail['user_type']],
                'class' => 'readonly-text',
                'readonly' => 'readonly'
            ),
        ));

        $form_individual->add(array(
            'type' => 'text',
            'name' => 'contact_type',
            'attributes' => array(
                'class' => 'e1 select-w-320',
                'id' => 'corporate_contact_type',
                'class' => 'readonly-text',
                'value' => $user_type[$user_detail['user_type']],
                'readonly' => 'readonly',
            ),
        ));

        /** add private note* */
        $moduleInfoArr = array();
        $moduleInfoArr[] = 'User';
        $moduleInfoArr[] = 'Contact';
        $moduleInfoArr[] = 'create-contact-private-note';
        $moduleDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getModuleAction($moduleInfoArr);

        $auth = new AuthenticationService();

        $userDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getCrmUserDetails(array('crm_user_id' => $auth->getIdentity()->crm_user_id));

        $roleDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->checkCrmUserPermission($userDetail[0]['role_id']);

        $addPrivateNotePermission = isset($roleDetail['moduleActionPermissions'][$moduleDetail['module_id']][$moduleDetail['action_id']]) ? $roleDetail['moduleActionPermissions'][$moduleDetail['module_id']][$moduleDetail['action_id']] : 1;
        /** end of add private note * */
        /** view private note* */
        $moduleInfoArr = array();
        $moduleInfoArr[] = 'User';
        $moduleInfoArr[] = 'Contact';
        $moduleInfoArr[] = 'view-contact-private-note';
        $moduleDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getModuleAction($moduleInfoArr);

        $auth = new AuthenticationService();

        $userDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getCrmUserDetails(array('crm_user_id' => $auth->getIdentity()->crm_user_id));

        $roleDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->checkCrmUserPermission($userDetail[0]['role_id']);

        $viewPrivateNotePermission = isset($roleDetail['moduleActionPermissions'][$moduleDetail['module_id']][$moduleDetail['action_id']]) ? $roleDetail['moduleActionPermissions'][$moduleDetail['module_id']][$moduleDetail['action_id']] : 1;
        /** end of add private note * */
        if ($request->isPost()) {

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $form_data = $request->getPost();
                if ($form_data['corporate_contact_type']) {
                    $form_data['corporate_contact_type'] = array_search($form_data['corporate_contact_type'], $user_type);
                } else {
                    $form_data['contact_type'] = array_search($form_data['contact_type'], $user_type);
                }

                $date = DATE_TIME_FORMAT;
                $form_data['add_date'] = $date;
                $form_data['modified_date'] = $date;
                $form_data['user_id'] = $this->decrypt($form_data['contact_id']);
                $user_id = $this->decrypt($form_data['contact_id']);

                if (isset($form_data['corporate_email_id']) && $form_data['corporate_email_id'] != '') {
                    $userDataArr = $this->getUserTable()->getUser(array('email_id' => $form_data['corporate_email_id']));
                    if (!empty($userDataArr) && $userDataArr['user_id'] != $user_id) {
                        $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['create_contact']['EMAIL_EXITS'], 'email_exits' => true);
                        $response->setContent(\Zend\Json\Json::encode($messages));
                        return $response;
                    }
                } else if (isset($form_data['email_id']) && $form_data['email_id'] != '') {
                    $userDataArr = $this->getUserTable()->getUser(array('email_id' => $form_data['email_id']));
                    if (!empty($userDataArr) && $userDataArr['user_id'] != $user_id) {
                        $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['create_contact']['EMAIL_EXITS'], 'email_exits' => true);
                        $response->setContent(\Zend\Json\Json::encode($messages));
                        return $response;
                    }
                }

                if (isset($form_data['contact_type'])) {
                    $filterParam = $contact->filterParam($form_data);
                } else {
                    $filterParam = $contact->filterParamCorporate($form_data);
                }
                if ($filterParam['contact_status'] == '2') {
                    $filterParam['contact_status'] = '0';
                    $filterParam['email_verification'] = '0';
                    $filterParam['reason'] = null;
                } else if ($filterParam['contact_status'] == '1') {
                    $filterParam['email_verification'] = '1';
                } else {
                    $filterParam['email_verification'] = '0';
                }

               // $filterParam = $contact->filterParam($form_data); 
                $last_update = $this->getContactTable()->updateUserDetails($filterParam);

                if ($last_update) {
                    $this->getContactTable()->deleteUserDetails(array('user_id' => $user_id));
                    $form_data['added_by'] = $this->auth->getIdentity()->crm_user_id;
                    if (isset($form_data['contact_type'])) {

                        $filterParam = $contact->filterParam($form_data);

                        $this->getContactTable()->insertAlternateContacts($filterParam);
                        if (!empty($form_data['afihc'])) {
                            foreach ($form_data['afihc'] as $val) {
                                $filterParam['afihc'] = $val;
                                $this->getContactTable()->insertAfihc($filterParam);
                            }
                        }

                        if (!empty($form_data['edu_degree_level'])) {
                            $edu_form = array();
                            for ($i = 0; $i < count($form_data['edu_degree_level']); $i++) {
                                $edu_form[$i]['edu_degree_level'] = isset($form_data['edu_degree_level'][$i]) ? $form_data['edu_degree_level'][$i] : null;
                                $edu_form[$i]['edu_institution'] = isset($form_data['edu_institution'][$i]) ? $form_data['edu_institution'][$i] : null;
                                $edu_form[$i]['edu_attended_year'] = isset($form_data['edu_attended_year'][$i]) ? $form_data['edu_attended_year'][$i] : null;
                                $edu_form[$i]['edu_major'] = isset($form_data['edu_major'][$i]) ? $form_data['edu_major'][$i] : null;
                                $edu_form[$i]['edu_gpa'] = isset($form_data['edu_gpa'][$i]) ? $form_data['edu_gpa'][$i] : null;
                            }
                            foreach ($edu_form as $val) {
                                $val['user_id'] = $user_id;
                                $val['add_date'] = $date;
                                $val['modified_date'] = $date;
                                $this->getContactTable()->insertEducation($val);
                            }
                        }
                        if (!empty($form_data['tagsandgroups_groups_to'])) {
                            foreach ($form_data['tagsandgroups_groups_to'] as $val) {
                                $value['user_id'] = $user_id;
                                $value['add_date'] = $date;
                                $value['modified_date'] = $date;
                                $value['groups'] = $val;
                                $this->getContactTable()->insertGroups($value);
                            }
                        }
                        //if (isset($form_data['addressinfo_country'])) {
                        $address_form = array();
                        for ($m = 0; $m < count($form_data['addressinfo_country']); $m++) {
                            $address_form[$m]['addressinfo_id'] = isset($form_data['addressinfo_id'][$m]) ? $form_data['addressinfo_id'][$m] : '';
                            $address_form[$m]['addressinfo_location_type'] = isset($form_data['addressinfo_location_type'][$m]) ? $form_data['addressinfo_location_type'][$m] : '';
                            
                            $address_form[$m]['title'] = isset($form_data['addressinfo_title'][$m]) ? rtrim($form_data['addressinfo_title'][$m], ',') : '';
                            $address_form[$m]['first_name'] = isset($form_data['addressinfo_first_name'][$m]) ? rtrim($form_data['addressinfo_first_name'][$m], ',') : '';
                            $address_form[$m]['last_name'] = isset($form_data['addressinfo_last_name'][$m]) ? rtrim($form_data['addressinfo_last_name'][$m], ',') : '';
                            
                            $address_form[$m]['addressinfo_street_address_1'] = isset($form_data['addressinfo_street_address_1'][$m]) ? rtrim($form_data['addressinfo_street_address_1'][$m], ',') : '';
                            $address_form[$m]['addressinfo_city'] = isset($form_data['addressinfo_city'][$m]) ? $form_data['addressinfo_city'][$m] : '';
                            $address_form[$m]['addressinfo_zip'] = isset($form_data['addressinfo_zip'][$m]) ? $form_data['addressinfo_zip'][$m] : '';
                            $address_form[$m]['addressinfo_street_address_2'] = isset($form_data['addressinfo_street_address_2'][$m]) ? rtrim($form_data['addressinfo_street_address_2'][$m], ',') : '';

                            $address_form[$m]['addressinfo_state'] = isset($form_data['addressinfo_state'][$m]) ? $form_data['addressinfo_state'][$m] : '';

                            if ((isset($form_data['addressinfo_location_shipping'][$m]) && $form_data['addressinfo_location_shipping'][$m] != '') && (isset($form_data['is_apo_po'][$m]) && $form_data['is_apo_po'][$m] != 1)) {
                                $address_form[$m]['addressinfo_country'] = isset($form_data['address_country_id'][$m]) ? $form_data['address_country_id'][$m] : null;
                            } else {
                                $address_form[$m]['addressinfo_country'] = isset($form_data['addressinfo_country'][$m]) ? $form_data['addressinfo_country'][$m] : null;
                            }
                            //$address_form[$m]['addressinfo_country'] = isset($form_data['addressinfo_country'][$m]) ? $form_data['addressinfo_country'][$m] : '';

                            if ((isset($form_data['addressinfo_location_shipping'][$m]) && $form_data['addressinfo_location_shipping'][$m] != '') && isset($form_data['is_apo_po'][$m]))
                                $address_form[$m]['address_type'] = $form_data['is_apo_po'][$m];
                        }
                        $j = 0;
                            
                        for ($count = 0; $count < $form_data['primary_location_count']; $count++) {
                            $key = 'primary_location' . $count;
                            if(isset($form_data[$key]))
                            {
                                $addressType = $form_data[$key];
                                if ($count == $form_data['address_primary_index']) {
                                    $address_form[$j]['addressinfo_location'] = "1," . $addressType;
                                } else {
                                    $address_form[$j]['addressinfo_location'] = $addressType;
                                }
                                if ($address_form[$j]['addressinfo_location'] != '') {
                                    $address_form[$j]['addressinfo_location'] = rtrim($address_form[$j]['addressinfo_location'], ",");
                                }
                                $j++;
                            }
                            
                        }
                        //echo "<pre>";print_r($address_form);die;
                        //}
                        if (isset($form_data['country_code'])) {
                            $phon_form = array();
                            for ($i = 0; $i < count($form_data['country_code']); $i++) {
                                if ($form_data['country_code'][$i] != '' || $form_data['area_code'][$i] != '' || $form_data['phone_no'][$i] != '' || $form_data['phone_type'][$i] != '') {
                                    $phon_form[$i]['country_code'] = isset($form_data['country_code'][$i]) ? $form_data['country_code'][$i] : null;
                                    $phon_form[$i]['area_code'] = isset($form_data['area_code'][$i]) ? $form_data['area_code'][$i] : null;
                                    $phon_form[$i]['phone_no'] = isset($form_data['phone_no'][$i]) ? $form_data['phone_no'][$i] : null;
                                    $phon_form[$i]['extension'] = isset($form_data['extension'][$i]) ? $form_data['extension'][$i] : null;
                                    $phon_form[$i]['phone_type'] = isset($form_data['phone_type'][$i]) ? $form_data['phone_type'][$i] : null;
                                    $primaryPhone = $form_data['primary-phone'];
                                    if ($i == $primaryPhone) {
                                        $phon_form[$i]['continfo_primary'] = 1;
                                    } else {
                                        $phon_form[$i]['continfo_primary'] = null;
                                    }
                                }
                            }
                        }
                    } else {
                        $filterParam = $contact->filterParamCorporate($form_data);
                        if (!empty($form_data['cor_tagsandgroups_groups_to'])) {
                            foreach ($form_data['cor_tagsandgroups_groups_to'] as $val) {
                                $value['user_id'] = $user_id;
                                $value['add_date'] = $date;
                                $value['modified_date'] = $date;
                                $value['groups'] = $val;
                                $this->getContactTable()->insertGroups($value);
                            }
                        }
                        if ($form_data['don_not_have_website'] == 0) {
                            if (!empty($form_data['website']) && $form_data['website'][0] != '') {
                                $this->getContactTable()->deleteWebsite(array('user_id' => $user_id));
                                foreach ($form_data['website'] as $val) {
                                    $website['user_id'] = $user_id;
                                    $website['add_date'] = $date;
                                    $website['modified_date'] = $date;
                                    $website['website'] = $val;
                                    $this->getContactTable()->insertWebsite($website);
                                }
                            }
                        }
                        //if (isset($form_data['corporate_addressinfo_country'])) {
                        $address_form = array();

                        for ($i = 0; $i < count($form_data['corporate_addressinfo_country']); $i++) {
                            $address_form[$i]['addressinfo_location_type'] = isset($form_data['corporate_addressinfo_location_type'][$i]) ? $form_data['corporate_addressinfo_location_type'][$i] : '';
                            $address_form[$i]['addressinfo_id'] = isset($form_data['corporate_addressinfo_id'][$i]) ? $form_data['corporate_addressinfo_id'][$i] : '';

                            $address_form[$i]['title'] = isset($form_data['corporate_addressinfo_title'][$i]) ? rtrim($form_data['corporate_addressinfo_title'][$i], ',') : '';
                            $address_form[$i]['first_name'] = isset($form_data['corporate_addressinfo_first_name'][$i]) ? rtrim($form_data['corporate_addressinfo_first_name'][$i], ',') : '';
                            $address_form[$i]['last_name'] = isset($form_data['corporate_addressinfo_last_name'][$i]) ? rtrim($form_data['corporate_addressinfo_last_name'][$i], ',') : '';
                            
                            $address_form[$i]['addressinfo_street_address_1'] = isset($form_data['corporate_addressinfo_street_address_1'][$i]) ? rtrim($form_data['corporate_addressinfo_street_address_1'][$i], ',') : '';
                            $address_form[$i]['addressinfo_city'] = isset($form_data['corporate_addressinfo_city'][$i]) ? $form_data['corporate_addressinfo_city'][$i] : '';
                            $address_form[$i]['addressinfo_zip'] = isset($form_data['corporate_addressinfo_zip'][$i]) ? $form_data['corporate_addressinfo_zip'][$i] : '';
                            $address_form[$i]['addressinfo_street_address_2'] = isset($form_data['corporate_addressinfo_street_address_2'][$i]) ? rtrim($form_data['corporate_addressinfo_street_address_2'][$i], ',') : '';
                            $address_form[$i]['addressinfo_state'] = isset($form_data['corporate_addressinfo_state'][$i]) ? $form_data['corporate_addressinfo_state'][$i] : '';

                            if ((isset($form_data['corporate_addressinfo_location_shipping'][$i]) && $form_data['corporate_addressinfo_location_shipping'][$i] != '') && (isset($form_data['corporate_is_apo_po'][$i]) && $form_data['corporate_is_apo_po'][$i] != 1)) {
                                $address_form[$i]['addressinfo_country'] = isset($form_data['corporate_address_country_id'][$i]) ? $form_data['corporate_address_country_id'][$i] : null;
                            } else {
                                $address_form[$i]['addressinfo_country'] = isset($form_data['corporate_addressinfo_country'][$i]) ? $form_data['corporate_addressinfo_country'][$i] : null;
                            }
                            //$address_form[$i]['addressinfo_country'] = isset($form_data['corporate_addressinfo_country'][$i]) ? $form_data['corporate_addressinfo_country'][$i] : '';

                            if ((isset($form_data['corporate_addressinfo_location_shipping'][$i]) && $form_data['corporate_addressinfo_location_shipping'][$i] != '') && isset($form_data['corporate_is_apo_po'][$i]))
                                $address_form[$i]['address_type'] = $form_data['corporate_is_apo_po'][$i];
                        }
                        $j = 0;
                        for ($count = 0; $count < $form_data['corporate_primary_location_count'];$count++)
                        {
                            $key = 'corporate_primary_location' . $count;
                            if (isset($form_data[$key]))
                            {
                                $addressType = $form_data[$key];
                                if ($count == $form_data['corporate_address_primary_index'])
                                {
                                    $address_form[$j]['addressinfo_location']= "1," . $addressType;
                                }
                                else
                                {
                                    $address_form[$j]['addressinfo_location']= $addressType;
                                }
                                if ($address_form[$j]['addressinfo_location']!= '')
                                {
                                    $address_form[$j]['addressinfo_location']= rtrim($address_form[$j]['addressinfo_location'],",");
                                }
                                $j++;
                            }
                        }
                        //}
                        if (isset($form_data['corporate_country_code'])) {
                            $phon_form = array();
                            for ($i = 0; $i < count($form_data['corporate_country_code']); $i++) {
                                if ($form_data['corporate_country_code'][$i] != '' || $form_data['corporate_area_code'][$i] != '' || $form_data['corporate_phone_no'][$i] != '' || $form_data['corporate_phone_type'][$i] != '') {
                                    $phon_form[$i]['country_code'] = isset($form_data['corporate_country_code'][$i]) ? $form_data['corporate_country_code'][$i] : null;
                                    $phon_form[$i]['area_code'] = isset($form_data['corporate_area_code'][$i]) ? $form_data['corporate_area_code'][$i] : null;
                                    $phon_form[$i]['phone_no'] = isset($form_data['corporate_phone_no'][$i]) ? $form_data['corporate_phone_no'][$i] : null;
                                    $phon_form[$i]['extension'] = isset($form_data['corporate_extension'][$i]) ? $form_data['corporate_extension'][$i] : null;
                                    $phon_form[$i]['phone_type'] = isset($form_data['corporate_phone_type'][$i]) ? $form_data['corporate_phone_type'][$i] : null;
                                    $primaryPhone = $form_data['corporate-primary-phone'];
                                    if ($i == $primaryPhone) {
                                        $phon_form[$i]['continfo_primary'] = 1;
                                    } else {
                                        $phon_form[$i]['continfo_primary'] = null;
                                    }
                                }
                            }
                        }
                    }
                    if ($filterParam['notes'] != '') {
                        $this->getContactTable()->insertNotes($filterParam);
                    }
                    if (!empty($address_form)) {
                        $isPrimaryAddress = true;
                        foreach ($address_form as $key => $addVal) {
                            if (isset($addVal['addressinfo_location']) && $addVal['addressinfo_location'] != '') {
                                $explodedAddress = explode(",", $addVal['addressinfo_location']);
                                if (in_array(1, $explodedAddress)) {
                                    $isPrimaryAddress = false;
                                }
                            }
                        }
                        foreach ($address_form as $key => $addVal) {
                            $addVal['user_id'] = $user_id;
                            
                            $addVal['add_date'] = $date;
                            $addVal['modified_date'] = $date;
                            if ($isPrimaryAddress && $key == 0) {
                                $addVal['addressinfo_location'] = 1;
                            }
                            //echo "<pre>";print_r($addVal);die;
                            if(isset($addVal['addressinfo_id']) && trim($addVal['addressinfo_id']) !='' && $addVal['addressinfo_id'] !=0)
                            {
                               $this->getContactTable()->updateAddressInfo($addVal); 
                            }
                            elseif(trim($addVal['addressinfo_country']) !='')
                            {
                                $this->getContactTable()->insertAddressInfo($addVal);
                            }
                            
                        }
                    }

                    foreach ($phon_form as $val) {
                        $val['user_id'] = $user_id;
                        $val['add_date'] = $date;
                        $val['modified_date'] = $date;
                        $this->getContactTable()->insertPhoneInformations($val);
                    }

                    $this->getContactTable()->insertCommunicationPreferences($filterParam);

                    if ($filterParam['tag_id'] != '') {
                        $postedTags = explode(",", $filterParam['tag_id']);
                        $mstTags = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTags();
                        $tags = array();
                        if (!empty($mstTags)) {
                            foreach ($mstTags as $val) {
                                $key = $val['tag_id'];
                                $tags[$key] = $val['tag_name'];
                            }
                        }
                        foreach ($postedTags as $val) {
                            if ($val) {
                                if (in_array($val, $tags)) {
                                    $tagId = array_search($val, $tags);
                                    $filterParam['tag_id'] = $tagId;
                                    $filterParam['user_id'] = $user_id;
                                    $this->getContactTable()->insertUserTags($filterParam);
                                } else {
                                    $filterParam['tagsandgroups_company_name'] = $val;
                                    $tagId = $this->getContactTable()->insertMasterTags($filterParam);
                                    $filterParam['tag_id'] = $tagId;
                                    $filterParam['user_id'] = $user_id;
                                    $this->getContactTable()->insertUserTags($filterParam);
                                }
                            }
                        }
                    }
                    $this->getContactTable()->insertDocument($filterParam);

                    /** insert into the change log */
                    $changeLogArray = array();
                    $changeLogArray['table_name'] = 'tbl_usr_change_logs';
                    $changeLogArray['activity'] = '2';/** 2 == for update */
                    $changeLogArray['id'] = $user_id;
                    $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                    $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                    $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                    if ($filterParam['upload_profile_image_id'] != '') {
                        $operation = 2;
                        $this->moveFileFromTempToContact($filterParam['upload_profile_image_id'], $user_id, $operation, $user_detail['profile_image']);
                    }

                    if (isset($filterParam['publicdocs_description']) && $filterParam['publicdocs_description'] != '') {
                        $allFilesNames = '';
                        for ($count = 0; $count <= $filterParam['publicdocs_upload_count']; $count++) {
                            $key = 'publicdocs_upload_file' . $count;
                            $filesName = $form_data[$key];
                            if ($filesName != '') {
                                $allFilesNames .= $filesName . ",";
                                $this->movePublicDocFileFromTempToContact($filesName, $user_id);
                            }
                        }
                        if ($allFilesNames != '') {
                            if (!empty($user_document) && $user_document['uploaded_files'] != '') {
                                $allFilesNames = $allFilesNames . "" . $user_document['uploaded_files'];
                            }
                            $allFilesNames = rtrim($allFilesNames, ",");
                            $filterParam['user_id'] = $user_id;
                            $filterParam['publicdocs_upload'] = $allFilesNames;
                            $this->getContactTable()->insertDocument($filterParam);
                        } else {
                            if (!empty($user_document) && $user_document['uploaded_files'] != '') {
                                $allFilesNames = $allFilesNames . "" . $user_document['uploaded_files'];
                                $filterParam['publicdocs_upload'] = $allFilesNames;
                            } else {
                                $filterParam['publicdocs_upload'] = '';
                            }
                            $filterParam['user_id'] = $user_id;
                            $this->getContactTable()->insertDocument($filterParam);
                        }
                    } else if (isset($filterParam['corporate_publicdocs_description']) && $filterParam['corporate_publicdocs_description'] != '') {
                        $allFilesNames = '';
                        for ($count = 0; $count <= $filterParam['publicdocs_upload_count']; $count++) {
                            $key = 'publicdocs_upload_file' . $count;
                            $filesName = $form_data[$key];
                            if ($filesName != '') {
                                $allFilesNames .= $filesName . ",";
                                $this->movePublicDocFileFromTempToContact($filesName, $user_id);
                            }
                        }
                        if ($allFilesNames != '') {
                            if (!empty($user_document) && $user_document['uploaded_files'] != '') {
                                $allFilesNames = $allFilesNames . "" . $user_document['uploaded_files'];
                            }
                            $allFilesNames = rtrim($allFilesNames, ",");
                            $filterParam['user_id'] = $user_id;
                            $filterParam['publicdocs_upload'] = $allFilesNames;
                            $this->getContactTable()->insertDocument($filterParam);
                        } else {
                            if (!empty($user_document) && $user_document['uploaded_files'] != '') {
                                $allFilesNames = $allFilesNames . "" . $user_document['uploaded_files'];
                                $filterParam['publicdocs_upload'] = $allFilesNames;
                            } else {
                                $filterParam['publicdocs_upload'] = '';
                            }
                            $filterParam['user_id'] = $user_id;
                            $this->getContactTable()->insertDocument($filterParam);
                        }
                    } else {
                        if (isset($form_data['contact_type'])) {
                            $allFilesNames = '';
                            for ($count = 0; $count <= $filterParam['publicdocs_upload_count']; $count++) {
                                $key = 'publicdocs_upload_file' . $count;
                                $filesName = $form_data[$key];
                                if ($filesName != '') {
                                    $allFilesNames .= $filesName . ",";
                                    $this->movePublicDocFileFromTempToContact($filesName, $user_id);
                                }
                            }
                            if ($allFilesNames != '') {
                                $allFilesNames = rtrim($allFilesNames, ",");
                                $filterParam['user_id'] = $user_id;
                                $filterParam['publicdocs_upload'] = $allFilesNames;
                                $this->getContactTable()->insertDocument($filterParam);
                            }
                        } else {
                            $allFilesNames = '';
                            for ($count = 0; $count <= $filterParam['cor_publicdocs_upload_count']; $count++) {
                                $key = 'cor_publicdocs_upload_file' . $count;
                                $filesName = $form_data[$key];
                                if ($filesName != '') {
                                    $allFilesNames .= $filesName . ",";
                                    $this->movePublicDocFileFromTempToContact($filesName, $user_id);
                                }
                            }
                            if ($allFilesNames != '') {
                                $allFilesNames = rtrim($allFilesNames, ",");
                                $filterParam['user_id'] = $user_id;
                                $filterParam['publicdocs_upload'] = $allFilesNames;
                                $this->getContactTable()->insertDocument($filterParam);
                            }
                        }
                    }

                    /** end insert into the change log */
                    $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['create_contact']['UPDATED_CONTACT_SUCCESS']);
                    $this->flashMessenger()->addMessage($this->_config['user_messages']['config']['create_contact']['UPDATED_CONTACT_SUCCESS']);
                } else {
                    $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['create_contact']['CREATE_CONTACT_FAIL']);
                }

                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $viewModel->setVariables(array(
            'form' => $form_individual,
            'user_type' => $user_detail['user_type'],
            'form_cororporate_foundation' => $form_cororporate_foundation,
            'jsLangTranslate' => array_merge($this->_config['user_messages']['config']['create_contact'], $applicationConfigIds),
            // 'user_list' => $get_contact_arr,
            'profile_image' => $user_detail['profile_image'],
            'user_phone_info' => $user_phone_info,
            'user_address_info' => $user_address_info,
            'user_education' => $user_education,
            'user_notes' => $user_notes,
            'user_id' => $this->encrypt($user_id),
            'userId' => $this->auth->getIdentity()->crm_user_id,
            'mode' => $this->encrypt('edit'),
            'moduleName' => $this->encrypt('user'),
            'user_afhic' => $user_afhic,
            'user_website' => $user_website,
            'docFiles' => $docFiles,
            'uploadedFilePath' => $uploadedFilePath,
            'assetsUploadDir' => $assets_upload_dir,
            'phoneTypeCorporate' => $phoneTypeCorporate,
            'phoneTypeIndividual' => $phoneTypeIndividual,
            'user_tag' => $user_tag_list,
            'addPrivateNotePermission' => $addPrivateNotePermission,
            'viewPrivateNotePermission' => $viewPrivateNotePermission,
            'ethnicityDesc' => $ethnicityDesc,
            'education_desc' => $education_desc,
            'nationalityDesc' => $nationalityDesc,
            'location_typeDesc' => $location_typeDesc,
            'tableName' => 'tbl_usr_notes',
            'tableField' => 'note_id'
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to get save search contact
     * @author Icreon Tech-DG
     * @return JSON
     */
    public function getSavedSearchAction() {
        $this->checkUserAuthentication();
        $this->getContactTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam = array();
            $dataParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
            $dataParam['is_active'] = '1';
            $dataParam['contact_search_id'] = '';

            $crmSearchArray = $this->getContactTable()->getSavedSearch($dataParam);
            $options = '<option value="">Select</option>';
            foreach ($crmSearchArray as $key => $val) {
                $options .="<option value='" . $val['contact_search_id'] . "'>" . $val['title'] . "</option>";
            }
            return $response->setContent($options);
        }
    }

    /**
     * This Action is used to save search contact
     * @author Icreon Tech-DG
     * @return Contact Save Search (JSON)
     */
    public function contactSaveSearchAction() {
        $this->checkUserAuthentication();
        $this->getContactTable();
        $this->getConfig();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $contact = new Contact($this->_adapter);
        if ($request->isPost()) {
            parse_str($request->getPost('searchString'), $searchParam);
            $search_name = $request->getPost('search_name');
            $messages = array();
            parse_str($request->getPost('searchString'), $searchParam);
            $searchParam['search_title'] = $search_name;
            //$searchParam = $contact->getInputFilterContactSearch($searchParam);
            $search_name = $contact->getInputFilterContactSearch($search_name);
            if (trim($search_name) != '') {
                $saveSearchParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $saveSearchParam['search_name'] = $search_name;
                $saveSearchParam['search_query'] = serialize($searchParam);
                if ($request->getPost('search_id') != '') {
                    $saveSearchParam['modified_date'] = DATE_TIME_FORMAT;
                    $saveSearchParam['search_id'] = $request->getPost('search_id');
                    if ($this->getContactTable()->updateContactSearch($saveSearchParam) == true) {
                        $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['search_contact']['SEARCH_UPDATED']);
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                } else {
                    $saveSearchParam['added_date'] = DATE_TIME_FORMAT;
                    if ($this->getContactTable()->saveContactSearch($saveSearchParam) == true) {
                        $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['search_contact']['SEARCH_SAVED']);
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                }
            }
        }
    }

    /**
     * This Action is used to getSavedSearchParam
     * @author Icreon Tech-DG
     * @return Save Search Param (JSON)
     */
    public function getSavedSearchParamAction() {
        $this->checkUserAuthentication();
        $this->getContactTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            if ($request->getPost('search_id') != '') {
                $dataParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                $dataParam['is_active'] = '1';
                $dataParam['contact_search_id'] = $request->getPost('search_id');
                $seachResult = $this->getContactTable()->getSavedSearch($dataParam);
                $seachResult = json_encode(unserialize($seachResult[0]['search_query']));
                return $response->setContent($seachResult);
            }
        }
    }

    /**
     * This Action is used to delete search 
     * @author Icreon Tech-DG
     * @return Delete Search (JSON)
     */
    public function deleteSearchAction() {
        $this->checkUserAuthentication();
        $this->getContactTable();
        $this->getConfig();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam['search_id'] = $request->getPost('search_id');
            $dataParam['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
            $dataParam['is_delete'] = 1;
            $dataParam['modified_date'] = DATE_TIME_FORMAT;
            $this->getContactTable()->deleteSearch($dataParam);
            $this->flashMessenger()->addMessage($this->_config['user_messages']['config']['search_contact']['RECORD_DELETED']);
            $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['search_contact']['RECORD_DELETED']);
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This Action is used to delete contact
     * @author Icreon Tech-DG
     * @return JSON
     */
    public function deleteContactAction() {
        $this->checkUserAuthentication();
        $this->getContactTable();
        $this->getConfig();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam['conatact_id'] = $request->getPost('conatact_id');
            $this->getContactTable()->deleteContact($dataParam);
            $this->flashMessenger()->addMessage($this->_config['user_messages']['config']['search_contact']['CONTACT_RECORD_DELETED']);
            $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['search_contact']['CONTACT_RECORD_DELETED']);
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This Action is used to dispaly phone no form elements .
     * @author Icreon Tech-AP
     * @return Phone Form
     */
    public function phoneNoAction() {
        $this->checkUserAuthentication();
        $form_phone = new ContactPhonenoForm();
        $this->getConfig();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $data['counter'] = isset($params['counter']) ? $params['counter'] : '';
        $data['container'] = isset($params['container']) ? $params['container'] : '';
        $data['divid'] = isset($params['divid']) ? $params['divid'] : '';

        $phoneTypeIndividual = array('' => 'Select', 'home' => 'Home', 'cell' => 'Mobile', 'work' => 'Work', 'fax' => 'Fax', 'others' => 'Others');
        $form_phone->get('phone_type[]')->setAttribute('options', $phoneTypeIndividual);
        $form_phone->get('continfo_primary[]')->setAttribute('onclick', 'checkPrimary(' . $data['counter'] . ')');
        $form_phone->get('continfo_primary[]')->setAttribute('id', 'continfo_primary' . $data['counter'] . '');

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);

        $viewModel->setVariables(array(
            'form_phone' => $form_phone,
            'jsLangTranslate' => $this->_config['user_messages']['config']['create_contact'],
            'params' => $data,
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to dispaly phone no form elements .
     * @author Icreon Tech-AP
     * @return Phone Form
     */
    public function corporatePhoneNoAction() {
        $this->checkUserAuthentication();
        $form_phone = new CorporatePhonenoForm();
        $this->getConfig();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $data['counter'] = isset($params['counter']) ? $params['counter'] : '';
        $data['container'] = isset($params['container']) ? $params['container'] : '';
        $data['divid'] = isset($params['divid']) ? $params['divid'] : '';
        $phoneTypeCorporate = array('' => 'Select', 'main' => 'Main', 'direct' => 'Direct', 'fax' => 'Fax', 'others' => 'Others');
        $form_phone->get('corporate_phone_type[]')->setAttribute('options', $phoneTypeCorporate);
        $form_phone->get('corporate_continfo_primary[]')->setAttribute('onclick', 'corporateCheckPrimary(' . $data['counter'] . ')');
        $form_phone->get('corporate_continfo_primary[]')->setAttribute('id', 'corporate_continfo_primary' . $data['counter'] . '');
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);

        $viewModel->setVariables(array(
            'form_phone' => $form_phone,
            'jsLangTranslate' => $this->_config['user_messages']['config']['create_contact'],
            'params' => $data,
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to dispaly afihc form elements .
     * @author Icreon Tech-DG
     * @return afihc form
     */
    public function afihcAction() {
        //$this->checkUserAuthenticationAjx();
        $form_afihc = new ContactAfihcForm();
        $this->getConfig();
        $params = $this->params()->fromRoute();
        $data['counter'] = $params['counter'];
        $data['container'] = $params['container'];
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'form_afihc' => $form_afihc,
            'jsLangTranslate' => $this->_config['user_messages']['config']['create_contact'],
            'params' => $data
                )
        );

        return $viewModel;
    }

    /**
     * This Action is used to dispaly phone no form elements .
     * @author Icreon Tech-AP
     * @return address form
     */
    public function addressFormAction() {
        $this->checkUserAuthentication();
        $form_address = new ContactAddressesForm();
        $this->getConfig();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $data['counter'] = isset($params['counter']) ? $params['counter'] : '';
        $data['container'] = isset($params['container']) ? $params['container'] : '';

        $fieldName = 'primary_location' . $data['counter'];
        $form_address->get('primary_location0')->setName($fieldName);
        $form_address->get('primary_location0')->setAttribute('id', $fieldName);
        
        $get_title = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTitle();
        $user_title = array();
        $user_title[''] = 'None';
        foreach ($get_title as $key => $val) {
            $user_title[$val['title']] = $val['title'];
        }
        $form_address->get('addressinfo_title[]')->setAttribute('options', $user_title);

        $form_address->get('addressinfo_location_primary[]')->setAttribute('id', 'addressinfo_location_primary' . $data['counter']);
        $form_address->get('addressinfo_location_billing[]')->setAttribute('id', 'addressinfo_location_billing' . $data['counter']);
        $form_address->get('addressinfo_location_shipping[]')->setAttribute('id', 'addressinfo_location_shipping' . $data['counter']);

        $form_address->get('addressinfo_location_primary[]')->setAttribute('onclick', 'addressTypePrimary(this.id,' . $data['counter'] . ');');
        $form_address->get('addressinfo_location_billing[]')->setAttribute('onchange', 'addressTypeBilling(this.id,' . $data['counter'] . ');');
        $form_address->get('addressinfo_location_shipping[]')->setAttribute('onchange', 'addressTypeShipping(this.id,' . $data['counter'] . ');enableApoPo(' . $data['counter'] . ',\'\');');
        $form_address->get('addressinfo_country[]')->setAttribute('id', $data['counter']);
        $form_address->get('is_apo_po[]')->setAttribute('id', 'is_apo_po' . $data['counter'])->setAttribute('onChange', 'setApoPostateCountry(' . $data['counter'] . ',this.value);');

        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
        $country_list = array();
        $country_list[''] = 'Select Country';
        foreach ($country as $key => $val) {
            $country_list[$val['country_id']] = $val['name'];
        }
        $form_address->get('addressinfo_country[]')->setAttribute('options', $country_list);
        $get_location_type = $this->getServiceLocator()->get('Common\Model\CommonTable')->getLocationType('');
        $location_type = array();
        $location_typeDesc = array();
        $location_type[''] = 'Select';
        foreach ($get_location_type as $key => $val) {
            $location_type[$val['location_id']] = $val['location'];
            $location_typeDesc[$val['location_id']] = $val['description'];
        }
        $form_address->get('addressinfo_location_type[]')->setAttribute('options', $location_type);

        $state = $this->getServiceLocator()->get('Common\Model\CommonTable')->getStates();
        $stateList = array();
        $stateList[''] = 'Select State';
        foreach ($state as $key => $val) {
            $stateList[$val['state_code']] = $val['state'] . " - " . $val['state_code'];
        }
        $form_address->get('address_state_id[]')->setAttribute('options', $stateList);
        $form_address->get('address_state_id[]')->setAttribute('id', 'address_state_id_' . $data['counter']);
        $form_address->get('addressinfo_state[]')->setAttribute('id', 'addressinfo_state_' . $data['counter']);

        $apoPoStates = $this->_config['transaction_config']['apo_po_state'];
        $apoPoStatesList[''] = 'Select State';

        foreach ($apoPoStates as $k => $v) {
            $apoPoStatesList[$k] = $v;
        }
        $form_address->get('address_state_apo[]')->setAttribute('options', $apoPoStatesList)->setAttribute('id', 'address_state_apo_' . $data['counter']);

        $form_address->get('address_country_id[]')->setAttribute('options', array(228 => 'UNITED STATES'))->setAttribute('id', 'address_country_id_' . $data['counter']);

        $addressArr = array();
        if (isset($request->getPost()->addressArr) && count($request->getPost()->addressArr) > 0) {
            $addressArr = $request->getPost()->addressArr;
        }

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'addressData' => $addressArr,
            'form_address' => $form_address,
            'jsLangTranslate' => $this->_config['user_messages']['config']['create_contact'],
            'params' => $data,
            'location_typeDesc' => $location_typeDesc
                )
        );

        return $viewModel;
    }

    /**
     * This Action is used to dispaly phone no form elements .
     * @author Icreon Tech-AP
     * @return address form
     */
    public function getContactInfoAction() {
        $this->checkUserAuthentication();
        $form_address = new ContactAddressesForm();
        $this->getConfig();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $data['counter'] = isset($params['counter']) ? $params['counter'] : '';
        $data['container'] = isset($params['container']) ? $params['container'] : '';
        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
        $country_list = array();
        $country_list[''] = 'Select Country';
        foreach ($country as $key => $val) {
            $country_list[$val['country_id']] = $val['name'];
        }
        $form_address->get('addressinfo_country[]')->setAttribute('options', $country_list);
        $addressArr = array();
        if (isset($request->getPost()->addressArr) && count($request->getPost()->addressArr) > 0) {
            $addressArr = $request->getPost()->addressArr;
        }

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'addressData' => $addressArr,
            'form_address' => $form_address,
            'jsLangTranslate' => $this->_config['user_messages']['config']['create_contact'],
            'params' => $data,
                )
        );

        return $viewModel;
    }

    /**
     * This Action is used to dispaly corporate address form
     * @author Icreon Tech-AP
     * @return corporate address form
     */
    public function corporateAddressFormAction() {
        $this->checkUserAuthentication();
        $corporate_form_address = new CorporateContactAddressesForm();
        $this->getConfig();
        $params = $this->params()->fromRoute();
        $data['counter'] = $params['counter'];
        $data['container'] = $params['container'];

        $get_title = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTitle();
        $user_title = array();
        $user_title[''] = 'None';
        foreach ($get_title as $key => $val) {
            $user_title[$val['title']] = $val['title'];
        }
        $corporate_form_address->get('corporate_addressinfo_title[]')->setAttribute('options', $user_title);        
        
        $fieldName = 'corporate_primary_location' . $data['counter'];
        $corporate_form_address->get('corporate_primary_location0')->setName($fieldName);
        $corporate_form_address->get('corporate_primary_location0')->setAttribute('id', $fieldName);

        $corporate_form_address->get('corporate_addressinfo_location_primary[]')->setAttribute('id', 'corporate_addressinfo_location_primary' . $data['counter']);
        $corporate_form_address->get('corporate_addressinfo_location_billing[]')->setAttribute('id', 'corporate_addressinfo_location_billing' . $data['counter']);
        $corporate_form_address->get('corporate_addressinfo_location_shipping[]')->setAttribute('id', 'corporate_addressinfo_location_shipping' . $data['counter']);

        $corporate_form_address->get('corporate_addressinfo_location_primary[]')->setAttribute('onclick', 'corporateAddressTypePrimary(this.id,' . $data['counter'] . ');');
        $corporate_form_address->get('corporate_addressinfo_location_billing[]')->setAttribute('onchange', 'corporateAddressTypeBilling(this.id,' . $data['counter'] . ');');
        $corporate_form_address->get('corporate_addressinfo_location_shipping[]')->setAttribute('onclick', 'corporateAddressTypeShipping(this.id,' . $data['counter'] . ');corporateEnableApoPo(' . $data['counter'] . ',\'\');');

        $corporate_form_address->get('corporate_is_apo_po[]')->setAttribute('id', 'corporate_is_apo_po' . $data['counter'])->setAttribute('onChange', 'corporateSetApoPostateCountry(' . $data['counter'] . ',this.value);');

        $get_location_type = $this->getServiceLocator()->get('Common\Model\CommonTable')->getLocationType('');
        $location_type = array();
        $location_typeDesc = array();
        $location_type[''] = 'Select';
        foreach ($get_location_type as $key => $val) {
            $location_type[$val['location_id']] = $val['location'];
            $location_typeDesc[$val['location_id']] = $val['description'];
        }
        $corporate_form_address->get('corporate_addressinfo_location_type[]')->setAttribute('options', $location_type);

        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
        $country_list = array();
        $country_list[''] = 'Select Country';
        foreach ($country as $key => $val) {
            $country_list[$val['country_id']] = $val['name'];
        }
        $corporate_form_address->get('corporate_addressinfo_country[]')->setAttribute('options', $country_list);
        $corporate_form_address->get('corporate_addressinfo_country[]')->setAttribute('id', $data['counter']);

        $state = $this->getServiceLocator()->get('Common\Model\CommonTable')->getStates();
        $stateList = array();
        $stateList[''] = 'Select State';
        foreach ($state as $key => $val) {
            $stateList[$val['state_code']] = $val['state'] . " - " . $val['state_code'];
        }
        $corporate_form_address->get('corporate_address_state_id[]')->setAttribute('options', $stateList);
        $corporate_form_address->get('corporate_address_state_id[]')->setAttribute('id', 'corporate_address_state_id_' . $data['counter']);
        $corporate_form_address->get('corporate_addressinfo_state[]')->setAttribute('id', 'corporate_addressinfo_state_' . $data['counter']);

        $apoPoStates = $this->_config['transaction_config']['apo_po_state'];
        $apoPoStatesList[''] = 'Select State';

        foreach ($apoPoStates as $k => $v) {
            $apoPoStatesList[$k] = $v;
        }
        $corporate_form_address->get('corporate_address_state_apo[]')->setAttribute('options', $apoPoStatesList)->setAttribute('id', 'corporate_address_state_apo_' . $data['counter']);

        $corporate_form_address->get('corporate_address_country_id[]')->setAttribute('options', array(228 => 'UNITED STATES'))->setAttribute('id', 'corporate_address_country_id_' . $data['counter']);

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'corporate_form_address' => $corporate_form_address,
            'jsLangTranslate' => $this->_config['user_messages']['config']['create_contact'],
            'params' => $data,
            'location_typeDesc' => $location_typeDesc
                )
        );

        return $viewModel;
    }

    /**
     * This Action is used to dispaly education form elements .
     * @author Icreon Tech-AP
     * @return Education Form
     */
    public function educationAction() {
        $this->checkUserAuthentication();
        $form_education = new ContactEducationForm();
        $this->getConfig();
        $params = $this->params()->fromRoute();
        $data['counter'] = $params['counter'];
        $data['container'] = $params['container'];
        $get_education_list = $this->getServiceLocator()->get('Common\Model\CommonTable')->getEducationDegreeList('');
        $education_list = array();
        $education_desc = array();
        $education_list[''] = 'Select';
        foreach ($get_education_list as $key => $val) {
            $education_list[$val['education_id']] = $val['education'];
            $education_desc[$val['education_id']] = $val['description'];
        }
        $form_education->get('edu_degree_level[]')->setAttribute('options', $education_list);
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'form_education' => $form_education,
            'jsLangTranslate' => $this->_config['user_messages']['config']['create_contact'],
            'params' => $data,
            'education_desc' => $education_desc
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to file upload form elements .
     * @author Icreon Tech-AP
     * @return upload file form
     */
    public function uploadFileAction() {
        $this->checkUserAuthentication();
        $form_upload_file = new ContactUploadForm();
        $this->getConfig();
        $params = $this->params()->fromRoute();
        $data['counter'] = $params['counter'];
        $data['container'] = $params['container'];
        $fieldName = 'publicdocs_upload' . $data['counter'];
        $fieldFileName = 'publicdocs_upload_file' . $data['counter'];

        $form_upload_file->get('publicdocs_upload_count')->setValue($data['counter']);
        $form_upload_file->get('publicdocs_upload0')->setName($fieldName);
        $form_upload_file->get('publicdocs_upload0')->setAttribute('id', $fieldName);

        $form_upload_file->get('publicdocs_upload_file0')->setName($fieldFileName);
        $form_upload_file->get('publicdocs_upload_file0')->setAttribute('id', $fieldFileName);

        if (isset($params['flag'])) {
            $data['flag'] = $params['flag'];
        }
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'form_upload_form' => $form_upload_file,
            'jsLangTranslate' => $this->_config['user_messages']['config']['create_contact'],
            'params' => $data,
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to file upload form elements .
     * @author Icreon Tech-AP
     * @return upload file form
     */
    public function uploadFileCorporateAction() {
        $this->checkUserAuthentication();
        $form_upload_file = new CorporateUploadForm();
        $this->getConfig();
        $params = $this->params()->fromRoute();
        $data['counter'] = $params['counter'];
        $data['container'] = $params['container'];
        $fieldName = 'cor_publicdocs_upload' . $data['counter'];
        $fieldFileName = 'cor_publicdocs_upload_file' . $data['counter'];

        $form_upload_file->get('cor_publicdocs_upload_count')->setValue($data['counter']);
        $form_upload_file->get('cor_publicdocs_upload0')->setName($fieldName);
        $form_upload_file->get('cor_publicdocs_upload0')->setAttribute('id', $fieldName);

        $form_upload_file->get('cor_publicdocs_upload_file0')->setName($fieldFileName);
        $form_upload_file->get('cor_publicdocs_upload_file0')->setAttribute('id', $fieldFileName);

        if (isset($params['flag'])) {
            $data['flag'] = $params['flag'];
        }
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'form_upload_form' => $form_upload_file,
            'jsLangTranslate' => $this->_config['user_messages']['config']['create_contact'],
            'params' => $data,
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to website form elements .
     * @author Icreon Tech-AP
     * @return website Form
     */
    public function websiteAction() {
        $this->checkUserAuthentication();
        $form_website = new ContactWebsiteForm();
        $this->getConfig();
        $params = $this->params()->fromRoute();
        $data['counter'] = $params['counter'];
        $data['container'] = $params['container'];
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'form_website' => $form_website,
            'jsLangTranslate' => $this->_config['user_messages']['config']['create_contact'],
            'params' => $data
                )
        );

        return $viewModel;
    }

    /**
     * This Action is used to see detail page of user/contact
     * @author Icreon Tech-DG
     * @return Array
     */
    public function viewContactAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        if ($this->auth->hasIdentity() === false) {
            return $this->redirect()->toRoute('crm');
        }
        $params = $this->params()->fromRoute();
        $this->getConfig();
        $this->getContactTable();

        $response = $this->getResponse();
        $messages = array();

        $request = $this->getRequest();
        $viewModel = new ViewModel();
		$source = "";
        if (isset($params['id']) && $params['id'] != '') {
            $user_id = $this->decrypt($params['id']);
            if (isset($params['mode']))
                $mode = $params['mode'];
            else
                $mode = '';
			if(isset($params['source']) && $params['source'] !=""){
				$source = $this->decrypt($params['source']);
			}
            $transactionCount = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getUserTransactionCount(array('user_id' => $user_id));
            $campaignCount = $this->getServiceLocator()->get('Campaign\Model\CampaignTable')->getUserCampaignCount(array('user_id' => $user_id));
            $membershipCount = $this->getServiceLocator()->get('User\Model\MembershipTable')->getUserMembershipCount(array('user_id' => $user_id, 'not_in_membership' => '1'));
            $caseCount = $this->getServiceLocator()->get('Cases\Model\CasesTable')->getCaseCount(array('user_id' => $user_id));
            $familyCount = $this->getServiceLocator()->get('Passenger\Model\FamilyhistoryTable')->getFamilyCount(array('user_id' => $user_id));
            $leadCount = $this->getServiceLocator()->get('Lead\Model\LeadTable')->getLeadCount(array('user_id' => $user_id));
            $pledgeCount = $this->getServiceLocator()->get('Pledge\Model\PledgeTable')->getPledgeCount(array('user_id' => $user_id));
            $activityArr = array();
            $activityArr['sourceType'] = '';
            $activityArr['activitySourceId'] = '';
            $activityArr['userId'] = $user_id;
            $activityCount = $this->getServiceLocator()->get('Activity\Model\ActivityTable')->getActivityCount($activityArr);

            $userDetail = $this->getContactTable()->getUserDetail(array('user_id' => $user_id));
            if ($userDetail['user_type'] == 1) {
                if ($userDetail['middle_name'] != '') {
                    $contactName = $userDetail['first_name'] . " " . $userDetail['middle_name'] . " " . $userDetail['last_name'];
                } else {
                    $contactName = $userDetail['first_name'] . " " . $userDetail['last_name'];
                }
            } else {
                $contactName = $userDetail['company_name'];
            }
            $viewModel->setVariables(array(
                'jsLangTranslate' => $this->_config['user_messages']['config']['view_user'],
                'user_id' => $this->encrypt($user_id),
                'activityForContact' => $this->encrypt(1),
                'encryptView' => $this->encrypt("view"),
                'mode' => $this->decrypt($mode),
                'transactionCount' => $transactionCount,
                'campaignCount' => $campaignCount,
                'membershipCount' => $membershipCount,
                'caseCount' => $caseCount,
                'activityCount' => $activityCount,
                'familyCount' => $familyCount,
                'moduleName' => $this->encrypt('user'),
                'contactName' => $contactName,
                'leadCount' => $leadCount,
                'pledgeCount' => $pledgeCount,
				'source'=>$source,
                    )
            );

            return $viewModel;
        }
    }

    /**
     * This Action is used to see detail page of user/contact
     * @author Icreon Tech-DG
     * @return Array
     */
    public function contactDetailAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $params = $this->params()->fromRoute();
        $this->getConfig();
        $this->getContactTable();
        $response = $this->getResponse();
        $messages = array();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        if (isset($params['id']) && $params['id'] != '') {
            $user_id = $this->decrypt($params['id']);
            $folder_struc_by_user = $this->FolderStructByUserId($user_id);
            $uploadedFilePath = $this->_config['file_upload_path']['assets_url'] . 'user/' . $folder_struc_by_user;
            $assets_upload_dir = $this->_config['file_upload_path']['assets_upload_dir'] . 'user/' . $folder_struc_by_user;
            $user_detail = $this->getContactTable()->getUserDetail(array('user_id' => $user_id));
            $alternate_contact = $this->getContactTable()->getUserAlternateContact(array('user_id' => $user_id));
            $user_education = $this->getContactTable()->getUserEducations(array('user_id' => $user_id));
            $user_notes = $this->getContactTable()->getUserNotes(array('user_id' => $user_id));
            $user_document = $this->getContactTable()->getUserDocuments(array('user_id' => $user_id));
            $user_tag = $this->getContactTable()->getUserTags(array('user_id' => $user_id));
            $user_group = $this->getContactTable()->getUserGroups(array('user_id' => $user_id));
            $user_address_info = $this->getContactTable()->getUserAddressInformations(array('user_id' => $user_id));
            $user_phone_info = $this->getContactTable()->getUserPhoneInformations(array('user_id' => $user_id));
            $user_website = $this->getContactTable()->getUserWebsites(array('user_id' => $user_id));
            $user_afhic = $this->getContactTable()->getUserAfihc(array('user_id' => $user_id));
            $user_communication = $this->getContactTable()->getCommunicationPreferences(array('user_id' => $user_id));

            /** view private note* */
            $moduleInfoArr = array();
            $moduleInfoArr[] = 'User';
            $moduleInfoArr[] = 'Contact';
            $moduleInfoArr[] = 'view-contact-private-note';
            $moduleDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getModuleAction($moduleInfoArr);

            $auth = new AuthenticationService();

            $userDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getCrmUserDetails(array('crm_user_id' => $auth->getIdentity()->crm_user_id));

            $roleDetail = $this->getServiceLocator()->get('User\Model\CrmuserTable')->checkCrmUserPermission($userDetail[0]['role_id']);

            $viewPrivateNotePermission = isset($roleDetail['moduleActionPermissions'][$moduleDetail['module_id']][$moduleDetail['action_id']]) ? $roleDetail['moduleActionPermissions'][$moduleDetail['module_id']][$moduleDetail['action_id']] : 1;
            /** end of add private note * */
            $viewModel->setVariables(array(
                'jsLangTranslate' => $this->_config['user_messages']['config']['view_user'],
                'user_detail' => $user_detail,
                'alternate_contact' => $alternate_contact,
                'user_education' => $user_education,
                'user_notes' => $user_notes,
                'user_document' => $user_document,
                'user_tag' => $user_tag,
                'user_group' => $user_group,
                'user_address_info' => $user_address_info,
                'user_phone_info' => $user_phone_info,
                'user_communication' => $user_communication,
                'user_website' => $user_website,
                'user_afhic' => $user_afhic,
                'assetsUploadDir' => $assets_upload_dir,
                'uploadedFilePath' => $uploadedFilePath,
                'viewPrivateNotePermission' => $viewPrivateNotePermission,
                'userId' => $this->auth->getIdentity()->crm_user_id,
                'tableName' => 'tbl_usr_notes',
                'tableField' => 'note_id'
                    )
            );

            return $viewModel;
        }
    }

    /**
     * This Action is used to see users saves search
     * @author Icreon Tech-DG
     * @return Array
     */
    public function savedSearchAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $params = $this->params()->fromRoute();
        $this->getConfig();
        $this->getContactTable();

        $response = $this->getResponse();
        $messages = array();

        $request = $this->getRequest();
        $viewModel = new ViewModel();

        if (isset($params['id']) && $params['id'] != '') {
            $user_id = $this->decrypt($params['id']);
            $user_detail = $this->getContactTable()->getUserDetail(array('user_id' => $user_id));
            $alternate_contact = $this->getContactTable()->getUserAlternateContact(array('user_id' => $user_id));
            $user_education = $this->getContactTable()->getUserEducations(array('user_id' => $user_id));
            $user_notes = $this->getContactTable()->getUserNotes(array('user_id' => $user_id));
            $user_document = $this->getContactTable()->getUserDocuments(array('user_id' => $user_id));
            $user_tag = $this->getContactTable()->getUserTags(array('user_id' => $user_id));
            $user_group = $this->getContactTable()->getUserGroups(array('user_id' => $user_id));
            $user_interest = $this->getContactTable()->getUserInterests(array('user_id' => $user_id));
            $user_address_info = $this->getContactTable()->getUserAddressInformations(array('user_id' => $user_id));
            $user_phone_info = $this->getContactTable()->getUserPhoneInformations(array('user_id' => $user_id));
            $user_website = $this->getContactTable()->getUserWebsites(array('user_id' => $user_id));
            $user_afhic = $this->getContactTable()->getUserAfihc(array('user_id' => $user_id));

            $viewModel->setVariables(array(
                'jsLangTranslate' => $this->_config['user_messages']['config']['view_user'],
                'user_detail' => $user_detail,
                'alternate_contact' => $alternate_contact,
                'user_education' => $user_education,
                'user_notes' => $user_notes,
                'user_document' => $user_document,
                'user_tag' => $user_tag,
                'user_group' => $user_group,
                'user_interest' => $user_interest,
                'user_address_info' => $user_address_info,
                'user_phone_info' => $user_phone_info,
                'user_website' => $user_website,
                'user_afhic' => $user_afhic
                    )
            );

            return $viewModel;
        }
    }

    public function createPopupContactAction() {
        $this->getConfig();
        $this->getContactTable();
        $this->checkUserAuthentication();
        $this->layout('popup');
        $contact_popup_form = new ContactPopupForm();
        $request = $this->getRequest();
        $contact = new Contact($this->_adapter);


        $get_title = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTitle();
        $user_title = array();
        $user_title[''] = 'None';
        foreach ($get_title as $key => $val) {
            $user_title[$val['title']] = $val['title'];
        }
        $contact_popup_form->get('title')->setAttribute('options', $user_title);

        $get_suffix = $this->getServiceLocator()->get('Common\Model\CommonTable')->getSuffix();
        $user_suffix = array();
        $user_suffix[''] = 'None';
        foreach ($get_suffix as $key => $val) {
            $user_suffix[$val['suffix']] = $val['suffix'];
        }
        $contact_popup_form->get('suffix')->setAttribute('options', $user_suffix);

        if ($request->isPost()) {
            $response = $this->getResponse();
            $contact_popup_form->setInputFilter($contact->getInputFilterCreatePopupContact());
            $contact_popup_form->setData($request->getPost());

            if (!$contact_popup_form->isValid()) {
                $errors = $contact_popup_form->getMessages();

                $msg = array();

                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'savecontact') {
                        foreach ($row as $rower) {
                            $msg [$key] = $this->_config['user_messages']['config']['create_popup_contact'][$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $form_data = $request->getPost();

                $date = DATE_TIME_FORMAT;
                $form_data['add_date'] = $date;
                $form_data['modified_date'] = $date;

                $filterParam = $contact->filterParamPopup($form_data);

                $user_id = $this->getContactTable()->insertPopupContact($filterParam);
                if ($user_id) {

					/**
					  * insert empty address record
					**/
					$addressVal = array();
					$addressVal['user_id'] = $user_id;
					$addressVal['add_date'] = DATE_TIME_FORMAT;
					$addressVal['modified_date'] = DATE_TIME_FORMAT;
					$this->getContactTable()->insertAddressInfo($addressVal);
					/* end of address entry **/

                    /** insert into the change log */
                    $changeLogArray = array();
                    $changeLogArray['table_name'] = 'tbl_usr_change_logs';
                    $changeLogArray['activity'] = '1';/** 1 == for insert */
                    $changeLogArray['id'] = $user_id;
                    $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
                    $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                    $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                    /** end insert into the change log */
                    $this->getServiceLocator()->get('User\Model\ContactTable')->updateContactId(array('user_id' => $user_id));
                    if ($filterParam['email_id']) {
                        $userDataArr = $this->getUserTable()->getUser(array('user_id' => $user_id));
                        $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                        $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                        //if ($filterParam['no_email_address']) {
                        //$signup_email_id_template_int = 1;
                        //$this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $signup_email_id_template_int, $this->_config['email_options']);
                        //}
                        $userDataArr['genrated_password'] = $filterParam['genrated_password'];
                        $userDataArr['verification_code'] = $filterParam['verify_code'];
                        $signup_email_id_template_int = 29;
                        $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $signup_email_id_template_int, $this->_config['email_options']);
                    }
                    //$this->getServiceLocator()->get('User\Model\UserTable')->insertDefaultContactCommunicationPreferences(array('user_id'=>$user_id));
                    $membershipDetailArr = array();
                    $membershipDetailArr[] = 1;
                    $getMemberShip = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);

                    $userMembershipData = array();
                    $userMembershipData['user_id'] = $user_id;
                    $userMembershipData['membership_id'] = $getMemberShip['membership_id'];
					$userMembershipInfo = $this->getServiceLocator()->get('UserMembership')->getUserMembership($getMemberShip);
					$userMembershipData['membership_date_from'] = $userMembershipInfo['membership_date_from'];
                    $userMembershipData['membership_date_to'] = $userMembershipInfo['membership_date_to'];
                   /* $startDate = date("Y-m-d");
                    if ($getMemberShip['validity_type'] == 1) {
                        $userMembershipData['membership_date_from'] = $startDate;
                        $startDay = $this->_config['financial_year']['srart_day'];
                        $startMonth = $this->_config['financial_year']['srart_month'];
                        $startYear = date("Y");
                        $startDate = $startYear . "-" . $startMonth . "-" . $startDay;
                        $addYear = ($getMemberShip['validity_time'] - date("Y")) + 1;
                        $futureDate = date('Y-m-d', strtotime('+' . $addYear . ' year', strtotime($startDate)));
                        $userMembershipData['membership_date_to'] = $futureDate;
                    } else if ($getMemberShip['validity_type'] == 2) {
                        $userMembershipData['membership_date_from'] = $startDate;
                        $futureDate = date('Y-m-d', strtotime('+' . $getMemberShip['validity_time'] . ' year', strtotime($startDate)));
                        $userMembershipData['membership_date_to'] = $futureDate;
                    }*/
                    $this->getServiceLocator()->get('User\Model\UserTable')->saveUserMembership($userMembershipData);
                    $middlename = (trim($form_data['middle_name']) != '') ? $form_data['middle_name'] . " " : "";
                    $contactName = $form_data['first_name'] . " " . $middlename . $form_data['last_name'];
                    $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['create_popup_contact']['CREATE_CONTACT_SUCCESS'], 'contact_name_id' => $user_id, 'contact_name' => $contactName);
                } else {
                    $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['create_popup_contact']['CREATE_CONTACT_FAIL']);
                }

                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'contact_popup_form' => $contact_popup_form,
            'jsLangTranslate' => $this->_config['user_messages']['config']['create_popup_contact']
        ));
        return $viewModel;
    }

    /**
     * This Action is used to get users searches
     * @author Icreon Tech-DG
     * @return Array
     */
    public function getUserSearchesAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getContactTable();
        $this->getConfig();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $response = $this->getResponse();
        $messages = array();
        $params = $this->params()->fromRoute();
        $user_id = $params['user_id'];

        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $type = $params['type'];
            $messages = array();
            $searchParam = array();
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit;
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['user_id'] = $this->decrypt($user_id);
            $searchParam['type'] = $type;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'save_date') ? 'save_date' : $searchParam['sort_field'];
            if ($type == 'woh') {
                $searchParam['w_user_id'] = $this->decrypt($user_id);
                $searches_arr = $this->getServiceLocator()->get('User\Model\WohTable')->getUserSearches($searchParam);
            } elseif ($type == 'fof') {
                $searches_arr = $this->getServiceLocator()->get('User\Model\FofTable')->getUserSearches($searchParam);
            } 
             elseif ($type == 'passindividual')
            { 
                $searches_arr = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getUserPassengerSearch($searchParam);
            }
            /*get ship records of a user starts*/
            elseif ($type == 'shiprecord')
            {
                $searches_arr = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getUserShipSearch($searchParam);
            }
            /*get ship records of a user ends*/
            /*get manifest records of a user starts*/
            elseif ($type == 'manifest')
            {
                $searches_arr = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getUserManifestSearch($searchParam);
                            
                //echo "<pre>";print_r($searches_arr);die;
            }
            /*get manifest records of a user ends*/
            else {
                $searches_arr = $this->getContactTable()->getUserSearches($searchParam);
            }


            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $jsonResult['page'] = $request->getPost('page');
            $jsonResult['records'] = $countResult[0]->RecordCount;
            $jsonResult['page'] = $request->getPost('page');
            $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);

            if ($type == 'woh') {
                //$searches_arr = $this->getServiceLocator()->get('User\Model\WohTable')->getUserSearches();
                if (!empty($searches_arr)) {
                    foreach ($searches_arr as $val) {
                        $arrCell['id'] = $val['woh_search_id'];
                        $arrCell['cell'] = array($val['title'], $this->OutputDateFormat($val['save_date'], 'dateformatampm'));
                        $subArrCell[] = $arrCell;
                        $jsonResult['rows'] = $subArrCell;
                    }
                }
            } elseif ($type == 'fof') {
                //$searches_arr = $this->getServiceLocator()->get('User\Model\FofTable')->getUserSearches();
                if (!empty($searches_arr)) {
                    foreach ($searches_arr as $val) {
                        $arrCell['id'] = $val['fof_search_id'];
                        $arrCell['cell'] = array($val['title'], $this->OutputDateFormat($val['save_date'], 'dateformatampm'));
                        $subArrCell[] = $arrCell;
                        $jsonResult['rows'] = $subArrCell;
                    }
                }
            } 
            elseif($type == 'passindividual') {
                //$searches_arr = $this->getContactTable()->getUserSearches($searchParam);
                if (!empty($searches_arr)) {
                    foreach ($searches_arr as $val) {
                        $arrCell['id'] = $val['passenger_search_id'];
                        
                        $titleLastName = (isset($val['PRIN_LAST_NAME']) and trim($val['PRIN_LAST_NAME']) != "") ? trim($val['PRIN_LAST_NAME']): "";
                        $titleFirstName = (isset($val['PRIN_FIRST_NAME']) and trim($val['PRIN_FIRST_NAME']) != "") ? trim($val['PRIN_FIRST_NAME']): "";
                        $titleN = "";
                        if($titleLastName != "" and $titleFirstName != "") { $titleN = $titleLastName.', '.$titleFirstName; }
                        else if($titleLastName != "" and $titleFirstName == "") { $titleN = $titleLastName; }
                        else if($titleLastName == "" and $titleFirstName != "") { $titleN = $titleFirstName; }
                        
                        if($titleN != "") { $titleN .= ' ('.$val['passenger_id'].')'; }
                        else { $titleN = $val['passenger_id']; }
                        $arrCell['cell'] = array($titleN, $this->OutputDateFormat($val['modified_date'], 'dateformatampm'));
                        $subArrCell[] = $arrCell;
                        $jsonResult['rows'] = $subArrCell;
                    }
                }
            }
            /*manage rows of record for ship records of a user starts*/
            elseif ($type == 'shiprecord')
            {
                if (!empty($searches_arr))
                {
                    foreach ($searches_arr as $val)
                    {
                        $arrCell['id'] = $val['ship_search_id'];

                        $shipname = $val['SHIPNAME'];

                        $arrCell['cell'] = array($shipname, $this->OutputDateFormat($val['modified_date'], 'dateformatampm'));
                        $subArrCell[] = $arrCell;
                        $jsonResult['rows'] = $subArrCell;
                    }
                }
            }
            /*manage rows of record for ship records of a user ends*/
            /*manage rows of record for manifest records of a user starts*/
            elseif($type == 'manifest') {
                //$searches_arr = $this->getContactTable()->getUserSearches($searchParam);
                if (!empty($searches_arr)) {
                    foreach ($searches_arr as $val) {
                         $arrCell['id'] = $val['manifest_search_id'];

                        $title = $val['title'];
                        if(isset($val['file_name']))
                        {
                            $title .=" / ".$val['file_name'];
                        }

                        $arrCell['cell'] = array($title, $this->OutputDateFormat($val['modified_date'], 'dateformatampm'));
                        $subArrCell[] = $arrCell;
                        $jsonResult['rows'] = $subArrCell;
                    }
                }
            }
            /*manage rows of record for manifest records of a user ends*/
            else {
                //$searches_arr = $this->getContactTable()->getUserSearches($searchParam);
                if (!empty($searches_arr)) {
                    foreach ($searches_arr as $val) {
                        $arrCell['id'] = $val['search_id'];
                        $arrCell['cell'] = array($val['title'], $this->OutputDateFormat($val['save_date'], 'dateformatampm'));
                        $subArrCell[] = $arrCell;
                        $jsonResult['rows'] = $subArrCell;
                    }
                }
            }

            $response->setContent(\Zend\Json\Json::encode($jsonResult));
            return $response;
        } else {
            $viewModel->setVariables(array(
                'user_id' => $user_id,
                'jsLangTranslate' => $this->_config['user_messages']['config']['user_searches'])
            );
            return $viewModel;
        }
    }

    /**
     * This Action is used to get users get-user-visitation
     * @author Icreon Tech-DG
     * @return Array
     */
    public function getUserVisitationAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getContactTable();
        $this->getConfig();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $response = $this->getResponse();
        $messages = array();
        $params = $this->params()->fromRoute();
        $user_id = $params['user_id'];

        if ($request->isXmlHttpRequest() && $request->isPost()) {

            $messages = array();
            $searchParam = array();
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit;
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['user_id'] = $this->decrypt($user_id);

            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'save_date') ? 'save_date' : $searchParam['sort_field'];

            $searches_arr = $this->getContactTable()->getUserVisitation($searchParam);
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();

            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $jsonResult['page'] = $request->getPost('page');
            $jsonResult['records'] = $countResult[0]->RecordCount;
            $jsonResult['page'] = $request->getPost('page');
            $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
            if (!empty($searches_arr)) {
                foreach ($searches_arr as $val) {
                    $arrCell['id'] = $val['user_visitation_id'];
                    $session_length = strtotime($val['visitation_to_date']) - strtotime($val['visitation_from_date']);
                    $arrCell['cell'] = array($this->OutputDateFormat($val['visitation_to_date'], 'dateformatampm'), $session_length);
                    $subArrCell[] = $arrCell;
                    $jsonResult['rows'] = $subArrCell;
                }
            }
            $response->setContent(\Zend\Json\Json::encode($jsonResult));
            return $response;
        } else {
            $viewModel->setVariables(array(
                'user_id' => $user_id,
                'jsLangTranslate' => $this->_config['user_messages']['config']['user_visitation'])
            );
            return $viewModel;
        }
    }

    /**
     * This action is used to move file from temp to user folder
     * @return json
     * @param 1 for add , 2 for edit
     * @author Icreon Tech - DG
     */
    public function moveFileFromTempToContact($filename, $user_id) {
        $this->getConfig();
        $temp_dir = $this->_config['file_upload_path']['temp_upload_dir'];
        $temp_thumbnail_dir = $this->_config['file_upload_path']['temp_upload_thumbnail_dir'];
        $temp_medium_dir = $this->_config['file_upload_path']['temp_upload_medium_dir'];
        $user_dir = $this->_config['file_upload_path']['assets_upload_dir'] . "/user/";
        $user_image = 'image/';
        $user_img_thumb = 'thumbnail/';
        $user_img_medium = 'medium/';
        $folder_struc_by_user = $this->FolderStructByUserId($user_id);
        $user_image_dir = $user_dir . $folder_struc_by_user . $user_image;
        $user_thumb_image_dir = $user_dir . $folder_struc_by_user . $user_image . $user_img_thumb;
        $user_medium_image_dir = $user_dir . $folder_struc_by_user . $user_image . $user_img_medium;
        if (!is_dir($user_image_dir)) {
            mkdir($user_image_dir, 0777, TRUE);
        }
        if (!is_dir($user_thumb_image_dir)) {
            mkdir($user_thumb_image_dir, 0777, TRUE);
        }
        if (!is_dir($user_medium_image_dir)) {
            mkdir($user_medium_image_dir, 0777, TRUE);
        }
        $temp_file = $temp_dir . $filename;
        $temp_thumbnail_file = $temp_thumbnail_dir . $filename;
        $temp_medium_file = $temp_medium_dir . $filename;
        $user_filename = $user_image_dir . $filename;
        $user_thumbnail_filename = $user_thumb_image_dir . $filename;
        $user_medium_filename = $user_medium_image_dir . $filename;

        if (file_exists($temp_file)) {
            if (copy($temp_file, $user_filename)) {
                unlink($temp_file);
            }
        }
        if (file_exists($temp_thumbnail_file)) {
            if (copy($temp_thumbnail_file, $user_thumbnail_filename)) {
                unlink($temp_thumbnail_file);
            }
        }
        if (file_exists($temp_medium_file)) {
            if (copy($temp_medium_file, $user_medium_filename)) {
                unlink($temp_medium_file);
            }
        }
    }

    /**
     * This action is used to move file from temp to user folder
     * @return json
     * @param 1 for add , 2 for edit
     * @author Icreon Tech - DG
     */
    public function movePublicDocFileFromTempToContact($filename, $user_id) {
        $this->getConfig();
        $temp_dir = $this->_config['file_upload_path']['temp_upload_dir'];
        $user_dir = $this->_config['file_upload_path']['assets_upload_dir'] . "user/";
        $user_doc = 'public_doc/';
        $folder_struc_by_user = $this->FolderStructByUserId($user_id);
        $user_doc_dir = $user_dir . $folder_struc_by_user . $user_doc;
        if (!is_dir($user_doc_dir)) {
            mkdir($user_doc_dir, 0777, TRUE);
        }
        $temp_file = $temp_dir . $filename;
        $user_filename = $user_doc_dir . $filename;

        if (file_exists($temp_file)) {
            if (copy($temp_file, $user_filename)) {
                unlink($temp_file);
            }
        }
    }

    /**
     * upload user public doc
     * @return JSON
     * @author Icreon Tech - DG
     */
    public function uploadUserPublicDocAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $file_input_names = array_keys($_FILES);
        $file_input_name = $file_input_names[0];
        $image_name_param = $file_input_name;
        $fileExt = $this->GetFileExt($_FILES[$image_name_param]['name']);
        $filename = $this->GenerateMediaFileName($fileExt[1]);    //Generate file name
        $_FILES[$image_name_param]['name'] = $filename;                 // assign name to file variable

        $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration
        $error_messages = array(
            'post_max_size' => 'The uploaded file exceeds the post_max_size directive in php.ini', // Set error messages
            'max_file_size' => 'File is too big',
            'min_file_size' => 'File is too small',
            'accept_file_types' => 'Filetype not allowed',
        );
        $options = array(
            'upload_dir' => $upload_file_path['temp_upload_dir'],
            'param_name' => $image_name_param, //file input name                      // Set configuration
            'inline_file_types' => $upload_file_path['doc_file_types_allowed'],
            'accept_file_types' => $upload_file_path['doc_file_types_allowed'],
            'max_file_size' => $upload_file_path['max_allowed_file_size'],
            'min_file_size' => $upload_file_path['min_allowed_file_size']
        );
        $upload_handler = new UploadHandler($options, true, $error_messages);
        $file_response = $upload_handler->jsonResponceData;

        if (isset($file_response[$image_name_param][0]->error)) {
            $messages = array('status' => "error", 'message' => $file_response[$image_name_param][0]->error);
        } else {
            $messages = array('status' => "success", 'message' => 'File Uploaded', 'filename' => $file_response[$image_name_param][0]->name);
        }
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * delete user public doc
     * @return JSON
     * @author Icreon Tech - DG
     */
    public function deleteContactDocAction() {
        $this->checkUserAuthentication();
        $this->getContactTable();
        $this->getConfig();
        $request = $this->getRequest();
        $response = $this->getResponse();

        if ($request->isPost()) {
            $dataParam['userId'] = $this->decrypt($request->getPost('userId'));
            $dataParam['fileName'] = $request->getPost('fileName');
            $user_document = $this->getContactTable()->getUserDocuments(array('user_id' => $dataParam['userId']));
            $user_dir = $this->_config['file_upload_path']['assets_upload_dir'] . "user/";
            $folder_struc_by_user = $this->FolderStructByUserId($dataParam['userId']);
            $user_doc = 'public_doc/';
            $user_doc_dir = $user_dir . $folder_struc_by_user . $user_doc;
            if (!empty($user_document)) {
                $uploadedFiles = $user_document['uploaded_files'];
                $files = explode(",", $uploadedFiles);
                $newFiles = array();
                foreach ($files as $val) {
                    if ($val == $dataParam['fileName']) {
                        unset($val);
                        $user_filename = $user_doc_dir . $dataParam['fileName'];
                        if (file_exists($user_filename)) {
                            unlink($user_filename);
                        }
                    } else {
                        $newFiles[] = $val;
                    }
                }
                $newFiles = implode(",", $newFiles);
                $dataParam['updatedDoc'] = $newFiles;
            }
            $this->getContactTable()->deleteContactPublicDoc($dataParam);
            $messages = array('status' => "success");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * manage unsubscribe users
     * @return JSON
     * @author Icreon Tech - DG
     */
    public function unsubscribeUsersAction() {
        $this->checkUserAuthentication();
        $this->getContactTable();
        $this->getConfig();
        $this->layout('crm');
        $request = $this->getRequest();
        $response = $this->getResponse();
        $viewModel = new ViewModel();
        $doNotEmail = 2;
        if ($request->isPost()) {
            $fileExt = $this->GetFileExt($_FILES['file_name']['name']);
            if ($fileExt[1] != 'xls') {
                $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['unsubscribe-users']['INVALID_EXCEL_EXTENSION']);
            } else {
                $import_handler = new SpreadsheetExcelReader($_FILES['file_name']['tmp_name']);
                $imported_data = $import_handler->dumpToArray();
                $data_param = array();
                if (!empty($imported_data)) {
                    foreach ($imported_data as $key => $val) {
                        if ($key > 0) {
                            $emailId = $val[1];
                            $usersByEmail = $this->getContactTable()->getCommunicationPreferences(array('email_id' => $emailId));
                            if (!empty($usersByEmail)) {
                                if (isset($usersByEmail['privacy']) && $usersByEmail['privacy'] != '') {
                                    if (strpos($usersByEmail['privacy'], $doNotEmail) === false) {
                                        $updatedPrivacy = $usersByEmail['privacy'] . "," . $doNotEmail;
                                        $this->getContactTable()->updateCommunicationPreferences(array('communicationPreferenceId' => $usersByEmail['communication_preference_id'], 'updatedPrivacy' => $updatedPrivacy));
                                    }
                                } else {
                                    if (isset($usersByEmail['communication_preference_id'])) {
                                        $updatedPrivacy = 2;
                                        $this->getContactTable()->updateCommunicationPreferences(array('communicationPreferenceId' => $usersByEmail['communication_preference_id'], 'updatedPrivacy' => $updatedPrivacy));
                                    }
                                }
                            }
                        }
                    }
                    $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['unsubscribe-users']['UNSUBSCRIBE_SUCCESS']);
                } else {
                    $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['unsubscribe-users']['INVALID_DATA']);
                }
            }
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
        $viewModel->setVariables(array(
            'jsLangTranslate' => $this->_config['user_messages']['config']['unsubscribe-users'],
                )
        );

        return $viewModel;
    }

    public function getContactBonusAction() {
        if ($this->auth->hasIdentity() === false) {
            return $this->redirect()->toRoute('crm');
        }
        $this->getConfig();
        //$this->getTables();
        $viewModel = new ViewModel();
        $viewModel->setTerminal('true');
        $params = $this->params()->fromRoute();
        $historySearchMessages = array_merge($this->_config['PassengerMessages']['config']['crm_family_history'], $this->_config['PassengerMessages']['config']['passenger_search']);
        $request = $this->getRequest();
        $response = $this->getResponse();
        $userFamilyHistoryBonus = new UserFamilyHistoryBonus();
        $userId = $this->decrypt($request->getPost('userid'));
        $userFamilyHistoryBonus->get('user_id')->setValue($userId);
        $userSaveSearchBonus = new UserSaveSearchBonus();
        $userId = $this->decrypt($request->getPost('userid'));
        $userSaveSearchBonus->get('user_id')->setValue($userId);
        $searchParam = array();
        $searchParam['user_id'] = $userId;
        $userFamilyHistoryBonusData = $this->getServiceLocator()->get('Passenger\Model\FamilyhistoryTable')->getUserFamilyHistoryBonus($searchParam);
        if (!empty($userFamilyHistoryBonusData)) {
            $userFamilyHistoryBonus->get('max_stories')->setValue($userFamilyHistoryBonusData[0]['max_stories']);
            $userFamilyHistoryBonus->get('max_story_characters')->setValue($userFamilyHistoryBonusData[0]['max_story_characters']);
            $userFamilyHistoryBonus->get('max_passengers')->setValue($userFamilyHistoryBonusData[0]['max_passengers']);
            $userFamilyHistoryBonus->get('max_saved_histories')->setValue($userFamilyHistoryBonusData[0]['max_saved_histories']);
        }
        /* Get membership detail for contact */

        $searchSaveType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getSearchSaveTypes();
        foreach ($searchSaveType as $index => $data) {
            $searchSaveTypes[$data['saved_search_type_id']] = $data['saved_search_type'];
        }

        $searchParam = array();
        $searchParam['user_id'] = $userId;
        $get_contact_arr = $this->getServiceLocator()->get('User\Model\ContactTable')->getContact($searchParam);
        $membershipId = (!empty($get_contact_arr[0]['membership_id'])) ? $get_contact_arr[0]['membership_id'] : 1;
        $membershipDetailArr = array();
        $membershipDetailArr[] = $membershipId;
        $getMembershipDetail = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
        /* End Get membership detail for contact */
        $viewModel->setVariables(array(
            'jsLangTranslate' => $historySearchMessages,
            'userId' => $this->decrypt($request->getPost('userid')),
            'userFamilyHistoryBonus' => $userFamilyHistoryBonus,
            'userSaveSearchBonus' => $userSaveSearchBonus,
            'membershipDetail' => $getMembershipDetail,
            'userFamilyHistoryBonusData' => $userFamilyHistoryBonusData,
            'searchSaveTypes' => $searchSaveTypes
                )
        );
        return $viewModel;
    }

    public function editContactSaveSearchBonusAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $message = $this->_config['PassengerMessages']['config']['crm_family_history'];
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $form_data = $request->getPost();
            $searchParam = array();
            $searchParam['user_id'] = $form_data['user_id'];
            $get_contact_arr = $this->getServiceLocator()->get('User\Model\ContactTable')->getContact($searchParam);
            $membershipId = (!empty($get_contact_arr[0]['membership_id'])) ? $get_contact_arr[0]['membership_id'] : 1;
            $membershipDetailArr = array();
            $membershipDetailArr[] = $membershipId;
            $getMembershipDetail = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
            $savedSearchInDays = explode(',', $getMembershipDetail['savedSearchInDays']);
            $i = 0;
            foreach ($form_data['bonus_saved_search'] as $key => $value) {
                $saveSearchData['membership_search_id'] = $key;
                $saveSearchData['bonus_saved_search_in_days'] = ($value != '') ? $value : 0;
                $saveSearchData['max_saved_search_in_days'] = (($value != '') ? $value : 0) + $savedSearchInDays[$i];
                $saveSearchData['modified_date'] = DATE_TIME_FORMAT;
                $this->getContactTable()->updateMembershipSaveSearch($saveSearchData);
                $i++;
            }
            $messages = array('status' => "success", 'message' => "Bonus Information Updated Successfully !", 'totalElement' => count($form_data['bonus_saved_search']));
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    public function changeLockStatusAction() {
        $this->checkUserAuthentication();
        $this->getContactTable();
        $this->getConfig();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {

            $dataParam['user_id'] = $this->decrypt($request->getPost('id'));
            $dataParam['status'] = $this->decrypt($request->getPost('type'));
            if ($dataParam['status'] == '0') {
                $dataParam['active'] = '1';
            } else {
                $dataParam['active'] = '0';
            }
            $dataParam['modify_by'] = $this->auth->getIdentity()->crm_user_id;
            $dataParam['modify_date'] = DATE_TIME_FORMAT;
            $this->getContactTable()->updateStatusUpdate($dataParam);
            $messages = array('status' => "success");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

}