<?php

/**
 * This controller is used to display user development
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Controller;

use Base\Model\UploadHandler;
use Base\Controller\BaseController;
use Zend\Authentication\AuthenticationService;
use Zend\View\Model\ViewModel;
use User\Model\Development;
use Zend\Mvc\Controller\Plugin\Params;
use Zend\Http\Request;
use User\Form\DevelopmentForm;
use User\Form\ContactUploadForm;
use User\Form\GiftForm;
use User\Form\JobHistoryForm;
use User\Form\CreateCompanyForm;
use User\Form\DevelopmentUploadForm;
use User\Form\SearchCrmUserCreditForm;
use Base\Model\SpreadsheetExcelReader;

/**
 * This controller is used to display user development
 * @package    User
 * @author     Icreon Tech - DG
 */
class DevelopmentController extends BaseController {

    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_transactionTable = null;
    protected $_developmentTable = null;

    public function __construct() {
        $auth = new AuthenticationService();
        $this->auth = $auth;
    }

    public function indexAction() {
        
    }

    /**
     * This functikon is used to get table connections
     * @return     array 
     * @author Icreon Tech - DG
     */
    public function getTables() {
        $sm = $this->getServiceLocator();
        $this->_developmentTable = $sm->get('User\Model\DevelopmentTable');
        $this->_transactionTable = $sm->get('Transaction\Model\TransactionTable');
        $this->_adapter = $sm->get('dbAdapter');
        return $this->_developmentTable;
    }

    /**
     * This functikon is used to get config variables
     * @return     array 
     * @author Icreon Tech - DG
     */
    public function getConfig() {
        $sm = $this->getServiceLocator();
        $this->_translator = $sm->get('translator');
        $this->_config = $sm->get('Config');
    }

    /**
     * This Action is used to display the user development
     * @author Icreon Tech-DG
     */
    public function getUserDevelopmentAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $this->layout('crm');
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $response = $this->getResponse();
        $request = $this->getRequest();
        $form = new DevelopmentForm();
        $job_history_form = new JobHistoryForm();
        $upload_form = new ContactUploadForm();
        $development_upload_form = new DevelopmentUploadForm();

        $gift_form = new GiftForm();
        $form->add($upload_form);
        $form->add($gift_form);
        $form->add($job_history_form);
        $params = $this->params()->fromRoute();
        $user_id = $params['user_id'];
        $form->get('user_id')->setValue($params['user_id']);
        $development = new Development($this->_adapter);

        $development_user_detail = $this->getTables()->getUserDevelopment(array('user_id' => $this->decrypt($params['user_id'])));

        $docFiles = array();
        if (!empty($development_user_detail) && $development_user_detail['document_files'] != '') {
            $docFiles = explode(",", $development_user_detail['document_files']);
        }
        $getUserJobHistory = $this->getTables()->getUserJobHistory(array('user_id' => $this->decrypt($params['user_id'])));        
        
        $getUserOrgGift = $this->getTables()->getUserOrganisationGift(array('user_id' => $this->decrypt($params['user_id'])));
        $this->assignFormValue($form, $development_user_detail);

        $get_org = $this->getServiceLocator()->get('Common\Model\CommonTable')->getOrganizations();
        $org_list = array();
        $org_list[''] = 'Select';
        foreach ($get_org as $key => $val) {
            $org_list[$val['organization_type_id']] = $val['organization_type'];
        }
        $gift_form->get('organization_type[]')->setAttribute('options', $org_list);

        $get_industry = $this->getServiceLocator()->get('Common\Model\CommonTable')->getIndustry();
        foreach ($get_industry as $key => $val) {
            $industry_list[$val['industry_id']] = $val['industry'];
            $industryListDesc[$val['industry_id']] = $val['industry_description'];
        }
        $job_history_form->get('industry[]')->setAttribute('options', $industry_list);

        $folder_struc_by_user = $this->FolderStructByUserId($this->decrypt($params['user_id']));
        $uploadedFilePath = $this->_config['file_upload_path']['assets_url'] . 'user/' . $folder_struc_by_user;
        if ($request->isPost()) {
            /** insert into the change log */
            $changeLogArray = array();
            $changeLogArray['table_name'] = 'tbl_usr_change_logs';
            $changeLogArray['activity'] = '2';/** 2 == for update */
            $changeLogArray['id'] = $this->decrypt($user_id);
            $changeLogArray['crm_user_id'] = $this->auth->getIdentity()->crm_user_id;
            $changeLogArray['added_date'] = DATE_TIME_FORMAT;
            $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
            /** end insert into the change log */
            $form_data = $request->getPost();
            
            $development_form = $development->getDevelopmentDataArray($form_data);

            $allFilesNames = '';
            for ($count = 0; $count <= $form_data['publicdocs_upload_count']; $count++) {
                $key = 'publicdocs_upload_file' . $count;
                $filesName = $form_data[$key];
                if ($filesName != '') {
                    $allFilesNames .= $filesName . ",";
                    $this->movePrivateDocFileFromTempToDevelopment($filesName, $this->decrypt($form_data['user_id']));
                }
            }
            if ($allFilesNames != '') {
                if (!empty($development_user_detail) && $development_user_detail['document_files'] != '') {
                    $allFilesNames = $allFilesNames . "" . $development_user_detail['document_files'];
                }
                $allFilesNames = rtrim($allFilesNames, ",");
                $development_form['publicdocs_upload'] = $allFilesNames;
            } else {
                if (!empty($development_user_detail) && $development_user_detail['document_files'] != '') {
                    $allFilesNames = $allFilesNames . "" . $development_user_detail['document_files'];
                    $development_form['publicdocs_upload'] = $allFilesNames;
                } else {
                    $development_form['publicdocs_upload'] = '';
                }
            }

            $development_form['added_by'] = $this->auth->getIdentity()->crm_user_id;
            $development_form['user_id'] = $this->decrypt($form_data['user_id']);
            if (!empty($development_user_detail)) {
                $this->getTables()->updateUserDevelopment($development_form);
            } else {
                $this->getTables()->insertUserDevelopment($development_form);
            }

            if (!empty($form_data['year'])) {
                $this->getTables()->deleteUserOrganisationGift(array('user_id' => $this->decrypt($form_data['user_id'])));
                $org_form = array();
                for ($i = 0; $i < count($form_data['year']); $i++) {
                    $org_form[$i]['year'] = isset($form_data['year'][$i]) ? $form_data['year'][$i] : null;
                    $org_form[$i]['organization'] = isset($form_data['organization'][$i]) ? $form_data['organization'][$i] : null;
                    $org_form[$i]['organization_type'] = isset($form_data['organization_type'][$i]) ? $form_data['organization_type'][$i] : null;
                    $org_form[$i]['amount'] = isset($form_data['amount'][$i]) ? $form_data['amount'][$i] : null;
                }
                foreach ($org_form as $val) {
                    $val['user_id'] = $this->decrypt($form_data['user_id']);
                    $val['added_by'] = $this->auth->getIdentity()->crm_user_id;
                    $this->getTables()->insertUserOrganisationGift($val);
                }
            }

            if (!empty($form_data['job_title'])) {
                $this->getTables()->deleteUserJobHistory(array('user_id' => $this->decrypt($form_data['user_id'])));
                $jobhistory_form = array();
                for ($i = 0; $i < count($form_data['job_title']); $i++) {
                    $jobhistory_form[$i]['job_title'] = isset($form_data['job_title'][$i]) ? $form_data['job_title'][$i] : null;
                    $jobhistory_form[$i]['industry'] = isset($form_data['industry'][$i]) ? $form_data['industry'][$i] : null;
                    $jobhistory_form[$i]['period_from'] = isset($form_data['period_from'][$i]) ? $this->DateFormat($form_data['period_from'][$i],'db_datetime_format') : null;
                    $jobhistory_form[$i]['period_to'] = isset($form_data['period_to'][$i]) ? $this->DateFormat($form_data['period_to'][$i],'db_datetime_format') : null;
                    $jobhistory_form[$i]['company_id'] = isset($form_data['company_id'][$i]) ? $form_data['company_id'][$i] : null;
                }
                foreach ($jobhistory_form as $val) {
                    $val['user_id'] = $this->decrypt($form_data['user_id']);
                    $this->getTables()->insertUserJobHistory($val);
                }
            }

            $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['development']['RECORD_UPDATED']);
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
        $viewModel->setVariables(array(
            'form' => $form,
            'userId' => $user_id,
            'docFiles' => $docFiles,
            'development_upload_form' => $development_upload_form,
            'getUserJobHistory' => $getUserJobHistory,
            'getUserOrgGift' => $getUserOrgGift,
            'uploadedFilePath' => $uploadedFilePath,
            'jsLangTranslate' => $this->_config['user_messages']['config']['development'],
            'industryListDesc' => $industryListDesc
                )
        );
        return $viewModel;
    }

    /**
     * This action is used to move file from temp to user folder
     * @return json
     * @param 1 for add , 2 for edit
     * @author Icreon Tech - DG
     */
    public function movePrivateDocFileFromTempToDevelopment($filename, $user_id) {
        $this->getConfig();
        $temp_dir = $this->_config['file_upload_path']['temp_upload_dir'];
        $user_dir = $this->_config['file_upload_path']['assets_upload_dir'] . "/user/";
        $user_doc = 'development_doc/';
        $folder_struc_by_user = $this->FolderStructByUserId($user_id);
        $user_doc_dir = $user_dir . $folder_struc_by_user . $user_doc;
        if (!is_dir($user_doc_dir)) {
            mkdir($user_doc_dir, 0777, TRUE);
        }
        $temp_file = $temp_dir . $filename;
        $user_filename = $user_doc_dir . $filename;

        if (file_exists($temp_file)) {
            if (copy($temp_file, $user_filename)) {
                unlink($temp_file);
            }
        }
    }

    /**
     * This Action is used to importUserDevelopment.
     * @author Icreon Tech-DG
     */
    public function importUserDevelopmentAction() {
        $this->checkUserAuthentication();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $form_data = $request->getPost();
        $this->getConfig();
        $params = $this->params()->fromRoute();
        $user_id = $params['user_id'];
        $excel_sheet_column = $this->_config['user_messages']['config']['excel_sheet_column'];
        $fileExt = $this->GetFileExt($_FILES['file_name']['name']);
        if ($fileExt[1] != 'xls') {
            $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['development']['CHECK_EXCEL']);
        } else {
            $development_user_detail = $this->getTables()->getUserDevelopment(array('user_id' => $this->decrypt($user_id)));
            $import_handler = new SpreadsheetExcelReader($_FILES['file_name']['tmp_name']);
            $imported_data = $import_handler->dumpToArray();
            $data_param = array();
            if (!empty($imported_data)) {
                foreach ($imported_data as $key => $val) {
                    if ($key > 0) {
                        $field_name = $this->makeExcelField($val[1]);
                        if (in_array($field_name, $excel_sheet_column)) {
                            $field = strtolower($field_name);
                            $data_param[$field] = $val[2];
                        }
                    }
                }

                if (!empty($data_param)) {
                    $data_param['user_id'] = $this->decrypt($user_id);
                    $data_param['added_by'] = $this->auth->getIdentity()->crm_user_id;
                    if (!empty($development_user_detail)) {
                        $data_param['count'] = 1;
                    } else {
                        $data_param['count'] = 0;
                    }

                    $this->getTables()->importUserDevelopment($data_param);
                }
            }
            $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['development']['EXCEL_IMPORTED']);
        }
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    public function makeExcelField($val) {
        $field_name = str_replace("'", "", $val);
        $field_name = str_replace("*", "", $field_name);
        $field_name = str_replace(",", "", $field_name);
        $field_name = str_replace(".", "", $field_name);
        $field_name = str_replace("/", " ", $field_name);
        $field_name = str_replace("-", " ", $field_name);
        $field_name = str_replace(" ", "_", $field_name);
        $field_name = str_replace("___", "_", $field_name);
        $field_name = str_replace("__", "_", $field_name);
        return strtolower($field_name);
    }

    /**
     * This Action is used to get gift.
     * @author Icreon Tech-DG
     */
    public function getUserSoleifGiftAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        parse_str($request->getPost('searchString'), $searchParam);
        $params = $this->params()->fromRoute();
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        $searchParam['start_index'] = $start;
        $searchParam['record_limit'] = $limit;
        $searchParam['user_id'] = $this->decrypt($params['user_id']);
        $searchParam['transaction_type'] = array('0' => '1');
        $searchParam['sort_field'] = $request->getPost('sidx');
        $searchParam['sort_order'] = $request->getPost('sord');
        $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'transaction_date') ? 'usr_transaction.transaction_date' : $searchParam['sort_field'];
        //$transaction_arr = $this->getTables()->getUserTransactions($searchParam);
        //$transaction_arr = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getUserTransactions($searchParam);
        $transaction_arr = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getUserGiftToSoleif($searchParam);
        $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();

        $jsonResult = array();
        $arrCell = array();
        $subArrCell = array();
        $jsonResult['page'] = $request->getPost('page');
        $jsonResult['records'] = $countResult[0]->RecordCount;
        $jsonResult['page'] = $request->getPost('page');
        $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);


        if (!empty($transaction_arr)) {
            foreach ($transaction_arr as $val) {
                $arrCell['id'] = $val['user_transaction_id'];
                $encryptedView = $this->encrypt('view');
                $encryptedTransId = $this->encrypt($val['transaction_id']);                
                //$transactionDetail  = "<a class='txt-decoration-underline' href='/get-transaction-detail/".$encryptedTransId."/".$encryptedView."'>".$val['transaction_id']."</a>";
                $transactionDetail = "<a onclick=getUserTransactionDetail('".$encryptedTransId."'); class='txt-decoration-underline' href='javascript:void(0);'>" . $val['transaction_id'] . "</a>";
                $arrCell['cell'] = array($this->OutputDateFormat($val['transaction_date'],'dateformatampm'), $val['product_total'], $transactionDetail, $val['product_name'],$val['campaign_code']);
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
        }
        $response->setContent(\Zend\Json\Json::encode($jsonResult));
        return $response;
    }

    /**
     * This Action is used to assign value in form.
     * @author Icreon Tech-DG
     */
    public function assignFormValue($form, $development_user_detail) {
        if (!empty($development_user_detail)) {
            $form->get('total_given_amount')->setAttribute('value', $development_user_detail['total_giving_amount']);
            $form->get('total_no_of_gifts')->setAttribute('value', $development_user_detail['total_number_of_gifts']);
            $form->get('last_gift_date')->setAttribute('value', $development_user_detail['last_gift_date']);
            $form->get('last_gift_amount')->setAttribute('value', $development_user_detail['last_gift_amount']);
            $form->get('largest_gift_date')->setAttribute('value', $development_user_detail['largest_gift_date']);
            $form->get('largest_gift_amount')->setAttribute('value', $development_user_detail['largest_gift_amount']);
            $form->get('total_income')->setAttribute('value', $development_user_detail['total_income']);
            $form->get('total_real_estate_value')->setAttribute('value', $development_user_detail['total_real_estate_value']);
            $form->get('total_stock_value')->setAttribute('value', $development_user_detail['total_stock_value']);
            $form->get('total_pension_value')->setAttribute('value', $development_user_detail['total_pension_value']);
            $form->get('total_political_non_profit_giving_amount')->setAttribute('value', $development_user_detail['total_political_non_profit_giving_amount']);
            $form->get('income_portion_of_egc_calculation')->setAttribute('value', $development_user_detail['income_portion_of_egc_calculation']);
            $form->get('pol_non_profit_giving_portion_of_egc_calc')->setAttribute('value', $development_user_detail['pol_non_profit_giving_portion_of_egc_calc']);
            $form->get('rating_summary_giving_capacity_rating')->setAttribute('value', $development_user_detail['rating_summary_giving_capacity_rating']);
            $form->get('rating_summary_giving_capacity_range')->setAttribute('value', $development_user_detail['rating_summary_giving_capacity_range']);
            $form->get('estimated_giving_capacity')->setAttribute('value', $development_user_detail['estimated_giving_capacity']);
            $form->get('propensity_to_give_score')->setAttribute('value', $development_user_detail['propensity_to_give_score_part1']);
            $form->get('propensity_to_give_score_part_2')->setAttribute('value', $development_user_detail['propensity_to_give_score_part2']);
            $form->get('combined_propensity_to_give_score')->setAttribute('value', $development_user_detail['combined_propensity_to_give_score']);
            $form->get('planned_giving_bequest')->setAttribute('value', $development_user_detail['planned_giving_bequest']);
            $form->get('planned_giving_annuity')->setAttribute('value', $development_user_detail['planned_giving_annuity']);
            $form->get('planned_giving_trust')->setAttribute('value', $development_user_detail['planned_giving_trust']);
            $form->get('rating_summary_influence')->setAttribute('value', $development_user_detail['rating_summary_influence']);
            $form->get('rating_summary_inclination')->setAttribute('value', $development_user_detail['rating_summary_inclination']);
            $form->get('total_quality_of_match')->setAttribute('value', $development_user_detail['total_quality_of_match']);
            $form->get('charitable_donations_of_qom')->setAttribute('value', $development_user_detail['charitable_donations_of_qom']);
            $form->get('trustees_qom')->setAttribute('value', $development_user_detail['trustees_qom']);
            $form->get('guidestar_directors_qom')->setAttribute('value', $development_user_detail['guidestar_directors_qom']);
            $form->get('guidestar_foundations_qom')->setAttribute('value', $development_user_detail['guidestar_foundations_qom']);
            $form->get('hoover_business_information_qom')->setAttribute('value', $development_user_detail['hoovers_business_information_qom']);
            $form->get('household_profile_qom')->setAttribute('value', $development_user_detail['household_profile_qom']);
            $form->get('lexis_nexis_qom')->setAttribute('value', $development_user_detail['lexis_nexis_qom']);
            $form->get('major_donor_qom')->setAttribute('value', $development_user_detail['major_donor_qom']);
            $form->get('philanthropic_donations_qom')->setAttribute('value', $development_user_detail['philanthropic_donations_qom']);
            $form->get('volunteers_and_directors_qom')->setAttribute('value', $development_user_detail['volunteers_and_directors_qom']);
            $form->get('wealth_id_securities_qom')->setAttribute('value', $development_user_detail['wealthid_securities_qom']);
            $form->get('bio_information')->setAttribute('value', $development_user_detail['bio_Information']);
            $form->get('description')->setAttribute('value', $development_user_detail['document_discription']);
        }
    }

    /**
     * This Action is used to gift form elements .
     * @author Icreon Tech-AP
     */
    public function giftAction() {
        $this->checkUserAuthentication();
        $gift_form = new GiftForm();
        $this->getConfig();
        $params = $this->params()->fromRoute();
        $data['counter'] = $params['counter'];
        $data['container'] = $params['container'];
        $get_org = $this->getServiceLocator()->get('Common\Model\CommonTable')->getOrganizations();
        $org_list = array();
        $org_list[''] = 'Select';
        foreach ($get_org as $key => $val) {
            $org_list[$val['organization_type_id']] = $val['organization_type'];
        }
        $gift_form->get('organization_type[]')->setAttribute('options', $org_list);
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'form_gift' => $gift_form,
            'jsLangTranslate' => $this->_config['user_messages']['config']['development'],
            'params' => $data,
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to job history form elements .
     * @author Icreon Tech-AP
     */
    public function jobHistoryAction() {
        $this->checkUserAuthentication();
        $job_history_form = new JobHistoryForm();
        $this->getConfig();
        $params = $this->params()->fromRoute();
        $get_industry = $this->getServiceLocator()->get('Common\Model\CommonTable')->getIndustry();
        $industry_list = array();
        $industry_list[''] = 'Select';
        foreach ($get_industry as $key => $val) {
            $industry_list[$val['industry_id']] = $val['industry'];
        }
        $job_history_form->get('industry[]')->setAttribute('options', $industry_list);
        $data['counter'] = $params['counter'];
        $data['container'] = $params['container'];
        
        $job_history_form->get('period_from[]')->setAttribute('id', 'period_from'.$data['counter']);
        $job_history_form->get('period_to[]')->setAttribute('id', 'period_to'.$data['counter']);
        $job_history_form->get('company_id[]')->setAttribute('id', 'company_id'.$data['counter']);
        $job_history_form->get('comp_name[]')->setAttribute('id', 'comp_name'.$data['counter']);
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'form_job_history' => $job_history_form,
            'jsLangTranslate' => $this->_config['user_messages']['config']['development'],
            'params' => $data,
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to create new company.
     * @author Icreon Tech-AP
     */
    public function createCompanyAction() {
        $this->checkUserAuthentication();
        $create_company_form = new CreateCompanyForm();
        $this->getConfig();
        $this->layout('popup');
        $viewModel = new ViewModel();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();        
        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();

        $country_list = array();
        $country_list[''] = 'Select Country';
        foreach ($country as $key => $val) {
            $country_list[$val['country_id']] = $val['name'];
        }
        $create_company_form->get('country')->setAttribute('options', $country_list);
        if(isset($params['key'])){
            $create_company_form->get('key')->setValue($params['key']);
        }
        

        if ($request->isPost()) {
            $response = $this->getResponse();
//            $contact_popup_form->setInputFilter($contact->getInputFilterCreatePopupContact());
//            $contact_popup_form->setData($request->getPost());
//
//            if (!$contact_popup_form->isValid()) {
//                $errors = $form_individual->getMessages();
//
//                $msg = array();
//
//                foreach ($errors as $key => $row) {
//                    if (!empty($row) && $key != 'savecontact') {
//                        foreach ($row as $rower) {
//                            $msg [$key] = $this->_config['user_messages']['config']['create_popup_contact'][$rower];
//                        }
//                    }
//                }
//                $messages = array('status' => "error", 'message' => $msg);
//            }
//            if (!empty($messages)) {
//                $response->setContent(\Zend\Json\Json::encode($messages));
//                return $response;
//            } else {
            $form_data = $request->getPost();
            $companyName = $form_data['name'];
            $date = DATE_TIME_FORMAT;
            $form_data['add_date'] = $date;
            $form_data['modified_date'] = $date;
            //$filterParam = $contact->filterParamPopup($form_data);
            $companyNames = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCompany(array($companyName));
            $cNames = array();
            if(!empty($companyNames)){
                foreach($companyNames as $val){
                    $cNames[] = $val['company_name'];
                }
            }
            
            if(!empty($cNames)){
                if(in_array($companyName, $cNames)){
                    $messages = array('status' => "error",'already_exits'=>'yes','message'=>'Company Name already exits.');
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                }
            }
            $company_id = $this->getServiceLocator()->get('Common\Model\CommonTable')->createCompany($form_data);
            if ($company_id) {
                $messages = array('status' => "success","comapny_id"=>$company_id,'company_name'=>$companyName,'key'=>$form_data['key']);
            } else {
                $messages = array('status' => "error");
            }
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
            //}
        }

        $viewModel->setVariables(array(
            'create_company_form' => $create_company_form,
            'jsLangTranslate' => $this->_config['user_messages']['config']['development'],
                )
        );
        return $viewModel;
    }

    /**
     * This action is used to upload file for campaign
     * @return json
     * @param void
     * @author Icreon Tech - DG
     */
    public function uploadDevelopmentFileAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $file_input_names = array_keys($_FILES);
        $file_input_name = $file_input_names[0];
        $image_name_param = $file_input_name;
        $fileExt = $this->GetFileExt($_FILES[$image_name_param]['name']);
        $filename = $this->GenerateMediaFileName($fileExt[1]);    //Generate file name
        $_FILES[$image_name_param]['name'] = $filename;                 // assign name to file variable

        $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration
        $error_messages = array(
            'post_max_size' => 'The uploaded file exceeds the post_max_size directive in php.ini', // Set error messages
            'max_file_size' => 'File is too big',
            'min_file_size' => 'File is too small',
            'accept_file_types' => 'Filetype not allowed',
        );
        $options = array(
            'upload_dir' => $upload_file_path['temp_upload_dir'],
            'param_name' => $image_name_param, //file input name                      // Set configuration
            'inline_file_types' => $upload_file_path['doc_file_types_allowed'],
            'accept_file_types' => $upload_file_path['doc_file_types_allowed'],
            'max_file_size' => $upload_file_path['max_allowed_file_size'],
            'min_file_size' => $upload_file_path['min_allowed_file_size']
        );
        $upload_handler = new UploadHandler($options, true, $error_messages);
        $file_response = $upload_handler->jsonResponceData;
        if (isset($file_response[$image_name_param][0]->error)) {
            $messages = array('status' => "error", 'message' => $file_response[$image_name_param][0]->error);
        } else {
            $messages = array('status' => "success", 'message' => 'File uploaded', 'filename' => $file_response[$image_name_param][0]->name);
        }
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * delete user public doc
     * @return JSON
     * @author Icreon Tech - DG
     */
    public function deleteContactDevelopmentDocAction() {
        $this->checkUserAuthentication();
        $this->getTables();
        $this->getConfig();
        $request = $this->getRequest();
        $response = $this->getResponse();

        if ($request->isPost()) {
            $dataParam['userId'] = $this->decrypt($request->getPost('userId'));
            $dataParam['fileName'] = $request->getPost('fileName');
            $development_user_detail = $this->getTables()->getUserDevelopment(array('user_id' => $dataParam['userId']));
            $user_dir = $this->_config['file_upload_path']['assets_upload_dir'] . "user/";
            $folder_struc_by_user = $this->FolderStructByUserId($dataParam['userId']);
            $user_doc = 'development_doc/';
            $user_doc_dir = $user_dir . $folder_struc_by_user . $user_doc;
            if (!empty($development_user_detail)) {
                $uploadedFiles = $development_user_detail['document_files'];
                $files = explode(",", $uploadedFiles);
                $newFiles = array();
                foreach ($files as $val) {
                    if ($val == $dataParam['fileName']) {
                        unset($val);

                        $user_filename = $user_doc_dir . $dataParam['fileName'];
                        if (file_exists($user_filename)) {
                            unlink($user_filename);
                        }
                    } else {
                        $newFiles[] = $val;
                    }
                }
                $newFiles = implode(",", $newFiles);
                $dataParam['updatedDoc'] = $newFiles;
            }
            $this->getTables()->updateContactDevelopmentDoc($dataParam);
            $messages = array('status' => "success");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }
	/**
     * This function is used to transaction list
     * @return array 
     * @param array
     * @author  Icreon Tech - DG
     */
    public function softCreditAction(){

        $this->checkUserAuthentication();
        $this->getConfig();
        //$this->getTables();
        $this->layout('popup');
        $form = new SearchCrmUserCreditForm();
        $urlParams = $this->params()->fromRoute();
        $messages = array();
		$receivedDateRange = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTransactionRange();
        $dateRange = array("" => "Select One");
        foreach ($receivedDateRange as $key => $val) {
            $dateRange[$val['range_id']] = $val['range'];
        }
        $form->get('received_date_range')->setAttribute('options', $dateRange);
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'searchForm' => $form,
            'messages' => $messages,
            'jsLangTranslate' => array_merge($this->_config['transaction_messages']['config']['search_crm_transaction'], $this->_config['transaction_messages']['config']['common_message'],$this->_config['user_messages']['config']['search_contact']),
        ));
        return $viewModel;
        
    }
	/**
     * This function is used to get queue status
     * @return array 
     * @param array
     * @author  Icreon Tech - DG
     */
    public function searchCreditTransactionAction(){
	//error_reporting(E_ALL);
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        parse_str($request->getPost('searchString'), $searchParam);
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
		
        $isExport = $request->getPost('export');
        if ($isExport != "" && $isExport == "excel") {
            $chunksize = $this->_config['export']['chunk_size'];
            $export_limit = $this->_config['export']['export_limit'];
			$searchParam['recordLimit'] = $chunksize;
        }
		if (isset($searchParam['received_date_range']) && $searchParam['received_date_range'] != 1 && $searchParam['received_date_range'] != '') {
                $dateRange = $this->getDateRange($searchParam['received_date_range']);
                $searchParam['received_date_from'] = $this->DateFormat($dateRange['from'], 'db_date_format_from');
                $searchParam['received_date_to'] = $this->DateFormat($dateRange['to'], 'db_date_format_to');
            } else if (isset($searchParam['received_date_range']) && $searchParam['received_date_range'] == '') {
                $searchParam['received_date_from'] = '';
                $searchParam['received_date_to'] = '';
            } else {
                if (isset($searchParam['received_date_from']) && $searchParam['received_date_from'] != '') {
                    $searchParam['received_date_from'] = $this->DateFormat($searchParam['received_date_from'], 'db_date_format_from');
                }
                if (isset($searchParam['received_date_to']) && $searchParam['received_date_to'] != '') {
                    $searchParam['received_date_to'] = $this->DateFormat($searchParam['received_date_to'], 'db_date_format_to');
                }
            }
        $searchParam['startIndex'] = $start;
        $searchParam['recordLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');
        //asd($searchParam);
        $page_counter = 1;
        $number_of_pages = 1;
        do {
           $searchResult = $this->getServiceLocator()->get('User\Model\DevelopmentTable')->getSoftCreditTransaction($searchParam);
           $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
           $jsonResult = array();
           $arrCell = array();
           $subArrCell = array();
           $total = $countResult[0]->RecordCount;
		   if ($isExport == "excel") {
                $filename = "crm_user_export_" . EXPORT_DATE_TIME_FORMAT . ".csv";
                if ($total > $chunksize) {
                    $number_of_pages = ceil($total / $chunksize);
                    $page_counter++;
                    $start = $chunksize * $page_counter - $chunksize;
                    $searchParam['startIndex'] = $start;
                    $searchParam['page'] = $page_counter;
                } else {
                    $number_of_pages = 1;
                    $page_counter++;
                }
            } else {
                $jsonResult['page'] = $page;
                $jsonResult['records'] = $countResult[0]->RecordCount;
                $jsonResult['total'] = @ceil($countResult[0]->RecordCount / $limit);
            }
        } while ($page_counter <= $number_of_pages && $isExport == "excel");

       

        if (count($searchResult) > 0) {
            $arrExport = array();
            foreach ($searchResult as $val) {
			$transactionId = $this->encrypt($val['transaction_id']);
			$selectTransaction = '<a href="javascript:void(0);" onclick="creditTransaction(&quot;' . $transactionId . '&quot;)" class="select_icon"></a>';
			
			if ($isExport == "excel") {
                 $arrExport[] = array("Transaction Date"=>$this->OutputDateFormat($val['transaction_date'],'calender'),"Name"=>$val['full_name'],"Transaction Id"=>$val['transaction_id'],"Transaction Amount"=>$val['transaction_amount']);
             } else {
               $arrCell['cell'] = array($this->OutputDateFormat($val['transaction_date'],'calender'),$val['transaction_id'],$val['full_name'],$val['transaction_amount'],$selectTransaction);
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }

            }
            if ($isExport == "excel") {
                $this->arrayToCsv($arrExport, $filename, $page_counter);
            }
        } else {
            $jsonResult['rows'] = array();
        }
        if ($isExport == "excel") {
            $this->downloadSendHeaders($filename);
            die;
        } else {
            return $response->setContent(\Zend\Json\Json::encode($jsonResult));
        }
    }
	/**
     * This function is used to get queue status
     * @return array 
     * @param array
     * @author  Icreon Tech - DG
     */
    public function searchSoftCreditReceivedAction(){
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        //parse_str($request->getPost('searchString'), $searchParam);
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        
        $isExport = $request->getPost('export');
        if ($isExport != "" && $isExport == "excel") {
            $chunksize = $this->_config['export']['chunk_size'];
            $export_limit = $this->_config['export']['export_limit'];
			$searchParam['recordLimit'] = $chunksize;
        }
		$urlParams = $this->params()->fromRoute();

		$searchParam['user_id'] = $this->decrypt($urlParams['user_id']);
        $searchParam['startIndex'] = $start;
        $searchParam['recordLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');
        $page_counter = 1;
        $number_of_pages = 1;
        do {
           $searchResult = $this->getServiceLocator()->get('User\Model\DevelopmentTable')->getSoftCreditReceived($searchParam);
           $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
           $jsonResult = array();
           $arrCell = array();
           $subArrCell = array();
           $total = $countResult[0]->RecordCount;
		   if ($isExport == "excel") {
                $filename = "crm_user_export_" . EXPORT_DATE_TIME_FORMAT . ".csv";
                if ($total > $chunksize) {
                    $number_of_pages = ceil($total / $chunksize);
                    $page_counter++;
                    $start = $chunksize * $page_counter - $chunksize;
                    $searchParam['startIndex'] = $start;
                    $searchParam['page'] = $page_counter;
                } else {
                    $number_of_pages = 1;
                    $page_counter++;
                }
            } else {
                $jsonResult['page'] = $page;
                $jsonResult['records'] = $countResult[0]->RecordCount;
                $jsonResult['total'] = @ceil($countResult[0]->RecordCount / $limit);
            }
        } while ($page_counter <= $number_of_pages && $isExport == "excel");


		$resultCnt = 0;
		$totalAmount = 0;
		$gridName = "listDonar";
        if (count($searchResult) > 0) {
            $arrExport = array();
            foreach ($searchResult as $val) {
			$view = $this->encrypt('view');
            $edit = $this->encrypt('edit');
			$source = $this->encrypt('soft_credit');
$name = "<a href='/view-contact/" . $this->encrypt($val['provider_user_id']) . "/" . $view . "/".$source."' target ='_blank'>" . $val['name'] . "</a>";
			//$name = $val['name'];
			$encryptId = $this->encrypt($val['transaction_id']);
			$rowId = $this->encrypt($val['soft_credit_id']);
            $viewTransaction = '<a href="/get-transaction-detail/' . $encryptId . '/' . $this->encrypt('view') . '"  alt="view transaction" target ="_blank">'.$val['transaction_id'].'</a>';
			
			$actions =  '<div class="action-col search-result blue-icon">
                                <a class="delete_contact delete-icon" onclick="deleteUserCreditTransaction(\'' . $rowId . '\',\'' . $gridName . '\');" href="#delete_user_credit_transaction">Delete<div class="tooltip">Delete<span></span></div></a>
                                </div>';
			$totalAmount = $totalAmount + $val['total_amount'];
			$totalAmount = number_format((float)$totalAmount, 2, '.', '');
			$arrCell['cell'] = array($this->OutputDateFormat($val['credit_date'],'calender'),$name,$val['total_amount'],$viewTransaction,$val['notes'],$actions);
            $subArrCell[] = $arrCell;
            $jsonResult['rows'] = $subArrCell;
			$resultCnt++;
			}
            if ($isExport == "excel") {
                $this->arrayToCsv($arrExport, $filename, $page_counter);
            }
			
        } else {
            $jsonResult['rows'] = array();
        }
        if ($isExport == "excel") {
            $this->downloadSendHeaders($filename);
            die;
        } else {
            return $response->setContent(\Zend\Json\Json::encode($jsonResult));
        }
    }
	/**
     * This function is used to get queue status
     * @return array 
     * @param array
     * @author  Icreon Tech - DG
     */
    public function searchSoftCreditProviderAction(){
        $this->checkUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $request = $this->getRequest();
        $response = $this->getResponse();
        parse_str($request->getPost('searchString'), $searchParam);
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;

        $isExport = $request->getPost('export');
        if ($isExport != "" && $isExport == "excel") {
            $chunksize = $this->_config['export']['chunk_size'];
            $export_limit = $this->_config['export']['export_limit'];
			$searchParam['recordLimit'] = $chunksize;
        }
		$urlParams = $this->params()->fromRoute();
		$searchParam['user_id'] = $this->decrypt($urlParams['user_id']);
        $searchParam['startIndex'] = $start;
        $searchParam['recordLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');
        $page_counter = 1;
        $number_of_pages = 1;
        do {
           $searchResult = $this->getServiceLocator()->get('User\Model\DevelopmentTable')->getSoftCreditProvider($searchParam);
           $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
           $jsonResult = array();
           $arrCell = array();
           $subArrCell = array();
           $total = $countResult[0]->RecordCount;
		   if ($isExport == "excel") {
                $filename = "crm_user_export_" . EXPORT_DATE_TIME_FORMAT . ".csv";
                if ($total > $chunksize) {
                    $number_of_pages = ceil($total / $chunksize);
                    $page_counter++;
                    $start = $chunksize * $page_counter - $chunksize;
                    $searchParam['startIndex'] = $start;
                    $searchParam['page'] = $page_counter;
                } else {
                    $number_of_pages = 1;
                    $page_counter++;
                }
            } else {
                $jsonResult['page'] = $page;
                $jsonResult['records'] = $countResult[0]->RecordCount;
                $jsonResult['total'] = @ceil($countResult[0]->RecordCount / $limit);
            }
        } while ($page_counter <= $number_of_pages && $isExport == "excel");


		$resultCnt = 0;
		$totalAmount = 0;
		$gridName = "listProvider";
        if (count($searchResult) > 0) {
            $arrExport = array();
            foreach ($searchResult as $val) {
			
			$encryptId = $this->encrypt($val['transaction_id']);
			$rowId = $this->encrypt($val['soft_credit_id']);
			$view = $this->encrypt('view');
			$source = $this->encrypt('soft_credit');
			
			$name = "<a href='/view-contact/" . $this->encrypt($val['user_id']) . "/" . $view . "/".$source."' target ='_blank'>" . $val['name'] . "</a>";
			
            $viewTransaction = '<a href="/get-transaction-detail/' . $encryptId . '/' . $this->encrypt('view') . '"  alt="view transaction" target ="_blank">'.$val['transaction_id'].'</a>';
			
			$deleteLink = '<a onclick="deleteCreditTransaction(\'' . $encryptId . '\')" href="#delete_transaction_content" class="delete_transaction delete-icon" alt="delete transaction"><div class="tooltip">Delete<span></span></div></a>';
			
			$actions =  '<div class="action-col search-result blue-icon">
                                <a class="delete_contact delete-icon" onclick="deleteUserCreditTransaction(\'' . $rowId . '\',\'' . $gridName . '\');" href="#delete_user_credit_transaction">Delete<div class="tooltip">Delete<span></span></div></a>
                                </div>';
			$totalAmount = $totalAmount + $val['total_amount'];
			$totalAmount = number_format((float)$totalAmount, 2, '.', '');
			$arrCell['cell'] = array($this->OutputDateFormat($val['credit_date'],'calender'),$name,$val['total_amount'],$viewTransaction,$val['notes'],$actions);
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
				$resultCnt++;
	
            }
            if ($isExport == "excel") {
                $this->arrayToCsv($arrExport, $filename, $page_counter);
            }
        } else {
            $jsonResult['rows'] = array();
        }
        if ($isExport == "excel") {
            $this->downloadSendHeaders($filename);
            die;
        } else {
            return $response->setContent(\Zend\Json\Json::encode($jsonResult));
        }
    }
	/**
     * This Action is used to delete soft credit
     * @author Icreon Tech-DG
     * @return JSON
     */
    public function deleteUserSoftCreditAction() {
        $this->checkUserAuthentication();
        $this->getConfig();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam['soft_credit_id'] = $this->decrypt($request->getPost('id'));
			$dataParam['is_delete'] = '1';
			$dataParam['modified_date'] = DATE_TIME_FORMAT;
            $this->getServiceLocator()->get('User\Model\DevelopmentTable')->deleteSoftCredit($dataParam);
            //$this->flashMessenger()->addMessage('Soft Credit deleted successfully');
            $messages = array('status' => "success", 'message' => 'Soft Credit deleted successfully');
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

}
