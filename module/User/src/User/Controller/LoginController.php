<?php

/**
 * This controller is used for authenticate the user and make session
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Controller;
//use Base\Model\BaseFacebook;
//use Base\Model\Facebook;

// s -
use Facebook\FacebookRequest;
use Facebook\FacebookRequestException;
use Facebook\GraphUser;
// e -

use Base\Controller\BaseController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Zend\Session\Container as SessionContainer;
use Zend\Mvc\Controller\Plugin\Params;
use Zend\Http\Request;
use Zend\Authentication\AuthenticationService;
use User\Form\LoginFormCrm;
use User\Model\User;
use User\Form\LoginForm;
use User\Form\ForgotForm;
use User\Form\ForgotFormCrm;
use User\Form\ResetPasswordForm;
use User\Form\ResetCrmPasswordForm;
use User\Form\TermsAndConditionForm;

/**
 * This controller is used for authenticate the user and make session
 * @package    User
 * @author     Icreon Tech - DG
 */
class LoginController extends BaseController {

    protected $_userTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_storage = null;
    protected $_authservice = null;
    protected $_flashMessage = null;

    /**
     * 
     * @return     array 
     * @param array $form
     * @author Icreon Tech - DG
     */
    function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        parent::__construct();
    }

    public function indexAction() {
        
    }

    /**
     * This function is used to get table and config file object
     * @return     array 
     * @param array $form
     * @author  Icreon Tech - DG
     */
    public function getUserTable() {
        if (!$this->_userTable) {
            $sm = $this->getServiceLocator();
            $this->_userTable = $sm->get('User\Model\UserTable');
            $this->_translator = $sm->get('translator');
            $this->_config = $sm->get('Config');
            $this->_adapter = $sm->get('dbAdapter');
        }
        return $this->_userTable;
    }

    /**
     * This function is used to get auth object
     * @return     array 
     * @param array
     * @author  Icreon Tech - DG
     */
    public function getAuthServiceByEmail() {
        if (!$this->_authservice) {
            $this->_authservice = $this->getServiceLocator()
                    ->get('AuthServiceByEmail');
        }
        return $this->_authservice;
    }

    /**
     * This function is used to get auth object
     * @return     array 
     * @param array
     * @author  Icreon Tech - DG
     */
    public function getAuthServiceByUsername() {
        if (!$this->_authservice) {
            $this->_authservice = $this->getServiceLocator()
                    ->get('AuthServiceByUsername');
        }
        return $this->_authservice;
    }

    /**
     * This function is used to get auth object
     * @return     array 
     * @param array
     * @author  Icreon Tech - DG
     */
    public function getSessionStorage() {
        if (!$this->_storage) {
            $this->_storage = $this->getServiceLocator()
                    ->get('User\Model\AuthStorage');
        }
        return $this->_storage;
    }

    /**
     * This action is used for authenticate user and create session.
     * @return     array 
     * @param array $form
     * @author Icreon Tech - DG
     */
    public function loginAction() {

        $params = $this->params()->fromRoute();
        $auth = new AuthenticationService();
                 
        $this->getUserTable();
           
        $termsAndConditionCurrentVersion = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCurrentTermsVersion();
        $termsAndConditionUserId = "";
                
        // Terms and Condition Page - start
        $titleTAC = "";
        $descriptionTAC = "";
        $tacParam = array();
        $tacParam['startIndex'] = '';
        $tacParam['recordLimit'] = '';
        $tacParam['sortField'] = '';
        $tacParam['sortOrder'] = '';
        $tacParam['status'] = '';
        $tacParam['type'] = '';
        $tacParam['content_id'] = $this->_config['terms_content_id'];
        $tacContentArr = $this->getServiceLocator()->get('Cms\Model\CmsTable')->getCrmContents($tacParam);
        $titleTAC = isset($tacContentArr[0]['content_title'])?$tacContentArr[0]['content_title']:"";
        $descriptionTAC = isset($tacContentArr[0]['description'])?$tacContentArr[0]['description']:"";
        $termsAndConditionForm = new TermsAndConditionForm();
        $termsAndConditionForm->get('terms_and_condition')->setValue($termsAndConditionCurrentVersion);
        // Terms and Condition Page - end
        if ($auth->hasIdentity() === true) {
            $user = $auth->getIdentity();
            if (isset($user->email_id) && !isset($params['social_mode'])) {
                $this->checkFrontUserSessionPopup();
            }
        }
        $request = $this->getRequest();
        $this->referPathSession = new SessionContainer('referPathSession');
        $refererUrlNp = '';
        $isLoginPopup = '';
        $isRedirectPopup = '';
        //if (isset($params['referer_path'])) {
        if (isset($params['referer_path']) && !isset($params['social_mode'])) {
            $refererUrlNp = $params['referer_path'];
            $refererUrlArr2 = explode('--', $params['referer_path']);
            $refererUrlAr1 = explode('@@', $refererUrlArr2[0]);
            $refererUrl = $refererUrlAr1[0];
            $refererUrlNp = $refererUrl;
            $isLoginPopup = isset($refererUrlArr2[1]) ? $refererUrlArr2[1] : '';

            $isRedirectPopup = isset($refererUrlAr1[1]) ? $refererUrlAr1[1] : '';

            $refURL = explode('~~', $this->decrypt($refererUrl));
            $arrRefrence = array('/search-woh' => '/view-woh-products', '/shop' => '/add-to-cart', '/shop' => '/add-to-wishlist', '/flag-of-faces' => '/add-fof-image', '/flag-of-faces' => '/search-fof', '/flag-of-faces' => '/save-search-fof');
            if ($refKey = array_search($refURL[0], $arrRefrence)) {
                $this->referPathSession->path = $refKey;
            } else {
                $this->referPathSession->path = $this->decrypt($refererUrl);
            }
            $this->referPathSession->loginPopup = $isLoginPopup;
            $this->referPathSession->redirectPopup = $isRedirectPopup;
        } else {
            $refererUrl = '';
			if($this->referPathSession->path=='')
				$this->referPathSession->path="/profile";
        }
        
        $form = new LoginForm();
    
        $user_config_msg = $this->_config['user_messages']['config']['user_login'];

        $user = new User($this->_adapter);
        $viewModel = new ViewModel();
        if (!empty($isLoginPopup))
            $this->layout('web-popup');
        $response = $this->getResponse();
        $messages = array();
        
        \Facebook\FacebookSession::setDefaultApplication($this->_config['facebook_app']['app_id'],$this->_config['facebook_app']['secret']);
        $helper = new \Facebook\FacebookRedirectLoginHelper(SITE_URL . "/login/profile/facebook",$this->_config['facebook_app']['app_id'],$this->_config['facebook_app']['secret']);
        
        $session = "";
        try { $session = $helper->getSessionFromRedirect(); } 
        catch(FacebookRequestException $ex) { $session = ""; } 
        catch(Exception $ex) { $session = ""; }
        
        if(isset($session) and $session != "") { 
            // Save the session 
           $fb_token = new SessionContainer('fb_token');
           $fb_token->value = $session->getToken(); 
        }

        // Requested permissions - optional
        $permissions = array(
          'email',
          'user_location',
          'user_birthday'
        );
         $fbLoginUrl = $helper->getLoginUrl($permissions);
        
        if (isset($params['social_mode'])) {
            
            $userInfo = array();
            $userFacebookProfile = array();
            $facebookUserId = "";
                
            $userInfo = (new \Facebook\FacebookRequest($session, 'GET', '/me'))->execute()->getGraphObject(GraphUser::className())->asArray();
            $userFacebookProfile = $userInfo;
            $facebookUserId = (isset($userInfo['id']) and trim($userInfo['id']) != "") ? trim($userInfo['id']) : "";

            if (isset($facebookUserId) and trim($facebookUserId) != "") {
                try {

                    $request2 = (new FacebookRequest( $session, 'GET', '/me/picture?type=large&redirect=false' ))->execute();
                    $facebookPicResult = $request2->getGraphObject()->asArray();
                    

                    if (isset($userInfo['email']) && !empty($userInfo) && $userInfo['email'] != '') {
                        $messages = $this->insertFacebookUser($userInfo, $facebookPicResult);
                    } else {
                        $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['user_login']['FACEBOOK_ERROR']);
                    }
                } catch (FacebookApiException $e) {
                    $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['user_login']['FACEBOOK_ERROR']);
                }
                /* echo $this->referPathSession->loginPopup;
                  echo "<br/>";
                  echo $this->referPathSession->redirectPopup;
                  echo "<br/>";
                  echo $this->referPathSession->path;
                  echo "alert(window.opener.addtocart(26));";

                  asd("test"); */
              // Start code to update the user_id after login
                
                
                           //udate user cart
                            $cartParam['user_id'] = "";
                            $cartParam['source_type'] = 'frontend';
                            $cartParam['user_session_id'] = session_id();
                            $cartlistSession = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getCartProducts($cartParam); 

                            $cartParam['user_id'] = $auth->getIdentity()->user_id;
                            $cartParam['source_type'] = 'frontend';
                            $cartlistUserId =  $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getCartProducts($cartParam); 
                            
                            //get all products from previous entery in productid =>quantity
                            $productIds= $this->checkDuplicateProduct($cartlistSession,$cartlistUserId);
                            //asd($productIds);
                            if(!empty($productIds['duplicate'])){
                                $productIds = $productIds['duplicate'];
                               $updateStatus = $this->updateUserCartProduct($productIds,$user->user_id); 

                               if($updateStatus){
                               
                                //$this->deleteSessionCartsData();  
                                $deleteUserParams['user_id'] = $auth->getIdentity()->user_id;
                                $deleteUserParams['user_session_id'] = session_id();
                                $duplicateProduct = array_keys($productIds);

                                $deleteUserParams['product_id'] = implode(',',$duplicateProduct);
                                $this->getServiceLocator()->get('Common\Model\CommonTable')->deleteSessionCartsData($deleteUserParams);

                               }
                            }
                            $updateUserParams['user_id'] = $auth->getIdentity()->user_id;
                            $updateUserParams['user_session_id'] = session_id();
                            $this->getServiceLocator()->get('Common\Model\CommonTable')->updateUserCartsData($updateUserParams);
                    // End code to update the user_id after login   
                if ($this->referPathSession->loginPopup == 'showPopup' && $this->referPathSession->redirectPopup == 'rd') { 
                    
                   $arrRefURL = explode('~~', $this->referPathSession->path);
                        
                    $flagStatusTacv = (isset($messages['flag_status_tacv']) and trim($messages['flag_status_tacv']) != "" and trim($messages['flag_status_tacv']) == "2") ? "2" : "";
                    
                    if($flagStatusTacv != ""){    
                        
                        $refPath = "";
                        $isPopup = "";
                        if (isset($arrRefURL[1]) && (isset($arrRefURL[0]) && $arrRefURL[0] = '/add-to-cart')) {
                             
                          $isPopup = "1";
                          $refPath = "/shop";
                        } elseif (isset($arrRefURL[1]) && (isset($arrRefURL[0]) && $arrRefURL[0] = '/add-to-wishlist')) {
                          $isPopup = "1";
                          $refPath = "/shop";
                        } else {
                          $isPopup = "2";
                          $refPath = $this->referPathSession->path;
                        }
                        
                       echo "<script>window.close();</script>";
                       if(isset($messages['flag_status_tacv']) and trim($messages['flag_status_tacv']) != "" and trim($messages['flag_status_tacv']) == "2" and isset($messages['user']) and trim($messages['user']) != "") {
                           $facebookIdN = isset($userFacebookProfile['id']) ? $userFacebookProfile['id'] : "";
                           $facebookEmailN = isset($userFacebookProfile['email']) ? $userFacebookProfile['email'] : "";
                           echo "<script>window.parent.opener.getFBInfo('".$messages['user']."','" . $refPath . "','".$facebookIdN."','".$facebookEmailN."','".$isPopup."');</script>";
                       }
                    }
                    else {
                    
                        if (isset($arrRefURL[1]) && (isset($arrRefURL[0]) && $arrRefURL[0] = '/add-to-cart')) {

                            //echo "<script>window.parent.opener.addtocart('".$arrRefURL[1]."');</script>";
                            echo "<script>window.parent.$('#closebox').colorbox.close();</script>";
                            //echo "<script>window.opener.updateLoginDetails();</script>";
                            echo "<script>window.parent.opener.top.location.href='/shop';</script>";
                            echo "<script>window.close();</script>";
                        } elseif (isset($arrRefURL[1]) && (isset($arrRefURL[0]) && $arrRefURL[0] = '/add-to-wishlist')) {
                            //echo "<script>window.parent.opener.addtowishlist('".$arrRefURL[1]."');</script>";
                            echo "<script>window.parent.$('#closebox').colorbox.close();</script>";
                            //echo "<script>window.opener.updateLoginDetails();</script>";
                            echo "<script>window.parent.opener.top.location.href='/shop';</script>";
                            echo "<script>window.close();</script>";
                        } else {
                            echo "<script>window.opener.updateLoginDetails();</script>";
                            echo "<script>window.opener.location.href='" . $this->referPathSession->path . "';</script>";
                            echo "<script>window.close();</script>";
                        }
                    
                    } 
                    
                    
                } else {
                    $flagStatusTacv = (isset($messages['flag_status_tacv']) and trim($messages['flag_status_tacv']) != "" and trim($messages['flag_status_tacv']) == "2") ? "2" : "";
                    
                    if($flagStatusTacv == "") {
                       echo "<script>window.parent.opener.top.location.href='" . $this->referPathSession->path . "';</script>";
                       echo "<script>window.close();</script>";
                    }
                    else {    
                          echo "<script>window.close();</script>";
                       if(isset($messages['flag_status_tacv']) and trim($messages['flag_status_tacv']) != "" and trim($messages['flag_status_tacv']) == "2" and isset($messages['user']) and trim($messages['user']) != "") {
                           $facebookIdN = isset($userFacebookProfile['id']) ? $userFacebookProfile['id'] : "";
                           $facebookEmailN = isset($userFacebookProfile['email']) ? $userFacebookProfile['email'] : "";
                           echo "<script>window.parent.opener.getFBInfo('".$messages['user']."','" . $this->referPathSession->path . "','".$facebookIdN."','".$facebookEmailN."','0');</script>";
                       }
                    }
                }
                   
                if(isset($messages['flag_status_tacv']) and trim($messages['flag_status_tacv']) != "" and trim($messages['flag_status_tacv']) == "2" and isset($messages['user']) and trim($messages['user']) != "") {
                  $termsAndConditionForm->get('user_id')->setValue($messages['user']);
                }
                
                $viewModel->setVariables(array(
                    'form' => $form,
                    'fbMessages' => $messages,
                    'fbLoginUrl' => $fbLoginUrl,
                    'facebookReferPath' => $this->referPathSession->path,
                    'jsLangTranslate' => $user_config_msg,
                    'titleTAC' => $titleTAC,
                    'descriptionTAC' => $descriptionTAC,
                    'termsAndConditionForm' => $termsAndConditionForm
                    )
                );
                unset($this->referPathSession->path);
                return $viewModel;
            }
        }

        if ($request->isPost()) {
            $form->setData($request->getPost());
            $form->setInputFilter($user->getInputFilterLogin());
            if (!$form->isValid()) {
                $errors = $form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            $msg [$key] = $this->_config['user_messages']['config']['user_login'][$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else { 
                $user->exchangeArrayLogin($form->getData());
                $is_email = strpos($user->user_name, "@");
                $active = 0;
                $login_state = '';
                $user_id = 0;
                $session_destroy = false;
				/*$ipList=explode(", ",$_SERVER['HTTP_X_FORWARDED_FOR']);
				 if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
					$ipList[]=$_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];*/
                                  $ipList[] = "192.168.4.104";
                if ($is_email || in_array($this->_config['allowed_ip']['ip'],$ipList)) {
                    $this->getAuthServiceByEmail()->getAdapter()
                            ->setIdentity($user->user_name)
                            ->setCredential($user->password);
                    $result = $this->getAuthServiceByEmail()->authenticate();
                    if ($result->isValid()) {
                        $columnsToReturn = array('user_id', 'user_type', 'title', 'first_name', 'middle_name', 'last_name', 'username', 'email_id', 'email_verified', 'verification_code', 'is_activated', 'is_active', 'is_delete', 'is_blocked', 'blocked_date', 'social_account_id', 'social_account_type', 'company_name', 'user_membership_id', 'terms_version_id');
                        $user = $this->getAuthServiceByEmail()->getAdapter()->getResultRowObject($columnsToReturn);
                       /* $is_activated = $user->is_activated;
                        $is_active = $user->is_active;
                        $is_delete = $user->is_delete;
                        $is_blocked = $user->is_blocked;
                        $blocked_date = $user->blocked_date;
                        */
                        
                        $is_activated = true;
                        $is_active = true;
                        $is_delete = true;
                        $is_email_verified = true;
                        $is_blocked = true;
                        $blocked_date = true;

                        $flagStatusTacVersion = "1";
                        if($user->terms_version_id != $termsAndConditionCurrentVersion) { 
                            $flagStatusTacVersion = "2";
                            $termsAndConditionUserId = $user->user_id;
                        }

                        if (!$is_activated && $_SERVER['REMOTE_ADDR'] != $this->_config['allowed_ip']['ip']) {
                            $session_destroy = true;
                            $login_state = 'disabled';
                            $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['user_login']['ACCOUNT_NOT_ACTIVE'], 'is_active' => false, 'id' => $this->encrypt($user->user_id));
                        } else if (!$is_active && $_SERVER['REMOTE_ADDR'] != $this->_config['allowed_ip']['ip']) {
                            $session_destroy = true;
                            $login_state = 'disabled';
                            $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['user_login']['ACCOUNT_NOT_ACTIVE']);
                        } else if ($is_blocked) {
                            $cur_date_time = strtotime(DATE_TIME_FORMAT);
                            $blocked_date = strtotime($blocked_date);
                            $spent_time = ($cur_date_time - $blocked_date) / 60;
                            if ($spent_time > $this->_config['user_blocked']['block_time_limit']) {
                              //  if($flagStatusTacVersion == "1") {
                                  $this->getUserTable()->userLockedUpdate(array('email_id' => $request->getPost('user_name'), 'is_blocked' => '0'));
                              //  }
                                if ($request->getPost('rememberme') == 1 and $flagStatusTacVersion == "1") {
                                    $this->getSessionStorage()->setRememberMe(1);
                                    $this->getAuthServiceByEmail()->setStorage($this->getSessionStorage());
                                }
                                if ($this->getAuthServiceByEmail()->hasIdentity() and $flagStatusTacVersion == "1") {
                                    $this->getAuthServiceByEmail()->getStorage()->write($user);
                                }
                                $active = 1;
                                $login_state = 'success';
                                $user_id = $user->user_id;
                                
                                if($flagStatusTacVersion == "2") {
                                  $authRemove = new AuthenticationService();
                                  if ($authRemove->hasIdentity() === true) {
                                    $authRemove->clearIdentity();
                                  }
                                }
							$cartParam['user_id'] = "";
                            $cartParam['source_type'] = 'frontend';
                            $cartParam['user_session_id'] = session_id();
                            $cartlistSession = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getCartProducts($cartParam); 

                            $cartParam['user_id'] = $user->user_id;;
                            $cartParam['source_type'] = 'frontend';
                            $cartlistUserId =  $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getCartProducts($cartParam); 
                            
                            //get all products from previous entery in productid =>quantity

                            $productIds= $this->checkDuplicateProduct($cartlistSession,$cartlistUserId);
                            if(!empty($productIds['duplicate'])){
                                $productIds = $productIds['duplicate'];
                               $updateStatus = $this->updateUserCartProduct($productIds,$user->user_id); 

                               if($updateStatus){
                                //$this->deleteSessionCartsData();  
	                                $deleteUserParams['user_id'] = $user->user_id;
	                                $deleteUserParams['user_session_id'] = session_id();
	                                $duplicatProdduct = array_keys($productIds);
                                      
	                                $deleteUserParams['product_id'] = implode(',',$duplicatProdduct);
	                                $this->getServiceLocator()->get('Common\Model\CommonTable')->deleteSessionCartsData($deleteUserParams);

                               }
                            }
                            
                             $updateUserParams['user_id'] = $user->user_id;;
                             $updateUserParams['user_session_id'] = session_id();
                             $this->getServiceLocator()->get('Common\Model\CommonTable')->updateUserCartsData($updateUserParams);
							 
                                if (isset($params['referer_path']))
                                    $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['user_login']['LOGIN_SUCCESSFULLY'], 'path' => $this->decrypt($params['referer_path']), 'flag_status_tacv'=>$flagStatusTacVersion, 'user'=>$termsAndConditionUserId);
                                else
                                    $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['user_login']['LOGIN_SUCCESSFULLY'], 'path' => '/profile', 'flag_status_tacv'=>$flagStatusTacVersion, 'user'=>$termsAndConditionUserId);
                            } else {
                                $session_destroy = true;
                                $login_state = 'locked';
                                $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['user_login']['ACCOUNT_BLOCKED']);
                            }
                        } else { 
                            if ($request->getPost('rememberme') == 1 and $flagStatusTacVersion == "1") {
                                $this->getSessionStorage()->setRememberMe(1);
                                $this->getAuthServiceByEmail()->setStorage($this->getSessionStorage());
                            }
                            if ($this->getAuthServiceByEmail()->hasIdentity() and $flagStatusTacVersion == "1") {
                                $this->getAuthServiceByEmail()->getStorage()->write($user);
                            }
                            $active = 1;
                            $login_state = 'success';
                            $user_id = $user->user_id;
                            
                            if($flagStatusTacVersion == "2") {
                                $authRemove = new AuthenticationService();
                                if ($authRemove->hasIdentity() === true) {
                                  $authRemove->clearIdentity();
                                }
                            }
                            if (isset($params['referer_path']))
                                $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['user_login']['LOGIN_SUCCESSFULLY'], 'path' => $this->decrypt($params['referer_path']), 'flag_status_tacv'=>$flagStatusTacVersion, 'user'=>$termsAndConditionUserId);
                            else
                                $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['user_login']['LOGIN_SUCCESSFULLY'], 'path' => '/profile', 'flag_status_tacv'=>$flagStatusTacVersion, 'user'=>$termsAndConditionUserId);
                        }
                        //
                        $kioskDetails = array();
                        $configVal = $this->_config['kiosk-system-settings'];
                        //$ipList = "192.168.4.104"; // write a function to return remote IP
						$ipList = $this->getServiceLocator()->get('KioskModules')->getRemoteIP();
						$isBookingKiosk = 0;// not booking system
						if($configVal[$ipList])
                        $isBookingKiosk = $configVal[$ipList]['isHotSystem'];
                        //$this->getUserVisitationRecord($user->user_id);
                        if($isBookingKiosk){
							
                            $messages['isBookingSystem']= 1;// yes booking system
							
                            $messages['kioskFeePaid'] = 0;// not paid 
                            $messages['isVisitRecord'] = 0;//record not found
                            
                            $kioskReservationDetails = $this->getKioskSytemDetails($user->user_id);
                            if($kioskReservationDetails)
                                $messages['kioskFeePaid'] = 1;
                            
                            $userVisitRecord = $this->getUserVisitationRecord($user->user_id);
                            if($userVisitRecord)
                                $messages['isVisitRecord'] = 1;
                        }
						
                    } 
                    else if($result->getCode() == "-3") {
                        $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['user_login']['USERNAME_PASS_INVALID2']);  
                    }
                    else {
                        $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['user_login']['USERNAME_PASS_INVALID']);
                    }
                } else {
                    $this->getAuthServiceByUsername()->getAdapter()
                            ->setIdentity($user->user_name)
                            ->setCredential($user->password);

                    $result = $this->getAuthServiceByUsername()->authenticate();
                    if ($result->isValid()) {
                        $columnsToReturn = array('user_id', 'user_type', 'title', 'first_name', 'middle_name', 'last_name', 'username', 'email_id', 'email_verified', 'verification_code', 'is_activated', 'is_active', 'is_delete', 'is_blocked', 'blocked_date', 'social_account_id', 'social_account_type', 'company_name', 'user_membership_id', 'terms_version_id');
                        $user = $this->getAuthServiceByUsername()->getAdapter()->getResultRowObject($columnsToReturn);
//                        $is_activated = $user->is_activated;
//                        $is_active = $user->is_active;
//                        $is_delete = $user->is_delete;
//                        $is_email_verified = $user->email_verified;
//                        $is_blocked = $user->is_blocked;
//                        $blocked_date = $user->blocked_date;
                        
                        $is_activated = true;
                        $is_active = true;
                        $is_delete = true;
                        $is_email_verified = true;
                        $is_blocked = true;
                        $blocked_date = true;
                        
                        
                        $flagStatusTacVersion = "1";
                        if($user->terms_version_id != $termsAndConditionCurrentVersion) { 
                            $flagStatusTacVersion = "2";
                            $termsAndConditionUserId = $user->user_id;
                        }
                        
                        if (!$is_email_verified) {
                            $session_destroy = true;
                            $login_state = 'disabled';
                            $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['user_login']['ACCOUNT_NOT_ACTIVE'], 'is_email_verified' => false, 'id' => $this->encrypt($user->user_id));
                        } else if (!$is_activated) {
                            $session_destroy = true;
                            $login_state = 'disabled';
                            $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['user_login']['ACCOUNT_NOT_ACTIVE'], 'is_active' => false, 'id' => $this->encrypt($user->user_id));
                        } else if (!$is_active) {
                            $session_destroy = true;
                            $login_state = 'disabled';
                            $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['user_login']['ACCOUNT_NOT_ACTIVE']);
                        } else if ($is_blocked) {
                            $cur_date_time = strtotime(DATE_TIME_FORMAT);
                            $blocked_date = strtotime($blocked_date);
                            $spent_time = ($cur_date_time - $blocked_date) / 60;
                            if ($spent_time > $this->_config['user_blocked']['block_time_limit']) {
                               // if($flagStatusTacVersion == "1") { 
                                    $this->getUserTable()->userLockedUpdate(array('username' => $request->getPost('user_name'), 'is_blocked' => '0'));
                               // }   
                                if ($request->getPost('rememberme') == 1 and $flagStatusTacVersion == "1") {
                                    $this->getSessionStorage()->setRememberMe(1);
                                    $this->getAuthServiceByUsername()->setStorage($this->getSessionStorage());
                                }
                                if ($this->getAuthServiceByUsername()->hasIdentity() and $flagStatusTacVersion == "1") {
                                    $this->getAuthServiceByUsername()->getStorage()->write($user);
                                }
                                $active = 1;
                                $login_state = 'success';
                                $user_id = $user->user_id;
                                
                              if($flagStatusTacVersion == "2") {
                                  $authRemove = new AuthenticationService();
                                  if ($authRemove->hasIdentity() === true) {
                                    $authRemove->clearIdentity();
                                  }
                               }
                                
                                if (isset($params['referer_path'])) {
                                    $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['user_login']['LOGIN_SUCCESSFULLY'], 'path' => $this->decrypt($params['referer_path']), 'flag_status_tacv'=>$flagStatusTacVersion, 'user'=>$termsAndConditionUserId);
                                }else
                                    $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['user_login']['LOGIN_SUCCESSFULLY'], 'path' => '/profile', 'flag_status_tacv'=>$flagStatusTacVersion, 'user'=>$termsAndConditionUserId);
                            } else {
                                $session_destroy = true;
                                $login_state = 'locked';
                                $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['user_login']['ACCOUNT_BLOCKED']);
                            }
                        } else {
                            if ($request->getPost('rememberme') == 1 and $flagStatusTacVersion == "1") {
                                $this->getSessionStorage()->setRememberMe(1);
                                $this->getAuthServiceByUsername()->setStorage($this->getSessionStorage());
                            }
                            if ($this->getAuthServiceByUsername()->hasIdentity() and $flagStatusTacVersion == "1") {
                                $this->getAuthServiceByUsername()->getStorage()->write($user);
                            }
                            $active = 1;
                            $login_state = 'success';
                            $user_id = $user->user_id;
                            
                            if($flagStatusTacVersion == "2") {
                                  $authRemove = new AuthenticationService();
                                  if ($authRemove->hasIdentity() === true) {
                                    $authRemove->clearIdentity();
                                  }
                             }
							 
                            //udate user cart
                            $cartParam['user_id'] = "";
                            $cartParam['source_type'] = 'frontend';
                            $cartParam['user_session_id'] = session_id();
                            $cartlistSession = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getCartProducts($cartParam); 

                            $cartParam['user_id'] = $user->user_id;
                            $cartParam['source_type'] = 'frontend';
                            $cartlistUserId =  $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->getCartProducts($cartParam); 
                            
                            //get all products from previous entery in productid =>quantity
                            $productIds= $this->checkDuplicateProduct($cartlistSession,$cartlistUserId);
                            //asd($productIds);
                            if(!empty($productIds['duplicate'])){
                                $productIds = $productIds['duplicate'];
                               $updateStatus = $this->updateUserCartProduct($productIds,$user->user_id); 

                               if($updateStatus){
                               
                                //$this->deleteSessionCartsData();  
                                $deleteUserParams['user_id'] = $user->user_id;
                                $deleteUserParams['user_session_id'] = session_id();
                                $duplicateProduct = array_keys($productIds);

                                $deleteUserParams['product_id'] = implode(',',$duplicateProduct);
                                $this->getServiceLocator()->get('Common\Model\CommonTable')->deleteSessionCartsData($deleteUserParams);

                               }
                            }
                             $updateUserParams['user_id'] = $user->user_id;;
                             $updateUserParams['user_session_id'] = session_id();
                             $this->getServiceLocator()->get('Common\Model\CommonTable')->updateUserCartsData($updateUserParams);
							 
                            if (isset($params['referer_path']))
                                $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['user_login']['LOGIN_SUCCESSFULLY'], 'path' => $this->decrypt($refererUrl), 'flag_status_tacv'=>$flagStatusTacVersion, 'user'=>$termsAndConditionUserId);
                            else
                                $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['user_login']['LOGIN_SUCCESSFULLY'], 'path' => '/profile', 'flag_status_tacv'=>$flagStatusTacVersion, 'user'=>$termsAndConditionUserId);
                        }
                        $kioskDetails = array();
                        $configVal = $this->_config['kiosk-system-settings'];
                        $ipList = $this->getServiceLocator()->get('KioskModules')->getRemoteIP();
						$isBookingKiosk = 0;
						if($configVal[$ipList])
                        $isBookingKiosk = $configVal[$ipList]['isHotSystem'];
						
                        //$this->getUserVisitationRecord($user->user_id);
                        if($isBookingKiosk){
                             $messages['isBookingSystem']=1;
                             $messages['kioskFeePaid'] = 0;
                             $messages['isVisitRecord'] = 0;
                             
                            $kioskReservationDetails = $this->getKioskSytemDetails($user->user_id);
                            if($kioskReservationDetails)
                                $messages['kioskFeePaid'] = 1;
                            
                            $userVisitRecord = $this->getUserVisitationRecord($user->user_id);
                            if($userVisitRecord)
                                $messages['isVisitRecord'] = 1;
                        }
                    } 
                    else {
                        $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['user_login']['USERNAME_PASS_INVALID']);
                    }
                }
                if ($session_destroy) {
                    $session = new SessionContainer();
                    $session->getManager()->getStorage()->clear();
                }

                $blocked_messages = $this->insertUserLogins($user_id, $request->getPost('user_name'), $active, $login_state);
                if (!empty($blocked_messages))
                    $messages = $blocked_messages;
                
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }

        
        
       
 
        $viewModel->setVariables(array(
            'form' => $form,
            'response' => $response,
            'fbLoginUrl' => $fbLoginUrl,
            'refererUrlNp' => $refererUrlNp,
            'refererUrl' => $refererUrl,
            'isLoginPopup' => $isLoginPopup,
            'isRedirectPopup' => $isRedirectPopup,
            'jsLangTranslate' => $user_config_msg,
            'titleTAC' => $titleTAC,
            'descriptionTAC' => $descriptionTAC,
			'allowed_IP' => $this->_config['allowed_ip']['ip'],
            'termsAndConditionForm' => $termsAndConditionForm
           )
        );

        return $viewModel;
    }

    
     
    /**
     * This function is used to update User Terms Condition Action
     * @param array Array
     * @author Icreon Tech - DG
     */
    public function updateUserTermsConditionAction() {
        try {
          $request = $this->getRequest();
          if ($request->isPost()) {
              $user_id = $request->getPost("user_id");
              $terms_and_condition = $request->getPost("terms_and_condition");
              if(isset($user_id) and trim($user_id) != "" and isset($terms_and_condition) and trim($terms_and_condition) != "") {
                 $this->getServiceLocator()->get('Common\Model\CommonTable')->updateUserTermsVersion(array('terms_version_id'=>$terms_and_condition,'terms_accept_date'=>DATE_TIME_FORMAT,'user_id'=>$user_id));
              
                 
                 // fb user - start
                   $facebook_id = $request->getPost("facebook_id");
                   $fb_email_id = $request->getPost("fb_email_id");
                  if(isset($facebook_id) and trim($facebook_id) != "" and isset($fb_email_id) and trim($fb_email_id) != "") { 
                    $this->getUserTable();
                    $fbUser = $this->getUserTable()->getSocialAccountUser(array('facebookId' => trim($facebook_id), 'facebookEmail' => trim($fb_email_id)));
                    $fbUser = (object) $fbUser;
                    $this->getAuthServiceByEmail()->getStorage()->write($fbUser); 
                  }
                 // fb user - end 
              }
              $messages = array('status'=>'success');
              $response = $this->getResponse();
              $response->setContent(\Zend\Json\Json::encode($messages));
              return $response;
          }
        }
        catch(Exception $e) {
              $messages = array('status'=>'error');
              $response = $this->getResponse();
              $response->setContent(\Zend\Json\Json::encode($messages));
              return $response;
        }
        die();
    }
    
    
    /**
     * This function is used to store user facebook detail
     * @param array Array
     * @author Icreon Tech - DG
     */
    public function insertFacebookUser($userFacebookProfile, $facebookPicResult) {
        $this->getUserTable();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $response = $this->getResponse();

        $active = 0;
        $loginState = '';
        $userId = 0;

        if (!empty($userFacebookProfile)) {
            $facebookUserData = array();
            $facebookUserData['facebook_id'] = isset($userFacebookProfile['id']) ? $userFacebookProfile['id'] : null;
            $facebookUserData['email_id'] = isset($userFacebookProfile['email']) ? $userFacebookProfile['email'] : null;
            $fbUser = $this->getUserTable()->getSocialAccountUser(array('facebookId' => $facebookUserData['facebook_id'], 'facebookEmail' => $facebookUserData['email_id']));
            if (!empty($fbUser)) {
                $userId = $fbUser['user_id'];
                $emailId = $fbUser['email_id'];

                $is_activated = $fbUser['is_activated'];
                $is_active = $fbUser['is_active'];
                $is_blocked = $fbUser['is_blocked'];
                $blocked_date = $fbUser['blocked_date'];
                
                
                $termsAndConditionCurrentVersion = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCurrentTermsVersion();
                $flagStatusTacVersion = "1";
                if($fbUser['terms_version_id'] != $termsAndConditionCurrentVersion) { 
                        $flagStatusTacVersion = "2";
                        $termsAndConditionUserId = $userId;
                }


                if (!$is_activated) {
                    $loginState = 'disabled';
                    $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['user_login']['ACCOUNT_NOT_ACTIVE'], 'is_active' => false, 'id' => $this->encrypt($userId));
                } else if (!$is_active) {
                    $loginState = 'disabled';
                    $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['user_login']['ACCOUNT_NOT_ACTIVE']);
                } else if ($is_blocked) {
                    $cur_date_time = strtotime(DATE_TIME_FORMAT);
                    $blocked_date = strtotime($blocked_date);
                    $spent_time = ($cur_date_time - $blocked_date) / 60;
                    if ($spent_time > $this->_config['user_blocked']['block_time_limit']) {
                        $this->getUserTable()->userLockedUpdate(array('email_id' => $emailId, 'is_blocked' => '0'));
                        $active = 1;
                        $loginState = 'success';
                        $fbUser = (object) $fbUser;
                        if($flagStatusTacVersion == "1") { $this->getAuthServiceByEmail()->getStorage()->write($fbUser); }
                        $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['user_login']['LOGIN_SUCCESSFULLY'], 'redirect' => true, 'flag_status_tacv'=>$flagStatusTacVersion, 'user'=>$termsAndConditionUserId);
                    } else {
                        $loginState = 'locked';
                        $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['user_login']['ACCOUNT_BLOCKED']);
                    }
                } else {
                    $fbUser = (object) $fbUser;
                    if($flagStatusTacVersion == "1") { $this->getAuthServiceByEmail()->getStorage()->write($fbUser); }
                    $active = 1;
                    $loginState = 'success';
                    $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['user_login']['LOGIN_SUCCESSFULLY'], 'redirect' => true, 'flag_status_tacv'=>$flagStatusTacVersion, 'user'=>$termsAndConditionUserId);
                }
            } else {

                if (!empty($facebookPicResult['url'])) {
                    $urlFbPic = isset($facebookPicResult['url']) ? $facebookPicResult['url'] : '';
                    if ($urlFbPic != "") {
                        $filename = basename($urlFbPic);
                        if ($filename != "") {
                            $facebookUserData['profile_iamge'] = $filename;
                        }
                    }
                }

                $facebookUserData['first_name'] = isset($userFacebookProfile['first_name']) ? $userFacebookProfile['first_name'] : null;
                $facebookUserData['last_name'] = isset($userFacebookProfile['last_name']) ? $userFacebookProfile['last_name'] : null;
                $facebookUserData['gender'] = (isset($userFacebookProfile['gender']) && $userFacebookProfile['gender'] == 'male') ? '1' : '2';

                $facebookUserData['email_verified'] = 1;
                $facebookUserData['is_activated'] = 1;
                $facebookUserData['is_active'] = 1;
                $facebookUserData['social_account_type'] = '1';
                $facebookUserData['added_date'] = DATE_TIME_FORMAT;
                $facebookUserData['modified_date'] = DATE_TIME_FORMAT;
                $facebookUserData['terms_version_id'] = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCurrentTermsVersion();
                $facebookUserData['terms_accept_date'] = DATE_TIME_FORMAT;
                $insertUserId = $this->getUserTable()->addSocialAccountUser($facebookUserData);
                if ($insertUserId) {
                    $this->getServiceLocator()->get('User\Model\ContactTable')->updateContactId(array('user_id' => $insertUserId));

                    $this->getServiceLocator()->get('User\Model\UserTable')->insertDefaultContactCommunicationPreferences(array('user_id' => $insertUserId));
                    $membershipDetailArr = array();
                    $membershipDetailArr[] = 1;
                    $getMemberShip = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);

                    $userMembershipData = array();
                    $userMembershipData['user_id'] = $insertUserId;
                    $userMembershipData['membership_id'] = $getMemberShip['membership_id'];
                    $startDate = date("Y-m-d");
                    if ($getMemberShip['validity_type'] == 1) {
                        $userMembershipData['membership_date_from'] = $startDate;
                        $startDay = $this->_config['financial_year']['srart_day'];
                        $startMonth = $this->_config['financial_year']['srart_month'];
                        $startYear = date("Y");
                        $startDate = $startYear . "-" . $startMonth . "-" . $startDay;
                        $addYear = $getMemberShip['validity_time'] - date("Y");
                        $futureDate = date('Y-m-d', strtotime('+' . $addYear . ' year', strtotime($startDate)));
                        $userMembershipData['membership_date_to'] = $futureDate;
                    } else if ($getMemberShip['validity_type'] == 2) {
                        $userMembershipData['membership_date_from'] = $startDate;
                        $futureDate = date('Y-m-d', strtotime('+' . $getMemberShip['validity_time'] . ' year', strtotime($startDate)));
                        $userMembershipData['membership_date_to'] = $futureDate;
                    }
                    $this->getServiceLocator()->get('User\Model\UserTable')->saveUserMembership($userMembershipData);

                    if (!empty($facebookPicResult['url'])) {
                        $urlFbPic = isset($facebookPicResult['url']) ? $facebookPicResult['url']: '';
                        if ($urlFbPic != "") {
                            $filename = basename($urlFbPic);
                            if ($filename != "") {
                                $userDir = $this->_config['file_upload_path']['assets_upload_dir'] . "/user/";
                                $userImage = 'image/';
                                $userImgThumb = 'thumbnail/';
                                $userImgMedum = 'medium/';
                                $folderStrucByUser = $this->FolderStructByUserId($insertUserId);
                                $userImageDir = $userDir . $folderStrucByUser . $userImage;
                                $userThumbImageDir = $userDir . $folderStrucByUser . $userImage . $userImgThumb;
                                $userMedumImageDir = $userDir . $folderStrucByUser . $userImage . $userImgMedum;
                                if (!is_dir($userImageDir)) {
                                    mkdir($userImageDir, 0777, TRUE);
                                }
                                $userFilename = $userImageDir . $filename;
                                $userThumbnailFilename = $userThumbImageDir . $filename;
                                $userMedumFilename = $userMedumImageDir . $filename;
                                if (!is_dir($userThumbImageDir)) {
                                    mkdir($userThumbImageDir, 0777, TRUE);
                                }
                                if (!is_dir($userMedumImageDir)) {
                                    mkdir($userMedumImageDir, 0777, TRUE);
                                }
                                copy($urlFbPic, $userFilename);
                                copy($urlFbPic, $userThumbnailFilename);
                                copy($urlFbPic, $userMedumFilename);
                            }
                        }
                    }
                    $active = 1;
                    $loginState = 'success';
                    $userId = $insertUserId;
                    $emailId = $facebookUserData['email_id'];
                    $fbUser = $this->getUserTable()->getSocialAccountUser(array('facebookId' => $facebookUserData['facebook_id']));
                    $fbUser = (object) $fbUser;
                    $this->getAuthServiceByEmail()->getStorage()->write($fbUser);
                    $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['user_login']['LOGIN_SUCCESSFULLY'], 'redirect' => true);
                }
            }
            $blocked_messages = $this->insertUserLogins($userId, $emailId, $active, $loginState);
            if (!empty($blocked_messages)) {
                $messages = $blocked_messages;
            }
            return $messages;
        }
    }

    /**
     * This function is used to store user login logs
     * @param array Array
     * @author Icreon Tech - DG
     */
    public function insertUserLogins($user_id, $user_name, $active, $login_state) {
        $messages = array();
        $is_email = strpos($user_name, "@");
        if ($is_email) {
            $userDataArr = $this->getUserTable()->getUser(array('email_id' => $user_name));
        } else {
            $userDataArr = $this->getUserTable()->getUser(array('username' => $user_name));
        }
        if ($user_id) {
            $loginsArr['user_id'] = $user_id;
        }

        $loginsArr['email_id'] = $user_name;
        $loginsArr['login_date'] = DATE_TIME_FORMAT;
        $loginsArr['active'] = $active;
        $loginsArr['ip'] = $_SERVER['REMOTE_ADDR'];
        $loginsArr['last_update_date'] = DATE_TIME_FORMAT;
        if ($login_state == '') {
            if (!empty($userDataArr)) {
                if ($userDataArr['is_blocked'] == '0')
                    $loginsArr['login_state'] = 'bad-password';
                else
                    $loginsArr['login_state'] = 'locked';
            } else {
                $loginsArr['login_state'] = 'no-member';
            }
        } else {
            $loginsArr['login_state'] = $login_state;
        }
        $loginsArr['source_id'] = $this->_config['transaction_source']['transaction_source_id'];
        $this->getUserTable()->insertLogins($loginsArr);
        if ($loginsArr['login_state'] == 'bad-password') {
            /* Check User block Count and get blocked */
            $config_user_blocked = $this->_config['user_blocked'];
            $before_thirty_time = date("Y-m-d H:i:s", strtotime("-" . $config_user_blocked['block_time_limit'] . " minutes"));
            $current_time = date("Y-m-d H:i:s", strtotime("now"));
            $blockedCount = $this->getUserTable()->getBlockCount(array('email_id' => $user_name, 'login_state' => 'bad-password', 'before_thirty_time' => $before_thirty_time, 'current_time' => $current_time));
            if ($blockedCount['block_count'] >= $config_user_blocked['invalid_attempt']) {
                if ($is_email) {
                    $userDataArr = $this->getUserTable()->userLockedUpdate(array('email_id' => $user_name, 'is_blocked' => '1'));
                } else {
                    $userDataArr = $this->getUserTable()->userLockedUpdate(array('username' => $user_name, 'is_blocked' => '1'));
                }
                $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['user_login']['ACCOUNT_BLOCKED']);
            }
        }
        else if ($loginsArr['login_state'] == 'locked') {
             $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['user_login']['ACCOUNT_LOCKED']);
        }
        return $messages;
    }

    /**
     * This action used to clear all the session and user session
     * @author Icreon Tech - DG
     */
    public function logoutAction() {
        $auth = new AuthenticationService();
        $param = $this->params()->fromRoute();
        $this->auth = $auth;
        $redirect = 'home';
        if ($this->auth->hasIdentity() === true) {
            $userArr = $this->auth->getIdentity();
            if (isset($userArr->crm_email_id)) {
                $redirect = 'crm';
                $crm_user_id = $userArr->crm_user_id;
                $this->getUserTable()->updateCrmUserLogins(array('crm_user_id' => $crm_user_id, 'crm_active' => '0'));
            } else {
		  $user_id = $userArr->user_id;
		  $params  = array();
		  $params['contact_id'] = $userArr->user_id;
		  $params['transaction_id'] = $userArr->last_transaction_id;
		  $params['kiosk_id'] = $userArr->kiosk_id;
		  $params['time_end'] = date('H:i:s',strtotime(DATE_TIME_FORMAT));
		  $params['pause_resume'] = '';
		  $params['queue_id'] = $userArr->queue_id;
                $this->getUserTable()->updateUserLogins(array('user_id' => $user_id, 'active' => '0'));
				
		  $this->getServiceLocator()->get('Kiosk\Model\KioskReservationTable')->updateReservationInfo($params);

		  if(isset($param['code'])){

		   	$logoutDateTime = date('Y-m-d h-i A',strtotime($this->OutputDateFormat($params['time_end'], 'dateformatampm')));

		   	$filename = $this->_config['file_upload_path']['assets_upload_dir'] . "kiosklogfile/".$user_id."_".$logoutDateTime.".txt";
				
		   	if(!file_exists($filename)){
		   		$this->kioskSession = new SessionContainer('kioskSession');
		       	$end_time = $this->kioskSession->time_end;
				$endDateTime = date('Y-m-d h-i A',strtotime($this->OutputDateFormat($end_time, 'dateformatampm')));
				$queue_id = $this->kioskSession->queue_id;
				$myfile = fopen($filename, "w");
				$content = "QueueId:".$queue_id.", End Time:".$endDateTime.", Logout Time:".$logoutDateTime;
				fwrite($myfile, $content);
				fclose($myfile);
		  	}
		 }

            }
            $this->getSessionStorage()->forgetMe();
            $this->auth->clearIdentity();
            $session = new SessionContainer();
            //$session->getManager()->destroy(array('send_expire_cookie' => true, 'clear_storage' => true));
            $session->getManager()->getStorage()->clear('kioskSession');
            // clear cache - start
          /*  clearstatcache();
            header("Expires: ".gmdate("D, d M Y H:i:s", time())." GMT");  
            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");  
            header("Cache-Control: no-cache, must-revalidate");  
            header("Pragma: no-cache");     
           */
            // unset cookies
            /*if(isset($_COOKIES) and !empty($_COOKIES)) { 
                foreach($_COOKIES as $name => $value){ 
                  setcookie($name,null, 1,"/",".".$_SERVER['SERVER_NAME']);        
                  setcookie($name,null, 1,"/",$_SERVER['SERVER_NAME']);  
               }              
             }
             unset($_SESSION);
             session_unset();
             session_destroy();
             */
            // clear cache - end
             
            $this->redirect()->toRoute($redirect);
        } else {
             $this->redirect()->toRoute('home');
        }
    }

    /**
     * This action is used for authenticate user and create session for crm.
     * @return     array 
     * @param array $form
     * @author Icreon Tech - DT
     */
    public function crmLoginAction() {

        //if already login, redirect to success page
        $auth = new AuthenticationService();
        $this->auth = $auth;
        if ($this->auth->hasIdentity() === true) {
            $user = $this->auth->getIdentity();
            if (isset($user->crm_email_id)) {
                return $this->redirect()->toRoute('dashboard');
            }
        }
        $flashMessages = array();
        if ($this->_flashMessage->hasMessages()) {
            $flashMessages = $this->_flashMessage->getMessages();
        }

        $params = $this->params()->fromRoute();
        if (isset($params['referer_path'])) {
            $refererUrl = $params['referer_path'];
        } else {
            $refererUrl = '';
        }

        $this->layout('crm-login');
        // $this->layout('crm-login')->testvar = "abc";
        $form = new LoginFormCrm();
        $this->getUserTable();
        $user_config_msg = $this->_config['user_messages']['config']['user_login_crm'];
        $request = $this->getRequest();
        $user = new User($this->_adapter);
        $viewModel = new ViewModel();
        $response = $this->getResponse();
        $messages = array();
        if ($request->isPost()) {
            $form->setInputFilter($user->getInputFilterLoginCrm());
            $form->setData($request->getPost());
            if (!$form->isValid()) {
                $errors = $form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            $msg [$key] = $this->_config['user_messages']['config']['user_login_crm'][$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $user->exchangeArrayLoginCrm($form->getData());
                $active = 0;
                $login_state = '';
                $user_id = 0;
                $this->getAuthServiceByEmailCrm()->getAdapter()
                        ->setIdentity($user->user_name)
                        ->setCredential($user->password);
                $result = $this->getAuthServiceByEmailCrm()->authenticate();
                if ($result->isValid()) {
                    $user = $this->getAuthServiceByEmailCrm()->getAdapter()->getResultRowObject();
                    $is_active = $user->crm_is_active;
                    $is_delete = $user->crm_is_deleted;
                    $is_blocked = $user->is_blocked;
                    $blocked_date = $user->blocked_date;
                    if (!$is_active) {
                        $login_state = 'disabled';
                        $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['user_login_crm']['ACCOUNT_NOT_ACTIVE']);
                    } else if ($is_blocked) {
                        $config_user_blocked = $this->_config['crm_user_blocked'];
                        $cur_date_time = strtotime(DATE_TIME_FORMAT);
                        $blocked_date = strtotime($blocked_date);
                        $spent_time = ($cur_date_time - $blocked_date) / 60;
                        if ($spent_time >= $config_user_blocked['block_time_limit']) {
                            $this->getUserTable()->userLockedUpdateCrm(array('crm_email_id' => $request->getPost('user_name'), 'is_blocked' => '0'));
                            if ($this->getAuthServiceByEmailCrm()->hasIdentity()) {
                                $this->getAuthServiceByEmailCrm()->getStorage()->write($user);
                            }
                            $active = 1;
                            $login_state = 'success';
                            $user_id = $user->crm_user_id;
                            if (isset($params['referer_path'])) {
                                $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['user_login_crm']['LOGIN_SUCCESSFULLY'], 'path' => $this->decrypt($params['referer_path']));
                            } else {
                                $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['user_login_crm']['LOGIN_SUCCESSFULLY'], 'path' => '/dashboard');
                            }
                            //$messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['user_login_crm']['LOGIN_SUCCESSFULLY']);
                        } else {
                            $login_state = 'locked';
                            $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['user_login_crm']['USER_BLOCKED']);
                        }
                    } else {
                        if ($this->getAuthServiceByEmailCrm()->hasIdentity()) {
                            $this->getAuthServiceByEmailCrm()->getStorage()->write($user);
                        }
                        $active = 1;
                        $login_state = 'success';
                        $user_id = $user->crm_user_id;
                        if (isset($params['referer_path']))
                            $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['user_login_crm']['LOGIN_SUCCESSFULLY'], 'path' => $this->decrypt($params['referer_path']));
                        else
                            $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['user_login_crm']['LOGIN_SUCCESSFULLY'], 'path' => '/dashboard');
                        //$messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['user_login_crm']['LOGIN_SUCCESSFULLY']);
                    }
                } else {
                    $email_id_wrong_cred = $request->getPost('user_name');
                    $userDataArr = $this->getUserTable()->getUserCrm(array('crm_email_id' => $email_id_wrong_cred));
                    if (!empty($userDataArr)) {
                        $is_blocked = $userDataArr['is_blocked'];
                        $blocked_date = $userDataArr['blocked_date'];
                        if ($is_blocked) {
                            $config_user_blocked = $this->_config['crm_user_blocked'];
                            $cur_date_time = strtotime(DATE_TIME_FORMAT);
                            $blocked_date = strtotime($blocked_date);
                            $spent_time = ($cur_date_time - $blocked_date) / 60;
                            if ($spent_time < $config_user_blocked['block_time_limit']) {
                                $login_state = 'locked';
                                $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['user_login_crm']['USER_BLOCKED']);
                            }
                        } else {
                            $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['user_login_crm']['USERNAME_PASS_INVALID']);
                        }
                    } else {
                        $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['user_login_crm']['USERNAME_PASS_INVALID']);
                    }
                }

                //$this->insertUserLoginsCrm($user_id, $request->getPost('user_name'), $active, $login_state);
                $blocked_messages = $this->insertUserLoginsCrm($user_id, $request->getPost('user_name'), $active, $login_state);
                if (!empty($blocked_messages))
                    $messages = $blocked_messages;
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $viewModel->setVariables(array(
            'form' => $form,
            'response' => $response,
            'messages' => $flashMessages,
            'refererUrl' => $refererUrl,
            'jsLangTranslate' => $user_config_msg
        ));
        //$viewModel->setTerminal(true);

        return $viewModel;
    }

    /**
     * This function is used to store user login logs
     * @param array Array
     * @author Icreon Tech - DT
     */
    public function insertUserLoginsCrm($user_id, $email_id, $active, $login_state) {
        $messages = array();

        $userDataArr = $this->getUserTable()->getUserCrm(array('crm_email_id' => $email_id));

        if ($user_id) {
            $loginsArr['crm_user_id'] = $user_id;
        }

        $loginsArr['crm_email_id'] = $email_id;
        $loginsArr['crm_login_date'] = DATE_TIME_FORMAT;
        $loginsArr['crm_active'] = $active;
        $loginsArr['crm_ip'] = $_SERVER['REMOTE_ADDR'];
        $loginsArr['crm_last_update_date'] = DATE_TIME_FORMAT;
        if ($login_state == '') {
            if (!empty($userDataArr)) {
                if ($userDataArr['is_blocked'] == '0')
                    $loginsArr['crm_login_state'] = 'bad-password';
                else
                    $loginsArr['crm_login_state'] = 'locked';
            } else {
                $loginsArr['crm_login_state'] = 'no-member';
            }
        } else {
            $loginsArr['crm_login_state'] = $login_state;
        }
        $this->getUserTable()->insertLoginsCrm($loginsArr);

        if ($loginsArr['crm_login_state'] == 'bad-password') {
            $config_user_blocked = $this->_config['crm_user_blocked'];
            $before_thirty_time = date("Y-m-d H:i:s", strtotime("-" . $config_user_blocked['block_time_limit'] . " minutes"));
            $current_time = date("Y-m-d H:i:s", strtotime("now"));
            $blockedCount = $this->getUserTable()->getBlockCountCrm(array('email_id' => $email_id, 'login_state' => 'bad-password', 'before_thirty_time' => $before_thirty_time, 'current_time' => $current_time));

            if ($blockedCount['crm_block_count'] >= $config_user_blocked['invalid_attempt']) {
                $userDataArr = $this->getUserTable()->userLockedUpdateCrm(array('crm_email_id' => $email_id, 'is_blocked' => '1'));
                $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['user_login_crm']['USER_BLOCKED']);
            }
        }
        return $messages;
    }

    /**
     * This function is used to get auth object for crm
     * @return     array 
     * @param array
     * @author Icreon Tech - DT
     */
    public function getAuthServiceByEmailCrm() {
        if (!$this->_authservice) {
            $this->_authservice = $this->getServiceLocator()
                    ->get('AuthServiceByEmailCrm');
        }
        return $this->_authservice;
    }

    /**
     * This Action is used to forget password of user
     * 
     * @author Icreon Tech - DG
     * @param code
     * @return Array
     */
    public function forgotAction() {
        $form = new ForgotForm();
        $this->getUserTable();

        $request = $this->getRequest();
        $user = new User($this->_adapter);
        $viewModel = new ViewModel();
        $this->layout('web-popup');
        $response = $this->getResponse();
        $messages = array();
        if ($request->isPost()) {
            $form->setData($request->getPost());
            $form->setInputFilter($user->getInputFilterForgot());
            if (!$form->isValid()) {
                $errors = $form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            $msg [$key] = $this->_config['user_messages']['config']['forgot_password'][$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $user->exchangeArrayForgot($form->getData());

                $userDataArr = $this->getUserTable()->getUser(array('email_id' => $user->email_id));
                if (empty($userDataArr)) {
                    $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['forgot_password']['EMAIL_NOT_EXITS']);
                } else {
                    $userForgotPasswordDetails = $this->getUserTable()->getUserForgotDetails(array('user_id' => $userDataArr['user_id'], 'current_datetime' => DATE_TIME_FORMAT));
                    if($userForgotPasswordDetails == "0") {
                        $forgotDataArr = $this->getUserTable()->getForgot(array('email_id' => $userDataArr['email_id']));
                        $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                        $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                        $userDataArr['code'] = $forgotDataArr['code'];
                        $signup_email_id_template_int = 2;
                        if ($userDataArr['user_type'] != 1) {
                           $userDataArr['first_name'] = $userDataArr['company_name'];
                        }
                        $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $signup_email_id_template_int, $this->_config['email_options']);
                        $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['forgot_password']['PASSWORD_LINK_SENT_SUCCESS']);
                    }
                    else {                     
                   
                        $code = $this->generateRandomString();
                        $this->getUserTable()->insertForgot(array('user_id' => $userDataArr['user_id'], 'code' => $code));
                        $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                        $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                        $userDataArr['code'] = $code;
                        $signup_email_id_template_int = 2;
                        if ($userDataArr['user_type'] != 1) {
                            $userDataArr['first_name'] = $userDataArr['company_name'];
                        }
                        $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $signup_email_id_template_int, $this->_config['email_options']);
                        $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['forgot_password']['PASSWORD_LINK_SENT_SUCCESS']);
                    }
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $viewModel->setVariables(array(
            'form' => $form,
            'response' => $response,
            'jsLangTranslate' => $this->_config['user_messages']['config']['forgot_password'])
        );

        return $viewModel;
    }

    /**
     * This Action is used to reset forget password
     * 
     * @author Icreon Tech - DG
     * @param code
     * @return Array
     */
    public function setPasswordAction() {
        $this->checkFrontUserSessionPopup();
        $form = new ResetPasswordForm();
        $this->getUserTable();
        $request = $this->getRequest();
        $user = new User($this->_adapter);
        $viewModel = new ViewModel();
        $response = $this->getResponse();
        $messages = array();
        //$this->layout('web-popup');
        $params = $this->params()->fromRoute();
        if (isset($params['code']) && $params['code'] != '') {
            $form->get('code')->setValue($params['code']);
        }
        if ($request->isPost()) {
            $form->setData($request->getPost());
            $form->setInputFilter($user->getInputFilterPassword());
            if (!$form->isValid()) {
                $errors = $form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            $msg [$key] = $this->_config['user_messages']['config']['forgot_password'][$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $user->exchangeArrayPassword($form->getData());
                $userDataArr = $this->getUserTable()->getUser(array('email_id' => $user->email_id));
                if (empty($userDataArr)) {
                    $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['forgot_password']['EMAIL_NOT_EXITS']);
                } else {
                    $forgotDataArr = $this->getUserTable()->getForgot(array('email_id' => $user->email_id));
                    if (empty($forgotDataArr)) {
                        $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['forgot_password']['RESET_PASSWORD_LINK_NOT_EXITS']);
                    } else {
                        if ($forgotDataArr['code'] == $request->getPost('code')) {
                            $userDataArr = $this->getUserTable()->updatePassword(array('user_id' => $userDataArr['user_id'], 'new_password' => $user->password));
                            $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['forgot_password']['PASSWORD_CHANGE_SUCCESS']);
                        } else {
                            $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['forgot_password']['RESET_PASSWORD_LINK_NOT_EXITS']);
                        }
                    }
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $viewModel->setVariables(array(
            'form' => $form,
            'code' => $params['code'],
            'response' => $response,
            'jsLangTranslate' => $this->_config['user_messages']['config']['forgot_password'])
        );

        return $viewModel;
    }

    /**
     * This Action is used to forget password of user for crm
     * @author Icreon Tech - DT
     * @param code
     * @return Array
     */
    public function crmForgotAction() {
        $auth = new AuthenticationService();
        $this->auth = $auth;
        if ($this->auth->hasIdentity() === true) {
            $user = $this->auth->getIdentity();
            if (isset($user->crm_email_id)) {
                return $this->redirect()->toRoute('dashboard');
            }
        }
        $form = new ForgotFormCrm();
        $this->getUserTable();
        $this->layout('crm-login');
        $request = $this->getRequest();
        $user = new User($this->_adapter);
        $viewModel = new ViewModel();

        $response = $this->getResponse();
        $messages = array();
        if ($request->isPost()) {
            $form->setInputFilter($user->getInputFilterForgotCrm());
            $form->setData($request->getPost());
            if (!$form->isValid()) {
                $errors = $form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            $msg [$key] = $this->_config['user_messages']['config']['forgot_password_crm'][$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $user->exchangeArrayForgotCrm($form->getData());

                $userDataArr = $this->getUserTable()->getUserCrm(array('crm_email_id' => $user->email_id));
                if (empty($userDataArr)) {
                    $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['forgot_password_crm']['EMAIL_NOT_EXITS']);
                } else {
                    $code = $this->generateRandomString();
                    $this->getUserTable()->insertForgotCrm(array('crm_user_id' => $userDataArr['crm_user_id'], 'code' => $code));
                    $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                    $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                    $userDataArr['code'] = $code;
                    $userDataArr['email_id'] = $userDataArr['crm_email_id'];
                    $userDataArr['first_name'] = $userDataArr['crm_first_name'];
                    $userDataArr['last_name'] = $userDataArr['crm_last_name'];

                    $signup_email_id_template_int = 3;
                    $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $signup_email_id_template_int, $this->_config['email_options']);
                    $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['forgot_password_crm']['PASSWORD_SENT_SUCCESS']);
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $viewModel->setVariables(array(
            'form' => $form,
            'response' => $response,
            'jsLangTranslate' => $this->_config['user_messages']['config']['forgot_password_crm'])
        );

        return $viewModel;
    }

    /**
     * This Action is used to reset forget password
     * @author Icreon Tech - DT
     * @param code
     * @return Array
     */
    public function setCrmPasswordAction() {
        $auth = new AuthenticationService();
        $this->auth = $auth;
        if ($this->auth->hasIdentity() === true) {
            $user = $this->auth->getIdentity();
            if (isset($user->crm_email_id)) {
                return $this->redirect()->toRoute('dashboard');
            }
        }
        $form = new ResetCrmPasswordForm();
        $this->getUserTable();
        $this->layout('crm-login');
        $request = $this->getRequest();
        $user = new User($this->_adapter);
        $viewModel = new ViewModel();

        $response = $this->getResponse();
        $messages = array();

        $params = $this->params()->fromRoute();
        if (isset($params['crmcode']) && $params['crmcode'] != '') {
            $form->get('code')->setValue($params['crmcode']);
            $forgotDataArr = $this->getUserTable()->getForgotCrm(array('code' => $params['crmcode']));
            if (empty($forgotDataArr)) {
                $this->flashMessenger()->addMessage($this->_config['user_messages']['config']['forgot_password_crm']['RESET_PASSWORD_LINK_NOT_EXITS']);
                return $this->redirect()->toRoute('crm');
            }
        }

        if ($request->isPost()) {
            $form->setInputFilter($user->getCrmInputFilterPassword());
            $form->setData($request->getPost());
            if (!$form->isValid()) {
                $errors = $form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            $msg [$key] = $this->_config['user_messages']['config']['forgot_password_crm']['EMAIL_NOT_EXITS'];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $user->exchangeArrayPasswordCrm($form->getData());
                $userDataArr = $this->getUserTable()->getUserCrm(array('crm_email_id' => $user->email_id));
                if (empty($userDataArr)) {
                    $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['forgot_password_crm']['EMAIL_NOT_EXITS']);
                } else {
                    $forgotDataArr = $this->getUserTable()->getForgotCrm(array('crm_email_id' => $user->email_id));
                    if (empty($forgotDataArr)) {
                        $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['forgot_password_crm']['RESET_PASSWORD_LINK_NOT_EXITS']);
                    } else {
                        if ($forgotDataArr['code'] == $request->getPost('code')) {
                            $userDataArr = $this->getUserTable()->updatePasswordCrm(array('crm_user_id' => $userDataArr['crm_user_id'], 'new_password' => $user->password));
                            $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['forgot_password_crm']['PASSWORD_CHANGE_SUCCESS']);
                        } else {
                            $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['forgot_password_crm']['RESET_PASSWORD_LINK_NOT_EXITS']);
                        }
                    }
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $viewModel->setVariables(array(
            'form' => $form,
            'response' => $response,
            'forget_errors' => $this->_config['user_messages']['config']['forgot_password_crm'],
            'jsLangTranslate' => $this->_config['user_messages']['config']['forgot_password_crm'])
        );

        return $viewModel;
    }
   /**
     * This function is used to get reservation information about the logged in user
     * @author Icreon Tech - DT
     * @param code
     * @return Array
     */
    protected function getKioskSytemDetails($userid){
        //$bookingDetails = array();
        $isKioskFeePaid = false;
        $contactParams['user_id'] = $userid;
        $contactParams['curr_date'] = date('Y-m-d',strtotime(DATE_TIME_FORMAT));
        $contactParams['queue_id'] =   "";
        $contactParams['kiosk_id'] = "";
        $contactParams['contact']  = "";
        $contactParams['startIndex'] = "";
        $contactParams['recordLimit']= "";
        $contactParams['sortField']  = "";
        $contactParams['sortOrder']  = "";
        $contactParams['payment_status'] = "";
        $contactDetails = $this->getServiceLocator()->get('Kiosk\Model\KioskReservationTable')->searchReservationQueued($contactParams);
        $bookingDetails = array('kioskFeePaid'=>0); 
        if(!empty($contactDetails)){
             $isKioskFeePaid = true;
        }
        return $isKioskFeePaid;
    }
    /**
     * This function is used to get visitation record about the logged in user
     * @author Icreon Tech - DT
     * @param code
     * @return Array
     */
    protected function getUserVisitationRecord($userid){
        $userRecordDetails = array();
        $userRecord = false;
        $searchParam['user_id'] = $userid;
        $userRecord = $this->getServiceLocator()->get('Kiosk\Model\KioskReservationTable')->getUserVisitRecord($searchParam); 
        //$userRecord = array('isBookingSystem'=>1,'isVisitRecord'=>0); 
        if(!empty($userRecord[0])){
             //$userRecordDetails = array('isBookingSystem'=>1,'isVisitRecord'=>1); 
             $userRecord = true;
        }
        return $userRecord;
    }
	
	
    protected function checkDuplicateProduct($sessionCartData,$userCartData){
     $result = array();
     foreach($sessionCartData as $cartData){
         foreach($userCartData as $userData){
             if(!empty($userData['user_id'])){
                 if($cartData['product_id'] == $userData['product_id']){
                    $result['duplicate'][$cartData['product_id']] = $cartData['product_qty'] + $userData['product_qty'];
     
                 }
                 else {
					$result['non_duplicate'][] = $cartData['product_id'];                 
                 }
             }
             
         }
         
     }  
     return $result;
   }
   protected function updateUserCartProduct($productListData,$user_id){
       foreach($productListData as $key => $val){
            $params['productId'] = $key;
            $params['user_id'] = $user_id;
            $params['item_type'] = '4';
            $params['product_mapping_id'] = "";
            $params['num_quantity'] = $val;
            $params['source_type'] = "frontend";
            $params['user_session_id'] = session_id();
            $productStatus = $this->getServiceLocator()->get('Transaction\Model\TransactionTable')->updateCartInventoryProduct($params);
       }
       return true;
   }

}
