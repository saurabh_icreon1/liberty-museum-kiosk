<?php

/**
 * This controller is used to display user membership
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Controller;

use Base\Controller\BaseController;
use Zend\View\Model\ViewModel;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Zend\Mvc\Controller\Plugin\Params;
use Zend\Http\Request;
use Zend\Authentication\AuthenticationService;
use User\Model\Membership;
use User\Form\MembershipForm;

/**
 * This controller is used to display user membership
 * @package    User
 * @author     Icreon Tech - DG
 */
class MembershipController extends BaseController {

    protected $_membershipTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_authservice = null;

    public function __construct() {
        //if already login, redirect to success page
        $this->_flashMessage = $this->flashMessenger();
        $auth = new AuthenticationService();
        $this->auth = $auth;
    }

    public function indexAction() {
        
    }

    /**
     * This functikon is used to get table connections
     * @return     array 
     * @author Icreon Tech - DG
     */
    public function getTables() {
        $sm = $this->getServiceLocator();
        $this->_membershipTable = $sm->get('User\Model\MembershipTable');
        $this->_adapter = $sm->get('dbAdapter');
        return $this->_membershipTable;
    }

    /**
     * This functikon is used to get config variables
     * @return     array 
     * @author Icreon Tech - DG
     */
    public function getConfig() {
        $sm = $this->getServiceLocator();
        $this->_translator = $sm->get('translator');
        $this->_config = $sm->get('Config');
    }

    /**
     * This Action is used to get users membership
     * @author Icreon Tech-DG
     * @return Array
     */
    public function getUserMembershipAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getConfig();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $response = $this->getResponse();
        $messages = array();
        $params = $this->params()->fromRoute();
        $user_id = $params['user_id'];

        if ($request->isXmlHttpRequest() && $request->isPost()) {
            $messages = array();
            $searchParam = array();
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit;
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $searchParam['user_id'] = $this->decrypt($user_id);
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'membership.transaction_date') ? 'membership.transaction_date' : $searchParam['sort_field'];

            $searches_arr = $this->getTables()->getUserMemberships($searchParam);
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();

            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $jsonResult['page'] = $request->getPost('page');
            $jsonResult['records'] = $countResult[0]->RecordCount;
            $jsonResult['page'] = $request->getPost('page');
            $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
            if (!empty($searches_arr)) {
                foreach ($searches_arr as $val) {
                    if ($val['membership_card_shipped'] == '0') {
                     // if(isset($val['membership_card_date_from']) and trim($val['membership_card_date_from']) != "" and isset($val['membership_card_date_to']) and trim($val['membership_card_date_to']) != "" and date("Y", strtotime(trim($val['membership_card_date_from']))) == date("Y", strtotime(DATE_TIME_FORMAT)) and date("Y", strtotime(trim($val['membership_card_date_to']))) == date("Y", strtotime(DATE_TIME_FORMAT))) { 
                       // $membership_shipped = '<a class="link-underline" href="javascript:void(0);" onclick="updateShipped(\'' . $this->encrypt($val['user_membership_card_id']) . '\');">No</a>';
                      //}
                      //else {
                       $membership_shipped = "No";  
                     // }
                    } else {
                        $membership_shipped = "Yes";
                    }
                    
                    $membership_title = $val['membership_title'];
                    $membership_color = $val['level_color'];
                    $membership_level = '<div style=color:' . $membership_color . '>' . $membership_title . '</div>';

                    if($val['membership_id'] != "1") {
                        $arrCell['id'] = $val['user_membership_id'];
                        $arrCell['cell'] = array($membership_level, date("Y-m-d", strtotime($val['membership_date_from'])), date("Y-m-d", strtotime($val['membership_date_to'])));
                        $subArrCell[] = $arrCell;
                        $jsonResult['rows'] = $subArrCell;
                    }
                }
            }
            $response->setContent(\Zend\Json\Json::encode($jsonResult));
            return $response;
        } else {
            $viewModel->setVariables(array(
                'user_id' => $user_id,
                'jsLangTranslate' => $this->_config['user_messages']['config']['user_membership'])
            );
            return $viewModel;
        }
    }

    /**
     * This Action is used to update user membership
     * @author Icreon Tech-DG
     * @return Array
     * @param keyword
     */
    public function editUserMembershipsAction() {
        $this->checkUserAuthentication();
        $this->layout('popup');
        $response = $this->getResponse();
        $messages = array();
        $params = $this->params()->fromRoute();
        $postData['user_membership_id'] = $this->decrypt($params['membership_id']);
        $postData['membership_shipped'] = '1';
        $last_update = $this->getTables()->userUpdateMembership($postData);
        if ($last_update)
            $messages = array('status' => "success");
        else {
            $messages = array('status' => "error", 'message' => $this->_config['user_messages']['config']['user_membership']['UPDATED_ERROR']);
        }
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This Action is used to update user membership benefit
     * @author Icreon Tech-DG
     * @return Array
     * @param keyword
     */
    public function memberBenefitAction() {
        $this->getTables();
        $this->getConfig();

        $viewModel = new ViewModel();
        $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '';
        $postData[0] = '';
        $postData[1] = '';
        $postData[3] = 'asc';
        $postData[2] = 'tbl_membership.minimun_donation_amount';
        $membershipData = $this->getTables()->getAllMembership($postData);
        $postData[0] = '';
        $postData[1] = '';
        $postData[3] = 'asc';
        $postData[2] = 'tbl_membership.minimun_donation_amount';
        $postData['currentYear'] = date("Y", strtotime(DATE_TIME_FORMAT));
        $postData['isActive'] = "1";
        $membershipData = $this->getServiceLocator()->get('User\Model\MembershipTable')->getAllMembership($postData);
        $i = 0;
		foreach ($membershipData as $key=>$donationAmount) {
			if($donationAmount['minimun_donation_amount']=='25.00'){
                 unset($membershipData[$key]);
            }
            if ($donationAmount['membership_id'] != '1' && $donationAmount['minimun_donation_amount'] > 0) {
                if ($i == 0) {
                    $min_donation = $donationAmount['minimun_donation_amount'];
                } else {
                    if ($min_donation > $donationAmount['minimun_donation_amount']) {
                        $min_donation = $donationAmount['minimun_donation_amount'];
                    }
                }
                $i++;
            }
        }
		$viewModel->setVariables(array(
            'membershipData' => $membershipData,
            'user_id' => $user_id,
            'minDonation' => $min_donation,
            'financialYearDay' => $this->_config['financial_year']['srart_day'],
            'financialYearMonth' => $this->_config['financial_year']['srart_month'],
			'membership_plus' => $this->_config['membership_plus']
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to update user membership
     * @author Icreon Tech-DG
     * @return Array
     * @param keyword
     */
    public function getUserMembershipInfoAction() {
        $this->checkFrontUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $flashMessages = '';
        if ($this->_flashMessage->hasMessages()) {
            $flashMessages = $this->_flashMessage->getMessages();
        }
        $params = $this->params()->fromRoute();
        $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->decrypt($params['user_id'])));
//        $postData[0] = '';
//        $postData[1] = '';
//        $postData[3] = 'asc';
//        $postData[2] = 'tbl_membership.minimun_donation_amount';
//        $postData['currentYear'] = date("Y", strtotime(DATE_TIME_FORMAT));
//        $membershipData = $this->getServiceLocator()->get('User\Model\MembershipTable')->getAllMembership($postData);
        
        // s
        $postData[0] = '';
        $postData[1] = '';
        $postData[3] = 'asc';
        $postData[2] = 'tbl_membership.minimun_donation_amount';
        $postData['currentYear'] = date("Y", strtotime(DATE_TIME_FORMAT));
        $postData['isActive'] = "1";
        $membershipData = $this->getServiceLocator()->get('User\Model\MembershipTable')->getAllMembership($postData);
        
        // e
        
        if ($request->isPost()) {
            $formData = $request->getPost();
            $autoRenew = $formData['auto_renew'];
            $this->getTables()->updateAutoRenewMembership(array('auto_renew' => $autoRenew, 'user_id' => $userDetail['user_id']));
            $this->flashMessenger()->addMessage($this->_config['user_messages']['config']['edit_user_membership']['UPDATED_SUCCCESS']);
            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }

        $membershipAmt = 0;
        if (!empty($membershipData)) {
            foreach ($membershipData as $val) {
                if ($val['membership_id'] == $userDetail['membership_id']) {
                    $membershipAmt = $val['minimun_donation_amount'];
                }
            }
        }
        
        
       $i = 0;
        foreach ($membershipData as $key) {
            if ($key['membership_id'] != '1' && $key['minimun_donation_amount'] > 0) {
                if ($i == 0) {
                    $min_donation = $key['minimun_donation_amount'];
                } else {
                    if ($min_donation > $key['minimun_donation_amount']) {
                        $min_donation = $key['minimun_donation_amount'];
                    }
                }
                $i++;
            }
        }
        
        
        $viewModel->setVariables(array(
            'profileId' => $userDetail['profile_id'],
            'membershipId' => $userDetail['membership_id'],
            'membershipTitle' => $userDetail['membership_title'],
            'membershipExpire' => $userDetail['membership_expire'],
            'isAutoRenewal' => $userDetail['is_auto_renewal'],
            'userId' => $params['user_id'],
            'flashMessages' => $flashMessages,
            'membershipAmt' => $membershipAmt,
            'membershipData' => $membershipData,
            'user_id' => $userDetail['user_id'],
            'jsLangTranslate' => $this->_config['user_messages']['config']['edit_user_membership'],
            'minDonation' => $min_donation,
            'financialYearDay' => $this->_config['financial_year']['srart_day'],
            'financialYearMonth' => $this->_config['financial_year']['srart_month'],
            'membership_plus' => $this->_config['membership_plus']
                ));
        return $viewModel;
    }

    /**
     * This Action is used to alter user membership by user
     * @author Icreon Tech - AS
     * @return Array
     * @param keyword
     */
    public function memberBenefitEntryAction() {
        $this->getTables();
        $params = $this->params()->fromRoute();
        $MembershipForm = new MembershipForm();
        $viewModel = new ViewModel();
        $this->layout('web-popup');
        $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '';
        $postData[0] = '';
        $postData[1] = '';
        $postData[3] = 'asc';
        $postData[2] = 'tbl_membership.minimun_donation_amount';
        $membershipData = $this->getTables()->getAllMembership($postData);
        $minAmount = $this->decrypt($params['minAmount']);
        $membershipID = $this->decrypt($params['membershipID']);
        $viewModel->setVariables(array(
            'membershipData' => $membershipData,
            'user_id' => $user_id,
            'minAmount' => $minAmount,
            'membershipID' => $membershipID,
            'membershipForm' => $MembershipForm
                )
        );
        return $viewModel;
    }

}
