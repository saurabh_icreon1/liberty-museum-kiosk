<?php

/**
 * This controller is used to display FOF detail
 * @package    User
 * @author     Icreon Tech - NS
 */

namespace User\Controller;

use Base\Model\UploadHandler;
use Base\Controller\BaseController;
use Zend\Authentication\AuthenticationService;
use Zend\View\Model\ViewModel;
use User\Model\Fof;
use Zend\Mvc\Controller\Plugin\Params;
use Zend\Http\Request;
use User\Form\FofForm;
use User\Form\FofSearchForm;
use User\Form\FofSearchSaveForm;
use Zend\Session\Container;

/**
 * This controller is used to display user detail
 * @package    User
 * @author     Icreon Tech - NS
 */
class FofController extends BaseController {

    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_authservice = null;
    protected $_fofTable = null;
    protected $_userTable = null;
    protected $_cmsTable = null;
    protected $_productTable = null;

    public function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        $auth = new AuthenticationService();
        $this->auth = $auth;
    }

    /**
     * This function is used to get table connections
     * @return     array 
     * @author Icreon Tech - NS
     */
    public function getFofTables() {
        $sm = $this->getServiceLocator();
        $this->_fofTable = $sm->get('User\Model\FofTable');
        $this->_userTable = $sm->get('User\Model\UserTable');
        $this->_cmsTable = $sm->get('Cms\Model\CmsTable');
        $this->_productTable = $sm->get('Product\Model\ProductTable');
        $this->_adapter = $sm->get('dbAdapter');
        $this->_auth = new AuthenticationService();
        return $this->_fofTable;
    }

    /**
     * This function is used to get config variables
     * @return     array 
     * @author Icreon Tech - NS
     */
    public function getConfig() {
        $sm = $this->getServiceLocator();
        $this->_translator = $sm->get('translator');
        $this->_config = $sm->get('Config');
    }

    /**
     * This function is used to landing page of FOF
     * @return Index Page      
     * @author Icreon Tech - NS
     */
    public function indexFofAction() {

        $this->getFofTables();
        $this->getConfig();
        $jsLangTranslate = $this->_config['user_messages']['config']['user_fof'];

        $dataParam = array();
        $dataParam['fof_id'] = '';
        $dataParam['people_photo'] = '';
        $dataParam['record_limit'] = '';
        $arrFofImages = $this->getFofTables()->getFOFImages($dataParam);
        $fofDirThumb = $this->_config['file_upload_path']['assets_url'] . "fof/thumb/";
        $fofDir = $this->_config['file_upload_path']['assets_url'] . "fof/";
        $fofDirThumbRelative = $this->_config['file_upload_path']['assets_upload_dir'] . "fof/thumb/";

        $fofSearchForm = new FofSearchForm();
        $fofSearchForm->setAttribute('action', '/search-fof');
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'jsLangTranslate' => $jsLangTranslate,
            'fofSearchForm' => $fofSearchForm,
            'arrFofImages' => $arrFofImages,
            'fofDirThumb' => $fofDirThumb,
            'fofDir' => $fofDir,
            'fofDirThumbRelative' => $fofDirThumbRelative
        ));

        return $viewModel;
    }

    /**
     * This function is used to about FOF
     * @return About Us Page      
     * @author Icreon Tech - NS
     */
    public function aboutFofAction() {
        $this->getFofTables();
        $this->getConfig();
        $jsLangTranslate = $this->_config['user_messages']['config']['user_fof'];

        $searchParam = array();
        $searchParam['type'] = '';
        //  $searchParam['status'] = 1;
        $searchParam['content_id'] = '13';
        $searchParam['startIndex'] = "";
        $searchParam['recordLimit'] = "";
        $searchParam['sortField'] = "";
        $searchParam['sortOrder'] = "";
        $pageContentArr = $this->_cmsTable->getCrmContents($searchParam);
        $content_title = isset($pageContentArr[0]['content_title']) ? $pageContentArr[0]['content_title'] : '';
        $description = isset($pageContentArr[0]['description']) ? $pageContentArr[0]['description'] : '';


        $dataParam = array();
        $dataParam['fof_id'] = '';
        $dataParam['people_photo'] = '';
        $dataParam['record_limit'] = 56;
        $arrFofImagesSearch = $this->getFofTables()->getFOFImages($dataParam);
        if (isset($arrFofImagesSearch) and count($arrFofImagesSearch) > 0) {
            shuffle($arrFofImagesSearch);
        } else {
            $arrFofImagesSearch = array();
        }
        $fofDirThumb = $this->_config['file_upload_path']['assets_url'] . "fof/thumb/";
        $fofDir = $this->_config['file_upload_path']['assets_url'] . "fof/";


        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'translate' => $jsLangTranslate,
            'content_title' => $content_title,
            'description' => $description,
            'arrFofImagesSearch' => $arrFofImagesSearch,
            'fofDirThumb' => $fofDirThumb,
            'fofDir' => $fofDir
        ));
        return $viewModel;
    }

    /**
     * This function is used to get Fof Search
     * @return     array
     * @author Icreon Tech - NS
     */
    public function searchFofAction() {
        $this->getFofTables();
        $this->getConfig();

        $request = $this->getRequest();

        $chkClickStatus = false;
        $peoplePhoto = '';
        $searchName = $request->getPost('search_name');


        if (isset($searchName) and trim($searchName) != "") {
            $searchName = isset($searchName) ? $searchName : "";
            $peoplePhoto = trim($searchName);
            $chkClickStatus = true;
        } else {
            $chkClickStatus = false;
            $searchName = "";
        }

        $viewModel = new ViewModel();

        $fofSearchForm = new FofSearchForm();

        $fofSearchSaveForm = new FofSearchSaveForm();

        $jsLangTranslate = $this->_config['user_messages']['config']['user_fof'];


        if ($this->_auth->hasIdentity() == true and isset($this->_auth->getIdentity()->user_id)) {
            $data = array();
            $data['user_id'] = $this->_auth->getIdentity()->user_id;
            $data['fof_search_id'] = '';
            $savedSearches = $this->getFofTables()->getUserSearches($data);

            $memberShipConfig = $this->getMembershipId($this->_auth->getIdentity()->user_id, '');
            $numMemberFalgOfFaces = isset($memberShipConfig[4]) ? $memberShipConfig[4] : '';
        } else {
            $savedSearches = array();
            $numMemberFalgOfFaces = '';
        }



        $fofDirThumb = $this->_config['file_upload_path']['assets_url'] . "fof/thumb/";
        $fofDir = $this->_config['file_upload_path']['assets_url'] . "fof/";
        $fofDirThumbRelative = $this->_config['file_upload_path']['assets_upload_dir'] . "fof/thumb/";

        $request = $this->getRequest();
        $params = $this->params()->fromRoute();


        if (isset($params['fof_id']) and trim($params['fof_id']) != "") {
            $fof_id = trim($params['fof_id']);
            if ($fof_id != "0") {
                $fof_id = $this->decrypt($fof_id);
                $dataParam['fof_id'] = $fof_id;

                $dataParam3 = array();
                $dataParam3['fof_id'] = $fof_id;
                $dataParam3['people_photo'] = '';
                $dataParam3['record_limit'] = '';
                $arrFofImagesSearch = $this->getFofTables()->getFOFImages($dataParam3);
                $peoplePhoto = isset($arrFofImagesSearch[0]['people_photo']) ? $arrFofImagesSearch[0]['people_photo'] : '';

                $chkClickStatus = true;
            }
        }


        if (isset($params['fof_search_id']) and trim($params['fof_search_id']) != "") {
            $fof_search_id = trim($params['fof_search_id']);
            if ($fof_search_id != "0") {
                $fof_search_id = $this->decrypt($fof_search_id);
                $dataParam2 = array();
                $dataParam2['user_id'] = '';
                $dataParam2['fof_search_id'] = $fof_search_id;
                $fofSearches = $this->getFofTables()->getUserSearches($dataParam2);
                $fofSearchesResults = $fofSearches[0];
                $fofSearchesResults1 = unserialize($fofSearchesResults['search_query']);
                $search_name = isset($fofSearchesResults1['search_name']) ? $fofSearchesResults1['search_name'] : '';
                $dataParam['people_photo'] = $search_name;
                $peoplePhoto = $search_name;

                $chkClickStatus = true;
            }
        }


        $dataParam = array();
        $dataParam['fof_id'] = '';
        $dataParam['people_photo'] = '';
        $dataParam['record_limit'] = 208;
        $dataParam['start_index'] = 0;
        $arrFofImages = $this->getFofTables()->getFOFImages($dataParam);


        $fofSearchForm->get('search_name')->setAttribute('value', $peoplePhoto);
        $fofSearchForm->get('submitsearch')->setAttribute('type', 'button');
        $fofSearchSaveForm->get('save_search_as')->setAttribute('placeholder', 'Save this search');

        //  $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => $jsLangTranslate,
            'fofSearchForm' => $fofSearchForm,
            'fofSearchSaveForm' => $fofSearchSaveForm,
            'numMemberFalgOfFaces' => $numMemberFalgOfFaces,
            'savedSearches' => $savedSearches,
            'arrFofImages' => $arrFofImages,
            'fofDirThumb' => $fofDirThumb,
            'fofDir' => $fofDir,
            'peoplePhoto' => $peoplePhoto,
            'fofDirThumbRelative' => $fofDirThumbRelative,
            'chkClickStatus' => $chkClickStatus,
            'facebookAppId' => $this->_config['facebook_app']['app_id']
                )
        );

        return $viewModel;
    }

    
    
    /**
     * This function is used to show Fof Image
     * @return     array
     * @author Icreon Tech - NS
     */
    public function showFofImageAction() {
        try {
            $this->getFofTables();
            $this->getConfig();
            
            $params = $this->params()->fromRoute();
            $arrFofImagesM = array();
            if(isset($params['fof_param']) and trim($params['fof_param']) != "") {
                $dataParam3 = array();
                $dataParam3['fof_id'] = trim($params['fof_param']);
                $dataParam3['people_photo'] = '';
                $dataParam3['record_limit'] = '';
                $arrFofImages = $this->getFofTables()->getFOFImages($dataParam3);
                $arrFofImagesM = $arrFofImages[0]; 
            }    
           
            
            
           $fofImage = isset($arrFofImagesM['fof_image'])?$arrFofImagesM['fof_image']:'';
           $fofDir = $this->_config['file_upload_path']['assets_url'] . "fof/".$fofImage;
           $fofDirRelative = $this->_config['file_upload_path']['assets_upload_dir'] . "fof/".$fofImage;
        
           $fofMain = "";
           $imagewidth = "";
           $imageheight = "";
           if(isset($fofImage) and trim($fofImage) != "" and file_exists($fofDirRelative)) {
                $fofMain = $fofDir;
                list($imagewidth, $imageheight, $imageType) = @getimagesize($fofDirRelative);
                if($imagewidth == "" or $imagewidth == "0") { $imagewidth  = ""; }
                if($imageheight == "" or $imageheight == "0") { $imageheight  = ""; }
           }
           else {
               $fofMain = "";
               $imagewidth = "";
               $imageheight = "";
           }
           
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            
            $viewModel->setVariables(array(
                'arrFofImagesM' => $arrFofImagesM,
                'fofMain' => $fofMain,
                'imagewidth' => $imagewidth,
                'imageheight' => $imageheight
               )
            );
             return $viewModel;
        }
        catch(Exception $e) {
            
        }
    }
    
    
    
    /**
     * This function is used to get Fof Data
     * @return     array
     * @author Icreon Tech - NS
     */
    public function getFofDataAction() {
        try {
            $this->getFofTables();
            $this->getConfig();

            $request = $this->getRequest();
            $response = $this->getResponse();
            if ($request->isPost()) {

                $dataParam = array();
                $dataParam['fof_id'] = '';
                $dataParam['people_photo'] = $request->getPost('search_name');
                $dataParam['search_flag_status'] = '0';
                $dataParam['record_limit'] = 208;
                $startIndex = $request->getPost('start_index');
                if (isset($startIndex) and trim($startIndex) != "" and is_numeric(trim($startIndex)) and trim($startIndex) > 0) {
                    $dataParam['start_index'] = trim($startIndex) + 1;
                } else {
                    $dataParam['start_index'] = 0;
                    $dataParam['record_limit'] = 208;
                }

                $arrFofImages = $this->getFofTables()->getFOFImages($dataParam);

                $dataParam['search_flag_status'] = '1';
                $dataParam['record_limit'] = '';
                $countFofImages = $this->getFofTables()->getFOFImages($dataParam);
                if (isset($countFofImages) and count($countFofImages) > 0) {
                    $countFofRes = $countFofImages[0]['total_filtered_record'];
                    $countFofRes = isset($countFofRes) ? $countFofRes : "0";
                } else {
                    $countFofRes = "0";
                }

                $fofDirThumbRelative = $this->_config['file_upload_path']['assets_upload_dir'] . "fof/thumb/";

                $arrFofImages2 = array();
                foreach ($arrFofImages as $fofVal) {
                    if (isset($fofVal['fof_image']) and trim($fofVal['fof_image']) != "") {
                        $arrFofImages2[] = $fofVal;
                    }
                }


                $fofDirThumb = $this->_config['file_upload_path']['assets_url'] . "fof/thumb/";
                $fofDir = $this->_config['file_upload_path']['assets_url'] . "fof/";

                $finalArr = array('fofDirThumb' => $fofDirThumb, 'fofDir' => $fofDir, 'countFofRes' => $countFofRes, 'data' => $arrFofImages2);
                $searchResult2 = json_encode($finalArr);
                return $response->setContent($searchResult2);
            }
        } catch (Exception $e) {
            return true;
        }
    }

    /**
     * This function is used to get Fof Search
     * @return     array
     * @author Icreon Tech - NS
     */
    public function getFofSearchAction() {
        try {
            $this->getFofTables();
            $this->getConfig();

            $request = $this->getRequest();
            $response = $this->getResponse();
            if ($request->isPost()) {
                if ($request->getPost('fof_search_id') != '') {
                    $search_id = $request->getPost('fof_search_id');
                    $dataParam = array();
                    $dataParam['user_id'] = $this->_auth->getIdentity()->user_id;
                    $dataParam['fof_search_id'] = $this->decrypt($search_id);
                    $savedSearches = $this->getFofTables()->getUserSearches($dataParam);
                    $searchResult = $savedSearches[0];

                    $searchResult2 = json_encode(unserialize($searchResult['search_query']));
                    return $response->setContent($searchResult2);
                }
            }
        } catch (Exception $e) {
            return true;
        }
    }

    /**
     * This function is used to save Fof Search
     * @return     array
     * @author Icreon Tech - NS
     */
    public function saveFofSearchAction() {
        try {
            $this->getFofTables();
            $this->getConfig();

            $request = $this->getRequest();
            $response = $this->getResponse();
            $messages = array();


            if ($request->isPost()) {
                $searchName = $request->getPost('search_name');
                parse_str($request->getPost('searchString'), $searchParam);
                if (trim($searchName) != '') {
                    $saveSearchParam = array();
                    $saveSearchParam['user_id'] = $this->_auth->getIdentity()->user_id;
                    $saveSearchParam['title'] = $searchName;
                    $saveSearchParam['search_query'] = serialize($searchParam);
                    $saveSearchParam['save_date'] = DATE_TIME_FORMAT;
                    $saveSearchParam['modified_date'] = DATE_TIME_FORMAT;
                    $data = array();
                    $data['user_id'] = $this->_auth->getIdentity()->user_id;
                    $data['fof_search_id'] = '';
                    $savedSearches = $this->getFofTables()->getUserSearches($data);
                    $memberShipConfig = $this->getMembershipId($this->_auth->getIdentity()->user_id, '');
                    $numMemberFalgOfFaces = isset($memberShipConfig[4]) ? $memberShipConfig[4] : '';
                    if($numMemberFalgOfFaces <= count($savedSearches)){
                        $messages = array('status' => "exceed");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                   }elseif ($this->getFofTables()->saveUserSearches($saveSearchParam) == true) {
                        $jsLangTranslate = $this->_config['user_messages']['config']['user_fof'];
                        // start 
                        if ($this->_auth->hasIdentity() == true and isset($this->_auth->getIdentity()->user_id)) {
                            $data = array();
                            $data['user_id'] = $this->_auth->getIdentity()->user_id;
                            $data['fof_search_id'] = '';
                            $savedSearches = $this->getFofTables()->getUserSearches($data);
                            $memberShipConfig = $this->getMembershipId($this->_auth->getIdentity()->user_id, '');
                            $numMemberFalgOfFaces = isset($memberShipConfig[4]) ? $memberShipConfig[4] : '';
                            
                            $htmlData = '';
                            if (count($savedSearches) > 0 and $savedSearches != false) {
                                if (isset($savedSearches) and count($savedSearches) < 4) {
                                    $scrollCls = '';
                                } else {
                                    $scrollCls = 'scroll-pane featured-scroll';
                                }
                                $htmlData = '<div class="dropdown">';
                                $htmlData .= '<div class="' . $scrollCls . '">';
                                $htmlData .= '<ul>';
                                foreach ($savedSearches as $vals) {
                                    $htmlData .= '<li>';
                                    $htmlData .= '<span>' . $vals['title'] . '</span>';
                                    $htmlData .= '<a href="#deletesearch" class="deletesearch sn-close" onClick="deleteSaveSearch(' . "'" . $this->encrypt($vals['fof_search_id']) . "'" . ');"></a>';
                                    $htmlData .= '<a class="sn-search" onclick="viewSavedSearch(' . "'" . $this->encrypt($vals['fof_search_id']) . "'" . ');"></a>';
                                    $htmlData .= '</li>';
                                }
                                $htmlData .= '</ul>';
                                $htmlData .= '</div>';
                                $htmlData .= '</div>';
                            } else {
                                $htmlData = '';
                            }
                        } else {
                            $htmlData = '';
                        }
                        // end


                        $messages = array('status' => "success", 'message' => $jsLangTranslate['FOF_SEARCH_SUCCESS_MSG'], 'dataresult' => $htmlData, 'countresult' => count($savedSearches));
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                }
            }
        } catch (Exception $e) {
            return true;
        }
    }

    /**
     * This function is used to delete Fof Search
     * @return     array
     * @author Icreon Tech - NS
     */
    public function deleteFofSearchAction() {
        try {
            $this->getFofTables();
            $this->getConfig();
            $response = $this->getResponse();
            $request = $this->getRequest();
            if ($request->isPost() and $request->getPost('fof_search_id') != "") {
                $fof_search_id = $request->getPost('fof_search_id');
                $fof_search_id = trim($fof_search_id);
                $dataParam = array();
                $dataParam['user_id'] = $this->_auth->getIdentity()->user_id;
                $dataParam['is_delete'] = 1;
                $dataParam['search_id'] = $this->decrypt($fof_search_id);
                $this->_userTable->deleteFofSearch($dataParam);


                $jsLangTranslate = $this->_config['user_messages']['config']['user_fof'];
                // start 
                if ($this->_auth->hasIdentity() == true and isset($this->_auth->getIdentity()->user_id)) {
                    $data = array();
                    $data['user_id'] = $this->_auth->getIdentity()->user_id;
                    $data['fof_search_id'] = '';
                    $savedSearches = $this->getFofTables()->getUserSearches($data);

                    $htmlData = '';
                    if (count($savedSearches) > 0 and $savedSearches != false) {
                        if (isset($savedSearches) and count($savedSearches) < 4) {
                            $scrollCls = '';
                        } else {
                            $scrollCls = 'scroll-pane featured-scroll';
                        }
                        $htmlData = '<div class="dropdown">';
                        $htmlData .= '<div class="' . $scrollCls . '">';
                        $htmlData .= '<ul>';
                        foreach ($savedSearches as $vals) {
                            $htmlData .= '<li>';
                            $htmlData .= '<span>' . $vals['title'] . '</span>';
                            $htmlData .= '<a href="#deletesearch" class="deletesearch sn-close" onClick="deleteSaveSearch(' . "'" . $this->encrypt($vals['fof_search_id']) . "'" . ');"></a>';
                            $htmlData .= '<a class="sn-search" onclick="viewSavedSearch(' . "'" . $this->encrypt($vals['fof_search_id']) . "'" . ');"></a>';
                            $htmlData .= '</li>';
                        }
                        $htmlData .= '</ul>';
                        $htmlData .= '</div>';
                        $htmlData .= '</div>';
                    } else {
                        $htmlData = '';
                    }
                } else {
                    $htmlData = '';
                }
                // end
                $messages = array('status' => "success", 'message' => $jsLangTranslate['FOF_SEARCH_SUCCESS_DELETED_MSG'], 'dataresult' => $htmlData, 'countresult' => count($savedSearches));
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        } catch (Exception $e) {
            return true;
        }
    }

    /**
     * This function is used to add Fof Image
     * @return     array
     * @author Icreon Tech - NS
     */
    public function addFofImageAction() {
        //$this->checkFrontUserAuthentication();
        $this->getFofTables();
        $this->getConfig();

        $user_id = '';
        $name = '';
        if (!isset($this->_auth->getIdentity()->user_id)) {
            $user_id = '';
            $name = '';
        }


        $discountVal = "0";
        if (isset($this->_auth->getIdentity()->user_id)) {
            $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->_auth->getIdentity()->user_id));


            if (isset($userDetail['membership_discount']) and trim($userDetail['membership_discount']) != "") {
                $discountVal = trim($userDetail['membership_discount']);
            }

            $user_id = $this->_auth->getIdentity()->user_id;
            if (isset($userDetail['user_type']) and trim($userDetail['user_type']) == "1") {
                $name = $userDetail['first_name'] . ' ' . $userDetail['last_name'];
            } else {
                $name = $userDetail['company_name'];
            }
        }

        $productParam2 = array();
        $productParam2['productType'] = 4;
        $productResultType = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getProductAttributeDetails($productParam2);


        $productSellPrice = "0.00";
        $isMembershipDiscount = "0";
        if (isset($productResultType) and count($productResultType) > 0) {
            $countResult = count($productResultType);
            $fofProductInfo = $productResultType[$countResult - 1];

            $productSellPrice = isset($fofProductInfo['product_sell_price']) ? $fofProductInfo['product_sell_price'] : "0.00";
            $isMembershipDiscount = isset($fofProductInfo['is_membership_discount']) ? $fofProductInfo['is_membership_discount'] : "0";
        }



        if ($isMembershipDiscount == "1" and $discountVal != "0") {
            $productSellPrice = $productSellPrice - $productSellPrice * $discountVal / 100;
            if ($productSellPrice > 0) {
                $productSellPrice = number_format($productSellPrice, 2);
            }
        }

        $viewModel = new ViewModel();
        $fofForm = new FofForm();
        if ($name != '') {
            $fofForm->get('donated_by')->setAttribute('value', $name);
            $fofForm->get('donated_by_edit')->setAttribute('value', $name);
        }

        $jsLangTranslate = $this->_config['user_messages']['config']['user_fof'];

        // $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => $jsLangTranslate,
            'fofForm' => $fofForm,
            'productSellPrice' => $productSellPrice,
            'nameUserDonatedBy' => $name));
        return $viewModel;
    }

    /**
     * This function is used to checkJsonFormatReturnString
     * @return     array
     * $get - 1 : names ("," Seperated) , 2 : Array (if json), 3 : Return Json
     * @author Icreon Tech - NS
     */
    private function checkJsonFormatReturnString($string, $get) {
        try {
            $strTxt = $string;
            $flag = json_decode($string) != null;

            // if string is Json else return string
            if ($flag) {
                $strTxt = json_decode($string);
                if ($get == "1") {
                    $arrNames = array();
                    foreach ($strTxt as $valST) {
                        if (isset($valST->name) and trim($valST->name) != "") {
                            array_push($arrNames, trim($valST->name));
                        }
                    }
                    if (count($arrNames) > 0) {
                        $strTxt = implode(", ", $arrNames);
                    } else {
                        $strTxt = "";
                    }
                } else if ($get == "3") {
                    $strTxt = $string;
                }
            } else {
                $strTxt = isset($string) ? trim($string) : '';
            }

            return $strTxt;
        } catch (Exception $e) {
            return $string;
        }
    }

    /**
     * This function is used to add Fof Image Process
     * @return     array
     * @author Icreon Tech - NS
     */
    public function addFofImageProcessAction() {

        try {
            $this->getConfig();
            $this->getFofTables();
            $request = $this->getRequest();
            $response = $this->getResponse();
            $fofObj = new Fof($this->_adapter);

            $fofListPagesMessages = $this->_config['user_messages']['config']['user_fof'];

            $params = array();
            $params['image_name'] = $request->getPost('image_name');
            $params['fof_image'] = $request->getPost('fof_image');
            $params['is_photo_uploaded'] = $request->getPost('is_photo_uploaded');
            $params['donated_by'] = $request->getPost('donated_by');
            $params['people_photo'] = $request->getPost('people_photo');
            $params['is_people_photo_visible'] = $request->getPost('is_people_photo_visible');
            $params['photo_caption'] = $request->getPost('photo_caption');
            $params['is_photo_caption_visible'] = $request->getPost('is_photo_caption_visible');
            $params['is_agree'] = $request->getPost('is_agree');

            $fofForm = new FofForm();
            $fofForm->setInputFilter($fofObj->getInputFilterAds($params));
            $fofForm->setData($request->getPost());

            if (!$fofForm->isValid()) {
                $errors = $fofForm->getMessages();

                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'Add to Cart') {
                        foreach ($row as $rower) {
                            $msg[$key] = isset($fofListPagesMessages[$rower]) ? $fofListPagesMessages[$rower] : '';
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $postData = $fofForm->getData();

                $user_id = '';
                if (!isset($this->_auth->getIdentity()->user_id)) {
                    $user_id = '';
                } else {
                    $user_id = $this->_auth->getIdentity()->user_id;
                }

                $postData['user_id'] = $user_id;
                $postData['user_session_id'] = session_id();
                $postData['added_by'] = 0;
                $postData['added_date'] = DATE_TIME_FORMAT;
                $postData['product_mapping_id'] = 2;
                $postData['product_id'] = $this->getFofTables()->getUserFofProductId(array('product_type_id' => '4'));
                $postData['product_type_id'] = 4;
                $productParam = array();
                $productParam['productType'] = 4;/** product */
                
                $proDetails = array();
                $proDetails['id'] = $postData['product_id'];
                $productResultAttr = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProductInfo($proDetails);
               // print_r($productResultAttr); die();

                $productResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getProductAttributeDetails($productParam);
                $postData['product_sku'] = $productResultAttr[0]['product_sku']; //$productResult[0]['product_sku'];/** product */
                $postData['people_photo_json'] = $postData['people_photo'];
                $postData['people_photo'] = $this->checkJsonFormatReturnString($postData['people_photo'], 1);

                $filterFofData = $fofObj->exchangeArray($postData);
                
                
                
                /*add campaign,promotion and user group id in cart start*/
                $promotionId="";
                $couponCode="";
                $user_group_id="";
               
                $this->campaignSession = new Container('campaign');
                $campaign_code = (isset($this->campaignSession->campaign_code) &&!empty($this->campaignSession->campaign_code) ? $this->campaignSession->campaign_code : "");
               
                $campaign_id = isset($this->campaignSession->campaign_id) &&!empty($this->campaignSession->campaign_id) ? $this->campaignSession->campaign_id : 0;
                $channel_id = (isset($this->campaignSession->channel_id) &&!empty($this->campaignSession->channel_id) ?                 $this->campaignSession->channel_id : 0);
                 
                if($campaign_id !=0 && $channel_id !=0)
                {
                    $campResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getChannelPromo(array('campaign_id' => $campaign_id, 'channel_id' => $channel_id));
                
                    if (isset($campResult[0]['promotion_id']) &&!empty($campResult[0]['promotion_id']))
                    {
                        $couponCode = $campResult[0]['coupon_code'];
                        $promotionId = $campResult[0]['promotion_id'];
                    }
                    else
                    {
                        $couponCode= "";
                        $promotionId = 0;
                    }
                    
                    $user_id = isset($this->auth->getIdentity()->user_id) ? $this->auth->getIdentity()->user_id : '';
                    if (isset($user_id) && $user_id != '')
                    {
                        $searchParam['user_id'] = $user_id;
                        $searchParam['campaign_id'] = $campaign_id;
                        $cmTable = $this->getServiceLocator()->get('Campaign\Model\CampaignTable');
                        $campaign_arr = $cmTable->getUserCampaigns($searchParam);
                        if($campaign_arr && isset($campaign_arr[0]['group_id']))
                        {
                            $user_group_id = $campaign_arr[0]['group_id'];
                        }
                        //echo "<pre>";var_dump($campaign_arr);die;
                    }
                }
                
                $filterFofData['campaign_id']=$campaign_id;
                $filterFofData['campaign_code']=$campaign_code;
                $filterFofData['promotion_id']=$promotionId;
                $filterFofData['coupon_code']=$couponCode;
                $filterFofData['user_group_id']=$user_group_id;
                //echo "<pre>";var_dump($filterFofData);die;
                /*add campaign,promotion and user group id in cart ends*/
                
                
                
                $returnData = $this->getFofTables()->insertUserFof($filterFofData);

                if ($returnData == true) {

                    $temp_image = $request->getPost('temp_image');

                    if (isset($params['fof_image']) and trim($params['fof_image']) != "") {
                        if (isset($temp_image) and trim($temp_image) != "") {
                            $temp_image = trim($temp_image);
                        } else {
                            $temp_image = '';
                        }

                        $this->moveFileImageToTemp($params['fof_image'],$temp_image);
                    }

                    $messages = array('status' => "success");
                } else {
                    $messages = array('status' => "error");
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This action is used for uploading image
     * @return json format data
     * @param void
     * @author Icreon Tech - NS
     */
    public function uploadUserFofImageAction() {
        try {
            $_FILES['upload_photo']['name'] = (isset($_FILES['upload_photo']['name']) and trim($_FILES['upload_photo']['name']) != "") ? $_FILES['upload_photo']['name'] : "";
            $filename_orginal = $_FILES['upload_photo']['name'];
            $fileExt = $this->GetFileExt($_FILES['upload_photo']['name']);
            $filename = $this->GenerateMediaFileName($fileExt[1]);    //Generate file name
            $_FILES['upload_photo']['name'] = $filename;                 // assign name to file variable
            $this->getConfig();
            $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration
            $error_messages = array(
                'post_max_size' => 'The uploaded file exceeds the post_max_size directive in php.ini', // Set error messages
                'max_file_size' => 'File is too big',
                'min_file_size' => 'File is too small',
                'accept_file_types' => 'Filetype not allowed',
                'min_height_width' => 'Minimum image width or height should be ' . $this->_config['file_upload_path']['user_fof_medium_width'] . 'px'
            );

            $options = array(
                'upload_dir' => $upload_file_path['temp_upload_dir'],
                'param_name' => 'upload_photo', //file input name                      // Set configuration
                'inline_file_types' => $upload_file_path['file_types_allowed'],
                'accept_file_types' => $upload_file_path['file_types_allowed'],
                'max_file_size' => $upload_file_path['max_allowed_file_size'],
                'min_file_size' => $upload_file_path['min_allowed_file_size'],
                'min_height_width' => $this->_config['file_upload_path']['user_fof_medium_width'],
                'image_versions' => array(
                    'fof' => array(
                        'max_width' => $this->_config['file_upload_path']['user_fof_main_width'],
                        'max_height' => $this->_config['file_upload_path']['user_fof_main_height']
                    ),
                    'small' => array(
                        'max_width' => $this->_config['file_upload_path']['user_fof_medium_width'],
                        'max_height' => $this->_config['file_upload_path']['user_fof_medium_height']
                    ))
            );

            $upload_handler = new UploadHandler($options, true, $error_messages);
            $file_response = $upload_handler->jsonResponceData;
            if (isset($file_response['upload_photo'][0]->error)) {
                $messages = array('status' => "error", 'message' => $file_response['upload_photo'][0]->error);
            } else {
                
                $temp_dir_fof_copyfrom = $this->_config['file_upload_path']['temp_upload_dir'].'fof/'.$file_response['upload_photo'][0]->name;
                $temp_dir_copyto = dirname($_SERVER['DOCUMENT_ROOT']) . '/public/js/pixenate/cache/'.$file_response['upload_photo'][0]->name; 
                @copy($temp_dir_fof_copyfrom, $temp_dir_copyto);
                @chmod($temp_dir_copyto, 0777);
                
                $messages = array('status' => "success", 'message' => 'Image uploaded', 'encryptfilename' => $this->encrypt($file_response['upload_photo'][0]->name), 'filename' => $file_response['upload_photo'][0]->name, 'temp_upload_medium_dir' => $this->_config['file_upload_path']['assets_url'] . 'temp/fof/', 'full_path_pixenate' => $this->encrypt($this->_config['js_file_path']['path'] . '/js/pixenate/cache/' . $file_response['upload_photo'][0]->name), 'full_path' => $this->encrypt($this->_config['file_upload_path']['assets_url'] . 'temp/fof/' . $file_response['upload_photo'][0]->name), 'encrypted_filename' => $this->encrypt($file_response['upload_photo'][0]->name), 'image_name' => $filename_orginal, 'preview_path' => $this->encrypt($this->_config['file_upload_path']['assets_url'] . 'temp/fof/' . $file_response['upload_photo'][0]->name), 'temp_upload_small_image_dir' => $this->_config['file_upload_path']['assets_url'] . 'temp/small/', 'temp_upload_small_image_path_dir' => $this->encrypt($this->_config['file_upload_path']['assets_url'] . 'temp/small/' . $file_response['upload_photo'][0]->name));
            }
            $response = $this->getResponse();
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        } catch (Exception $e) {
            return true;
        }
    }

    /**
     * This function is used to upload User Fof Image Iframe
     * @return     array
     * @author Icreon Tech - NS
     */
    public function uploadUserFofImageIframeAction() {

        $this->getConfig();
        $viewModel = new ViewModel();
        //  $this->layout('popup');
        $jsLangTranslate = $this->_config['user_messages']['config']['user_fof'];

        $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration

        $param = $this->params()->fromRoute();

        if (isset($param['image_path']) and trim($param['image_path']) != "") {
            $image_path = $this->decrypt($param['image_path']);  
        } else {
            $image_path = '';
        }

        if (isset($param['image_name']) and trim($param['image_name']) != "") {
            $image_name = $this->decrypt($param['image_name']);
            $upload_file_path_fof = $this->encrypt($this->_config['js_file_path']['path'] . '/js/pixenate/cache/' . $image_name);
            $image_path_small = $this->_config['file_upload_path']['assets_url'] . 'temp/small/' . $image_name;
        } else {
            $image_name = '';
            $upload_file_path_fof = ''; //$this->encrypt('/images/fof-profile-large1.jpg');
            $image_path_small = '';
        }
        $crm_user_id = '';
        $auth = new AuthenticationService();
        $this->auth = $auth;
        $userDetail = (array) $this->auth->getIdentity();
        $crm_user_id = isset($userDetail['crm_user_id']) ? $userDetail['crm_user_id'] : '';


        $request = $this->getRequest();
        $IntRotate = $request->getPost('IntRotate');
        $IntRotate = isset($IntRotate) ? $IntRotate : 0;

        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => $jsLangTranslate,
            'image_path' => $image_path,
            'image_name' => $image_name,
            'int_rotate' => $IntRotate,
            'upload_file_path_fof' => $upload_file_path_fof,
            'crm_user_id' => $crm_user_id,
            'image_path_small' => $image_path_small
                )
        );
        return $viewModel;
    }

    /**
     * This function is used to show upload User Fof Image Iframe Preview
     * @return     array
     * @author Icreon Tech - NS
     */
    public function uploadUserFofImageIframePreviewAction() {

        $this->getConfig();
        $viewModel = new ViewModel();
        $param = $this->params()->fromRoute();

        $image_path1 = '';
         
        if (isset($param['image_path']) and trim($param['image_path']) != "") {
            if (isset($param['image_flag']) and trim($param['image_flag']) == "1") {
				$jsFilePath = $this->_config['js_file_path']['path'];
               $image_path = '/js/pixenate/cache/' . $this->decrypt(trim($param['image_path']));
                //$image_path1 = 'http://' . $_SERVER['SERVER_NAME'].'/js/pixenate/cache/'.$this->decrypt(trim($param['image_path']));
               $image_path1 = dirname($_SERVER['DOCUMENT_ROOT']) . '/js/pixenate/cache/'. $this->decrypt(trim($param['image_path']));
                  
            } else if (isset($param['image_flag']) and trim($param['image_flag']) == "2") {
                $image_path = $this->decrypt(trim($param['image_path']));
                $image_path1 = $this->decrypt(trim($param['image_path']));
            } else {
                $image_path = '';
                $image_path1 = '';
            }
        } else {
            $image_path = '';
            $image_path1 = '';
        }

        
      
//        $imagewidth = "";
//        $imageheight = "";       
//        if(isset($image_path1) and trim($image_path1) != "") {
//           list($imagewidth, $imageheight, $imageType) = @getimagesize($image_path1);
//           if($imagewidth == "" or $imagewidth == "0") { $imagewidth  = ""; }
//           if($imageheight == "" or $imageheight == "0") { $imageheight  = ""; }
//        }        
        
       
        
        $imagewidth = "";
        $imageheight = "";

        $crm_user_id = '';
        $auth = new AuthenticationService();
        $this->auth = $auth;
        $userDetail = (array) $this->auth->getIdentity();
        $crm_user_id = isset($userDetail['crm_user_id']) ? $userDetail['crm_user_id'] : '';

        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'image_path' => $image_path,
            'crm_user_id' => $crm_user_id,
            'imagewidth' => $imagewidth,
            'imageheight' => $imageheight
                )
        );
        return $viewModel;
    }

    
    /**
     * This function is used to upload User Fof Image Iframe CRM
     * @return     array
     * @author Icreon Tech - NS
     */
    public function uploadUserFofImageIframeCRMAction() {

        $this->getConfig();
        $viewModel = new ViewModel();
        $jsLangTranslate = $this->_config['user_messages']['config']['user_fof'];
        $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration

        $param = $this->params()->fromRoute();

        if(isset($param['fof_id']) and $param['fof_id'] != "none") {
          $fofData = array();
          $fofData['fof_id'] = $this->decrypt($param['fof_id']);
          $fofResult = $this->getServiceLocator()->get('User\Model\UserTable')->getFof($fofData);
        }
        else {
          $fofResult = array();
        }
        
 
  

      /*
        try {
            $temp_dir_fof_copyfrom = dirname($_SERVER['DOCUMENT_ROOT']) . '/public/js/pixenate/cache/' . $fofResult[0]['fof_image'];
            $temp_dir_copyto = dirname($_SERVER['DOCUMENT_ROOT']) . '/public/js/pixenate/cache/' . $fofResult[0]['fof_image'];
            @copy($temp_dir_fof_copyfrom, $temp_dir_copyto);
            @chmod($temp_dir_copyto, 0777);
         } 
         catch (Exception $e) { }
       */  
         
 
        if (isset($fofResult[0]['fof_image']) and trim($fofResult[0]['fof_image']) != "" and trim($fofResult[0]['fof_image']) == $this->decrypt($param['fof_modified_image'])) {
            $image_path = $this->_config['js_file_path']['path'].'/js/pixenate/cache/'.$fofResult[0]['fof_image'];
            //$this->_config['file_upload_path']['assets_url'] . 'fof/' 
        }
        else if (isset($param['fof_modified_image']) and trim($param['fof_modified_image']) != "" and trim($fofResult[0]['fof_image']) != $this->decrypt($param['fof_modified_image'])) {
            $image_path = $this->_config['js_file_path']['path'].'/js/pixenate/cache/'.$this->decrypt($param['fof_modified_image']);
            //$this->_config['js_file_path']['path']
             //$this->_config['file_upload_path']['assets_url'] . 'temp/fof/' 
        } 
        else {
            $image_path = '';
        }
        
       // if(isset($image_path) and trim($image_path) != "") {
           //  $imageContent = @file_get_contents($image_path);
        //}
        
        $imagewidth = "";
        $imageheight = "";
//        if(isset($image_path) and trim($image_path) != "") {
//            list($imagewidth, $imageheight, $imageType) = @getimagesize($image_path);
//            if($imagewidth == "" or $imagewidth == "0") { $imagewidth  = ""; }
//            if($imageheight == "" or $imageheight == "0") { $imageheight  = ""; }
//        }
        
        
        if (isset($fofResult[0]['fof_image']) and trim($fofResult[0]['fof_image']) != "" and trim($fofResult[0]['fof_image']) == $this->decrypt($param['fof_modified_image'])) {
            $image_name = $fofResult[0]['fof_image'];
        }
        else if (isset($param['fof_modified_image']) and trim($param['fof_modified_image']) != "" and trim($fofResult[0]['fof_image']) != $this->decrypt($param['fof_modified_image'])) {
             $image_name = $this->decrypt($param['fof_modified_image']);   
        }
        else {
            $image_name = '';
        }
        
        $crm_user_id = '';
        $auth = new AuthenticationService();
        $this->auth = $auth;
        $userDetail = (array) $this->auth->getIdentity();
        $crm_user_id = isset($userDetail['crm_user_id']) ? $userDetail['crm_user_id'] : '';

        $request = $this->getRequest();
        $IntRotate = $request->getPost('IntRotate');
        $IntRotate = isset($IntRotate) ? $IntRotate : 0;
        
        $encryptedFofId = (isset($fofResult[0]['fof_id']) and trim($fofResult[0]['fof_id']) != "") ? $this->encrypt($fofResult[0]['fof_id']) : "";

        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => $jsLangTranslate,
            'image_path' => $image_path,
            'image_name' => $image_name,
            'int_rotate' => $IntRotate,
            'crm_user_id' => $crm_user_id,
            'encrypted_fof_id' => $encryptedFofId
          )
        );
        return $viewModel;
    }
    
    /**
     * This function is used to show upload User Fof Image Iframe Preview CRM
     * @return     array
     * @author Icreon Tech - NS
     */
    public function uploadUserFofImageIframePreviewCRMAction() {

        $this->getConfig();
        $viewModel = new ViewModel();
        $param = $this->params()->fromRoute();
        
        if(isset($param['fof_id']) and $param['fof_id'] != "none") {
           $fofData = array();
           $fofData['fof_id'] = $this->decrypt($param['fof_id']);
           $fofResult = $this->getServiceLocator()->get('User\Model\UserTable')->getFof($fofData);
        }
        else {
          $fofResult = array();
         }
        
         $flag_status = (isset($param['flag_status']) and trim($param['flag_status']) != "") ? $param['flag_status'] : "";

        if (isset($fofResult[0]['fof_image']) and trim($fofResult[0]['fof_image']) != "" and trim($fofResult[0]['fof_image']) == $this->decrypt($param['fof_modified_image']) and $flag_status == "1") {
            $image_path = $this->_config['js_file_path']['path'].'/js/pixenate/cache/'.$fofResult[0]['fof_image'];
            $image_path1 = dirname($_SERVER['DOCUMENT_ROOT']).'/public/js/pixenate/cache/'.$fofResult[0]['fof_image'];
           // $image_path = $this->_config['file_upload_path']['assets_url'] . 'fof/' .$fofResult[0]['fof_image'];
           // $image_path1 = $this->_config['file_upload_path']['assets_upload_dir'] . 'fof/' .$fofResult[0]['fof_image'];
        }
        else if (isset($param['fof_modified_image']) and trim($param['fof_modified_image']) != "" and trim($fofResult[0]['fof_image']) != $this->decrypt($param['fof_modified_image'])  and $flag_status == "1") {
            $image_path = $this->_config['js_file_path']['path'].'/js/pixenate/cache/'.$this->decrypt($param['fof_modified_image']);
            //$this->_config['file_upload_path']['assets_url'].'temp/fof/'
            $image_path1 =  dirname($_SERVER['DOCUMENT_ROOT']).'/public/js/pixenate/cache/'.$this->decrypt($param['fof_modified_image']);
            //$this->_config['js_file_path']['path'].'/temp/fof/'.$this->decrypt($param['fof_modified_image']);
        } 
        else if (isset($param['fof_modified_image']) and trim($param['fof_modified_image']) != "" and trim($fofResult[0]['fof_image']) != $this->decrypt($param['fof_modified_image'])  and $flag_status == "2") {
            $image_path = $this->_config['js_file_path']['path'].'/js/pixenate/cache/' . $this->decrypt($param['fof_modified_image']);
            $image_path1 = dirname($_SERVER['DOCUMENT_ROOT']).'/public/js/pixenate/cache/'.$this->decrypt($param['fof_modified_image']);
        } 
        else if ($flag_status == "5") {
            if (isset($fofResult[0]['fof_image']) and trim($fofResult[0]['fof_image']) != "" and trim($fofResult[0]['fof_image']) == $this->decrypt($param['fof_modified_image']) and $flag_status == "1") {
              // $image_path = $this->_config['file_upload_path']['assets_url'] . 'fof/' .$fofResult[0]['fof_image'];
              // $image_path1 = $this->_config['file_upload_path']['assets_upload_dir'] . 'fof/' .$fofResult[0]['fof_image'];
               
                $image_path = $this->_config['js_file_path']['path'].'/js/pixenate/cache/'.$fofResult[0]['fof_image'];
                $image_path1 = dirname($_SERVER['DOCUMENT_ROOT']).'/public/js/pixenate/cache/'.$fofResult[0]['fof_image'];
            }
            else if (isset($param['fof_modified_image']) and trim($param['fof_modified_image']) != "" and trim($fofResult[0]['fof_image']) != $this->decrypt($param['fof_modified_image'])  and $flag_status == "2") {
               $image_path = $this->_config['js_file_path']['path'].'/js/pixenate/cache/' . $this->decrypt($param['fof_modified_image']);
               $image_path1 = dirname($_SERVER['DOCUMENT_ROOT']).'/public/js/pixenate/cache/'.$this->decrypt($param['fof_modified_image']);
            } 
            else {
                $image_path = '';
                $image_path1 = '';
            }
        }
        else {
            $image_path = '';
            $image_path1 = '';
        }
        
        $imagewidth = "";
        $imageheight = "";
//        if(isset($image_path1) and trim($image_path1) != "") {
//            list($imagewidth, $imageheight, $imageType) = @getimagesize($image_path1);
//            if($imagewidth == "" or $imagewidth == "0") { $imagewidth  = ""; }
//            if($imageheight == "" or $imageheight == "0") { $imageheight  = ""; }
//        }
          
        $crm_user_id = '';
        $auth = new AuthenticationService();
        $this->auth = $auth;
        $userDetail = (array) $this->auth->getIdentity();
        $crm_user_id = isset($userDetail['crm_user_id']) ? $userDetail['crm_user_id'] : '';
 
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'old_img' => (isset($fofResult[0]['fof_image']) and trim($fofResult[0]['fof_image']) != "") ? trim($fofResult[0]['fof_image']) : "",
            'new_img' => (isset($param['fof_modified_image']) and trim($param['fof_modified_image']) != "") ? $this->decrypt(trim($param['fof_modified_image'])) : "",
            'image_path' => $image_path,
            'crm_user_id' => $crm_user_id,
            'imagewidth' => $imagewidth,
            'imageheight' => $imageheight,
            'people_photo_json' => (isset($fofResult[0]['people_photo_json']) and trim($fofResult[0]['people_photo_json']) != "") ? trim($fofResult[0]['people_photo_json']) : ""
         )
        );
        return $viewModel;
    }
    
    
    /**
    * This function is used to small image view fof
    * @return     encrypted text
    * @author Icreon Tech - NS
    */
    public function smallImageViewFofAction() {
        try {
            $this->getConfig();
            $param = $this->params()->fromRoute();

            $image_path1 = '';
            if (isset($param['image_path']) and trim($param['image_path']) != "") {
                    if (isset($param['image_flag']) and trim($param['image_flag']) == "1") {
                            $image_name_original =  $this->decrypt(trim($param['image_name']));                          
                        // start
                            $temp_file = dirname($_SERVER['DOCUMENT_ROOT']) . '/public/js/pixenate/cache/'. $this->decrypt(trim($param['image_path']));
                            
                             
                            $fof_dir_thumb = $this->_config['file_upload_path']['temp_upload_dir'] . "small/";
                            $fof_filename_thumb = $fof_dir_thumb . $image_name_original;

                            //if($image_name_original != "") {
                             $this->resizeImage($fof_filename_thumb, $temp_file, $this->_config['file_upload_path']['user_fof_medium_width'], $this->_config['file_upload_path']['user_fof_medium_height']);
                            //}
                            
                            $image_path1 = $this->_config['file_upload_path']['assets_url'] . 'temp/small/'.$image_name_original;
                            $temp_fileAssets = dirname($_SERVER['DOCUMENT_ROOT']) . '/assets/temp/small/'. $image_name_original;
                         // end
                            
                        $image_path1 =  $this->_config['js_file_path']['path'].'/js/pixenate/cache/'.$this->decrypt(trim($param['image_path']));
                        $temp_fileAssets = "";
                            
                    } else if (isset($param['image_flag']) and trim($param['image_flag']) == "2") {
                           $image_name_original =  $this->decrypt(trim($param['image_name'])); 
                           
                           $temp_file = dirname($_SERVER['DOCUMENT_ROOT']) . '/public/js/pixenate/cache/'. $image_name_original; 
                           
                           $fof_dir_thumb = $this->_config['file_upload_path']['temp_upload_dir'] . "small/";
                           $fof_filename_thumb = $fof_dir_thumb . $image_name_original;
                            
                          // if($image_name_original != "") {
                             $this->resizeImage($fof_filename_thumb, $temp_file, $this->_config['file_upload_path']['user_fof_medium_width'], $this->_config['file_upload_path']['user_fof_medium_height']);
                          // }
                            
                            $image_path1 = $this->_config['file_upload_path']['assets_url'] . 'temp/small/'.$image_name_original;
                            $temp_fileAssets = dirname($_SERVER['DOCUMENT_ROOT']) . '/assets/temp/small/'. $image_name_original;
                    } else {
                            $image_path1 = '';
                            $temp_fileAssets = '';
                    }
            } else {
                    $image_path1 = '';
            }


            $imagewidth = "72";
            $imageheight = "72";
         /* 
            if(isset($temp_fileAssets) and trim($temp_fileAssets) != "") {
                    list($imagewidth, $imageheight, $imageType) = @getimagesize($temp_fileAssets);
                    if($imagewidth == "" or $imagewidth == "0") { $imagewidth  = ""; }
                    if($imageheight == "" or $imageheight == "0") { $imageheight  = ""; }
            }
          */  
             $viewModel = new ViewModel();
             $viewModel->setTerminal(true);
             $viewModel->setVariables(array(
                     'image_path' => $image_path1,
                     'imagewidth' => $imagewidth,
                     'imageheight' => $imageheight
                )
             );
             return $viewModel;
        }
        catch (Exception $e){ }
    }
    
    
    /**
     * This function is used to encrypt text
     * @return     encrypted text
     * @author Icreon Tech - NS
     */
//    public function fofEncryptTextAction() {
//        try {
//            $this->getConfig();
//            $request = $this->getRequest();
//            $decryptText = $request->getPost('decryptText');
//            if (isset($decryptText) and trim($decryptText) != "") {
//                $encryptText = $this->encrypt(trim($decryptText));
//            } else {
//                $encryptText = '';
//            }
//         } catch (Exception $e) {
//                $encryptText = '';
//         }
//           echo $encryptText; 
//           die();
//    }
    
     public function fofEncryptTextAction() {
        try {
            $this->getConfig();
            $request = $this->getRequest();
            $param = $this->params()->fromRoute();
            
         // $decryptText = $request->getPost('decryptText');            
            $decryptText = isset($param['decryptText']) ? $param['decryptText'] : "";
            
            if (isset($decryptText) and trim($decryptText) != "") {
                $encryptText = $this->encrypt(trim($decryptText));
            } else {
                $encryptText = '';
            }
        } catch (Exception $e) {
            $encryptText = '';
        }
        
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode(array("encryptText" => $encryptText)));
        return $response;
        die();
    }

    /**
     * This function is used to resize Image
     * @return     array
     * @author Icreon Tech - NS
     */
    public function resizeImage($new_image_name, $image, $resizeWidth, $resizeHeight) {
        try {
            list($imagewidth, $imageheight, $imageType) = @getimagesize($image);
            $imageType = image_type_to_mime_type($imageType);


            $SMALL_IMAGE_MAX_WIDTH = $resizeWidth;
            $SMALL_IMAGE_MAX_HEIGHT = $resizeHeight;

            $source_aspect_ratio = $imagewidth / $imageheight;
            $thumbnail_aspect_ratio = $SMALL_IMAGE_MAX_WIDTH / $SMALL_IMAGE_MAX_HEIGHT;
            if ($imagewidth <= $SMALL_IMAGE_MAX_WIDTH && $imageheight <= $SMALL_IMAGE_MAX_HEIGHT) {
                $small_image_width = $imagewidth;
                $small_image_height = $imageheight;
            } elseif ($thumbnail_aspect_ratio > $source_aspect_ratio) {
                $small_image_width = (int) ($SMALL_IMAGE_MAX_HEIGHT * $source_aspect_ratio);
                $small_image_height = $SMALL_IMAGE_MAX_HEIGHT;
            } else {
                $small_image_width = $SMALL_IMAGE_MAX_WIDTH;
                $small_image_height = (int) ($SMALL_IMAGE_MAX_WIDTH / $source_aspect_ratio);
            }

            $newImageWidth = $small_image_width;
            $newImageHeight = $small_image_height;

            $newImage = imagecreatetruecolor($newImageWidth, $newImageHeight);

            switch ($imageType) {
                case "image/gif":
                    $source = imagecreatefromgif($image);
                    break;
                case "image/pjpeg":
                case "image/jpeg":
                case "image/jpg":
                    $source = imagecreatefromjpeg($image);
                    break;
                case "image/png":
                case "image/x-png":
                    $source = imagecreatefrompng($image);
                    break;
            }

            imagecopyresampled($newImage, $source, 0, 0, 0, 0, $newImageWidth, $newImageHeight, $imagewidth, $imageheight);
            switch ($imageType) {
                case "image/gif":
                    imagegif($newImage, $new_image_name);
                    break;
                case "image/pjpeg":
                case "image/jpeg":
                case "image/jpg":
                    imagejpeg($newImage, $new_image_name, 90);
                    break;
                case "image/png":
                case "image/x-png":
                    imagepng($newImage, $new_image_name);
                    break;
            }

            chmod($new_image_name, 0777);
            return true;
        } catch (Exception $e) {
            return true;
        }
    }

    /**
     * This action is used to move file to temp folder
     * @param filename
     * @return
     * @author Icreon Tech - NS
     */
    public function moveFileImageToTemp($filename, $cachefilename) {
        try {
            if (isset($filename) and trim($filename) != "") {
                
                $temp_dir = dirname($_SERVER['DOCUMENT_ROOT']) . '/public/js/pixenate/cache/'; 
                $temp_dir_fof = dirname($_SERVER['DOCUMENT_ROOT']) . '/public/js/pixenate/cache/'; //$this->_config['file_upload_path']['temp_upload_dir'] . 'fof/';

                $temp_file = $temp_dir . $cachefilename;
                $temp_fof_file = $temp_dir_fof . $filename;

                if (isset($cachefilename) and trim($cachefilename) != "" and file_exists($temp_file)) {
                    $temp_file = $temp_file;
                } else {
                    $temp_file = $temp_fof_file;
                }
                
                $fof_dir_root = $this->_config['file_upload_path']['temp_upload_dir'] . "fof/";
                $fof_dir_thumb = $this->_config['file_upload_path']['temp_upload_dir'] . "fof/thumb/";
                $fof_dir_medium = $this->_config['file_upload_path']['temp_upload_dir'] . "fof/medium/";
                $fof_dir_large = $this->_config['file_upload_path']['temp_upload_dir'] . "fof/large/";
                $fof_filename_large = $fof_dir_large . $filename;
                $fof_filename_medium = $fof_dir_medium . $filename;
                $fof_filename_thumb = $fof_dir_thumb . $filename;
                $fof_filename_root = $fof_dir_root . $filename;


                if (isset($temp_file) and trim($temp_file) != "" and file_exists($temp_file)) {
                    copy($temp_file, $fof_filename_root);
                    $this->resizeImage($fof_filename_thumb, $fof_filename_root, $this->_config['file_upload_path']['user_fof_medium_width'], $this->_config['file_upload_path']['user_fof_medium_height']);
                    $this->resizeImage($fof_filename_medium, $fof_filename_root, $this->_config['file_upload_path']['user_fof_medium_width'], $this->_config['file_upload_path']['user_fof_medium_height']);
                    $this->resizeImage($fof_filename_large, $fof_filename_root, $this->_config['file_upload_path']['user_fof_large_width'], $this->_config['file_upload_path']['user_fof_large_height']);
                }
            }
        } catch (Exception $e) {
            
        }
    }

    /**
     * This action is used to move file from temp to fof folder
     * @param filename
     * @return
     * @author Icreon Tech - NS
     */
    public function moveFileFromTempToFOF($filename) {
        try {
            if (isset($filename) and trim($filename) != "") {
//                $temp_dir = dirname($_SERVER['DOCUMENT_ROOT']) . '/public/js/pixenate/cache/'; //$this->_config['file_upload_path']['temp_upload_dir'];
                $temp_dir_fof = $this->_config['file_upload_path']['temp_upload_dir'] . 'fof/';

//                $temp_file = $temp_dir . $cachefilename;
                $temp_fof_file = $temp_dir_fof . $filename;
//
//                if (isset($cachefilename) and trim($cachefilename) != "" and file_exists($temp_file)) {
//                    $temp_file = $temp_file;
//                } else {
                $temp_file = $temp_fof_file;
                //               }

                $fof_dir_root = $this->_config['file_upload_path']['assets_upload_dir'] . "fof/";
                $fof_dir_thumb = $this->_config['file_upload_path']['assets_upload_dir'] . "fof/thumb/";
                $fof_dir_medium = $this->_config['file_upload_path']['assets_upload_dir'] . "fof/medium/";
                $fof_dir_large = $this->_config['file_upload_path']['assets_upload_dir'] . "fof/large/";
                $fof_filename_large = $fof_dir_large . $filename;
                $fof_filename_medium = $fof_dir_medium . $filename;
                $fof_filename_thumb = $fof_dir_thumb . $filename;
                $fof_filename_root = $fof_dir_root . $filename;


                if (file_exists($temp_file) && file_exists($temp_file)) {
                    if (copy($temp_file, $fof_filename_root)) {
                        $this->resizeImage($fof_filename_thumb, $fof_filename_root, $this->_config['file_upload_path']['user_fof_medium_width'], $this->_config['file_upload_path']['user_fof_medium_height']);
                        $this->resizeImage($fof_filename_medium, $fof_filename_root, $this->_config['file_upload_path']['user_fof_medium_width'], $this->_config['file_upload_path']['user_fof_medium_height']);
                        $this->resizeImage($fof_filename_large, $fof_filename_root, $this->_config['file_upload_path']['user_fof_large_width'], $this->_config['file_upload_path']['user_fof_large_height']);
                        if (file_exists($temp_file)) {
                            unlink($temp_file);
                        }
                        if (file_exists($temp_fof_file)) {
                            unlink($temp_fof_file);
                        }
                    }
                }
            }
        } catch (Exception $e) {
            
        }
    }

}

