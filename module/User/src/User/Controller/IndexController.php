<?php

/**
 * This controller is used for add, edit and delete
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Controller;

use Base\Controller\BaseController;
use Zend\View\Model\ViewModel;
use User\Model\User;
use User\Form\UserForm;
use Zend\Session\Container as SessionContainer;

/**
 * This controller is used for add, edit and delete
 * @package    User
 * @author     Icreon Tech - DG
 */
class IndexController extends BaseController {

    protected $userTable;

    public function ihsindexAction() {
        
    }

    public function addAction() {
        
    }

    public function editAction() {
        
    }

    public function deleteAction() {
        
    }

    /**
     * This function is used to get time zone offset
     * @return     array
     * @param array $form
     * @author Icreon Tech - DG
     */
    public function timeZoneOffsetAction() {
        $response = $this->getResponse();
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $param = $this->params()->fromRoute();
        $request = $this->getRequest();

        if ($request->isXmlHttpRequest()) {
            if (isset($param)) {
                $timeZone = !empty($param['time_zone']) ? $param['time_zone'] : null;
                $this->timeZoneSession = new SessionContainer('timeZone');
                $this->timeZoneSession->timeZone = $timeZone;
                $data = array('timeZone' => $timeZone);
                $response->setContent(\Zend\Json\Json::encode($data));
                return $response;
            }
        }

        $viewModel->setVariables(array(
        ));
        return $viewModel;
    }

}
