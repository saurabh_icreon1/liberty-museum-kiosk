<?php

/**
 * This controller is used to display user detail
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Controller;

use Base\Model\UploadHandler;
use Authorize\lib\AuthnetXML;
use Base\Controller\BaseController;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Zend\Session\Container as SessionContainer;
use Zend\View\Model\ViewModel;
use User\Model\User;
use Zend\Mvc\Controller\Plugin\Params;
use Zend\Http\Request;
use User\Form\BioWohForm;
use User\Form\ContactEditBasicForm;
use User\Form\EditUserPasswordForm;
use User\Form\EditUserSecurityQuestionForm;
use User\Form\EditUserEmailInfoForm;
use User\Form\ContactPhonenoForm;
//use User\Form\ContactAddressesForm;
use User\Form\ContactAddressesFrontForm;
use User\Form\UserDemographicForm;
use User\Form\ContactEducationForm;
use User\Form\JobHistoryForm;
use User\Form\AddTokenForm;
use User\Form\ContactusForm;
use User\Model\Contact;
use Cases\Model\Cases;
// s -
use Facebook\FacebookRequest;
use Facebook\FacebookRequestException;
use Facebook\GraphUser;

// e -

/**
 * This controller is used to display user detail
 * @package    User
 * @author     Icreon Tech - DG
 */
class UserController extends BaseController {

    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_authservice = null;
    protected $_storage = null;
    protected $_userTable = null;

    //protected $auth = null;

    public function __construct() {
        $this->_flashMessage = $this->flashMessenger();
    }

    public function indexAction() {
        
    }

    /**
     * This functikon is used to get table connections
     * @return     array 
     * @author Icreon Tech - DG
     */
    public function getTables() {
        $sm = $this->getServiceLocator();
        $this->_userTable = $sm->get('User\Model\UserTable');
        $this->_adapter = $sm->get('dbAdapter');
        $this->_config = $sm->get('Config');
        return $this->_userTable;
    }

    /**
     * This functikon is used to get config variables
     * @return     array 
     * @author Icreon Tech - DG
     */
    public function getConfig() {
        $sm = $this->getServiceLocator();
        $this->_translator = $sm->get('translator');
        $this->_config = $sm->get('Config');
    }

    /**
     * This function is used to get auth object
     * @return     array 
     * @param array
     * @author  Icreon Tech - DG
     */
    public function getSessionStorage() {
        if (!$this->_storage) {
            $this->_storage = $this->getServiceLocator()
                    ->get('User\Model\AuthStorage');
        }
        return $this->_storage;
    }

    /**
     * This Action is used to display the user profile
     * @author Icreon Tech-DG
     */
    public function getProfileAction() {
        $this->checkFrontUserAuthentication();
        $this->getConfig();
        $viewModel = new ViewModel();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $auth = new AuthenticationService();
        $params = $this->params()->fromRoute();
        $mode = '';
        if (isset($params['mode'])) {
            $mode = $this->decrypt($params['mode']);
        }
        $this->auth = $auth;
        if (isset($this->auth->getIdentity()->user_id) and trim($this->auth->getIdentity()->user_id) != "") {
            $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->auth->getIdentity()->user_id, 'front_end' => 1));
            $userAddressDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations(array('user_id' => $userDetail['user_id'], 'address_location' => '1'));
            $membershipDetailArr = array();
            $membershipDetailArr[] = $userDetail['membership_id'];
            $getMemberShip = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);

            $folderStrucByUser = $this->FolderStructByUserId($userDetail['user_id']);
            $uploadedFilePath = $this->_config['file_upload_path']['assets_url'] . 'user/' . $folderStrucByUser . "image/medium/";
            $assetsUploadDir = $this->_config['file_upload_path']['assets_upload_dir'] . 'user/' . $folderStrucByUser . "image/medium/";

            $jsLangTranslate = $this->_config['user_messages']['config']['user_profile'];
            $viewModel->setVariables(array(
                'userDetail' => $userDetail,
                'userAddressDetail' => $userAddressDetail,
                'assetsUploadDir' => $assetsUploadDir,
                'getMemberShip' => $getMemberShip,
                'uploadedFilePath' => $uploadedFilePath,
                'userId' => $this->encrypt($userDetail['user_id']),
                'mode' => $mode,
                'jsLangTranslate' => $jsLangTranslate
                    )
            );
            return $viewModel;
        }
    }

    /**
     * This Action is used to display the user documents
     * @author Icreon Tech-DG
     */
    public function getMyFileAction() {
        $this->checkFrontUserAuthentication();
        $this->getConfig();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $this->layout('layout');
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $auth = new AuthenticationService();
        $this->auth = $auth;
        if (isset($this->auth->getIdentity()->user_id) and trim($this->auth->getIdentity()->user_id) != "") {
            $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->auth->getIdentity()->user_id));
            $membershipDetailArr = array();
            $membershipDetailArr[] = $userDetail['membership_id'];
            $getMemberShip = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);

            $postData[0] = '';
            $postData[1] = '';
            $postData[3] = 'asc';
            $postData[2] = 'CAST(tbl_membership.minimun_donation_amount AS SIGNED INTEGER)';
            $membershipData = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMasterMembership($postData);

            $jsLangTranslate = $this->_config['user_messages']['config']['user_profile'];
            $viewModel->setVariables(array(
                'jsLangTranslate' => $jsLangTranslate,
                'getMemberShip' => $getMemberShip,
                'minimunDonationAmount' => $membershipData[0]['minimun_donation_amount']
                    )
            );
            return $viewModel;
        }
    }

    /**
     * This Action is used to display the user profile
     * @author Icreon Tech-DG
     */
    public function getDocumentSearchAction() {
        $this->checkFrontUserAuthentication();
        $this->getConfig();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $this->layout('layout');
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $viewModel->setVariables(array(
            'jsLangTranslate' => $this->_config['user_messages']['config']['user_profile_document_search']
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to display the passenger search list
     * @author Icreon Tech-DG
     */
    public function getPassengerSearchAction() {
        $this->checkFrontUserAuthentication();
        $this->getConfig();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $this->layout('layout');
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $params = $this->params()->fromRoute();
        $searchParam = array(
            'user_id' => $this->decrypt($params['user_id']),
            'type' => 1,
            'sort_field' => 'save_date',
            'sort_order' => 'desc'
        );
        $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->decrypt($params['user_id'])));
        $membershipDetailArr = array();
        $membershipDetailArr[] = $userDetail['membership_id'];
        $getMemberShip = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
        $saveSearchTypePS = 1; //Passenger Search
        $saveSearchTypePR = 5; //Passenger Record
        $maxSearchCountPS = 0;
        $maxSearchCountPR = 0;
        $savedSearchInDays = explode(',', $getMemberShip['savedSearchInDays']);
        $savedSearchTypeIds = explode(',', $getMemberShip['savedSearchTypeIds']);
        foreach ($savedSearchTypeIds as $key => $val) {
            if ($val == $saveSearchTypePS) {
                $maxSearchCountPS = $savedSearchInDays[$key];
            }
            if ($val == $saveSearchTypePR) {
                $maxSearchCountPR = $savedSearchInDays[$key];
            }
        }
        $passengerSearch = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserSearches($searchParam);
        $passengerSavedSearch = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getUserPassengerSearch(array('user_id' => $this->decrypt($params['user_id'])));
        $passengerPurchasedRecord = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getPassengerRecordPurchased(array('user_id' => $this->decrypt($params['user_id']), 'product_type_id' => 5));
        $productParam = array();
        $productParam['productType'] = '5';/** product */
        $productResult = array();
        $productResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getProductAttributeDetails($productParam);
        $discount = 0;  
        $auth = new AuthenticationService();
        if (!empty($auth->getIdentity()->user_id)) {
            if (defined('MEMBERSHIP_DISCOUNT') && $productResult[0]['is_membership_discount']==1) {
                            $discount = MEMBERSHIP_DISCOUNT;
            }        
        } 
        
        $viewModel->setVariables(array(
            'productResult' => $productResult,
            'passengerSearch' => $passengerSearch,
            'passengerSavedSearch' => $passengerSavedSearch,
            'passengerPurchasedRecord' => $passengerPurchasedRecord,
            'maxSearchCountPR' => $maxSearchCountPR,
            'maxSearchCountPS' => $maxSearchCountPS,
            'membershipId' => $userDetail['membership_id'],
            'jsLangTranslate' => $this->_config['user_messages']['config']['user_profile_document_search'],
            'facebookAppId' => $this->_config['facebook_app']['app_id'],
			'allowed_IP' => $this->_config['allowed_ip']['ip'],
            'discount'=>$discount
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to display manifest search list
     * @author Icreon Tech-DG
     */
    public function getManifestSearchAction() {
        $this->checkFrontUserAuthentication();
        $this->getConfig();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $params = $this->params()->fromRoute();
        $this->layout('layout');
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }

        $productParam = array();
        $productParam['productType'] = '7';/** manifest */
        $productParam['isOptions'] = 'yes';
        $productResult = array();
        $productResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getProductAttributeDetails($productParam);
        $discount = 0;  
        $auth = new AuthenticationService();
        if (!empty($auth->getIdentity()->user_id)) {
            if (defined('MEMBERSHIP_DISCOUNT') && $productResult[0]['is_membership_discount']==1) {
                            $discount = MEMBERSHIP_DISCOUNT;
            }        
        } 
        
        $manifestSavedSearch = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getUserManifestSearch(array('user_id' => $this->decrypt($params['user_id'])));

        $manifestPurchasedRecord = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getManifestPurchased(array('user_id' => $this->decrypt($params['user_id']), 'product_type_id' => 7));
        //  asd($manifestPurchasedRecord);
        $viewModel->setVariables(array(
            'manifestSavedSearch' => $manifestSavedSearch,
            'productResult' => $productResult,
            'jsLangTranslate' => $this->_config['user_messages']['config']['user_profile_document_search'],
            'facebookAppId' => $this->_config['facebook_app']['app_id'],
            'manifestPurchasedRecord' => $manifestPurchasedRecord,
			'allowed_IP' => $this->_config['allowed_ip']['ip'],
                    'discount'=>$discount
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to display the user profile
     * @author Icreon Tech-DG
     */
    public function getShipSearchAction() {
        $this->checkFrontUserAuthentication();
        $this->getConfig();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $this->layout('layout');
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $params = $this->params()->fromRoute();
        $searchParam = array(
            'user_id' => $this->decrypt($params['user_id']),
            'type' => 2,
            'sort_field' => 'save_date',
            'sort_order' => 'desc'
        );
        $shipSearch = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserSearches($searchParam);
        $savedShipSearch = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getUserSavedShip(array('user_id' => $this->decrypt($params['user_id'])));

        $shipPurchasedRecord = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getShipRecordPurchased(array('user_id' => $this->decrypt($params['user_id']), 'product_type_id' => 8));

        $shipAdditionalCopies = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getShipAdditionalCopies(array('product_type_id' => 8, 'product_id' => 2));
        //asd($shipPurchasedRecord);
        $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->decrypt($params['user_id'])));
        $membershipDetailArr = array();
        $membershipDetailArr[] = $userDetail['membership_id'];
        $getMemberShip = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
        $saveSearchTypeSS = 2; //Ship Search
        $saveSearchTypeSR = 7; //ShipRecord
        $maxSearchCountSS = 0;
        $maxSearchCountSR = 0;
        $savedSearchInDays = explode(',', $getMemberShip['savedSearchInDays']);
        $savedSearchTypeIds = explode(',', $getMemberShip['savedSearchTypeIds']);
        foreach ($savedSearchTypeIds as $key => $val) {
            if ($val == $saveSearchTypeSS) {
                $maxSearchCountSS = $savedSearchInDays[$key];
            }
            if ($val == $saveSearchTypeSR) {
                $maxSearchCountSR = $savedSearchInDays[$key];
            }
        }

        $productParamShip = array();
        $productParamShip['productType'] = '8';/** Ship  */
        $productParamShip['isOptions'] = 'yes';
        $productResult = array();
        $productResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getProductAttributeDetails($productParamShip);
        $discount = 0;  
        $auth = new AuthenticationService();
        if (!empty($auth->getIdentity()->user_id)) {
            if (defined('MEMBERSHIP_DISCOUNT') && $productResult[0]['is_membership_discount']==1) {
                            $discount = MEMBERSHIP_DISCOUNT;
            }        
        }
        $viewModel->setVariables(array(
            'productResult' => $productResult,
            'shipSearch' => $shipSearch,
            'savedShipSearch' => $savedShipSearch,
            'shipPurchasedRecord' => $shipPurchasedRecord,
            'shipAdditionalCopies' => $shipAdditionalCopies,
            'maxSearchCountSS' => $maxSearchCountSS,
            'maxSearchCountSR' => $maxSearchCountSR,
            'membershipId' => $userDetail['membership_id'],
            'jsLangTranslate' => $this->_config['user_messages']['config']['user_profile_document_search'],
            'facebookAppId' => $this->_config['facebook_app']['app_id'],
			'allowed_IP' => $this->_config['allowed_ip']['ip'],
            'discount'=>$discount
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to display the user oral histor info
     * @author Icreon Tech-DG
     */
    public function myOralHistoryInfoAction() {
        $this->checkFrontUserAuthentication();
        $this->getConfig();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $this->layout('layout');
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $params = $this->params()->fromRoute();
        $searchParam = array(
            'user_id' => $this->decrypt($params['user_id']),
            'type' => 4,
            'sort_field' => 'save_date',
            'sort_order' => 'desc'
        );
        $oralHistorySearch = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserSearches($searchParam);

        $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->decrypt($params['user_id'])));
        $membershipDetailArr = array();
        $membershipDetailArr[] = $userDetail['membership_id'];
        $getMemberShip = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
        $saveSearchTypeOHR = 6; //Oral History Search
        $maxSearchCountOHR = 0;

        $savedSearchInDays = explode(',', $getMemberShip['savedSearchInDays']);
        $savedSearchTypeIds = explode(',', $getMemberShip['savedSearchTypeIds']);
        foreach ($savedSearchTypeIds as $key => $val) {
            if ($val == $saveSearchTypeOHR) {
                $maxSearchCountOHR = $savedSearchInDays[$key];
            }
        }

        $viewModel->setVariables(array(
            'oralHistorySearch' => $oralHistorySearch,
            'maxSearchCountOHR' => $maxSearchCountOHR,
            'membershipId' => $userDetail['membership_id'],
            'jsLangTranslate' => $this->_config['user_messages']['config']['user_profile_document_search']
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to display fof search list
     * @author Icreon Tech-DG
     */
    public function getFofListAction() {
        $this->checkFrontUserAuthentication();
        $this->getConfig();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $this->layout('layout');
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $params = $this->params()->fromRoute();
        $fofSearch = $this->getServiceLocator()->get('User\Model\UserTable')->getUserFofSearch(array('user_id' => $this->decrypt($params['user_id'])));
        $userFof = $this->getServiceLocator()->get('User\Model\UserTable')->getUserFof(array('user_id' => $this->decrypt($params['user_id'])));

        //$folderStrucByUser = $this->FolderStructByUserId($this->decrypt($params['user_id']));
        $uploadedFilePath = $this->_config['file_upload_path']['assets_url'] . 'fof/medium';
        $assetsUploadDir = $this->_config['file_upload_path']['assets_upload_dir'] . 'fof/medium';
        $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->decrypt($params['user_id'])));
        $membershipDetailArr = array();
        $membershipDetailArr[] = $userDetail['membership_id'];
        $getMemberShip = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
        $saveSearchTypeFOF = 4; //FOF Search        
        $maxSearchCountFOF = 0;

        $savedSearchInDays = explode(',', $getMemberShip['savedSearchInDays']);
        $savedSearchTypeIds = explode(',', $getMemberShip['savedSearchTypeIds']);
        foreach ($savedSearchTypeIds as $key => $val) {
            if ($val == $saveSearchTypeFOF) {
                $maxSearchCountFOF = $savedSearchInDays[$key];
            }
        }

        $flashMessages = '';
        if ($this->_flashMessage->hasMessages()) {
            $flashMessages = $this->_flashMessage->getMessages();
        }

        $viewModel->setVariables(array(
            'fofSearch' => $fofSearch,
            'userFof' => $userFof,
            'assetsUploadDir' => $assetsUploadDir,
            'uploadedFilePath' => $uploadedFilePath,
            'maxSearchCountFOF' => $maxSearchCountFOF,
            'membershipId' => $userDetail['membership_id'],
            'jsLangTranslate' => $this->_config['user_messages']['config']['user_fof'],
            'flashMessages' => $flashMessages
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to delete fof search
     * @author Icreon Tech-DG
     */
    public function deleteFofSearchAction() {
        $this->checkFrontUserAuthentication();
        $this->getTables();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $dataParam = array();
            $response = $this->getResponse();
            $messages = array();

            if ($request->getPost('search_id') != '') {
                $dataParam['search_id'] = $request->getPost('search_id');
                $auth = new AuthenticationService();
                $this->auth = $auth;
                $userDetail = (array) $this->auth->getIdentity();
                $dataParam['user_id'] = $userDetail['user_id'];
                $dataParam['is_delete'] = 1;
                $this->getTables()->deleteFofSearch($dataParam);
                $messages = array('status' => "success");
                return $response->setContent(\Zend\Json\Json::encode($messages));
            }
        }
    }

    /**
     * This Action is used to display woh search list
     * @author Icreon Tech-DG
     */
    public function getWohListAction() {
        $this->checkFrontUserAuthentication();
        $this->getConfig();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $this->layout('layout');
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $params = $this->params()->fromRoute();
        $wohSearch = $this->getServiceLocator()->get('User\Model\UserTable')->getUserWohSearches(array('user_id' => $this->decrypt($params['user_id'])));
        //asd($wohSearch);
        $userWoh = $this->getServiceLocator()->get('User\Model\WohTable')->getUserWoh(array('user_id' => $this->decrypt($params['user_id']), 'sort_field' => 'usr_wall_of_honor.modified_date', 'sort_order' => 'desc'));
        if (!empty($userWoh)) {
            foreach ($userWoh as $key => $val) {
                $wohNameString = $val['final_name'];
                $userWoh[$key]['honoree_name'] = $wohNameString;
            }
        }
        $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->decrypt($params['user_id'])));
        $membershipDetailArr = array();
        $membershipDetailArr[] = $userDetail['membership_id'];
        $getMemberShip = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);
        $saveSearchTypeWOH = 3; //WOH Search        
        $maxSearchCountWOH = 0;

        $savedSearchInDays = explode(',', $getMemberShip['savedSearchInDays']);
        $savedSearchTypeIds = explode(',', $getMemberShip['savedSearchTypeIds']);
        foreach ($savedSearchTypeIds as $key => $val) {
            if ($val == $saveSearchTypeWOH) {
                $maxSearchCountWOH = $savedSearchInDays[$key];
            }
        }

        $productParam = array();
        $productParam['productType'] = '6';/** WOH */
        $productResult = array();
        $productResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getProductAttributeDetails($productParam);
        $productIds = array();
        if (!empty($productResult)) {
            foreach ($productResult as $key => $val) {
                if ($val['additional_product_1'] != 0) {
                    $productIds[$val['additional_product_1']] = 'Additional';
                }
                if ($val['additional_product_2'] != 0) {
                    $productIds[$val['additional_product_2']] = 'Boi';
                }
                if ($val['additional_product_3'] != 0) {
                    $productIds[$val['additional_product_3']] = 'Personalized';
                }
            }
        }

        $productResult = array();
        if (!empty($productIds)) {
            foreach ($productIds as $key => $val) {
                $productDetails = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProductInfo(array('id' => $key));
                $productResult[$val] = $productDetails[0];
            }
        }
        
        $viewModel->setVariables(array(
            'wohSearch' => $wohSearch,
            'userWoh' => $userWoh,
            'productResult' => $productResult,
            'membershipId' => $userDetail['membership_id'],
            'maxSearchCountWOH' => $maxSearchCountWOH,
            'jsLangTranslate' => $this->_config['user_messages']['config']['user_woh'],
            'facebookAppId' => $this->_config['facebook_app']['app_id'],
			'allowed_IP' => $this->_config['allowed_ip']['ip']
                )
        );
        return $viewModel;
    }

    public function wohAdditionalProductAction() {
        $this->getTables();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $params = $this->params()->fromRoute();
        $path = $this->_config['file_upload_path']['assets_url'] . "product/thumbnail/";
        $productParam = array();
        $productParam['productType'] = '6';/** WOH */
        $productResult = array();
        $productResult = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getProductAttributeDetails($productParam);
        $productIds = array();
        if (!empty($productResult)) {
            foreach ($productResult as $key => $val) {
                if ($val['additional_product_1'] != 0) {
                    $productIds[$val['additional_product_1']] = 'Additional';
                }
                if ($val['additional_product_2'] != 0) {
                    $productIds[$val['additional_product_2']] = 'Boi';
                }
                if ($val['additional_product_3'] != 0) {
                    $productIds[$val['additional_product_3']] = 'Personalized';
                }
            }
        }
        $productResult = array();
        if (!empty($productIds)) {
            foreach ($productIds as $key => $val) {
                $productDetails = $this->getServiceLocator()->get('Product\Model\ProductTable')->getProductInfo(array('id' => $key));
                $productResult[$val] = $productDetails[0];
            }
        }
        if (isset($params['item_id']) && !empty($params['item_id'])) {
            $params['item_id'] = $params['item_id'];
        } else {
            $params['item_id'] = '';
        }
        
        $discount = 0;  
        $auth = new AuthenticationService();
        if (!empty($auth->getIdentity()->user_id)) {
            if (defined('MEMBERSHIP_DISCOUNT')) {
                            $discount = MEMBERSHIP_DISCOUNT;
            }        
        }        
        
        $viewModel->setVariables(array(
            'bioCertificateProdId' => isset($params['bio_certificate_prod_id']) ? $params['bio_certificate_prod_id'] : '',
            'panelNo' => $params['panel_no'],
            'isFinalize' => $params['is_finalize'],
            'itemId' => $params['item_id'],
            'productResult' => $productResult,
            'path' => $path,
            'discount'=>$discount
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to delete woh search
     * @author Icreon Tech-DG
     */
    public function deleteWohSearchAction() {
        $this->getTables();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $dataParam = array();
            $response = $this->getResponse();
            $messages = array();

            if ($request->getPost('search_id') != '') {
                $dataParam['search_id'] = $request->getPost('search_id');
                $auth = new AuthenticationService();
                $this->auth = $auth;
                $userDetail = (array) $this->auth->getIdentity();
                $dataParam['user_id'] = $userDetail['user_id'];
                $dataParam['is_delete'] = 1;
                $this->getTables()->deleteWohSearch($dataParam);
                $messages = array('status' => "success");
                return $response->setContent(\Zend\Json\Json::encode($messages));
            }
        }
    }

    /**
     * This Action is used to edit user fof setting
     * @author Icreon Tech-DG
     */
    public function userFofSettingAction() {
        $this->checkFrontUserAuthentication();
        $this->getConfig();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $this->layout('web-popup');
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $params = $this->params()->fromRoute();

        $fofSetting = $this->getServiceLocator()->get('User\Model\UserTable')->getFof(array('fof_id' => $params['fof_id']));

        if ($request->isPost()) {
            $form_data = $request->getPost();
            $this->getServiceLocator()->get('User\Model\UserTable')->updateUserFofSetting(array('fof_id' => $form_data['fof_id'], 'is_people_photo_visible' => $form_data['is_people_photo_visible'], 'is_photo_caption_visible' => $form_data['is_photo_caption_visible']));
            $messages = array('status' => "success", 'message' => "Flag of Faces updated successfully.");
            $this->flashMessenger()->addMessage("Flag of Faces updated successfully.");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }

        $uploadedFilePath = $this->_config['file_upload_path']['assets_url'] . 'fof/large';
        $assetsUploadDir = $this->_config['file_upload_path']['assets_upload_dir'] . 'fof/large';

        $viewModel->setVariables(array(
            'fofSetting' => $fofSetting,
            'jsLangTranslate' => $this->_config['user_messages']['config']['user_woh'],
            'uploadedFilePath' => $uploadedFilePath,
            'assetsUploadDir' => $assetsUploadDir
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to edit user woh 
     * @author Icreon Tech-DG
     */
    public function editWohAction() {
        $this->checkFrontUserAuthentication();
        $this->getConfig();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $this->layout('web-popup');
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $params = $this->params()->fromRoute();
        $wohData = $this->getServiceLocator()->get('User\Model\UserTable')->getWoh(array('woh_id' => $params['woh_id']));

        if ($request->isPost()) {
            $form_data = $request->getPost();
            $this->getServiceLocator()->get('User\Model\UserTable')->updateWoh(array('woh_id' => $form_data['woh_id'], 'donated_by' => $form_data['donated_by']));
            $messages = array('status' => "success");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }

        $viewModel->setVariables(array(
            'wohData' => $wohData,
            'jsLangTranslate' => $this->_config['user_messages']['config']['user_woh']
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to edit bio woh 
     * @author Icreon Tech-DG
     */
    public function editBioWohAction() {
        $this->checkFrontUserAuthentication();
        $this->getConfig();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $this->layout('web-popup');
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $form = new BioWohForm();
        $params = $this->params()->fromRoute();
        $form->get('woh_id')->setValue($params['woh_id']);

        $arrWoh = $this->getServiceLocator()->get('User\Model\WohTable')->getUserWohTraDetailById(array('woh_id' => $params['woh_id']));
        $final_name = isset($arrWoh[0]['final_name']) ? $arrWoh[0]['final_name'] : '';
        $origin_country = isset($arrWoh[0]['origin_country']) ? $arrWoh[0]['origin_country'] : '';
        $form->get('name')->setValue(str_replace("<br>", " ", $final_name));
        $form->get('immigrated_from')->setValue($origin_country);

        if (isset($params['bio_certificate_id']))
            $bioWohData = $this->getTables()->getBioWoh(array('bio_certificate_product_id' => $params['bio_certificate_id']));

        if (!empty($bioWohData)) {
            $form->get('woh_id')->setValue($bioWohData[0]['woh_id']);
            $form->get('bio_certificate_id')->setValue($bioWohData[0]['bio_certificate_id']);
            $form->get('name')->setValue($bioWohData[0]['name']);
            $form->get('immigrated_from')->setValue($bioWohData[0]['immigrated_from']);
            $form->get('ship_method')->setValue($bioWohData[0]['method_of_travel']);
            $form->get('port_of_entry')->setValue($bioWohData[0]['port_of_entry']);
            if ($bioWohData[0]['date_of_entry_to_us'] != '' && $bioWohData[0]['date_of_entry_to_us'] != '0000:00:00') {
                $form->get('date_of_entry')->setValue($this->DateFormat($bioWohData[0]['date_of_entry_to_us'], 'calender'));
            }
            $form->get('additional_info')->setValue($bioWohData[0]['additional_info']);
        }
        if ($request->isPost()) {
            $form_data = $request->getPost();
            $form_data['date_of_entry'] = $this->DateFormat($form_data['date_of_entry']);
            $this->getTables()->updateBioWoh($form_data);
            $messages = array('status' => "success");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
        $viewModel->setVariables(array(
            'form' => $form,
            'jsLangTranslate' => $this->_config['user_messages']['config']['user_bio_woh']
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to view bio woh 
     * @author Icreon Tech-DG
     */
    public function viewBioWohAction() {
        $this->getConfig();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $this->layout('web-popup');
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $form = new BioWohForm();
        $params = $this->params()->fromRoute();
        $bioWohData = $this->getTables()->getBioWoh(array('bio_certificate_product_id' => $params['bio_certificate_id']));

        $viewModel->setVariables(array(
            'bioWohData' => $bioWohData[0],
            'jsLangTranslate' => $this->_config['user_messages']['config']['user_bio_woh']
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to edit user info
     * @author Icreon Tech-DG
     */
    public function editUserInfoAction() {
        $this->checkFrontUserAuthentication();
        $this->getConfig();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();

        $auth = new AuthenticationService();
        $this->auth = $auth;
        $userDetail = (array) $this->auth->getIdentity();

        $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $userDetail['user_id']));

        $params = $this->params()->fromRoute();
        $mode = '';
        if (isset($params['mode'])) {
            $mode = $this->decrypt($params['mode']);
        }

        $viewModel->setVariables(array(
            'userId' => $this->encrypt($userDetail['user_id']),
            'user_id' => $userDetail['user_id'],
            'userDetail' => $userDetail,
            'mode' => $mode,
            'jsLangTranslate' => $this->_config['user_messages']['config']['edit_user_info'],
			'allowed_IP' => $this->_config['allowed_ip']['ip']
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to edit basic info
     * @author Icreon Tech-DG
     */
    public function editBasicInfoAction() {
        $this->checkFrontUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $user = new User($this->_adapter);
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $flashMessages = '';
        if ($this->_flashMessage->hasMessages()) {
            $flashMessages = $this->_flashMessage->getMessages();
        }
        $form = new ContactEditBasicForm();
        $params = $this->params()->fromRoute();

        $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->decrypt($params['user_id'])));

        $folderStrucByUser = $this->FolderStructByUserId($userDetail['user_id']);
        $uploadedFilePath = $this->_config['file_upload_path']['assets_url'] . 'user/' . $folderStrucByUser . "image/thumbnail/";
        $assetsUploadDir = $this->_config['file_upload_path']['assets_upload_dir'] . 'user/' . $folderStrucByUser . "image/thumbnail/";

//        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
//        $countryList = array();
//        $countryList[''] = 'Select Country';
//        foreach ($country as $key => $val) {
//            $countryList[$val['country_id']] = $val['name'];
//        }
//        $form->get('country_id')->setAttribute('options', $countryList);

        if (!empty($userDetail)) {

            if ($userDetail['user_type'] == 1) {
                $getTitle = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTitle();
                $userTitle = array();
                $userTitle[' '] = 'None';
                foreach ($getTitle as $key => $val) {
                    $userTitle[$val['title']] = $val['title'];
                }
                $form->get('title')->setAttribute('options', $userTitle);

                $getSuffix = $this->getServiceLocator()->get('Common\Model\CommonTable')->getSuffix();
                $userSuffix = array();
                $userSuffix[' '] = 'None';
                foreach ($getSuffix as $key => $val) {
                    $userSuffix[$val['suffix']] = $val['suffix'];
                }
                $form->get('suffix')->setAttribute('options', $userSuffix);

                $form->get('title')->setAttribute('value', $userDetail['title']);
                $form->get('first_name')->setAttribute('value', $userDetail['first_name']);
                $form->get('middle_name')->setAttribute('value', $userDetail['middle_name']);
                $form->get('last_name')->setAttribute('value', $userDetail['last_name']);
                $form->get('suffix')->setAttribute('value', $userDetail['suffix']);
            } else {
                $form->add(array(
                    'type' => 'text',
                    'name' => 'corporate_company_name',
                    'attributes' => array(
                        'id' => 'corporate_company_name',
                        'class' => 'width-127'
                    ),
                ));
                $form->get('corporate_company_name')->setAttribute('value', $userDetail['company_name']);
                $form->add(array(
                    'type' => 'text',
                    'name' => 'corporate_sic_code',
                    'attributes' => array(
                        'id' => 'corporate_sic_code',
                        'class' => 'width-127'
                    ),
                ));
                $form->get('corporate_sic_code')->setAttribute('value', $userDetail['sic_code']);
                $form->add(array(
                    'type' => 'text',
                    'name' => 'corporate_legal_name',
                    'attributes' => array(
                        'id' => 'corporate_legal_name',
                        'class' => 'width-127'
                    ),
                ));
                $form->get('corporate_legal_name')->setAttribute('value', $userDetail['legal_name']);
            }

            $form->get('contact_id')->setAttribute('value', $this->encrypt($userDetail['user_id']));
            $form->get('upload_profile_image_id')->setAttribute('value', $userDetail['profile_image']);

            $form->get('email_id')->setAttribute('value', $userDetail['email_id']);
            $form->get('conf_email_id')->setAttribute('value', $userDetail['email_id']);
            $form->get('username')->setAttribute('value', $userDetail['username']);
            $form->get('security_question')->setAttribute('value', $userDetail['security_question']);
            $form->get('security_answer')->setAttribute('value', $userDetail['security_answer']);

            //$form->get('country_id')->setAttribute('value', $userDetail['country_id']);
            //$form->get('postal_code')->setAttribute('value', $userDetail['zip_code']);
        }

        if ($request->isPost()) {
            // n-submit - start
            $typeSubmit = "";
            if ($request->getPost("submitbutton1") != "") {
                $typeSubmit = "1";
            } else if ($request->getPost("submitbutton2") != "") {
                $typeSubmit = "2";
            } else if ($request->getPost("submitbutton3") != "") {
                $typeSubmit = "3";
            } else if ($request->getPost("submitbutton4") != "") {
                $typeSubmit = "4";
            } else {
                $typeSubmit = "6";
            }
            // n-submit - end

            $form->setData($request->getPost());
            $validationGroups = array('csrf', 'contact_id', 'email_id', 'conf_email_id', 'upload_profile_image', 'upload_profile_image_id', 'username', 'security_question', 'security_answer');
            if ($userDetail['user_type'] == 1) {
                $validationGroups[] = 'last_name';
                $validationGroups[] = 'first_name';
                $validationGroups[] = 'middle_name';
                $validationGroups[] = 'suffix';
                $validationGroups[] = 'title';
            } else {
                $validationGroups[] = 'corporate_legal_name';
                $validationGroups[] = 'corporate_company_name';
                $validationGroups[] = 'corporate_sic_code';
            }

            $form->setValidationGroup($validationGroups);
            $form->setInputFilter($user->getInputFilterEditBasicInfo());
            if (!$form->isValid()) {
                $errors = $form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submitbutton' && $key != 'submitbutton6' && $key != 'submitbutton1' && $key != 'submitbutton2' && $key != 'submitbutton3' && $key != 'submitbutton4') {
                        foreach ($row as $rower) {
                            $msg [$key] = $this->_config['user_messages']['config']['edit_basic_info'][$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                // print_r($typeSubmit); die();
                $user->exchangeEditBasicInfoArray($form->getData());
                $userBasicInfoData = array();
                $userBasicInfoData = $user->userBasicInfoData($user);
                $userBasicInfoData['contact_id'] = $this->decrypt($request->getPost('contact_id'));
                //asd($userBasicInfoData);
                $userBasicInfoData["typeSubmit"] = $typeSubmit;
                $arrResult = $this->getTables()->updateUserInfo($userBasicInfoData);


                if ($userBasicInfoData['profile_image'] != $userDetail['profile_image'] && $userBasicInfoData['profile_image'] != '') {
                    $this->moveFileFromTempToContact($userBasicInfoData['profile_image'], $userBasicInfoData['contact_id']);
                }

                $fileName1 = $this->_config['file_upload_path']['assets_url'] . "user/" . $folderStrucByUser . "image/thumbnail/" . $arrResult['profile_image'];
                $filePath1 = $this->_config['file_upload_path']['assets_upload_dir'] . "user/" . $folderStrucByUser . "image/thumbnail/" . $arrResult['profile_image'];
                if (file_exists($filePath1) and isset($arrResult['profile_image']) and trim($arrResult['profile_image']) != "" and $arrResult['profile_image'] != NULL) {
                    $fileName2 = $fileName1;
                } else {
                    $fileName2 = $this->_config['img_file_path']['path'] . '/images/foundation/edit-profile.png';
                }

                if ($arrResult['profile_image'] == NULL) {
                    $fileName2 = $this->_config['img_file_path']['path'] . '/images/foundation/edit-profile.png';
                }


                $arRes = array(
                    'corporate_company_name' => isset($arrResult['company_name']) ? $arrResult['company_name'] : '',
                    'corporate_legal_name' => isset($arrResult['legal_name']) ? $arrResult['legal_name'] : '',
                    'corporate_sic_code' => isset($arrResult['sic_code']) ? $arrResult['sic_code'] : '',
                    'upload_profile_image_id' => isset($arrResult['profile_image']) ? $arrResult['profile_image'] : '',
                    'title' => isset($arrResult['title']) ? $arrResult['title'] : '',
                    'first_name' => isset($arrResult['first_name']) ? $arrResult['first_name'] : '',
                    'middle_name' => isset($arrResult['middle_name']) ? $arrResult['middle_name'] : '',
                    'last_name' => isset($arrResult['last_name']) ? $arrResult['last_name'] : '',
                    'fileImageName' => $fileName2
                );



                //  $this->flashMessenger()->addMessage($this->_config['user_messages']['config']['edit_basic_info']['DATA_UPDATED_SUCCCESS']);
                $messages = array('status' => "success", 'jsondata' => json_encode($arRes), 'message' => $this->_config['user_messages']['config']['edit_basic_info']['DATA_UPDATED_SUCCCESS']);
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }



        // Update Email info - start 
        $formEmailInfo = new EditUserEmailInfoForm();
        if (!empty($userDetail)) {
            $formEmailInfo->get('contact_id')->setAttribute('value', $this->encrypt($userDetail['user_id']));
            $formEmailInfo->get('username')->setAttribute('value', $userDetail['username']);
        }
        // Update Email info - end
        // Security Question - Start
        $formQuestion = new EditUserSecurityQuestionForm();
        $secQuestion = $this->getServiceLocator()->get('Common\Model\QuestionTable')->getSecurityQuestion();
        $secQuestionList = array();
        $secQuestionList[' '] = 'Select Security Question';
        foreach ($secQuestion as $key => $val) {
            $secQuestionList[$val['security_question_id']] = $val['security_question'];
        }
        $formQuestion->get('security_question')->setAttribute('options', $secQuestionList);
        if (!empty($userDetail)) {
            $formQuestion->get('contact_id')->setAttribute('value', $this->encrypt($userDetail['user_id']));
        }
        // Security Question - End
        // Password - start
        $formUserPassword = new EditUserPasswordForm();
        if (!empty($userDetail)) {
            $formUserPassword->get('contact_id')->setAttribute('value', $this->encrypt($userDetail['user_id']));
        }
        // Password - end

        $viewModel->setVariables(array(
            'form' => $form,
            'formEmailInfo' => $formEmailInfo,
            'formQuestion' => $formQuestion,
            'formUserPassword' => $formUserPassword,
            'securityQuestion' => isset($userDetail['security_question']) ? $userDetail['security_question'] : '',
            'username' => $userDetail['username'],
            'profileImage' => $userDetail['profile_image'],
            'userType' => $userDetail['user_type'],
            'assetsUploadDir' => $assetsUploadDir,
            'uploadedFilePath' => $uploadedFilePath,
            'flashMessages' => $flashMessages,
            'jsLangTranslate' => array_merge($this->_config['user_messages']['config']['edit_basic_info'], $this->_config['user_messages']['config']['user_edit_mail'], $this->_config['user_messages']['config']['user_edit_security_question'], $this->_config['user_messages']['config']['user_update_password']),
            'userDetail' => $userDetail
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to edit user pic
     * @author Icreon Tech-DG
     */
    public function uploadUserPicAction() {
        $this->checkFrontUserAuthentication();
        $this->getConfig();
        $params = $this->params()->fromRoute();
        $userId = $this->decrypt($params['user_id']);
        $fileExt = $this->GetFileExt($_FILES['upload_profile_image']['name']);
        $filename = $this->GenerateMediaFileName($fileExt[1]);    //Generate file name
        $_FILES['upload_profile_image']['name'] = $filename;                 // assign name to file variable

        $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration
        $error_messages = array(
            'post_max_size' => 'The uploaded file exceeds the post_max_size directive in php.ini', // Set error messages
            'max_file_size' => 'File is too big',
            'min_file_size' => 'File is too small',
            'accept_file_types' => 'Filetype not allowed',
        );
        $options = array(
            'upload_dir' => $upload_file_path['temp_upload_dir'],
            'param_name' => 'upload_profile_image', //file input name                      // Set configuration
            'inline_file_types' => $upload_file_path['file_types_allowed'],
            'accept_file_types' => $upload_file_path['file_types_allowed'],
            'max_file_size' => $upload_file_path['max_allowed_file_size'],
            'min_file_size' => $upload_file_path['min_allowed_file_size'],
            'image_versions' => array(
                'medium' => array(
                    'max_width' => $this->_config['file_upload_path']['user_medium_width'],
                    'max_height' => $this->_config['file_upload_path']['user_medium_height']
                ),
                'thumbnail' => array(
                    'max_width' => $this->_config['file_upload_path']['user_thumb_width'],
                    'max_height' => $this->_config['file_upload_path']['user_thumb_height']
            ))
        );
        $upload_handler = new UploadHandler($options, true, $error_messages);
        $file_response = $upload_handler->jsonResponceData;

        // $height = $this->getHeight($upload_file_path['assets_url'] . 'temp/medium/' . $file_response['upload_profile_image'][0]->name);
        // $width = $this->getWidth($upload_file_path['assets_url'] . 'temp/medium/' . $file_response['upload_profile_image'][0]->name);
        if (isset($file_response['upload_profile_image'][0]->error)) {
            $messages = array('status' => "error", 'message' => $file_response['upload_profile_image'][0]->error);
        } else {
            $this->moveFileFromTempToContact($userId, $file_response['upload_profile_image'][0]->name);
            $messages = array('status' => "success", 'message' => 'Image Uploaded', 'filename' => $file_response['upload_profile_image'][0]->name, 'assets_url' => $upload_file_path['assets_url'] . 'temp/medium/', 'height' => $this->_config['file_upload_path']['user_medium_height'], 'width' => $this->_config['file_upload_path']['user_thumb_width']);
        }
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

//You do not need to alter these functions
    function getHeight($image) {
        $sizes = @getimagesize($image);
        $height = $sizes[1];
        return $height;
    }

//You do not need to alter these functions
    function getWidth($image) {
        $sizes = @getimagesize($image);
        $width = $sizes[0];
        return $width;
    }

    /**
     * This action is used to move file from temp to user folder
     * @return json
     * @param 1 for add , 2 for edit
     * @author Icreon Tech - DG
     */
    public function moveFileFromTempToContact($filename, $user_id) {
        $this->getConfig();
        $temp_dir = $this->_config['file_upload_path']['temp_upload_dir'];
        $temp_thumbnail_dir = $this->_config['file_upload_path']['temp_upload_thumbnail_dir'];
        $temp_medium_dir = $this->_config['file_upload_path']['temp_upload_medium_dir'];
        $user_dir = $this->_config['file_upload_path']['assets_upload_dir'] . "/user/";
        $user_image = 'image/';
        $user_img_thumb = 'thumbnail/';
        $user_img_med = 'medium/';
        $folder_struc_by_user = $this->FolderStructByUserId($user_id);
        $user_image_dir = $user_dir . $folder_struc_by_user . $user_image;
        $user_thumb_image_dir = $user_dir . $folder_struc_by_user . $user_image . $user_img_thumb;
        $user_med_image_dir = $user_dir . $folder_struc_by_user . $user_image . $user_img_med;
        if (!is_dir($user_image_dir)) {
            mkdir($user_image_dir, 0777, TRUE);
        }
        if (!is_dir($user_thumb_image_dir)) {
            mkdir($user_thumb_image_dir, 0777, TRUE);
        }
        if (!is_dir($user_med_image_dir)) {
            mkdir($user_med_image_dir, 0777, TRUE);
        }
        $temp_file = $temp_dir . $filename;
        $temp_thumbnail_file = $temp_thumbnail_dir . $filename;
        $temp_medium_file = $temp_medium_dir . $filename;
        $user_filename = $user_image_dir . $filename;
        $user_thumbnail_filename = $user_thumb_image_dir . $filename;
        $user_medium_filename = $user_med_image_dir . $filename;

        if (file_exists($temp_file)) {
            if (copy($temp_file, $user_filename)) {
                unlink($temp_file);
            }
        }
        if (file_exists($temp_thumbnail_file)) {
            if (copy($temp_thumbnail_file, $user_thumbnail_filename)) {
                unlink($temp_thumbnail_file);
            }
        }
        if (file_exists($temp_medium_file)) {
            if (copy($temp_medium_file, $user_medium_filename)) {
                unlink($temp_medium_file);
            }
        }
    }

    /**
     * This Action is used to edit user password
     * @author Icreon Tech-DG
     */
    public function editUserPasswordAction() {
        $this->getConfig();
        $this->getTables();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $this->layout('web-popup');
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $user = new User($this->_adapter);
        $form = new EditUserPasswordForm();
        $params = $this->params()->fromRoute();

        $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->decrypt($params['user_id'])));

        if (!empty($userDetail)) {
            $form->get('contact_id')->setAttribute('value', $this->encrypt($userDetail['user_id']));
        }
        if ($request->isPost()) {
            $form->setData($request->getPost());
            $form->setInputFilter($user->getInputFilterUpdatePassword());
            if (!$form->isValid()) {
                $errors = $form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            $msg [$key] = $this->_config['user_messages']['config']['user_update_password'][$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $user->exchangeUpdatePasswordArray($form->getData());

                if (strtolower($userDetail['security_answer']) != strtolower($user->security_answer) and $user->security_answer != "") {
                    $msg = array('security_answer' => $this->_config['user_messages']['config']['user_update_password']['NOT_MATCH_SECURITY_ANSWER']);
                    $messages = array('status' => "error", 'message' => $msg);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                } else {
                    $this->getTables()->updatePassword(array('user_id' => $this->decrypt($request->getPost('contact_id')), 'new_password' => $request->getPost('password')));
                    // $this->flashMessenger()->addMessage($this->_config['user_messages']['config']['user_update_password']['DATA_UPDATED_SUCCCESS']);
                    $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['user_update_password']['DATA_UPDATED_SUCCCESS']);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                }
            }
        }
        $viewModel->setVariables(array(
            'form' => $form,
            'securityQuestion' => $userDetail['security_question'],
            'jsLangTranslate' => $this->_config['user_messages']['config']['user_update_password']
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to edit security question
     * @author Icreon Tech-DG
     */
    public function editUserSecurityQuestionAction() {
        $this->getConfig();
        $this->getTables();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $this->layout('web-popup');
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $user = new User($this->_adapter);
        $form = new EditUserSecurityQuestionForm();
        $params = $this->params()->fromRoute();

        $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->decrypt($params['user_id'])));

        $secQuestion = $this->getServiceLocator()->get('Common\Model\QuestionTable')->getSecurityQuestion();
        $secQuestionList = array();
        $secQuestionList[''] = 'Select Security Question';
        foreach ($secQuestion as $key => $val) {
            $secQuestionList[$val['security_question_id']] = $val['security_question'];
        }
        $form->get('security_question')->setAttribute('options', $secQuestionList);

        if (!empty($userDetail)) {
            $form->get('contact_id')->setAttribute('value', $this->encrypt($userDetail['user_id']));
        }
        if ($request->isPost()) {
            $form->setInputFilter($user->getInputFilterEditUserSecurityQuestion());
            $form->setData($request->getPost());
            if (!$form->isValid()) {
                $errors = $form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            $msg [$key] = $this->_config['user_messages']['config']['user_edit_security_question'][$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $user->exchangeEditSecurityQuestionArray($form->getData());
                $userPassword = md5($user->password . $userDetail['salt']);
                if ($userDetail['password'] != $userPassword) {
                    $msg = array('password' => $this->_config['user_messages']['config']['user_edit_security_question']['INVALID_PASSWORD']);
                    $messages = array('status' => "error", 'message' => $msg);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                } else if (strtolower($userDetail['security_answer']) != strtolower($user->confirm_security_answer) and $user->confirm_security_answer != "") {
                    $msg = array('confirm_security_answer' => $this->_config['user_messages']['config']['user_edit_security_question']['NOT_MATCH_SECURITY_ANSWER']);
                    $messages = array('status' => "error", 'message' => $msg);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                } else {
                    $resQuestion = $this->getTables()->updateUserSecurityQuestionInfo(array('user_id' => $this->decrypt($request->getPost('contact_id')), 'security_question' => $user->security_question, 'security_answer' => $user->security_answer));

                    $secQName = '';
                    $secAnsName = '';
                    if (isset($resQuestion['security_question_id'])) {
                        foreach ($secQuestion as $key => $val) {
                            if ($resQuestion['security_question_id'] == $val['security_question_id']) {
                                $secQName = $val['security_question'];
                            }
                        }
                    }

                    if (isset($resQuestion['security_answer']) and trim($resQuestion['security_answer'])) {
                        $secAnsName = trim($resQuestion['security_answer']);
                    }

                    $arrRes = array(
                        'secQName' => isset($secQName) ? $secQName : '',
                        'secAnsName' => isset($secAnsName) ? $secAnsName : ''
                    );


                    // $this->flashMessenger()->addMessage($this->_config['user_messages']['config']['user_edit_security_question']['DATA_UPDATED_SUCCCESS']);
                    $messages = array('status' => "success", 'jsondata' => json_encode($arrRes), 'message' => $this->_config['user_messages']['config']['user_edit_security_question']['DATA_UPDATED_SUCCCESS']);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                }
            }
        }
        $viewModel->setVariables(array(
            'form' => $form,
            'securityQuestion' => $userDetail['security_question'],
            'jsLangTranslate' => $this->_config['user_messages']['config']['user_edit_security_question']
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to edit user email
     * @author Icreon Tech-DG
     */
    public function editUserEmailInfoAction() {
        $request = $this->getRequest();
        $this->getConfig();
        $this->getTables();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        $this->layout('web-popup');
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $user = new User($this->_adapter);
        $form = new EditUserEmailInfoForm();
        $params = $this->params()->fromRoute();

        $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->decrypt($params['user_id'])));

        if (!empty($userDetail)) {
            $form->get('contact_id')->setAttribute('value', $this->encrypt($userDetail['user_id']));
            $form->get('username')->setAttribute('value', $userDetail['username']);
        }
        if ($request->isPost()) {
            $form->setData($request->getPost());
            $form->setInputFilter($user->getInputFilterEditEmailInfo());
            if (!$form->isValid()) {
                $errors = $form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            $msg [$key] = $this->_config['user_messages']['config']['user_edit_mail'][$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $user->exchangeEditEmailArray($form->getData());

                $userPassword = md5($user->password . $userDetail['salt']);
                if ($userDetail['password'] != $userPassword) {
                    $msg = array('password' => $this->_config['user_messages']['config']['user_edit_mail']['INVALID_PASSWORD']);
                    $messages = array('status' => "error", 'message' => $msg);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                } else if (strtolower($userDetail['security_answer']) != strtolower($user->confirm_security_answer) and $user->confirm_security_answer != "") {
                    $msg = array('confirm_security_answer' => $this->_config['user_messages']['config']['user_edit_mail']['NOT_MATCH_SECURITY_ANSWER']);
                    $messages = array('status' => "error", 'message' => $msg);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                } else {
                    if (isset($user->email_id) && $user->email_id != '') {
                        $isEmailExits = $this->getServiceLocator()->get('User\Model\UserTable')->isEmailExits(array('email_id' => $user->email_id));
                        if ($isEmailExits) {
                            $messages = array('status' => "error", 'result' => $isEmailExits, 'message' => $this->_config['user_messages']['config']['user_signup']['EMAIL_ID_ALREADY']);
                            return $response->setContent(\Zend\Json\Json::encode($messages));
                        }
                    }

                    $verifyCode = $this->GetRandomAlphaNumeric(8);
                    $this->getTables()->updateUserEmailInfo(array('user_id' => $this->decrypt($request->getPost('contact_id')), 'email_id' => $user->email_id, 'verification_code' => $verifyCode));

                    $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->decrypt($params['user_id'])));

                    if ($userDetail['user_type'] != 1) {
                        $userDetail['first_name'] = $userDetail['company_name'];
                    }

                    $userDataArr = $userDetail;
                    $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                    $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                    $userDataArr['passenger_id'] = $this->_config['soleif_email']['name'];

                    $updateEmailTemplateId = 25;
                    $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $updateEmailTemplateId, $this->_config['email_options']);

                    $updateEmailTemplateId = 26;
                    $userDataArr['email_id'] = $userDataArr['alternate_email_id'];
                    $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $updateEmailTemplateId, $this->_config['email_options']);
                    // $this->flashMessenger()->addMessage($this->_config['user_messages']['config']['user_edit_mail']['DATA_UPDATED_SUCCCESS']);
                    $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['user_edit_mail']['DATA_UPDATED_SUCCCESS']);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                }
            }
        }
        $viewModel->setVariables(array(
            'form' => $form,
            'username' => $userDetail['username'],
            'securityQuestion' => $userDetail['security_question'],
            'jsLangTranslate' => $this->_config['user_messages']['config']['user_edit_mail']
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to edit user conatct info
     * @author Icreon Tech-DG
     */
    public function editUserContactInfoAction() {
        $this->checkFrontUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $flashMessages = '';
        if ($this->_flashMessage->hasMessages()) {
            $flashMessages = $this->_flashMessage->getMessages();
        }
        $params = $this->params()->fromRoute();

        $userAddressInfo = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations(array('user_id' => $this->decrypt($params['user_id'])));
        $userPhoneInfo = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserPhoneInformations(array('user_id' => $this->decrypt($params['user_id'])));

        $viewModel->setVariables(array(
            'flashMessages' => $flashMessages,
            'userPhoneInfo' => $userPhoneInfo,
            'userAddressInfo' => $userAddressInfo,
            'jsLangTranslate' => $this->_config['user_messages']['config']['edit_contact_info']
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to delete phone info
     * @author Icreon Tech-DG
     * @return Delete Phone (JSON)
     */
    public function deleteUserPhoneInfoAction() {
        $this->getTables();
        $this->getConfig();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam['phone_id'] = $request->getPost('phone_id');
            $this->getTables()->deleteUserPhoneInfo($dataParam);
            $this->flashMessenger()->addMessage($this->_config['user_messages']['config']['edit_contact_info']['DATA_DELETED_SUCCCESS']);
            $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['edit_contact_info']['DATA_DELETED_SUCCCESS']);
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This Action is used to delete phone info
     * @author Icreon Tech-DG
     * @return Delete Address (JSON)
     */
    public function deleteUserAddressInfoAction() {
        $this->getTables();
        $this->getConfig();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam['address_id'] = $request->getPost('address_id');
            $dataParam['is_delete'] = 1;
            $this->getTables()->deleteUserAddressInfo($dataParam);
            $this->flashMessenger()->addMessage($this->_config['user_messages']['config']['edit_contact_info']['DATA_DELETED_SUCCCESS']);
            $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['edit_contact_info']['DATA_DELETED_SUCCCESS']);
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This Action is used to add/edit phone info
     * @author Icreon Tech-DG
     * @return Add Edit Phone (JSON)
     */
    public function addEditUserPhoneInfoAction() {
        $this->checkFrontUserAuthentication();
        $auth = new AuthenticationService();
        $this->getConfig();
        $this->getTables();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $user = new User($this->_adapter);
        $form_phone = new ContactPhonenoForm();

        $params = $this->params()->fromRoute();
        $mode = '';
        if (isset($params['mode'])) {
            $mode = $this->decrypt($params['mode']);
        }
        $this->auth = $auth;
        $userDetail = (array) $this->auth->getIdentity();
        if ($userDetail['user_type'] == 1) {
            $phoneTypeIndividual = array(' ' => 'Select', 'home' => 'Home', 'cell' => 'Mobile', 'work' => 'Work', 'fax' => 'Fax', 'others' => 'Others');
        } else {
            $phoneTypeIndividual = array(' ' => 'Select', 'main' => 'Main', 'direct' => 'Direct', 'fax' => 'Fax', 'others' => 'Others');
        }
        $form_phone->get('phone_type[]')->setAttribute('options', $phoneTypeIndividual);
        $form_phone->get('phone_type[]')->setAttribute('name', 'phone_type');
        $form_phone->get('country_code[]')->setAttribute('name', 'country_code');
        $form_phone->get('area_code[]')->setAttribute('name', 'area_code');
        $form_phone->get('phone_no[]')->setAttribute('name', 'phone_no');
        $form_phone->get('extension[]')->setAttribute('name', 'extension');

        $form_phone->get('phone_type[]')->setAttribute('id', 'phone_type');
        $form_phone->get('country_code[]')->setAttribute('id', 'country_code');
        $form_phone->get('area_code[]')->setAttribute('id', 'area_code');
        $form_phone->get('phone_no[]')->setAttribute('id', 'phone_no');
        $form_phone->get('extension[]')->setAttribute('id', 'extension');

        $form_phone->add(array(
            'name' => 'phone_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'phone_id',
            ),
        ));
        $form_phone->add(array(
            'name' => 'submitbutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save Phone',
                'id' => 'submitbutton',
                'class' => 'button'
            ),
        ));
        $form_phone->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Cancel',
                'id' => 'cancelbutton',
                'class' => 'myfile-edit-blue edit-green button',
                'onClick' => "closediv('phone_info_div');"
            ),
        ));
        $form_phone->remove('continfo_primary[]');

        $params = $this->params()->fromRoute();
        if (isset($params['phone_id'])) {
            $form_phone->get('submitbutton')->setValue('Update Phone Number');
            $phoneInfo = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserPhoneInformations(array('phone_id' => $params['phone_id']));
            if (!empty($phoneInfo)) {
                $form_phone->get('phone_id')->setValue($phoneInfo[0]['phone_contact_id']);
                $form_phone->get('phone_type[]')->setValue($phoneInfo[0]['type']);
                $form_phone->get('country_code[]')->setValue($phoneInfo[0]['country_code']);
                $form_phone->get('area_code[]')->setValue($phoneInfo[0]['area_code']);
                $form_phone->get('phone_no[]')->setValue($phoneInfo[0]['phone_number']);
                $form_phone->get('extension[]')->setValue($phoneInfo[0]['extension']);
                $form_phone->get('is_primary')->setValue($phoneInfo[0]['is_primary']);
                if (!empty($phoneInfo[0]['is_primary']))
                    $form_phone->get('is_primary')->setAttribute('disabled', 'true');
            }
        }
        if ($request->isPost()) {
            $form_phone->setData($request->getPost());
            $form_phone->setInputFilter($user->getUserPhoneInfoInputFilter());
            if (!$form_phone->isValid()) {
                $errors = $form_phone->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submitbutton') {
                        foreach ($row as $rower) {
                            $msg [$key] = $this->_config['user_messages']['config']['edit_contact_info'][$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $user->exchangeArrayUserPhoneInfo($form_phone->getData());
                $phon_form = array();


                $phon_form['phone_id'] = isset($user->phone_id) ? $user->phone_id : null;
                $phon_form['phone_type'] = isset($user->phone_type) ? $user->phone_type : null;
                $phon_form['country_code'] = isset($user->country_code) ? $user->country_code : null;
                $phon_form['area_code'] = isset($user->area_code) ? $user->area_code : null;
                $phon_form['phone_no'] = isset($user->phone_no) ? $user->phone_no : null;
                $phon_form['extension'] = isset($user->extension) ? $user->extension : null;
                //$phon_form['is_primary'] = isset($user->is_primary) ? $user->is_primary : null;
                $phon_form['continfo_primary'] = isset($user->is_primary) ? $user->is_primary : null;
                $phon_form['modified_date'] = DATE_TIME_FORMAT;

                if (!empty($phon_form['continfo_primary']))
                    $this->getServiceLocator()->get('User\Model\ContactTable')->updateUserPrimaryPhone(array('user_id' => $auth->getIdentity()->user_id));

                if ($phon_form['phone_type'] != '' || $phon_form['country_code'] != '' || $phon_form['area_code'] != '' || $phon_form['phone_no'] != '' || $phon_form['extension'] != '') {
                    if (isset($user->phone_id) && $user->phone_id != '') {
                        $this->getServiceLocator()->get('User\Model\UserTable')->updatePhoneInformations($phon_form);
                        $this->flashMessenger()->addMessage($this->_config['user_messages']['config']['edit_contact_info']['PHONE_UPDATED_SUCCCESS']);
                    } else {
                        $phon_form['user_id'] = $auth->getIdentity()->user_id;
                        $phon_form['add_date'] = DATE_TIME_FORMAT;
                        $this->getServiceLocator()->get('User\Model\ContactTable')->insertPhoneInformations($phon_form);
                        $this->flashMessenger()->addMessage($this->_config['user_messages']['config']['edit_contact_info']['PHONE_SAVED_SUCCCESS']);
                    }
                    $messages = array('status' => "success");
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                }
            }
        }

        $viewModel->setVariables(array(
            'form_phone' => $form_phone,
            'jsLangTranslate' => $this->_config['user_messages']['config']['edit_contact_info']
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to add/edit address info
     * @author Icreon Tech-DG
     * @return Add Edit Address (JSON)
     */
    public function addEditUserAddressInfoAction() {


        $this->checkFrontUserAuthentication();
        $auth = new AuthenticationService();
        $this->getConfig();
        $this->getTables();

        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
             
        
        $applicationConfigIds = $this->_config['application_config_ids'];
        $uSId = $applicationConfigIds['united_state_country_id'];
        $user = new User($this->_adapter);
        $params = $this->params()->fromRoute();
        $form_address = new ContactAddressesFrontForm();

        $apoPoStates = $this->_config['transaction_config']['apo_po_state'];
        $form_address->get('apo_po_state')->setAttribute('options', $apoPoStates);

        $country = $this->getServiceLocator()->get('Common\Model\CountryTable')->getCountry();
        $country_list = array();
        $country_list[' '] = 'Select Country';
        foreach ($country as $key => $val) {
            $country_list[$val['country_id']] = $val['name'];
        }
        
        $getTitle = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTitle();
        $user_title = array();
        $userTitle[' '] = 'None';
        foreach ($getTitle as $key => $val) {
            $userTitle[$val['title']] = $val['title'];
        }        
        $form_address->get('title')->setAttribute('options', $userTitle);
        $form_address->get('addressinfo_country[]')->setAttribute('options', $country_list);
        $form_address->get('addressinfo_country[]')->setAttribute('name', 'addressinfo_country');

        $get_location_type = $this->getServiceLocator()->get('Common\Model\CommonTable')->getLocationType('');
        $location_type = array();
        $location_type[' '] = 'Select';
        foreach ($get_location_type as $key => $val) {
            $location_type[$val['location_id']] = $val['location'];
        }
        $form_address->get('addressinfo_location_type[]')->setAttribute('options', $location_type);
        $form_address->get('addressinfo_location_type[]')->setAttribute('name', 'addressinfo_location_type');
        $form_address->get('addressinfo_location_billing[]')->setAttribute('name', 'addressinfo_location_billing');
        $form_address->get('addressinfo_location_shipping[]')->setAttribute('name', 'addressinfo_location_shipping');
        $form_address->get('addressinfo_street_address_1[]')->setAttribute('name', 'addressinfo_street_address_1');
        $form_address->get('addressinfo_street_address_2[]')->setAttribute('name', 'addressinfo_street_address_2');
        $form_address->get('addressinfo_city[]')->setAttribute('name', 'addressinfo_city');
        $form_address->get('addressinfo_state[]')->setAttribute('name', 'addressinfo_state');
        $form_address->get('addressinfo_zip[]')->setAttribute('name', 'addressinfo_zip');
        $form_address->get('address_state_id[]')->setAttribute('name', 'address_state_id');
        $form_address->get('is_primary[]')->setAttribute('name', 'is_primary');

        $form_address->get('addressinfo_country[]')->setAttribute('id', '0');
        $form_address->get('addressinfo_location_type[]')->setAttribute('id', 'addressinfo_location_type');
        $form_address->get('addressinfo_location_billing[]')->setAttribute('id', 'addressinfo_location_billing');
        $form_address->get('addressinfo_location_shipping[]')->setAttribute('id', 'addressinfo_location_shipping');
        $form_address->get('is_primary[]')->setAttribute('id', 'is_primary');
        $form_address->get('addressinfo_street_address_1[]')->setAttribute('id', 'addressinfo_street_address_1');
        $form_address->get('addressinfo_street_address_2[]')->setAttribute('id', 'addressinfo_street_address_2');
        $form_address->get('addressinfo_city[]')->setAttribute('id', 'addressinfo_city');
        $form_address->get('addressinfo_state[]')->setAttribute('id', 'addressinfo_state');
        $form_address->get('addressinfo_zip[]')->setAttribute('id', 'addressinfo_zip');
        $form_address->get('address_state_id[]')->setAttribute('id', 'address_state_id_0');

        $form_address->remove('primary_location0');
        $form_address->remove('addressinfo_location[]');
        $form_address->remove('addressinfo_location_primary[]');
        $form_address->remove('primary_location_count');
        $form_address->remove('addressinfo_another_contact_address[]');
        $form_address->remove('addressinfo_street_address_3[]');
        $form_address->remove('addressinfo_search_contact[]');

        $state = $this->getServiceLocator()->get('Common\Model\CommonTable')->getStates();
        $stateList = array();
        $stateList[' '] = 'Select State';
        foreach ($state as $key => $val) {
            $stateList[$val['state_code']] = $val['state'] . " - " . $val['state_code'];
        }
        $form_address->get('address_state_id[]')->setAttribute('options', $stateList);

        $form_address->add(array(
            'name' => 'address_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'address_id',
            ),
        ));

        $form_address->add(array(
            'name' => 'submitbutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save Address',
                'id' => 'submitbutton',
                'class' => 'button'
            ),
        ));


        $form_address->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Cancel',
                'id' => 'cancelbutton',
                'class' => 'myfile-edit-blue edit-green button',
                'onClick' => "closediv('address_info_div');"
            ),
        ));

        if (isset($params['address_id'])) {
            $form_address->get('submitbutton')->setValue('Update Address');
            $addressInfo = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations(array('address_information_id' => $params['address_id']));

            if (!empty($addressInfo)) {
                $form_address->get('address_id')->setValue($addressInfo[0]['address_information_id']);
                
                $form_address->get('title')->setValue($addressInfo[0]['title']);
                $form_address->get('first_name')->setValue($addressInfo[0]['first_name']);
              //  $form_address->get('middle_name')->setValue($addressInfo[0]['middle_name']);
                $form_address->get('last_name')->setValue($addressInfo[0]['last_name']);
                
                
                $form_address->get('addressinfo_city[]')->setValue($addressInfo[0]['city']);
                $form_address->get('addressinfo_zip[]')->setValue($addressInfo[0]['zip_code']);
                $form_address->get('address_state_id[]')->setValue($addressInfo[0]['state']);
                $form_address->get('addressinfo_street_address_2[]')->setValue($addressInfo[0]['street_address_two']);
                $form_address->get('addressinfo_street_address_1[]')->setValue($addressInfo[0]['street_address_one']);
                $form_address->get('addressinfo_location_type[]')->setValue($addressInfo[0]['location_id']);

                if (empty($addressInfo[0]['address_type']))
                    $addressInfo[0]['address_type'] = 1;
                else
                    $addressInfo[0]['address_type'] = $addressInfo[0]['address_type'];
                $form_address->get('is_apo_po_record')->setValue($addressInfo[0]['address_type']);

                if ($addressInfo[0]['address_type'] == 1) {
                    $form_address->get('addressinfo_country[]')->setValue($addressInfo[0]['country_id']);
                    $form_address->get('addressinfo_state[]')->setValue($addressInfo[0]['state']);
                } else if ($addressInfo[0]['address_type'] == 2) {
                    $form_address->get('apo_po_shipping_country')->setValue($addressInfo[0]['country_id']);
                    $form_address->get('address_state_id[]')->setValue($addressInfo[0]['state']);
                } else if ($addressInfo[0]['address_type'] == 3) {
                    $form_address->get('apo_po_shipping_country')->setValue($addressInfo[0]['country_id']);
                    $form_address->get('apo_po_state')->setValue($addressInfo[0]['state']);
                } else {
                    $form_address->get('addressinfo_country[]')->setValue($addressInfo[0]['country_id']);
                    $form_address->get('addressinfo_state[]')->setValue($addressInfo[0]['state']);
                    $form_address->get('address_state_id[]')->setValue($addressInfo[0]['state']);
                }

                if (isset($addressInfo[0]['location_name']) && $addressInfo[0]['location_name'] != '') {
                    $location = explode(",", $addressInfo[0]['location_name']);
                    if (in_array(2, $location)) {
                        $form_address->get('addressinfo_location_billing[]')->setValue(1);
                    }
                    if (in_array(3, $location)) {
                        $form_address->get('addressinfo_location_shipping[]')->setValue(1);
                    }
                    if (in_array(1, $location)) {
                        $form_address->get('is_primary[]')->setValue(1);
                    }
                }
            }
        }

        if (isset($params['mode']) && $params['mode'] == 'creditCard') {
            $form_address->get('addressinfo_location_shipping[]')->setAttribute('type', 'hidden');
            $form_address->get('addressinfo_location_shipping[]')->setValue('1');
            $form_address->get('addressinfo_location_billing[]')->setAttribute('type', 'hidden');
            $form_address->get('addressinfo_location_billing[]')->setValue('1');
        }

        if ($request->isPost()) {
            $form_address->setData($request->getPost());
            //asd($request->getPost());
            $form_address->setInputFilter($user->getUserAddressInfoInputFilter());
            if (!$form_address->isValid()) {
                $errors = $form_address->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submitbutton') {
                        foreach ($row as $rower) {
                            $msg [$key] = $this->_config['user_messages']['config']['edit_contact_info'][$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $formData = $form_address->getData();
                $form_address = array();
                //  asd($formData);
                if (isset($formData['addressinfo_location_shipping'])) {
                    if ($formData['is_apo_po_record'] == 1) { // Residential/CorporatePO
                        $form_address['addressinfo_country'] = isset($formData['addressinfo_country']) ? $formData['addressinfo_country'] : null;

                        if (isset($formData['shipping_state_id']) && $formData['shipping_state_id'] != '')
                            $form_address['addressinfo_state'] = $formData['shipping_state_id'];

                        if (isset($formData['address_state_id']) && $formData['address_state_id'] != '')
                            $form_address['addressinfo_state'] = $formData['address_state_id'];

                        if (isset($formData['addressinfo_state']) && $formData['addressinfo_state'] != '')
                            $form_address['addressinfo_state'] = $formData['addressinfo_state'];

                        $form_address['address_type'] = 1;
                    } else if ($formData['is_apo_po_record'] == 2) { // PO address
                        $form_address['addressinfo_country'] = isset($formData['apo_po_shipping_country']) ? $formData['apo_po_shipping_country'] : null;

                        if (isset($formData['shipping_state_id']) && $formData['shipping_state_id'] != '')
                            $form_address['addressinfo_state'] = $formData['shipping_state_id'];

                        $form_address['addressinfo_state'] = isset($formData['address_state_id']) ? $formData['address_state_id'] : null;
                        $form_address['address_type'] = 2;
                    }
                    if ($formData['is_apo_po_record'] == 3) { // APO Address
                        $form_address['addressinfo_country'] = isset($formData['apo_po_shipping_country']) ? $formData['apo_po_shipping_country'] : null;
                        $form_address['addressinfo_state'] = isset($formData['apo_po_state']) ? $formData['apo_po_state'] : null;
                        $form_address['address_type'] = 3;
                    }
                } else if (isset($formData['addressinfo_location_billing'])) {
                    $form_address['addressinfo_country'] = isset($formData['addressinfo_country']) ? $formData['addressinfo_country'] : null;

                    if (isset($formData['addressinfo_state']) && !empty($formData['addressinfo_state']))
                        $form_address['addressinfo_state'] = isset($formData['addressinfo_state']) ? $formData['addressinfo_state'] : null;
                    else
                        $form_address['addressinfo_state'] = isset($formData['address_state_id']) ? $formData['address_state_id'] : null;
                }
                $form_address['address_id'] = isset($formData['address_id']) ? $formData['address_id'] : null;
                $form_address['addressinfo_zip'] = isset($formData['addressinfo_zip']) ? $formData['addressinfo_zip'] : null;

                $form_address['addressinfo_city'] = isset($formData['addressinfo_city']) ? $formData['addressinfo_city'] : null;
                $form_address['addressinfo_street_address_2'] = isset($formData['addressinfo_street_address_2']) ? $formData['addressinfo_street_address_2'] : null;
                $form_address['addressinfo_street_address_1'] = isset($formData['addressinfo_street_address_1']) ? $formData['addressinfo_street_address_1'] : null;
                $form_address['addressinfo_location_type'] = isset($formData['addressinfo_location_type']) ? $formData['addressinfo_location_type'] : null;
                
                
                $form_address['title'] = isset($formData['title']) ? $formData['title'] : null;
                $form_address['first_name'] = isset($formData['first_name']) ? $formData['first_name'] : null;
            //    $form_address['middle_name'] = isset($formData['middle_name']) ? $formData['middle_name'] : null;
                $form_address['last_name'] = isset($formData['last_name']) ? $formData['last_name'] : null;                

                
                $addressinfoLocation = array();

                if ($params['mode'] == 'creditCard') // this is used while we are adding the record from add credit
                    unset($formData['addressinfo_location_shipping']);

                if (isset($formData['addressinfo_location_billing'])) {
                    $addressinfoLocation[] = 2;
                }
                if (isset($formData['addressinfo_location_shipping'])) {
                    $addressinfoLocation[] = 3;
                }
                if (isset($formData['is_primary'])) {
                    $addressinfoLocation[] = 1;
                }

                if (!empty($addressinfoLocation)) {
                    $form_address['addressinfo_location'] = implode(",", $addressinfoLocation);
                } else {
                    $form_address['addressinfo_location'] = null;
                }
                $form_address['modified_date'] = DATE_TIME_FORMAT;

                if (isset($formData['is_primary'])) {
                    $this->getServiceLocator()->get('User\Model\ContactTable')->updatePrimaryAddressInformations(array('user_id' => $auth->getIdentity()->user_id));
                }
                if (isset($formData['address_id']) && $formData['address_id'] != '') {

                    //  asd($form_address);
                    $this->getServiceLocator()->get('User\Model\UserTable')->updateAddressInformations($form_address);
                    $this->flashMessenger()->addMessage($this->_config['user_messages']['config']['edit_contact_info']['ADDRESS_UPDATED_SUCCCESS']);
                } else {
                    $form_address['user_id'] = $auth->getIdentity()->user_id;
                    $form_address['add_date'] = DATE_TIME_FORMAT;
                    $this->getServiceLocator()->get('User\Model\ContactTable')->insertAddressInfo($form_address);
                    $this->flashMessenger()->addMessage($this->_config['user_messages']['config']['edit_contact_info']['ADDRESS_SAVED_SUCCCESS']);
                }
                $messages = array('status' => "success", 'mode' => $params['mode']);
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $viewModel->setVariables(array(
            'form_address' => $form_address,
            'mode' => $params['mode'],
            'us_state_id_value' => @$addressInfo[0]['state'],
            'jsLangTranslate' => array_merge($this->_config['user_messages']['config']['edit_contact_info'], $applicationConfigIds)
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to add/edit demographic info
     * @author Icreon Tech-DG
     * @return Add Edit Address (JSON)
     */
    public function editUserDemographicsInfoAction() {
        $this->checkFrontUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $user = new User($this->_adapter);
        $params = $this->params()->fromRoute();
        $form = new UserDemographicForm();
        $formEducation = new ContactEducationForm();
        $form->add($formEducation);

        $jobHistoryForm = new JobHistoryForm();
        $form->add($jobHistoryForm);

        $flashMessages = '';
        if ($this->_flashMessage->hasMessages()) {
            $flashMessages = $this->_flashMessage->getMessages();
        }

        $getIndustry = $this->getServiceLocator()->get('Common\Model\CommonTable')->getIndustry();
        $industryList = array();
        $industryList[' '] = 'Select';
        foreach ($getIndustry as $key => $val) {
            $industryList[$val['industry_id']] = $val['industry'];
        }
        $jobHistoryForm->get('industry[]')->setAttribute('options', $industryList);



        $getIncomeRange = $this->getServiceLocator()->get('Common\Model\CommonTable')->getIncomeRange();
        $incomeRange = array();
        $incomeRange[' '] = 'Select';
        foreach ($getIncomeRange as $key => $val) {
            $incomeRange[$val['income_range_id']] = $val['income_range'];
        }
        $form->get('demographics_income_range')->setAttribute('options', $incomeRange);

        if (isset($params['user_id'])) {

            $getUserJobHistory = $this->getServiceLocator()->get('User\Model\DevelopmentTable')->getUserJobHistory(array('user_id' => $this->decrypt($params['user_id'])));
            $userEducation = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserEducations(array('user_id' => $this->decrypt($params['user_id'])));

            $eduIds = '';
            if (!empty($userEducation)) {
                $eduType = array();
                foreach ($userEducation as $val) {
                    $eduType[] = $val['education_id'];
                }
                if (!empty($eduType)) {
                    $eduIds = implode(",", $eduType);
                }
            }

            $getEducationList = $this->getServiceLocator()->get('Common\Model\CommonTable')->getEducationDegreeList(array('education_id' => $eduIds));
            $educationList = array();
            $educationList[' '] = 'Select';
            foreach ($getEducationList as $key => $val) {
                $educationList[$val['education_id']] = $val['education'];
            }
            $formEducation->get('edu_degree_level[]')->setAttribute('options', $educationList);


            $form->get('user_id')->setValue($params['user_id']);
            $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->decrypt($params['user_id'])));

            $getNationality = $this->getServiceLocator()->get('Common\Model\CommonTable')->getNationality(array('nationality_id' => $userDetail['nationality_id']));
            $nationality = array();
            $nationality[' '] = 'Select';
            foreach ($getNationality as $key => $val) {
                $nationality[$val['nationality_id']] = $val['nationality'];
            }
            $form->get('demographics_nationality')->setAttribute('options', $nationality);

            $getEthinicity = $this->getServiceLocator()->get('Common\Model\CommonTable')->getEthinicity(array('ethnicity_id' => $userDetail['ethnicity_id']));
            $ethnicity = array();
            $ethnicity[' '] = 'Select';
            foreach ($getEthinicity as $key => $val) {
                $ethnicity[$val['ethnicity_id']] = $val['ethnicity'];
            }
            $form->get('demographics_ethnicity')->setAttribute('options', $ethnicity);

            if (!empty($userDetail)) {
                $form->get('demographics_gender')->setValue($userDetail['gender']);
                $form->get('demographics_marital_status')->setValue($userDetail['marital_status']);
                $form->get('have_child')->setValue($userDetail['children_below_eighteen']);
                $form->get('demographics_income_range')->setValue($userDetail['income_range_id']);
                $form->get('demographics_nationality')->setValue($userDetail['nationality_id']);
                $form->get('demographics_ethnicity')->setValue($userDetail['ethnicity_id']);
            }
        }
        if ($request->isPost()) {
            $formData = $request->getPost();
            $formDemographic = array();
            $formDemographic['user_id'] = isset($formData['user_id']) ? $this->decrypt($formData['user_id']) : null;
            $formDemographic['gender'] = isset($formData['demographics_gender']) ? $formData['demographics_gender'] : null;
            $formDemographic['have_child'] = isset($formData['have_child']) ? $formData['have_child'] : null;
            $formDemographic['demographics_income_range'] = isset($formData['demographics_income_range']) ? $formData['demographics_income_range'] : null;
            $formDemographic['demographics_nationality'] = isset($formData['demographics_nationality']) ? $formData['demographics_nationality'] : null;
            $formDemographic['demographics_ethnicity'] = isset($formData['demographics_ethnicity']) ? $formData['demographics_ethnicity'] : null;
            $formDemographic['demographics_marital_status'] = isset($formData['demographics_marital_status']) ? $formData['demographics_marital_status'] : null;

            $this->getServiceLocator()->get('User\Model\UserTable')->updateUserDemographics($formDemographic);

            if (!empty($formData['edu_degree_level'])) {
                $eduForm = array();
                for ($i = 0; $i < count($formData['edu_degree_level']); $i++) {
                    $eduForm[$i]['edu_degree_level'] = isset($formData['edu_degree_level'][$i]) ? $formData['edu_degree_level'][$i] : null;
                    $eduForm[$i]['edu_institution'] = isset($formData['edu_institution'][$i]) ? $formData['edu_institution'][$i] : null;
                    $eduForm[$i]['edu_attended_year'] = isset($formData['edu_attended_year'][$i]) ? $formData['edu_attended_year'][$i] : null;
                    $eduForm[$i]['edu_major'] = isset($formData['edu_major'][$i]) ? $formData['edu_major'][$i] : null;
                    $eduForm[$i]['edu_gpa'] = isset($formData['edu_gpa'][$i]) ? $formData['edu_gpa'][$i] : null;
                }
                $this->getServiceLocator()->get('User\Model\UserTable')->deleteEducation(array('user_id' => $this->decrypt($formData['user_id'])));
                foreach ($eduForm as $val) {
                    $val['user_id'] = isset($formData['user_id']) ? $this->decrypt($formData['user_id']) : null;
                    $val['add_date'] = DATE_TIME_FORMAT;
                    $val['modified_date'] = DATE_TIME_FORMAT;
                    $this->getServiceLocator()->get('User\Model\ContactTable')->insertEducation($val);
                }
            }

            if (!empty($formData['job_title'])) {
                $this->getServiceLocator()->get('User\Model\DevelopmentTable')->deleteUserJobHistory(array('user_id' => $this->decrypt($formData['user_id'])));
                $jobhistoryForm = array();
                for ($i = 0; $i < count($formData['job_title']); $i++) {
                    $jobhistoryForm[$i]['job_title'] = isset($formData['job_title'][$i]) ? $formData['job_title'][$i] : null;
                    $jobhistoryForm[$i]['industry'] = isset($formData['industry'][$i]) ? $formData['industry'][$i] : null;
                    $jobhistoryForm[$i]['period_from'] = isset($formData['period_from'][$i]) ? $this->DateFormat($formData['period_from'][$i], 'db_datetime_format') : null;
                    $jobhistoryForm[$i]['period_to'] = isset($formData['period_to'][$i]) ? $this->DateFormat($formData['period_to'][$i], 'db_datetime_format') : null;
                    if (isset($formData['company_id'][$i]) && $formData['company_id'][$i] != '') {
                        $jobhistoryForm[$i]['company_id'] = isset($formData['company_id'][$i]) ? $formData['company_id'][$i] : null;
                    } else {
                        if (isset($formData['comp_name'][$i]) && $formData['comp_name'][$i] != '') {
                            $jobhistoryForm[$i]['company_id'] = $companyId = $this->getServiceLocator()->get('User\Model\ContactTable')->insertMasterCompanies(array('company_name' => $formData['comp_name'][$i]));
                        }
                    }
                }
                foreach ($jobhistoryForm as $val) {
                    $val['user_id'] = $this->decrypt($formData['user_id']);
                    $this->getServiceLocator()->get('User\Model\DevelopmentTable')->insertUserJobHistory($val);
                }
            }

            $this->flashMessenger()->addMessage($this->_config['user_messages']['config']['edit_user_demographic']['DEMOGRAPHIC_UPDATED_SUCCCESS']);
            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
        $viewModel->setVariables(array(
            'form' => $form,
            'flashMessages' => $flashMessages,
            'getUserJobHistory' => $getUserJobHistory,
            'userEducation' => $userEducation,
            'jsLangTranslate' => $this->_config['user_messages']['config']['edit_user_demographic']
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to job history form elements .
     * @author Icreon Tech-DG
     */
    public function addUserJobInfoAction() {
        $jobHistoryForm = new JobHistoryForm();
        $this->getConfig();
        $params = $this->params()->fromRoute();
        $getIndustry = $this->getServiceLocator()->get('Common\Model\CommonTable')->getIndustry();
        $industryList = array();
        $industryList[' '] = 'Select';
        foreach ($getIndustry as $key => $val) {
            $industryList[$val['industry_id']] = $val['industry'];
        }
        $jobHistoryForm->get('industry[]')->setAttribute('options', $industryList);
        $data['counter'] = $params['counter'];
        $data['container'] = $params['container'];

        $jobHistoryForm->get('period_from[]')->setAttribute('id', 'period_from' . $data['counter']);
        $jobHistoryForm->get('period_to[]')->setAttribute('id', 'period_to' . $data['counter']);
        $jobHistoryForm->get('company_id[]')->setAttribute('id', 'company_id' . $data['counter']);
        $jobHistoryForm->get('comp_name[]')->setAttribute('id', 'comp_name' . $data['counter']);
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jobHistoryForm' => $jobHistoryForm,
            'jsLangTranslate' => $this->_config['user_messages']['config']['edit_user_demographic'],
            'params' => $data,
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to dispaly education form elements .
     * @author Icreon Tech-DG
     * @return Education Form
     */
    public function addUserEducationInfoAction() {
        $formEducation = new ContactEducationForm();
        $this->getConfig();
        $params = $this->params()->fromRoute();
        $data['counter'] = $params['counter'];
        $data['container'] = $params['container'];
        $getEducationList = $this->getServiceLocator()->get('Common\Model\CommonTable')->getEducationDegreeList('');
        $educationList = array();
        $educationList[' '] = 'Select';
        foreach ($getEducationList as $key => $val) {
            $educationList[$val['education_id']] = $val['education'];
        }
        $formEducation->get('edu_degree_level[]')->setAttribute('options', $educationList);
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'formEducation' => $formEducation,
            'jsLangTranslate' => $this->_config['user_messages']['config']['edit_user_demographic'],
            'params' => $data,
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to dispaly user social info
     * @author Icreon Tech-DG
     * @return Education Form
     */
    public function userSocialInfoAction() {
        $this->checkFrontUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }


        \Facebook\FacebookSession::setDefaultApplication($this->_config['facebook_app']['app_id'], $this->_config['facebook_app']['secret']);
        $helper = new \Facebook\FacebookRedirectLoginHelper(SITE_URL . "/login/profile/facebook", $this->_config['facebook_app']['app_id'], $this->_config['facebook_app']['secret']);

        $session = "";
        try {
            $session = $helper->getSessionFromRedirect();
        } catch (FacebookRequestException $ex) {
            $session = "";
        } catch (Exception $ex) {
            $session = "";
        }


        // Requested permissions - optional
        $permissions = array(
            'email',
            'user_location',
            'user_birthday'
        );
        $fbLoginUrl = $helper->getLoginUrl($permissions);

        $fb_token = new SessionContainer('fb_token');
        if (isset($fb_token->value) and trim($fb_token->value) != "") {
            //Create new session from saved access_token
            $session = new \Facebook\FacebookSession($fb_token->value);
        }

        $userInfo = array();
        $userFacebookProfile = array();
        $facebookUserId = "";
        $fbLogoutUrl = "";

        if (isset($session)) {
            $userInfo = (new \Facebook\FacebookRequest($session, 'GET', '/me'))->execute()->getGraphObject(GraphUser::className())->asArray();
            $fbLogoutUrl = $helper->getLogoutUrl($session, SITE_URL . '/logout');
        }


        if (isset($userInfo) and !empty($userInfo)) {
            $userFacebookProfile = $userInfo;
            $facebookUserId = (isset($userInfo['id']) and trim($userInfo['id']) != "") ? trim($userInfo['id']) : "";
        }

//         
//        $facebook = new \Facebook(array(
//                    'appId' => $this->_config['facebook_app']['app_id'],
//                    'secret' => $this->_config['facebook_app']['secret']
//                ));
//        $facebookUserId = $facebook->getUser();
//        if ($facebookUserId)
//            $userFacebookProfile = $facebook->api('/' . $facebookUserId);
//
        $emailId = '';
//
//        $fbLoginUrl = $facebook->getLoginUrl(
//                array(
//                    'scope' => 'email,offline_access,publish_stream,user_birthday,user_location,user_work_history',
//                    'req_perms' => 'friends_birthday,friends_location',
//                    'redirect_uri' => SITE_URL . "/login/profile/facebook",
//                    'display' => 'popup'
//                )
//        );


        if (isset($userFacebookProfile) && !empty($userFacebookProfile)) {
            $emailId = $userFacebookProfile['email'];
        }

        $this->auth = new AuthenticationService();
        if ($this->auth->hasIdentity() === true) {
            $userArr = $this->auth->getIdentity();
            $user_id = $userArr->user_id;
        }

        if ($request->isXmlHttpRequest() && $request->isPost()) {
//            $facebook = new \Facebook(array(
//                        'appId' => $this->_config['facebook_app']['app_id'],
//                        'secret' => $this->_config['facebook_app']['secret']
//                    ));
            //$facebook->destroySession();
            $this->getServiceLocator()->get('User\Model\UserTable')->updateSocialInfo($user_id);
            /* $this->getServiceLocator()->get('User\Model\UserTable')->updateUserLogins(array('user_id' => $user_id, 'active' => '0'));
              $facebook->destroySession();
              $auth = new AuthenticationService();
              $this->auth = $auth;
              $redirect = 'home';
              // asd($this->auth->hasIdentity());
              if ($this->auth->hasIdentity() === true) {
              $userArr = $this->auth->getIdentity();
              $user_id = $userArr->user_id;
              $this->getServiceLocator()->get('User\Model\UserTable')->updateUserLogins(array('user_id' => $user_id, 'active' => '0'));
              $this->getSessionStorage()->forgetMe();
              $this->auth->clearIdentity();
              $session = new SessionContainer();
              $session->getManager()->destroy();
              $session->getManager()->getStorage()->clear(); */
            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
        $arrSocialInfo = $this->getServiceLocator()->get('User\Model\UserTable')->getSocialInfo($user_id);
        $facebookUserId = $arrSocialInfo['social_account_id'];

        $viewModel->setVariables(array(
            'facebookUserId' => $facebookUserId,
            'emailId' => $emailId,
            'fbLoginUrl' => $fbLoginUrl,
            'fbLogoutUrl' => $fbLogoutUrl,
            'jsLangTranslate' => $this->_config['user_messages']['config']['users-social']
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to edit user preferences info
     * @author Icreon Tech-DG
     */
    public function editUserPreferencesInfoAction() {
        $this->checkFrontUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $flashMessages = '';
        if ($this->_flashMessage->hasMessages()) {
            $flashMessages = $this->_flashMessage->getMessages();
        }
        $params = $this->params()->fromRoute();

        $userPreferencesInfo = $this->getServiceLocator()->get('User\Model\ContactTable')->getCommunicationPreferences(array('user_id' => $this->decrypt($params['user_id'])));
        $updatedPrivacy = '';
        if ($request->isPost()) {
            $formData = $request->getPost();
            $dataArr = array();
            $privacyArr = explode(",", $userPreferencesInfo['privacy']);
            if ($formData['privacy'] == 2) {
                if (in_array(2, $privacyArr)) {
                    $updatedPrivacy = $userPreferencesInfo['privacy'];
                } else {
                    $updatedPrivacy = $userPreferencesInfo['privacy'] . "," . $formData['privacy'];
                }
            } else {
                if (in_array(2, $privacyArr)) {
                    $key = array_search(2, $privacyArr);
                    unset($privacyArr[$key]);
                    $updatedPrivacy = implode(",", $privacyArr);
                }
            }
            $dataArr['user_id'] = $this->decrypt($params['user_id']);
            $dataArr['privacy'] = ltrim($updatedPrivacy, ",");
            $dataArr['annotation_privacy'] = $formData['annotation_privacy'];
            $dataArr['family_history_privacy'] = $formData['family_history_privacy'];
            $this->getServiceLocator()->get('User\Model\UserTable')->updateCommunicationPreferences($dataArr);
            $this->flashMessenger()->addMessage($this->_config['user_messages']['config']['users-preferences-info']['DATA_UPDATED_SUCCCESS']);
            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
        $viewModel->setVariables(array(
            'flashMessages' => $flashMessages,
            'userPreferencesInfo' => $userPreferencesInfo,
            'userId' => $params['user_id'],
            'jsLangTranslate' => $this->_config['user_messages']['config']['users-preferences-info']
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to edit user credit card info
     * @author Icreon Tech-DG
     */
    public function editUserCreditCardInfoAction() {
        $this->checkFrontUserAuthentication();
        $this->getConfig();
        $this->getTables();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $flashMessages = '';
        if ($this->_flashMessage->hasMessages()) {
            $flashMessages = $this->_flashMessage->getMessages();
        }
        $params = $this->params()->fromRoute();
        //$userAddressInfo = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations(array('user_id' => $this->decrypt($params['user_id'])));
        //$userTokenInfo = $this->getServiceLocator()->get('User\Model\UserTable')->getUserTokenInformations(array('user_id' => $this->decrypt($params['user_id'])));

        $auth = new AuthenticationService();
        $userAddressInfo = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations(array('user_id' => $auth->getIdentity()->user_id));
        $userTokenInfo = $this->getServiceLocator()->get('User\Model\UserTable')->getUserTokenInformations(array('user_id' => $auth->getIdentity()->user_id));
    

        if ($request->isPost()) {
            $formData = $request->getPost();
            $dataArr = array();
            $privacyArr = explode(",", $userPreferencesInfo['privacy']);
            if ($formData['privacy'] == 2) {
                if (in_array(2, $privacyArr)) {
                    $updatedPrivacy = $userPreferencesInfo['privacy'];
                } else {
                    $updatedPrivacy = $userPreferencesInfo['privacy'] . "," . $formData['privacy'];
                }
            } else {
                if (in_array(2, $privacyArr)) {
                    $key = array_search(2, $privacyArr);
                    unset($privacyArr[$key]);
                    $updatedPrivacy = implode(",", $privacyArr);
                }
            }
            //$dataArr['user_id'] = $this->decrypt($params['user_id']);
            $dataArr['user_id'] = $auth->getIdentity()->user_id;
            $dataArr['privacy'] = ltrim($updatedPrivacy, ",");
            $dataArr['annotation_privacy'] = $formData['annotation_privacy'];
            $dataArr['family_history_privacy'] = $formData['family_history_privacy'];

            $this->flashMessenger()->addMessage($this->_config['user_messages']['config']['users-preferences-info']['DATA_UPDATED_SUCCCESS']);
            $this->getServiceLocator()->get('User\Model\UserTable')->updateCommunicationPreferences($dataArr);
        }
        if (!empty($userTokenInfo)) {
            foreach ($userTokenInfo as $key => $val) {
                /*
                $createCustomerProfile = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
                $createCustomerProfile->getCustomerProfileRequest(array('customerProfileId' => $val['profile_id']));
                 //asd($createCustomerProfile);
                $cardNumbrer = $createCustomerProfile->profile->paymentProfiles->payment->creditCard->cardNumber;
                $cardExpiryDate = $createCustomerProfile->profile->paymentProfiles->payment->creditCard->expirationDate;
                $userTokenInfo[$key]['card_number'] = (string) $cardNumbrer;
                $userTokenInfo[$key]['expiry_date'] = (string) $cardExpiryDate;
                
                // add - n -start
                $cardFirstName = $createCustomerProfile->profile->paymentProfiles->billTo->firstName;
                $cardLastName  = $createCustomerProfile->profile->paymentProfiles->billTo->lastName;*/
                
                //$cardFullName = "";
                //if(isset($cardFirstName) and trim($cardFirstName) != "") { $cardFullName .= trim($cardFirstName); }
                //if(isset($cardLastName) and trim($cardLastName) != "") { $cardFullName .= " ".trim($cardLastName); }
                //$cardFullName = (isset($cardFullName) and trim($cardFullName) != "") ? trim($cardFullName) : "N/A";
                
                $userTokenInfo[$key]['card_number'] = (string) $val['credit_card_no'];
                $userTokenInfo[$key]['expiry_date'] = 'XXXX';
                //$userTokenInfo[$key]['full_name'] = (string) $cardFullName;
                // add - n - end
            }
        }
        $viewModel->setVariables(array(
            'flashMessages' => $flashMessages,
            'userAddressInfo' => $userAddressInfo,
            'userTokenInfo' => $userTokenInfo,
            'userId' => $auth->getIdentity()->user_id,
            'jsLangTranslate' => $this->_config['user_messages']['config']['users-credit-card-info']
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to add token info
     * @author Icreon Tech-DG
     * @return Add Edit Address (JSON)
     */
    public function addTokenInfoAction() {
        $this->checkFrontUserAuthentication();
        $auth = new AuthenticationService();
        $this->getConfig();
        $this->getTables();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $user = new User($this->_adapter);
        $params = $this->params()->fromRoute();
        $form = new AddTokenForm();

        // - n - start
        $userAddressInfo = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations(array('user_id' => $auth->getIdentity()->user_id));
        // - n - end

        $creditCardResult = $this->getServiceLocator()->get('User\Model\UserTable')->getCreditCards();
        foreach ($creditCardResult as $key => $val) {
            $creditCardResultArray[$val['credit_card_type_id']] = strtolower($val['credit_card']);
        }

        $expirationMonth = array(' ' => 'Select Month', '01' => 'January', '02' => 'February', '03' => 'March', '04' => 'April', '05' => 'May', '06' => 'June', '07' => 'July', '08' => 'August', '09' => 'September', '10' => 'October', '11' => 'November', '12' => 'December');
        $form->get('expiration_month')->setAttribute('options', $expirationMonth);

        $expirationYear = array();
        $curentYear = date("Y");
        $next10Year = date("Y") + 10;
        $expirationYear[' '] = 'Select Year';
        for ($curentYear; $curentYear <= $next10Year; $curentYear++) {
            $expirationYear[$curentYear] = $curentYear;
        }
        $form->get('expiration_year')->setAttribute('options', $expirationYear);
		if($auth->getIdentity()->user_type == '1'){
			$name = "";
			if($auth->getIdentity()->first_name!='')
				$name.= $auth->getIdentity()->first_name;
			if($auth->getIdentity()->middle_name!='')
				$name.= " ".$auth->getIdentity()->middle_name;
			if($auth->getIdentity()->last_name!='')
				$name.= " ".$auth->getIdentity()->last_name;
		}else{
			$name = $auth->getIdentity()->company_name;
		}
		
		$form->get('first_name')->setValue($name);
        if ($request->isPost()) {
            $form->setData($request->getPost());
            $form->setInputFilter($user->addTokenInputFilter());
            if (!$form->isValid()) {
                $errors = $form->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submitbutton') {
                        foreach ($row as $rower) {
                            if (isset($this->_config['user_messages']['config']['users-credit-card-info'][$rower]))
                                $msg [$key] = $this->_config['user_messages']['config']['users-credit-card-info'][$rower];
                        }
                    }
                }
                if (isset($msg) and count($msg) > 0) {
                    $messages = array('status' => "error", 'message' => $msg);
                }
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $formData = $form->getData();
//echo strtolower($formData['credit_card_type']);
                $creditCardType = array_search(strtolower($formData['credit_card_type']), $creditCardResultArray);
                //die;
                $nameString = $formData['first_name'];
                $nameArray = @explode(" ", $nameString, 2);
                $formData['first_name'] = isset($nameArray[0]) ? $nameArray[0] : "";
                $formData['last_name'] = isset($nameArray[1]) ? $nameArray[1] : "";


                if (!isset($formData['billing_address_id']) || $formData['billing_address_id'] == '') {
                    $messages = array('status' => "server_error", 'keyname' => "billingaddress", 'message' => $this->_config['user_messages']['config']['users-credit-card-info']['EMPTY_BILLING_ADDRESS']);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                }
                $addressInfoId = $formData['billing_address_id'];
                $userAddressInfo = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations(array('address_information_id' => $addressInfoId));

                $createCustomerProfile = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
                $createCustomerProfile->createCustomerProfileRequest(array(
                    'profile' => array(
                        'merchantCustomerId' => $auth->getIdentity()->user_id . time(),
                        'email' => $auth->getIdentity()->email_id,
                        'paymentProfiles' => array(
                            'billTo' => array(
                                'firstName' => $formData['first_name'],
                                'lastName' => $formData['last_name'],
                                'address' => isset($userAddressInfo[0]['street_address_one']) ? $userAddressInfo[0]['street_address_one'] : '',
                                'city' => isset($userAddressInfo[0]['city']) ? $userAddressInfo[0]['city'] : '',
                                'state' => isset($userAddressInfo[0]['state']) ? $userAddressInfo[0]['state'] : '',
                                'zip' => isset($userAddressInfo[0]['zip_code']) ? $userAddressInfo[0]['zip_code'] : '',
                                'country' => isset($userAddressInfo[0]['country_name']) ? $userAddressInfo[0]['country_name'] : ''
                            ),
                            'payment' => array(
                                'creditCard' => array(
                                    'cardNumber' => $formData['card_number'],
                                    'expirationDate' => $formData['expiration_year'] . "-" . $formData['expiration_month'],
                                    'cardCode' => $formData['card_ccv'],
                                ),
                            )
                        ),
                    )
                ));

                if ($createCustomerProfile->isSuccessful()) {
                    $customerProfileId = $createCustomerProfile->customerProfileId;
                    $dataArr['user_id'] = $auth->getIdentity()->user_id;
                    $dataArr['profile_id'] = $customerProfileId;
                    $dataArr['credit_card_type_id'] = $creditCardType;
                    $dataArr['credit_card_no'] = 'XXXX'.substr($formData['card_number'],-4);
                    $dataArr['added_by'] = $auth->getIdentity()->user_id;
                    $dataArr['billing_address_id'] = $formData['billing_address_id'];
                    $dataArr['modified_by'] = $auth->getIdentity()->user_id;
                    $this->getServiceLocator()->get('User\Model\UserTable')->insertTokenInfo($dataArr);
                    $this->flashMessenger()->addMessage($this->_config['user_messages']['config']['users-credit-card-info']['TOKEN_ADDED_SUCCCESS']);
                    $messages = array('status' => "success");
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                } else {
                    if ($createCustomerProfile->messages->message->code == 'E00003') {
                        $errorMessage = array('0' => $this->_config['user_messages']['config']['users-credit-card-info']['INVALID_CVV']);
                    } else {
                        $errorMessage = $createCustomerProfile->messages->message->text;
                    }
                    $messages = array('status' => "server_error", 'message' => $errorMessage);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                }
            }
        }
        $viewModel->setVariables(array(
            'form' => $form,
            'jsLangTranslate' => $this->_config['user_messages']['config']['users-credit-card-info'],
            'userAddressInfo' => $userAddressInfo,
            'creditCardResultArray' => \Zend\Json\Json::encode($creditCardResultArray)
                )
        );
        return $viewModel;
    }
    /**
     * add new token using ssl
     * @return \Zend\View\Model\ViewModel
     */
    public function addNewTokenInfoAction() {
        $this->checkFrontUserAuthentication();
        $this->getConfig();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();

        $auth = new AuthenticationService();
        $this->auth = $auth;
        $userDetail = (array) $this->auth->getIdentity();

        $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $userDetail['user_id']));

        $params = $this->params()->fromRoute();
        $mode = '';
        if (isset($params['mode'])) {
            $mode = $this->decrypt($params['mode']);
        }

        $flashMessages = '';
        if ($this->_flashMessage->hasMessages()) {
            $flashMessages = $this->_flashMessage->getMessages();
        }
        
        $viewModel->setVariables(array(
            'flashMessages' => $flashMessages,
            'userId' => $this->encrypt($userDetail['user_id']),
            'user_id' => $userDetail['user_id'],
            'userDetail' => $userDetail,
            'mode' => $mode,
            'jsLangTranslateUser' => $this->_config['user_messages']['config']['edit_user_info'],
            'jsLangTranslateAddressDelete' => $this->_config['user_messages']['config']['edit_user_info'],
            'jsLangTranslateCreditCard' => $this->_config['user_messages']['config']['users-credit-card-info'],
			'allowed_IP' => $this->_config['allowed_ip']['ip']
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to show User Address Info
     * @author Icreon Tech-DG
     * @return Delete Address (JSON)
     */
    public function showUserAddressInfoAction() {
        $this->checkFrontUserAuthentication();
        $auth = new AuthenticationService();
        $this->getConfig();
        $this->getTables();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();

        $viewModel->setTerminal(true);


        // - n - start
        $userAddressInfo = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations(array('user_id' => $auth->getIdentity()->user_id));
        // - n - end

        $viewModel->setVariables(array(
            'userAddressInfo' => $userAddressInfo,
            'jsLangTranslate' => $this->_config['user_messages']['config']['users-credit-card-info']
                )
        );
        return $viewModel;
    }

    /**
     * This Action is used to delete token info
     * @author Icreon Tech-DG
     * @return Delete Address (JSON)
     */
    public function deleteUserTokenInfoAction() {
        $this->getTables();
        $this->getConfig();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam['paymentProfileId'] = $this->decrypt($request->getPost('paymentProfileId'));
            $dataParam['profileId'] = $this->decrypt($request->getPost('profileId'));
            $dataParam['isDelete'] = 1;
            //$createCustomerProfile = new \AuthnetXML($this->_config['authorize_net_payment_gateway']['API_LOGIN_ID'], $this->_config['authorize_net_payment_gateway']['TRANSACTION_KEY'], $this->_config['authorize_net_payment_gateway']['ENV']);
            //$createCustomerProfile->deleteCustomerProfileRequest(array('customerProfileId' => $dataParam['profileId']));
            $this->getTables()->deleteUserTokenInfo($dataParam);
            $this->flashMessenger()->addMessage($this->_config['user_messages']['config']['users-credit-card-info']['DATA_DELETED_SUCCCESS']);
            $messages = array('status' => "success", 'message' => $this->_config['user_messages']['config']['users-credit-card-info']['DATA_DELETED_SUCCCESS']);
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This Action is used for contact us
     * @author Icreon Tech-SK
     */
    public function contactusAction() {
        $auth = new AuthenticationService();
        if ($auth->hasIdentity() === true) {
            $user = $auth->getIdentity();
            if (isset($user->email_id)) {
                $this->checkFrontUserSessionPopup();
            }
        }
        $this->getConfig();
        $this->getTables();
        $response = $this->getResponse();
        $request = $this->getRequest();


        $viewModel = new ViewModel();
        $this->layout('web-popup');

        $form = new ContactusForm($this->_config['file_upload_path']);

        /* Case type */
        $caseTypeArr = array();
        $caseTypeArr[] = '';
        $getCaseType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getCaseTypes($caseTypeArr);
        $caseType = array();
        $caseType[''] = 'Select';
        if ($getCaseType !== false) {
            foreach ($getCaseType as $key => $val) {
                $caseType[$val['case_type_id']] = $val['case_type'];
            }
        }
        $form->get('case_type')->setAttribute('options', $caseType);
        /* End  Case type */
        $user = new User($this->_adapter);
        if ($request->isPost()) {
            $form->setData($request->getPost());
            $form->setInputFilter($user->getInputFilterContactUs());
            if (!$form->isValid()) {
                $errors = $form->getMessages();
                $msg = array();

                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'submit') {
                        foreach ($row as $rower) {
                            if ($key == 'captcha') {
                                $msg [$key] = $this->_config['user_messages']['config']['user_signup']['CAPTCHA_EMPTY'];
                            } else {
                                $msg [$key] = $this->_config['user_messages']['config']['user_signup'][$rower];
                            }
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }

            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $data = $request->getPost()->toArray();
                $getContact = $this->getServiceLocator()->get('User\Model\ContactTable')->getContact(array('user_email' => $data['email_id']));
				
                if (empty($getContact)) {
                    $contact = new Contact($this->_adapter);
                    $data['contact_type'] = 1;
                    $added_date = $data['add_date'] = DATE_TIME_FORMAT;
                    $modified_date = $data['modified_date'] = DATE_TIME_FORMAT;

                    $filterParam = $contact->filterParam($data);

                    $filterParam['username'] = null;
                    $filterParam['source'] = 3;
                    $filterParam['webaccesss_enabled_login'] = 1;

                    $user_id = $this->getServiceLocator()->get('User\Model\ContactTable')->insertContact($filterParam);

                    if ($user_id) {
                        $this->getServiceLocator()->get('User\Model\ContactTable')->updateContactId(array('user_id' => $user_id));

                        $this->getServiceLocator()->get('User\Model\UserTable')->insertDefaultContactCommunicationPreferences(array('user_id' => $user_id));

                        /* user membership */
                        $membershipDetailArr = array();
                        $membershipDetailArr[] = 1;
                        $getMemberShip = $this->getServiceLocator()->get('Setting\Model\SettingTable')->getMembershipById($membershipDetailArr);

                        $userMembershipData = array();
                        $userMembershipData['user_id'] = $user_id;
                        $userMembershipData['membership_id'] = $getMemberShip['membership_id'];
						$userMembershipInfo = $this->getServiceLocator()->get('UserMembership')->getUserMembership($getMemberShip);
					$userMembershipData['membership_date_from'] = $userMembershipInfo['membership_date_from'];
                    $userMembershipData['membership_date_to'] = $userMembershipInfo['membership_date_to'];
                        /*$startDate = date("Y-m-d");
                        if ($getMemberShip['validity_type'] == 1) {
                            $userMembershipData['membership_date_from'] = $startDate;
                            $startDay = $this->_config['financial_year']['srart_day'];
                            $startMonth = $this->_config['financial_year']['srart_month'];
                            $startYear = date("Y");
                            $startDate = $startYear . "-" . $startMonth . "-" . $startDay;
                            $addYear = ($getMemberShip['validity_time'] - date("Y")) + 1;
                            $futureDate = date('Y-m-d', strtotime('+' . $addYear . ' year', strtotime($startDate)));
                            $userMembershipData['membership_date_to'] = $futureDate;
                        } else if ($getMemberShip['validity_type'] == 2) {
                            $userMembershipData['membership_date_from'] = $startDate;
                            $futureDate = date('Y-m-d', strtotime('+' . $getMemberShip['validity_time'] . ' year', strtotime($startDate)));
                            $userMembershipData['membership_date_to'] = $futureDate;
                        }*/
                        $this->getServiceLocator()->get('User\Model\UserTable')->saveUserMembership($userMembershipData);

                        /* end of membership */

						/**
						  * insert empty address record
						**/
						$addVal = array();
						$addVal['user_id'] = $user_id;
						$addVal['add_date'] = DATE_TIME_FORMAT;
						$addVal['modified_date'] = DATE_TIME_FORMAT;
						$this->getServiceLocator()->get('User\Model\ContactTable')->insertAddressInfo($addVal);
						/* end of address entry **/

                        /* email */
                        $userDataArr = $this->getServiceLocator()->get('User\Model\UserTable')->getUser(array('user_id' => $user_id));
                        $userDataArr['admin_email'] = $this->_config['soleif_email']['email'];
                        $userDataArr['admin_name'] = $this->_config['soleif_email']['name'];
                        $userDataArr['genrated_password'] = $filterParam['genrated_password'];
                        $userDataArr['verification_code'] = $filterParam['verify_code'];
                        $userDataArr['first_name'] = $data['first_name'];
                        $signup_email_id_template_int = 4;
                        $this->getServiceLocator()->get('Common\Model\EmailTable')->sendEmail($userDataArr, $signup_email_id_template_int, $this->_config['email_options']);
                        /* end of email */

                        $data['user_id'] = $user_id;
                        $data['phone_no'] = $data['phone'];

                        $this->getServiceLocator()->get('User\Model\ContactTable')->insertPhoneInformations($data);

                        $createCaseFormArr['subject'] = 'Unable to signup';
                        $createCaseFormArr['contact_name_id'] = $user_id;
                        $createCaseFormArr['added_by'] = $user_id;
                        $createCaseFormArr['added_date'] = DATE_TIME_FORMAT;
                        #$createCaseFormArr['modify_by'] = $user_id;
                        #$createCaseFormArr['modify_date'] = DATE_TIME_FORMAT;
                        $createCaseFormArr['start_date'] = '';
                        $createCaseFormArr['start_time'] = '';
                        $createCaseFormArr['case_status'] = 1;
                        $createCaseFormArr['case_type'] = $data['case_type'];
                        $createCaseFormArr['message'] = $data['description'];

                        $cases = new Cases($this->_adapter);
                        $createCaseArr = $cases->getCreateCaseArr($createCaseFormArr);
                        $this->getServiceLocator()->get('Cases\Model\CasesTable')->saveCase($createCaseArr);
                        $messages = array('status' => "success-msg");
                        $response->setContent(\Zend\Json\Json::encode($messages));
                        return $response;
                    }
                } else {
                    $createCaseFormArr['subject'] = 'Unable to signup';
					$createCaseFormArr['contact_name_id'] = $getContact[0]['user_id'];
					$createCaseFormArr['added_by'] = $getContact[0]['user_id'];
					$createCaseFormArr['added_date'] = DATE_TIME_FORMAT;
					#$createCaseFormArr['modify_by'] = $getContact[0]['user_id'];
					#$createCaseFormArr['modify_date'] = DATE_TIME_FORMAT;
					$createCaseFormArr['start_date'] = '';
					$createCaseFormArr['start_time'] = '';
					$createCaseFormArr['case_status'] = 1;
					$createCaseFormArr['case_type'] = $data['case_type'];
					$createCaseFormArr['message'] = $data['description'];

					$cases = new Cases($this->_adapter);
					$createCaseArr = $cases->getCreateCaseArr($createCaseFormArr);
					$this->getServiceLocator()->get('Cases\Model\CasesTable')->saveCase($createCaseArr);
					$messages = array('status' => "success-msg");
					$response->setContent(\Zend\Json\Json::encode($messages));
					return $response;
                }
            }
        }

        $viewModel->setVariables(array(
            'form' => $form,
            'response' => $response,
            'jsLangTranslate' => $this->_config['user_messages']['config']['user_signup'])
        );

        return $viewModel;
    }

    /**
     * This function is used to get detail about user message center
     * @return     array
     * @param array $form
     * @author Icreon Tech - DG
     */
    public function userMessageCenterAction() {
        $this->checkFrontUserAuthentication();
        $auth = new AuthenticationService();
        $this->getConfig();
        $this->getTables();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
        }
        $this->auth = $auth;
        if (isset($auth->getIdentity()->user_id) and trim($auth->getIdentity()->user_id) != "") {
            $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $auth->getIdentity()->user_id));
            $userAddressInfo = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserAddressInformations(array('user_id' => $userDetail['user_id']));
            $userPhoneInfo = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserPhoneInformations(array('user_id' => $userDetail['user_id']));
            $userTokenInfo = $this->getServiceLocator()->get('User\Model\UserTable')->getUserTokenInformations(array('user_id' => $userDetail['user_id']));

            $getUserJobHistory = $this->getServiceLocator()->get('User\Model\DevelopmentTable')->getUserJobHistory(array('user_id' => $userDetail['user_id']));
            $userEducation = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserEducations(array('user_id' => $userDetail['user_id']));
            $userSurvey = $this->getServiceLocator()->get('User\Model\SurveyTable')->getUserSurvey(array('user_id' => $userDetail['user_id']));

            $profileMissing = false;
            $wohMissing = false;
            $membershipExpire = false;
            $profilepath = '';

            if (empty($userSurvey)) {
                $profileMissing = true;
                $profilepath = 'survey';
            }
            if ($userDetail['gender'] == '' || $userDetail['income_range_id'] = '' || $userDetail['income_range_id'] <= 0 || $userDetail['nationality'] == '' || $userDetail['ethnicity'] == '' || $userDetail['marital_status'] == '' || empty($getUserJobHistory) || empty($userEducation)) {
                $profileMissing = true;
                $profilepath = 'demographic';
            }
            if (empty($userTokenInfo)) {
                $profileMissing = true;
                $profilepath = 'token';
            }
            if (empty($userAddressInfo) || empty($userPhoneInfo)) {
                $profileMissing = true;
                $profilepath = 'contact';
            }

            $userWoh = $this->getServiceLocator()->get('User\Model\WohTable')->getUserWoh(array('user_id' => $userDetail['user_id'], 'sort_field' => 'usr_wall_of_honor.modified_date', 'sort_order' => 'desc'));
            if (!empty($userWoh)) {
                foreach ($userWoh as $val) {
                    if (isset($val['panel_no']) and trim($val['panel_no']) != "" and ($val['is_finalize'] == 0 or (isset($val['is_finalize']) and trim($val['is_finalize']) == ""))) {
                        $wohMissing = true;
                    }
                }
            }

            $curdate = date("Y-m-d");
            $expireAlert = strtotime(date("Y-m-d", strtotime($userDetail['membership_expire'])) . " -1 month");
            if ($userDetail['membership_id'] != 1 && $expireAlert < strtotime($curdate) && strtotime($userDetail['membership_expire']) > strtotime($curdate)) {
                $membershipExpire = true;
            }

            $viewModel->setVariables(array(
                'profileMissing' => $profileMissing,
                'wohMissing' => $wohMissing,
                'profilepath' => $profilepath,
                'membershipExpire' => $membershipExpire,
                'jsLangTranslate' => $this->_config['user_messages']['config']['message_center']
                    )
            );
            return $viewModel;
        }
    }

    /**
     * This function is used to get digital certificate
     * @return     array
     * @param array $form
     * @author Icreon Tech - DG
     */
    public function digitalCertificateAction() {
        $this->getConfig();
		$auth = new AuthenticationService();
        $response = $this->getResponse();
        $request = $this->getRequest();
        $viewModel = new ViewModel();
        if ($request->isXmlHttpRequest()) {
            $viewModel->setTerminal(true);
            
            $user_id = $auth->getIdentity()->user_id;
            $visitationCount = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserVisitationCount(array('user_id' => $user_id));
            $searchParam['start_index'] = 0;
            $searchParam['record_limit'] = $visitationCount['total'];
            $searchParam['user_id'] = $user_id;
            $searchParam['sort_field'] = 'created_at';
            $searchParam['sort_order'] = 'desc';
            $visitationArray = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserVisitation($searchParam);            
        }

        $viewModel->setVariables(array(
                    'visitationArray'=>$visitationArray,
                    'jsLangTranslate' => $this->_config['user_messages']['config']['search_contact']
                )
        );
        return $viewModel;
    }

    function loginDetailsAction() {
        $auth = new AuthenticationService();
        $auth = $auth->getIdentity();
        $str = '';
        if (isset($auth->user_id) && $auth->user_id != '') {
            if (isset($auth->first_name) && $auth->first_name && $auth->user_type == 1) {
                $str.= "Welcome ";
                $str.= ucfirst($auth->first_name) . " " . ucfirst($auth->last_name);
                $str.= " <a href='/profile'>My Profile</a>";
            } else if (isset($auth->company_name) && $auth->company_name && $auth->user_type != 1) {
                $str.= "Welcome ";
                $str.= ucfirst($auth->company_name);
                $str.= " <a href='/profile'>My Profile</a>";
            }
            $str.= " <a href='/logout'>Logout</a>";
        }
        echo $str;
        die;
    }

}