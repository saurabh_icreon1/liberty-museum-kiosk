<?php

namespace User\Service;

use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;

class UserMembership implements ServiceLocatorAwareInterface {

    protected $serviceLocator;
    protected static $serviceLocatorStatic;

    /**
     * Retrieve service manager instance
     *
     * @return ServiceManager
     */
    public function getServiceLocator() {

        if ($this->serviceLocator === null && self::$serviceLocatorStatic !== null) {
            $this->setServiceLocator(self::$serviceLocatorStatic);
        }

        return $this->serviceLocator;
    }

    /**
     * Set service locator
     *
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {

        $this->serviceLocator = $serviceLocator;

        self::$serviceLocatorStatic = $serviceLocator;

        return $this;
    }
	 /**
     * This Service is used to get user membership period
     * @author Icreon Tech-DG
     * @return membership data
     */
    public function getUserMembership($getMemberShip = array()){
		 $sm = $this->getServiceLocator();
		 $config = $sm->get('Config');
	     $userMembershipData = array();
         $startDate = date("Y-m-d",strtotime(DATE_TIME_FORMAT));
					$membershipDateFrom = '';
					$membershipDateTo = '';

            if ($getMemberShip['validity_type'] == 1) {
                $membershipDateFrom = $startDate;
                $startDay = $config['financial_year']['srart_day'];
                $startMonth = $config['financial_year']['srart_month'];
                $startYear = date("Y",strtotime(DATE_TIME_FORMAT));
                $startDate = $startYear . "-" . $startMonth . "-" . $startDay;
				$addYear = ($getMemberShip['validity_time'] - $startYear) + 1;    
                $futureDate = date('Y-m-d', strtotime('+' . $addYear . ' year -1 day', strtotime($startDate)));
                $your_date = $futureDate;
                $now = time();
                $datediff = strtotime($your_date) - $now;
                if ((isset($config['membership_plus']) && !empty($config['membership_plus'])) && (floor($datediff / (60 * 60 * 24)) < $config['membership_plus'])) {
                     $addYear = ($getMemberShip['validity_time'] - $startYear) + 2;
                    $futureDate = date('Y-m-d', strtotime('+' . $addYear . ' year', strtotime($startDate)));
                    $futureDate = date('Y-m-d', strtotime('-1 day', strtotime($futureDate)));
                }
                $membershipDateTo = $futureDate;
            } else if ($getMemberShip['validity_type'] == 2) {
                $membershipDateFrom = $startDate;
                $futureDate = date('Y-m-d', strtotime('+' . $getMemberShip['validity_time'] . ' year -1 day', strtotime($startDate)));
                $membershipDateTo = $futureDate;
            }
			$userMembershipData['membership_date_from'] = $membershipDateFrom;
			$userMembershipData['membership_date_to'] = $membershipDateTo ;
			return $userMembershipData;
    }
    

}
