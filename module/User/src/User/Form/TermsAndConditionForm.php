<?php

/**
 * This form is used for registration.
 * @package    User
 * @author     Icreon Tech - NS
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This page is used for users form
 * @package    User
 * @author     Icreon Tech - NS
 */
class TermsAndConditionForm extends Form {

    public function __construct($name = null) {

        parent::__construct('update_terms_and_condition');
        $this->setAttribute('method', 'post');
         
        
        $this->add(array(
            'name' => 'user_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'user_id'
            )
        ));
        
       $this->add(array(
            'name' => 'terms_and_condition',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'terms_and_condition'
            )
        ));
       
       $this->add(array(
            'name' => 'is_fb',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'is_fb',
                'value' => '0'
            )
        ));
       
       $this->add(array(
            'name' => 'referpath',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'referpath'
            )
        ));
       
       $this->add(array(
            'name' => 'facebook_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'facebook_id'
            )
        ));
       
       $this->add(array(
            'name' => 'fb_email_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'fb_email_id'
            )
        ));
       
      $this->add(array(
            'name' => 'is_popup',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'is_popup',
                'value' => '0'
            )
        ));
      
            

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'I Agree',
                'class' => 'button green',
                'id' => 'submitbutton',
            ),
        ));
    }

}