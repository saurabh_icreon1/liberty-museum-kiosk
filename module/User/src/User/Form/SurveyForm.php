<?php

/**
 * This form is used for SurveyForm
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This class is used for SurveyForm
 * @package    User
 * @author     Icreon Tech - AP
 */
class SurveyForm extends Form {

    public function __construct($name = null) {
        /* we want to ignore the name passed */
        parent::__construct('survey');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'user_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'user_id'
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'id' => 'submitbutton',
                'class' => 'save-btn button'
            ),
        ));
        $this->add(array(
            'name' => 'survey_immigrate',
            'attributes' => array(
                'type' => 'text',
                'id' => 'survey_immigrate',
                'style' => 'width:120px',
                'maxlength' => '50'
            ),
        ));
    }

}