<?php

/**
 * This form is used for email verification
 * @category   Zend
 * @package    User_ActivationForm
 * @version    2.2
 * @author     Icreon Tech - DG
 */

namespace User\Form;

use Zend\Form\Form;

class ActivationForm extends Form {

    public function __construct($name = null) {

        // we want to ignore the name passed
        parent::__construct('activation');
        $this->setAttribute('method', 'post');

         $this->add(array(
            'name' => 'referal_path_url',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'referal_path_url',
            )
        ));
         
        $this->add(array(
            'name' => 'verify_code',
            'attributes' => array(
                'type' => 'text',
                'id' => 'verify_code',
            )
        ));

        $this->add(array('type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 600
                )
            )
        ));
        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Verify',
                'class' => 'button',
                'id' => 'submitbutton',
            ),
        ));
    }

}