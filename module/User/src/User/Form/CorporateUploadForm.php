<?php

/**
 * This file is used for file upload
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This Class is used for file upload
 * @package  User
 * @author   Icreon Tech - DG
 */
class CorporateUploadForm extends Form {

    public function __construct($name = null) {
        /* we want to ignore the name passed */
        parent::__construct('corporateupload');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->add(array(
            'name' => 'cor_publicdocs_upload0',
            'attributes' => array(
                'type' => 'file',
                'class' => 'fileUploadAddMoreCor',
                'id' => 'cor_publicdocs_upload0',
            ),
        ));
        $this->add(array(
            'name' => 'cor_publicdocs_upload_file0',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'cor_publicdocs_upload_file0',
            ),
        ));
        $this->add(array(
            'name' => 'cor_publicdocs_upload_count',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'cor_publicdocs_upload_count',
                'value' => '0',
            ),
        ));
//        $this->add(array(
//            'name' => 'upload[]',
//            'attributes' => array(
//                'type' => 'button',
//                'value' => 'upload',
//                'id' => 'upload',
//                'class' => 'cancel-btn m-l-20'
//            ),
//        ));
    }

}