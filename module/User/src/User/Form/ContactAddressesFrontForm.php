<?php

/**
 * This form is used for create address section in individual type in contact.
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This class is used for create address form elemts .
 * @author     Icreon Tech - AP
 */
class ContactAddressesFrontForm extends Form {

    public function __construct($name = null) {
        /* we want to ignore the name passed */
        parent::__construct('street_address');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'addressinfo_location_type[]',
            'options' => array(
                'value_options' => array(
                    '0' => 'Home',
                    '1' => 'Work',
                    '2' => 'Vacation',
                    '3' => 'Web'
                ),
            ),
            'attributes' => array(
                'id' => 'addressinfo_location_type',
                'class' => 'e1',
            )
        ));
        $this->add(array(
            'type' => 'MultiCheckbox',
            'name' => 'addressinfo_location[]',
            'options' => array(
                'value_options' => array(
                    '1' => 'Primary Location',
                    '2' => 'Billing Location',
                    '3' => 'Shipping Location'
                ),
            ),
            'attributes' => array(
                'value' => '', /* set checked to '1' */
                'class' => 'addressinfo_location',
                'id' => 'addressinfo_locationp'
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'addressinfo_location_primary[]',
            'options' => array(
                'value_options' => array(
                    '1' => 'Primary Location',
                ),
            ),
            'attributes' => array(
                'value' => '', /* set checked to '1' */
                'class' => 'address-checkbox',
                'id' => 'addressinfo_location_primary0',
                'onclick' => 'addressTypePrimary(this.id,0);'
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'addressinfo_location_billing[]',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'options' => array(
                'use_hidden_element' => false
            ),
            'attributes' => array(
                'value' => '', /* set checked to '1' */
                'class' => 'address-checkbox',
                'id' => 'addressinfo_location_billing0',
                'onchange' => 'addressTypeBilling(this.id,0);'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'addressinfo_location_shipping[]',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'value' => '', /* set checked to '1' */
                'class' => 'address-checkbox',
                'id' => 'addressinfo_location_shipping0',
                'onchange' => 'addressTypeShipping(this.id,0);'
            ),
            'options' => array(
                'use_hidden_element' => false
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_primary[]',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'value' => '', /* set checked to '1' */
                'class' => 'address-checkbox',
                'id' => 'is_primary0',
            ),
            'options' => array(
                'use_hidden_element' => false
            ),
        ));

        $this->add(array(
            'type' => 'hidden',
            'name' => 'primary_location0',
            'attributes' => array(
                'value' => '', /* set checked to '1' */
                'class' => 'e4 addressinfo_location',
                'id' => 'primary_location0'
            )
        ));

        $this->add(array(
            'type' => 'hidden',
            'name' => 'primary_location_count',
            'attributes' => array(
                'value' => '1',
                'id' => 'primary_location_count'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'addressinfo_another_contact_address[]',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                //'onClick' => 'loadIndividualContact(this.id);',
                'id' => 'addressinfo_another_contact_address',
                'class' => 'address-checkbox'
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'title',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'title',
                'class' => 'e1'
            )
        ));
        
        $this->add(array(
            'name' => 'first_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'first_name'
            )
        ));
        
        $this->add(array(
            'name' => 'last_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_name'
            )
        ));
        
        $this->add(array(
            'name' => 'middle_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'middle_name'
            )
        ));        
        $this->add(array(
            'name' => 'addressinfo_street_address_1[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'addressinfo_street_address_1'
            )
        ));
        $this->add(array(
            'name' => 'addressinfo_street_address_2[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'addressinfo_street_address_2'
            )
        ));
        $this->add(array(
            'name' => 'addressinfo_street_address_3[]',
            'attributes' => array(
                'type' => 'text',
            )
        ));
        $this->add(array(
            'name' => 'addressinfo_city[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'addressinfo_city'
            )
        ));
        $this->add(array(
            'type' => 'text',
            'name' => 'addressinfo_state[]',
            'attributes' => array(
                'id' => 'addressinfo_state_0',
                'value' => '' /* set selected to 'blank' */
            )
        ));
        $this->add(array(
            'name' => 'addressinfo_zip[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'addressinfo_zip'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'addressinfo_country[]',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'id' => '0',
                'onchange' => "selectUsaState(this.value,this.id,'individual')",
                'class' => 'e1',
                'value' => '' /* set selected to 'blank' */
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'address_state_id[]',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'id' => 'address_state_id_0',
                'onchange' => "assignStateValue(this.value,this.id,'individual')",
                'class' => 'e1',
                'value' => '' /* set selected to 'blank' */
            )
        ));
	$this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'address_state_po[]',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'id' => 'address_state_po_0',
                'onchange' => "assignStateValue(this.value,this.id,'individual')",
                'class' => 'e1',
                'value' => '' /* set selected to 'blank' */
            )
        ));
	$this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'address_state_apo[]',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'id' => 'address_state_apo_0',
                'onchange' => "assignStateValue(this.value,this.id,'individual')",
                'class' => 'e1',
                'value' => '' /* set selected to 'blank' */
            )
        ));
		
	$this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'address_country_id[]',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'id' => 'address_country_id_0',
                'class' => 'e1',
                'value' => '' /* set selected to 'blank' */
            )
        ));

        $this->add(array('type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 600
                )
            )
        ));
        $this->add(array(
            'name' => 'addressinfo_search_contact[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'addressinfo_search_contact',
                'class' => "search-icon"
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'is_apo_po_record',
            'options' => array(
                "value_options" => array(
                    '1' => 'Residential/Corporate',
                    '2' => 'PO',
                    '3' => 'APO'
                )
            ),
            'attributes' => array(
                'id' => 'is_apo_po_record',
                'class' => 'e3 is_apo_po_record',
                'onClick' => 'setApoPoAddress(this.value)',
                'value' => '1'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'apo_po_shipping_country',
            'options' => array(
                'value_options' => array(
                    '228' => 'UNITED STATES'
                )
            ),
            'attributes' => array(
                'id' => 'apo_po_shipping_country',
                'class' => "e1"
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'apo_po_state',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'apo_po_state',
                'class' => 'e1',
            )
        ));
		
	$this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'is_apo_po[]',
            'options' => array(
                "value_options" => array(
                    '1' => 'Residential/Corporate',
                    '2' => 'PO',
                    '3' => 'APO'
                )
            ),
            'attributes' => array(
                'id' => 'is_apo_po',
                'class' => 'address-checkbox is_apo_po',
                'value' => '1',
		'onChange' => 'setApoPostateCountry(0,this.value);',
            )
        ));
    }

}