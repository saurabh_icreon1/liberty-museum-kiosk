<?php

/**
 * This form is used for saving WOH
 * @category   Zend
 * @package    User_WohForm
 * @version    2.2
 * @author     Icreon Tech - NS
 */

namespace User\Form;

use Zend\Form\Form;

class WohForm extends Form {

    public function __construct($name = null) {

        parent::__construct('create-Woh');
        $this->setAttribute('id', 'add_woh');
        $this->setAttribute('name', 'add_woh');
        $this->setAttribute('method', 'post');
        
      
       $this->add(array(
            'name' => 'product_woh_frame_price_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'product_woh_frame_price_id'
            )
        ));
       
       $this->add(array(
            'name' => 'product_woh_frame_price_qty',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'product_woh_frame_price_qty'
            )
        ));
             
       $this->add(array(
            'name' => 'product_number_duplicate_copies_certificate',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'product_number_duplicate_copies_certificate'
            )
        ));
       
      $this->add(array(
            'name' => 'product_woh_frame_price_value',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'product_woh_frame_price_value'
            )
        ));
                  
                  
      $this->add(array(
            'name' => 'logged_in_flag',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'logged_in_flag'
            )
        ));
  
     $this->add(array(
            'type' => 'hidden',
            'name' => 'woh_main_product',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'woh_main_product'
             )
        ));
            
 
      
      //// start
      
            $this->add(array(
                  'name' => 'product_attribute_option_id',
                  'attributes' => array(
                      'type' => 'hidden',
                      'id' => 'product_attribute_option_id'
                  )
              ));

              $this->add(array(
                  'name' => 'first_name_one',
                  'attributes' => array(
                      'type' => 'text',
                      'id' => 'first_name_one',
                      'maxlength' => '45',
                      'class' => 'button'
                  )
              ));
              $this->add(array(
                  'name' => 'first_name_two',
                  'attributes' => array(
                      'type' => 'text',
                      'id' => 'first_name_two',
                      'maxlength' => '45',
                      'class' => 'button'
                  )
              ));
              $this->add(array(
                  'name' => 'other_init_one',
                  'attributes' => array(
                      'type' => 'text',
                      'id' => 'other_init_one',
                      'maxlength' => '2',
                      'class' => 'button'
                  )
              ));
              $this->add(array(
                  'name' => 'other_init_two',
                  'attributes' => array(
                      'type' => 'text',
                      'id' => 'other_init_two',
                      'maxlength' => '2',
                      'class' => 'button'
                  )
              ));
              $this->add(array(
                  'name' => 'other_name',
                  'attributes' => array(
                      'type' => 'text',
                      'id' => 'other_name',
                      'maxlength' => '45',
                      'class' => 'button'
                  )
              ));
              $this->add(array(
                  'name' => 'last_name_one',
                  'attributes' => array(
                      'type' => 'text',
                      'id' => 'last_name_one',
                      'maxlength' => '45',
                      'class' => 'button'
                  )
              ));
              $this->add(array(
                  'name' => 'last_name_two',
                  'attributes' => array(
                      'type' => 'text',
                      'id' => 'last_name_two',
                      'maxlength' => '45',
                      'class' => 'button'
                  )
              ));

              $this->add(array(
                  'name' => 'first_line',
                  'attributes' => array(
                      'type' => 'text',
                      'id' => 'first_line',
                      'maxlength' => '45',
                      'class' => 'button'
                  )
              ));
              $this->add(array(
                  'name' => 'second_line',
                  'attributes' => array(
                      'type' => 'text',
                      'id' => 'second_line',
                      'maxlength' => '45',
                      'class' => 'button'
                  )
              ));

              $this->add(array(
                  'type' => 'Zend\Form\Element\Checkbox',
                  'name' => 'is_usa',
                  'checked_value' => '1',
                  'unchecked_value' => '0',                  
                  'attributes' => array(
                      'id' => 'is_usa',
                      'class' => 'checkbox e2'
                  ),
                'options' => array(
                    'use_hidden_element' => false
                 ),
              ));
              $this->add(array(
                  'name' => 'other_country',
                  'attributes' => array(
                      'type' => 'text',
                      'id' => 'other_country',
                      'maxlength' => '30'
                  )
              ));

              $this->add(array(
                  'name' => 'donated_by',
                  'attributes' => array(
                      'type' => 'text',
                      'id' => 'donated_by',
                      'maxlength' => '30'
                  )
              ));

              $this->add(array(
                  'type' => 'Zend\Form\Element\Select',
                  'name' => 'donated_for',
                  'options' => array(
                      'value_options' => array(
                          ' ' => 'Select',
                          '1' => 'In Memory of',
                          '2' => 'In Honor of'
                      )
                  ),
                  'attributes' => array(
                      'id' => 'donated_for',
                     // 'class' => 'e1',
                      'onclick' => 'javascript:verifyDonatedFor(this.value);'
                  )
              ));
      
      //// end
      
    /// contact start
              
        $this->add(array(
            'name' => 'first_name[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'first_name_1',
                'maxlength' => '20',
                'class' => 'button'
            )
        ));
        $this->add(array(
            'name' => 'last_name[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_name_1',
                'maxlength' => '20',
                'class' => 'button'
            )
        ));
        $this->add(array(
            'name' => 'email_id[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email_id_1',
                'class' => 'button'
            )
        ));        
              
    /// contact end
        
              
         $this->add(array(
             'name' => 'num_of_duplicate_copies_of_certificate',
             'attributes' => array(
                 'type' => 'text',
                 'id' => 'num_of_duplicate_copies_of_certificate',
                 'value' => '0',
                 'class' => 'crt-copy button'
             )
         ));
         
       $this->add(array(
              'name' => 'viewframes',
              'attributes' => array(
                  'type' => 'button',
                  'value' => 'View Frames',
                  'id' => 'viewframes',
                  'class' => 'crt-green-btn button',
                  'onclick' => "viewFramesPopup();"
              ),
          ));
      
       $this->add(array(
             'name' => 'i_confirm',
             'type' => 'Checkbox',
             'attributes' => array(
                 'value' => '0',
                 'class' => 'checkbox',
				 'id' => 'i_confirm'
             ),
             'options' => array(
                 'value_options' => array(
                     '0' => 'Checkbox',
                     '1' => 'Checkbox',
                 ),
                 'use_hidden_element' => false
             ),
        ));
           
        $this->add(array(
             'name' => 'is_agree',
             'type' => 'Checkbox',
             'attributes' => array(
                 'value' => '0',
                 'class' => 'checkbox',
		 'id' => 'is_agree'
             ),
             'options' => array(
                 'value_options' => array(
                     '0' => 'Checkbox',
                     '1' => 'Checkbox',
                 ),
                 'use_hidden_element' => false
             ),
        ));            
       
       
        $this->add(array(
              'name' => 'submitorder',
              'attributes' => array(
                  'type' => 'submit',
                  'value' => 'Submit Order',
                  'id' => 'submitorder',
                  'class' => 'button'
              ),
          ));

        $this->add(array(
              'name' => 'submitorder1',
              'attributes' => array(
                  'type' => 'submit',
                  'value' => 'Next',
                  'id' => 'submitorder1',
                  'class' => 'crt-copy-btn button'
              ),
          ));
       
        $this->add(array(
              'name' => 'submitorder2',
              'attributes' => array(
                  'type' => 'submit',
                  'value' => 'Next',
                  'id' => 'submitorder2',
                  'class' => 'crt-copy-btn button'
              ),
          ));
       
        $this->add(array(
              'name' => 'submitorder3',
              'attributes' => array(
                  'type' => 'submit',
                  'value' => 'Next',
                  'id' => 'submitorder3',
                  'class' => 'crt-copy-btn button'
              ),
          ));
        
         $this->add(array(
              'name' => 'submitorder4',
              'attributes' => array(
                  'type' => 'submit',
                  'value' => 'Next',
                  'id' => 'submitorder4',
                  'class' => 'crt-copy-btn button'
              ),
          ));
       
    }

}