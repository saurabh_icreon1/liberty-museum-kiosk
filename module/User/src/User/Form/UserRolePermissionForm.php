<?php

/**
 * This form is used for SurveyForm
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This class is used for SurveyForm
 * @package    User
 * @author     Icreon Tech - AP
 */
class UserRolePermissionForm extends Form {

    public function __construct($name = null) {
        /* we want to ignore the name passed */
        parent::__construct('UserRolePermission');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'role_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'role_id'
            ),
        ));
        $this->add(array(
            'name' => 'module_permission_submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'id' => 'module_permission_submitbutton',
                'class' => 'save-btn'
            ),
        ));

		$this->add(array(
            'name' => 'module_permission_cancel',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Cancel',
                'id' => 'module_permission_cancelbutton',
                'class' => 'cancel-btn m-l-10',
                'onClick' => 'backToSearch();'
            ),
        ));
		
		
    }

}