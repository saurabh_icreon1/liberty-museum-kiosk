<?php

/**
 * This form is used for registration.
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Form;

use Zend\Form\Form;
use Zend\Form\Element\Captcha;
use Zend\Captcha\Image as CaptchaImage;

/**
 * This page is used for users form
 * @package    User
 * @author     Icreon Tech - DG
 */
class UserVisitRecordForm extends Form {

    protected $captchaAdapter;

    public function __construct($config = null) {
        // we want to ignore the name passed
        parent::__construct('uservisitrecord');
        $this->setAttribute('method', 'post');

      $this->add(array(
            'name' => 'arrival_date',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'arrival_date'
            )
        ));
      $this->add(array(
            'name' => 'user_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'user_id'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'visiting_with',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    '2' => 'Family',
                    '3' => 'Friend',
                    '4' => 'School',
                    '5' => 'Myself',
                    '6' => 'Other',
                ),
            ),
            'attributes' => array(
                'id' => 'visiting_with',
                'value' => '' //set selected to 'blank'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'birth_year',
            'options' => array(
                'value_options' => array(
                    '' => 'Select Year',

                ),
            ),
            'attributes' => array(
                'id' => 'birth_year',
                'value' => '' //set selected to 'blank'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'matrial_status',
            'options' => array(
                'value_options' => array(
                    '1' => 'Rather not disclose',
                    '2' => 'Single',
                    '3' => 'Married',
                    '4' => 'Divorced',
                    '5' => 'Widowed',
                ),
            ),
            'attributes' => array(
                'id' => 'matrial_status',
                'value' => '1', //set selected to 'blank'
                'class'=>'e3'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'gender',
            'options' => array(
                'value_options' => array(
                    '1' => 'Rather not disclose',
                    '2' => 'Male',
                    '3' => 'Female'
                   
                ),
            ),
            'attributes' => array(
                'id' => 'gender',
                'value' => '1', //set selected to 'blank'
                'class'=>'e3'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'ethnicity',
            'options' => array(
                'value_options' => array(
                    '1' => 'Rather not disclose',
                    '2' => 'Russian',
                    '3' => 'Polish'
                   
                ),
            ),
            'attributes' => array(
                'id' => 'ethnicity',
                'value' => '1' //set selected to 'blank'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'nationality',
            'options' => array(
                'value_options' => array(
                    '1' => 'Rather not disclose',
                    '2' => 'Russian',
                    '3' => 'Polish'
                   
                ),
            ),
            'attributes' => array(
                'id' => 'nationality',
                'value' => '1' //set selected to 'blank'
            )
        ));
    

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'birth_country',
            'options' => array(
                'value_options' => array(
                    '' => 'Select Country',
                ),
            ),
            'attributes' => array(
                'id' => 'birth_country',
                'value' => '' //set selected to 'blank'
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'immigration_year',
            'options' => array(
                'value_options' => array(
                    '' => 'Select Year',
                ),
            ),
            'attributes' => array(
                'id' => 'immigration_year',
                'value' => '' //set selected to 'blank'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'port_entry',
            'options' => array(
                'value_options' => array(
                    '' => 'Seelct Port',
                    '2' => 'Port1',
                    '3' => 'Port2'
                   
                ),
            ),
            'attributes' => array(
                'id' => 'port_entry',
                'value' => '' //set selected to 'blank'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'occupation',
            'options' => array(
                'value_options' => array(
                    '' => 'Select Year',
                ),
            ),
            'attributes' => array(
                'id' => 'occupation',
                'value' => '' //set selected to 'blank'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Textarea',
            'name' => 'message',
            'attributes' => array(
                'class' => 'e3 textarea',
                'id'=>'message'
            )
        ));
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'agree',
            'attributes' => array(
                'value' => '' //set checked to '1'
            )
        ));

        $this->add(array('type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 600
                )
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Continue',
                'class' => 'button green',
                'id' => 'submitbutton',
            ),
        ));
    }

}