<?php

/**
 * This form is used for email verification
 * @category   Zend
 * @package    User_ActivationForm
 * @version    2.2
 * @author     Icreon Tech - DG
 */

namespace User\Form;

use Zend\Form\Form;

class WohBioCertificateForm extends Form {

    public function __construct($name = null) {

        parent::__construct('bio-certificate-Woh');
        $this->setAttribute('id', 'bio_certificate_woh');
        $this->setAttribute('name', 'bio_certificate_woh');
        $this->setAttribute('method', 'post');
        
      
          $this->add(array(
			  'name' => 'bio_certificate_prod_id',
			  'attributes' => array(
				  'type' => 'hidden',
				  'id' => 'bio_certificate_prod_id'
			  )
		  ));

          
       
		  $this->add(array(
			  'name' => 'wohid',
			  'attributes' => array(
				  'type' => 'hidden',
				  'id' => 'wohid'
			  )
		  ));

		  $this->add(array(
				  'name' => 'productid',
				  'attributes' => array(
					  'type' => 'hidden',
					  'id' => 'productid'
				  )
			  ));

		  $this->add(array(
				  'name' => 'inforeq',
			  'attributes' => array(
				  'type' => 'hidden',
				  'id' => 'inforeq'
			  )
		  ));
	  
	 $this->add(array(
		  'name' => 'bio_certificate_qty',
		  'attributes' => array(
			  'type' => 'text',
			  'id' => 'bio_certificate_qty',
			  'class'=>'input-width-200',
              'maxlength' => 10,
              'readonly'=>'readonly'
		  )
	  ));
	  
	  
	  $this->add(array(
		  'name' => 'bio_name',
		  'attributes' => array(
			  'type' => 'text',
			  'id' => 'bio_name'
		  )
	  ));
     
	  $this->add(array(
		  'name' => 'immigrated_from',
		  'attributes' => array(
			  'type' => 'text',
			  'id' => 'immigrated_from'
		  )
	  ));

	  $this->add(array(
		  'name' => 'method_of_travel',
		  'attributes' => array(
			  'type' => 'text',
			  'id' => 'method_of_travel'
		  )
	  ));

	  $this->add(array(
		  'name' => 'port_of_entry',
		  'attributes' => array(
			  'type' => 'text',
			  'id' => 'port_of_entry'
		  )
	  ));
	 
	  $this->add(array(
		  'name' => 'date_of_entry_to_us',
		  'attributes' => array(
			  'type' => 'text',
			  'id' => 'date_of_entry_to_us',
			  'class' => 'width-120 cal-icon'
		  )
	  ));

	  $this->add(array(
		  'name' => 'additional_info',
		  'attributes' => array(
			  'type' => 'text',
			  'id' => 'additional_info'
		  )
	  ));	  
       
   $this->add(array(
		  'name' => 'bBioCertificateWohSubmit',
		  'attributes' => array(
			  'type' => 'submit',
			  'value' => 'Add To Cart',
			  'id' => 'bBioCertificateWohSubmit',
			  'class' => 'button cart-btn'
		  ),
	  ));
 

    }

}