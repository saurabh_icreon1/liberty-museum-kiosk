<?php

/**
 * This form is used for create user relationship.
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Form;

use Zend\Form\Form;

class CreateUserRelationshipForm extends Form {

    public function __construct($name = null) {
        parent::__construct('createuserrelationship');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'notes',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'notes',
            )
        ));
        $this->add(array(
            'name' => 'user_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'user_id',
            )
        ));
        $this->add(array(
            'name' => 'relative_user_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'relative_user_id',
            )
        ));
        $this->add(array(
            'name' => 'contact',
            'attributes' => array(
                'type' => 'text',
                'id' => 'contact',
				'class' => 'search-icon',
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'type',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'id' => 'type',
                'class' => 'e1 select-w-320',
                'value' => '' //set selected to 'blank'
            )
        ));
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'enabled',
            'attributes' => array(
                'class' => 'e2',
                'id' => 'check-1',
                'value' => '' //set checked to '1'
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'class' => 'save-btn',
                'id' => 'submitbutton',
            ),
        ));
    }

}