<?php

/**
 * This page is used for crm forgot users form
 * @package    User_LOGIN
 * @author     Icreon Tech - DT
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This form is used for crm forgot users form
 * @package    User_Login_ForgotFormCrm
 * @author     Icreon Tech - DT
 */
class ForgotFormCrm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('crm_forgot');
        $this->setAttribute('method', 'post');


        $this->add(array(
            'name' => 'email_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email_id',
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'SEND PASSWORD',
                'id' => 'submitbutton',
                'class' => 'save-btn'
            ),
        ));
    }

}