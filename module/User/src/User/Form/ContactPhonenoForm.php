<?php

/**
 * This form is used for create phone no in contact
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace User\Form;

use Zend\Form\Form;
use User\Form\PhonenoForm;

/**
 * This class is used for create phone no from
 * @package    User
 * @author     Icreon Tech - AP
 */
class ContactPhonenoForm extends Form {

    public function __construct($name = null) {
        /* we want to ignore the name passed */
        parent::__construct('phoneno');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'country_code[]',
            'attributes' => array(
                'type' => 'text',
                'class' => 'width-125 m-r-20'
            )
        ));
        $this->add(array(
            'name' => 'primary-phone',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'primary-phone',
                'value' => '0'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_primary',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'options' => array(
                'use_hidden_element' => false
            ),
            'attributes' => array(
                'value' => '0', /* set checked to '1' */
                'class' => 'e4 addressinfo_location_billing',
            )
        ));        

        $this->add(array(
            'name' => 'area_code[]',
            'attributes' => array(
                'type' => 'text',
                'class' => 'width-80 m-r-20'
            )
        ));
        $this->add(array(
            'name' => 'phone_no[]',
            'attributes' => array(
                'type' => 'text',
                'class' => 'phone_no m-r-20',
                'id' => 'phone_no[]'
            ),
        ));
        $this->add(array(
            'name' => 'extension[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'extension[]',
                'class' => 'width-80 m-r-20'
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'phone_type[]',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                ),
            ),
            'attributes' => array(
                'id' => 'phone_type',
                'class' => 'e1 left select-w-150 phone-type',
                'value' => '' /* set selected to 'blank' */
            )
        ));

        $this->add(array('type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 600
                )
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'continfo_primary[]',
            'options' => array(
                'value_options' => array(
                    '1' => 'Primary',
                ),
            ),
            'attributes' => array(
                'value' => '', /* set checked to '1' */
                'class' => 'phone-radio',
                'id' => 'continfo_primary0',
                'onclick' => 'checkPrimary(0);'
            )
        ));
    }

}