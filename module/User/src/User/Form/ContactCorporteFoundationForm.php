<?php

/**
 * This form is used for Corporate or Foundation.
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This class is used for create form elements for contact of type Corporate or Foundation.
 * @package    User
 * @author     Icreon Tech - AP
 */
class ContactCorporteFoundationForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('contact_corporatefoundation');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'corporate_contact_type',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'class' => 'e1 select-w-320',
                'id' => 'corporate_contact_type',
                'value' => '2',
                'onChange' => 'javascript:loadForm(this.id);',
            ),
        ));
        $this->add(array(
            'name' => 'corporate_upload_profile_image',
            'attributes' => array(
                'type' => 'file',
                'value' => 'upload',
                'id' => 'corporate_upload_profile_image',
            ),
        ));
        $this->add(array(
            'name' => 'corporate_upload_profile_image_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'corporate_upload_profile_image_id',
            ),
        ));
        $this->add(array(
            'name' => 'corporate_address_primary_index',
            'attributes' => array(
                'type' => 'hidden',
                'value' => '0',
                'id' => 'corporate_address_primary_index',
            ),
        ));
        $this->add(array(
            'name' => 'corporate_company_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'corporate_company_name',
            )
        ));
        
        $this->add(array(
            'name' => 'corporate_addressinfo_first_name[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'corporate_addressinfo_first_name'
            )
        )); 
        $this->add(array(
            'name' => 'acorporate_ddressinfo_last_name[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'corporate_addressinfo_last_name'
            )
        )); 
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'corporate_addressinfo_title[]',

            'attributes' => array(
                'class' => 'e1 select-w-320',
                'id' => 'corporate_addressinfo_title',
                'value' => '' //set selected to 'blank'
            )
        ));          
        
        $this->add(array(
            'name' => 'corporate_legal_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'corporate_legal_name'
            )
        ));
        $this->add(array(
            'name' => 'corporate_sic_code',
            'attributes' => array(
                'type' => 'text',
                'id' => 'corporate_sic_code',
            )
        ));

        $this->add(array(
            'name' => 'corporate_email_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'corporate_email_id',
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'corporate_no_email_address',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'class' => 'checkbox',
            'attributes' => array(
                'id' => 'corporate_no_email_address',
                'class' => 'e2'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'corporate_source',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'class' => 'e1 select-w-320',
                'id' => 'corporate_source',
                'value' => '' //set selected to 'blank'
            )
        ));
//        $this->add(array(
//            'type' => 'Zend\Form\Element\Select',
//            'name' => 'corporate_country_id',
//            'options' => array(
//                'value_options' => array(
//                    '' => 'Select'
//                ),
//            ),
//            'attributes' => array(
//                'class' => 'e1 select-w-320',
//                'id' => 'corporate_country_id',
//                'value' => '' //set selected to 'blank'
//            )
//        ));
//        $this->add(array(
//            'name' => 'corporate_zip_code',
//            'attributes' => array(
//                'type' => 'text',
//                'id' => 'corporate_zip_code'
//            )
//        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'corporate_addressinfo_location_type',
            'options' => array(
                'value_options' => array(
                    '0' => 'Home',
                    '1' => 'Work',
                    '2' => 'Vacation Home',
                    '3' => 'Others'
                ),
            ),
            'attributes' => array(
                'class' => 'e1 select-w-320',
                'id' => 'corporate_addressinfo_location_type',
                'value' => '' /* set selected to 'blank' */
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'corporate_communicationpref_email_greetings',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                ),
            ),
            'attributes' => array(
                'class' => 'e1 select-w-320',
                'id' => 'corporate_communicationpref_email_greetings',
                'onChange' => 'show_custome_box(this.id,"corporate_custome_email");'
            ),
        ));
        $this->add(array(
            'name' => 'corporate_communicationpref_custome_email_greetings',
            'attributes' => array(
                'type' => 'text',
                'id' => 'corporate_communicationpref_custome_email_greetings'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'corporate_communicationpref_postal_greetings',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                ),
            ),
            'attributes' => array(
                'class' => 'e1 select-w-320',
                'id' => 'corporate_communicationpref_postal_greetings',
                'onChange' => 'show_custome_box(this.id,"corporate_custome_postal");'
            ),
        ));
        $this->add(array(
            'name' => 'corporate_communicationpref_custome_postal_greetings',
            'attributes' => array(
                'type' => 'text',
                'id' => 'corporate_communicationpref_custome_postal_greetings'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'corporate_communicationpref_address',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                ),
            ),
            'attributes' => array(
                'class' => 'e1 select-w-320',
                'id' => 'corporate_communicationpref_address',
                'onChange' => 'show_custome_box(this.id,"corporate_custome_address");'
            ),
        ));
        $this->add(array(
            'name' => 'corporate_communicationpref_custome_address_greetings',
            'attributes' => array(
                'type' => 'text',
                'id' => 'corporate_communicationpref_custome_address_greetings'
            ),
        ));
        $this->add(array(
            'type' => 'MultiCheckbox',
            'name' => 'corporate_communicationpref_privacy',
            'options' => array(
                'value_options' => array(
                    '1' => 'Do not phone',
                    '2' => 'Do not email',
                    '3' => 'Do not mail',
                    '4' => 'Do not SMS',
                    '5' => 'Do not trade',
                    '6' => 'Once a year',
                ),
            ),
            'attributes' => array(
                'class' => 'e4 corporate_communicationpref_privacy'
            ),
        ));
        $this->add(array(
            'type' => 'MultiCheckbox',
            'name' => 'corporate_communicationpref_preferred_method',
            'options' => array(
                'value_options' => array(
                    '1' => 'Phone',
                    '2' => 'Email',
                    '3' => 'Post Mail',
                    '4' => 'SMS',
                    '5' => 'Fax',
                ),
            ),
            'attributes' => array(
                'class' => 'e4 corporate_communicationpref_preferred_method'
            ),
        ));

        $this->add(array(
            'name' => 'corporate_tagsandgroups_company_name',
            'attributes' => array(
                'type' => 'text',
                'class' => 'search-icon',
                'id' => 'corporate_tagsandgroups_company_name'
            ),
        ));
        $this->add(array(
            'name' => 'corporate_tag_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'corporate_tag_id'
            ),
        ));
        $this->add(array(
            'type' => 'MultiCheckbox',
            'name' => 'corporate_tagsandgroups_groups',
            'options' => array(
                'value_options' => array(
                    '0' => 'Administrators',
                    '1' => 'Advisory Board',
                    '2' => 'Case Resources',
                    '3' => 'Newsletter Suscribers',
                    '4' => 'Summer Program Volunteer',
                ),
            ),
            'attributes' => array(
                'value' => '', //set checked to '1'
                'class' => 'e4 corporate_tagsandgroups_groups',
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'cor_tagsandgroups_groups_from',
            'options' => array(
                'value_options' => array(),
            ),
            'attributes' => array(
                'id' => 'cor_tagsandgroups_groups_from',
                'multiple' => 'multiple',
                'value' => ''                
            )
        ));
        
        $this->add(array(
            'type' => 'Select',
            'name' => 'cor_tagsandgroups_groups_to',
            'options' => array(
                'value_options' => array(),
            ),
            'attributes' => array(
                'id' => 'cor_tagsandgroups_groups_to',
                'multiple' => 'multiple',
                'value' => ''
            )
        ));
        
        $this->add(array(
            'name' => 'cor_addbutton',
            'attributes' => array(
                'type' => 'button',
                'value' => 'ADD >>',
                'id' => 'cor_addbutton',
                'class' => 'save-btn m-l-10'
            ),
        ));

        $this->add(array(
            'name' => 'cor_removebutton',
            'attributes' => array(
                'type' => 'button',
                'value' => '<< REMOVE',
                'id' => 'cor_removebutton',
                'class' => 'cancel-btn m-l-10'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'corporate_webaccesss_security_question',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                ),
            ),
            'attributes' => array(
                'class' => 'e1 select-w-320',
                'id' => 'corporate_webaccesss_security_question',
            ),
        ));
        $this->add(array(
            'type' => 'text',
            'name' => 'corporate_webaccesss_security_answer',
            'attributes' => array(
                'id' => 'corporate_webaccesss_security_answer',
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'corporate_webaccesss_enabled_login',
            'attributes' => array(
                'value' => '', //set checked to '1'
                'class' => 'e2',
                'id' => 'corporate_webaccesss_enabled_login'
            )
        ));
        $this->add(array(
            'name' => 'corporate_publicdocs_description',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'corporate_publicdocs_description',
                'class' => 'width-90'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'corporate_notes_private',
            'attributes' => array(
                'value' => '', //set checked to '1'
                'class' => 'e2',
                'id' => 'corporate_notes_private'
            )
        ));
        $this->add(array(
            'name' => 'corporate_notes',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'corporate_notes',
                'class' => 'width-90'
            ),
        ));

        $this->add(array(
            'name' => 'corporate_addressinfo_search_contact',
            'attributes' => array(
                'type' => 'text',
                'id' => 'corporate_addressinfo_search_contact',
                'class' => 'search-icon'
            )
        ));

        $this->add(array(
            'name' => 'corporate_submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'id' => 'corporate_submitbutton',
                'class' => 'save-btn'
            ),
        ));
        $this->add(array(
            'name' => 'corporate_cancel',
            'attributes' => array(
                'type' => 'button',
                'value' => 'cancel',
                'id' => 'corporate_cancel',
                'onclick' => 'backToSearch()',
                'class' => 'cancel-btn m-l-20'
            ),
        ));
        $this->add(array(
            'name' => 'contact_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'contact_id',
            ),
        ));
    }

}