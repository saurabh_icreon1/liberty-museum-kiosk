<?php

/**
 * This page is used for change crm user  password
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This form is used for change crm user  password
 * @package    User
 * @author     Icreon Tech - AP
 */
class CrmUserChangePasswordForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('changePassword');
        $this->setAttribute('method', 'post');


        $this->add(array(
            'name' => 'crm_user_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'crm_user_id',
            )
        ));

        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type' => 'password',
                'id' => 'password'
            )
        ));
        $this->add(array(
            'name' => 'password_conf',
            'attributes' => array(
                'type' => 'Password',
                'id' => 'password_conf'
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Change Password',
                'id' => 'submitbutton',
                'class' => 'save-btn'
            ),
        ));
     $this->add(array('type'=>'Zend\Form\Element\Csrf',
     'name'=>'csrf',
     'options' => array(
             'csrf_options' => array(
                     'timeout' => 600
             )
     )
   ));
    }

}