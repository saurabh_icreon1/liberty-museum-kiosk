<?php

/**
 * This form is used for edit bio woh
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This class is used for create boi woh 
 * @package    User
 * @author     Icreon Tech - DG
 */
class BioWohForm extends Form {

    public function __construct($name = null) {
        /* we want to ignore the name passed */
        parent::__construct('edit-biowoh');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'bio_certificate_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'bio_certificate_id'
            )
        ));
        $this->add(array(
            'name' => 'woh_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'woh_id'
            )
        ));
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'name'
            )
        ));
        $this->add(array(
            'name' => 'immigrated_from',
            'attributes' => array(
                'type' => 'text',
                'id' => 'immigrated_from'
            )
        ));
        $this->add(array(
            'name' => 'ship_method',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ship_method'
            )
        ));
        $this->add(array(
            'name' => 'port_of_entry',
            'attributes' => array(
                'type' => 'text',
                'id' => 'port_of_entry'
            )
        ));
        $this->add(array(
            'name' => 'date_of_entry',
            'attributes' => array(
                'type' => 'text',
                'id' => 'date_of_entry',
                'class' => 'cal-icon'
            )
        ));
        $this->add(array(
            'name' => 'additional_info',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'additional_info'
            )
        ));
        $this->add(array(
            'name' => 'finalize',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'finalize',
                'class' => 'button semibold',
                'value' => 'Finalize'
            )
        ));
        $this->add(array(
            'name' => 'save_later',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'save_later',
                'class' => 'button semibold green',
                'value' => 'Save for Later'
            )
        ));
    }

}