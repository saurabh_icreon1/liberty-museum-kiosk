<?php

/**
 * This form is used to create search crm user assignment form
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This clss is used to create search crm user assignment form elemets
 * @package    User
 * @author     Icreon Tech - AP
 */
class SearchAssignmentForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('search');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'type' => 'Select',
            'name' => 'source',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'source',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'activity_type',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'activity_type',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));
        $this->add(array(
            'name' => 'subject',
            'attributes' => array(
                'type' => 'text',
                'id' => 'subject',
            )
        ));
        $this->add(array(
            'name' => 'with',
            'attributes' => array(
                'type' => 'text',
                'id' => 'with',
                'class'=>'search-icon ui-autocomplete-input valid'
            )
        ));
        $this->add(array(
            'name' => 'assigned',
            'attributes' => array(
                'type' => 'text',
                'id' => 'assigned',
                'class'=>'search-icon ui-autocomplete-input valid'
            )
        ));        
        $this->add(array(
            'type' => 'Select',
            'name' => 'scheduled',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    '1' => 'Chose Data Range',
                    '2' => 'This Year',
                    '3' => 'This Quarter',
                    '4' => 'This Month',
                    '5' => 'This Week',
                    '6' => 'Previous Year',
                    '7' => 'Previous Quarter',
                    '8' => 'Previous Month',
                    '9'=>'Previous Week'
                ),
            ),
            'attributes' => array(
                'id' => 'scheduled',
                'class' => 'e1 select-w-320',
                'value' => '',
                'onChange'=>'showFromTo(this.value)'
            )
        ));
        $this->add(array(
            'name' => 'from',
            'attributes' => array(
                'type' => 'text',
                'id' => 'from',
                'class'=>'width-120 cal-icon'
            )
        ));
        $this->add(array(
            'name' => 'to',
            'attributes' => array(
                'type' => 'text',
                'id' => 'to',
                'class'=>'width-120 cal-icon'
            )
        ));    
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Search',
                'id' => 'submitbutton',
                'class' => 'save-btn'
            ),
        ));
          $this->add(array(
            'name' => 'assigned_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'assigned_id'
            ),
        ));
        $this->add(array(
            'name' => 'user_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'user_id'
            ),
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'saved_search',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'value' => '',
                'class' => 'e1 select-w-320',
                'id' => 'saved_search',
                'onChange' => 'getSavedSearchResult(this.value);'
            ),
        ));
    }

}