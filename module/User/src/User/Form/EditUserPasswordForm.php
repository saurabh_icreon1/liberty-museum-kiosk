<?php

/**
 * This page is used for update password users form
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This page is used for update password  users form
 * @author     Icreon Tech - DG
 */
class EditUserPasswordForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('edituserpassword');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'contact_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'contact_id',
            )
        ));
        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type' => 'password',
                'id' => 'password',
                'class' => 'width-200'
            )
        ));

        $this->add(array(
            'name' => 'password_conf',
            'attributes' => array(
                'type' => 'password',
                'id' => 'password_conf',
                 'class' => 'width-200'
            )
        ));
        $this->add(array(
            'name' => 'security_answer',
            'attributes' => array(
                'type' => 'text',
                'id' => 'security_answer',
                 'class' => 'width-200'
            )
        ));

        $this->add(array('type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 600
                )
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Update Password',
                'id' => 'submitbutton',
                'class' => 'button'
            ),
        ));
    }

}