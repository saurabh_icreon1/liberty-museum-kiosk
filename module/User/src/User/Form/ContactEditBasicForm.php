<?php

/**
 * This form is used for ContactEditBasicForm
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Form;

use Zend\Form\Form;

/** This class is used for create form elements for contact of type Individual
 * @author     Icreon Tech - DG
 */
class ContactEditBasicForm extends Form {

    public function __construct($name = null) {
        /* we want to ignore the name passed */
        parent::__construct('edit_basic_info');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->add(array(
            'name' => 'upload_profile_image',
            'attributes' => array(
                'type' => 'file',
                'value' => 'upload',
                'id' => 'upload_profile_image',
            ),
        ));
        $this->add(array(
            'name' => 'upload_profile_image_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'upload_profile_image_id',
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'title',
            'options' => array(
                'value_options' => array(
                )
            ),
            'attributes' => array(
                'id' => 'title',
                'class' => 'edit-title',
                'value' => ''
            )
        ));

        $this->add(array(
            'name' => 'first_name',
            'attributes' => array(
                'type' => 'text',
                'class' => 'width-127', //'edit-firstname button',
                'id' => 'first_name'
            )
        ));
        $this->add(array(
            'name' => 'middle_name',
            'attributes' => array(
                'type' => 'text',
                'class' => 'width-127', //'edit-middlename button',
                'id' => 'middle_name'
            )
        ));
        $this->add(array(
            'name' => 'last_name',
            'attributes' => array(
                'type' => 'text',
                'class' => 'width-127', //'edit-lastname button',
                'id' => 'last_name'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'suffix',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'class' => 'edit-suffix',
                'id' => 'suffix',
                'value' => ''
            )
        ));
        $this->add(array(
            'name' => 'email_id',
            'attributes' => array(
                'type' => 'hidden',
                'class' => 'edit-email button',
                'id' => 'email_id',
                'readonly' => true,
            )
        ));
        $this->add(array(
            'name' => 'update_email',
            'attributes' => array(
                'type' => 'button',
                'onclick' => 'editEmail();',
                'value' => 'Update Email',
                'id' => 'update_email',
                'class' => 'edit-info-btn button'
            ),
        ));
        $this->add(array(
            'name' => 'conf_email_id',
            'attributes' => array(
                'type' => 'hidden',
                'readonly' => true,
                'id' => 'conf_email_id',
                'class' => 'edit-confirm-email button'
            )
        ));
        $this->add(array(
            'name' => 'username',
            'attributes' => array(
                'type' => 'text',
                'class' => 'edit-username button',
                'id' => 'username',
                'readonly' => true,
            )
        ));

        $this->add(array(
            'name' => 'update_password',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Update Password',
                'onclick' => 'editPassword();',
                'id' => 'update_password',
                'class' => 'edit-info-btn button'
            ),
        ));

     
      

        $this->add(array(
            'type' => 'text',
            'name' => 'security_question',
            'attributes' => array(
                'readonly' => true,
                'id' => 'security_question',
            ),
        ));
        $this->add(array(
            'type' => 'text',
            'name' => 'security_answer',
            'attributes' => array(
                'readonly' => true,
                'id' => 'security_answer',
                'class' => 'edit-security button'
            ),
        ));
        $this->add(array(
            'name' => 'submitbutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'id' => 'submitbutton',
                'class' => 'button' //myfile-edit-blue button
            ),
        ));
        
        // start --
          $this->add(array(
            'name' => 'submitbutton1',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'id' => 'submitbutton1',
                'class' => 'button' //myfile-edit-blue button
            ),
          ));
          
        $this->add(array(
            'name' => 'submitbutton2',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'id' => 'submitbutton2',
                'class' => 'button' //myfile-edit-blue button
            ),
        ));
     
     
         $this->add(array(
            'name' => 'submitbutton3',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'id' => 'submitbutton3',
                'class' => 'button' //myfile-edit-blue button
            ),
        ));
         
       $this->add(array(
            'name' => 'submitbutton4',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'id' => 'submitbutton4',
                'class' => 'button' //myfile-edit-blue button
            ),
        ));
     
     
      $this->add(array(
            'name' => 'submitbutton5',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'id' => 'submitbutton5',
                'class' => 'button' //myfile-edit-blue button
            ),
        ));
      
       $this->add(array(
            'name' => 'submitbutton6',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'id' => 'submitbutton6',
                'class' => 'button', //myfile-edit-blue button
                'style' => 'display:none;'
            ),
        ));
       
       // end --  
         
          
      $this->add(array(
            'name' => 'cancel2',
            'attributes' => array(
                'type' => 'button',
                'value' => 'cancel',
                'class' => 'button green cancelsave' //myfile-edit-blue edit-green button
            ),
        ));
          
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'value' => 'cancel',
                'id' => 'cancel',
                'onclick' => 'goToProfile();',
                'class' => 'button green' //myfile-edit-blue edit-green button
            ),
        ));
        $this->add(array(
            'name' => 'update_question',
            'attributes' => array(
                'type' => 'button',
                'onclick' => 'editQuestion();',
                'value' => 'Update Question',
                'id' => 'update_question',
                'class' => 'edit-info-btn button'
            ),
        ));
        $this->add(array('type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 600
                )
            )
        ));
        $this->add(array(
            'name' => 'contact_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'contact_id',
            ),
        ));
    }

}