<?php

/**
 * This form is used for upload files in development form
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This class is used for upload files in development form
 * @package    User
 * @author     Icreon Tech - AP
 */
class DevelopmentUploadForm extends Form {

    public function __construct($name = null) {
        /* we want to ignore the name passed */
        parent::__construct('developmentupload');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->add(array(
            'name' => 'file_name',
            'attributes' => array(
                'type' => 'file',
                'id' => 'file_name',
            ),
        ));
    }

}