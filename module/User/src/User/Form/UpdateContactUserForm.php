<?php

/**
 * This form is used to update multiple contact users.
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This class is used to create update multiple crm users form .
 * @package    User
 * @author     Icreon Tech - AP
 */
class UpdateContactUserForm extends Form {

    public function __construct($name = null) {
        /* we want to ignore the name passed */
        parent::__construct('update_contact_user');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'membership_package_value',
            'options' => array(
                'value_options' => array(
                    ' ' => 'Select',
                    '0' => 'Pending',
                    '1' => 'Sent',
                ),
            ),
            'attributes' => array(
                'id' => 'membership_package_value',
                'value' => '', //set selected to 'blank'
                'class' => 'e1 select-w-320 left'
            )
        ));

        $this->add(array(
            'name' => 'updateButton',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Update',
                'id' => 'updateButton',
                'class' => 'save-btn m-l-5',
            ),
        ));
    }

}