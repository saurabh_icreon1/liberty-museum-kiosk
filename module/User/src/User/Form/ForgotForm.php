<?php

/**
 * This page is used for forgot users form
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This page is used for forgot users form
 * @author     Icreon Tech - DG
 */
class ForgotForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('forgot');
        $this->setAttribute('method', 'post');


        $this->add(array(
            'name' => 'email_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email_id',
            )
        ));
        $this->add(array('type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 600
                )
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit',
                'id' => 'submitbutton',
                'class' => 'button'
            ),
        ));
    }

}