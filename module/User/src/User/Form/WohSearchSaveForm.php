<?php

/**
 * This form is used for WohSearchSaveForm
 * @category   Zend
 * @package    User_WohSearchSaveForm
 * @version    2.2
 * @author     Icreon Tech - NS
 */

namespace User\Form;

use Zend\Form\Form;

class WohSearchSaveForm extends Form {

    public function __construct($name = null) {

        parent::__construct('save-Search-Woh');
        $this->setAttribute('id', 'save_search');
        $this->setAttribute('name', 'save_search');
        $this->setAttribute('method', 'post');

         
	  $this->add(array(
		  'name' => 'save_search_as',
		  'attributes' => array(
			  'type' => 'text',
			  'id' => 'save_search_as',
                          'maxlength' => '50',
                          'class' => 'button'
		  )
	  ));
     
    $this->add(array(
		  'name' => 'savesearch',
		  'attributes' => array(
			  'type' => 'submit',
			  'value' => '', //Save Search
			  'id' => 'savesearch',
			  'class' => 'woh-savesearch button' //button float-right
		  ),
	  ));

    }

}