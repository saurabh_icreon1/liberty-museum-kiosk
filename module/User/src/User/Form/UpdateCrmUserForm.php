<?php

/**
 * This form is used to update multiple crm users.
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This class is used to create update multiple crm users form .
 * @package    User
 * @author     Icreon Tech - AP
 */
class UpdateCrmUserForm extends Form {

    public function __construct($name = null) {
        /* we want to ignore the name passed */
        parent::__construct('update_crm_user');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'activity',
            'options' => array(
                'value_options' => array(
                    'active' => 'Unblock the selected users',
                    'block' => 'Block the selected users',
                    'delete' => 'Cancel the selected users account',
                ),
            ),
            'attributes' => array(
                'id' => 'activity',
                'value' => '', //set selected to 'blank'
                'class' => 'e1 select-w-320'
            )
        ));

        $this->add(array(
            'name' => 'updateButton',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Update',
                'id' => 'updateButton',
                'class' => 'save-btn m-l-5',
            ),
        ));
    }

}