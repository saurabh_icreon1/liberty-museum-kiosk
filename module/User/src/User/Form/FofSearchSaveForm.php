<?php

/**
 * This form is used for save Fof
 * @category   Zend
 * @package    User_ActivationForm
 * @version    2.2
 * @author     Icreon Tech - NS
 */

namespace User\Form;

use Zend\Form\Form;

class FofSearchSaveForm extends Form {

    public function __construct($name = null) {

        parent::__construct('save-Search-Fof');
        $this->setAttribute('id', 'save_fof');
        $this->setAttribute('name', 'save_fof');
        $this->setAttribute('method', 'post');

         
	  $this->add(array(
		  'name' => 'save_search_as',
		  'attributes' => array(
			  'type' => 'text',
			  'id' => 'save_search_as',
			  'class' => 'save',
		  )
	  ));
     
        $this->add(array(
                      'name' => 'savesearch',
                      'attributes' => array(
                              'type' => 'submit',
                              'value' => '',
                              'id' => 'savesearch',
                              'class' => 'savebtn'
                      ),
              ));

    }

}