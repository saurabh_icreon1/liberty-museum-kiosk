<?php

/**
 * This form is used for create phone no in contact
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Form;

use Zend\Form\Form;
use User\Form\PhonenoForm;

/**
 * This class is used for create phone no from
 * @package    User
 * @author     Icreon Tech - DG
 */
class CorporatePhonenoForm extends Form {

    public function __construct($name = null) {
        /* we want to ignore the name passed */
        parent::__construct('corporatephoneno');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'corporate-primary-phone',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'corporate-primary-phone',
                'value' => '0'
            )
        ));
        
        $this->add(array(
            'name' => 'corporate_country_code[]',
            'attributes' => array(
                'type' => 'text',
                'class' => 'm-l-20 width-125 m-r-20'
            )
        ));

        $this->add(array(
            'name' => 'corporate_area_code[]',
            'attributes' => array(
                'type' => 'text',
                'class' => 'width-80 m-r-20'
            )
        ));
        $this->add(array(
            'name' => 'corporate_phone_no[]',
            'attributes' => array(
                'type' => 'text',
                'class' => 'phone_no m-r-20',
                'id' => 'corporate_phone_no[]'
            ),
        ));

        $this->add(array(
            'name' => 'corporate_extension[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'corporate_extension[]',
                'class' => 'width-80 m-r-20'
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'corporate_phone_type[]',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                ),
            ),
            'attributes' => array(
                'id' => 'corporate_phone_type',
                'class' => 'e1 left select-w-150 phone-type',
                'value' => '' /* set selected to 'blank' */
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'corporate_continfo_primary[]',
            'options' => array(
                'value_options' => array(
                    '1' => 'Primary',
                ),
            ),
            'attributes' => array(
                'value' => '', /* set checked to '1' */
                'class' => 'phone-radio',
                'id' => 'corporate_continfo_primary0',
                'onclick' => 'corporateCheckPrimary(0);'
            )
        ));
    }

}