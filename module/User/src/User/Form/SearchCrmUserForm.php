<?php

/**
 * This form is used to create search crm users form
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This clss is used to create search crm users form elemets
 * @package    User
 * @author     Icreon Tech - AP
 */
class SearchCrmUserForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('search');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'name_email',
            'attributes' => array(
                'type' => 'text',
                'id' => 'name_email',
            )
        ));
        $this->add(array(
            'name' => 'user_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'user_name'
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'role',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'role',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'status',
            'options' => array(
                'value_options' => array(
                    '' => 'All',
                    '1' => 'Active',
                    '0' => 'Blocked'
                ),
            ),
            'attributes' => array(
                'id' => 'status',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Search',
                'id' => 'submitbutton',
                'class' => 'search-btn'
            ),
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'saved_search',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'value' => '',
                'class' => 'e1 select-w-320',
                'id' => 'saved_search',
                'onChange' => 'getSavedSearchResult(this.value);'
            ),
        ));
    }

}