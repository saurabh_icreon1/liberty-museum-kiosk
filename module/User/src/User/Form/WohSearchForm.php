<?php

/**
 * This form is used for WohSearchForm
 * @category   Zend
 * @package    User_WohSearchForm
 * @version    2.2
 * @author     Icreon Tech - NS
 */

namespace User\Form;

use Zend\Form\Form;

class WohSearchForm extends Form {

    public function __construct($name = null) {

        parent::__construct('search-Woh');
        $this->setAttribute('id', 'search_woh');
        $this->setAttribute('name', 'search_woh');
        $this->setAttribute('method', 'post');
        
      
	   $this->add(array(
		  'name' => 'page',
		  'attributes' => array(
			  'type' => 'hidden',
			  'id' => 'page'
		  )
	  ));
     
	 $this->add(array(
		  'name' => 'sort_field',
		  'attributes' => array(
			  'type' => 'hidden',
			  'id' => 'sort_field'
		  )
	  ));
	  
	  $this->add(array(
		  'name' => 'sort_order',
		  'attributes' => array(
			  'type' => 'hidden',
			  'id' => 'sort_order'
		  )
	  ));
	  
	  $this->add(array(
		  'name' => 'start_index',
		  'attributes' => array(
			  'type' => 'hidden',
			  'id' => 'start_index'
		  )
	  ));
	  
	 $this->add(array(
		  'name' => 'limit',
		  'attributes' => array(
			  'type' => 'hidden',
			  'id' => 'limit'
		  )
	  ));
	 
         
	  $this->add(array(
		  'name' => 'last_name',
		  'attributes' => array(
			  'type' => 'text',
			  'id' => 'last_name',
                          'class' => 'button'
		  )
	  ));
     
	  $this->add(array(
		  'name' => 'first_name',
		  'attributes' => array(
			  'type' => 'text',
			  'id' => 'first_name',
                          'class' => 'button'
		  )
	  ));
          
            $this->add(array(
		  'name' => 'first_name_initial',
		  'attributes' => array(
			  'type' => 'text',
			  'id' => 'first_name_initial',
                          'maxlength' => '1',
			  'class' => 'first-name'
		  )
	  ));

	  $this->add(array(
		  'name' => 'other_part_name',
		  'attributes' => array(
			  'type' => 'text',
			  'id' => 'other_part_name',
                          'class' => 'button'
		  )
	  ));

	  $this->add(array(
		  'name' => 'country',
		  'attributes' => array(
			  'type' => 'text',
			  'id' => 'country',
                          'class' => 'country'
		  )
	  ));
	 
	  $this->add(array(
		  'name' => 'donated_by',
		  'attributes' => array(
			  'type' => 'text',
			  'id' => 'donated_by',
                          'class' => 'woh-input button'
		  )
	  ));

            $this->add(array(
		  'name' => 'donated_by_add',
		  'attributes' => array(
			  'type' => 'text',
			  'id' => 'donated_by_add',
                          'class' => 'woh-input button'
		  )
	  ));
            
	  $this->add(array(
		  'name' => 'panel_number',
		  'attributes' => array(
			  'type' => 'text',
			  'id' => 'panel_number',
                           'class' => 'woh-input1 button'
		  )
	  ));	  
         
          
   // For view selection - start ( 1 - grid , 2 - listing )  
       $this->add(
            array(
                'name' => 'view_woh_type',
                'attributes' => array(
                'type' => 'hidden',
                'value' => '1',    
                'id' => 'view_woh_type'
               )
	  ));	  
   // For view selection - end                 
       
    $this->add(
            array(
                 'name' => 'submitsearch1',
                 'attributes' => array(
                 'type' => 'button',
                 'value' => 'Find',
                 'id' => 'submitsearch1',
                 'class' => 'find-btn button'
                 ),
	  ));
	  
    $this->add(array(
		  'name' => 'submitsearch2',
		  'attributes' => array(
			  'type' => 'button',
			  'value' => 'Search',
			  'id' => 'submitsearch2',
			  'class' => 'find-btn button'
		  ),
	  ));

    $this->add(array(
		  'name' => 'submitsearch3',
		  'attributes' => array(
			  'type' => 'submit',
			  'value' => 'Search',
			  'id' => 'submitsearch3',
			  'class' => 'button'
		  ),
	  ));

    }

}