<?php

/**
 * This file is used for jobhistory form
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This form is used for create jobhistory  form elements
 * @package    User
 * @author     Icreon Tech - DG
 */
class JobHistoryForm extends Form {

    public function __construct($name = null) {
        parent::__construct('jobhistory');
        $this->setAttribute('method', 'post');


        $this->add(array(
            'name' => 'job_title[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'job_title[]'
            ),
        ));
        $this->add(array(
            'name' => 'company_id[]',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'company_id0'
            ),
        ));
        $this->add(array(
            'name' => 'comp_name[]',
            'attributes' => array(
                'type' => 'text',
                'class' => 'company_name_auto_search',
                'id' => 'comp_name0'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'industry[]',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                ),
            ),
            'attributes' => array(
                'id' => 'industry[]',
                'value' => '', /* set selected to 'blank' */
                'class' => 'e1'
            )
        ));
        $this->add(array(
            'name' => 'period_from[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'period_from0',
                'class' => 'width-120 cal-icon period_from_dev'
            ),
        ));
        $this->add(array(
            'name' => 'period_to[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'period_to0',
                'class' => 'width-120 cal-icon period_to_dev'
            ),
        ));
    }

}