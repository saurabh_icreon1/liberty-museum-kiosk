<?php

/**
 * This form is used for SurveyForm
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This class is used for Dashlet Settings (CRM)
 * @package    User
 * @author     Icreon Tech - SK
 */
class DashletSettingForm extends Form {

    public function __construct($name = null) {
        /* we want to ignore the name passed */
        parent::__construct('DashletSetting');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'role_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'role_id'
            ),
        ));
        $this->add(array(
            'name' => 'module_dashlet_submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'id' => 'module_dashlet_submitbutton',
                'class' => 'save-btn'
            ),
        ));

        $this->add(array(
            'name' => 'module_dashlet_cancel',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Cancel',
                'id' => 'module_dashlet_cancelbutton',
                'class' => 'cancel-btn m-l-10',
                'onClick' => 'backToSearch();'
            ),
        ));
		
		
    }

}