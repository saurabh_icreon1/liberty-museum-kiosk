<?php

/**
 * This form is used to create gift form
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This class is used to create gift form elements
 * @package    User
 * @author     Icreon Tech - AP
 */
class GiftForm extends Form {

    public function __construct($name = null) {
        /* we want to ignore the name passed */
        parent::__construct('gift');
        $this->setAttribute('method', 'post');


        $this->add(array(
            'name' => 'year[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'year[]'
            ),
        ));
        $this->add(array(
            'name' => 'organization[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'organization[]'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'organization_type[]',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    '1' => 'Organization Type 1',
                    '2' => 'Organization Type 2',
                    '3' => 'Organization Type 3',
                    '4' => 'Organization Type 4',
                    '5' => 'Organization Type 5'
                ),
            ),
            'attributes' => array(
                'id' => 'organization_type',
                'value' => '',
                'class' => 'e1'
            )
        ));
        $this->add(array(
            'name' => 'amount[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'amount[]'
            ),
        ));
    }

}