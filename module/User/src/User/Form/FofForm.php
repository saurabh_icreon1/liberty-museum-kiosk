<?php

/**
 * This form is used for adding Flag of faces
 * @category   Zend
 * @package    User_FofForm
 * @version    2.2
 * @author     Icreon Tech - NS
 */

namespace User\Form;

use Zend\Form\Form;

class FofForm extends Form {

    public function __construct($name = null) {

        parent::__construct('create-Fof');
        $this->setAttribute('id', 'add_fof');
        $this->setAttribute('name', 'add_fof');
        $this->setAttribute('method', 'post');
        
        
         $this->add(array(
            'name' => 'image_name',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'image_name'
            )
        ));
         
       $this->add(array(
            'name' => 'fof_image',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'fof_image'
            )
        ));
       
      $this->add(array(
            'name' => 'temp_image',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'temp_image'
            )
        ));
            
       
           		 
     $this->add(array(
         'name' => 'upload_photo_button',
         'attributes' => array(
             'type' => 'button',
             'value' => 'Upload photo',
             'id' => 'upload_photo_button',
             'class' => 'button'
         ),
      ));
     
       $this->add(array(
            'type' => 'file',
            'name' => 'upload_photo',
            'attributes' => array(
                'id' => 'upload_photo'//,
				//'class' => 'file-fof'
            )
        ));
                
          
   /*     $this->add(array(
            'name' => 'is_photo_uploaded',
            'type' => 'Checkbox',
            'attributes' => array(
                'class' => 'checkbox',
                'id' => 'is_photo_uploaded'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => 'Checkbox',
                    '0' => 'Checkbox',
                ),
            ),
			'use_hidden_element' => false
       ));
            
       $this->add(array(
             'name' => 'is_photo_uploaded',
             'type' => 'Checkbox',
             'attributes' => array(
                 //'value' => '0',
                 'class' => 'checkbox',
				 'id' => 'is_photo_uploaded'
             ),
             'options' => array(
                 'value_options' => array(
                     '0' => 'Checkbox',
                     '1' => 'Checkbox',
                 ),
                'use_hidden_element' => false
             ),
        ));
		*/ 
		      
             
        $this->add(array(
             'name' => 'is_photo_uploaded',
             'type' => 'Checkbox',
             'attributes' => array(
                 'value' => '0',
                 'class' => 'css-checkbox',
		         'id' => 'is_photo_uploaded'
             ),
             'options' => array(
                 'value_options' => array(
                     '0' => 'Checkbox',
                     '1' => 'Checkbox',
                 ),
                 'use_hidden_element' => false
             ),
        ));  

		
        $this->add(array(
             'name' => 'donated_by',
             'attributes' => array(
                 'type' => 'text',
                 'id' => 'donated_by',
                 'maxlength' => '100'
             )
         ));
        
        $this->add(array(
             'name' => 'donated_by_edit',
             'attributes' => array(
                 'type' => 'text',
                 'id' => 'donated_by_edit',
                 'maxlength' => '100'
             )
         ));
           
         $this->add(array(
             'name' => 'people_photo',
             'attributes' => array(
                 'type' => 'hidden',
                 'id' => 'people_photo',
                // 'maxlength' => '100',
                 'class' => 'textarea'
             )
         ));
         
          $this->add(array(
             'name' => 'people_photo_edit',
             'attributes' => array(
                 'type' => 'hidden',
                 'id' => 'people_photo_edit',
               //  'maxlength' => '100',
                 'class' => 'textarea'
             )
         )); 
           
          

      $this->add(array(
            'name' => 'is_people_photo_visible',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'is_people_photo_visible',
				'value' => '1'
            )
        ));
           	  
         $this->add(array(
             'name' => 'photo_caption',
             'attributes' => array(
                 'type' => 'textarea',
                 'id' => 'photo_caption',
                 'maxlength' => '40',
                 'class' => 'textarea'
             )
         ));
         
        $this->add(array(
             'name' => 'photo_caption_edit',
             'attributes' => array(
                 'type' => 'textarea',
                 'id' => 'photo_caption_edit',
                 'maxlength' => '40',
                 'class' => 'textarea'
             )
         ));
         
	   
		$this->add(array(
            'name' => 'is_photo_caption_visible',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'is_photo_caption_visible',
				'value' => '1'
            )
        ));
		
   
        $this->add(array(
             'name' => 'is_agree',
             'type' => 'Checkbox',
             'attributes' => array(
                 'value' => '0',
                 'class' => 'css-checkbox',
				 'id' => 'is_agree'
             ),
             'options' => array(
                 'value_options' => array(
                     '0' => 'Checkbox',
                     '1' => 'Checkbox',
                 ),
                'use_hidden_element' => false
             ),
        ));
            
        
        $this->add(array(
             'name' => 'savebutton1',
             'attributes' => array(
                 'type' => 'submit',
                 'value' => 'Photo Correction',
                 'id' => 'savebutton1',
                 'class' => 'button'
             ),
         ));
		 
        $this->add(array(
             'name' => 'savebutton2',
             'attributes' => array(
                 'type' => 'submit',
                 'value' => 'Confirm Details',
                 'id' => 'savebutton2',
                 'class' => 'button'
             ),
         ));
		 
        $this->add(array(
             'name' => 'savebutton',
             'attributes' => array(
                 'type' => 'submit',
                 'value' => 'Add to Cart',
                 'id' => 'savebutton',
                 'class' => 'button green add'
             ),
         ));

        $this->add(array(
             'name' => 'cancelbutton',
             'attributes' => array(
                 'type' => 'button',
                 'value' => 'Cancel',
                 'id' => 'cancelbutton',
                 'class' => 'button green',
                 'onclick' => 'javascript:window.location.reload();'
             ),
         ));
        
        
        $this->add(array(
         'name' => 'step3fullBack',
         'attributes' => array(
                 'type' => 'button',
                 'value' => 'BACK',
                 'id' => 'step3fullBack',
                 'class' => 'back-btn button'
           ),
        ));

        $this->add(array(
                 'name' => 'step3fullNext',
                 'attributes' => array(
                         'type' => 'submit',
                         'value' => 'NEXT',
                         'id' => 'step3fullNext',
                         'class' => 'next-btn button'
                 ),
         ));

        $this->add(array(
                 'name' => 'step4fullBack',
                 'attributes' => array(
                         'type' => 'button',
                         'value' => 'BACK',
                         'id' => 'step4fullBack',
                         'class' => 'back-btn button'
                 ),
         ));

    }

}