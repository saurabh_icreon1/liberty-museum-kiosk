<?php

/**
 * This page is used for login users form
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This page is used for Login users form
 * @author     Icreon Tech - DG
 */
class LoginForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('login');
        $this->setAttribute('method', 'post');


        $this->add(array(
            'name' => 'user_name',
            'attributes' => array(
                'type' => 'text',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Password',
            'name' => 'password'
        ));

        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'rememberme',
            'attributes' => array(
                'value' => '' //set checked to '1'
            )
        ));
/*
        $this->add(array('type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 600
                )
            )
        ));*/
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Login',
                'id' => 'submitbutton',
                'class' => 'button green'
            ),
        ));
    }

}