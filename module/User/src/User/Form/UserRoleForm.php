<?php

/**
 * This form is used to add user role
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This class is used to create from elemnts for user role 
 * @package    User
 * @author     Icreon Tech - AP
 */
class UserRoleForm extends Form {

    public function __construct($param = null) {
        parent::__construct('user_role');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'role_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'role_name'
            )
        ));
        $this->add(array(
            'name' => 'savebutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => "Add Role",
                'class' => 'save-btn m-l-5',
                'id' => 'savebutton'
            )
        ));
        $this->add(array('type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 600
                )
            )
        ));
    }

}