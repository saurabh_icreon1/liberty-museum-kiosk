<?php

/**
 * This page is used for reset password form
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This page is used for reset password form
 * @author     Icreon Tech - DG
 */
class ResetPasswordForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('password-reset');
        $this->setAttribute('method', 'post');


        $this->add(array(
            'name' => 'code',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'code',
            )
        ));

        $this->add(array(
            'name' => 'email_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email_id',
            )
        ));
        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type' => 'Password',
                'id' => 'password',
            )
        ));
        $this->add(array(
            'name' => 'password_conf',
            'attributes' => array(
                'type' => 'Password',
                'id' => 'password_conf'
            )
        ));

        $this->add(array('type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 600
                )
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Reset Password',
                'id' => 'submitbutton',
                'class' => 'button'
            ),
        ));
    }

}