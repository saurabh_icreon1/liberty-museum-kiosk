<?php

/**
 * This form is used for save search crm users 
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This class is used to create save search crm users  form elements
 * @package    User
 * @author     Icreon Tech - AP
 */
class SaveSearchCrmUserForm extends Form {

    public function __construct($name = null) {
        /* we want to ignore the name passed */
        parent::__construct('save_search');
        $this->setAttribute('method', 'post');
        $this->setAttribute('id', 'save_search');

        $this->add(array(
            'name' => 'search_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'search_id'
            )
        ));
        $this->add(array(
            'name' => 'search_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'search_name'
            )
        ));
        $this->add(array(
            'name' => 'savebutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => "Save",
                'class' => 'save-btn',
                'id' => 'savebutton'
            )
        ));
    }

}

?>
