<?php

/**
 * This form is used for registration.
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Form;

use Zend\Form\Form;
use Zend\Form\Element\Captcha;
use Zend\Captcha\Image as CaptchaImage;

/**
 * This page is used for users form
 * @author     Icreon Tech - DG
 */
class ContactusForm extends Form {

    protected $captchaAdapter;

    public function __construct($config = null) {
        $dirdata = './public';
        //pass captcha image options
        $captchaImage = new CaptchaImage(array(
                    'font' => $dirdata . '/fonts/opensans-bold-webfont.ttf',
                    'width' => 200,
                    'Expiration' => '100',
                    'height' => 80,
                    'WordLen' => 6,
                    'dotNoiseLevel' => 20,
                    'lineNoiseLevel' => 2)
        );
        $captchaImage->setImgDir($config['assets_upload_dir'] . 'captcha/');
        $captchaImage->setImgUrl($config['assets_url'] . 'captcha/');

        // we want to ignore the name passed
        parent::__construct('contactus');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'first_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'first_name'
            )
        ));
        $this->add(array(
            'name' => 'last_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_name'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'case_type',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'case_type',
                'class' => 'e1'
            )
        ));
        $this->add(array(
            'name' => 'email_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email_id'
            )
        ));
        $this->add(array(
            'name' => 'phone',
            'attributes' => array(
                'type' => 'text',
                'id' => 'phone'
            )
        ));
        $this->add(array(
            'name' => 'description',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'description'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Captcha',
            'name' => 'captcha',
            'options' => array(
                'captcha' => $captchaImage,
            ),
            'attributes' => array(
                'id' => 'captcha'
            )
        ));

        $this->add(array('type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 600
                )
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Submit',
                'class' => 'button green',
                'id' => 'submitbutton',
            ),
        ));
    }

}