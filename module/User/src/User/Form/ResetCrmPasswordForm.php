<?php

/**
 * This page is used for reset crm user forgot password
 * @package    User_Login_ResetCrmPasswordForm
 * @author     Icreon Tech - DT
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This form is used for reset crm user forgot password
 * @package    User_Login_ResetCrmPasswordForm
 * @author     Icreon Tech - DT
 */
class ResetCrmPasswordForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('crm_password_reset');
        $this->setAttribute('method', 'post');


        $this->add(array(
            'name' => 'code',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'code',
            )
        ));

        $this->add(array(
            'name' => 'email_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email_id',
                'class' => 'reset'
            )
        ));
        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type' => 'Password',
                'id' => 'password',
                'class' => 'reset'
            )
        ));
        $this->add(array(
            'name' => 'password_conf',
            'attributes' => array(
                'type' => 'Password',
                'id' => 'password_conf',
                'class' => 'reset'
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Reset Password',
                'id' => 'submitbutton',
                'class' => 'button'
            ),
        ));
    }

}