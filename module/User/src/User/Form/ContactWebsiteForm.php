<?php

/**
 * This form is used for create website section in contact.
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This class is used for create website section in contact.
 * @package    User
 * @author     Icreon Tech - AP
 */
class ContactWebsiteForm extends Form {

    public function __construct($name = null) {
        /* we want to ignore the name passed */
        parent::__construct('website');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'website[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'website',
                'class' => 'phone_no'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'don_not_have_website',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'don_not_have_website',
                'class' => 'e2'
            ),
        ));
    }

}