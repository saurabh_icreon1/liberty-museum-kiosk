<?php

/**
 * This form is used for alter membership amount
 * @category   Zend
 * @package    User_ActivationForm
 * @version    2.2
 * @author     Icreon Tech - AS
 */

namespace User\Form;

use Zend\Form\Form;

class MembershipForm extends Form {

    public function __construct($name = null) {

        // we want to ignore the name passed
        parent::__construct('membershipqqqqq');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'amount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'amount',
                'onkeyup' => 'submitMembership()'
            )
        ));
        $this->add(array(
            'name' => 'jsonValue',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'jsonValue',
            )
        ));
        $this->add(array(
            'name' => 'membershipId',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'membershipId',
            )
        ));
        $this->add(array(
            'name' => 'minAmount',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'minAmount',
            )
        ));    
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Add Membership',
                'class' => 'button',
                'id' => 'submit',
            ),
        ));
    }

}
