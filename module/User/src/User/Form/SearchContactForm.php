<?php

/**
 * This form is used tof search contact for admin
 * @package    User
 * @author     Icreon Tech - AS
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This page is used for search users form
 * @author     Icreon Tech - DG
 */
class SearchContactForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('search');
        $this->setAttribute('method', 'post');

        // new fields - start
        $this->add(array(
	'name' => 'first_name',
	'attributes' => array(
            'type' => 'text',
            'id' => 'first_name',
          )
        ));

        $this->add(array(
            'name' => 'last_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_name',
            )
        ));

        $this->add(array(
            'name' => 'company_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'company_name',
            )
        ));	

        $this->add(array(
            'name' => 'email_address',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email_address',
             )
        ));	
        // new fields - end
        
        $this->add(array(
            'name' => 'user_email',
            'attributes' => array(
                'type' => 'text',
                'id' => 'user_email',
            )
        ));
        $this->add(array(
            'name' => 'phone',
            'attributes' => array(
                'type' => 'text',
                'id' => 'phone'
            )
        ));
        $this->add(array(
            'name' => 'transaction',
            'attributes' => array(
                'type' => 'text',
                'id' => 'transaction'
            )
        ));
        $this->add(array(
            'name' => 'contact_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'contact_id'
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'association',
            'options' => array(
                'value_options' => array(
                    '' => 'Any',
                    '1' => 'Account',
                    '0' => 'Non Account'
                ),
            ),
            'attributes' => array(
                'id' => 'association',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));
        
        $this->add(array(
            'type' => 'Select',
            'name' => 'status',
            'options' => array(
                'value_options' => array(
                    '' => 'Any',
                    '1' => 'Active',
                    '0' => 'Inactive'
                ),
            ),
            'attributes' => array(
                'id' => 'status',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'last_transaction',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    'choose_date' => 'Choose Date',
                    'this_year' => 'This Year',
                    'this_quarter' => 'This Quarter',
                    'this_month' => 'This Month',
                    'this_week' => 'This Week',
                    'previous_year' => 'Previous Year',
                    'previous_quarter' => 'Previous Quarter',
                    'previous_month' => 'Previous Month',
                    'previous_week' => 'Previous Week',
                ),
            ),
            'attributes' => array(
                'id' => 'last_transaction',
                'class' => 'e1 select-w-320',
                'value' => 'choose_date'
            )
        ));
        
        $this->add(array(
            'type' => 'Select',
            'name' => 'registration_date',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    'choose_date' => 'Choose Date',
                    'this_year' => 'This Year',
                    'this_quarter' => 'This Quarter',
                    'this_month' => 'This Month',
                    'this_week' => 'This Week',
                    'previous_year' => 'Previous Year',
                    'previous_quarter' => 'Previous Quarter',
                    'previous_month' => 'Previous Month',
                    'previous_week' => 'Previous Week',
                ),
            ),
            'attributes' => array(
                'id' => 'registration_date',
                'class' => 'e1 select-w-320',
                'value' => 'choose_date'
            )
        ));

        $this->add(array(
            'name' => 'from_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'from_date',
                'class' => 'calendar-input cal-icon',
                "readonly" => "readonly"
            )
        ));

        $this->add(array(
            'name' => 'to_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'to_date',
                'class' => 'calendar-input cal-icon',
                "readonly" => "readonly"
            )
        ));
        
        $this->add(array(
            'name' => 'registration_date_from',
            'attributes' => array(
                'type' => 'text',
                'id' => 'registration_date_from',
                'class' => 'calendar-input cal-icon',
                "readonly" => "readonly"
            )
        ));

        $this->add(array(
            'name' => 'registration_date_to',
            'attributes' => array(
                'type' => 'text',
                'id' => 'registration_date_to',
                'class' => 'calendar-input cal-icon',
                "readonly" => "readonly"
            )
        ));
        
        $this->add(array(
            'name' => 'afihc',
            'attributes' => array(
                'type' => 'text',
                'id' => 'afihc'
            )
        ));
        $this->add(array(
            'name' => 'membership_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'membership_id'
            )
        ));
        $this->add(array(
            'name' => 'source',
            'type' => 'Select',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'id' => 'source',
                'class' => 'e1 select-w-320',
                'value' => '',
                'size' => '4'
            )
        ));
        $this->add(array(
            'name' => 'type',
            'type' => 'Select',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'id' => 'type',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));
        $this->add(array(
            'name' => 'membership_package_shipped',
            'type' => 'Select',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    '1' => 'Yes',
                    '0' => 'No'
                ),
            ),
            'attributes' => array(
                'id' => 'membership_package_shipped',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));
        $this->add(array(
            'name' => 'membership_year',
            'type' => 'Select',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'id' => 'membership_year',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));



        $this->add(array(
            'name' => 'company',
            'attributes' => array(
                'type' => 'text',
                'class' => 'search-icon',
                'id' => 'company'
            )
        ));
        $this->add(array(
            'name' => 'company_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'company_id'
            )
        ));
        $this->add(array(
            'name' => 'groups',
            'type' => 'Select',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'id' => 'groups',
                'class' => 'e1 select-w-320',
                'multiple' => 'multiple',
                'value' => ''
            )
        ));
//        $this->add(array(
//            'name' => 'gender',
//            'type' => 'radio',
//			
//            'options' => array(
//                'value_options' => array(
//                    'male' => 'Male',
//                    'female' => 'Female'
//                ),
//            ),
//            'attributes' => array(
//                'id' => 'gender',
//                'class' => 'e3 gender'
//            )
//        ));

        $this->add(array(
            'name' => 'gender',
            'type' => 'Select',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    'male' => 'Male',
                    'female' => 'Female'
                ),
            ),
            'attributes' => array(
                'id' => 'gender',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));

        $this->add(array(
            'name' => 'zip',
            'attributes' => array(
                'type' => 'text',
                'id' => 'zip '
            )
        ));

        $this->add(array(
            'name' => 'state',
            'attributes' => array(
                'type' => 'text',
                'id' => 'state'
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'state_select',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'state_select',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_usa',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'class' => 'checkbox',
                'id' => 'is_usa',
                'class' => 'e2',
                'value' => '1'
            )
        ));


        $this->add(array(
            'name' => 'country',
            'type' => 'text',
            'attributes' => array(
                'id' => 'country',
                'class' => 'search-icon',
                'value' => ''
            )
        ));
        $this->add(array(
            'name' => 'country_id',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 'country_id',
                'value' => ''
            )
        ));

        $this->add(array(
            'name' => 'omx_num',
            'type' => 'text',
            'attributes' => array(
                'id' => 'omx_num',
                'value' => ''
            )
        ));
        
        $this->add(array(
            'name' => 'searchbutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Search',
                'class' => 'search-btn',
                'id' => 'searchbutton',
            ),
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'saved_search',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'value' => '',
                'class' => 'e1 select-w-320',
                'id' => 'saved_search',
                'onChange' => 'getSavedSearchResult();'
            ),
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'membership_title',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'value' => '',
                'class' => 'e1 select-w-320',
                'id' => 'membership_title'
            ),
        ));
        
        // n - start
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'membership_cards_package_shipped',
            'options' => array(
                'value_options' => array(
                    '1' => 'All',
                    '2' => 'Pending',
                    '3' => 'Sent',
                ),
            ),
            'attributes' => array(
                'class' => 'e3',
                'value' => '1'
            )
         ));        
        
        $this->add(array(
            'name' => 'membership_cards_year',
            'type' => 'Select',
            'options' => array(
                'value_options' => array(
                 '' => 'Select'
                ),
            ),
            'attributes' => array(
                'id' => 'membership_cards_year',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));
        // n - end
    }

}