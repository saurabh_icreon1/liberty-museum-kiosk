<?php

/**
 * This page is used for update user security question
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This page is used for update security question
 * @author     Icreon Tech - DG
 */
class EditUserSecurityQuestionForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('editusersecurityquestion');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'contact_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'contact_id',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'security_question',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                ),
            ),
            'attributes' => array(
                'class' => '',
                'id' => 'security_question',
            ),
        ));
        $this->add(array(
            'type' => 'text',
            'name' => 'security_answer',
            'attributes' => array(
                'id' => 'security_answer',
                 'class' => 'width-200'
            ),
        ));

        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type' => 'password',
                'id' => 'password',
                 'class' => 'width-200'
            )
        ));

        $this->add(array(
            'name' => 'confirm_security_answer',
            'attributes' => array(
                'type' => 'text',
                'id' => 'confirm_security_answer',
                 'class' => 'width-200'
            )
        ));

        $this->add(array('type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 600
                )
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Update Security',
                'id' => 'submitbutton',
                'class' => 'button'
            ),
        ));
    }

}