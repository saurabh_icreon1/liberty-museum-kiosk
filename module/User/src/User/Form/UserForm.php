<?php

/**
 * This form is used for registration.
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Form;

use Zend\Form\Form;
use Zend\Form\Element\Captcha;
use Zend\Captcha\Image as CaptchaImage;

/**
 * This page is used for users form
 * @package    User
 * @author     Icreon Tech - DG
 */
class UserForm extends Form {

    protected $captchaAdapter;

    public function __construct($config = null) {
        $dirdata = './public';
        //pass captcha image options
        $captchaImage = new CaptchaImage(array(
                    'font' => $dirdata . '/fonts/opensans-bold-webfont.ttf',
                    'width' => 200,
                    'Expiration' => '100',
                    'height' => 80,
                    'WordLen' => 6,
                    'dotNoiseLevel' => 20,
                    'lineNoiseLevel' => 2)
        );
        $captchaImage->setImgDir($config['assets_upload_dir'] . 'captcha/');
        $captchaImage->setImgUrl($config['assets_url'] . 'captcha/');

        // we want to ignore the name passed
        parent::__construct('user');
        $this->setAttribute('method', 'post');

      $this->add(array(
            'name' => 'referal_url_path',
            'attributes' => array(
                'type' => 'hidden',
            )
        ));
           
        $this->add(array(
            'name' => 'first_name',
            'attributes' => array(
                'type' => 'text',
            )
        ));
        $this->add(array(
            'name' => 'middle_name',
            'attributes' => array(
                'type' => 'text',
            )
        ));
        $this->add(array(
            'name' => 'last_name',
            'attributes' => array(
                'type' => 'text',
            )
        ));
        $this->add(array(
            'name' => 'email_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email_id'
            )
        ));
        $this->add(array(
            'name' => 'email_id_conf',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email_id_conf'
            )
        ));
        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type' => 'Password',
                'id' => 'password',
            )
        ));
        $this->add(array(
            'name' => 'password_conf',
            'attributes' => array(
                'type' => 'Password',
                'id' => 'password_conf'
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'country_id',
            'options' => array(
                'value_options' => array(
                    '' => 'Select Country',
                    '2' => 'India',
                    '3' => 'USA'
                ),
            ),
            'attributes' => array(
                'id' => 'country_id',
                'value' => '' //set selected to 'blank'
            )
        ));


        $this->add(array(
            'name' => 'postal_code',
            'attributes' => array(
                'type' => 'text',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'security_question_id',
            'options' => array(
                'value_options' => array(
                    '' => 'Select Question',
                    '2' => 'What is your name?',
                    '3' => 'Where do you live?'
                ),
            ),
            'attributes' => array(
                'id' => 'security_question_id',
                'value' => '' //set selected to 'blank'
            )
        ));
        $this->add(array(
            'name' => 'security_answer',
            'attributes' => array(
                'type' => 'text',
            )
        ));
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'agree',
            'attributes' => array(
                'value' => '' //set checked to '1'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Captcha',
            'name' => 'captcha',
            'options' => array(
                'captcha' => $captchaImage,
            ),
            'attributes' => array(
                'id' => 'captcha'
            )
        ));

        $this->add(array('type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 600
                )
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Continue',
                'class' => 'button green',
                'id' => 'submitbutton',
            ),
        ));
        
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'email_not',
            'attributes' => array(
                'value' => '', //set checked to '1'
                'id'=>'email_not',
                //'onclick'=>'createUserName(this)'
            )
        ));
        
        $this->add(array(
            'name' => 'phone_digit',
            'attributes' => array(
                'type' => 'text',
                'id' => 'phone_digit'
            )
        ));
      $this->add(array(
            'name' => 'address_1',
            'attributes' => array(
                'type' => 'text',
                'id' => 'address_1'
            )
        ));
      $this->add(array(
            'name' => 'address_2',
            'attributes' => array(
                'type' => 'text',
                'id' => 'address_2'
            )
        ));
      $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'country_id_username',
            'options' => array(
                'value_options' => array(
                    '' => 'Select Country',
                ),
            ),
            'attributes' => array(
                'id' => 'country_id_username',
                'value' => '' //set selected to 'blank'
            )
        ));
       $this->add(array(
            'name' => 'city',
            'attributes' => array(
                'type' => 'text',
                'id' => 'city'
            )
        ));
       $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'state_id',
            'options' => array(
                'value_options' => array(
                    '' => 'Select Country',
                ),
            ),
            'attributes' => array(
                'id' => 'state_id',
                'value' => '', //set selected to 'blank'
            )
        ));
       
       $this->add(array(
            'name' => 'postal',
            'attributes' => array(
                'type' => 'text',
                'id' => 'postal'
            )
        ));
        $this->add(array(
            'name' => 'username',
            'attributes' => array(
                'type' => 'text',
                'id' => 'username'
            )
        ));
    }

}