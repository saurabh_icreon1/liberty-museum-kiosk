<?php

/**
 * This form is used to get list of Group for Rule
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This class is used to get list of Group for Rule
 * @package    User
 * @author     Icreon Tech - DG
 */
class RuleGroupForm extends Form {

    public function __construct($name = null) {

        /* we want to ignore the name passed */
        parent::__construct('rulegroup');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'group_id_name',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'id' => 'group_id_name',
                'class' => 'e1',
                'value' => ''
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'group_id_address',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'id' => 'group_id_address',
                'class' => 'e1',
                'value' => ''
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'group_id_phone',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'id' => 'group_id_phone',
                'class' => 'e1',
                'value' => ''
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'group_id_last_name_address',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'id' => 'group_id_last_name_address',
                'class' => 'e1',
                'value' => ''
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'group_id_last_name_phone',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'id' => 'group_id_last_name_phone',
                'class' => 'e1',
                'value' => ''
            ),
        ));
    }

}

