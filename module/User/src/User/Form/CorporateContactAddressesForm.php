<?php

/**
 * This form is used for adddress section in corporate form
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This class is used for adddress section in corporate form
 * @package    User
 * @author     Icreon Tech - AP
 */
class CorporateContactAddressesForm extends Form {

    public function __construct($name = null) {
        /* we want to ignore the name passed */
        parent::__construct('corporate_street_address');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'corporate_addressinfo_location_type[]',
            'options' => array(
                'value_options' => array(
                    '0' => 'Home',
                    '1' => 'Work',
                    '2' => 'Vacation',
                    '3' => 'Web'
                ),
            ),
            'attributes' => array(
                'id' => 'corporate_addressinfo_location_type',
                'class' => 'e1',
            )
        ));
        $this->add(array(
            'type' => 'MultiCheckbox',
            'name' => 'corporate_addressinfo_location[]',
            'options' => array(
                'value_options' => array(
                    '1' => 'Primary Location',
                    '2' => 'Billing Location',
                    '3' => 'Shipping Location'
                ),
            ),
            'attributes' => array(
                'value' => '', //set checked to '1'
                'class' => 'e4 corporate_addressinfo_location',
            )
        ));
        
        
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'corporate_addressinfo_location_primary[]',
            'options' => array(
                'value_options' => array(
                    '1' => 'Primary Location',
                ),
            ),
            'attributes' => array(
                'value' => '', /* set checked to '1' */
                'class' => 'address-checkbox',
                'id' => 'corporate_addressinfo_location_primary0',
                'onclick' => 'corporateAddressTypePrimary(this.id,0);'
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'corporate_addressinfo_location_billing[]',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'options' => array(
                'use_hidden_element' => false
            ),
            'attributes' => array(
                'value' => '', /* set checked to '1' */
                'class' => 'address-checkbox',
                'id' => 'corporate_addressinfo_location_billing0',
                'onchange' => 'corporateAddressTypeBilling(this.id,0);'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'corporate_addressinfo_location_shipping[]',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'value' => '', /* set checked to '1' */
                'class' => 'address-checkbox',
                'id' => 'corporate_addressinfo_location_shipping0'
            ),
            'options' => array(
                'use_hidden_element' => false
            ),
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'corporate_primary_location0',
            'attributes' => array(
                'value' => '', /* set checked to '1' */
                'class' => 'e4 addressinfo_location',
                'id' => 'corporate_primary_location0'
            )
        ));
        
        $this->add(array(
            'type' => 'hidden',
            'name' => 'corporate_primary_location_count',
            'attributes' => array(
                'value' => '1',
                'id' => 'corporate_primary_location_count'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'corporate_addressinfo_another_contact_address[]',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                //'onClick' => 'loadIndividualContact(this.id);',
                'id' => 'corporate_addressinfo_another_contact_address',
                'class' => 'address-checkbox'
            ),
        ));
        $this->add(array(
            'name' => 'corporate_addressinfo_first_name[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'corporate_addressinfo_first_name'
            )
        )); 
        $this->add(array(
            'name' => 'corporate_addressinfo_last_name[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'corporate_addressinfo_last_name'
            )
        )); 
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'corporate_addressinfo_title[]',

            'attributes' => array(
                'class' => 'e1 select-w-320',
                'id' => 'corporate_addressinfo_title',
                'value' => '' //set selected to 'blank'
            )
        ));        

        $this->add(array(
            'name' => 'corporate_addressinfo_street_address_1[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'corporate_addressinfo_street_address_1'
            )
        ));
        $this->add(array(
            'name' => 'corporate_addressinfo_street_address_2[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'corporate_addressinfo_street_address_2',
            )
        ));
        $this->add(array(
            'name' => 'corporate_addressinfo_street_address_3[]',
            'attributes' => array(
                'type' => 'text',
            )
        ));
        $this->add(array(
            'name' => 'corporate_addressinfo_city[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'corporate_addressinfo_city',
            )
        ));
        $this->add(array(
            'type' => 'text',
            'name' => 'corporate_addressinfo_state[]',
            'attributes' => array(
                'id' => 'corporate_addressinfo_state_0',
                'value' => '' //set selected to 'blank'
            )
        ));
        $this->add(array(
            'name' => 'corporate_addressinfo_zip[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'corporate_addressinfo_zip',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'corporate_addressinfo_country[]',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'class' => 'e1',
                'onchange' => "selectUsaState(this.value,this.id,'corporate')",
                'id' => '0',
                'value' => '' //set selected to 'blank'
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'corporate_address_state_id[]',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'id' => 'corporate_address_state_id_0',
                'onchange' => "assignStateValue(this.value,this.id,'corporate')",
                'class' => 'e1',
                'value' => '' /* set selected to 'blank' */
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'corporate_address_state_po[]',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'id' => 'corporate_address_state_po_0',
                'onchange' => "assignStateValue(this.value,this.id,'corporate')",
                'class' => 'e1',
                'value' => '' /* set selected to 'blank' */
            )
        ));
	$this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'corporate_address_state_apo[]',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'id' => 'corporate_address_state_apo_0',
                'onchange' => "assignStateValue(this.value,this.id,'corporate')",
                'class' => 'e1',
                'value' => '' /* set selected to 'blank' */
            )
        ));
		
	$this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'corporate_address_country_id[]',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'id' => 'corporate_address_country_id_0',
                'class' => 'e1',
                'value' => '' /* set selected to 'blank' */
            )
        ));
        
        $this->add(array(
            'name' => 'corporate_addressinfo_search_contact[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'corporate_addressinfo_search_contact',
                'class' => "search-icon"
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'corporate_apo_po_state',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'corporate_apo_po_state',
                'class' => 'e1',
            )
        ));
		
	$this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'corporate_is_apo_po[]',
            'options' => array(
                "value_options" => array(
                    '1' => 'Residential/Corporate',
                    '2' => 'PO',
                    '3' => 'APO'
                )
            ),
            'attributes' => array(
                'id' => 'corporate_is_apo_po',
                'class' => 'address-checkbox corporate_is_apo_po',
                'value' => '1',
		'onChange' => 'corporateSetApoPostateCountry(0,this.value);',
            )
        ));
    }

}