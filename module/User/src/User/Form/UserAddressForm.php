<?php

/**
 * This form is used for registration.
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Form;

use Zend\Form\Form;
use Zend\Form\Element\Captcha;
use Zend\Captcha\Image as CaptchaImage;

/**
 * This page is used for users form
 * @package    User
 * @author     Icreon Tech - DG
 */
class UserVisitRecordForm extends Form {

    protected $captchaAdapter;

    public function __construct($config = null) {
        // we want to ignore the name passed
        parent::__construct('useraddress');
        $this->setAttribute('method', 'post');

      $this->add(array(
            'name' => 'user_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'user_id'
            )
        ));
      $this->add(array(
            'name' => 'phone_digit',
            'attributes' => array(
                'type' => 'text',
                'id' => 'phone_digit'
            )
        ));
      $this->add(array(
            'name' => 'address_1',
            'attributes' => array(
                'type' => 'text',
                'id' => 'address_1'
            )
        ));
      $this->add(array(
            'name' => 'address_2',
            'attributes' => array(
                'type' => 'text',
                'id' => 'address_2'
            )
        ));
      $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'country_id',
            'options' => array(
                'value_options' => array(
                    '' => 'Select Country',
                ),
            ),
            'attributes' => array(
                'id' => 'country_id',
                'value' => '' //set selected to 'blank'
            )
        ));
       $this->add(array(
            'name' => 'city',
            'attributes' => array(
                'type' => 'text',
                'id' => 'city'
            )
        ));
       $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'state_id',
            'options' => array(
                'value_options' => array(
                    '' => 'Select Country',
                ),
            ),
            'attributes' => array(
                'id' => 'state_id',
                'value' => '' //set selected to 'blank'
            )
        ));
       
       $this->add(array(
            'name' => 'postal',
            'attributes' => array(
                'type' => 'text',
                'id' => 'postal'
            )
        ));
        $this->add(array(
            'name' => 'username',
            'attributes' => array(
                'type' => 'text',
                'id' => 'username'
            )
        ));

        $this->add(array('type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 600
                )
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Continue',
                'class' => 'button green',
                'id' => 'submitbutton',
            ),
        ));
    }

}