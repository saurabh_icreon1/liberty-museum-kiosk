<?php

/**
 * This file is used for delete crm users form
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This class is used for delete crm users form elements
 * @package    User
 * @author     Icreon Tech - AP
 */
class DeleteUserForm extends Form {

    public function __construct($param = null) {
        // we want to ignore the name passed
        parent::__construct('deleteCrmUser');
        $this->setAttribute('method', 'post');


        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'name',
                'class'=>'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'assigned_to_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'assigned_to_id',
            )
        ));
        $this->add(array(
            'name' => 'user_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'user_id',
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Cancel Accounts',
                'id' => 'yes_delete_user',
                'class' => 'save-btn'
            ),
        ));
        $this->add(array(
            'name' => 'remove',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Cancel',
                'id' => 'removePopup',
                'class' => 'cancel-btn m-l-20'
            ),
        ));
        $this->add(array('type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'options' => array(
                'corpcsrf_options' => array(
                    'timeout' => 600
                )
            )
        ));
    }

}