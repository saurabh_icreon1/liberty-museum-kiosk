<?php

/**
 * This page is used for update user security question
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This page is used for update security question
 * @author     Icreon Tech - DG
 */
class EditUserEmailInfoForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('edituseremailinfo');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'contact_id',
            'attributes' => array( 
                'type' => 'hidden',
                'id' => 'contact_id',
            )
        ));
        
        $this->add(array(
            'type' => 'text',
            'name' => 'email_id',
            'attributes' => array(
                'id' => 'email_id',
                 'class' => 'width-127'
            ),
        ));
        $this->add(array(
            'type' => 'text',
            'name' => 'email_id_conf',
            'attributes' => array(
                'id' => 'email_id_conf',
                 'class' => 'width-127'
            ),
        ));
        $this->add(array(
            'type' => 'text',
            'name' => 'username',
            'attributes' => array(
                'readonly'=>true,
                'id' => 'username',
                 'class' => 'width-127'
            ),
        ));
        
        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type' => 'password',
                'id' => 'password',
                 'class' => 'width-127'
            )
        ));
        
        $this->add(array(
            'name' => 'confirm_security_answer',
            'attributes' => array(
                'type' => 'text',
                'id' => 'confirm_security_answer',
                 'class' => 'width-127'
            )
        ));
        $this->add(array('type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 600
                )
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Update Security',
                'id' => 'submitbutton',
                'class' => 'button'
            ),
        ));
    }

}