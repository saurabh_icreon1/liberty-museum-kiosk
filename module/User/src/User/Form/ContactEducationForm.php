<?php

/**
 * This form is used for create education in contact.
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This class is used for create education in contact.
 * @author     Icreon Tech - AP
 */
class ContactEducationForm extends Form {

    public function __construct($name = null) {
        /* we want to ignore the name passed */
        parent::__construct('education');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'edu_degree_level[]',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'id' => 'edu_degree_level',
                'class' => 'e1',
                'value' => '' /* set selected to 'blank' */
            )
        ));
        $this->add(array(
            'name' => 'edu_institution[]',
            'attributes' => array(
                'type' => 'text',
            )
        ));
        $this->add(array(
            'name' => 'edu_attended_year[]',
            'attributes' => array(
                'type' => 'text',
            )
        ));
        $this->add(array(
            'name' => 'edu_major[]',
            'attributes' => array(
                'type' => 'text',
            )
        ));
        $this->add(array(
            'name' => 'edu_gpa[]',
            'attributes' => array(
                'type' => 'text',
            )
        ));
    }

}