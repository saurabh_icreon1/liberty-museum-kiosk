<?php

/**
 * This page is used for create crm users form
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This class is used for create crm users form elements
 * @package    User_CrmUserForm
 * @author     Icreon Tech - AP
 */
class CrmUserForm extends Form {

    public function __construct($param = null) {
        /* we want to ignore the name passed */
        parent::__construct('crm_user');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->add(array(
            'name' => 'first_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'first_name'
            )
        ));

        $this->add(array(
            'name' => 'last_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_name'
            )
        ));
        $this->add(array(
            'name' => 'phone_no',
            'attributes' => array(
                'type' => 'text',
                'id' => 'phone_no'
            )
        ));
        $this->add(array(
            'name' => 'address',
            'attributes' => array(
                'type' => 'text',
                'id' => 'address'
            )
        ));
        $this->add(array(
            'name' => 'city',
            'attributes' => array(
                'type' => 'text',
                'id' => 'city'
            )
        ));
        $this->add(array(
            'name' => 'state',
            'attributes' => array(
                'type' => 'text',
                'id' => 'state'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'state_select',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'state_select',
                'value' => '', //set selected to 'blank'
                'class' => 'e1 select-w-320'
            )
        ));
        $this->add(array(
            'name' => 'zip',
            'attributes' => array(
                'type' => 'text',
                'id' => 'zip'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'country_id',
            'options' => array(
                'value_options' => array(
                    '' => 'Select Country',
                ),
            ),
            'attributes' => array(
                'id' => 'country_id',
                'value' => '', //set selected to 'blank'
                'class' => 'e1 select-w-320',
                'onchange' => 'onChangeCountry(this.value)'
            )
        ));
        $this->add(array(
            'name' => 'crm_email_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'crm_email_id'
            )
        ));

        $this->add(array(
            'name' => 'password',
            'attributes' => array(
                'type' => 'Password',
                'id' => 'password',
            )
        ));
        $this->add(array(
            'name' => 'password_conf',
            'attributes' => array(
                'type' => 'Password',
                'id' => 'password_conf'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'status',
            'options' => array(
                'value_options' => array(
                    '0' => 'Blocked',
                    '1' => 'Active',
                ),
            ),
            'attributes' => array(
                'value' => '', //set checked to '1'
                'class' => 'e3'
            )
        ));


        $this->add(array(
            'type' => 'MultiCheckbox',
            'name' => 'role',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'class' => 'e4 role'
            ),
        ));

        $this->add(array(
            'name' => 'report_to',
            'attributes' => array(
                'type' => 'text',
                'id' => 'report_to',
                'class' => 'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'report_to_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'report_to_id'
            )
        ));
        $this->add(array(
            'name' => 'virtualface',
            'attributes' => array(
                'type' => 'file',
                'id' => 'virtualface'
            )
        ));
        $this->add(array(
            'name' => 'crm_user_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'crm_user_id'
            )
        ));
        $this->add(array(
            'name' => 'thumbnail_virtualface',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'thumbnail_virtualface'
            )
        ));
        $this->add(array(
            'name' => 'old_thumbnail_virtualface',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'old_thumbnail_virtualface'
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'id' => 'submitbutton',
                'class' => 'save-btn'
            ),
        ));
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Cancel',
                'id' => 'cancel',
                'class' => 'cancel-btn m-l-20',
                'onClick' => "javascript:window.location.href='/crm-users'",
            ),
        ));
        $this->add(array(
            'name' => 'remove',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Remove',
                'id' => 'remove',
                'class' => 'cancel-btn m-l-20',
            ),
        ));
         $this->add(array('type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 100
                )
            )
        ));
       
    }

}