<?php

/**
 * This form is used for create contact of type Individual.
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace User\Form;

use Zend\Form\Form;

/** This class is used for create form elements for contact of type Individual
 * @author     Icreon Tech - AP
 */
class ContactIndividualForm extends Form {

    public function __construct($name = null) {
        /* we want to ignore the name passed */
        parent::__construct('contact_individual');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'contact_type',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'contact_type',
                'value' => '1',
                'class' => 'e1 select-w-320',
                'onChange' => 'javascript:loadForm(this.id);',
            )
        ));
        $this->add(array(
            'name' => 'upload_profile_image',
            'attributes' => array(
                'type' => 'file',
                'value' => 'upload',
                'id' => 'upload_profile_image',
            ),
        ));
        $this->add(array(
            'name' => 'upload_profile_image_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'upload_profile_image_id',
            ),
        ));
        $this->add(array(
            'name' => 'address_primary_index',
            'attributes' => array(
                'type' => 'hidden',
                'value' => '0',
                'id' => 'address_primary_index',
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'title',
            'options' => array(
                'value_options' => array(
                    '0' => 'None',
                    '1' => 'Mr',
                    '2' => 'Mrs.',
                )
            ),
            'attributes' => array(
                'class' => 'e1 select-w-320',
                'id' => 'title',
                'value' => '' //set selected to 'blank'
            )
        ));

//        $this->add(array(
//            'type' => 'Zend\Form\Element\Select',
//            'name' => 'country_id',
//            'options' => array(
//                'value_options' => array(
//                    '' => 'Select'
//                ),
//            ),
//            'attributes' => array(
//                'class' => 'e1 select-w-320',
//                'id' => 'country_id',
//                'value' => '' //set selected to 'blank'
//            )
//        ));
//        
//        $this->add(array(
//            'name' => 'zip_code',
//            'attributes' => array(
//                'type' => 'text',
//                'id' => 'zip_code'
//            )
//        ));
        $this->add(array(
            'name' => 'first_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'first_name'
            )
        ));
        $this->add(array(
            'name' => 'middle_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'middle_name'
            )
        ));
        $this->add(array(
            'name' => 'last_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_name'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'suffix',
            'options' => array(
                'value_options' => array(
                    '0' => 'None',
                    '1' => 'jr.',
                    '2' => 'sr.'
                ),
            ),
            'attributes' => array(
                'class' => 'e1 select-w-320',
                'id' => 'suffix',
                'value' => '' //set selected to 'blank'
            )
        ));
        $this->add(array(
            'name' => 'email_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email_id',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'bulk_mail',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'class' => 'e2',
                'id' => 'bulk_mail'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'no_email_address',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'class' => 'e2',
                'id' => 'no_email_address',
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'source',
            'options' => array(
                'value_options' => array(
                    '0' => 'Any',
                    '1' => 'Direct',
                    '2' => 'Ellis',
                    '3' => 'Web'
                ),
            ),
            'attributes' => array(
                'class' => 'e1 select-w-320',
                'id' => 'source',
                'value' => '' //set selected to 'blank'
            )
        ));
        $this->add(array(
            'name' => 'afihc',
            'attributes' => array(
                'type' => 'text',
                'id' => 'afihc'
            )
        ));
        $this->add(array(
            'name' => 'alt_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'alt_name',
            )
        ));
        $this->add(array(
            'name' => 'alt_email',
            'attributes' => array(
                'type' => 'text',
                'id' => 'alt_email',
            )
        ));
        $this->add(array(
            'name' => 'alt_phoneno',
            'attributes' => array(
                'type' => 'text',
                'id' => 'alt_phoneno',
            )
        ));
        $this->add(array(
            'name' => 'alt_relationship',
            'attributes' => array(
                'type' => 'text',
                'id' => 'alt_relationship',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'communicationpref_email_greetings',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    '1' => 'Dear {contact.first_name}',
                    '2' => 'Dear {contact.individual_prefix} {contact.first_name}{contact.last_name}',
                    '3' => 'Dear {contact.individual_prefix}{contact.last_name}',
                    'Customized' => 'Customized'
                ),
            ),
            'attributes' => array(
                'class' => 'e1 select-w-320',
                'id' => 'communicationpref_email_greetings',
                'onChange' => 'show_custome_box(this.id,"custome_email");'
            ),
        ));
        $this->add(array(
            'name' => 'communicationpref_custome_email_greetings',
            'attributes' => array(
                'type' => 'text',
                'id' => 'communicationpref_custome_email_greetings'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'communicationpref_postal_greetings',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    '1' => 'Dear {contact.first_name}',
                    '2' => 'Dear {contact.individual_prefix} {contact.first_name}{contact.last_name}',
                    '3' => 'Dear {contact.individual_prefix}{contact.last_name}',
                    'Customized' => 'Customized'
                ),
            ),
            'attributes' => array(
                'class' => 'e1 select-w-320',
                'id' => 'communicationpref_postal_greetings',
                'onChange' => 'show_custome_box(this.id,"custome_postal");'
            ),
        ));
        $this->add(array(
            'name' => 'communicationpref_custome_postal_greetings',
            'attributes' => array(
                'type' => 'text',
                'id' => 'communicationpref_custome_postal_greetings'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'communicationpref_address',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    '1' => 'Dear {contact.first_name}',
                    '2' => 'Dear {contact.individual_prefix} {contact.first_name}{contact.last_name}',
                    '3' => 'Dear {contact.individual_prefix}{contact.last_name}',
                    'Customized' => 'Customized'
                ),
            ),
            'attributes' => array(
                'class' => 'e1 select-w-320',
                'id' => 'communicationpref_address',
                'onChange' => 'show_custome_box(this.id,"custome_address");'
            ),
        ));
        $this->add(array(
            'name' => 'communicationpref_custome_address_greetings',
            'attributes' => array(
                'type' => 'text',
                'id' => 'communicationpref_custome_address_greetings'
            ),
        ));
        $this->add(array(
            'type' => 'MultiCheckbox',
            'name' => 'communicationpref_privacy',
            'options' => array(
                'value_options' => array(
                    '1' => 'Do not phone',
                    '2' => 'Do not email',
                    '3' => 'Do not mail',
                    '4' => 'Do not SMS',
                    '5' => 'Do not trade',
                    '6' => 'Once a year',
                ),
            ),
            'attributes' => array(
                'value' => '', //set checked to '1'
                'class' => 'e4 communicationpref_privacy'
            )
        ));
        $this->add(array(
            'type' => 'MultiCheckbox',
            'name' => 'communicationpref_preferred_method',
            'options' => array(
                'value_options' => array(
                    '1' => 'Phone',
                    '2' => 'Email',
                    '3' => 'Post Mail',
                    '4' => 'SMS',
                    '5' => 'Fax',
                ),
            ),
            'attributes' => array(
                'value' => '', //set checked to '1'
                'class' => 'e4 communicationpref_preferred_method'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'demographics_gender',
            'options' => array(
                'value_options' => array(
                    '2' => 'Female',
                    '1' => 'Male',
                ),
            ),
            'attributes' => array(
                'class' => 'e3',
                'value' => '', //set checked to '1'
                'tag' => 'div'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'demographics_marital_status',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    '1' => 'Single',
                    '2' => 'Married',
                    '3' => 'Separated',
                    '4' => 'Divorced'
                ),
            ),
            'attributes' => array(
                'class' => 'e1 select-w-320',
                'id' => 'demographics_marital_status',
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'demographics_age_limit',
            'attributes' => array(
                'value' => '', //set checked to '1'
                'class' => 'e2',
                'id' => 'demographics_age_limit'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'demographics_income_range',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    '0' => '<50K',
                    '1' => '51K-100K',
                    '2' => '100K-200K',
                    '3' => '201K-500K',
                    '4' => '501K +'
                ),
            ),
            'attributes' => array(
                'class' => 'e1 select-w-320',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'demographics_nationality',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'class' => 'e1 select-w-320',
                'id' => 'demographics_nationality',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'demographics_ethnicity',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'class' => 'e1 select-w-320',
                'id' => 'demographics_ethnicity',
            )
        ));
        $this->add(array(
            'name' => 'tagsandgroups_company_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'tagsandgroups_company_name',
                'class' => 'search-icon',
            ),
        ));
        $this->add(array(
            'name' => 'tag_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'tag_id',
            ),
        ));
        $this->add(array(
            'type' => 'MultiCheckbox',
            'name' => 'tagsandgroups_groups',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'value' => '', //set checked to '1'
                'class' => 'e4 tagsandgroups_groups',
            )
        ));
        
        
        $this->add(array(
            'type' => 'Select',
            'name' => 'tagsandgroups_groups_from',
            'options' => array(
                'value_options' => array(),
            ),
            'attributes' => array(
                'id' => 'tagsandgroups_groups_from',
                'multiple' => 'multiple',
                'value' => ''                
            )
        ));
        
        $this->add(array(
            'type' => 'Select',
            'name' => 'tagsandgroups_groups_to',
            'options' => array(
                'value_options' => array(),
            ),
            'attributes' => array(
                'id' => 'tagsandgroups_groups_to',
                'multiple' => 'multiple',
                'value' => ''
            )
        ));
        
        $this->add(array(
            'name' => 'addbutton',
            'attributes' => array(
                'type' => 'button',
                'value' => 'ADD >>',
                'id' => 'addbutton',
                'class' => 'save-btn m-l-10'
            ),
        ));

        $this->add(array(
            'name' => 'removebutton',
            'attributes' => array(
                'type' => 'button',
                'value' => '<< REMOVE',
                'id' => 'removebutton',
                'class' => 'cancel-btn m-l-10'
            ),
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'webaccesss_enabled_login',
            'attributes' => array(
                'value' => '', //set checked to '1'
                'class' => 'e2',
                'id' => 'webaccesss_enabled_login'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'webaccesss_security_question',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                ),
            ),
            'attributes' => array(
                'class' => 'e1 select-w-320',
                'id' => 'webaccesss_security_question',
            ),
        ));
        $this->add(array(
            'type' => 'text',
            'name' => 'webaccesss_security_answer',
            'attributes' => array(
                'id' => 'webaccesss_security_answer',
            ),
        ));
        $this->add(array(
            'name' => 'publicdocs_description',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'publicdocs_description',
                'class' => 'width-90'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'notes_private',
            'attributes' => array(
                'value' => '', //set checked to '1'
                'class' => 'e2',
                'id' => 'notes_private'
            )
        ));
        $this->add(array(
            'name' => 'notes',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'notes',
                'class' => 'width-90'
            ),
        ));
        $this->add(array(
            'name' => 'addressinfo_search_contact[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'addressinfo_search_contact',
                'class' => 'search-icon'
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'id' => 'submitbutton',
                'class' => 'save-btn'
            ),
        ));
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'value' => 'cancel',
                'id' => 'cancel',
                'onclick' => 'backToSearch()',
                'class' => 'cancel-btn m-l-20'
            ),
        ));
        $this->add(array(
            'name' => 'contact_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'contact_id',
            ),
        ));
    }

}