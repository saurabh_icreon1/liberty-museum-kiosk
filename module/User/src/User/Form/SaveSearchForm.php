<?php

/**
 * This form is used to save search contact for admin
 * @package    User
 * @author     Icreon Tech - AS
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This page is used for save search form
 * @author     Icreon Tech - DG
 */
class SaveSearchForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('save_search');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'search_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'search_id'
            )
        ));
        $this->add(array(
            'name' => 'search_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'search_name'
            )
        ));

        $this->add(array(
            'name' => 'savebutton',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Save',
                'id' => 'savebutton',
                'class' => 'save-btn m-l-5',
            ),
        ));
        $this->add(array(
            'name' => 'deletebutton',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Delete',
                'id' => 'deletebutton',
                'style' => 'display:none;',
                'class' => 'save-btn m-l-5',
            ),
        ));
    }

}