<?php

/**
 * This form is used for AddTokenForm 
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This class is used for create AddTokenForm
 * @author     Icreon Tech - DG
 */
class AddTokenForm extends Form {

    public function __construct($name = null) {
        /* we want to ignore the name passed */
        parent::__construct('add_token');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'first_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'first_name',
                'style' => 'width:350px; margin-right:5px;'
            )
        ));
//        $this->add(array(
//            'name' => 'middle_name',
//            'attributes' => array(
//                'type' => 'text',
//                'id' => 'middle_name',
//                'style' => 'width:88px; margin-right:5px;'
//            )
//        ));
//        $this->add(array(
//            'name' => 'last_name',
//            'attributes' => array(
//                'type' => 'text',
//                'id' => 'last_name',
//                'style' => 'width:110px;'
//            )
//        ));

//        $this->add(array(
//            'type' => 'Zend\Form\Element\Select',
//            'name' => 'card_type',
//            'options' => array(
//                'value_options' => array(
//                    ' ' => 'Select Card',
//                    'JCB' => 'JCB',
//                    'Discover' => 'Discover',
//                    'American Express' => 'American Express',
//                    'Visa' => 'Visa',
//                    'Diners Club' => 'Diners Club'
//                ),
//            ),
//            'attributes' => array(
//                'id' => 'card_type'                
//            )
//        ));

        $this->add(array(
            'name' => 'card_number',
            'attributes' => array(
                'type' => 'text',
                'id' => 'card_number'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'expiration_month',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                ),
            ),
            'attributes' => array(
                'id' => 'expiration_month',
               
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'expiration_year',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'id' => 'expiration_year',
                
            )
        ));
        $this->add(array(
            'name' => 'card_ccv',
            'attributes' => array(
                'type' => 'password',
                'id' => 'card_ccv'
            )
        ));
        $this->add(array(
            'name' => 'billing_address_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'billing_address_id'
            )
        ));
        $this->add(array('type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf'
//            'options' => array(
//                'csrf_options' => array(
//                    'timeout' => 600
//                )
//            )
        ));
        $this->add(array(
            'name' => 'submitbutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'id' => 'submitbutton',
                'class' => 'button'
            ),
        ));
        
        $this->add(array(
            'name' => 'cancelbutton',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Cancel',
                'id' => 'cancelbutton',
                'class' => 'button'
            ),
        ));
    }

}