<?php

/**
 * This form is used for create afihc 
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This form is used for create afihc 
 * @author     Icreon Tech - DG
 */
class ContactAfihcForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('afihc');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'afihc[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'afihc',
                'class' => 'phone_no'
            )
        ));
    }

}