<?php

/**
 * This form is used for edit demograpohic
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Form;

use Zend\Form\Form;

/** This class is used for edit user demographic
 * @package    User
 * @author     Icreon Tech - DG
 */
class UserDemographicForm extends Form {

    public function __construct($name = null) {
        /* we want to ignore the name passed */
        parent::__construct('user_demographic');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'user_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'user_id',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'demographics_gender',
            'options' => array(
                'value_options' => array(
                    '2' => 'Female',
                    '1' => 'Male',
                ),
            ),
            'attributes' => array(                
                'value' => ''
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'demographics_marital_status',
            'options' => array(
                'value_options' => array(
                    ' ' => 'Select',
                    '1' => 'Single',
                    '2' => 'Married',
                    '3' => 'Separated',
                    '4' => 'Divorced'
                ),
            ),
            'attributes' => array(
                'id' => 'demographics_marital_status',
            ),
        ));
    
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'have_child',
            'options' => array(
                'value_options' => array(
                    '1' => 'Yes',
                    '0' => 'No',
                ),
            ),
            'attributes' => array(                
                'value' => '',
                'id' => 'have_child'
                
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'demographics_income_range',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'class' => '',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'demographics_nationality',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'class' => '',
                'id' => 'demographics_nationality',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'demographics_ethnicity',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
            ),
            'attributes' => array(
                'class' => '',
                'id' => 'demographics_ethnicity',
            )
        ));
        
        $this->add(array(
            'name' => 'submitbutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'id' => 'submitbutton',
                'class' => 'button'
            ),
        ));
    }

}