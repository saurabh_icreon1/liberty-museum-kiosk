<?php

/**
 * This file is used for ContactStatusForm
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This form is used for create jobhistory  form elements
 * @package    User
 * @author     Icreon Tech - DG
 */
class ContactStatusForm extends Form {

    public function __construct($name = null) {
        parent::__construct('contactstatus');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'contact_status',
            'options' => array(
                'value_options' => array(
                    '1' => 'Active',
                    '0' => 'Inactive',
                    '2' => 'Pending Email Verification'
                ),
            ),
            'attributes' => array(
                'id' => 'contact_status',
                'value' => '',
                'class' => 'e1',
                'onchange' => 'changeStatus()'
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'reason',
            'options' => array(
                'value_options' => array(
                    '0' => 'Select',                    
                ),
            ),
            'attributes' => array(
                'id' => 'reason',
                'value' => '',
                'class' => 'e1',
                'onchange' => 'changeReason()'
            )
        ));
        
        $this->add(array(
            'type' => 'textarea',
            'name' => 'reason_other',
            'attributes' => array(
                'id' => 'reason_other',
                'class' => 'width-60'
            ),
        ));
    }

}