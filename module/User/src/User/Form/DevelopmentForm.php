<?php

/**
 * This form is used for development
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This class is used for development form
 * @package    User
 * @author     Icreon Tech - AP
 */
class DevelopmentForm extends Form {

    public function __construct($name = null) {

        /* we want to ignore the name passed */
        parent::__construct('development');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->add(array(
            'name' => 'user_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'user_id'
            ),
        ));
        $this->add(array(
            'name' => 'import_file',
            'attributes' => array(
                'type' => 'file',
                'id' => 'import_file'
            ),
        ));

        $this->add(array(
            'name' => 'total_given_amount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'total_given_amount',
            )
        ));
        $this->add(array(
            'name' => 'total_no_of_gifts',
            'attributes' => array(
                'type' => 'text',
                'id' => 'total_no_of_gifts',
            )
        ));
        $this->add(array(
            'name' => 'last_gift_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_gift_date',
				'class'=>'width-155 cal-icon'
            )
        ));
        $this->add(array(
            'name' => 'last_gift_amount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_gift_amount',
            )
        ));
        $this->add(array(
            'name' => 'largest_gift_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'largest_gift_date',
			    'class'=>'width-155 cal-icon'
            )
        ));
        $this->add(array(
            'name' => 'largest_gift_amount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'largest_gift_amount'
            )
        ));
        $this->add(array(
            'name' => 'total_income',
            'attributes' => array(
                'type' => 'text',
                'id' => 'total_income'
            )
        ));
        $this->add(array(
            'name' => 'total_real_estate_value',
            'attributes' => array(
                'type' => 'text',
                'id' => 'total_real_estate_value'
            )
        ));
        $this->add(array(
            'name' => 'total_stock_value',
            'attributes' => array(
                'type' => 'text',
                'id' => 'total_stock_value',
            )
        ));
        $this->add(array(
            'name' => 'total_pension_value',
            'attributes' => array(
                'type' => 'text',
                'id' => 'total_pension_value'
            )
        ));
        $this->add(array(
            'name' => 'total_political_non_profit_giving_amount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'total_political_non_profit_giving_amount'
            )
        ));
        $this->add(array(
            'name' => 'income_portion_of_egc_calculation',
            'attributes' => array(
                'type' => 'text',
                'id' => 'income_portion_of_egc_calculation',
            )
        ));
        $this->add(array(
            'name' => 'pol_non_profit_giving_portion_of_egc_calc',
            'attributes' => array(
                'type' => 'text',
                'id' => 'pol_non_profit_giving_portion_of_egc_calc'
            )
        ));
        $this->add(array(
            'name' => 'rating_summary_giving_capacity_rating',
            'attributes' => array(
                'type' => 'text',
                'id' => 'rating_summary_giving_capacity_rating'
            )
        ));
        $this->add(array(
            'name' => 'rating_summary_giving_capacity_range',
            'attributes' => array(
                'type' => 'text',
                'id' => 'rating_summary_giving_capacity_range'
            )
        ));
        $this->add(array(
            'name' => 'estimated_giving_capacity',
            'attributes' => array(
                'type' => 'text',
                'id' => 'estimated_giving_capacity',
            )
        ));
        $this->add(array(
            'name' => 'propensity_to_give_score',
            'attributes' => array(
                'id' => 'propensity_to_give_score',
                'type' => 'text',
            )
        ));
        $this->add(array(
            'name' => 'propensity_to_give_score_part_2',
            'attributes' => array(
                'type' => 'text',
                'id' => 'propensity_to_give_score_part_2'
            )
        ));
        $this->add(array(
            'name' => 'combined_propensity_to_give_score',
            'attributes' => array(
                'type' => 'text',
                'id' => 'combined_propensity_to_give_score',
            )
        ));
        $this->add(array(
            'name' => 'planned_giving_bequest',
            'attributes' => array(
                'type' => 'text',
                'id' => 'planned_giving_bequest',
            )
        ));

        $this->add(array(
            'name' => 'planned_giving_annuity',
            'attributes' => array(
                'id' => 'planned_giving_annuity',
                'type' => 'text'
            )
        ));
        $this->add(array(
            'name' => 'planned_giving_trust',
            'attributes' => array(
                'type' => 'text',
                'id' => 'planned_giving_trust'
            )
        ));

        $this->add(array(
            'name' => 'rating_summary_influence',
            'attributes' => array(
                'type' => 'text',
                'id' => 'rating_summary_influence'
            )
        ));
        $this->add(array(
            'name' => 'rating_summary_inclination',
            'attributes' => array(
                'type' => 'text',
                'id' => 'rating_summary_inclination',
            )
        ));
        $this->add(array(
            'name' => 'total_quality_of_match',
            'attributes' => array(
                'type' => 'text',
                'id' => 'total_quality_of_match',
            )
        ));
        $this->add(array(
            'name' => 'charitable_donations_of_qom',
            'attributes' => array(
                'type' => 'text',
                'id' => 'charitable_donations_of_qom',
            )
        ));

        $this->add(array(
            'name' => 'trustees_qom',
            'attributes' => array(
                'type' => 'text',
                'id' => 'trustees_qom'
            )
        ));
        $this->add(array(
            'name' => 'guidestar_directors_qom',
            'attributes' => array(
                'type' => 'text',
                'id' => 'guidestar_directors_qom',
            )
        ));
        $this->add(array(
            'name' => 'guidestar_foundations_qom',
            'attributes' => array(
                'type' => 'text',
                'id' => 'guidestar_foundations_qom',
            )
        ));
        $this->add(array(
            'name' => 'hoover_business_information_qom',
            'attributes' => array(
                'type' => 'text',
                'id' => 'hoover_business_information_qom',
            )
        ));
        $this->add(array(
            'name' => 'household_profile_qom',
            'attributes' => array(
                'type' => 'text',
                'id' => 'household_profile_qom',
            )
        ));
        $this->add(array(
            'name' => 'lexis_nexis_qom',
            'attributes' => array(
                'type' => 'text',
                'id' => 'lexis_nexis_qom',
            )
        ));
        $this->add(array(
            'name' => 'major_donor_qom',
            'attributes' => array(
                'type' => 'text',
                'id' => 'major_donor_qom',
            )
        ));
        $this->add(array(
            'name' => 'philanthropic_donations_qom',
            'attributes' => array(
                'type' => 'text',
                'id' => 'philanthropic_donations_qom',
            )
        ));
        $this->add(array(
            'name' => 'volunteers_and_directors_qom',
            'attributes' => array(
                'type' => 'text',
                'id' => 'volunteers_and_directors_qom',
            )
        ));
        $this->add(array(
            'name' => 'wealth_id_securities_qom',
            'attributes' => array(
                'type' => 'text',
                'id' => 'wealth_id_securities_qom',
            )
        ));
        $this->add(array(
            'name' => 'bio_information',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'bio_information',
                'class' => 'width-90'
            )
        ));
        $this->add(array(
            'name' => 'date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'date',
            )
        ));
        $this->add(array(
            'name' => 'amount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'amount',
            )
        ));
        $this->add(array(
            'name' => 'transaction',
            'attributes' => array(
                'type' => 'text',
                'id' => 'transaction',
            )
        ));
        $this->add(array(
            'name' => 'campaign_code',
            'attributes' => array(
                'type' => 'text',
                'id' => 'campaign_code',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'sort_by',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    '1' => 'Latest Transaction Date',
                ),
            ),
            'attributes' => array(
                'id' => 'sort_by',
                'value' => '' //set selected to 'blank'
            ),
        ));
        $this->add(array(
            'type' => 'textarea',
            'name' => 'description',
            'attributes' => array(
                'id' => 'description',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'id' => 'submitbutton',
                'class' => 'save-btn'
            ),
        ));
          $this->add(array('type' => 'Zend\Form\Element\Csrf',
            'name' => 'corpcsrf',
            'options' => array(
                'corpcsrf_options' => array(
                    'timeout' => 600
                )
            )
        ));
    }

}