<?php

/**
 * This form is used for fof search
 * @category   Zend
 * @package    User_ActivationForm
 * @version    2.2
 * @author     Icreon Tech - NS
 */

namespace User\Form;

use Zend\Form\Form;

class FofSearchForm extends Form {

    public function __construct($name = null) {

        parent::__construct('search-Fof');
        $this->setAttribute('id', 'search_fof');
        $this->setAttribute('name', 'search_fof');
        $this->setAttribute('method', 'post');


        $this->add(array(
                'name' => 'search_name',
                'attributes' => array(
                        'type' => 'text',
                        'id' => 'search_name'
                )
        ));	  

        $this->add(array(
                      'name' => 'submitsearch',
                      'attributes' => array(
                              'type' => 'submit',
                              'value' => 'Search',
                              'id' => 'submitsearch',
                              'class' => 'button'
                      ),
       )); 
    }

}