<?php

/**
 * This page is used for crm user login
 * @package    User_Login_LoginFormCrm
 * @author     Icreon Tech - DT
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This form is used for crm user login
 * @package    User_Login_LoginFormCrm
 * @author     Icreon Tech - DT
 */
class LoginFormCrm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('login_crm');
        $this->setAttribute('method', 'post');


        $this->add(array(
            'name' => 'user_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'user_name',
                'value' => 'Email Address',
                'onfocus' => 'javascript:if(this.value==this.defaultValue){this.value=""}',
                'onblur' => 'javascript:if(this.value==""){this.value=this.defaultValue}'
            ),
        ));
        $this->add(array(
            'type' => 'text',
            'name' => 'password',
            'attributes' => array(
                'id' => 'password',
                'value' => 'Password'
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'LOGIN',
                'id' => 'submitbutton',
                'class' => 'button'
            ),
        ));
    }

}