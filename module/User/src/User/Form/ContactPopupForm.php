<?php

/**
 * This page is used for create new contact from popup window
 * @package    User_Contact_ContactPopupForm
 * @author     Icreon Tech - DT
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This class is used for create new contact from popup window
 * @package    ContactPopupForm
 * @author     Icreon Tech - DT
 */
class ContactPopupForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('contactpopup');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'title',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'title',
                'class' => 'e1'
            )
        ));
        $this->add(array(
            'name' => 'first_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'first_name'
            ),
        ));
        $this->add(array(
            'name' => 'middle_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'middle_name'
            ),
        ));
        $this->add(array(
            'name' => 'last_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_name'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'suffix',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'suffix',
                'class' => 'e1'
            )
        ));
        $this->add(array(
            'name' => 'email_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email_id'
            ),
        ));
        $this->add(array(
            'name' => 'savecontact',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'savecontact',
                'class' => 'save-btn',
                'value' => 'Save'
            )
        ));
    }

}