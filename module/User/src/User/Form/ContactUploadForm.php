<?php

/**
 * This file is used for file upload
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This Class is used for file upload
 * @package  User
 * @author   Icreon Tech - AP
 */
class ContactUploadForm extends Form {

    public function __construct($name = null) {
        /* we want to ignore the name passed */
        parent::__construct('upload');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->add(array(
            'name' => 'publicdocs_upload0',
            'attributes' => array(
                'type' => 'file',
                'class' => 'fileUploadAddMore',
                'id' => 'publicdocs_upload0',
            ),
        ));
        $this->add(array(
            'name' => 'publicdocs_upload_file0',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'publicdocs_upload_file0',
            ),
        ));
        $this->add(array(
            'name' => 'publicdocs_upload_count',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'publicdocs_upload_count',
                'value' => '0',
            ),
        ));
//        $this->add(array(
//            'name' => 'upload[]',
//            'attributes' => array(
//                'type' => 'button',
//                'value' => 'upload',
//                'id' => 'upload',
//                'class' => 'cancel-btn m-l-20'
//            ),
//        ));
    }

}