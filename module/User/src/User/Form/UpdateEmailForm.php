<?php

/**
 * This form is used for updat email
 * @package    User
 * @author     Icreon Tech - DG
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This page is used for update email form
 * @author     Icreon Tech - DG
 */
class UpdateEmailForm extends Form {

    public function __construct($name = null) {

        // we want to ignore the name passed
        parent::__construct('updateemail');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'user_id',
            'attributes' => array(
                'type' => 'hidden',
            )
        ));
        $this->add(array(
            'name' => 'email',
            'attributes' => array(
                'type' => 'text',
                'id' => 'email'
            )
        ));
        $this->add(array(
            'name' => 'confirm_email',
            'attributes' => array(
                'type' => 'text',
                'id' => 'confirm_email'
            )
        ));
        $this->add(array('type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 600
                )
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Update',
                'id' => 'submitbutton',
            ),
        ));
    }

}