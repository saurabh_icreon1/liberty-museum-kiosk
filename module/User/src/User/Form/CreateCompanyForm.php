<?php

/**
 * This form is used for create comapy 
 * @package    User
 * @author     Icreon Tech - AP
 */

namespace User\Form;

use Zend\Form\Form;

/**
 * This class is used for create comapy from elements
 * @package    User
 * @author     Icreon Tech - AP
 */
class CreateCompanyForm extends Form {

    public function __construct($name = null) {
        /* we want to ignore the name passed */
        parent::__construct('create-company');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'key',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'key'
            )
        ));
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'name'
            )
        ));
        $this->add(array(
            'name' => 'comp_email',
            'attributes' => array(
                'type' => 'text',
                'id' => 'comp_email'
            )
        ));
        $this->add(array(
            'name' => 'phone',
            'attributes' => array(
                'type' => 'text',
                'id' => 'phone'
            )
        ));
        $this->add(array(
            'name' => 'description',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'description'
            )
        ));
        $this->add(array(
            'name' => 'street',
            'attributes' => array(
                'type' => 'text',
                'id' => 'street'
            )
        ));
        $this->add(array(
            'name' => 'city',
            'attributes' => array(
                'type' => 'text',
                'id' => 'city'
            )
        ));
        $this->add(array(
            'name' => 'state',
            'attributes' => array(
                'type' => 'text',
                'id' => 'state'
            )
        ));
        $this->add(array(
            'name' => 'postal_code',
            'attributes' => array(
                'type' => 'text',
                'id' => 'postal_code'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'country',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                ),
            ),
            'attributes' => array(
                'id' => 'country',
                'class' => 'e1',
                'value' => '' /* set selected to 'blank' */
            ),
        ));
        $this->add(array(
            'name' => 'save',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'save',
                'class' => 'save-btn',
                'value' => 'Save'
            )
        ));
    }

}