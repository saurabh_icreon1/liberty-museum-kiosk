<?php

/**
 * This file define all controller and routes
 * @package    User
 * @author     Icreon Tech - DG
 */
return array(
    'controllers' => array(
        'invokables' => array(
            'User\Controller\Index' => 'User\Controller\IndexController',
            'User\Controller\Signup' => 'User\Controller\SignupController',
            'User\Controller\Login' => 'User\Controller\LoginController',
            'User\Controller\Contact' => 'User\Controller\ContactController',
            'User\Controller\User' => 'User\Controller\UserController',
            'User\Controller\Survey' => 'User\Controller\SurveyController',
            'User\Controller\Development' => 'User\Controller\DevelopmentController',
            'User\Controller\Membership' => 'User\Controller\MembershipController',
            'User\Controller\Relationship' => 'User\Controller\RelationshipController',
            'User\Controller\Mergecontact' => 'User\Controller\MergecontactController',
            'User\Controller\Crmuser' => 'User\Controller\CrmuserController',
            'User\Controller\Fof' => 'User\Controller\FofController',
            'User\Controller\Woh' => 'User\Controller\WohController'
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'user' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user[/][:action][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'User\Controller\Index',
                        'action' => 'index',
                    ),
                ),
            ),
            'home' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Home\Controller\Index',
                        'action' => 'index',
                    ),
                ),
            ),
            'signup' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/signup[/:referalUrl]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Signup',
                        'action' => 'signup',
                    ),
                ),
            ),
            'updateUserTermsCondition' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/update-user-terms-condition',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Login',
                        'action' => 'updateUserTermsCondition',
                    ),
                ),
            ),
            'forgot-password' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/forgot',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Login',
                        'action' => 'forgot',
                    ),
                ),
            ),
            'password-reset' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/password[/][:code]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Login',
                        'action' => 'setPassword',
                    ),
                ),
            ),
            'createsurvey' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-survey[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Survey',
                        'action' => 'getUserSurvey',
                    ),
                ),
            ),
            'userdevelopment' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-development[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Development',
                        'action' => 'getUserDevelopment',
                    ),
                ),
            ),
            'gift' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/gift[/:counter][/:container]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Development',
                        'action' => 'gift',
                    ),
                ),
            ),
            'jobhistory' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/jobhistory[/:counter][/:container]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Development',
                        'action' => 'job-history',
                    ),
                ),
            ),
            'createcompany' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-company[/:key]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Development',
                        'action' => 'create-company',
                    ),
                ),
            ),
            'user-profile' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/profile[/:mode]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'getProfile',
                    ),
                ),
            ),
            'activation' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/activation[/:code][/:referalUrl]',
                    'constraints' => array(
                        'code' => '[a-zA-Z0-9_-]*',
                        'id' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Signup',
                        'action' => 'activation',
                    ),
                ),
            ),
            'login' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/login[/:referer_path][/:social_mode]',
                    'defaults' => array(
                        'controller' => 'User\Controller\Login',
                        'action' => 'login',
                    ),
                ),
            ),
            'logout' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/logout[/:code]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'User\Controller\Login',
                        'action' => 'logout',
                    ),
                ),
            ),
            'updateemail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/update-email[/][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Signup',
                        'action' => 'update-email',
                    ),
                ),
            ),
            'resendmail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/resend-email[/][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Signup',
                        'action' => 'resend-email',
                    ),
                ),
            ),
            'verifymail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/verify-email[/][/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Signup',
                        'action' => 'verify-email',
                    ),
                ),
            ),
            'deletecontact' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-contact',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'delete-contact',
                    ),
                ),
            ),
            'createcontact' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-contact',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'create-contact',
                    ),
                ),
            ),
            'phoneno' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/phone-no[/:counter][/:container][/:divid]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'phone-no',
                    ),
                ),
            ),
            'corporatephoneno' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/corporate-phone-no[/:counter][/:container][/:divid]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'corporate-phone-no',
                    ),
                ),
            ),
            'addressform' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/address-form[/:counter][/:container]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'address-form',
                    ),
                ),
            ),
            'getcontactinfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-contact-info[/:counter][/:container]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'get-contact-info',
                    ),
                ),
            ),
            'afihcform' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/afihc[/:counter][/:container]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'afihc',
                    ),
                ),
            ),
            'corporateaddressform' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/corporate-address-form[/:counter][/:container]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'corporate-address-form',
                    ),
                ),
            ),
            'uploadfile' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-file[/:counter][/:container][/:flag]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'upload-file',
                    ),
                ),
            ),
            'corporateuploadfile' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-file-corporate[/:counter][/:container][/:flag]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'upload-file-corporate',
                    ),
                ),
            ),
            'website' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/website[/:counter][/:container]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'website',
                    ),
                ),
            ),
            'education' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/education[/:counter][/:container]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'education',
                    ),
                ),
            ),
            'getcontact' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/contact[/:new_contact]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'get-contact',
                    ),
                ),
            ),
            'viewcontact' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-contact[/:id][/:mode]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'view-contact',
                    ),
                ),
            ),
            'editcontact' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-contact[/:id][/:mode]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'edit-contact',
                    ),
                ),
            ),
            'searchcontact' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/search-contact[/:new_contact]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'searchContact',
                    ),
                ),
            ),
            'getContactMembershipPackage' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/membership-package[/:new_contact]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'getContactMembershipPackage',
                    ),
                ),
            ),
            'searchContactMembershipPackage' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/search-membership-package[/:new_contact]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'searchContactMembershipPackage',
                    ),
                ),
            ),
            'updateContactMembershipPackageStatus' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/update-contact-membership-package-status',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'updateContactMembershipPackageStatus',
                    ),
                ),
            ),
            'savesearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/contact-save-search[/]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'contactSaveSearch',
                    ),
                ),
            ),
            'savedsearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-saved-search[/]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'get-saved-search',
                    ),
                ),
            ),
            'deletesearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-search[/]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'delete-search',
                    ),
                ),
            ),
            'savesearchparam' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-saved-search-param[/]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'get-saved-search-param',
                    ),
                ),
            ),
            'crm' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm[/:referer_path]',
                    'defaults' => array(
                        'controller' => 'User\Controller\Login',
                        'action' => 'crmLogin',
                    ),
                ),
            ),
            'crm-forgot-password' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-forgot',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Login',
                        'action' => 'crmForgot',
                    ),
                ),
            ),
            'crm-password-reset' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-password[/][:crmcode]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Login',
                        'action' => 'setCrmPassword',
                    ),
                ),
            ),
            'dashboard' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/dashboard',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'dashboard',
                    ),
                ),
            ),
            'dashboardsetting' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/configure-dashboard',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'configureDashboard',
                    ),
                ),
            ),
            'usersearches' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-searches[/:user_id][/:type]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'get-user-searches',
                    ),
                ),
            ),
            'usersearchestype' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-search-type[/:user_id][/:type]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'get-user-search-type',
                    ),
                ),
            ),
            'createpopupcontact' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-popup-contact',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'createPopupContact',
                    ),
                ),
            ),
            'usermembership' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-memberships[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Membership',
                        'action' => 'get-user-membership',
                    ),
                ),
            ),
            'editusermembership' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-user-memberships[/:membership_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Membership',
                        'action' => 'edit-user-memberships',
                    ),
                ),
            ),
            'userrelationship' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-relationships[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Relationship',
                        'action' => 'get-user-relationships',
                    ),
                ),
            ),
            'uservisitation' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-visitation[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'get-user-visitation',
                    ),
                ),
            ),
            'deleterelationship' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-user-relationship',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Relationship',
                        'action' => 'deleteUserRelationship',
                    ),
                ),
            ),
            'userrelationshipsinformation' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-relationships-information[/:relationship_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Relationship',
                        'action' => 'getUserRelationshipInfo',
                    ),
                ),
            ),
            'edituserrelationship' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-user-relationships[/:relationship_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Relationship',
                        'action' => 'editUserRelationships',
                    ),
                ),
            ),
            'createuserrelationship' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-user-relationships[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Relationship',
                        'action' => 'addUserRelationship',
                    ),
                ),
            ),
            'usersoleifgift' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-soleif-gift[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Development',
                        'action' => 'getUserSoleifGift',
                    ),
                ),
            ),
            'contactdetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/contact-detail[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'contactDetail',
                    ),
                ),
            ),
            'importdevelopment' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/import-user-development[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Development',
                        'action' => 'importUserDevelopment',
                    ),
                ),
            ),
            'uploaddevelopmentfile' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-development-file[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Development',
                        'action' => 'uploadDevelopmentFile',
                    ),
                ),
            ),
            'uploadcontactimage' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-user-thumbnail',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'uploadUserThumbnail',
                    ),
                ),
            ),
            'uploadcontactdoc' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-user-public-doc',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'uploadUserPublicDoc',
                    ),
                ),
            ),
            'findmergerule' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/merge-rule',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Mergecontact',
                        'action' => 'getMergeRule',
                    ),
                ),
            ),
            'duplicatecontact' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/duplicate-contact[/:rule][/:group]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Mergecontact',
                        'action' => 'getDuplicateContact',
                    ),
                ),
            ),
            'adddedupeexception' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/dedupe-exception-contact[/:user_id][/:duplicate_user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Mergecontact',
                        'action' => 'addDedupeException',
                    ),
                ),
            ),
            'adddedupeexception' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/dedupe-exception-contact[/:user_id][/:duplicate_user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Mergecontact',
                        'action' => 'addDedupeException',
                    ),
                ),
            ),
            'addduplicatecontactinfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/duplicate-contact-info[/:user_id][/:duplicate_user_id][/:rule][/:group]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Mergecontact',
                        'action' => 'getDuplicateContactInfo',
                    ),
                ),
            ),
            'flipcontact' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/flip-contact[/:user_id][/:duplicate_user_id][/:rule]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Mergecontact',
                        'action' => 'flipContact',
                    ),
                ),
            ),
            'mergecontact' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/merge-contact[/:next][/:rule][/:group]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Mergecontact',
                        'action' => 'editMergeContact',
                    ),
                ),
            ),
            'createcrmuser' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-crm-user',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'addCrmUser',
                    ),
                ),
            ),
            'editcrmuser' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-user[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'editCrmUser',
                    ),
                ),
            ),
            'getcrmusersinfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-user-info[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'getCrmUserInfo',
                    ),
                ),
            ),
            'getCrmuserDetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crmuser-detail[/:id][/:mode]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'getCrmuserDetail',
                    ),
                ),
            ),
            'crmusers' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-users',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'getCrmUsers',
                    ),
                ),
            ),
            'editcrmusersstatus' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/change-crm-user-status[/:ids]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'editCrmUserStatus',
                    ),
                ),
            ),
            /*             * */
            'getsearchcrmusers' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-search-crmusers',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'getSearchCrmUsers',
                    ),
                ),
            ),
            'savecrmusersearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/save-crm-user-search',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'saveCrmUserSearch',
                    ),
                ),
            ),
            'userrole' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-crm-user-role',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'addCrmUserMasterRole',
                    ),
                ),
            ),
            'getcrmuserrole' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-user-roles',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'getCrmUserMasterRole',
                    ),
                ),
            ),
            'editcrmuserrole' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-user-role',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'editCrmUserMasterRole',
                    ),
                ),
            ),
            'deletecrmuserrole' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-user-role',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'deleteCrmUserMasterRole',
                    ),
                ),
            ),
            'getcrmusersearchinfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-user-search-info',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'getCrmUserSearchInfo',
                    ),
                ),
            ),
            'deletecrmusersearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-user-search',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'deleteCrmUserSearch',
                    ),
                ),
            ),
            'uploadcrmuserprofile' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-crm-user-profile',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'uploadCrmUserProfile',
                    ),
                ),
            ),
            'deleteContactPublicDoc' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-contact-doc',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'deleteContactDoc',
                    ),
                ),
            ),
            'delteDevelopmentDoc' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete_contact_development_doc',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Development',
                        'action' => 'deleteContactDevelopmentDoc',
                    ),
                ),
            ),
            'getSavedCrmUserSearchSelect' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-saved-crm-user-search-select',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'getSavedCrmUserSearchSelect',
                    ),
                ),
            ),
            'getCrmUserAssignment' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-user-assignments',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'getCrmUserAssignment',
                    ),
                ),
            ),
            'getCrmUserAssignmentSearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-user-assignment-search',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'getCrmUserAssignmentSearch',
                    ),
                ),
            ),
            'addCrmUserAssignmentSearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/save-crm-user-assignment-search',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'addCrmUserAssignmentSearch',
                    ),
                ),
            ),
            'getCrmUserAssignmentSearchInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-user-assignment-search-info',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'getCrmUserAssignmentSearchInfo',
                    ),
                ),
            ),
            'deleteCrmUserAssignmentSearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-user-assignment-search',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'deleteCrmUserAssignmentSearch',
                    ),
                ),
            ),
            'getSavedCrmUserAssignmentSearchSelect' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-user-assignment-save-search',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'getSavedCrmUserAssignmentSearchSelect',
                    ),
                ),
            ),
            'crmuserrolepermission' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-user-role-permission[/:role_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'getCrmUserRolePermission',
                    ),
                ),
            ),
            'changeCrmUserPassword' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/change-crm-user-password[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'changeCrmUserPassword',
                    ),
                ),
            ),
            'myfile' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/my-file[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'getMyFile',
                    ),
                ),
            ),
            'getdocumentsearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/my-document-search[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'getDocumentSearch',
                    ),
                ),
            ),
            'get-passenger-search' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/my-passenger-search[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'getPassengerSearch',
                    ),
                ),
            ),
            'get-manifest-search' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/my-manifest-search[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'getManifestSearch',
                    ),
                ),
            ),
            'get-ship-search' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/my-ship-search[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'getShipSearch',
                    ),
                ),
            ),
            'getfoflist' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/my-flag-of-faces[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'getFofList',
                    ),
                ),
            ),
            'getwohlist' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/my-wall-of-honor[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'getWohList',
                    ),
                ),
            ),
            'deletefofsearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-fof-search-myfile',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'deleteFofSearch',
                    ),
                ),
            ),
            'deletehohsearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-woh-search-myfile',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'deleteWohSearch',
                    ),
                ),
            ),
            'userfofsetting' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-fof-setting[/:fof_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'userFofSetting',
                    ),
                ),
            ),
            'wohedit' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-wall-of-honor[/:woh_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'editWoh',
                    ),
                ),
            ),
            'woheditbio' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-bio-wall-of-honor[/:woh_id][/:bio_certificate_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'editBioWoh',
                    ),
                ),
            ),
            'viewbiowoh' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-bio-wall-of-honor[/:bio_certificate_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'viewBioWoh',
                    ),
                ),
            ),
            'isemailexits' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/email-exits',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Signup',
                        'action' => 'isEmailExits',
                    ),
                ),
            ),
            'memberbenefit' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/member-benefit',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Membership',
                        'action' => 'memberBenefit',
                    ),
                ),
            ),
            'edituserinfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-user-info[/:mode]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'editUserInfo',
                    ),
                ),
            ),
            'editbasicinfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-basic-info[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'editBasicInfo',
                    ),
                ),
            ),
            'uploaduserpic' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-user-pic[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'uploadUserPic',
                    ),
                ),
            ),
            'edituserpassword' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-user-password[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'editUserPassword',
                    ),
                ),
            ),
            'editsecurityquestion' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-security-question[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'editUserSecurityQuestion',
                    ),
                ),
            ),
            'editemailinfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-email-info[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'editUserEmailInfo',
                    ),
                ),
            ),
            'verifyupdateemail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/verify-update-email[/:verification_code]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Signup',
                        'action' => 'verifyUpdateEmail',
                    ),
                ),
            ),
            'editusercontactinfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-user-contact-info[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'editUserContactInfo',
                    ),
                ),
            ),
            'editusercontactinfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-user-contact-info[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'editUserContactInfo',
                    ),
                ),
            ),
            'deleteuserphoneinfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-user-phone-info',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'deleteUserPhoneInfo',
                    ),
                ),
            ),
            'deleteuseraddressinfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-user-address-info',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'deleteUserAddressInfo',
                    ),
                ),
            ),
            'addedituserphoneinfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-edit-user-phone-info[/:phone_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'addEditUserPhoneInfo',
                    ),
                ),
            ),
            'addedituseraddressinfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-edit-user-address-info[/:mode][/:address_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'addEditUserAddressInfo',
                    ),
                ),
            ),
            'showUserAddressInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/show-user-address-info',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'showUserAddressInfo',
                    ),
                ),
            ),
            'addeditusersurveyinfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-edit-user-survey-info[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Survey',
                        'action' => 'addEditUserSurveyInfo',
                    ),
                ),
            ),
            'refreshcaptcha' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/refresh-captcha',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Signup',
                        'action' => 'refreshCaptcha',
                    ),
                ),
            ),
            'edituserdemographicsinfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-user-demographics-info[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'editUserDemographicsInfo',
                    ),
                ),
            ),
            'addusereducationinfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-user-education-info[/:counter][/:container]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'addUserEducationInfo',
                    ),
                ),
            ),
            'adduserjobhistory' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-user-job-history[/:counter][/:container]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'addUserJobInfo',
                    ),
                ),
            ),
            'usermembershipinfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-membership-info[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Membership',
                        'action' => 'getUserMembershipInfo',
                    ),
                ),
            ),
            'unsubscribeusers' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/unsubscribe-users',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'unsubscribeUsers',
                    ),
                ),
            ),
            'indexFof' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/flag-of-faces',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Fof',
                        'action' => 'indexFof',
                    ),
                ),
            ),
            'addFofImage' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-fof-image',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Fof',
                        'action' => 'addFofImage',
                    ),
                ),
            ),
            'addFofImageProcess' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-fof-image-process',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Fof',
                        'action' => 'addFofImageProcess',
                    ),
                ),
            ),
            'uploadUserFofImage' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-user-fof-image',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Fof',
                        'action' => 'uploadUserFofImage',
                    ),
                ),
            ),
            'uploadUserFofImageIframe' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-user-fof-image-iframe[/:image_path][/:image_name]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Fof',
                        'action' => 'uploadUserFofImageIframe',
                    ),
                ),
            ),
            'uploadUserFofImageIframePreview' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-user-fof-image-iframe-preview[/:image_path][/:image_flag]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Fof',
                        'action' => 'uploadUserFofImageIframePreview',
                    ),
                ),
            ),
            'uploadUserFofImageIframeCRM' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-user-fof-image-iframe-crm[/:fof_id][/:fof_modified_image]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Fof',
                        'action' => 'uploadUserFofImageIframeCRM',
                    ),
                ),
            ),
            'uploadUserFofImageIframePreviewCRM' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-user-fof-image-iframe-preview-crm[/:fof_id][/:fof_modified_image][/:flag_status]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Fof',
                        'action' => 'uploadUserFofImageIframePreviewCRM',
                    ),
                ),
            ),
            'smallImageViewFof' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/small-image-view-fof[/:image_path][/:image_flag][/:image_name]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Fof',
                        'action' => 'smallImageViewFof',
                    ),
                ),
            ),
            'fofEncryptText' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/fof-encrypt-text[/:decryptText]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Fof',
                        'action' => 'fofEncryptText',
                    ),
                ),
            ),
            'aboutFof' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/about-fof',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Fof',
                        'action' => 'aboutFof',
                    ),
                ),
            ),
            'searchFof' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/search-fof[/:fof_id][/:fof_search_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Fof',
                        'action' => 'searchFof',
                    ),
                ),
            ),
            'getFofData' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-fof-data',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Fof',
                        'action' => 'getFofData',
                    ),
                ),
            ),
            'showFofImage' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/show-fof-image[/:fof_param]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Fof',
                        'action' => 'showFofImage',
                    ),
                ),
            ),
            'saveFofSearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/save-fof-search',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Fof',
                        'action' => 'saveFofSearch',
                    ),
                ),
            ),
            'getFofSearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-fof-user-saved-search-select',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Fof',
                        'action' => 'getFofSearch',
                    ),
                ),
            ),
            'deleteFofSearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-fof-search',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Fof',
                        'action' => 'deleteFofSearch',
                    ),
                ),
            ),
            'addWoh' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-woh',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Woh',
                        'action' => 'addWoh',
                    ),
                ),
            ),
            'addWohProcess' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-woh-process',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Woh',
                        'action' => 'addWohProcess',
                    ),
                ),
            ),
            'aboutWoh' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/about-woh',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Woh',
                        'action' => 'aboutWoh',
                    ),
                ),
            ),
            'searchWoh' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/search-woh[/:woh_id][/:woh_search_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Woh',
                        'action' => 'searchWoh',
                    ),
                ),
            ),
            'getWohList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/woh-list',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Woh',
                        'action' => 'getWohList',
                    ),
                ),
            ),
            'moreWohList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/more-woh-list',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Woh',
                        'action' => 'moreWohList',
                    ),
                ),
            ),
            'getSavedWohSearchSelect' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-woh-user-saved-search-select',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Woh',
                        'action' => 'getSavedWohSearchSelect',
                    ),
                ),
            ),
            'saveWohSearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/save-woh-search',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Woh',
                        'action' => 'saveWohSearch',
                    ),
                ),
            ),
            'deleteWohSearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-woh-search',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Woh',
                        'action' => 'deleteWohSearch',
                    ),
                ),
            ),
            'viewWohProducts' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-woh-products[/:wohParams]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Woh',
                        'action' => 'viewWohProducts',
                    ),
                ),
            ),
            'addToCartWohProducts' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-to-cart-woh-products',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Woh',
                        'action' => 'addToCartWohProducts',
                    ),
                ),
            ),
            'getWohFrames' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-woh-frames',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Woh',
                        'action' => 'getWohFrames',
                    ),
                ),
            ),
            'getWohOtherProducts' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-woh-other-products',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Woh',
                        'action' => 'getWohOtherProducts',
                    ),
                ),
            ),
            'generateWohFormByNamingConvention' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/woh-generate-form-by-naming-convention',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Woh',
                        'action' => 'generateWohFormByNamingConvention',
                    ),
                ),
            ),
            'addAdditionalContact' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/woh-add-additional-contact',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Woh',
                        'action' => 'addAdditionalContact',
                    ),
                ),
            ),
            'usersocialinfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-social-info[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'userSocialInfo',
                    ),
                ),
            ),
            'edituserpreferences' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-user-preferences[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'editUserPreferencesInfo',
                    ),
                ),
            ),
            'myoralhistoryinfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/my-oral-history-info[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'myOralHistoryInfo',
                    ),
                ),
            ),
            'usercreditcardinfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/credit-card-info[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'editUserCreditCardInfo',
                    ),
                ),
            ),
            'contactus' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/contactus',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'contactus',
                    ),
                ),
            ),
            'addtokeninfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-token',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'addTokenInfo',
                    ),
                ),
            ),
            'deletetokeninfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-user-token-info',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'deleteUserTokenInfo',
                    ),
                ),
            ),
            'messagecenter' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/message-center',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'userMessageCenter',
                    ),
                ),
            ),
            'deleteuserdashlet' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-user-dashlet',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'deleteUserDashlet',
                    ),
                ),
            ),
            'topcontacts' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/top-contacts',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'topContacts',
                    ),
                ),
            ),
            'topsellingproducts' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/top-selling-products',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'topSellingProducts',
                    ),
                ),
            ),
            'transactionqueue' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/transaction-queue',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'transactionQueue',
                    ),
                ),
            ),
            'transactionsummary' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/transaction-summary',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Crmuser',
                        'action' => 'transactionSummary',
                    ),
                ),
            ),
            'gettimezoneoffset' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-time-zone-offset[/:time_zone]',
                    'defaults' => array(
                        'controller' => 'User\Controller\Index',
                        'action' => 'timeZoneOffset',
                    ),
                ),
            ),
            'dashletvisiblitysetting' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/dashlet-visiblity-setting',
                    'defaults' => array(
                        'controller' => 'User\Controller\Crmuser',
                        'action' => 'dashletVisiblitySetting',
                    ),
                ),
            ),
            'savedashboardsetting' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/save-dashboard-setting[/:encrypted_param][/:dashlet][/:isDragged]',
                    'defaults' => array(
                        'controller' => 'User\Controller\Crmuser',
                        'action' => 'saveDashboardSetting',
                    ),
                ),
            ),
            'listTableFields' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/list-table-fields[/:dash_id][/:search_id][/:search_type][/:visible]',
                    'defaults' => array(
                        'controller' => 'User\Controller\Crmuser',
                        'action' => 'listTableFields',
                    ),
                ),
            ),
            'updateDashletTable' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/update-dashlet-table',
                    'defaults' => array(
                        'controller' => 'User\Controller\Crmuser',
                        'action' => 'updateDashletTable',
                    ),
                ),
            ),
            'updateSystemDashlet' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/update-system-dashlet',
                    'defaults' => array(
                        'controller' => 'User\Controller\Crmuser',
                        'action' => 'updateSystemDashlet',
                    ),
                ),
            ),
            'userdigitalcertificate' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-digital-certificate[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'digitalCertificate',
                    ),
                ),
            ),
            'wohadditionalproduct' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/woh-additional-product[/:panel_no][/:is_finalize][/:bio_certificate_prod_id]',
                    'defaults' => array(
                        'controller' => 'User\Controller\user',
                        'action' => 'wohAdditionalProduct',
                    ),
                ),
            ),
            'memberbenefitentry' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/member-benefit-entry[/:minAmount][/:membershipID]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Membership',
                        'action' => 'memberBenefitEntry',
                    ),
                ),
            ),
            'logindetails' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/login-details',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'User',
                        'action' => 'loginDetails',
                    ),
                ),
            ),
            'getContactBonus' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/contact-bonus',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'getContactBonus',
                    ),
                ),
            ),
            'editContactSaveSearchBonus' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-contact-save-search-bonus',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'editContactSaveSearchBonus',
                    ),
                ),
            ),
            'resendactivationcode' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/resend-activation-code',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Signup',
                        'action' => 'resendActivationCode',
                    ),
                ),
            ),
            'changeLockStatus' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/change-lock-status',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Contact',
                        'action' => 'changeLockStatus',
                    ),
                ),
            ),
			'recentcontact' => array(
			'type' => 'segment',
			'options' => array(
				'route' => '/recent-contact[/:new_contact]',
				'defaults' => array(
					'__NAMESPACE__' => 'User\Controller',
                    'controller' => 'Crmuser',
					'action' => 'recentContact',
				),
			),
		),
            'visitationRecord' => array(
			'type' => 'segment',
			'options' => array(
				'route' => '/visitation-record[/:user_id]',
				'defaults' => array(
		'__NAMESPACE__' => 'User\Controller',
                    'controller' => 'Signup',
					'action' => 'visitationRecord',
				),
			),
		),
            'isUsernameExist' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/username-exist',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Signup',
                        'action' => 'isUsernameExist',
                    ),
                ),
            ),
            'viewWohImage' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-woh-image[/:panel_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'Woh',
                        'action' => 'viewWohImage',
                    ),
                ),
            ),
            
        ),		
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'navigation' => array(
        'default' => array(
            array(
                'label' => 'Contact',
                'route' => 'getcontact',
                'pages' => array(
                    array(
                        'label' => 'Search',
                        'route' => 'getcontact',
                        'pages' => array(
                            array(
                                'label' => 'View',
                                'route' => 'contactdetail',
                                'action' => 'contactDetail',
                            ),
                            array(
                                'label' => 'Edit',
                                'route' => 'editcontact',
                                'action' => 'edit-contact',
                            ),
                            array(
                                'label' => 'Transactions',
                                'route' => 'getUserTransactions',
                                'action' => 'get-user-transactions',
                            ),
                            array(
                                'label' => 'Campaign',
                                'route' => 'getUserCampaigns',
                                'action' => 'get-user-campaigns',
                            ),
                            array(
                                'label' => 'Survey',
                                'route' => 'createsurvey',
                                'action' => 'getUserSurvey',
                            ),
                            array(
                                'label' => 'Saved Searches',
                                'route' => 'usersearches',
                                'action' => 'get-user-searches',
                            ),
                            array(
                                'label' => 'Development',
                                'route' => 'userdevelopment',
                                'action' => 'getUserDevelopment',
                            ),
                            array(
                                'label' => 'Leads',
                                'route' => 'contactleads',
                                'action' => 'contactLeads',
                            ),
                            array(
                                'label' => 'Family Histories',
                                'route' => 'getContactFamilyHistory',
                                'action' => 'getContactFamilyHistory',
                            ),
                            array(
                                'label' => 'Relationships',
                                'route' => 'userrelationship',
                                'action' => 'get-user-relationships',
                                'pages' => array(
                                    array(
                                        'label' => 'Add',
                                        'route' => 'createuserrelationship',
                                        'action' => 'addUserRelationship',
                                    ),
                                    array(
                                        'label' => 'Edit',
                                        'route' => 'edituserrelationship',
                                        'action' => 'editUserRelationships',
                                    ),
                                    array(
                                        'label' => 'View',
                                        'route' => 'userrelationshipsinformation',
                                        'action' => 'getUserRelationshipInfo',
                                    )
                                ),
                            ),
                            array(
                                'label' => 'Membership',
                                'route' => 'usermembership',
                                'action' => 'get-user-membership',
                            ),
                            array(
                                'label' => 'Visitation Log',
                                'route' => 'uservisitation',
                                'action' => 'get-user-visitation',
                            ),
                            array(
                                'label' => 'Change Log',
                                'route' => 'changeloguser',
                                'action' => 'change-log'
                            ),
                            array(
                                'label' => 'Create',
                                'route' => 'createcontact',
                                'action' => 'create-contact'
                            ),
                            array(
                                'label' => 'Activities',
                                'route' => 'getContactActivities',
                                'action' => 'getContactActivities'
                            ),
                            array(
                                'label' => 'Pledge',
                                'route' => 'contactpledges',
                                'action' => 'contactPledges',
                            ),
                        )
                    ),
                    array(
                        'label' => 'Membership Package',
                        'route' => 'getContactMembershipPackage',
                        'action' => 'getContactMembershipPackage'
                    ),
                ),
            ),
            array(
                'label' => 'Search',
                'route' => 'getCrmUserAssignment',
                'pages' => array(
                    array(
                        'label' => 'List',
                        'route' => 'getCrmUserAssignment',
                        'action' => 'getCrmUserAssignment',
                    ),
                ),
            ),
            array(
                'label' => 'User Roles',
                'route' => 'userrole',
                'pages' => array(
                    array(
                        'label' => 'List',
                        'route' => 'userrole',
                        'pages' => array(
                            array(
                                'label' => 'User Role Permission',
                                'route' => 'crmuserrolepermission',
                                'action' => 'getCrmUserRolePermission',
                            ),
                        ),
                    ),
                ),
            ),
            array(
                'label' => 'Search',
                'route' => 'crmusers',
                'pages' => array(
                    array(
                        'label' => 'View',
                        'route' => 'getcrmusersinfo',
                        'action' => 'getCrmUserInfo',
                    ),
                    array(
                        'label' => 'Edit',
                        'route' => 'editcrmuser',
                        'action' => 'editCrmUser',
                    ),
                    array(
                        'label' => 'Change Log ',
                        'route' => 'getcrmuserchangelog',
                        'action' => 'get-change-log',
                    ),
                ),
            ),
            array(
                'label' => 'Find & Merge',
                'route' => 'findmergerule',
                'pages' => array(
                    array(
                        'label' => 'Rules',
                        'route' => 'findmergerule',
                        'pages' => array(
                            array(
                                'label' => 'Name',
                                'route' => 'duplicatecontact',
                                'pages' => array(
                                    array(
                                        'label' => 'Merge',
                                        'route' => 'addduplicatecontactinfo',
                                        'action' => 'getDuplicateContactInfo',
                                    ),
                                ),
                            ),
                        ),
                    ),
                ),
            ),
            array(
                'label' => 'Unsubscribe Users',
                'route' => 'unsubscribeusers'
            )
        ),
    ),
    'user_messages' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
    'user_blocked' => array(
        'release_time' => '30',
        'block_time_limit' => '30',
        'invalid_attempt' => '3',
    ),
    'crm_user_blocked' => array(
        'block_time_limit' => '30',
        'invalid_attempt' => '3',
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
            'navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'user' => __DIR__ . '/../view',
            'signup' => __DIR__ . '/../view',
            'login' => __DIR__ . '/../view',
            'contact' => __DIR__ . '/../view',
        ),
    ),
    'global_config' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
);
