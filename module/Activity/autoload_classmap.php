<?php
/**
 * This page is used for activity module autoloading classes before module intilize.
 * @package    Activity
 * @author     Icreon Tech - DT
 */
return array(
    'Activity\Module'                       => __DIR__ . '/Module.php',
    'Activity\Controller\ActivityController'   => __DIR__ . '/src/Activity/Controller/ActivityController.php',
    'Activity\Model\Activity'            => __DIR__ . '/src/Activity/Model/Activity.php',
    'Activity\Model\ActivityTable'                   => __DIR__ . '/src/Activity/Model/ActivityTable.php'
);
?>