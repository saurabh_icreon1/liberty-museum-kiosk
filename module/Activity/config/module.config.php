<?php

/**
 * This page is used for activity module configuration details.
 * @package    Activity
 * @author     Icreon Tech - DT
 */
return array(
    'controllers' => array(
        'invokables' => array(
            'Activity\Controller\Activity' => 'Activity\Controller\ActivityController'
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'addActivity' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-activity[/:userid][/:sourceid][/:activitysourceid]',
                    'defaults' => array(
                        'controller' => 'Activity\Controller\Activity',
                        'action' => 'addActivity',
                    ),
                ),
            ),
            'getActivities' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/activities[/][:userid][/][:sourceid]',
                    'defaults' => array(
                        'controller' => 'Activity\Controller\Activity',
                        'action' => 'getActivities',
                    ),
                ),
            ),
            'uploadAttachmentActivity' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-attachment-activity',
                    'defaults' => array(
                        'controller' => 'Activity\Controller\Activity',
                        'action' => 'uploadAttachmentActivity',
                    ),
                ),
            ),
            'getActivitySubject' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-activity-subject',
                    'defaults' => array(
                        'controller' => 'Activity\Controller\Activity',
                        'action' => 'getActivitySubject',
                    ),
                ),
            ),
            'saveActivitySearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/save-activity-search',
                    'defaults' => array(
                        'controller' => 'Activity\Controller\Activity',
                        'action' => 'saveActivitySearch',
                    ),
                ),
            ),
            'getSavedActivitySearchSelect' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-saved-activity-search-select',
                    'defaults' => array(
                        'controller' => 'Activity\Controller\Activity',
                        'action' => 'getSavedActivitySearchSelect',
                    ),
                ),
            ),
            'getActivitySearchSavedParam' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-activity-search-saved-param',
                    'defaults' => array(
                        'controller' => 'Activity\Controller\Activity',
                        'action' => 'getActivitySearchSavedParam',
                    ),
                ),
            ),
            'deleteSearchActivity' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-search-activity',
                    'defaults' => array(
                        'controller' => 'Activity\Controller\Activity',
                        'action' => 'deleteSearchActivity',
                    ),
                ),
            ),
            'deleteActivity' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-activity',
                    'defaults' => array(
                        'controller' => 'Activity\Controller\Activity',
                        'action' => 'deleteActivity',
                    ),
                ),
            ),
            'getActivityDetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-activity-detail/:activity_id[/][:mode]',
                    'defaults' => array(
                        'controller' => 'Activity\Controller\Activity',
                        'action' => 'getActivityDetail',
                    ),
                ),
            ),
            'editActivityDetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-activity-detail/:activity_id',
                    'defaults' => array(
                        'controller' => 'Activity\Controller\Activity',
                        'action' => 'editActivityDetail',
                    ),
                ),
            ),
            'viewActivityDetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-activity-detail[/][/:activity_id]',
                    'defaults' => array(
                        'controller' => 'Activity\Controller\Activity',
                        'action' => 'viewActivityDetail',
                    ),
                )
            ),
            'editActivity' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-activity',
                    'defaults' => array(
                        'controller' => 'Activity\Controller\Activity',
                        'action' => 'editActivity',
                    ),
                ),
            ),
            'downloadActivityFile' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/download-activity-file/:fileName',
                    'defaults' => array(
                        'controller' => 'Activity\Controller\Activity',
                        'action' => 'downloadActivityFile',
                    ),
                ),
            ),
            'getContactActivities' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/contact-activities[/]',
                    'defaults' => array(
                        'controller' => 'Activity\Controller\Activity',
                        'action' => 'getContactActivities',
                    ),
                ),
            ),
            'getPledgeContact' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-pledge-contact',
                    'defaults' => array(
                        'controller' => 'Activity\Controller\Activity',
                        'action' => 'getPledgeContact',
                    ),
                ),
            ),            
            'getactivityleads' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-activity-leads[/]',
                    'defaults' => array(
                        'controller' => 'Activity\Controller\Activity',
                        'action' => 'getActivityLeads',
                    ),
                ),
            ),            
            'getactivitypledges' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-activity-pledges[/]',
                    'defaults' => array(
                        'controller' => 'Activity\Controller\Activity',
                        'action' => 'getActivityPledges',
                    ),
                ),
            ),            
            'getactivitycases' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-activity-cases[/]',
                    'defaults' => array(
                        'controller' => 'Activity\Controller\Activity',
                        'action' => 'getActivityCases',
                    ),
                ),
            ),				            
            'createtempactivity' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-temp-activity',
                    'defaults' => array(
                        'controller' => 'Activity\Controller\Activity',
                        'action' => 'createTempActivity',
                    ),
                ),
            ),			            
            'gettempactivity' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-temp-activity',
                    'defaults' => array(
                        'controller' => 'Activity\Controller\Activity',
                        'action' => 'getTempActivity',
                    ),
                ),
            ),			            
            'deletetempactivity' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-temp-activity',
                    'defaults' => array(
                        'controller' => 'Activity\Controller\Activity',
                        'action' => 'deleteTempActivity',
                    ),
                ),
            ),			            
            'countrelatedtempactivitysources' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/count-related-temp-activity-sources',
                    'defaults' => array(
                        'controller' => 'Activity\Controller\Activity',
                        'action' => 'countRelatedTempActivitySources',
                    ),
                ),
            ),			            
            'viewrelatedactivitysources' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-related-activity-sources',
                    'defaults' => array(
                        'controller' => 'Activity\Controller\Activity',
                        'action' => 'viewRelatedActivitySources',
                    ),
                ),
            ),			            
            'deleterelatedactivitysource' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-related-activity-source',
                    'defaults' => array(
                        'controller' => 'Activity\Controller\Activity',
                        'action' => 'deleteRelatedActivitySource',
                    ),
                ),
            ),
            
        )
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'navigation' => array(
        'default' => array(
            array(
                'label' => 'Activity',
                'route' => 'getActivities',
                'pages' => array(
                    array(
                        'label' => 'Search',
                        'route' => 'getActivities',
                        'pages' => array(
                            array(
                                'label' => 'Create',
                                'route' => 'addActivity',
                                'action' => 'addActivity',
                            ),
                            array(
                                'label' => 'View',
                                'route' => 'viewActivityDetail',
                                'action' => 'viewActivityDetail',
                            ),
                            array(
                                'label' => 'Edit',
                                'route' => 'editActivityDetail',
                                'action' => 'editActivityDetail',
                            ),
                            array(
                                'label' => 'Change Log',
                                'route' => 'activitychangelog',
                                'action' => 'change-log'
                            )
                            
                        ),
                    ),
                )
            )
        ),
    ),
    'activity_messages' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
    'file_upload_information' => array(
        'number_of_attachment' => 3,
        'file_types_allowed' => '/\.*$/i',
        'max_allowed_file_size' => ((pow(1024, 2)) * 2) //in byte 2 mb
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
            'navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'activity' => __DIR__ . '/../view'
        ),
        'template_map' => array(
            'layout/custom' => __DIR__ . '/../../module/Common/layout/crm.phtml',
            'error/404' => __DIR__ . '/../../module/Common/error/404.phtml',
            'error/index' => __DIR__ . '/../../module/Common/error/index.phtml',
        ),
    ),
    'global_config' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
);
