<?php

/**
 * This controller is used for activity module
 * @package    Activity_ActivityController
 * @author     Icreon Tech - DT
 */

namespace Activity\Controller;

use Base\Controller\BaseController;
use Base\Model\UploadHandler;
use Zend\View\Model\ViewModel;
use Activity\Form\ActivityForm;
use Activity\Form\SearchActivityForm;
use Activity\Form\SaveSearchForm;
use Activity\Model\Activity;
use Pledge\Model\Pledge;
use Cases\Model\Cases;
use Common\Model\Common;
use Zend\Http\Request;
use Zend\Authentication\AuthenticationService;

/**
 * This class is used to perform all activities of Activity module
 *
 * Here we do create,edit,update,save search ,update save search , listing of activity
 *
 * @category Zend
 * @package Activity_ActivityController
 * @link http://SITE_URL/activities
 */
class ActivityController extends BaseController {

    protected $_activityTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_flashMessage = null;

    function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        parent::__construct();
    }

    /**
     * This function is used to get table and config file object
     * @return     array
     * @param void
     * @author Icreon Tech - DT
     */
    public function getActivityTable() {
        $this->checkUserAuthentication();
        if (!$this->_activityTable) {
            $serviceManager = $this->getServiceLocator();
            $this->_activityTable = $serviceManager->get('Activity\Model\ActivityTable');
            $this->_translator = $serviceManager->get('translator');
            $this->_config = $serviceManager->get('Config');
            $this->_adapter = $serviceManager->get('dbAdapter');
            $this->_auth = new AuthenticationService();
        }
        return $this->_activityTable;
    }

    /**
     * This action is used to search activities
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function getActivitiesAction() {
        $this->checkUserAuthentication();
        $this->getActivityTable();
        $params = $this->params()->fromRoute();
        $activitySearchMessage = $this->_config['activity_messages']['config']['activity_search_message'];
        $activity = new Activity($this->_adapter);
        $searchActivityForm = new SearchActivityForm();
        $saveSearchForm = new SaveSearchForm();
        $request = $this->getRequest();
        /* Activity type */
        $getActivityType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getActivityType();
        $activityType = array();
        $activityTypeDesc = array();
        $activityType[''] = 'Select';
        if ($getActivityType !== false) {
            foreach ($getActivityType as $key => $val) {
                $activityType[$val['activity_type_id']] = $val['activity_type'];
                $activityTypeDesc[$val['activity_type_id']] = $val['type_description'];
            }
        }
        $searchActivityForm->get('activity_type_id')->setAttribute('options', $activityType);
        /* End  Activity type  */
        /* Activity schedule follow up type */
        $getActivityFollowupTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getActivitySourceTypes();
        $activityFollowupTypes = array();
        $activityFollowupTypes[''] = 'Select';
        if ($getActivityFollowupTypes !== false) {
            foreach ($getActivityFollowupTypes as $key => $val) {
                $activityFollowupTypes[$val['activity_source_type_id']] = $val['activity_source_type'];
            }
        }
        $searchActivityForm->get('activity_source_type_id')->setAttribute('options', $activityFollowupTypes);
        /* End  Activity schedule follow up type  */

        /* Get date range */
        $caseDateRange = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTransactionRange();
        $dateRange = array("" => "Select One");
        foreach ($caseDateRange as $key => $val) {
            $dateRange[$val['range_id']] = $val['range'];
        }
        $searchActivityForm->get('added_date_range')->setAttribute('options', $dateRange);
        /* end Get date range */

        /* Saved search data */
        $getSearchArray = array();
        $getSearchArray['crm_user_id'] = (isset($this->_auth->getIdentity()->crm_user_id) and trim($this->_auth->getIdentity()->crm_user_id) != "") ? $this->_auth->getIdentity()->crm_user_id : "";
        $getSearchArray['activity_search_id'] = '';
        $getSearchArray['is_active'] = '1';
        $activitySavedSearchArray = $activity->getActivitySavedSearchArr($getSearchArray);
        $activitySearchArray = $this->_activityTable->getActivitySavedSearch($activitySavedSearchArray);
        $activitySearchList = array();
        $activitySearchList[''] = 'Select';
        foreach ($activitySearchArray as $key => $val) {
            $activitySearchList[$val['activity_search_id']] = stripslashes($val['title']);
        }
        $searchActivityForm->get('saved_search')->setAttribute('options', $activitySearchList);

        /* end  Saved search data */
        if (($request->isXmlHttpRequest() && $request->isPost()) || $request->getPost('export') == 'excel') {
            $response = $this->getResponse();
            $postArray = $request->getPost()->toArray();
            parse_str($request->getPost('searchString'), $searchParam);

            $dashlet = $request->getPost('crm_dashlet');
            $dashletSearchId = $request->getPost('searchId');
            $dashletId = $request->getPost('dashletId');
            $dashletSearchType = $request->getPost('searchType');
            $dashletSearchString = $request->getPost('dashletSearchString');
            if (isset($dashletSearchString) && $dashletSearchString != '') {
                $searchParam = unserialize($dashletSearchString);
            }

            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit; // do not put $limit*($page - 1)
            $searchParam['start_index'] = $start;
            $searchParam['record_limit'] = $limit;
            $isExport = $request->getPost('export');
            if ($isExport != "" && $isExport == "excel") {
                $chunksize = $this->_config['export']['chunk_size'];
                $export_limit = $this->_config['export']['export_limit'];

                $searchParam['record_limit'] = $chunksize;
            }
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            if (isset($searchParam['added_date_range']) && $searchParam['added_date_range'] != 1 && $searchParam['added_date_range'] != '') {
                $dateRange = $this->getDateRange($searchParam['added_date_range']);
                $searchParam['added_date_from'] = $this->DateFormat($dateRange['from'], 'db_date_format_from');
                $searchParam['added_date_to'] = $this->DateFormat($dateRange['to'], 'db_date_format_to');
            } else if (isset($searchParam['added_date_range']) && $searchParam['added_date_range'] == '') {
                $searchParam['added_date_from'] = '';
                $searchParam['added_date_to'] = '';
            } else {
                if (isset($searchParam['added_date_from']) && $searchParam['added_date_from'] != '') {
                    $searchParam['added_date_from'] = $this->DateFormat($searchParam['added_date_from'], 'db_date_format_from');
                }
                if (isset($searchParam['added_date_to']) && $searchParam['added_date_to'] != '') {
                    $searchParam['added_date_to'] = $this->DateFormat($searchParam['added_date_to'], 'db_date_format_to');
                }
            }
            $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'modified_date') ? 'tbl_activity.modified_date' : $searchParam['sort_field'];

            $page_counter = 1;
            do {
                $searchActivityArr = $activity->getActivitySearchArr($searchParam);
                $seachResult = $this->_activityTable->getAllActivities($searchActivityArr);
                $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
                $dashletResult = $this->getServiceLocator()->get('User\Model\CrmuserTable')->getTableStruct(array('user_id' => $this->_auth->getIdentity()->crm_user_id, 'search_id' => $dashletSearchId, 'dash_id' => $dashletId, 'search_type' => $dashletSearchType));

                $dashletColumnName = array();
                if (!empty($dashletResult)) {
                    $dashletColumn = explode(",", $dashletResult[0]['table_column_names']);
                    foreach ($dashletColumn as $val) {
                        if (strpos($val, ".")) {
                            $dashletColumnName[] = trim(strstr($val, ".", false), ".");
                        } else {
                            $dashletColumnName[] = trim($val);
                        }
                    }
                }
                $jsonResult = array();
                $arrCell = array();
                $subArrCell = array();
                $total = $countResult[0]->RecordCount;
                if ($isExport == "excel") {

                    /* if ($total > $export_limit) {
                      $totalExportedFiles = ceil($total / $export_limit);
                      $exportzip = 1;
                      }
                     * */

                    if ($total > $chunksize) {
                        $number_of_pages = ceil($total / $chunksize);
                        //echo $number_of_pages .'--'.$page_counter;
                        //echo "<br>";
                        $page_counter++;
                        $start = $chunksize * $page_counter - $chunksize;
                        $searchParam['start_index'] = $start;
                        $searchParam['page'] = $page_counter;
                    } else {
                        $number_of_pages = 1;
                        $page_counter++;
                    }
                } else {
                    $page_counter = $page;
                    $number_of_pages = ceil($total / $limit);
                    $jsonResult['records'] = $countResult[0]->RecordCount;
                    $jsonResult['page'] = $request->getPost('page');
                    $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
                }
                //$jsonResult['page'] = $request->getPost('page');
                //$jsonResult['records'] = $countResult[0]->RecordCount;
                //$jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
                if (!empty($seachResult)) {
                    $arrExport = array();
                    foreach ($seachResult as $val) {
                        $dashletCell = array();
                        $arrCell['id'] = $val['activity_id'];
                        $encryptId = $this->encrypt($val['activity_id']);
                        $viewLink = '<a href="/get-activity-detail/' . $encryptId . '/' . $this->encrypt('view') . '" class="view-icon" alt="view activity"><div class="tooltip">View<span></span></div></a>';
                        $editLink = '<a href="/get-activity-detail/' . $encryptId . '/' . $this->encrypt('edit') . '" class="edit-icon" alt="edit activity"><div class="tooltip">Edit<span></span></div></a>';
                        $deleteLink = '<a onclick="deleteActivity(\'' . $encryptId . '\')" href="#delete_activity_content" class="delete_activity delete-icon" alt="delete activity"><div class="tooltip">Delete<span></span></div></a>';
                        $actions = '<div class="action-col">' . $viewLink . $editLink . $deleteLink . '</div>';
                        $activityDate = ($val['activity_date_time'] == 0) ? 'N/A' : $this->OutputDateFormat($val['activity_date_time'], 'dateformatampm');
                        $fullName = trim($val['full_name']);
                        $fullName = (!empty($fullName)) ? $val['full_name'] : $val['company_name'];

                        if (false !== $dashletColumnKey = array_search('activity_source_type', $dashletColumnName))
                            $dashletCell[$dashletColumnKey] = '<a href="/get-activity-detail/' . $encryptId . '/' . $this->encrypt('view') . '" class="txt-decoration-underline">' . $val['activity_source_type'] . '</a>';
                        if (false !== $dashletColumnKey = array_search('activity_type', $dashletColumnName))
                            $dashletCell[$dashletColumnKey] = $val['activity_type'];
                        if (false !== $dashletColumnKey = array_search('activity_subject', $dashletColumnName))
                            $dashletCell[$dashletColumnKey] = $this->chunkData($val['activity_subject'], 20);
                        if (false !== $dashletColumnKey = array_search('full_name', $dashletColumnName))
                            $dashletCell[$dashletColumnKey] = $fullName;
                        if (false !== $dashletColumnKey = array_search('activity_date_time', $dashletColumnName))
                            $dashletCell[$dashletColumnKey] = $activityDate;
                        if (false !== $dashletColumnKey = array_search('assigned_to_fullname', $dashletColumnName))
                            $dashletCell[$dashletColumnKey] = $val['assigned_to_fullname'];

                        if (isset($dashlet) && $dashlet == 'dashlet') {
                            $arrCell['cell'] = $dashletCell;
                        } else if ($isExport == "excel") {
                            $arrExport[] = array('Assign To' => $val['assigned_to_fullname'], 'Source' => $val['activity_source_type'], 'Activity Type' => $val['activity_type'], 'Subject' => $val['activity_subject'], 'With' => $fullName, 'Date' => $activityDate);
                        } else {
                            $arrCell['cell'] = array($val['assigned_to_fullname'], $val['activity_source_type'], $val['activity_type'], $this->chunkData($val['activity_subject'], 20), $fullName, $activityDate, $actions);
                        }
                        $subArrCell[] = $arrCell;
                        $jsonResult['rows'] = $subArrCell;
                    }
                    if ($isExport == "excel") {
                        $filename = "activity_export_" . date("Y-m-d") . ".csv";
                        $this->arrayToCsv($arrExport, $filename, $page_counter);
                    }
                } else {
                    $jsonResult['rows'] = array();
                }
            } while ($page_counter <= $number_of_pages && $isExport == "excel");
            if ($isExport == "excel") {
                $this->downloadSendHeaders("activity_export_" . date("Y-m-d") . ".csv");
                die;
            } else {
                $response->setContent(\Zend\Json\Json::encode($jsonResult));
                return $response;
            }
            //asd($jsonResult);
            // return $response->setContent(\Zend\Json\Json::encode($jsonResult));
        } else {
            $this->layout('crm');
            $messages = array();
            if ($this->_flashMessage->hasMessages()) {
                $messages = $this->_flashMessage->getMessages();
            }


            if (isset($params['userid']) && $params['userid'] != '') {
                $common = new Common($this->_adapter);
                $contArr = array();
                $userId = $this->decrypt($params['userid']);
                $contArr['user_id'] = $userId;
                $contactArr = $common->getContactArr($contArr);
                $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getContact($contactArr);
                $searchActivityForm->get('contact_name_id')->setValue($userDetail[0]['user_id']);
                $searchActivityForm->get('contact_name')->setValue($userDetail[0]['full_name']);
            }
            $sourceId = (isset($params['sourceid'])) ? $this->decrypt($params['sourceid']) : '';
            if (!empty($sourceId)) {
                $searchActivityForm->get('activity_source_type_id')->setValue($sourceId);
            }

            $viewModel = new ViewModel();
            $response = $this->getResponse();
            $viewModel->setVariables(array(
                'form' => $searchActivityForm,
                'search_form' => $saveSearchForm,
                'messages' => $messages,
                'jsLangTranslate' => $activitySearchMessage,
                'activityTypeDesc' => $activityTypeDesc
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to create activity
     * @return json 
     * @param void
     * @author Icreon Tech - DT
     */
    public function addActivityAction() {
        $this->checkUserAuthentication();
        $this->getActivityTable();
        $params = $this->params()->fromRoute();
        $activityMessage = $this->_config['activity_messages']['config']['activity_create_message'];
        $lead_create_messages = $this->_config['lead_messages']['config']['lead_search'];
        $caseSearchMessages = $this->_config['case_messages']['config']['case_search_message'];
        $activityCreateMessage = array_merge($activityMessage, $lead_create_messages, $caseSearchMessages);
        $activity = new Activity($this->_adapter);
        $createActivityForm = new ActivityForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        /* Activity type */
        $getActivityType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getActivityType();
        $activityType = array();
        $activityTypeDesc = array();
        $activityType[''] = '';
        if ($getActivityType !== false) {
            foreach ($getActivityType as $key => $val) {
                $activityType[$val['activity_type_id']] = $val['activity_type'];
                $activityTypeDesc[$val['activity_type_id']] = $val['type_description'];
            }
        }
        $createActivityForm->get('activity_type_id')->setAttribute('options', $activityType);
        /* End  Activity type  */
        $createActivityForm->get('follow_up_activity_type_id')->setAttribute('options', $activityType);

        /* Activity status */
        $getActivityStatus = $this->getServiceLocator()->get('Common\Model\CommonTable')->getActivityStatus();
        $activityStatus = array();
        $activityStatus[''] = '';
        if ($getActivityStatus !== false) {
            foreach ($getActivityStatus as $key => $val) {
                $activityStatus[$val['activity_status_id']] = $val['activity_status'];
            }
        }
        $createActivityForm->get('activity_status_id')->setAttribute('options', $activityStatus);
        /* End  Activity status  */

        /* Activity priorities */
        $getActivityPriorities = $this->getServiceLocator()->get('Common\Model\CommonTable')->getActivityPriorities();
        $activityStatus = array();
        $activityPriorities[''] = '';
        if ($getActivityPriorities !== false) {
            foreach ($getActivityPriorities as $key => $val) {
                $activityPriorities[$val['activity_priority_id']] = $val['activity_priority'];
            }
        }
        $createActivityForm->get('activity_priority_id')->setAttribute('options', $activityPriorities);
        /* End  Activity priorities  */
        /* Activity schedule follow up type */
        $getSourceTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getActivitySourceTypes();
        $sourceTypes = array();
        $sourceTypes[''] = 'Select';
        if ($getSourceTypes !== false) {
            foreach ($getSourceTypes as $key => $val) {
                $sourceTypes[$val['activity_source_type_id']] = $val['activity_source_type'];
            }
        }
        $createActivityForm->get('activity_source_type_id')->setAttribute('options', $sourceTypes);
        /* End  Activity schedule follow up type  */

        $numberOfAttachment = $this->_config['file_upload_information']['number_of_attachment'];
        $this->addAttachmentElements($createActivityForm, $numberOfAttachment);

        if ($request->isPost()) {
            $postedData = $request->getPost()->toArray();
            parse_str($postedData['dataStr'], $data);
            $sourceData = $postedData['sourceData'];
            $createActivityForm->setInputFilter($activity->getInputFilterCreateActivity());
            $createActivityForm->setData($data);
            if (!$createActivityForm->isValid()) {
                $errors = $createActivityForm->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'continue') {
                        foreach ($row as $type_error => $rower) {
                            $msg [$key] = $activityCreateMessage[$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            $activitySourceTypeId = $data['activity_source_type_id'];
            $activitySourceId = '';
            if ($activitySourceTypeId != '') {
                $activitySourceId = $sourceData;
                if ($activitySourceTypeId == 2) {
                    #$activitySourceId = $request->getPost('lead_user_name_id');
                    $mesg = 'P_LEAD_CONTACT_EMPTY';
                    $mesgKey = 'lead_user_name';
                } else if ($activitySourceTypeId == 3) {
                    #$activitySourceId = $request->getPost('pledge_user_name_id');
                    $mesg = 'P_PLEDGE_CONTACT_EMPTY';
                    $mesgKey = 'pledge_user_name';
                } else if ($activitySourceTypeId == 4) {
                    #$activitySourceId = $request->getPost('case_subject_id');
                    $mesg = 'P_CASE_SUBJECT_EMPTY';
                    $mesgKey = 'case_subject';
                }
                if ($activitySourceId == '' || empty($activitySourceId)) {
                    $msg = array($mesgKey => $activityCreateMessage[$mesg]);
                    $messages = array('status' => "error", 'message' => $msg);
                }
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $createActivityFormArr = $createActivityForm->getData();
                $createActivityFormArr['added_date'] = DATE_TIME_FORMAT;
                $createActivityFormArr['modified_date'] = DATE_TIME_FORMAT;
                #$createActivityFormArr['activity_source_id'] = '';
                $create_activity_arr = $activity->getCreateActivityArr($createActivityFormArr);
                $create_activity_arr[8] = $this->InputDateFormat($create_activity_arr[8], 'dateformatampm');
                if (isset($create_activity_arr[14]) && !empty($create_activity_arr[14])) {
                    $create_activity_arr[14] = $this->InputDateFormat($create_activity_arr[14], 'dateformatampm');
                }
                $activityId = $this->_activityTable->saveActivity($create_activity_arr);

                /* Activity Source save */
                if (!empty($activitySourceId)) {
                    foreach ($activitySourceId as $key => $source_id) {
                        $sourceInsert = array();
                        $sourceInsert['activity_id'] = $activityId;
                        $sourceInsert['source_id'] = $source_id;
                        $sourceInsert['source_type'] = $activitySourceTypeId;
                        $sourceInsert['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                        $sourceInsert['user_id'] = $createActivityFormArr['contact_name_id'];
                        $sourceInsert['added_date'] = DATE_TIME_FORMAT;
                        $sourceInsert['modified_date'] = DATE_TIME_FORMAT;
                        $this->_activityTable->insertCrmActivitySources($sourceInsert);                        
                    }
                    $delData = array();
                    $delData['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                    $delData['user_session_id'] = session_id();
                    $delData['user_id'] = $createActivityFormArr['contact_name_id'];
                    $this->_activityTable->deleteCrmTmpActivities($delData);
                }
                /* End of Activity Source save */

                /* Activity attachments save */
                $attachmentDetail = array();
                for ($i = 1; $i <= $numberOfAttachment; $i++) {
                    if ($createActivityFormArr['file_name_hidden_' . $i] != '') {
                        $createActivityFormArr['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                        $createActivityFormArr['activity_id'] = $activityId;
                        $createActivityFormArr['file_name'] = $createActivityFormArr['file_name_hidden_' . $i];
                        $createActivityFormArr['is_private'] = $createActivityFormArr['is_private_' . $i];
                        $activityAttachmentArr = $activity->getActivityAttachmentArr($createActivityFormArr);
                        $attachmentId = $this->_activityTable->saveActivityAttachment($activityAttachmentArr);
                        $attachmentDetail[$attachmentId] = $createActivityFormArr['file_name_hidden_' . $i];
                    }
                }
                $this->moveFileFromTempToActivity($attachmentDetail);
                /* End activity attachments save */
                /** insert into the change log */
                $changeLogArray = array();
                $changeLogArray['table_name'] = 'tbl_act_change_logs';
                $changeLogArray['activity'] = '1';
                $changeLogArray['id'] = $activityId;
                $changeLogArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                /** end insert into the change log */
                $this->flashMessenger()->addMessage($activityCreateMessage['ACTIVITY_CREATED_SUCCESSFULLY']);
                $messages = array('status' => "success", 'message' => $activityCreateMessage['ACTIVITY_CREATED_SUCCESSFULLY']);
            }
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        } else {
            $this->layout('crm');
            $crm_user_name = $this->_auth->getIdentity()->crm_first_name . " " . $this->_auth->getIdentity()->crm_last_name;
            $createActivityForm->get('assigned_by_crm_user')->setValue($crm_user_name);
            $createActivityForm->get('assigned_by_crm_user_id')->setValue($this->_auth->getIdentity()->crm_user_id);
            if (isset($params['userid']) && $params['userid'] != '') {
                $common = new Common($this->_adapter);
                $contArr = array();
                $userId = $this->decrypt($params['userid']);
                $contArr['user_id'] = $userId;
                $contactArr = $common->getContactArr($contArr); 
                $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getContact($contArr);
                $createActivityForm->get('contact_name_id')->setValue($userDetail[0]['user_id']);
                $userName = (trim($userDetail[0]['full_name']) == '') ? $userDetail[0]['company_name'] : $userDetail[0]['full_name'];
                $createActivityForm->get('contact_name')->setValue($userName);
            }
            $sourceId = (isset($params['sourceid'])) ? $this->decrypt($params['sourceid']) : '';
            if (!empty($sourceId)) {
                if ($sourceId != 1) {
                    $createActivityForm->get('activity_source_type_id')->setValue($sourceId);
                    $activitySourceId = (isset($params['activitysourceid'])) ? $this->decrypt($params['activitysourceid']) : '';
                    if ($activitySourceId != '') {
                        $sourceActivityDetailArr = array();
                        $sourceActivityDetailArr[] = $sourceId;
                        $sourceActivityDetailArr[] = $activitySourceId;
                        
                        $getActivitySourceDetail = $this->_activityTable->getActivitySourceType($sourceActivityDetailArr);
                        $createActivityForm->get('case_subject')->setValue(@$getActivitySourceDetail['case_subject']);
                        $createActivityForm->get('case_subject_id')->setValue(@$getActivitySourceDetail['case_subject_id']);
                        $createActivityForm->get('pledge_user_name')->setValue(@$getActivitySourceDetail['pledge_user_name']);
                        $createActivityForm->get('pledge_user_name_id')->setValue(@$getActivitySourceDetail['pledge_user_name_id']);
                        $createActivityForm->get('lead_user_name')->setValue(@$getActivitySourceDetail['lead_user_name']);
                        $createActivityForm->get('lead_user_name_id')->setValue(@$getActivitySourceDetail['lead_user_name_id']);
                        
                        $user_session_id = session_id();
                        $crm_user_id = $this->_auth->getIdentity()->crm_user_id;

                        $delData = array();
                        $delData['crm_user_id'] = $crm_user_id;

                        $delData['user_id'] = $userId;
                        $delData['source_type'] = $sourceId;
                        $delData['user_session_id'] = $user_session_id;

                        if (isset($delData['user_session_id']))
                            $this->_activityTable->deleteCrmTmpActivities($delData);

                        $data['crm_user_id'] = $crm_user_id;
                        $data['user_session_id'] = $user_session_id;
                        $data['user_id'] = $userId;
                        $data['source_type'] = $sourceId;
                        $data['source_id'] = $activitySourceId;
                        $data['Location'] = 1;
                        $data['added_date'] = DATE_TIME_FORMAT;
                        $data['modified_date'] = DATE_TIME_FORMAT;
                        $insertedData = $this->_activityTable->insertCrmTmpActivities($data);
                    }
                }
            }
            $user_session_id = session_id();
            $viewModel = new ViewModel();
            $viewModel->setVariables(array(
                'jsLangTranslate' => $activityCreateMessage,
                'number_of_attachment' => $numberOfAttachment,
                'create_activity_form' => $createActivityForm,
                'sourceId' => $sourceId,
                'crm_user_id' => $this->_auth->getIdentity()->crm_user_id,
                'user_session_id' => $user_session_id,
                'activityTypeDesc' => $activityTypeDesc
            ));

            return $viewModel;
        }
    }

    /**
     * This action is used to edit activity
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function editActivityAction() {

        $this->checkUserAuthentication();
        $request = $this->getRequest();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $this->getActivityTable();
            $postedData = $request->getPost()->toArray();
			//asd($postedData);
            parse_str($postedData['dataStr'], $data);
            $sourceData = isset($postedData['sourceData'])?$postedData['sourceData']:array();
            //asd($data['mode'],2);asd($sourceData);
            if ($data['activity_id'] != '') {
                $createActivityForm = new ActivityForm();
                $createActivityForm->remove('activity_type_id');
                /* Activity type */
                $getActivityType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getActivityType();
                $activityType = array();
                $activityTypeDesc = array();
                $activityType[''] = '';
                if ($getActivityType !== false) {
                    foreach ($getActivityType as $key => $val) {
                        $activityType[$val['activity_type_id']] = $val['activity_type'];
                        $activityTypeDesc[$val['activity_type_id']] = $val['type_description'];
                    }
                }
                $createActivityForm->get('follow_up_activity_type_id')->setAttribute('options', $activityType);
                /* End  Activity type  */


                /* Activity status */
                $getActivityStatus = $this->getServiceLocator()->get('Common\Model\CommonTable')->getActivityStatus();
                $activityStatus = array();
                $activityStatus[''] = '';
                if ($getActivityStatus !== false) {
                    foreach ($getActivityStatus as $key => $val) {
                        $activityStatus[$val['activity_status_id']] = $val['activity_status'];
                    }
                }
                $createActivityForm->get('activity_status_id')->setAttribute('options', $activityStatus);
                /* End  Activity status  */

                /* Activity priorities */
                $getActivityPriorities = $this->getServiceLocator()->get('Common\Model\CommonTable')->getActivityPriorities();
                $activityStatus = array();
                $activityPriorities[''] = '';
                if ($getActivityPriorities !== false) {
                    foreach ($getActivityPriorities as $key => $val) {
                        $activityPriorities[$val['activity_priority_id']] = $val['activity_priority'];
                    }
                }
                $createActivityForm->get('activity_priority_id')->setAttribute('options', $activityPriorities);
                /* End  Activity priorities  */
                /* Activity schedule follow up type */
                $getSourceTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getActivitySourceTypes();
                $sourceTypes = array();
                $sourceTypes[''] = 'Select';
                if ($getSourceTypes !== false) {
                    foreach ($getSourceTypes as $key => $val) {
                        $sourceTypes[$val['activity_source_type_id']] = $val['activity_source_type'];
                    }
                }
                $createActivityForm->get('activity_source_type_id')->setAttribute('options', $sourceTypes);

                /* End  Activity schedule follow up type  */
                $fileUploadInformation = $this->_config['file_upload_path'];
                $numberOfAttachment = $this->_config['file_upload_information']['number_of_attachment'];

                $this->addAttachmentElements($createActivityForm, $numberOfAttachment);
                $postArray = $request->getPost()->toArray();



                $activityCreateMessage = $this->_config['activity_messages']['config']['activity_create_message'];

                $activity = new Activity($this->_adapter);

                $response = $this->getResponse();
                $createActivityForm->setInputFilter($activity->getInputFilterEditActivity());
                $createActivityForm->setData($data);
                if (!$createActivityForm->isValid()) {
                    $errors = $createActivityForm->getMessages();
                    $msg = array();
                    foreach ($errors as $key => $row) {
                        if (!empty($row) && $key != 'continue') {
                            foreach ($row as $rower) {
                                $msg [$key] = $activityCreateMessage[$rower];
                            }
                        }
                    }
                    $messages = array('status' => "error", 'message' => $msg);
                }
                $activitySourceTypeId = $data['activity_source_type_id'];
                $activitySourceId = '';
                if ($activitySourceTypeId != '') {
                    $activitySourceId = $sourceData;
                    if ($activitySourceTypeId == 2) {
                        #$activitySourceId = $request->getPost('lead_user_name_id');
                        $mesg = 'P_LEAD_CONTACT_EMPTY';
                        $mesgKey = 'lead_user_name';
                    } else if ($activitySourceTypeId == 3) {
                        #$activitySourceId = $request->getPost('pledge_user_name_id');
                        $mesg = 'P_PLEDGE_CONTACT_EMPTY';
                        $mesgKey = 'pledge_user_name';
                    } else if ($activitySourceTypeId == 4) {
                        #$activitySourceId = $request->getPost('case_subject_id');
                        $mesg = 'P_CASE_SUBJECT_EMPTY';
                        $mesgKey = 'case_subject';
                    }
                    if ($activitySourceId == '' || empty($activitySourceId)) {
                        $msg = array($mesgKey => $activityCreateMessage[$mesg]);
                        $messages = array('status' => "error", 'message' => $msg);
                    }
                }
                if (!empty($messages)) {
                    $response->setContent(\Zend\Json\Json::encode($messages));
                } else {
                    $editActivityFormArr = $createActivityForm->getData();
                    if (isset($editActivityFormArr['activity_id']) && $editActivityFormArr['activity_id'] != '' && $editActivityFormArr['activity_id'] != '0') {
                        $editActivityFormArr['modified_date'] = DATE_TIME_FORMAT;
                        #$editActivityFormArr['activity_source_id'] = $activitySourceId;
                        $editActivityArr = $activity->getEditActivityArr($editActivityFormArr);
                        $editActivityArr[8] = $this->InputDateFormat($editActivityArr[8], 'dateformatampm');
                        $editActivityArr[14] = $this->InputDateFormat($editActivityArr[14], 'dateformatampm');
                        $activityId = $this->_activityTable->editActivity($editActivityArr);
                        $actArray = array();
                        /* Activity Source save */
                        if (!empty($activitySourceId)) {
                            $user_session_id = session_id();
                            $crm_user_id = $this->_auth->getIdentity()->crm_user_id;
                            $delData2 = array();
                            /* $delData2['crm_user_id'] = $crm_user_id;

                              $delData2['user_id'] = $data['contact_name_id'];
                              $delData2['source_type'] = $data['activity_source_type_id']; */
                            $delData2['modified_date'] = DATE_TIME_FORMAT;
                            if (isset($data['mode']) && $data['mode'] == 'edit') {
                                $delData2['activity_id'] = $activityId;
                            }
                            if (isset($delData2['activity_id']) && $delData2['activity_id'] != '') {
                                //asd($delData2,2);
                                $this->_activityTable->deleteCrmRelatedActivities($delData2);
                            }
                            //asd($activitySourceId,2);
                            foreach ($activitySourceId as $key => $source_id) {
                                $sourceInsert = array();
                                $sourceInsert['activity_id'] = $activityId;
                                $sourceInsert['source_id'] = $source_id;
                                $sourceInsert['source_type'] = $activitySourceTypeId;
                                $sourceInsert['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                                $sourceInsert['user_id'] = $editActivityFormArr['contact_name_id'];
                                $sourceInsert['added_date'] = DATE_TIME_FORMAT;
                                $sourceInsert['modified_date'] = DATE_TIME_FORMAT;
                                $insertData = $this->_activityTable->insertCrmActivitySources($sourceInsert);
                                $actArray[] = $insertData[0]['act_source_id'];
                            }
                            if (!empty($actArray)) {
                                $delData = array();
                                $delData['crm_user_id'] = $crm_user_id;
                                $delData['user_session_id'] = $user_session_id;
                                $delData['user_id'] = $editActivityFormArr['contact_name_id'];
                                $this->_activityTable->deleteCrmTmpActivities($delData);
                            }
                        }
                        //die;
                        /* End of Activity Source save */

                        /* Activity attachments save */
                        $activityDetailArr = array();
                        $activityDetailArr[] = $activityId;
                        $activityDetailArr[] = '';
                        $getActivityAttachmentDetail = $this->_activityTable->getActivityAttachmentsById($activityDetailArr);
                        $attachmentDetail = array();
                        for ($i = 1; $i <= $numberOfAttachment; $i++) {
                            if ($editActivityFormArr['file_name_attachment_' . $i] != '') {
                                if ($editActivityFormArr['file_name_hidden_' . $i] != '') {
                                    //For update attachment
                                    $editActivityFormArr['activity_attachment_id'] = $editActivityFormArr['file_name_attachment_' . $i];
                                    $editActivityFormArr['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                                    $editActivityFormArr['file_name'] = $editActivityFormArr['file_name_hidden_' . $i];
                                    $editActivityFormArr['is_private'] = $editActivityFormArr['is_private_' . $i];
                                    $activityEditAttachmentArr = $activity->getActivityEditAttachmentArr($editActivityFormArr);
                                    $attachmentId = $this->_activityTable->editActivityAttachment($activityEditAttachmentArr);
                                    $attachmentDetail[$attachmentId] = $editActivityFormArr['file_name_hidden_' . $i];
                                } else {
                                    //delete activity atttachment
                                    $activityDetailArr[1] = $editActivityFormArr['file_name_attachment_' . $i];
                                    $getActivityAttachmentDeleteDetail = $this->_activityTable->getActivityAttachmentsById($activityDetailArr);
                                    $deleteActivityAttachmentArr = array();
                                    $deleteActivityAttachmentArr[] = $editActivityFormArr['file_name_attachment_' . $i];
                                    $attachmentId = $this->_activityTable->deleteActivityAttachment($deleteActivityAttachmentArr);
                                    $activityDir = $fileUploadInformation['assets_upload_dir'] . "/activities/";
                                    $activityThumbnailDir = $fileUploadInformation['assets_upload_dir'] . "/activities/thumbnail/";
                                    $fileName = $activityDir . $getActivityAttachmentDeleteDetail[0]['file_name'];
                                    $thumbnailFileName = $activityThumbnailDir . $getActivityAttachmentDeleteDetail[0]['file_name'];
                                    if (file_exists($fileName)) {
                                        unlink($fileName);
                                    }
                                    if (file_exists($thumbnailFileName)) {
                                        unlink($thumbnailFileName);
                                    }
                                }
                            } else {
                                //For add new attachment
                                if ($editActivityFormArr['file_name_hidden_' . $i] != '') {
                                    $editActivityFormArr['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                                    $editActivityFormArr['activity_id'] = $activityId;
                                    $editActivityFormArr['file_name'] = $editActivityFormArr['file_name_hidden_' . $i];
                                    $editActivityFormArr['is_private'] = $editActivityFormArr['is_private_' . $i];
                                    $activityAttachmentArr = $activity->getActivityAttachmentArr($editActivityFormArr);
                                    $attachmentId = $this->_activityTable->saveActivityAttachment($activityAttachmentArr);
                                    $attachmentDetail[$attachmentId] = $editActivityFormArr['file_name_hidden_' . $i];
                                }
                            }
                        }
                        $oldAttachmentArr = array();
                        if (!empty($getActivityAttachmentDetail)) {
                            foreach ($getActivityAttachmentDetail as $oldAttachment) {
                                $oldAttachmentArr[$oldAttachment['activity_attachment_id']] = $oldAttachment['file_name'];
                            }
                        }
                        $this->moveFileFromTempToActivity($attachmentDetail, $oldAttachmentArr, 2);
                        /* End activity attachments save */
                        /** insert into the change log */
                        $changeLogArray = array();
                        $changeLogArray['table_name'] = 'tbl_act_change_logs';
                        $changeLogArray['activity'] = '2';
                        $changeLogArray['id'] = $editActivityFormArr['activity_id'];
                        $changeLogArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                        $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                        $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                        $this->flashMessenger()->addMessage($activityCreateMessage['ACTIVITY_UPDATED_SUCCESSFULLY']);
                        $messages = array('status' => "success", 'message' => $activityCreateMessage['ACTIVITY_UPDATED_SUCCESSFULLY']);
                    } else {
                        $this->flashMessenger()->addMessage($activityCreateMessage['ACTIVITY_UPDATED_FAIL']);
                        $messages = array('status' => "fail", 'message' => $activityCreateMessage['ACTIVITY_UPDATED_FAIL']);
                    }
                    $response->setContent(\Zend\Json\Json::encode($messages));
                }
                return $response;
            } else {
                return $this->redirect()->toRoute('getActivities');
            }
        } else {
            return $this->redirect()->toRoute('getActivities');
        }
    }

    /**
     * This Action is used to add attachments elements in form
     * @param form Object,Int number of attachment elements
     * @return void
     * @author Icreon Tech -DT
     */
    public function addAttachmentElements($form, $numberOfAttachments, $oldAttachments = array()) {
        for ($i = 1; $i <= $numberOfAttachments; $i++) {
            $fileName = 'file_name_' . $i;
            $fileNameHidden = 'file_name_hidden_' . $i;
            $isPrivate = 'is_private_' . $i;
            $fileNameAttachment = 'file_name_attachment_' . $i;
            $fileNameHiddenValue = '';
            $isPrivateValue = '0';
            $fileNameAttachmentValue = '';
            if (!empty($oldAttachments) > 0) {
                $fileNameHiddenValue = $oldAttachments[0]['file_name'];
                $isPrivateValue = $oldAttachments[0]['is_private'];
                $fileNameAttachmentValue = $oldAttachments[0]['activity_attachment_id'];
                array_shift($oldAttachments);
            }
            $form->add(array(
                'name' => $fileName,
                'attributes' => array(
                    'type' => 'file',
                    'value' => 'upload',
                    'id' => $fileName,
                    'class' => 'attachmentfiles'
                ),
            ));
            $form->add(array(
                'name' => $fileNameHidden,
                'attributes' => array(
                    'type' => 'hidden',
                    'id' => $fileNameHidden,
                    'value' => $fileNameHiddenValue
                ),
            ));
            $form->add(array(
                'type' => 'Zend\Form\Element\Checkbox',
                'name' => $isPrivate,
                'checked_value' => '1',
                'unchecked_value' => '0',
                'attributes' => array(
                    'class' => 'e2',
                    'id' => $isPrivate,
                    'value' => $isPrivateValue
                )
            ));
            $form->add(array(
                'name' => $fileNameAttachment,
                'attributes' => array(
                    'type' => 'hidden',
                    'id' => $fileNameAttachment,
                    'value' => $fileNameAttachmentValue
                ),
            ));
        }
    }

    /**
     * This Action is used to save the activity search
     * @param this will pass an array $saveSearchParam
     * @return this will be json format with comfirmation message
     * @author Icreon Tech -DT
     */
    public function saveActivitySearchAction() {
        $this->checkUserAuthentication();
        $this->getActivityTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $messages = array();
        $activity = new Activity($this->_adapter);
        $activtySearchMessages = $this->_config['activity_messages']['config']['activity_search_message'];
        if ($request->isPost()) {
            parse_str($request->getPost('searchString'), $searchParam);
            $searchName = $request->getPost('search_name');
            $messages = array();
            parse_str($request->getPost('searchString'), $searchParam);
            $searchParam['search_title'] = $searchName;
            $searchParam = $activity->getInputFilterActivitySearch($searchParam);

            $searchName = $activity->getInputFilterActivitySearch($searchName);

            if (trim($searchName) != '') {
                $saveSearchParam['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $saveSearchParam['title'] = $searchName;
                $saveSearchParam['search_query'] = serialize($searchParam);
                $saveSearchParam['is_active'] = 1;
                $saveSearchParam['is_delete'] = '0';

                if ($request->getPost('search_id') != '') {
                    $saveSearchParam['modified_date'] = DATE_TIME_FORMAT;
                    $saveSearchParam['activity_search_id'] = $request->getPost('search_id');
                    $activity_update_array = $activity->getAddActivitySearchArr($saveSearchParam);
                    if ($this->_activityTable->updateActivitySearch($activity_update_array) == true) {
                        $this->flashMessenger()->addMessage($activtySearchMessages['SAVE_UPDATED_MSG']);
                        $messages = array('status' => "success");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                } else {
                    $saveSearchParam['added_date'] = DATE_TIME_FORMAT;
                    $activityAddArray = $activity->getAddActivitySearchArr($saveSearchParam);
                    if ($this->_activityTable->saveActivitySearch($activityAddArray) == true) {
                        $this->flashMessenger()->addMessage($activtySearchMessages['SAVE_SUCCESS_MSG']);
                        $messages = array('status' => "success");
                        return $response->setContent(\Zend\Json\Json::encode($messages));
                    }
                }
            }
        }
    }

    /**
     * This Action is used to generte the select option after saving the new search title
     * @param this will pass an array $dataParam
     * @return this will be a string having the all options
     * @author Icreon Tech - DT
     */
    public function getSavedActivitySearchSelectAction() {
        $this->checkUserAuthentication();
        $this->getActivityTable();
        $activity = new Activity($this->_adapter);
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $searchArray = array();
            $searchArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
            $searchArray['activity_search_id'] = '';
            $searchArray['is_active'] = '1';
            $activitySavedSearchArray = $activity->getActivitySavedSearchArr($searchArray);
            $activitySearchArray = $this->_activityTable->getActivitySavedSearch($activitySavedSearchArray);
            $options = '';
            foreach ($activitySearchArray as $key => $val) {
                $options .="<option value='" . $val['activity_search_id'] . "'>" . $val['title'] . "</option>";
            }
            return $response->setContent($options);
        }
    }

    /**
     * This Action is used to get the saved activity search
     * @param this will pass an array $dataParam
     * @return this will be json format
     * @author Icreon Tech - DT
     */
    public function getActivitySearchSavedParamAction() {
        $this->checkUserAuthentication();
        $this->getActivityTable();
        $activity = new Activity($this->_adapter);
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            if ($request->getPost('search_id') != '') {

                $searchArray = array();
                $searchArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $searchArray['activity_search_id'] = $request->getPost('search_id');
                $searchArray['is_active'] = '1';
                $activitySavedSearchArray = $activity->getActivitySavedSearchArr($searchArray);
                $activitySearchArray = $this->_activityTable->getActivitySavedSearch($activitySavedSearchArray);

                $searchResult = json_encode(unserialize($activitySearchArray[0]['search_query']));
                return $response->setContent($searchResult);
            }
        }
    }

    /**
     * This Action is used to delete the the saved activity search
     * @param this will take an array $dataParam
     * @return this will be a confirmation message in json
     * @author Icreon Tech - DT
     */
    public function deleteSearchActivityAction() {
        $this->checkUserAuthentication();
        $this->getActivityTable();
        $activity = new Activity($this->_adapter);
        $request = $this->getRequest();
        $response = $this->getResponse();
        $activitySearchMessages = $this->_config['activity_messages']['config']['activity_search_message'];
        if ($request->isPost()) {
            $deleteArray = array();
            $deleteArray['is_deleted'] = 1;
            $deleteArray['modified_date'] = DATE_TIME_FORMAT;
            $deleteArray['activity_search_id'] = $request->getPost('search_id');
            $activityDeleteSavedSearchArray = $activity->getDeleteActivitySavedSearchArr($deleteArray);
            $this->_activityTable->deleteSavedSearch($activityDeleteSavedSearchArray);
            $messages = array('status' => "success");
            $this->flashMessenger()->addMessage($activitySearchMessages['SEARCH_DELETE_CONFIRM']);
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This action is used to delete activity
     * @param  void
     * @return json format string
     * @author Icreon Tech - DT
     */
    public function deleteActivityAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $activityId = $request->getPost('activity_id');
            if ($activityId != '') {
                $activityId = $this->decrypt($activityId);
                $postArr = $request->getPost();
                $postArr['activity_id'] = $activityId;
                $this->getActivityTable();
                $activitySearchMessages = $this->_config['activity_messages']['config']['activity_search_message'];
                $activity = new Activity($this->_adapter);

                $response = $this->getResponse();

                $activityArr = $activity->getArrayForRemoveActivity($postArr);
                $this->_activityTable->removeActivityById($activityArr);

                $delDataRelatedSource = array();
                $delDataRelatedSource['activity_id'] = $activityId;
                $this->_activityTable->deleteCrmRelatedActivities($delDataRelatedSource);

                $changeLogArray = array();
                $changeLogArray['table_name'] = 'tbl_act_change_logs';
                $changeLogArray['activity'] = '3';
                $changeLogArray['id'] = $activityId;
                $changeLogArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
                $changeLogArray['added_date'] = DATE_TIME_FORMAT;
                $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
                /** end insert into the change log */
                $this->flashMessenger()->addMessage($activitySearchMessages['ACTIVITY_DELETED_SUCCESSFULLY']);
                $messages = array('status' => "success", 'message' => $activitySearchMessages['ACTIVITY_DELETED_SUCCESSFULLY']);

                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                return $this->redirect()->toRoute('activities');
            }
        } else {
            return $this->redirect()->toRoute('activities');
        }
    }

    /**
     * This Action is used to set post array elements value to form
     * @param this will pass an array $searchParam
     * @return void
     * @author Icreon Tech -DT
     */
    public function setValueForElement($postArr = array(), $form = array()) {
        if (!is_array($postArr)) {
            $postArr = $postArr->toArray();
        }
        $elements = $form->getElements();
        foreach ($elements as $elementName => $elementObject) {

            if (array_key_exists($elementName, $postArr)) {
                if (is_array($postArr[$elementName])) {
                    $value = implode(',', $postArr[$elementName]);
                } else {

                    $value = $postArr[$elementName];
                }
                $elementObject->setValue($value);
            }
        }
    }

    /**
     * This action is used to show all activity detail
     * @param void
     * @return array
     * @author Icreon Tech - DT
     */
    public function getActivityDetailAction() {
        $this->checkUserAuthentication();
        $this->getActivityTable();
        $activityCreateMessages = $this->_config['activity_messages']['config']['activity_create_message'];
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();
        if (isset($params['activity_id']) && $params['activity_id'] != '') {
            $this->layout('crm');
            $this->getActivityTable();
            $activityId = $this->decrypt($params['activity_id']);
            $activityDetailArr = array();
            $activityDetailArr[] = $activityId;
            $activityData = $this->_activityTable->getActivityById($activityDetailArr);
            $mode = (isset($params['mode']) && $params['mode'] != '') ? $this->decrypt($params['mode']) : 'view';
            $viewModel = new ViewModel();
            $viewModel->setVariables(array(
                'jsLangTranslate' => $activityCreateMessages,
                'activity_id' => $activityId,
                'encrypt_activity_id' => $params['activity_id'],
                'activity_data' => $activityData,
                'module_name' => $this->encrypt('activity'),
                'mode' => $mode,
                'login_user_detail' => $this->_auth->getIdentity()
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to show view activity detail on ajax call
     * @param void
     * @return     array
     * @author Icreon Tech - DT
     */
    public function viewActivityDetailAction() {
        $this->checkUserAuthentication();
        $this->getActivityTable();
        $activityMessage = $this->_config['activity_messages']['config']['activity_create_message'];
        $lead_create_messages = $this->_config['lead_messages']['config']['lead_search'];
        $caseSearchMessages = $this->_config['case_messages']['config']['case_search_message'];
        $activitySearchMessage = array_merge($activityMessage, $lead_create_messages, $caseSearchMessages);
        $activity = new Activity($this->_adapter);
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();
        if (isset($params['activity_id']) && $params['activity_id'] != '') {
            $activityId = $this->decrypt($params['activity_id']);
            $activityDetailArr = array();
            $activityDetailArr[] = $activityId;
            $activityData = $this->_activityTable->getActivityById($activityDetailArr);
            $activityDetailArr[] = '';
            $getActivityAttachmentDetail = $this->_activityTable->getActivityAttachmentsById($activityDetailArr);
            $viewModel = new ViewModel();
            $viewModel->setTerminal(true);
            $viewModel->setVariables(array(
                'jsLangTranslate' => $activitySearchMessage,
                'activity_id' => $activityId,
                'encrypt_activity_id' => $params['activity_id'],
                'activity_data' => $activityData,
                'file_upload_information' => array_merge($this->_config['file_upload_information'], $this->_config['file_upload_path']),
                'activity_attachment_data' => $getActivityAttachmentDetail,
                'login_user_detail' => $this->_auth->getIdentity()
            ));
            return $viewModel;
        }
    }

    /**
     * This action is used to get edit activity detail page
     * @return string
     * @param void
     * @author Icreon Tech - DT
     */
    public function editActivityDetailAction() {
        $this->checkUserAuthentication();
        $this->getActivityTable();
        $activityCreateMessage = $this->_config['activity_messages']['config']['activity_create_message'];
        $activity = new Activity($this->_adapter);
        $createActivityForm = new ActivityForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        /* Activity type */
        $getActivityType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getActivityType();
        $activityType = array();
        $activityTypeDesc = array();
        $activityType[''] = '';
        if ($getActivityType !== false) {
            foreach ($getActivityType as $key => $val) {
                $activityType[$val['activity_type_id']] = $val['activity_type'];
                $activityTypeDesc[$val['activity_type_id']] = $val['type_description'];
            }
        }
        $createActivityForm->get('activity_type_id')->setAttribute('options', $activityType);
        $createActivityForm->get('activity_type_id')->setAttribute('disabled', "disabled");
        /* End  Activity type  */
        $createActivityForm->get('follow_up_activity_type_id')->setAttribute('options', $activityType);

        /* Activity status */
        $getActivityStatus = $this->getServiceLocator()->get('Common\Model\CommonTable')->getActivityStatus();
        $activityStatus = array();
        $activityStatus[''] = '';
        if ($getActivityStatus !== false) {
            foreach ($getActivityStatus as $key => $val) {
                $activityStatus[$val['activity_status_id']] = $val['activity_status'];
            }
        }
        $createActivityForm->get('activity_status_id')->setAttribute('options', $activityStatus);
        /* End  Activity status  */

        /* Activity priorities */
        $getActivityPriorities = $this->getServiceLocator()->get('Common\Model\CommonTable')->getActivityPriorities();
        $activityStatus = array();
        $activityPriorities[''] = '';
        if ($getActivityPriorities !== false) {
            foreach ($getActivityPriorities as $key => $val) {
                $activityPriorities[$val['activity_priority_id']] = $val['activity_priority'];
            }
        }
        $createActivityForm->get('activity_priority_id')->setAttribute('options', $activityPriorities);
        /* End  Activity priorities  */
        /* Activity schedule follow up type */
        $getSourceTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getActivitySourceTypes();
        $sourceTypes = array();
        $sourceTypes[''] = 'Select';
        if ($getSourceTypes !== false) {
            foreach ($getSourceTypes as $key => $val) {
                $sourceTypes[$val['activity_source_type_id']] = $val['activity_source_type'];
            }
        }
        $createActivityForm->get('activity_source_type_id')->setAttribute('options', $sourceTypes);
        /* End  Activity schedule follow up type  */
        $createActivityForm->get('saveactivity')->setValue('UPDATE');



        $activityId = '';
        $params = $this->params()->fromRoute();

        if (isset($params['activity_id']) && $params['activity_id'] != '') {
            $activityId = $this->decrypt($params['activity_id']);
            $activityDetailArr = array();
            $activityDetailArr[] = $activityId;
            $getActivityDetail = $this->_activityTable->getActivityById($activityDetailArr);
            //asd($getActivityDetail);die;
            $activityDetailArr[] = '';
            $getActivityAttachmentDetail = $this->_activityTable->getActivityAttachmentsById($activityDetailArr);
            $numberOfAttachment = $this->_config['file_upload_information']['number_of_attachment'];
            $this->addAttachmentElements($createActivityForm, $numberOfAttachment, $getActivityAttachmentDetail);
            $activityDateTimeArr = explode(' ', $this->OutputDateFormat($getActivityDetail['activity_date_time'], 'dateformatampm'));
            $getActivityDetail['activity_date'] = $activityDateTimeArr[0];
            $getActivityDetail['activity_time'] = $activityDateTimeArr[1] . " " . $activityDateTimeArr[2];
            if (isset($getActivityDetail['follow_up_activity_date']) && !empty($getActivityDetail['follow_up_activity_date']) && $getActivityDetail['follow_up_activity_date'] != '0000-00-00 00:00:00') {
                $follow_activity_date_time_arr = explode(' ', $this->OutputDateFormat($getActivityDetail['follow_up_activity_date'], 'dateformatampm'));
            } else {
                $follow_activity_date_time_arr[0] = 0;
                $follow_activity_date_time_arr[1] = 0;
            }

            $getActivityDetail['follow_up_activity_date'] = ($follow_activity_date_time_arr[0] == 0) ? '' : $follow_activity_date_time_arr[0];
            $getActivityDetail['follow_up_activity_time'] = ($follow_activity_date_time_arr[1] == 0) ? '' : $follow_activity_date_time_arr[1] . " " . $follow_activity_date_time_arr[2];
            $this->setValueForElement($getActivityDetail, $createActivityForm);
            $userName = (!empty($getActivityDetail['USER'])) ? $getActivityDetail['USER'] : $getActivityDetail['company_name'];
            $createActivityForm->get('contact_name')->setValue($userName);
            $createActivityForm->get('contact_name_id')->setValue($getActivityDetail['user_id']);
            $createActivityForm->get('activity_id')->setValue($activityId);
            //asd($this->_config['file_upload_information']);
        }
        $encryptedActivityId = (isset($params['activity_id']) && $params['activity_id'] != '') ? $params['activity_id'] : '';
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => $activityCreateMessage,
            'create_activity_form' => $createActivityForm,
            'activity_id' => $activityId,
            'number_of_attachment' => $numberOfAttachment,
            'encrypted_activity_id' => $encryptedActivityId,
            'file_upload_information' => array_merge($this->_config['file_upload_information'], $this->_config['file_upload_path']),
            'activity_attachment_detail' => $getActivityAttachmentDetail,
            'activityTypeDesc' => $activityTypeDesc,
            'crm_user_id' => $this->_auth->getIdentity()->crm_user_id,
            'user_session_id' => session_id()
        ));

        return $viewModel;
    }

    /**
     * This action is used to upload file for activity
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function uploadAttachmentActivityAction() {
        $this->checkUserAuthentication();
        $fileInputNames = array_keys($_FILES);
        $fileInputName = $fileInputNames[0];
        $fileExt = $this->GetFileExt($_FILES[$fileInputName]['name']);
        $filename = $this->GenerateMediaFileName($fileExt[1]);    //Generate file name
        $_FILES[$fileInputName]['name'] = $filename;                 // assign name to file variable
        $this->getActivityTable();
        $activityCreateMessage = $this->_config['activity_messages']['config']['activity_create_message'];
        $uploadFilePath = $this->_config['file_upload_path'];   // get file upload configuration

        $fileUploadInformation = $this->_config['file_upload_information'];
        $errorMessages = array(
            'post_max_size' => $activityCreateMessage['ATTACHMENT_POST_SIZE_EXCEED'], // Set error messages
            'max_file_size' => sprintf($activityCreateMessage['FILE_SIZE_BIG'], ($fileUploadInformation['max_allowed_file_size'] / 1024 / 1024)),
            'accept_file_types' => $activityCreateMessage['FILE_TYPE_NOT_ALLOWED'],
            2 => sprintf($activityCreateMessage['FILE_SIZE_BIG'], ($fileUploadInformation['max_allowed_file_size'] / 1024 / 1024))
        );
        $options = array(
            'upload_dir' => $uploadFilePath['temp_upload_dir'],
            'param_name' => $fileInputName, //file input name                      // Set configuration
            'inline_file_types' => $fileUploadInformation['file_types_allowed'],
            'accept_file_types' => $fileUploadInformation['file_types_allowed'],
            'max_file_size' => $fileUploadInformation['max_allowed_file_size']
        );
        $uploadHandler = new UploadHandler($options, true, $errorMessages);
        $fileResponse = $uploadHandler->jsonResponceData;
        if (isset($fileResponse[$fileInputName][0]->error)) {
            $messages = array('status' => "error", 'message' => $fileResponse[$fileInputName][0]->error);
        } else {
            $messages = array('status' => "success", 'message' => 'File uploaded', 'filename' => $fileResponse[$fileInputName][0]->name);
        }
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This action is used to move file from temp to activity folder
     * @return json
     * @param 1 for add , 2 for edit
     * @author Icreon Tech - DT
     */
    public function moveFileFromTempToActivity($attachmentDetail, $oldAttachmentDetail = array(), $operation = 1) {
        $this->getActivityTable();
        $uploadFilePath = $this->_config['file_upload_path'];   // get file upload configuration
        $tempDir = $uploadFilePath['temp_upload_dir'];
        $tempThumbnailDir = $uploadFilePath['temp_upload_thumbnail_dir'];
        $activityDir = $uploadFilePath['assets_upload_dir'] . "/activities/";
        $activityThumbnailDir = $uploadFilePath['assets_upload_dir'] . "/activities/thumbnail/";

        foreach ($attachmentDetail as $filename) {
            if ($filename != '') {
                $tempFile = $tempDir . $filename;
                $tempThumbnailFile = $tempThumbnailDir . $filename;
                $activityFilename = $activityDir . $filename;
                $activityThumbnailFilename = $activityThumbnailDir . $filename;

                if (file_exists($tempFile)) {
                    if (copy($tempFile, $activityFilename)) {
                        unlink($tempFile);
                    }
                }
                if (file_exists($tempThumbnailFile)) {
                    if (copy($tempThumbnailFile, $activityThumbnailFilename)) {
                        unlink($tempThumbnailFile);
                    }
                }
                if ($operation == 2) {
                    $removeFiles = array_diff($oldAttachmentDetail, $attachmentDetail);
                    if (count($removeFiles) > 0) {
                        foreach ($removeFiles as $oldfilename) {
                            $activityOldFilename = $activityDir . $oldfilename;
                            $activityOldThumbnailFilename = $activityThumbnailDir . $oldfilename;
                            if (file_exists($activityOldFilename)) {
                                unlink($activityOldFilename);
                            }
                            if (file_exists($activityOldThumbnailFilename)) {
                                unlink($activityOldThumbnailFilename);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * This function is used to get subject autocomplete values
     * @return     json
     * @param void
     * @author Icreon Tech - DT
     */
    public function getActivitySubjectAction() {
        $this->checkUserAuthentication();
        $this->getActivityTable();
        $activity = new Activity($this->_adapter);
        $response = $this->getResponse();
        /* Crm User */
        $request = $this->getRequest();
        $postArr = $request->getPost()->toArray();
        $postArr['sort_field'] = 'modified_date';
        $postArr['sort_order'] = 'desc';
        $searchActivityArr = $activity->getActivitySearchArr($postArr);

        $seachResult = $this->_activityTable->getAllActivities($searchActivityArr);
        $allSubjects = array();
        $i = 0;
        if ($seachResult !== false) {
            foreach ($seachResult as $subject) {
                $allSubjects[$i] = array();
                $allSubjects[$i]['subject'] = $subject['activity_subject'];
                $i++;
            }
        }
        /* End Crm User */
        $response->setContent(\Zend\Json\Json::encode($allSubjects));
        return $response;
    }

    /**
     *  This function is used to download file
     * @param string
     * @return void
     * @author
     * */
    public function downloadActivityFileAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $this->getActivityTable();
        $uploadFilePath = $this->_config['file_upload_path'];   // get file upload configuration
        $activityDir = $uploadFilePath['assets_upload_dir'] . "/activities/";
        $fileName = $activityDir . $params['fileName'];
        if (file_exists($fileName)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($fileName));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($fileName));
            ob_clean();
            flush();
            readfile($fileName);
            exit;
        } else {
            return $this->redirect()->toRoute('activities');
        }
    }

    /**
     * This action is used to search activities for contacts
     * @return json
     * @param void
     * @author Icreon Tech - DT
     */
    public function getContactActivitiesAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        if ($request->isPost() && $request->isXmlHttpRequest()) {
            $this->getActivityTable();
            $activitySearchMessage = $this->_config['activity_messages']['config']['activity_search_message'];
            $activity = new Activity($this->_adapter);
            $searchActivityForm = new SearchActivityForm();
            /* Activity type */
            $getActivityType = $this->getServiceLocator()->get('Common\Model\CommonTable')->getActivityType();
            $activityType = array();
            $activityTypeDesc = array();
            $activityType[''] = 'Select';
            if ($getActivityType !== false) {
                foreach ($getActivityType as $key => $val) {
                    $activityType[$val['activity_type_id']] = $val['activity_type'];
                    $activityTypeDesc[$val['activity_type_id']] = $val['type_description'];
                }
            }
            $searchActivityForm->get('activity_type_id')->setAttribute('options', $activityType);
            /* End  Activity type  */

            /* Get date range */
            $caseDateRange = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTransactionRange();
            $dateRange = array("" => "Select One");
            foreach ($caseDateRange as $key => $val) {
                $dateRange[$val['range_id']] = $val['range'];
            }
            $searchActivityForm->get('added_date_range')->setAttribute('options', $dateRange);
            /* end Get date range */

            /* Activity schedule follow up type */
            $getActivityFollowupTypes = $this->getServiceLocator()->get('Common\Model\CommonTable')->getActivitySourceTypes();
            $activityFollowupTypes = array();
            $activityFollowupTypes[''] = 'Select';
            if ($getActivityFollowupTypes !== false) {
                foreach ($getActivityFollowupTypes as $key => $val) {
                    $activityFollowupTypes[$val['activity_source_type_id']] = $val['activity_source_type'];
                }
            }
            $searchActivityForm->get('activity_source_type_id')->setAttribute('options', $activityFollowupTypes);
            /* End  Activity schedule follow up type  */
            $sortorder = $request->getPost('sord');
            if (!empty($sortorder)) {
                $response = $this->getResponse();
                $postArray = $request->getPost()->toArray();
                parse_str($request->getPost('searchString'), $searchParam);
                $limit = $request->getPost('rows');
                $page = $request->getPost('page');
                $start = $limit * $page - $limit; // do not put $limit*($page - 1)
                $searchParam['start_index'] = $start;
                $searchParam['record_limit'] = $limit;
                $searchParam['sort_field'] = $request->getPost('sidx');
                $searchParam['sort_order'] = $request->getPost('sord');
                if (isset($searchParam['added_date_range']) && $searchParam['added_date_range'] != 1 && $searchParam['added_date_range'] != '') {
                    $dateRange = $this->getDateRange($searchParam['added_date_range']);
                    $searchParam['added_date_from'] = $this->DateFormat($dateRange['from'], 'db_date_format');
                    $searchParam['added_date_to'] = $this->DateFormat($dateRange['to'], 'db_date_format');
                } else if (isset($searchParam['added_date_range']) && $searchParam['added_date_range'] == '') {
                    $searchParam['added_date_from'] = '';
                    $searchParam['added_date_to'] = '';
                } else {
                    if (isset($searchParam['added_date_from']) && $searchParam['added_date_from'] != '') {
                        $searchParam['added_date_from'] = $this->DateFormat($searchParam['added_date_from'], 'db_date_format');
                    }
                    if (isset($searchParam['added_date_to']) && $searchParam['added_date_to'] != '') {
                        $searchParam['added_date_to'] = $this->DateFormat($searchParam['added_date_to'], 'db_date_format');
                    }
                }
                $searchParam['sort_field'] = (isset($searchParam['sort_field']) && $searchParam['sort_field'] == 'modified_date') ? 'tbl_activity.modified_date' : $searchParam['sort_field'];
                if ($searchParam['activity_source_type_id'] == 1) {
                    unset($searchParam['activity_source_type_id']);
                    unset($searchParam['activity_source_id']);
                } else {
                    unset($searchParam['user_id']);
                }
                $searchActivityArr = $activity->getActivitySearchArr($searchParam);
                //asd($searchParam);
                $seachResult = $this->_activityTable->getAllActivities($searchActivityArr);
                $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
                $jsonResult = array();
                $arrCell = array();
                $subArrCell = array();
                $jsonResult['page'] = $request->getPost('page');
                $jsonResult['records'] = $countResult[0]->RecordCount;
                $jsonResult['total'] = ceil($countResult[0]->RecordCount / $limit);
                if (!empty($seachResult)) {
                    foreach ($seachResult as $val) {
                        $arrCell['id'] = $val['activity_id'];
                        $encryptId = $this->encrypt($val['activity_id']);
                        $viewLink = '<a href="/get-activity-detail/' . $encryptId . '/' . $this->encrypt('view') . '" class="view-icon" alt="view activity"><div class="tooltip">View<span></span></div></a>';
                        $editLink = '<a href="/get-activity-detail/' . $encryptId . '/' . $this->encrypt('edit') . '" class="edit-icon" alt="edit activity"><div class="tooltip">Edit<span></span></div></a>';
                        $deleteLink = '<a onclick="deleteActivity(\'' . $encryptId . '\')" href="#delete_activity_content" class="delete_activity delete-icon" alt="delete activity"><div class="tooltip">Delete<span></span></div></a>';
                        $actions = '<div class="action-col">' . $viewLink . $editLink . $deleteLink . '</div>';
                        $modifiedDate = ($val['modified_date'] == 0) ? 'N/A' : $this->OutputDateFormat($val['modified_date'], 'dateformatampm');
                        $arrCell['cell'] = array($val['activity_type'], $this->chunkData($val['activity_subject'], 20), $this->chunkData($val['assigned_by_fullname'], 20), $this->chunkData($val['full_name'], 20), $this->chunkData($val['assigned_to_fullname'], 20), $modifiedDate, $val['activity_status'], $actions);
                        $subArrCell[] = $arrCell;
                        $jsonResult['rows'] = $subArrCell;
                    }
                } else {
                    $jsonResult['rows'] = array();
                }
                return $response->setContent(\Zend\Json\Json::encode($jsonResult));
            } else {
                $userId = $request->getPost('userid');
                if (!empty($userId)) {
                    $userId = $this->decrypt($userId);
                    $searchActivityForm->get('user_id')->setValue($userId);
                }
                $sourceId = $request->getPost('sourceid');
                if (!empty($sourceId)) {
                    $sourceId = $this->decrypt($sourceId);
                    $searchActivityForm->remove('activity_source_type_id');
                    $searchActivityForm->add(array('name' => 'activity_source_type_id',
                        'attributes' => array(
                            'type' => 'hidden',
                            'id' => 'activity_source_type_id',
                    )));
                    $searchActivityForm->get('activity_source_type_id')->setValue($sourceId);
                }
                $sourceActivityId = $request->getPost('sourceactivityid');
                // if (!empty($userId)) {
                $sourceActivityId = $this->decrypt($sourceActivityId);
                $searchActivityForm->get('activity_source_id')->setValue($sourceActivityId);
                //}

                $searchActivityForm->get('searchactivitybutton')->setAttribute('id', 'searchcontactactivitybutton');

                $viewModel = new ViewModel();
                $viewModel->setTerminal(true);
                $response = $this->getResponse();
                $viewModel->setVariables(array(
                    'encrypted_user_id' => $this->encrypt($userId),
                    'encrypted_source_id' => $this->encrypt($sourceId),
                    'encrypted_activity_source_id' => $this->encrypt($sourceActivityId),
                    'form' => $searchActivityForm,
                    'jsLangTranslate' => $activitySearchMessage,
                    'activityTypeDesc' => $activityTypeDesc
                ));
                return $viewModel;
            }
        }
    }

    /**
     * This function is used to get contact autocomplete values for pledge
     * @return     json
     * @param void
     * @author Icreon Tech - DT
     */
    public function getPledgeContactAction() {
        $this->checkUserAuthentication();
        $this->getActivityTable();
        $activities = new Activity($this->_adapter);
        $response = $this->getResponse();
        /* Crm User */
        $request = $this->getRequest();
        $postArr = $request->getPost()->toArray();
        $postArr['sort_field'] = 'tbl_pledge.modified_date';
        $postArr['sort_order'] = 'desc';
        $searchPledgeArr = $activities->getPledgeContactArr($postArr);
        $seachResult = $this->_activityTable->getAllPledgeContact($searchPledgeArr);

        $allContacts = array();
        $i = 0;
        if ($seachResult !== false) {
            foreach ($seachResult as $subject) {
                $fullName = (!empty($subject['company_name_with_id'])) ? $subject['company_name_with_id'] : $subject['full_name_with_id'];
                if (!empty($fullName)) {
                    $allContacts[$i] = array();
                    $allContacts[$i]['id'] = $subject['pledge_id'];
                    $allContacts[$i]['full_name_with_id'] = $fullName;
                    $i++;
                }
            }
        }
        /* End Crm User */
        $response->setContent(\Zend\Json\Json::encode($allContacts));
        return $response;
    }

    public function getActivityLeadsAction() {
        try {
            $this->checkUserAuthentication();
            $this->getActivityTable();
            $response = $this->getResponse();
            $request = $this->getRequest();
            $user_id = $request->getPost('user_id');
            $sourceType = $request->getPost('sourceType');
            $mode = $request->getPost('mode');
            $activity_id = $request->getPost('activity_id');
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit;
            //$searchParam['startIndex'] = $start;
            //$searchParam['recordLimit'] = $limit;
            $searchParam['sortField'] = $request->getPost('sidx');
            $searchParam['sortOrder'] = $request->getPost('sord');
            if ($user_id != '') {
                $searchParam['user_id'] = $user_id;
            }
            //asd($searchParam);
            $get_contact_arr = $this->getServiceLocator()->get('Lead\Model\LeadTable')->getLead($searchParam);
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $total = $countResult[0]->RecordCount;
            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();

            $jsonResult['records'] = isset($countResult[0]->RecordCount) ? $countResult[0]->RecordCount : '';
            $jsonResult['page'] = $page;
            $jsonResult['total'] = ($total == 0) ? 0 : @ceil($countResult[0]->RecordCount / $total);
            $tmpIds = array();
            if (!empty($get_contact_arr)) {
                foreach ($get_contact_arr as $k => $val) {
                    $arrCell['id'] = $val['lead_id'];
                    $status = ($val['status'] == 0) ? 'Closed' : 'Open';
                    $arrCell['cell'] = array($arrCell['id'], $val['lead_type'], $val['lead_source'], $val['opportunity_amount'], $status);
                    $subArrCell[] = $arrCell;
                    $jsonResult['rows'] = $subArrCell;
                }

                $searchParam2 = array();
                $crm_user_id = $this->_auth->getIdentity()->crm_user_id;
                $searchParam2['crm_user_id'] = $crm_user_id;
                $searchParam2['user_id'] = $user_id;
                $searchParam2['source_type'] = $sourceType;
                if ($mode == 'create') {
                    $user_session_id = session_id();
                    $searchParam2['user_session_id'] = $user_session_id;
                    //$searchParam2['startIndex'] = $start;
                    //$searchParam2['recordLimit'] = $limit;
                    $searchParam2['sortField'] = $request->getPost('sidx');
                    $searchParam2['sortOrder'] = $request->getPost('sord');
                    $tmpActs = $this->_activityTable->getCrmTmpActivities($searchParam2);
                } elseif ($mode == 'edit') {
                    $searchParam2['activity_id'] = $activity_id;
                    //$searchParam2['start_Index'] = $start;
                    //$searchParam2['record_limit'] = $limit;
                    $searchParam2['sort_field'] = $request->getPost('sidx');
                    $searchParam2['sort_order'] = $request->getPost('sord');

                    $tmpActs = $this->_activityTable->getCrmRelatedActivitySources($searchParam2);
                }
                if (!empty($tmpActs)) {
                    foreach ($tmpActs as $key => $value) {
                        $tmpIds[] = $value['lead_id'];
                    }
                }
            }
            $jsonResult['selectedIds'] = $tmpIds;
            $response->setContent(\Zend\Json\Json::encode($jsonResult));
            return $response;
        } catch (Exception $e) {
            throw new \Exception('There is some error.');
        }
    }

    public function getActivityPledgesAction() {
        try {
            $this->checkUserAuthentication();
            $this->getActivityTable();
            $response = $this->getResponse();
            $request = $this->getRequest();
            $user_id = $request->getPost('user_id');
            $sourceType = $request->getPost('sourceType');
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $mode = $request->getPost('mode');
            $activity_id = $request->getPost('activity_id');
            $start = $limit * $page - $limit;
            $searchParam = array();
            //$searchParam['start_index'] = $start;
            //$searchParam['record_limit'] = $limit;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            if ($user_id != '') {
                $searchParam['user_id'] = $user_id;
            }
            $pledge = new Pledge($this->_adapter);
            $searchPledgeArr = $pledge->getPledgeSearchArr($searchParam);
            $searchResult = $this->getServiceLocator()->get('Pledge\Model\PledgeTable')->getAllPledge($searchPledgeArr);
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $total = $countResult[0]->RecordCount;
            //$number_of_pages = ceil($total / $limit);
            $jsonResult['records'] = $countResult[0]->RecordCount;
            $jsonResult['page'] = $page;
            $jsonResult['total'] = ($total == 0) ? 0 : ceil($countResult[0]->RecordCount / $total);
			
			$tmpIds = array();
			
            if (!empty($searchResult)) {
                foreach ($searchResult as $val) {
                    $arrCell['id'] = $val['pledge_id'];
                    $encrypt_id = $this->encrypt($val['pledge_id']);
                    $pledgeAddedDate = ($val['added_date'] == 0) ? 'N/A' : $this->OutputDateFormat($val['added_date'], 'dateformatampm');

                    if ($val['pledge_status'] == 1) {
                        $status = "Pending";
                    }
                    if ($val['pledge_status'] == 2) {
                        $status = "Completed";
                    }
                    if ($val['pledge_status'] == 3) {
                        $status = "Canceled";
                    }
                    if ($val['pledge_status'] == 4) {
                        $status = "In progress";
                    }
                    if ($val['pledge_status'] == 5) {
                        $status = "Overdue";
                    }

                    $balanceAmount = isset($val['BalanceAmount']) ? $val['BalanceAmount'] : '0.00';


                    $fullName = (!empty($val['full_name'])) ? $val['full_name'] : $val['company_name'];
                    $arrCell['cell'] = array($arrCell['id'], '<a class="txt-decoration-underline" href="/get-pledge-info/'.$this->encrypt($val['pledge_id']).'/'.$this->encrypt('view').'">'.$val['pledge_id'].'</a>', $val['pledge_amount'], $balanceAmount, $val['pledge_for'], $pledgeAddedDate, $status);
                    $subArrCell[] = $arrCell;
                    $jsonResult['rows'] = $subArrCell;
                }
                $searchParam2 = array();
                $crm_user_id = $this->_auth->getIdentity()->crm_user_id;
                $searchParam2['crm_user_id'] = $crm_user_id;
                $searchParam2['user_id'] = $user_id;
                $searchParam2['source_type'] = $sourceType;
                if ($mode == 'create') {
                    $user_session_id = session_id();
                    $searchParam2['user_session_id'] = $user_session_id;
                    //$searchParam2['startIndex'] = $start;
                   // $searchParam2['recordLimit'] = $limit;
                    $searchParam2['sortField'] = $request->getPost('sidx');
                    $searchParam2['sortOrder'] = $request->getPost('sord');
                    $tmpActs = $this->_activityTable->getCrmTmpActivities($searchParam2);
                } elseif ($mode == 'edit') {
                    $searchParam2['activity_id'] = $activity_id;
                    //$searchParam2['start_Index'] = $start;
                    //$searchParam2['record_limit'] = $limit;
                    $searchParam2['sort_field'] = $request->getPost('sidx');
                    $searchParam2['sort_order'] = $request->getPost('sord');
                    $tmpActs = $this->_activityTable->getCrmRelatedActivitySources($searchParam2);
                }

                if (!empty($tmpActs)) {
                    foreach ($tmpActs as $key => $value) {
                        $tmpIds[] = $value['pledge_id'];
                    }
                }
            } else {
                $jsonResult['rows'] = array();
            }
            $jsonResult['selectedIds'] = $tmpIds;
            $response->setContent(\Zend\Json\Json::encode($jsonResult));
            return $response;
        } catch (Exception $e) {
            throw new \Exception('There is some error.');
        }
    }

    public function getActivityCasesAction() {
        try {
            $this->checkUserAuthentication();
            $this->getActivityTable();
            $response = $this->getResponse();
            $request = $this->getRequest();
            $user_id = $request->getPost('user_id');
            $sourceType = $request->getPost('sourceType');
            $mode = $request->getPost('mode');
            $activity_id = $request->getPost('activity_id');
            $limit = $request->getPost('rows');
			$page = $request->getPost('page');
            $start = $limit * $page - $limit;
            $searchParam = array();
            //$searchParam['start_index'] = $start;
            //$searchParam['record_limit'] = $limit;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            if ($user_id != '') {
                $searchParam['contact_id'] = $user_id;
            }
            $cases = new Cases($this->_adapter);
			$searchCaseArr = $cases->getCaseSearchArr($searchParam);
            $searchResult = $this->getServiceLocator()->get('Cases\Model\CasesTable')->getAllCase($searchCaseArr);
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $total = $countResult[0]->RecordCount;
            //$number_of_pages = ceil($total / $limit);
            $jsonResult['records'] = $countResult[0]->RecordCount;
            $jsonResult['page'] = $page;
            $jsonResult['total'] = ($total == 0) ? 0 : ceil($countResult[0]->RecordCount / $total);
			
			$tmpIds = array();
                
            if (!empty($searchResult)) {
                foreach ($searchResult as $val) {
                    $arrCell['id'] = $val['case_contact_id'];
                    $encryptId = $this->encrypt($val['case_contact_id']);
                    $caseStartDate = ($val['case_start_date'] == 0) ? 'N/A' : $this->OutputDateFormat($val['case_start_date'], 'dateformatampm');
                    $arrCell['cell'] = array($arrCell['id'], '<a class="txt-decoration-underline" href="/get-case-detail/'.$this->encrypt($val['case_contact_id']).'/'.$this->encrypt('view').'">'.$val['case_contact_id'].'</a>', $this->chunkData($val['case_subject'], 20), $val['user_transaction_id'], $caseStartDate, $val['case_type'], $val['case_status']);
                    $subArrCell[] = $arrCell;
                    $jsonResult['rows'] = $subArrCell;
                }

                $searchParam2 = array();
                $crm_user_id = $this->_auth->getIdentity()->crm_user_id;
                $searchParam2['crm_user_id'] = $crm_user_id;
                $searchParam2['user_id'] = $user_id;
                $searchParam2['source_type'] = $sourceType;
                if ($mode == 'create') {
                    $user_session_id = session_id();
                    $searchParam2['user_session_id'] = $user_session_id;
                    //$searchParam2['startIndex'] = $start;
                    //$searchParam2['recordLimit'] = $limit;
                    $searchParam2['sortField'] = $request->getPost('sidx');
                    $searchParam2['sortOrder'] = $request->getPost('sord');
                    $tmpActs = $this->_activityTable->getCrmTmpActivities($searchParam2);
                } elseif ($mode == 'edit') {
                    $searchParam2['activity_id'] = $activity_id;
                    //$searchParam2['start_Index'] = $start;
                    //$searchParam2['record_limit'] = $limit;
                    $searchParam2['sort_field'] = $request->getPost('sidx');
                    $searchParam2['sort_order'] = $request->getPost('sord');

                    $tmpActs = $this->_activityTable->getCrmRelatedActivitySources($searchParam2);
                }

                if (!empty($tmpActs)) {
                    foreach ($tmpActs as $key => $value) {
                        $tmpIds[] = $value['case_contact_id'];
                    }
                }
            } else {
                $jsonResult['rows'] = array();
            }
            $jsonResult['selectedIds'] = $tmpIds;
            $response->setContent(\Zend\Json\Json::encode($jsonResult));
            return $response;
        } catch (Exception $e) {
            throw new \Exception('There is some error.');
        }
    }

    public function createTempActivityAction() {
        try {
            $this->checkUserAuthentication();
            $this->getActivityTable();
            $response = $this->getResponse();
            $request = $this->getRequest();
            $postedData = $request->getPost()->toArray();
            $sourceType = $postedData['sourceType'];
            $tmpActivities = $postedData['tmpActivities'];
            $user_id = $postedData['user_id'];
            $mode = $request->getPost('mode');
            $activity_id = $request->getPost('activity_id');
            $source = array();
            foreach ($tmpActivities as $key => $value) {
                $source[] = (int) str_replace('jqg_list_case_', '', $value);
            }
            $data = array();
            $insertedDataArray = array();
            if (!empty($source)) {
                $user_session_id = session_id();
                $crm_user_id = $this->_auth->getIdentity()->crm_user_id;

                $delData = array();
                $delData['crm_user_id'] = $crm_user_id;

                $delData['user_id'] = $user_id;
                $delData['source_type'] = $sourceType;
                $delData['user_session_id'] = $user_session_id;

                if (isset($mode) && $mode == 'edit') {
                    $delData['activity_id'] = $activity_id;
                }

                if (isset($delData['user_session_id']))
                    $this->_activityTable->deleteCrmTmpActivities($delData);

                /* if(isset($delData['activity_id']) && $delData['activity_id']!=''){
                  $this->_activityTable->deleteCrmRelatedActivities($delData);
                  } */

                foreach ($source as $index => $val) {
                    $data['crm_user_id'] = $crm_user_id;
                    $data['user_session_id'] = $user_session_id;
                    $data['user_id'] = $user_id;
                    $data['source_type'] = $sourceType;
                    $data['source_id'] = $val;
                    $data['Location'] = 1;
                    $data['added_date'] = DATE_TIME_FORMAT;
                    $data['modified_date'] = DATE_TIME_FORMAT;
                    $insertedData = $this->_activityTable->insertCrmTmpActivities($data);
                    $insertedDataArray[] = $insertedData[0]['tmp_activity_id'];
                }
                if (count($source) == count($insertedDataArray)) {
                    $messages = array('status' => "success", 'crm_user_id' => $crm_user_id, 'user_session_id' => $user_session_id, 'user_id' => $user_id, 'source_type' => $sourceType);
                    $response->setContent(\Zend\Json\Json::encode($messages));
                    return $response;
                }
            }
        } catch (Exception $e) {
            throw new \Exception('There is some error.');
        }
    }

    public function getTempActivityAction() {
        try {
            $this->checkUserAuthentication();
            $this->getActivityTable();
            $response = $this->getResponse();
            $request = $this->getRequest();
            $crm_user_id = $request->getPost('crm_user_id');
            $user_id = $request->getPost('user_id');
            $user_session_id = $request->getPost('user_session_id');
            $source_type = $request->getPost('source_type');
            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit;
            $searchParam = array();
            $searchParam['crm_user_id'] = $crm_user_id;
            $searchParam['user_session_id'] = $user_session_id;
            $searchParam['user_id'] = $user_id;
            $searchParam['source_type'] = $source_type;
            //$searchParam['startIndex'] = $start;
            //$searchParam['recordLimit'] = $limit;
            $searchParam['sortField'] = $request->getPost('sidx');
            $searchParam['sortOrder'] = $request->getPost('sord');

            $get_contact_arr = $this->_activityTable->getCrmTmpActivities($searchParam);
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $total = $countResult[0]->RecordCount;

            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();

            $jsonResult['records'] = isset($countResult[0]->RecordCount) ? $countResult[0]->RecordCount : '';
            $jsonResult['page'] = $page;
            $jsonResult['total'] = ($total == 0) ? 0 : @ceil($countResult[0]->RecordCount / $total);

            if (!empty($get_contact_arr)) {
                foreach ($get_contact_arr as $k => $val) {
                    $delete_link = "<a href='javascript:void(0);' onclick='deleteTempSource(" . $val['tmp_activity_id'] . ");' class='delete_temp_content delete-icon'>Delete<div class='tooltip'>Delete<span></span></div></a>";
                    $action = '<div class="action-col">' . $delete_link . '</div>';
                    if ($source_type == 2) {
                        $arrCell['id'] = $val['lead_id'];
                        $status = ($val['status'] == 0) ? 'Closed' : 'Open';
                        $arrCell['cell'] = array($arrCell['id'], $val['lead_type'], $val['lead_source'], $val['opportunity_amount'], $status, $action);
                    } elseif ($source_type == 3) {
                        $arrCell['id'] = $val['pledge_id'];
                        $encrypt_id = $this->encrypt($val['pledge_id']);
                        $pledgeAddedDate = ($val['added_date'] == 0) ? 'N/A' : $this->OutputDateFormat($val['added_date'], 'dateformatampm');

                        if ($val['pledge_status'] == 1) {
                            $status = "Pending";
                        }
                        if ($val['pledge_status'] == 2) {
                            $status = "Completed";
                        }
                        if ($val['pledge_status'] == 3) {
                            $status = "Canceled";
                        }
                        if ($val['pledge_status'] == 4) {
                            $status = "In progress";
                        }
                        if ($val['pledge_status'] == 5) {
                            $status = "Overdue";
                        }

                        $balanceAmount = isset($val['BalanceAmount']) ? $val['BalanceAmount'] : '0.00';
                        $arrCell['cell'] = array($arrCell['id'], '<a class="txt-decoration-underline" href="/get-pledge-info/'.$this->encrypt($val['pledge_id']).'/'.$this->encrypt('view').'">'.$val['pledge_id'].'</a>', $val['pledge_amount'], $balanceAmount, $val['pledge_for'], $pledgeAddedDate, $status, $action);
                    } elseif ($source_type == 4) {
                        $arrCell['id'] = $val['case_contact_id'];
                        $encryptId = $this->encrypt($val['case_contact_id']);
                        $caseStartDate = ($val['case_start_date'] == 0) ? 'N/A' : $this->OutputDateFormat($val['case_start_date'], 'dateformatampm');
                        $arrCell['cell'] = array($arrCell['id'], '<a class="txt-decoration-underline" href="/get-case-detail/'.$this->encrypt($val['case_contact_id']).'/'.$this->encrypt('view').'">'.$val['case_contact_id'].'</a>', $this->chunkData($val['case_subject'], 20), $val['user_transaction_id'], $caseStartDate, $val['case_type'], $val['case_status'], $action);
                    }

                    $subArrCell[] = $arrCell;
                    $jsonResult['rows'] = $subArrCell;
                }
            }
            $response->setContent(\Zend\Json\Json::encode($jsonResult));
            return $response;
        } catch (Exception $e) {
            throw new \Exception('There is some error.');
        }
    }

    public function deleteTempActivityAction() {
        try {
            $this->checkUserAuthentication();
            $this->getActivityTable();
            $response = $this->getResponse();
            $request = $this->getRequest();
            $postedData = $request->getPost()->toArray();
            $postedData['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
            $this->_activityTable->deleteCrmTmpActivities($postedData);
            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        } catch (Exception $e) {
            throw new \Exception('There is some error.');
        }
    }

    public function countRelatedTempActivitySourcesAction() {
        try {
            $this->checkUserAuthentication();
            $this->getActivityTable();
            $response = $this->getResponse();
            $request = $this->getRequest();
            $postedData = $request->getPost()->toArray();
            $searchParam2 = array();
            $searchParam2['crm_user_id'] = $postedData['crm_user_id'];
            $searchParam2['user_session_id'] = $postedData['user_session_id'];
            $searchParam2['user_id'] = $postedData['user_id'];
            $searchParam2['source_type'] = $postedData['sourceType'];
            $searchParam2['startIndex'] = '';
            $searchParam2['recordLimit'] = '';
            $searchParam2['sortField'] = '';
            $searchParam2['sortOrder'] = '';

            $tmpIds = array();
            $tmpActs = $this->_activityTable->getCrmTmpActivities($searchParam2);
            if (!empty($tmpActs)) {
                foreach ($tmpActs as $key => $value) {
                    if ($postedData['sourceType'] == 2)
                        $tmpIds[] = $value['lead_id'];
                    elseif ($postedData['sourceType'] == 3)
                        $tmpIds[] = $value['pledge_id'];
                    elseif ($postedData['sourceType'] == 4)
                        $tmpIds[] = $value['case_contact_id'];
                }
            }
            $messages = array('status' => "success", 'relatedCount' => count($tmpIds));
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        } catch (Exception $e) {
            throw new \Exception('There is some error.');
        }
    }

    public function viewRelatedActivitySourcesAction() {
        $this->checkUserAuthentication();
        $this->getActivityTable();
        $params = $this->params()->fromRoute();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $activityMessage = $this->_config['activity_messages']['config']['activity_create_message'];
        $lead_create_messages = $this->_config['lead_messages']['config']['lead_search'];
        $caseSearchMessages = $this->_config['case_messages']['config']['case_search_message'];
        $activitySearchMessage = array_merge($activityMessage, $lead_create_messages, $caseSearchMessages);
        if ($request->isPost()) {
            $crm_user_id = $this->_auth->getIdentity()->crm_user_id;
            $source_type = $request->getPost('sourceType');

            $mode = $request->getPost('mode');
            if (isset($mode) && $mode == 'edit') {
                $user_id = $request->getPost('user_id');
                $activity_id = $request->getPost('activity_id');
            } else {
                $user_id = $this->decrypt($request->getPost('user_id'));
                $activity_id = $this->decrypt($request->getPost('activity_id'));
            }

            $limit = $request->getPost('rows');
            $page = $request->getPost('page');
            $start = $limit * $page - $limit;

            $searchParam = array();
            $searchParam['crm_user_id'] = $crm_user_id;
            $searchParam['activity_id'] = $activity_id;
            $searchParam['user_id'] = $user_id;
            $searchParam['source_type'] = $source_type;
            //$searchParam['start_Index'] = $start;
            //$searchParam['record_limit'] = $limit;
            $searchParam['sort_field'] = $request->getPost('sidx');
            $searchParam['sort_order'] = $request->getPost('sord');
            //asd($searchParam);
            $searchResult = $this->_activityTable->getCrmRelatedActivitySources($searchParam);
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $total = $countResult[0]->RecordCount;

            $jsonResult = array();
            $arrCell = array();
            $subArrCell = array();
            $jsonResult['records'] = $countResult[0]->RecordCount;
            $jsonResult['page'] = $page;
            $jsonResult['total'] = ($total==0) ? 0 : ceil($countResult[0]->RecordCount / $total);
            if (!empty($searchResult)) {
                foreach ($searchResult as $val) {
                    if (isset($mode) && $mode == 'edit') {
                        $delete_link = "<a href='javascript:void(0);' onclick='deleteRelatedSource(" . $val['act_source_id'] . ");' class='delete_temp_content delete-icon'>Delete<div class='tooltip'>Delete<span></span></div></a>";
                        $action = '<div class="action-col">' . $delete_link . '</div>';
                    }
                    if ($source_type == 4) {
                        $arrCell['id'] = $val['case_contact_id'];
                        $encryptId = $this->encrypt($val['case_contact_id']);
                        $caseStartDate = ($val['case_start_date'] == 0) ? 'N/A' : $this->OutputDateFormat($val['case_start_date'], 'dateformatampm');
                        $arrCell['cell'] = array('<a class="txt-decoration-underline" href="/get-case-detail/'.$this->encrypt($val['case_contact_id']).'/'.$this->encrypt('view').'">'.$val['case_contact_id'].'</a>', $this->chunkData($val['case_subject'], 20), $val['user_transaction_id'], $caseStartDate, $val['case_type'], $val['case_status']);
                    } elseif ($source_type == 3) {
                        $arrCell['id'] = $val['pledge_id'];
                        $encrypt_id = $this->encrypt($val['pledge_id']);
                        $pledgeAddedDate = ($val['added_date'] == 0) ? 'N/A' : $this->OutputDateFormat($val['added_date'], 'dateformatampm');

                        if ($val['pledge_status'] == 1) {
                            $status = "Pending";
                        }
                        if ($val['pledge_status'] == 2) {
                            $status = "Completed";
                        }
                        if ($val['pledge_status'] == 3) {
                            $status = "Canceled";
                        }
                        if ($val['pledge_status'] == 4) {
                            $status = "In progress";
                        }
                        if ($val['pledge_status'] == 5) {
                            $status = "Overdue";
                        }

                        $balanceAmount = isset($val['BalanceAmount']) ? $val['BalanceAmount'] : '0.00';
                        $arrCell['cell'] = array('<a class="txt-decoration-underline" href="/get-pledge-info/'.$this->encrypt($val['pledge_id']).'/'.$this->encrypt('view').'">'.$val['pledge_id'].'</a>', $val['pledge_amount'], $balanceAmount, $val['pledge_for'], $pledgeAddedDate, $status);
                    } elseif ($source_type == 2) {
                        $arrCell['id'] = $val['lead_id'];
                        $status = ($val['status'] == 0) ? 'Closed' : 'Open';
                        $arrCell['cell'] = array($val['lead_type'], $val['lead_source'], $val['opportunity_amount'], $status);
                    }
                    if (isset($action))
                        array_push($arrCell['cell'], $action);
                    $subArrCell[] = $arrCell;
                    $jsonResult['rows'] = $subArrCell;
                }
            } else {
                $jsonResult['rows'] = array();
            }
            $response->setContent(\Zend\Json\Json::encode($jsonResult));
            return $response;
        }
    }
    
    public function deleteRelatedActivitySourceAction(){
        try {
           $this->checkUserAuthentication();
            $this->getActivityTable();
            $params = $this->params()->fromRoute();
            $request = $this->getRequest();
            $response = $this->getResponse();
            if ($request->isPost()) {
                $act_source_id = $request->getPost('act_source_id');
                $activity_id = $request->getPost('activity_id');
                $delData = array();
                $delData['act_source_id'] = $act_source_id;
                $delData['activity_id'] = $activity_id;
                $this->_activityTable->deleteCrmRelatedActivities($delData);
                $messages = array('status' => "success");
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } 
        } catch (Exception $e) {
            throw new \Exception('There is some error.');
        }
        
    }

}
