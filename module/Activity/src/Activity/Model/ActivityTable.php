<?php

/**
 * This model is used for Activity module database related work.
 * @package    Activity
 * @author     Icreon Tech - DT
 */

namespace Activity\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This class is used for Activity module database related work.
 * @package    Activity_Activity
 * @author     Icreon Tech - DT
 */
class ActivityTable {

    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
    }

    /**
     * Function for insert activity data
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function saveActivity($activityData) {
        $procquesmarkapp = $this->appendQuestionMars(count($activityData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_act_insertActivity(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $activityData[0]);
        $stmt->getResource()->bindParam(2, $activityData[1]);
        $stmt->getResource()->bindParam(3, $activityData[2]);
        $stmt->getResource()->bindParam(4, $activityData[3]);
        $stmt->getResource()->bindParam(5, $activityData[4]);
        $stmt->getResource()->bindParam(6, $activityData[5]);
        $stmt->getResource()->bindParam(7, $activityData[6]);
        $stmt->getResource()->bindParam(8, $activityData[7]);
        $stmt->getResource()->bindParam(9, $activityData[8]);
        $stmt->getResource()->bindParam(10, $activityData[9]);
        $stmt->getResource()->bindParam(11, $activityData[10]);
        $stmt->getResource()->bindParam(12, $activityData[11]);
        $stmt->getResource()->bindParam(13, $activityData[12]);
        $stmt->getResource()->bindParam(14, $activityData[13]);
        $stmt->getResource()->bindParam(15, $activityData[14]);
        $stmt->getResource()->bindParam(16, $activityData[15]);
        $stmt->getResource()->bindParam(17, $activityData[16]);
        $stmt->getResource()->bindParam(18, $activityData[17]);

        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for insert activity attachment data
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function saveActivityAttachment($activityData) {
        $procquesmarkapp = $this->appendQuestionMars(count($activityData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_act_insertAttachment(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $activityData[0]);
        $stmt->getResource()->bindParam(2, $activityData[1]);
        $stmt->getResource()->bindParam(3, $activityData[2]);
        $stmt->getResource()->bindParam(4, $activityData[3]);
        $stmt->getResource()->bindParam(5, $activityData[4]);
        $stmt->getResource()->bindParam(6, $activityData[5]);

        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for update activity attachment data
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function editActivityAttachment($activityData) {
        $procquesmarkapp = $this->appendQuestionMars(count($activityData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_act_updateAttachment(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $activityData[0]);
        $stmt->getResource()->bindParam(2, $activityData[1]);
        $stmt->getResource()->bindParam(3, $activityData[2]);
        $stmt->getResource()->bindParam(4, $activityData[3]);
        $stmt->getResource()->bindParam(5, $activityData[4]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for update activity attachment data
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function deleteActivityAttachment($activityData) {
        $procquesmarkapp = $this->appendQuestionMars(count($activityData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_act_deleteAttachment(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $activityData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for edit activity data
     * @author Icreon Tech - DT
     * @return Int
     * @param Array
     */
    public function editActivity($activityData) {
        //asd($activityData);
        $procquesmarkapp = $this->appendQuestionMars(count($activityData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_act_updateActivity(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $activityData[0]);
        $stmt->getResource()->bindParam(2, $activityData[1]);
        $stmt->getResource()->bindParam(3, $activityData[2]);
        $stmt->getResource()->bindParam(4, $activityData[3]);
        $stmt->getResource()->bindParam(5, $activityData[4]);
        $stmt->getResource()->bindParam(6, $activityData[5]);
        $stmt->getResource()->bindParam(7, $activityData[6]);
        $stmt->getResource()->bindParam(8, $activityData[7]);
        $stmt->getResource()->bindParam(9, $activityData[8]);
        $stmt->getResource()->bindParam(10, $activityData[9]);
        $stmt->getResource()->bindParam(11, $activityData[10]);
        $stmt->getResource()->bindParam(12, $activityData[11]);
        $stmt->getResource()->bindParam(13, $activityData[12]);
        $stmt->getResource()->bindParam(14, $activityData[13]);
        $stmt->getResource()->bindParam(15, $activityData[14]);
        $stmt->getResource()->bindParam(16, $activityData[15]);
        $stmt->getResource()->bindParam(17, $activityData[16]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['LAST_INSERT_ID'];
        } else {
            throw new \Exception('There is some error.');
        }
    }

    /**
     * Function for get activities data
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function getAllActivities($activityData) {

        $procquesmarkapp = $this->appendQuestionMars(count($activityData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_act_getActivities(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $activityData[0]);
        $stmt->getResource()->bindParam(2, $activityData[1]);
        $stmt->getResource()->bindParam(3, $activityData[2]);
        $stmt->getResource()->bindParam(4, $activityData[3]);
        $stmt->getResource()->bindParam(5, $activityData[4]);
        $stmt->getResource()->bindParam(6, $activityData[5]);
        $stmt->getResource()->bindParam(7, $activityData[6]);
        $stmt->getResource()->bindParam(8, $activityData[7]);
        $stmt->getResource()->bindParam(9, $activityData[8]);
        $stmt->getResource()->bindParam(10, $activityData[9]);
        $stmt->getResource()->bindParam(11, $activityData[10]);
        $stmt->getResource()->bindParam(12, $activityData[11]);
        $stmt->getResource()->bindParam(13, $activityData[12]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * This function is used to get the saved search listing for activity search
     * @param this will be an array.
     * @return this will return a title of the saved search
     * @author Icreon Tech -DT
     */
    public function getActivitySavedSearch($activityData = array()) {
        $procquesmarkapp = $this->appendQuestionMars(count($activityData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_act_getActivitySavedSearch(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $activityData[0]);
        $stmt->getResource()->bindParam(2, $activityData[1]);
        $stmt->getResource()->bindParam(3, $activityData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
    }

    /**
     * This function will save the search title into the table for activity search
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech -DT
     */
    public function saveActivitySearch($activityData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($activityData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_act_insertCrmSearchSave(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $activityData[0]);
            $stmt->getResource()->bindParam(2, $activityData[1]);
            $stmt->getResource()->bindParam(3, $activityData[2]);
            $stmt->getResource()->bindParam(4, $activityData[3]);
            $stmt->getResource()->bindParam(5, $activityData[4]);
            $stmt->getResource()->bindParam(6, $activityData[5]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update the search title into the table for activity search
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech -DT
     */
    public function updateActivitySearch($activityData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($activityData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_act_updateCrmSearchSave(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $activityData[0]);
            $stmt->getResource()->bindParam(2, $activityData[1]);
            $stmt->getResource()->bindParam(3, $activityData[2]);
            $stmt->getResource()->bindParam(4, $activityData[3]);
            $stmt->getResource()->bindParam(5, $activityData[4]);
            $stmt->getResource()->bindParam(6, $activityData[5]);
            $stmt->getResource()->bindParam(7, $activityData[6]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will delete the saved activity search titles
     * @param this will be an array having the params.
     * @return this will return a confirmation
     * @author Icreon Tech -DT
     */
    public function deleteSavedSearch($activityData = array()) {
        try {
            $procquesmarkapp = $this->appendQuestionMars(count($activityData));
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_act_deleteCrmSearchSave(" . $procquesmarkapp . ")");
            $stmt->getResource()->bindParam(1, $activityData[0]);
            $stmt->getResource()->bindParam(2, $activityData[1]);
            $stmt->getResource()->bindParam(3, $activityData[2]);
            $result = $stmt->execute();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function for remove activity
     * @author Icreon Tech - DT
     * @return Void
     * @param Array
     */
    public function removeActivityById($activityData) {
        $procquesmarkapp = $this->appendQuestionMars(count($activityData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_act_deleteActivity(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $activityData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * Function for get activity data on the base of id
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getActivityById($activityData) {
        $procquesmarkapp = $this->appendQuestionMars(count($activityData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_act_getActivityDetails(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $activityData[0]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet[0];
        else
            return false;
    }

    /**
     * Function for get activity source type data for pre fill
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getActivitySourceType($activityData) {
        $procquesmarkapp = $this->appendQuestionMars(count($activityData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_act_getActivitySourceDetails(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $activityData[0]);
        $stmt->getResource()->bindParam(2, $activityData[1]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet[0];
        else
            return false;
    }

    /* Function for get activity count
     * @author Icreon Tech- DT
     * param void
     * return array
     */

    public function getActivityCount($param) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getActivityCount(?,?,?)");
        $stmt->getResource()->bindParam(1, $param['sourceType']);
        $stmt->getResource()->bindParam(2, $param['activitySourceId']);
        $stmt->getResource()->bindParam(3, $param['userId']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
			return count($resultSet);
        } else {
            return false;
        }
    }

    /**
     * Function for get all pledge data
     * @author Icreon Tech - DT
     * @return Array
     * @param Array
     */
    public function getAllPledgeContact($activityData) {

        $procquesmarkapp = $this->appendQuestionMars(count($activityData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_ple_getPledgeContact(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $activityData[0]);
        $stmt->getResource()->bindParam(2, $activityData[1]);
        $stmt->getResource()->bindParam(3, $activityData[2]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for get activity attachment data on the base of activity id
     * @author Icreon Tech - DT
     * @return array
     * @param Array
     */
    public function getActivityAttachmentsById($activityData) {
        $procquesmarkapp = $this->appendQuestionMars(count($activityData));
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_act_getActivityAttachmentDetails(" . $procquesmarkapp . ")");
        $stmt->getResource()->bindParam(1, $activityData[0]);
        $stmt->getResource()->bindParam(2, $activityData[1]);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    /**
     * Function for append number of questions marks for stored procedure
     * @author Icreon Tech - DT
     * @return String
     * @param Array
     */
    public function appendQuestionMars($count) {
        $returnstr = '';
        for ($i = 1; $i <= $count; $i++) {
            $returnstr.='?,';
        }
        return rtrim($returnstr, ',');
    }

    public function insertCrmTmpActivities($data = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_act_insertCrmTmpActivities(?,?,?,?,?,?,?,?)");
        $stmt->getResource()->bindParam(1, $data['crm_user_id']);
        $stmt->getResource()->bindParam(2, $data['user_session_id']);
        $stmt->getResource()->bindParam(3, $data['user_id']);
        $stmt->getResource()->bindParam(4, $data['source_type']);
        $stmt->getResource()->bindParam(5, $data['source_id']);
        $stmt->getResource()->bindParam(6, $data['Location']);
        $stmt->getResource()->bindParam(7, $data['added_date']);
        $stmt->getResource()->bindParam(8, $data['modified_date']);

        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    public function getCrmTmpActivities($data = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_act_getCrmTmpActivities(?,?,?,?,?,?,?,?)");
        $stmt->getResource()->bindParam(1, $data['crm_user_id']);
        $stmt->getResource()->bindParam(2, $data['user_session_id']);
        $stmt->getResource()->bindParam(3, $data['user_id']);
        $stmt->getResource()->bindParam(4, $data['source_type']);
        $stmt->getResource()->bindParam(5, $data['startIndex']);
        $stmt->getResource()->bindParam(6, $data['recordLimit']);
        $stmt->getResource()->bindParam(7, $data['sortField']);
        $stmt->getResource()->bindParam(8, $data['sortOrder']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }

    public function deleteCrmTmpActivities($data = array()) {
        $crm_user_id = (isset($data['crm_user_id']) && $data['crm_user_id']!='')?$data['crm_user_id']:'';
        $user_session_id = (isset($data['user_session_id']) && $data['user_session_id']!='')?$data['user_session_id']:'';
        $user_id = (isset($data['user_id']) && $data['user_id']!='')?$data['user_id']:'';
        $source_type = (isset($data['source_type']) && $data['source_type']!='')?$data['source_type']:'';
        $tmp_activity_id = (isset($data['tmp_activity_id']) && $data['tmp_activity_id']!='')?$data['tmp_activity_id']:'';
        
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_act_deleteCrmTmpActivities(?,?,?,?,?)");
        $stmt->getResource()->bindParam(1, $crm_user_id);
        $stmt->getResource()->bindParam(2, $user_session_id);
        $stmt->getResource()->bindParam(3, $user_id);
        $stmt->getResource()->bindParam(4, $source_type);
        $stmt->getResource()->bindParam(5, $tmp_activity_id);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }
    
    public function insertCrmActivitySources($data = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare("CALL usp_act_insertCrmActivitySources(?,?,?,?,?,?,?)");
            $stmt->getResource()->bindParam(1, $data['activity_id']);
            $stmt->getResource()->bindParam(2, $data['source_id']);
            $stmt->getResource()->bindParam(3, $data['source_type']);
            $stmt->getResource()->bindParam(4, $data['crm_user_id']);
            $stmt->getResource()->bindParam(5, $data['user_id']);
            $stmt->getResource()->bindParam(6, $data['added_date']);
            $stmt->getResource()->bindParam(7, $data['modified_date']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
				return $resultSet;
			else
				return false;
        } catch (Exception $e) {
            return false;
        }       
    }
    
    public function getCrmRelatedActivitySources($data = array()){
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_act_getCrmRelatedActivitySources(?,?,?,?,?,?,?,?,?)");
        
        $activity_id = (isset($data['activity_id']) && $data['activity_id']!='')?$data['activity_id']:'';
        $source_id = (isset($data['source_id']) && $data['source_id']!='')?$data['source_id']:'';
        $crm_user_id = (isset($data['crm_user_id']) && $data['crm_user_id']!='')?$data['crm_user_id']:'';
        $user_id = (isset($data['user_id']) && $data['user_id']!='')?$data['user_id']:'';
        $source_type = (isset($data['source_type']) && $data['source_type']!='')?$data['source_type']:'';
        $start_Index = (isset($data['start_Index']) && $data['start_Index']!='')?$data['start_Index']:'';
        $record_limit = (isset($data['record_limit']) && $data['record_limit']!='')?$data['record_limit']:'';
        $sort_field = (isset($data['sort_field']) && $data['sort_field']!='')?$data['sort_field']:'';
        $sort_order = (isset($data['sort_order']) && $data['sort_order']!='')?$data['sort_order']:'';
        
        $stmt->getResource()->bindParam(1, $activity_id);
        $stmt->getResource()->bindParam(2, $source_id);
        $stmt->getResource()->bindParam(3, $crm_user_id);
        $stmt->getResource()->bindParam(4, $user_id);
        $stmt->getResource()->bindParam(5, $source_type);
        $stmt->getResource()->bindParam(6, $start_Index);
        $stmt->getResource()->bindParam(7, $record_limit);
        $stmt->getResource()->bindParam(8, $sort_field);
        $stmt->getResource()->bindParam(9, $sort_order);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!empty($resultSet))
            return $resultSet;
        else
            return false;
    }
    
    public function deleteCrmRelatedActivities($data = array()) {
        $crm_user_id = (isset($data['crm_user_id']) && $data['crm_user_id']!='')?$data['crm_user_id']:'';
        $user_id = (isset($data['user_id']) && $data['user_id']!='')?$data['user_id']:'';
        $source_type = (isset($data['source_type']) && $data['source_type']!='')?$data['source_type']:'';
        $activity_id = (isset($data['activity_id']) && $data['activity_id']!='')?$data['activity_id']:'';
        $modified_date = (isset($data['modified_date']) && $data['modified_date']!='')?$data['modified_date']:DATE_TIME_FORMAT;
        $act_source_id = (isset($data['act_source_id']) && $data['act_source_id']!='')?$data['act_source_id']:'';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_act_deleteCrmRelatedActivities(?,?,?,?,?,?)");
        $stmt->getResource()->bindParam(1, $activity_id);
        $stmt->getResource()->bindParam(2, $crm_user_id);
        $stmt->getResource()->bindParam(3, $user_id);
        $stmt->getResource()->bindParam(4, $source_type);
        $stmt->getResource()->bindParam(5, $modified_date);
        $stmt->getResource()->bindParam(6, $act_source_id);
        $result = $stmt->execute();
        $statement = $result->getResource();
		$statement->closeCursor();	
    }

}