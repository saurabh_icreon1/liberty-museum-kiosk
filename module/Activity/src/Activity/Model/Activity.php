<?php

/**
 * This model is used for activity module validation.
 * @package    Activity_Activity
 * @author     Icreon Tech - DT
 */

namespace Activity\Model;

use Activity\Module;
// Add these import statements
use Base\Model\BaseModel;
use Zend\Db\Adapter\Adapter;
// Add these import statements
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

/**
 * This class is used for activity module validation.
 * @package    Activity_Activity
 * @author     Icreon Tech - DT
 */
class Activity extends BaseModel {

    protected $inputFilter;
    protected $adapter;

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }
      /**
     * This method is used to call add activity procedure parameter
     * @param array
     * @return array
     * @author Icreon Tech - DT
     */
    public function getCreateActivityArr($data) {
        $returnArr = array();
        $returnArr[]=(isset($data['activity_source_type_id']) && !empty($data['activity_source_type_id']))?$data['activity_source_type_id']:'';
        $returnArr[]=(isset($data['activity_source_id']) && !empty($data['activity_source_id']))?$data['activity_source_id']:'';
        $returnArr[]=(isset($data['activity_type_id']) && !empty($data['activity_type_id']))?$data['activity_type_id']:'';
        $returnArr[]=(isset($data['assigned_by_crm_user_id']) && !empty($data['assigned_by_crm_user_id']))?$data['assigned_by_crm_user_id']:'';
        $returnArr[]=(isset($data['contact_name_id']) && !empty($data['contact_name_id']))?$data['contact_name_id']:'';
        $returnArr[]=(isset($data['assigned_to_crm_user_id']) && !empty($data['assigned_to_crm_user_id']))?$data['assigned_to_crm_user_id']:'';
        $returnArr[]=(isset($data['activity_subject']) && !empty($data['activity_subject']))?$data['activity_subject']:'';
        $returnArr[]=(isset($data['activity_location']) && !empty($data['activity_location']))?$data['activity_location']:'';
        if(isset($data['activity_date']) && !empty($data['activity_date']))
        {
            
            $returnArr[]=$data['activity_date']." ".$data['activity_time'];
        }
        else
        {
            $returnArr[]='';
        }
        $returnArr[]=(isset($data['activity_duration']) && !empty($data['activity_duration']))?$data['activity_duration']:NULL;
        $returnArr[]=(isset($data['activity_status_id']) && !empty($data['activity_status_id']))?$data['activity_status_id']:'';
        $returnArr[]=(isset($data['activity_detail']) && !empty($data['activity_detail']))?$data['activity_detail']:'';
        $returnArr[]=(isset($data['activity_priority_id']) && !empty($data['activity_priority_id']))?$data['activity_priority_id']:'';
        $returnArr[]=(isset($data['follow_up_activity_type_id']) && !empty($data['follow_up_activity_type_id']))?$data['follow_up_activity_type_id']:'';
        if(isset($data['follow_up_activity_date']) && !empty($data['follow_up_activity_date']))
        {

            $returnArr[]=$data['follow_up_activity_date']." ".$data['follow_up_activity_time'];
        }
        else
        {
            $returnArr[]='';
        }
        $returnArr[]=(isset($data['follow_up_activity_subject']) && !empty($data['follow_up_activity_subject']))?$data['follow_up_activity_subject']:'';
        $returnArr[]=(isset($data['added_date']) && !empty($data['added_date']))?$data['added_date']:'';
        $returnArr[]=(isset($data['modified_date']) && !empty($data['modified_date']))?$data['modified_date']:'';

        return $returnArr;
    }

     /**
     * This method is used to call edit activity procedure parameter
     * @param array
     * @return array
     * @author Icreon Tech - DT
     */
    public function getEditActivityArr($data) {
        $returnArr = array();
        $returnArr[]=(isset($data['activity_id']) && !empty($data['activity_id']))?$data['activity_id']:'';
        $returnArr[]=(isset($data['activity_source_type_id']) && !empty($data['activity_source_type_id']))?$data['activity_source_type_id']:'';
        $returnArr[]=(isset($data['activity_source_id']) && !empty($data['activity_source_id']))?$data['activity_source_id']:'';
        $returnArr[]=(isset($data['assigned_by_crm_user_id']) && !empty($data['assigned_by_crm_user_id']))?$data['assigned_by_crm_user_id']:'';
        $returnArr[]=(isset($data['contact_name_id']) && !empty($data['contact_name_id']))?$data['contact_name_id']:'';
        $returnArr[]=(isset($data['assigned_to_crm_user_id']) && !empty($data['assigned_to_crm_user_id']))?$data['assigned_to_crm_user_id']:'';
        $returnArr[]=(isset($data['activity_subject']) && !empty($data['activity_subject']))?$data['activity_subject']:'';
        $returnArr[]=(isset($data['activity_location']) && !empty($data['activity_location']))?$data['activity_location']:'';
        if(isset($data['activity_date']) && !empty($data['activity_date']))
        {

            $returnArr[]=$data['activity_date']." ".$data['activity_time'];
        }
        else
        {
            $returnArr[]='';
        }
        $returnArr[]=(isset($data['activity_duration']) && !empty($data['activity_duration']))?$data['activity_duration']:NULL;
        $returnArr[]=(isset($data['activity_status_id']) && !empty($data['activity_status_id']))?$data['activity_status_id']:'';
        $returnArr[]=(isset($data['activity_detail']) && !empty($data['activity_detail']))?$data['activity_detail']:'';
        $returnArr[]=(isset($data['activity_priority_id']) && !empty($data['activity_priority_id']))?$data['activity_priority_id']:'';
        $returnArr[]=(isset($data['follow_up_activity_type_id']) && !empty($data['follow_up_activity_type_id']))?$data['follow_up_activity_type_id']:'';
        if(isset($data['follow_up_activity_date']) && !empty($data['follow_up_activity_date']))
        {

            $returnArr[]=$data['follow_up_activity_date']." ".$data['follow_up_activity_time'];
        }
        else
        {
            $returnArr[]='';
        }
        $returnArr[]=(isset($data['follow_up_activity_subject']) && !empty($data['follow_up_activity_subject']))?$data['follow_up_activity_subject']:'';
        $returnArr[]=(isset($data['modified_date']) && !empty($data['modified_date']))?$data['modified_date']:'';

        return $returnArr;
    }

    /**
     * This method is used to call add activity attachment procedure parameter
     * @param array
     * @return array
     * @author Icreon Tech - DT
     */
     public function getActivityAttachmentArr($data) {
        $returnArr = array();
        $returnArr[]=(isset($data['activity_id']) && !empty($data['activity_id']))?$data['activity_id']:'';
        $returnArr[]=(isset($data['file_name']) && !empty($data['file_name']))?$data['file_name']:'';
        $returnArr[]=(isset($data['is_private']) && !empty($data['is_private']))?$data['is_private']:'0';
        $returnArr[]=(isset($data['added_by']) && !empty($data['added_by']))?$data['added_by']:'';
        $returnArr[]=(isset($data['added_date']) && !empty($data['added_date']))?$data['added_date']:'';
        $returnArr[]=(isset($data['modified_date']) && !empty($data['modified_date']))?$data['modified_date']:'';
         return $returnArr;
     }

     /**
     * This method is used to call add activity attachment procedure parameter
     * @param array
     * @return array
     * @author Icreon Tech - DT
     */
     public function getActivityEditAttachmentArr($data) {
        $returnArr = array();
        $returnArr[]=(isset($data['activity_attachment_id']) && !empty($data['activity_attachment_id']))?$data['activity_attachment_id']:'';
        $returnArr[]=(isset($data['file_name']) && !empty($data['file_name']))?$data['file_name']:'';
        $returnArr[]=(isset($data['is_private']) && !empty($data['is_private']))?$data['is_private']:'0';
        $returnArr[]=(isset($data['added_by']) && !empty($data['added_by']))?$data['added_by']:'';
        $returnArr[]=(isset($data['modified_date']) && !empty($data['modified_date']))?$data['modified_date']:'';
         return $returnArr;
     }
    
     /**
     * This method is used to call edit case procedure parameter
     * @param code
     * @return Void
     * @author Icreon Tech - DT
     */
    public function getCreateEditArr($data) {
        $returnArr = array();
        $returnArr[]=(isset($data['transaction_id']) && !empty($data['transaction_id']))?$data['transaction_id']:'';
        $returnArr[]=(isset($data['contact_name_id']) && !empty($data['contact_name_id']))?$data['contact_name_id']:'';
        $returnArr[]=(isset($data['mode']) && !empty($data['mode']))?$data['mode']:'';
        $returnArr[]=(isset($data['message']) && !empty($data['message']))?$data['message']:'';
        $returnArr[]=(isset($data['subject']) && !empty($data['subject']))?$data['subject']:'';
        $returnArr[]=(isset($data['case_type']) && !empty($data['case_type']))?$data['case_type']:'';
        $returnArr[]=(isset($data['case_status']) && !empty($data['case_status']))?$data['case_status']:'';
        $returnArr[]=(isset($data['reason']) && !empty($data['reason']))?$data['reason']:'';
        if(isset($data['start_date']) && !empty($data['start_date']))
        {

            $returnArr[]=date('Y-m-d H:i:s',  strtotime($data['start_date']." ".$data['start_time']));
        }
        else
        {
            $returnArr[]='';
        }
        $returnArr[]=(isset($data['duration']) && !empty($data['duration']))?$data['duration']:'';
        $returnArr[]=(isset($data['urgent']) && !empty($data['urgent']))?$data['urgent']:'';
        $returnArr[]=(isset($data['assigned_to_id']) && !empty($data['assigned_to_id']))?$data['assigned_to_id']:'';
        $returnArr[]=(isset($data['modify_by']) && !empty($data['modify_by']))?$data['modify_by']:'';
        $returnArr[]=(isset($data['modify_date']) && !empty($data['modify_date']))?$data['modify_date']:'';
        $returnArr[]=(isset($data['case_contact_id']) && !empty($data['case_contact_id']))?$data['case_contact_id']:'';
        return $returnArr;
    }
    /**
     * This method is used to add filtering and validation to the form elements of create activity
     * @param void
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getInputFilterCreateActivity() {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'activity_id',
                        'required' => false,
                    )));
            
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activity_source_type_id',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'activity_type_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'ACTIVITY_TYPE_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'assigned_by_crm_user',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'ASSIGNED_BY_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
           $inputFilter->add($factory->createInput(array(
                        'name' => 'assigned_by_crm_user_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'contact_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'USER_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
           $inputFilter->add($factory->createInput(array(
                        'name' => 'contact_name_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'assigned_to_crm_user',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'assigned_to_crm_user_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activity_subject',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'SUBJECT_EMPTY',
                                    )
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activity_location',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'LOCATION_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));


            $inputFilter->add($factory->createInput(array(
                        'name' => 'activity_date',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'DATE_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activity_time',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'TIME_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activity_duration',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'Digits',
                                'options' => array(
                                    'messages' => array(
                                        'notDigits' => 'DURATION_NUMERIC',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activity_status_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'STATUS_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activity_priority_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'PRIORITY_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activity_detail',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                     /*   'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[A-Za-z0-9\n\r ,.-]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'ALPHA_NUMERIC_SPECIALSYMPOL'
                                    ),
                                ),
                            ),
                        ), */
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'file_name_1',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'file_name_1_hidden',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'file_name_2',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'file_name_2_hidden',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'file_name_3',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'file_name_3_hidden',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_private_1',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_private_2',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_private_3',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'follow_up_activity_type_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'follow_up_activity_date',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'follow_up_activity_time',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'follow_up_activity_subject',
                        'required' => false,
                    )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }


    /**
     * This method is used to add filtering and validation to the form elements of edit activity
     * @param void
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getInputFilterEditActivity() {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'activity_id',
                        'required' => false,
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'activity_source_type_id',
                        'required' => false,
                    )));

            
            $inputFilter->add($factory->createInput(array(
                        'name' => 'assigned_by_crm_user',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'ASSIGNED_BY_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
           $inputFilter->add($factory->createInput(array(
                        'name' => 'assigned_by_crm_user_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'contact_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'USER_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
           $inputFilter->add($factory->createInput(array(
                        'name' => 'contact_name_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'assigned_to_crm_user',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'assigned_to_crm_user_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activity_subject',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'SUBJECT_EMPTY',
                                    )
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activity_location',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'LOCATION_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));


            $inputFilter->add($factory->createInput(array(
                        'name' => 'activity_date',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'DATE_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activity_time',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'TIME_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activity_duration',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'Digits',
                                'options' => array(
                                    'messages' => array(
                                        'notDigits' => 'DURATION_NUMERIC',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activity_status_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'STATUS_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activity_priority_id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'PRIORITY_EMPTY',
                                    ),
                                ),
                            ),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activity_detail',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        /*'validators' => array(
                            array(
                                'name' => 'regex', false,
                                'options' => array(
                                    'pattern' => '/^[A-Za-z0-9\n\r ,.-]+$/',
                                    'messages' => array(\Zend\Validator\Regex::NOT_MATCH => 'ALPHA_NUMERIC_SPECIALSYMPOL'
                                    ),
                                ),
                            ),
                        ), */
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'file_name_1',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'file_name_1_hidden',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'file_name_2',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'file_name_2_hidden',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'file_name_3',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'file_name_3_hidden',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_private_1',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_private_2',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_private_3',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'follow_up_activity_type_id',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'follow_up_activity_date',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'follow_up_activity_time',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'follow_up_activity_subject',
                        'required' => false,
                    )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    /**
         * This method is used to return the stored procedure array for search actvitiy
         * @param array
         * @return array
         * @author Icreon Tech - DT

      */
    public function getActivitySearchArr($dataArr=array()) {
      
        $returnArr = array();
        $returnArr[] = (isset($dataArr['activity_source_type_id']) && $dataArr['activity_source_type_id'] != '') ? $dataArr['activity_source_type_id'] : '';
        $returnArr[] = (isset($dataArr['activity_source_id']) && $dataArr['activity_source_id'] != '') ? $dataArr['activity_source_id'] : '';
        $returnArr[] = (isset($dataArr['activity_type_id']) && $dataArr['activity_type_id'] != '') ? $dataArr['activity_type_id'] : '';
        $returnArr[] = (isset($dataArr['assigned_to_crm_user_id']) && $dataArr['assigned_to_crm_user_id'] != '') ? $dataArr['assigned_to_crm_user_id'] : '';
        $returnArr[] = (isset($dataArr['assigned_by_crm_user_id']) && $dataArr['assigned_by_crm_user_id'] != '') ? $dataArr['assigned_by_crm_user_id'] : '';
        $returnArr[] = (isset($dataArr['activity_subject']) && $dataArr['activity_subject'] != '') ? $dataArr['activity_subject'] : '';
        $returnArr[] = (isset($dataArr['user_id']) && $dataArr['user_id'] != '') ? $dataArr['user_id'] : '';
        $returnArr[] = (isset($dataArr['added_date_from']) && $dataArr['added_date_from'] != '') ? $dataArr['added_date_from'] : '';
        $returnArr[] = (isset($dataArr['added_date_to']) && $dataArr['added_date_to'] != '') ? $dataArr['added_date_to'] : '';
        $returnArr[] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '';
        $returnArr[] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $returnArr[] = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $returnArr[] = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        return $returnArr;
    }

    /**
         * This method is used to return the stored procedure array for create search result
         * @param code
         * @return Void
         * @author Icreon Tech - DT

      */
    public function getAddActivitySearchArr($dataArr=array()) {

        $returnArr = array();
        $returnArr[] = (isset($dataArr['crm_user_id']) && $dataArr['crm_user_id'] != '') ? $dataArr['crm_user_id'] : '';
        $returnArr[] = (isset($dataArr['title']) && $dataArr['title'] != '') ? $dataArr['title'] : '';
        $returnArr[] = (isset($dataArr['search_query']) && $dataArr['search_query'] != '') ? $dataArr['search_query'] : '';
        $returnArr[] = (isset($dataArr['is_active']) && $dataArr['is_active'] != '') ? $dataArr['is_active'] : '';
        $returnArr[] = (isset($dataArr['is_delete']) && $dataArr['is_delete'] != '') ? $dataArr['is_delete'] : '';
        //$returnArr[] = (isset($dataArr['added_date'])) ? $dataArr['added_date'] : (isset($dataArr['modified_date']))?$dataArr['modified_date']:'';
        if(isset($dataArr['added_date']))
        {
            $returnArr[] = $dataArr['added_date'];
        }
        else
        {
            $returnArr[] = $dataArr['modified_date'];
        }
        if(isset($dataArr['activity_search_id']) && $dataArr['activity_search_id']!='')
        {
            $returnArr[] = $dataArr['activity_search_id'];
        }
        return $returnArr;
    }

     /**
         * This method is used to return the stored procedure array for search pledge contact
         * @param array
         * @return array
         * @author Icreon Tech - DT

      */
    public function getPledgeContactArr($dataArr=array()) {

        $returnArr = array();
        $returnArr[] = (isset($dataArr['pledge_user_name']) && $dataArr['pledge_user_name'] != '') ? $dataArr['pledge_user_name'] : '';
        $returnArr[] = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $returnArr[] = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        return $returnArr;
    }
    /**
         * This method is used to return the stored procedure array for saved search
         * @param Array
         * @return Array
         * @author Icreon Tech - DT

      */
    public function getActivitySavedSearchArr($dataArr=array()) {

        $returnArr = array();
        $returnArr[] = (isset($dataArr['crm_user_id']) && $dataArr['crm_user_id'] != '') ? $dataArr['crm_user_id'] : '';
        $returnArr[] = (isset($dataArr['activity_search_id']) && $dataArr['activity_search_id'] != '') ? $dataArr['activity_search_id'] : '';
        $returnArr[] = (isset($dataArr['is_active'])) ? $dataArr['is_active'] : '';
        return $returnArr;
    }

    /**
         * This method is used to return the stored procedure array for delete saved search
         * @param Array
         * @return Array
         * @author Icreon Tech - DT

      */
    public function getDeleteActivitySavedSearchArr($dataArr=array()) {
        $returnArr = array();
        $returnArr[] = (isset($dataArr['is_deleted']) && $dataArr['is_deleted'] != '') ? $dataArr['is_deleted'] : '';
        $returnArr[] = (isset($dataArr['modified_date']) && $dataArr['modified_date'] != '') ? $dataArr['modified_date'] : '';
        $returnArr[] = (isset($dataArr['activity_search_id']) && $dataArr['activity_search_id'] != '') ? $dataArr['activity_search_id'] : '';
        return $returnArr;
    }
   
    /**
         * This method is used to return the stored procedure array for activity search
         * @param code
         * @return Void
         * @author Icreon Tech - DT

    */
    public function getInputFilterActivitySearch($filterData) {
        $inputFilterData = array();
        if (is_array($filterData)) {
            foreach ($filterData as $key => $val) {
                $inputFilterData[$key] = trim(addslashes(strip_tags($val)));
            }
        } else {
            $inputFilterData = trim(addslashes(strip_tags($filterData)));
        }


        return $inputFilterData;
    }

   



    /**
     * Function used to get array of elements need to call remove activity
     * @param Array
     * @return Array
     * @author Icreon Tech - DT
     */
    public function getArrayForRemoveActivity($dataArr) {
        $returnArr = array();
        $returnArr[]=(isset($dataArr['activity_id']) && $dataArr['activity_id'] != '')?$dataArr['activity_id']:'';
        return $returnArr;
    }
    /**
     * Function used to check variables activity
     * @author Icreon Tech - DT
     * @return boolean
     * @param Array
     */
    public function exchangeArray($data) {
        
    }

}