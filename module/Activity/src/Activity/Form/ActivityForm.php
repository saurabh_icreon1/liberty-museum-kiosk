<?php

/**
 * This page is used for Create and edit Activity form.
 * @package    Activity_ActivityForm
 * @author     Icreon Tech - DT
 */

namespace Activity\Form;

use Zend\Form\Form;

/**
 * This class is used for Create and edit Activity form.
 * @package    ActivityForm
 * @author     Icreon Tech - DT
 */
class ActivityForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('create_activity');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->add(array(
            'name' => 'activity_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'activity_id'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'activity_source_type_id',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'activity_source_type_id',
                'class' => 'e1'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'activity_type_id',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'activity_type_id',
                'class' => 'e1'
            )
        ));
        $this->add(array(
            'name' => 'assigned_by_crm_user',
            'attributes' => array(
                'type' => 'text',
                'id' => 'assigned_by_crm_user',
                'class' => 'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'assigned_by_crm_user_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'assigned_by_crm_user_id'
            )
        ));
        $this->add(array(
            'name' => 'lead_user_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'lead_user_name',
                'class' => 'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'lead_user_name_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'lead_user_name_id'
            )
        ));
        $this->add(array(
            'name' => 'pledge_user_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'pledge_user_name',
                'class' => 'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'pledge_user_name_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'pledge_user_name_id'
            )
        ));
        $this->add(array(
            'name' => 'case_subject',
            'attributes' => array(
                'type' => 'text',
                'id' => 'case_subject',
                'class' => 'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'case_subject_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'case_subject_id'
            )
        ));
        $this->add(array(
            'name' => 'contact_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'contact_name',
                'class' => 'search-icon'
            )
        ));
        
        $this->add(array(
            'name' => 'contact_name_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'contact_name_id'
            )
        ));
        $this->add(array(
            'name' => 'assigned_to_crm_user',
            'attributes' => array(
                'type' => 'text',
                'id' => 'assigned_to_crm_user',
                'class' => 'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'assigned_to_crm_user_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'assigned_to_crm_user_id'
            )
        ));
        $this->add(array(
            'name' => 'activity_subject',
            'attributes' => array(
                'type' => 'text',
                'id' => 'activity_subject'
            )
        ));
        $this->add(array(
            'name' => 'activity_location',
            'attributes' => array(
                'type' => 'text',
                'id' => 'activity_location',
            )
        ));
        $this->add(array(
            'name' => 'activity_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'activity_date',
                'class' => 'width-120 cal-icon',
            )
        ));
        $this->add(array(
            'name' => 'activity_time',
            'attributes' => array(
                'type' => 'text',
                'id' => 'activity_time',
                'class' => 'width-120 time-icon',
            )
        ));
        $this->add(array(
            'name' => 'activity_duration',
            'attributes' => array(
                'type' => 'text',
                'id' => 'activity_duration',
                'class' => 'width-97',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'activity_status_id',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'activity_status_id',
                'class' => 'e1'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'activity_priority_id',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'activity_priority_id',
                'onChange' => '',
                'class' => 'e1'
            )
        ));
        $this->add(array(
            'name' => 'activity_detail',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'activity_detail',
                "class" => "width-60"
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'follow_up_activity_type_id',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'follow_up_activity_type_id',
                'class' => 'e1'
            )
        ));
        $this->add(array(
            'name' => 'follow_up_activity_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'follow_up_activity_date',
                'class' => 'width-120 cal-icon',
            )
        ));
        $this->add(array(
            'name' => 'follow_up_activity_time',
            'attributes' => array(
                'type' => 'text',
                'id' => 'follow_up_activity_time',
                'class' => 'width-120 time-icon',
            )
        ));
        $this->add(array(
            'name' => 'follow_up_activity_subject',
            'attributes' => array(
                'type' => 'text',
                'id' => 'follow_up_activity_subject'
            )
        ));

        $this->add(array(
            'name' => 'saveactivity',
            'attributes' => array(
                'type' => 'button',
                'id' => 'saveactivity',
                'class' => 'save-btn',
                'value' => 'Save',
            )
        ));
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'id' => 'cancel',
                'class' => 'cancel-btn m-l-20',
                'value' => 'CANCEL',
                'onclick' => 'window.location.href="/activities"'
            )
        ));
        
        $this->add(array(
            'name' => 'selectsource',
            'attributes' => array(
                'type' => 'button',
                'id' => 'selectsource',
                'class' => 'save-btn',
                'value' => 'SELECT'
            )
        ));
        $this->add(array(
            'name' => 'cancelsource',
            'attributes' => array(
                'type' => 'button',
                'id' => 'cancelsource',
                'class' => 'cancel-btn m-l-10',
                'value' => 'CANCEL'
            )
        ));
    }

}
