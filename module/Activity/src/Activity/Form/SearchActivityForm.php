<?php

/**
 * This page is used for search Activity form.
 * @package    Activity_SearchActivityForm
 * @author     Icreon Tech - DT
 */

namespace Activity\Form;

use Zend\Form\Form;
use Activity\Form\SearchActivityForm;

/**
 * This form is used for search Activity.
 * @package    SearchActivityForm
 * @author     Icreon Tech - DT
 */
class SearchActivityForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('search_activity');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'activity_source_type_id',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'activity_source_type_id',
                'class' => 'e1'
            )
        ));
         $this->add(array(
            'name' => 'activity_source_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'activity_source_id',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'activity_type_id',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'activity_type_id',
                'class' => 'e1'
            )
        ));

        $this->add(array(
            'name' => 'assigned_to_crm_user',
            'attributes' => array(
                'type' => 'text',
                'id' => 'assigned_to_crm_user',
                'class' => 'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'assigned_to_crm_user_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'assigned_to_crm_user_id',
            )
        ));
        $this->add(array(
            'name' => 'activity_subject',
            'attributes' => array(
                'type' => 'text',
                'id' => 'activity_subject',
                'class' => 'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'assigned_by_crm_user',
            'attributes' => array(
                'type' => 'text',
                'id' => 'assigned_by_crm_user',
                'class' => 'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'assigned_by_crm_user_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'assigned_by_crm_user_id'
            )
        ));
        $this->add(array(
            'name' => 'user',
            'attributes' => array(
                'type' => 'text',
                'id' => 'user',
                'class' => 'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'user_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'user_id'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'added_date_range',
            'attributes' => array(
                'id' => 'added_date_range',
                'class' => 'e1 select-w-320',
                'onChange' => 'showDate(this.id,"date_range_div")'
            )
        ));
        $this->add(array(
            'name' => 'added_date_from',
            'attributes' => array(
                'type' => 'text',
                'class' => 'calendar-input width-97',
                'id' => 'added_date_from',
            )
        ));
        $this->add(array(
            'name' => 'added_date_to',
            'attributes' => array(
                'type' => 'text',
                'class' => 'calendar-input width-97',
                'id' => 'added_date_to',
            )
        ));

        $this->add(array(
            'name' => 'searchactivitybutton',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'searchactivitybutton',
                'class' => 'search-btn',
                'value' => 'SEARCH'
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'saved_search',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'value' => '',
                'class' => 'e1 select-w-320',
                'id' => 'saved_search',
                'onChange' => 'getSavedActivitySearchResult();'
            ),
        ));
    }

}

