<?php

/**
 * This page is used for activity module configuration details and language file configuration.
 * @package    Activity_Module
 * @author     Icreon Tech - DT
 */

namespace Activity;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\I18n\Translator\Translator;
use Zend\Validator\AbstractValidator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Activity\Model\Activity;
use Activity\Model\ActivityTable;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Adapter\DbTable as DbTableAuthAdapter;

/**
 * This class is used for case module configuration details and language file configuration.
 * @package    Module
 * @author     Icreon Tech - DT
 */
class Module implements AutoloaderProviderInterface, ConfigProviderInterface, ViewHelperProviderInterface {

    public function onBootstrap(MvcEvent $event) {
        $translator = $event->getApplication()->getServiceManager()->get('translator');
        $eventManager = $event->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $translator->addTranslationFile(
                'phpArray', './module/Activity/languages/en/language.php', 'default', 'en_US'
        );
        //AbstractValidator::setDefaultTranslator($translator);
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Get Autoloader Configuration
     * @return array
     */
    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    /**
     * Get Service Configuration
     *
     * @return array
     */
    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Activity\Model\ActivityTable' => function($serviceManager) {
                    $tableGateway = $serviceManager->get('ActivityTableGateway');
                    $table = new ActivityTable($tableGateway);
                    return $table;
                },
                'ActivityTableGateway' => function ($serviceManager) {
                    $dbAdapter = $serviceManager->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Activity($dbAdapter));
                    return new TableGateway('tbl_act_activites', $dbAdapter, null, $resultSetPrototype);
                },
                'dbAdapter' => function($serviceManager) {
                    $dbAdapter = $serviceManager->get('Zend\Db\Adapter\Adapter');
                    return $dbAdapter;
                },
            ),
        );
    }

    /**
     * Get View Helper Configuration
     *
     * @return array
     */
    public function getViewHelperConfig() {
        return array(
            'factories' => array(
            ),
        );
    }

    /**
     * Get Controller Configuration
     *
     * @return array
     */
    public function getControllerConfig() {
        return array();
    }

    public function loadConfiguration(MvcEvent $event) {
        $application = $event->getApplication();
        $serviceManager = $application->getServiceManager();
    }

    public function loadCommonViewVars(MvcEvent $event) {
        $event->getViewModel()->setVariables(array(
            'auth' => $event->getApplication()->getServiceManager()->get('AuthService')
        ));
    }

}
