<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'Cms\Controller\Cms' => 'Cms\Controller\CmsController',
            'Cms\Controller\Ad' => 'Cms\Controller\AdController',
            'Cms\Controller\HomeSlider' => 'Cms\Controller\HomeSliderController',
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'pages' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-contents',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'getCrmCmsContent',
                    ),
                ),
            ),
            'cmscontenttypes' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-cms-content-type',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'getCrmCmsContentType',
                    ),
                ),
            ),
            'addCrmGallery' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-crm-gallery',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'addCrmGallery',
                    ),
                ),
            ),
            'getCrmGallery' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-galleries',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'getCrmGallery',
                    ),
                ),
            ),
            'getSearchCrmGallery' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-search-crm-gallery',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'getSearchCrmGallery',
                    ),
                ),
            ),
            'editCrmGallery' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-gallery[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Cms\Controller',
                        'controller' => 'Cms',
                        'action' => 'editCrmGallery',
                    ),
                ),
            ),
            'getCrmGalleryInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-gallery-info[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Cms\Controller',
                        'controller' => 'Cms',
                        'action' => 'getCrmGalleryInfo',
                    ),
                ),
            ),
            'getCrmGalleryDetails' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-gallery-detail[/:id][/:mode]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Cms\Controller',
                        'controller' => 'Cms',
                        'action' => 'getCrmGalleryDetails',
                    ),
                ),
            ),
            'deleteCrmGalleryContent' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-gallery-content',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Cms\Controller',
                        'controller' => 'Cms',
                        'action' => 'deleteCrmGalleryContent',
                    ),
                ),
            ),
            'addcrmcmscontent' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-crm-content[/:content_type_id]',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'addCrmCmsContent',
                    ),
                ),
            ),
            'addCrmGalleryContent' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-crm-gallery-content[/:id]',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'addCrmGalleryContent',
                    ),
                ),
            ),
            'fileUploader' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/cms-upload',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'fileUploader',
                    ),
                ),
            ),
            'deleteGalleryImage' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-gallery-image[/:image]',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'deleteGalleryImage',
                    ),
                ),
            ),
            'viewcrmcmscontent' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-crm[/:content_id][/:mode]',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'viewCrmCmsContent',
                    ),
                ),
            ),
            'editcrmcmscontent' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-content[/:content_id][/:mode]',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'editCrmCmsContent',
                    ),
                ),
            ),
            'listcrmcontents' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/list-crm-contents',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'listCrmContents',
                    ),
                ),
            ),
            'deletecrmcmscontent' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-content',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'deleteCrmCmsContent',
                    ),
                ),
            ),
            'crmcontentchangelogs' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-content-change-logs[/:content_id][/:mode]',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'crmContentChangeLogs',
                    ),
                ),
            ),
            'listchangelogs' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/list-change-logs[/:content_id]',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'listChangeLogs',
                    ),
                ),
            ),
            'getCrmCmsContentLogInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-content-change-log-info[/:content_id][/:content_log_id][/:mode]',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'getCrmCmsContentLogInfo',
                    ),
                ),
            ),
            'checkcontenturl' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/check-content-url[/:url]',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'checkContentUrl',
                    ),
                ),
            ),
            'uploadcmsthumbnail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-cms-thumbnail',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'uploadCmsThumbnail',
                    ),
                ),
            ),
            'selectmenu' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/select-menu',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'selectMenu',
                    ),
                ),
            ),
            'editcmsmenudragdrop' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-cms-menu-drag-drop',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'editCmsMenuDragDrop',
                    ),
                ),
            ),
            'updatecmscontent' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/update-cms-content',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'updateCmsContent',
                    ),
                ),
            ),
            'uploadcmspdf' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-cms-pdf',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'uploadCmsPdf',
                    ),
                ),
            ),
            'downloadfile' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/download-file/:fileName',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'downloadFile',
                    ),
                ),
            ),
            'insertGallery' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/insert-gallery',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'insertGallery',
                    ),
                ),
            ),
            'insertGalleryDocument' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/insert-gallery-document',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'insertGalleryDocument',
                    ),
                ),
            ),
            'contactUs' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/contact-us',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'contactUs',
                    ),
                ),
            ),
            'addCrmAd' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-crm-ads',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Ad',
                        'action' => 'addCrmAd',
                    ),
                ),
            ),
            'addCrmAdProcess' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-crm-ads-process',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Ad',
                        'action' => 'addCrmAdProcess',
                    ),
                ),
            ),
            'editCrmAd' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-ads[/:ads_id]',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Ad',
                        'action' => 'editCrmAd',
                    ),
                ),
            ),
            'editCrmAdProcess' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-ads-process',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Ad',
                        'action' => 'editCrmAdProcess',
                    ),
                ),
            ),
            'getFrontAd' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/front-ads[/:page_url][/:ad_zone_id][/:start_Index]',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Ad',
                        'action' => 'getFrontAd',
                    ),
                ),
            ),
            'getAdsStatistics' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-ads-statistics[/:ad_id]',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Ad',
                        'action' => 'getAdsStatistics',
                    ),
                ),
            ),
            'addAdsClicksStatistics' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-clicks-ads[/:adId]',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Ad',
                        'action' => 'addAdsClicksStatistics',
                    ),
                ),
            ),
            'getCrmAd' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-ads',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Ad',
                        'action' => 'getCrmAd',
                    ),
                ),
            ),
            'listcrmads' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/list-crm-ads',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Ad',
                        'action' => 'listCrmAds',
                    ),
                ),
            ),
            'uploadCrmAdsImage' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-crm-ads-image',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Ad',
                        'action' => 'uploadCrmAdsImage',
                    ),
                ),
            ),
            'removeImageTemp' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-ads-remove-temp-file',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Ad',
                        'action' => 'removeImageTemp',
                    ),
                ),
            ),
            'deleteCrmAd' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-ad[/:ads_id]',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Ad',
                        'action' => 'deleteCrmAd',
                    ),
                ),
            ),
            'manageFaCategory' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/manage-fa-category',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'manageFaCategory',
                    ),
                ),
            ),
            'getFaCategory' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-fa-data[/:id]',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'getFaCategory',
                    ),
                ),
            ),
            'getPeoplingData' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-peopling-data[/:id]',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'getPeoplingData',
                    ),
                ),
            ),
            'deleteCrmGallery' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-gallery',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'deleteCrmGallery',
                    ),
                ),
            ),
            'viewMenu' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-menu',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'viewMenu',
                    ),
                ),
            ),
            'viewCmsContent' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-cms-content[/:content_id][/:mode]',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'viewCmsContent',
                    ),
                ),
            ),
            'getHaData' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-ha-data[/:id]',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'getHaData',
                    ),
                ),
            ),
            // Home Slider  - Start 
            'getCrmHomeSlider' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-home-slider',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\HomeSlider',
                        'action' => 'getCrmHomeSlider',
                    ),
                ),
            ),
            'listCrmHomeSlider' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/list-crm-home-slider',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\HomeSlider',
                        'action' => 'listCrmHomeSlider',
                    ),
                ),
            ),
            'addCrmHomeSlider' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-crm-home-slider',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\HomeSlider',
                        'action' => 'addCrmHomeSlider',
                    ),
                ),
            ),
            'addCrmHomeSliderProcess' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-crm-home-slider-process',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\HomeSlider',
                        'action' => 'addCrmHomeSliderProcess',
                    ),
                ),
            ),
            'editCrmHomeSlider' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-home-slider[/:slider_image_id]',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\HomeSlider',
                        'action' => 'editCrmHomeSlider',
                    ),
                ),
            ),
            'editCrmHomeSliderProcess' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-home-slider-process',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\HomeSlider',
                        'action' => 'editCrmHomeSliderProcess',
                    ),
                ),
            ),
            'uploadCrmHomeSliderImage' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-crm-home-slider-image',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\HomeSlider',
                        'action' => 'uploadCrmHomeSliderImage',
                    ),
                ),
            ),
            'deleteCrmHomeSlider' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-home-slider[/:slider_image_id]',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\HomeSlider',
                        'action' => 'deleteCrmHomeSlider',
                    ),
                ),
            ),
            // Home Slider - End
            'getTimelineData' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-timeline-data[/:year]',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'getTimelineData',
                    ),
                ),
            ),
            'getSolTimelineData' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-sol-timeline-data[/:year]',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'getSolTimelineData',
                    ),
                ),
            ),
            'getSponserData' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-sponser-data[/:id]',
                    'defaults' => array(
                        'controller' => 'Cms\Controller\Cms',
                        'action' => 'getSponserData',
                    ),
                ),
            ),
        )
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'navigation' => array(
        'default' => array(
            array(
                'label' => 'Home Slider',
                'route' => 'getCrmHomeSlider',
                'pages' => array(
                    array(
                        'label' => 'Create Home Slider',
                        'route' => 'addCrmHomeSlider'
                    ),
                    array(
                        'label' => 'Modify Home Slider',
                        'route' => 'editCrmHomeSlider'
                    ),
                )
            ),
            array(
                'label' => 'Ads',
                'route' => 'getCrmAd',
                'pages' => array(
                    array(
                        'label' => 'Create Ads',
                        'route' => 'addCrmAd'
                    ),
                    array(
                        'label' => 'Modify Ads',
                        'route' => 'editCrmAd'
                    ),
                )
            ),
            array(
                'label' => 'CMS',
                'route' => 'pages',
                'pages' => array(
                    array(
                        'label' => 'Add Content',
                        'route' => 'cmscontenttypes',
                        'pages' => array(
                            array(
                                'label' => 'Create',
                                'route' => 'addcrmcmscontent',
                                'pages' => array()
                            )
                        )
                    ),
                    array(
                        'label' => 'Edit',
                        'route' => 'editcrmcmscontent',
                        'pages' => array()
                    ),
                    array(
                        'label' => 'Change Log',
                        'route' => 'crmcontentchangelogs',
                        'pages' => array()
                    ),
                    array(
                        'label' => 'Search',
                        'route' => 'pages',
                        'pages' => array(),
                    )
                ),
            ),
            array(
                'label' => 'Gallery',
                'route' => 'getCrmGallery',
                'pages' => array(
                    array(
                        'label' => 'Search',
                        'route' => 'getCrmGallery',
                        'pages' => array(
                            array(
                                'label' => 'View',
                                'route' => 'getCrmGalleryInfo'
                            ),
                            array(
                                'label' => 'Edit',
                                'route' => 'editCrmGallery'
                            ),
                            array(
                                'label' => 'Change Log',
                                'route' => 'crmgallerychangelog'
                            ),
                        ),
                    ),
                    array(
                        'label' => 'Create',
                        'route' => 'addCrmGallery'
                    ),
                ),
            ),
        ),
    ),
    'Cms_messages' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'cms' => __DIR__ . '/../view'
        ),
        'template_map' => array(
            'layout/custom' => __DIR__ . '/../../module/Common/layout/crm.phtml',
            'error/404' => __DIR__ . '/../../module/Common/error/404.phtml',
            'error/index' => __DIR__ . '/../../module/Common/error/index.phtml',
        ),
    ),
    'global_config' => array(
        'config' => include __DIR__ . '/../languages/en/language.php'
    ),
);
