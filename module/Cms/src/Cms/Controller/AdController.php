<?php

/**
 * This controller is used for ads pages
 * @category   Zend
 * @package    Cms_AdController
 * @version    2.2
 * @author     Icreon Tech - NS
 */

namespace Cms\Controller;

use Base\Model\UploadHandler;
use Base\Controller\BaseController;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Zend\View\Model\ViewModel;
use Cms\Form\AdForm;
use Cms\Form\SearchAdForm;
use Cms\Model\Ad;
use Zend\Http\Request;
use Zend\Authentication\AuthenticationService;
use stdClass;
use Zend\Session\Container;

class AdController extends BaseController {

    protected $_adTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_flashMessage = null;
    protected $_menuSelectArr = array(0 => '<ROOT>');

    function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        parent::__construct();
    }

    /**
     * This function is used to get table and config file object
     * @return     array
     * @param void
     * @author Icreon Tech - NS
     */
    public function getAdTable() {
        if (!$this->_adTable) {
            $sm = $this->getServiceLocator();
            $this->_adTable = $sm->get('Cms\Model\AdTable');
            $this->_translator = $sm->get('translator');
            $this->_config = $sm->get('Config');
            $this->_adapter = $sm->get('dbAdapter');
            $this->_auth = new AuthenticationService();
        }
        return $this->_adTable;
    }

    /**
     * This action is used to front ad
     * @return $viewModel
     * @param void
     * @author Icreon Tech - NS
     */
    public function getFrontAdAction() {

        $this->getAdTable();
        $request = $this->getRequest();
        $adsListPagesMessages = array_merge($this->_config['Cms_messages']['config']['ADS_PAGE_LIST_MESSAGE'], $this->_config['Cms_messages']['config']['ADS_ERROR_MSG']);
        $ad_zone_id = '';
        $param = $this->params()->fromRoute();
		
        if (isset($param['page_url']) and trim($param['page_url']) != "" and isset($param['ad_zone_id']) and trim($param['ad_zone_id']) != "" and isset($param['start_Index']) and trim($param['start_Index']) != "") {
            $page_url = $this->decrypt(trim($param['page_url']));
            $ad_zone_id = $this->decrypt(trim($param['ad_zone_id']));
            $start_Index = $this->decrypt(trim($param['start_Index']));
            $page_url = trim($page_url, '/');
			
            $siteAds = new Container('ads');
            $siteAdsPageUrl = $siteAds->offsetGet('ads_url_' . $ad_zone_id);
           

            if (isset($siteAdsPageUrl) and trim($siteAdsPageUrl) != "" and trim($siteAdsPageUrl) != $page_url and $page_url != "") {
                $siteAds->getManager()->getStorage()->clear('ads');
                $siteAds->offsetSet('ads_url_' . $ad_zone_id, $page_url);
            } else {
                $siteAds->offsetSet('ads_url_' . $ad_zone_id, $page_url);
            }


            $paramInfo = array();
            $paramInfo['page_url'] = $page_url;
            $paramInfo['ad_zone_id'] = trim($ad_zone_id);
            $paramInfo['curr_date'] = date("Y-m-d", strtotime(DATE_TIME_FORMAT));
			
			if(trim($ad_zone_id) == 3 && $page_url==''){
				// display random add for home page right section
				$paramInfo['start_Index'] = '';
				$paramInfo['record_limit'] = '';
				$paramInfo['rand_flag'] = 1;
			}else{
				$paramInfo['start_Index'] = 0;
				$paramInfo['record_limit'] = 1;
				$paramInfo['rand_flag'] = 0;
			}


            $paramInfo['not_ad_ids'] = '';
            $getCRMAdInfoCount = $this->getAdTable()->getCrmFrontAd($paramInfo);
			
            $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
            $RecordCount = $countResult[0]->RecordCount;
            
            // When footer for page is not added then check for home page footer - Start
            if($RecordCount == "0" and trim($ad_zone_id) == "2" and $page_url != "") {
                $paramInfo = array();
                $paramInfo['page_url'] = "";
                $paramInfo['ad_zone_id'] = 2;
                $paramInfo['curr_date'] = date("Y-m-d", strtotime(DATE_TIME_FORMAT));
                $paramInfo['start_Index'] = 0;
                $paramInfo['record_limit'] = 1;
                $paramInfo['rand_flag'] = 0;

                $paramInfo['not_ad_ids'] = '';
                $getCRMAdInfoCount = $this->getAdTable()->getCrmFrontAd($paramInfo);
                $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
                $RecordCount = $countResult[0]->RecordCount;
            }
            // When footer for page is not added then check for home page footer - End


            $flag = false;
            $siteAdsValIndex = $siteAds->offsetGet('val_index_' . $ad_zone_id);
            if (isset($siteAdsValIndex)) {
                if (isset($siteAdsValIndex) and count($siteAdsValIndex) > 0 and count($siteAdsValIndex) < $RecordCount) {
                    $paramInfo['not_ad_ids'] = implode(",", $siteAdsValIndex);
                } else {
                    $flag = true;
                }
            }

            $getCRMAdInfo = $this->getAdTable()->getCrmFrontAd($paramInfo);
			

            if (isset($getCRMAdInfo[0]) and is_array($getCRMAdInfo[0])) {
                $frontAdInfo = $getCRMAdInfo[0];
                $remoteAddr = $request->getServer('REMOTE_ADDR');
                $adId = isset($frontAdInfo['ad_id']) ? $frontAdInfo['ad_id'] : '';

                if ($flag == true) {
                    //$siteAds->getManager()->getStorage()->clear('ads');
                    $siteAds->offsetSet('start_index_' . $ad_zone_id, 0);
                    $siteAds->offsetSet('key_index_' . $ad_zone_id, array(0));
                    $siteAds->offsetSet('val_index_' . $ad_zone_id, array($adId));
                }
                $siteAdsStartIndex = $siteAds->offsetGet('start_index_' . $ad_zone_id);
                if (!isset($siteAdsStartIndex)) {
                    $siteAds->offsetSet('start_index_' . $ad_zone_id, 0);
                    $siteAds->offsetSet('key_index_' . $ad_zone_id, array(0));
                    $siteAds->offsetSet('val_index_' . $ad_zone_id, array($adId));
                } else {
                    $siteAdsStartIndex = $siteAds->offsetGet('start_index_' . $ad_zone_id);
                    $siteAdsKeyIndex = $siteAds->offsetGet('key_index_' . $ad_zone_id);
                    $siteAdsValIndex = $siteAds->offsetGet('val_index_' . $ad_zone_id);
                    
                    if (in_array($siteAdsStartIndex, $siteAdsKeyIndex) and count($siteAdsValIndex) < $RecordCount) {
                        $siteAdsStartIndex = $siteAdsStartIndex + 1;
                        $siteAds->offsetSet('start_index_' . $ad_zone_id, $siteAdsStartIndex);

                        array_push($siteAdsKeyIndex, $siteAdsStartIndex);
                        $siteAds->offsetSet('key_index_' . $ad_zone_id, $siteAdsKeyIndex);

                        array_push($siteAdsValIndex, $adId);
                        $siteAds->offsetSet('val_index_' . $ad_zone_id, $siteAdsValIndex);
                    }
                    if(count($siteAdsValIndex) >= $RecordCount)
                    {
                       //$siteAds->getManager()->getStorage()->clear('ads'); 
                       $siteAds->offsetSet('start_index_' . $ad_zone_id, 0);
                       $siteAds->offsetSet('key_index_' . $ad_zone_id, array(0));
                       $siteAds->offsetSet('val_index_' . $ad_zone_id, array($adId));
                   
                    }
                }

                if (isset($this->_auth->getIdentity()->user_id)) {
                    $userIdExt = $this->_auth->getIdentity()->user_id;
                } else {
                    $userIdExt = "";
                }

                $this->getAdTable()->getCrmAdStatisticsInsertImpressions(array('ad_id' => $adId, 'user_id' => $userIdExt, 'user_ip' => $remoteAddr, 'impressions_date' => date("Y-m-d H:i:s", strtotime(DATE_TIME_FORMAT))));
            } else {
                $frontAdInfo = array();
            }
        }
        $servParam = $request->getServer();
        
        if (isset($servParam['HTTP_SSLSESSIONID']) && $servParam['HTTP_SSLSESSIONID'] != "")
        {
            $adsDir = $this->_config['file_upload_path']['assets_url_https'] . "cms/ads/";
        }
        else
        {
            $adsDir = $this->_config['file_upload_path']['assets_url'] . "cms/ads/";
        }

        if ($ad_zone_id == "1") {
            $defaultImage = '';
        } else if ($ad_zone_id == "2") {
            $defaultImage = '';
        } else if ($ad_zone_id == "3") {
            $defaultImage = '';
        } else if ($ad_zone_id == "4") {
            $defaultImage = '';
        }

        $this->layout('popup');

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => $adsListPagesMessages,
            'frontAdInfo' => $frontAdInfo,
            'adsDir' => $adsDir,
            'rootAdsDir' => $this->_config['file_upload_path']['assets_upload_dir'] . "cms/ads/",
            'defaultImage' => $defaultImage,
            'adZoneId' => $ad_zone_id
        ));
        return $viewModel;
    }

    /**
     * This function is used to add Ads Clicks Statistics
     * @return Ads Statistics   
     * @author Icreon Tech - NS
     */
    public function addAdsClicksStatisticsAction() {
        try {
            $this->getAdTable();
            $request = $this->getRequest();
            $param = $this->params()->fromRoute();

            if (isset($param['adId']) and trim($param['adId']) != "") {
                $adId = $this->decrypt(trim($param['adId']));
                $remoteAddr = $request->getServer('REMOTE_ADDR');

                if (isset($this->_auth->getIdentity()->user_id)) {
                    $userIdExt = $this->_auth->getIdentity()->user_id;
                } else {
                    $userIdExt = "";
                }
                $this->getAdTable()->getCrmFrontAdStatisticsInsertClicks(array('ad_id' => $adId, 'user_id' => $userIdExt, 'user_ip' => $remoteAddr, 'click_date' => date("Y-m-d H:i:s", strtotime(DATE_TIME_FORMAT))));
            }
            return true;
        } catch (Exception $e) {
            return true;
        }
    }

    /**
     * This function is used to get Ads Statistics
     * @return Ads Statistics   
     * @author Icreon Tech - NS
     */
    public function getAdsStatisticsAction() {
        $this->getAdTable();

        $jsLangTranslate = $this->_config['Cms_messages']['config']['ADS_PAGE_LIST_MESSAGE'];

        $param = $this->params()->fromRoute();
        $ads_id = $this->decrypt($param['ad_id']);


        $paramInfo = array();
        $paramInfo['ad_id'] = $ads_id;
        $getCRMAdInfo = $this->getAdTable()->getCrmAdInfo($paramInfo);


        if (isset($getCRMAdInfo['activate_date']) and trim($getCRMAdInfo['activate_date']) != "") {
            $activateDate = $getCRMAdInfo['activate_date'];
            $activateDateToTime = strtotime($activateDate);
            $activateDateMain = date('m/d/Y', $activateDateToTime);
        } else {
            $activateDateToTime = "";
            $activateDateMain = "";
        }

        if (isset($getCRMAdInfo['end_date']) and trim($getCRMAdInfo['end_date']) != "") {
            $endDate = $getCRMAdInfo['end_date'];
            $endDateToTime = strtotime($endDate);
            $endDateMain = date('m/d/Y', $endDateToTime);
        } else {
            $endDateMain = "";
            $endDateToTime = "";
        }

        if (isset($getCRMAdInfo['activate_date']) and trim($getCRMAdInfo['activate_date']) != "" and isset($getCRMAdInfo['end_date']) and trim($getCRMAdInfo['end_date']) != "") {

            $datetime1 = strtotime($getCRMAdInfo['activate_date']); // or your date as well
            $datetime2 = strtotime($getCRMAdInfo['end_date']);
            $datediff = $datetime2 - $datetime1;
            $daysBalance = floor($datediff / (60 * 60 * 24)) . ' days';
        } else {
            $daysBalance = '';
        }

        if (isset($getCRMAdInfo['is_active']) and trim($getCRMAdInfo['is_active']) != "") {
            $isActive = trim($getCRMAdInfo['is_active']);
        } else {
            $isActive = "0";
        }


        $dateArray = array(
            '0' => array('ad_id' => $ads_id, 'start_date_time' => date("Y-m-d H:i:s", strtotime(DATE_TIME_FORMAT . '-1 hour')), 'end_date_time' => date('Y-m-d H:i:s', strtotime(DATE_TIME_FORMAT)), 'type' => 'LastHour'),
            '1' => array('ad_id' => $ads_id, 'start_date_time' => date('Y-m-d 00:00:00', strtotime(DATE_TIME_FORMAT)), 'end_date_time' => date('Y-m-d 23:59:59', strtotime(DATE_TIME_FORMAT)), 'type' => 'Today'),
            '2' => array('ad_id' => $ads_id, 'start_date_time' => date('Y-m-d 00:00:00', strtotime(DATE_TIME_FORMAT . '-1 day')), 'end_date_time' => date('Y-m-d 23:59:59', strtotime(DATE_TIME_FORMAT . '-1 day')), 'type' => 'Yesterday'),
            '3' => array('ad_id' => $ads_id, 'start_date_time' => date('Y-m-d 00:00:00', strtotime(DATE_TIME_FORMAT . 'sunday last week -1 week +1 day')), 'end_date_time' => date('Y-m-d 23:59:59', strtotime(DATE_TIME_FORMAT . 'sunday last week -1 week +7 day')), 'type' => 'LastWeek'),
            '4' => array('ad_id' => $ads_id, 'start_date_time' => date('Y-m-d 00:00:00', strtotime(DATE_TIME_FORMAT . 'first day of last month')), 'end_date_time' => date('Y-m-d 23:59:59', strtotime(DATE_TIME_FORMAT . 'last day of last month')), 'type' => 'LastMonth'),
            '5' => array('ad_id' => $ads_id, 'start_date_time' => date('Y-m-d 00:00:00', strtotime(DATE_TIME_FORMAT . 'first day of -2 months')), 'end_date_time' => date('Y-m-d H:i:s', strtotime(DATE_TIME_FORMAT)), 'type' => 'ThreeMonths'),
            '6' => array('ad_id' => $ads_id, 'start_date_time' => date('Y-m-d 00:00:00', strtotime(DATE_TIME_FORMAT . 'first day of -5 months')), 'end_date_time' => date('Y-m-d H:i:s', strtotime(DATE_TIME_FORMAT)), 'type' => 'SixMonths'),
            '7' => array('ad_id' => $ads_id, 'start_date_time' => date('Y-m-d 00:00:00', strtotime(DATE_TIME_FORMAT . 'first day of -11 months')), 'end_date_time' => date('Y-m-d H:i:s', strtotime(DATE_TIME_FORMAT)), 'type' => 'OneYear'),
            '8' => array('ad_id' => $ads_id, 'start_date_time' => '', 'end_date_time' => '', 'type' => 'AllTime')
        );


        $count = 0;
        foreach ($dateArray as $Val) {
            $clicksArr = $this->getAdTable()->getCrmFrontAdStatisticsClicks($Val);
            $dateArray[$count]['AdClicks'] = isset($clicksArr[0]['AdClicks']) ? $clicksArr[0]['AdClicks'] : 0;
            $dateArray[$count]['UniqueClicks'] = isset($clicksArr[0]['UniqueClicks']) ? $clicksArr[0]['UniqueClicks'] : 0;

            $impressionArr = $this->getAdTable()->getCrmFrontAdStatisticsImpressions($Val);
            $dateArray[$count]['AdImpressions'] = isset($impressionArr[0]['AdImpressions']) ? $impressionArr[0]['AdImpressions'] : 0;
            $dateArray[$count]['UniqueImpressions'] = isset($impressionArr[0]['UniqueImpressions']) ? $impressionArr[0]['UniqueImpressions'] : 0;
            $count++;
        }

        $this->layout('popup');

        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'jsLangTranslate' => $jsLangTranslate,
            'dateArray' => $dateArray,
            'activateDateMain' => $activateDateMain,
            'activateDateToTime' => $activateDateToTime,
            'endDateMain' => $endDateMain,
            'endDateToTime' => $endDateToTime,
            'isActive' => $isActive,
            'daysBalance' => $daysBalance
        ));
        return $viewModel;
    }

    /**
     * This action is used to listing of ads 
     * @return $viewModel
     * @param void
     * @author Icreon Tech - NS
     */
    public function getCrmAdAction() {
        $this->checkUserAuthentication();
        $this->getAdTable();
        $adsListPagesMessages = array_merge($this->_config['Cms_messages']['config']['ADS_PAGE_LIST_MESSAGE'], $this->_config['Cms_messages']['config']['ADS_ERROR_MSG']);
        $this->layout('crm');
        $addForm = new SearchAdForm();
        $messages = array();
        $arrCRMAdZones = $this->getAdTable()->getCrmAdZones();
        $arrCRMAdZonesFormElement = array('' => 'Select');
        foreach ($arrCRMAdZones as $valZones) {
            $arrCRMAdZonesFormElement[$valZones['ad_zone_id']] = trim($valZones['ad_zone']);
        }
        $addForm->get('ad_zone_id')->setAttribute('options', $arrCRMAdZonesFormElement);
        
        $arrCRMAdTypes = $this->getAdTable()->getCrmAdTypes();
        $arrCRMAdTypesFormElement = array('' => 'Select');
        foreach ($arrCRMAdTypes as $valAdType) {
                $arrCRMAdTypesFormElement[$valAdType['ad_type_id']] = trim($valAdType['ad_type']);
        }
        $addForm->get('ad_type_id')->setAttribute('options', $arrCRMAdTypesFormElement);

        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }

        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'jsLangTranslate' => $adsListPagesMessages,
            'messages' => $messages,
            'form' => $addForm
        ));
        return $viewModel;
    }

    /**
     * This action is used to show ads grid
     * @return json format data
     * @param void
     * @author Icreon Tech - NS
     */
    public function listCrmAdsAction() {
        $this->checkUserAuthentication();
        $this->getAdTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $searchParam['ad_title'] = "";
        $searchParam['page_url'] = "";
        $searchParam['ad_zone_id'] = "";
        $searchParam['ad_status'] = "";
        $searchParam['ad_type_id'] = "";
        $searchParam['description'] = "";
        $searchParam['activate_date'] = "";
        $searchParam['end_date'] = "";
        parse_str($request->getPost('searchString'), $searchParam);
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;

        $searchParam['startIndex'] = $start;
        $searchParam['recordLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');
        $params = $this->params()->fromRoute();
        
        if(isset($searchParam['activate_date']) and trim($searchParam['activate_date']) != "") { $searchParam['activate_date'] = date("Y-m-d", strtotime(trim($searchParam['activate_date']))); }
        if(isset($searchParam['end_date']) and trim($searchParam['end_date']) != "") { $searchParam['end_date'] = date("Y-m-d", strtotime(trim($searchParam['end_date']))); }
        
        $searchResult = $this->getAdTable()->getCrmAds($searchParam);
        $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
        $jsonResult = array();
        $arrCell = array();
        $subArrCell = array();
		$isExpired = 0;
        $jsonResult['page'] = $page;
        $jsonResult['records'] = $countResult[0]->RecordCount;
        $jsonResult['total'] = @ceil($countResult[0]->RecordCount / $limit);
        if (isset($searchResult) and count($searchResult) > 0) {
            foreach ($searchResult as $val) {
                $arrCell['id'] = $val['ad_id'];
                if ($val['is_active'] == 1) {
                    $isActive = "Active";
                } else {
                    $isActive = "Inactive";
                }
                $statistics = "<a class='stats-icon' href='javascript: viewStatistics(" . '"' . $this->encrypt($val['ad_id']) . '"' . ");' ><div class='tooltip'>Statistics<span></span></div></a>";
                $edit = "<a class='edit-icon' href='/edit-crm-ads/" . $this->encrypt($val['ad_id']) . "'><div class='tooltip'>Edit<span></span></div></a>";
                $delete = "<a class='delete-icon deleteads' href='#delete_ads' onclick=deleteAds('" . $val['ad_id'] . "')><div class='tooltip'>Delete<span></span></div></a>";

                $pageUrl = '';
                if (isset($val['page_url']) and trim($val['page_url']) != "") {
                    $pageUrl = SITE_URL . '/' . $val['page_url'];
                } else {
                    $pageUrl = SITE_URL;
                }
                
				$isExpired = 0;
				$endDateCurrZoneFormat = "";
				$activeDateCurrZoneFormat = "";
				//asd($val['end_date'],0);
				if(!empty($val['end_date'])){
					$endDate = $this->OutputDateFormat($val['end_date'], 'calender');
					$endDateCurrZoneFormat = date('Y-m-d',strtotime($this->OutputDateFormat($val['end_date'], 'dateformatampm')));
					$userDate = date("Y-m-d", strtotime(DATE_TIME_FORMAT) + ($this->timeZoneSession->timeZone * 60)); 
					$datediff = strtotime($endDateCurrZoneFormat) - strtotime($userDate);
					if($datediff < 0 ){
						$isExpired = 1;
					}
				}
				if(!empty($val['activate_date'])){
					$activeDateCurrZoneFormat = date('Y-m-d',strtotime($this->OutputDateFormat($val['activate_date'], 'dateformatampm')));
				}
				
				
                $arrCell['cell'] = array($val['ad_title'], $pageUrl, stripslashes($val['ad_zone_name']),$val['ad_description'],(isset($val['activate_date']) and trim($val['activate_date']) != "")?$activeDateCurrZoneFormat:'N/A',(isset($val['end_date']) and trim($val['end_date']) != "")?$endDateCurrZoneFormat:'N/A', $isActive, $statistics . $edit . $delete,$isExpired);
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
        }
        return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    /**
     * This action is used to create ads
     * @return $viewModel
     * @param void
     * @author Icreon Tech - AS
     */
    public function addCrmAdAction() {
        $this->checkUserAuthentication();
        $this->getAdTable();
        $adsListPagesMessages = array_merge($this->_config['Cms_messages']['config']['ADS_PAGE_LIST_MESSAGE'], $this->_config['Cms_messages']['config']['ADS_ERROR_MSG']);

        $this->layout('crm');
        $addForm = new AdForm();
        $arrCRMAdTypes = $this->getAdTable()->getCrmAdTypes();
        $arrCRMAdZones = $this->getAdTable()->getCrmAdZones();

        $arrCRMAdTypesFormElement = array('' => 'Select');
        foreach ($arrCRMAdTypes as $valAdType) {
            $arrCRMAdTypesFormElement[$valAdType['ad_type_id']] = trim($valAdType['ad_type']);
        }

        $arrCRMAdZonesFormElement = array('' => 'Select');
        foreach ($arrCRMAdZones as $valZones) {
            $arrCRMAdZonesFormElement[$valZones['ad_zone_id']] = trim($valZones['ad_zone']);
        }

        $addForm->get('ad_type_id')->setAttribute('options', $arrCRMAdTypesFormElement);
        $addForm->get('ad_zone_id')->setAttribute('options', $arrCRMAdZonesFormElement);


        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'adForm' => $addForm,
            'jsLangTranslate' => $adsListPagesMessages
        ));
        return $viewModel;
    }

    /**
     * This action for saving ad
     * @return json
     * @param void
     * @author Icreon Tech - NS
     */
    public function addCrmAdProcessAction() {

        try {
            $this->checkUserAuthentication();
            $this->getAdTable();
            $request = $this->getRequest();
            $response = $this->getResponse();
            $adsObj = new Ad($this->_adapter);

            $adsListPagesMessages = array_merge($this->_config['Cms_messages']['config']['ADS_PAGE_LIST_MESSAGE'], $this->_config['Cms_messages']['config']['ADS_ERROR_MSG']);

            $params = array();
            $params['ad_title'] = $request->getPost('ad_title');
            $params['page_url'] = $request->getPost('page_url');
            $params['ad_zone_id'] = $request->getPost('ad_zone_id');
            $params['ad_type_id'] = $request->getPost('ad_type_id');
            $params['image_ad'] = $request->getPost('upload_file_name');
            $params['image_ad_url'] = $request->getPost('image_ad_url');
            $params['text_ad'] = $request->getPost('text_ad');
            $params['activate_date'] = $request->getPost('activate_date');
            $params['activate_time'] = $request->getPost('activate_time');
            $params['end_date'] = $request->getPost('end_date');
            $params['end_time'] = $request->getPost('end_time');
            $params['description'] = $request->getPost('description');
            $params['is_active'] = $request->getPost('is_active');
            $params['is_deleted'] = $request->getPost('is_deleted');
            $params['added_by'] = $this->_auth->getIdentity()->crm_user_id;
            $params['added_date'] = DATE_TIME_FORMAT;
            $addForm = new AdForm();
            $addForm->setInputFilter($adsObj->getInputFilterAds($params));
            $addForm->setData($request->getPost());

            if (!$addForm->isValid()) {
                $errors = $addForm->getMessages();

                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'Save') {
                        foreach ($row as $rower) {
                            $msg[$key] = isset($adsListPagesMessages[$rower]) ? $adsListPagesMessages[$rower] : '';
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $postData = $addForm->getData();

                $postData['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                $postData['added_date'] = DATE_TIME_FORMAT;

                $postData['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                $postData['modified_date'] = DATE_TIME_FORMAT;

                $filterAdsData = $adsObj->exchangeArray($postData);
				
				if (isset($postData['activate_date']) and !empty($postData['activate_date']))
					$filterAdsData['activate_date'] = $this->InputDateFormat($postData['activate_date'], 'dateformatampm');
				if (isset($postData['end_date']) and !empty($postData['end_date']))
					$filterAdsData['end_date'] = $this->InputDateFormat($postData['end_date'], 'dateformatampm');
				
                $returnData = $this->getAdTable()->insertCrmAds($filterAdsData);

                if ($returnData == true) {
                    $this->moveFileFromTempToCmsAds($filterAdsData['image_ad']);
                    $this->flashMessenger()->addMessage($adsListPagesMessages['L_ADS_ADDED']);
                    $messages = array('status' => "success");
                } else {
                    $this->flashMessenger()->addMessage($adsListPagesMessages['L_ADS_ERROR_ADDED']);
                    $messages = array('status' => "error");
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This action is used to edit ads
     * @return $viewModel
     * @param void
     * @author Icreon Tech - NS
     */
    public function editCrmAdAction() {
        $this->checkUserAuthentication();
        $this->getAdTable();
        $adsListPagesMessages = array_merge($this->_config['Cms_messages']['config']['ADS_PAGE_LIST_MESSAGE'], $this->_config['Cms_messages']['config']['ADS_ERROR_MSG']);

        $param = $this->params()->fromRoute();
        $ads_id = $this->decrypt($param['ads_id']);
        $param_info = array();
        $param_info['ad_id'] = $ads_id;
        $getCRMAdInfo = $this->getAdTable()->getCrmAdInfo($param_info);
        $this->layout('crm');
        $addForm = new AdForm();

        $arrCRMAdTypes = $this->getAdTable()->getCrmAdTypes();
        $arrCRMAdZones = $this->getAdTable()->getCrmAdZones();

        $arrCRMAdTypesFormElement = array('' => 'Select');
        foreach ($arrCRMAdTypes as $valAdType) {
            $arrCRMAdTypesFormElement[$valAdType['ad_type_id']] = trim($valAdType['ad_type']);
        }

        $arrCRMAdZonesFormElement = array('' => 'Select');
        foreach ($arrCRMAdZones as $valZones) {
            $arrCRMAdZonesFormElement[$valZones['ad_zone_id']] = trim($valZones['ad_zone']);
        }

        $addForm->get('ad_type_id')->setAttribute('options', $arrCRMAdTypesFormElement);
        $addForm->get('ad_zone_id')->setAttribute('options', $arrCRMAdZonesFormElement);

        $p_ad_type_id = 0;
        $p_image_ad = '';

        $addForm->get('ad_id')->setAttribute('value', $getCRMAdInfo['ad_id']);
        if (isset($getCRMAdInfo['ad_title']) and trim($getCRMAdInfo['ad_title']) != "") {
            $addForm->get('ad_title')->setAttribute('value', $getCRMAdInfo['ad_title']);
        }
        if (isset($getCRMAdInfo['description']) and trim($getCRMAdInfo['description']) != "") {
            $addForm->get('description')->setAttribute('value', $getCRMAdInfo['description']);
        }
        if (isset($getCRMAdInfo['page_url']) and trim($getCRMAdInfo['page_url']) != "") {
            $addForm->get('page_url')->setAttribute('value', $getCRMAdInfo['page_url']);
        }
        if (isset($getCRMAdInfo['ad_zone_id']) and trim($getCRMAdInfo['ad_zone_id']) != "") {
            $addForm->get('ad_zone_id')->setAttribute('value', $getCRMAdInfo['ad_zone_id']);
        }
        if (isset($getCRMAdInfo['ad_type_id']) and trim($getCRMAdInfo['ad_type_id']) != "") {
            $p_ad_type_id = trim($getCRMAdInfo['ad_type_id']);
            $addForm->get('ad_type_id')->setAttribute('value', $getCRMAdInfo['ad_type_id']);
        }
        if (isset($getCRMAdInfo['image_ad']) and trim($getCRMAdInfo['image_ad']) != "") {
            $p_image_ad = trim($getCRMAdInfo['image_ad']);
            $addForm->get('upload_file_name')->setAttribute('value', $getCRMAdInfo['image_ad']);
            $addForm->get('old_upload_file_name')->setAttribute('value', $getCRMAdInfo['image_ad']);
        }
        if (isset($getCRMAdInfo['image_ad_url']) and trim($getCRMAdInfo['image_ad_url']) != "") {
            $addForm->get('image_ad_url')->setAttribute('value', $getCRMAdInfo['image_ad_url']);
        }
        if (isset($getCRMAdInfo['text_ad']) and trim($getCRMAdInfo['text_ad']) != "") {
            $addForm->get('text_ad')->setAttribute('value', $getCRMAdInfo['text_ad']);
        }

        if (isset($getCRMAdInfo['activate_date']) and trim($getCRMAdInfo['activate_date']) != "") {
            /*$activateDate = $getCRMAdInfo['activate_date'];
            $activateDateToTime = strtotime($activateDate);
            $activateDateMain = date('m/d/Y', $activateDateToTime);*/
			$activateDateMain = date('m/d/Y',strtotime($this->OutputDateFormat($getCRMAdInfo['activate_date'], 'dateformatampm')));
            $addForm->get('activate_date')->setAttribute('value', $activateDateMain);
        }

        if (isset($getCRMAdInfo['end_date']) and trim($getCRMAdInfo['end_date']) != "") {
            /*$endDate = $getCRMAdInfo['end_date'];
            $endDateToTime = strtotime($endDate);
            $endDateMain = date('m/d/Y', $endDateToTime);*/
			$endDateMain = date('m/d/Y',strtotime($this->OutputDateFormat($getCRMAdInfo['end_date'], 'dateformatampm')));
            $addForm->get('end_date')->setAttribute('value', $endDateMain);
        }

        if (isset($getCRMAdInfo['is_new_window']) and trim($getCRMAdInfo['is_new_window']) != "") {
            $addForm->get('is_new_window')->setAttribute('value', trim($getCRMAdInfo['is_new_window']));
        }

        if (isset($getCRMAdInfo['is_active']) and trim($getCRMAdInfo['is_active']) != "") {
            $addForm->get('is_active')->setAttribute('value', trim($getCRMAdInfo['is_active']));
        }


        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'adForm' => $addForm,
            'jsLangTranslate' => $adsListPagesMessages,
            'p_ad_type_id' => $p_ad_type_id,
            'p_image_ad' => $p_image_ad
        ));
        return $viewModel;
    }

    /**
     * This action is used to modifying ads
     * @return $viewModel
     * @param void
     * @author Icreon Tech - NS
     */
    public function editCrmAdProcessAction() {

        try {
            $this->checkUserAuthentication();
            $this->getAdTable();
            $request = $this->getRequest();
            $response = $this->getResponse();
            $adsObj = new Ad($this->_adapter);

            $adsListPagesMessages = array_merge($this->_config['Cms_messages']['config']['ADS_PAGE_LIST_MESSAGE'], $this->_config['Cms_messages']['config']['ADS_ERROR_MSG']);

            $params = array();
            $params['ad_id'] = $request->getPost('ad_id');
            $params['ad_title'] = $request->getPost('ad_title');
            $params['description'] = $request->getPost('description');
            $params['page_url'] = $request->getPost('page_url');
            $params['ad_zone_id'] = $request->getPost('ad_zone_id');
            $params['ad_type_id'] = $request->getPost('ad_type_id');
            $params['image_ad'] = $request->getPost('upload_file_name');
            $params['image_ad_url'] = $request->getPost('image_ad_url');
            $params['text_ad'] = $request->getPost('text_ad');
            $params['activate_date'] = $request->getPost('activate_date');
            $params['end_date'] = $request->getPost('end_date');
            $params['is_active'] = $request->getPost('is_active');
            $params['is_deleted'] = $request->getPost('is_deleted');
            $params['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
            $params['modified_date'] = DATE_TIME_FORMAT;

            $addForm = new AdForm();
            $addForm->setInputFilter($adsObj->getInputFilterAds($params));
            $addForm->setData($request->getPost());



            if (!$addForm->isValid()) {
                $errors = $addForm->getMessages();

                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'Update') {
                        foreach ($row as $rower) {
                            $msg[$key] = $adsListPagesMessages[$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $postData = $addForm->getData();
                $postData['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                $postData['modified_date'] = DATE_TIME_FORMAT;

                $filterAdsData = $adsObj->exchangeArray($postData);

				if (isset($postData['activate_date']) and !empty($postData['activate_date']))
					$filterAdsData['activate_date'] = $this->InputDateFormat($postData['activate_date'], 'dateformatampm');
				if (isset($postData['end_date']) and !empty($postData['end_date']))
					$filterAdsData['end_date'] = $this->InputDateFormat($postData['end_date'], 'dateformatampm');

                $returnData = $this->getAdTable()->updateCrmAds($filterAdsData);

                if ($returnData == true) {

                    // unlink - start
                    try {
                        $oldUploadFileName = $request->getPost('old_upload_file_name');
                        if(isset($oldUploadFileName) and trim($oldUploadFileName) != "" and isset($filterAdsData['image_ad']) and trim($oldUploadFileName) != trim($filterAdsData['image_ad'])) {
                             $del_ads_dir = $this->_config['file_upload_path']['assets_upload_dir'] . "cms/ads/";
                             $del_ads_filename = $del_ads_dir.$oldUploadFileName;
                             if(file_exists($del_ads_filename)) {
                                 unlink($del_ads_filename);
                             }
                         }
                    }
                    catch(Exception $e) { }
                    // unlink - end    
                    if(isset($filterAdsData['image_ad']) and trim($filterAdsData['image_ad']) != "") {    
                        $this->moveFileFromTempToCmsAds($filterAdsData['image_ad']);
                    }

                    $this->flashMessenger()->addMessage($adsListPagesMessages['L_ADS_UPDATED']);
                    $messages = array('status' => "success");
                } else {
                    $this->flashMessenger()->addMessage($adsListPagesMessages['L_ADS_ERROR_UPDATED']);
                    $messages = array('status' => "error");
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This action is used to move file from temp to ads folder
     * @param filename 
     * @return 
     * @author Icreon Tech - NS
     */
    public function moveFileFromTempToCmsAds($filename) {
        if (isset($filename) and trim($filename) != "") {
            $temp_dir = $this->_config['file_upload_path']['temp_upload_dir'];
            $temp_thumbnail_dir = $this->_config['file_upload_path']['temp_upload_thumbnail_dir'];
            $temp_upload_medium_dir = $this->_config['file_upload_path']['temp_upload_medium_dir'];

            $temp_file = $temp_dir . $filename;
            $temp_thumbnail_file = $temp_thumbnail_dir . $filename;
            $temp_medium_file = $temp_upload_medium_dir . $filename;

            $ads_dir = $this->_config['file_upload_path']['assets_upload_dir'] . "cms/ads/";
            $ads_filename = $ads_dir . $filename;


            if (file_exists($temp_file) && file_exists($temp_medium_file)) {
                if (copy($temp_medium_file, $ads_filename)) {
                    if (file_exists($temp_file)) {
                        unlink($temp_file);
                    }
                    if (file_exists($temp_thumbnail_file)) {
                        unlink($temp_thumbnail_file);
                    }
                    if (file_exists($temp_medium_file)) {
                        unlink($temp_medium_file);
                    }
                }
            }
        }
    }

    /**
     * This action is used for uploading image
     * @return json format data
     * @param void
     * @author Icreon Tech - NS
     */
    public function uploadCrmAdsImageAction() {

        $ad_zone_id = isset($_POST['ad_zone_id']) ? $_POST['ad_zone_id'] : 0;
        if (empty($ad_zone_id)) {
            $ad_zone_id = 0;
        }

        $fileExt = $this->GetFileExt($_FILES['image_ad']['name']);
        $filename = $this->GenerateMediaFileName($fileExt[1]);    //Generate file name
        $_FILES['image_ad']['name'] = $filename;                 // assign name to file variable
        $this->getAdTable();
        $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration
        $error_messages = array(
            'post_max_size' => 'The uploaded file exceeds the post_max_size directive in php.ini', // Set error messages
            'max_file_size' => 'File is too big',
            'min_file_size' => 'File is too small',
            'accept_file_types' => 'Filetype not allowed',
        );

        $options = array(
            'upload_dir' => $upload_file_path['temp_upload_dir'],
            'param_name' => 'image_ad', //file input name                      // Set configuration
            'inline_file_types' => $upload_file_path['file_types_allowed'],
            'accept_file_types' => $upload_file_path['file_types_allowed'],
            'max_file_size' => $upload_file_path['max_allowed_file_size'],
            'min_file_size' => $upload_file_path['min_allowed_file_size'],
            'image_versions' => array(
                'medium' => array(
                    'max_width' => $this->_config['file_upload_path']['user_medium_width'],
                    'max_height' => $this->_config['file_upload_path']['user_medium_height']
                ),
                'thumbnail' => array(
                    'max_width' => $this->_config['file_upload_path']['user_thumb_width'],
                    'max_height' => $this->_config['file_upload_path']['user_thumb_height']
            ))
        );

        if ($ad_zone_id == "2") {
            $options['image_versions']['medium']['max_width'] = $this->_config['file_upload_path']['user_footer_ads_width'];
            $options['image_versions']['medium']['max_height'] = $this->_config['file_upload_path']['user_footer_ads_height'];
        }
        if ($ad_zone_id == "3") {
            $options['image_versions']['medium']['max_width'] = $this->_config['file_upload_path']['user_right_ads_width'];
            $options['image_versions']['medium']['max_height'] = $this->_config['file_upload_path']['user_right_ads_height'];
        }



        $upload_handler = new UploadHandler($options, true, $error_messages);
        $file_response = $upload_handler->jsonResponceData;
        if (isset($file_response['image_ad'][0]->error)) {
            $messages = array('status' => "error", 'message' => $file_response['image_ad'][0]->error);
        } else {
            $messages = array('status' => "success", 'message' => 'Image uploaded', 'filename' => $file_response['image_ad'][0]->name);
        }
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This action is used for deleting ads
     * @return json format data
     * @param void
     * @author Icreon Tech - NS
     */
    public function deleteCrmAdAction() {

        try {
            $this->checkUserAuthentication();
            $this->getAdTable();
            $response = $this->getResponse();
            $adsListPagesMessages = array_merge($this->_config['Cms_messages']['config']['ADS_PAGE_LIST_MESSAGE'], $this->_config['Cms_messages']['config']['ADS_ERROR_MSG']);

            $request = $this->getRequest();
            $ads_id = $request->getPost('ads_id');
            $params = array();
            $params['ads_id'] = $ads_id;
            $returnData = $this->getAdTable()->deleteCrmAds($params);

            if ($returnData == true) {
                $this->flashMessenger()->addMessage($adsListPagesMessages['L_ADS_DELETED']);
                $messages = array('status' => "success");
            } else {
                $this->flashMessenger()->addMessage($adsListPagesMessages['L_ADS_ERROR_DELETED']);
                $messages = array('status' => "error");
            }
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This action is used for removing image from temp folder
     * @return json format data
     * @param void
     * @author Icreon Tech - NS
     */
    public function removeImageTempAction() {
        $this->checkUserAuthentication();
        $this->getAdTable();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $filename = $request->getPost('upload_file_name');

            $temp_dir = $this->_config['file_upload_path']['temp_upload_dir'];
            $temp_thumbnail_dir = $this->_config['file_upload_path']['temp_upload_thumbnail_dir'];
            $temp_upload_medium_dir = $this->_config['file_upload_path']['temp_upload_medium_dir'];

            $temp_file = $temp_dir . $filename;
            $temp_thumbnail_file = $temp_thumbnail_dir . $filename;
            $temp_medium_file = $temp_upload_medium_dir . $filename;

            if (file_exists($temp_file)) {
                unlink($temp_file);
            }
            if (file_exists($temp_thumbnail_file)) {
                unlink($temp_thumbnail_file);
            }
            if (file_exists($temp_medium_file)) {
                unlink($temp_medium_file);
            }

            $messages = array('status' => 1);
        } else {
            $messages = array('status' => 0);
        }
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

}