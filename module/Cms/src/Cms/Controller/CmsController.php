<?php

/**
 * This controller is used for cms pages
 * @category   Zend
 * @package    Cms_CmsController
 * @version    2.2
 * @author     Icreon Tech - AS
 */

namespace Cms\Controller;

use Base\Model\UploadHandler;
use Base\Controller\BaseController;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Zend\View\Model\ViewModel;
use Cms\Form\SearchPageForm;
use Cms\Form\AddCrmGalleryForm;
use Cms\Form\SearchGalleryForm;
use Cms\Form\AddContentForm;
use Cms\Form\EditContentForm;
use Cms\Form\uploadGalleryImagesForm;
use Cms\Form\InsertGalleryForm;
use Cms\Form\MenuForm;
use Cms\Form\AddFaCategoryForm;
use Cms\Model\Cms;
use Zend\Http\Request;
use Zend\Authentication\AuthenticationService;
use Zend\Session\Container;
use stdClass;

class CmsController extends BaseController {

    protected $_cmsTable = null;
    protected $_fofTable = null;
    protected $_wohTable = null;
    protected $_adTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_flashMessage = null;
    protected $_menuSelectArr = array(0 => '<ROOT>');

    function __construct() {
        $this->_flashMessage = $this->flashMessenger();

        parent::__construct();
    }

    /**
     * This function is used to get table and config file object
     * @return     array
     * @param void
     * @author Icreon Tech - DT
     */
    public function getCmsTable() {
        if (!$this->_cmsTable) {
            $sm = $this->getServiceLocator();
            $this->_cmsTable = $sm->get('Cms\Model\CmsTable');

            $this->_translator = $sm->get('translator');
            $this->_config = $sm->get('Config');
            $this->_adapter = $sm->get('dbAdapter');
            $this->_auth = new AuthenticationService();
        }
        return $this->_cmsTable;
    }

    public function getAdTable() {
        if (!$this->_adTable) {
            $sm = $this->getServiceLocator();
            $this->_adTable = $sm->get('Cms\Model\AdTable');
        }
        return $this->_adTable;
    }

    public function getFofTable() {
        if (!$this->_fofTable) {
            $sm = $this->getServiceLocator();
            $this->_fofTable = $sm->get('User\Model\FofTable');
        }
        return $this->_fofTable;
    }

    public function getWohTable() {
        if (!$this->_wohTable) {
            $sm = $this->getServiceLocator();
            $this->_wohTable = $sm->get('User\Model\WohTable');
        }
        return $this->_wohTable;
    }

    /**
     * This action is used to listing of cms content type pages
     * @param void
     * @return array
     * @author Icreon Tech - AS
     */
    public function getCrmCmsContentTypeAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $this->getCmsTable();
        $cmsListPagesMessages = $this->_config['Cms_messages']['config']['CMS_PAGE_LIST_MESSAGE'];
        $cms = new Cms($this->_adapter);
        $this->layout('crm');
        $cmsType = $this->getCmsTable()->getCrmCmsContentTypes();
        $param = array();
        $i = 0;
        foreach ($cmsType as $key) {
            $param[$i]['content_type'] = $key['content_type'];
            $param[$i]['content_type_description'] = $key['content_type_description'];
            $param[$i]['content_type_id'] = $this->encrypt($key['content_type_id']);
            $i++;
        }
        $messages = array();
        $viewModel = new ViewModel();
        $response = $this->getResponse();
        $viewModel->setVariables(array(
            'messages' => $messages,
            'jsLangTranslate' => $cmsListPagesMessages,
            'cmsType' => $param
        ));
        return $viewModel;
    }

    /**
     * This action is used to listing of cms content type pages
     * @return $viewModel
     * @param void
     * @author Icreon Tech - AS
     */
    public function getCrmCmsContentAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $this->getCmsTable();
        $cmsListPagesMessages = array_merge($this->_config['Cms_messages']['config']['CMS_PAGE_LIST_MESSAGE'], $this->_config['Cms_messages']['config']['CMS_ERROR_MSG']);
        $cms = new Cms($this->_adapter);
        $searchPageForm = new SearchPageForm();
        $statusArray = array('' => 'Any', '1' => 'Published', '0' => 'Not Published');
        $typeId['id'] = "";
        $cmsType = $this->getCmsTable()->getCrmCmsContentTypes($typeId);
        //asd($cmsType);
        $typeArray = array('Select');
        foreach ($cmsType as $key) {
            if ($key['content_type_id'] != 9 && $key['content_type_id'] != 10 && $key['content_type_id'] != 12 && $key['content_type_id'] != 13 && $key['content_type_id'] != 14)
                $typeArray[$key['content_type_id']] = $key['content_type'];
        }
        $searchPageForm->get('status')->setAttribute('options', $statusArray);
        $searchPageForm->get('type')->setAttribute('options', $typeArray);
        $this->layout('crm');
        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        $viewModel = new ViewModel();
        $response = $this->getResponse();
        $viewModel->setVariables(array(
            'form' => $searchPageForm,
            'messages' => $messages,
            'jsLangTranslate' => $cmsListPagesMessages,
        ));
        return $viewModel;
    }

    /**
     * This action is used to add cms content
     * @return $viewModel
     * @param void
     * @author Icreon Tech - AS
     */
    public function addCrmCmsContentAction() {
        $this->checkUserAuthentication();
        $addForm = new AddContentForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getCmsTable();
        $cmsObj = new Cms($this->_adapter);
        $params = $this->params()->fromRoute();
        $flag = 0;
        $typeId['id'] = $this->decrypt($params['content_type_id']);
        $cmsType = $this->getCmsTable()->getCrmCmsContentTypes($typeId);
        $cmsListPagesMessages = array_merge($this->_config['Cms_messages']['config']['CMS_PAGE_LIST_MESSAGE'], $this->_config['Cms_messages']['config']['CMS_ERROR_MSG']);
        $this->layout('crm');
        $viewModel = new ViewModel();
        $cmsContent = $this->getCmsTable()->getCrmCmsContentPage($typeId);
        foreach ($cmsContent as $key) {
            $id = str_replace(" ", "_", $key['content_field_title']);
            switch ($key['content_field_type']) {
                case "textarea":
                    $addForm->add(array(
                        'name' => $id,
                        'attributes' => array(
                            'type' => 'textarea',
                            'id' => $id,
                            'class' => 'width-90'
                        )
                    ));
                    break;
                case "text" :
                    if ($key['content_field_title'] == 'Date') {
                        $addForm->add(array(
                            'name' => $id,
                            'attributes' => array(
                                'type' => 'text',
                                'id' => $id,
                                'class' => 'width-90'
                            )
                        ));
                        $addForm->get('Date')->setLabel('Date');
                    } else {
                        $addForm->add(array(
                            'name' => $id,
                            'attributes' => array(
                                'type' => 'text',
                                'id' => $id,
                                'class' => 'width-90'
                            )
                        ));
                    }
                    break;
                case "radio" :
                case "hidden" :
                    $addForm->add(array(
                        'name' => $id,
                        'attributes' => array(
                            'type' => 'hidden',
                            'id' => $id,
                            'value' => "0"
                        )
                    ));
                    break;
                case "checkbox" :
                    $addForm->add(array(
                        'name' => $id,
                        'type' => 'Checkbox',
                        'attributes' => array(
                            'id' => $id,
                            'class' => "e2 $id",
                        ),
                        'options' => array(
                            'value_options' => array(
                                '0' => 'Checkbox',
                                '1' => 'Checkbox',
                            ),
                        ),
                    ));
                    break;
                case "select" :
                    $addForm->add(array(
                        'type' => 'Select',
                        'name' => $id,
                        'options' => array(
                            'value_options' => array(),
                        ),
                        'attributes' => array(
                            'id' => $id,
                            'class' => 'e1 select-w-320',
                            'value' => ''
                            )));
                    break;
                case "file" :
                    $addForm->add(array(
                        'name' => $id,
                        'type' => 'file',
                        'attributes' => array(
                            'id' => $id
                            )));
                    $addForm->add(array(
                        'name' => "Upload_File",
                        'attributes' => array(
                            'type' => 'hidden',
                            'id' => "Upload_File"
                        )
                    ));
                    break;
                case "editor" :
                    $flag = 1;
                    $addForm->add(array(
                        'name' => 'email_set_text',
                        'type' => 'Checkbox',
                        'attributes' => array(
                            'value' => '0',
                            'id' => 'email_set_text',
                            'class' => 'e2',
                            'onclick' => 'setPlainText(this)'
                        ),
                        'options' => array(
                            'value_options' => array(
                                '0' => 'Checkbox',
                                '1' => 'Checkbox',
                            ),
                        ),
                    ));
                    $addForm->add(array(
                        'name' => $id,
                        'attributes' => array(
                            'type' => 'textarea',
                            'id' => 'email_message_body',
                            'class' => 'myTextEditor width-250'
                        )
                    ));
                    break;
            }
        }

        // $addForm->get('Date')->setLabel('Date');
        if ($typeId['id'] == "1") {
            $footerMenuLink = $this->getCmsTable()->getMenuList('2');
            $addForm->get('Header_Menu_Placement')->setAttribute('readonly', 'readonly');
            $menu[] = '<Root>';
            foreach ($footerMenuLink as $key => $val) {
                if ($val['parent_menu_id'] == '0') {
                    //echo $val['menu'] . "-";
                    //$menu[$val['menu_id']] = stripslashes($val['menu']);
                }
                $menu[$val['menu_id']] = stripslashes($val['menu']);
            }
            $addForm->get('Footer_Menu_Placement')->setAttribute('options', $menu);
        } else if ($typeId['id'] == '6') {
            $addForm->get('Date')->setLabel('Expiry Date<span class="red">*</span>');
        } else if ($typeId['id'] == '7') {
            $type['id'] = '';
            $cmsTypeDrop = $this->getCmsTable()->getCrmCmsContentTypes($type);
            $typeArray = array('Select');
            foreach ($cmsTypeDrop as $key) {
                if (($key['content_type_id'] > 1 && $key['content_type_id'] < 6) || ($key['content_type_id'] > 7 && $key['content_type_id'] != 11 && $key['content_type_id'] != 15))
                    $typeArray[$key['content_type_id']] = $key['content_type'];
            }
            $addForm->get('Page_Content')->setAttribute('options', $typeArray);
            $geneology['id'] = 13;
            $geneologyList = array('Any');
            $parentContent = $this->getCmsTable()->getGeneologyParentContent($geneology);
            foreach ($parentContent as $key)
                $geneologyList[$key['content_id']] = $key['page_title'];
            $addForm->get('Parent_Content')->setAttribute('options', $geneologyList);
            $dispOrder = array();
            $dispOrder[] = "Select";
            $dispOrder[1] = "Newest first";
            $dispOrder[2] = "Oldest first";
            $dispOrder[3] = "Title";
            $addForm->get('Display_Order')->setAttribute('options', $dispOrder);
            $footerMenuLink = $this->getCmsTable()->getMenuList('2');
            $addForm->get('Header_Menu_Placement')->setAttribute('readonly', 'readonly');
            $menu[] = '<Root>';
            foreach ($footerMenuLink as $key => $val) {
                if ($val['parent_menu_id'] == '0') {
                    //echo $val['menu'] . "-";
                    //$menu[$val['menu_id']] = stripslashes($val['menu']);
                }
                $menu[$val['menu_id']] = stripslashes($val['menu']);
            }
            $addForm->get('Footer_Menu_Placement')->setAttribute('options', $menu);
        } else if ($typeId['id'] == '8') {
            $searchResult = $this->getCmsTable()->getFaCaterogy();
            $categoryList = array('' => "Any");
            foreach ($searchResult as $key => $val) {
                $categoryList[$val['fa_category_id']] = stripslashes($val['fa_category_name']);
            }
            $addForm->get('Category')->setAttribute('options', $categoryList);
        } else if ($typeId['id'] == '15') {
            $geneology['id'] = 14;
            $geneologyList = array('Any');
            $parentContent = $this->getCmsTable()->getGeneologyParentContent($geneology);
            foreach ($parentContent as $key)
                $geneologyList[$key['content_id']] = $key['page_title'];
            $addForm->get('Parent_Content')->setAttribute('options', $geneologyList);
        }
        $rightList = array();
        $rightResult = $this->getCmsTable()->getRightSection();
        foreach ($rightResult as $key) {
            $rightList[$key['content_block_id']] = $key['content_block_name'];
        }
        $addForm->get('right_section')->setAttribute('options', $rightList);
        $response = $this->getResponse();

        /** Add cms content */
        if ($request->isPost()) {
            $insertParam = array();
            $insertParam['Parent_Content'] = "";
            $insertParam['content_type_id'] = $typeId['id'];
            $insertParam['Title'] = "";
            $insertParam['Short_Description'] = "";
            $insertParam['Description'] = "";
            $insertParam['Page_Content'] = "";
            $insertParam['Display_Order'] = "";
            $insertParam['Designation'] = "";
            $insertParam['Company_Name'] = "";
            $insertParam['Upload_File'] = "";
            $insertParam['Date'] = "";
            $insertParam['Year'] = "";
            $insertParam['Link'] = "";
            $insertParam['is_deleted'] = 0;
            $insertParam['Footer_Menu_Placement'] = 0;
            $insertParam['Header_Menu_Item_Id'] = 0;
            $insertParam['right_section'] = "";
            $insertParam['is_right'] = 0;
            $insertParam['num_ads'] = 0;
            $insertParam['content_url'] = "";
            $insertParam['page_title'] = "";
            $insertParam['page_description'] = "";
            $insertParam['abstract'] = "";
            $insertParam['keywords'] = "";
            $insertParam['is_publish'] = 0;
            $insertParam['addedDate'] = DATE_TIME_FORMAT;
            $insertParam['addedBy'] = $this->_auth->getIdentity()->crm_user_id;
            $insertParam['Name'] = "";
            $insertParam['Claim_To_Fame'] = "";
            $insertParam['Passenger_Id'] = "";
            $insertParam['Category'] = "";
            $insertParam['accessLevel'] = "";
            $postData = $request->getPost();
            $array_access = array();
            foreach ($postData as $key => $val) {
                if ($key == 'Date' || $key == 'Expiry_Date') {
                    $insertParam['Date'] = date("Y-m-d", strtotime($val));
                } else if ($key == 'Non-Logged_in' && $val == '1') {
                    array_push($array_access, '1');
                } else if ($key == 'Logged_in_Non-Members' && $val == '1') {
                    array_push($array_access, '2');
                } else if ($key == 'Logged_in_Members' && $val == '1') {
                    array_push($array_access, '3');
                } else {
                    $insertParam[$key] = $val;
                }
            }
            $insertParam['accessLevel'] = implode(",", $array_access);
            if (isset($insertParam['Upload_File']) && !empty($insertParam['Upload_File'])) {
                $this->moveFileFromTempToCms($insertParam['Upload_File']);
            }
            $returnData = $this->getCmsTable()->insertCmsContent($insertParam);
            if (isset($insertParam['right_section']) && !empty($insertParam['right_section'])) {
                foreach ($insertParam['right_section'] as $key) {
                    $insertRight['content_id'] = $returnData[0]['LAST_INSERT_ID'];
                    $insertRight['right_section_id'] = $key;
                    $insertRight['addedDate'] = DATE_TIME_FORMAT;
                    $insertRight['addedBy'] = $this->_auth->getIdentity()->crm_user_id;
                    $this->getCmsTable()->insertCmsRight($insertRight);
                }
            }
            if (isset($insertParam['Provide_Navigation_from_Menu']) && !empty($insertParam['Provide_Navigation_from_Menu'])) {
                if (isset($insertParam['Header_Menu_Placement']) && !empty($insertParam['Header_Menu_Placement'])) {
                    $returnMenu = $this->getCmsTable()->insertCmsMenu($insertParam);
                    $insertParam['content_id'] = $returnData[0]['LAST_INSERT_ID'];
                    $insertParam['menu_id'] = $returnMenu[0]['LAST_INSERT_ID'];
                    $insertParam['menu_type'] = 1;
                    $this->getCmsTable()->insertCmsContentMenu($insertParam);
                }
                if (isset($insertParam['Footer_Menu_Placement']) && !empty($insertParam['Footer_Menu_Placement'])) {
                    $insertParam['menu_id'] = $insertParam['Footer_Menu_Placement'];
                    $insertParam['menu_type'] = 2;
                    $this->getCmsTable()->insertCmsContentMenu($insertParam);
                }
            }
            if ($returnData) {
                $this->flashMessenger()->addMessage($cmsListPagesMessages['CONTENT_ADDED']);
                $messages = array('status' => "success");
            } else {
                $this->flashMessenger()->addMessage($cmsListPagesMessages['CONTENT_ERROR']);
                $messages = array('status' => "error");
            }
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
        $viewModel->setVariables(array(
            'customForm' => $cmsContent,
            'addForm' => $addForm,
            'jsLangTranslate' => $cmsListPagesMessages,
            'pageId' => $typeId['id'],
            'pageName' => $cmsType[0]['content_type'],
            'id' => $params['content_type_id'],
            'flag' => $flag,
            'filePath' => $this->_config['file_upload_path']
        ));
        return $viewModel;
    }

    /**
     * This action is used to create gallery
     * @param array
     * @return array    
     * @author Icreon Tech - AP
     */
    public function addCrmGalleryAction() {
        $this->checkUserAuthentication();
        $this->getCmsTable();
        $galleryMsg = array_merge($this->_config['Cms_messages']['config']['common'], $this->_config['Cms_messages']['config']['crm-gallery']);
        $this->layout('crm');
        $cmsObj = new Cms($this->_adapter);
        $formObj = new AddCrmGalleryForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $formObj->setInputFilter($cmsObj->getInputFilterCrmGallery());
            $formObj->setData($request->getPost());
            if (!$formObj->isValid()) {
                $errors = $formObj->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'add_gallery') {
                        foreach ($row as $rower) {
                            $msg [$key] = $galleryMsg[$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $formData = $formObj->getData();
                $date = DATE_TIME_FORMAT;
                $formData['add_date'] = $date;
                $formData['add_by'] = $this->_auth->getIdentity()->crm_user_id;
                $formData['modified_date'] = $date;
                $formData['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                $filterParam = $cmsObj->exchangeArray($formData);

                $galleryId = $this->getCmsTable()->insertCrmGallery($filterParam);
                if ($galleryId) {

                    /** insert into the change log */
                    $data['activity'] = '1';
                    $data['id'] = $galleryId;
                    $this->crmGalleryChangeLogs($data);
                    //$this->flashMessenger()->addMessage($galleryMsg['ALBUMB_INSERT_SUCCESS']);
                    $messages = array('id' => $this->encrypt($galleryId), 'status' => "success", 'message' => $galleryMsg['ALBUMB_INSERT_SUCCESS']);
                } else {
                    $this->flashMessenger()->addMessage($crmMessages['ALBUMB_INSERT_FAIL']);
                    $messages = array('status' => "error", 'message' => $galleryMsg['ALBUMB_INSERT_FAIL']);
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $viewModel = new ViewModel();
        $response = $this->getResponse();
        $viewModel->setVariables(array(
            'jsLangTranslate' => $galleryMsg,
            'form' => $formObj,
        ));
        return $viewModel;
    }

    /**
     * This action is used to edit of cms gallery    
     * @param void
     * @return array
     * @author Icreon Tech - AP
     */
    public function editCrmGalleryAction() {
        $this->checkUserAuthentication();
        $this->getCmsTable();
        $galleryMsg = array_merge($this->_config['Cms_messages']['config']['common'], $this->_config['file_upload_path'], $this->_config['Cms_messages']['config']['crm-gallery']);
        $cmsObj = new Cms($this->_adapter);
        $formObj = new AddCrmGalleryForm();
        $formObj->setAttribute('name', 'edit_gallery');
        $formObj->setAttribute('id', 'edit_gallery');
        $request = $this->getRequest();
        $response = $this->getResponse();

        $param = $this->params()->fromRoute();
        if (isset($param['id']) && !empty($param['id'])) {
            $searchParam['gallery_id'] = $this->decrypt($param['id']);
            if ($searchParam['gallery_id'] == '') {
                return $this->redirect()->toRoute('crm-galleries');
            }
            $searchResult = $this->getCmsTable()->getCrmGalleryInfo($searchParam);
            $contentFileName = !empty($searchResult[0]['content_file_name']) ? explode(',', $searchResult[0]['content_file_name']) : '';
            //        $contentFileName = explode(',', $searchResult[0]['content_file_name']);
            $physicalFileName = explode(',', $searchResult[0]['phy_file_name']);
            $galleryImagesIds = explode(',', $searchResult[0]['gallery_image_ids']);
            $galleryFileType = explode(',', $searchResult[0]['file_type']);
            if (isset($searchResult[0]['gallery_name']) && !empty($searchResult[0]['gallery_name'])) {
                $formObj->get('albumb_name')->setAttribute('value', $searchResult[0]['gallery_name']);
            }
            if (isset($searchResult[0]['gallery_description']) && !empty($searchResult[0]['gallery_description'])) {
                $formObj->get('albumb_description')->setAttribute('value', $searchResult[0]['gallery_description']);
            }
            $formObj->get('albumb_id')->setAttribute('value', $searchParam['gallery_id']);
        }
        if ($request->isPost()) {
            $formObj->setInputFilter($cmsObj->getInputFilterCrmGallery());
            $formObj->setData($request->getPost());
            if (!$formObj->isValid()) {
                $errors = $formObj->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'add_gallery') {
                        foreach ($row as $rower) {
                            $msg [$key] = $galleryMsg[$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $formData = $formObj->getData();
                $date = DATE_TIME_FORMAT;
                $formData['modified_date'] = $date;
                $formData['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                $formData['gallery_id'] = $formData['albumb_id'];
                $filterParam = $cmsObj->exchangeArray($formData);
                $return = $this->getCmsTable()->updateCrmGallery($filterParam);

                if ($return) {

                    /** insert into the change log */
                    $data['activity'] = '2';
                    $data['id'] = $formData['gallery_id'];
                    $this->crmGalleryChangeLogs($data);

                    $this->flashMessenger()->addMessage($galleryMsg['ALBUMB_UPDATED_SUCCESS']);
                    $messages = array('status' => "success", 'message' => $galleryMsg['ALBUMB_INSERT_SUCCESS']);
                } else {
                    $this->flashMessenger()->addMessage($galleryMsg['ALBUMB_INSERT_FAIL']);
                    $messages = array('status' => "error", 'message' => $galleryMsg['ALBUMB_INSERT_FAIL']);
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $response = $this->getResponse();
        $viewModel->setVariables(array(
            'jsLangTranslate' => $galleryMsg,
            'form' => $formObj,
            'contentFileName' => $contentFileName,
            'physicalFileName' => $physicalFileName,
            'galleryImagesIds' => $galleryImagesIds,
            'imgPath' => $galleryMsg['assets_url'] . 'cmsgallery/' . $this->FolderStructByUserId($searchParam['gallery_id']) . 'image',
            'messages' => $messages,
            'gallery_id' => $param['id'],
            'decoded_gallery_id' => $searchParam['gallery_id'],
            'galleryFileType' => $galleryFileType,
        ));
        return $viewModel;
    }

    /**
     * This action is used to list gallery record
     * @param array
     * @return array    
     * @author Icreon Tech - AP
     */
    public function getCrmGalleryAction() {

        $this->checkUserAuthentication();
        $this->getCmsTable();
        $galleryMsg = $this->_config['Cms_messages']['config']['crm-gallery'];
        $this->layout('crm');
        $searchFormObj = new SearchGalleryForm();
        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        $viewModel = new ViewModel();
        $response = $this->getResponse();

        $viewModel->setVariables(array(
            'jsLangTranslate' => $galleryMsg,
            'searchForm' => $searchFormObj,
            'messages' => $messages
        ));
        return $viewModel;
    }

    /**
     * This action is used to get gallery record
     * @param array
     * @return array    
     * @author Icreon Tech - AP
     */
    public function getSearchCrmGalleryAction() {
        $this->checkUserAuthentication();
        $this->getCmsTable();
        $galleryMsg = array_merge($this->_config['file_upload_path'], $this->_config['Cms_messages']['config']['crm-gallery']);
        $request = $this->getRequest();
        $response = $this->getResponse();
        parse_str($request->getPost('searchString'), $searchParam);
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        $searchParam['gallery_name'] = isset($searchParam['albumb_name']) ? $searchParam['albumb_name'] : '';
        $searchParam['startIndex'] = $start;
        $searchParam['recordLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');
        $searchResult = $this->getCmsTable()->getCrmGalleries($searchParam);
        $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
        $imageName = $galleryMsg['assets_url'] . 'cmsgallery/';
        $imagePath = $galleryMsg['assets_upload_dir'] . 'cmsgallery/';
        $noImagePath = "<img src='img/user_no_image.jpg'>";
        $jsonResult = array();
        $arrCell = array();
        $subArrCell = array();
        $jsonResult['page'] = $page;
        $jsonResult['records'] = $countResult[0]->RecordCount;
        $jsonResult['total'] = @ceil($countResult[0]->RecordCount / $limit);
        $filetype = array();
        if (count($searchResult) > 0) {
            foreach ($searchResult as $k => $val) {
                $filetype = (explode(',', $val['file_type']));
                foreach ($filetype as $k => $type) {
                    if ($k == 0 && $type == 2) {
                        $noImagePath = "<img src='img/video-thumb.png'>";
                    }
                }
                $img = $imageName . $this->FolderStructByUserId($val['gallery_id']) . 'image/thumbnail/' . $val['gallery_cover'];
                $videoImageeName = '<img src="' . $img . '">';
                $videoImagePath = $imagePath . $this->FolderStructByUserId($val['gallery_id']) . 'image/thumbnail/' . $val['gallery_cover'];
                $arrCell['id'] = $val['gallery_id'];
                $encrypt_id = $this->encrypt($val['gallery_id']);
                $view = '<a href="/get-crm-gallery-detail/' . $encrypt_id . '/' . $this->encrypt('view') . '" class="view-icon"><div class="tooltip">View<span></span></div></a>';
                $edit = '<a class="edit-icon" href="/get-crm-gallery-detail/' . $encrypt_id . '/' . $this->encrypt('edit') . '"><div class="tooltip">Edit<span></span></div></a> ';
                $delete = '<a class="delete-icon deletecontent" href="#delete_content" onclick=deleteContent("' . $encrypt_id . '")><div class="tooltip">Delete<span></span></div></a>';
                $uploadImage = '<a class="" href="/upload-crm-gallery-content/' . $encrypt_id . '">' . $galleryMsg['L_UPLOAD_IMAGE_VIDEO'] . '</a>';
                $arrCell['cell'] = array(stripslashes($val['gallery_name']), !empty($val['img_video_num']) ? $val['img_video_num'] : '0', is_file($videoImagePath) ? $videoImageeName : $noImagePath, $this->OutputDateFormat($val['created_on'], 'dateformatampm'), $view . $edit . $delete . ' ' . $uploadImage);
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
        }
        return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    /**
     * This action is used to get gallery info
     * @param array
     * @return array    
     * @author Icreon Tech - AP
     */
    public function getCrmGalleryInfoAction() {
        $this->checkUserAuthentication();
        $this->getCmsTable();
        $galleryMsg = array_merge($this->_config['file_upload_path'], $this->_config['Cms_messages']['config']['crm-gallery']);
        $param = $this->params()->fromRoute();
        $searchParam['gallery_id'] = $this->decrypt($param['id']);
        if ($searchParam['gallery_id'] == '') {
            return $this->redirect()->toRoute('crm-galleries');
        }
        $searchResult = $this->getCmsTable()->getCrmGalleryInfo($searchParam);
        $contentFileName = !empty($searchResult[0]['content_file_name']) ? explode(',', $searchResult[0]['content_file_name']) : '';
//        $contentFileName = explode(',', $searchResult[0]['content_file_name']);
        $physicalFileName = explode(',', $searchResult[0]['phy_file_name']);
        $galleryFileType = explode(',', $searchResult[0]['file_type']);
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'searchResult' => $searchResult[0],
            'jsLangTranslate' => $galleryMsg,
            'contentFileName' => $contentFileName,
            'physicalFileName' => $physicalFileName,
            'galleryFileType' => $galleryFileType,
            'imgPath' => $galleryMsg['assets_url'] . 'cmsgallery/' . $this->FolderStructByUserId($searchParam['gallery_id']) . 'image'
        ));
        return $viewModel;
    }

    /**
     * This action is used to show all cms gallery detail
     * @return     array
     * @author Icreon Tech - AP
     */
    public function getCrmGalleryDetailsAction() {

        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getCmsTable();
        $galleryMsg = $this->_config['Cms_messages']['config']['crm-gallery'];
        $params = $this->params()->fromRoute();
        $searchParam['gallery_id'] = $this->decrypt($params['id']);
        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'gallery_id' => $searchParam['gallery_id'],
            'mode' => $this->decrypt($params['mode']),
            'id' => $params['id'],
            'moduleName' => $this->encrypt('crmgallery'),
            'jsLangTranslate' => $galleryMsg,
            'messages' => $messages
        ));
        return $viewModel;
    }

    /* This Function is used to delet gallery image
     * @param id
     * @return Json
     * @author Icreon Tech - AP
     */

    public function deleteCrmGalleryContentAction() {
        $this->checkUserAuthentication();
        $this->getCmsTable();
        $galleryMsg = $this->_config['Cms_messages']['config']['crm-gallery'];
        $cmsObj = new Cms($this->_adapter);

        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $dataParam['gallery_content_id'] = $request->getPost('image_id');
            $dataParam['file_type'] = $request->getPost('file_type');
            $date = DATE_TIME_FORMAT;
            $dataParam['gallery_id'] = $request->getPost('gallery_id');
            $dataParam['modified_date'] = $date;
            $dataParam['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
            if ($this->getCmsTable()->deleteCmsGalleryContent($dataParam)) {
                /** insert into the change log */
                $data['activity'] = '3';
                $data['id'] = $dataParam['gallery_id'];
                $this->crmGalleryChangeLogs($data);

                $this->flashMessenger()->addMessage($galleryMsg['IMAGE_DELETE_CONFIRM']);
                $messages = array('status' => "success");
                return $response->setContent(\Zend\Json\Json::encode($messages));
            }
        }
    }

    /* function used for multi file upload
     * @param array
     * @return array
     * @author Icreon Team -AP
     */

    public function addCrmGalleryContentAction() {
        $this->checkUserAuthentication();
        $this->layout('crm');
        $this->getCmsTable();
        $msg = array_merge($this->_config['file_upload_path'], $this->_config['Cms_messages']['config']['crm-gallery']);
        $param = $this->params()->fromRoute();
        $uploadForm = new uploadGalleryImagesForm;
        $cmsObj = new Cms($this->_adapter);
        if (isset($param['id']) && !empty($param['id'])) {
            $searchParam['gallery_id'] = $this->decrypt($param['id']);
            $searchResult = $this->getCmsTable()->getCrmGalleryInfo($searchParam);
            $uploadForm->get('gallery_id')->setAttribute('value', $searchParam['gallery_id']);
        }
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $uploadForm->setInputFilter($cmsObj->getInputFilterCrmGalleryImages());
            $uploadForm->setData($request->getPost());

            if (!$uploadForm->isValid()) {
                $errors = $uploadForm->getMessages();
                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'add_gallery') {
                        foreach ($row as $rower) {
                            echo $msg [$key] = $galleryMsg[$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $formData = array();
                $imageCounter = 0;
                $covergallId = '';
                $videoCounter = 0;
                $date = DATE_TIME_FORMAT;
                $data = $request->getPost();
                foreach ($data['content_file_name'] as $k => $val) {
                    $formData['content_file_name'] = $val;
                    $formData['gallery_id'] = $data['gallery_id'];
                    $file_type = explode("/", $data['file_type'][$k]);
                    $formData['file_type'] = $file_type[0];
                    $formData['phy_file_name'] = $data['phy_file_name'][$k];
                    $formData['added_date'] = $date;
                    $formData['modified_date'] = $date;
                    $formData['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                    $formData['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                    if ($formData['file_type'] == 'image') {
                        $imageCounter = $imageCounter + 1;
                        $formData['file_type'] = 1;
                    } else {
                        $videoCounter = $videoCounter + 1;
                        $formData['file_type'] = 2;
                    }
                    $galleryContentId = $this->getCmsTable()->insertCrmGalleryContent($formData);
                    $this->moveFileFromTempToUser($formData['phy_file_name'], $formData['gallery_id']);

                    if ($k == 0) {
                        $covergallId = $galleryContentId;
                    }
                }
                $galleryData['gallery_id'] = $data['gallery_id'];
                $galleryData['modified_date'] = $date;
                $galleryData['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                $galleryData['gallery_content_id'] = $covergallId;
                $galleryData['num_image'] = $imageCounter;
                $galleryData['num_video'] = $videoCounter;
                $filterGalleryData = $cmsObj->exchangeArray($galleryData);
                $this->getCmsTable()->updateCrmGallery($filterGalleryData);
                /** insert into the change log */
                $data['activity'] = '2';
                $data['id'] = $galleryData['gallery_id'];
                $this->crmGalleryChangeLogs($data);
                $this->flashMessenger()->addMessage($msg['ALBUMB_UPDATED_SUCCESS']);
                $messages = array('status' => "success");
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        }
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'jsLangTranslate' => $msg,
            'param' => $searchResult['0'],
            'form' => $uploadForm
        ));

        return $viewModel;
    }

    /* function used for upload file to temp folder
     * @param array
     * @return json
     * @author Icreon Team -AP
     */

    public function fileUploaderAction() {
        $this->getCmsTable();
        $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration
        if ($_FILES) {
            if (isset($_FILES['files']['name'][0]) && $_FILES['files']['name'][0] != '') {
                $image_name_param = 'files';
            } else {
                $image_name_param = 'files';
            }
            $physicalName = $_FILES[$image_name_param]['name'];
            $fileType = $_FILES[$image_name_param]['type'][0];
            $fileExt = $this->GetFileExt($_FILES[$image_name_param]['name'][0]);
            $filename = $this->GenerateMediaFileName($fileExt[1]);    //Generate file name
            $_FILES[$image_name_param]['name'][0] = $filename;                 // assign name to file variable

            $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration
            $error_messages = array(
                'post_max_size' => 'The uploaded file exceeds the post_max_size directive in php.ini', // Set error messages
                'max_file_size' => 'File is too big',
                'min_file_size' => 'File is too small',
                'accept_file_types' => 'Filetype not allowed',
            );
            $options = array(
                'upload_dir' => $upload_file_path['temp_upload_dir'],
                'upload_url' => $upload_file_path['assets_url'] . '/temp/',
                'param_name' => $image_name_param, //file input name                      // Set configuration
                'inline_file_types' => $upload_file_path['gallery_file_type'],
                'accept_file_types' => $upload_file_path['gallery_file_type'],
                'max_file_size' => $upload_file_path['gallery_file_size'],
                'min_file_size' => $upload_file_path['min_allowed_file_size'],
                'content_file_name' => $physicalName
            );
            $upload_handler = new UploadHandler($options, true, $error_messages);
            $randomNo = $this->generateRandomString();
            $upload_handler->jsonResponceData['files']['0']->randomId = $randomNo;
            echo json_encode($upload_handler->jsonResponceData);
        }
        die;
    }

    /**
     * This action is used to show contents grid
     * @return json format data
     * @param void
     * @author Icreon Tech - AS
     */
    public function listCrmContentsAction() {
        $this->checkUserAuthentication();
        $this->getCmsTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        parse_str($request->getPost('searchString'), $searchParam);
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        if (!isset($searchParam['title']) || empty($searchParam['title'])) {
            $searchParam['title'] = "";
        }
        if (!isset($searchParam['status']) && empty($searchParam['status'])) {
            $searchParam['status'] = "";
        }
        if (!isset($searchParam['type']) || empty($searchParam['type'])) {
            $searchParam['type'] = "";
        }
        $searchParam['startIndex'] = $start;
        $searchParam['recordLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');
        $searchParam['content_id'] = "";
        $params = $this->params()->fromRoute();
        $editContentId = $this->_config['terms_content_id'];
        $searchResult = $this->getCmsTable()->getCrmContents($searchParam);
        $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
        $searchLog = $this->getCmsTable()->getCmsContentChangeLogs($searchParam);
        $jsonResult = array();
        $arrCell = array();
        $subArrCell = array();
        $jsonResult['page'] = $page;
        $jsonResult['records'] = $countResult[0]->RecordCount;
        $jsonResult['total'] = @ceil($countResult[0]->RecordCount / $limit);
        // asd($searchResult);
        if (count($searchResult) > 0) {
            foreach ($searchResult as $val) {
                $arrCell['id'] = $val['content_id'];
                if ($val['is_publish'] == 1) {
                    $isPublish = "Published";
                } else {
                    $isPublish = "Not Published";
                }
                if (!in_array($val['content_type_id'], array(3, 8, 11, 15, 16))) {
                    if ($val['is_publish'] == 1) {
                        $title = '<a class="txt-decoration-underline" href="' . $val['content_url'] . '" target="_blank">' . stripslashes($val['content_title']) . '</a>';
                    } else {
                        $title = '<a class="txt-decoration-underline" href="' . $val['content_url'] . '/' . $this->encrypt($val['content_url']) . '" target="_blank">' . stripslashes($val['content_title']) . '</a>';
                    }
                } else {
                    $title = stripslashes($val['content_title']);
                }
                $view = '<a class="view-icon" href="/view-crm/' . $this->encrypt($val['content_id']) . '/' . $this->encrypt('view') . '"><div class="tooltip">View<span></span></div></a>';
                $edit = '<a class="edit-icon" href="/view-crm/' . $this->encrypt($val['content_id']) . '/' . $this->encrypt('edit') . '"><div class="tooltip">Edit<span></span></div></a>';
                $delete = '';
                if ($val['content_id'] != $editContentId) {
                    $delete = "<a class='delete-icon deletecontent' href='#delete_content' onclick=deleteContent('" . $val['content_id'] . "')><div class='tooltip'>Delete<span></span></div></a>";
                }
                $arrCell['cell'] = array($val['content_id'], $val['is_publish'], $title, stripslashes($val['content_type']), $isPublish, $this->OutputDateFormat($val['modified_date'], 'dateformatampm'), $val['modified_user'], $view . $edit . $delete);
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
        }
        return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    /**
     * This action is used to show contents grid
     * @return json format data
     * @param void
     * @author Icreon Tech - AS
     */
    public function deleteCrmCmsContentAction() {
        $this->checkUserAuthentication();
        $this->getCmsTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        if ($request->isPost()) {
            $dataParam['deleted'] = "1";
            $dataParam['content_id'] = $request->getPost('content_id');
            $dataParam['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
            $dataParam['modified_date'] = DATE_TIME_FORMAT;
            $this->getCmsTable()->deleteCmsContent($dataParam);
            $this->flashMessenger()->addMessage($this->_config['Cms_messages']['config']['CMS_ERROR_MSG']['CONTENT_DELETED']);
            $messages = array('status' => "success");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This action is used to edit content
     * @return json format data
     * @param void
     * @author Icreon Tech - AS
     */
    public function editCrmCmsContentAction() {
        $this->checkUserAuthentication();
        $addForm = new EditContentForm();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $this->getCmsTable();
        $cmsObj = new Cms($this->_adapter);
        $searchParam['status'] = "";
        $searchParam['type'] = "";
        $searchParam['content_id'] = $this->decrypt($this->params()->fromRoute('content_id'));
        $searchParam['startIndex'] = "";
        $searchParam['recordLimit'] = "";
        $searchParam['sortField'] = "";
        $searchParam['sortOrder'] = "";
        $searchResult = $this->getCmsTable()->getCrmContent($searchParam);
        $result = $this->getCmsTable()->getCrmContentMenu($searchParam);
        // asd($result);
        $flag = 0;
        $typeId['id'] = $searchResult[0]['content_type_id'];
        $cmsType = $this->getCmsTable()->getCrmCmsContentTypes($typeId);
        $cmsListPagesMessages = array_merge($this->_config['Cms_messages']['config']['CMS_PAGE_LIST_MESSAGE'], $this->_config['Cms_messages']['config']['CMS_ERROR_MSG']);
        $editContentId = $this->_config['terms_content_id'];
        $viewModel = new ViewModel();
        $viewModel->setTerminal(TRUE);
        $cmsContent = $this->getCmsTable()->getCrmCmsContentPage($typeId);
        foreach ($cmsContent as $key) {
            $id = str_replace(" ", "_", $key['content_field_title']);
            switch ($key['content_field_type']) {
                case "textarea":
                    $addForm->add(array(
                        'name' => $id,
                        'attributes' => array(
                            'type' => 'textarea',
                            'id' => $id,
                            'class' => 'width-90'
                        )
                    ));
                    break;
                case "text" :
                    $addForm->add(array(
                        'name' => $id,
                        'attributes' => array(
                            'type' => 'text',
                            'id' => $id,
                            'class' => 'width-90'
                        )
                    ));
                    break;
                case "radio" :
                case "hidden" :
                    $addForm->add(array(
                        'name' => $id,
                        'attributes' => array(
                            'type' => 'hidden',
                            'id' => $id,
                            'value' => "0"
                        )
                    ));
                    break;
                case "checkbox" :
                    $addForm->add(array(
                        'name' => $id,
                        'type' => 'Checkbox',
                        'attributes' => array(
                            'id' => $id,
                            'class' => "e2 $id",
                        ),
                        'options' => array(
                            'value_options' => array(
                                '0' => 'Checkbox',
                                '1' => 'Checkbox',
                            ),
                        ),
                    ));
                    break;
                case "select" :
                    $addForm->add(array(
                        'type' => 'Select',
                        'name' => $id,
                        'options' => array(
                            'value_options' => array(),
                        ),
                        'attributes' => array(
                            'id' => $id,
                            'class' => 'e1 select-w-320',
                            'value' => ''
                            )));
                    break;
                case "file" :
                    $addForm->add(array(
                        'name' => $id,
                        'type' => 'file',
                        'attributes' => array(
                            'id' => $id
                            )));
                    $addForm->add(array(
                        'name' => "Upload_File",
                        'attributes' => array(
                            'type' => 'hidden',
                            'id' => "Upload_File"
                        )
                    ));
                    break;
                case "editor" :
                    $flag = 1;
                    $addForm->add(array(
                        'name' => 'email_set_text',
                        'type' => 'Checkbox',
                        'attributes' => array(
                            'value' => '0',
                            'id' => 'email_set_text',
                            'class' => 'e2',
                            'onclick' => 'setPlainText(this)'
                        ),
                        'options' => array(
                            'value_options' => array(
                                '0' => 'Checkbox',
                                '1' => 'Checkbox',
                            ),
                        ),
                    ));
                    $addForm->add(array(
                        'name' => $id,
                        'attributes' => array(
                            'type' => 'textarea',
                            'id' => 'email_message_body',
                            'class' => 'myTextEditor width-250'
                        )
                    ));
                    break;
            }
        }
        if ($typeId['id'] == "1") {
            $footerMenuLink = $this->getCmsTable()->getMenuList('2');
            $addForm->get('Header_Menu_Placement')->setAttribute('readonly', 'readonly');
            $menu[] = '<Root>';
            foreach ($footerMenuLink as $key => $val) {
                $menu[$val['menu_id']] = stripslashes($val['menu']);
            }
            $addForm->get('Footer_Menu_Placement')->setAttribute('options', $menu);
        } else if ($typeId['id'] == '7') {
            $type['id'] = '';
            $cmsTypeDrop = $this->getCmsTable()->getCrmCmsContentTypes($type);
            $typeArray = array('Select');
            foreach ($cmsTypeDrop as $key) {
                if (($key['content_type_id'] > 1 && $key['content_type_id'] < 6) || ($key['content_type_id'] > 7 && $key['content_type_id'] != 11 && $key['content_type_id'] != 14))
                    $typeArray[$key['content_type_id']] = $key['content_type'];
            }
            $addForm->get('Page_Content')->setAttribute('options', $typeArray);
            $dispOrder = array();
            $dispOrder[] = "Select";
            $dispOrder[1] = "Newest first";
            $dispOrder[2] = "Oldest first";
            $dispOrder[3] = "Title";
            $addForm->get('Display_Order')->setAttribute('options', $dispOrder);
            $footerMenuLink = $this->getCmsTable()->getMenuList('2');
            $addForm->get('Header_Menu_Placement')->setAttribute('readonly', 'readonly');
            $menu[] = '<Root>';
            foreach ($footerMenuLink as $key => $val) {
                $menu[$val['menu_id']] = stripslashes($val['menu']);
            }
            $addForm->get('Footer_Menu_Placement')->setAttribute('options', $menu);
        } else if ($typeId['id'] == '8') {
            $faCatResult = $this->getCmsTable()->getFaCaterogy();
            $categoryList = array('' => "Any");
            foreach ($faCatResult as $key => $val) {
                $categoryList[$val['fa_category_id']] = stripslashes($val['fa_category_name']);
            }
            $addForm->get('Category')->setAttribute('options', $categoryList);
        }
        $rightList = array();
        $rightResult = $this->getCmsTable()->getRightSection();
        foreach ($rightResult as $key) {
            $rightList[$key['content_block_id']] = $key['content_block_name'];
        }
        $addForm->get('right_section')->setAttribute('options', $rightList);
        if (isset($searchResult[0]['content_title']) && !empty($searchResult[0]['content_title'])) {
            if ($searchResult[0]['content_type_id'] == 8) {
                $addForm->get('Name')->setAttribute('value', $searchResult[0]['content_title']);
            } else {
                $addForm->get('Title')->setAttribute('value', $searchResult[0]['content_title']);
            }
        }
        if (isset($searchResult[0]['short_description']) && !empty($searchResult[0]['short_description'])) {
            $addForm->get('Short_Description')->setAttribute('value', $searchResult[0]['short_description']);
        }
        if (isset($searchResult[0]['description']) && !empty($searchResult[0]['description'])) {
            $addForm->get('Description')->setAttribute('value', $searchResult[0]['description']);
        }
        if (isset($searchResult[0]['page_title']) && !empty($searchResult[0]['page_title'])) {
            $addForm->get('page_title')->setAttribute('value', $searchResult[0]['page_title']);
        }
        if (isset($searchResult[0]['page_description']) && !empty($searchResult[0]['page_description'])) {
            $addForm->get('page_description')->setAttribute('value', $searchResult[0]['page_description']);
        }
        if (isset($searchResult[0]['page_abstract']) && !empty($searchResult[0]['page_abstract'])) {
            $addForm->get('abstract')->setAttribute('value', $searchResult[0]['page_abstract']);
        }
        if (isset($searchResult[0]['page_keywords']) && !empty($searchResult[0]['page_keywords'])) {
            $addForm->get('keywords')->setAttribute('value', $searchResult[0]['page_keywords']);
        }
        if (isset($searchResult[0]['designation']) && !empty($searchResult[0]['designation'])) {
            $addForm->get('Designation')->setAttribute('value', $searchResult[0]['designation']);
        }
        if (isset($searchResult[0]['company_name']) && !empty($searchResult[0]['company_name'])) {
            $addForm->get('Company_Name')->setAttribute('value', $searchResult[0]['company_name']);
        }
        if (isset($searchResult[0]['uploaded_file']) && !empty($searchResult[0]['uploaded_file'])) {
            $addForm->get('Upload_File')->setAttribute('value', $searchResult[0]['uploaded_file']);
        }
        if (isset($searchResult[0]['content_date']) && !empty($searchResult[0]['content_date']) && $searchResult[0]['content_date'] != '0000-00-00') {
            $addForm->get('Date')->setAttribute('value', $searchResult[0]['content_date']);
            if ($searchResult[0]['content_type_id'] == '6') {
                $addForm->get('Date')->setLabel('Expiry Date<span class="red">*</span>');
            } else {
                $addForm->get('Date')->setLabel('Date');
            }
        }
        if (isset($searchResult[0]['content_url']) && !empty($searchResult[0]['content_url'])) {
            $addForm->get('content_url')->setAttribute('value', $searchResult[0]['content_url']);
            if ($searchResult[0]['content_id'] == $editContentId) {
                $addForm->get('content_url')->setAttribute('readonly', 'readonly');
            }
        }
        if (isset($searchResult[0]['url']) && !empty($searchResult[0]['url'])) {
            $addForm->get('Link')->setAttribute('value', $searchResult[0]['url']);
        }
        if (isset($searchResult[0]['is_publish']) && !empty($searchResult[0]['is_publish'])) {
            $addForm->get('is_published')->setAttribute('value', $searchResult[0]['is_publish']);
        }
        if (isset($searchResult[0]['page_content_id']) && !empty($searchResult[0]['page_content_id'])) {
            $addForm->get('Page_Content')->setAttribute('value', $searchResult[0]['page_content_id']);
        }
        if (isset($searchResult[0]['display_order']) && !empty($searchResult[0]['display_order'])) {
            $addForm->get('Display_Order')->setAttribute('value', $searchResult[0]['display_order']);
        }
        if (isset($searchResult[0]['is_right_section']) && !empty($searchResult[0]['is_right_section'])) {
            $addForm->get('is_right')->setAttribute('value', $searchResult[0]['is_right_section']);
        }
        if (isset($searchResult[0]['num_ad_blocks']) && !empty($searchResult[0]['num_ad_blocks'])) {
            $addForm->get('num_ads')->setAttribute('value', $searchResult[0]['num_ad_blocks']);
        }
        if (isset($searchResult[0]['fa_claim_to_fame']) && !empty($searchResult[0]['fa_claim_to_fame'])) {
            $addForm->get('Claim_To_Fame')->setAttribute('value', $searchResult[0]['fa_claim_to_fame']);
        }
        if (isset($searchResult[0]['fa_passenger_id']) && !empty($searchResult[0]['fa_passenger_id'])) {
            $addForm->get('Passenger_Id')->setAttribute('value', $searchResult[0]['fa_passenger_id']);
        }
        if (isset($searchResult[0]['fa_category_id']) && !empty($searchResult[0]['fa_category_id'])) {
            $addForm->get('Category')->setAttribute('value', $searchResult[0]['fa_category_id']);
        }
        if (isset($searchResult[0]['access_label']) && !empty($searchResult[0]['access_label'])) {
            // asd($searchResult[0]['access_label']);
            if (strstr($searchResult[0]['access_label'], '1')) {
                $addForm->get('Non-Logged_in')->setAttribute('value', '1');
            }
            if (strstr($searchResult[0]['access_label'], '2')) {
                $addForm->get('Logged_in_Non-Members')->setAttribute('value', '1');
            }
            if (strstr($searchResult[0]['access_label'], '3')) {
                $addForm->get('Logged_in_Members')->setAttribute('value', '1');
            }
        }
        if (isset($searchResult[0]['block_id']) && !empty($searchResult[0]['block_id'])) {
            $block = explode(',', $searchResult[0]['block_id']);
            $addForm->get('right_section')->setAttribute('value', $block);
        }
        if (isset($searchResult[0]['content_year']) && !empty($searchResult[0]['content_year']) && $searchResult[0]['content_year'] != '0000') {
            $addForm->get('Year')->setAttribute('value', $searchResult[0]['content_year']);
        }
        if (isset($result) && !empty($result)) {
            foreach ($result as $key) {
                if ($key['menu_type'] == 1) {
                    $addForm->get('Provide_Navigation_from_Menu')->setAttribute('value', '1');
                    $addForm->get('Navigation_Link_Title')->setAttribute('value', $key['menu']);
                    $addForm->get('Header_Menu_Placement')->setAttribute('value', $key['parent']);
                    $addForm->get('Header_Menu_Item_Id')->setAttribute('value', $key['parentId']);
                    $addForm->get('Menu_Id')->setAttribute('value', $key['menu_id']);
                } else if ($key['menu_type'] == 2) {
                    $addForm->get('Provide_Navigation_from_Menu')->setAttribute('value', '1');
                    $addForm->get('Footer_Menu_Placement')->setAttribute('value', $key['menu_id']);
                }
            }
        }
        if ($request->isPost()) {
            $insertParam = array();
            $insertParam['content_id'] = $this->decrypt($this->params()->fromRoute('content_id'));
            $insertParam['Title'] = "";
            $insertParam['Short_Description'] = "";
            $insertParam['Description'] = "";
            $insertParam['Page_Content'] = "";
            $insertParam['Display_Order'] = "";
            $insertParam['Designation'] = "";
            $insertParam['Company_Name'] = "";
            $insertParam['Upload_File'] = "";
            $insertParam['Date'] = "";
            $insertParam['Link'] = "";
            $insertParam['is_deleted'] = 0;
            $insertParam['revision_id'] = $searchResult[0]['revision_id'] + 1;
            $insertParam['content_url'] = "";
            $insertParam['page_title'] = "";
            $insertParam['page_description'] = "";
            $insertParam['abstract'] = "";
            $insertParam['keywords'] = "";
            $insertParam['is_publish'] = 1;
            $insertParam['Header_Menu_Item_Id'] = 0;
            $insertParam['Footer_Menu_Placement'] = 0;
            $insertParam['right_section'] = "";
            $insertParam['is_right'] = 0;
            $insertParam['num_ads'] = 0;
            $insertParam['Name'] = "";
            $insertParam['Claim_To_Fame'] = "";
            $insertParam['Passenger_Id'] = "";
            $insertParam['Category'] = "";
            $insertParam['addedDate'] = DATE_TIME_FORMAT;
            $insertParam['addedBy'] = $this->_auth->getIdentity()->crm_user_id;
            $insertParam['accessLevel'] = "";
            $insertParam['is_new'] = 0;
            $postData = $request->getPost();
            $array_access = array();
            foreach ($postData as $key => $val) {
                if ($key == 'Date' || $key == 'Expiry_Date') {
                    $insertParam['Date'] = date("Y-m-d", strtotime($val));
                } else if ($key == 'Non-Logged_in' && $val == '1') {
                    array_push($array_access, '1');
                } else if ($key == 'Logged_in_Non-Members' && $val == '1') {
                    array_push($array_access, '2');
                } else if ($key == 'Logged_in_Members' && $val == '1') {
                    array_push($array_access, '3');
                } else {
                    $insertParam[$key] = $val;
                }
            }
            $insertParam['accessLevel'] = implode(",", $array_access);
            if (isset($insertParam['Upload_File']) && !empty($insertParam['Upload_File'])) {
                $this->moveFileFromTempToCms($insertParam['Upload_File']);
            }
            if (!isset($insertParam['is_right']) || empty($insertParam['is_right'])) {
                $insertParam['num_ads'] = 0;
            }
            if (isset($insertParam['Name']) && !empty($insertParam['Name'])) {
                $insertParam['Title'] = $insertParam['Name'];
            }
            /* Update main content  */
            $returnData = $this->getCmsTable()->updateCmsContent($insertParam);

            /* Right section updation */
            if (isset($insertParam['is_right']) && !empty($insertParam['is_right'])) {
                if (isset($insertParam['right_section']) && !empty($insertParam['right_section'])) {
                    $deleteRight['content_id'] = $insertParam['content_id'];
                    $deleteRight['addedBy'] = $this->_auth->getIdentity()->crm_user_id;
                    $this->getCmsTable()->deleteRight($deleteRight);
                    foreach ($insertParam['right_section'] as $key) {
                        $insertRight['content_id'] = $insertParam['content_id'];
                        $insertRight['right_section_id'] = $key;
                        $insertRight['addedDate'] = DATE_TIME_FORMAT;
                        $insertRight['addedBy'] = $this->_auth->getIdentity()->crm_user_id;
                        $this->getCmsTable()->insertCmsRight($insertRight);
                    }
                }
            } else {
                $deleteRight['content_id'] = $insertParam['content_id'];
                $deleteRight['addedBy'] = $this->_auth->getIdentity()->crm_user_id;
                $this->getCmsTable()->deleteRight($deleteRight);
            }
            /* Menu navigation updation */
            if (isset($insertParam['Provide_Navigation_from_Menu']) && !empty($insertParam['Provide_Navigation_from_Menu'])) {
                if (isset($insertParam['Header_Menu_Placement']) && !empty($insertParam['Header_Menu_Placement'])) {
                    if (isset($insertParam['Menu_Id']) && !empty($insertParam['Menu_Id'])) {
                        $insertParam['modifiedDate'] = DATE_TIME_FORMAT;
                        $insertParam['modifiedBy'] = $this->_auth->getIdentity()->crm_user_id;
                        $this->getCmsTable()->updateCmsContentMenu($insertParam);
                        $insertParam['menu_id'] = $insertParam['Menu_Id'];                        
                    } else {
                        $this->getCmsTable()->deleteCmsHeaderMenu($insertParam);
                        $returnMenu = $this->getCmsTable()->insertCmsMenu($insertParam);
                        $insertParam['menu_id'] = $returnMenu[0]['LAST_INSERT_ID'];
                    }
                    $insertParam['menu_type'] = '1';
                    $this->getCmsTable()->insertCmsContentMenu($insertParam);
                    $params['publish'] = $insertParam['is_published'];
                    $params['id'] = $insertParam['content_id'];
                    $params['modifiedBy'] = $this->_auth->getIdentity()->crm_user_id;
                    $params['modifiedDate'] = DATE_TIME_FORMAT;
                    $returnStatus = $this->getCmsTable()->updateCmsContentStatus($params);
                } else {
                    $params['content_id'] = $insertParam['content_id'];
                    if (isset($insertParam['Header_Menu_Item_Id']) && !empty($insertParam['Header_Menu_Item_Id'])) {
                        $this->getCmsTable()->deleteCmsHeaderMenu($params);
                    }
                }
                if (isset($insertParam['Footer_Menu_Placement']) && !empty($insertParam['Footer_Menu_Placement'])) {
                    $insertParam['menu_id'] = $insertParam['Footer_Menu_Placement'];
                    $insertParam['menu_type'] = '2';
                    $this->getCmsTable()->insertCmsContentMenu($insertParam);
                    $params['publish'] = $insertParam['is_published'];
                    $params['id'] = $insertParam['content_id'];
                    $params['modifiedBy'] = $this->_auth->getIdentity()->crm_user_id;
                    $params['modifiedDate'] = DATE_TIME_FORMAT;
                    $returnStatus = $this->getCmsTable()->updateCmsFooterStatus($params);
                } else {
                    $params['content_id'] = $insertParam['content_id'];
                    $this->getCmsTable()->deleteCmsFooterMenu($params);
                }
            } else {
                $params['content_id'] = $insertParam['content_id'];
                if (isset($insertParam['Header_Menu_Item_Id']) && !empty($insertParam['Header_Menu_Item_Id'])) {
                    $this->getCmsTable()->deleteCmsHeaderMenu($params);
                }
                if (isset($insertParam['Footer_Menu_Placement']) && !empty($insertParam['Footer_Menu_Placement'])) {
                    $this->getCmsTable()->deleteCmsFooterMenu($params);
                }
            }
            /* Terms new version updation */
            if (isset($insertParam['is_new']) && !empty($insertParam['is_new']) && $insertParam['content_id'] == $editContentId) {
                $this->getCmsTable()->insertTermsVersion($insertParam);
            }

            if ($returnData == true) {
                $this->flashMessenger()->addMessage($cmsListPagesMessages['CONTENT_UPDATED']);
                $messages = array('status' => "success");
            } else {
                $this->flashMessenger()->addMessage($cmsListPagesMessages['CONTENT_ERROR_UPDATE']);
                $messages = array('status' => "error");
            }
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
        $filePath = $this->_config['file_upload_path']['assets_url'];
        $fileRelativePath = $this->_config['file_upload_path']['assets_upload_dir'];
        $response = $this->getResponse();
        $viewModel->setVariables(array(
            'searchResult' => $searchResult[0],
            'filePath' => $filePath,
            'fileRelativePath' => $fileRelativePath,
            'customForm' => $cmsContent,
            'addForm' => $addForm,
            'jsLangTranslate' => $cmsListPagesMessages,
            'pageId' => $typeId['id'],
            'pageName' => $cmsType[0]['content_type'],
            'id' => $this->params()->fromRoute('content_id'),
            'flag' => $flag,
            'contentId' => $searchResult[0]['page_content_id'],
            'editId' => $editContentId
        ));
        return $viewModel;
    }

    /**
     * This action is used for creating tabbing page
     * @return json format data
     * @param void
     * @author Icreon Tech - AS
     */
    public function viewCrmCmsContentAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $this->getCmsTable();
        $cmsListPagesMessages = array_merge($this->_config['Cms_messages']['config']['CMS_PAGE_LIST_MESSAGE'], $this->_config['Cms_messages']['config']['CMS_ERROR_MSG']);
        $cms = new Cms($this->_adapter);
        $this->layout('crm');
        $messages = array();
        $viewModel = new ViewModel();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        $viewModel->setVariables(array(
            'messages' => $messages,
            'jsLangTranslate' => $cmsListPagesMessages,
            'id' => $params['content_id'],
            'mode' => $this->decrypt($params['mode'])
        ));
        return $viewModel;
    }

    /**
     * This action is used for change log
     * @return json format data
     * @param void
     * @author Icreon Tech - AS
     */
    public function crmContentChangeLogsAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $this->getCmsTable();
        $cmsListPagesMessages = array_merge($this->_config['Cms_messages']['config']['CMS_PAGE_LIST_MESSAGE'], $this->_config['Cms_messages']['config']['CMS_ERROR_MSG']);
        $cms = new Cms($this->_adapter);
        $messages = array();
        $viewModel = new ViewModel();
        $viewModel->setTerminal(TRUE);
        $response = $this->getResponse();
        $viewModel->setVariables(array(
            'messages' => $messages,
            'jsLangTranslate' => $cmsListPagesMessages,
            'id' => $this->params()->fromRoute('content_id'),
            'mode' => "changeLog"
        ));
        return $viewModel;
    }

    /**
     * This action is used for change log
     * @return json format data
     * @param void
     * @author Icreon Tech - AS
     */
    public function listChangeLogsAction() {
        $this->checkUserAuthentication();
        $this->getCmsTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        parse_str($request->getPost('searchString'), $searchParam);
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;
        $searchParam['startIndex'] = $start;
        $searchParam['recordLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');
        $searchParam['content_id'] = $this->decrypt($this->params()->fromRoute('content_id'));
        $searchParam['content_log_id'] = $this->decrypt($this->params()->fromRoute('content_log_id'));
        $params = $this->params()->fromRoute();
        $searchResult = $this->getCmsTable()->getCmsContentChangeLogs($searchParam);
        $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
        $jsonResult = array();
        $arrCell = array();
        $subArrCell = array();
        $jsonResult['page'] = $page;
        $jsonResult['records'] = $countResult[0]->RecordCount;
        $jsonResult['total'] = @ceil($countResult[0]->RecordCount / $limit);
        if (count($searchResult) > 0) {
            $i = 1;
            foreach ($searchResult as $val) {
                switch ($val['activity']) {
                    case '1': $act = "Added";
                        break;
                    case '2' : $act = "Updated";
                        break;
                    case '3' : $act = "Deleted";
                        break;
                }
                if ($i <= 5) {
                    $view = '<a class="view-icon" href="javascript:void(0)" onclick="viewChangeLogContent(\'' . $this->encrypt($val['content_id']) . '\',\'' . $this->encrypt($val['content_log_id']) . '\')"><div class="tooltip">View<span></span></div></a>';
                    // $view = "<a class='view-icon' href='/crm-content-change-log-info/" . $this->encrypt($val['content_log_id']) . "'><div class='tooltip'>View<span></span></div></a>";
                    $i++;
                } else {
                    $view = '';
                }
                $arrCell['cell'] = array($act, $val['crm_first_name'], $this->OutputDateFormat($val['activity_added_date'], 'dateformatampm'), "#" . $val['revision_id'], $view);
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
        }
        return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    /**
     * This action is used to view content page from change log
     * @return json format data
     * @param void
     * @author Icreon Tech - AS
     */
    public function getCrmCmsContentLogInfoAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $this->getCmsTable();
        $cmsListPagesMessages = array_merge($this->_config['Cms_messages']['config']['CMS_PAGE_LIST_MESSAGE'], $this->_config['Cms_messages']['config']['CMS_ERROR_MSG']);
        $cms = new Cms($this->_adapter);
        $searchParam['startIndex'] = '';
        $searchParam['recordLimit'] = '';
        $searchParam['sortField'] = '';
        $searchParam['sortOrder'] = '';
        $searchParam['content_id'] = '';
        $searchParam['content_log_id'] = $this->decrypt($this->params()->fromRoute('content_log_id'));
        $params = $this->params()->fromRoute();
        $searchResult = $this->getCmsTable()->getCmsContentChangeLogs($searchParam);
        $params['id'] = $searchResult[0]['content_type_id'];
        $searchMenuResult = $this->getCmsTable()->getCrmContentMenu($searchResult[0]);
        $searchTypeResult = $this->getCmsTable()->getCrmCmsContentPage($params);
        $messages = array();
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $response = $this->getResponse();
        $viewModel->setVariables(array(
            'messages' => $messages,
            'jsLangTranslate' => $cmsListPagesMessages,
            'searchType' => $searchTypeResult,
            'searchResult' => $searchResult,
            'searchMenu' => $searchMenuResult
        ));
        return $viewModel;
    }

    /**
     * This action is used to view content page from change log
     * @return json format data
     * @param void
     * @author Icreon Tech - AS
     */
    public function viewCmsContentAction() {
        $this->checkUserAuthentication();
        $request = $this->getRequest();
        $this->getCmsTable();
        $cmsListPagesMessages = array_merge($this->_config['Cms_messages']['config']['CMS_PAGE_LIST_MESSAGE'], $this->_config['Cms_messages']['config']['CMS_ERROR_MSG']);
        $cms = new Cms($this->_adapter);
        $searchParam['contentId'] = $this->decrypt($this->params()->fromRoute('content_id'));
        $params = $this->params()->fromRoute();
        $searchResult = $this->getCmsTable()->getCurrentCmsContent($searchParam);
        $params['id'] = $searchResult[0]['content_type_id'];
        $searchMenuResult = $this->getCmsTable()->getCrmContentMenu($searchResult[0]);
        $searchTypeResult = $this->getCmsTable()->getCrmCmsContentPage($params);
        $messages = array();
        $viewModel = new ViewModel();
        $viewModel->setTerminal(TRUE);
        $response = $this->getResponse();
        $viewModel->setVariables(array(
            'messages' => $messages,
            'jsLangTranslate' => $cmsListPagesMessages,
            'searchType' => $searchTypeResult,
            'searchResult' => $searchResult,
            'searchMenu' => $searchMenuResult
        ));
        return $viewModel;
    }

    /**
     * This action is used to delete images/videos from cms gallery    
     * @param void
     * @return json format data
     * @author Icreon Tech - AS
     */
    public function deleteGalleryImageAction() {
        $this->checkUserAuthentication();
        $this->getCmsTable();
        $msg = $this->_config['file_upload_path'];
        $response = $this->getResponse();
        $param = $this->params()->fromRoute();
        if (!empty($param['image'])) {
            $tempFilePath = $msg['temp_upload_dir'] . '/' . $param['image'];
            $tempThumbnamilFilePath = $msg['temp_upload_thumbnail_dir'] . '/' . $param['image'];
            if (is_file($tempFilePath)) {
                unlink($tempFilePath);
            }
            if (is_file($tempThumbnamilFilePath)) {
                unlink($tempThumbnamilFilePath);
            }
            $messages = array('status' => "success", 'message' => 'Image Delete');
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This action is used to move file from temp to campaign folder
     * @param filename ,gallery_id
     * @return 
     * @author Icreon Tech - AP
     */
    public function moveFileFromTempToUser($filename, $galleryId) {
        $this->getCmsTable();
        $temp_dir = $this->_config['file_upload_path']['temp_upload_dir'];
        $temp_thumbnail_dir = $this->_config['file_upload_path']['temp_upload_thumbnail_dir'];
        $gallery_dir = $this->_config['file_upload_path']['assets_upload_dir'] . "cmsgallery/";
        $gallery_image = 'image/';
        $gallery_img_thumb = 'thumbnail/';
        $folder_struc_by_gallery = $this->FolderStructByUserId($galleryId);
        $gallery_image_dir = $gallery_dir . $folder_struc_by_gallery . $gallery_image;
        $gallery_thumb_image_dir = $gallery_dir . $folder_struc_by_gallery . $gallery_image . $gallery_img_thumb;
        if (!is_dir($gallery_image_dir)) {
            mkdir($gallery_image_dir, 0777, TRUE);
        }
        if (!is_dir($gallery_thumb_image_dir)) {
            mkdir($gallery_thumb_image_dir, 0777, TRUE);
        }
        $temp_file = $temp_dir . $filename;
        $temp_thumbnail_file = $temp_thumbnail_dir . $filename;
        $gallery_filename = $gallery_image_dir . $filename;
        $gallery_thumbnail_filename = $gallery_thumb_image_dir . $filename;
        if (file_exists($temp_file)) {
            if (copy($temp_file, $gallery_filename)) {
                unlink($temp_file);
            }
        }
        if (file_exists($temp_thumbnail_file)) {
            if (copy($temp_thumbnail_file, $gallery_thumbnail_filename)) {
                unlink($temp_thumbnail_file);
            }
        }
    }

    /**
     * This action is used checking content url
     * @return json format data
     * @param void
     * @author Icreon Tech - AS
     */
    public function checkContentUrlAction() {
        $this->checkUserAuthentication();
        $this->getCmsTable();
        $cmsListPagesMessages = array_merge($this->_config['Cms_messages']['config']['CMS_PAGE_LIST_MESSAGE'], $this->_config['Cms_messages']['config']['CMS_ERROR_MSG']);
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        $params['id'] = $this->decrypt($request->getPost('id'));
        $searchResult = $this->getCmsTable()->getCrmContentsUrl($params);
        if ($searchResult[0]['isAvailable'] > 0) {
            $msg = $cmsListPagesMessages['URL_EXIST'];
            $messages = array('status' => "success", 'message' => $msg);
        } else {
            $msg = $cmsListPagesMessages['URL_AVAILABLE'];
            $messages = array('status' => "error", 'message' => $msg);
        }
        return $response->setContent(\Zend\Json\Json::encode($messages));
    }

    /**
     * This action is used for uploading image
     * @return json format data
     * @param void
     * @author Icreon Tech - AS
     */
    public function uploadCmsThumbnailAction() {
        $fileExt = $this->GetFileExt($_FILES['Image']['name']);
        $filename = $this->GenerateMediaFileName($fileExt[1]);    // Generate file name
        $_FILES['Image']['name'] = $filename;                     // assign name to file variable
        $this->getCmsTable();
        $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration
        $error_messages = array(
            'post_max_size' => 'The uploaded file exceeds the post_max_size directive in php.ini', // Set error messages
            'max_file_size' => 'File is too big',
            'min_file_size' => 'File is too small',
            'accept_file_types' => 'Filetype not allowed',
        );
        $options = array(
            'upload_dir' => $upload_file_path['temp_upload_dir'],
            'param_name' => 'Image', //file input name                      // Set configuration
            'inline_file_types' => $upload_file_path['file_types_allowed'],
            'accept_file_types' => $upload_file_path['file_types_allowed'],
            'max_file_size' => $upload_file_path['max_allowed_file_size'],
            'min_file_size' => $upload_file_path['min_allowed_file_size']
        );
        $upload_handler = new UploadHandler($options, true, $error_messages);
        $file_response = $upload_handler->jsonResponceData;
        if (isset($file_response['Image'][0]->error)) {
            $messages = array('status' => "error", 'message' => $file_response['Image'][0]->error);
        } else {
            $messages = array('status' => "success", 'message' => 'Image uploaded', 'filename' => $file_response['Image'][0]->name);
        }
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This action is used to move file from temp to campaign folder
     * @param 1 for add , 2 for edit
     * @return json
     * @author Icreon Tech - AS
     */
    public function moveFileFromTempToCms($filename, $operation = 1, $oldfilename = '') {
        $this->getCmsTable();
        $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration
        $temp_dir = $upload_file_path['temp_upload_dir'];
        $temp_thumbnail_dir = $upload_file_path['temp_upload_thumbnail_dir'];
        $cms_dir = $upload_file_path['assets_upload_dir'] . "cms/";
        $cms_thumbnail_dir = $upload_file_path['assets_upload_dir'] . "cms/thumbnail/";
        $temp_file = $temp_dir . $filename;

        $temp_thumbnail_file = $temp_thumbnail_dir . $filename;
        $cms_filename = $cms_dir . $filename;
        $cms_thumbnail_filename = $cms_thumbnail_dir . $filename;
        if (!empty($filename) && isset($filename)) {
            if (file_exists($temp_file)) {
                if (copy($temp_file, $cms_filename)) {
                    unlink($temp_file);
                }
            }
            if (file_exists($temp_thumbnail_file)) {
                if (copy($temp_thumbnail_file, $cms_thumbnail_filename)) {
                    unlink($temp_thumbnail_file);
                }
            }
            if ($operation == 2) {
                if ($oldfilename != $filename) {
                    $cms_old_filename = $cms_dir . $oldfilename;
                    $cms_old_thumbnail_filename = $cms_thumbnail_dir . $oldfilename;
                    if ($oldfilename != '') {
                        if (file_exists($cms_old_filename)) {
                            unlink($cms_old_filename);
                        }
                        if (file_exists($cms_old_thumbnail_filename)) {
                            unlink($cms_old_thumbnail_filename);
                        }
                    }
                }
            }
        }
    }

    /**
     * This action is used to open pop up
     * @return json
     * @author Icreon Tech - AS
     */
    public function selectMenuAction() {
        $this->checkUserAuthentication();
        $this->getCmsTable();
        $menuForm = new MenuForm();
        $cmsObj = new Cms($this->_adapter);
        $cmsListPagesMessages = array_merge($this->_config['Cms_messages']['config']['CMS_PAGE_LIST_MESSAGE'], $this->_config['Cms_messages']['config']['CMS_ERROR_MSG']);
        $headerMenuLink = $this->getCmsTable()->getMenuList('1');
        $menuArr = $this->buildTreeArray($headerMenuLink);
        $cmsJsonStr = json_encode($menuArr);
        $viewModel = new ViewModel();
        $this->layout('popup');
        $viewModel->setVariables(array(
            'menuForm' => $menuForm,
            'jsLangTranslate' => $cmsListPagesMessages,
            'cmsJsonStr' => $cmsJsonStr
        ));
        return $viewModel;
    }

    /**
     * This function is used to get categories build tree array
     * @param   array
     * @return  array
     * @author  Icreon Tech - DT
     */
    function buildTreeArray(array $elements, $parentId = 0) {
        $branch = array();
        foreach ($elements as $element) {
            if ($element['parent_menu_id'] == $parentId) {
                $children = $this->buildTreeArray($elements, $element['menu_id'], $element['menu']);
                if ($children) {
                    $element['children'] = $children;
                }
                $element['id'] = $element['menu_id'];
                $element['name'] = $element['menu'];
                unset($element['parent_menu_id']);
                $branch[] = $element;
            }
        }
        return $branch;
    }

    /**
     * This function is used to get categories build tree array
     * @param   array
     * @return  array
     * @author  Icreon Tech - AS
     */
    public function editCmsMenuDragDropAction() {
        $this->checkUserAuthentication();
        $this->getCmsTable();
        $cmsObj = new Cms($this->_adapter);
        $cmsListPagesMessages = array_merge($this->_config['Cms_messages']['config']['CMS_PAGE_LIST_MESSAGE'], $this->_config['Cms_messages']['config']['CMS_ERROR_MSG']);
        $request = $this->getRequest();
        $response = $this->getResponse();
        if ($request->isPost()) {
            $parentId = $request->getPost('parentId');
            $sibling = $request->getPost('siblings');
            $childCatArr = explode(',', $request->getPost('siblings'));
            $order = 1;
            foreach ($childCatArr as $key) {
                $param = array();
                $param['parentId'] = $request->getPost('parentId');
                $param['menuId'] = $key;
                $param['sortOrder'] = $order++;
                $param['modifiedBy'] = $this->_auth->getIdentity()->crm_user_id;
                $param['modifiedDate'] = DATE_TIME_FORMAT;
                $this->getCmsTable()->changeOrderMenu($param);
            }
            $messages = array('status' => "success", 'message' => $cmsListPagesMessages['MENU_UPDATE_SUCCESS']);
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This function is used to change status of cms content
     * @param   array
     * @return  array
     * @author  Icreon Tech - AS
     */
    public function updateCmsContentAction() {
        $this->checkUserAuthentication();
        $this->getCmsTable();
        $cmsObj = new Cms($this->_adapter);
        $cmsListPagesMessages = array_merge($this->_config['Cms_messages']['config']['CMS_PAGE_LIST_MESSAGE'], $this->_config['Cms_messages']['config']['CMS_ERROR_MSG']);
        $request = $this->getRequest();
        $response = $this->getResponse();

        if ($request->isPost()) {
            $params = array();
            $params['publish'] = $request->getPost('publish');
            $id = $request->getPost('ids');
            foreach ($id as $key) {
                $params['id'] = $key;
                $params['modifiedBy'] = $this->_auth->getIdentity()->crm_user_id;
                $params['modifiedDate'] = DATE_TIME_FORMAT;
                $this->getCmsTable()->updateCmsContentStatus($params);
            }
            $this->flashMessenger()->addMessage($cmsListPagesMessages['CONTENT_UPDATE_SUCCESS']);
            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
    }

    /**
     * This action is used for uploading image
     * @return json format data
     * @param void
     * @author Icreon Tech - AS
     */
    public function uploadCmsPdfAction() {

        $fileExt = $this->GetFileExt($_FILES['Upload_PDF']['name']);
        $filename = $this->GenerateMediaFileName($fileExt[1]);    //Generate file name
        $_FILES['Upload_PDF']['name'] = $filename;                 // assign name to file variable
        $this->getCmsTable();
        $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration
        $error_messages = array(
            'post_max_size' => 'The uploaded file exceeds the post_max_size directive in php.ini', // Set error messages
            'max_file_size' => 'File is too big',
            'min_file_size' => 'File is too small',
            'accept_file_types' => 'Filetype not allowed',
        );
        $options = array(
            'upload_dir' => $upload_file_path['temp_upload_dir'],
            'param_name' => 'Upload_PDF', //file input name                      // Set configuration
            'inline_file_types' => $upload_file_path['doc_file_types_allowed'],
            'accept_file_types' => '/\.pdf$/i',
            'max_file_size' => $upload_file_path['max_allowed_file_size'],
            'min_file_size' => $upload_file_path['min_allowed_file_size']
        );
        $upload_handler = new UploadHandler($options, true, $error_messages);
        $file_response = $upload_handler->jsonResponceData;
        if (isset($file_response['Upload_PDF'][0]->error)) {
            $messages = array('status' => "error", 'message' => $file_response['Upload_PDF'][0]->error);
        } else {
            $messages = array('status' => "success", 'message' => 'PDF uploaded', 'filename' => $file_response['Upload_PDF'][0]->name);
        }
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }

    /**
     * This function is used to insert into the change log
     * @param  array
     * @return void
     * @author Icreon Tech - AP
     */
    public function crmGalleryChangeLogs($param = array()) {
        $changeLogArray = array();
        $changeLogArray['table_name'] = 'tbl_cms_gallery_change_logs';
        $changeLogArray['activity'] = $param['activity'];/** 1 == for insert,2 == for update, 3 == for delete */
        $changeLogArray['id'] = $param['id'];
        $changeLogArray['crm_user_id'] = $this->_auth->getIdentity()->crm_user_id;
        $changeLogArray['added_date'] = DATE_TIME_FORMAT;
        $this->getServiceLocator()->get('Logs\Model\ChangelogTable')->insertChangeLog($changeLogArray);
    }

    public function viewCmsPageAction() { 
        $this->getCmsTable();
        
        $moduleSession = new Container('moduleSession');
        if($moduleSession->moduleGroup == 3)
            $this->layout('wohlayout');
                    
		$searchParam = array();
        $searchParam['currentTime'] = DATE_TIME_FORMAT;
        $kioskResult = $this->getServiceLocator()->get('Kiosk\Model\KioskManagementTable')->searchKiosk($searchParam);
        $param = $this->params()->fromRoute();

		//asd($_SERVER['REQUEST_URI']);
        
        foreach($kioskResult as $key=>$val){
            $kioskIPArray[] = $val['kiosk_ip'];
        }
		$pageLayoutFlag = '';     
        $visitationModule = 0;
        if(in_array($_SERVER['REMOTE_ADDR'], $kioskIPArray)) { 
            $kioskModuleParam = array();
            $kioskModuleParam['name_or_ip'] = $_SERVER['REMOTE_ADDR'];
            $kioskModuleResult = $this->getServiceLocator()->get('Kiosk\Model\KioskModuleTable')->searchKioskModule($kioskModuleParam);

            $moduleArray = explode(',', $kioskModuleResult['0']['modules']);
            $moduleStatusArray = explode(',', $kioskModuleResult['0']['module_status']);
            $visitationKey = array_search('15', $moduleArray); // 15 = visitation;
            if($moduleStatusArray[$visitationKey] == 1)
                $visitationModule = 1;
            
           
            if(count($kioskModuleResult) >0 ) { 
                $kioskModuleArray = explode(',', $kioskModuleResult[0]['modules']);
                $kioskModuleStatusArray = explode(',', $kioskModuleResult[0]['module_status']);
				$moduleKay1 = array_search('3',$kioskModuleArray); // 3 for reservation (Purchase session)
				$moduleKay2 = array_search('14',$kioskModuleArray); 
				$moduleKay3 = array_search('15',$kioskModuleArray); 
                if($kioskModuleStatusArray[$moduleKay1] == 1 || $kioskModuleStatusArray[$moduleKay2] == 1 ||  $kioskModuleStatusArray[$moduleKay3] == 1) {
                   $this->layout('pos');     
                }
            }elseif($_SERVER['REQUEST_URI'] == '/pos-help' || $_SERVER['REQUEST_URI']=='/privacy')
				 $this->layout('pos');
			elseif($_SERVER['REQUEST_URI']=='/woh-privacy')
				 $this->layout('wohlayout');

        }
		
        $requestUriName[1] = $this->getServiceLocator()->get('request')->getRequestUri();
        $tmpArr = explode('?', $requestUriName[1], 2);
        if (isset($tmpArr[0]))
        {
            $requestUriName[1] = $tmpArr[0];
        }
        $cmsObj = new Cms($this->_adapter);
        $filePath = $this->_config['file_upload_path']['assets_url'];
        $fileRelativePath = $this->_config['file_upload_path']['assets_upload_dir'];
        $jsFilePath = $this->_config['js_file_path']['path'];
        $jsVersion = $this->_config['file_versions']['js_version'];
        $cssFilePath = $this->_config['css_file_path']['path'];
        $cssVersion = $this->_config['file_versions']['css_version'];
        $cmsListPagesMessages = array_merge($this->_config['Cms_messages']['config']['CMS_PAGE_LIST_MESSAGE'], $this->_config['Cms_messages']['config']['CMS_ERROR_MSG']);
        $routeName = explode('/', $requestUriName[1]);
        if (isset($routeName[2]) && !empty($routeName[2]) && $this->decrypt($routeName[2]) == $routeName[1]) {
            $routeName[2] = '1';
        } else {
            $routeName[2] = '';
        }
        $content = $this->getCmsTable()->getCrmContentUrl($routeName);
        $patternGallery = "/{gallery}(.*?){\/gallery}/";
        $patternTabbed = "/{tabbed}(.*?){\/tabbed}/";
        preg_match_all($patternTabbed, $content[0]['description'], $tabbedGallery);
        if (isset($tabbedGallery) && !empty($tabbedGallery) && (count($tabbedGallery[0]) > 0)) {
            foreach ($tabbedGallery[0] as $key => $val) {
                $tabGallery[0][0] = $val;
                $content[0]['description'] = $this->insertGalleryDocument($content[0]['description'], $tabGallery, '1', $key);
            }
        }
        preg_match_all($patternGallery, $content[0]['description'], $gallery);
        if (isset($gallery) && !empty($gallery)) {
            $content[0]['description'] = $this->insertGalleryDocument($content[0]['description'], $gallery, '2', '0');
        }
        $searchCat = '';
        $pageContent = "";
        $template = "cms/cms/" . $content[0]['template_file_name'];
        if ($content[0]['content_type'] == "View") {
            $searchParam['type'] = $content[0]['page_content_id'];
            $searchParam['status'] = 1;
            $searchParam['content_id'] = '';
            $searchParam['startIndex'] = "";
            $searchParam['recordLimit'] = "";
            $searchParam['sortField'] = "";
            $searchParam['sortOrder'] = "";
            if ($content[0]['page_content_id'] == 4) {
                $searchParam['sortField'] = "content_date";
                $searchParam['sortOrder'] = "desc";
                $searchResult = $this->getCmsTable()->getCrmContents($searchParam);
                $pageContent = $searchResult;
            } else if ($content[0]['page_content_id'] == 5) {
                $searchParam['sortField'] = "content_title";
                $searchParam['sortOrder'] = "asc";
                $searchResult = $this->getCmsTable()->getCrmContents($searchParam);
                $pageContent = $searchResult;
            } else if ($content[0]['page_content_id'] == 8) {
                $searchCat = $this->getCmsTable()->getFaCaterogy();
                $faParam['faCat'] = $searchCat[0]['fa_category_id'];
                $faParam['faPage'] = $content[0]['page_content_id'];
                $searchResult = $this->getCmsTable()->getFaContents($faParam);
                foreach ($searchResult as $key => $val) {
                    $searchParam['passenger_id'] = $val['fa_passenger_id'];
                    $date = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getPassengerRecord($searchParam);
                    $searchResult[$key]['name'] = (isset($date[0]['PRIN_FIRST_NAME']) && !empty($date[0]['PRIN_FIRST_NAME']) ? $date[0]['PRIN_FIRST_NAME'] : '' )." ".(isset($date[0]['PRIN_LAST_NAME']) && !empty($date[0]['PRIN_LAST_NAME']) ? $date[0]['PRIN_LAST_NAME'] : '' );
                    $searchResult[$key]['content_date'] = $date[0]['DATE_ARRIVE'];                    
                }          
                
                $pageContent = $searchResult;
            } else if ($content[0]['page_content_id'] == 9) {
                $haParam['pageContent'] = '10';
                $haParam['contentType'] = '11';
                $searchResult = $this->getCmsTable()->getHaContents($haParam);
                $pageContent = $searchResult;
            } else if ($content[0]['page_content_id'] == 10) {
                $haParam['pageContent'] = '';
                $haParam['contentType'] = '11';
                $searchResult = $this->getCmsTable()->getHaContents($haParam);
                $pageContent = $searchResult;
            } else if ($content[0]['page_content_id'] == 12) {
                /** Geneology Main Page */
                $haParam['pageContent'] = '13';
                $haParam['contentType'] = '';
                $searchResult = $this->getCmsTable()->getHaContents($haParam);
                $pageContent = $searchResult;
            } else if ($content[0]['page_content_id'] == 13) {
                /** Geneology Description Page */
                $haParam['pageContent'] = '7';
                $haParam['parentId'] = $content[0]['content_id'];
                $searchResult = $this->getCmsTable()->getGenContents($haParam);
                $pageContent = $searchResult;
            } else if ($content[0]['page_content_id'] == 14) {
                /** Geneology Details Page */
                $haParam['pageContent'] = '15';
                $haParam['parentId'] = $content[0]['content_id'];
                $searchResult = $this->getCmsTable()->getGenContents($haParam);
                $pageContent = $searchResult;
            } else {
                $searchResult = $this->getCmsTable()->getCrmContents($searchParam);
                $pageContent = $searchResult;
            }
        } else if ($content[0]['content_type'] == "Announcements") {
            $usrAccessLabel = '1';
            if (isset($this->_auth->getIdentity()->user_id) && $this->_auth->getIdentity()->user_id != '') {
                $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->_auth->getIdentity()->user_id));
                if (isset($userDetail['membership_id']) and $userDetail['membership_id'] == "1")
                    $usrAccessLabel = '2';
                else if (isset($userDetail['membership_id']) and $userDetail['membership_id'] != "1" and strtotime($userDetail['membership_expire']) > strtotime(date("Y-m-d", strtotime(DATE_TIME_FORMAT))))
                    $usrAccessLabel = '3';
                else
                    $usrAccessLabel = '2';
            }
            $searchResult = $this->getCmsTable()->getAnnouncementPre($content);
            //asd($searchResult);
            $pageContent = $searchResult;
        }

        $adParam['page_url'] = $content[0]['content_url'];
        $adParam['ad_zone_id'] = '3';   // 3 for right section
        $adParam['curr_date'] = DATE_TIME_FORMAT;
        $adParam['start_Index'] = '0';
        $adParam['record_limit'] = $content[0]['num_ad_blocks'];
        $adParam['rand_flag'] = '1';
        $adsContent = $this->getAdTable()->getCrmFrontAd($adParam);
        $rightList = explode(',', $content[0]['content_block_id']);
        $rightPos = explode(',', $content[0]['content_position']);
        for ($i = 0; $i < count($rightList); $i++) {
            $htmlContent = '';
            switch ($rightList[$i]) {
                case 1:
                    $htmlContent = $this->flagOfFacesRandomImages($jsFilePath,$jsVersion,$filePath,$fileRelativePath);
                    break;

                case 2:
                    $htmlContent = $this->wallOfHonorListingSearch('', '', $this->_config['js_file_path']['path'],$this->_config['file_versions']['js_version']);
                    break;

                case 3:
                    $htmlContent = $this->additionalResources();
                    break;

                case 4:
                    $htmlContent = $this->donate();
                    break;

                case 6:
                    if (isset($this->_auth->getIdentity()->user_id) && !empty($this->_auth->getIdentity()->user_id)) {
                        $isLogin = true;
                    } else {
                        $isLogin = false;
                    }
                    $htmlContent = $this->haveAnAccount($isLogin);
                    break;
                case 7:
                    $htmlContent = $this->wallOfHonorMenu($adParam['page_url'], $this->getServiceLocator()->get('Cms\Model\CmsTable')->getCrmCmsMenus(array('parentMenuId' => '8')));
                    break;
                case 8:
                    $htmlContent = $this->announcementsList($jsFilePath,$jsVersion,$cssFilePath,$cssVersion);
                    break;
                case 9:
                    $htmlContent = $this->blogFeaturedData($this->getServiceLocator()->get('Blog\Model\BlogTable')->getBlogPostData(), $this->getServiceLocator()->get('Blog\Model\BlogTable')->getBlogFeaturedPostData(), $this->_config['file_upload_path']);
                    break;
                case 10:
                    $htmlContent = $this->blogRecentData($this->getServiceLocator()->get('Blog\Model\BlogTable')->getBlogPostData(), $this->getServiceLocator()->get('Blog\Model\BlogTable')->getBlogRecentPostData(), $this->_config['file_upload_path']);
                    break;
                default:
                    $htmlContent = "";
            }
            $rightContent[$i]['id'] = $rightList[$i];
            $rightContent[$i]['pos'] = $rightPos[0];
            $rightContent[$i]['content'] = $htmlContent;
        }
        $template = "cms/cms/" . $content[0]['template_file_name'];
        $viewModel = new ViewModel();
		//echo $template;die;
        $viewModel->setTemplate($template);
        $request = $this->getRequest();
        $keyword = "";
        if ($request->isPost()) {
            $content[0]['description'] = $str;
        }
        $server_url = $this->getRequest()->getUri()->getScheme() . '://' . $this->getRequest()->getUri()->getHost();
        $server_url . "/" . $content[0]['content_url'];
		if($content[0]['content_url'] == 'woh-shipping-terms' || $content[0]['content_url'] == 'woh-terms-of-use')
			$viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'keyword' => $keyword,
            'content' => $content,
            'rightContent' => $rightContent,
            'adsContent' => $adsContent,
            'jsLangTranslate' => $cmsListPagesMessages,
            'pageContent' => $pageContent,
            'filePath' => $filePath,
            'fileRelativePath' => $fileRelativePath,
            'serverUrl' => $server_url,
            'searchCat' => $searchCat
        ));
        return $viewModel;
    }

    /**
     *  This function is used to download file
     * @param string
     * @return void
     * @author Icreon Tech - AS
     * */
    public function downloadFileAction() {
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $this->getCmsTable();
        $uploadFilePath = $this->_config['file_upload_path'];   // get file upload configuration
        $activityDir = $uploadFilePath['assets_upload_dir'] . "/cms/";
        $fileName = $activityDir . $params['fileName'];
        if (file_exists($fileName)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($fileName));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($fileName));
            ob_clean();
            flush();
            readfile($fileName);
            exit;
        }
    }

    /**
     *  This function is used to insert gallery code in the editor
     * @param string
     * @return void
     * @author Icreon Tech - AS
     * */
    public function insertGalleryAction() {
        $this->checkUserAuthentication();
        $this->layout('popup');
        $this->getCmsTable();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $insertGalleryForm = new InsertGalleryForm();
        $cmsListPagesMessages = array_merge($this->_config['Cms_messages']['config']['CMS_PAGE_LIST_MESSAGE'], $this->_config['Cms_messages']['config']['CMS_ERROR_MSG']);
        $response = $this->getResponse();
        if ($request->isPost()) {
            if ($request->getPost('is_tabbed') == '1') {
                $result = "{tabbed}" . "<br />";
                foreach ($request->getPost('gallery') as $key => $val) {
                    $result = $result . "{gallery}{id}" . $val . "{/id}{thumb}" . $request->getPost('thumbnails') . "{/thumb}{/gallery}" . "<br />";
                }
                $result = $result . "{/tabbed}";
            } else {
                $result = '';
                foreach ($request->getPost('gallery') as $key => $val) {
                    $result = $result . "{gallery}{id}" . $val . "{/id}{thumb}" . $request->getPost('thumbnails') . "{/thumb}{/gallery}" . "<br />";
                }
            }
            $response->setContent(\Zend\Json\Json::encode($result));
            return $response;
        }
        $viewModel = new ViewModel();
        $searchParam['gallery_name'] = '';
        $searchParam['startIndex'] = '';
        $searchParam['recordLimit'] = '';
        $searchParam['sortField'] = 'gallery_name';
        $searchParam['sortOrder'] = 'asc';
        $searchResult = $this->getCmsTable()->getCrmGalleries($searchParam);
        $galleryList = array();
        $galleryList[''] = 'Select';
        foreach ($searchResult as $key => $val) {
            if ($val['num_image'] != 0 || $val['num_video'] != 0) {
                $galleryList[$val['gallery_id']] = stripslashes($val['gallery_name']);
            }
        }
        $insertGalleryForm->get('gallery')->setAttribute('options', $galleryList);
        $viewModel->setVariables(array(
            'insertGallery' => $insertGalleryForm,
            'jsLangTranslate' => $cmsListPagesMessages
        ));
        return $viewModel;
    }

    public function contactUsAction() {
        $request = $this->getRequest();
        $this->getCmsTable();
        $cmsListPagesMessages = $this->_config['Cms_messages']['config']['CMS_PAGE_LIST_MESSAGE'];
        $cms = new Cms($this->_adapter);
        $cmsType = $this->getCmsTable()->getCrmCmsContentTypes();
        $messages = array();
        $viewModel = new ViewModel();
        $response = $this->getResponse();
        if (isset($this->_auth->getIdentity()->user_id) && !empty($this->_auth->getIdentity()->user_id)) {
            $isLogin = true;
        } else {
            $isLogin = false;
        }
        $rightContent = $this->haveAnAccount($isLogin);
        $viewModel->setVariables(array(
            'messages' => $messages,
            'jsLangTranslate' => $cmsListPagesMessages,
            'rightContent' => $rightContent
        ));
        return $viewModel;
    }

    public function insertGalleryDocumentAction() {
        $this->getCmsTable();
        $request = $this->getRequest();
        $params = $this->params()->fromRoute();
        $response = $this->getResponse();
        $galleryMsg = array_merge($this->_config['file_upload_path'], $this->_config['Cms_messages']['config']['crm-gallery']);
        $gallery['gallery_id'] = $request->getPost('galleryId');
        $gallery['thumb'] = $request->getPost('thumb');
        $gallery['orientation'] = $request->getPost('orientation');
        $searchResult = $this->getCmsTable()->getCrmGalleryInfo($gallery);
        $contentFile = !empty($searchResult[0]['content_file_name']) ? explode(',', $searchResult[0]['content_file_name']) : '';
        $physicalFile = explode(',', $searchResult[0]['phy_file_name']);
        $galleryFile = explode(',', $searchResult[0]['file_type']);
        for ($i = 0; $i < $gallery['thumb']; $i++) {
            if (isset($contentFile[$i]) && !empty($contentFile[$i]))
                $contentFileName[$i] = $contentFile[$i];
        }
        for ($i = 0; $i < $gallery['thumb']; $i++) {
            if (isset($physicalFile[$i]) && !empty($physicalFile[$i]))
                $physicalFileName[$i] = $physicalFile[$i];
        }
        for ($i = 0; $i < $gallery['thumb']; $i++) {
            if (isset($galleryFile[$i]) && !empty($galleryFile[$i]))
                $galleryFileType[$i] = $galleryFile[$i];
        }
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'orientation' => $gallery['orientation'],
            'searchResult' => $searchResult[0],
            'contentFileName' => $contentFileName,
            'physicalFileName' => $physicalFileName,
            'galleryFileType' => $galleryFileType,
            'imgPath' => $galleryMsg['assets_url'] . 'cmsgallery/' . $this->FolderStructByUserId($gallery['gallery_id']) . 'image'
        ));
        return $viewModel;
    }

    /**
     * This function is used to manage famous arrival category
     * @param string
     * @return void
     * @author Icreon Tech - AS
     * */
    public function manageFaCategoryAction() {
        $this->checkUserAuthentication();
        $this->getCmsTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $categoryForm = new AddFaCategoryForm();
        $cmsObj = new Cms($this->_adapter);
        $cmsListPagesMessages = array_merge($this->_config['Cms_messages']['config']['CMS_PAGE_LIST_MESSAGE'], $this->_config['Cms_messages']['config']['CMS_ERROR_MSG']);
        $categoryList = $this->getCmsTable()->getFaCaterogy();
        if ($request->isPost()) {
            $param['id'] = $request->getPost('id');
            $param['name'] = $request->getPost('name');
            $param['img_name'] = $request->getPost('img_name');
            $param['old_image'] = $request->getPost('old_image');
            $param['added_by'] = $this->_auth->getIdentity()->crm_user_id;
            $param['added_date'] = DATE_TIME_FORMAT;
            if (isset($param['id']) && !empty($param['id'])) {
                if (isset($param['img_name']) && !empty($param['img_name']) && ($param['img_name'] != $param['old_image'])) {
                    $this->moveFileFromTempToCms($param['img_name']);
                }
                $this->getCmsTable()->updateFaCategory($param);
            } else {
                $this->getCmsTable()->insertFaCategory($param);
                $this->moveFileFromTempToCms($param['img_name']);
            }
            $messages = array('status' => "success");
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        }
        $viewModel = new ViewModel();
        $this->layout('popup');
        $viewModel->setVariables(array(
            'jsLangTranslate' => $cmsListPagesMessages,
            'categoryList' => $categoryList,
            'categoryFrom' => $categoryForm
        ));
        return $viewModel;
    }

    public function flagOfFacesRandomImages($jsFilePath,$jsVersion,$assets_url,$assets_upload_dir) {
        try {
            $fofDirThumb = $assets_url . "fof/thumb/";
            $fofDir = $assets_url . "fof/";
            $fofDirThumbRelative = $assets_upload_dir . "fof/thumb/";


            $dataParam = array();
            $dataParam['fof_id'] = '';
            $dataParam['people_photo'] = '';
            $dataParam['record_limit'] = 50;
            $dataParam['start_index'] = 0;
            $arrFofImagesSearch = $this->getFofTable()->getFOFImages($dataParam);



            if (isset($arrFofImagesSearch) and count($arrFofImagesSearch) > 0) {
                shuffle($arrFofImagesSearch);
            } else {
                $arrFofImagesSearch = array();
            }

            $htmlContent = '<script type="text/javascript" src="'.$jsFilePath.'/js/user/fof.cms.js?v='.$jsVersion.'"></script>';
            $htmlContent .= '<div class="flag-random-right-section clearfix"><h1>Flag Random Images</h1><div class="inner">';
            $countImages = 0;
            if (isset($arrFofImagesSearch) and count($arrFofImagesSearch) > 0) {
                foreach ($arrFofImagesSearch as $valsFof) {
                    if (isset($valsFof['fof_image']) and trim($valsFof['fof_image']) != "" and file_exists($fofDirThumbRelative . $valsFof['fof_image']) and $countImages < 30) {

                        $htmlContent .= '<a onclick="return fofImage(\'' . $valsFof['fof_id'] . '\');"  rel="lightbox"><img width="72" height="72" src="' . $fofDirThumb . $valsFof['fof_image'] . '" style="cursor: pointer;" /></a>';
                        $countImages = $countImages + 1;
                    }
                }
            } else {
                $htmlContent .= 'No record found';
            }
            $htmlContent .= '</div></div>';

            return $htmlContent;
        } catch (Exception $e) {
            return '';
        }
    }

    public function wallOfHonorListingSearch($Url, $arrVal, $jsFilePath, $jsVersion) {
        try {

            $first_name = '';
            $last_name = '';
            $panel_number = '';
            $other_part_name = '';
            $country = '';
            $donated_by = '';
            if (isset($arrVal) and is_array($arrVal)) {
                $first_name = isset($arrVal['first_name']) ? $arrVal['first_name'] : '';
                $last_name = isset($arrVal['last_name']) ? $arrVal['last_name'] : '';
                $panel_number = isset($arrVal['panel_number']) ? $arrVal['panel_number'] : '';
                $other_part_name = isset($arrVal['other_part_name']) ? $arrVal['other_part_name'] : '';
                $country = isset($arrVal['country']) ? $arrVal['country'] : '';
                $donated_by = isset($arrVal['donated_by']) ? $arrVal['donated_by'] : '';
            }
			
			$htmlContent = '<script type="text/javascript" src="'.$jsFilePath.'/js/user/woh.search.cms.js?v='.$jsVersion.'"></script>';
            $htmlContent .= '<form id="search_woh" name="search_woh" method="post" action="/search-woh" onsubmit="return searchWoh(' . "'" . $Url . "'" . ');">';
            $htmlContent .= '<input type="hidden" value="1" id="page" name="page">';
            $htmlContent .= '<input type="hidden" value="usr_wall_of_honor.woh_id" id="sort_field" name="sort_field">';
            $htmlContent .= '<input type="hidden" value="desc" id="sort_order" name="sort_order">';
            $htmlContent .= '<input type="hidden" value="0" id="start_index" name="start_index">';
            $htmlContent .= '<input type="hidden" value="7" id="limit" name="limit">';
            $htmlContent .= '<input type="hidden" value="2" id="view_woh_type" name="view_woh_type">';
            $htmlContent .= '<div class="wall-of-honor-bg">';
            $htmlContent .= '<div class="wrapper">';
            $htmlContent .= '<div class="woh-detail-section">';
            $htmlContent .= '<div class="leftarea-box"> <span>The American Immigrant</span>';
            $htmlContent .= '<h1>Wall Of Honor</h1>';
            if ($Url == 'search-woh') {
                //$htmlContent .= '<div class="question"><a href="/about-the-woh">Need more information? Click here.</a></div>';
            }
            $htmlContent .= '</div>';
            $htmlContent .= '<div class="rightarea-box">';
            $htmlContent .= '<input type="text" value="' . $first_name . '" placeholder="First Name" class="button placeholder" id="first_name" name="first_name">';
            $htmlContent .= '<div class="left">';
            $htmlContent .= '<small>*</small>';
            $htmlContent .= '<input type="text" value="' . $last_name . '" placeholder="Last Name" class="button placeholder" id="last_name" name="last_name">';
            $htmlContent .= '</div>';
            $htmlContent .= '<input type="text" value="' . $other_part_name . '" placeholder="Other Part of Name" class="button placeholder" id="other_part_name" name="other_part_name">';
            $htmlContent .= '<input type="text" value="' . $country . '" placeholder="Country of Origin" class="country placeholder" id="country" name="country">';
            $htmlContent .= '<input type="text" value="' . $donated_by . '" placeholder="Donated By" class="woh-input button placeholder" id="donated_by_add" name="donated_by_add">';
            $htmlContent .= '<span class="or">OR</span>';
            $htmlContent .= '<input type="text" value="' . $panel_number . '" placeholder="Panel #" class="woh-input1 button placeholder" id="panel_number" name="panel_number">';
            $htmlContent .= '<input type="submit" value="Find" class="find-btn button" id="submitsearch1" name="submitsearch1">';
            $htmlContent .= '</div>';
            $htmlContent .= '</div>';
            $htmlContent .= '</div>';
            $htmlContent .= '</div>';
            $htmlContent .= '</form>';

            return $htmlContent;
        } catch (Exception $e) {
            return '';
        }
    }

    public function wallOfHonorMenu($menuName, $categoryList) {
        try {
            $arrClass = array('about', 'search', 'name', 'visiting', 'gift', 'photo');
            $htmlData = '<div class="left-nav"><ul>';


            if (isset($categoryList) and count($categoryList) > 0) {
                $categoryListNew = array();

                $count0 = 0;
                $count1 = 3;
                foreach ($categoryList as $val0) {
                    if ($count0 > 0) {
                        $categoryListNew[$count1] = $val0;
                        $count1 = $count1 + 1;
                    } else {
                        $categoryListNew[0] = $val0;
                    }
                    $count0 = $count0 + 1;
                }

                $categoryListNew[1] = array('menuId' => '', 'menuName' => 'Search Wall Of Honor', 'menuURL' => 'search-woh');
                $categoryListNew[2] = array('menuId' => '', 'menuName' => 'Add Wall Of Honor', 'menuURL' => 'add-woh');
                ksort($categoryListNew);
                $count = 0;
                foreach ($categoryListNew as $val) {
                    if (isset($menuName) and trim($menuName) != "" and trim($menuName) == $val['menuURL']) {
                        $class = ' class="active" ';
                    } else {
                        $class = '';
                    }
                    $htmlData .= '<li ' . $class . '><a href="/' . $val['menuURL'] . '" title="' . $val['menuName'] . '" class="' . $arrClass[$count] . '"></a></li>';
                    $count++;
                }
            }
            $htmlData .= '</ul></div>';
            return $htmlData;
        } catch (Exception $e) {
            return '';
        }
    }

//    public function wallOfHonorListingSearch() {
//        try {
//            $param = array();
//            $param['sort_field'] = 'usr_wall_of_honor.woh_id';
//            $param['sort_order'] = 'desc';
//            $param['record_limit'] = 6;
//            $wohData = $this->getWohTable()->getUserWoh($param);
//            $htmlContent = '';
//            $htmlContent .= '<div class="block">';
//            $htmlContent .= '<div class="content clearfix">';
//            if (isset($wohData) and count($wohData) > 0) {
//                $htmlContent .= '<h2>Is your family name on the<br>Wall of Honor?</h2>';
//                foreach ($wohData as $key => $val) {
//                    $namingConventionArr = $this->getNamingConventionArr($val['attribute_option_name'], '', $val);
//                    $htmlContent .= '<label class="normal">' . $namingConventionArr['honoree_name'] . '</label>';
//                }
//            }
//
//            $htmlContent .= '<form id="search_woh" name="search_woh" method="post" action="/search-woh">';
//            $htmlContent .= '<div class="form-sep"><label>First Name (Initial)</label><input id="first_name_initial" class="first-name" type="text" value="" maxlength="1" 
//name="first_name_initial" /></div>';
//            $htmlContent .= '<div class="form-sep"><label>Last Name</label><input id="last_name" type="text" value="" name="last_name" /></div>';
//            $htmlContent .= '<div class="form-sep"><input id="submitsearch1" class="button" type="submit" value="Search" name="submitsearch1"></div>';
//            $htmlContent .= '<div class="form-sep"><label>OR</label></div>';
//            $htmlContent .= '<div class="form-sep">Panel #<input id="panel_number" type="text" value="" name="panel_number"></div>';
//            $htmlContent .= '<div class="form-sep"><input id="submitsearch2" class="button" type="submit" value="Search" name="submitsearch2"></div>';
//            $htmlContent .= '</form></div></div>';
//
//            return $htmlContent;
//        } catch (Exception $e) {
//            return '';
//        }
//    }

    /**
     * This function is used to get additionalResources
     * @param string
     * @return void
     * @author Icreon Tech - NS
     * */
    public function additionalResources() {
        try {
            $htmlContent = '';
            $htmlContent .= '<div class="block family-right-box"><div class="content">';
            $htmlContent .= '<h1>Additional Resources:</h1><ul>';
            $htmlContent .= '<li><a href="#">Click here for the National Park Service - Statue of Liberty Web site.</a></li>';
            $htmlContent .= '<li><a href="#">Click here for the Statue of Liberty Light Show.</a></li>';
            $htmlContent .= '</ul></div></div>';
            return $htmlContent;
        } catch (Exception $e) {
            return '';
        }
    }

    public function donate() {
        try {
            $htmlContent = '';
            $htmlContent .= '
		    <script type="text/javascript">
				   $(document).ready(function() {
				   
						jQuery.validator.addMethod("floating", function(value, element) {
							return this.optional(element) || /^-?\d*(\.\d{1,2})?$/.test(value);
						},"Please enter valid value.");
	
						$("#donation_add_to_cart_plugin").validate({
							/*submitHandler: function() {
							$.colorbox({href:"/add-to-cart-donation-front-cms/"+$("#donation_add_to_cart_plugin > #amount").val(),fastIframe:false,width:"652px",height:"252px",iframe:true,onComplete: function(){ $("#addCartDonationCms").css({"display":"block"}); }});
							},*/
						
							rules: {
								amount: {
									required : true,
									floating : true,
									minlength : 1,
									maxlength : 10,
									min:1
								}
							},
							messages: {
								amount : {
									required : "Please enter donation amount."
								}
							}
						});
					})
			  </script> 		
		   ';
            $htmlContent .= '<div class="block donate-box border clearfix m-t-none"><div class="content white-bg">';
            $htmlContent .= '<h2>Donate</h2>';
            $htmlContent .= '<p>Ellis Island and the Statue of Liberty need your support. Donate now to help preserve the islands for future generations.</p>';
            $htmlContent .= '<form action="/donate" method="post" name="donation_add_to_cart_plugin_donate" id="donation_add_to_cart_plugin">';
            $htmlContent .= '<input type="hidden" name="matching_gift_front" id="matching_gift_front" value="0" />';
            $htmlContent .= '$&nbsp;<input name="amount" type="text" id="amount" value="0" maxlength="10" />';
            $htmlContent .= '<input name="donationaddtocartplugin_test" type="submit" id="donationaddtocartplugin_test" class="button pink semibold" value="Donate Now">';
            $htmlContent .= '</form>';
            $htmlContent .= '</div></div>';

            return $htmlContent;
        } catch (Exception $e) {
            return '';
        }
    }

    /**
     * This function is used to get haveAnAccount
     * @param string
     * @return void
     * @author Icreon Tech - NS
     * */
    public function haveAnAccount($isLogin) {
        //echo $isLogin;
        try {
            if (isset($_SERVER['HTTP_REFERER'])) {
                $refererUrl = $_SERVER['HTTP_REFERER'];
            } else {
                if (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] == '/login') {
                    $refererUrl = SITE_URL . '/profile';
                } else {
                    $refererUrl = SITE_URL . $_SERVER['REQUEST_URI'];
                }
            }

            if ($isLogin == false) {
                $loginAction = '<a class="btn-green button" href="javascript:void(0)" onClick="checkUserLogin(\'' . $this->encrypt('/profile') . '\')">Login</a>';
            } else {
                $loginAction = '<a class="btn-green button" href="/login/' . $this->encrypt($refererUrl) . '">Login</a>';
            }

            $postData[0] = '';
            $postData[1] = '';
            $postData[3] = 'asc';
            $postData[2] = 'tbl_membership.minimun_donation_amount';
            $membershipData = $this->getServiceLocator()->get('User\Model\MembershipTable')->getAllMembership($postData);
            $i = 0;
            foreach ($membershipData as $key) {
                if ($key['membership_id'] != '1' && $key['minimun_donation_amount'] > 0) {
                    if ($i == 0) {
                        $min_donation = $key['minimun_donation_amount'];
                    } else {
                        if ($min_donation > $key['minimun_donation_amount']) {
                            $min_donation = $key['minimun_donation_amount'];
                        }
                    }
                    $i++;
                }
            }
            $htmlContent = '';
            $htmlContent .= '<div class="foundation-right border">
                            <h1>have an account?</h1>
                            <p>Create a free account to search for family arrival records and learn more about Lady Liberty and Ellis Island.</p>
                            <h2>Be a part of history!</h2>' . $loginAction . '<a class="btn-blue button" onclick="signupPopup();" href="javascript:void(0);">Join us!</a></div>';

            return $htmlContent;
        } catch (Exception $e) {
            return '';
        }
    }

    /**
     * This function is used to get announcementsList
     * @param string
     * @return void
     * @author Icreon Tech - NS
     * */
    public function announcementsList($jsFilePath,$jsVersion,$cssFilePath,$cssVersion) {
        $this->getCmsTable();
        try {

            //User Status 1 :  Non-logged in , 2 : Logged in Non-member, 3 : Logged in Members                  
            $usrAccessLabel = '1';
            if (isset($this->_auth->getIdentity()->user_id) && $this->_auth->getIdentity()->user_id != '') {
                $userDetail = $this->getServiceLocator()->get('User\Model\ContactTable')->getUserDetail(array('user_id' => $this->_auth->getIdentity()->user_id));
                if (isset($userDetail['membership_id']) and $userDetail['membership_id'] == "1")
                    $usrAccessLabel = '2';
                else if (isset($userDetail['membership_id']) and $userDetail['membership_id'] != "1" and strtotime($userDetail['membership_expire']) > strtotime(date("Y-m-d", strtotime(DATE_TIME_FORMAT))))
                    $usrAccessLabel = '3';
                else
                    $usrAccessLabel = '2';
            }

            $announcementsParam = array(
                'startIndex' => '',
                'recordLimit' => '',
                'sortField' => 'modified_date',
                'sortOrder' => 'DESC',
                'status' => '1',
                'type' => '6',
                'content_id' => '',
                'access_label' => $usrAccessLabel,
                'content_date' => date("Y-m-d", strtotime(DATE_TIME_FORMAT))
            );

            $latestAnnoucment = $this->getServiceLocator()->get('Cms\Model\CmsTable')->getCrmContents($announcementsParam);
			$htmlContent = '<div id="message_center" class="message-center white-bg">';
            $htmlContent .= '<script type="text/javascript" src="'.$jsFilePath.'/js/jquery.jscrollpane.min.js?v='.$jsVersion.'"></script><link rel="stylesheet" href="'.$cssFilePath.'/css/scroll.css?v='.$cssVersion.'">';
            $htmlContent .= '<script type="text/javascript">$(function(){ $(".scroll-pane").jScrollPane(); });</script>';
            $htmlContent .= '<div class="scroll-pane message-scroll">';
            $htmlContent .= '<h1>Related Announcements</h1>';
            if (isset($latestAnnoucment) and is_array($latestAnnoucment) and count($latestAnnoucment) > 0) {
                $htmlContent .= '<ul>';
                foreach ($latestAnnoucment as $laVal) {
                    $linkAnnoncment = (isset($laVal['content_url']) and trim($laVal['content_url']) != "") ? ' href = "' . trim($laVal['content_url']) . '"  ' : '';
                    if ($linkAnnoncment != '') {
                        $htmlContent .= '<li><span><a ' . $linkAnnoncment . '>' . $laVal['content_title'] . '</a><a ' . $linkAnnoncment . '><small></small></a></span></li>';
                    } else {
                        $htmlContent .= '<li><span>' . $laVal['content_title'] . '</span></li>';
                    }
                }
                $htmlContent .= '</ul>';
            }
            $htmlContent .= '</div>';
            $htmlContent .= '</div>';
            return $htmlContent;
        } catch (Exception $e) {
            return '';
        }
    }

    /**
     * This function is used to get blogFeaturedData
     * @param string
     * @return void
     * @author Icreon Tech - NS
     * */
    function blogFeaturedData($blogPostData, $blogFeaturedData, $upload_file_path) {

        if (isset($blogPostData[0]['blog_image']) && !empty($blogPostData[0]['blog_image'])) {
            $blog_image_path = $upload_file_path['assets_url'] . "blog/medium/";
            $blog_image_exist = $upload_file_path['assets_upload_dir'] . "blog/medium/";
        } else {
            $blog_image_path = '';
            $blog_image_exist = '';
        }

        $htmlFD = '';
        $htmlFD .= '<div class="blog-right-section mar-top-29"><h1>FEATURED POSTS</h1>';
        if (isset($blogFeaturedData) && !empty($blogFeaturedData)) {
            foreach ($blogFeaturedData as $key => $val) {
                $htmlFD .= '<div class="right-section-frame">';
                if (isset($val['blog_image']) and trim($val['blog_image']) != "" and file_exists($blog_image_exist . $val['blog_image'])) {
                    $htmlFD .= '<div class="frame-image"><img src="' . $blog_image_path . $val['blog_image'] . '"></div>';
                } else {
                    $htmlFD .= '<div class="frame-image"><img src="'.$this->_config['img_file_path']['path'].'/images/no-img.png"></div>';
                }
                $htmlFD .= '<div class="frame-image-content">';
                $htmlFD .= '<a href="/blog/' . $this->Encrypt($val['blog_id']) . '"><h1>' . stripslashes($val['blog_title']) . '</h1></a>';
                $htmlFD .= '<p>by <strong>' . $val['author_name'] . '</strong></p>';
                $htmlFD .= '<p class="full"><strong>' . $this->OutputDateFormat($val['publish_date'], 'displaymonthformat') . '</strong></p>';
                $htmlFD .= '</div>';
                $htmlFD .= '</div>';
            }
        }
        $htmlFD .= '</div>';

        return $htmlFD;
    }

    /**
     * This function is used to get blogRecentData
     * @param string
     * @return void
     * @author Icreon Tech - NS
     * */
    function blogRecentData($blogPostData, $blogRecentData, $upload_file_path) {

        if (isset($blogPostData[0]['blog_image']) && !empty($blogPostData[0]['blog_image'])) {
            $blog_image_path = $upload_file_path['assets_url'] . "blog/medium/";
            $blog_image_exist = $upload_file_path['assets_upload_dir'] . "blog/medium/";
        } else {
            $blog_image_path = '';
            $blog_image_exist = '';
        }

        $htmlFD = '';
        $htmlFD .= '<div class="blog-right-section mar-top-29"><h1>RECENT POSTS</h1>';
        if (isset($blogRecentData) && !empty($blogRecentData)) {
            foreach ($blogRecentData as $key => $val) {
                $htmlFD .= '<div class="right-section-frame">';
                if (isset($val['blog_image']) and trim($val['blog_image']) != "" and file_exists($blog_image_exist . $val['blog_image'])) {
                    $htmlFD .= '<div class="frame-image"><img src="' . $blog_image_path . $val['blog_image'] . '"></div>';
                } else {
                    $htmlFD .= '<div class="frame-image"><img src="'.$this->_config['img_file_path']['path'].'/images/no-img.png"></div>';
                }
                $htmlFD .= '<div class="frame-image-content">';
                $htmlFD .= '<a href="/blog/' . $this->Encrypt($val['blog_id']) . '"><h1>' . stripslashes($val['blog_title']) . '</h1></a>';
                $htmlFD .= '<p>by <strong>' . $val['author_name'] . '</strong></p>';
                $htmlFD .= '<p class="full"><strong>' . $this->OutputDateFormat($val['publish_date'], 'displaymonthformat') . '</strong></p>';
                $htmlFD .= '</div>';
                $htmlFD .= '</div>';
            }
        }
        $htmlFD .= '</div>';

        return $htmlFD;
    }

    /**
     * This function is used to get famous arrival data category wise
     * @param string
     * @return void
     * @author Icreon Tech - AS
     * */
    public function getFaCategoryAction() {
        $this->getCmsTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        $faParam['faCat'] = $this->decrypt($params['id']);
        $faParam['faPage'] = '8';
        $searchResult = $this->getCmsTable()->getFaContents($faParam);
        foreach ($searchResult as $key => $val) {
            $searchParam['passenger_id'] = $val['fa_passenger_id'];
            $date = $this->getServiceLocator()->get('Passenger\Model\PassengerTable')->getPassengerRecord($searchParam);
            $searchResult[$key]['name'] = (isset($date[0]['PRIN_FIRST_NAME']) && !empty($date[0]['PRIN_FIRST_NAME']) ? $date[0]['PRIN_FIRST_NAME'] : '' )." ".(isset($date[0]['PRIN_LAST_NAME']) && !empty($date[0]['PRIN_LAST_NAME']) ? $date[0]['PRIN_LAST_NAME'] : '' );
            $searchResult[$key]['content_date'] = $date[0]['DATE_ARRIVE'];
        }
        $filePath = $this->_config['file_upload_path']['assets_url'];
        $fileRelativePath = $this->_config['file_upload_path']['assets_upload_dir'];
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'searchResult' => $searchResult,
            'filePath' => $filePath,
            'fileRelativePath' => $fileRelativePath,
        ));
        return $viewModel;
    }

    /**
     * This function is used to get peopling of america data
     * @param string
     * @return void
     * @author Icreon Tech - AS
     * */
    public function getPeoplingDataAction() {
        $this->getCmsTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        $searchParam['status'] = "";
        $searchParam['type'] = "";
        $searchParam['content_id'] = $this->decrypt($params['id']);
        $searchParam['startIndex'] = "";
        $searchParam['recordLimit'] = "";
        $searchParam['sortField'] = "";
        $searchParam['sortOrder'] = "";
        $searchResult = $this->getCmsTable()->getCrmContents($searchParam);
        $patternGallery = "/{gallery}(.*?){\/gallery}/";
        $gallery = array();
        preg_match($patternGallery, $searchResult[0]['description'], $gallery);
        $searchCat = '';
        if (isset($gallery) && !empty($gallery)) {
            $searchResult[0]['description'] = preg_replace($patternGallery, "<div id='gallery'></div>", $searchResult[0]['description']);
            $patternId = "/{id}(.*?){\/id}/";
            $patternThumb = "/{thumb}(.*?){\/thumb}/";
            $patternOrientation = "/{orientation}(.*?){\/orientation}/";
            preg_match($patternId, $gallery[1], $gallery['id']);
            preg_match($patternThumb, $gallery[1], $gallery['thumb']);
            preg_match($patternOrientation, $gallery[1], $gallery['orientation']);
        }
        $filePath = $this->_config['file_upload_path']['assets_url'];
        $fileRelativePath = $this->_config['file_upload_path']['assets_upload_dir'];
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'gallery' => $gallery,
            'searchResult' => $searchResult,
            'filePath' => $filePath,
            'fileRelativePath' => $fileRelativePath,
        ));
        return $viewModel;
    }

    /**
     * This function is used to delete cms gallery
     * @param string
     * @return void
     * @author Icreon Tech - AS
     * */
    public function deleteCrmGalleryAction() {
        $this->getCmsTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        if ($request->isPost()) {
            $dataParam['content_id'] = $this->decrypt($request->getPost('content_id'));
            $dataParam['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
            $dataParam['modified_date'] = DATE_TIME_FORMAT;
            $this->getCmsTable()->deleteCmsGallery($dataParam);
            $this->flashMessenger()->addMessage($this->_config['Cms_messages']['config']['crm-gallery']['CONTENT_DELETED']);
            $messages = array('status' => "success");
            return $response->setContent(\Zend\Json\Json::encode($messages));
        }
    }

    /**
     * This action is used to open pop up
     * @return json
     * @author Icreon Tech - AS
     */
    public function viewMenuAction() {
        $this->checkUserAuthentication();
        $this->getCmsTable();
        $menuForm = new MenuForm();
        $cmsObj = new Cms($this->_adapter);
        $cmsListPagesMessages = array_merge($this->_config['Cms_messages']['config']['CMS_PAGE_LIST_MESSAGE'], $this->_config['Cms_messages']['config']['CMS_ERROR_MSG']);
        $headerMenuLink = $this->getCmsTable()->getMenuList('1');
        $menuArr = $this->buildTreeArray($headerMenuLink);
        $cmsJsonStr = json_encode($menuArr);
        $viewModel = new ViewModel();
        $this->layout('popup');
        $viewModel->setVariables(array(
            'menuForm' => $menuForm,
            'jsLangTranslate' => $cmsListPagesMessages,
            'cmsJsonStr' => $cmsJsonStr
        ));
        return $viewModel;
    }

    /**
     * This action is used to get heritage award data
     * @return json
     * @author Icreon Tech - AS
     */
    public function getHaDataAction() {
        $this->getCmsTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        $haParam['contentId'] = $params['id'];
        $searchResult = $this->getCmsTable()->getHeritageContents($haParam);
        $filePath = $this->_config['file_upload_path']['assets_url'];
        $fileRelativePath = $this->_config['file_upload_path']['assets_upload_dir'];
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $viewModel->setVariables(array(
            'pageContent' => $searchResult,
            'filePath' => $filePath,
            'fileRelativePath' => $fileRelativePath,
        ));
        return $viewModel;
    }

    function insertGalleryDocument($content, $gallery, $type, $key) {
        $galleryMsg = array_merge($this->_config['file_upload_path'], $this->_config['Cms_messages']['config']['crm-gallery']);
        if ($type == '1') {
            $patternGallery = "/{gallery}(.*?){\/gallery}/";
            preg_match_all($patternGallery, $gallery[0][0], $tabbedGallery);
            $galleryContent = "<ul class='gallery_tab'>";
            for ($i = 0; $i < count($tabbedGallery[0]); $i++) {
                $patternId = "/{id}(.*?){\/id}/";
                preg_match($patternId, $tabbedGallery[1][$i], $search['gallery_id']);
                $param['gallery_id'] = $search['gallery_id'][1];
                $searchResult = $this->getCmsTable()->getCrmGalleryInfo($param);
                if (isset($searchResult[0]['file_type']) && !empty($searchResult[0]['file_type'])) {
                    $galleryContent = $galleryContent . "<li onclick='displaySlider(" . $searchResult[0]['gallery_id'] . "," . $key . ")'>" . $searchResult[0]['gallery_name'] . "</li>";
                }
            }
            $galleryContent = $galleryContent . "</ul>";
            for ($i = 0, $j = 0; $i < count($tabbedGallery[0]); $i++) {
                $patternId = "/{id}(.*?){\/id}/";
                $patternThumb = "/{thumb}(.*?){\/thumb}/";
                preg_match($patternId, $tabbedGallery[1][$i], $search['gallery_id']);
                preg_match($patternThumb, $tabbedGallery[1][$i], $search['thumb']);
                $param['gallery_id'] = $search['gallery_id'][1];
                $searchResult = $this->getCmsTable()->getCrmGalleryInfo($param);
                if (isset($searchResult[0]['file_type']) && !empty($searchResult[0]['file_type'])) {
                    $imgPath = $galleryMsg['assets_url'] . 'cmsgallery/' . $this->FolderStructByUserId($search['gallery_id'][1]) . 'image';
                    $physicalFileName = explode(',', $searchResult[0]['phy_file_name']);
                    $fileType = explode(',', $searchResult[0]['file_type']);
                    $display = '';
                    if ($j > 0) {
                        $display = "style = 'display:none'";
                        $j++;
                    }
                    $galleryContent = $galleryContent . "
                <div id='slide_" . $key . "_" . $search['gallery_id'][1] . "' class='slider-section width-746' " . $display . ">
                <a href='javascript:void(0)' class='prev'></a>
                <a href='javascript:void(0)' class='next'></a>
                <div class='slider'>";
                    $count = count($physicalFileName) < $search['thumb'][1] ? count($physicalFileName) : $search['thumb'][1];
                    for ($j = 0; $j < $count; $j++) {
                        if ($fileType[$j] == 2) {
                            $galleryContent = $galleryContent . "<div class='sliderdiv'>                                
                                <div id='video_" . $j . "'></div>
                                <script type='text/javascript'>
                                    jwplayer('video_" . $j . "').setup({
                                        file: '" . $imgPath . '/' . $physicalFileName[$j] . "',
                                        height: 430,
                                        image: '".$this->_config['img_file_path']['path']."/img/videoImage1.jpg',
                                        width: 740
                                    });
                                </script></div>";
                        } else {
                            $galleryContent = $galleryContent . "<div class='sliderdiv'><img src='" . $imgPath . "/" . $physicalFileName[$j] . "'></div>";
                        }
                    }
                    $galleryContent = $galleryContent . " </div></div>";
                }
            }
            $content = str_replace($gallery[0][0], $galleryContent, $content);
        } else if ($type == '2') {
            for ($i = 0; $i < count($gallery[0]); $i++) {
                $patternId = "/{id}(.*?){\/id}/";
                $patternThumb = "/{thumb}(.*?){\/thumb}/";
                preg_match($patternId, $gallery[1][$i], $search['gallery_id']);
                preg_match($patternThumb, $gallery[1][$i], $search['thumb']);
                $param['gallery_id'] = $search['gallery_id'][1];
                $galleryContent = '';
                $searchResult = $this->getCmsTable()->getCrmGalleryInfo($param);
                if (isset($searchResult[0]['file_type']) && !empty($searchResult[0]['file_type'])) {
                    $imgPath = $galleryMsg['assets_url'] . 'cmsgallery/' . $this->FolderStructByUserId($search['gallery_id'][1]) . 'image';
                    $physicalFileName = explode(',', $searchResult[0]['phy_file_name']);
                    $fileType = explode(',', $searchResult[0]['file_type']);
                    $galleryContent = $galleryContent . "
                    <div class='slider-section width-746'>
                    <a href='javascript:void(0)' class='prev'></a>
                    <a href='javascript:void(0)' class='next'></a>
                    <div class='slider' id='slider_" . $search['gallery_id'][1] . "'>";
                    $count = count($physicalFileName) < $search['thumb'][1] ? count($physicalFileName) : $search['thumb'][1];
                    for ($j = 0; $j < $count; $j++) {
                        if ($fileType[$j] == 2) {
                            $galleryContent = $galleryContent . "<div class='sliderdiv'>                                
                                <div id='video_" . $j . "'></div>
                                <script type='text/javascript'>
                                    jwplayer('video_" . $j . "').setup({
                                        file: '" . $imgPath . '/' . $physicalFileName[$j] . "',
                                        height: 360,
                                        image: '".$this->_config['img_file_path']['path']."/img/videoImage1.jpg',
                                        width: 700
                                    });
                                </script></div>";
                        } else {
                            $galleryContent = $galleryContent . "<div class='sliderdiv'><img src='" . $imgPath . "/" . $physicalFileName[$j] . "'></div>";
                        }
                    }
                    $galleryContent = $galleryContent . " </div></div>";
                }
                $content = str_replace($gallery[0][$i], $galleryContent, $content);
            }
        }
        return $content;
    }

    /**
     * This action is used to get timeline data
     * @return json
     * @author Icreon Tech - AS
     */
    public function getTimelineDataAction() {
        $this->getCmsTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        $searchParam['pageContent'] = '17';
        $searchParam['year'] = $params['year'];
        $searchResult = $this->getCmsTable()->getTimelineContents($searchParam);
        $filePath = $this->_config['file_upload_path']['assets_url'];
        $fileRelativePath = $this->_config['file_upload_path']['assets_upload_dir'];
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $patternGallery = "/{gallery}(.*?){\/gallery}/";
        $patternTabbed = "/{tabbed}(.*?){\/tabbed}/";
        preg_match_all($patternTabbed, $searchResult[0]['description'], $tabbedGallery);
        if (isset($tabbedGallery) && !empty($tabbedGallery) && (count($tabbedGallery[0]) > 0)) {
            foreach ($tabbedGallery[0] as $key => $val) {
                $tabGallery[0][0] = $val;
                $searchResult[0]['description'] = $this->insertGalleryDocument($searchResult[0]['description'], $tabGallery, '1', $key);
            }
        }
        preg_match_all($patternGallery, $searchResult[0]['description'], $gallery);
        if (isset($gallery) && !empty($gallery)) {
            $searchResult[0]['description'] = $this->insertGalleryDocument($searchResult[0]['description'], $gallery, '2', '0');
        }
        $viewModel->setVariables(array(
            'pageContent' => $searchResult,
            'filePath' => $filePath,
            'fileRelativePath' => $fileRelativePath,
        ));
        return $viewModel;
    }

    /**
     * This action is used to get statue of liberty timeline data
     * @return json
     * @author Icreon Tech - AS
     */
    public function getSolTimelineDataAction() {
        $this->getCmsTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        $searchParam['pageContent'] = '18';
        $searchParam['year'] = $params['year'];
        $searchResult = $this->getCmsTable()->getTimelineContents($searchParam);
        $filePath = $this->_config['file_upload_path']['assets_url'];
        $fileRelativePath = $this->_config['file_upload_path']['assets_upload_dir'];
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        if (isset($searchResult) && !empty($searchResult)) {
            $patternGallery = "/{gallery}(.*?){\/gallery}/";
            $patternTabbed = "/{tabbed}(.*?){\/tabbed}/";
            preg_match_all($patternTabbed, $searchResult[0]['description'], $tabbedGallery);
            if (isset($tabbedGallery) && !empty($tabbedGallery) && (count($tabbedGallery[0]) > 0)) {
                foreach ($tabbedGallery[0] as $key => $val) {
                    $tabGallery[0][0] = $val;
                    $searchResult[0]['description'] = $this->insertGalleryDocument($searchResult[0]['description'], $tabGallery, '1', $key);
                }
            }
            preg_match_all($patternGallery, $searchResult[0]['description'], $gallery);
            if (isset($gallery) && !empty($gallery)) {
                $searchResult[0]['description'] = $this->insertGalleryDocument($searchResult[0]['description'], $gallery, '2', '0');
            }
        }
        $viewModel->setVariables(array(
            'pageContent' => $searchResult,
            'filePath' => $filePath,
            'fileRelativePath' => $fileRelativePath,
        ));
        return $viewModel;
    }

    /**
     * This action is used to get sponser data
     * @return json
     * @author Icreon Tech - AS
     */
    public function getSponserDataAction() {
        $this->getCmsTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
        $params = $this->params()->fromRoute();
        $searchParam['status'] = "";
        $searchParam['type'] = "";
        $searchParam['content_id'] = $params['id'];
        $searchParam['startIndex'] = "";
        $searchParam['recordLimit'] = "";
        $searchParam['sortField'] = "";
        $searchParam['sortOrder'] = "";
        $searchResult = $this->getCmsTable()->getCrmContent($searchParam);
        $filePath = $this->_config['file_upload_path']['assets_url'];
        $fileRelativePath = $this->_config['file_upload_path']['assets_upload_dir'];
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        $patternGallery = "/{gallery}(.*?){\/gallery}/";
        $patternTabbed = "/{tabbed}(.*?){\/tabbed}/";
        preg_match_all($patternTabbed, $searchResult[0]['description'], $tabbedGallery);
        if (isset($tabbedGallery) && !empty($tabbedGallery) && (count($tabbedGallery[0]) > 0)) {
            foreach ($tabbedGallery[0] as $key => $val) {
                $tabGallery[0][0] = $val;
                $searchResult[0]['description'] = $this->insertGalleryDocument($searchResult[0]['description'], $tabGallery, '1', $key);
            }
        }
        preg_match_all($patternGallery, $searchResult[0]['description'], $gallery);
        if (isset($gallery) && !empty($gallery)) {
            $searchResult[0]['description'] = $this->insertGalleryDocument($searchResult[0]['description'], $gallery, '2', '0');
        }
        $viewModel->setVariables(array(
            'pageContent' => $searchResult,
            'filePath' => $filePath,
            'fileRelativePath' => $fileRelativePath,
        ));
        return $viewModel;
    }

}