<?php

/**
 * This controller is used for home banner pages
 * @category   Zend
 * @package    Cms_AdController
 * @version    2.2
 * @author     Icreon Tech - NS
 */

namespace Cms\Controller;

use Base\Model\UploadHandler;
use Base\Controller\BaseController;
use Zend\Authentication\Adapter\DbTable as AuthAdapter;
use Zend\View\Model\ViewModel;
use Cms\Form\HomeSliderForm;
use Cms\Form\SearchHomeSliderForm;
use Cms\Model\HomeSlider;
use Zend\Http\Request;
use Zend\Authentication\AuthenticationService;
use stdClass;
use Zend\Session\Container;

class HomeSliderController extends BaseController {

    protected $_homesliderTable = null;
    protected $_translator = null;
    protected $_config = null;
    protected $_adapter = null;
    protected $_flashMessage = null;
    protected $_menuSelectArr = array(0 => '<ROOT>');

    function __construct() {
        $this->_flashMessage = $this->flashMessenger();
        parent::__construct();
    }

    /**
     * This function is used to get table and config file object
     * @return     array
     * @param void
     * @author Icreon Tech - NS
     */
    public function getHomeSliderTable() {
        if (!$this->_homesliderTable) {
            $sm = $this->getServiceLocator();
            $this->_homesliderTable = $sm->get('Cms\Model\HomeSliderTable');
            $this->_translator = $sm->get('translator');
            $this->_config = $sm->get('Config');
            $this->_adapter = $sm->get('dbAdapter');
            $this->_auth = new AuthenticationService();
        }
        return $this->_homesliderTable;
    }

    

    /**
     * This action is used to listing of home slider 
     * @return $viewModel
     * @param void
     * @author Icreon Tech - NS
     */
    public function getCrmHomeSliderAction() {
        $this->checkUserAuthentication();
        $this->getHomeSliderTable();
        $hsListPagesMessages = array_merge($this->_config['Cms_messages']['config']['HOMESLIDER_PAGE_LIST_MESSAGE'], $this->_config['Cms_messages']['config']['HOMESLIDER_ERROR_MSG']);
        $this->layout('crm');

        $messages = array();
        if ($this->_flashMessage->hasMessages()) {
            $messages = $this->_flashMessage->getMessages();
        }

        $formHS = new SearchHomeSliderForm();
        
        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'jsLangTranslate' => $hsListPagesMessages,
            'messages' => $messages,
            'formHS' => $formHS
        ));
        return $viewModel;
    }

    /**
     * This action is used to show home slider grid
     * @return json format data
     * @param void
     * @author Icreon Tech - NS
     */
    public function listCrmHomeSliderAction() {
        $this->checkUserAuthentication();
        $this->getHomeSliderTable();
        $request = $this->getRequest();
        $response = $this->getResponse();
 
        $searchParam['image_title'] = "";
        $searchParam['is_active'] = "";
        parse_str($request->getPost('searchString'), $searchParam);
        $limit = $request->getPost('rows');
        $page = $request->getPost('page');
        $start = $limit * $page - $limit;

        $searchParam['startIndex'] = $start;
        $searchParam['recordLimit'] = $limit;
        $searchParam['sortField'] = $request->getPost('sidx');
        $searchParam['sortOrder'] = $request->getPost('sord');
        $searchParam['isActive'] = "";
        $params = $this->params()->fromRoute();

        $searchResult = $this->getHomeSliderTable()->getCrmHomeSlider($searchParam);
        $countResult = $this->getServiceLocator()->get('Common\Model\CommonTable')->getTotalCount();
        $jsonResult = array();
        $arrCell = array();
        $subArrCell = array();
        $jsonResult['page'] = $page;
        $jsonResult['records'] = $countResult[0]->RecordCount;
        $jsonResult['total'] = @ceil($countResult[0]->RecordCount / $limit);
        if (isset($searchResult) and count($searchResult) > 0) {
            foreach ($searchResult as $val) {
                $arrCell['id'] = $val['slider_image_id'];
                if ($val['is_active'] == 1) {
                    $isActive = "Active";
                } else {
                    $isActive = "Inactive";
                }
               
                $edit = "<a class='edit-icon' href='/edit-crm-home-slider/" . $this->encrypt($val['slider_image_id']) . "'><div class='tooltip'>Edit<span></span></div></a>";
                $delete = "<a class='delete-icon deletebanner' href='#delete_banner' onclick=deleteBanner('" . $this->encrypt($val['slider_image_id']) . "')><div class='tooltip'>Delete<span></span></div></a>";
 

                $arrCell['cell'] = array($val['image_title'], $val['image_link'], $val['image_order'], $isActive, $edit . $delete);
                $subArrCell[] = $arrCell;
                $jsonResult['rows'] = $subArrCell;
            }
        }
        return $response->setContent(\Zend\Json\Json::encode($jsonResult));
    }

    /**
     * This action is used to create home slider
     * @return $viewModel
     * @param void
     * @author Icreon Tech - AS
     */
    public function addCrmHomeSliderAction() {
        $this->checkUserAuthentication();
        $this->getHomeSliderTable();
        $adsListPagesMessages = array_merge($this->_config['Cms_messages']['config']['HOMESLIDER_PAGE_LIST_MESSAGE'], $this->_config['Cms_messages']['config']['HOMESLIDER_ERROR_MSG']);

        $this->layout('crm');
        $hsForm = new HomeSliderForm();

        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'hsForm' => $hsForm,
            'jsLangTranslate' => $adsListPagesMessages,
            'minWidth' => $this->_config['file_upload_path']['user_home_slider_main_width'],
            'minHeight' => $this->_config['file_upload_path']['user_home_slider_main_height']            
        ));
        return $viewModel;
    }

    /**
     * This action for saving ad
     * @return json
     * @param void
     * @author Icreon Tech - NS
     */
    public function addCrmHomeSliderProcessAction() {

        try {
            $this->checkUserAuthentication();
            $this->getHomeSliderTable();
            $request = $this->getRequest();
            $response = $this->getResponse();
            $hsObj = new HomeSlider($this->_adapter);

            $adsListPagesMessages = array_merge($this->_config['Cms_messages']['config']['HOMESLIDER_PAGE_LIST_MESSAGE'], $this->_config['Cms_messages']['config']['HOMESLIDER_ERROR_MSG']);

            $params = array();
            $params['image_title'] = $request->getPost('image_title');
            $params['image_name'] = $request->getPost('image_name');
            $params['image_description'] = $request->getPost('image_description');
            $params['image_link'] = $request->getPost('image_link');
            $params['link_open'] = $request->getPost('link_open');
            $params['image_order'] = $request->getPost('image_order');
            $params['is_active'] = $request->getPost('is_active');
            $params['is_deleted'] = 0;
            $params['added_by'] = $this->_auth->getIdentity()->crm_user_id;
            $params['added_date'] = DATE_TIME_FORMAT;
            $params['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
            $params['modified_date'] = DATE_TIME_FORMAT;
            $hsForm = new HomeSliderForm();
            $hsForm->setInputFilter($hsObj->getInputFilterHomeSlider($params));
            $hsForm->setData($request->getPost());

            if (!$hsForm->isValid()) {
                $errors = $hsForm->getMessages();

                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'Save') {
                        foreach ($row as $rower) {
                            $msg[$key] = isset($adsListPagesMessages[$rower]) ? $adsListPagesMessages[$rower] : '';
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $postData = $hsForm->getData();

                $postData['added_by'] = $this->_auth->getIdentity()->crm_user_id;
                $postData['added_date'] = DATE_TIME_FORMAT;

                $postData['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                $postData['modified_date'] = DATE_TIME_FORMAT;

                $filterAdsData = $hsObj->exchangeArray($postData);
                $returnData = $this->getHomeSliderTable()->insertCrmHomeSlider($filterAdsData);

                if ($returnData == true) {
                    $this->moveFileFromTempToBanner($filterAdsData['image_name']);
                    $this->flashMessenger()->addMessage($adsListPagesMessages['L_HOMESLIDER_ADDED']);
                    $messages = array('status' => "success");
                } else {
                    $this->flashMessenger()->addMessage($adsListPagesMessages['L_HOMESLIDER_ERROR_ADDED']);
                    $messages = array('status' => "error");
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This action is used to edit ads
     * @return $viewModel
     * @param void
     * @author Icreon Tech - NS
     */
    public function editCrmHomeSliderAction() {
        $this->checkUserAuthentication();
        $this->getHomeSliderTable();

        $adsListPagesMessages = array_merge($this->_config['Cms_messages']['config']['HOMESLIDER_PAGE_LIST_MESSAGE'], $this->_config['Cms_messages']['config']['HOMESLIDER_ERROR_MSG']);

        $param = $this->params()->fromRoute();
        $slider_image_id = $this->decrypt($param['slider_image_id']);
        $param_info = array();
        $param_info['slider_image_id'] = $slider_image_id;
        $getCRMHsInfo = $this->getHomeSliderTable()->getCrmHomeSliderInfo($param_info);
        $this->layout('crm');
        $hsForm = new HomeSliderForm();

      
        $p_image_name = '';

        $hsForm->get('slider_image_id')->setAttribute('value', $getCRMHsInfo['slider_image_id']);
        
        if (isset($getCRMHsInfo['image_title']) and trim($getCRMHsInfo['image_title']) != "") {
            $hsForm->get('image_title')->setAttribute('value', $getCRMHsInfo['image_title']);
        }
        
        if (isset($getCRMHsInfo['image_name']) and trim($getCRMHsInfo['image_name']) != "") {
            $p_image_name = $getCRMHsInfo['image_name'];
            $hsForm->get('image_name')->setAttribute('value', $getCRMHsInfo['image_name']);
            $hsForm->get('old_image_name')->setAttribute('value', $getCRMHsInfo['image_name']);
        }
        
        if (isset($getCRMHsInfo['image_description']) and trim($getCRMHsInfo['image_description']) != "") {
            $hsForm->get('image_description')->setAttribute('value', $getCRMHsInfo['image_description']);
        }
        
        if (isset($getCRMHsInfo['image_link']) and trim($getCRMHsInfo['image_link']) != "") {
            $hsForm->get('image_link')->setAttribute('value', $getCRMHsInfo['image_link']);
        }
        
        if (isset($getCRMHsInfo['link_open']) and trim($getCRMHsInfo['link_open']) != "") {
            $hsForm->get('link_open')->setAttribute('value', $getCRMHsInfo['link_open']);
        }
        
        if (isset($getCRMHsInfo['image_order']) and trim($getCRMHsInfo['image_order']) != "") {
            $hsForm->get('image_order')->setAttribute('value', $getCRMHsInfo['image_order']);
        }
        
        if (isset($getCRMHsInfo['is_active']) and trim($getCRMHsInfo['is_active']) != "") {
            $hsForm->get('is_active')->setAttribute('value', $getCRMHsInfo['is_active']);
        }

        $viewModel = new ViewModel();
        $viewModel->setVariables(array(
            'hsForm' => $hsForm,
            'jsLangTranslate' => $adsListPagesMessages,
            'p_image_name' => $p_image_name,
            'minWidth' => $this->_config['file_upload_path']['user_home_slider_main_width'],
            'minHeight' => $this->_config['file_upload_path']['user_home_slider_main_height'] 
        ));
        return $viewModel;
    }

    /**
     * This action is used to modifying home slider
     * @return $viewModel
     * @param void
     * @author Icreon Tech - NS
     */
    public function editCrmHomeSliderProcessAction() {

        try {
            $this->checkUserAuthentication();
            $this->getHomeSliderTable();
            $request = $this->getRequest();
            $response = $this->getResponse();
            $hsObj = new HomeSlider($this->_adapter);

            $adsListPagesMessages =  array_merge($this->_config['Cms_messages']['config']['HOMESLIDER_PAGE_LIST_MESSAGE'], $this->_config['Cms_messages']['config']['HOMESLIDER_ERROR_MSG']);

            $params = array();
            $params['slider_image_id'] = $request->getPost('slider_image_id');
            $params['image_title'] = $request->getPost('image_title');
            $params['image_name'] = $request->getPost('image_name');
            $params['image_description'] = $request->getPost('image_description');
            $params['image_link'] = $request->getPost('image_link');
            $params['link_open'] = $request->getPost('link_open');
            $params['image_order'] = $request->getPost('image_order');
            $params['is_active'] = $request->getPost('is_active');
            $params['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
            $params['modified_date'] = DATE_TIME_FORMAT;

            $hsForm = new HomeSliderForm();
            $hsForm->setInputFilter($hsObj->getInputFilterHomeSlider($params));
            $hsForm->setData($request->getPost());

            if (!$hsForm->isValid()) {
                $errors = $hsForm->getMessages();

                $msg = array();
                foreach ($errors as $key => $row) {
                    if (!empty($row) && $key != 'Update') {
                        foreach ($row as $rower) {
                            $msg[$key] = $adsListPagesMessages[$rower];
                        }
                    }
                }
                $messages = array('status' => "error", 'message' => $msg);
            }
            if (!empty($messages)) {
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            } else {
                $postData = $hsForm->getData();
                $postData['modified_by'] = $this->_auth->getIdentity()->crm_user_id;
                $postData['modified_date'] = DATE_TIME_FORMAT;

                $filterAdsData = $hsObj->exchangeArray($postData);    
                $returnData = $this->getHomeSliderTable()->updateHomeSlider($filterAdsData);

                if ($returnData == true) {
                    $old_image_name = $request->getPost('old_image_name');
                    if(isset($old_image_name) and trim($old_image_name) != "" and $old_image_name != $filterAdsData['image_name']) {
                       
                        $banner_dir = $this->_config['file_upload_path']['assets_upload_dir'] . "homeslider/";
                        $banner_filename = $banner_dir . $old_image_name;

                        $banner_dir_thumbnail = $this->_config['file_upload_path']['assets_upload_dir'] . "homeslider/thumbnail/";
                        $banner_filename_thumbnail = $banner_dir_thumbnail . $old_image_name;

                        if(file_exists($banner_filename)) {  unlink($banner_filename); }
                        if(file_exists($banner_filename_thumbnail)) {  unlink($banner_filename_thumbnail); }                        
                    }
                    
                    $this->moveFileFromTempToBanner($filterAdsData['image_name']);

                    $this->flashMessenger()->addMessage($adsListPagesMessages['L_HOMESLIDER_UPDATED']);
                    $messages = array('status' => "success");
                } else {
                    $this->flashMessenger()->addMessage($adsListPagesMessages['L_HOMESLIDER_ERROR_UPDATED']);
                    $messages = array('status' => "error");
                }
                $response->setContent(\Zend\Json\Json::encode($messages));
                return $response;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This action is used to move file from temp to  folder
     * @param filename 
     * @return 
     * @author Icreon Tech - NS
     */
    public function moveFileFromTempToBanner($filename) {
        $this->getHomeSliderTable();
        if (isset($filename) and trim($filename) != "") {
            $temp_dir = $this->_config['file_upload_path']['temp_upload_dir'];
            $temp_file = $temp_dir . $filename;
            
            $banner_dir = $this->_config['file_upload_path']['assets_upload_dir'] . "homeslider/";
            $banner_filename = $banner_dir . $filename;

            $banner_dir_thumbnail = $this->_config['file_upload_path']['assets_upload_dir'] . "homeslider/thumbnail/";
            $banner_filename_thumbnail = $banner_dir_thumbnail . $filename;

            if (isset($temp_file) and trim($temp_file) != "" and file_exists($temp_file)) {
               //copy($temp_file, $banner_filename);
               $this->resizeImage($banner_filename,$temp_file,$this->_config['file_upload_path']['user_home_slider_main_width'],$this->_config['file_upload_path']['user_home_slider_main_height']);
               $this->resizeImage($banner_filename_thumbnail,$temp_file,$this->_config['file_upload_path']['user_home_slider_thumbnail_width'],$this->_config['file_upload_path']['user_home_slider_thumbnail_height']);
            }
        }
    }

    
    /**
     * This function is used to resize Image
     * @return     array
     * @author Icreon Tech - NS
     */
    public function resizeImage($new_image_name, $image, $resizeWidth, $resizeHeight) {
        try {
            list($imagewidth, $imageheight, $imageType) = getimagesize($image);
            $imageType = image_type_to_mime_type($imageType);


            $SMALL_IMAGE_MAX_WIDTH = $resizeWidth;
            $SMALL_IMAGE_MAX_HEIGHT = $resizeHeight;

            $source_aspect_ratio = $imagewidth / $imageheight;
            $thumbnail_aspect_ratio = $SMALL_IMAGE_MAX_WIDTH / $SMALL_IMAGE_MAX_HEIGHT;
            if ($imagewidth <= $SMALL_IMAGE_MAX_WIDTH && $imageheight <= $SMALL_IMAGE_MAX_HEIGHT) {
                $small_image_width = $imagewidth;
                $small_image_height = $imageheight;
            } elseif ($thumbnail_aspect_ratio > $source_aspect_ratio) {
                $small_image_width = (int) ($SMALL_IMAGE_MAX_HEIGHT * $source_aspect_ratio);
                $small_image_height = $SMALL_IMAGE_MAX_HEIGHT;
            } else {
                $small_image_width = $SMALL_IMAGE_MAX_WIDTH;
                $small_image_height = (int) ($SMALL_IMAGE_MAX_WIDTH / $source_aspect_ratio);
            }

            $newImageWidth = $small_image_width;
            $newImageHeight = $small_image_height;

            $newImage = imagecreatetruecolor($newImageWidth, $newImageHeight);

            switch ($imageType) {
                case "image/gif":
                    $source = imagecreatefromgif($image);
                    break;
                case "image/pjpeg":
                case "image/jpeg":
                case "image/jpg":
                    $source = imagecreatefromjpeg($image);
                    break;
                case "image/png":
                case "image/x-png":
                    $source = imagecreatefrompng($image);
                    break;
            }

            imagecopyresampled($newImage, $source, 0, 0, 0, 0, $newImageWidth, $newImageHeight, $imagewidth, $imageheight);
            switch ($imageType) {
                case "image/gif":
                    imagegif($newImage, $new_image_name);
                    break;
                case "image/pjpeg":
                case "image/jpeg":
                case "image/jpg":
                    imagejpeg($newImage, $new_image_name, 90);
                    break;
                case "image/png":
                case "image/x-png":
                    imagepng($newImage, $new_image_name);
                    break;
            }

            chmod($new_image_name, 0777);
            return true;
        } catch (Exception $e) {
            return true;
        }
    }
    
    
    /**
     * This action is used for uploading image
     * @return json format data
     * @param void
     * @author Icreon Tech - NS
     */
    public function uploadCrmHomeSliderImageAction() {

        $fileExt = $this->GetFileExt($_FILES['image_name_file']['name']);
        $filename = $this->GenerateMediaFileName($fileExt[1]);    //Generate file name
        $_FILES['image_name_file']['name'] = $filename;                 // assign name to file variable
        $this->getHomeSliderTable();
        $upload_file_path = $this->_config['file_upload_path'];   // get file upload configuration
        
        $aspectRatio = $this->aspectRatio($this->_config['file_upload_path']['user_home_slider_main_width'],$this->_config['file_upload_path']['user_home_slider_main_height']);
        
        $error_messages = array(
            'post_max_size' => 'The uploaded file exceeds the post_max_size directive in php.ini', // Set error messages
            'max_file_size' => 'File is too big. Maximum file size should be less than '.$upload_file_path['max_allowed_file_size'].' byte',
            'min_file_size' => 'File is too small. Minimum file size should be more than '.$upload_file_path['min_allowed_file_size'].' byte',
            'accept_file_types' => 'Filetype not allowed. Allowed file types are : JPG,PNG,GIF',
            'min_width' => "Minimum of image width should be ".$this->_config['file_upload_path']['user_home_slider_main_width']."px",
            'min_height' => "Minimum of image height should be ".$this->_config['file_upload_path']['user_home_slider_main_height']."px",
            'aspect_ratio' => "Image requires a aspect ratio of ".($this->_config['file_upload_path']['user_home_slider_main_width']/$aspectRatio) . ":" . ($this->_config['file_upload_path']['user_home_slider_main_height']/$aspectRatio)
        );

        $options = array(
            'upload_dir' => $upload_file_path['temp_upload_dir'],
            'param_name' => 'image_name_file', //file input name                      // Set configuration
            'inline_file_types' => $upload_file_path['file_types_allowed'],
            'accept_file_types' => $upload_file_path['file_types_allowed'],
            'max_file_size' => $upload_file_path['max_allowed_file_size'],
            'min_file_size' => $upload_file_path['min_allowed_file_size'],
            'min_width' => $this->_config['file_upload_path']['user_home_slider_main_width'],
            'min_height' => $this->_config['file_upload_path']['user_home_slider_main_height'],
            'aspect_ratio' => "1"
        );
 
        $upload_handler = new UploadHandler($options, true, $error_messages);
        $file_response = $upload_handler->jsonResponceData;
        if (isset($file_response['image_name_file'][0]->error)) {
            $messages = array('status' => "error", 'message' => $file_response['image_name_file'][0]->error);
        } else {
            $messages = array('status' => "success", 'message' => 'Image uploaded', 'filename' => $file_response['image_name_file'][0]->name);
        }
        $response = $this->getResponse();
        $response->setContent(\Zend\Json\Json::encode($messages));
        return $response;
    }
    
    
      /**
     * This action is used for aspect ratio
     * @return json format data
     * @param void
     * @author Icreon Tech - NS
     */
    private function aspectRatio($a, $b) {
        if ($a == 0 || $b == 0) return abs( max(abs($a), abs($b)) );
        $r = $a % $b;
        return ($r != 0) ? $this->aspectRatio($b, $r) : abs($b);
    }
    

    /**
     * This action is used for deleting home slider
     * @return json format data
     * @param void
     * @author Icreon Tech - NS
     */
    public function deleteCrmHomeSliderAction() {

        try {
            $this->checkUserAuthentication();
            $this->getHomeSliderTable();
            $response = $this->getResponse();
            $adsListPagesMessages = array_merge($this->_config['Cms_messages']['config']['HOMESLIDER_PAGE_LIST_MESSAGE'], $this->_config['Cms_messages']['config']['HOMESLIDER_ERROR_MSG']);

            $request = $this->getRequest();
            $slider_image_id = $request->getPost('slider_image_id');
            $params = array();
            $params['slider_image_id'] = $this->decrypt($slider_image_id);
            $returnData = $this->getHomeSliderTable()->deleteHomeSlider($params);

            if ($returnData == true) {
                $this->flashMessenger()->addMessage($adsListPagesMessages['L_HOMESLIDER_DELETED']);
                $messages = array('status' => "success");
            } else {
                $this->flashMessenger()->addMessage($adsListPagesMessages['L_HOMESLIDER_ERROR_DELETED']);
                $messages = array('status' => "error");
            }
            $response->setContent(\Zend\Json\Json::encode($messages));
            return $response;
        } catch (Exception $e) {
            return false;
        }
    }

}