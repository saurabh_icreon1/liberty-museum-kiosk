<?php

/**
 * This model is used for ad's.
 * @category   Zend
 * @package    Ads
 * @author     Icreon Tech - NS
 */

namespace Cms\Model;

use Zend\Db\TableGateway\TableGateway;

class AdTable {
    
    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
    }
    
    /**
    * This function is used to listing of Ads content type pages
    * @return $resultset 
    * @param $params
    * @author Icreon Tech - NS
    */
    public function getCrmAds($params = array()) {
        try {
            
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ads_getCrmAds(?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['startIndex']);
            $stmt->getResource()->bindParam(2, $params['recordLimit']);
            $stmt->getResource()->bindParam(3, $params['sortField']);
            $stmt->getResource()->bindParam(4, $params['sortOrder']);
            $stmt->getResource()->bindParam(5, $params['ad_title']);
            $stmt->getResource()->bindParam(6, $params['page_url']);
            $stmt->getResource()->bindParam(7, $params['ad_zone_id']);
            $stmt->getResource()->bindParam(8, $params['ad_status']);
            $stmt->getResource()->bindParam(9, $params['ad_type_id']);
            $stmt->getResource()->bindParam(10, $params['description']);
            $stmt->getResource()->bindParam(11, $params['activate_date']);
            $stmt->getResource()->bindParam(12, $params['end_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    
     /**
    * This function is used to listing of Ads content type pages
    * @return $resultset 
    * @param $params
    * @author Icreon Tech - NS
    */
    public function getCrmAdInfo($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ads_getCrmAdInfo(?)');
            $stmt->getResource()->bindParam(1, $params['ad_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet[0];
        } catch (Exception $e) {
            return false;
        }
    }
    
   
     /**
    * This function is used to  Crm Front Ad Statistics Clicks
    * @return $resultset 
    * @param $params
    * @author Icreon Tech - NS
    */
    public function getCrmFrontAdStatisticsClicks($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ads_getStatisticsCrmAdsClicks(?,?,?)');
            $stmt->getResource()->bindParam(1, $params['ad_id']);
            $stmt->getResource()->bindParam(2, $params['start_date_time']);
            $stmt->getResource()->bindParam(3, $params['end_date_time']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
    * This function is used to  Crm Front Ad Statistics Impressions
    * @return $resultset 
    * @param $params
    * @author Icreon Tech - NS
    */
    public function getCrmFrontAdStatisticsImpressions($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ads_getStatisticsCrmAdsImpressions(?,?,?)');
            $stmt->getResource()->bindParam(1, $params['ad_id']);
            $stmt->getResource()->bindParam(2, $params['start_date_time']);
            $stmt->getResource()->bindParam(3, $params['end_date_time']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    
    
    /**
    * This function is used to  Crm Front Insert Ad Statistics Clicks
    * @return $resultset 
    * @param $params
    * @author Icreon Tech - NS
    */
    public function getCrmFrontAdStatisticsInsertClicks($params = array()) {
  
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ads_insertCrmAdsClicks(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['ad_id']);
            $stmt->getResource()->bindParam(2, $params['user_id']);
            $stmt->getResource()->bindParam(3, $params['user_ip']);
            $stmt->getResource()->bindParam(4, $params['click_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
    * This function is used to  Crm Front Insert Ad Statistics Impressions
    * @return $resultset 
    * @param $params
    * @author Icreon Tech - NS
    */
    public function getCrmAdStatisticsInsertImpressions($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ads_insertCrmAdsImpressions(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['ad_id']);
            $stmt->getResource()->bindParam(2, $params['user_id']);
            $stmt->getResource()->bindParam(3, $params['user_ip']);
            $stmt->getResource()->bindParam(4, $params['impressions_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
    * This function is used to  Crm Front Ad information
    * @return $resultset 
    * @param $params
    * @author Icreon Tech - NS
    */
    public function getCrmFrontAd($params = array()) {
        try {
            
            $ad_id = isset($params['ad_id'])?$params['ad_id']:'';  
            $not_adids = isset($params['not_ad_ids'])?$params['not_ad_ids']:'';  
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ads_getCrmFrontAd(?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['page_url']);
            $stmt->getResource()->bindParam(2, $params['ad_zone_id']);
            $stmt->getResource()->bindParam(3, $params['curr_date']);
            $stmt->getResource()->bindParam(4, $params['start_Index']);
            $stmt->getResource()->bindParam(5, $params['record_limit']);
            $stmt->getResource()->bindParam(6, $params['rand_flag']);
            $stmt->getResource()->bindParam(7, $not_adids);
            $stmt->getResource()->bindParam(8, $ad_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    
    
    /**
    * This function is used to listing of Ads content type pages
    * @return $resultset 
    * @param $params
    * @author Icreon Tech - NS
    */
    public function getCrmAdTypes() {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ads_get_ad_types()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
    * This function is used to listing of Ads content type pages
    * @return $resultset 
    * @param $params
    * @author Icreon Tech - NS
    */
    public function getCrmAdZones() {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ads_get_ad_zones()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    
    
    
       /**
     * This function is used to insert Crm Ads
     * @param $params
     * @return boolean 
     * @author Icreon Tech - NS
     */
    public function insertCrmAds($param = array()) {
       
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ads_insertCrmAds(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['ad_title']);
            $stmt->getResource()->bindParam(2, $param['page_url']);
            $stmt->getResource()->bindParam(3, $param['ad_zone_id']);
            $stmt->getResource()->bindParam(4, $param['ad_type_id']);
            $stmt->getResource()->bindParam(5, $param['image_ad']);
            $stmt->getResource()->bindParam(6, $param['image_ad_url']);
            $stmt->getResource()->bindParam(7, $param['text_ad']);
            $stmt->getResource()->bindParam(8, $param['activate_date']);
            $stmt->getResource()->bindParam(9, $param['end_date']);
            $stmt->getResource()->bindParam(10, $param['is_new_window']);
            $stmt->getResource()->bindParam(11, $param['is_active']);            
            $stmt->getResource()->bindParam(12, $param['is_deleted']);
            $stmt->getResource()->bindParam(13, $param['added_by']);
            $stmt->getResource()->bindParam(14, $param['added_date']);
            $stmt->getResource()->bindParam(15, $param['modified_by']);
            $stmt->getResource()->bindParam(16, $param['modified_date']);
            $stmt->getResource()->bindParam(17, $param['description']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
    
       /**
     * This function is used to update Crm Ads
     * @param $params
     * @return boolean 
     * @author Icreon Tech - NS
     */
    public function updateCrmAds($param = array()) {
        try {
            
            if(empty($param['ad_id'])) { $param['ad_id'] = NULL; }
            if(empty($param['ad_title'])) { $param['ad_title'] = NULL; }
            if(empty($param['page_url'])) { $param['page_url'] = NULL; }
            if(empty($param['ad_zone_id'])) { $param['ad_zone_id'] = NULL; }
            if(empty($param['ad_type_id'])) { $param['ad_type_id'] = NULL; }
            if(empty($param['image_ad'])) { $param['image_ad'] = NULL; }
            if(empty($param['image_ad_url'])) { $param['image_ad_url'] = NULL; }
            if(empty($param['text_ad'])) { $param['text_ad'] = NULL; }
            if(empty($param['activate_date'])) { $param['activate_date'] = NULL; }
            if(empty($param['end_date'])) { $param['end_date'] = NULL; }
            if(empty($param['is_new_window'])) { $param['is_new_window'] = NULL; }
            if(empty($param['is_active'])) { $param['is_active'] = NULL; }
            if(empty($param['is_deleted'])) { $param['is_deleted'] = NULL; }
            if(empty($param['added_by'])) { $param['added_by'] = NULL; }
            if(empty($param['added_date'])) { $param['added_date'] = NULL; }
            if(empty($param['modified_by'])) { $param['modified_by'] = NULL; }
            if(empty($param['modified_date'])) { $param['modified_date'] = NULL; }
            if(empty($param['description'])) { $param['description'] = NULL; }
            
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ads_updateCrmAds(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['ad_id']);
            $stmt->getResource()->bindParam(2, $param['ad_title']);
            $stmt->getResource()->bindParam(3, $param['page_url']);
            $stmt->getResource()->bindParam(4, $param['ad_zone_id']);
            $stmt->getResource()->bindParam(5, $param['ad_type_id']);
            $stmt->getResource()->bindParam(6, $param['image_ad']);
            $stmt->getResource()->bindParam(7, $param['image_ad_url']);
            $stmt->getResource()->bindParam(8, $param['text_ad']);
            $stmt->getResource()->bindParam(9, $param['activate_date']);
            $stmt->getResource()->bindParam(10, $param['end_date']);
            $stmt->getResource()->bindParam(11, $param['is_new_window']);
            $stmt->getResource()->bindParam(12, $param['is_active']);
            $stmt->getResource()->bindParam(13, $param['is_deleted']);
            $stmt->getResource()->bindParam(14, $param['added_by']);
            $stmt->getResource()->bindParam(15, $param['added_date']);
            $stmt->getResource()->bindParam(16, $param['modified_by']);
            $stmt->getResource()->bindParam(17, $param['modified_date']);
            $stmt->getResource()->bindParam(18, $param['description']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return $e;
        }
    }
    
    
          /**
     * This function is used to delete Crm Ads
     * @param $params
     * @return boolean 
     * @author Icreon Tech - NS
     */
    public function deleteCrmAds($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_ads_deleteCrmAds(?)');
            $stmt->getResource()->bindParam(1, $param['ads_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
           /**
     * This function is used to getUniquePageUrlCrmAds
     * @param $params
     * @return boolean 
     * @author Icreon Tech - NS
     */
    public function getUniquePageUrlCrmAds($param = array()) {
        try {
            if(isset($param['page_url']) and trim($param['page_url']) != "" and isset($param['ad_zone_id']) and trim($param['ad_zone_id']) != "") {
                $param['page_url'] = trim($param['page_url']);
                $param['ad_zone_id'] = trim($param['ad_zone_id']);
                
                $stmt = $this->dbAdapter->createStatement();
                $stmt->prepare('CALL usp_ads_getUniquePageUrl(?,?)');
                $stmt->getResource()->bindParam(1, $param['page_url']);
                $stmt->getResource()->bindParam(2, $param['ad_zone_id']);
                $result = $stmt->execute();
                $statement = $result->getResource();
                $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
                $statement->closeCursor();
                return $resultSet[0]['UniqueCount'];
            }
            else {
                return 0;    
            }
        } catch (Exception $e) {
            return 0;
        }
    }
    
}