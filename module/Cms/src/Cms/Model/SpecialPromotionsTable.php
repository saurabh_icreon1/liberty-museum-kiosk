<?php

/**
 * This model is used for home slider
 * @category   Zend
 * @package    Ads
 * @author     Icreon Tech - NS
 */

namespace Cms\Model;

use Zend\Db\TableGateway\TableGateway;

class SpecialPromotionsTable {
    
    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
    }
    
    /**
    * This function is used to listing of home slider content type pages
    * @return $resultset 
    * @param $params
    * @author Icreon Tech - NS
    */
    public function getSpecialPromotions($params = array()) {
        try {            
            
            $image_title = isset($params['image_title'])?$params['image_title']:'';
            $is_active = isset($params['is_active'])?$params['is_active']:'';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getSpecialPromotions(?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['startIndex']);
            $stmt->getResource()->bindParam(2, $params['recordLimit']);
            $stmt->getResource()->bindParam(3, $params['sortField']);
            $stmt->getResource()->bindParam(4, $params['sortOrder']);
            $stmt->getResource()->bindParam(5, $params['isActive']);
            $stmt->getResource()->bindParam(6, $image_title);
            $stmt->getResource()->bindParam(7, $is_active);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            if(!empty($resultSet)) { return $resultSet; }
            else { return array(); }
        } catch (Exception $e) {
            return array();
        }
    }
    
     /**
    * This function is used to get home slider data
    * @return $resultset 
    * @param $params
    * @author Icreon Tech - NS
    */
    public function getSpecialPromotionsInfo($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_getSpecialPromotionsInfo(?)');
            $stmt->getResource()->bindParam(1, $params['slider_image_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            if(!empty($resultSet[0])) { return $resultSet[0]; }
            else { return array(); }
        } catch (Exception $e) {
            return array();
        }
    }
    
    
    
       /**
     * This function is used to insert Home Slider
     * @param $params
     * @return boolean 
     * @author Icreon Tech - NS
     */
    public function insertSpecialPromotions($param = array()) {
       
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_insertSpecialPromotions(?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['image_title']);
            $stmt->getResource()->bindParam(2, $param['image_name']);
            $stmt->getResource()->bindParam(3, $param['image_description']);
            $stmt->getResource()->bindParam(4, $param['image_link']);
            $stmt->getResource()->bindParam(5, $param['link_open']);
            $stmt->getResource()->bindParam(6, $param['image_order']);
            $stmt->getResource()->bindParam(7, $param['is_active']);
            $stmt->getResource()->bindParam(8, $param['is_deleted']);
            $stmt->getResource()->bindParam(9, $param['added_by']);
            $stmt->getResource()->bindParam(10, $param['added_date']);
            $stmt->getResource()->bindParam(11, $param['modified_by']);            
            $stmt->getResource()->bindParam(12, $param['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
    
       /**
     * This function is used to update Home Slider
     * @param $params
     * @return boolean 
     * @author Icreon Tech - NS
     */
    public function updateSpecialPromotions($param = array()) {   
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_updateSpecialPromotions(?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['slider_image_id']);
            $stmt->getResource()->bindParam(2, $param['image_title']);
            $stmt->getResource()->bindParam(3, $param['image_name']);
            $stmt->getResource()->bindParam(4, $param['image_description']);
            $stmt->getResource()->bindParam(5, $param['image_link']);
            $stmt->getResource()->bindParam(6, $param['link_open']);
            $stmt->getResource()->bindParam(7, $param['image_order']);
            $stmt->getResource()->bindParam(8, $param['is_active']);
            $stmt->getResource()->bindParam(9, $param['modified_by']);
            $stmt->getResource()->bindParam(10, $param['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return $e;
        }
    }
    
    
     /**
     * This function is used to delete Home Slider
     * @param $params
     * @return boolean 
     * @author Icreon Tech - NS
     */
    public function deleteSpecialPromotions($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_usr_deleteSpecialPromotions(?)');
            $stmt->getResource()->bindParam(1, $param['slider_image_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
}