<?php

/**
 * This model is used for Home Slider validation.
 * @category   Zend
 * @package    Cms
 * @author     Icreon Tech - NS
 */

namespace Cms\Model;

use Cms\Module;
// Add these import statements
use Base\Model\BaseModel;
use Zend\Db\Adapter\Adapter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class HomeSlider extends BaseModel {

    protected $inputFilter;
    protected $adapter;

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    /**
     * Function used to check variables cases     
     * @param Array
     * @return array
     * @author Icreon Tech - NS
     */
    public function exchangeArray($param = array()) {

        $parameter = array();        
        $parameter['slider_image_id'] = (isset($param['slider_image_id'])) ? $param['slider_image_id'] : '';
        $parameter['image_title'] = (isset($param['image_title'])) ? $param['image_title'] : '';      
        $parameter['image_name'] = (isset($param['image_name'])) ? $param['image_name'] : ''; 
        $parameter['image_description'] = (isset($param['image_description'])) ? $param['image_description'] : '';         
        $parameter['image_link'] = (isset($param['image_link'])) ? $param['image_link'] : '';      
        $parameter['link_open'] = (isset($param['link_open'])) ? $param['link_open'] : '';    
        if(isset($param['image_order'])) {
            if(trim($param['image_order']) != "") {
                $parameter['image_order'] = trim($param['image_order']);
            }
            else {
                $parameter['image_order'] = NULL;
            }
        }
        else {
            $parameter['image_order'] = NULL;
        }
        
        
        $parameter['is_active'] = (isset($param['is_active'])) ? $param['is_active'] : 0;
        $parameter['is_deleted'] = (isset($param['is_deleted'])) ? $param['is_deleted'] : 0;
        $parameter['added_by'] = (isset($param['added_by'])) ? $param['added_by'] : '';
        $parameter['added_date'] = (isset($param['added_date'])) ? $param['added_date'] : '';
        $parameter['modified_by'] = (isset($param['modified_by'])) ? $param['modified_by'] : '';
        $parameter['modified_date'] = (isset($param['modified_date'])) ? $param['modified_date'] : '';

        return $parameter;
    }

    public function getInputFilterHomeSlider($param = array()) {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'image_title',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
             $inputFilter->add($factory->createInput(array(
                        'name' => 'image_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'image_description',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'image_link',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'link_open',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'image_order',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_active',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
             
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

}
