<?php

/**
 * This model is used for cms validation.
 * @category   Zend
 * @package    Cms
 * @author     Icreon Tech - AS
 */

namespace Cms\Model;

use Cms\Module;
// Add these import statements
use Base\Model\BaseModel;
use Zend\Db\Adapter\Adapter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Cms extends BaseModel {

    protected $inputFilter;
    protected $adapter;

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    /**
     * Function used to check variables cases     
     * @param Array
     * @return array
     * @author Icreon Tech - AP
     */
    public function exchangeArray($param = array()) { 
     
        $parameter = array();
         $parameter['gallery_name'] = (isset($param['albumb_name'])) ? $param['albumb_name'] : null;
         $parameter['gallery_description'] = (isset($param['albumb_description'])) ? $param['albumb_description'] : null;
        if (!empty($param['add_date'])) {
            $parameter['add_date'] = (isset($param['add_date'])) ? $param['add_date'] : null;
        }
        if (!empty($param['add_by'])) {
            $parameter['add_by'] = (isset($param['add_by'])) ? $param['add_by'] : null;
        }
        $parameter['modified_date'] = (isset($param['modified_date'])) ? $param['modified_date'] : null;
        $parameter['modified_by'] = (isset($param['modified_by'])) ? $param['modified_by'] : null;
     //   $parameter['gallery_content_id'] = (isset($param['gallery_content_id'])) ? $param['gallery_content_id'] : null;
        $parameter['num_image'] = (isset($param['num_image'])) ? $param['num_image'] : null;
        $parameter['num_video'] = (isset($param['num_video'])) ? $param['num_video'] : null;
        if (!empty($param['gallery_id'])) {
            $parameter['gallery_id'] = (isset($param['gallery_id'])) ? $param['gallery_id'] : null;
        }

        return $parameter;
    }

    public function getInputFilterCms($param = array()) {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            foreach ($param as $key) {
                $id = str_replace(" ", "_", $key['content_field_title']);
                if ($key['content_field_type'] == "editor") {
                    $inputFilter->add($factory->createInput(array(
                                'name' => $id,
                                'required' => false,
                            )));
                } else {
                    $inputFilter->add($factory->createInput(array(
                                'name' => $id,
                                'required' => false,
                                'filters' => array(
                                    array('name' => 'StripTags'),
                                    array('name' => 'StringTrim'),
                                ),
                            )));
                }
            }
            $inputFilter->add($factory->createInput(array(
                        'name' => 'page_title',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'page_description',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'abstract',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'keywords',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
          
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

     /**
     * Function used to check validation for cms add gallery form
     * @param Array	 
     * @return array
     * @author Icreon Tech - AP
     */
    public function getInputFilterCrmGallery() {
         if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'albumb_name',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'albumb_description',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
           $inputFilter->add($factory->createInput(array(
                'name'     => 'csrf',
                'required' => true,
                'validators' => array(
                            array(
                                'name' => 'csrf',
                                'options' => array(
                                    'messages' => array(
                                        'notSame' => 'NOT_SAME',
                                    ),
                                ),
                            ),
                        ),
            )));
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }
     /**
     * Function used to check validation for cms add gallery images form
     * @param Array	 
     * @return array
     * @author Icreon Tech - AP
     */
    public function getInputFilterCrmGalleryImages(){
             if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'image_name',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'gallery_id',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
        
    }
    
   
}