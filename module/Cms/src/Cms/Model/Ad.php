<?php

/**
 * This model is used for cms validation.
 * @category   Zend
 * @package    Cms
 * @author     Icreon Tech - NS
 */

namespace Cms\Model;

use Cms\Module;
// Add these import statements
use Base\Model\BaseModel;
use Zend\Db\Adapter\Adapter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Ad extends BaseModel {

    protected $inputFilter;
    protected $adapter;

    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    /**
     * Function used to check variables cases     
     * @param Array
     * @return array
     * @author Icreon Tech - NS
     */
    public function exchangeArray($param = array()) {

        $parameter = array();

        if (isset($param['ad_id']) and trim($param['ad_id']) != "") {
            $parameter['ad_id'] = (isset($param['ad_id'])) ? $param['ad_id'] : '';
        }

        $parameter['ad_title'] = (isset($param['ad_title'])) ? $param['ad_title'] : '';
        $parameter['description'] = (isset($param['description'])) ? $param['description'] : '';


        if (isset($param['page_url']) and trim($param['page_url']) != "") {
            $parameter['page_url'] = trim($param['page_url']);
        } else {
            $parameter['page_url'] = (isset($param['page_url'])) ? $param['page_url'] : '';
        }

        $parameter['ad_zone_id'] = (isset($param['ad_zone_id'])) ? $param['ad_zone_id'] : '';
        $parameter['ad_type_id'] = (isset($param['ad_type_id'])) ? $param['ad_type_id'] : '';
        $parameter['image_ad'] = (isset($param['upload_file_name'])) ? $param['upload_file_name'] : '';

        if (isset($param['image_ad_url']) and trim($param['image_ad_url']) != "") {
            $parameter['image_ad_url'] = trim($param['image_ad_url']);
        } else {
            $parameter['image_ad_url'] = (isset($param['image_ad_url'])) ? $param['image_ad_url'] : '';
        }

        $parameter['text_ad'] = (isset($param['text_ad'])) ? $param['text_ad'] : '';

        if (isset($param['activate_date']) and !empty($param['activate_date'])) {
            $parameter['activate_date'] = date("Y-m-d", strtotime($param['activate_date']));
        } else {
            $parameter['activate_date'] = null;
        }

        if (isset($param['end_date']) and !empty($param['end_date'])) {
            $parameter['end_date'] = date("Y-m-d", strtotime($param['end_date']));
        } else {
            $parameter['end_date'] = null;
        }

        $parameter['is_new_window'] = (isset($param['is_new_window'])) ? $param['is_new_window'] : 0;
        $parameter['is_active'] = (isset($param['is_active'])) ? $param['is_active'] : 0;
        $parameter['is_deleted'] = (isset($param['is_deleted'])) ? $param['is_deleted'] : 0;
        $parameter['added_by'] = (isset($param['added_by'])) ? $param['added_by'] : '';
        $parameter['added_date'] = (isset($param['added_date'])) ? $param['added_date'] : '';
        $parameter['modified_by'] = (isset($param['modified_by'])) ? $param['modified_by'] : '';
        $parameter['modified_date'] = (isset($param['modified_date'])) ? $param['modified_date'] : '';

        return $parameter;
    }

    public function getInputFilterAds($param = array()) {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'ad_title',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'ERROR_TITLE_EMPTY',
                                    ),
                                ),
                            )
//                           ,
//                            array(
//                                'name' => 'Alnum',
//                                'options' => array(
//                                    'messages' => array(
//                                      \Zend\I18n\Validator\Alnum::INVALID => "ERROR_TITLE_ALPHANUMERIC"
//                                       //       \Zend\Validator\EmailAddress::INVALID_FORMAT => "ERROR_TITLE_ALPHANUMERIC",
//                                    ),
//                                ),
//                              )
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'page_url',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
//                  'validators' => array(
//                            array(
//                                'name' => 'Alnum',
//                                'options' => array(
//                                    'messages' => array(
//                                       \Zend\Validator\Uri::NOT_URI => "ERROR_PAGE_URL_VALID",
//                                    ),
//                                ),
//                            ) 
//                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'ad_zone_id',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'ad_type_id',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'image_ad',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'image_ad_url',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'text_ad',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activate_date',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'activate_time',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'end_date',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'end_time',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'is_active',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'description',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

}
