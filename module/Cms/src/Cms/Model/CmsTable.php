<?php

/**
 * This model is used for cms.
 * @category   Zend
 * @package    Cms
 * @author     Icreon Tech - AS
 */

namespace Cms\Model;

use Zend\Db\TableGateway\TableGateway;

class CmsTable {

    protected $tableGateway;
    protected $dbAdapter;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
    }

    /**
     * Function for to insert cms gallery
     * @param array
     * @return boolean
     * @author Icreon Tech - AP
     */
    public function insertCrmGallery($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_insertCrmGallery(?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['gallery_name']);
            $stmt->getResource()->bindParam(2, $param['gallery_description']);
            $stmt->getResource()->bindParam(3, $param['add_date']);
            $stmt->getResource()->bindParam(4, $param['add_by']);
            $stmt->getResource()->bindParam(5, $param['num_image']);
            $stmt->getResource()->bindParam(6, $param['num_video']);
            $stmt->getResource()->bindParam(7, $param['modified_date']);
            $stmt->getResource()->bindParam(8, $param['modified_by']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to listing of cms content type pages
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function getCrmCmsContentTypes($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_getCrmCmsContentTypes(?)');
            $stmt->getResource()->bindParam(1, $params['id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the crm gallery data
     * @param  array.
     * @return array
     * @author Icreon Tech -AP
     */
    public function getCrmGalleries($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_getCrmGalleries(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['gallery_name']);
            $stmt->getResource()->bindParam(2, $param['startIndex']);
            $stmt->getResource()->bindParam(3, $param['recordLimit']);
            $stmt->getResource()->bindParam(4, $param['sortField']);
            $stmt->getResource()->bindParam(5, $param['sortOrder']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the crm gallery info
     * @param  array.
     * @return array
     * @author Icreon Tech -AP
     */
    public function getCrmGalleryInfo($param = array()) {
        try {
            // asd($param);
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_getCrmGalleryInfo(?)');
            $stmt->getResource()->bindParam(1, $param['gallery_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to update crm gallery data
     * @param  array.
     * @return boolean
     * @author Icreon Tech -AP
     */
    public function updateCrmGallery($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_updateCrmGallery(?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['gallery_id']);
            $stmt->getResource()->bindParam(2, $param['modified_date']);
            $stmt->getResource()->bindParam(3, $param['modified_by']);
            $stmt->getResource()->bindParam(4, $param['gallery_name']);
            $stmt->getResource()->bindParam(5, $param['gallery_description']);
            //    $stmt->getResource()->bindParam(6, $param['gallery_content_id']);
            $stmt->getResource()->bindParam(6, $param['num_image']);
            $stmt->getResource()->bindParam(7, $param['num_video']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /* Function to delete cms gallery content
     * @param array
     * @return boolean
     * @author Icreon Team -AP
     */

    public function deleteCmsGalleryContent($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_deleteCrmGalleryContent(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['gallery_content_id']);
            $stmt->getResource()->bindParam(2, $param['file_type']);
            $stmt->getResource()->bindParam(3, $param['modified_date']);
            $stmt->getResource()->bindParam(4, $param['modified_by']);
            $stmt->getResource()->bindParam(5, $param['gallery_id']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get page type form
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function getCrmCmsContentPage($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_getCrmCmsContentPage(?)');
            $stmt->getResource()->bindParam(1, $params['id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get menu array
     * @return $resultset 
     * @param void
     * @author Icreon Tech - AS
     */
    public function getMenuList($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_getMenuList(?)');
            $stmt->getResource()->bindParam(1, $params);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to insert page content
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function insertCmsContent($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_insertCrmCmsContent(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['content_type_id']);
            if (isset($params['Title']) && !empty($params['Title'])) {
                $params['Title'] = ucwords($params['Title']);
            } else if (isset($params['Name']) && !empty($params['Name'])) {
                $params['Title'] = ucwords($params['Name']);
            }
            $stmt->getResource()->bindParam(2, $params['Title']);
            $stmt->getResource()->bindParam(3, $params['Short_Description']);
            $stmt->getResource()->bindParam(4, $params['Description']);
            $stmt->getResource()->bindParam(5, $params['Page_Content']);
            $stmt->getResource()->bindParam(6, $params['Display_Order']);
            $stmt->getResource()->bindParam(7, $params['Designation']);
            $stmt->getResource()->bindParam(8, $params['Company_Name']);
            $stmt->getResource()->bindParam(9, $params['Upload_File']);
            $stmt->getResource()->bindParam(10, $params['Date']);
            $stmt->getResource()->bindParam(11, $params['Link']);
            $stmt->getResource()->bindParam(12, $params['is_deleted']);
            $stmt->getResource()->bindParam(13, $params['content_url']);
            $stmt->getResource()->bindParam(14, $params['page_title']);
            $stmt->getResource()->bindParam(15, $params['page_description']);
            $stmt->getResource()->bindParam(16, $params['abstract']);
            $stmt->getResource()->bindParam(17, $params['keywords']);
            $stmt->getResource()->bindParam(18, $params['is_publish']);
            $stmt->getResource()->bindParam(19, $params['addedDate']);
            $stmt->getResource()->bindParam(20, $params['addedBy']);
            $stmt->getResource()->bindParam(21, $params['is_published']);
            $stmt->getResource()->bindParam(22, $params['Category']);
            $stmt->getResource()->bindParam(23, $params['Claim_To_Fame']);
            $stmt->getResource()->bindParam(24, $params['Passenger_Id']);
            $stmt->getResource()->bindParam(25, $params['is_right']);
            $stmt->getResource()->bindParam(26, $params['num_ads']);
            $stmt->getResource()->bindParam(27, $params['Year']);
            $stmt->getResource()->bindParam(28, $params['Parent_Content']);
            $stmt->getResource()->bindParam(29, $params['accessLevel']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to insert page content
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function getCrmContents($params = array()) {
        try {
            $access_label = isset($params['access_label']) ? $params['access_label'] : '';
            $content_date = isset($params['content_date']) ? $params['content_date'] : '';
            $title = isset($params['title']) ? $params['title'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_listCmsContent(?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['startIndex']);
            $stmt->getResource()->bindParam(2, $params['recordLimit']);
            $stmt->getResource()->bindParam(3, $params['sortField']);
            $stmt->getResource()->bindParam(4, $params['sortOrder']);
            $stmt->getResource()->bindParam(5, $params['status']);
            $stmt->getResource()->bindParam(6, $params['type']);
            $stmt->getResource()->bindParam(7, $params['content_id']);
            $stmt->getResource()->bindParam(8, $access_label);
            $stmt->getResource()->bindParam(9, $content_date);
            $stmt->getResource()->bindParam(10, $title);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to delete page content
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function deleteCmsContent($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_deleteCrmCmsContent(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['content_id']);
            $stmt->getResource()->bindParam(2, $params['modified_by']);
            $stmt->getResource()->bindParam(3, $params['modified_date']);
            $stmt->getResource()->bindParam(4, $params['deleted']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to update page content
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function updateCmsContent($params = array()) {
        try {
            $params['Description'] = addslashes($params['Description']);
            $params['Title'] = addslashes($params['Title']);
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_updateCrmCmsContent(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['content_id']);
            $stmt->getResource()->bindParam(2, $params['Title']);
            $stmt->getResource()->bindParam(3, $params['Short_Description']);
            $stmt->getResource()->bindParam(4, $params['Description']);
            $stmt->getResource()->bindParam(5, $params['page_content_id']);
            $stmt->getResource()->bindParam(6, $params['display_order']);
            $stmt->getResource()->bindParam(7, $params['Designation']);
            $stmt->getResource()->bindParam(8, $params['Company_Name']);
            $stmt->getResource()->bindParam(9, $params['Upload_File']);
            $stmt->getResource()->bindParam(10, $params['Date']);
            $stmt->getResource()->bindParam(11, $params['Link']);
            $stmt->getResource()->bindParam(12, $params['is_deleted']);
            $stmt->getResource()->bindParam(13, $params['content_url']);
            $stmt->getResource()->bindParam(14, $params['page_title']);
            $stmt->getResource()->bindParam(15, $params['page_description']);
            $stmt->getResource()->bindParam(16, $params['abstract']);
            $stmt->getResource()->bindParam(17, $params['keywords']);
            $stmt->getResource()->bindParam(18, $params['is_publish']);
            $stmt->getResource()->bindParam(19, $params['addedDate']);
            $stmt->getResource()->bindParam(20, $params['addedBy']);
            $stmt->getResource()->bindParam(21, $params['is_published']);
            $stmt->getResource()->bindParam(22, $params['revision_id']);
            $stmt->getResource()->bindParam(23, $params['Category']);
            $stmt->getResource()->bindParam(24, $params['Claim_To_Fame']);
            $stmt->getResource()->bindParam(25, $params['Passenger_Id']);
            $stmt->getResource()->bindParam(26, $params['is_right']);
            $stmt->getResource()->bindParam(27, $params['num_ads']);
            $stmt->getResource()->bindParam(28, $params['accessLevel']);
            $stmt->getResource()->bindParam(29, $params['Year']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to insert gallery content
     * @param $params
     * @return boolean 
     * @author Icreon Tech - AP
     */
    public function insertCrmGalleryContent($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_insertCrmGalleryContent(?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['gallery_id']);
            $stmt->getResource()->bindParam(2, $param['file_type']);
            $stmt->getResource()->bindParam(3, $param['phy_file_name']);
            $stmt->getResource()->bindParam(4, $param['content_file_name']);
            $stmt->getResource()->bindParam(5, $param['added_date']);
            $stmt->getResource()->bindParam(6, $param['added_by']);
            $stmt->getResource()->bindParam(7, $param['modified_date']);
            $stmt->getResource()->bindParam(8, $param['modified_by']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if (!empty($resultSet))
                return $resultSet['0']['LAST_INSERT_ID'];
            else
                return false;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to update page content
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function getCmsContentChangeLogs($params = array()) {
        try {

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_getCrmCmsContentLogs(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['startIndex']);
            $stmt->getResource()->bindParam(2, $params['recordLimit']);
            $stmt->getResource()->bindParam(3, $params['sortField']);
            $stmt->getResource()->bindParam(4, $params['sortOrder']);
            $stmt->getResource()->bindParam(5, $params['content_id']);
            $stmt->getResource()->bindParam(6, $params['content_log_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Function is used to get gallery details 
     * @param Array
     * @return array
     * @author Icreon Tech - AP
     */
    public function getCrmGalleryContent($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_getCrmGalleryContent(?)');
            $stmt->getResource()->bindParam(1, $param['gallery_content_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
        return rtrim($returnstr, ',');
    }

    /**
     * This function is used to update page menus
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function insertCmsMenu($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_insertCrmCmsMenu(?,?,?,?,?)');
            if (isset($params['Navigation_Link_Title']) && !empty($params['Navigation_Link_Title'])) {
                $params['Navigation_Link_Title'] = ucwords($params['Navigation_Link_Title']);
            }
            $stmt->getResource()->bindParam(1, $params['Navigation_Link_Title']);
            $stmt->getResource()->bindParam(2, $params['Header_Menu_Item_Id']);
            $stmt->getResource()->bindParam(3, $params['added_date']);
            $stmt->getResource()->bindParam(4, $params['added_by']);
            $stmt->getResource()->bindParam(5, $params['is_publish']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to update page menus
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function insertCmsContentMenu($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_insertCrmCmsContentMenu(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['content_id']);
            $stmt->getResource()->bindParam(2, $params['menu_id']);
            $stmt->getResource()->bindParam(3, $params['addedDate']);
            $stmt->getResource()->bindParam(4, $params['addedBy']);
            $stmt->getResource()->bindParam(5, $params['menu_type']);            
            $result = $stmt->execute();
            $statement = $result->getResource();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to update page menus
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function getCrmContentsUrl($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_getcrmcontenturl(?,?)');
            $stmt->getResource()->bindParam(1, $params['url']);
            $stmt->getResource()->bindParam(2, $params['id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to save the rearranged menus
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function changeOrderMenu($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_updateMenuOrder(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['parentId']);
            $stmt->getResource()->bindParam(2, $params['menuId']);
            $stmt->getResource()->bindParam(3, $params['sortOrder']);
            $stmt->getResource()->bindParam(4, $params['modifiedBy']);
            $stmt->getResource()->bindParam(5, $params['modifiedDate']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            //$resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to save the rearranged menus
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function updateCmsContentStatus($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_updateCmsContentStatus(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['publish']);
            $stmt->getResource()->bindParam(2, $params['id']);
            $stmt->getResource()->bindParam(3, $params['modifiedBy']);
            $stmt->getResource()->bindParam(4, $params['modifiedDate']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * This function is used to save the rearranged menus
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function updateCmsFooterStatus($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_updateCmsFooterStatus(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['publish']);
            $stmt->getResource()->bindParam(2, $params['id']);
            $stmt->getResource()->bindParam(3, $params['modifiedBy']);
            $stmt->getResource()->bindParam(4, $params['modifiedDate']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * This function is used to save the rearranged menus
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function getCrmContentMenu($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_getCmsContentMenu(?)');
            $stmt->getResource()->bindParam(1, $params['content_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get page content
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function getCrmContentUrl($params = array()) {
        try {
            $view = '';
            if (isset($params[1]) && !empty($params[1])) {
                $url = $params['1'];
            } else {
                $url = '';
            }
            if (isset($params[2]) && !empty($params[2])) {
                $view = $params[2];
            }
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_getCmsContent(?,?)');
            $stmt->getResource()->bindParam(1, $url);
            $stmt->getResource()->bindParam(2, $view);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get page content
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function updateCmsMenu($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_updateCmsContentMenu(?)');
            $stmt->getResource()->bindParam(1, $params['content_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get page content
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function getCrmCmsContentPageMenu($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_gCmsContentMenu(?)');
            $stmt->getResource()->bindParam(1, $params['content_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get page content
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function getFaCaterogy() {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_getFaCategory()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to insert famous arrivals category
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function insertFaCategory($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_insertFaCategory(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['name']);
            $stmt->getResource()->bindParam(2, $params['img_name']);
            $stmt->getResource()->bindParam(3, $params['added_by']);
            $stmt->getResource()->bindParam(4, $params['added_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to insert famous arrivals category
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function updateFaCategory($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_updateFaCategory(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['id']);
            $stmt->getResource()->bindParam(2, $params['name']);
            $stmt->getResource()->bindParam(3, $params['img_name']);
            $stmt->getResource()->bindParam(4, $params['added_by']);
            $stmt->getResource()->bindParam(5, $params['added_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to select all right section content
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function getRightSection() {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_getRightSection()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to insert cms right section
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function insertCmsRight($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_insertRightSection(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['content_id']);
            $stmt->getResource()->bindParam(2, $params['right_section_id']);
            $stmt->getResource()->bindParam(3, $params['addedDate']);
            $stmt->getResource()->bindParam(4, $params['addedBy']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to insert cms right section
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function getAdsContents($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_getAdsContent(?,?)');
            $stmt->getResource()->bindParam(1, $params['num_ad_blocks']);
            $stmt->getResource()->bindParam(2, $params['content_url']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to insert cms right section
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function getGeneologyParentContent($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_getGenologyParent(?)');
            $stmt->getResource()->bindParam(1, $params['id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get famous arrival content
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function getFaContents($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_listFaContent(?,?)');
            $stmt->getResource()->bindParam(1, $params['faCat']);
            $stmt->getResource()->bindParam(2, $params['faPage']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get heritage award content
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function getHaContents($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_getHaContent(?,?)');
            $stmt->getResource()->bindParam(1, $params['pageContent']);
            $stmt->getResource()->bindParam(2, $params['contentType']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get geneology content
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function getGenContents($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_getGenContent(?,?)');
            $stmt->getResource()->bindParam(1, $params['pageContent']);
            $stmt->getResource()->bindParam(2, $params['parentId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get menus with respect to parent menu
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - NS
     */
    public function getCrmCmsMenus($params = array()) {
        try {

            $params['parentMenuId'] = isset($params['parentMenuId']) ? $params['parentMenuId'] : 0;

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_getMenus(?)');
            $stmt->getResource()->bindParam(1, $params['parentMenuId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return array();
        }
    }

    /* Function to delete cms gallery
     * @param array
     * @return boolean
     * @author Icreon Team -AS
     */

    public function deleteCmsGallery($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_deleteCrmGallery(?,?,?)');
            $stmt->getResource()->bindParam(1, $param['content_id']);
            $stmt->getResource()->bindParam(2, $param['modified_by']);
            $stmt->getResource()->bindParam(3, $param['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function getHeritageContents($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_getHeritageContent(?)');
            $stmt->getResource()->bindParam(1, $params['contentId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    public function getCurrentCmsContent($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_getLatestCmsContent(?)');
            $stmt->getResource()->bindParam(1, $params['contentId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    public function getAnnouncementNext($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_getAnnouncementNext(?,?)');
            $stmt->getResource()->bindParam(1, $params['content_id']);
            $stmt->getResource()->bindParam(2, $params['content_type_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    
    public function getAnnouncementPre($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_getAnnouncementPre(?,?)');
            $stmt->getResource()->bindParam(1, $params['content_id']);
            $stmt->getResource()->bindParam(2, $params['content_type_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }   
    
    /**
     * This function is used to insert page content
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function getCrmContent($params = array()) {
        try {
            $access_label = isset($params['access_label']) ? $params['access_label'] : '';
            $content_date = isset($params['content_date']) ? $params['content_date'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_getCmsEditContent(?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $params['startIndex']);
            $stmt->getResource()->bindParam(2, $params['recordLimit']);
            $stmt->getResource()->bindParam(3, $params['sortField']);
            $stmt->getResource()->bindParam(4, $params['sortOrder']);
            $stmt->getResource()->bindParam(5, $params['status']);
            $stmt->getResource()->bindParam(6, $params['type']);
            $stmt->getResource()->bindParam(7, $params['content_id']);
            $stmt->getResource()->bindParam(8, $access_label);
            $stmt->getResource()->bindParam(9, $content_date);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * This function is used to insert page content
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function deleteRight($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_deleteCmsRight(?,?)');
            $stmt->getResource()->bindParam(1, $params['content_id']);
            $stmt->getResource()->bindParam(2, $params['user_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * This function is used to get timeline content
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function getTimelineContents($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_getTimelineContent(?,?)');            
            $stmt->getResource()->bindParam(1, $params['pageContent']);
            $stmt->getResource()->bindParam(2, $params['year']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * This function is used to delete header menu
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function deleteCmsHeaderMenu($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_deleteCmsHeaderMenu(?)');
            $stmt->getResource()->bindParam(1, $params['content_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * This function is used to delete header menu
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function deleteCmsFooterMenu($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_deleteCmsFooterMenu(?)');
            $stmt->getResource()->bindParam(1, $params['content_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
    
    /**
     * This function is used to delete header menu
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function insertTermsVersion($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_insertTermsVersion(?,?)');
            $stmt->getResource()->bindParam(1, $params['addedDate']);
            $stmt->getResource()->bindParam(2, $params['addedBy']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
    
    /**
     * This function is used to delete header menu
     * @return $resultset 
     * @param $params
     * @author Icreon Tech - AS
     */
    public function updateCmsContentMenu($params = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cms_updateCrmCmsMenu(?,?,?,?,?,?)');
            if (isset($params['Navigation_Link_Title']) && !empty($params['Navigation_Link_Title'])) {
                $params['Navigation_Link_Title'] = ucwords($params['Navigation_Link_Title']);
            }
            $stmt->getResource()->bindParam(1, $params['Menu_Id']);
            $stmt->getResource()->bindParam(2, $params['Navigation_Link_Title']);
            $stmt->getResource()->bindParam(3, $params['Header_Menu_Item_Id']);
            $stmt->getResource()->bindParam(4, $params['modifiedDate']);
            $stmt->getResource()->bindParam(5, $params['modifiedBy']);
            $stmt->getResource()->bindParam(6, $params['is_publish']);
            $result = $stmt->execute();
            $statement = $result->getResource();            
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}