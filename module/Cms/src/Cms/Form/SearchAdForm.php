<?php

/**
 * This form is used to search ads page 
 * @package    CMS
 * @author     Icreon Tech - AS
 */

namespace Cms\Form;

use Zend\Form\Form;

class SearchAdForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('search-Ads');
        $this->setAttribute('id', 'search_ads');
        $this->setAttribute('name', 'search_ads');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'ad_title',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ad_title',
                'maxlength' => 50
            )
        ));

        $this->add(array(
            'name' => 'page_url',
            'attributes' => array(
                'type' => 'text',
                'id' => 'page_url',
                'maxlength' => '255',
                'class' => 'width-17 m-l-5'
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'ad_zone_id',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
                'disable_inarray_validator' => true
            ),
            'attributes' => array(
                'id' => 'ad_zone_id',
                'class' => 'e1 select2-offscreen',
                'value' => ''
            )
        ));



        $this->add(array(
            'type' => 'Select',
            'name' => 'ad_status',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    '1' => 'Active',
                    '0' => 'Inactive'
                ),
            ),
            'attributes' => array(
                'id' => 'ad_status',
                'class' => 'e1 select2-offscreen',
                'value' => ''
            )
        ));

      $this->add(array(
            'type' => 'Select',
            'name' => 'ad_type_id',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
                'disable_inarray_validator' => true
            ),
            'attributes' => array(
                'id' => 'ad_type_id',
                'class' => 'e1 select2-offscreen',
                'value' => ''
            )
        ));
           
      $this->add(array(
            'name' => 'description',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'description',
                'class' => 'width-300'
            )
        ));
      
       $this->add(array(
            'name' => 'activate_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'activate_date'
            )
        ));
           
       
       $this->add(array(
            'name' => 'end_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'end_date'
            )
        ));
           
        $this->add(array(
            'name' => 'searchbutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Search',
                'id' => 'searchbutton',
                'class' => 'search-btn m-l-10'
            ),
        ));
    }

}