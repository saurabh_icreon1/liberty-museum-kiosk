<?php

/**
 * This form is used to search banner page 
 * @package    CMS
 * @author     Icreon Tech - NS
 */

namespace Cms\Form;

use Zend\Form\Form;

class SearchHomeSliderForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('search-HS');
        $this->setAttribute('id', 'search_hs');
        $this->setAttribute('name', 'search_hs');
        $this->setAttribute('method', 'post');

         $this->add(array(
            'name' => 'image_title',
            'attributes' => array(
                'type' => 'text',
                'id' => 'image_title',
                'maxlength' => 200
            )
        )); 

    

         $this->add(array(
            'type' => 'Select',
            'name' => 'is_active',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    '1' => 'Active',
                    '0' => 'Inactive'
                ),
            ),
            'attributes' => array(
                'id' => 'is_active',
                'class' => 'e1 select2-offscreen',
                'value' => ''
            )
        ));

 
        $this->add(array(
            'name' => 'searchbutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Search',
                'id' => 'searchbutton',
                'class' => 'search-btn m-l-10'
            ),
        ));
    }

}