<?php

/**
 * This form is used for create crm gallery
 * @package    Cms
 * @author     Icreon Tech - AP
 */

namespace Cms\Form;

use Zend\Form\Form;

/**
 * This class is used for create crm gallery
 * @package    Cms
 * @author     Icreon Tech - AP
 */
class AddCrmGalleryForm extends Form {

    public function __construct($name = null) {
        parent::__construct('addGallery');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'type' => 'text',
            'name' => 'albumb_name',
            'attributes' => array(
                'id' => 'albumb_name'
            )
        ));
        $this->add(array(
            'type' => 'textarea',
            'name' => 'albumb_description',
            'attributes' => array(
                'id' => 'albumb_description',
                'class' => 'width-90'
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'albumb_id',
            'attributes' => array(
                'id' => 'albumb_id'
            )
        ));
        $this->add(array(
            'name' => 'add_gallery',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'add_gallery',
                'class' => 'save-btn',
                'value' => 'Save'
            )
        ));
        $this->add(array(
            'name' => 'remove',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Cancel',
                'id' => 'remove',
                'class' => 'cancel-btn m-l-20',
                'onClick' => "javascript:window.location.href='/crm-galleries'",
            ),
        ));
        $this->add(array('type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'options' => array(
                'corpcsrf_options' => array(
                    'timeout' => 600
                )
            )
        ));
    }

}

