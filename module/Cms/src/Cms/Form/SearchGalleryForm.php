<?php

/**
 * This form is used for search crm gallery
 * @package    Cms
 * @author     Icreon Tech - AP
 */

namespace Cms\Form;

use Zend\Form\Form;

/**
 * This class is used for search crm gallery
 * @package    Cms
 * @author     Icreon Tech - AP
 */

class SearchGalleryForm extends Form {

    public function __construct($name = null) {
        parent::__construct('searchGallary');
        $this->setAttribute('method', 'post');
        
        $this->add(array(
            'type' => 'text',
            'name' => 'albumb_name',
            'attributes' => array(
                'id' => 'albumb_name'
            )
        ));
       
        $this->add(array(
            'name' => 'search',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'search',
                'class' => 'save-btn',
                'value' => 'Search'
            )
        ));
    
       
    }

}

