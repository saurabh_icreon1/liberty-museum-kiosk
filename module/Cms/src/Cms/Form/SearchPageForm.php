<?php

/**
 * This form is used for create Search page.
 * @category   Zend
 * @package    Cms_SearchPageForm
 * @author     Icreon Tech - AG
 */

namespace Cms\Form;

use Zend\Form\Form;

class SearchPageForm extends Form {

    public function __construct($name = null) {
        parent::__construct('search_page');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'type' => 'text',
            'name' => 'title',
            'attributes' => array(
                'id' => 'title'
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'status',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'status',
                'class' => 'e1 select-w-320'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'type',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'type',
                'class' => 'e1 select-w-320'
            )
        ));

        $this->add(array(
            'name' => 'searchpagebutton',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'searchpagebutton',
                'class' => 'search-btn',
                'value' => 'SEARCH'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'publish',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    '1' => 'Publish Selected Content',
                    '0' => 'Unpublish Selected Content'
                ),
            ),
            'attributes' => array(
                'id' => 'publish',
                'class' => 'e1 select-w-320'
            )
        ));
        $this->add(array(
            'name' => 'updatebutton',
            'attributes' => array(
                'type' => 'button',
                'id' => 'updatebutton',
                'class' => 'save-btn',
                'value' => 'Update'
            )
        ));
    }

}

