<?php

/**
 * This form is used for upload crm  gallery images
 * @package    Cms
 * @author     Icreon Tech - AP
 */

namespace Cms\Form;

use Zend\Form\Form;

/**
 * This class is used for upload crm  gallery images
 * @package    Cms
 * @author     Icreon Tech - AP
 */
class uploadGalleryImagesForm extends Form {

    public function __construct($name = null) {
        parent::__construct('fileupload');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->add(array(
            'type' => 'file',
            'name' => 'files[]',
            'attributes' => array(
                'id' => 'files',
            //'multiple'=>true
            )
        ));
        $this->add(array(
            'type' => 'checkbox',
            'name' => 'deletAll',
            //'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'deletAll',
                'class' => 'e2 toggle',
                'onClick' => 'addClass(this.value);'
            )
        ));
        $this->add(array(
            'type' => 'hidden',
            'name' => 'gallery_id',
            'attributes' => array(
                'id' => 'gallery_id'
            )
        ));

        $this->add(array(
            'type' => 'hidden',
            'name' => 'counter',
            'attributes' => array(
                'id' => 'counter',
            )
        ));
        $this->add(array(
            'name' => 'save_images',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'save_images',
                'class' => 'save-btn',
                'value' => 'Save'
            )
        ));
     $this->add(array(
            'name' => 'cancelbutton',
            'attributes' => array(
                'type' => 'button',
                'id' => 'cancelbutton',
                'class' => 'cancel-btn m-l-10',
                'value' => 'Cancel',
                'onClick'=>"javascript:window.location.href='/crm-galleries'"
            )
        ));
    }

}

