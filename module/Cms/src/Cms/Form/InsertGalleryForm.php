<?php

/**
 * This form is used for create crm gallery
 * @package    Cms
 * @author     Icreon Tech - AP
 */

namespace Cms\Form;

use Zend\Form\Form;

/**
 * This class is used for insert crm gallery into editor
 * @package    Cms
 * @author     Icreon Tech - AS
 */

class InsertGalleryForm extends Form {

    public function __construct($name = null) {
        parent::__construct('insertGallery');
        $this->setAttribute('method', 'post');        
        $this->add(array(
            'type' => 'Select',
            'name' => 'gallery',
            'options' => array(
                'value_options' => array(),
            ),
            'attributes' => array(
                'id' => 'gallery',
                'multiple' => 'multiple',
                'value' => ''
            )
        ));
        $this->add(array(
            'type' => 'text',
            'name' => 'thumbnails',
            'attributes' => array(
                'id' => 'thumbnails'
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'orientation',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    '1' => 'Horizontal',
                    '2' => 'Vertical'
                ),
            ),
            'attributes' => array(
                'id' => 'orientation',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));
        $this->add(array(
            'name' => 'is_tabbed',
            'type' => 'Checkbox',
            'attributes' => array(
                'value' => '0',
                'id' => 'is_tabbed',
                'class' => 'e2',
            ),
            'options' => array(
                'value_options' => array(
                    '0' => 'Checkbox',
                    '1' => 'Checkbox',
                ),
            ),
        ));
        $this->add(array(
            'name' => 'insert_gallery',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'insert_gallery',
                'class' => 'save-btn',
                'value' => 'Insert'
            )
        ));
        $this->add(array('type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'options' => array(
                'corpcsrf_options' => array(
                    'timeout' => 600
                )
            )
        ));
    }

}


