<?php

/**
 * This form is used to edit content page 
 * @package    CMS
 * @author     Icreon Tech - AS
 */

namespace Cms\Form;

use Zend\Form\Form;

class EditContentForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('add_content');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'content_url',
            'attributes' => array(
                'type' => 'text',
                'id' => 'content_url'
            )
        ));
        $this->add(array(
            'name' => 'content_url_hidden',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'content_url_hidden',
                'value' => '0'
            )
        ));
        $this->add(array(
            'name' => 'page_title',
            'attributes' => array(
                'type' => 'text',
                'id' => 'page_title'
            )
        ));

        $this->add(array(
            'name' => 'page_description',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'page_description',
                'class' => 'width-90'
            )
        ));

        $this->add(array(
            'name' => 'abstract',
            'attributes' => array(
                'type' => 'text',
                'id' => 'abstract'
            )
        ));

        $this->add(array(
            'name' => 'keywords',
            'attributes' => array(
                'type' => 'text',
                'id' => 'keywords'
            )
        ));


        $this->add(array(
            'name' => 'is_published',
            'type' => 'Checkbox',
            'attributes' => array(
                'value' => '0',
                'id' => 'is_published',
                'class' => 'e2',
            ),
            'options' => array(
                'value_options' => array(
                    '0' => 'Checkbox',
                    '1' => 'Checkbox',
                ),
            ),
        ));
        $this->add(array(
            'name' => 'savebutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'id' => 'savebutton',
                'class' => 'save-btn m-l-10'
            ),
        ));
        $this->add(array(
            'name' => 'updatebutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Update',
                'id' => 'updatebutton',
                'class' => 'save-btn'
            ),
        ));

        $this->add(array(
            'name' => 'cancelbutton',
            'attributes' => array(
                'type' => 'button',
                'value' => 'CANCEL',
                'id' => 'cancelbutton',
                'class' => 'cancel-btn m-l-10',
                'onclick' => 'window.location.href="/crm-contents"'
            ),
        ));
        $this->add(array(
            'name' => 'gallery_data',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'gallery_data'
            ),
        ));
        $this->add(array(
            'name' => 'is_right',
            'type' => 'Checkbox',
            'attributes' => array(
                'value' => '0',
                'id' => 'is_right',
                'class' => 'e2',
            ),
            'options' => array(
                'value_options' => array(
                    '0' => 'Checkbox',
                    '1' => 'Checkbox',
                ),
            ),
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'right_section',
            'options' => array(
                'value_options' => array(),
            ),
            'attributes' => array(
                'id' => 'right_section',
                'multiple' => 'multiple',
                'value' => ''
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'num_ads',
            'options' => array(
                'value_options' => array(
                    '0' => '0',
                    '1' => '1',
                    '2' => '2',
                    '3' => '3',
                    '4' => '4',
                    '5' => '5'
                ),
            ),
            'attributes' => array(
                'id' => 'num_ads',
                'value' => '',
                'class' => 'e1 select2-offscreen',
            )
        ));
        $this->add(array(
            'name' => 'is_new',
            'type' => 'Checkbox',
            'attributes' => array(
                'value' => '0',
                'id' => 'is_new',
                'class' => 'e2',
            ),
            'options' => array(
                'value_options' => array(
                    '0' => 'Checkbox',
                    '1' => 'Checkbox',
                ),
            ),
        ));
    }

}