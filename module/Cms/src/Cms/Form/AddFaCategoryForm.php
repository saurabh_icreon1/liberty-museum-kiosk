<?php

/**
 * This is used for add famous arrival category
 * @package    Cms
 * @author     Icreon Tech -AS.
 */

namespace Cms\Form;

use Zend\Form\Form;

class AddFaCategoryForm extends Form {

    public function __construct($name = null) {
        parent::__construct('categoryForm');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'name',
            ),
        ));
        $this->add(array(
            'name' => 'Image',
            'attributes' => array(
                'type' => 'file',
                'id' => 'Image',
            ),
        ));
         $this->add(array(
            'name' => 'img_name',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'img_name',
            ),
        ));
          $this->add(array(
            'name' => 'id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'id',
            ),
        ));
        $this->add(array(
            'name' => 'old_image',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'old_image',
            ),
        ));
        $this->add(array(
            'name' => 'save',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'id' => 'savebutton',
                'class' => 'save-btn'
            ),
        ));
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'id' => 'cancel',
                'class' => 'cancel-btn',
                'value' => 'Cancel',
                'onclick' => 'parent.$.colorbox.close();'
            ),
        ));
    }

}
