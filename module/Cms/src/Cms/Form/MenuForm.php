<?php
/**
 * This is used for save menu form.
 * @package    Cms
 * @author     Icreon Tech -AS.
 */
namespace Cms\Form;

use Zend\Form\Form;

class MenuForm extends Form {

    public function __construct($name = null) {
        parent::__construct('menuForm');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'save',
            'attributes' => array(
                'type' => 'button',
                'id' => 'save',
                'class' => 'save-btn m-r-10',
                'value' => 'Save'
            ),
        ));
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'id' => 'cancel',
                'class' => 'cancel-btn',
                'value' => 'Cancel',
                'onclick' => 'parent.$.colorbox.close();'
            ),
        ));
    }

}