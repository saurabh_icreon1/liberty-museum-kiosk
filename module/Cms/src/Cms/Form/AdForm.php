<?php

/**
 * This form is used to create and modify ads page 
 * @package    CMS
 * @author     Icreon Tech - NS
 */

namespace Cms\Form;

use Zend\Form\Form;

class AdForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('create-Ads');
        $this->setAttribute('id', 'add_ads');
        $this->setAttribute('name', 'add_ads');
        $this->setAttribute('method', 'post');


        $this->add(array(
            'name' => 'ad_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'ad_id',
                'value' => '0'
            )
        ));

        $this->add(array(
            'name' => 'upload_file_name',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'upload_file_name'
            )
        ));
        
       $this->add(array(
            'name' => 'old_upload_file_name',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'old_upload_file_name'
            )
        )); 


        $this->add(array(
            'name' => 'ad_title',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ad_title',
                'maxlength' => 50
            )
        ));

        $this->add(array(
            'name' => 'page_url',
            'attributes' => array(
                'type' => 'text',
                'id' => 'page_url',
                'maxlength' => '255',
                'class' => 'width-130 m-l-5'
            )
        ));


        $this->add(array(
            'type' => 'Select',
            'name' => 'ad_zone_id',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
                'disable_inarray_validator' => true
            ),
            'attributes' => array(
                'id' => 'ad_zone_id',
                'class' => 'e1 select2-offscreen',
                'value' => ''
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'ad_type_id',
            'options' => array(
                'value_options' => array(
                    '' => 'Select'
                ),
                'disable_inarray_validator' => true
            ),
            'attributes' => array(
                'id' => 'ad_type_id',
                'class' => 'e1 select2-offscreen',
                'value' => ''
            )
        ));


        $this->add(array(
            'type' => 'file',
            'name' => 'image_ad',
            'attributes' => array(
                'id' => 'image_ad'
            )
        ));

        $this->add(array(
            'type' => 'text',
            'name' => 'image_ad_url',
            'attributes' => array(
                'id' => 'image_ad_url',
                'maxlength' => '1000'
            )
        ));

        $this->add(array(
            'name' => 'is_new_window',
            'type' => 'Checkbox',
            'attributes' => array(
                'value' => '0',
                'class' => 'checkbox'
            ),
            'options' => array(
                'value_options' => array(
                    '0' => 'Checkbox',
                    '1' => 'Checkbox',
                ),
            ),
        ));


        $this->add(array(
            'name' => 'text_ad',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'text_ad',
                'class' => 'width-300'
            )
        ));


        $this->add(array(
            'name' => 'activate_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'activate_date'
            )
        ));

        $this->add(array(
            'name' => 'end_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'end_date'
            )
        ));

        $this->add(array(
            'name' => 'activate_time',
            'attributes' => array(
                'type' => 'text',
                'id' => 'activate_time'
            )
        ));

        $this->add(array(
            'name' => 'end_time',
            'attributes' => array(
                'type' => 'text',
                'id' => 'end_time'
            )
        ));

        $this->add(array(
            'name' => 'description',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'description',
                'class' => 'width-300'
            )
        ));


        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_active',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'is_active',
                'class' => 'checkbox e2'
            ),
        ));
        
        $this->add(array(
            'name' => 'savebutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'id' => 'savebutton',
                'class' => 'save-btn m-l-10'
            ),
        ));

        $this->add(array(
            'name' => 'updatebutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Update',
                'id' => 'updatebutton',
                'class' => 'save-btn m-l-10'
            ),
        ));

        $this->add(array(
            'name' => 'cancelbutton',
            'attributes' => array(
                'type' => 'button',
                'value' => 'CANCEL',
                'id' => 'cancelbutton',
                'class' => 'cancel-btn m-l-10',
                'onclick' => 'window.location.href="/crm-ads"'
            ),
        ));
    }

}