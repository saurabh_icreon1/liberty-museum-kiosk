<?php

/**
 * This form is used to create and modify home banner page 
 * @package    CMS
 * @author     Icreon Tech - NS
 */

namespace Cms\Form;

use Zend\Form\Form;

class HomeSliderForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('create-Banner');
        $this->setAttribute('id', 'add_banner');
        $this->setAttribute('name', 'add_banner');
        $this->setAttribute('method', 'post');


        $this->add(array(
            'name' => 'slider_image_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'slider_image_id',
                'value' => ''
            )
        ));
        
        $this->add(array(
            'name' => 'image_title',
            'attributes' => array(
                'type' => 'text',
                'id' => 'image_title',
                'maxlength' => 200
            )
        )); 
        
        $this->add(array(
            'type' => 'file',
            'name' => 'image_name_file',
            'attributes' => array(
                'id' => 'image_name_file'
            )
        ));
        
        $this->add(array(
            'name' => 'image_name',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'image_name'
            )
        ));
        
       $this->add(array(
            'name' => 'old_image_name',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'old_image_name'
            )
        ));

        $this->add(array(
            'name' => 'image_description',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'image_description',
                'maxlength' => 500,
                'class' => 'width-300'
            )
        ));

        $this->add(array(
            'type' => 'text',
            'name' => 'image_link',
            'attributes' => array(
                'id' => 'image_link',
                'maxlength' => 500
            )
        ));
               
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'link_open',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'link_open',
                'class' => 'checkbox e2'
            ),
        ));

       $this->add(array(
            'type' => 'text',
            'name' => 'image_order',
            'attributes' => array(
                'id' => 'image_order',
                'maxlength' => 11
            )
        ));
            
      $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_active',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'is_active',
                'class' => 'checkbox e2'
            ),
        ));
      

        $this->add(array(
            'name' => 'savebutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'id' => 'savebutton',
                'class' => 'save-btn m-l-10'
            ),
        ));

        $this->add(array(
            'name' => 'updatebutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Update',
                'id' => 'updatebutton',
                'class' => 'save-btn m-l-10'
            ),
        ));

        $this->add(array(
            'name' => 'cancelbutton',
            'attributes' => array(
                'type' => 'button',
                'value' => 'CANCEL',
                'id' => 'cancelbutton',
                'class' => 'cancel-btn m-l-10',
                'onclick' => 'window.location.href="/crm-home-slider"'
            ),
        ));
    }

}