<?php 
return array(
    'Cms\Module'                       => __DIR__ . '/Module.php',
    'Cms\Controller\CmsController'   => __DIR__ . '/src/Cms/Controller/CmsController.php',
    'Cms\Model\Cms'            => __DIR__ . '/src/Cms/Model/Cms.php',
    'Cms\Model\CmsTable'                   => __DIR__ . '/src/Cms/Model/CmsTable.php'
);
?>