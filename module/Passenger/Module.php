<?php

/**
 * This is used for Passenger Document module.
 * @package    Document
 * @author     Icreon Tech -SR.
 */

namespace Passenger;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\Validator\AbstractValidator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Passenger\Model\Passenger;
use Passenger\Model\PassengerTable;
use Passenger\Model\Familyhistory;
use Passenger\Model\FamilyhistoryTable;
use Passenger\Model\Document;
use Passenger\Model\DocumentTable;
use Passenger\Model\Manifest;
use Passenger\Model\ManifestTable;
use Passenger\View\Helper\AbsoluteUrl;
use Passenger\Model\Oralhistory;
use Passenger\Model\OralhistoryTable;

class Module implements AutoloaderProviderInterface, ConfigProviderInterface, ViewHelperProviderInterface {

    public function onBootstrap(MvcEvent $e) {
        $translator = $e->getApplication()->getServiceManager()->get('translator');
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
        $translator->addTranslationFile(
                'phpArray', './module/Passenger/languages/en/language.php', 'default', 'en_US'
        );
        $viewModel = $e->getApplication()->getMvcEvent()->getViewModel();
        $sm = $e->getApplication()->getServiceManager();
        $this->_config = $sm->get('Config');
		$viewModel->imageFilePath = $imageFilePath = $this->_config['img_file_path']['path'];
        //AbstractValidator::setDefaultTranslator($translator);
    }

    public function getConfig() {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Get Autoloader Configuration
     * @return array
     */
    public function getAutoloaderConfig() {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    /**
     * Get Service Configuration
     *
     * @return array
     */
    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Passenger\Model\PassengerTable' => function($sm) {
                    $tableGateway = $sm->get('PassengerTableGateway');
                    $table = new PassengerTable($tableGateway);
                    return $table;
                },
                'PassengerTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Passenger($dbAdapter));
                    return new TableGateway('TBLELLISRAW', $dbAdapter, null, $resultSetPrototype);
                },
                'Passenger\Model\FamilyhistoryTable' => function($sm) {
                    $tableGateway = $sm->get('FamilyhistoryTableGateway');
                    $table = new FamilyhistoryTable($tableGateway);
                    return $table;
                },
                'FamilyhistoryTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Passenger($dbAdapter));
                    return new TableGateway('tbl_usr_family_histories', $dbAdapter, null, $resultSetPrototype);
                },
                'Passenger\Model\DocumentTable' => function($sm) {
                    $tableGateway = $sm->get('DocumentTableGateway');
                    $table = new DocumentTable($tableGateway);
                    return $table;
                },
                'DocumentTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Document($dbAdapter));
                    return new TableGateway('TBLELLISRAW', $dbAdapter, null, $resultSetPrototype);
                },
                'Passenger\Model\ManifestTable' => function($sm) {
                    $tableGateway = $sm->get('ManifestTableGateway');
                    $table = new ManifestTable($tableGateway);
                    return $table;
                },
                'ManifestTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Manifest($dbAdapter));
                    return new TableGateway('TBLMANIMAGE', $dbAdapter, null, $resultSetPrototype);
                },
                'Passenger\Model\OralhistoryTable' => function($sm) {
                    $tableGateway = $sm->get('OralhistoryTableGateway');
                    $table = new OralhistoryTable($tableGateway);
                    return $table;
                },
                'OralhistoryTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Passenger($dbAdapter));
                    return new TableGateway('names', $dbAdapter, null, $resultSetPrototype);
                },
                'dbAdapter' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    return $dbAdapter;
                },
            ),
        );
    }

    /**
     * Get View Helper Configuration
     *
     * @return array
     */
    public function getViewHelperConfig() {
        return array(
            'factories' => array(
                // the array key here is the name you will call the view helper by in your view scripts
                'absoluteUrl' => function($sm) {
                    $locator = $sm->getServiceLocator(); // $sm is the view helper manager, so we need to fetch the main service manager
                    return new AbsoluteUrl($locator->get('Request'));
                },
            ),
        );
    }

    /**
     * Get Controller Configuration
     *
     * @return array
     */
    public function getControllerConfig() {
        return array();
    }

    public function loadConfiguration(MvcEvent $e) {
        $application = $e->getApplication();
        $serviceManager = $application->getServiceManager();
        $sharedManager = $application->getEventManager()->getSharedManager();
        $router = $serviceManager->get('router');
        $request = $serviceManager->get('request');

        $matchedRoute = $router->match($request);
        if (null !== $matchedRoute) {
            $sharedManager->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function($e) use ($serviceManager) {
                        $serviceManager->get('ControllerPluginManager')->get('AclPlugin')
                                ->doAuthorization($e);
                    }, 2
            );
        }
    }

    public function loadCommonViewVars(MvcEvent $e) {
        $e->getViewModel()->setVariables(array(
            'auth' => $e->getApplication()->getServiceManager()->get('AuthService')
        ));
    }

}
