<?php

return array(
    'Passenger\Module' => __DIR__ . '/Module.php',
    'Passenger\Controller\PassengerController' => __DIR__ . '/src/Passenger/Controller/PassengerController.php',
    'Passenger\Model\Passenger' => __DIR__ . '/src/Passenger/Model/Passenger.php',
    'Passenger\Model\PassengerTable' => __DIR__ . '/src/Passenger/Model/PassengerTable.php',
    'Passenger\Model\Familyhistory' => __DIR__ . '/src/Passenger/Model/Familyhistory.php',
    'Passenger\Model\FamilyhistoryTable' => __DIR__ . '/src/Passenger/Model/FamilyhistoryTable.php',
);
?>