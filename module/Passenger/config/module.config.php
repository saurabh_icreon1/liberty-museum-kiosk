<?php

/**
 * This is used for Passenger Document module.
 * @package    Document
 * @author     Icreon Tech -SR.
 */
return array(
    'controllers' => array(
        'invokables' => array(
            'Passenger\Controller\Passenger' => 'Passenger\Controller\PassengerController',
            'Passenger\Controller\Document' => 'Passenger\Controller\DocumentController',
            'Passenger\Controller\Manifest' => 'Passenger\Controller\ManifestController',
            'Passenger\Controller\Familyhistory' => 'Passenger\Controller\FamilyhistoryController',
            'Passenger\Controller\Oralhistory' => 'Passenger\Controller\OralhistoryController',
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'passengerdetails' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/passenger-details[/:id][/:mode][/:manifest_file]',
                    'defaults' => array(
                        'controller' => 'Passenger\Controller\Passenger',
                        'action' => 'passengerDetails',
                    ),
                ),
            ),
            'passenger' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/passenger[/:searchId]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'index',
                    ),
                ),
            ),
            'getSearchPassenger' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-search-passenger',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'getSearchPassenger',
                    ),
                ),
            ),
            'clearSearchPassengerSession' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/clear-search-passenger-session',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'clearSearchPassengerSession',
                    ),
                ),
            ),
            /*
              'getSearchPassengerlist' => array(
              'type' => 'segment',
              'options' => array(
              'route' => '/passenger-resultzzzzzzzzzz',
              'defaults' => array(
              '__NAMESPACE__' => 'Passenger\Controller',
              'controller' => 'Passenger',
              'action' => 'getSearchPassenger',
              ),
              ),
              ), */
            'passengerResult' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/passenger-result[/:search_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'passengerResult',
                    ),
                ),
            ),
            'shipResult' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/ship-result[/:ship_name][/:searchId]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'shipResult',
                    ),
                ),
            ),
            'shipsearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/ship[/:ship_name][/:searchId]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'getSearchShip',
                    ),
                ),
            ),
            'shiplist' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/ship-list',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'shipList',
                    ),
                ),
            ),
            'getdepartureport' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-departure-port',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'getDeparturePort',
                    ),
                ),
            ),
            'getship' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-ship',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'getShip',
                    ),
                ),
            ),
            'getsavepassengersearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-save-passenger-search',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'getSavePassengerSearch',
                    ),
                ),
            ),
            'deletesearchpassenger' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-search-passenger',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'deleteSearchPassenger',
                    ),
                ),
            ),
            'savesearchpassenger' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/save-search-passenger',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'saveSearchPassenger',
                    ),
                ),
            ),
            'getsaveshipsearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-save-ship-search',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'getSaveShipSearch',
                    ),
                ),
            ),
            'deletesearchship' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-search-ship',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'deleteSearchShip',
                    ),
                ),
            ),
            'savesearchship' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/save-search-ship',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'saveSearchShip',
                    ),
                ),
            ),
            'crmpassenger' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-passenger',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'crmPassenger',
                    ),
                ),
            ),
            'getcrmsearchpassenger' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-search-passenger',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getCrmSearchPassenger',
                    ),
                ),
            ),
            'savecrmsearchpassenger' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/save-crm-search-passenger',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'saveCrmSearchPassenger',
                    ),
                ),
            ),
            'getcrmsearchpassengersavedparam' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-search-passenger-saved-param',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getCrmSearchPassengerSavedParam',
                    ),
                ),
            ),
            'getsavedcrmsearchselect' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-saved-crm-search-select',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getSavedCrmSearchSelect',
                    ),
                ),
            ),
            'deletecrmsearchpassenger' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-search-passenger',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'deleteCrmSearchPassenger',
                    ),
                ),
            ),
            'viewCrmPassenger' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-crm-passenger[/:id][/:mode]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'viewCrmPassenger'
                    ),
                ),
            ),
            'editpassenger' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-passenger[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'editPassenger'
                    ),
                ),
            ),
            'editcrmpassenger' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-passenger[/:id][/:mode]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'viewCrmPassenger'
                    ),
                ),
            ),
            'editship' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-ship[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'editShip'
                    ),
                ),
            ),
            'crmAnnotationCorrection' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-annotation-correction[/:new_annotation]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'crmAnnotationCorrection'
                    ),
                ),
            ),
            'getCrmSearchAnnotationCorrection' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-annotation-correction',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getCrmSearchAnnotationCorrection'
                    ),
                ),
            ),
            'savecrmannotationsearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/save-crm-annotation-search',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'saveCrmAnnotationSearch'
                    ),
                ),
            ),
            ' getsavedcrmannotationsearchselect' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-saved-crm-annotation-search-select',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getSavedCrmAnnotationSearchSelect'
                    ),
                ),
            ),
            'getcrmsearchannotationsavedparam' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-search-annotation-saved-param',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getCrmSearchAnnotationSavedParam',
                    ),
                ),
            ),
            'deletecrmsearchannotation' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-search-annotation',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'deleteCrmSearchAnnotation',
                    ),
                ),
            ),
            'crmship' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-ship',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'crmShip',
                    ),
                ),
            ),
            'crmshiplist' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-ship-list',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'crmShipList',
                    ),
                ),
            ),
            'savecrmshipsearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/save-crm-ship-search',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'saveCrmShipSearch',
                    ),
                ),
            ),
            'getsavedcrmshipsearchselect' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-saved-crm-ship-search-select',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getSavedCrmShipSearchSelect',
                    ),
                ),
            ),
            'getcrmsearchshipsavedparam' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-search-ship-saved-param',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getCrmSearchShipSavedParam',
                    ),
                ),
            ),
            'getcrmsearchship' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-search-ship',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getCrmSearchShip',
                    ),
                ),
            ),
            'viewcrmship' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-crm-ship[/:id][/:mode][/:ship_name]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'viewCrmShip',
                    ),
                ),
            ),
            'getcrmshiptriplist' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-ship-trip-list[/:id][/:shiplineId]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getCrmShipTripList',
                    ),
                ),
            ),
            'getAssets' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-assets',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getAssets',
                    ),
                ),
            ),
            'listlineships' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/list-lineships[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'listLineships',
                    ),
                ),
            ),
            'getlistlineships' => array(
                'type' => 'segment',
                'options' => array(
                    //'route' => '/get-list-lineships[/:ship_name][/:DATE_ARRIVE]',
                    'route' => '/get-list-lineships[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getListLineships',
                    ),
                ),
            ),
            'shippassenger' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/ship-passenger[/:ship_name][/:DATE_ARRIVE]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'shipPassenger',
                    ),
                ),
            ),
            'getshippassengerpist' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-ship-passenger-list[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getShipPassengerList',
                    ),
                ),
            )
            ,
            'viewcrmshipdetails' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-crm-ship-details[/:id][/:ship_name]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'viewCrmShipDetails',
                    ),
                ),
            ),
            'editTripInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-trip-info',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'editTripInfo',
                    ),
                ),
            ),
            'editcrmshipdetails' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-ship-details[/:id][/:ship_name]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'editCrmShipDetails',
                    ),
                ),
            ),
            'editCrmPassengerDetails' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-passenger-details[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'editCrmPassengerDetails',
                    ),
                ),
            ),
            'editshipDetails' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-ship[/:id][/:mode]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'viewCrmShip'
                    ),
                ),
            ),
            'deleteCrmSearchShip' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-search-ship',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'deleteCrmSearchShip'
                    ),
                ),
            ),
            'getCrmShipline' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-shipline',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getCrmShipline'
                    ),
                ),
            ),
            'getCrmShiplineInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-shipline-information[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getCrmShiplineInfo'
                    ),
                ),
            ),
            'getCrmSearchShipline' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-shipline',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getCrmSearchShipline'
                    ),
                ),
            ),
            'addCrmAnnotationCorrection' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-crm-annotation-correction',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'addCrmAnnotationCorrection'
                    ),
                ),
            ),
            'addCrmAnnotationCorrectionPassenger' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-crm-annotation-correction[/:passenger_id][/:mode]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'addCrmAnnotationCorrection'
                    ),
                ),
            ),
            'getCrmAnnotationCorrectionPassenger' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-annotation-correction-passenger',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getCrmAnnotationCorrectionPassenger'
                    ),
                ),
            ),
            'editCrmShipline' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-shipline[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'editCrmShipline'
                    ),
                ),
            ),
            'saveShiplineSearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/save-shipline-search',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'saveShiplineSearch',
                    ),
                ),
            ),
            'deleteCrmSearchshipline' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-search-shipline',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'deleteCrmSearchshipline',
                    ),
                ),
            ),
            'viewCrmPassengerAnnotation' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-passenger-annotation[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getCrmPassengerAnnotation',
                    ),
                ),
            ),
            'getCrmPassengerAnnotationList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-passenger-Annotation-List[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getCrmPassengerAnnotationList',
                    ),
                ),
            ),
            'getCrmSearchShiplineSavedParam' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-search-shipline-saved-select',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getCrmSearchShiplineSavedParam',
                    ),
                ),
            ),
            'getSavedShiplineSearchSelect' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-saved-shipline-search-select',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getSavedShiplineSearchSelect',
                    ),
                ),
            ),
            'getCrmAnnotationCorrectionInfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-annotation-correction-information[/:id][/:act]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getCrmAnnotationCorrectionInfo',
                    ),
                ),
            ),
            'getCrmBulkSearchPassenger' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-bulk-search-passenger',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getCrmBulkSearchPassenger',
                    ),
                ),
            ),
            'editCrmBulkship' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-bulkship',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'editCrmBulkship',
                    ),
                ),
            ),
            'editCrmAnnotationCorrection' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-annotation-correction[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'addCrmAnnotationCorrection'
                    ),
                ),
            ),
            'showManifestImage' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/show-manifest-image[/:filename][/:arrival_date][/:ship_name][/:passenger_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'showManifestImage'
                    ),
                ),
            ),
            'showManifestMoreImage' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/show-manifest-more-image[/:seriesRoll][/:frame][/:passenger_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'showManifestMoreImage'
                    ),
                ),
            ),
            'deleteAnnotationCorrection' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-annotation-correction',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'deleteAnnotationCorrection'
                    ),
                ),
            ),
            'viewCrmPassengerDetails' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-crm-passenger-details[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'viewCrmPassengerDetails'
                    ),
                ),
            ),
            'getPassenger' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-passenger[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getPassenger'
                    ),
                ),
            ),
            'getPassengerid' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-passenger-id[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getPassengerId'
                    ),
                ),
            ),
            'manifest' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-manifest',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Manifest',
                        'action' => 'getManifest'
                    ),
                ),
            ),
            'view-manifest' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-manifest',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Manifest',
                        'action' => 'viewManifest'
                    ),
                ),
            ),
            'savemanifestsearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/save-manifest-search',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Manifest',
                        'action' => 'saveManifestSearch'
                    ),
                ),
            ),
            'getcrmsearchmanifestinfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-search-manifest-info',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Manifest',
                        'action' => 'getCrmSearchManifestInfo'
                    ),
                ),
            )
            ,
            'crmDownloadShipImage' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-download-ship-image[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'crmDownloadShipImage'
                    ),
                ),
            ),
            'getshipautolist' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-ship-list',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getShipList'
                    ),
                ),
            ),
            'getsavedcrmmanifestsearchselect' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-search-manifest',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Manifest',
                        'action' => 'getCrmSearchManifest',
                    ),
                ),
            ),
            'getSearchManifest' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-search-manifest',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Manifest',
                        'action' => 'getSearchManifest',
                    ),
                ),
            ),
            'viewcrmmanifest' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-crm-manifest[/:frame][/:roll_no][/:mode]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Manifest',
                        'action' => 'viewCrmManifest'
                    ),
                ),
            ),
            'viewCrmManifestDetails' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-crm-manifest-details[/:frame][/:roll_no]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Manifest',
                        'action' => 'viewCrmManifestDetails'
                    ),
                ),
            ),
            'editcrmmanifestdetails' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-manifest-details[/:frame][/:roll_no]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Manifest',
                        'action' => 'editCrmManifestDetails',
                    ),
                ),
            ),
            'editcrmmaifest' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-manifest[/:frame][/:roll_no][/:mode]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Manifest',
                        'action' => 'viewCrmManifest'
                    ),
                ),
            ),
            'deleteCrmSearchManifest' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-search-manifest',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Manifest',
                        'action' => 'deleteCrmSearchManifest'
                    ),
                ),
            ),
            'viewCrmShipline' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/shipline[/:id][/:mode]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'viewCrmShipline',
                    ),
                ),
            ),
            'getCrmSearchBulkPassengerList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-search-passenger-list',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getCrmSearchBulkPassengerList',
                    ),
                ),
            ),
            'getPassengerRoll' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-passenger-roll',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getPassengerRoll',
                    ),
                ),
            ),
            'getRollFrame' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-roll-frame',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getRollFrame',
                    ),
                ),
            ),
            'passengerAnnotation' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/passenger-annotation[/:id][/:ship_name][/:date_arrive][/:annotation_id][/:manifest_file][/:line_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'passengerAnnotation',
                    ),
                ),
            ),
            'passengerRecord' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/passenger-record[/:id][/:ship_name][/:date_arrive][/:annotation_id][/:manifest_file][/:line_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'passengerRecord',
                    ),
                ),
            ),
            'passengerShipManifest' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/passenger-ship-manifest[/:id][/:ship_name][/:date_arrive][/:annotation_id][/:line_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'passengerShipManifest',
                    ),
                ),
            ),
            'passengerShipInformation' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/passenger-ship-information[/:id][/:ship_name][/:date_arrive][/:annotation_id][/:manifest_file][/:line_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'passengerShipInformation',
                    ),
                ),
            ),
            'textPassengerList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/text-passenger-list[/:id][/:ship_name][/:date_arrive][/:annotation_id][/:manifest_file][/:line_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'textPassengerList',
                    ),
                ),
            ),
            'passengerAddAnnotation' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/passenger-add-annotation[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'passengerAddAnnotation',
                    ),
                ),
            ),
            'passengerAddCorrection' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/passenger-add-correction[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'passengerAddCorrection',
                    ),
                ),
            ),
            'deletesavedpassenger' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-saved-passenger',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'deleteSavedPassenger',
                    ),
                ),
            ),
            'deletemanifestsearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-manifest-search',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'deleteManifestSearch',
                    ),
                ),
            ),
            'deletesavedship' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-saved-ship',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'deleteSavedShip',
                    ),
                ),
            ),
            'getTextPassengerList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-text-passenger-list[/:id][/:ship_name][/:date_arrive][/:manifest_file][/:line_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'getTextPassengerList',
                    ),
                ),
            ),
            'passengerSaveToFile' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/passenger-save-to-file[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'passengerSaveToFile',
                    ),
                ),
            ),
            'userfamilyhistory' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user-family-history[/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'getUserFamilyhistory',
                    ),
                ),
            ),
            'deletesearchfamilyhistory' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-search-family-history',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'deleteSearchFamilyhistory',
                    ),
                ),
            ),
            'deleteuserfamilyhistory' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-user-family-history',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'deleteUserFamilyhistory',
                    ),
                ),
            ),
            'shipSaveToFile' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/ship-save-to-file[/:id][/:arrival_date][/:ship_name]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'shipSaveToFile',
                    ),
                ),
            ),
            'getSearchPassengerList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-search-passenger-list',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'getSearchPassengerList',
                    ),
                ),
            ),
            'addcrmfamilyhistory' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/create-crm-family-history[/:contact_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'addCrmFamilyhistory',
                    ),
                ),
            ),
            'getcrmfamilyhistory' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-family-history',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'getCrmFamilyhistory',
                    ),
                ),
            ),
            'associateexistingpassenger' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/associate-existing-passenger',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'associateExistingPassenger',
                    ),
                ),
            ),
            'uploadmainimage' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-main-image',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'uploadMainImage',
                    ),
                ),
            ),
            'uploadimage1' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-image-one',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'uploadImageOne',
                    ),
                ),
            ),
            'uploadimage2' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-image-two',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'uploadImageTwo',
                    ),
                ),
            ),
            'uploadimage3' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-image-three',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'uploadImageThree',
                    ),
                ),
            ),
            'uploadimage4' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-image-four',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'uploadImageFour',
                    ),
                ),
            ),
            'uploadimage5' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/upload-image-five',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'uploadImageFive',
                    ),
                ),
            ),
            'getcrmfamilyhistorydetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-family-history-detail[/:id][/:mode]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'getCrmFamilyHistoryDetail',
                    ),
                ),
            ),
            'getcrmsearchfamilyhistoryinfo' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-search-family-history-info[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'getCrmSearchFamilyHistoryInfo',
                    ),
                ),
            ),
            'editcrmfamilyhistory' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-crm-family-history[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'editCrmFamilyHistory',
                    ),
                ),
            ),
            'addAdditionalImage' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-additional-image',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'addAdditionalImage',
                    ),
                ),
            ),
            'deletecrmfamilyhistory' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-family-history[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'deleteCrmFamilyHistory',
                    ),
                ),
            ),
            'savefamilyhistorysearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/save-family-history-search',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'saveFamilyHistorySearch',
                    ),
                ),
            ),
            'getcrmsearchfamilysavedselect' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-crm-search-family-saved-select',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'getCrmSearchFamilySavedSelect',
                    ),
                ),
            ),
            'deletecrmsearchfamilyhistory' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-search-familyhistory',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'deleteCrmSearchFamilyhistory',
                    ),
                ),
            ),
            'addmorepassenger' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-more-passenger',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'addMorePassenger',
                    ),
                ),
            ),
            'addmoremanualperson' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-more-manual-person',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'addMoreManualFamilyPerson',
                    ),
                ),
            ),
            'updatecrmfamilyhistory' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/update-crm-family-history',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'updateCrmFamilyhistory',
                    ),
                ),
            ),
            'getContactFamilyHistory' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/contact-family-history',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'getContactFamilyHistory',
                    ),
                ),
            ),
            'updatecrmfamilyperson' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/update-crm-family-person',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'updateCrmFamilyPerson',
                    ),
                ),
            ),
            'getContactFamilyHistoryList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/contact-family-history-list[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'getContactFamilyHistoryList',
                    ),
                ),
            ),
            'deleteAdditionalImage' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-additional-image',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'deleteAdditionalImage',
                    ),
                ),
            ),
            'editContactFamilyHistoryBonus' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-contact-family-history-bonus',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'editContactFamilyHistoryBonus',
                    ),
                ),
            ),
            'showPassengerDetailsSummary' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/show-passenger-details-summary[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'showPassengerDetailsSummary',
                    ),
                ),
            ),
            'manifestSaveToFile' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/manifest-save-to-file[/:id][/:date_arrival][/:ship_id][/:ship_name][/:passenger_id][/:data_source]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'manifestSaveToFile',
                    ),
                ),
            ),
            'deletecrmfamilyperson' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-crm-family-person',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'deleteCrmFamilyPerson',
                    ),
                ),
            ),
            'getShipLine' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-ship-line',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'getShipLine',
                    ),
                ),
            ),
            'getShipBuilder' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-ship-builder',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'getShipBuilder',
                    ),
                ),
            ),
            'getSearchShipList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-search-ship-list',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'getSearchShipList',
                    ),
                ),
            ),
            'shipDetails' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/ship-details[/:ship_name][/:date_arrive][/:mode][/:manifest_file][/:line_id]',
                    'defaults' => array(
                        'controller' => 'Passenger\Controller\Passenger',
                        'action' => 'shipDetails',
                    ),
                ),
            ),
            'shipLine' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/ship-line[/:line_id][/:line_name]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'shipLine',
                    ),
                ),
            ),
            'shiplineListMore' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/ship-line-list-more[/:line_id][/:data_source]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'shiplineListMore',
                    ),
                ),
            ),
            'familyhistories' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/family-histories',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'familyHistories',
                    ),
                ),
            ),
            'searchuserfamilyhistory' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/search-user-familyhistory',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'searchUserFamilyhistory',
                    ),
                ),
            ),
            'viewuserfamilyhistory' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-user-familyhistory[/:id][/:contact_name][/:birth_country_id][/:showrequest]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'viewUserFamilyhistory',
                    ),
                ),
            ),
            'sendemailfamilyhistory' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/sendemail-familyhistory[/:family_history_id][/:story_title][/:defaultPermission][/:user_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'sendemailFamilyhistory',
                    ),
                ),
            ),
            'saveuserfamilyhistorysearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/save-user-family-history-search',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'saveUserFamilyHistorySearch',
                    ),
                ),
            ),
            'getusersearchfamilysavedselect' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-user-search-family-saved-select',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'getUserSearchFamilySavedSelect',
                    ),
                ),
            ),
            'associatemorepassenger' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/associate-more-passenger',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'associateMorePassenger',
                    ),
                ),
            ),
            'associatemoremanualperson' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/associate-more-manual-person',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'associateMoreManualFamilyPerson',
                    ),
                ),
            ),
            'adduserfamilyhistory' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/add-user-family-history',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'addUserFamilyHistory',
                    ),
                ),
            ),
            'edituserfamilyhistory' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/edit-user-family-history[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'editUserFamilyHistory',
                    ),
                ),
            ),
            'deleteuserfamilyperson' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-user-family-person',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'deleteUserFamilyPerson',
                    ),
                ),
            ),
            'updateuserfamilyhistory' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/update-user-family-history',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'updateUserFamilyhistory',
                    ),
                ),
            ),
            'viewuserfamilyhistorydetail' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-user-family-history-detail[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'viewUserFamilyHistoryDetail',
                    ),
                ),
            ),
            'deleteusersearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/delete-user-search',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'deleteUserSearch',
                    ),
                ),
            ),
            'getShipArrivalDates' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-ship-arrival-dates',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'getShipArrivalDates',
                    ),
                ),
            ),
            'saveuserhistories' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/save-user-histories',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'saveUserHistories',
                    ),
                ),
            ),
            'moreuserfamilyhistory' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/more-user-familyhistory',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'moreUserFamilyhistory',
                    ),
                ),
            ),
            'oralhistories' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/oral-histories',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Oralhistory',
                        'action' => 'oralHistories',
                    ),
                ),
            ),
            'oralhistorylibrary' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/oral-history-library[/:saved_search_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Oralhistory',
                        'action' => 'oralHistoryLibrary',
                    ),
                ),
            ),
            'oralhistorylist' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/oral-history-list',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Oralhistory',
                        'action' => 'oralHistoryList',
                    ),
                ),
            ),
            'saveoralhistorysearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/save-oral-history-search',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Oralhistory',
                        'action' => 'saveOralHistorySearch',
                    ),
                ),
            ),
            'getusersavedsearchselect' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-user-saved-search-select',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Oralhistory',
                        'action' => 'getUserSavedSearchSelect',
                    ),
                ),
            ),
            'moreoralhistories' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/more-oral-histories',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Oralhistory',
                        'action' => 'moreOralHistories',
                    ),
                ),
            ),
            'getPassengerAnnotationListing' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-passenger-annotation-listing[/:id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'getPassengerAnnotationListing',
                    ),
                ),
            ),
            'emailAnnotationContact' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/email-annotation-contact',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'emailAnnotationContact',
                    ),
                ),
            ),
            'manifestPassenger' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/manifest-passenger[/:fileName]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Manifest',
                        'action' => 'manifestPassenger',
                    ),
                ),
            ),
            'getManifestPassengerList' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-manifest-passenger-list',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Manifest',
                        'action' => 'getManifestPassengerList',
                    ),
                ),
            ),
            'getRelatedProducts' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-related-products[/:product_type_id][/:product]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'getRelatedProducts',
                    ),
                ),
            ),
            'updateBulkPassenger' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/update-bulk-passenger',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'updateBulkPassenger',
                    ),
                ),
            ),
            'updateBulkPassenger' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/update-bulk-passenger',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'updateBulkPassenger',
                    ),
                ),
            ),
            'viewPassenger' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/view-passenger[/:id][/:target]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'passenger',
                        'action' => 'viewPassenger',
                    ),
                ),
            ),
            'check-valid-rollnumber' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/check-valid-rollnumber',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'checkValidRollnumber',
                    ),
                ),
            ),
            'crmannotationduplicatecheck' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/crm-annotation-duplicate-check[/:user_id][/:passenger_id][/:activity_type][/:annotation_correction_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Document',
                        'action' => 'crmAnnotationDuplicateCheck',
                    ),
                ),
            ),
            'oralhistoryaudio' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/oral-history-audio[/:filePath]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Oralhistory',
                        'action' => 'oralHistoryAudio',
                    ),
                ),
            ),
            'oralhistorytext' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/oral-history-text[/:filePath]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Oralhistory',
                        'action' => 'oralHistoryText',
                    ),
                ),
            ),
            'fullpassengersearch' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/full-passenger-search',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'fullPassengerSearch',
                    ),
                ),
            ),
        'showManifestBigImage' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/show-manifest-big-image[/:filename][/:data_source]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'showManifestBigImage'
                    ),
                ),
            ), 
        'getAuthorisedAssociatedFamilyPerson' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-authorised-associated-family-person',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Familyhistory',
                        'action' => 'getAuthorisedAssociatedFamilyPerson'
                    ),
                ),
            ),
            'sendPassengerRecord' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/send-passenger-record',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'sendPassengerRecord',
                    ),
                ),
            ), 
            'sendShipRecord' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/send-ship-record',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'sendShipRecord',
                    ),
                ),
            ),
			'printCertificateSend' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/print-certificate-send',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'printCertificateSend',
                    ),
                ),
            ),
			'getAnnotationLog' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-annotation-log[/:passengerId]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'getAnnotationLog',
                    ),
                ),
            ),
			'getPassengerRecordDetails' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-passenger-record-details',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'getPassengerRecordDetails',
                    ),
                ),
            ),            
			'samplePassengerRecord' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/sample-passenger-record[/:passenger_id]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Passenger\Controller',
                        'controller' => 'Passenger',
                        'action' => 'samplePassengerRecord',
                    ),
                ),
            ),            
        ),
    ),
    'translator' => array(
        'locale' => 'en_US',
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'navigation' => array(
        'default' => array(
            array(
                'label' => 'Passenger',
                'route' => 'crmpassenger',
                'pages' => array(
                    array(
                        'label' => 'Search',
                        'route' => 'crmpassenger',
                        'pages' => array(
                            /* array(
                              'label' => 'View',
                              'route' => 'viewCrmPassenger',
                              'action' => 'viewCrmPassenger',
                              ), */
                            array(
                                'label' => 'View',
                                'route' => 'viewCrmPassengerDetails',
                                'action' => 'viewCrmPassengerDetails',
                            ),
                            array(
                                'label' => 'Edit',
                                'route' => 'editCrmPassengerDetails',
                                'action' => 'editCrmPassengerDetails',
                            )
                            ,
                            array(
                                'label' => 'Annotations & Corrections',
                                'route' => 'viewCrmPassengerAnnotation',
                                'action' => 'getCrmPassengerAnnotation',
                            ),
                            array(
                                'label' => 'Change Log',
                                'route' => 'changelogpassenger',
                                'action' => 'change-log'
                            )
                        ),
                    )
                ),
            ),
            array(
                'label' => 'Ship',
                'route' => 'crmship',
                'pages' => array(
                    array(
                        'label' => 'Search',
                        'route' => 'crmship',
                        'pages' => array(
                            /* array(
                              'label' => 'View',
                              'route' => 'viewCrmPassenger',
                              'action' => 'viewCrmPassenger',
                              ), */
                            array(
                                'label' => 'View',
                                'route' => 'viewcrmshipdetails',
                                'action' => 'viewCrmShipDetails',
                            ),
                            array(
                                'label' => 'Edit',
                                'route' => 'editcrmshipdetails',
                                'action' => 'editCrmShipDetails',
                            ),
                            array(
                                'label' => 'Change Log',
                                'route' => 'changelog',
                                'action' => 'change-log'
                            )
                        ),
                    )
                ),
            ),
            array(
                'label' => 'Annotations & Corrections',
                'route' => 'crmpassenger',
                'pages' => array(
                    array(
                        'label' => 'Search',
                        'route' => 'crmAnnotationCorrection',
                        'pages' => array(
                            array(
                                'label' => 'Create',
                                'route' => 'addCrmAnnotationCorrectionPassenger',
                                'action' => 'addCrmAnnotationCorrection',
                            ),
                            array(
                                'label' => 'Edit',
                                'route' => 'editCrmAnnotationCorrection',
                                'action' => 'addCrmAnnotationCorrection',
                            ),
                            array(
                                'label' => 'Annotations & Corrections',
                                'route' => 'viewCrmPassengerAnnotation',
                                'action' => 'getCrmPassengerAnnotation',
                            )
                        ),
                    )
                ),
            ),
            array(
                'label' => 'Manifest',
                'route' => 'manifest',
                'pages' => array(
                    array(
                        'label' => 'Search',
                        'route' => 'manifest',
                        'pages' => array(
                            array(
                                'label' => 'Change Log',
                                'route' => 'manifestchangelog',
                                'action' => 'change-log'
                            ),
                            array(
                                'label' => 'View',
                                'route' => 'viewCrmManifestDetails',
                                'action' => 'viewCrmManifestDetails',
                            ),
                            array(
                                'label' => 'Edit',
                                'route' => 'editcrmmanifestdetails',
                                'action' => 'editCrmManifestDetails',
                            ),
                            array(
                                'label' => 'Change Log',
                                'route' => 'changelog',
                                'action' => 'change-log'
                            )
                        ),
                    )
                ),
            ),
            array(
                'label' => 'Ship Line',
                'route' => 'getCrmShipline',
                'pages' => array(
                    array(
                        'label' => 'Search',
                        'route' => 'getCrmShipline',
                        'pages' => array(
                            array(
                                'label' => 'View',
                                'route' => 'getCrmShiplineInfo',
                                'action' => 'getCrmShiplineInfo',
                            ),
                            array(
                                'label' => 'Edit',
                                'route' => 'editCrmShipline',
                                'action' => 'editCrmShipline',
                            ),
                            array(
                                'label' => 'Change Log',
                                'route' => 'shiplinechangelog',
                                'action' => 'change-log'
                            )
                        ),
                    )
                ),
            ),
            array(
                'label' => 'Family History',
                'route' => 'getcrmfamilyhistory',
                'pages' => array(
                    array(
                        'label' => 'Search',
                        'route' => 'getcrmfamilyhistory',
                        'pages' => array(
                            array(
                                'label' => 'Create',
                                'route' => 'addcrmfamilyhistory',
                                'action' => 'addCrmFamilyhistory',
                            ),
                            array(
                                'label' => 'View',
                                'route' => 'getcrmsearchfamilyhistoryinfo',
                                'action' => 'getCrmSearchFamilyHistoryInfo',
                            ),
                            array(
                                'label' => 'Edit',
                                'route' => 'editcrmfamilyhistory',
                                'action' => 'editCrmFamilyHistory',
                            ),
                            array(
                                'label' => 'Change Log',
                                'route' => 'familyhistorychangelog',
                                'action' => 'change-log'
                            )
                        )
                    ),
                ),
            ),
        )
    ),
    'PassengerMessages' => array(
        'config' => include __DIR__ . '/../languages/en/language.php',
        'navigation' => 'Zend\Navigation\Service\DefaultNavigationFactory'
    ),
    'records_per_page' => array(
        'numRecPerPage' => 6,
        'numRecPerPageGridView' => 6,
        'numRecPerPageListView' => 20,
    ),
    'service_manager' => array(
        'factories' => array(
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'passenger' => __DIR__ . '/../view'
        ),
    ),
);
