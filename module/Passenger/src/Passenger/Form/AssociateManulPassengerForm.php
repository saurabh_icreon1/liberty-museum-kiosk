<?php

namespace Passenger\Form;

use Zend\Form\Form;

/**
 * This form is used to submit annotation from 
 * @package    Document
 * @author     Icreon Tech - SK
 */
class AssociateManulPassengerForm extends Form {

    public function __construct($name = null) {
        parent::__construct('AssociateManualPassenger');
        $this->setAttribute('method', 'post');
        
        $this->add(array(
            'name' => 'first_name_manual[]',
            'attributes' => array(
                'type' => 'text'
            )
        ));
        $this->add(array(
            'name' => 'last_name_manual[]',
            'attributes' => array(
                'type' => 'text'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'relationship_id_manual[]',
            'options' => array(
                    'value_options' => array(
                    ),
            ),
            'attributes' => array(
                'value' => '',
                 'class' => 'e1',
            )
        ));
        $this->add(array(
            'name' => 'year_of_birth_manual[]',
            'attributes' => array(
                'type' => 'text',
                'class' => 'width-155',
                'maxlength' => '4'
            )
        ));
        $this->add(array(
            'name' => 'country_of_birth_manual[]',
            'attributes' => array(
                'type' => 'text',
                'id' => 'country_of_birth_manual',
                'class' => 'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'country_of_birth_manual_id[]',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'country_of_birth_manual_id',
            )
        ));
        $this->add(array(
            'name' => 'panel_no_manual[]',
            'attributes' => array(
                'type' => 'text'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'birth_country_id_manual[]',
            'options' => array(
                    'value_options' => array(
                    ),
            ),
            'attributes' => array(
                'value' => '',
                 'class' => 'e1',
            )
        ));
    }
}