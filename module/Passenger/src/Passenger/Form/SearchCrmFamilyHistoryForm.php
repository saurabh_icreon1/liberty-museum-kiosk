<?php

namespace Passenger\Form;

use Zend\Form\Form;

/**
 * This form is used to submit annotation from 
 * @package    Document
 * @author     Icreon Tech - SK
 */
class SearchCrmFamilyHistoryForm extends Form {

    public function __construct($name = null) {
        parent::__construct('SearchCrmFamilyHistory');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'first_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'first_name',
                'class' => 'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'last_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_name',
                'class' => 'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'contact_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'contact_name',
                'class' => 'search-icon'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'is_status',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    '2' => 'Published',
                    '1' => 'Unpublished'
                ),
            ),
            'attributes' => array(
                'id' => 'is_status',
                'class' => 'e1 select-w-320'
            )
        ));
        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type' => 'text',
                'id' => 'title'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'is_sharing',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    '1' => 'Yes',
                    '0' => 'No'
                ),
            ),
            'attributes' => array(
                'id' => 'is_sharing',
                'class' => 'e1 select-w-320'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_featured',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'is_featured',
                'class' => 'e2'
            )
        ));
        $this->add(array(
            'name' => 'country',
            'attributes' => array(
                'type' => 'text',
                'id' => 'country',
                'class' => 'search-icon',
            )
        ));
        $this->add(array(
            'name' => 'country_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'country_id'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'added_date_range',
            'attributes' => array(
                'id' => 'added_date_range',
                'class' => 'e1 select-w-320',
                'onChange' => 'showDate(this.id,"date_range_div")'
            )
        ));
        $this->add(array(
            'name' => 'added_date_from',
            'attributes' => array(
                'type' => 'text',
                'class' => 'width-125 cal-icon',
                'id' => 'added_date_from',
            )
        ));
        $this->add(array(
            'name' => 'added_date_to',
            'attributes' => array(
                'type' => 'text',
                'class' => 'width-125 cal-icon',
                'id' => 'added_date_to',
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'saved_searches',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                ),
            ),
            'attributes' => array(
                'id' => 'saved_searches',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));
        $this->add(array(
            'name' => 'searchbutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'SEARCH',
                'id' => 'searchbutton',
                'class' => 'search-btn'
            ),
        ));
    }

}
