<?php

/**
 * This form is used to search shipline for admin
 * @package    Document
 * @author     Icreon Tech - AS
 */

namespace Passenger\Form;

use Zend\Form\Form;

/**
 * This form is used to search shipline for admin
 * @package    Document
 * @author     Icreon Tech - AS
 */
class SearchShipLineForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('search_shipline');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'line_name_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'line_name_id'
            )
        ));
        $this->add(array(
            'name' => 'line_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'line_name'
            )
        ));

        $this->add(array(
            'name' => 'city',
            'attributes' => array(
                'type' => 'text',
                'id' => 'city'
            )
        ));

        $this->add(array(
            'name' => 'start_year_date_from',
            'attributes' => array(
                'type' => 'text',
                'id' => 'start_year_date_from',
                'class' => 'width-124 cal-icon'
            )
        ));

        $this->add(array(
            'name' => 'start_year_date_to',
            'attributes' => array(
                'type' => 'text',
                'id' => 'start_year_date_to',
                'class' => 'width-124 cal-icon'
            )
        ));
        $this->add(array(
            'name' => 'end_year_date_to',
            'attributes' => array(
                'type' => 'text',
                'id' => 'end_year_date_to',
                'class' => 'width-124 cal-icon'
            )
        ));
        $this->add(array(
            'name' => 'end_year_date_from',
            'attributes' => array(
                'type' => 'text',
                'id' => 'end_year_date_from',
                'class' => 'width-124 cal-icon'
            )
        ));


        $this->add(array(
            'type' => 'Text',
            'name' => 'country',
            'options' => array(),
            'attributes' => array(
                'id' => 'country'
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'start_year',
            'options' => array(),
            'attributes' => array(
                'id' => 'start_year',
                'class' => 'e1 select-w-320'
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'end_year',
            'options' => array(),
            'attributes' => array(
                'id' => 'end_year',
                'class' => 'e1 select-w-320'
            )
        ));


        $this->add(array(
            'name' => 'search',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'SEARCH',
                'id' => 'search',
                'class' => 'search-btn',
            ),
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'saved_searches',
            'attributes' => array(
                'id' => 'saved_searches',
                'class' => 'e1 select-w-320'
            )
        ));
    }

}