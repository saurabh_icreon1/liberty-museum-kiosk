<?php

namespace Passenger\Form;

use Zend\Form\Form;

/**
 * This form is used to search oral history 
 * @package    Document
 * @author     Icreon Tech - SK
 */
class OralHistoryForm extends Form {

    public function __construct($name = null) {
        parent::__construct('OralHistory');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'first_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'first_name',
                'placeholder'=>'First Name',
                'class' => 'button'
            )
        ));
        $this->add(array(
            'name' => 'last_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_name',
                'placeholder'=>'Last Name',
                'class' => 'button'
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'type',
            'options' => array(
                
            ),
            'attributes' => array(
                'id' => 'type',
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'country',
            'options' => array(
                    'value_options' => array(
                    ),
            ),
            'attributes' => array(
                'value' => '',
                 'class' => 'e1',
                'id' => 'country'
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'topic',
            'options' => array(
                    'value_options' => array(
                    ),
            ),
            'attributes' => array(
                'value' => '',
                 'class' => 'e1',
                'id' => 'topic'
            )
        ));
        
        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type' => 'text',
                'id' => 'title'
            )
        ));
        
        $this->add(array(
            'name' => 'search',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Search',
                'id' => 'search',
                'class' => 'button'
            ),
        ));
        
        $this->add(array(
            'name' => 'save',
            'attributes' => array(
                'type' => 'button',
                'value' => 'save',
                'id' => 'save',
                'class' => 'button'
            ),
        ));
        
    }

}