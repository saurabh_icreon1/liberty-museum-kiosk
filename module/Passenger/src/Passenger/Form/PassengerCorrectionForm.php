<?php

/**
 * This form is used to submit annotation from 
 * @package    Document
 * @author     Icreon Tech - AS
 */

namespace Passenger\Form;

use Zend\Form\Form;

/**
 * This form is used to submit annotation from 
 * @package    Document
 * @author     Icreon Tech - AS
 */
class PassengerCorrectionForm extends Form {

    public function __construct($name = null) {
        parent::__construct('correction');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'annotation_correction_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'annotation_correction_id'
            )
        ));
        $this->add(array(
            'name' => 'first_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'first_name',
                'placeholder' => 'First Name'
            )
        ));
        $this->add(array(
            'name' => 'last_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_name',
                'placeholder' => 'Last Name'
            )
        ));        
        $this->add(array(
            'name' => 'nationality',
            'attributes' => array(
                'type' => 'text',
                'id' => 'nationality',
                'placeholder' => 'Nationality'
            )
        ));        
        $this->add(array(
            'name' => 'last_residence',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_residence',
                'placeholder' => 'Last Place of Residence'
            )
        ));
        $this->add(array(
            'name' => 'birth_place',
            'attributes' => array(
                'type' => 'text',
                'id' => 'birth_place',
                 'placeholder' => 'Place of Birth'
            )
        ));        
        $this->add(array(
            'name' => 'arrival_age',
            'attributes' => array(
                'type' => 'text',
                'id' => 'arrival_age',
                 'class'=>'wid-70',
                 'placeholder' => 'Age'
            )
        ));  
         $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'gender',
            'attributes' => array(
                'id' => 'gender'
            ),
            'options' => array(
                'value_options' => array(
                ),
            ),
        ));   
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'marital_status',
            'attributes' => array(
                'id' => 'marital_status'
            ),
            'options' => array(
                'value_options' => array(

                ),
            ),
        ));          
       
        $this->add(array(
            'name' => 'arrival_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'arrival_date',
                'class' => 'wid-120 cal-icon',
                'placeholder' => 'Date of Arrival'
            )
        ));
        $this->add(array(
            'name' => 'ship',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ship',
                'placeholder' => 'Ship of Travel'
            )
        ));        
        $this->add(array(
            'name' => 'port_departure',
            'attributes' => array(
                'type' => 'text',
                'id' => 'port_departure',
                'placeholder' => 'Port of Departure'
            )
        ));
        $this->add(array(
            'name' => 'line_number',
            'attributes' => array(
                'type' => 'text',
                'id' => 'line_number',
                'class'=>'wid-70',
                'placeholder' => 'Line No'
            )
        ));        

        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'agreement',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'agreement',
                'class' => 'e2'
            )
        ));

        $this->add(array(
            'name' => 'save',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'save',
                'class' => 'button',
                'value' => 'Submit'
            )
        ));
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'id' => 'cancel',
                'class' => 'button',
                'value' => 'Cancel'
            )
        ));
    }

}