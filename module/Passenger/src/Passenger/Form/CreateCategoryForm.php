<?php

/**
* This is used for category form.
* @package    Category
* @author     Icreon Tech -AP.
*/ 

namespace Passenger\Form;
use Zend\Form\Form;
class CreateCategoryForm extends Form {
    public function __construct($name = null) 
    {
        parent::__construct('create-category');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'category_name',
            'attributes' => array(
                'type'  => 'text',
                'id' => 'category_name'
            ),
        )); 
        $this->add(array(
            'name' => 'description',
            'attributes' => array(
                'type'  => 'text',
                'id' => 'description'
            ),
        )); 
       $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'parent_category',
            'attributes' => array(
                'multiple' => 'multiple',
                'id' => 'parent_category',
                'size' => '4',
                'class' => 'e1 select-w-320'
            )
        ));
        $this->add(array(
            'name' => 'save',
            'attributes' => array(
                'type'  => 'Submit',
                'id' => 'save',
                'class' => 'save-btn',
                'value' => 'Save'
            ),
        )); 
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type'  => 'button',
                'id' => 'cancel',
                'class' => 'cancel-btn m-l-20',
                'value' => 'Cancel'
            ),
        )); 
    }
}