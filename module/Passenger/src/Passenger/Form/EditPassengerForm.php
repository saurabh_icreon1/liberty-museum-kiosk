<?php

/**
 * This form is used to for edit Passenger 
 * @package    Document
 * @author     Icreon Tech - AS
 */

namespace Passenger\Form;

use Zend\Form\Form;

/**
 * This form is used to for edit Passenger 
 * @package    Document
 * @author     Icreon Tech - AS
 */
class EditPassengerForm extends Form {

    public function __construct($name = null) {
        parent::__construct('passenger');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'passenger_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'passenger_id'
            )
        ));
        $this->add(array(
            'name' => 'file_name',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'file_name'
            )
        ));
        $this->add(array(
            'name' => 'data_source',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'data_source'
            )
        ));        
        $this->add(array(
            'name' => 'batch',
            'attributes' => array(
                'type' => 'text',
                'id' => 'batch',
                'readonly' => 'readonly'
            )
        ));

        $this->add(array(
            'name' => 'ref_line',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ref_line'
            )
        ));

        $this->add(array(
            'name' => 'ref_page',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ref_page'
            )
        ));
        $this->add(array(
            'name' => 'print_age',
            'attributes' => array(
                'type' => 'text',
                'id' => 'print_age'
            )
        ));

        $this->add(array(
            'name' => 'nationality',
            'attributes' => array(
                'type' => 'text',
                'id' => 'nationality'
            )
        ));

        $this->add(array(
            'name' => 'age_arrival',
            'attributes' => array(
                'type' => 'text',
                'id' => 'age_arrival',
                'maxlength' => '3'
            )
        ));

        $this->add(array(
            'name' => 'first_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'first_name'
            )
        ));

        $this->add(array(
            'name' => 'last_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_name'
            )
        ));
        $this->add(array(
            'name' => 'residence',
            'attributes' => array(
                'type' => 'text',
                'id' => 'residence'
            )
        ));
        $this->add(array(
            'name' => 'port_of_departure',
            'attributes' => array(
                'type' => 'text',
                'id' => 'port_of_departure'
            )
        ));        
        /*
          $this->add(array(
          'name' => 'ship',
          'attributes' => array(
          'type' => 'text',
          'id' => 'ship'
          )
          )); */
        $this->add(array(
            'name' => 'ship_name_iss',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ship_name_iss'
            )
        ));
        $this->add(array(
            'name' => 'arrival_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'arrival_date',
                'class' => 'cal-icon'
            )
        ));

        $this->add(array(
            'name' => 'birth_year',
            'attributes' => array(
                'type' => 'text',
                'id' => 'birth_year'
            )
        ));
  
          $this->add(array(
          'name' => 'ship_arrival_date',
          'attributes' => array(
          'type' => 'text',
          'id' => 'ship_arrival_date',
          'class' => 'cal-icon'
          )
          ));
  

/*
        $this->add(array(
            'type' => 'Select',
            'name' => 'ship_arrival_date',
            'attributes' => array(
                'id' => 'ship_arrival_date',
                'class' => 'e1 select-w-320',
                'value' => ''
            ),
            'options' => array(
                'disable_inarray_validator' => true
            )
        ));*/

        $this->add(array(
            'name' => 'frame',
            'attributes' => array(
                'type' => 'text',
                'id' => 'frame'
            )
        ));

        $this->add(array(
            'name' => 'roll_number',
            'attributes' => array(
                'type' => 'text',
                'id' => 'roll_number'
            )
        ));

        $this->add(array(
            'name' => 'ethnicity',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ethnicity'
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'series',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    'T715' => 'T715',
                    'M237' => 'M237'
                ),
            ),
            'attributes' => array(
                'id' => 'series',
                'disabled' => 'disabled',
                'class' => 'e1 select-w-320',
                'value' => '',
            )
        ));
        $this->add(array(
            'type' => 'Radio',
            'name' => 'gender',
            'attributes' => array(
                'class' => 'e3 gender'
            ),
            'options' => array(
                'value_options' => array(
                    'M' => 'Male',
                    'F' => 'Female',
                    'U' => 'Unknown',
                ),
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'maritial_status',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    'S' => 'Single',
                    'M' => 'Married',
                    'D' => 'Divorced',
                    'W' => 'Widowed',
                    'U' => 'Unknown'
                ),
            ),
            'attributes' => array(
                'id' => 'maritial_status',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));

        $this->add(array(
            'name' => 'us_citizen',
            'attributes' => array(
                'type' => 'Checkbox',
                'id' => 'us_citizen',
                'value' => 1,
                'class' => 'e2',
            ),
        ));
        $this->add(array(
            'name' => 'crew_member',
            'attributes' => array(
                'type' => 'Checkbox',
                'id' => 'crew_member',
                'value' => 1,
                'class' => 'e2',
            ),
        ));

        $this->add(array(
            'name' => 'ship',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ship',
                'class' => 'search-icon'
            )
        ));

        $this->add(array(
            'name' => 'update',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Update',
                'id' => 'update',
                'class' => 'save-btn'
            ),
        ));
        $this->add(array(
            'name' => 'place_of_birth',
            'attributes' => array(
                'type' => 'text',
                'id' => 'place_of_birth'
            )
        ));

        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Cancel',
                'id' => 'cancel',
                'class' => 'cancel-btn m-l-15'
            ),
        ));
    }

}