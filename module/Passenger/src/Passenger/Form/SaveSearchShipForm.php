<?php

/**
 * This form is used to save search ship for admin
 * @package    Document
 * @author     Icreon Tech - AS
 */

namespace Passenger\Form;

use Zend\Form\Form;

/**
 * This form is used to save search ship for admin
 * @package    Document
 * @author     Icreon Tech - AS
 */
class SaveSearchShipForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('save_search');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'search_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'search_id'
            )
        ));
        $this->add(array(
            'name' => 'search_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'search_name'
            )
        ));

        $this->add(array(
            'name' => 'savebutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'id' => 'savebutton',
                'class' => 'save-btn m-l-5',
            ),
        ));
        $this->add(array(
            'name' => 'deletebutton',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Delete',
                'id' => 'deletebutton',
                'style' => 'display:none;',
                'class' => 'save-btn m-l-15',
            ),
        ));
    }

}