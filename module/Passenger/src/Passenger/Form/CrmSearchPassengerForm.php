<?php

/**
 * This form is used to search passenger for admin
 * @package    Document
 * @author     Icreon Tech - AS
 */

namespace Passenger\Form;

use Zend\Form\Form;

/**
 * This class is used to make the form component
 * @package    Document
 * @author     Icreon Tech - AS
 */
class CrmSearchPassengerForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('search_passenger');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'passenger_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'passenger_id'
            )
        ));

        $this->add(array(
            'name' => 'ship',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ship',
                'class'=>'search-icon'
            )
        ));

        $this->add(array(
            'name' => 'first_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'first_name'
            )
        ));
        $this->add(array(
            'name' => 'last_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_name'
            )
        ));
        $this->add(array(
            'name' => 'frame',
            'attributes' => array(
                'type' => 'text',
                'id' => 'frame'
            )
        ));

        $this->add(array(
            'name' => 'roll_number',
            'attributes' => array(
                'type' => 'text',
                'id' => 'roll_number',
                'class'=>'width-130'
            )
        ));
        $this->add(array(
            'name' => 'dept_port',
            'attributes' => array(
                'type' => 'text',
                'id' => 'dept_port'
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'ethnicity',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'ethnicity',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));
        $this->add(array(
            'name' => 'last_residence',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_residence'
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'series',
            'options' => array(
                'value_options' => array(
                    'T715' => 'T715',
                    'M237' => 'M237'
                ),
            ),
            'attributes' => array(
                'id' => 'series',
                'class' => 'e1 select-w-150',
                'value' => ''
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'arrival_year',
            'options' => array(
                'value_options' => array(
                    '' => 'All',
                    '1879 - 1900' => '1879 - 1900',
                    '1901 - 1910' => '1901 - 1910',
                    '1911 - 1920' => '1911 - 1920',
                    '1921 - 1930' => '1921 - 1930'
                ),
            ),
            'attributes' => array(
                'id' => 'arrival_year',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'day_arrival',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'day_arrival',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'month_arrival',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    '1' => 'January',
                    '2' => 'February',
                    '3' => 'March',
                    '4' => 'April',
                    '5' => 'May',
                    '6' => 'June',
                    '7' => 'July',
                    '8' => 'August',
                    '9' => 'September',
                    '10' => 'October',
                    '11' => 'November',
                    '12' => 'December'
                ),
            ),
            'attributes' => array(
                'id' => 'month_arrival',
                'class' => 'e1 select-w-320'
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'birth_year',
            'options' => array(
                'value_options' => array(
                    '' => 'All',
                    '1680 - 1700' => '1680 - 1700',
                    '1701 - 1720' => '1701 - 1720',
                    '1721 - 1740' => '1721 - 1740',
                    '1741 - 1760' => '1741 - 1760',
                    '1761 - 1780' => '1761 - 1780',
                    '1781 - 1800' => '1781 - 1800',
                    '1801 - 1820' => '1801 - 1820',
                    '1821 - 1840' => '1821 - 1840',
                    '1841 - 1860' => '1841 - 1860',
                    '1861 - 1880' => '1861 - 1880',
                    '1881 - 1900' => '1881 - 1900',
                    '1901 - 1920' => '1901 - 1920',
                    '1921 - 1940' => '1921 - 1940',
                    '1941 - 1960' => '1941 - 1960',
                    '1961 - 1974' => '1961 - 1974'
                ),
            ),
            'attributes' => array(
                'id' => 'birth_year',
                'class' => 'e1 select-w-320'
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'current_age_range',
            'options' => array(
                'value_options' => array(
                    '' => 'All',
                    '90 - 100' => '90 - 100',
                    '101 - 110' => '101 - 110',
                    '111 - 120' => '111 - 120',
                    '121 - 130' => '121 - 130',
                    '131 - 140' => '131 - 140',
                    '141 - 150' => '141 - 150',
                    '151 - 160' => '151 - 160',
                    '161 - 170' => '161 - 170',
                    '171 - 180' => '171 - 180',
                    '181 - 190' => '181 - 190',
                    '191 - 200' => '191 - 200',
                    '211 - 220' => '211 - 220',
                    '221 - 230' => '221 - 230'
                ),
            ),
            'attributes' => array(
                'id' => 'current_age_range',
                'class' => 'e1 select-w-320'
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'age_at_arrival',
            'options' => array(
                'value_options' => array(
                    '' => 'All',
                    '0 - 10' => '0 - 10',
                    '11 - 20' => '11 - 20',
                    '21 - 30' => '21 - 30',
                    '31 - 40' => '31 - 40',
                    '41 - 50' => '41 - 50',
                    '51 - 60' => '51 - 60',
                    '61 - 70' => '61 - 70',
                    '71 - 80' => '71 - 80',
                    '81 - 90' => '81 - 90',
                    '91 - 100' => '91 - 100',
                    '101 - 110' => '101 - 110',
                    '111 - 120' => '111 - 120',
                    '121 - 130' => '121 - 130',
                    '131 - 140' => '131 - 140',
                    '141 - 150' => '141 - 150'
                ),
            ),
            'attributes' => array(
                'id' => 'age_at_arrival',
                'class' => 'e1 select-w-320'
            )
        ));        
        
        $this->add(array(
            'type' => 'Select',
            'name' => 'gender',
            'options' => array(
                'value_options' => array(
                    '' => 'Any',
                    'M' => 'Male',
                    'F' => 'Female',
                    'U' => 'Unknown'
                ),
            ),
            'attributes' => array(
                'id' => 'gender',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'maritial_status',
            'options' => array(
                'value_options' => array(
                    '' => 'Any',
                    'S' => 'Single',
                    'M' => 'Married',
                    'D' => 'Divorced',
                    'W' => 'Widowed',
                    'U' => 'Unknown'
                ),
            ),
            'attributes' => array(
                'id' => 'maritial_status',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'arrival_port',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'arrival_port',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));

        $this->add(array(
            'name' => 'savebutton',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Save',
                'id' => 'savebutton'
            ),
        ));
        $this->add(array(
            'name' => 'place_of_birth',
            'attributes' => array(
                'type' => 'text',
                'id' => 'place_of_birth'
            )
        ));  
        $this->add(array(
            'name' => 'town',
            'attributes' => array(
                'type' => 'text',
                'id' => 'town',
            ),
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'saved_search',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'value' => '',
                'class' => 'e1 select-w-320',
                'id' => 'saved_search',
                'onChange' => 'getSavedCrmSearchResult();'
            ),
        ));
    }

}