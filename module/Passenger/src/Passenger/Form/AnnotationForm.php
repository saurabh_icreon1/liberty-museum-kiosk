<?php

/**
 * This form is used to submit annotation from 
 * @package    Document
 * @author     Icreon Tech - AS
 */

namespace Passenger\Form;

use Zend\Form\Form;

/**
 * This form is used to submit annotation from 
 * @package    Document
 * @author     Icreon Tech - AS
 */
class AnnotationForm extends Form {

    public function __construct($name = null) {
        parent::__construct('annotation');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'contact_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'contact_name',
                'class'=>'search-icon',
                'style'=>'width:300px!important'
            )
        ));
        $this->add(array(
            'name' => 'contact_name_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'contact_name_id',
                
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'type',
            'options' => array(
                'value_options' => array(
                    '1' => 'Annotations',
                    '2' => 'Corrections'
                ),
            ),
            'attributes' => array(
                'value' => '',
                'class' => 'e1 select-w-320',
                'id' => 'type'
            ),
        ));
        $this->add(array(
            'name' => 'first_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'first_name'
            )
        ));
        $this->add(array(
            'name' => 'user_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'user_id'
            )
        ));
        $this->add(array(
            'name' => 'annotation_correction_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'annotation_correction_id'
            )
        ));

        $this->add(array(
            'name' => 'activity_type',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'activity_type'
            )
        ));

        $this->add(array(
            'name' => 'last_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_name'
            )
        ));

        $this->add(array(
            'name' => 'nationality',
            'attributes' => array(
                'type' => 'text',
                'id' => 'nationality'
            )
        ));
        $this->add(array(
            'name' => 'last_residence',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_residence'
            )
        ));
        $this->add(array(
            'name' => 'birth_place',
            'attributes' => array(
                'type' => 'text',
                'id' => 'birth_place'
            )
        ));        
        $this->add(array(
            'name' => 'arrival_age',
            'attributes' => array(
                'type' => 'text',
                'id' => 'arrival_age'
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'gender',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'gender',
                'class' => 'e1 select-w-150',
                'value' => ''
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'marital_status',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'marital_status',
                'class' => 'e1 select-w-150',
                'value' => ''
            )
        ));
        /* $this->add(array(
          'name' => 'gender',
          'attributes' => array(
          'type' => 'text',
          'id' => 'gender'
          )
          ));
          $this->add(array(
          'name' => 'marital_status',
          'attributes' => array(
          'type' => 'text',
          'id' => 'marital_status'
          )
          )); */
        $this->add(array(
            'name' => 'arrival_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'arrival_date',
                'class' => 'cal-icon'
            )
        ));
        $this->add(array(
            'name' => 'ship',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ship',
                'class' => 'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'port_departure',
            'attributes' => array(
                'type' => 'text',
                'id' => 'port_departure'
            )
        ));
        $this->add(array(
            'name' => 'line',
            'attributes' => array(
                'type' => 'text',
                'id' => 'line'
            )
        ));
        $this->add(array(
            'name' => 'birth_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'birth_date',
                'class' => 'cal-icon'
            )
        ));
        $this->add(array(
            'name' => 'death_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'death_date',
                'class' => 'cal-icon'
            )
        ));
        $this->add(array(
            'name' => 'occupation',
            'attributes' => array(
                'type' => 'text',
                'id' => 'occupation'
            )
        ));
        $this->add(array(
            'name' => 'spouse',
            'attributes' => array(
                'type' => 'text',
                'id' => 'spouse'
            )
        ));
        $this->add(array(
            'name' => 'children',
            'attributes' => array(
                'type' => 'text',
                'id' => 'children'
            )
        ));
        $this->add(array(
            'name' => 'us_relatives',
            'attributes' => array(
                'type' => 'text',
                'id' => 'us_relatives'
            )
        ));
        $this->add(array(
            'name' => 'us_residence',
            'attributes' => array(
                'type' => 'text',
                'id' => 'us_residence'
            )
        ));
        $this->add(array(
            'name' => 'military_experience',
            'attributes' => array(
                'type' => 'text',
                'id' => 'military_experience'
            )
        ));
        $this->add(array(
            'name' => 'religious_community',
            'attributes' => array(
                'type' => 'text',
                'id' => 'religious_community'
            )
        ));

        $this->add(array(
            'name' => 'note',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'note'
            )
        ));
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'email_send',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'email_send',
                'class' => 'e2'
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'status',
            'options' => array(
                'value_options' => array(
                    '0' => 'Pending',
                    '1' => 'Completed'
                ),
            ),
            'attributes' => array(
                'id' => 'status',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));

        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'terms',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'terms',
                'class' => 'e2'
            )
        ));
        $this->add(array(
            'name' => 'date_of_marriage',
            'attributes' => array(
                'type' => 'text',
                'id' => 'date_of_marriage',
                'class' => 'cal-icon'
            )
        ));
        $this->add(array(
            'name' => 'parent_names',
            'attributes' => array(
                'type' => 'text',
                'id' => 'parent_names',
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'first_time_in_us',
            'attributes' => array(
                'id' => 'first_time_in_us',
            ),
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'is_reauthorization',
            'options' => array(
                'value_options' => array(
                    '1' => 'No',
                    '0' => 'Yes'
                ),
            ),
            'attributes' => array(
                'id' => 'is_reauthorization',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));


        $this->add(array(
            'name' => 'save',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'save',
                'class' => 'save-btn',
                'value' => 'SAVE'
            )
        ));
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'id' => 'cancel',
                'class' => 'cancel-btn',
                'value' => 'CANCEL'
            )
        ));
    }

}