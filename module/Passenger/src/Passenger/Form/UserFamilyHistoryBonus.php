<?php

namespace Passenger\Form;

use Zend\Form\Form;

/**
 * This form is used to user family history bonus
 * @package    Document
 * @author     Icreon Tech - DT
 */
class UserFamilyHistoryBonus extends Form {

    public function __construct($name = null) {
        parent::__construct('user_family_history_bonus');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'user_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'user_id',
            )
        ));
        $this->add(array(
            'name' => 'max_stories',
            'attributes' => array(
                'type' => 'text',
                'id' => 'max_stories',
                'class' => "width-155"
            )
        ));
        $this->add(array(
            'name' => 'max_story_characters',
            'attributes' => array(
                'type' => 'text',
                'id' => 'max_story_characters',
                'class' => "width-155"
            )
        ));
        $this->add(array(
            'name' => 'max_passengers',
            'attributes' => array(
                'type' => 'text',
                'id' => 'max_passengers',
                'class' => "width-155"
            )
        ));
        $this->add(array(
            'name' => 'max_saved_histories',
            'attributes' => array(
                'type' => 'text',
                'id' => 'max_saved_histories',
                'class' => "width-155"

            )
        ));
        $this->add(array(
            'name' => 'save_user_family_history_bonus',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'UPDATE',
                'id' => 'save_user_family_history_bonus',
                'class' => 'save-btn m-l-10'
            ),
        ));

    }

}