<?php

/**
 * This form is used to search passenger for admin
 * @package    Document
 * @author     Icreon Tech - AS
 */

namespace Passenger\Form;

use Zend\Form\Form;

/**
 * This class is used to make the form component
 * @package    Document
 * @author     Icreon Tech - AS
 */
class CrmSearchBulkPassengerForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('search_passenger');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'search_type',
            'options' => array(
                'value_options' => array(
                    '2' => 'By Series/Roll Number/Frame',
                    '1' => 'By Passenger ID',
                    '3' => 'By Trip',
                ),
            ),
            'attributes' => array(
                'class' => 'e3 search_type',
                'onclick' => 'showSearchOption(this)',
                'value'=>2
            )
        ));

        $this->add(array(
            'name' => 'start_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'start_id'
            )
        ));
        $this->add(array(
            'name' => 'end_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'end_id'
            )
        ));
        $this->add(array(
            'name' => 'ship_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ship_name',
                'class'=>'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'arrival_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'arrival_date',
                'class'=>'cal-icon'
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'series',
            'attributes' => array(
                'id' => 'series',
                'class' => 'e2 select-w-150'
            )
        ));        
        $this->add(array(
            'name' => 'roll',
            'attributes' => array(
                'type' => 'text',
                'id' => 'roll',
                'readonly'=>'readonly',
                'class' => 'width-130'
            )
        ));
        $this->add(array(
            'name' => 'frame',
            'attributes' => array(
                'type' => 'text',
                'id' => 'frame',
                'readonly'=>'readonly'
            )
        ));
        $this->add(array(
            'name' => 'search',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Search',
                'id' => 'search',
                'class' => 'search-btn'
            ),
        ));
    }

}