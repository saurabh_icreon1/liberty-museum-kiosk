<?php

/**
 * This form is used to save search shipline for admin
 * @package    Document
 * @author     Icreon Tech - AS
 */

namespace Passenger\Form;

use Zend\Form\Form;

/**
 * This form is used to save search shipline for admin
 * @package    Document
 * @author     Icreon Tech - AS
 */
class SaveSearchShiplineForm extends Form {

    public function __construct($name = null) {
        parent::__construct('save_search');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'save_search_as',
            'attributes' => array(
                'type' => 'text',
                'id' => 'save_search_as'
            )
        ));


        $this->add(array(
            'name' => 'savesearch',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'SAVE',
                'id' => 'savesearch',
                'class' => 'save-btn m-l-15'
            ),
        ));
        $this->add(array(
            'name' => 'search_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'search_id',
                'class' => 'save-btn m-l-15'
            ),
        ));
        $this->add(array(
            'name' => 'deletebutton',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Delete',
                'id' => 'deletebutton',
                'class' => 'save-btn m-l-15',
                'style' => 'display:none;',
            ),
        ));
    }

}