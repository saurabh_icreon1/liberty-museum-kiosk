<?php

/**
 * This form is used for Seach Manifest.
 * @package    Document
 * @author     Icreon Tech - AP
 */

namespace Passenger\Form;

use Zend\Form\Form;

/**
 * This form is used for Seach Manifest.
 * @package    Document
 * @author     Icreon Tech - AP
 */
class SearchManifestForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('search-manifest');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'frame',
            'attributes' => array(
                'type' => 'text',
                'id' => 'frame'
            )
        ));
        $this->add(array(
            'name' => 'roll_number',
            'attributes' => array(
                'type' => 'text',
                'id' => 'roll_number',
                'class' => 'width-130'
            )
        ));
        $this->add(array(
            'name' => 'file_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'file_name'
            )
        ));
        $this->add(array(
            'type' => 'select',
            'name' => 'series',
            'attributes' => array(
                'id' => 'series',
                'class' => 'e1 select-w-150',
            ),
        ));
        $this->add(array(
            'name' => 'ship_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ship_name',
                'class'=>'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'arrival_from_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'arrival_from_date',
                'class' => 'width-124 cal-icon',
            )
        ));
        $this->add(array(
            'name' => 'arrival_to_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'arrival_to_date',
                'class' => 'width-124 cal-icon',
            )
        ));
        $this->add(array(
            'name' => 'ude_batch_number',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ude_batch_number'
            )
        ));
        $this->add(array(
            'name' => 'search',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Search',
                'id' => 'search',
                'class' => 'search-btn',
            ),
        ));
        $this->add(array(
            'name' => 'update',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Update',
                'id' => 'update',
                'class' => 'save-btn',
            ),
        ));
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Cancel',
                'id' => 'cancel',
                'class' => 'cancel-btn m-l-10',
            ),
        ));
    }

}
