<?php

namespace Passenger\Form;

use Zend\Form\Form;

/**
 * This form is used to submit annotation from 
 * @package    Document
 * @author     Icreon Tech - SK
 */
class SearchUserFamilyHistoryForm extends Form {

    public function __construct($name = null) {
        parent::__construct('SearchUserFamilyHistory');
        $this->setAttribute('method', 'post');
		$this->setAttribute('class', 'right');

        $this->add(array(
            'name' => 'first_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'first_name',
                'class' => 'search-icon'
            )
        ));

        $this->add(array(
            'name' => 'last_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_name',
                'class' => 'search-icon'
            )
        ));

        $this->add(array(
            'name' => 'contact_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'contact_name',
                'placeholder' => 'Name',
                'autocomplete' => 'off'
            )
        ));

        $this->add(array(
            'name' => 'searchbutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Search',
                'id' => 'searchbutton',
                'class' => 'button mar-right',
            ),
        ));
    }

}
