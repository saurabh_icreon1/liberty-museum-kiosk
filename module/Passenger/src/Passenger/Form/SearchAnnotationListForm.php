<?php

/**
 * This form is used to save search passenger for admin
 * @package    Document
 * @author     Icreon Tech - AS
 */

namespace Passenger\Form;

use Zend\Form\Form;

/**
 * This form is used to save search passenger for admin
 * @package    Document
 * @author     Icreon Tech - AS
 */
class SearchAnnotationListForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('annotation_list');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'passenger_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'passenger_name'
            )
        ));

        $this->add(array(
            'name' => 'passenger_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'passenger_id'
            )
        ));

        $this->add(array(
            'name' => 'contact_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'contact_name'
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'type',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    '1' => 'Annotations',
                    '2' => 'Corrections'
                ),
            ),
            'attributes' => array(
                'value' => '',
                'class' => 'e1 select-w-320',
                'id' => 'type'
            ),
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'status',
            'options' => array(
                'value_options' => array(
                    '' => 'All',
                    '0' => 'Pending',
                    '1' => 'Completed',
                ),
            ),
            'attributes' => array(
                'value' => '',
                'class' => 'e1 select-w-320',
                'id' => 'status'
            ),
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'requested_on',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    'choose_date' => 'Choose Date',
                    'this_year' => 'This Year',
                    'this_quarter' => 'This Quarter',
                    'this_month' => 'This Month',
                    'this_week' => 'This Week',
                    'previous_year' => 'Previous Year',
                    'previous_quarter' => 'Previous Quarter',
                    'previous_month' => 'Previous Month',
                    'previous_week' => 'Previous Week',
                ),
            ),
            'attributes' => array(
                'id' => 'requested_on',
                'class' => 'e1 select-w-320',
                'value' => '1'
            )
        ));

        $this->add(array(
            'name' => 'from_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'from_date',
                'class' => 'width-124 cal-icon',
                "readonly" => "readonly"
            )
        ));

        $this->add(array(
            'name' => 'to_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'to_date',
                'class' => 'width-124 cal-icon',
                "readonly" => "readonly"
            )
        ));

        $this->add(array(
            'name' => 'search',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'SEARCH',
                'id' => 'search',
                'class' => 'search-btn',
            ),
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'saved_search',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    'mario_ross' => 'Mario Ross'
                ),
            ),
            'attributes' => array(
                'id' => 'saved_search',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));
    }

}