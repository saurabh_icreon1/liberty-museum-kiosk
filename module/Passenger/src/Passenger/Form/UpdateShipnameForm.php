<?php

/**
* This is used for ship search form.
* @package    Passenger
* @author     Icreon Tech - SK
*/

namespace Passenger\Form; 

use Zend\Form\Element; 
use Zend\Form\Form;


class UpdateShipnameForm extends Form

{ 
    public function __construct($name = null)
    { 
        parent::__construct(''); 
        
        $this->setAttribute('method', 'post'); 
        
        $this->add(array( 
            'name' => 'old_ship_name',
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array(
                "id" => "old_ship_name"
            ), 
            'options' => array( 
            ), 
        )); 
 
        $this->add(array(
            'name' => 'new_ship_name', 
            'type' => 'Zend\Form\Element\Text', 
            'attributes' => array(
                "id" => "new_ship_name"
            ), 
            'options' => array( 
            ), 
        ));

        $this->add(array(
            'name' => 'update',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'UPDATE',
                'id' => 'update',
                'class' => 'save-btn m-l-25'
            ),
        ));

        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'value' => 'CANCEL',
                'id' => 'cancel',
                'class' => 'cancel-btn m-l-20'
            ),
        ));
    } 
} 