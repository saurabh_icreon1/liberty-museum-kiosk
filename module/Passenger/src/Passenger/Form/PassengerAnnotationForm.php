<?php

/**
 * This form is used to submit annotation from 
 * @package    Document
 * @author     Icreon Tech - AS
 */

namespace Passenger\Form;

use Zend\Form\Form;

/**
 * This form is used to submit annotation from 
 * @package    Document
 * @author     Icreon Tech - AS
 */
class PassengerAnnotationForm extends Form {

    public function __construct($name = null) {
        parent::__construct('annotation');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'annotation_correction_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'annotation_correction_id'
            )
        ));
        $this->add(array(
            'name' => 'contact_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'contact_name'
            )
        ));
        $this->add(array(
            'name' => 'contact_name_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'contact_name_id'
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'type',
            'options' => array(
                'value_options' => array(
                    '1' => 'Annotations',
                    '2' => 'Corrections'
                ),
            ),
            'attributes' => array(
                'value' => '',
                'class' => 'e1 select-w-320',
                'id' => 'type'
            ),
        ));
        $this->add(array(
            'name' => 'first_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'first_name',
                'placeholder' => 'First Name'
            )
        ));
        $this->add(array(
            'name' => 'last_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_name',
                'placeholder' => 'Last Name'
            )
        ));
        $this->add(array(
            'name' => 'nationality',
            'attributes' => array(
                'type' => 'text',
                'id' => 'nationality',
                'placeholder' => 'Nationality'
            )
        ));
        $this->add(array(
            'name' => 'last_residence',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_residence',
                'placeholder' => 'Last Place of Residence'
            )
        ));
        $this->add(array(
            'name' => 'birth_place',
            'attributes' => array(
                'type' => 'text',
                'id' => 'birth_place',
                'placeholder' => 'Place of Birth'
            )
        ));        
        $this->add(array(
            'name' => 'arrival_age',
            'attributes' => array(
                'type' => 'text',
                'id' => 'arrival_age',
                'class' => 'wid-70',
                'placeholder' => 'Age'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'gender',
            'attributes' => array(
                'id' => 'gender'
            ),
            'options' => array(
                'value_options' => array(
                ),
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'marital_status',
            'attributes' => array(
                'id' => 'marital_status'
            ),
            'options' => array(
                'value_options' => array(
                ),
            ),
        ));

        $this->add(array(
            'name' => 'arrival_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'arrival_date',
                'class' => 'wid-120 cal-icon',
                'placeholder' => 'Date of Arrival'
            )
        ));
        $this->add(array(
            'name' => 'ship',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ship',
                'placeholder' => 'Ship of Travel'
            )
        ));
        $this->add(array(
            'name' => 'port_departure',
            'attributes' => array(
                'type' => 'text',
                'id' => 'port_departure',
                'placeholder' => 'Port of Departure'
            )
        ));
        $this->add(array(
            'name' => 'line_number',
            'attributes' => array(
                'type' => 'text',
                'id' => 'line_number',
                'class' => 'wid-70',
                'placeholder' => 'Line No'
            )
        ));

        $this->add(array(
            'name' => 'birth_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'birth_date',
                'class' => 'cal-icon',
                'placeholder' => 'Date of Birth'
            )
        ));
        $this->add(array(
            'name' => 'death_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'death_date',
                'class' => 'cal-icon',
                'placeholder' => 'Date of Death'
            )
        ));
        $this->add(array(
            'name' => 'occupation',
            'attributes' => array(
                'type' => 'text',
                'id' => 'occupation',
                'placeholder' => 'Occupation'
            )
        ));
        $this->add(array(
            'name' => 'spouse',
            'attributes' => array(
                'type' => 'text',
                'id' => 'spouse',
                'placeholder' => 'Spouse'
            )
        ));
        $this->add(array(
            'name' => 'children',
            'attributes' => array(
                'type' => 'text',
                'id' => 'children',
                'placeholder' => 'Children'
            )
        ));
        $this->add(array(
            'name' => 'us_relatives',
            'attributes' => array(
                'type' => 'text',
                'id' => 'us_relatives',
                'placeholder' => 'Other Relatives in U.S.'
            )
        ));
        $this->add(array(
            'name' => 'us_residence',
            'attributes' => array(
                'type' => 'text',
                'id' => 'us_residence',
                'placeholder' => 'Where Settled in U.S.'
            )
        ));
        $this->add(array(
            'name' => 'military_experience',
            'attributes' => array(
                'type' => 'text',
                'id' => 'military_experience',
                'placeholder' => 'Military Experience'
            )
        ));
        $this->add(array(
            'name' => 'religious_community',
            'attributes' => array(
                'type' => 'text',
                'id' => 'religious_community',
                'placeholder' => 'Religious Community'
            )
        ));


        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'agreement',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'agreement',
                'class' => 'e2'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'annotation_email',
            'attributes' => array(
                'id' => 'annotation_email',
                'value' => 1
            ),
            'options' => array(
                'value_options' => array(
                    '1' => 'ON',
                    '0' => 'Off',
                ),
            ),
        ));
        $this->add(array(
            'name' => 'date_of_marriage',
            'attributes' => array(
                'type' => 'text',
                'id' => 'date_of_marriage',
                'class' => 'cal-icon',
                'placeholder' => 'Date of Marriage'
            )
        ));
        $this->add(array(
            'name' => 'parent_names',
            'attributes' => array(
                'type' => 'text',
                'id' => 'parent_names',
                'placeholder' => 'Parent\'s Names'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'first_time_in_us',
            'attributes' => array(
                'id' => 'first_time_in_us',
            ),
        ));

        $this->add(array(
            'name' => 'save',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'save',
                'class' => 'button',
                'value' => 'SAVE'
            )
        ));
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'id' => 'cancel',
                'class' => 'button',
                'value' => 'CANCEL'
            )
        ));
    }

}