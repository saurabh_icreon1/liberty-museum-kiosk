<?php

/**
 * This form is used to edit ship details for admin
 * @package    Document
 * @author     Icreon Tech - AS
 */

namespace Passenger\Form;

use Zend\Form\Form;

/**
 * This form is used to edit ship details for admin
 * @package    Document
 * @author     Icreon Tech - AS
 */
class EditShipForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('edit_ship');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'ship_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'ship_id'
            )
        ));
        $this->add(array(
            'name' => 'oldShipName',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'oldShipName'
            )
        ));
        $this->add(array(
            'name' => 'shipline_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'shipline_id'
            )
        ));
        $this->add(array(
            'name' => 'shipline',
            'attributes' => array(
                'type' => 'text',
                'id' => 'shipline'
            )
        ));

        $this->add(array(
            'name' => 'name_when_built',
            'attributes' => array(
                'type' => 'text',
                'id' => 'name_when_built'
            )
        ));

        $this->add(array(
            'name' => 'gross_weight',
            'attributes' => array(
                'type' => 'text',
                'id' => 'gross_weight'
            )
        ));
        $this->add(array(
            'name' => 'length',
            'attributes' => array(
                'type' => 'text',
                'id' => 'length'
            )
        ));

        $this->add(array(
            'name' => 'beam',
            'attributes' => array(
                'type' => 'text',
                'id' => 'beam'
            )
        ));


        $this->add(array(
            'name' => 'num_screws',
            'attributes' => array(
                'type' => 'text',
                'id' => 'num_screws'
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'build_year',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                ),
            ),
            'attributes' => array(
                'id' => 'build_year',
                'class' => 'e1 select-w-320',
            )
        ));
        $this->add(array(
            'name' => 'ship_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ship_name'
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'scrapped_year',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                ),
            ),
            'attributes' => array(
                'id' => 'scrapped_year',
                'class' => 'e1 select-w-320',
            )
        ));
        $this->add(array(
            'name' => 'builder',
            'attributes' => array(
                'type' => 'text',
                'id' => 'builder'
            )
        ));
        $this->add(array(
            'name' => 'build_city',
            'attributes' => array(
                'type' => 'text',
                'id' => 'build_city'
            )
        ));

        $this->add(array(
            'name' => 'build_country',
            'attributes' => array(
                'type' => 'text',
                'id' => 'build_country'
            )
        ));

        $this->add(array(
            'name' => 'service_speed_knots',
            'attributes' => array(
                'type' => 'text',
                'id' => 'service_speed_knots'
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'engine_type',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                ),
            ),
            'attributes' => array(
                'id' => 'engine_type',
                'class' => 'e1 select-w-320',
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'scrapped_month',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'scrapped_month',
                'class' => 'e1 select-w-320',
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'scrapped_day',
            'attributes' => array(
                'id' => 'scrapped_day',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'is_edited_image',
            'options' => array(
                'value' => '1'
            ),
            'attributes' => array(
                'id' => 'is_edited_image',
                'class' => 'e2'
            )
        ));
        $this->add(array(
            'type' => 'text',
            'name' => 'scrapped_country',
            'attributes' => array(
                'id' => 'scrapped_country',
                'value' => ''
            )
        ));

        $this->add(array(
            'name' => 'update',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Update',
                'id' => 'update',
                'class' => 'save-btn m-l-25'
            ),
        ));



        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Cancel',
                'id' => 'cancel',
                'class' => 'cancel-btn m-l-20'
            ),
        ));
    }

}