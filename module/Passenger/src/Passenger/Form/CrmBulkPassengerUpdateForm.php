<?php

/**
 * This form is used to search passenger for admin
 * @package    Document
 * @author     Icreon Tech - AS
 */

namespace Passenger\Form;

use Zend\Form\Form;

/**
 * This class is used to make the form component
 * @package    Document
 * @author     Icreon Tech - AS
 */
class CrmBulkPassengerUpdateForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('update_passenger');
        $this->setAttribute('method', 'post');


        $this->add(array(
            'name' => 'p_roll_no',
            'attributes' => array(
                'type' => 'text',
                'maxlength'=>'15',
                'id' => 'p_roll_no',
                'class'=>'width-200'
            )
        ));
        $this->add(array(
            'name' => 'p_frame',
            'attributes' => array(
                'type' => 'text',
                'maxlength'=>'6',
                'id' => 'p_frame',
                'class'=>'width-200'
            )
        ));
        $this->add(array(
            'name' => 'p_ship_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'p_ship_name',
                'class'=>'width-200 search-icon',
            )
        ));
        $this->add(array(
            'name' => 'p_arrival_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'p_arrival_date',
                'class'=>'cal-icon width-200',
                
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'p_series',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    'T715'=>'T715',
                    'M237'=>'M237'
                ),
            ),
            'attributes' => array(
                'id' => 'p_series',
                'class' => 'e2 select-w-200'

            )
        ));        
        $this->add(array(
            'name' => 'p_last_name',
            'attributes' => array(
                'type' => 'text',
                'maxlength'=>'50',
                'id' => 'p_last_name',
                'class'=>'width-200'
            )
        ));        
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Update',
                'id' => 'submit',
                'class' => 'save-btn'
            ),
        ));
    }

}