<?php

namespace Passenger\Form;

use Zend\Form\Form;

/**
 * This form is used to submit annotation from 
 * @package    Document
 * @author     Icreon Tech - SK
 */
class FamilyHistoryForm extends Form {

    public function __construct($name = null) {
        parent::__construct('FamilyHistory');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'title',
            'attributes' => array(
                'type' => 'text',
                'id' => 'title',
            )
        ));
        $this->add(array(
            'name' => 'contact_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'contact_name',
                'class' => 'search-icon'
            )
        ));
        $this->add(array(
            'name' => 'contact_name_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'contact_name_id'
            )
        ));
        $this->add(array(
            'name' => 'story',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'story',
                'class' => 'myTextEditor width-90'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'is_sharing',
            'options' => array(
                'value_options' => array(
                    '1' => 'Viewable by public',
                    '0' => 'Not Shared',
                ),
            ),
            'attributes' => array(
                'id' => 'is_sharing',
                'value' => '',
                'onClick' => 'javascript:enableFamilyHistory(this.value);',
                'class' => 'e3'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_featured',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'is_featured',
                'class' => 'address-checkbox m-t-10'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'is_notifiy',
            'checked_value' => '1',
            'unchecked_value' => '0',
            'attributes' => array(
                'id' => 'is_notifiy',
                'class' => 'address-checkbox  m-t-10'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'is_email_on',
            'options' => array(
                'value_options' => array(
                    '1' => 'on',
                    '0' => 'off'
                ),
            ),
            'attributes' => array(
                'id' => 'is_email_on',
                'value' => '',
                'class' => 'address-checkbox'
            )
        ));
        $this->add(array(
            'name' => 'main_content',
            'attributes' => array(
                'type' => 'file',
                'value' => 'upload',
                'id' => 'main_content',
                'class' => 'm-l-50'
            ),
        ));
        $this->add(array(
            'name' => 'main_content_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'main_content_id',
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'location_one_video_type',
            'options' => array(
                'value_options' => array(
                    '1' => '',
                ),
            ),
            'attributes' => array(
                'id' => 'location_one_video_type',
                'value' => '',
                'onClick' => 'javascript:enableVideoImage(this.id,\'one\');',
                'class' => 'address-checkbox'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'location_one_image_type',
            'options' => array(
                'value_options' => array(
                    '1' => '',
                ),
            ),
            'attributes' => array(
                'id' => 'location_one_image_type',
                'value' => '',
                'onClick' => 'javascript:enableVideoImage(this.id,\'one\');',
                'class' => 'address-checkbox'
            )
        ));
        $this->add(array(
            'name' => 'location_one_video',
            'attributes' => array(
                'type' => 'text',
                'id' => 'location_one_video',
                'class' => 'width-260',
                'disabled' => 'disabled',
                'placeholder' => 'YouTube/Vimeo video URL'
            )
        ));
        $this->add(array(
            'name' => 'location_one_image',
            'attributes' => array(
                'type' => 'file',
                'value' => 'upload',
                'id' => 'location_one_image',
                'class' => 'width-260',
                'disabled' => 'disabled'
            ),
        ));
        $this->add(array(
            'name' => 'location_one_image_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'location_one_image_id'
            ),
        ));
        $this->add(array(
            'name' => 'location_one_caption',
            'attributes' => array(
                'type' => 'text',
                'id' => 'location_one_caption',
                'placeholder' => 'Caption',
                'class' => 'm-l-50'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'location_two_video_type',
            'options' => array(
                'value_options' => array(
                    '1' => '',
                ),
            ),
            'attributes' => array(
                'id' => 'location_two_video_type',
                'value' => '',
                'onClick' => 'javascript:enableVideoImage(this.id,\'two\');',
                'class' => 'address-checkbox'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'location_two_image_type',
            'options' => array(
                'value_options' => array(
                    '1' => '',
                ),
            ),
            'attributes' => array(
                'id' => 'location_two_image_type',
                'value' => '',
                'onClick' => 'javascript:enableVideoImage(this.id,\'two\');',
                'class' => 'address-checkbox'
            )
        ));
        $this->add(array(
            'name' => 'location_two_video',
            'attributes' => array(
                'type' => 'text',
                'id' => 'location_two_video',
                'class' => 'width-260',
                'disabled' => 'disabled',
                'placeholder' => 'YouTube/Vimeo video URL'
            )
        ));
        $this->add(array(
            'name' => 'location_two_image',
            'attributes' => array(
                'type' => 'file',
                'value' => 'upload',
                'id' => 'location_two_image',
                'class' => 'width-260',
                'disabled' => 'disabled'
            ),
        ));
        $this->add(array(
            'name' => 'location_two_image_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'location_two_image_id',
            ),
        ));
        $this->add(array(
            'name' => 'location_two_caption',
            'attributes' => array(
                'type' => 'text',
                'id' => 'location_two_caption',
                'placeholder' => 'Caption',
                'class' => 'm-l-50'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'location_three_video_type',
            'options' => array(
                'value_options' => array(
                    '1' => '',
                ),
            ),
            'attributes' => array(
                'id' => 'location_three_video_type',
                'value' => '',
                'class' => 'e2',
                'onClick' => 'javascript:enableVideoImage(this.id,\'three\');',
                'class' => 'address-checkbox'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'location_three_image_type',
            'options' => array(
                'value_options' => array(
                    '1' => '',
                ),
            ),
            'attributes' => array(
                'id' => 'location_three_image_type',
                'value' => '',
                'onClick' => 'javascript:enableVideoImage(this.id,\'three\');',
                'class' => 'address-checkbox'
            )
        ));
        $this->add(array(
            'name' => 'location_three_video',
            'attributes' => array(
                'type' => 'text',
                'id' => 'location_three_video',
                'class' => 'width-260',
                'disabled' => 'disabled',
                'placeholder' => 'YouTube/Vimeo video URL'
            )
        ));
        $this->add(array(
            'name' => 'location_three_image',
            'attributes' => array(
                'type' => 'file',
                'value' => 'upload',
                'id' => 'location_three_image',
                'class' => 'width-260',
                'disabled' => 'disabled'
            ),
        ));
        $this->add(array(
            'name' => 'location_three_image_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'location_three_image_id'
            ),
        ));
        $this->add(array(
            'name' => 'location_three_caption',
            'attributes' => array(
                'type' => 'text',
                'id' => 'location_three_caption',
                'placeholder' => 'Caption',
                'class' => 'm-l-50'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'location_four_video_type',
            'options' => array(
                'value_options' => array(
                    '1' => '',
                ),
            ),
            'attributes' => array(
                'id' => 'location_four_video_type',
                'value' => '',
                'onClick' => 'javascript:enableVideoImage(this.id,\'four\');',
                'class' => 'address-checkbox'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'location_four_image_type',
            'options' => array(
                'value_options' => array(
                    '1' => '',
                ),
            ),
            'attributes' => array(
                'id' => 'location_four_image_type',
                'value' => '',
                'onClick' => 'javascript:enableVideoImage(this.id,\'four\');',
                'class' => 'address-checkbox'
            )
        ));
        $this->add(array(
            'name' => 'location_four_video',
            'attributes' => array(
                'type' => 'text',
                'id' => 'location_four_video',
                'class' => 'width-260',
                'disabled' => 'disabled',
                'placeholder' => 'YouTube/Vimeo video URL'
            )
        ));
        $this->add(array(
            'name' => 'location_four_image',
            'attributes' => array(
                'type' => 'file',
                'value' => 'upload',
                'id' => 'location_four_image',
                'class' => 'width-260',
                'disabled' => 'disabled'
            ),
        ));
        $this->add(array(
            'name' => 'location_four_image_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'location_four_image_id'
            ),
        ));
        $this->add(array(
            'name' => 'location_four_caption',
            'attributes' => array(
                'type' => 'text',
                'id' => 'location_four_caption',
                'placeholder' => 'Caption',
                'class' => 'm-l-50'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'location_five_video_type',
            'options' => array(
                'value_options' => array(
                    '1' => '',
                ),
            ),
            'attributes' => array(
                'id' => 'location_five_video_type',
                'value' => '',
                'onClick' => 'javascript:enableVideoImage(this.id,\'five\');',
                'class' => 'address-checkbox'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'location_five_image_type',
            'options' => array(
                'value_options' => array(
                    '1' => '',
                ),
            ),
            'attributes' => array(
                'id' => 'location_five_image_type',
                'value' => '',
                'class' => 'auto',
                'onClick' => 'javascript:enableVideoImage(this.id,\'five\');',
                'class' => 'address-checkbox'
            )
        ));
        $this->add(array(
            'name' => 'location_five_video',
            'attributes' => array(
                'type' => 'text',
                'id' => 'location_five_video',
                'class' => 'width-260',
                'disabled' => 'disabled',
                'placeholder' => 'YouTube/Vimeo video URL'
            )
        ));
        $this->add(array(
            'name' => 'location_five_image',
            'attributes' => array(
                'type' => 'file',
                'value' => 'upload',
                'id' => 'location_five_image',
                'class' => 'width-260',
                'disabled' => 'disabled'
            ),
        ));
        $this->add(array(
            'name' => 'location_five_image_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'location_five_image_id'
            ),
        ));
        $this->add(array(
            'name' => 'location_five_caption',
            'attributes' => array(
                'type' => 'text',
                'id' => 'location_five_caption',
                'placeholder' => 'Caption',
                'class' => 'm-l-50'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'is_in_db',
            'options' => array(
                'value_options' => array(
                    '1' => '',
                ),
            ),
            'attributes' => array(
                'id' => 'is_in_db',
                'value' => '',
                'onClick' => 'javascript:enableAssociation(this.id);',
                'class' => 'address-checkbox'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'is_manual',
            'options' => array(
                'value_options' => array(
                    '1' => '',
                ),
            ),
            'attributes' => array(
                'id' => 'is_manual',
                'value' => '',
                'onClick' => 'javascript:enableAssociation(this.id);',
                'class' => 'address-checkbox'
            )
        ));

        $this->add(array(
            'name' => 'is_status',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'is_status'
            )
        ));
        
        $this->add(array(
            'name' => 'family_content_id[]',
            'attributes' => array(
                'type' => 'hidden',
            )
        ));
        $this->add(array(
            'name' => 'family_history_id',
            'attributes' => array(
                'type' => 'hidden',
            )
        ));
        
        $this->add(array(
            'name' => 'first_name',
            'attributes' => array(
                'type' => 'text'
            )
        ));
        $this->add(array(
            'name' => 'last_name',
            'attributes' => array(
                'type' => 'text'
            )
        ));
        $this->add(array(
            'name' => 'passenger_id',
            'attributes' => array(
                'type' => 'text',                
                'class' => 'search-icon',
                'autocomplete' => 'off'
            )
        ));
        $this->add(array(
            'name' => 'panel_no',
            'attributes' => array(
                'type' => 'text'
            )
        ));
        $this->add(array(
            'name' => 'year_of_birth',
            'attributes' => array(
                'type' => 'text',
                'class' => 'width-155'
            )
        ));
        $this->add(array(
            'name' => 'country_of_birth',
            'attributes' => array(
                'type' => 'text',
                'class' => 'e1 select-w-140'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'relationship_id',
            'options' => array(
                    'value_options' => array(
                    ),
            ),
            'attributes' => array(
                'value' => '',
                 'class' => 'e1',
            )
        ));
        
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'birth_country_id',
            'options' => array(
                    'value_options' => array(
                    ),
            ),
            'attributes' => array(
                'value' => '',
                 'class' => 'e1',
            )
        ));
        
        $this->add(array('type' => 'Zend\Form\Element\Csrf',
            'name' => 'csrf',
            'options' => array(
                'csrf_options' => array(
                    'timeout' => 600
                )
            )
        ));

        $this->add(array(
            'name' => 'crm_user_family_history_submitbutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save',
                'id' => 'crm_user_family_history_submitbutton',
                'class' => 'save-btn',
                'onClick' => 'javascript:addStatus("0");',
            ),
        ));

        $this->add(array(
            'name' => 'crm_user_family_history_save_and_publishbutton',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save and Publish',
                'id' => 'crm_user_family_history_save_and_publishbutton',
                'class' => 'save-btn m-l-10',
                'onClick' => 'javascript:addStatus("1");',
            ),
        ));

        $this->add(array(
            'name' => 'crm_user_family_history_cancel',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Cancel',
                'id' => 'module_permission_cancelbutton',
                'class' => 'cancel-btn m-l-10',
                'onclick' => 'javascript:backToSearch();',
            ),
        )); 
        
    }

}