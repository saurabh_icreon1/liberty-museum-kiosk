<?php

/**
 * This form is used to search passenger for admin
 * @package    Document
 * @author     Icreon Tech - AS
 */

namespace Passenger\Form;

use Zend\Form\Form;

/**
 * This class is used to make the form component
 * @package    Document
 * @author     Icreon Tech - AS
 */
class CrmShipManifestForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('manifest');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'roll_no',
            'attributes' => array(
                'type' => 'text',
//                'maxlength'=>'4',
                'id' => 'roll_no',
                'class' => 'select-w-150',
                'style'=>'width:100px !important'
            )
        ));
        $this->add(array(
            'name' => 'frame_no_max',
            'attributes' => array(
                'type' => 'hidden',
//                'maxlength'=>'4',
                'id' => 'frame_no_max',
                'class' => 'select-w-150'
            )
        )); 
        $this->add(array(
            'name' => 'roll_no_range',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'roll_no_range',
                'class' => 'select-w-150'
            )
        ));        
        $this->add(array(
            'name' => 'roll_no_min',
            'attributes' => array(
                'type' => 'hidden',
                'maxlength'=>'4',
                'id' => 'roll_no_min',
                'class' => 'select-w-150'
            )
        ));
        $this->add(array(
            'name' => 'passengerId',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'passengerId',
                'class' => 'select-w-150'
            )
        ));        
        $this->add(array(
            'name' => 'roll_no_max',
            'attributes' => array(
                'type' => 'hidden',
                'maxlength'=>'4',
                'id' => 'roll_no_max',
                'class' => 'select-w-150'
            )
        ));        
        
        $this->add(array(
            'name' => 'frame_no',
            'attributes' => array(
                'type' => 'text',
                'maxlength'=>'4',
                'id' => 'frame_no',
                'class' => 'select-w-150'
            )
        ));
        $this->add(array(
            'name' => 'roll_no_minus_1',
            'attributes' => array(
                'type' => 'button',
                'id' => 'roll_no_minus_1',
                'value'=>'-1'
            )
        ));
        $this->add(array(
            'name' => 'roll_no_plus_1',
            'attributes' => array(
                'type' => 'button',
                'id' => 'roll_no_plus_1',
                'value'=>'+1'
            )
        ));        
        $this->add(array(
            'name' => 'frame_no_plus_1',
            'attributes' => array(
                'type' => 'button',
                'id' => 'frame_no_plus_1',
                'value'=>'+1'
            )
        ));     
        
        $this->add(array(
            'name' => 'frame_no_plus_2',
            'attributes' => array(
                'type' => 'button',
                'id' => 'frame_no_plus_2',
                'value'=>'+2'
            )
        ));     
        
        $this->add(array(
            'name' => 'frame_no_plus_3',
            'attributes' => array(
                'type' => 'button',
                'id' => 'frame_no_plus_3',
                'value'=>'+3'
            )
        ));     
        
        $this->add(array(
            'name' => 'frame_no_plus_4',
            'attributes' => array(
                'type' => 'button',
                'id' => 'frame_no_plus_4',
                'value'=>'+4'
            )
        ));             
        
        $this->add(array(
            'name' => 'frame_no_minus_1',
            'attributes' => array(
                'type' => 'button',
                'id' => 'frame_no_minus_1',
                'value'=>'-1'
            )
        ));
        
        $this->add(array(
            'name' => 'frame_no_minus_2',
            'attributes' => array(
                'type' => 'button',
                'id' => 'frame_no_minus_2',
                'value'=>'-2'
            )
        ));
        $this->add(array(
            'name' => 'frame_no_minus_3',
            'attributes' => array(
                'type' => 'button',
                'id' => 'frame_no_minus_3',
                'value'=>'-3'
            )
        ));
        $this->add(array(
            'name' => 'frame_no_minus_4',
            'attributes' => array(
                'type' => 'button',
                'id' => 'frame_no_minus_4',
                'value'=>'-4'
            )
        ));
        
        
        $this->add(array(
            'type' => 'Select',
            'name' => 'series_no',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'series_no',
                'class' => ' select-w-150'

            )
        ));        
       
        $this->add(array(
            'name' => 'display',
            'attributes' => array(
                'type' => 'button',
                'id' => 'display',
                'value'=>'Display'
            )
        ));
        
    }

}