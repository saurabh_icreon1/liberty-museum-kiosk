<?php

/**
 * This form is used to edit maifest details for admin
 * @package    Document
 * @author     Icreon Tech - SR
 */

namespace Passenger\Form;

use Zend\Form\Form;

/**
 * This form is used to edit maifest details for admin
 * @package    Document
 * @author     Icreon Tech - SR
 */
class EditManifestForm extends Form {

    public function __construct($name = null) {
        parent::__construct('edit_manifest');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'frame',
            'attributes' => array(
                'type' => 'text',
                'id' => 'frame',
                'maxlength' => '4'
            )
        ));
        $this->add(array(
            'name' => 'roll_number',
            'attributes' => array(
                'type' => 'text',
                'id' => 'roll_number',
                'maxlength' => '10'
            )
        ));
        $this->add(array(
            'name' => 'frame_old',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'frame_old'
            )
        ));
        $this->add(array(
            'name' => 'roll_number_old',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'roll_number_old'
            )
        ));
        $this->add(array(
            'name' => 'series',
            'attributes' => array(
                'type' => 'text',
                'id' => 'series',
                'readonly' => 'readonly'
            )
        ));

        $this->add(array(
            'name' => 'arrival_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'arrival_date',
                'class' => 'cal-icon'
            )
        ));

        $this->add(array(
            'name' => 'ude_batch_number',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ude_batch_number',
                'maxlength' => '8'
            )
        ));
        $this->add(array(
            'name' => 'file_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'file_name',
                'maxlength' => '20'
            )
        ));

        $this->add(array(
            'name' => 'ship',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ship',
                'class' => 'search-icon'
            )
        ));


        $this->add(array(
            'name' => 'update',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Update',
                'id' => 'update',
                'class' => 'save-btn'
            ),
        ));
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'is_edited_image',
            'options' => array(
                'value' => '1'
            ),
            'attributes' => array(
                'id' => 'is_edited_image',
                'class' => 'e2'
            )
        ));
        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'value' => 'Cancel',
                'id' => 'cancel',
                'class' => 'cancel-btn m-l-20'
            ),
        ));
    }

}