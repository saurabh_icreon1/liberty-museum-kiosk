<?php

/**
 * This form is used to edit shipline details for admin
 * @package    Document
 * @author     Icreon Tech - AS
 */

namespace Passenger\Form;

use Zend\Form\Form;

/**
 * This form is used to edit shipline details for admin
 * @package    Document
 * @author     Icreon Tech - AS
 */
class EditShiplineForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('edit_shipline');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'linename_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'linename_id',
                'readonly' => 'readonly'
            )
        ));
        $this->add(array(
            'name' => 'ship_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'ship_id'
            )
        ));
        $this->add(array(
            'name' => 'line_id',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'linename_id',
                'readonly' => 'readonly'
            )
        ));
        $this->add(array(
            'name' => 'name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'name'
            )
        ));

        $this->add(array(
            'name' => 'start_year',
            'attributes' => array(
                'type' => 'text',
                'id' => 'start_year'
            )
        ));

        $this->add(array(
            'name' => 'end_year',
            'attributes' => array(
                'type' => 'text',
                'id' => 'end_year'
            )
        ));
        $this->add(array(
            'name' => 'total_pass',
            'attributes' => array(
                'type' => 'text',
                'id' => 'total_pass'
            )
        ));

        $this->add(array(
            'name' => 'assets_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'assets_id',
                'readonly' => 'readonly'
            )
        ));


        $this->add(array(
            'name' => 'flag',
            'attributes' => array(
                'type' => 'text',
                'id' => 'flag'
            )
        ));
        $this->add(array(
            'name' => 'home_city',
            'attributes' => array(
                'type' => 'text',
                'id' => 'home_city',
            )
        ));
        $this->add(array(
            'name' => 'home_country',
            'attributes' => array(
                'type' => 'text',
                'id' => 'home_country'
            )
        ));


        $this->add(array(
            'name' => 'update',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'UPDATE',
                'id' => 'update',
                'class' => 'save-btn m-l-25'
            ),
        ));



        $this->add(array(
            'name' => 'cancel',
            'attributes' => array(
                'type' => 'button',
                'value' => 'CANCEL',
                'id' => 'cancel',
                'class' => 'cancel-btn m-l-20',
                'onclick' => 'window.location.href="/crm-shipline"'
            ),
        ));
    }

}