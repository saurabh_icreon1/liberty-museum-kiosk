<?php

/**
 * This form is used to search ship for admin
 * @package    Document
 * @author     Icreon Tech - AS
 */

namespace Passenger\Form;

use Zend\Form\Form;

/**
 * This form is used to search ship for admin
 * @package    Document
 * @author     Icreon Tech - AS
 */
class SearchShipForm extends Form {

    public function __construct($name = null) {
        // we want to ignore the name passed
        parent::__construct('search_ship');
        $this->setAttribute('method', 'post');

        $this->add(array(
            'name' => 'ship_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ship_id'
            )
        ));
        $this->add(array(
            'name' => 'ship_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ship_name'
            )
        ));

        $this->add(array(
            'name' => 'builder',
            'attributes' => array(
                'type' => 'text',
                'id' => 'builder'
            )
        ));

        $this->add(array(
            'name' => 'shipline_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'shipline_name'
            )
        ));


        $this->add(array(
            'type' => 'Select',
            'name' => 'engine_type',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    'not_available' => 'Not Available'
                ),
            ),
            'attributes' => array(
                'id' => 'engine_type',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'month_arrival',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                    '1' => 'January',
                    '2' => 'February',
                    '3' => 'March',
                    '4' => 'April',
                    '5' => 'May',
                    '6' => 'June',
                    '7' => 'July',
                    '8' => 'August',
                    '9' => 'September',
                    '10' => 'October',
                    '11' => 'November',
                    '12' => 'December'
                ),
            ),
            'attributes' => array(
                'id' => 'month_arrival',
                'class' => 'e1 select-w-320'
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'day_arrival',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'day_arrival',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'arrival_year',
            'options' => array(
                'value_options' => array(
                    '' => 'All',
                    '1879 - 1900' => '1879 - 1900',
                    '1901 - 1910' => '1901 - 1910',
                    '1911 - 1920' => '1911 - 1920',
                    '1921 - 1930' => '1921 - 1930'
                ),
            ),
            'attributes' => array(
                'id' => 'arrival_year',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'build_year',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                ),
            ),
            'attributes' => array(
                'id' => 'build_year',
                'class' => 'e1 select-w-320',
                'value' => '1'
            )
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'scrap_year',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                ),
            ),
            'attributes' => array(
                'id' => 'scrap_year',
                'class' => 'e1 select-w-320',
                'value' => '1'
            )
        ));

        $this->add(array(
            'name' => 'build_from_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'build_from_date',
                'readonly' => 'readonly',
                'class' => 'width-124 cal-icon'
            )
        ));

        $this->add(array(
            'name' => 'build_to_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'build_to_date',
                'readonly' => 'readonly',
                'class' => 'width-124 cal-icon'
            )
        ));

        $this->add(array(
            'name' => 'scrap_from_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'scrap_from_date',
                'readonly' => 'readonly',
                'class' => 'width-124 cal-icon'
            )
        ));

        $this->add(array(
            'name' => 'scrap_to_date',
            'attributes' => array(
                'type' => 'text',
                'id' => 'scrap_to_date',
                'readonly' => 'readonly',
                'class' => 'width-124 cal-icon'
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'saved_search',
            'options' => array(
                'value_options' => array(
                    '' => 'Select',
                ),
            ),
            'attributes' => array(
                'id' => 'saved_search',
                'class' => 'e1 select-w-320',
                'value' => ''
            )
        ));
        $this->add(array(
            'name' => 'search',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Search',
                'id' => 'search',
                'class' => 'search-btn',
            ),
        ));
    }

}