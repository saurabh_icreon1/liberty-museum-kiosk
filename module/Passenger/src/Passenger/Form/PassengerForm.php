<?php

/**
 * This is used for Passenger search form.
 * @package    Document
 * @author     Icreon Tech -SR.
 */

namespace Passenger\Form;

use Zend\Form\Form;

/**
 * This is used for Passenger search form.
 * @package    Document
 * @author     Icreon Tech -SR.
 */
class PassengerForm extends Form {

    public function __construct($name = null) {
        parent::__construct('passenger');
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', 'passenger-result');
        $this->add(array(
            'name' => 'initital_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'initital_name',
                'placeholder'=>'First Name or Initial'
            ),
        ));
        $this->add(array(
            'name' => 'sort_field',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'sort_field',
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'arrival_port',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'arrival_port',
            ),
        ));        
        
        $this->add(array(
            'name' => 'sort_order',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'sort_order',
            ),
        ));
        $this->add(array(
            'name' => 'record_offset',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'record_offset',
                'value' => '0'
            ),
        ));
        $this->add(array(
            'name' => 'match_type',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'match_type',
                'value' => ''
            ),
        ));
        $this->add(array(
            'name' => 'div_name',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'div_name',
                'value' => ''
            ),
        ));
        $this->add(array(
            'name' => 'search_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'search_name',
                'class'=>'button',
                'placeholder'=>'Save this search'
            ),
        ));
        $this->add(array(
            'name' => 'last_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_name',
                'placeholder'=>'Last Name'
            ),
        ));

        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'search_type1',
            'options' => array(
                'value' => 'exact'
            ),
            'attributes' => array(
                'id' => 'search_type1',
                'onClick' => 'searchPassenger();'
            )
        ));
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'search_type2',
            'options' => array(
                'value' => 'close'
            ),
            'attributes' => array(
                'id' => 'search_type2',
                'onClick' => 'searchPassenger();'
            )
        ));
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'search_type3',
            'options' => array(
                'value' => 'sound'
            ),
            'attributes' => array(
                'id' => 'search_type3',
                'onClick' => 'searchPassenger();'
            )
        ));

        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'search_type4',
            'options' => array(
                'value' => 'alternate'
            ),
            'attributes' => array(
                'id' => 'search_type4',
                'onClick' => 'searchPassenger();'
            )
        ));

        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'search_type5',
            'options' => array(
                'value' => 'contains'
            ),
            'attributes' => array(
                'id' => 'search_type5',
                'onClick' => 'searchPassenger();'
            )
        ));

        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'search_type6',
            'options' => array(
                'value' => 'eqfirst'
            ),
            'attributes' => array(
                'id' => 'search_type6',
                'onClick' => 'searchPassenger();'
            )
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\MultiCheckbox',
            'name' => 'gender',
            'options' => array(
                    'value_options' => array(
                        'male' => array(
                            'label'=>"Male",
                            'label_attributes' => array('class'=>'css-label male','for'=>'male'),
                            'value' => 'M',
                            'attributes' => array(
                                'id' => 'male',
                            ),
                        ),
                        'female' => array(
                            'label'=>'Female',
                            'label_attributes' => array('class'=>'css-label female','for'=>'female'),
                            'value' => 'F',
                            'attributes' => array(
                                'id' => 'female',
                            ),
                        )
                    ),
                )
        ));
        
        
    $this->add(array(
            'type' => 'Zend\Form\Element\MultiCheckbox',
            'name' => 'marital_status',
            'options' => array(
                    'value_options' => array(
                        'single' => array(
                            'label'=>'Single',
                            'label_attributes' => array('class'=>'css-label single','for'=>'single'),
                            'value' => 'S',
                            'attributes' => array(
                                'id' => 'single',
                            ),
                        ),
                        'married' => array(
                            'label'=>'Married',
                            'label_attributes' => array('class'=>'css-label married','for'=>'married'),
                            'value' => 'M',
                            'attributes' => array(
                                'id' => 'married',
                            ),
                        ),
                        'widowed' => array(
                            'label'=>'Widowed',
                            'label_attributes' => array('class'=>'css-label widowed','for'=>'widowed'),
                            'value' => 'W',
                            'attributes' => array(
                                'id' => 'widowed',
                            ),
                        ),
                        'divorced' => array(
                            'label'=>'Divorced',
                            'label_attributes' => array('class'=>'css-label divorced','for'=>'divorced'),
                            'value' => 'D',
                            'attributes' => array(
                                'id' => 'divorced',
                            ),
                        )
                    ),
                )
        ));        
        /*
        $this->add(array(
            'type' => 'Zend\Form\Element\MultiCheckbox',
            'name' => 'marital_status',
            'options' => array(
                'value_options' => array(
                    'S' => ' Single',
                    'M' => ' Married',
                    'W' => ' Widowed',
                    'D' => ' Divorced'
                ),
            ),
            'attributes' => array(
                'onClick' => 'searchPassenger();'
            )
        ));*/
        $this->add(array(
            'name' => 'town',
            'attributes' => array(
                'type' => 'text',
                'id' => 'town',
                'placeholder'=>'Name of Town'
            ),
        ));
         $this->add(array(
            'name' => 'last_residence',
            'attributes' => array(
                'type' => 'text',
                'id' => 'last_residence',
                'placeholder'=>'Last Place of Residence'
            )
        ));
        $this->add(array(
            'name' => 'ship_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ship_name',
                'placeholder'=>'Ship Name'
            ),
        ));
        $this->add(array(
            'name' => 'port_of_departure',
            'attributes' => array(
                'type' => 'text',
                'id' => 'port_of_departure',
                'placeholder'=>'Port of Departure'
            ),
        ));
        $this->add(array(
            'name' => 'birth_year_from',
            'attributes' => array(
                'type' => 'text',
                'id' => 'birth_year_from',
                'readonly' => 'readonly',
            )
        ));
        $this->add(array(
            'name' => 'birth_year_to',
            'attributes' => array(
                'type' => 'text',
                'id' => 'birth_year_to',
                'readonly' => 'readonly',
            )
        ));        

        $this->add(array(
            'name' => 'arrival_age_from',
            'attributes' => array(
                'type' => 'text',
                'id' => 'arrival_age_from',
                'readonly' => 'readonly',
            ),
        ));
        $this->add(array(
            'name' => 'arrival_age_to',
            'attributes' => array(
                'type' => 'text',
                'id' => 'arrival_age_to',
                'readonly' => 'readonly',
            ),
        ));        

        $this->add(array(
            'name' => 'year_of_arrival_from',
            'attributes' => array(
                'type' => 'text',
                'id' => 'year_of_arrival_from',
                'readonly' => 'readonly',
            ),
        ));
        $this->add(array(
            'name' => 'year_of_arrival_to',
            'attributes' => array(
                'type' => 'text',
                'id' => 'year_of_arrival_to',
                'readonly' => 'readonly',
            ),
        ));        
        
        
        $this->add(array(
            'name' => 'month_of_arrival_from',
            'attributes' => array(
                'type' => 'text',
                'id' => 'month_of_arrival_from',
                'readonly' => 'readonly',
            ),
        ));
        $this->add(array(
            'name' => 'month_of_arrival_to',
            'attributes' => array(
                'type' => 'text',
                'id' => 'month_of_arrival_to',
                'readonly' => 'readonly',
            )
        ));        

        $this->add(array(
            'name' => 'day_of_arrival_from',
            'attributes' => array(
                'type' => 'text',
                'id' => 'day_of_arrival_from',
                'readonly' => 'readonly',
            )
        ));        
        
        $this->add(array(
            'name' => 'day_of_arrival_to',
            'attributes' => array(
                'type' => 'text',
                'id' => 'day_of_arrival_to',
                'readonly' => 'readonly',
            )
        ));

        $this->add(array(
            'name' => 'current_age_from',
            'attributes' => array(
                'type' => 'text',
                'id' => 'current_age_from',
                'readonly' => 'readonly',
            )
        ));
        $this->add(array(
            'name' => 'current_age_to',
            'attributes' => array(
                'type' => 'text',
                'id' => 'current_age_to',
                'readonly' => 'readonly',
            )
        ));        


        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'arrival_port',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'arrival_port',
            ),
        ));
        $this->add(array(
            'name' => 'passenger_id',
            'attributes' => array(
                'type' => 'text',
                'id' => 'passenger_id',
                'placeholder'=>'Passenger ID'
            ),
        ));
        $this->add(array(
            'name' => 'first_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'first_name',
                'placeholder'=>'Passenger First Name'
            ),
        ));
        $this->add(array(
            'name' => 'companion_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'companion_name',
                'placeholder'=>'Companion First Name'
            ),
        ));
        $this->add(array(
            'name' => 'place_of_birth',
            'attributes' => array(
                'type' => 'text',
                'id' => 'place_of_birth',
                'placeholder'=>'Place of Birth'
            ),
        ));        
        $this->add(array(
            'name' => 'ethnicity_search',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'ethnicity_search'
            ),
        ));
		$this->add(array(
            'type' => 'Zend\Form\Element\Radio',
            'name' => 'search_type_town',
            'options' => array(
                //'use_hidden_element' => true,
                'value_options' => array(
                             'exact' => 'Exact Match',
                             'startwith' => 'Start With',
                             'soundlike' => 'Sounds Like',
                     ),
            ),
            'attributes' => array(
                //'id' => 'current_status',
                'class' => 'e2',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'submitbutton',
				'value' => 'Search',
                'class' => 'blue-btn next'
            ),
        ));
        $this->add(array(
            'name' => 'reset',
            'attributes' => array(
                'type' => 'reset',
                'value' => 'Reset',
                'id' => 'resetbutton',
                'class' => 'button semibold green'
            ),
        ));
        $this->add(array(
            'name' => 'save',
            'attributes' => array(
                'type' => 'button',
                'value' => '',
                'id' => 'savebutton',
                'title'=>'Save this search',
                'alt'=>'Save this search',
                'class' => 'savesearch button'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\MultiCheckbox',
            'name' => 'ethnicity',
            'options' => array(
            ),
            'attributes' => array(
                'class'=>'ethnicityCls'
            )
        ));
    }

}