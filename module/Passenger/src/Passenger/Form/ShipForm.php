<?php

/**
 * This is used for ship search form.
 * @package    Document
 * @author     Icreon Tech -SR.
 */

namespace Passenger\Form;

use Zend\Form\Form;

/**
 * This is used for ship search form.
 * @package    Document
 * @author     Icreon Tech -SR.
 */
class ShipForm extends Form {

    public function __construct($name = null) {
        parent::__construct('ship');
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '');

        $this->add(array(
            'name' => 'ship_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ship_name',
                'placeholder'=>'Ship Name',
				'class'=>'large-input'
            ),
        ));
        $this->add(array(
            'name' => 'sort_field',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'sort_field',
                'value' => 'field18'
            ),
        ));
        $this->add(array(
            'name' => 'sort_order',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'sort_order',
                'value' => 'desc'
            ),
        ));
        $this->add(array(
            'name' => 'record_offset',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'record_offset',
                'value' => '0'
            ),
        ));
        $this->add(array(
            'name' => 'match_type',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'match_type',
                'value' => '5'
            ),
        ));

        $this->add(array(
            'name' => 'ship_name_org',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ship_name_org'
            ),
        ));
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'search_type1',
            'options' => array(
                'value' => '1'
            ),
            'attributes' => array(
                'id' => 'search_type1',
                'onClick' => 'searchShip();'
            )
        ));
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'search_type2',
            'options' => array(
                'value' => '1'
            ),
            'attributes' => array(
                'id' => 'search_type2',
                'onClick' => 'searchShip();'
            )
        ));
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'search_type3',
            'options' => array(
                'value' => '1'
            ),
            'attributes' => array(
                'id' => 'search_type3',
                'onClick' => 'searchShip();'
            )
        ));
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'search_type4',
            'options' => array(
                'value' => '1'
            ),
            'attributes' => array(
                'id' => 'search_type4',
                'onClick' => 'searchShip();'
            )
        ));
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'search_type5',
            'options' => array(
                'value' => '1'
            ),
            'attributes' => array(
                'id' => 'search_type5',
                'onClick' => 'searchShip();'
            )
        ));
        $this->add(array(
            'type' => 'Checkbox',
            'name' => 'search_type6',
            'options' => array(
                'value' => '1'
            ),
            'attributes' => array(
                'id' => 'search_type6',
                'onClick' => 'searchShip();'
            )
        ));
        $this->add(array(
            'name' => 'search_name',
            'attributes' => array(
                'type' => 'text',
                'id' => 'search_name',
                'placeholder'=>'Save this search'
            ),
        ));

        $this->add(array(
            'name' => 'year_of_arrival',
            'attributes' => array(
                'type' => 'text',
                'id' => 'year_of_arrival',
                'class' => 'input-bg-none',
                'value' => '1879 - 1960'
            ),
        ));
        $this->add(array(
            'name' => 'month_of_arrival',
            'attributes' => array(
                'type' => 'text',
                'id' => 'month_of_arrival',
                'class' => 'input-bg-none',
                'value' => '1 - 12' 
            ),
        ));
        $this->add(array(
            'name' => 'day_of_arrival',
            'attributes' => array(
                'type' => 'text',
                'id' => 'day_of_arrival',
                'class' => 'input-bg-none',
                'value' => '1 - 31' 
            ),
        ));
        
       $this->add(array(
            'name' => 'year_of_arrival_from',
            'attributes' => array(
                'type' => 'text',
                'id' => 'year_of_arrival_from',
                'readonly' => 'readonly',
            ),
        ));
        $this->add(array(
            'name' => 'year_of_arrival_to',
            'attributes' => array(
                'type' => 'text',
                'id' => 'year_of_arrival_to',
                'readonly' => 'readonly',
            ),
        ));        
        
        
        $this->add(array(
            'name' => 'month_of_arrival_from',
            'attributes' => array(
                'type' => 'text',
                'id' => 'month_of_arrival_from',
                'readonly' => 'readonly',
            ),
        ));
        $this->add(array(
            'name' => 'month_of_arrival_to',
            'attributes' => array(
                'type' => 'text',
                'id' => 'month_of_arrival_to',
                'readonly' => 'readonly',
            )
        ));        

        $this->add(array(
            'name' => 'day_of_arrival_from',
            'attributes' => array(
                'type' => 'text',
                'id' => 'day_of_arrival_from',
                'readonly' => 'readonly',
            )
        ));        
        
        $this->add(array(
            'name' => 'day_of_arrival_to',
            'attributes' => array(
                'type' => 'text',
                'id' => 'day_of_arrival_to',
                'readonly' => 'readonly',
            )
        ));        
        
        $this->add(array(
            'name' => 'port_of_departure',
            'attributes' => array(
                'type' => 'text',
                'id' => 'port_of_departure',
                'placeholder'=>'Port of Departure'
            ),
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'arrival_port',
            'options' => array(
                'value_options' => array(
                ),
            ),
            'attributes' => array(
                'id' => 'arrival_port',
            ),
        ));
        $this->add(array(
            'name' => 'ship_line',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ship_line',
                'placeholder'=>'Ship Line'
            ),
        ));
        $this->add(array(
            'name' => 'ship_builder',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ship_builder',
                'placeholder'=>'Ship Builder'
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'submitbutton',
                'class' => 'button',
                'value' => 'Results'
            ),
        ));
        $this->add(array(
            'name' => 'reset',
            'attributes' => array(
                'type' => 'reset',
                'value' => 'Reset',
                'class' => 'button semibold green',
                'id' => 'resetbutton',
            ),
        ));
        $this->add(array(
            'name' => 'div_name',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'div_name',
                'value' => ''
            ),
        ));        
        
        $this->add(array(
            'name' => 'save',
            'attributes' => array(
                'type' => 'button',
                'class' => 'savesearch button',
                'title'=>'Save this search',
                'alt'=>'Save this search',
                'id' => 'savebutton',
            ),
        ));
    }

}