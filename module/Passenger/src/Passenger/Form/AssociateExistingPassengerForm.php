<?php

namespace Passenger\Form;

use Zend\Form\Form;

/**
 * This form is used to submit annotation from 
 * @package    Document
 * @author     Icreon Tech - SK
 */
class AssociateExistingPassengerForm extends Form {

    public function __construct($name = null) {
        parent::__construct('AssociateExistingPassenger');
        $this->setAttribute('method', 'post');
        
        $this->add(array(
            'name' => 'passenger_id_db[]',
            'attributes' => array(
                'type' => 'text',
                'class' => 'search-icon',
                'id' => 'passenger_id',
                'autocomplete' => 'off'
            )
        ));
        $this->add(array(
            'type' => 'Zend\Form\Element\Select',
            'name' => 'relationship_id_db[]',
            'options' => array(
                    'value_options' => array(
                    ),
            ),
            'attributes' => array(
                'value' => '',
                 'class' => 'e1',
            )
        ));
        $this->add(array(
            'name' => 'panel_no_db[]',
            'attributes' => array(
                'type' => 'text',
            )
        ));
    }
}