<?php

/**
 * This is used for Passenger/Document module.
 * @package    Document
 * @author     Icreon Tech -SR.
 */

namespace Passenger\Model;

use Base\Model\BaseModel;
use Passenger\Module;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\Adapter\Adapter;

/**
 * This is Class used for Passenger/Document actions.
 * @package    Document
 * @author     Icreon Tech -SR.
 */
class Manifest extends BaseModel {

    protected $inputFilter;
    protected $searchParam;
    protected $inputFilterCrmSearch;
    protected $adapter;

    /**
     * This is used for dafault initialization of objects.
     * @package    Document
     * @author     Icreon Tech -SR.
     */
    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    /**
     * Function used to exchange the values
     * @author Icreon Tech - SR
     * @return $returnArray
     * @param $data array
     */
    public function exchangeArray($data) {
        
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    /**
     * Function used to pass for input filter
     * @author Icreon Tech - SR
     * @return array
     * @param Array
     */
    public function getInputFilterCrmSearch($filterData) {
        $inputFilterData = array();
        if (is_array($filterData)) {
            foreach ($filterData as $key => $val) {
                $inputFilterData[$key] = trim(addslashes(strip_tags($val)));
            }
        } else {
            $inputFilterData = trim(addslashes(strip_tags($filterData)));
        }
        return $this->inputFilterCrmSearch = $inputFilterData;
    }

    /**
     * Function used to check validation for manifest
     * @author Icreon Tech - SR
     * @return boolean
     * @param Array
     */
    public function getManifestInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'frame',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'FRAME_MSG',
                                    ),
                                ),
                            )
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'roll_number',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'ROLL_NUMBER_MSG',
                                    ),
                                ),
                            )
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'arrival_date',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'ARRIVAL_DATE_MSG',
                                    ),
                                ),
                            )
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'ude_batch_number',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'UDE_BATCH_MSG',
                                    ),
                                ),
                            )
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'file_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'FILE_NAME_MSG',
                                    ),
                                ),
                            )
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'ship',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'SHIP_NAME_MSG',
                                    ),
                                ),
                            )
                        )
                    )));
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * Function used to exchange manifest values (assignment of values)
     * @author Icreon Tech - SR
     * @return $returnArray
     * @param $dataArr array
     */
    public function assignManifestArray($dataArr = array()) {
        $returnArray = array();
        $returnArray['frame'] = isset($dataArr['frame']) ? $dataArr['frame'] : '';
        $returnArray['roll_number'] = isset($dataArr['roll_number']) ? $dataArr['roll_number'] : '';
        $returnArray['file_name'] = isset($dataArr['file_name']) ? $dataArr['file_name'] : '';
        $returnArray['series'] = isset($dataArr['series']) ? $dataArr['series'] : '';
        $returnArray['ship_name'] = isset($dataArr['ship_name']) ? $dataArr['ship_name'] : '';

        if (isset($dataArr['arrival_from_date']) && $dataArr['arrival_from_date'] != '') {
            $returnArray['arrival_from_date'] = $dataArr['arrival_from_date'];
        } else {
            $returnArray['arrival_from_date'] = '';
        }

        if (isset($dataArr['arrival_to_date']) && $dataArr['arrival_to_date'] != '') {
            $returnArray['arrival_to_date'] = $dataArr['arrival_to_date'];
        } else {
            $returnArray['arrival_to_date'] = '';
        }
        $returnArray['ude_batch_number'] = isset($dataArr['ude_batch_number']) ? $dataArr['ude_batch_number'] : '';
        $returnArray['getRecordsCount'] = isset($dataArr['getRecordsCount']) ? $dataArr['getRecordsCount'] : '';
        $returnArray['startIndex'] = isset($dataArr['startIndex']) ? $dataArr['startIndex'] : '';
        $returnArray['recordLimit'] = isset($dataArr['recordLimit']) ? $dataArr['recordLimit'] : '';
        $returnArray['sortField'] = isset($dataArr['sortField']) ? $dataArr['sortField'] : '';
        $returnArray['sortOrder'] = isset($dataArr['sortOrder']) ? $dataArr['sortOrder'] : '';
        return $returnArray;
    }

    /**
     * Function used to exchange manifest values (assignment of values)
     * @author Icreon Tech - SR
     * @return $returnArray
     * @param $data array
     */
    public function exchangeManifestArray($data) {
        $this->frame = (isset($data['frame']) && !empty($data['frame'])) ? $data['frame'] : '';
        $this->roll_number = (isset($data['roll_number']) && !empty($data['roll_number'])) ? $data['roll_number'] : '';
        $this->arrival_date = (isset($data['arrival_date']) && !empty($data['arrival_date'])) ? $data['arrival_date'] : '';
        $this->ude_batch_number = (isset($data['ude_batch_number']) && !empty($data['ude_batch_number'])) ? $data['ude_batch_number'] : '';
        $this->file_name = (isset($data['file_name']) && !empty($data['file_name'])) ? $data['file_name'] : '';
        $this->ship = (isset($data['ship']) && !empty($data['ship'])) ? $data['ship'] : '';
         $this->is_edited_image = (isset($data['is_edited_image']) && !empty($data['is_edited_image'])) ? $data['is_edited_image'] : '';
    }

}