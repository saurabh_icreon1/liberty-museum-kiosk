<?php

/**
 * This is used for FamilyHistory  module.
 * @package    Passenger
 * @author     Icreon Tech -DG.
 */

namespace Passenger\Model;

use Passenger\Module;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\Adapter\Adapter;

/**
 * This is class used for FamilyHistory module.
 * @author     Icreon Tech -DG.
 */
class Familyhistory implements InputFilterAwareInterface {

    protected $inputFilter;
    protected $adapter;

    /**
     * This is used for dafault initialization of objects.
     * @package    Passenger
     * @author     Icreon Tech -DG.
     */
    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    /**
     * Function used to exchange FamilyHistory values (assignment of values)
     * @return null
     * @param $data array
     * @author Icreon Tech - DG
     */
    public function exchangeArray($data) {
        
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }
    
    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                'name' => 'csrf',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'csrf',
                        'options' => array(
                            'messages' => array(
                                'notSame' => 'NOT_MATCH',
                            ),
                        ),
                    ),
                ),
            )));
            $inputFilter->add($factory->createInput(array(
                'name' => 'title',
                'required' => true,
                'filters' => array(
                    array('name' => 'StripTags'),
                    array('name' => 'StringTrim'),
                ),
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                'isEmpty' => 'TITLE_EMPTY',
                            ),
                        ),
                    ),
                ),
            )));
            $inputFilter->add($factory->createInput(array(
                'name' => 'story',
                'required' => true,
                'validators' => array(
                    array(
                        'name' => 'NotEmpty',
                        'options' => array(
                            'messages' => array(
                                'isEmpty' => 'STORY_EMPTY',
                            ),
                        ),
                    ),
                ),
            )));
            $inputFilter->add($factory->createInput(array(
                'name' => 'is_notifiy',
                'required' => false,
            )));
            $inputFilter->add($factory->createInput(array(
                'name' => 'is_email_on',
                'required' => false,
            )));
            $inputFilter->add($factory->createInput(array(
                'name' => 'is_featured',
                'required' => false,
            )));
            $inputFilter->add($factory->createInput(array(
                'name' => 'location_one_video_type',
                'required' => false,
            )));
            $inputFilter->add($factory->createInput(array(
                'name' => 'location_one_image_type',
                'required' => false,
            )));
            $inputFilter->add($factory->createInput(array(
                'name' => 'location_two_video_type',
                'required' => false,
            )));
            $inputFilter->add($factory->createInput(array(
                'name' => 'location_two_image_type',
                'required' => false,
            )));
            $inputFilter->add($factory->createInput(array(
                'name' => 'location_three_video_type',
                'required' => false,
            )));
            $inputFilter->add($factory->createInput(array(
                'name' => 'location_three_image_type',
                'required' => false,
            )));
            $inputFilter->add($factory->createInput(array(
                'name' => 'location_four_video_type',
                'required' => false,
            )));
            $inputFilter->add($factory->createInput(array(
                'name' => 'location_four_image_type',
                'required' => false,
            )));
            $inputFilter->add($factory->createInput(array(
                'name' => 'location_five_video_type',
                'required' => false,
            )));
            $inputFilter->add($factory->createInput(array(
                'name' => 'location_five_image_type',
                'required' => false,
            )));
            $inputFilter->add($factory->createInput(array(
                'name' => 'is_in_db',
                'required' => false,
            )));
            $inputFilter->add($factory->createInput(array(
                'name' => 'is_manual',
                'required' => false,
            )));
            $inputFilter->add($factory->createInput(array(
                'name' => 'relationship_id',
                'required' => false,
            )));
            $inputFilter->add($factory->createInput(array(
                'name' => 'birth_country_id',
                'required' => false,
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }	

    public function getArrayCopy() {
        return get_object_vars($this);
    }
    
    /**
     * Function used to return FamilyHistory search result
     * @return null
     * @param $data array
     * @author Icreon Tech - SK
     */
    public function getFamilyHistorySearchArray($dataArr = array()) {

        $returnArr = array();
		
        if(isset($dataArr['is_shared'])){
            if($dataArr['is_shared']=='0')
                    $returnArr['is_shared'] = 'No';
            else
                    $returnArr['is_shared'] = 'Yes';
        }

        if(isset($dataArr['is_featured'])){
            if($dataArr['is_featured']=='0')
                    $returnArr['is_featured'] = 'No';
            else
                    $returnArr['is_featured'] = 'Yes';
        }

        if(isset($dataArr['is_status'])){
            if($dataArr['is_status']=='1')
                    $returnArr['is_status'] = 'Published';
            else
                    $returnArr['is_status'] = 'Un-Published';
        }        
		
        $returnArr['start_index'] = (isset($dataArr['start_index']) && $dataArr['start_index'] != '') ? $dataArr['start_index'] : '0';
        $returnArr['record_limit'] = (isset($dataArr['record_limit']) && $dataArr['record_limit'] != '') ? $dataArr['record_limit'] : '';
        $returnArr['sort_field'] = (isset($dataArr['sort_field']) && $dataArr['sort_field'] != '') ? $dataArr['sort_field'] : '';
        $returnArr['sort_order'] = (isset($dataArr['sort_order']) && $dataArr['sort_order'] != '') ? $dataArr['sort_order'] : '';
        return $returnArr;
    }

    public function getInputFilterCreateHistory() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
			
            $inputFilter->add($factory->createInput(array(
                    'name' => 'title',
                    'required' => true,
                    'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                            array(
                                    'name' => 'NotEmpty',
                                    'options' => array(
                                            'messages' => array(
                                                    'isEmpty' => 'not empty',
                                            ),
                                    ),
                            ),
                    ),
            )));

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

	public function getSearchArray($dataArr = array()) {
        $returnArr = array();
        
		$returnArr[] = (isset($dataArr['first_name']) && $dataArr['first_name'] != '') ? $dataArr['first_name'] : '';
		$returnArr[] = (isset($dataArr['last_name']) && $dataArr['last_name'] != '') ? $dataArr['last_name'] : '';
		$returnArr[] = (isset($dataArr['birth_country_id']) && $dataArr['birth_country_id'] != '') ? $dataArr['birth_country_id'] : '';

        return $returnArr;
    }
    
    /**
     * Function used to pass for input filter
     * @author Icreon Tech - AS
     * @return array
     * @param Array
     */
    public function getInputFilterCrmSearch($filterData) {
        $inputFilterData = array();
        if (is_array($filterData)) {
            foreach ($filterData as $key => $val) {
                $inputFilterData[$key] = trim(addslashes(strip_tags($val)));
            }
        } else {
            $inputFilterData = trim(addslashes(strip_tags($filterData)));
        }
        return $this->inputFilterCrmSearch = $inputFilterData;
    }
}