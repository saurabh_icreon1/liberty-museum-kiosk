<?php

/**
 * This is used for Passenger module.
 * @package    Passenger
 * @author     Icreon Tech -SR.
 */

namespace Passenger\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This class is used for Passenger module.
 * @author     Icreon Tech -SR.
 */
class ManifestTable {

    protected $tableGateway;
    protected $connection;
    protected $dbAdapter;

    /**
     * This is used for dafault initialization of objects.
     * @author     Icreon Tech -SR.
     */
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
        $this->connection = $this->tableGateway->getAdapter()->getDriver()->getConnection();
    }

    /**
     * This function will save the search title into the table for manifest search
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech -SR
     */
    public function saveManifestSearch($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_saveManifestSearch(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $stmt->getResource()->bindParam(2, $param['search_name']);
            $stmt->getResource()->bindParam(3, $param['search_query']);
            $stmt->getResource()->bindParam(4, $param['isActive']);
            $stmt->getResource()->bindParam(5, $param['isDelete']);
            $stmt->getResource()->bindParam(6, $param['added_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the saved search listing for manifest
     * @param this will be an array.
     * @return this will return a title of the saved search
     * @author Icreon Tech -SR
     */
    public function getManifestSavedSearch($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getCrmSearchManifests(?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $stmt->getResource()->bindParam(2, $param['isActive']);
            $stmt->getResource()->bindParam(3, $param['searchId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the saved search listing for manifest
     * @param this will be an array.
     * @return this will return a title of the saved search
     * @author Icreon Tech -SR
     */
    public function getCrmManifestSavedSearch($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getCrmSearchManifests(?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $stmt->getResource()->bindParam(2, $param['isActive']);
            $stmt->getResource()->bindParam(3, $param['searchId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update the search title and the search parameters
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech -SR
     */
    public function updateCrmManifestSearch($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_updateCrmSearchManifest(?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $stmt->getResource()->bindParam(2, $param['search_name']);
            $stmt->getResource()->bindParam(3, $param['search_query']);
            $stmt->getResource()->bindParam(4, $param['isActive']);
            $stmt->getResource()->bindParam(5, $param['isDelete']);
            $stmt->getResource()->bindParam(6, $param['modifiend_date']);
            $stmt->getResource()->bindParam(7, $param['search_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the manifest details
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech -SR
     */
    public function searchCrmManifest($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getManifests(?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['frame']);
            $stmt->getResource()->bindParam(2, $param['roll_number']);
            $stmt->getResource()->bindParam(3, $param['file_name']);
            $stmt->getResource()->bindParam(4, $param['series']);
            $stmt->getResource()->bindParam(5, $param['ship_name']);
            $stmt->getResource()->bindParam(6, $param['arrival_from_date']);
            $stmt->getResource()->bindParam(7, $param['arrival_to_date']);
            $stmt->getResource()->bindParam(8, $param['ude_batch_number']);
            $stmt->getResource()->bindParam(9, $param['startIndex']);
            $stmt->getResource()->bindParam(10, $param['recordLimit']);
            $stmt->getResource()->bindParam(11, $param['sortField']);
            $stmt->getResource()->bindParam(12, $param['sortOrder']);
            $stmt->getResource()->bindParam(13, $param['getRecordsCount']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
//asd($resultSet);
            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to update the crm manifest
     * @param this will be an array.
     * @return this will return true
     * @author Icreon Tech -SR
     */
    public function updateCrmManifest($param = array()) {
        try {

            $frameOld = isset($param['frame_old']) ? $param['frame_old'] : '';
            $rollNumberOld = isset($param['roll_number_old']) ? $param['roll_number_old'] : '';
            $frame = isset($param['frame']) ? $param['frame'] : '';
            $rollNumber = isset($param['roll_number']) ? $param['roll_number'] : '';
            $arrivalDate = isset($param['arrival_date']) ? $param['arrival_date'] : '';
            $udeBatchNumber = isset($param['ude_batch_number']) ? $param['ude_batch_number'] : '';
            $fileName = isset($param['file_name']) ? $param['file_name'] : '';
            $ship = isset($param['ship']) ? $param['ship'] : '';
            $modifiedTime = isset($param['modified_time']) ? $param['modified_time'] : '';
            $passengerId = isset($param['passenger_id']) ? $param['passenger_id'] : '';
            $filename_old = isset($param['filename_old']) ? $param['filename_old'] : '';
            $ude_batch_no_old = isset($param['ude_batch_no_old']) ? $param['ude_batch_no_old'] : '';
            $is_edited_image = isset($param['is_edited_image']) ? $param['is_edited_image'] : '';

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_updateCrmManifest(?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $frameOld);
            $stmt->getResource()->bindParam(2, $rollNumberOld);
            $stmt->getResource()->bindParam(3, $frame);
            $stmt->getResource()->bindParam(4, $rollNumber);
            $stmt->getResource()->bindParam(5, $arrivalDate);
            $stmt->getResource()->bindParam(6, $udeBatchNumber);
            $stmt->getResource()->bindParam(7, $fileName);
            $stmt->getResource()->bindParam(8, $ship);
            $stmt->getResource()->bindParam(9, $modifiedTime);
            $stmt->getResource()->bindParam(10, $passengerId);
            $stmt->getResource()->bindParam(11, $filename_old);
            $stmt->getResource()->bindParam(12, $ude_batch_no_old);
            $stmt->getResource()->bindParam(13, $is_edited_image);
            $result = $stmt->execute();
            $statement = $result->getResource();
            //$resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            //asd($resultSet);
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will delete the saved crm manifest titles
     * @param this will be an array having the params.
     * @return this will return a confirmation
     * @author Icreon Tech -SR
     */
    public function deleteCrmManifestSearch($param = array()) {

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_deleteCrmSearchManifest(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $stmt->getResource()->bindParam(2, $param['isDelete']);
            $stmt->getResource()->bindParam(3, $param['modifiend_date']);
            $stmt->getResource()->bindParam(4, $param['search_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the passenger count
     * @param this will be an array having the search params.
     * @return this will return array
     * @author Icreon Tech -SR
     */
    public function getManifestPassengerCount($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $fileName = isset($param['file_name']) ? $param['file_name'] : '';

            $stmt->prepare('CALL usp_pas_getManifestPassengerCount(?)');
            $stmt->getResource()->bindParam(1, $fileName);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    public function getManifestNoOfFrame($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $roll_nbr = isset($param['roll_nbr']) ? $param['roll_nbr'] : '';
            $arrival_date = isset($param['arrival_date']) ? $param['arrival_date'] : '';

            $stmt->prepare('CALL usp_pas_getManifestNoOfFrame(?,?)');
            $stmt->getResource()->bindParam(1, $roll_nbr);
            $stmt->getResource()->bindParam(2, $arrival_date);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    public function getManifestPassengers($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $fileName = isset($param['fileName']) ? $param['fileName'] : '';

            $stmt->prepare('CALL usp_pas_manifestPassengerListDetails(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $fileName);
            $stmt->getResource()->bindParam(2, $param['startIndex']);
            $stmt->getResource()->bindParam(3, $param['recordLimit']);
            $stmt->getResource()->bindParam(4, $param['sortField']);
            $stmt->getResource()->bindParam(5, $param['sortOrder']);


            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

}