<?php

/**
 * This is used for Passenger module.
 * @package    Passenger
 * @author     Icreon Tech -SR.
 */

namespace Passenger\Model;

use Passenger\Module;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\Adapter\Adapter;

/**
 * This is class used for Passenger module.
 * @author     Icreon Tech -SR.
 */
class Passenger implements InputFilterAwareInterface {

    protected $inputFilter;
    protected $adapter;

    /**
     * This is used for dafault initialization of objects.
     * @package    Document
     * @author     Icreon Tech -SR.
     */
    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    /**
     * Function used to exchange passenger values (assignment of values)
     * @return null
     * @param $data array
     * @author Icreon Tech - SR* 
     */
    public function exchangeArray($data) {
        
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

    /**
     * Function used to check validation for correction
     * @author Icreon Tech - SR
     * @return boolean
     * @param Array
     */
    public function getInputFilterCorrection() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'first_name',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'last_name',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'nationality',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'last_residence',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'arrival_age',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'gender',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'marital_status',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'ship',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'port_departure',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'line_number',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * Function used to exchange passenger correction values (assignment of values)
     * @author Icreon Tech - SR
     * @return void
     * @param $data array
     */
    public function exchangeCorrectionArray($data) {

        $this->first_name = (isset($data['first_name']) && !empty($data['first_name'])) ? $data['first_name'] : '';
        $this->last_name = (isset($data['last_name']) && !empty($data['last_name'])) ? $data['last_name'] : '';
        $this->nationality = (isset($data['nationality']) && !empty($data['nationality'])) ? $data['nationality'] : '';
        $this->last_residence = (isset($data['last_residence']) && !empty($data['last_residence'])) ? $data['last_residence'] : '';
        $this->arrival_age = (isset($data['arrival_age']) && !empty($data['arrival_age'])) ? $data['arrival_age'] : '';
        $this->gender = (isset($data['gender']) && !empty($data['gender'])) ? $data['gender'] : '';
        $this->marital_status = (isset($data['marital_status']) && !empty($data['marital_status'])) ? $data['marital_status'] : '';
        $this->arrival_date = (isset($data['arrival_date']) && !empty($data['arrival_date'])) ? $data['arrival_date'] : '';
        $this->ship = (isset($data['ship']) && !empty($data['ship'])) ? $data['ship'] : '';
        $this->port_departure = (isset($data['port_departure']) && !empty($data['port_departure'])) ? $data['port_departure'] : '';
        $this->line_number = (isset($data['line_number']) && !empty($data['line_number'])) ? $data['line_number'] : '';
        $this->birth_place = (isset($data['birth_place']) && !empty($data['birth_place'])) ? $data['birth_place'] : '';
    }

    /**
     * Function used to exchange passenger annotation values (assignment of values)
     * @author Icreon Tech - SR
     * @return void
     * @param $data array
     */
    public function exchangeAnnotationArray($data) {

        $this->first_name = (isset($data['first_name']) && !empty($data['first_name'])) ? $data['first_name'] : '';
        $this->last_name = (isset($data['last_name']) && !empty($data['last_name'])) ? $data['last_name'] : '';
        $this->nationality = (isset($data['nationality']) && !empty($data['nationality'])) ? $data['nationality'] : '';
        $this->last_residence = (isset($data['last_residence']) && !empty($data['last_residence'])) ? $data['last_residence'] : '';
        $this->arrival_age = (isset($data['arrival_age']) && !empty($data['arrival_age'])) ? $data['arrival_age'] : '';
        $this->gender = (isset($data['gender']) && !empty($data['gender'])) ? $data['gender'] : '';
        $this->marital_status = (isset($data['marital_status']) && !empty($data['marital_status'])) ? $data['marital_status'] : '';
        $this->arrival_date = (isset($data['arrival_date']) && !empty($data['arrival_date'])) ? $data['arrival_date'] : '';
        $this->ship = (isset($data['ship']) && !empty($data['ship'])) ? $data['ship'] : '';
        $this->port_departure = (isset($data['port_departure']) && !empty($data['port_departure'])) ? $data['port_departure'] : '';
        $this->line_number = (isset($data['line_number']) && !empty($data['line_number'])) ? $data['line_number'] : '';

        $this->birth_date = (isset($data['birth_date']) && !empty($data['birth_date'])) ? $data['birth_date'] : '';
        $this->death_date = (isset($data['death_date']) && !empty($data['death_date'])) ? $data['death_date'] : '';
        $this->occupation = (isset($data['occupation']) && !empty($data['occupation'])) ? $data['occupation'] : '';
        $this->spouse = (isset($data['spouse']) && !empty($data['spouse'])) ? $data['spouse'] : '';
        $this->children = (isset($data['children']) && !empty($data['children'])) ? $data['children'] : '';
        $this->us_relatives = (isset($data['us_relatives']) && !empty($data['us_relatives'])) ? $data['us_relatives'] : '';
        $this->us_residence = (isset($data['us_residence']) && !empty($data['us_residence'])) ? $data['us_residence'] : '';
        $this->military_experience = (isset($data['military_experience']) && !empty($data['military_experience'])) ? $data['military_experience'] : '';
        $this->religious_community = (isset($data['religious_community']) && !empty($data['religious_community'])) ? $data['religious_community'] : '';
        $this->annotation_email = (isset($data['annotation_email']) && !empty($data['annotation_email'])) ? $data['annotation_email'] : '';

        $this->date_of_marriage = (isset($data['date_of_marriage']) && !empty($data['date_of_marriage'])) ? $data['date_of_marriage'] : '';
        $this->parent_names = (isset($data['parent_names']) && !empty($data['parent_names'])) ? $data['parent_names'] : '';
        $this->first_time_in_us = (isset($data['first_time_in_us']) && !empty($data['first_time_in_us'])) ? $data['first_time_in_us'] : '0';
        $this->birth_place = (isset($data['birth_place']) && !empty($data['birth_place'])) ? $data['birth_place'] : '';
    }

    /**
     * Function used to check validation for annotation
     * @author Icreon Tech - SR
     * @return boolean
     * @param Array
     */
    public function getInputFilterAnnotation() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'first_name',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'last_name',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'nationality',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'last_residence',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'arrival_age',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'gender',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'marital_status',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'ship',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'port_departure',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'line_number',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'death_date',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'birth_date',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'first_time_in_us',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'occupation',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'spouse',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'children',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'us_relatives',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'us_residence',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'military_experience',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'religious_community',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'parent_names',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
             $inputFilter->add($factory->createInput(array(
                        'name' => 'type',
                        'required' => false,
                    )));

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

}