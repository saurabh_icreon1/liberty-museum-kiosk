<?php

/**
 * This is used for Passenger module.
 * @package    Passenger
 * @author     Icreon Tech -SR.
 */

namespace Passenger\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This class is used for FamilyHistory module.
 * @author     Icreon Tech -DG.
 */
class FamilyhistoryTable {

    protected $tableGateway;
    protected $connection;
    protected $dbAdapter;

    /**
     * This is used for dafault initialization of objects.
     * @author     Icreon Tech -DG
     */
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
        $this->connection = $this->tableGateway->getAdapter()->getDriver()->getConnection();
    }

    /**
     * This function will fetch getUserSavedFamilyHistory
     * @param ship name
     * @return this will return arrry
     * @author Icreon Tech -DG
     */
    public function getUserSavedFamilyHistory($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_geFamilyhistorySearches(?)');
            $stmt->getResource()->bindParam(1, $param['user_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to delete the saved fh search 
     * @param this will be an array having the detele condition parameters, user_id int, search_id int .
     * @return this will return the confirmation
     * @author Icreon Tech -DG
     */
    public function deleteSearchFamilyHistory($param = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_pas_deleteUserSearchFamilyhistory(?,?)');
        $stmt->getResource()->bindParam(1, $param['is_delete']);
        $stmt->getResource()->bindParam(2, $param['search_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * This function is used to delete the fh
     * @param this will be an array having the detele condition parameters, user_id int, search_id int .
     * @return this will return the confirmation
     * @author Icreon Tech -DG
     */
    public function deleteFamilyHistory($param = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_pas_deleteFamilyhistory(?,?)');
        $stmt->getResource()->bindParam(1, $param['family_history_id']);
        $stmt->getResource()->bindParam(2, $param['is_delete']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    public function insertCrmFamilyhistory($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_insertCrmFamilyhistory(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['user_id']);
            $stmt->getResource()->bindParam(2, $param['title']);
            $stmt->getResource()->bindParam(3, $param['story']);
            $stmt->getResource()->bindParam(4, $param['is_sharing']);
            $stmt->getResource()->bindParam(5, $param['is_featured']);
            $stmt->getResource()->bindParam(6, $param['is_email_on']);
            $stmt->getResource()->bindParam(7, $param['main_content']);
            $stmt->getResource()->bindParam(8, $param['num_content']);
            $stmt->getResource()->bindParam(9, $param['is_status']);
            $stmt->getResource()->bindParam(10, $param['publish_date']);
            $stmt->getResource()->bindParam(11, $param['is_deleted']);
            $stmt->getResource()->bindParam(12, $param['added_by']);
            $stmt->getResource()->bindParam(13, $param['added_date']);
            $stmt->getResource()->bindParam(14, $param['modified_by']);
            $stmt->getResource()->bindParam(15, $param['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    public function insertCrmFamilyContent($param = array()) {
        try {

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_insertCrmFamilyContent(?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['family_history_id']);
            $stmt->getResource()->bindParam(2, $param['location_no']);
            $stmt->getResource()->bindParam(3, $param['location_content']);
            $stmt->getResource()->bindParam(4, $param['content_type']);
            $stmt->getResource()->bindParam(5, $param['content_caption']);
            $stmt->getResource()->bindParam(6, $param['is_deleted']);
            $stmt->getResource()->bindParam(7, $param['added_by']);
            $stmt->getResource()->bindParam(8, $param['added_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function insertCrmFamilyPerson($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_insertCrmFamilyPerson(?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['family_history_id']);
            $stmt->getResource()->bindParam(2, $param['passenger_id']);
            $stmt->getResource()->bindParam(3, $param['first_name']);
            $stmt->getResource()->bindParam(4, $param['last_name']);
            $stmt->getResource()->bindParam(5, $param['year_of_birth']);
            $stmt->getResource()->bindParam(6, $param['birth_country_id']);
            $stmt->getResource()->bindParam(7, $param['panel_no']);

            $stmt->getResource()->bindParam(8, $param['relationship_id']);
            $stmt->getResource()->bindParam(9, $param['is_deleted']);
            $stmt->getResource()->bindParam(10, $param['added_by']);
            $stmt->getResource()->bindParam(11, $param['added_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function getAllFamilyHistories($param = array()) {
        try {
            
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getCrmFamilyhistories(?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $family_history_id = (isset($param['family_history_id']) && $param['family_history_id'] != '' && !is_null($param['family_history_id'])) ? $param['family_history_id'] : '';
            $user_id = (isset($param['user_id']) && $param['user_id'] != '' && $param['user_id'] != '0' && !is_null($param['user_id'])) ? $param['user_id'] : '';
            $contact_name = (isset($param['contact_name']) && $param['contact_name'] != '' && !is_null($param['contact_name'])) ? addslashes($param['contact_name']) : '';
            $is_status = (isset($param['is_status']) && $param['is_status'] != '' && !is_null($param['is_status'])) ? $param['is_status'] : '';
            $title = (isset($param['title']) && $param['title'] != '' && !is_null($param['title'])) ? $param['title'] : '';
            $added_date_from = (isset($param['added_date_from']) && $param['added_date_from'] != '' && !is_null($param['added_date_from'])) ? $param['added_date_from'] : '';
            $added_date_to = (isset($param['added_date_to']) && $param['added_date_to'] != '' && !is_null($param['added_date_to'])) ? $param['added_date_to'] : '';
            $is_sharing = (isset($param['is_sharing']) && $param['is_sharing'] != '' && !is_null($param['is_sharing'])) ? $param['is_sharing'] : '';
            $birth_country_id = (isset($param['country_id']) && $param['country_id'] != '' && !is_null($param['country_id'])) ? $param['country_id'] : '';
            $start_index = (isset($param['start_index']) && $param['start_index'] != '' && !is_null($param['start_index'])) ? $param['start_index'] : '';
            $record_limit = (isset($param['record_limit']) && $param['record_limit'] != '' && !is_null($param['record_limit'])) ? $param['record_limit'] : '';
            $sort_field = (isset($param['sort_field']) && $param['sort_field'] != '' && !is_null($param['sort_field'])) ? $param['sort_field'] : '';
            $sort_order = (isset($param['sort_order']) && $param['sort_order'] != '' && !is_null($param['sort_order'])) ? $param['sort_order'] : '';
            $isFeatured = (isset($param['is_featured']) && !empty($param['is_featured']) && !is_null($param['is_featured'])) ? $param['is_featured'] : '';
            $stmt->getResource()->bindParam(1, $family_history_id);
            $stmt->getResource()->bindParam(2, $user_id);
            $stmt->getResource()->bindParam(3, $contact_name);
            $stmt->getResource()->bindParam(4, $is_status);
            $stmt->getResource()->bindParam(5, $title);
            $stmt->getResource()->bindParam(6, $is_sharing);
            $stmt->getResource()->bindParam(7, $birth_country_id);
            $stmt->getResource()->bindParam(8, $added_date_from);
            $stmt->getResource()->bindParam(9, $added_date_to);
            $stmt->getResource()->bindParam(10, $start_index);
            $stmt->getResource()->bindParam(11, $record_limit);
            $stmt->getResource()->bindParam(12, $sort_field);
            $stmt->getResource()->bindParam(13, $sort_order);
            $stmt->getResource()->bindParam(14, $isFeatured);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();            
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    
    public function getUserFamilyHistories($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getUserFamilyhistories(?,?,?,?,?,?,?,?,?)');
            $family_history_id = (isset($param['family_history_id']) && $param['family_history_id'] != '' && !is_null($param['family_history_id'])) ? $param['family_history_id'] : '';
            $user_id = (isset($param['user_id']) && $param['user_id'] != '' && $param['user_id'] != '0' && !is_null($param['user_id'])) ? $param['user_id'] : '';
            $contact_name = (isset($param['contact_name']) && $param['contact_name'] != '' && !is_null($param['contact_name'])) ? $param['contact_name'] : '';
            $birth_country_id = (isset($param['country_id']) && $param['country_id'] != '' && !is_null($param['country_id'])) ? $param['country_id'] : '';
            $start_index = (isset($param['start_index']) && $param['start_index'] != '' && !is_null($param['start_index'])) ? $param['start_index'] : '';
            $record_limit = (isset($param['record_limit']) && $param['record_limit'] != '' && !is_null($param['record_limit'])) ? $param['record_limit'] : '';
            $sort_field = (isset($param['sort_field']) && $param['sort_field'] != '' && !is_null($param['sort_field'])) ? $param['sort_field'] : '';
            $sort_order = (isset($param['sort_order']) && $param['sort_order'] != '' && !is_null($param['sort_order'])) ? $param['sort_order'] : '';
            $isFeatured = (isset($param['is_featured']) && !empty($param['is_featured']) && !is_null($param['is_featured'])) ? $param['is_featured'] : '';
            $stmt->getResource()->bindParam(1, $family_history_id);
            $stmt->getResource()->bindParam(2, $user_id);
            $stmt->getResource()->bindParam(3, $contact_name);         
            $stmt->getResource()->bindParam(4, $isFeatured);
            $stmt->getResource()->bindParam(5, $birth_country_id);
            $stmt->getResource()->bindParam(6, $start_index);
            $stmt->getResource()->bindParam(7, $record_limit);
            $stmt->getResource()->bindParam(8, $sort_field);
            $stmt->getResource()->bindParam(9, $sort_order);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used view a family history information
     * @param array
     * @return arry
     * @author Icreon Tech - AS
     */
    public function getFamilyHistoryInfo($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getCrmSearchFamilyhistoryInfo(?)');
            @$stmt->getResource()->bindParam(1, $param['family_history_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used view a family history information
     * @param array
     * @return arry
     * @author Icreon Tech - AS
     */
    public function deleteFamilyHistoryInfo($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_deleteCrmFamilyhistory(?,?,?)');
            $stmt->getResource()->bindParam(1, $param['id']);
            $stmt->getResource()->bindParam(2, $param['modifiedBy']);
            $stmt->getResource()->bindParam(3, $param['modifiedDate']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used view user family content information
     * @param array
     * @return arry
     * @author Icreon Tech - SK
     */
    public function getUserFamilyContent($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getCrmFamilyContent(?,?)');

            $family_content_id = (isset($param['family_content_id']) && (!is_null($param['family_content_id']) && $param['family_content_id'] != '')) ? $param['family_content_id'] : '';
            $family_history_id = (isset($param['family_history_id']) && (!is_null($param['family_history_id']) && $param['family_history_id'] != '')) ? $param['family_history_id'] : '';
            $stmt->getResource()->bindParam(1, $family_content_id);
            $stmt->getResource()->bindParam(2, $family_history_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the saved search listing for family history
     * @param this will be an array.
     * @return this will return a title of the saved search
     * @author Icreon Tech - AS
     */
    public function getCrmFamilySavedSearch($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getCrmSearchFamilyhistory(?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $stmt->getResource()->bindParam(2, $param['isActive']);
            $stmt->getResource()->bindParam(3, $param['searchId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to save search listing for family history
     * @param this will be an array.
     * @return this will return a title of the saved search
     * @author Icreon Tech - AS
     */
    public function saveCrmFamilyHistorySearch($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_insertCrmSearchFamilyhistory(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $stmt->getResource()->bindParam(2, $param['search_name']);
            $stmt->getResource()->bindParam(3, $param['search_query']);
            $stmt->getResource()->bindParam(4, $param['isActive']);
            $stmt->getResource()->bindParam(5, $param['isDelete']);
            $stmt->getResource()->bindParam(6, $param['added_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the saved search listing for family history
     * @param this will be an array.
     * @return this will return a title of the saved search
     * @author Icreon Tech - AS
     */
    public function getCrmFamilyHistorySavedSearch($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getCrmFamilyHistorySavedSearch(?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $stmt->getResource()->bindParam(2, $param['isActive']);
            $stmt->getResource()->bindParam(3, $param['campaignSearchId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will delete the saved crm search 
     * @param this will be an array having the params.
     * @return this will return a confirmation
     * @author Icreon Tech -AS
     */
    public function deleteCrmFamilyhistorySearch($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_deleteCrmSearchFamilyhistory(?,?)');
            $stmt->getResource()->bindParam(1, $param['search_id']);
            $stmt->getResource()->bindParam(2, $param['modifiend_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used view user family person information
     * @param array
     * @return arry
     * @author Icreon Tech - SK
     */
    public function getUserFamilyPerson($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getCrmFamilyPerson(?,?,?,?,?,?)');
            $family_content_id = (isset($param['family_content_id']) && (!is_null($param['family_content_id']) && $param['family_content_id'] != '')) ? $param['family_content_id'] : '';
            $family_history_id = (isset($param['family_history_id']) && (!is_null($param['family_history_id']) && $param['family_history_id'] != '')) ? $param['family_history_id'] : '';
            $passenger_id = (isset($param['passenger_id']) && (!is_null($param['passenger_id']) && $param['passenger_id'] != '')) ? $param['passenger_id'] : '';
            $first_name = (isset($param['first_name']) && (!is_null($param['first_name']) && $param['first_name'] != '')) ? $param['first_name'] : '';
            $last_name = (isset($param['last_name']) && (!is_null($param['last_name']) && $param['last_name'] != '')) ? $param['last_name'] : '';
            $birth_country_id = (isset($param['birth_country_id']) && (!is_null($param['birth_country_id']) && $param['birth_country_id'] != '')) ? $param['birth_country_id'] : '';
            $stmt->getResource()->bindParam(1, $family_content_id);
            $stmt->getResource()->bindParam(2, $family_history_id);
            $stmt->getResource()->bindParam(3, $passenger_id);
            $stmt->getResource()->bindParam(4, $first_name);
            $stmt->getResource()->bindParam(5, $last_name);
            $stmt->getResource()->bindParam(6, $birth_country_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    public function updateCrmFamilyhistory($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_updateCrmFamilyhistory(?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            if (is_null($param['user_id']) || $param['user_id'] == '')
                $user_id = '';
            if ($param['is_sharing'] == '')
                $param['is_sharing'] = '0';
            if ($param['is_featured'] == '')
                $param['is_featured'] = '0';
            if ($param['is_email_on'] == '')
                $param['is_email_on'] = '0';
            if ($param['is_notify_contact'] == '')
                $param['is_notify_contact'] = '0';

            $stmt->getResource()->bindParam(1, $param['family_history_id']);
            $stmt->getResource()->bindParam(2, $param['user_id']);
            $stmt->getResource()->bindParam(3, $param['title']);
            $stmt->getResource()->bindParam(4, $param['story']);
            $stmt->getResource()->bindParam(5, $param['is_sharing']);
            $stmt->getResource()->bindParam(6, $param['is_featured']);
            $stmt->getResource()->bindParam(7, $param['is_email_on']);
            $stmt->getResource()->bindParam(8, $param['is_notify_contact']);
            $stmt->getResource()->bindParam(9, $param['main_content']);
            $stmt->getResource()->bindParam(10, $param['num_content']);
            $stmt->getResource()->bindParam(11, $param['is_status']);
            $stmt->getResource()->bindParam(12, $param['modified_by']);
            $stmt->getResource()->bindParam(13, $param['modified_date']);
            $stmt->getResource()->bindParam(14, $param['publish_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
        } catch (Exception $e) {
            return false;
        }
    }

    public function updateCrmFamilyContent($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_updateCrmFamilyContent(?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['family_content_id']);
            $stmt->getResource()->bindParam(2, $param['location_no']);
            $stmt->getResource()->bindParam(3, $param['location_content']);
            $stmt->getResource()->bindParam(4, $param['content_type']);
            $stmt->getResource()->bindParam(5, $param['content_caption']);
            $stmt->getResource()->bindParam(6, $param['modified_by']);
            $stmt->getResource()->bindParam(7, $param['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
        } catch (Exception $e) {
            return false;
        }
    }

    public function updateCrmFamilyPerson($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_updateCrmFamilyPerson(?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['family_person_id']);
            $stmt->getResource()->bindParam(2, $param['passenger_id']);
            $stmt->getResource()->bindParam(3, $param['first_name']);
            $stmt->getResource()->bindParam(4, $param['last_name']);
            $stmt->getResource()->bindParam(5, $param['year_of_birth']);
            $stmt->getResource()->bindParam(6, $param['birth_country_id']);
            $stmt->getResource()->bindParam(7, $param['panel_no']);
            $stmt->getResource()->bindParam(8, $param['relationship_id']);
            $stmt->getResource()->bindParam(9, $param['modified_by']);
            $stmt->getResource()->bindParam(10, $param['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
        } catch (Exception $e) {
            return false;
        }
    }

    /* Function for get family count
     * @author Icreon Tech- AS
     * param void
     * return array
     */

    public function getFamilyCount($param) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare("CALL usp_com_getFamilyCount(?)");
        $stmt->getResource()->bindParam(1, $param['user_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if ($resultSet) {
            return $resultSet[0]['totalRecord'];
        } else {
            return false;
        }
    }

    /* Function for save crm family history bonus
     * @author Icreon Tech- DT
     * param array
     * return array
     */

    public function insertUserFamilyHistoryBonus($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_insertCrmContactFamilyhistoryBonus(?,?,?,?,?,?,?,?,?)');
            $max_stories = (!empty($param['max_stories'])) ? $param['max_stories'] : NULL;
            $max_story_characters = (!empty($param['max_story_characters'])) ? $param['max_story_characters'] : NULL;
            $max_passengers = (!empty($param['max_passengers'])) ? $param['max_passengers'] : NULL;
            $max_saved_histories = (!empty($param['max_saved_histories'])) ? $param['max_saved_histories'] : NULL;
            $stmt->getResource()->bindParam(1, $param['user_id']);
            $stmt->getResource()->bindParam(2, $max_stories);
            $stmt->getResource()->bindParam(3, $max_story_characters);
            $stmt->getResource()->bindParam(4, $max_passengers);
            $stmt->getResource()->bindParam(5, $max_saved_histories);
            $stmt->getResource()->bindParam(6, $param['added_by']);
            $stmt->getResource()->bindParam(7, $param['added_date']);
            $stmt->getResource()->bindParam(8, $param['modified_by']);
            $stmt->getResource()->bindParam(9, $param['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get family history bonus
     * @param array
     * @return arry
     * @author Icreon Tech - DT
     */
    public function getUserFamilyHistoryBonus($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getCrmContactFamilyhistoryBonus(?)');
            $user_id = (isset($param['user_id']) && (!is_null($param['user_id']) && $param['user_id'] != '')) ? $param['user_id'] : '';
            $stmt->getResource()->bindParam(1, $user_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to update search listing for family history
     * @param this will be an array.
     * @return this will return a title of the saved search
     * @author Icreon Tech - SK
     */
    public function updateCrmFamilyHistorySearch($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_updateCrmSearchFamilyhistory(?,?,?,?,	?)');
            $modifed_date = date('Y-m-d H:i:s');
            $stmt->getResource()->bindParam(1, $param['search_id']);
            $stmt->getResource()->bindParam(2, $param['crm_user_id']);
            $stmt->getResource()->bindParam(3, $param['search_name']);
            $stmt->getResource()->bindParam(4, $param['search_query']);
            $stmt->getResource()->bindParam(5, $modifed_date);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function deleteCrmFamilyPerson($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_deleteCrmFamilyPerson(?,?,?)');
            $modifed_date = date('Y-m-d H:i:s');
            $stmt->getResource()->bindParam(1, $param['family_person_id']);
            $stmt->getResource()->bindParam(2, $param['modified_by']);
            $stmt->getResource()->bindParam(3, $modifed_date);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function searchUserFamilyhistories($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_searchUserFamilyhistories(?,?,?,?,?,?)');
            $contact_name = (isset($param['contact_name']) && (!is_null($param['contact_name']) && $param['contact_name'] != '')) ? addslashes($param['contact_name']) : '';
            $user_id = (isset($param['user_id']) && (!is_null($param['user_id']) && $param['user_id'] != '')) ? $param['user_id'] : '';
            $start_index = (isset($param['start_index']) && $param['start_index'] != '' && !is_null($param['start_index'])) ? $param['start_index'] : '';
            $record_limit = (isset($param['record_limit']) && $param['record_limit'] != '' && !is_null($param['record_limit'])) ? $param['record_limit'] : '';
            $sort_field = (isset($param['sort_field']) && $param['sort_field'] != '' && !is_null($param['sort_field'])) ? $param['sort_field'] : '';
            $sort_order = (isset($param['sort_order']) && $param['sort_order'] != '' && !is_null($param['sort_order'])) ? $param['sort_order'] : '';

            $stmt->getResource()->bindParam(1, $contact_name);
            $stmt->getResource()->bindParam(2, $user_id);
            $stmt->getResource()->bindParam(3, $start_index);
            $stmt->getResource()->bindParam(4, $record_limit);
            $stmt->getResource()->bindParam(5, $sort_field);
            $stmt->getResource()->bindParam(6, $sort_order);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    public function saveUserFamilyHistoryDetailSearch($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_insertUserSearchFamilyhistory(?,?,?,?,?,?,?,?)');
            $family_history_id = (isset($param['family_history_id']) && $param['family_history_id'] != '') ? $param['family_history_id'] : '';
            $stmt->getResource()->bindParam(1, $family_history_id);
            $stmt->getResource()->bindParam(2, $param['user_id']);
            $stmt->getResource()->bindParam(3, $param['title']);
            $stmt->getResource()->bindParam(4, $param['search_query']);
            $stmt->getResource()->bindParam(5, $param['is_active']);
            $stmt->getResource()->bindParam(6, $param['is_deleted']);
            $stmt->getResource()->bindParam(7, $param['save_date']);
            $stmt->getResource()->bindParam(8, $param['modified_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    public function saveUserFamilyHistorySearch($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_insertUserSearches(?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['search_type']);
            $stmt->getResource()->bindParam(2, $param['user_id']);
            $stmt->getResource()->bindParam(3, $param['title']);
            $stmt->getResource()->bindParam(4, $param['search_query']);
            $stmt->getResource()->bindParam(5, $param['is_active']);
            $stmt->getResource()->bindParam(6, $param['is_deleted']);
            $stmt->getResource()->bindParam(7, $param['save_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    public function getUserSearches($param = array()) {
        try {
            $search_id = (isset($param['search_id']) && $param['search_id'] != '') ? $param['search_id'] : '';
            $user_id = (isset($param['user_id']) && $param['user_id'] != '') ? $param['user_id'] : '';
            $search_type = (isset($param['search_type']) && $param['search_type'] != '') ? $param['search_type'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_geUserSearches(?,?,?)');
            $stmt->getResource()->bindParam(1, $search_id);
            $stmt->getResource()->bindParam(2, $user_id);
            $stmt->getResource()->bindParam(3, $search_type);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    public function deleteUserSearch($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_deleteUserSearches(?)');
            $stmt->getResource()->bindParam(1, $param['search_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
        } catch (Exception $e) {
            return false;
        }
    }

    public function searchAuthorisedAssociatedPersons($param = array()) {
        try {
            $contact_name = (isset($param['contact_name']) && $param['contact_name'] != '') ? addslashes($param['contact_name']) : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getFamilyHistoryAuthorisedAssociatedPerson(?)');
            $stmt->getResource()->bindParam(1, $contact_name);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

	public function getFeaturedFamilyHistories($param = array()){
		try {
            
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getCrmFeaturedFamilyhistories(?,?,?,?)');
            $start_index = (isset($param['start_index']) && $param['start_index'] != '' && !is_null($param['start_index'])) ? $param['start_index'] : '';
            $record_limit = (isset($param['record_limit']) && $param['record_limit'] != '' && !is_null($param['record_limit'])) ? $param['record_limit'] : '';
            $sort_field = (isset($param['sort_field']) && $param['sort_field'] != '' && !is_null($param['sort_field'])) ? $param['sort_field'] : '';
            $sort_order = (isset($param['sort_order']) && $param['sort_order'] != '' && !is_null($param['sort_order'])) ? $param['sort_order'] : '';
            $stmt->getResource()->bindParam(1, $start_index);
            $stmt->getResource()->bindParam(2, $record_limit);
            $stmt->getResource()->bindParam(3, $sort_field);
            $stmt->getResource()->bindParam(4, $sort_order);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
	}

}

