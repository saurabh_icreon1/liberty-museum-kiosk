<?php

/**
 * This is used for OralHistory  module.
 * @package    Passenger
 * @author     Icreon Tech -DG.
 */

namespace Passenger\Model;

use Passenger\Module;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\Adapter\Adapter;

/**
 * This is class used for OralHistory module.
 * @author     Icreon Tech - SK.
 */
class Oralhistory implements InputFilterAwareInterface {

    protected $inputFilter;
    protected $adapter;

    /**
     * This is used for dafault initialization of objects.
     * @package    Passenger
     * @author     Icreon Tech -DG.
     */
    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    /**
     * Function used to exchange FamilyHistory values (assignment of values)
     * @return null
     * @param $data array
     * @author Icreon Tech - DG
     */
    public function exchangeArray($data) {
        
    }
    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }
    
    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }	
    public function getArrayCopy() {
        return get_object_vars($this);
    }
}