<?php

/**
 * This is used for Passenger module.
 * @package    Passenger
 * @author     Icreon Tech -SR.
 */

namespace Passenger\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This class is used for Passenger module.
 * @author     Icreon Tech -SR.
 */
class PassengerTable {

    protected $tableGateway;
    protected $connection;
    protected $dbAdapter;

    /**
     * This is used for dafault initialization of objects.
     * @author     Icreon Tech -SR.
     */
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
        $this->connection = $this->tableGateway->getAdapter()->getDriver()->getConnection();
    }

    /**
     * This function will fetch the all search passengers records on the bases of search parameters
     * @param this will be an array having the search Params.
     * @return this will return a passenger array
     * @author Icreon Tech -SR
     */
    public function getPassengerRecord($param = array()) {
        try { //asd($param);
            $passengerId = isset($param['passenger_id']) ? trim($param['passenger_id']) : '';
            $shipName = isset($param['ship_name']) ? trim(addslashes($param['ship_name'])) : '';
            $dateArrive = isset($param['date_arrive']) ? trim($param['date_arrive']) : '';
            $startIndex = isset($param['startIndex']) ? $param['startIndex'] : '';
            $recordLimit = isset($param['recordLimit']) ? $param['recordLimit'] : '';
            $sortField = isset($param['sortField']) ? $param['sortField'] : '';
            $sortOrder = isset($param['sortOrder']) ? $param['sortOrder'] : '';
            if (empty($passengerId) && empty($shipName) && empty($dateArrive)) {
                return false;
            } else {
                $stmt = $this->dbAdapter->createStatement();
                $stmt->prepare('CALL usp_pas_searchPassenger(?,?,?,?,?,?,?)');
                $stmt->getResource()->bindParam(1, $passengerId);
                $stmt->getResource()->bindParam(2, $shipName);
                $stmt->getResource()->bindParam(3, $dateArrive);
                $stmt->getResource()->bindParam(4, $startIndex);
                $stmt->getResource()->bindParam(5, $recordLimit);
                $stmt->getResource()->bindParam(6, $sortField);
                $stmt->getResource()->bindParam(7, $sortOrder);
                $result = $stmt->execute();
                $statement = $result->getResource();
                $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
                $statement->closeCursor();
                return $resultSet;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will save the search title into the table for passenger search
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech -SR
     */
    public function savePassengerSearch($params = array()) {
        $search_type = $params['search_type'];
        $userId = $params['user_id'];
        $title = trim(addslashes($params['search_name']));
        $searchQuery = addslashes(serialize($params));
        $isActive = $params['isActive'];
        $isDelete = $params['isDelete'];
        $saveDate = DATE_TIME_FORMAT;
        $result = $this->connection->execute("CALL usp_pas_savePassengerSearch('" . $search_type . "','" . $userId . "','" . $title . "','" . $searchQuery . "','" . $isActive . "','" . $isDelete . "','" . $saveDate . "')");
        return true;
    }

    /**
     * This function is used to get the passenger search listing
     * @param this will be an array having the search param user_id int, isActive enum, search_id int .
     * @return this will return the result set array
     * @author Icreon Tech -SR
     */
    public function getPassengerSearch($params = array()) {
        $userId = $params['user_id'];
        $isActive = $params['isActive'];

        if (isset($params['search_type']) && $params['search_type'] != '')
            $searchType = $params['search_type'];
        else
            $searchType = '';

        if (isset($params['search_id']) && $params['search_id'] != '')
            $search_id = $params['search_id'];
        else
            $search_id = '';

        $result = $this->connection->execute("CALL usp_pas_getPassengerSearch('" . $userId . "','" . $isActive . "','" . $search_id . "','" . $searchType . "')");
        $statement = $result->getResource();
        return $resultSet = $statement->fetchAll(\PDO::FETCH_OBJ);
    }

    /**
     * This function is used to delete the passenger search record OR ship search record
     * @param this will be an array having the detele condition parameters, user_id int, search_id int .
     * @return this will return the confirmation
     * @author Icreon Tech -SR
     */
    public function deleteSavedPassengerSearch($params = array()) {
        $userId = $params['user_id'];
        $isDeleted = $params['is_delete'];
        $searchId = $params['search_id'];

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_deletePassengerSearch(?,?,?)');
            $stmt->getResource()->bindParam(1, $userId);
            $stmt->getResource()->bindParam(2, $isDeleted);
            $stmt->getResource()->bindParam(3, $searchId);
            $result = $stmt->execute();
            $statement = $result->getResource();
            //$resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }



        // $result = $this->connection->execute("CALL usp_pas_deletePassengerSearch('" . $userId . "','" . $isDeleted . "','" . $searchId . "')");
        //return true;
    }

    /**
     * This function is used to get all ship name
     * @param array type to get the ship result.
     * @return this will return a ship name array
     * @author Icreon Tech - SR
     */
    public function getShip($params = array()) {
        if (isset($params['ship_name']) && $params['ship_name'] != '')
            $ship_name = addslashes($params['ship_name']);
        else
            $ship_name = '';

        $result = $this->connection->execute("CALL usp_pas_getShip('" . $ship_name . "')");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!$resultSet) {
            throw new \Exception("Could not find record.");
        }
        return $resultSet;
    }

    /**
     * This function is used to get all arrival port
     * @param array type to get the arrival port.
     * @return this will return a arrival port array
     * @author Icreon Tech - SR
     */
    public function getArrivalPort($params = array()) {
        if (isset($params['ship_arrival_port']) && $params['ship_arrival_port'] != '')
            $ship_arrival_port = $params['ship_arrival_port'];
        else
            $ship_arrival_port = '';
        $result = $this->connection->execute("CALL usp_pas_getArrivalPort('" . $ship_arrival_port . "')");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!$resultSet) {
            throw new \Exception("Could not find record.");
        }
        return $resultSet;
    }

    /**
     * This function is used to get all departure port
     * @param array type to get the departure port.
     * @return this will return a departure port array
     * @author Icreon Tech - SR
     */
    public function getDeparturePort($params = array()) {
        if (isset($params['port_of_departure']) && $params['port_of_departure'] != '')
            $port_of_departure = $params['port_of_departure'];
        else
            $port_of_departure = '';
        $limit = isset($params['limit']) ? $params['limit'] : '';

        $result = $this->connection->execute("CALL usp_pas_getDeparturePort('" . $port_of_departure . "','" . $limit . "')");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!$resultSet) {
            // throw new \Exception("Could not find record.");
        }
        return $resultSet;
    }

    /**
     * This function is used to get Ethnicity
     * @param array type to get Ethnicity.
     * @return this will return a Ethnicity array
     * @author Icreon Tech - SR
     */
    public function getEthnicity($params = array()) {

        $result = $this->connection->execute("CALL usp_com_getEthnicity('')");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!$resultSet) {
            throw new \Exception("Could not find record.");
        }
        return $resultSet;
    }

    /**
     * This function will fetch the all ship search
     * @param this will be an array having the search Params.
     * @return this will return a ship array
     * @author Icreon Tech -SR
     */
    public function getShipSearch($params = array()) {
        echo "<pre>";
        print_R($params);
        echo "sarch";
        die;
    }

    /**
     * This function will fetch the all annotation and correction
     * @param this will be an array
     * @return this will return a annotation/correction array
     * @author Icreon Tech -SR
     */
    public function getPassengeAnnotationCorrection($param = array()) {
        try {
            $passengerId = isset($param['passenger_id']) ? $param['passenger_id'] : '';
            $annotationId = isset($param['annotation_id']) ? $param['annotation_id'] : '';
            $recOffset = isset($param['recOffset']) ? $param['recOffset'] : '';
            $recLimit = isset($param['recLimit']) ? $param['recLimit'] : '';
            $status = isset($param['status']) ? $param['status'] : '';
            $activityType = isset($param['activity_type']) ? $param['activity_type'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_annotationCorrection(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $passengerId);
            $stmt->getResource()->bindParam(2, $annotationId);
            $stmt->getResource()->bindParam(3, $recOffset);
            $stmt->getResource()->bindParam(4, $recLimit);
            $stmt->getResource()->bindParam(5, $status);
            $stmt->getResource()->bindParam(6, $activityType);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will fetch the product attributes
     * @param this will be an array
     * @return this will return product attributes array
     * @author Icreon Tech -SR
     */
    public function getProductAttributeDetails($param = array()) {
        try {
            $productId = isset($param['productId']) ? $param['productId'] : '';
            $productType = isset($param['productType']) ? $param['productType'] : '';
            $isOptions = isset($param['isOptions']) ? $param['isOptions'] : '';
            $categoryId = isset($param['categoryId']) ? $param['categoryId'] : '';

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pro_getProductAttributesDetails(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $productId);
            $stmt->getResource()->bindParam(2, $productType);
            $stmt->getResource()->bindParam(3, $isOptions);
            $stmt->getResource()->bindParam(4, $categoryId);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will fetch the passenger ship details
     * @param ship name
     * @return this will return ship details record
     * @author Icreon Tech -SR
     */
    public function getShipDetails($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            if (isset($param['ship_name']) && !empty($param['ship_name'])) {
                $shipDetails = addslashes($param['ship_name']);
                $type = '1';
            } else {
                $shipDetails = $param['ship_id'];
                $type = '2';
            }

            $shipLineId = (isset($param['ship_line_id']) && !empty($param['ship_line_id'])) ? $param['ship_line_id'] : '';
            $stmt->prepare('CALL usp_pas_getPassengerShip(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $shipDetails);
            $stmt->getResource()->bindParam(2, $param['asset_for']);
            $stmt->getResource()->bindParam(3, $type);
            $stmt->getResource()->bindParam(4, $shipLineId);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will insert the correction
     * @param this will be an array having the params .
     * @return this will return a confirmation
     * @author Icreon Tech - SR
     */
    public function insertPassengerCorrection($param) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_insertPassengerCorrection(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['user_id']);
            $stmt->getResource()->bindParam(2, $param['passenger_id']);
            $stmt->getResource()->bindParam(3, $param['first_name']);
            $stmt->getResource()->bindParam(4, $param['last_name']);
            $stmt->getResource()->bindParam(5, $param['nationality']);
            $stmt->getResource()->bindParam(6, $param['last_residence']);
            $stmt->getResource()->bindParam(7, $param['arrival_age']);
            $stmt->getResource()->bindParam(8, $param['gender']);
            $stmt->getResource()->bindParam(9, $param['marital_status']);
            $stmt->getResource()->bindParam(10, $param['arrival_date']);
            $stmt->getResource()->bindParam(11, $param['ship']);
            $stmt->getResource()->bindParam(12, $param['port_departure']);
            $stmt->getResource()->bindParam(13, $param['line_number']);
            $stmt->getResource()->bindParam(14, $param['added_by']); // added by
            $stmt->getResource()->bindParam(15, $param['added_on']); // added on 
            $stmt->getResource()->bindParam(16, $param['modified_by']);
            $stmt->getResource()->bindParam(17, $param['modified_on']);
            $stmt->getResource()->bindParam(18, $param['activity_type']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will get the correction details for the used and passeneger
     * @param this will be an array having the params.
     * @return this will return a confirmation
     * @author Icreon Tech - SR
     */
    public function getPassengerCorrectionDetails($param) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getPassengerAnnotationCorrection(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['user_id']);
            $stmt->getResource()->bindParam(2, $param['passenger_id']);
            $stmt->getResource()->bindParam(3, $param['activity_type']);
            $stmt->getResource()->bindParam(4, $param['status']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update the correction
     * @param this will be an array having the params.
     * @return this will return a confirmation
     * @author Icreon Tech - SR
     */
    public function updatePassengerCorrection($param) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_updatePassengerCorrection(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['annotation_correction_id']);
            $stmt->getResource()->bindParam(2, $param['first_name']);
            $stmt->getResource()->bindParam(3, $param['last_name']);
            $stmt->getResource()->bindParam(4, $param['nationality']);
            $stmt->getResource()->bindParam(5, $param['last_residence']);
            $stmt->getResource()->bindParam(6, $param['arrival_age']);
            $stmt->getResource()->bindParam(7, $param['gender']);
            $stmt->getResource()->bindParam(8, $param['marital_status']);
            $stmt->getResource()->bindParam(9, $param['arrival_date']);
            $stmt->getResource()->bindParam(10, $param['ship']);
            $stmt->getResource()->bindParam(11, $param['port_departure']);
            $stmt->getResource()->bindParam(12, $param['line_number']);
            $stmt->getResource()->bindParam(13, $param['modified_by']);
            $stmt->getResource()->bindParam(14, $param['modified_on']);
            $stmt->getResource()->bindParam(15, $param['birth_place']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will fetch the user passenger search
     * @param ship name
     * @return this will return user passenger search
     * @author Icreon Tech -DG
     */
    public function getUserPassengerSearch($param = array()) {
        try {
            $user_id = isset($param['user_id']) ? trim($param['user_id']) : '';
            $passenger_id = isset($param['passenger_id']) ? trim($param['passenger_id']) : '';
            $sort_order = (isset($param['sort_order']) and trim($param['sort_order']) != "") ? trim($param['sort_order']) : '';
            $sort_field = (isset($param['sort_field']) and trim($param['sort_field']) != "") ? trim($param['sort_field']) : '';
            $start_index = (isset($param['start_index']) and trim($param['start_index']) != '') ? $param['start_index'] : '';
            $record_limit = (isset($param['record_limit']) and trim($param['record_limit']) != '') ? $param['record_limit'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getUserPassengerSearch(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $user_id);
            $stmt->getResource()->bindParam(2, $passenger_id);
            $stmt->getResource()->bindParam(3, $sort_field);
            $stmt->getResource()->bindParam(4, $sort_order);
            $stmt->getResource()->bindParam(5, $start_index);
            $stmt->getResource()->bindParam(6, $record_limit);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will fetch the user manifest search
     * @param ship name
     * @return this will return user manifest search
     * @author Icreon Tech -DG
     */
    public function getUserManifestSearch($param = array()) {
        try {
            $user_id = isset($param['user_id']) ? $param['user_id'] : '';
            $frame = isset($param['frame']) ? $param['frame'] : '';
            $roll_nbr = isset($param['roll_nbr']) ? $param['roll_nbr'] : '';
            $fileName = isset($param['file_name']) ? $param['file_name'] : '';
            if (!empty($user_id)) {
                $stmt = $this->dbAdapter->createStatement();
                $stmt->prepare('CALL usp_pas_getUserManifestSearch(?,?,?,?)');
                $stmt->getResource()->bindParam(1, $user_id);
                $stmt->getResource()->bindParam(2, $frame);
                $stmt->getResource()->bindParam(3, $roll_nbr);
                $stmt->getResource()->bindParam(4, $fileName);
                $result = $stmt->execute();
                $statement = $result->getResource();
                $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

                $statement->closeCursor();
                return $resultSet;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will fetch the user manifest search
     * @param ship name
     * @return this will return user manifest search
     * @author Icreon Tech -DG
     */
    public function getUserSavedShip($param = array()) {
        try {
            $user_id = isset($param['user_id']) ? $param['user_id'] : '';
            $line_ship_id = isset($param['line_ship_id']) ? $param['line_ship_id'] : '';
            $ship_arrival_date = isset($param['ship_arrival_date']) ? $param['ship_arrival_date'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getUserSavedShip(?,?,?)');
            $stmt->getResource()->bindParam(1, $user_id);
            $stmt->getResource()->bindParam(2, $line_ship_id);
            $stmt->getResource()->bindParam(3, $ship_arrival_date);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to delete the saved passenger
     * @param this will be an array having the detele condition parameters, user_id int, search_id int .
     * @return this will return the confirmation
     * @author Icreon Tech -DG
     */
    public function deleteSavedPassenger($param = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_pas_deleteSavedPassenger(?,?,?)');
        $stmt->getResource()->bindParam(1, $param['user_id']);
        $stmt->getResource()->bindParam(2, $param['is_delete']);
        $stmt->getResource()->bindParam(3, $param['search_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * This function is used to delete the manifest sarch 
     * @param this will be an array having the detele condition parameters, user_id int, search_id int .
     * @return this will return the confirmation
     * @author Icreon Tech -DG
     */
    public function deleteManifestSearch($param = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_pas_deleteManifestSearch(?,?,?)');
        $stmt->getResource()->bindParam(1, $param['user_id']);
        $stmt->getResource()->bindParam(2, $param['is_delete']);
        $stmt->getResource()->bindParam(3, $param['search_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * This function is used to delete the saved ship sarch 
     * @param this will be an array having the detele condition parameters, user_id int, search_id int .
     * @return this will return the confirmation
     * @author Icreon Tech -DG
     */
    public function deleteSavedShip($param = array()) {
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_pas_deleteSavedShip(?,?,?)');
        $stmt->getResource()->bindParam(1, $param['user_id']);
        $stmt->getResource()->bindParam(2, $param['is_delete']);
        $stmt->getResource()->bindParam(3, $param['search_id']);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $statement->closeCursor();
    }

    /**
     * This function will insert the annotation
     * @param this will be an array having the params.
     * @return this will return a confirmation
     * @author Icreon Tech - SR
     */
    public function insertPassengerAnnotation($param) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_insertPassengerAnnotation(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['user_id']);
            $stmt->getResource()->bindParam(2, $param['passenger_id']);
            $stmt->getResource()->bindParam(3, $param['first_name']);
            $stmt->getResource()->bindParam(4, $param['last_name']);
            $stmt->getResource()->bindParam(5, $param['nationality']);
            $stmt->getResource()->bindParam(6, $param['last_residence']);
            $stmt->getResource()->bindParam(7, $param['arrival_age']);
            $stmt->getResource()->bindParam(8, $param['gender']);
            $stmt->getResource()->bindParam(9, $param['marital_status']);
            $stmt->getResource()->bindParam(10, $param['arrival_date']);
            $stmt->getResource()->bindParam(11, $param['ship']);
            $stmt->getResource()->bindParam(12, $param['port_departure']);
            $stmt->getResource()->bindParam(13, $param['line_number']);
            $stmt->getResource()->bindParam(14, $param['added_by']); // added by
            $stmt->getResource()->bindParam(15, $param['added_on']); // added on 
            $stmt->getResource()->bindParam(16, $param['modified_by']);
            $stmt->getResource()->bindParam(17, $param['modified_on']);
            $stmt->getResource()->bindParam(18, $param['activity_type']);
            $stmt->getResource()->bindParam(19, $param['birth_date']);
            $stmt->getResource()->bindParam(20, $param['death_date']);
            $stmt->getResource()->bindParam(21, $param['occupation']);
            $stmt->getResource()->bindParam(22, $param['spouse']);
            $stmt->getResource()->bindParam(23, $param['children']);
            $stmt->getResource()->bindParam(24, $param['us_residence']);
            $stmt->getResource()->bindParam(25, $param['us_relatives']);
            $stmt->getResource()->bindParam(26, $param['military_experience']);
            $stmt->getResource()->bindParam(27, $param['religious_community']);
            $stmt->getResource()->bindParam(28, $param['annotation_email']);
            $stmt->getResource()->bindParam(29, $param['date_of_marriage']);
            $stmt->getResource()->bindParam(30, $param['parent_names']);
            $stmt->getResource()->bindParam(31, $param['first_time_in_us']);
            $stmt->getResource()->bindParam(32, $param['status']);
            $stmt->getResource()->bindParam(33, $param['birth_place']);


            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update the annotation
     * @param this will be an array having the params.
     * @return this will return a confirmation
     * @author Icreon Tech - SR
     */
    public function updatePassengerAnnotation($param) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_updatePassengerAnnotation(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['annotation_correction_id']);
            $stmt->getResource()->bindParam(2, $param['first_name']);
            $stmt->getResource()->bindParam(3, $param['last_name']);
            $stmt->getResource()->bindParam(4, $param['nationality']);
            $stmt->getResource()->bindParam(5, $param['last_residence']);
            $stmt->getResource()->bindParam(6, $param['arrival_age']);
            $stmt->getResource()->bindParam(7, $param['gender']);
            $stmt->getResource()->bindParam(8, $param['marital_status']);
            $stmt->getResource()->bindParam(9, $param['arrival_date']);
            $stmt->getResource()->bindParam(10, $param['ship']);
            $stmt->getResource()->bindParam(11, $param['port_departure']);
            $stmt->getResource()->bindParam(12, $param['line_number']);
            $stmt->getResource()->bindParam(13, $param['modified_by']);
            $stmt->getResource()->bindParam(14, $param['modified_on']);

            $stmt->getResource()->bindParam(15, $param['birth_date']);
            $stmt->getResource()->bindParam(16, $param['death_date']);
            $stmt->getResource()->bindParam(17, $param['occupation']);
            $stmt->getResource()->bindParam(18, $param['spouse']);
            $stmt->getResource()->bindParam(19, $param['children']);
            $stmt->getResource()->bindParam(20, $param['us_residence']);
            $stmt->getResource()->bindParam(21, $param['us_relatives']);
            $stmt->getResource()->bindParam(22, $param['military_experience']);
            $stmt->getResource()->bindParam(23, $param['religious_community']);
            $stmt->getResource()->bindParam(24, $param['annotation_email']);
            $stmt->getResource()->bindParam(25, $param['date_of_marriage']);
            $stmt->getResource()->bindParam(26, $param['parent_names']);
            $stmt->getResource()->bindParam(27, $param['first_time_in_us']);
            $stmt->getResource()->bindParam(28, $param['is_reauthorization']);
            $stmt->getResource()->bindParam(29, $param['birth_place']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will fetch the passenger manifest image
     * @param this will be an array having the search Params.
     * @return this will return a passenger manifest
     * @author Icreon Tech -SR
     */
    public function getPassengerManifest($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getPassengerManifests(?)');
            $stmt->getResource()->bindParam(1, $param['passenger_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will fetch the passenger manifest image
     * @param this will be an array having the search Params.
     * @return this will return a passenger manifest
     * @author Icreon Tech -SR
     */
    public function getPassengerManifestOther($param = array()) {
        try {
            $passenger_id = isset($param['passenger_id']) ? $param['passenger_id'] : '';
            $ship_name = isset($param['ship_name']) ? addslashes($param['ship_name']) : '';


            $where_condition = isset($param['where_condition']) ? $param['where_condition'] : '';
            // $date_arrive = isset($param['date_arrive']) ? $param['date_arrive'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getPassengerManifestsBatchnara(?,?,?)');
            $stmt->getResource()->bindParam(1, $passenger_id);
            $stmt->getResource()->bindParam(2, $ship_name);
            $stmt->getResource()->bindParam(3, $where_condition);
            //$stmt->getResource()->bindParam(3, $date_arrive);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function save the passenger record into the file
     * @param this will be an array.
     * @return this will return confirmation
     * @author Icreon Tech -SR
     */
    public function addToFile($param = array()) {
        $passenger_id = isset($param['passenger_id']) ? $param['passenger_id'] : '';
        $user_id = isset($param['user_id']) ? $param['user_id'] : '';
        $save_date = isset($param['save_date']) ? $param['save_date'] : '';
        $modified_date = isset($param['modified_date']) ? $param['modified_date'] : '';
        $title = isset($param['title']) ? $param['title'] : '';
        $search_query = isset($param['search_query']) ? $param['search_query'] : '';

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_saveSearchedPassengerDetails(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $passenger_id);
            $stmt->getResource()->bindParam(2, $user_id);
            $stmt->getResource()->bindParam(3, $title);
            $stmt->getResource()->bindParam(4, $search_query);
            $stmt->getResource()->bindParam(5, $save_date);
            $stmt->getResource()->bindParam(6, $modified_date);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function save the passenger record into the file
     * @param this will be an array.
     * @return this will return confirmation
     * @author Icreon Tech -SR
     */
    public function addToFileShip($param = array()) {
        $line_ship_id = isset($param['line_ship_id']) ? $param['line_ship_id'] : '';
        $ship_arrival_date = isset($param['ship_arrival_date']) ? $param['ship_arrival_date'] : '';
        $user_id = isset($param['user_id']) ? $param['user_id'] : '';
        $save_date = isset($param['save_date']) ? $param['save_date'] : '';
        $modified_date = isset($param['modified_date']) ? $param['modified_date'] : '';
        $title = isset($param['title']) ? $param['title'] : '';
        $search_query = isset($param['search_query']) ? $param['search_query'] : '';

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_saveSearchedShipDetails(?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $line_ship_id);
            $stmt->getResource()->bindParam(2, $user_id);
            $stmt->getResource()->bindParam(3, $title);
            $stmt->getResource()->bindParam(4, $search_query);
            $stmt->getResource()->bindParam(5, $save_date);
            $stmt->getResource()->bindParam(6, $modified_date);
            $stmt->getResource()->bindParam(7, $ship_arrival_date);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will fetch the user ship search
     * @param ship name
     * @return this will return user ship search
     * @author Icreon Tech -DG
     */
    public function getUserShipSearch($param = array()) {
        try {

            $user_id = isset($param['user_id']) ? $param['user_id'] : '';
            $line_ship_id = isset($param['line_ship_id']) ? $param['line_ship_id'] : '';
            $ship_arrival_date = isset($param['ship_arrival_date']) ? $param['ship_arrival_date'] : '';
            $stmt = $this->dbAdapter->createStatement();
            // $stmt->prepare('CALL usp_pas_getUserPassengerSearch(?,?)');
            $stmt->prepare('CALL usp_pas_getUserSavedShip(?,?,?)');
            $stmt->getResource()->bindParam(1, $user_id);
            $stmt->getResource()->bindParam(2, $line_ship_id);
            $stmt->getResource()->bindParam(3, $ship_arrival_date);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will fetch the ship manifest image
     * @param this will be an array having the search Params.
     * @return this will return a ship manifest
     * @author Icreon Tech -SR
     */
    public function getShipManifest($param = array()) {
        try {
            $shipName = (isset($param['shipName']) && !empty($param['shipName'])) ? addslashes($param['shipName']) : '';
            $dateArrive = isset($param['dateArrive']) ? $param['dateArrive'] : '';

            if (!empty($shipName) && !empty($dateArrive)) {

                $stmt = $this->dbAdapter->createStatement();
                // $stmt->prepare('CALL usp_pas_getShipManifests(?,?)');
                $stmt->prepare('CALL usp_pas_getAllShipManifests(?,?)');

                $stmt->getResource()->bindParam(1, $shipName);
                $stmt->getResource()->bindParam(2, $dateArrive);
                $result = $stmt->execute();
                $statement = $result->getResource();
                $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
                $statement->closeCursor();
                return $resultSet;
            } else {
                return $resultSet[0]['message'] = '0';
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function save the the additional image
     * @param this will be an array.
     * @return this will return confirmation
     * @author Icreon Tech -SRgetPassengerManifestAdditionalImage
     */
    public function savePassengerManifestAdditionalImage($param = array()) {
        $passenger_id = isset($param['passenger_id']) ? $param['passenger_id'] : '';
        $session_id = session_id();
        $file_name = isset($param['file_name']) ? $param['file_name'] : '';
        $user_id = isset($param['user_id']) ? $param['user_id'] : '';
        $file_path = isset($param['file_path']) ? $param['file_path'] : '';
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_savePassengerManifestAdditionalImage(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $passenger_id);
            $stmt->getResource()->bindParam(2, $session_id);
            $stmt->getResource()->bindParam(3, $file_name);
            $stmt->getResource()->bindParam(4, $user_id);
            $stmt->getResource()->bindParam(5, $file_path);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will fetch the ship manifest image
     * @param this will be an array having the search Params.
     * @return this will return a ship manifest
     * @author Icreon Tech -SR
     */
    public function getPassengerManifestAdditionalImage($param = array()) {
        try {
            $passenger_id = isset($param['passenger_id']) ? $param['passenger_id'] : '';
            $user_id = isset($param['user_id']) ? $param['user_id'] : '';
            $filename = isset($param['filename']) ? $param['filename'] : '';
            $session_id = session_id();

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getPassengerManifestAdditionalImage(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $passenger_id);
            $stmt->getResource()->bindParam(2, $user_id);
            $stmt->getResource()->bindParam(3, $session_id);
            $stmt->getResource()->bindParam(4, $filename);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function save the the additional image
     * @param this will be an array.
     * @return this will return confirmation
     * @author Icreon Tech -SR
     */
    public function removePassengerManifestAdditionalImage($param = array()) {
        $image_id = isset($param['image_id']) ? $param['image_id'] : '';
        $user_id = isset($param['user_id']) ? $param['user_id'] : '';
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_removePassengerManifestAdditionalImage(?,?)');
            $stmt->getResource()->bindParam(1, $image_id);
            $stmt->getResource()->bindParam(2, $user_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function save the passenger record into the file
     * @param this will be an array.
     * @return this will return confirmation
     * @author Icreon Tech -SR
     */
    public function addToFileManifest($param = array()) {
        $roll_nbr = isset($param['roll_nbr']) ? $param['roll_nbr'] : '';
        $frame = isset($param['frame']) ? $param['frame'] : '';
        $user_id = isset($param['user_id']) ? $param['user_id'] : '';
        $save_date = isset($param['save_date']) ? $param['save_date'] : '';
        $modified_date = isset($param['modified_date']) ? $param['modified_date'] : '';
        $ship_arrival_date = isset($param['ship_arrival_date']) ? $param['ship_arrival_date'] : '';
        $ship_id = isset($param['ship_id']) ? $param['ship_id'] : '';
        $ship_name = isset($param['ship_name']) ? $param['ship_name'] : '';
        $passengerId = isset($param['passenger_id']) ? $param['passenger_id'] : '';
        $fileName = isset($param['file_name']) ? $param['file_name'] : '';

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_saveSearchedPassengerShipManifest(?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $roll_nbr);
            $stmt->getResource()->bindParam(2, $frame);
            $stmt->getResource()->bindParam(3, $user_id);
            $stmt->getResource()->bindParam(4, $save_date);
            $stmt->getResource()->bindParam(5, $modified_date);
            $stmt->getResource()->bindParam(6, $ship_arrival_date);
            $stmt->getResource()->bindParam(7, $ship_id);
            $stmt->getResource()->bindParam(8, $ship_name);
            $stmt->getResource()->bindParam(9, $passengerId);
            $stmt->getResource()->bindParam(10, $fileName);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will fetch the passenger ethnicity
     * @param this will be an array having the search Params.
     * @return this will return a ship manifest
     * @author Icreon Tech -SR
     */
    public function getPassengerEthnicity($param = array()) {
        try {
            $passenger_id = isset($param['passengerId']) ? $param['passengerId'] : '';

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getPassengerEthnicity(?)');
            $stmt->getResource()->bindParam(1, $passenger_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get ship line
     * @return this will return a ship line name array
     * @author Icreon Tech - SR
     */
    public function getShipLine($param = array()) {
        $shipLine = isset($param['ship_line']) ? $param['ship_line'] : '';
        $limit = isset($param['limit']) ? $param['limit'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_pas_getShipline(?,?)');
        $stmt->getResource()->bindParam(1, $shipLine);
        $stmt->getResource()->bindParam(2, $limit);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
        if (!$resultSet) {
            
        }
        return $resultSet;
    }

    /**
     * This function is used to get ship builder
     * @return this will return a ship builder name array
     * @author Icreon Tech - SR
     */
    public function getShipBuilder($param = array()) {
        $shipBuilder = isset($param['ship_builder']) ? $param['ship_builder'] : '';
        $limit = isset($param['limit']) ? $param['limit'] : '';
        $stmt = $this->dbAdapter->createStatement();
        $stmt->prepare('CALL usp_pas_getShipBuilder(?,?)');
        $stmt->getResource()->bindParam(1, $shipBuilder);
        $stmt->getResource()->bindParam(2, $limit);
        $result = $stmt->execute();
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
        if (!$resultSet) {
            
        }
        return $resultSet;
    }

    /**
     * This function will fetch the all search passengers records on the bases of search parameters
     * @param this will be an array having the search Params.
     * @return this will return a passenger array
     * @author Icreon Tech -SR
     */
    public function getManifestPassengerRecord($param = array()) {
        try { 
            $manifestImage = isset($param['manifestImage']) ? $param['manifestImage'] : '';
            $sortField = isset($param['sortField']) ? $param['sortField'] : '';
            $sortOrder = isset($param['sortOrder']) ? $param['sortOrder'] : '';
            $limit = isset($param['recordLimit']) ? $param['recordLimit'] : '';
            $offset = isset($param['startIndex']) ? $param['startIndex'] : '';            
            
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getManifestPassenger(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $manifestImage);
            $stmt->getResource()->bindParam(2, $sortField);
            $stmt->getResource()->bindParam(3, $sortOrder);
            $stmt->getResource()->bindParam(4, $limit);
            $stmt->getResource()->bindParam(5, $offset);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
           // asd($resultSet);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function save the passenger record into the file
     * @param this will be an array.
     * @return this will return confirmation
     * @author Icreon Tech -SR
     */
    public function getShiplineShipListing($param = array()) {

        $lineId = isset($param['line_id']) ? $param['line_id'] : '';
        $sortField = isset($param['sort_field']) ? $param['sort_field'] : '';
        $sortOrder = isset($param['sort_order']) ? $param['sort_order'] : '';
        $limit = isset($param['limit']) ? $param['limit'] : '';
        $offset = isset($param['offset']) ? $param['offset'] : '';

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getShiplineShipList(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $lineId);
            $stmt->getResource()->bindParam(2, $sortField);
            $stmt->getResource()->bindParam(3, $sortOrder);
            $stmt->getResource()->bindParam(4, $limit);
            $stmt->getResource()->bindParam(5, $offset);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    public function getShipDeparturePort($param = array()) {
        try {
            $shipName = isset($param['ship_name']) ? addslashes($param['ship_name']) : '';
            $dateArrive = isset($param['date_arrive']) ? $param['date_arrive'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getShipDeparturePorts(?,?)');
            $stmt->getResource()->bindParam(1, $shipName);
            $stmt->getResource()->bindParam(2, $dateArrive);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function to get the passenger purchased record
     * @param this will be an array.
     * @return this will return confirmation
     * @author Icreon Tech -DG
     */
    public function getPassengerRecordPurchased($param = array()) {
        try {
            $userId = isset($param['user_id']) ? $param['user_id'] : '';
            $productTypeId = isset($param['product_type_id']) ? $param['product_type_id'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getPassengerRecordPurchased(?,?)');
            $stmt->getResource()->bindParam(1, $userId);
            $stmt->getResource()->bindParam(2, $productTypeId);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function to get the ship purchased record
     * @param this will be an array.
     * @return this will return confirmation
     * @author Icreon Tech -DG
     */
    public function getShipRecordPurchased($param = array()) {
        try {
            $userId = isset($param['user_id']) ? $param['user_id'] : '';
            $productTypeId = isset($param['product_type_id']) ? $param['product_type_id'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getShipRecordPurchased(?,?)');
            $stmt->getResource()->bindParam(1, $userId);
            $stmt->getResource()->bindParam(2, $productTypeId);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function to get the ship purchased record
     * @param this will be an array.
     * @return this will return confirmation
     * @author Icreon Tech -DG
     */
    public function getShipAdditionalCopies($param = array()) {
        try {
            $productTypeId = isset($param['product_type_id']) ? $param['product_type_id'] : '';
            $productId = isset($param['product_id']) ? $param['product_id'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getShipAdditionalCopies(?,?)');
            $stmt->getResource()->bindParam(1, $productTypeId);
            $stmt->getResource()->bindParam(2, $productId);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will fetch the passenger manifest
     * @param this will be an array having the search Params.
     * @return this will return a manifest
     * @author Icreon Tech -SR
     */
    public function getPassengeManifestImage($param = array()) {
        try {
            $companionName = isset($param['companion_name']) ? $param['companion_name'] : '';

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_passengerManifestImage(?)');
            $stmt->getResource()->bindParam(1, $firstName);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will fetch the ship manifest image
     * @param this will be an array having the search Params.
     * @return this will return a ship manifest
     * @author Icreon Tech -SR
     */
    public function getShipManifestRecord($param = array()) {
        try {
            $shipName = isset($param['shipName']) ? addslashes($param['shipName']) : '';
            $dateArrive = isset($param['dateArrive']) ? $param['dateArrive'] : '';
            $limit = isset($param['limit']) ? $param['limit'] : '';
            $startIndex = isset($param['startIndex']) ? $param['startIndex'] : '';

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getShipManifestsRecord(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $shipName);
            $stmt->getResource()->bindParam(2, $dateArrive);
            $stmt->getResource()->bindParam(3, $startIndex);
            $stmt->getResource()->bindParam(4, $limit);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            // asd($resultSet);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function to get the manifest purchased record
     * @param this will be an array.
     * @return this will return confirmation
     * @author Icreon Tech - DG
     */
    public function getManifestPurchased($param = array()) {
        try {
            $userId = isset($param['user_id']) ? $param['user_id'] : '';
            $productTypeId = isset($param['product_type_id']) ? $param['product_type_id'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getManifestRecordPurchased(?,?)');
            $stmt->getResource()->bindParam(1, $userId);
            $stmt->getResource()->bindParam(2, $productTypeId);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function to get the manifest details of purchased record
     * @param this will be an array.
     * @return this will return confirmation
     * @author Icreon Tech - AS
     * @return \Passenger\Model\this|boolean 
     */
    public function getManifestDetails($param = array()) {
        try {
            $id = isset($param['id']) ? $param['id'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getManifestRecordDetails(?)');
            $stmt->getResource()->bindParam(1, $id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function  to get theship line details for a ship
     * @param this will be an array.
     * @return this will return confirmation
     * @author Icreon Tech - AS
     * @return \Passenger\Model\this|boolean 
     */
    public function getShipLineDetails($param = array()) {
        try {
            //asd($param);
            $ship_name = isset($param['ship_name']) ? $param['ship_name'] : '';
            $ship_id = isset($param['ship_id']) ? $param['ship_id'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getShipsShipline(?,?)');
            $stmt->getResource()->bindParam(1, $ship_name);
            $stmt->getResource()->bindParam(2, $ship_id);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get ship manifest details
     * @param this will be an array.
     * @return this will return confirmation
     * @author Icreon Tech - SR
     */
    public function getShipManifestDetails($param = array()) {
        try {
            $image = isset($param['manifest']) ? $param['manifest'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getShipManifestDetails(?)');
            $stmt->getResource()->bindParam(1, $image);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will fetch the ship manifest image
     * @param this will be an array having the search Params.
     * @return this will return a ship manifest
     * @author Icreon Tech -SR
     */
    public function getFamilyShipManifest($param = array()) {
        try { 
            $shipName = (isset($param['shipName']) && !empty($param['shipName'])) ? addslashes($param['shipName']) : '';
            $dateArriveFrom = isset($param['dateArriveFrom']) ? $param['dateArriveFrom'] : '';
            $dateArriveTo = isset($param['dateArriveTo']) ? $param['dateArriveTo'] : '';
            $dataSource = isset($param['dataSource']) ? $param['dataSource'] : '';
            $fileNamePattern = isset($param['fileNamePattern']) ? $param['fileNamePattern'] : '';
            if (!empty($shipName) && !empty($dateArriveFrom)) {
                $stmt = $this->dbAdapter->createStatement();
                $stmt->prepare('CALL usp_pas_getShipManifests(?,?,?,?,?)');
                $stmt->getResource()->bindParam(1, $shipName);
                $stmt->getResource()->bindParam(2, $dateArriveFrom);
                $stmt->getResource()->bindParam(3, $dateArriveTo);
                $stmt->getResource()->bindParam(4, $dataSource);
                $stmt->getResource()->bindParam(5, $fileNamePattern);
                $result = $stmt->execute();
                $statement = $result->getResource();
                $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
                $statement->closeCursor();
                return $resultSet;
            }
            else
            {
                 return false;
            }
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * This function will fetch the ship manifest image
     * @param this will be an array having the search Params.
     * @return this will return a ship manifest
     * @author Icreon Tech -SR
     */
    public function getFamilyShipManifestListing($param = array()) {
        try {
            $startRange = (isset($param['start_range']) && !empty($param['start_range'])) ? $param['start_range'] : '';
            $endRange = isset($param['end_range']) ? $param['end_range'] : '';
            $dataSource = isset($param['data_source']) ? $param['data_source'] : '';
            $fileNamePattern = isset($param['fileNamePattern']) ? $param['fileNamePattern'] : '';

                $stmt = $this->dbAdapter->createStatement();
                $stmt->prepare('CALL usp_pas_getFamilyShipManifestsListing(?,?,?,?)');
                $stmt->getResource()->bindParam(1, $startRange);
                $stmt->getResource()->bindParam(2, $endRange);
                $stmt->getResource()->bindParam(3, $dataSource);
                $stmt->getResource()->bindParam(4, $fileNamePattern);                

                $result = $stmt->execute();
                $statement = $result->getResource();
                $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
                
                $statement->closeCursor();
                return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }    
    
   /**
     * This function will fetch the ship manifest image
     * @param this will be an array having the search Params.
     * @return this will return a ship manifest
     * @author Icreon Tech -SR
     */
    public function getShipManifestFrame($param = array()) {
        try {
            $shipName = (isset($param['shipName']) && !empty($param['shipName'])) ? addslashes($param['shipName']) : '';
            $dateArrive = isset($param['dateArrive']) ? $param['dateArrive'] : '';

            if (!empty($shipName) && !empty($dateArrive)) {

                $stmt = $this->dbAdapter->createStatement();
                $stmt->prepare('CALL usp_pas_getAllShipManifestsFrame(?,?)');

                $stmt->getResource()->bindParam(1, $shipName);
                $stmt->getResource()->bindParam(2, $dateArrive);
                $result = $stmt->execute();
                $statement = $result->getResource();
                $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
                $statement->closeCursor();
                return $resultSet;
            } else {
                return $resultSet[0]['message'] = '0';
            }
        } catch (Exception $e) {
            return false;
        }
    }
    
     public function getShiplineDetailsByLineId($param = array()) {
        try {
                $lineId = (isset($param['lineId']) && !empty($param['lineId'])) ? addslashes($param['lineId']) : '';
                $stmt = $this->dbAdapter->createStatement();
                $stmt->prepare('CALL usp_pas_getShiplineDetailsByLineId(?)');
                $stmt->getResource()->bindParam(1, $lineId);
                $result = $stmt->execute();
                $statement = $result->getResource();
                $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
                $statement->closeCursor();
                return $resultSet;

        } catch (Exception $e) {
            return false;
        }
    }    
    /**
     * This function will fetch the ship manifest image
     * @param this will be an array having the search Params.
     * @return this will return a ship manifest
     * @author Icreon Tech -SR
     */
    public function getShipManifestRecords($param = array()) {
        try {
            $shipName = (isset($param['shipName']) && !empty($param['shipName'])) ? addslashes($param['shipName']) : '';
            $dateArrive = isset($param['dateArrive']) ? $param['dateArrive'] : '';            
            $frame = (isset($param['frame']) && !empty($param['frame'])) ? addslashes($param['frame']) : '';

            if (!empty($frame) && !empty($frame)) {

                $stmt = $this->dbAdapter->createStatement();
                $stmt->prepare('CALL usp_pas_getAllShipManifestsImages(?,?,?)');
                $stmt->getResource()->bindParam(1, $shipName);
                $stmt->getResource()->bindParam(2, $dateArrive);                
                $stmt->getResource()->bindParam(3, $frame);
                $result = $stmt->execute();
                $statement = $result->getResource();
                $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
                $statement->closeCursor();
                return $resultSet;
            } else {
                return $resultSet[0]['message'] = '0';
            }
        } catch (Exception $e) {
            return false;
        }
    }
	
	public function getPassengerRecordByPID($param = array()){
		try{
			$pid = (isset($param['passenger_id']) && $param['passenger_id']!='') ? $param['passenger_id'] : '';
			if($pid!=''){
				$stmt = $this->dbAdapter->createStatement();
				$stmt->prepare('CALL usp_pas_getPassengerRecordByPID(?)');
				$stmt->getResource()->bindParam(1, $pid);
                $result = $stmt->execute();
                $statement = $result->getResource();
                $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
                $statement->closeCursor();
                return $resultSet;
			}else {
				return false;
			}
		} catch (Exception $e) {
			return false;
		}
	} 
}