<?php

/**
 * This is used for Passenger module.
 * @package    Document
 * @author     Icreon Tech -SR.
 */

namespace Passenger\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This class is used for Passenger module.
 * @author     Icreon Tech -SR.
 */
class DocumentTable {

    protected $tableGateway;
    protected $connection;
    protected $dbAdapter;

    /**
     * This is used for dafault initialization of objects.
     * @author     Icreon Tech -SR.
     */
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
        $this->connection = $this->tableGateway->getAdapter()->getDriver()->getConnection();
    }

    /**
     * This function will fetch the all search passengers records on the bases of search parameters
     * @param this will be an array having the search Params.
     * @return this will return a passenger array
     * @author Icreon Tech -SR
     */
    public function searchCrmPassenger($param = array()) {

        if (isset($param['passenger_id']) && $param['passenger_id'] != '')
            $param1 = trim($param['passenger_id']);
        else
            $param1 = '';
        if (isset($param['ship']) && $param['ship'] != '')
            $param2 = trim($param['ship']);
        else
            $param2 = '';
        if (isset($param['first_name']))
            $param3 = $param['first_name'];
        else
            $param3 = '';
        if (isset($param['last_name']))
            $param4 = $param['last_name'];
        else
            $param4 = '';
        if (isset($param['series']))
            $param5 = $param['series'];
        else
            $param5 = '';
        if (isset($param['frame']))
            $param6 = $param['frame'];
        else
            $param6 = '';
        if (isset($param['roll_number']))
            $param7 = $param['roll_number'];
        else
            $param7 = '';
        if (isset($param['arrival_year']) && $param['arrival_year'] != '') {
            $arr_year_arr = explode(' - ', $param['arrival_year']);
            $param8 = $arr_year_arr[0] . '-01-01 00:00:00';
            $param9 = $arr_year_arr[1] . '-12-31 23:59:59';
        } else {
            $param8 = '';
            $param9 = '';
        }
        if (isset($param['day_arrival']) && $param['day_arrival'] != '')
            $param10 = trim($param['day_arrival']);
        else
            $param10 = '';

        if (isset($param['month_arrival']) && $param['month_arrival'] != '')
            $param11 = trim($param['month_arrival']);
        else
            $param11 = '';


        if (isset($param['birth_year']) && $param['birth_year'] != '') {
            $birth_year_arr = explode(' - ', $param['birth_year']);
            $param12 = $birth_year_arr[0];
            $param13 = $birth_year_arr[1];
        } else {
            $param12 = '';
            $param13 = '';
        }

        if (isset($param['current_age_range']) && $param['current_age_range'] != '') {
            $age_arr = explode(' - ', $param['current_age_range']);
            $param14 = $age_arr[0];
            $param15 = $age_arr[1];
        } else {
            $param14 = '';
            $param15 = '';
        }

        if (isset($param['gender']) && $param['gender'] != '')
            $param16 = trim($param['gender']);
        else
            $param16 = '';

        if (isset($param['maritial_status']) && $param['maritial_status'] != '')
            $param17 = trim($param['maritial_status']);
        else
            $param17 = '';

        if (isset($param['dept_port']) && $param['dept_port'] != '')
            $param18 = trim($param['dept_port']);
        else
            $param18 = '';

        if (isset($param['arrival_port']) && $param['arrival_port'] != '')
            $param19 = trim($param['arrival_port']);
        else
            $param19 = '';


        if (isset($param['ethnicity']) && $param['ethnicity'] != '')
            $param20 = trim($param['ethnicity']);
        else
            $param20 = '';


        if (isset($param['last_residence']) && $param['last_residence'] != '')
            $param21 = trim($param['last_residence']);
        else
            $param21 = '';

        if (isset($param['startIndex']) && $param['startIndex'] != '')
            $param22 = trim($param['startIndex']);
        else
            $param22 = '';

        if (isset($param['recordLimit']) && $param['recordLimit'] != '')
            $param23 = trim($param['recordLimit']);
        else
            $param23 = '';

        if (isset($param['sortField']) && $param['sortField'] != '')
            $param24 = trim($param['sortField']);
        else
            $param24 = '';

        if (isset($param['sortOrder']) && $param['sortOrder'] != '')
            $param25 = trim($param['sortOrder']);
        else
            $param25 = '';

        if (isset($param['getRecordsCount']) && $param['getRecordsCount'] != '')
            $param26 = trim($param['getRecordsCount']);
        else
            $param26 = '';

        $result = $this->connection->execute("CALL usp_pas_crmSearchPassenger('" . $param1 . "','" . $param2 . "','" . $param3 . "','" . $param4 . "','" . $param5 . "','" . $param6 . "','" . $param7 . "','" . $param8 . "','" . $param9 . "','" . $param10 . "','" . $param11 . "','" . $param12 . "','" . $param13 . "','" . $param14 . "','" . $param15 . "','" . $param16 . "','" . $param17 . "','" . $param18 . "','" . $param19 . "','" . $param20 . "','" . $param21 . "','" . $param22 . "','" . $param23 . "','" . $param24 . "','" . $param25 . "','" . $param26 . "')");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

        return $resultSet;
    }

    /**
     * This function is used to get all passenger count
     * @return this will return records count
     * @author Icreon Tech - SR
     */
    public function searchCrmPassengerCount() {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_crmSearchPassengerCount()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_OBJ);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get all ship count
     * @return this will return records count
     * @author Icreon Tech - SR
     */
    public function searchCrmShipCount() {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_crmSearchShipCount()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_OBJ);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    public function getTotalCount() {

        $result = $this->connection->execute("CALL usp_com_getRecordCount()");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_OBJ);
        return $resultSet;
    }

    /**
     * This function is used to get all arrival port
     * @param array type to get the arrival port.
     * @return this will return a arrival port array
     * @author Icreon Tech - SR
     */
    public function getArrivalPort($param = array()) {
        if (isset($param['ship_arrival_port']) && $param['ship_arrival_port'] != '')
            $ship_arrival_port = $param['ship_arrival_port'];
        else
            $ship_arrival_port = '';
        $result = $this->connection->execute("CALL usp_pas_getArrivalPort('" . $ship_arrival_port . "')");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!$resultSet) {
            throw new \Exception("Could not find record.");
        }
        return $resultSet;
    }

    /**
     * This function will save the search title into the table for crm passenger search
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech -SR
     */
    public function saveCrmPassengerSearch($param = array()) {
        try {
            $result = $this->connection->execute("CALL usp_pas_saveCrmPassengerSearch('" . $param['crm_user_id'] . "','" . $param['search_name'] . "','" . $param['search_query'] . "','" . $param['isActive'] . "','" . $param['isDelete'] . "','" . $param['added_date'] . "')");
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update the search title into the table for crm passenger search
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech -SR
     */
    public function updateCrmPassengerSearch($param = array()) {
        try {

            $result = $this->connection->execute("CALL usp_pas_updateCrmPassengerSearch('" . $param['crm_user_id'] . "','" . $param['search_name'] . "','" . $param['search_query'] . "','" . $param['isActive'] . "','" . $param['isDelete'] . "','" . $param['modifiend_date'] . "','" . $param['search_id'] . "')");
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will delete the saved crm search titles
     * @param this will be an array having the params.
     * @return this will return a confirmation
     * @author Icreon Tech -SR
     */
    public function deleteCrmPassengerSearch($param = array()) {
        try {

            $result = $this->connection->execute("CALL usp_pas_deleteCrmPassengerSearch('" . $param['crm_user_id'] . "','" . $param['isDelete'] . "','" . $param['modifiend_date'] . "','" . $param['search_id'] . "')");
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the saved search listing for CRM passenger search
     * @param this will be an array.
     * @return this will return a title of the saved search
     * @author Icreon Tech -SR
     */
    public function getCrmSavedSearch($param = array()) {
        $result = $this->connection->execute("CALL usp_pas_getCrmSavedSearch('" . $param['crm_user_id'] . "','" . $param['isActive'] . "','" . $param['passengerSearchId'] . "')");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        return $resultSet;
    }

    /**
     * This function is used to get all Ethnicity
     * @param array type to get the Ethnicity.
     * @return this will return a Ethnicity array
     * @author Icreon Tech - SR
     */
    public function getEthnicity($param = array()) {

        $result = $this->connection->execute("CALL usp_com_getEthnicity('')");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
        $statement->closeCursor();
        if (!$resultSet) {
            throw new \Exception("Could not find record.");
        }
        return $resultSet;
    }

    /**
     * This function will update the passenger information
     * @param this will be an array.
     * @return this will return a confirmation
     * @author Icreon Tech -SR
     */
    public function updateCrmPassenger($param = array()) {
        try {

            $fileName = isset($param['FILENAME']) ? $param['FILENAME'] : '';
            $imageAutoId = isset($param['MANIMAGE_AUTOID']) ? $param['MANIMAGE_AUTOID'] : '';
            $refLine = isset($param['ref_line']) ? $param['ref_line'] : '';
            $portOfDeparture = isset($param['port_of_departure']) ? $param['port_of_departure'] : '';
              
              
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_updatePassenger(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['passenger_id']);
            $stmt->getResource()->bindParam(2, $param['batch']);
            $stmt->getResource()->bindParam(3, $param['ref_page']);
            $stmt->getResource()->bindParam(4, $param['arrival_date']);
            $stmt->getResource()->bindParam(5, $param['last_name']);
            $stmt->getResource()->bindParam(6, $param['first_name']);
            $stmt->getResource()->bindParam(7, $param['birth_year']);
            $stmt->getResource()->bindParam(8, $param['gender']);
            $stmt->getResource()->bindParam(9, $param['maritial_status']);
            $stmt->getResource()->bindParam(10, $param['residence']);
            $stmt->getResource()->bindParam(11, $param['ship']);
            $stmt->getResource()->bindParam(12, $param['ship_name_iss']);
            $stmt->getResource()->bindParam(13, $param['ship_arrival_date']);
            $stmt->getResource()->bindParam(14, $param['nationality']);
            $stmt->getResource()->bindParam(15, $param['us_citizen']);
            $stmt->getResource()->bindParam(16, $param['crew_member']);
            $stmt->getResource()->bindParam(17, $param['soundex_lname']);
            $stmt->getResource()->bindParam(18, $param['print_age']);
            $stmt->getResource()->bindParam(19, $param['age_arrival']);
            $stmt->getResource()->bindParam(20, $param['place_of_birth']);
            $stmt->getResource()->bindParam(21, $fileName);
            $stmt->getResource()->bindParam(22, $imageAutoId);
            $stmt->getResource()->bindParam(23, $refLine);
            $stmt->getResource()->bindParam(24, $portOfDeparture);


            $result = $stmt->execute();
            $statement = $result->getResource();
            // $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            // print_R($resultSet);
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update the passenger information in main image table
     * @param this will be an array.
     * @return this will return a confirmation
     * @author Icreon Tech -SR
     */
    public function updateCrmPassengerMainImage($param = array()) {
        try {

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_updatePassengerMainImage(?,?,?)');
            $stmt->getResource()->bindParam(1, $param['mainimage_autoid']);
            $stmt->getResource()->bindParam(2, $param['roll_number']);
            $stmt->getResource()->bindParam(3, $param['frame']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            // $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            // print_R($resultSet);
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will save the search title into the table for crm search annotation
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech -SR
     */
    public function saveCrmAnnotationSearch($param = array()) {

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_saveCrmAnnotationSearch(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $stmt->getResource()->bindParam(2, $param['search_name']);
            $stmt->getResource()->bindParam(3, $param['search_query']);
            $stmt->getResource()->bindParam(4, $param['isActive']);
            $stmt->getResource()->bindParam(5, $param['isDelete']);
            $stmt->getResource()->bindParam(6, $param['added_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the saved search listing for annotation
     * @param this will be an array.
     * @return this will return a title of the saved search
     * @author Icreon Tech -SR
     */
    public function getCrmAnnotationSavedSearch($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getCrmAnnotationSavedSearch(?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $stmt->getResource()->bindParam(2, $param['isActive']);
            $stmt->getResource()->bindParam(3, $param['searchId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update the search title into the table for crm passenger search
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech -SR
     */
    public function updateCrmAnnotationSearch($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_updateCrmAnnotationSearch(?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $stmt->getResource()->bindParam(2, $param['search_name']);
            $stmt->getResource()->bindParam(3, $param['search_query']);
            $stmt->getResource()->bindParam(4, $param['isActive']);
            $stmt->getResource()->bindParam(5, $param['isDelete']);
            $stmt->getResource()->bindParam(6, $param['modifiend_date']);
            $stmt->getResource()->bindParam(7, $param['search_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will delete the saved crm search titles
     * @param this will be an array having the params.
     * @return this will return a confirmation
     * @author Icreon Tech -SR
     */
    public function deleteCrmAnnotationSearch($param = array()) {

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_deleteCrmAnnotationSearch(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $stmt->getResource()->bindParam(2, $param['isDelete']);
            $stmt->getResource()->bindParam(3, $param['modifiend_date']);
            $stmt->getResource()->bindParam(4, $param['search_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will save the search title into the table for crm ship search
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech -SR
     */
    public function saveCrmShipSearch($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_saveCrmShipSearch(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $stmt->getResource()->bindParam(2, $param['search_name']);
            $stmt->getResource()->bindParam(3, $param['search_query']);
            $stmt->getResource()->bindParam(4, $param['isActive']);
            $stmt->getResource()->bindParam(5, $param['isDelete']);
            $stmt->getResource()->bindParam(6, $param['added_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update the search title into the table for crm passenger search
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech -SR
     */
    public function updateCrmShipSearch($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_updateCrmShipSearch(?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $stmt->getResource()->bindParam(2, $param['search_name']);
            $stmt->getResource()->bindParam(3, $param['search_query']);
            $stmt->getResource()->bindParam(4, $param['isActive']);
            $stmt->getResource()->bindParam(5, $param['isDelete']);
            $stmt->getResource()->bindParam(6, $param['modifiend_date']);
            $stmt->getResource()->bindParam(7, $param['search_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the saved search listing for ship
     * @param this will be an array.
     * @return this will return a title of the saved search
     * @author Icreon Tech -SR
     */
    public function getCrmShipSavedSearch($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getCrmShipSavedSearch(?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $stmt->getResource()->bindParam(2, $param['isActive']);
            $stmt->getResource()->bindParam(3, $param['searchId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the the engine type listing
     * @param this will be an array.
     * @return this will return a engine type 
     * @author Icreon Tech -SR
     */
    public function getEngine($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getEngineType()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the ship records
     * @param this will be an array.
     * @return this will return search result of ship 
     * @author Icreon Tech -SR
     */
    public function searchCrmShip($param = array()) {

        try {
            if(!empty($param['ship_name']) || !empty($param['ship_id'])) {
            
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_crmSearchShip(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['ship_id']);
            $stmt->getResource()->bindParam(2, $param['ship_name']);
            $stmt->getResource()->bindParam(3, $param['engine_type']);
            $stmt->getResource()->bindParam(4, $param['builder']);
            $stmt->getResource()->bindParam(5, $param['arrival_year_start']);
            $stmt->getResource()->bindParam(6, $param['arrival_year_end']);
            $stmt->getResource()->bindParam(7, $param['month_arrival']);
            $stmt->getResource()->bindParam(8, $param['day_arrival']);
            $stmt->getResource()->bindParam(9, $param['build_from_date']);
            $stmt->getResource()->bindParam(10, $param['build_to_date']);
            $stmt->getResource()->bindParam(11, $param['shipline_name']);
            $stmt->getResource()->bindParam(12, $param['scrap_from_date']);
            $stmt->getResource()->bindParam(13, $param['scrap_to_date']);
            $stmt->getResource()->bindParam(14, $param['startIndex']);
            $stmt->getResource()->bindParam(15, $param['recordLimit']);
            $stmt->getResource()->bindParam(16, $param['sortField']);
            $stmt->getResource()->bindParam(17, $param['sortOrder']);

            $stmt->getResource()->bindParam(18, $param['groupby']);
            $stmt->getResource()->bindParam(19, $param['getRecordsCount']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            //echo "<pre>";
            //print_r($resultSet);
            //die;
            $statement->closeCursor();

            return $resultSet;
            }
            else {
                return false;
                
            }
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the ship details
     * @param this will be an array.
     * @return this will return search result of ship 
     * @author Icreon Tech -SR
     */
    public function getShipTripDetails($param = array()) {

        try {
            $shipId = isset($param['ship_id']) ? $param['ship_id'] : '';
            $shiplineId = isset($param['shiplineId']) ? $param['shiplineId'] : '';
            $startIndex = isset($param['startIndex']) ? $param['startIndex'] : '';
            $recordLimit = isset($param['recordLimit']) ? $param['recordLimit'] : '';
            $sortField = isset($param['sortField']) ? $param['sortField'] : '';
            $sortOrder = isset($param['sortOrder']) ? $param['sortOrder'] : '';
            $shipName = isset($param['shipName']) ? $param['shipName'] : '';

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_crmShipTripDetails(?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $shipId);
            $stmt->getResource()->bindParam(2, $shiplineId);
            $stmt->getResource()->bindParam(3, $startIndex);
            $stmt->getResource()->bindParam(4, $recordLimit);
            $stmt->getResource()->bindParam(5, $sortField);
            $stmt->getResource()->bindParam(6, $sortOrder);
            $stmt->getResource()->bindParam(7, $shipName);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the shipLine ship listing
     * @param this will be an array.
     * @return this will return search result of ship 
     * @author Icreon Tech -SR
     */
    public function getShipLineShipList($param = array()) {
        try {

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_crmShipLineShipListing(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['LINEID']);
            $stmt->getResource()->bindParam(2, $param['startIndex']);
            $stmt->getResource()->bindParam(3, $param['recordLimit']);
            $stmt->getResource()->bindParam(4, $param['sortField']);
            $stmt->getResource()->bindParam(5, $param['sortOrder']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the ship passenger
     * @param this will be an array.
     * @return this will return search result of passenger 
     * @author Icreon Tech -SR
     */
    public function getShipPassengerList($param = array()) {
        try {
            $shipName = isset($param['ship_name']) ? addslashes($param['ship_name']) : '';

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_crmShipPassengerList(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $shipName);
            $stmt->getResource()->bindParam(2, $param['DATE_ARRIVE']);
            $stmt->getResource()->bindParam(3, $param['startIndex']);
            $stmt->getResource()->bindParam(4, $param['recordLimit']);
            $stmt->getResource()->bindParam(5, $param['sortField']);
            $stmt->getResource()->bindParam(6, $param['sortOrder']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the ship passenger
     * @param this will be an array.
     * @return this will return search result of passenger 
     * @author Icreon Tech -SR
     */
    public function getShipPassengerListCount($param = array()) {
        try {
            $shipName = addslashes($param['ship_name']);
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_crmShipPassengerListCount(?,?)');
            $stmt->getResource()->bindParam(1, $shipName);
            $stmt->getResource()->bindParam(2, $param['DATE_ARRIVE']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the ship passenger
     * @param this will be an array.
     * @return this will return search result of passenger 
     * @author Icreon Tech -SR
     */
    public function searchCrmShipImage($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_crmShipImage(?,?)');
            $stmt->getResource()->bindParam(1, $param['ship_id']);
            $stmt->getResource()->bindParam(2, $param['asset_for']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to update the crm ship
     * @param this will be an array.
     * @return this will return true
     * @author Icreon Tech -SR
     */
    public function updateCrmShip($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_updateCrmShip(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['ship_id']);
            $stmt->getResource()->bindParam(2, $param['shipline_id']);
            $stmt->getResource()->bindParam(3, $param['name_when_built']);
            $stmt->getResource()->bindParam(4, $param['gross_weight']);
            $stmt->getResource()->bindParam(5, $param['length']);
            $stmt->getResource()->bindParam(6, $param['beam']);
            $stmt->getResource()->bindParam(7, $param['engine_type']);
            $stmt->getResource()->bindParam(8, $param['num_screws']);
            $stmt->getResource()->bindParam(9, $param['scrapped_country']);
            $stmt->getResource()->bindParam(10, $param['builder']);
            $stmt->getResource()->bindParam(11, $param['service_speed_knots']);
            $stmt->getResource()->bindParam(12, $param['scrapped_month']);
            $stmt->getResource()->bindParam(13, $param['scrapped_day']);
            $stmt->getResource()->bindParam(14, $param['build_year']);
            $stmt->getResource()->bindParam(15, $param['scrapped_year']);
            $stmt->getResource()->bindParam(16, $param['build_country']);
            $stmt->getResource()->bindParam(17, $param['ship_name_iss']);
            $stmt->getResource()->bindParam(18, $param['updated_on']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            // $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            // print_R($resultSet);
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update trip information
     * @param this will be an array.
     * @return this will return a confirmation
     * @author Icreon Tech -SR
     */
    public function updateTripInformation($param = array()) {
        try {

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_updateTripInformation(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['ArrivalDate']);
            $stmt->getResource()->bindParam(2, $param['PortofDeparture']);
            $stmt->getResource()->bindParam(3, $param['ID']);
            $stmt->getResource()->bindParam(4, $param['DateArrive']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            //$resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update crm passenger ship information
     * @param this will be an array.
     * @return this will return a confirmation
     * @author Icreon Tech -SR
     */
    public function updateCrmPassengerShip($param = array()) {
        try {

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_updateCrmPassengerShip(?,?)');
            $stmt->getResource()->bindParam(1, $param['oldShipName']);
            $stmt->getResource()->bindParam(2, $param['newShipName']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            //$resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will delete the saved crm search titles
     * @param this will be an array having the params.
     * @return this will return a confirmation
     * @author Icreon Tech -SR
     */
    public function deleteCrmShipSearch($param = array()) {
        try {

            $result = $this->connection->execute("CALL usp_pas_deleteCrmShipSearch('" . $param['crm_user_id'] . "','" . $param['isDelete'] . "','" . $param['modifiend_date'] . "','" . $param['search_id'] . "')");
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to update the crm ship Line table
     * @param this will be an array.
     * @return this will return true
     * @author Icreon Tech -SR
     */
    public function updateCrmShipInfoShipLine($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_updateCrmShipInfoShipLine(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['ship_id']);
            $stmt->getResource()->bindParam(2, $param['LINEID']);
            $stmt->getResource()->bindParam(3, $param['ship_name']);
            $stmt->getResource()->bindParam(4, $param['updated_on']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            //$resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            // print_R($resultSet);
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to update the crm ship edited image flage
     * @param this will be an array.
     * @return this will return true
     * @author Icreon Tech -SR
     */
    public function updateCrmShipImage($param = array()) {

        try {
            $stmt = $this->dbAdapter->createStatement();

            $isEditedImage = isset($param['is_edited_image']) ? trim($param['is_edited_image']) : '';
            $shipId = isset($param['ship_id']) ? trim($param['ship_id']) : '';
            $assetfor = isset($param['assetfor']) ? $param['assetfor'] : '';

            $stmt->prepare('CALL usp_pas_updateCrmShipImageEditFlag(?,?,?)');
            $stmt->getResource()->bindParam(1, $isEditedImage);
            $stmt->getResource()->bindParam(2, $shipId);
            $stmt->getResource()->bindParam(3, $assetfor);

            $result = $stmt->execute();
            $statement = $result->getResource();
            //$resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            //asd($resultSet);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to update the crm ship Line table
     * @param this will be an array.
     * @return this will return true
     * @author Icreon Tech -SR
     */
    public function updateCrmShipLine($param = array()) {

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_updateCrmShipLine(?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['shipline_id']);
            $stmt->getResource()->bindParam(2, $param['line_id']);
            $stmt->getResource()->bindParam(3, $param['name']);
            $stmt->getResource()->bindParam(4, $param['start_year']);
            $stmt->getResource()->bindParam(5, $param['end_year']);
            $stmt->getResource()->bindParam(6, $param['total_pass']);
            $stmt->getResource()->bindParam(7, $param['asset_id']);
            $stmt->getResource()->bindParam(8, $param['flag']);
            $stmt->getResource()->bindParam(9, $param['home_city']);
            $stmt->getResource()->bindParam(10, $param['home_country']);
            $stmt->getResource()->bindParam(11, $param['updated_on']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            //$resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            //asd($resultSet);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will retrieve shipline list
     * @param this will be an array.
     * @return this will return a confirmation
     * @author Icreon Tech - AS
     */
    public function searchShipline($param = array()) {

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getCrmShiplineSearch(?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['line_name_id']);
            $stmt->getResource()->bindParam(2, $param['line_name']);
            $stmt->getResource()->bindParam(3, $param['city']);
            $stmt->getResource()->bindParam(4, $param['country']);
            $stmt->getResource()->bindParam(5, $param['start_year_date_from']);
            $stmt->getResource()->bindParam(6, $param['start_year_date_to']);
            $stmt->getResource()->bindParam(7, $param['end_year_date_from']);
            $stmt->getResource()->bindParam(8, $param['end_year_date_to']);
            $stmt->getResource()->bindParam(9, $param['startIndex']);
            $stmt->getResource()->bindParam(10, $param['recordLimit']);
            $stmt->getResource()->bindParam(11, $param['sortField']);
            $stmt->getResource()->bindParam(12, $param['sortOrder']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    public function searchCrmShipline($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_crmShiplineInfo(?)');
            $stmt->getResource()->bindParam(1, $param['shipline_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will retrieve annotation and corrections
     * @param this will be an array.
     * @return this will return a confirmation
     * @author Icreon Tech - SR
     */
    public function searchCrmAnnotationCorrection($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_crmSearchAnnotationCorrection(?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['passenger_id']);
            $stmt->getResource()->bindParam(2, $param['passenger_name']);
            $stmt->getResource()->bindParam(3, $param['status']);
            $stmt->getResource()->bindParam(4, $param['contact_name']);
            $from_date = '';
            $to_date = '';
            if (isset($param['from_date']) && $param['from_date'] != '')
                $from_date = $param['from_date'];
            if (isset($param['to_date']) && $param['to_date'] != '')
                $to_date = $param['to_date'];
            $stmt->getResource()->bindParam(5, $from_date);
            $stmt->getResource()->bindParam(6, $to_date);
            $stmt->getResource()->bindParam(7, $param['type']);
            $stmt->getResource()->bindParam(8, $param['startIndex']);
            $stmt->getResource()->bindParam(9, $param['recordLimit']);
            $stmt->getResource()->bindParam(10, $param['sortField']);
            $stmt->getResource()->bindParam(11, $param['sortOrder']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();            
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will save the used added annotation and corrections
     * @param this will be an array.
     * @return this will return a confirmation
     * @author Icreon Tech - SR
     */
    function searchUserAddedAnnotationCorrectionPassenger($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_crmUserAddedAnnotationCorrectionPassenger(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');

            $stmt->getResource()->bindParam(1, $param['contact_name_id']);
            $stmt->getResource()->bindParam(2, $param['passenger_id']);
            $stmt->getResource()->bindParam(3, $param['first_name']);
            $stmt->getResource()->bindParam(4, $param['last_name']);
            $stmt->getResource()->bindParam(5, $param['p_year_of_birth_start']);
            $stmt->getResource()->bindParam(6, $param['p_year_of_birth_end']);
            $stmt->getResource()->bindParam(7, $param['p_current_age_start']);
            $stmt->getResource()->bindParam(8, $param['p_current_age_end']);
            $stmt->getResource()->bindParam(9, $param['type']);
            $stmt->getResource()->bindParam(10, $param['marital_status']);
            $stmt->getResource()->bindParam(11, $param['gender']);
            $stmt->getResource()->bindParam(12, $param['p_arrival_year_start']);
            $stmt->getResource()->bindParam(13, $param['p_arrival_year_end']);
            $stmt->getResource()->bindParam(14, $param['arrival_month']);
            $stmt->getResource()->bindParam(15, $param['arrival_day']);
            $stmt->getResource()->bindParam(16, $param['last_residence']);
            $stmt->getResource()->bindParam(17, $param['dept_port']);
            $stmt->getResource()->bindParam(18, $param['arrival_port']);
            $stmt->getResource()->bindParam(19, $param['ethnicity']);
            $stmt->getResource()->bindParam(20, $param['ship']);
            $stmt->getResource()->bindParam(21, $param['annotation_correction_id']);
            $stmt->getResource()->bindParam(22, $param['startIndex']);
            $stmt->getResource()->bindParam(23, $param['recordLimit']);
            $stmt->getResource()->bindParam(24, $param['sortField']);
            $stmt->getResource()->bindParam(25, $param['sortOrder']);


            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
//asd($resultSet);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will save the search title into the table for crm campaign search
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech -AS
     */
    public function saveCrmShiplineSearch($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_saveCrmShiplineSearch(?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $stmt->getResource()->bindParam(2, $param['search_name']);
            $stmt->getResource()->bindParam(3, $param['search_query']);
            $stmt->getResource()->bindParam(4, $param['isActive']);
            $stmt->getResource()->bindParam(5, $param['isDelete']);
            $stmt->getResource()->bindParam(6, $param['added_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get the saved search listing for shipline
     * @param this will be an array.
     * @return this will return a title of the saved search
     * @author Icreon Tech - AS
     */
    public function getCrmShiplineSavedSearch($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_getCrmShiplineSavedSearch(?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $stmt->getResource()->bindParam(2, $param['isActive']);
            $stmt->getResource()->bindParam(3, $param['campaignSearchId']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();

            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will delete the saved crm search titles
     * @param this will be an array having the params.
     * @return this will return a confirmation
     * @author Icreon Tech -AS
     */
    public function deleteCrmShiplineSearch($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_cam_deleteCrmShiplineSearch(?,?,?)');
            $stmt->getResource()->bindParam(1, $param['search_id']);
            $stmt->getResource()->bindParam(2, $param['isDelete']);
            $stmt->getResource()->bindParam(3, $param['modifiend_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            //$resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will update the search title into the table for crm passenger search
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech - AS
     */
    public function updateCrmShiplineSearch($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_updateCrmShiplineSearch(?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['crm_user_id']);
            $stmt->getResource()->bindParam(2, $param['search_name']);
            $stmt->getResource()->bindParam(3, $param['search_query']);
            $stmt->getResource()->bindParam(4, $param['isActive']);
            $stmt->getResource()->bindParam(5, $param['isDelete']);
            $stmt->getResource()->bindParam(6, $param['modifiend_date']);
            $stmt->getResource()->bindParam(7, $param['search_id']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will insert the annotation and correction
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech - AS
     */
    public function insertAnnotation($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_insertCrmAnnotationCorrection(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['first_name']);
            $stmt->getResource()->bindParam(2, $param['last_name']);
            $stmt->getResource()->bindParam(3, $param['nationality']);
            $stmt->getResource()->bindParam(4, $param['last_residence']);
            $stmt->getResource()->bindParam(5, $param['arrival_age']);
            $stmt->getResource()->bindParam(6, $param['gender']);
            $stmt->getResource()->bindParam(7, $param['marital_status']);
            $stmt->getResource()->bindParam(8, $param['ship']);
            $stmt->getResource()->bindParam(9, $param['port_departure']);
            $stmt->getResource()->bindParam(10, $param['line']);
            $stmt->getResource()->bindParam(11, $param['birth_date']);
            $stmt->getResource()->bindParam(12, $param['death_date']);
            $stmt->getResource()->bindParam(13, $param['occupation']);
            $stmt->getResource()->bindParam(14, $param['spouse']);
            $stmt->getResource()->bindParam(15, $param['children']);
            $stmt->getResource()->bindParam(16, $param['us_relatives']);
            $stmt->getResource()->bindParam(17, $param['us_residence']);
            $stmt->getResource()->bindParam(18, $param['military_experience']);
            $stmt->getResource()->bindParam(19, $param['religious_community']);
            //    $stmt->getResource()->bindParam(20, $param['annotation_correction_id']);
            $stmt->getResource()->bindParam(20, $param['activity_type']);
            $stmt->getResource()->bindParam(21, $param['added_date']);
            $stmt->getResource()->bindParam(22, $param['added_by']);
            $stmt->getResource()->bindParam(23, $param['passenger_id']);
            $stmt->getResource()->bindParam(24, $param['arrival_date']);
            $stmt->getResource()->bindParam(25, $param['user_id']);

            $stmt->getResource()->bindParam(26, $param['date_of_marriage']);
            $stmt->getResource()->bindParam(27, $param['parent_names']);
            $stmt->getResource()->bindParam(28, $param['first_time_in_us']);
            $stmt->getResource()->bindParam(29, $param['status']);
            $stmt->getResource()->bindParam(30, $param['birth_place']);



            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to update the crm ship Line table
     * @param this will be an array.
     * @return this will return true
     * @author Icreon Tech -SR
     */
    public function updateCrmBulkship($param = array()) {

        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_updateBulkShip(?,?)');
            $stmt->getResource()->bindParam(1, $param['old_ship_name']);
            $stmt->getResource()->bindParam(2, $param['new_ship_name']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to update the annotation and correction table.
     * @param this will be an array.
     * @return this will return true
     * @author Icreon Tech -SR
     */
    public function updateAnnotationPending($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_updateCrmAnnotationCorrection(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['birth_date']);
            $stmt->getResource()->bindParam(2, $param['death_date']);
            $stmt->getResource()->bindParam(3, $param['occupation']);
            $stmt->getResource()->bindParam(4, $param['spouse']);
            $stmt->getResource()->bindParam(5, $param['children']);
            $stmt->getResource()->bindParam(6, $param['us_relatives']);
            $stmt->getResource()->bindParam(7, $param['us_residence']);
            $stmt->getResource()->bindParam(8, $param['military_experience']);
            $stmt->getResource()->bindParam(9, $param['religious_community']);
            $stmt->getResource()->bindParam(10, $param['annotation_correction_id']);
            $stmt->getResource()->bindParam(11, $param['modified_by']);
            $stmt->getResource()->bindParam(12, $param['modified_date']);
            $stmt->getResource()->bindParam(13, $param['note']);
            $stmt->getResource()->bindParam(14, $param['email_send']);
            $stmt->getResource()->bindParam(15, $param['status']);
            $stmt->getResource()->bindParam(16, $param['activity_type']);
            $stmt->getResource()->bindParam(17, $param['user_id']);

            $stmt->getResource()->bindParam(18, $param['date_of_marriage']);
            $stmt->getResource()->bindParam(19, $param['parent_names']);
            $stmt->getResource()->bindParam(20, $param['first_time_in_us']);

            $stmt->getResource()->bindParam(21, $param['first_name']);
            $stmt->getResource()->bindParam(22, $param['last_name']);
            $stmt->getResource()->bindParam(23, $param['nationality']);
            $stmt->getResource()->bindParam(24, $param['last_residence']);
            $stmt->getResource()->bindParam(25, $param['arrival_age']);
            $stmt->getResource()->bindParam(26, $param['gender']);
            $stmt->getResource()->bindParam(27, $param['marital_status']);
            $stmt->getResource()->bindParam(28, $param['ship']);
            $stmt->getResource()->bindParam(29, $param['port_departure']);
            $stmt->getResource()->bindParam(30, $param['line']);
            $stmt->getResource()->bindParam(31, $param['arrival_date']);
            $stmt->getResource()->bindParam(32, $param['is_reauthorization']);
            $stmt->getResource()->bindParam(33, $param['birth_place']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            //$resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to update the annotation and correction table.
     * @param this will be an array.
     * @return this will return true
     * @author Icreon Tech -SR
     */
    public function updateCorrectionPending($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_updateCrmCorrection(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['first_name']);
            $stmt->getResource()->bindParam(2, $param['last_name']);
            $stmt->getResource()->bindParam(3, $param['nationality']);
            $stmt->getResource()->bindParam(4, $param['last_residence']);
            $stmt->getResource()->bindParam(5, $param['arrival_age']);
            $stmt->getResource()->bindParam(6, $param['gender']);
            $stmt->getResource()->bindParam(7, $param['marital_status']);
            $stmt->getResource()->bindParam(8, $param['ship']);
            $stmt->getResource()->bindParam(9, $param['port_departure']);
            $stmt->getResource()->bindParam(10, $param['annotation_correction_id']);
            $stmt->getResource()->bindParam(11, $param['modified_by']);
            $stmt->getResource()->bindParam(12, $param['modified_date']);
            $stmt->getResource()->bindParam(13, $param['note']);
            $stmt->getResource()->bindParam(14, $param['email_send']);
            $stmt->getResource()->bindParam(15, $param['status']);
            $stmt->getResource()->bindParam(16, $param['line']);
            $stmt->getResource()->bindParam(17, $param['activity_type']);

            $stmt->getResource()->bindParam(18, $param['first_name_status']);
            $stmt->getResource()->bindParam(19, $param['last_name_status']);
            $stmt->getResource()->bindParam(20, $param['nationality_status']);
            $stmt->getResource()->bindParam(21, $param['last_residence_status']);
            $stmt->getResource()->bindParam(22, $param['age_at_arrival_status']);
            $stmt->getResource()->bindParam(23, $param['arrival_date_status']);
            $stmt->getResource()->bindParam(24, $param['gender_status']);
            $stmt->getResource()->bindParam(25, $param['maritial_approve_status']);
            $stmt->getResource()->bindParam(26, $param['ship_name_status']);
            $stmt->getResource()->bindParam(27, $param['departure_port_name_status']);
            $stmt->getResource()->bindParam(28, $param['line_no_status']);
            $stmt->getResource()->bindParam(29, $param['arrival_date']);
            $stmt->getResource()->bindParam(30, $param['user_id']);
            $stmt->getResource()->bindParam(31, $param['birth_place']);
            $stmt->getResource()->bindParam(32, $param['birth_place_status']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            //$resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to update the main ANNOTATION TABLE.
     * @param this will be an array.
     * @return this will return true
     * @author Icreon Tech -SR
     */
    public function updateAnnotationComplete($param = array()) {
        try {

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_updateCrmAnnotation(?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['birth_date']);
            $stmt->getResource()->bindParam(2, $param['death_date']);
            $stmt->getResource()->bindParam(3, $param['occupation']);
            $stmt->getResource()->bindParam(4, $param['spouse']);
            $stmt->getResource()->bindParam(5, $param['children']);
            $stmt->getResource()->bindParam(6, $param['us_relatives']);
            $stmt->getResource()->bindParam(7, $param['us_residence']);
            $stmt->getResource()->bindParam(8, $param['military_experience']);
            $stmt->getResource()->bindParam(9, $param['religious_community']);
            $stmt->getResource()->bindParam(10, $param['passenger_id']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            //$resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            // asd($resultSet);
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to update the main passenger table.
     * @param this will be an array.
     * @return this will return true
     * @author Icreon Tech -SR
     */
    public function updateCorrectionComplete($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_updateCorrectionPassenger(?,?,?,?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['passenger_id']);
            $stmt->getResource()->bindParam(2, addslashes($param['first_name']));
            $stmt->getResource()->bindParam(3, addslashes($param['last_name']));
            $stmt->getResource()->bindParam(4, addslashes($param['nationality']));
            $stmt->getResource()->bindParam(5, addslashes($param['last_residence']));
            $stmt->getResource()->bindParam(6, $param['arrival_age']);
            $stmt->getResource()->bindParam(7, $param['arrival_date']);
            $stmt->getResource()->bindParam(8, $param['gender']);
            $stmt->getResource()->bindParam(9, $param['maritial_status']);
            $stmt->getResource()->bindParam(10, addslashes($param['ship']));
            $stmt->getResource()->bindParam(11, addslashes($param['port_departure']));
            $stmt->getResource()->bindParam(12, $param['line']);
            $stmt->getResource()->bindParam(13, $param['birth_place']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will delete the delete the annotation and correction
     * @param this will be an array having the params.
     * @return this will return a confirmation
     * @author Icreon Tech -SR
     */
    public function deleteCrmAnnotaionCorrection($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_deleteCrmAnnotationCorrection(?,?,?)');
            $stmt->getResource()->bindParam(1, $param['id']);
            $stmt->getResource()->bindParam(2, $param['isDelete']);
            $stmt->getResource()->bindParam(3, $param['modifiend_date']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will save the search title into the table for crm campaign search
     * @param this will be an array having the search params and search title .
     * @return this will return a confirmation
     * @author Icreon Tech -AS
     */
    public function getPassengerRecord($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_crmPassengerRecords(?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['passenger_id']);
            $stmt->getResource()->bindParam(2, $param['startIndex']);
            $stmt->getResource()->bindParam(3, $param['recordLimit']);
            $stmt->getResource()->bindParam(4, $param['sortField']);
            $stmt->getResource()->bindParam(5, $param['sortOrder']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to get all ship name
     * @param array type to get the ship result.
     * @return this will return a ship name array
     * @author Icreon Tech - SR
     */
    public function getShip($param = array()) {
        if (isset($param['ship_name']) && $param['ship_name'] != '')
            $ship_name = addslashes($param['ship_name']);
        else
            $ship_name = '';

        $result = $this->connection->execute("CALL usp_pas_getShip('" . $ship_name . "')");
        $statement = $result->getResource();
        $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);

        $statement->closeCursor();
        if (!$resultSet) {
            //throw new \Exception("Could not find record.");
        }
        return $resultSet;
    }

    /**
     * This function will get the list of all roll numbers on the bases of series
     * @param this will be an array having the search params.
     * @return this will return a array of roll numbers
     * @author Icreon Tech - SR
     */
    public function getRollNumbers($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getCrmRollNbr(?)');
            $stmt->getResource()->bindParam(1, $param['series']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will get the list of all frames on bases of roll numbers
     * @param this will be an array having the search params.
     * @return this will return a array of frames
     * @author Icreon Tech - SR
     */
    public function getRollNumbersFrames($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $rollNbr = isset($param['rollNbr']) ? $param['rollNbr'] : '';
            $frame = isset($param['frame']) ? $param['frame'] : '';
            $limit = isset($param['limit']) ? $param['limit'] : '';
            $startIndex = isset($param['startIndex']) ? $param['startIndex'] : '';
            $stmt->prepare('CALL usp_pas_getCrmRollNbrFrame(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $rollNbr);
            $stmt->getResource()->bindParam(2, $limit);
            $stmt->getResource()->bindParam(3, $startIndex);
            $stmt->getResource()->bindParam(4, $frame);
            $result = $stmt->execute();

            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function will get the list of all arrivals dates.
     * @param this will be an array having the search params.
     * @return this will return a array of arrivals dates
     * @author Icreon Tech - SR
     */
    public function getShipArrival($param = array()) {
        try {
            $ship_name = isset($param['ship_name']) ? addslashes($param['ship_name']) : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getShipArrivalDates(?)');
            $stmt->getResource()->bindParam(1, $ship_name);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to update the passenger ethnicity
     * @return this will return confirmation
     * @author Icreon Tech - SR
     */
    public function updateCrmPassengerEthnicity($param = array()) {
        try {

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_updatePassengerEthnicity(?,?,?)');
            $stmt->getResource()->bindParam(1, $param['passenger_id']);
            $stmt->getResource()->bindParam(2, $param['ethnicity']);
            $stmt->getResource()->bindParam(3, $param['nationality']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            // $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            // print_R($resultSet);
            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to update the bulk passenger details
     * @return this will return confirmation
     * @author Icreon Tech - SR
     */
    public function updateBulkPassenger($param = array()) {
        try {

            $fileName = isset($param['file_name']) ? $param['file_name'] : '';
            $rollNo = isset($param['roll_no']) ? $param['roll_no'] : '';
            $frame = isset($param['frame']) ? $param['frame'] : '';
            $shipName = isset($param['ship_name']) ? addslashes($param['ship_name']) : '';
            $arrivalDate = isset($param['arrival_date']) ? $param['arrival_date'] : '';
            $passengerId = isset($param['passenger_id']) ? $param['passenger_id'] : '';
            $shipArrivalDate = isset($param['ship_arrival_date']) ? $param['ship_arrival_date'] : '';
            $lastName = isset($param['last_name']) ? $param['last_name'] : '';
            $manifestFileName = isset($param['manifest_file_name']) ? $param['manifest_file_name'] : '';
            $autoId = isset($param['manifest_auto_id']) ? $param['manifest_auto_id'] : '';
            $stmt = $this->dbAdapter->createStatement();
         //   asd($param);
            $stmt->prepare('CALL usp_pas_updateBulkPassenger(?,?,?,?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $fileName);
            $stmt->getResource()->bindParam(2, $rollNo);
            $stmt->getResource()->bindParam(3, $frame);
            $stmt->getResource()->bindParam(4, $shipName);
            $stmt->getResource()->bindParam(5, $arrivalDate);
            $stmt->getResource()->bindParam(6, $passengerId);
            $stmt->getResource()->bindParam(7, $shipArrivalDate);
            $stmt->getResource()->bindParam(8, $lastName);
            $stmt->getResource()->bindParam(9, $manifestFileName);
            $stmt->getResource()->bindParam(10, $autoId);

            $result = $stmt->execute();
            $statement = $result->getResource();
           // $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
          //  asd($resultSet);

            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to update the bulk passenger details
     * @return this will return confirmation
     * @author Icreon Tech - SR
     */    
public function updateCrmMultiplePassenger($param = array())
{
        try {
            $fileName = isset($param['file_name']) ? trim($param['file_name']) : '';
            $shipName = isset($param['ship_name']) ? addslashes(trim($param['ship_name'])) : '';
            $arrivalDate = isset($param['arrival_date']) ? $param['arrival_date'] : '';
            $passengerId = isset($param['passenger_id']) ? $param['passenger_id'] : '';
            $shipArrivalDate = isset($param['ship_arrival_date']) ? $param['ship_arrival_date'] : '';
            $lastName = isset($param['last_name']) ? trim($param['last_name']) : '';
            $autoId = isset($param['MANIMAGE_AUTOID']) ? $param['MANIMAGE_AUTOID'] : '';
            
            $stmt = $this->dbAdapter->createStatement();
            //asd($param);
            $stmt->prepare('CALL usp_pas_updateMultiplePassenger(?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $fileName);
            $stmt->getResource()->bindParam(2, $shipName);
            $stmt->getResource()->bindParam(3, $arrivalDate);
            $stmt->getResource()->bindParam(4, $passengerId);
            $stmt->getResource()->bindParam(5, $shipArrivalDate);
            $stmt->getResource()->bindParam(6, $lastName);
            $stmt->getResource()->bindParam(7, $autoId);

            $result = $stmt->execute();
            $statement = $result->getResource();

            $statement->closeCursor();
            return true;
        } catch (Exception $e) {
            return false;
        }    
    
}
    
    /**
     * This function is used to update the bulk passenger details
     * @return this will return confirmation
     * @author Icreon Tech - SR
     */
    public function checkValidRollNumber($param = array()) {
        try {
            $rollNbr = isset($param['roll_nbr']) ? $param['roll_nbr'] : '';
            $frameNbr = isset($param['frame_no']) ? $param['frame_no'] : '';
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_checkValidRollNumber(?,?)');
            $stmt->getResource()->bindParam(1, $rollNbr);
            $stmt->getResource()->bindParam(2, $frameNbr);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * This function is used to check the dupliacte annotation check by a user
     * @param this will be an array.
     * @author Icreon Tech - SR
     */
    public function checkPassengerAnnotationDuplicate($param = array()) {
        try {
            $userId = isset($param['userId']) ? $param['userId'] : '';
            $passengerId = isset($param['passengerId']) ? $param['passengerId'] : '';
            $activityType = isset($param['activityType']) ? $param['activityType'] : '';
            $annotationCorrectionId = isset($param['annotationCorrectionId']) ? $param['annotationCorrectionId'] : '';

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_checkPassengerAnnotation(?,?,?,?)');
            $stmt->getResource()->bindParam(1, $userId);
            $stmt->getResource()->bindParam(2, $passengerId);
            $stmt->getResource()->bindParam(3, $activityType);
            $stmt->getResource()->bindParam(4, $annotationCorrectionId);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    
    
    /**
     * This function will fetch the passenger manifest image
     * @param this will be an array having the search Params.
     * @return this will return a passenger manifest
     * @author Icreon Tech -SR
     */
    public function getManifestDetails($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getManifestDetails(?)');
            $stmt->getResource()->bindParam(1, $param['manifestImage']);
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }    
    /**
     * This function will insert the passenger information in main image table
     * @param this will be an array.
     * @return this will return a confirmation
     * @author Icreon Tech -SR
     */
    public function insertCrmPassengerMainImage($param = array()) {
        try {

            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_insertPassengerMainImage(?,?,?,?,?,?,?)');
            $stmt->getResource()->bindParam(1, $param['file_name']);
            $stmt->getResource()->bindParam(2, $param['roll_number']);
            $stmt->getResource()->bindParam(3, $param['frame']);
            $stmt->getResource()->bindParam(4, $param['arrival_date']);
            $stmt->getResource()->bindParam(5, $param['page_nbr']);
            $stmt->getResource()->bindParam(6, $param['ship_name']);
            $stmt->getResource()->bindParam(7, $param['data_source']);

            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            if ($resultSet) {
                return $resultSet[0]['LAST_INSERT_ID'];
            } else {
                return false;
            }            
            
        } catch (Exception $e) {
            return false;
        }
    }
}
