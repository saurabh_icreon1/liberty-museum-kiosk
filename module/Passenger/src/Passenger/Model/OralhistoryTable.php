<?php

/**
 * This is used for Passenger module.
 * @package    Passenger
 * @author     Icreon Tech -SR.
 */

namespace Passenger\Model;

use Zend\Db\TableGateway\TableGateway;

/**
 * This class is used for OralHistory module.
 * @author     Icreon Tech - SK.
 */
class OralhistoryTable {

    protected $tableGateway;
    protected $connection;
    protected $dbAdapter;

    /**
     * This is used for dafault initialization of objects.
     * @author     Icreon Tech -DG
     */
    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
        $this->dbAdapter = $this->tableGateway->getAdapter();
        $this->connection = $this->tableGateway->getAdapter()->getDriver()->getConnection();
    }

    /**
     * This function will return country list
     * @return this will return arrry
     * @author Icreon Tech - SK
     */
    public function getCountryList($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getCountryList()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * This function will return topic list
     * @return this will return arrry
     * @author Icreon Tech - SK
     */
    public function getTopicList($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getTopicList()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * This function will return type list
     * @return this will return arrry
     * @author Icreon Tech - SK
     */
    public function getTypeList($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getTypeList()');
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }
    
    /**
     * This function will return oral histories
     * @return this will return arrry
     * @author Icreon Tech - SK
     */
    public function getOralHistories($param = array()) {
        try {
            $stmt = $this->dbAdapter->createStatement();
            $stmt->prepare('CALL usp_pas_getOralHistory(?,?,?,?,?,?,?,?,?)');
            
            $first_name = (isset($param['first_name']) && $param['first_name']!='')?$param['first_name']:'';
            $last_name = (isset($param['last_name']) && $param['last_name']!='')?$param['last_name']:'';
            $type_id = (isset($param['type_id']) && $param['type_id']!='')?$param['type_id']:'';
            $country_id = (isset($param['country_id']) && $param['country_id']!='')?$param['country_id']:'';
            $topic_id = (isset($param['topic']) && $param['topic']!='')?$param['topic']:'';
            
            $start_index = (isset($param['start_index']) && $param['start_index'] != '' && !is_null($param['start_index'])) ? $param['start_index'] : '';
            $record_limit = (isset($param['record_limit']) && $param['record_limit'] != '' && !is_null($param['record_limit'])) ? $param['record_limit'] : '';
            $sort_field = (isset($param['sort_field']) && $param['sort_field'] != '' && !is_null($param['sort_field'])) ? $param['sort_field'] : '';
            $sort_order = (isset($param['sort_order']) && $param['sort_order'] != '' && !is_null($param['sort_order'])) ? $param['sort_order'] : '';
            
            
            $stmt->getResource()->bindParam(1, $first_name);
            $stmt->getResource()->bindParam(2, $last_name);
            $stmt->getResource()->bindParam(3, $type_id);
            $stmt->getResource()->bindParam(4, $country_id);
            $stmt->getResource()->bindParam(5, $topic_id);
            $stmt->getResource()->bindParam(6, $start_index);
            $stmt->getResource()->bindParam(7, $record_limit);
            $stmt->getResource()->bindParam(8, $sort_field);
            $stmt->getResource()->bindParam(9, $sort_order);
            
            $result = $stmt->execute();
            $statement = $result->getResource();
            $resultSet = $statement->fetchAll(\PDO::FETCH_ASSOC);
            $statement->closeCursor();
            return $resultSet;
        } catch (Exception $e) {
            return false;
        }
    }

}