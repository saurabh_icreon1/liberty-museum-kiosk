<?php

/**
 * This is used for Passenger module.
 * @package    Passenger
 * @author     Icreon Tech -SR.
 */

namespace Passenger\Model;

use Base\Model\BaseModel;
use Passenger\Module;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Db\Adapter\Adapter;

/**
 * This is used for Passenger module.
 * @author     Icreon Tech -SR.
 */
class Document extends BaseModel {

    protected $inputFilter;
    protected $searchParam;
    protected $inputFilterCrmSearch;
    protected $adapter;
    public $batch;
    public $series;
    public $roll_number;
    public $ref_line;
    public $ref_page;
    public $arrival_date;
    public $last_name;
    public $first_name;
    public $print_age;
    public $birth_year;
    public $gender;
    public $maritial_status;
    public $residence;
    public $ship;
    public $ship_name_iss;
    public $ship_arrival_date;
    public $passenger_id;
    public $nationality;
    public $filename;

    /**
     * This is used for dafault initialization of objects.
     * @package    Document
     * @author     Icreon Tech -SR.
     */
    public function __construct(Adapter $adapter) {
        $this->adapter = $adapter;
    }

    /**
     * Function used to exchange passenger values (assignment of values)
     * @author Icreon Tech - SR
     * @return void
     * @param $data array
     */
    public function exchangeArray($data) {
        $this->batch = (isset($data['batch']) && !empty($data['batch'])) ? $data['batch'] : '';
        $this->series = (isset($data['series']) && !empty($data['series'])) ? $data['series'] : '';
        $this->roll_number = (isset($data['roll_number']) && !empty($data['roll_number'])) ? $data['roll_number'] : '';
        $this->frame = (isset($data['frame']) && !empty($data['frame'])) ? $data['frame'] : '';
        $this->ref_line = (isset($data['ref_line']) && !empty($data['ref_line'])) ? $data['ref_line'] : '';
        $this->ref_page = (isset($data['ref_page']) && !empty($data['ref_page'])) ? $data['ref_page'] : '';
        $this->arrival_date = (isset($data['arrival_date']) && !empty($data['arrival_date'])) ? $data['arrival_date'] : '';
        $this->last_name = (isset($data['last_name']) && !empty($data['last_name'])) ? $data['last_name'] : '';
        $this->first_name = (isset($data['first_name']) && !empty($data['first_name'])) ? $data['first_name'] : '';
        $this->print_age = (isset($data['print_age']) && !empty($data['print_age'])) ? $data['print_age'] : '';
        $this->birth_year = (isset($data['birth_year']) && !empty($data['birth_year'])) ? $data['birth_year'] : '';
        $this->gender = (isset($data['gender']) && !empty($data['gender'])) ? $data['gender'] : '';
        $this->maritial_status = (isset($data['maritial_status']) && !empty($data['maritial_status'])) ? $data['maritial_status'] : '';
        $this->residence = (isset($data['residence']) && !empty($data['residence'])) ? $data['residence'] : '';
        $this->ship = (isset($data['ship']) && !empty($data['ship'])) ? $data['ship'] : '';
        $this->ship_name_iss = (isset($data['ship_name_iss']) && !empty($data['ship_name_iss'])) ? $data['ship_name_iss'] : '';
        $this->ship_arrival_date = (isset($data['ship_arrival_date']) && !empty($data['ship_arrival_date'])) ? $data['ship_arrival_date'] : '';
        $this->passenger_id = (isset($data['passenger_id']) && !empty($data['passenger_id'])) ? $data['passenger_id'] : '';
        $this->nationality = (isset($data['nationality']) && !empty($data['nationality'])) ? $data['nationality'] : '';
        $this->file_name = (isset($data['file_name']) && !empty($data['file_name'])) ? $data['file_name'] : '';
        $this->us_citizen = (isset($data['us_citizen']) && !empty($data['us_citizen'])) ? $data['us_citizen'] : '';
        $this->crew_member = (isset($data['crew_member']) && !empty($data['crew_member'])) ? $data['crew_member'] : '';
        $this->ethnicity = (isset($data['ethnicity']) && !empty($data['ethnicity'])) ? $data['ethnicity'] : '';
        $this->place_of_birth = (isset($data['place_of_birth']) && !empty($data['place_of_birth'])) ? $data['place_of_birth'] : '';
        $this->date_source = (isset($data['date_source']) && !empty($data['date_source'])) ? $data['date_source'] : '';

        $this->age_arrival = (isset($data['age_arrival']) && !empty($data['age_arrival'])) ? $data['age_arrival'] : '';
        $this->port_of_departure = (isset($data['port_of_departure']) && !empty($data['port_of_departure'])) ? trim($data['port_of_departure']) : '';
    }

    /**
     * Function used to assign the values
     * @author Icreon Tech - SR
     * @return $returnArray
     * @param $dataArr array
     */
    public function assignData($dataArr = array()) {
        $returnArray = array();
        $returnArray['ship_id'] = isset($dataArr['ship_id']) ? $dataArr['ship_id'] : '';
        //  $returnArray['shiplineId'] = isset($dataArr['ship_id']) ? $dataArr['shiplineId'] : '';
        $returnArray['ship_name'] = isset($dataArr['ship_name']) ? $dataArr['ship_name'] : '';
        $returnArray['engine_type'] = isset($dataArr['engine_type']) ? $dataArr['engine_type'] : '';
        $returnArray['builder'] = isset($dataArr['builder']) ? $dataArr['builder'] : '';
        $returnArray['build_from_date'] = isset($dataArr['build_from_date']) ? $dataArr['build_from_date'] : '';
        $returnArray['build_to_date'] = isset($dataArr['build_to_date']) ? $dataArr['build_to_date'] : '';
        $returnArray['scrap_from_date'] = isset($dataArr['scrap_from_date']) ? $dataArr['scrap_from_date'] : '';
        $returnArray['scrap_to_date'] = isset($dataArr['scrap_to_date']) ? $dataArr['scrap_to_date'] : '';
        $arrival_year_array = isset($dataArr['arrival_year']) ? explode(' - ', $dataArr['arrival_year']) : '';
        if (isset($dataArr['arrival_year']) && $dataArr['arrival_year'] != '') {
            $returnArray['arrival_year_start'] = $arrival_year_array[0] . '-01-01 00:00:00';
            $returnArray['arrival_year_end'] = $arrival_year_array[1] . '-12-31 23:59:59';
        } else {
            $returnArray['arrival_year_start'] = '';
            $returnArray['arrival_year_end'] = '';
        }
        $returnArray['month_arrival'] = isset($dataArr['month_arrival']) ? $dataArr['month_arrival'] : '';
        $returnArray['day_arrival'] = isset($dataArr['day_arrival']) ? $dataArr['day_arrival'] : '';
        $returnArray['shipline_name'] = isset($dataArr['shipline_name']) ? $dataArr['shipline_name'] : '';

        return $returnArray;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

    /**
     * Function used to check validation for passenger
     * @author Icreon Tech - SR
     * @return boolean
     * @param Array
     */
    public function getInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'batch',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'BATCH_MSG',
                                    ),
                                ),
                            )
                        ),
                    )));



            $inputFilter->add($factory->createInput(array(
                        'name' => 'series',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'nationality',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'file_name',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'roll_number',
                        'required' => false, //true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'ROLL_NUMBER_MSG',
                                    ),
                                ),
                            )
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'frame',
                        'required' => false, //true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'FRAME_MSG',
                                    ),
                                ),
                            )
                        ),
                    )));



            $inputFilter->add($factory->createInput(array(
                        'name' => 'arrival_date',
                        'required' => false, //true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'ARRIVAL_DATE_MSG',
                                    ),
                                ),
                            )
                        ),
                    )));


            $inputFilter->add($factory->createInput(array(
                        'name' => 'last_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'LAST_NAME_MSG',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'messages' => array(
                                        'stringLengthTooShort' => "LAST_NAME_MIN_MSG",
                                        'stringLengthTooLong' => "LAST_NAME_MAX_MSG",
                                    ),
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 20,
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'first_name',
                        'required' => false, //true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'FIRST_NAME_MSG',
                                    ),
                                ),
                            ),
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'messages' => array(
                                        'stringLengthTooShort' => 'FIRST_NAME_MIN_MSG',
                                        'stringLengthTooLong' => 'FIRST_NAME_MAX_MSG',
                                    ),
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 50,
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'birth_year',
                        'required' => false, //true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'BIRTH_YEAR_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'birth_year',
                        'required' => false, //true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'BIRTH_YEAR_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'maritial_status',
                        'required' => false, //true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'MARITAL_STATUS_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'gender',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'residence',
                        'required' => false, //true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'RESIDENCE_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'ship',
                        'required' => false, //true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'SHIP_MSG',
                                    ),
                                ),
                            ),
                        ),
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'us_citizen',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'port_of_departure',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));            
            
            $inputFilter->add($factory->createInput(array(
                        'name' => 'crew_member',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'data_source',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                    )));            
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * Function used to check validation for ship
     * @author Icreon Tech - SR
     * @return boolean
     * @param Array
     */
    public function getShipInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'name_when_built',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'NAME_WHEN_BUILT_MSG',
                                    ),
                                ),
                            )
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'gross_weight',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'length',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'beam',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'engine_type',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'num_screws',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'scrapped_country',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'builder',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'service_speed_knots',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'scrapped_month',
                        'required' => false
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'scrapped_day',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'build_year',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'BUILD_YEAR_MSG',
                                    ),
                                ),
                            )
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'scrapped_year',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'SCRAPPED_YEAR_MSG',
                                    ),
                                ),
                            )
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'build_city',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'BUILD_CITY_MSG',
                                    ),
                                ),
                            )
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'build_country',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'BUILD_COUNTRY_MSG',
                                    ),
                                ),
                            )
                        ),
                    )));


            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * Function used to check validation for annotation
     * @author Icreon Tech - SR
     * @return boolean
     * @param Array
     */
    public function getAnnotationInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'first_name',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'last_name',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'nationality',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'last_residence',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'arrival_age',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'gender',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'marital_status',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'ship',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'port_departure',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'line',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'birth_date',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'death_date',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'occupation',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'spouse',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'children',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'us_relatives',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'us_residence',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'military_experience',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'religious_community',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'note',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'email_send',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'status',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'terms',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'first_name_status',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'last_name_status',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'nationality_status',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'last_residence_status',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'age_at_arrival_status',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'arrival_date_status',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'gender_status',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'maritial_approve_status',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'ship_name_status',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'departure_port_name_status',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'line_no_status',
                        'required' => false,
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'birth_place_status',
                        'required' => false,
                    )));


            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * Function used to pass for input filter
     * @author Icreon Tech - SR
     * @return array
     * @param Array
     */
    public function getInputFilterCrmSearch($filterData) {
        $inputFilterData = array();
        if (is_array($filterData)) {
            foreach ($filterData as $key => $val) {
                $inputFilterData[$key] = trim(addslashes(strip_tags($val)));
            }
        } else {
            $inputFilterData = trim(addslashes(strip_tags($filterData)));
        }
        return $this->inputFilterCrmSearch = $inputFilterData;
    }

    /**
     * Function used to exchange ship values (assignment of values)
     * @author Icreon Tech - SR
     * @return $returnArray
     * @param $data array
     */
    public function exchangeShipArray($data) {
        $this->name_when_built = (isset($data['name_when_built']) && !empty($data['name_when_built'])) ? $data['name_when_built'] : '';
        $this->gross_weight = (isset($data['gross_weight']) && !empty($data['gross_weight'])) ? $data['gross_weight'] : '';
        $this->length = (isset($data['length']) && !empty($data['length'])) ? $data['length'] : '';
        $this->beam = (isset($data['beam']) && !empty($data['beam'])) ? $data['beam'] : '';
        $this->engine_type = (isset($data['engine_type']) && !empty($data['engine_type'])) ? $data['engine_type'] : '';
        $this->num_screws = (isset($data['num_screws']) && !empty($data['num_screws'])) ? $data['num_screws'] : '';
        $this->scrapped_country = (isset($data['scrapped_country']) && !empty($data['scrapped_country'])) ? $data['scrapped_country'] : '';
        $this->builder = (isset($data['builder']) && !empty($data['builder'])) ? $data['builder'] : '';
        $this->service_speed_knots = (isset($data['service_speed_knots']) && !empty($data['service_speed_knots'])) ? $data['service_speed_knots'] : '';
        $this->scrapped_month = (isset($data['scrapped_month']) && !empty($data['scrapped_month'])) ? $data['scrapped_month'] : '';
        $this->scrapped_day = (isset($data['scrapped_day']) && !empty($data['scrapped_day'])) ? $data['scrapped_day'] : '';
        $this->build_year = (isset($data['build_year']) && !empty($data['build_year'])) ? $data['build_year'] : '';
        $this->scrapped_year = (isset($data['scrapped_year']) && !empty($data['scrapped_year'])) ? $data['scrapped_year'] : '';
        $this->build_city = (isset($data['build_city']) && !empty($data['build_city'])) ? $data['build_city'] : '';
        $this->build_country = (isset($data['build_country']) && !empty($data['build_country'])) ? $data['build_country'] : '';
        $this->ship_name_iss = (isset($data['ship_name_iss']) && !empty($data['ship_name_iss'])) ? $data['ship_name_iss'] : '';
        $this->ship_id = (isset($data['ship_id']) && !empty($data['ship_id'])) ? $data['ship_id'] : '';
        $this->shipline_id = (isset($data['shipline_id']) && !empty($data['shipline_id'])) ? $data['shipline_id'] : '';
        $this->updated_on = (isset($data['updated_on']) && !empty($data['updated_on'])) ? $data['updated_on'] : '';
        $this->is_edited_image = (isset($data['is_edited_image']) && !empty($data['is_edited_image'])) ? $data['is_edited_image'] : '';
    }

    public function getArrayCopy() {
        return get_object_vars($this);
    }

    /**
     * Function used to check validation for shipline
     * @author Icreon Tech - SR
     * @return boolean
     * @param Array
     */
    public function getShiplineInputFilter() {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'NAME_MSG',
                                    ),
                                ),
                            )
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'linename_id',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'total_pass',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'asset_id',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'flag',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'home_city',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'home_country',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        )
                    )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'start_year',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'START_YEAR_MSG',
                                    ),
                                ),
                            )
                        ),
                    )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'end_year',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'NotEmpty',
                                'options' => array(
                                    'messages' => array(
                                        'isEmpty' => 'END_YEAR_MSG',
                                    ),
                                ),
                            )
                        ),
                    )));



            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    /**
     * Function used to exchange ship name values for bulk update (assignment of values)
     * @author Icreon Tech - SK
     * @return $returnArray
     * @param $data array
     */
    public function exchangeShipUpdateArray($data) {
        $this->old_ship_name = (isset($data['old_ship_name']) && !empty($data['old_ship_name'])) ? $data['old_ship_name'] : '';
        $this->new_ship_name = (isset($data['new_ship_name']) && !empty($data['new_ship_name'])) ? $data['new_ship_name'] : '';
    }

    /**
     * Function used to exchange Annotation Param the values
     * @author Icreon Tech - SR
     * @return $returnArray
     * @param $dataArr array
     */
    public function exchangeAnnotationParam($dataArr = array()) {
        $returnArray = array();

        $returnArray['contact_name_id'] = isset($dataArr['contact_name_id']) ? $dataArr['contact_name_id'] : '';
        $returnArray['passenger_id'] = isset($dataArr['passenger_id']) ? $dataArr['passenger_id'] : '';
        $returnArray['first_name'] = isset($dataArr['first_name']) ? $dataArr['first_name'] : '';
        $returnArray['last_name'] = isset($dataArr['last_name']) ? $dataArr['last_name'] : '';
        $returnArray['p_year_of_birth_start'] = isset($dataArr['p_year_of_birth_start']) ? $dataArr['p_year_of_birth_start'] : '';
        $returnArray['p_year_of_birth_end'] = isset($dataArr['p_year_of_birth_end']) ? $dataArr['p_year_of_birth_end'] : '';
        $returnArray['p_current_age_start'] = isset($dataArr['p_current_age_start']) ? $dataArr['p_current_age_start'] : '';
        $returnArray['p_current_age_end'] = isset($dataArr['p_current_age_end']) ? $dataArr['p_current_age_end'] : '';
        $returnArray['type'] = isset($dataArr['type']) ? $dataArr['type'] : '';
        $returnArray['marital_status'] = isset($dataArr['marital_status']) ? $dataArr['marital_status'] : '';
        $returnArray['gender'] = isset($dataArr['gender']) ? $dataArr['gender'] : '';
        $returnArray['p_arrival_year_start'] = isset($dataArr['p_arrival_year_start']) ? $dataArr['p_arrival_year_start'] : '';
        $returnArray['p_arrival_year_end'] = isset($dataArr['p_arrival_year_end']) ? $dataArr['p_arrival_year_end'] : '';
        $returnArray['arrival_month'] = isset($dataArr['arrival_month']) ? $dataArr['arrival_month'] : '';
        $returnArray['arrival_day'] = isset($dataArr['arrival_day']) ? $dataArr['arrival_day'] : '';
        $returnArray['last_residence'] = isset($dataArr['last_residence']) ? $dataArr['last_residence'] : '';
        $returnArray['dept_port'] = isset($dataArr['dept_port']) ? $dataArr['dept_port'] : '';
        $returnArray['arrival_port'] = isset($dataArr['arrival_port']) ? $dataArr['arrival_port'] : '';
        $returnArray['ethnicity'] = isset($dataArr['ethnicity']) ? $dataArr['ethnicity'] : '';
        $returnArray['ship'] = isset($dataArr['ship']) ? $dataArr['ship'] : '';
        $returnArray['annotation_correction_id'] = isset($dataArr['annotation_correction_id']) ? $dataArr['annotation_correction_id'] : '';
        $returnArray['arrival_date'] = isset($dataArr['arrival_date']) ? $dataArr['arrival_date'] : '';

        $returnArray['startIndex'] = isset($dataArr['startIndex']) ? $dataArr['startIndex'] : '';
        $returnArray['recordLimit'] = isset($dataArr['recordLimit']) ? $dataArr['recordLimit'] : '';
        $returnArray['sortField'] = isset($dataArr['sortField']) ? $dataArr['sortField'] : '';
        $returnArray['sortOrder'] = isset($dataArr['sortOrder']) ? $dataArr['sortOrder'] : '';

        return $returnArray;
    }

}